package com.salmon.sdk.core.a;

import com.salmon.sdk.a.g;
import com.salmon.sdk.b.a;
import com.salmon.sdk.b.c;
import com.salmon.sdk.c.k;
import com.salmon.sdk.d.i;
import java.util.ArrayList;

final class d implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f6170a;

    d(c cVar) {
        this.f6170a = cVar;
    }

    public final void a() {
        i.c(c.f6165a, "load ad start");
    }

    public final void a(Object obj) {
        boolean unused = this.f6170a.f6169e = false;
        a aVar = (a) obj;
        if (aVar != null && aVar.b() != null && aVar.b().size() > 0 && aVar.b().get(0) != null) {
            ArrayList arrayList = new ArrayList();
            for (c next : aVar.b()) {
                if (this.f6170a.f6168d.f6069d == com.salmon.sdk.b.i.f6078a || next.m()) {
                    arrayList.add(next);
                } else {
                    com.salmon.sdk.a.c.a(g.a(this.f6170a.f6167b)).a(next);
                }
            }
            if (arrayList.size() > 0) {
                j.a(this.f6170a.f6167b).a(arrayList, false, false, true, 0, false);
            }
        }
    }

    public final void b() {
        boolean unused = this.f6170a.f6169e = false;
    }

    public final void c() {
        boolean unused = this.f6170a.f6169e = false;
    }
}
