package com.salmon.sdk.core.a;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.salmon.sdk.a.b;
import com.salmon.sdk.a.g;
import com.salmon.sdk.b.a;
import com.salmon.sdk.b.c;
import com.salmon.sdk.b.e;
import com.salmon.sdk.b.f;
import com.salmon.sdk.c.d;
import com.salmon.sdk.c.k;
import com.salmon.sdk.c.l;
import com.salmon.sdk.core.MainService;
import com.salmon.sdk.d.h;
import com.salmon.sdk.d.i;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class j {
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public static String f6185f = j.class.getSimpleName();

    /* renamed from: h  reason: collision with root package name */
    private static j f6186h;

    /* renamed from: a  reason: collision with root package name */
    b f6187a;

    /* renamed from: b  reason: collision with root package name */
    boolean f6188b = false;

    /* renamed from: c  reason: collision with root package name */
    HashMap<String, l> f6189c = new HashMap<>();

    /* renamed from: d  reason: collision with root package name */
    boolean f6190d = false;

    /* renamed from: e  reason: collision with root package name */
    int f6191e = 0;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public Context f6192g;
    /* access modifiers changed from: private */
    public f i;

    private j(Context context) {
        this.f6192g = context;
        try {
            this.i = v.a(this.f6192g).b().a(f.f6066a);
        } catch (Throwable th) {
        }
        this.f6187a = b.a(g.a(context));
    }

    public static j a(Context context) {
        if (f6186h == null) {
            f6186h = new j(context);
        }
        return f6186h;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.c.a.a(com.salmon.sdk.c.k, boolean):void
     arg types: [com.salmon.sdk.core.a.l, int]
     candidates:
      com.salmon.sdk.c.d.a(java.util.Map, byte[]):java.lang.Object
      com.salmon.sdk.c.a.a(java.util.Map<java.lang.String, java.util.List<java.lang.String>>, byte[]):T
      com.salmon.sdk.c.a.a(com.salmon.sdk.c.k, boolean):void */
    private synchronized void a(String str, boolean z, boolean z2) {
        int i2 = 0;
        synchronized (this) {
            i.b("rush", " getReferFromNetByPkgName()");
            if (!TextUtils.isEmpty(str)) {
                if (!str.contains("com.google") && !str.contains("com.android") && !str.contains("samsung")) {
                    this.i = v.a(this.f6192g).b().a(f.f6066a);
                    this.f6188b = false;
                    d dVar = new d(this.f6192g, g.a(this.f6192g), "lite", a.f6036a);
                    dVar.a(com.salmon.sdk.d.l.b(this.f6192g, com.salmon.sdk.core.a.f6148a, "APPID", Integer.parseInt(com.salmon.sdk.core.a.j)));
                    dVar.c(com.salmon.sdk.d.l.b(this.f6192g, com.salmon.sdk.core.a.f6148a, "APPKEY", com.salmon.sdk.core.a.k));
                    dVar.d(str);
                    dVar.g();
                    if (this.i.w == 1) {
                        i2 = b.a(g.a(this.f6192g)).b(str).size();
                    }
                    if (z2) {
                        i.b("rush", "download load ad start pkgname: " + str);
                        dVar.b(i2 + this.i.s);
                    } else {
                        i.b("rush", "install load ad start pkgname: " + str);
                        dVar.b(i2 + this.i.t);
                    }
                    if (z) {
                        dVar.a("lite");
                    } else {
                        dVar.a("wap");
                    }
                    dVar.b("full_screen");
                    com.salmon.sdk.b.j a2 = v.a(this.f6192g).b().a();
                    if (a2 != null && !TextUtils.isEmpty(a2.f6084a)) {
                        dVar.e(a2.f6084a);
                    }
                    dVar.a((k) new l(this, z2, str, z), false);
                    if (this.i != null && z) {
                        new Handler(Looper.getMainLooper()).postDelayed(new m(this, str), (long) this.i.n);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, String str2, String str3) {
        try {
            if (this.i == null || this.i.l != com.salmon.sdk.b.i.f6079b) {
                i.b("rush", "refers: " + str2 + "   pkgName" + str);
                com.salmon.sdk.d.k.a(this.f6192g, str, str2);
                c(str, str2, str3);
                return;
            }
            i.b("rush", " bf_status:off ");
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.core.a.j.a(java.lang.String, boolean, boolean):void
     arg types: [java.lang.String, int, int]
     candidates:
      com.salmon.sdk.core.a.j.a(java.lang.String, java.lang.String, java.lang.String):void
      com.salmon.sdk.core.a.j.a(java.lang.String, boolean, boolean):void */
    private synchronized void c(String str) {
        a(str, true, false);
    }

    private void c(String str, String str2, String str3) {
        String[] split = str3.split(";;");
        i.b("rush", "send camapign count:" + split.length);
        if (Build.VERSION.SDK_INT >= 22) {
            for (String str4 : split) {
                com.salmon.sdk.d.a.a.a(1004312, "session_id=" + String.valueOf(com.salmon.sdk.d.a.a.b(String.valueOf(str4))) + "&campaign_id=" + str4 + "&type=rush&pkg=" + str);
                com.salmon.sdk.d.a.a.c(String.valueOf(str4));
            }
        }
        new Thread(new r(this, str, str2, split)).start();
    }

    static /* synthetic */ boolean c(j jVar) {
        return jVar.i != null && jVar.i.r == 1;
    }

    private boolean d(String str) {
        try {
            if (this.f6187a == null) {
                return false;
            }
            return System.currentTimeMillis() - Long.parseLong(this.f6187a.a(str)) >= (this.i == null ? 432000000 : (long) this.i.m);
        } catch (Exception e2) {
            return false;
        }
    }

    public final void a() {
        try {
            this.i = v.a(this.f6192g).b().a(f.f6066a);
            if (this.i != null) {
                for (e next : com.salmon.sdk.a.f.a(g.a(this.f6192g)).b()) {
                    if (System.currentTimeMillis() - next.c() > this.i.f6072g) {
                        com.salmon.sdk.a.f.a(g.a(this.f6192g)).a(String.valueOf(next.d()));
                    }
                }
            }
        } catch (Error | Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.c.a.a(com.salmon.sdk.c.k, boolean):void
     arg types: [com.salmon.sdk.core.a.k, int]
     candidates:
      com.salmon.sdk.c.i.a(java.util.Map<java.lang.String, java.util.List<java.lang.String>>, byte[]):java.lang.Object
      com.salmon.sdk.c.a.a(java.util.Map<java.lang.String, java.util.List<java.lang.String>>, byte[]):T
      com.salmon.sdk.c.a.a(com.salmon.sdk.c.k, boolean):void */
    public final void a(com.salmon.sdk.b.b bVar) {
        if (this.i == null) {
            i.e(f6185f, "mAppStrategy==null return");
        } else if (this.i == null || this.i.j != com.salmon.sdk.b.i.f6079b) {
            new com.salmon.sdk.c.i(this.f6192g, bVar).a((k) new k(this), false);
        } else {
            i.b("rush", "down rf_status:off");
        }
    }

    public final void a(String str) {
        String str2;
        try {
            this.i = v.a(this.f6192g).b().a(f.f6066a);
            if (this.i != null && this.i.f6068c == com.salmon.sdk.b.i.f6079b) {
                i.b("rush", "down status:off");
            } else if (!TextUtils.isEmpty(str) && !h.a(str)) {
                h.f6178b.add(str);
                if (this.i == null || this.i.j != com.salmon.sdk.b.i.f6079b) {
                    if (!d(str)) {
                        List<e> b2 = com.salmon.sdk.a.f.a(g.a(this.f6192g)).b(str, (long) this.i.m);
                        if (b2.size() > 0) {
                            i.b("rush", "rush refer pkgname: " + str);
                            String str3 = "";
                            String str4 = "";
                            if (this.i.u != 1) {
                                if (this.i.u != 0) {
                                    i.b("rush", "download click send latest refer----");
                                    Iterator<e> it = b2.iterator();
                                    while (true) {
                                        if (!it.hasNext()) {
                                            str2 = str3;
                                            break;
                                        }
                                        e next = it.next();
                                        if (next.b() != null) {
                                            String b3 = next.b();
                                            str4 = String.valueOf(next.d());
                                            str2 = b3;
                                            break;
                                        }
                                        int b4 = com.salmon.sdk.d.l.b(this.f6192g, com.salmon.sdk.core.a.f6148a, "APPID", Integer.parseInt(com.salmon.sdk.core.a.j));
                                        com.salmon.sdk.b.d dVar = new com.salmon.sdk.b.d(String.valueOf(next.d()), next.a(), System.currentTimeMillis());
                                        h.a(this.f6192g, b4);
                                        h.d().add(dVar);
                                        h.a(this.f6192g, b4).c();
                                    }
                                } else {
                                    i.b("rush", "download click send all refer----");
                                    StringBuffer stringBuffer = new StringBuffer();
                                    StringBuffer stringBuffer2 = new StringBuffer();
                                    for (e next2 : b2) {
                                        if (next2.b() != null) {
                                            stringBuffer.append(next2.b());
                                            stringBuffer.append(";;");
                                            stringBuffer2.append(String.valueOf(next2.d()));
                                            stringBuffer2.append(";;");
                                        }
                                        int b5 = com.salmon.sdk.d.l.b(this.f6192g, com.salmon.sdk.core.a.f6148a, "APPID", Integer.parseInt(com.salmon.sdk.core.a.j));
                                        com.salmon.sdk.b.d dVar2 = new com.salmon.sdk.b.d(String.valueOf(next2.d()), next2.a(), System.currentTimeMillis());
                                        h.a(this.f6192g, b5);
                                        h.d().add(dVar2);
                                        h.a(this.f6192g, b5).c();
                                    }
                                    if (!TextUtils.isEmpty(stringBuffer.toString())) {
                                        str3 = stringBuffer.substring(0, (stringBuffer.length() - 1) - 2);
                                    }
                                    str4 = !TextUtils.isEmpty(stringBuffer2.toString()) ? stringBuffer2.substring(0, (stringBuffer2.length() - 1) - 2) : str4;
                                    str2 = str3;
                                }
                            } else {
                                i.b("rush", "download click send earliest refer----");
                                int size = b2.size() - 1;
                                while (true) {
                                    if (size < 0) {
                                        str2 = str3;
                                        break;
                                    } else if (b2.get(size).b() != null) {
                                        String b6 = b2.get(size).b();
                                        str4 = String.valueOf(b2.get(size).d());
                                        str2 = b6;
                                        break;
                                    } else {
                                        int b7 = com.salmon.sdk.d.l.b(this.f6192g, com.salmon.sdk.core.a.f6148a, "APPID", Integer.parseInt(com.salmon.sdk.core.a.j));
                                        com.salmon.sdk.b.d dVar3 = new com.salmon.sdk.b.d(String.valueOf(b2.get(size).d()), b2.get(size).a(), System.currentTimeMillis());
                                        h.a(this.f6192g, b7);
                                        h.d().add(dVar3);
                                        h.a(this.f6192g, b7).c();
                                        size--;
                                    }
                                }
                            }
                            b(str, str2, str4);
                            if (this.i.p == 1) {
                                i.b("rush", "add rushClickMode=1");
                                return;
                            }
                        }
                    }
                    c b8 = com.salmon.sdk.a.c.a(g.a(this.f6192g)).b(str);
                    if (b8 == null) {
                        c(str);
                        return;
                    }
                    i.b("rush", "add cache offer");
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(b8);
                    a(arrayList, true, false, false, 0, true);
                    com.salmon.sdk.a.c.a(g.a(this.f6192g)).a(b8.a());
                    i.b(f6185f, "本地有缓存的上报.下载.....");
                    com.salmon.sdk.c.e.a(this.f6192g, true, this.i.y == 1, str, this.i.x, this.i.z, this.i.A);
                    return;
                }
                i.b("rush", "add rf_status:off");
            }
        } catch (Exception e2) {
        } catch (Error e3) {
        }
    }

    public final void a(String str, long j, String str2, boolean z) {
        e eVar = new e();
        eVar.a(str);
        eVar.b(str2);
        eVar.b(j);
        eVar.a(System.currentTimeMillis());
        eVar.a(z);
        com.salmon.sdk.a.f.a(g.a(this.f6192g)).a(eVar);
    }

    public final void a(String str, String str2, String str3) {
        com.salmon.sdk.d.k.a(this.f6192g, str2, str3);
        com.salmon.sdk.d.j.a(this.f6192g, str2);
        c(str2, str3, str);
    }

    public final void a(String str, boolean z, boolean z2, int i2, int i3) {
        d dVar = new d(this.f6192g, g.a(this.f6192g), "lite", a.f6036a);
        dVar.a(com.salmon.sdk.d.l.b(this.f6192g, com.salmon.sdk.core.a.f6148a, "APPID", Integer.parseInt(com.salmon.sdk.core.a.j)));
        dVar.c(com.salmon.sdk.d.l.b(this.f6192g, com.salmon.sdk.core.a.f6148a, "APPKEY", com.salmon.sdk.core.a.k));
        dVar.d(str);
        dVar.g();
        dVar.b(i2);
        if (!z2) {
            i.b("rush", "install reload ad pkgname: " + str);
            dVar.a("lite");
        } else {
            i.b("rush", "donwload reload ad pkgname: " + str);
            dVar.a("wap");
        }
        dVar.b("full_screen");
        com.salmon.sdk.b.j a2 = v.a(this.f6192g).b().a();
        if (a2 != null && !TextUtils.isEmpty(a2.f6084a)) {
            dVar.e(a2.f6084a);
        }
        dVar.a(new t(this, z2, str, i3, z));
    }

    public final void a(List<c> list, boolean z, boolean z2, boolean z3, int i2, boolean z4) {
        if (list != null && list.size() != 0) {
            if (i2 < list.size()) {
                c cVar = list.get(i2);
                n nVar = new n(this, list, i2, z2, z, z3, z4);
                if (cVar != null) {
                    if (z2) {
                        com.salmon.sdk.d.a.a.a(1004310, "campaign_id=" + cVar.b() + "&type=rush&pkg=" + cVar.f() + "&extra=2");
                    } else {
                        com.salmon.sdk.d.a.a.a(1004310, "campaign_id=" + cVar.b() + "&type=rush&pkg=" + cVar.f() + "extra=1&msg1=" + h.a(this.f6192g, cVar.f()) + "&msg2=" + h.b(this.f6192g, cVar.f()));
                    }
                    if (!TextUtils.isEmpty(cVar.e())) {
                        MainService.b(new o(this, cVar));
                    }
                    if (!z2) {
                        int b2 = com.salmon.sdk.d.l.b(this.f6192g, com.salmon.sdk.core.a.f6148a, "APPID", Integer.parseInt(com.salmon.sdk.core.a.j));
                        com.salmon.sdk.b.d dVar = new com.salmon.sdk.b.d(String.valueOf(cVar.b()), cVar.f(), System.currentTimeMillis());
                        h.a(this.f6192g, b2);
                        h.d().add(dVar);
                        h.a(this.f6192g, b2).c();
                    }
                    com.salmon.sdk.d.a.a.a(String.valueOf(cVar.b()));
                    MainService.a(new p(this, cVar.a(), cVar, nVar));
                }
            } else if (z) {
                i.b("rush", "install click list has finish click");
            } else {
                i.b("rush", "download click list has finish click");
            }
        }
    }

    public final void b() {
        b.a(g.a(this.f6192g)).b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.core.a.j.a(java.lang.String, boolean, boolean):void
     arg types: [java.lang.String, int, int]
     candidates:
      com.salmon.sdk.core.a.j.a(java.lang.String, java.lang.String, java.lang.String):void
      com.salmon.sdk.core.a.j.a(java.lang.String, boolean, boolean):void */
    public final void b(String str) {
        try {
            this.i = v.a(this.f6192g).b().a(f.f6066a);
            if (this.i == null || this.i.f6068c != com.salmon.sdk.b.i.f6079b) {
                e.a(this.f6192g);
                if (!e.a(str)) {
                    try {
                        com.salmon.sdk.d.c.a.a().a((com.salmon.sdk.d.c.c) new g(e.a(this.f6192g), str));
                    } catch (Throwable th) {
                    }
                    if (!d(str)) {
                        i.b(f6185f, "filter by clicktime:" + str);
                    } else if (TextUtils.isEmpty(str) || h.a(str)) {
                        i.b("rush", "down app installed pkgname: " + str);
                    } else if (this.i == null || this.i.j != com.salmon.sdk.b.i.f6079b) {
                        c b2 = this.i.q == com.salmon.sdk.b.i.f6078a ? com.salmon.sdk.a.c.a(g.a(this.f6192g)).b(str) : null;
                        if (b2 == null) {
                            i.b("rush", "down rushFromNetByPkgName: " + str);
                            a(str, false, true);
                        } else if (this.i.p != 2) {
                            i.b("rush", "down click pkgName: " + str);
                            ArrayList arrayList = new ArrayList();
                            arrayList.add(b2);
                            a(arrayList, false, true, false, 0, false);
                            com.salmon.sdk.a.c.a(g.a(this.f6192g)).a(b2.a());
                            i.b(f6185f, "本地有缓存的上报.下载.....");
                            com.salmon.sdk.c.e.a(this.f6192g, true, this.i.y == 1, str, this.i.x, this.i.z, this.i.A);
                        } else {
                            i.b("rush", "down rushClickMode=2 pkgName: " + str);
                        }
                    } else {
                        i.b("rush", "down rf_status:off");
                    }
                }
            } else {
                i.b("rush", "down status:off");
            }
        } catch (Exception e2) {
        } catch (Error e3) {
        }
    }
}
