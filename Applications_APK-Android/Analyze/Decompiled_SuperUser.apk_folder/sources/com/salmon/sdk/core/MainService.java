package com.salmon.sdk.core;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import com.salmon.sdk.d.i;
import com.salmon.sdk.d.l;

public class MainService extends Service {

    /* renamed from: a  reason: collision with root package name */
    static Handler f6143a;

    /* renamed from: b  reason: collision with root package name */
    j f6144b;

    /* renamed from: c  reason: collision with root package name */
    e f6145c;

    /* renamed from: d  reason: collision with root package name */
    d f6146d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final String f6147e = MainService.class.getSimpleName();

    private void a() {
        this.f6145c = new e(getApplicationContext());
    }

    static /* synthetic */ void a(MainService mainService) {
        if (a.f6149b) {
            i.b(mainService.f6147e, "Service tick--->");
            try {
                if (mainService.f6145c == null) {
                    mainService.a();
                }
                mainService.f6145c.a(mainService);
            } catch (Error | Exception e2) {
            }
        }
    }

    public static void a(Runnable runnable) {
        f6143a.post(runnable);
    }

    public static void b(Runnable runnable) {
        f6143a.postDelayed(runnable, 2000);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        try {
            Log.i(this.f6147e, "Service onCreate   --->");
            super.onCreate();
            if (f6143a == null) {
                f6143a = new i(this, Looper.getMainLooper());
            }
            if (this.f6145c == null) {
                a();
            }
            if (this.f6144b == null || this.f6144b.isInterrupted()) {
                this.f6144b = new j(this);
                this.f6144b.start();
            }
            this.f6146d = new d(f6143a);
            this.f6146d.f6246a = this;
            getContentResolver().registerContentObserver(Uri.parse(a.f6155h), true, this.f6146d);
        } catch (Error | Exception e2) {
        }
    }

    public void onDestroy() {
        getContentResolver().unregisterContentObserver(this.f6146d);
        Intent intent = new Intent();
        intent.setClass(this, MainService.class);
        startService(intent);
        super.onDestroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, int):void
     arg types: [com.salmon.sdk.core.MainService, java.lang.String, java.lang.String, int]
     candidates:
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, long):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, int):void */
    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent != null) {
            try {
                String stringExtra = intent.getStringExtra("CMD");
                i.c(this.f6147e, "cmd onstartCommand" + stringExtra);
                if (!TextUtils.isEmpty(stringExtra) && stringExtra.equals("SDK_STATUS")) {
                    a.f6149b = intent.getBooleanExtra("STATUS", true);
                    return 1;
                } else if ("SDK_INIT".equals(stringExtra)) {
                    int intExtra = intent.getIntExtra("APPID", Integer.parseInt(a.j));
                    String stringExtra2 = intent.getStringExtra("APPKEY");
                    l.a((Context) this, a.f6148a, "APPID", intExtra);
                    l.a(this, a.f6148a, "APPKEY", stringExtra2);
                    return 1;
                }
            } catch (Exception e2) {
            } catch (Error e3) {
            }
        }
        if (a.f6149b) {
            if (this.f6145c == null) {
                a();
            }
            if (intent != null) {
                String stringExtra3 = intent.getStringExtra("CMD");
                i.c(this.f6147e, "cmd onstartCommand" + stringExtra3);
                if (this.f6145c != null) {
                    this.f6145c.a(this, stringExtra3, intent);
                }
            }
        }
        return 1;
    }
}
