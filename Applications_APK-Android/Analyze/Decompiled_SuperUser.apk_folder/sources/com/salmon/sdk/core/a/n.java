package com.salmon.sdk.core.a;

import android.text.TextUtils;
import com.salmon.sdk.a.b;
import com.salmon.sdk.a.g;
import com.salmon.sdk.b.c;
import com.salmon.sdk.d.i;
import java.util.List;

final class n implements u {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f6200a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ int f6201b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ boolean f6202c;

    /* renamed from: d  reason: collision with root package name */
    final /* synthetic */ boolean f6203d;

    /* renamed from: e  reason: collision with root package name */
    final /* synthetic */ boolean f6204e;

    /* renamed from: f  reason: collision with root package name */
    final /* synthetic */ boolean f6205f;

    /* renamed from: g  reason: collision with root package name */
    final /* synthetic */ j f6206g;

    n(j jVar, List list, int i, boolean z, boolean z2, boolean z3, boolean z4) {
        this.f6206g = jVar;
        this.f6200a = list;
        this.f6201b = i;
        this.f6202c = z;
        this.f6203d = z2;
        this.f6204e = z3;
        this.f6205f = z4;
    }

    public final void a(String str, long j, boolean z, boolean z2) {
        int i;
        try {
            String f2 = ((c) this.f6200a.get(this.f6201b)).f();
            if (z) {
                if (this.f6202c) {
                    i.b("rush", "download click success and save refer id:" + j);
                } else {
                    if (this.f6203d) {
                        if (this.f6206g.i.v == 1 && this.f6201b == 0) {
                            this.f6206g.b(f2, str, String.valueOf(j));
                        } else if (this.f6206g.i.v != 1) {
                            this.f6206g.b(f2, str, String.valueOf(j));
                        }
                    }
                    i.b("rush", "install click success and save refer id:" + j);
                }
                if (!TextUtils.isEmpty(str)) {
                    this.f6206g.a(f2, j, str, this.f6203d);
                }
            }
            if (this.f6204e) {
                if (this.f6202c) {
                    i.b("rush", "download newload ad list click,current index = " + this.f6201b + " id:" + j);
                } else {
                    i.b("rush", "install newload ad list click,current index = " + this.f6201b + " id:" + j);
                }
                int i2 = this.f6201b + 1;
                if (this.f6202c && h.a(f2)) {
                    i2 = this.f6200a.size();
                }
                this.f6206g.a(this.f6200a, this.f6203d, this.f6202c, this.f6204e, i2, false);
                return;
            }
            if (this.f6202c) {
                i.b("rush", "donwload save ad click----- pkgname:" + f2);
                i = this.f6206g.i.s;
            } else {
                i.b("rush", "install save ad click----- pkgname:" + f2);
                i = this.f6206g.i.t;
            }
            int i3 = i - 1;
            int size = i + b.a(g.a(this.f6206g.f6192g)).b(f2).size();
            if (!this.f6202c && j.c(this.f6206g) && z2 && this.f6205f) {
                i.b("rush", "installSave ad click retry--- id:" + j);
                this.f6206g.a(this.f6200a, this.f6203d, this.f6202c, this.f6204e, 0, false);
            } else if (i3 > 0 && size > 0) {
                if (this.f6202c) {
                    i.b("rush", "download reload campaign------- pkgname:" + f2);
                } else {
                    i.b("rush", "install reload campaign------- pkgname:" + f2);
                }
                this.f6206g.a(f2, this.f6203d, this.f6202c, size, i3);
            }
        } catch (Exception e2) {
        } catch (Error e3) {
        }
    }
}
