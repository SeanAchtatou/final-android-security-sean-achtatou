package com.salmon.sdk.core.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.salmon.sdk.b.d;
import com.salmon.sdk.d.c.a;
import com.salmon.sdk.d.c.c;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e {

    /* renamed from: b  reason: collision with root package name */
    private static e f6171b = null;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public static CopyOnWriteArraySet<d> f6172d = new CopyOnWriteArraySet<>();

    /* renamed from: a  reason: collision with root package name */
    private Context f6173a;

    /* renamed from: c  reason: collision with root package name */
    private SharedPreferences f6174c;

    private e(Context context) {
        this.f6173a = context;
        a.a().a((c) new f(this));
    }

    public static e a(Context context) {
        if (f6171b == null) {
            synchronized (e.class) {
                if (f6171b == null) {
                    f6171b = new e(context);
                }
            }
        }
        return f6171b;
    }

    public static boolean a(String str) {
        if (f6172d != null && f6172d.size() > 0) {
            Iterator<d> it = f6172d.iterator();
            while (it.hasNext()) {
                d next = it.next();
                if (next.a().equalsIgnoreCase(str)) {
                    if (next.b() > System.currentTimeMillis() - 86400000) {
                        return true;
                    }
                    f6172d.remove(next);
                    return false;
                }
            }
        }
        return false;
    }

    public static CopyOnWriteArraySet<d> b() {
        if (f6172d == null) {
            f6172d = new CopyOnWriteArraySet<>();
        }
        return f6172d;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a() {
        /*
            r10 = this;
            monitor-enter(r10)
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.e.f6172d     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            if (r0 == 0) goto L_0x000d
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.e.f6172d     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            int r0 = r0.size()     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            if (r0 > 0) goto L_0x0013
        L_0x000d:
            java.util.concurrent.CopyOnWriteArraySet r0 = r10.c()     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            com.salmon.sdk.core.a.e.f6172d = r0     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
        L_0x0013:
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.e.f6172d     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            if (r0 == 0) goto L_0x0043
            java.util.concurrent.CopyOnWriteArraySet r1 = new java.util.concurrent.CopyOnWriteArraySet     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            r1.<init>()     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.e.f6172d     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
        L_0x0022:
            boolean r0 = r2.hasNext()     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            if (r0 == 0) goto L_0x0045
            java.lang.Object r0 = r2.next()     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            com.salmon.sdk.b.d r0 = (com.salmon.sdk.b.d) r0     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            long r4 = r0.b()     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            r8 = 86400000(0x5265c00, double:4.2687272E-316)
            long r6 = r6 - r8
            int r3 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r3 <= 0) goto L_0x0022
            r1.add(r0)     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            goto L_0x0022
        L_0x0042:
            r0 = move-exception
        L_0x0043:
            monitor-exit(r10)
            return
        L_0x0045:
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.e.f6172d     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            if (r0 == 0) goto L_0x004e
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.e.f6172d     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            r0.clear()     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
        L_0x004e:
            int r0 = r1.size()     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            if (r0 <= 0) goto L_0x0059
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.e.f6172d     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            r0.addAll(r1)     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
        L_0x0059:
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.e.f6172d     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            r10.a(r0)     // Catch:{ Throwable -> 0x0042, all -> 0x005f }
            goto L_0x0043
        L_0x005f:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.core.a.e.a():void");
    }

    public final void a(Set<d> set) {
        if (this.f6173a != null) {
            if (set == null || set.size() <= 0) {
                try {
                    this.f6174c = this.f6173a.getSharedPreferences("downing", 0);
                    SharedPreferences.Editor edit = this.f6174c.edit();
                    edit.remove("_downing");
                    edit.commit();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                try {
                    String a2 = d.a(set);
                    this.f6174c = this.f6173a.getSharedPreferences("downing", 0);
                    SharedPreferences.Editor edit2 = this.f6174c.edit();
                    edit2.putString("_downing", a2);
                    edit2.commit();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        }
    }

    public final CopyOnWriteArraySet<d> c() {
        if (this.f6173a == null) {
            return null;
        }
        this.f6174c = this.f6173a.getSharedPreferences("downing", 0);
        CopyOnWriteArraySet<d> copyOnWriteArraySet = new CopyOnWriteArraySet<>();
        String string = this.f6174c.getString("_downing", "");
        if (TextUtils.isEmpty(string)) {
            return copyOnWriteArraySet;
        }
        try {
            JSONArray jSONArray = new JSONArray(string);
            for (int i = 0; i < jSONArray.length(); i++) {
                d dVar = new d();
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                dVar.a(jSONObject.optString("campaignId"));
                dVar.b(jSONObject.optString("packageName"));
                dVar.a(jSONObject.optLong("updateTime"));
                copyOnWriteArraySet.add(dVar);
            }
            return copyOnWriteArraySet;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return copyOnWriteArraySet;
        }
    }
}
