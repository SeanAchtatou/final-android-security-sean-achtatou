package com.salmon.sdk.core;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.salmon.sdk.b.b;
import com.salmon.sdk.core.a.h;
import com.salmon.sdk.core.a.j;
import com.salmon.sdk.core.a.v;
import com.salmon.sdk.d.c.c;
import com.salmon.sdk.d.l;
import io.fabric.sdk.android.services.common.a;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class e {

    /* renamed from: a  reason: collision with root package name */
    static int f6247a = 1;

    /* renamed from: b  reason: collision with root package name */
    static int f6248b = 1;

    /* renamed from: c  reason: collision with root package name */
    static String f6249c = "";

    /* renamed from: g  reason: collision with root package name */
    private static String f6250g = "";

    /* renamed from: d  reason: collision with root package name */
    Context f6251d;

    /* renamed from: e  reason: collision with root package name */
    long f6252e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final String f6253f = e.class.getSimpleName();

    public e(Context context) {
        h.a(context.getApplicationContext(), f6247a).a();
        l.a(this.f6251d, a.f6148a, "APPLIST_LAST_UPDATE", System.currentTimeMillis());
        this.f6251d = context;
    }

    /* access modifiers changed from: private */
    public static String b(String str, String str2) {
        try {
            return Uri.parse(str).getQueryParameter(str2);
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static List<String> b(b bVar, String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(str);
        while (true) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL((String) arrayList.get(arrayList.size() - 1)).openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.addRequestProperty("Accept-Charset", "UTF-8;");
                httpURLConnection.addRequestProperty(a.HEADER_USER_AGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2.8) Firefox/3.6.8");
                httpURLConnection.addRequestProperty("Referer", "https://android.clients.google.com/");
                httpURLConnection.setConnectTimeout(30000);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                String headerField = httpURLConnection.getHeaderField("Location");
                httpURLConnection.disconnect();
                if (responseCode == 302) {
                    arrayList.add(headerField);
                }
                if (responseCode == 200) {
                    bVar.a(String.valueOf(httpURLConnection.getContentLength()));
                }
                if (responseCode != 302) {
                    return arrayList;
                }
            } catch (Exception e2) {
            }
        }
    }

    private synchronized void b(Context context) {
        if (f6247a == 1 || TextUtils.isEmpty(f6249c)) {
            f6247a = l.b(context, a.f6148a, "APPID", Integer.parseInt(a.j));
            f6249c = l.b(context, a.f6148a, "APPKEY", a.k);
            f6248b = l.b(context, a.f6148a, "RANK", 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [android.content.Context, java.lang.String, java.lang.String, long]
     candidates:
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, int):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, long):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long */
    private void c(Context context) {
        b(context);
        com.salmon.sdk.d.a.a.a(context, f6247a);
        if (System.currentTimeMillis() - l.a(this.f6251d, a.f6148a, "APPLIST_LAST_UPDATE", (Long) 0L).longValue() > 86400000) {
            com.salmon.sdk.d.c.a.a().a((c) new f(this));
            l.a(this.f6251d, a.f6148a, "APPLIST_LAST_UPDATE", System.currentTimeMillis());
        }
    }

    public final void a(Context context) {
        if (a.f6149b) {
            Log.i("fire", "tick running");
            this.f6251d = context;
            c(context);
            if (System.currentTimeMillis() - this.f6252e > 3600000) {
                j.a(this.f6251d).a();
                j.a(this.f6251d).b();
                com.salmon.sdk.core.a.c.a(this.f6251d).b();
                this.f6252e = System.currentTimeMillis();
            }
            v.a(this.f6251d).a();
            com.salmon.sdk.core.a.a.a(this.f6251d).a();
            com.salmon.sdk.core.a.c.a(this.f6251d).a();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008c, code lost:
        java.lang.System.gc();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x006f A[Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }, ExcHandler: Exception (e java.lang.Exception), Splitter:B:4:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008b A[ExcHandler: OutOfMemoryError (e java.lang.OutOfMemoryError), Splitter:B:4:0x0035] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r5, java.lang.String r6, android.content.Intent r7) {
        /*
            r4 = this;
            if (r7 == 0) goto L_0x0056
            java.lang.String r0 = r4.f6253f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "-----------------------------------------"
            r1.<init>(r2)
            java.lang.String r2 = r7.getAction()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.salmon.sdk.d.i.b(r0, r1)
            java.lang.String r0 = r4.f6253f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "ServiceImpl onStartCommand--->      CMD: "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            com.salmon.sdk.d.i.b(r0, r1)
            r4.c(r5)
            if (r6 == 0) goto L_0x0056
            java.lang.String r0 = "ADDAD"
            boolean r0 = r0.equals(r6)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            if (r0 == 0) goto L_0x0071
            java.lang.String r0 = "PKG"
            java.lang.String r0 = r7.getStringExtra(r0)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            if (r1 != 0) goto L_0x0056
            java.lang.String r1 = com.salmon.sdk.core.e.f6250g     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            boolean r1 = r1.equals(r0)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            if (r1 == 0) goto L_0x0057
            java.lang.String r0 = r4.f6253f     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            java.lang.String r1 = "触发实时安装 实时请求--重复包安装消息"
            com.salmon.sdk.d.i.b(r0, r1)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
        L_0x0056:
            return
        L_0x0057:
            com.salmon.sdk.core.e.f6250g = r0     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            java.lang.String r1 = r4.f6253f     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            java.lang.String r2 = "触发实时安装 实时请求"
            com.salmon.sdk.d.i.b(r1, r2)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            com.salmon.sdk.a.d r1 = com.salmon.sdk.a.d.a()     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            r1.a(r0)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            com.salmon.sdk.core.a.j r1 = com.salmon.sdk.core.a.j.a(r5)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            r1.a(r0)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            goto L_0x0056
        L_0x006f:
            r0 = move-exception
            goto L_0x0056
        L_0x0071:
            java.lang.String r0 = "REMOVED"
            boolean r0 = r0.equals(r6)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            if (r0 == 0) goto L_0x0090
            java.lang.String r0 = "PKG"
            java.lang.String r0 = r7.getStringExtra(r0)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            if (r1 != 0) goto L_0x0056
            java.util.List<java.lang.String> r1 = com.salmon.sdk.core.a.h.f6178b     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            r1.remove(r0)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            goto L_0x0056
        L_0x008b:
            r0 = move-exception
            java.lang.System.gc()
            goto L_0x0056
        L_0x0090:
            java.lang.String r0 = "RUN_APP"
            boolean r0 = r0.equals(r6)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            if (r0 == 0) goto L_0x00b6
            java.lang.String r0 = "PKG"
            java.lang.String r0 = r7.getStringExtra(r0)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            java.lang.String r1 = "RE"
            java.lang.String r1 = r7.getStringExtra(r1)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            java.lang.String r2 = "CID"
            java.lang.String r2 = r7.getStringExtra(r2)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            android.content.Context r3 = r4.f6251d     // Catch:{ Throwable -> 0x00b4, Exception -> 0x006f, OutOfMemoryError -> 0x008b }
            com.salmon.sdk.core.a.j r3 = com.salmon.sdk.core.a.j.a(r3)     // Catch:{ Throwable -> 0x00b4, Exception -> 0x006f, OutOfMemoryError -> 0x008b }
            r3.a(r2, r0, r1)     // Catch:{ Throwable -> 0x00b4, Exception -> 0x006f, OutOfMemoryError -> 0x008b }
            goto L_0x0056
        L_0x00b4:
            r0 = move-exception
            goto L_0x0056
        L_0x00b6:
            java.lang.String r0 = "DOWN"
            boolean r0 = r0.equals(r6)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            if (r0 == 0) goto L_0x0056
            java.lang.String r0 = "id"
            r1 = -1
            int r0 = r7.getIntExtra(r0, r1)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            if (r0 <= 0) goto L_0x0056
            com.salmon.sdk.d.c.a r1 = com.salmon.sdk.d.c.a.a()     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            com.salmon.sdk.core.g r2 = new com.salmon.sdk.core.g     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            r2.<init>(r4, r0)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            r1.a(r2)     // Catch:{ Exception -> 0x006f, OutOfMemoryError -> 0x008b, Throwable -> 0x00d4 }
            goto L_0x0056
        L_0x00d4:
            r0 = move-exception
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.core.e.a(android.content.Context, java.lang.String, android.content.Intent):void");
    }
}
