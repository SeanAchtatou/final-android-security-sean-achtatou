package com.salmon.sdk.core;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import com.salmon.sdk.d.i;
import com.salmon.sdk.d.l;
import java.util.List;

public final class d extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    Context f6246a;

    public d(Handler handler) {
        super(handler);
    }

    public final boolean deliverSelfNotifications() {
        return super.deliverSelfNotifications();
    }

    public final void onChange(boolean z) {
    }

    public final void onChange(boolean z, Uri uri) {
        int intValue;
        if (uri != null) {
            try {
                super.onChange(z, uri);
                i.b("download", "onChange uri:" + uri.toString());
                List<String> pathSegments = uri.getPathSegments();
                if (pathSegments != null && pathSegments.size() > 0) {
                    String str = pathSegments.get(pathSegments.size() - 1);
                    if (!TextUtils.isEmpty(str) && !str.contains("downloads") && (intValue = Integer.valueOf(str).intValue()) != l.b(this.f6246a, a.f6148a, "downloadId", -1) && intValue >= 0 && com.salmon.sdk.a.d.a().b(str)) {
                        l.a(this.f6246a, a.f6148a, "downloadId", intValue);
                        Intent intent = new Intent();
                        intent.setClass(this.f6246a, MainService.class);
                        intent.putExtra("CMD", "DOWN");
                        intent.putExtra("id", intValue);
                        this.f6246a.startService(intent);
                    }
                }
            } catch (Error | Exception e2) {
            }
        }
    }
}
