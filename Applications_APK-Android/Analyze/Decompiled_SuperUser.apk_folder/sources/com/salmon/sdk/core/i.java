package com.salmon.sdk.core;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class i extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainService f6259a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    i(MainService mainService, Looper looper) {
        super(looper);
        this.f6259a = mainService;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 10:
                MainService.a(this.f6259a);
                break;
        }
        super.handleMessage(message);
    }
}
