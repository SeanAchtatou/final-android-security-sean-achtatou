package com.salmon.sdk.core.a;

import com.salmon.sdk.a.d;
import com.salmon.sdk.d.i;
import java.util.ArrayList;

final class k implements com.salmon.sdk.c.k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f6193a;

    k(j jVar) {
        this.f6193a = jVar;
    }

    public final void a() {
    }

    public final void a(Object obj) {
        if (obj != null) {
            ArrayList<String> arrayList = (ArrayList) obj;
            if (arrayList == null || arrayList.size() != 1) {
                i.b(j.f6185f, "返回多个，安装的是需要上报");
            } else {
                d.a().a(true);
                i.b(j.f6185f, "返回1个，安装的是不需要上报");
            }
            if (arrayList == null || arrayList.size() <= 0) {
                i.b(j.f6185f, "no pkg get pkgsize:----------");
                return;
            }
            i.b(j.f6185f, "pkg get pkgsize:----------" + arrayList.size());
            for (String b2 : arrayList) {
                this.f6193a.b(b2);
            }
        }
    }

    public final void b() {
    }

    public final void c() {
    }
}
