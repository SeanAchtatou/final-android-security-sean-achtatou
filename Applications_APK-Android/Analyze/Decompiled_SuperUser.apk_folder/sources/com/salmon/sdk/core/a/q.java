package com.salmon.sdk.core.a;

import android.net.Uri;
import android.util.Log;
import com.salmon.sdk.b.c;
import com.salmon.sdk.c.l;
import com.salmon.sdk.c.p;
import com.salmon.sdk.d.a.a;
import com.salmon.sdk.d.i;
import com.salmon.sdk.d.k;
import java.util.Map;

final class q implements p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f6213a;

    q(p pVar) {
        this.f6213a = pVar;
    }

    public final void a(boolean z, Uri uri, boolean z2) {
        Map<String, String> a2;
        String str = null;
        try {
            l lVar = this.f6213a.f6212d.f6189c.get(String.valueOf(this.f6213a.f6210b.a()));
            if (lVar != null) {
                lVar.a();
                this.f6213a.f6212d.f6189c.remove(String.valueOf(this.f6213a.f6210b.a()));
            }
            if (!z || uri == null) {
                Log.i("rush", "click false");
                if (this.f6213a.f6211c != null) {
                    this.f6213a.f6211c.a(null, this.f6213a.f6210b.a(), false, z2);
                    return;
                }
                return;
            }
            i.a(j.f6185f, " url:" + uri.toString());
            Log.i("rush", "click true");
            j jVar = this.f6213a.f6212d;
            c cVar = this.f6213a.f6210b;
            String uri2 = uri.toString();
            if (jVar.f6187a != null) {
                jVar.f6187a.a(cVar, "", uri2);
            }
            String uri3 = uri.toString();
            if ((uri3.startsWith("market://") || uri3.startsWith("https://play.google.com/")) && (a2 = k.a(uri3)) != null && a2.containsKey("referrer")) {
                str = "referrer=" + a2.get("referrer");
            }
            if (str != null) {
                a.a(1004311, "session_id=" + String.valueOf(a.b(String.valueOf(this.f6213a.f6210b.b()))) + "&campaign_id=" + this.f6213a.f6210b.b() + "&type=rush&pkg=" + this.f6213a.f6210b.f());
            }
            if (this.f6213a.f6211c != null) {
                this.f6213a.f6211c.a(str, this.f6213a.f6210b.a(), true, z2);
            }
        } catch (Error | Exception e2) {
        }
    }
}
