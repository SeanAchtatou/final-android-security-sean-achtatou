package com.salmon.sdk.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.salmon.sdk.d.i;

public class MainReceiver extends BroadcastReceiver {
    private static void a(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, MainService.class);
        intent.putExtra("CMD", "START");
        context.startService(intent);
    }

    public void onReceive(Context context, Intent intent) {
        i.b("MainReceiver", "-----------------------------------------" + intent.getAction());
        try {
            i.b("rush", intent.getAction());
            if (intent.getAction().equals(b.f6234d) || intent.getAction().equals(b.f6235e) || intent.getAction().equals(b.f6236f) || intent.getAction().equals(b.f6232b) || b.f6231a.equals(intent.getAction())) {
                if (b.f6231a.equals(intent.getAction())) {
                    switch (intent.getIntExtra("CMD", 0)) {
                        case 1:
                            k.a(System.currentTimeMillis());
                            return;
                        case 2:
                            if (System.currentTimeMillis() - k.a() > 180000) {
                                Intent intent2 = new Intent();
                                intent2.setClass(context, MainService.class);
                                intent2.putExtra("CMD", "START");
                                context.startService(intent2);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                } else if (System.currentTimeMillis() - k.a() > 60000) {
                    a(context);
                }
            } else if (intent.getAction().equals(b.f6233c)) {
                a(context);
            } else {
                if (b.f6237g.equals(intent.getAction())) {
                    String schemeSpecificPart = intent.getData().getSchemeSpecificPart();
                    i.c("MainReceiver", "packageADD packageName = " + schemeSpecificPart);
                    if (!TextUtils.isEmpty(schemeSpecificPart)) {
                        Intent intent3 = new Intent();
                        intent3.setClass(context, MainService.class);
                        intent3.putExtra("CMD", "ADDAD");
                        intent3.putExtra("PKG", schemeSpecificPart);
                        context.startService(intent3);
                    }
                }
                if (intent.getAction().equals(b.f6238h)) {
                    String schemeSpecificPart2 = intent.getData().getSchemeSpecificPart();
                    if (!TextUtils.isEmpty(schemeSpecificPart2) && !schemeSpecificPart2.equals(context.getPackageName())) {
                        Intent intent4 = new Intent();
                        intent4.setClass(context, MainService.class);
                        intent4.putExtra("CMD", "REMOVED");
                        intent4.putExtra("PKG", schemeSpecificPart2);
                        context.startService(intent4);
                    }
                }
            }
        } catch (Exception e2) {
        } catch (Error e3) {
        }
    }
}
