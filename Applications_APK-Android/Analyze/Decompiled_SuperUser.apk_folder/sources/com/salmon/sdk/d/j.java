package com.salmon.sdk.d;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    public static String f6322a = "";

    /* renamed from: b  reason: collision with root package name */
    public static String f6323b = "";

    public static List<String> a(Context context) {
        int i = 0;
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        try {
            List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
            while (true) {
                int i2 = i;
                if (i2 >= installedPackages.size()) {
                    break;
                }
                copyOnWriteArrayList.add(installedPackages.get(i2).packageName);
                i = i2 + 1;
            }
        } catch (Exception e2) {
            i.e("PackageUtil", "get package info list error: ");
        }
        return copyOnWriteArrayList;
    }

    public static boolean a(Context context, String str) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 0);
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.setPackage(packageInfo.packageName);
            ResolveInfo next = context.getPackageManager().queryIntentActivities(intent, 0).iterator().next();
            if (next != null) {
                String str2 = next.activityInfo.packageName;
                String str3 = next.activityInfo.name;
                Intent intent2 = new Intent("android.intent.action.MAIN");
                intent2.setComponent(new ComponentName(str2, str3));
                intent2.addFlags(268435456);
                context.startActivity(intent2);
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }
}
