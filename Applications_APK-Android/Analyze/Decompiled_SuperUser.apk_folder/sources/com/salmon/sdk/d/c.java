package com.salmon.sdk.d;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.LinkedBlockingQueue;

final class c implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    boolean f6290a;

    /* renamed from: b  reason: collision with root package name */
    private final LinkedBlockingQueue<IBinder> f6291b;

    private c() {
        this.f6290a = false;
        this.f6291b = new LinkedBlockingQueue<>(1);
    }

    /* synthetic */ c(byte b2) {
        this();
    }

    public final IBinder a() {
        if (this.f6290a) {
            throw new IllegalStateException();
        }
        this.f6290a = true;
        return this.f6291b.take();
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            this.f6291b.put(iBinder);
        } catch (InterruptedException e2) {
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
    }
}
