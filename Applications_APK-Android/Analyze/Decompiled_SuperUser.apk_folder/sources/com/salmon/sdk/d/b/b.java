package com.salmon.sdk.d.b;

import android.content.Context;
import com.salmon.sdk.d.a;
import com.salmon.sdk.d.i;
import com.salmon.sdk.d.l;

final class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f6285a;

    b(Context context) {
        this.f6285a = context;
    }

    public final void run() {
        try {
            String unused = a.f6282f = a.i(this.f6285a);
            i.c("phoneid", "gaid1=" + a.f6282f);
        } catch (Exception e2) {
            i.d("phoneid", "GET ADID ERROR TRY TO GET FROM GOOGLE PLAY APP");
            try {
                new a();
                String unused2 = a.f6282f = a.a(this.f6285a).a();
            } catch (Exception e3) {
                i.d("phoneid", "GET ADID FROM GOOGLE PLAY APP ERROR");
            }
            i.c("phoneid", "gaid2=" + a.f6282f);
        }
        boolean unused3 = a.f6281e = true;
        l.a(this.f6285a, com.salmon.sdk.core.a.f6148a, "Google_ADID", a.f6282f);
        i.c("phoneid", "gaid=" + a.f6282f);
    }
}
