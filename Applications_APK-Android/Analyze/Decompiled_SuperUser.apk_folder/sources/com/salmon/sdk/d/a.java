package com.salmon.sdk.d;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import java.io.IOException;

public final class a {
    public static b a(Context context) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("Cannot be called from the main thread");
        }
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            c cVar = new c((byte) 0);
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            if (context.bindService(intent, cVar, 1)) {
                try {
                    d dVar = new d(cVar.a());
                    b bVar = new b(dVar.a(), dVar.b());
                    context.unbindService(cVar);
                    return bVar;
                } catch (Exception e2) {
                    throw e2;
                } catch (Throwable th) {
                    context.unbindService(cVar);
                    throw th;
                }
            } else {
                throw new IOException("Google Play connection failed");
            }
        } catch (Exception e3) {
            throw e3;
        }
    }
}
