package com.salmon.sdk.d.c;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class a {

    /* renamed from: b  reason: collision with root package name */
    private static a f6292b = null;

    /* renamed from: a  reason: collision with root package name */
    private final int f6293a;

    /* renamed from: c  reason: collision with root package name */
    private ExecutorService f6294c;

    /* renamed from: d  reason: collision with root package name */
    private ExecutorService f6295d;

    /* renamed from: e  reason: collision with root package name */
    private ExecutorService f6296e;

    protected a() {
        this.f6293a = 2;
        this.f6294c = null;
        this.f6295d = null;
        this.f6296e = null;
        this.f6294c = Executors.newFixedThreadPool(2);
        this.f6295d = Executors.newCachedThreadPool();
        this.f6296e = Executors.newSingleThreadExecutor();
    }

    public static a a() {
        if (f6292b == null) {
            f6292b = new a();
        }
        return f6292b;
    }

    public final void a(c cVar) {
        this.f6295d.execute(cVar);
    }

    public final void a(Runnable runnable) {
        a((c) new b(this, runnable));
    }
}
