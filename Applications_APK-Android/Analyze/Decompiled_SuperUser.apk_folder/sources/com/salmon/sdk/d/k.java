package com.salmon.sdk.d;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import com.salmon.sdk.core.a;
import java.util.HashMap;
import java.util.Map;

public final class k {
    public static Map<String, String> a(String str) {
        String[] split = str.split("[?]");
        if (str.length() <= 1 || split.length <= 1 || split[1] == null) {
            return null;
        }
        String str2 = split[1];
        HashMap hashMap = new HashMap();
        for (String split2 : str2.split("[&]")) {
            String[] split3 = split2.split("[=]");
            if (split3.length > 1) {
                hashMap.put(split3[0], split3[1]);
            } else if (!"".equals(split3[0])) {
                hashMap.put(split3[0], "");
            }
        }
        return hashMap;
    }

    public static void a(Context context, String str, String str2) {
        if (TextUtils.isEmpty(str2)) {
            i.b("rush", "referStr == null-------no send---");
            return;
        }
        try {
            String[] split = str2.split(";;");
            i.b("rush", "send refer count:" + split.length);
            for (String str3 : split) {
                try {
                    Intent intent = new Intent(f.b(a.i));
                    if (Build.VERSION.SDK_INT >= 13) {
                        intent.addFlags(32);
                    }
                    intent.putExtra(new String(Base64.decode("cmVmZXJyZXI=".getBytes(), 0)), str2);
                    intent.setPackage(str);
                    i.b("SA", "pkg/cls is:" + str + "/" + ((String) null));
                    i.b("SA", "r is " + str3);
                    context.sendBroadcast(intent);
                    i.b("SA", "sent.");
                } catch (Exception e2) {
                }
            }
        } catch (Exception e3) {
        }
    }
}
