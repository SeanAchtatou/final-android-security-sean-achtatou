package com.salmon.sdk.d.a;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.kingouser.com.util.ShellUtils;
import com.salmon.sdk.c.a;
import com.salmon.sdk.d.h;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public final class c extends a<Integer> {

    /* renamed from: d  reason: collision with root package name */
    String[] f6271d = null;

    /* renamed from: e  reason: collision with root package name */
    Context f6272e;

    /* renamed from: f  reason: collision with root package name */
    int f6273f;

    /* renamed from: g  reason: collision with root package name */
    int f6274g;

    public c(Context context, int i) {
        super(context);
        this.f6272e = context;
        this.f6274g = i;
    }

    private static String a(String str) {
        try {
            return URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Map map, byte[] bArr) {
        return Integer.valueOf(this.f6273f);
    }

    public final void a(String[] strArr, int i) {
        this.f6271d = strArr;
        this.f6273f = i;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return com.salmon.sdk.core.c.f6240b;
    }

    /* access modifiers changed from: protected */
    public final Map<String, String> c() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final byte[] d() {
        StringBuffer stringBuffer = new StringBuffer();
        for (String str : this.f6271d) {
            if (!TextUtils.isEmpty(str)) {
                stringBuffer.append(str);
                stringBuffer.append(ShellUtils.COMMAND_LINE_END);
            }
        }
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("platform=1&");
        stringBuffer2.append("os_version=").append(Build.VERSION.RELEASE).append("&");
        stringBuffer2.append("package_name=").append(h.l(this.f6272e)).append("&");
        stringBuffer2.append("app_version_name=").append(h.i(this.f6272e)).append("&");
        stringBuffer2.append("orientation=1&");
        stringBuffer2.append("brand=").append(Build.BRAND).append("&");
        stringBuffer2.append("model=").append(a(Build.MODEL)).append("&");
        stringBuffer2.append("android_id=").append(h.d(this.f6272e)).append("&");
        stringBuffer2.append("gaid=").append(com.salmon.sdk.d.b.a.a(this.f6272e)).append("&");
        stringBuffer2.append("mac=").append(h.g(this.f6272e)).append("&");
        stringBuffer2.append("imei=").append(h.a(this.f6272e)).append("&");
        stringBuffer2.append("mnc=").append(h.c(this.f6272e)).append("&");
        stringBuffer2.append("mcc=").append(h.b(this.f6272e)).append("&");
        stringBuffer2.append("network_type=").append(h.m(this.f6272e)).append("&");
        stringBuffer2.append("language=").append(h.f(this.f6272e)).append("&");
        stringBuffer2.append("timezone=").append(a(h.b())).append("&");
        stringBuffer2.append("gp_version=").append(h.n(this.f6272e)).append("&");
        stringBuffer2.append("gpsv=").append(h.o(this.f6272e)).append("&");
        stringBuffer2.append("screen_size=").append(h.j(this.f6272e) + "x" + h.k(this.f6272e)).append("&");
        stringBuffer2.append("app_id=").append(this.f6274g).append("&");
        stringBuffer2.append("sdk_version=SA_SR_3.3.0&");
        stringBuffer2.append("data=").append(a(stringBuffer.toString()));
        return stringBuffer2.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return false;
    }
}
