package com.salmon.sdk.d;

import com.lody.virtual.os.VUserInfo;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f6304a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    private static int a(char c2) {
        if (c2 >= 'A' && c2 <= 'Z') {
            return c2 - 'A';
        }
        if (c2 >= 'a' && c2 <= 'z') {
            return (c2 - 'a') + 26;
        }
        if (c2 >= '0' && c2 <= '9') {
            return (c2 - '0') + 26 + 26;
        }
        switch (c2) {
            case '+':
                return 62;
            case '/':
                return 63;
            case '=':
                return 0;
            default:
                throw new RuntimeException("unexpected code: " + c2);
        }
    }

    private static void a(String str, OutputStream outputStream) {
        int i = 0;
        int length = str.length();
        while (true) {
            if (i < length && str.charAt(i) <= ' ') {
                i++;
            } else if (i != length) {
                int a2 = (a(str.charAt(i)) << 18) + (a(str.charAt(i + 1)) << 12) + (a(str.charAt(i + 2)) << 6) + a(str.charAt(i + 3));
                outputStream.write((a2 >> 16) & VUserInfo.FLAG_MASK_USER_TYPE);
                if (str.charAt(i + 2) != '=') {
                    outputStream.write((a2 >> 8) & VUserInfo.FLAG_MASK_USER_TYPE);
                    if (str.charAt(i + 3) != '=') {
                        outputStream.write(a2 & VUserInfo.FLAG_MASK_USER_TYPE);
                        i += 4;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    public static byte[] a(String str) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(str, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            try {
                byteArrayOutputStream.close();
            } catch (IOException e2) {
                System.err.println("Error while decoding BASE64: " + e2.toString());
            }
            return byteArray;
        } catch (IOException e3) {
            throw new RuntimeException();
        }
    }
}
