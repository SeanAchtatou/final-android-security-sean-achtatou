package com.salmon.sdk.d.b;

import android.content.Context;
import android.util.Log;
import java.util.Map;

final class d {

    /* renamed from: a  reason: collision with root package name */
    private static String f6289a = "deviceKey";

    static /* synthetic */ void a(Context context, String str) {
        try {
            context.getSharedPreferences("sdk_scl_pid_config", 0).edit().putString(f6289a, str).commit();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public static Map<String, String> b(Context context) {
        String str;
        try {
            str = context.getSharedPreferences("sdk_scl_pid_config", 0).getString(f6289a, "");
        } catch (Exception e2) {
            str = null;
        }
        if (str == null || "".equals(str.trim())) {
            return null;
        }
        Log.d("phoneid", "get from sp : " + str);
        return a.a(str);
    }
}
