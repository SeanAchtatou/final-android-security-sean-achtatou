package com.salmon.sdk.d;

import android.app.ActivityManager;
import android.content.Context;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import java.util.List;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    public static int f6324a = 1;

    public static boolean a(Context context, String str) {
        try {
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService(ServiceManagerNative.ACTIVITY)).getRunningAppProcesses();
            if (runningAppProcesses != null) {
                for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                    String[] strArr = runningAppProcessInfo.pkgList;
                    int i = 0;
                    while (true) {
                        if (i < strArr.length) {
                            if (strArr[i].contains(str)) {
                                return true;
                            }
                            i++;
                        }
                    }
                }
            }
        } catch (Exception e2) {
        }
        return false;
    }

    public static <T extends String> boolean a(T t) {
        return t != null && t.length() > 0;
    }
}
