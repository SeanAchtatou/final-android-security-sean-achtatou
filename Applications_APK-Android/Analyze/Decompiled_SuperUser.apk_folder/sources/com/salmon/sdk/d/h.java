package com.salmon.sdk.d;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.lody.virtual.helper.utils.FileUtils;
import com.salmon.sdk.d.b.a;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.TimeZone;
import java.util.UUID;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static String f6314a;

    /* renamed from: b  reason: collision with root package name */
    private static String f6315b;

    /* renamed from: c  reason: collision with root package name */
    private static String f6316c;

    /* renamed from: d  reason: collision with root package name */
    private static String f6317d;

    /* renamed from: e  reason: collision with root package name */
    private static String f6318e;

    public static synchronized String a() {
        String str;
        synchronized (h.class) {
            if (TextUtils.isEmpty(f6318e)) {
                f6318e = "Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; MI 2S Build/JRO03L) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
            }
            str = f6318e;
        }
        return str;
    }

    public static String a(Context context) {
        try {
            String d2 = a.d(context);
            i.c("DeviceUtil", "imei:" + d2);
            if (d2 != null) {
                return d2;
            }
            String c2 = c();
            return c2 == null ? "" : c2;
        } catch (Exception e2) {
            return "";
        }
    }

    public static String a(Context context, String str) {
        if (str == null || "".equals(str)) {
            return "";
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 1);
            return packageInfo != null ? packageInfo.versionName : "";
        } catch (Exception e2) {
            return "";
        }
    }

    public static int b(Context context, String str) {
        if (str == null || "".equals(str)) {
            return -1;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 1);
            if (packageInfo != null) {
                return packageInfo.versionCode;
            }
            return -1;
        } catch (Exception e2) {
            return -1;
        }
    }

    public static String b() {
        return TimeZone.getDefault().getDisplayName(false, 0);
    }

    public static String b(Context context) {
        String simOperator = ((TelephonyManager) context.getSystemService("phone")).getSimOperator();
        return (simOperator == null || simOperator.length() <= 3) ? "" : simOperator.substring(0, 3);
    }

    private static String c() {
        FileOutputStream fileOutputStream;
        if (f6314a == null) {
            File file = new File(Environment.getExternalStorageDirectory().toString(), "/.a/track_id.bin");
            try {
                if (!file.exists()) {
                    UUID randomUUID = UUID.randomUUID();
                    String uuid = randomUUID.toString();
                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdirs();
                    }
                    try {
                        file.createNewFile();
                        fileOutputStream = new FileOutputStream(file);
                        try {
                            fileOutputStream.write(uuid.getBytes());
                            fileOutputStream.flush();
                            fileOutputStream.close();
                            f6314a = randomUUID.toString();
                        } catch (IOException e2) {
                            e = e2;
                            try {
                                throw e;
                            } catch (Throwable th) {
                                th = th;
                            }
                        }
                    } catch (IOException e3) {
                        e = e3;
                        fileOutputStream = null;
                        throw e;
                    } catch (Throwable th2) {
                        th = th2;
                        fileOutputStream = null;
                        if (fileOutputStream != null) {
                            fileOutputStream.close();
                        }
                        throw th;
                    }
                } else {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
                    byte[] bArr = new byte[((int) randomAccessFile.length())];
                    randomAccessFile.readFully(bArr);
                    randomAccessFile.close();
                    f6314a = new String(bArr);
                }
            } catch (IOException e4) {
                throw e4;
            } catch (IOException e5) {
                throw e5;
            } catch (IOException e6) {
            }
        }
        return f6314a == null ? "" : f6314a;
    }

    public static String c(Context context) {
        try {
            String simOperator = ((TelephonyManager) context.getSystemService("phone")).getSimOperator();
            if (simOperator != null && simOperator.length() > 3) {
                return simOperator.substring(3, simOperator.length());
            }
        } catch (Exception e2) {
        }
        return "";
    }

    public static String d(Context context) {
        try {
            String b2 = a.b(context);
            i.c("DeviceUtil", "AndroidId:" + b2);
            return b2;
        } catch (Exception e2) {
            return "";
        }
    }

    public static String e(Context context) {
        try {
            if (!TextUtils.isEmpty(null)) {
                return null;
            }
            String a2 = g.a(Settings.Secure.getString(context.getContentResolver(), "android_id"));
            return a2 == null ? "" : a2;
        } catch (Exception e2) {
            return "";
        }
    }

    public static String f(Context context) {
        return context.getResources().getConfiguration().locale.getLanguage();
    }

    public static String g(Context context) {
        try {
            String c2 = a.c(context);
            i.c("DeviceUtil", "macAddress:" + c2);
            return c2;
        } catch (Exception e2) {
            return "";
        }
    }

    public static int h(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e2) {
            return -1;
        }
    }

    public static String i(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e2) {
            return "";
        }
    }

    public static int j(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int k(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static String l(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
        } catch (Exception e2) {
            return "";
        }
    }

    public static int m(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return 0;
            }
            int type = activeNetworkInfo.getType();
            if (type == 0) {
                return activeNetworkInfo.getSubtype();
            }
            if (type == 1) {
            }
            return -1;
        } catch (Exception e2) {
            return -1;
        }
    }

    public static String n(Context context) {
        if (f6315b == null) {
            try {
                f6315b = context.getPackageManager().getPackageInfo("com.android.vending", (int) FileUtils.FileMode.MODE_IWUSR).versionName;
            } catch (PackageManager.NameNotFoundException e2) {
                f6315b = "0";
            }
        }
        return f6315b;
    }

    public static String o(Context context) {
        if (f6316c == null) {
            try {
                f6316c = context.getPackageManager().getPackageInfo("com.google.android.gms", (int) FileUtils.FileMode.MODE_IWUSR).versionName;
            } catch (PackageManager.NameNotFoundException e2) {
                f6316c = "0";
            }
        }
        return f6316c;
    }

    public static String p(Context context) {
        if (!TextUtils.isEmpty(f6317d)) {
            return f6317d;
        }
        f6317d = l.b(context, com.salmon.sdk.core.a.f6148a, "Google_ADID", "");
        i.c("DeviceUtil", "mGoogleAdvertisingId:" + f6317d);
        return f6317d;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001c A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String q(android.content.Context r4) {
        /*
            java.lang.String r1 = ""
            java.lang.String r0 = "wifi"
            java.lang.Object r0 = r4.getSystemService(r0)     // Catch:{ Exception -> 0x002c }
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0     // Catch:{ Exception -> 0x002c }
            if (r0 == 0) goto L_0x0030
            android.net.wifi.WifiInfo r0 = r0.getConnectionInfo()     // Catch:{ Exception -> 0x002c }
            if (r0 == 0) goto L_0x0030
            java.lang.String r0 = r0.getMacAddress()     // Catch:{ Exception -> 0x002c }
            if (r0 != 0) goto L_0x001f
            java.lang.String r0 = ""
        L_0x001a:
            if (r0 != 0) goto L_0x0032
            java.lang.String r0 = ""
        L_0x001e:
            return r0
        L_0x001f:
            java.lang.String r2 = ":"
            java.lang.String r3 = ""
            java.lang.String r0 = r0.replaceAll(r2, r3)     // Catch:{ Exception -> 0x002c }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x002c }
            goto L_0x001a
        L_0x002c:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0030:
            r0 = r1
            goto L_0x001a
        L_0x0032:
            java.lang.String r0 = r0.trim()
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.d.h.q(android.content.Context):java.lang.String");
    }

    public static String r(Context context) {
        String str;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            str = telephonyManager != null ? telephonyManager.getDeviceId() : null;
        } catch (Exception e2) {
            e2.printStackTrace();
            str = "";
        }
        return str == null ? "" : str.trim();
    }
}
