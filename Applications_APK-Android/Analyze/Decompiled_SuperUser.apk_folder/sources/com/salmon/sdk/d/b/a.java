package com.salmon.sdk.d.b;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;
import com.salmon.sdk.d.f;
import com.salmon.sdk.d.h;
import com.salmon.sdk.d.i;
import java.util.HashMap;
import java.util.Map;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static String f6277a = f.b("opbf1pslo3m=");

    /* renamed from: b  reason: collision with root package name */
    public static String f6278b = f.b("opbf1pVXhJjFo3RBhc==");

    /* renamed from: c  reason: collision with root package name */
    public static String f6279c = f.b("opbf1pIlAC==");

    /* renamed from: d  reason: collision with root package name */
    public static String f6280d = f.b("opbf1pYUh3+=");
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public static boolean f6281e = false;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public static String f6282f = null;

    /* renamed from: g  reason: collision with root package name */
    private static String f6283g = null;

    /* renamed from: h  reason: collision with root package name */
    private static String f6284h = null;
    private static String i = null;

    public static String a(Context context) {
        Map<String, String> g2 = g(context);
        if (g2 != null) {
            f6282f = g2.get(f6277a);
            f6283g = g2.get(f6278b);
            f6284h = g2.get(f6279c);
            i = g2.get(f6280d);
        }
        return f6282f;
    }

    static /* synthetic */ Map a(String str) {
        String[] split;
        if (str == null || "".equals(str.trim()) || (split = str.split("\\|")) == null || split.length < 4) {
            return null;
        }
        HashMap hashMap = new HashMap();
        hashMap.put(f6277a, split[0]);
        hashMap.put(f6278b, split[1]);
        hashMap.put(f6279c, split[2]);
        hashMap.put(f6280d, split[3]);
        return hashMap;
    }

    public static String b(Context context) {
        Map<String, String> g2 = g(context);
        if (g2 != null) {
            f6282f = g2.get(f6277a);
            f6283g = g2.get(f6278b);
            f6284h = g2.get(f6279c);
            i = g2.get(f6280d);
        }
        return f6283g;
    }

    public static String c(Context context) {
        Map<String, String> g2 = g(context);
        if (g2 != null) {
            f6282f = g2.get(f6277a);
            f6283g = g2.get(f6278b);
            f6284h = g2.get(f6279c);
            i = g2.get(f6280d);
        }
        return f6284h;
    }

    public static String d(Context context) {
        Map<String, String> g2 = g(context);
        if (g2 != null) {
            f6282f = g2.get(f6277a);
            f6283g = g2.get(f6278b);
            f6284h = g2.get(f6279c);
            i = g2.get(f6280d);
        }
        return i;
    }

    public static void e(Context context) {
        i.c("phoneid", "getAdid is called");
        new Thread(new b(context)).start();
    }

    private static synchronized Map<String, String> g(Context context) {
        Map<String, String> a2;
        synchronized (a.class) {
            a2 = d.b(context);
            if (a2 == null) {
                a2 = c.b();
                if (a2 == null) {
                    int i2 = 0;
                    if (!f6281e) {
                        e(context);
                        while (!f6281e && i2 < 10) {
                            try {
                                Thread.sleep(1000);
                                i2++;
                            } catch (InterruptedException e2) {
                                e2.printStackTrace();
                            }
                        }
                    }
                    f6282f = f6282f == null ? "" : f6282f.trim();
                    f6283g = h(context);
                    f6284h = j(context);
                    i = k(context);
                    a2 = new HashMap<>();
                    a2.put(f6277a, f6282f);
                    a2.put(f6278b, f6283g);
                    a2.put(f6279c, f6284h);
                    a2.put(f6280d, i);
                    String str = f6282f + "|" + f6283g + "|" + f6284h + "|" + i;
                    d.a(context, str);
                    boolean unused = c.b(str);
                }
            }
        }
        return a2;
    }

    private static String h(Context context) {
        try {
            if (TextUtils.isEmpty(f6283g)) {
                String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
                f6283g = string;
                if (string == null || "9774d56d682e549c".equals(f6283g)) {
                    f6283g = "";
                }
            }
        } catch (Exception e2) {
            f6283g = "";
        }
        String trim = f6283g == null ? "" : f6283g.trim();
        f6283g = trim;
        return trim;
    }

    /* access modifiers changed from: private */
    public static String i(Context context) {
        try {
            Class<?> cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
            Class<?> cls2 = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient.Info");
            return (String) cls2.getMethod("getId", new Class[0]).invoke(cls.getMethod("getAdvertisingIdInfo", Context.class).invoke(null, context).getClass(), new Object[0]);
        } catch (Exception e2) {
            throw e2;
        }
    }

    private static String j(Context context) {
        try {
            if (TextUtils.isEmpty(f6284h)) {
                f6284h = h.q(context);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            f6284h = "";
        }
        String trim = f6284h == null ? "" : f6284h.trim();
        f6284h = trim;
        return trim;
    }

    private static String k(Context context) {
        try {
            if (TextUtils.isEmpty(i)) {
                i = h.r(context);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            i = "";
        }
        String trim = i == null ? "" : i.trim();
        i = trim;
        return trim;
    }
}
