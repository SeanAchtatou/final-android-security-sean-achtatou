package com.salmon.sdk.d;

import android.content.Context;
import android.content.SharedPreferences;

public final class l {
    public static Long a(Context context, String str, String str2, Long l) {
        if (context == null) {
            return 0L;
        }
        try {
            return Long.valueOf(context.getSharedPreferences(str, 0).getLong(str2, l.longValue()));
        } catch (Throwable th) {
            return l;
        }
    }

    public static void a(Context context, String str, String str2, int i) {
        if (context != null) {
            try {
                SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
                edit.putInt(str2, i);
                edit.apply();
            } catch (Exception e2) {
            }
        }
    }

    public static void a(Context context, String str, String str2, long j) {
        if (context != null) {
            try {
                SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
                edit.putLong(str2, j);
                edit.apply();
            } catch (Exception e2) {
            }
        }
    }

    public static void a(Context context, String str, String str2, String str3) {
        if (context != null) {
            try {
                SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
                edit.putString(str2, String.valueOf(str3));
                edit.apply();
            } catch (Exception e2) {
            }
        }
    }

    public static int b(Context context, String str, String str2, int i) {
        if (context == null) {
            return 0;
        }
        try {
            return context.getSharedPreferences(str, 0).getInt(str2, i);
        } catch (Throwable th) {
            return i;
        }
    }

    public static String b(Context context, String str, String str2, String str3) {
        if (context == null) {
            return null;
        }
        try {
            return context.getSharedPreferences(str, 0).getString(str2, str3);
        } catch (Throwable th) {
            return str3;
        }
    }
}
