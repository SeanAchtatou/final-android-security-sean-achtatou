package com.salmon.sdk.d.b;

import android.os.Environment;
import com.salmon.sdk.d.f;
import java.io.File;

final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final String f6286a = f.b("LdsFkpsOh1Df");

    /* renamed from: b  reason: collision with root package name */
    private static final String f6287b = f.b("LPllk3RYApy4v7+pvD==");

    /* renamed from: c  reason: collision with root package name */
    private static String f6288c;

    static {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), f6286a);
            if (!file.exists()) {
                file.mkdirs();
            }
            f6288c = new File(file, f6287b).getAbsolutePath();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003e A[SYNTHETIC, Splitter:B:22:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x0012] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map<java.lang.String, java.lang.String> b() {
        /*
            r0 = 0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x002e, all -> 0x0039 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x002e, all -> 0x0039 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x002e, all -> 0x0039 }
            java.lang.String r4 = com.salmon.sdk.d.b.c.f6288c     // Catch:{ Exception -> 0x002e, all -> 0x0039 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x002e, all -> 0x0039 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x002e, all -> 0x0039 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x002e, all -> 0x0039 }
            java.lang.String r1 = r2.readLine()     // Catch:{ Exception -> 0x0061, all -> 0x005f }
            if (r1 == 0) goto L_0x001c
            java.lang.String r1 = r1.trim()     // Catch:{ Exception -> 0x0064, all -> 0x005f }
        L_0x001c:
            r2.close()     // Catch:{ Exception -> 0x005b }
        L_0x001f:
            if (r1 == 0) goto L_0x002d
            java.lang.String r2 = ""
            java.lang.String r3 = r1.trim()
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0042
        L_0x002d:
            return r0
        L_0x002e:
            r1 = move-exception
            r1 = r0
            r2 = r0
        L_0x0031:
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ Exception -> 0x0037 }
            goto L_0x001f
        L_0x0037:
            r2 = move-exception
            goto L_0x001f
        L_0x0039:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x003c:
            if (r2 == 0) goto L_0x0041
            r2.close()     // Catch:{ Exception -> 0x005d }
        L_0x0041:
            throw r0
        L_0x0042:
            java.lang.String r0 = "phoneid"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "get from sdcard : "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r0, r2)
            java.util.Map r0 = com.salmon.sdk.d.b.a.a(r1)
            goto L_0x002d
        L_0x005b:
            r2 = move-exception
            goto L_0x001f
        L_0x005d:
            r1 = move-exception
            goto L_0x0041
        L_0x005f:
            r0 = move-exception
            goto L_0x003c
        L_0x0061:
            r1 = move-exception
            r1 = r0
            goto L_0x0031
        L_0x0064:
            r3 = move-exception
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.d.b.c.b():java.util.Map");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001e A[SYNTHETIC, Splitter:B:12:0x001e] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0028 A[SYNTHETIC, Splitter:B:18:0x0028] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean b(java.lang.String r5) {
        /*
            r0 = 0
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x001a, all -> 0x0024 }
            java.lang.String r3 = com.salmon.sdk.d.b.c.f6288c     // Catch:{ IOException -> 0x001a, all -> 0x0024 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x001a, all -> 0x0024 }
            byte[] r2 = r5.getBytes()     // Catch:{ IOException -> 0x0032, all -> 0x0030 }
            r3 = 0
            int r4 = r2.length     // Catch:{ IOException -> 0x0032, all -> 0x0030 }
            r1.write(r2, r3, r4)     // Catch:{ IOException -> 0x0032, all -> 0x0030 }
            r1.close()     // Catch:{ IOException -> 0x0032, all -> 0x0030 }
            r0 = 1
            r1.close()     // Catch:{ Exception -> 0x002c }
        L_0x0019:
            return r0
        L_0x001a:
            r1 = move-exception
            r1 = r2
        L_0x001c:
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ Exception -> 0x0022 }
            goto L_0x0019
        L_0x0022:
            r1 = move-exception
            goto L_0x0019
        L_0x0024:
            r0 = move-exception
            r1 = r2
        L_0x0026:
            if (r1 == 0) goto L_0x002b
            r1.close()     // Catch:{ Exception -> 0x002e }
        L_0x002b:
            throw r0
        L_0x002c:
            r1 = move-exception
            goto L_0x0019
        L_0x002e:
            r1 = move-exception
            goto L_0x002b
        L_0x0030:
            r0 = move-exception
            goto L_0x0026
        L_0x0032:
            r2 = move-exception
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.d.b.c.b(java.lang.String):boolean");
    }
}
