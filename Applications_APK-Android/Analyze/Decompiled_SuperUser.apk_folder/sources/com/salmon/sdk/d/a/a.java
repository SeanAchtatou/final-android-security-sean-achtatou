package com.salmon.sdk.d.a;

import android.content.Context;
import com.kingouser.com.util.ShellUtils;
import com.salmon.sdk.c.k;
import com.salmon.sdk.d.i;
import com.salmon.sdk.d.l;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f6263a = a.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static String f6264b = "";

    /* renamed from: c  reason: collision with root package name */
    private static String f6265c = "";
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public static Context f6266d = null;

    /* renamed from: e  reason: collision with root package name */
    private static c f6267e = null;

    /* renamed from: f  reason: collision with root package name */
    private static HashMap<String, Long> f6268f = new HashMap<>();

    /* renamed from: g  reason: collision with root package name */
    private static File f6269g = null;

    /* renamed from: h  reason: collision with root package name */
    private static FileWriter f6270h = null;
    private static AtomicInteger i;
    /* access modifiers changed from: private */
    public static boolean j = false;
    private static boolean k = false;
    private static int l = 1;
    private static k m = new b();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    public static void a(int i2, String str) {
        i.a(f6263a, "EVENT: [" + i2 + "] " + str);
        if (f6269g != null && i != null) {
            try {
                if (f6270h == null) {
                    f6270h = new FileWriter(f6269g, true);
                }
                f6270h.append((CharSequence) str);
                f6270h.append((CharSequence) ("&key=" + i2));
                f6270h.append((CharSequence) ("&ts=" + String.valueOf(System.currentTimeMillis() / 1000)));
                f6270h.append((CharSequence) ShellUtils.COMMAND_LINE_END);
                f6270h.flush();
                i.incrementAndGet();
            } catch (OutOfMemoryError e2) {
                System.gc();
            } catch (Throwable th) {
            }
            c();
        }
    }

    public static void a(String str) {
        f6268f.put(str, Long.valueOf(System.currentTimeMillis()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    public static boolean a(Context context, int i2) {
        l = i2;
        f6266d = context;
        if (f6267e == null) {
            f6267e = new c(f6266d, i2);
        }
        if (i == null) {
            i = new AtomicInteger(0);
        }
        f6264b = context.getApplicationContext().getFilesDir().getAbsolutePath() + File.separator + "log" + File.separator + "sr_agent_log";
        f6265c = context.getApplicationContext().getFilesDir().getAbsolutePath() + File.separator + "log" + File.separator + "sr_temp_log";
        try {
            if (f6269g == null) {
                File file = new File(f6264b);
                f6269g = file;
                if (!file.getParentFile().exists()) {
                    f6269g.getParentFile().mkdirs();
                }
                if (!f6269g.exists()) {
                    f6269g.createNewFile();
                }
            }
            if (f6270h == null) {
                f6270h = new FileWriter(f6269g, true);
            }
            d();
            return true;
        } catch (Exception e2) {
            return false;
        } catch (OutOfMemoryError e3) {
            System.gc();
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    public static long b(String str) {
        if (!f6268f.containsKey(str)) {
            return -1;
        }
        return System.currentTimeMillis() - f6268f.get(str).longValue();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003e A[SYNTHETIC, Splitter:B:19:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0075 A[SYNTHETIC, Splitter:B:35:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0085 A[SYNTHETIC, Splitter:B:43:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0097 A[SYNTHETIC, Splitter:B:51:0x0097] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void b(int r7) {
        /*
            r0 = 0
            java.lang.Class<com.salmon.sdk.d.a.a> r3 = com.salmon.sdk.d.a.a.class
            monitor-enter(r3)
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x00b8, OutOfMemoryError -> 0x006d, Throwable -> 0x0080, all -> 0x0090 }
            java.lang.String r1 = com.salmon.sdk.d.a.a.f6265c     // Catch:{ Exception -> 0x00b8, OutOfMemoryError -> 0x006d, Throwable -> 0x0080, all -> 0x0090 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x00b8, OutOfMemoryError -> 0x006d, Throwable -> 0x0080, all -> 0x0090 }
            boolean r1 = r4.exists()     // Catch:{ Exception -> 0x00b8, OutOfMemoryError -> 0x006d, Throwable -> 0x0080, all -> 0x0090 }
            if (r1 != 0) goto L_0x0014
            r4.createNewFile()     // Catch:{ Exception -> 0x00b8, OutOfMemoryError -> 0x006d, Throwable -> 0x0080, all -> 0x0090 }
        L_0x0014:
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00b8, OutOfMemoryError -> 0x006d, Throwable -> 0x0080, all -> 0x0090 }
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ Exception -> 0x00b8, OutOfMemoryError -> 0x006d, Throwable -> 0x0080, all -> 0x0090 }
            java.io.File r5 = com.salmon.sdk.d.a.a.f6269g     // Catch:{ Exception -> 0x00b8, OutOfMemoryError -> 0x006d, Throwable -> 0x0080, all -> 0x0090 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x00b8, OutOfMemoryError -> 0x006d, Throwable -> 0x0080, all -> 0x0090 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x00b8, OutOfMemoryError -> 0x006d, Throwable -> 0x0080, all -> 0x0090 }
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ Exception -> 0x00bb, OutOfMemoryError -> 0x00b1, Throwable -> 0x00ac, all -> 0x00a5 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x00bb, OutOfMemoryError -> 0x00b1, Throwable -> 0x00ac, all -> 0x00a5 }
            r0 = 0
        L_0x0026:
            java.lang.String r5 = r1.readLine()     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            if (r5 == 0) goto L_0x0048
            int r0 = r0 + 1
            if (r0 <= r7) goto L_0x0026
            r2.append(r5)     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            java.lang.String r5 = "\n"
            r2.append(r5)     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            goto L_0x0026
        L_0x0039:
            r0 = move-exception
            r0 = r1
            r1 = r2
        L_0x003c:
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ IOException -> 0x00b6 }
            if (r0 == 0) goto L_0x0046
            r0.close()     // Catch:{ IOException -> 0x00b6 }
        L_0x0046:
            monitor-exit(r3)
            return
        L_0x0048:
            r2.flush()     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            java.util.concurrent.atomic.AtomicInteger r0 = com.salmon.sdk.d.a.a.i     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            java.util.concurrent.atomic.AtomicInteger r5 = com.salmon.sdk.d.a.a.i     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            int r5 = r5.get()     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            int r5 = r5 - r7
            r0.set(r5)     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            java.io.File r0 = com.salmon.sdk.d.a.a.f6269g     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            r0.delete()     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            java.io.File r0 = com.salmon.sdk.d.a.a.f6269g     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            r4.renameTo(r0)     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            r0 = 0
            com.salmon.sdk.d.a.a.f6270h = r0     // Catch:{ Exception -> 0x0039, OutOfMemoryError -> 0x00b4, Throwable -> 0x00af }
            r2.close()     // Catch:{ IOException -> 0x006b }
            r1.close()     // Catch:{ IOException -> 0x006b }
            goto L_0x0046
        L_0x006b:
            r0 = move-exception
            goto L_0x0046
        L_0x006d:
            r1 = move-exception
            r1 = r0
            r2 = r0
        L_0x0070:
            java.lang.System.gc()     // Catch:{ all -> 0x00aa }
            if (r2 == 0) goto L_0x0046
            r2.close()     // Catch:{ IOException -> 0x007e }
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ IOException -> 0x007e }
            goto L_0x0046
        L_0x007e:
            r0 = move-exception
            goto L_0x0046
        L_0x0080:
            r1 = move-exception
            r1 = r0
            r2 = r0
        L_0x0083:
            if (r2 == 0) goto L_0x0046
            r2.close()     // Catch:{ IOException -> 0x008e }
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ IOException -> 0x008e }
            goto L_0x0046
        L_0x008e:
            r0 = move-exception
            goto L_0x0046
        L_0x0090:
            r1 = move-exception
            r2 = r0
            r6 = r0
            r0 = r1
            r1 = r6
        L_0x0095:
            if (r2 == 0) goto L_0x009f
            r2.close()     // Catch:{ IOException -> 0x00a3 }
            if (r1 == 0) goto L_0x009f
            r1.close()     // Catch:{ IOException -> 0x00a3 }
        L_0x009f:
            throw r0     // Catch:{ all -> 0x00a0 }
        L_0x00a0:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x00a3:
            r1 = move-exception
            goto L_0x009f
        L_0x00a5:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x0095
        L_0x00aa:
            r0 = move-exception
            goto L_0x0095
        L_0x00ac:
            r2 = move-exception
            r2 = r0
            goto L_0x0083
        L_0x00af:
            r0 = move-exception
            goto L_0x0083
        L_0x00b1:
            r2 = move-exception
            r2 = r0
            goto L_0x0070
        L_0x00b4:
            r0 = move-exception
            goto L_0x0070
        L_0x00b6:
            r0 = move-exception
            goto L_0x0046
        L_0x00b8:
            r1 = move-exception
            r1 = r0
            goto L_0x003c
        L_0x00bb:
            r2 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.d.a.a.b(int):void");
    }

    private static synchronized void c() {
        synchronized (a.class) {
            if (f6267e == null) {
                f6267e = new c(f6266d, l);
            }
            if (!j && i.get() > 8) {
                j = true;
                try {
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(f6269g));
                    int i2 = 0;
                    String[] strArr = new String[50];
                    while (i2 < 50) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        strArr[i2] = readLine;
                        i2++;
                    }
                    bufferedReader.close();
                    f6267e.a(strArr, i2);
                    f6267e.a(m);
                } catch (Exception e2) {
                }
            }
        }
    }

    public static void c(String str) {
        if (f6268f.containsKey(str)) {
            f6268f.remove(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [android.content.Context, java.lang.String, java.lang.String, long]
     candidates:
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, int):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, long):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long */
    private static void d() {
        if (System.currentTimeMillis() - l.a(f6266d, com.salmon.sdk.core.a.f6148a, "LOG_SEND_TIME", (Long) 0L).longValue() > 1800000 && !j) {
            i.b("Agent", "sendLogByTime:30 minites");
            if (f6267e == null) {
                f6267e = new c(f6266d, l);
            }
            if (!j && i.get() > 0) {
                j = true;
                try {
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(f6269g));
                    int i2 = 0;
                    String[] strArr = new String[50];
                    while (i2 < 50) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        strArr[i2] = readLine;
                        i2++;
                    }
                    bufferedReader.close();
                    f6267e.a(strArr, i2);
                    f6267e.a(m);
                } catch (OutOfMemoryError e2) {
                    System.gc();
                } catch (Throwable th) {
                }
            }
        }
    }
}
