package com.salmon.sdk.d;

import android.text.TextUtils;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.HashMap;
import java.util.Map;

public class f {

    /* renamed from: a  reason: collision with root package name */
    static String f6305a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    /* renamed from: b  reason: collision with root package name */
    static String f6306b = "ceM7uVqJHjxLv/NGmR8Sib31AhokWsZnDlwQ+YdPKB9EOUXFC4t5aIpr0fg2Tyz6";

    /* renamed from: c  reason: collision with root package name */
    static String f6307c = "vSoajc7dRzpWifGyNxZnV5k+DHLYhJ46lt0U3QrgEuq8sw/XMeBAT2Fb9P1OIKmC";

    /* renamed from: d  reason: collision with root package name */
    private static final String f6308d = f.class.getSimpleName();

    /* renamed from: e  reason: collision with root package name */
    private static Map<Character, Character> f6309e;

    /* renamed from: f  reason: collision with root package name */
    private static Map<Character, Character> f6310f;

    /* renamed from: g  reason: collision with root package name */
    private static Map<Character, Character> f6311g = null;

    /* renamed from: h  reason: collision with root package name */
    private static char[] f6312h = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private static byte[] i;
    private static char[] j = f6305a.toCharArray();
    private static char[] k = f6306b.toCharArray();

    static {
        f6309e = null;
        f6310f = null;
        byte[] bArr = new byte[FileUtils.FileMode.MODE_IWUSR];
        // fill-array-data instruction
        bArr[0] = -1;
        bArr[1] = -1;
        bArr[2] = -1;
        bArr[3] = -1;
        bArr[4] = -1;
        bArr[5] = -1;
        bArr[6] = -1;
        bArr[7] = -1;
        bArr[8] = -1;
        bArr[9] = -1;
        bArr[10] = -1;
        bArr[11] = -1;
        bArr[12] = -1;
        bArr[13] = -1;
        bArr[14] = -1;
        bArr[15] = -1;
        bArr[16] = -1;
        bArr[17] = -1;
        bArr[18] = -1;
        bArr[19] = -1;
        bArr[20] = -1;
        bArr[21] = -1;
        bArr[22] = -1;
        bArr[23] = -1;
        bArr[24] = -1;
        bArr[25] = -1;
        bArr[26] = -1;
        bArr[27] = -1;
        bArr[28] = -1;
        bArr[29] = -1;
        bArr[30] = -1;
        bArr[31] = -1;
        bArr[32] = -1;
        bArr[33] = -1;
        bArr[34] = -1;
        bArr[35] = -1;
        bArr[36] = -1;
        bArr[37] = -1;
        bArr[38] = -1;
        bArr[39] = -1;
        bArr[40] = -1;
        bArr[41] = -1;
        bArr[42] = -1;
        bArr[43] = 62;
        bArr[44] = -1;
        bArr[45] = -1;
        bArr[46] = -1;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = 61;
        bArr[58] = -1;
        bArr[59] = -1;
        bArr[60] = -1;
        bArr[61] = -1;
        bArr[62] = -1;
        bArr[63] = -1;
        bArr[64] = -1;
        bArr[65] = 0;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = 10;
        bArr[76] = 11;
        bArr[77] = 12;
        bArr[78] = 13;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -1;
        bArr[92] = -1;
        bArr[93] = -1;
        bArr[94] = -1;
        bArr[95] = -1;
        bArr[96] = -1;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        bArr[123] = -1;
        bArr[124] = -1;
        bArr[125] = -1;
        bArr[126] = -1;
        bArr[127] = -1;
        i = bArr;
        int length = f6305a.length();
        f6309e = new HashMap(length);
        f6310f = new HashMap(length);
        for (int i2 = 0; i2 < length; i2++) {
            f6309e.put(Character.valueOf(j[i2]), Character.valueOf(k[i2]));
            f6310f.put(Character.valueOf(k[i2]), Character.valueOf(j[i2]));
        }
    }

    private f() {
    }

    public static String a(String str) {
        char[] charArray;
        try {
            if (!TextUtils.isEmpty(str) && (charArray = a(str.getBytes()).toCharArray()) != null && charArray.length > 0) {
                char[] cArr = new char[charArray.length];
                for (int i2 = 0; i2 < charArray.length; i2++) {
                    char c2 = charArray[i2];
                    cArr[i2] = (f6309e.containsKey(Character.valueOf(c2)) ? f6309e.get(Character.valueOf(c2)) : Character.valueOf(c2)).charValue();
                }
                return new String(cArr);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }

    private static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = bArr.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                break;
            }
            int i3 = i2 + 1;
            byte b2 = bArr[i2] & 255;
            if (i3 == length) {
                stringBuffer.append(f6312h[b2 >>> 2]);
                stringBuffer.append(f6312h[(b2 & 3) << 4]);
                stringBuffer.append("==");
                break;
            }
            int i4 = i3 + 1;
            byte b3 = bArr[i3] & 255;
            if (i4 == length) {
                stringBuffer.append(f6312h[b2 >>> 2]);
                stringBuffer.append(f6312h[((b2 & 3) << 4) | ((b3 & 240) >>> 4)]);
                stringBuffer.append(f6312h[(b3 & 15) << 2]);
                stringBuffer.append("=");
                break;
            }
            i2 = i4 + 1;
            byte b4 = bArr[i4] & 255;
            stringBuffer.append(f6312h[b2 >>> 2]);
            stringBuffer.append(f6312h[((b2 & 3) << 4) | ((b3 & 240) >>> 4)]);
            stringBuffer.append(f6312h[((b3 & 15) << 2) | ((b4 & 192) >>> 6)]);
            stringBuffer.append(f6312h[b4 & 63]);
        }
        return stringBuffer.toString();
    }

    public static String b(String str) {
        Exception exc;
        String str2;
        String str3 = "";
        try {
            if (TextUtils.isEmpty(str)) {
                return str3;
            }
            char[] charArray = str.toCharArray();
            if (charArray != null && charArray.length > 0) {
                char[] cArr = new char[charArray.length];
                for (int i2 = 0; i2 < charArray.length; i2++) {
                    char c2 = charArray[i2];
                    cArr[i2] = (f6310f.containsKey(Character.valueOf(c2)) ? f6310f.get(Character.valueOf(c2)) : Character.valueOf(c2)).charValue();
                }
                str3 = new String(cArr);
            }
            try {
                return new String(d(str3));
            } catch (Exception e2) {
                Exception exc2 = e2;
                str2 = str3;
                exc = exc2;
            }
        } catch (Exception e3) {
            Exception exc3 = e3;
            str2 = str3;
            exc = exc3;
            exc.printStackTrace();
            return str2;
        }
    }

    public static String c(String str) {
        char[] charArray;
        try {
            if (!TextUtils.isEmpty(str) && (charArray = a(str.getBytes()).toCharArray()) != null && charArray.length > 0) {
                char[] cArr = new char[charArray.length];
                for (int i2 = 0; i2 < charArray.length; i2++) {
                    char c2 = charArray[i2];
                    if (f6311g == null) {
                        f6311g = new HashMap();
                        for (int i3 = 0; i3 < "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".length(); i3++) {
                            f6311g.put(Character.valueOf("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(i3)), Character.valueOf("vSoajc7dRzpWifGyNxZnV5k+DHLYhJ46lt0U3QrgEuq8sw/XMeBAT2Fb9P1OIKmC".charAt(i3)));
                        }
                    }
                    cArr[i2] = (f6311g.containsKey(Character.valueOf(c2)) ? f6311g.get(Character.valueOf(c2)) : Character.valueOf(c2)).charValue();
                }
                return new String(cArr);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006c A[LOOP:0: B:1:0x000e->B:26:0x006c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0075 A[EDGE_INSN: B:32:0x0075->B:27:0x0075 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0075 A[EDGE_INSN: B:35:0x0075->B:27:0x0075 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0075 A[EDGE_INSN: B:36:0x0075->B:27:0x0075 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0075 A[EDGE_INSN: B:37:0x0075->B:27:0x0075 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x001e A[LOOP:2: B:6:0x001e->B:30:0x007e, LOOP_START, PHI: r1 
      PHI: (r1v1 int) = (r1v0 int), (r1v11 int) binds: [B:5:0x001c, B:30:0x007e] A[DONT_GENERATE, DONT_INLINE]] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] d(java.lang.String r9) {
        /*
            r8 = 61
            r7 = -1
            byte[] r2 = r9.getBytes()
            int r3 = r2.length
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream
            r4.<init>(r3)
            r0 = 0
        L_0x000e:
            if (r0 >= r3) goto L_0x0075
        L_0x0010:
            byte[] r5 = com.salmon.sdk.d.f.i
            int r1 = r0 + 1
            byte r0 = r2[r0]
            byte r5 = r5[r0]
            if (r1 >= r3) goto L_0x001c
            if (r5 == r7) goto L_0x0080
        L_0x001c:
            if (r5 == r7) goto L_0x0075
        L_0x001e:
            byte[] r6 = com.salmon.sdk.d.f.i
            int r0 = r1 + 1
            byte r1 = r2[r1]
            byte r6 = r6[r1]
            if (r0 >= r3) goto L_0x002a
            if (r6 == r7) goto L_0x007e
        L_0x002a:
            if (r6 == r7) goto L_0x0075
            int r1 = r5 << 2
            r5 = r6 & 48
            int r5 = r5 >>> 4
            r1 = r1 | r5
            r4.write(r1)
        L_0x0036:
            int r1 = r0 + 1
            byte r0 = r2[r0]
            if (r0 != r8) goto L_0x0041
            byte[] r0 = r4.toByteArray()
        L_0x0040:
            return r0
        L_0x0041:
            byte[] r5 = com.salmon.sdk.d.f.i
            byte r5 = r5[r0]
            if (r1 >= r3) goto L_0x0049
            if (r5 == r7) goto L_0x007c
        L_0x0049:
            if (r5 == r7) goto L_0x0075
            r0 = r6 & 15
            int r0 = r0 << 4
            r6 = r5 & 60
            int r6 = r6 >>> 2
            r0 = r0 | r6
            r4.write(r0)
        L_0x0057:
            int r0 = r1 + 1
            byte r1 = r2[r1]
            if (r1 != r8) goto L_0x0062
            byte[] r0 = r4.toByteArray()
            goto L_0x0040
        L_0x0062:
            byte[] r6 = com.salmon.sdk.d.f.i
            byte r1 = r6[r1]
            if (r0 >= r3) goto L_0x006a
            if (r1 == r7) goto L_0x007a
        L_0x006a:
            if (r1 == r7) goto L_0x0075
            r5 = r5 & 3
            int r5 = r5 << 6
            r1 = r1 | r5
            r4.write(r1)
            goto L_0x000e
        L_0x0075:
            byte[] r0 = r4.toByteArray()
            goto L_0x0040
        L_0x007a:
            r1 = r0
            goto L_0x0057
        L_0x007c:
            r0 = r1
            goto L_0x0036
        L_0x007e:
            r1 = r0
            goto L_0x001e
        L_0x0080:
            r0 = r1
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.d.f.d(java.lang.String):byte[]");
    }
}
