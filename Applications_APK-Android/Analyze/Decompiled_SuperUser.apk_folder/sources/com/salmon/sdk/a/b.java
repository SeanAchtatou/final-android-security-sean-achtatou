package com.salmon.sdk.a;

import android.database.Cursor;
import com.salmon.sdk.b.c;

public class b extends a {

    /* renamed from: c  reason: collision with root package name */
    private static final String f6020c = b.class.getName();

    /* renamed from: d  reason: collision with root package name */
    private static b f6021d;

    private b(g gVar) {
        super(gVar);
    }

    public static b a(g gVar) {
        if (f6021d == null) {
            f6021d = new b(gVar);
        }
        return f6021d;
    }

    private synchronized boolean a(c cVar) {
        Cursor cursor = null;
        boolean z = false;
        synchronized (this) {
            try {
                cursor = a().rawQuery("SELECT campaign_id FROM campaign_click WHERE campaign_id='" + cVar.a() + "'", null);
                if (cursor == null || cursor.getCount() <= 0) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    if (cursor != null) {
                        if (!cursor.isClosed()) {
                            cursor.close();
                        }
                    }
                } else {
                    cursor.close();
                    if (cursor != null) {
                        if (!cursor.isClosed()) {
                            cursor.close();
                        }
                    }
                    z = true;
                }
            } catch (Exception e2) {
                if (cursor != null) {
                    if (!cursor.isClosed()) {
                        cursor.close();
                    }
                }
            } catch (Throwable th) {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
                throw th;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00a6 A[SYNTHETIC, Splitter:B:37:0x00a6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.String a(java.lang.String r8) {
        /*
            r7 = this;
            r0 = 0
            monitor-enter(r7)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0090, all -> 0x00a0 }
            java.lang.String r2 = "SELECT last_click_time FROM campaign_click WHERE package_name='"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0090, all -> 0x00a0 }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ Exception -> 0x0090, all -> 0x00a0 }
            java.lang.String r2 = "' ORDER BY last_click_time DESC"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0090, all -> 0x00a0 }
            java.lang.String r2 = r1.toString()     // Catch:{ Exception -> 0x0090, all -> 0x00a0 }
            android.database.sqlite.SQLiteDatabase r1 = r7.a()     // Catch:{ Exception -> 0x0090, all -> 0x00a0 }
            r3 = 0
            android.database.Cursor r1 = r1.rawQuery(r2, r3)     // Catch:{ Exception -> 0x0090, all -> 0x00a0 }
            if (r1 == 0) goto L_0x0063
            int r0 = r1.getCount()     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            if (r0 <= 0) goto L_0x0063
            r1.moveToFirst()     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.String r0 = "last_click_time"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.String r3 = com.salmon.sdk.a.b.f6020c     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.String r5 = "sql --->"
            r4.<init>(r5)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.String r4 = "      ---->result: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            com.salmon.sdk.d.i.b(r3, r2)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            r1.close()     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            if (r1 == 0) goto L_0x0061
            boolean r2 = r1.isClosed()     // Catch:{ all -> 0x009d }
            if (r2 != 0) goto L_0x0061
            r1.close()     // Catch:{ all -> 0x009d }
        L_0x0061:
            monitor-exit(r7)
            return r0
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
        L_0x0068:
            java.lang.String r0 = com.salmon.sdk.a.b.f6020c     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.String r4 = "sql --->"
            r3.<init>(r4)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.String r3 = "      ---->result: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            com.salmon.sdk.d.i.b(r0, r2)     // Catch:{ Exception -> 0x00b2, all -> 0x00b0 }
            if (r1 == 0) goto L_0x008d
            boolean r0 = r1.isClosed()     // Catch:{ all -> 0x009d }
            if (r0 != 0) goto L_0x008d
            r1.close()     // Catch:{ all -> 0x009d }
        L_0x008d:
            java.lang.String r0 = "0"
            goto L_0x0061
        L_0x0090:
            r1 = move-exception
        L_0x0091:
            if (r0 == 0) goto L_0x008d
            boolean r1 = r0.isClosed()     // Catch:{ all -> 0x009d }
            if (r1 != 0) goto L_0x008d
            r0.close()     // Catch:{ all -> 0x009d }
            goto L_0x008d
        L_0x009d:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x00a0:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x00a4:
            if (r1 == 0) goto L_0x00af
            boolean r2 = r1.isClosed()     // Catch:{ all -> 0x009d }
            if (r2 != 0) goto L_0x00af
            r1.close()     // Catch:{ all -> 0x009d }
        L_0x00af:
            throw r0     // Catch:{ all -> 0x009d }
        L_0x00b0:
            r0 = move-exception
            goto L_0x00a4
        L_0x00b2:
            r0 = move-exception
            r0 = r1
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.b.a(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.salmon.sdk.b.c r9, java.lang.String r10, java.lang.String r11) {
        /*
            r8 = this;
            monitor-enter(r8)
            boolean r0 = r8.a(r9)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            if (r0 == 0) goto L_0x0055
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.String r1 = "UPDATE campaign_click set click_times = click_times +1,last_click_time = '"
            r0.<init>(r1)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.String r1 = "',click_ruls = '"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.StringBuilder r0 = r0.append(r11)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.String r1 = "' WHERE campaign_id = '"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            long r2 = r9.a()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.String r1 = "'"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            android.database.sqlite.SQLiteDatabase r1 = r8.a()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r1.execSQL(r0)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
        L_0x003f:
            java.lang.String r1 = com.salmon.sdk.a.b.f6020c     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.String r3 = "sql --->"
            r2.<init>(r3)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            com.salmon.sdk.d.i.b(r1, r0)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
        L_0x0053:
            monitor-exit(r8)
            return
        L_0x0055:
            java.lang.String r0 = "INSERT INTO campaign_click(campaign_id,package_name,version,click_ruls,last_click_time,click_times,result) VALUES(?,?,?,?,?,?,?)"
            android.database.sqlite.SQLiteDatabase r1 = r8.a()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r2 = 7
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r3 = 0
            long r4 = r9.a()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r2[r3] = r4     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r3 = 1
            java.lang.String r4 = r9.f()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r2[r3] = r4     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r3 = 2
            r2[r3] = r10     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r3 = 3
            r2[r3] = r11     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r3 = 4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r4.<init>()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r2[r3] = r4     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r3 = 5
            r4 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r2[r3] = r4     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r3 = 6
            java.lang.String r4 = "0"
            r2[r3] = r4     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            r1.execSQL(r0, r2)     // Catch:{ Exception -> 0x009b, Error -> 0x00a0, all -> 0x009d }
            goto L_0x003f
        L_0x009b:
            r0 = move-exception
            goto L_0x0053
        L_0x009d:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x00a0:
            r0 = move-exception
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.b.a(com.salmon.sdk.b.c, java.lang.String, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003a, code lost:
        if (r0 != null) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if (r0.isClosed() == false) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0042, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0060, code lost:
        if (r1.isClosed() == false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0062, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0066, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0067, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0039 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:4:0x0007] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005c A[SYNTHETIC, Splitter:B:29:0x005c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<java.lang.String> b(java.lang.String r7) {
        /*
            r6 = this;
            r0 = 0
            monitor-enter(r6)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0053 }
            r1.<init>()     // Catch:{ all -> 0x0053 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0039, all -> 0x0056 }
            java.lang.String r3 = "SELECT campaign_id FROM campaign_click WHERE package_name='"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0039, all -> 0x0056 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x0039, all -> 0x0056 }
            java.lang.String r3 = "'"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0039, all -> 0x0056 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0039, all -> 0x0056 }
            android.database.sqlite.SQLiteDatabase r3 = r6.a()     // Catch:{ Exception -> 0x0039, all -> 0x0056 }
            r4 = 0
            android.database.Cursor r0 = r3.rawQuery(r2, r4)     // Catch:{ Exception -> 0x0039, all -> 0x0056 }
        L_0x0025:
            boolean r2 = r0.moveToNext()     // Catch:{ Exception -> 0x0039, all -> 0x0066 }
            if (r2 == 0) goto L_0x0047
            java.lang.String r2 = "campaign_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0039, all -> 0x0066 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0039, all -> 0x0066 }
            r1.add(r2)     // Catch:{ Exception -> 0x0039, all -> 0x0066 }
            goto L_0x0025
        L_0x0039:
            r2 = move-exception
            if (r0 == 0) goto L_0x0045
            boolean r2 = r0.isClosed()     // Catch:{ all -> 0x0053 }
            if (r2 != 0) goto L_0x0045
            r0.close()     // Catch:{ all -> 0x0053 }
        L_0x0045:
            monitor-exit(r6)
            return r1
        L_0x0047:
            if (r0 == 0) goto L_0x0045
            boolean r2 = r0.isClosed()     // Catch:{ all -> 0x0053 }
            if (r2 != 0) goto L_0x0045
            r0.close()     // Catch:{ all -> 0x0053 }
            goto L_0x0045
        L_0x0053:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0056:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x005a:
            if (r1 == 0) goto L_0x0065
            boolean r2 = r1.isClosed()     // Catch:{ all -> 0x0053 }
            if (r2 != 0) goto L_0x0065
            r1.close()     // Catch:{ all -> 0x0053 }
        L_0x0065:
            throw r0     // Catch:{ all -> 0x0053 }
        L_0x0066:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.b.b(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b() {
        /*
            r6 = this;
            monitor-enter(r6)
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0029, Error -> 0x0027, all -> 0x0024 }
            r2 = 432000000(0x19bfcc00, double:2.13436359E-315)
            long r0 = r0 - r2
            android.database.sqlite.SQLiteDatabase r2 = r6.a()     // Catch:{ Exception -> 0x0029, Error -> 0x0027, all -> 0x0024 }
            java.lang.String r3 = "campaign_click"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0029, Error -> 0x0027, all -> 0x0024 }
            java.lang.String r5 = "last_click_time<"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0029, Error -> 0x0027, all -> 0x0024 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x0029, Error -> 0x0027, all -> 0x0024 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0029, Error -> 0x0027, all -> 0x0024 }
            r1 = 0
            r2.delete(r3, r0, r1)     // Catch:{ Exception -> 0x0029, Error -> 0x0027, all -> 0x0024 }
        L_0x0022:
            monitor-exit(r6)
            return
        L_0x0024:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0027:
            r0 = move-exception
            goto L_0x0022
        L_0x0029:
            r0 = move-exception
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.b.b():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0073 A[SYNTHETIC, Splitter:B:35:0x0073] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<java.lang.String> c() {
        /*
            r7 = this;
            r0 = 0
            monitor-enter(r7)
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x006a }
            r2.<init>()     // Catch:{ all -> 0x006a }
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ all -> 0x006a }
            r3.<init>()     // Catch:{ all -> 0x006a }
            java.lang.String r1 = "SELECT campaign_id,package_name FROM campaign_click"
            android.database.sqlite.SQLiteDatabase r4 = r7.a()     // Catch:{ Exception -> 0x007f, all -> 0x006d }
            r5 = 0
            android.database.Cursor r1 = r4.rawQuery(r1, r5)     // Catch:{ Exception -> 0x007f, all -> 0x006d }
        L_0x0017:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            if (r0 == 0) goto L_0x005e
            java.lang.String r0 = "campaign_id"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            java.lang.String r4 = r1.getString(r0)     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            java.lang.String r0 = "package_name"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            java.lang.String r5 = r1.getString(r0)     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            java.lang.Object r0 = r3.get(r5)     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            if (r0 == 0) goto L_0x0043
            java.lang.Object r0 = r3.get(r5)     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            if (r0 != 0) goto L_0x0017
        L_0x0043:
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            r3.put(r5, r0)     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            r2.add(r4)     // Catch:{ Exception -> 0x004f, all -> 0x007d }
            goto L_0x0017
        L_0x004f:
            r0 = move-exception
            r0 = r1
        L_0x0051:
            if (r0 == 0) goto L_0x005c
            boolean r1 = r0.isClosed()     // Catch:{ all -> 0x006a }
            if (r1 != 0) goto L_0x005c
            r0.close()     // Catch:{ all -> 0x006a }
        L_0x005c:
            monitor-exit(r7)
            return r2
        L_0x005e:
            if (r1 == 0) goto L_0x005c
            boolean r0 = r1.isClosed()     // Catch:{ all -> 0x006a }
            if (r0 != 0) goto L_0x005c
            r1.close()     // Catch:{ all -> 0x006a }
            goto L_0x005c
        L_0x006a:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x006d:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0071:
            if (r1 == 0) goto L_0x007c
            boolean r2 = r1.isClosed()     // Catch:{ all -> 0x006a }
            if (r2 != 0) goto L_0x007c
            r1.close()     // Catch:{ all -> 0x006a }
        L_0x007c:
            throw r0     // Catch:{ all -> 0x006a }
        L_0x007d:
            r0 = move-exception
            goto L_0x0071
        L_0x007f:
            r1 = move-exception
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.b.c():java.util.List");
    }
}
