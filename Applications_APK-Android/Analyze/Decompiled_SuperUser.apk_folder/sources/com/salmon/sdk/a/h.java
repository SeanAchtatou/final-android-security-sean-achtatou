package com.salmon.sdk.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class h extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f6035a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(g gVar, Context context, String str, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, i);
        this.f6035a = gVar;
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        g.a(sQLiteDatabase);
    }

    public final void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        g.b(sQLiteDatabase);
        g.a(sQLiteDatabase);
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        g.b(sQLiteDatabase);
        g.a(sQLiteDatabase);
    }
}
