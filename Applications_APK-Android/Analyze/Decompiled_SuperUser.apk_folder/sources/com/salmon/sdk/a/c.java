package com.salmon.sdk.a;

import android.content.ContentValues;
import android.database.Cursor;
import com.google.firebase.analytics.FirebaseAnalytics;

public final class c extends a {

    /* renamed from: c  reason: collision with root package name */
    private static c f6022c;

    private c(g gVar) {
        super(gVar);
    }

    public static c a(g gVar) {
        if (f6022c == null) {
            f6022c = new c(gVar);
        }
        return f6022c;
    }

    private synchronized com.salmon.sdk.b.c a(Cursor cursor) {
        com.salmon.sdk.b.c cVar;
        cVar = new com.salmon.sdk.b.c();
        cVar.a(cursor.getLong(cursor.getColumnIndex("id")));
        cVar.b(cursor.getLong(cursor.getColumnIndex("ad_id")));
        cVar.a(cursor.getString(cursor.getColumnIndex("impression_url")));
        cVar.b(cursor.getString(cursor.getColumnIndex("click_url")));
        cVar.c(cursor.getString(cursor.getColumnIndex("notice_url")));
        cVar.d(cursor.getString(cursor.getColumnIndex("package_name")));
        cVar.e(cursor.getString(cursor.getColumnIndex("icon_url")));
        cVar.j(cursor.getString(cursor.getColumnIndex("pkg_url")));
        cVar.f(cursor.getString(cursor.getColumnIndex("app_name")));
        cVar.g(cursor.getString(cursor.getColumnIndex("app_desc")));
        cVar.a(Double.parseDouble(cursor.getString(cursor.getColumnIndex("app_score"))));
        cVar.h(cursor.getString(cursor.getColumnIndex("image_url")));
        cVar.i(cursor.getString(cursor.getColumnIndex("image_size")));
        cVar.a(Boolean.valueOf(cursor.getString(cursor.getColumnIndex("pre_click"))).booleanValue());
        return cVar;
    }

    private synchronized boolean c(long j) {
        Cursor cursor = null;
        boolean z = false;
        synchronized (this) {
            try {
                cursor = this.f6019b.rawQuery("SELECT id FROM campaign WHERE id=" + j, null);
                if (cursor.getCount() > 0) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    z = true;
                } else if (cursor != null) {
                    cursor.close();
                }
            } catch (Exception e2) {
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return z;
    }

    public final synchronized long a(com.salmon.sdk.b.c cVar) {
        long j = -1;
        synchronized (this) {
            if (cVar != null) {
                try {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("id", Long.valueOf(cVar.a()));
                    contentValues.put("ad_id", Long.valueOf(cVar.b()));
                    contentValues.put("impression_url", cVar.c());
                    contentValues.put("click_url", cVar.d());
                    contentValues.put("notice_url", cVar.e());
                    contentValues.put("package_name", cVar.f());
                    contentValues.put("icon_url", cVar.g());
                    contentValues.put("pkg_url", cVar.n());
                    contentValues.put("app_name", cVar.h());
                    contentValues.put("app_desc", cVar.i());
                    contentValues.put("app_score", Double.valueOf(cVar.j()));
                    contentValues.put("image_url", cVar.k());
                    contentValues.put("image_size", cVar.l());
                    contentValues.put("pre_click", Boolean.valueOf(cVar.m()));
                    contentValues.put("get_time", Long.valueOf(System.currentTimeMillis()));
                    if (c(cVar.a())) {
                        this.f6019b.update(FirebaseAnalytics.Param.CAMPAIGN, contentValues, "id = " + cVar.a(), null);
                    } else {
                        this.f6019b.insert(FirebaseAnalytics.Param.CAMPAIGN, null, contentValues);
                    }
                    j = cVar.a();
                } catch (Error | Exception e2) {
                } catch (OutOfMemoryError e3) {
                    System.gc();
                }
            }
        }
        return j;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(long r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.String r1 = "id="
            r0.<init>(r1)     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            android.database.sqlite.SQLiteDatabase r1 = r5.f6019b     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.String r2 = "campaign"
            r3 = 0
            r1.delete(r2, r0, r3)     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
        L_0x0018:
            monitor-exit(r5)
            return
        L_0x001a:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x001d:
            r0 = move-exception
            goto L_0x0018
        L_0x001f:
            r0 = move-exception
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.c.a(long):void");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.String r1 = "package_name="
            r0.<init>(r1)     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            android.database.sqlite.SQLiteDatabase r1 = r4.f6019b     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.String r2 = "campaign"
            r3 = 0
            r1.delete(r2, r0, r3)     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
        L_0x0018:
            monitor-exit(r4)
            return
        L_0x001a:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x001d:
            r0 = move-exception
            goto L_0x0018
        L_0x001f:
            r0 = move-exception
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.c.a(java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0040 A[SYNTHETIC, Splitter:B:24:0x0040] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.salmon.sdk.b.c b(java.lang.String r6) {
        /*
            r5 = this;
            r0 = 0
            monitor-enter(r5)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x002f, all -> 0x003a }
            java.lang.String r2 = "SELECT * FROM campaign WHERE package_name = '"
            r1.<init>(r2)     // Catch:{ Exception -> 0x002f, all -> 0x003a }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x002f, all -> 0x003a }
            java.lang.String r2 = "' order by get_time desc"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x002f, all -> 0x003a }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x002f, all -> 0x003a }
            android.database.sqlite.SQLiteDatabase r2 = r5.f6019b     // Catch:{ Exception -> 0x002f, all -> 0x003a }
            r3 = 0
            android.database.Cursor r1 = r2.rawQuery(r1, r3)     // Catch:{ Exception -> 0x002f, all -> 0x003a }
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x0046, all -> 0x0044 }
            if (r2 == 0) goto L_0x0028
            com.salmon.sdk.b.c r0 = r5.a(r1)     // Catch:{ Exception -> 0x0046, all -> 0x0044 }
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()     // Catch:{ all -> 0x0037 }
        L_0x002d:
            monitor-exit(r5)
            return r0
        L_0x002f:
            r1 = move-exception
            r1 = r0
        L_0x0031:
            if (r1 == 0) goto L_0x002d
            r1.close()     // Catch:{ all -> 0x0037 }
            goto L_0x002d
        L_0x0037:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x003a:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ all -> 0x0037 }
        L_0x0043:
            throw r0     // Catch:{ all -> 0x0037 }
        L_0x0044:
            r0 = move-exception
            goto L_0x003e
        L_0x0046:
            r2 = move-exception
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.c.b(java.lang.String):com.salmon.sdk.b.c");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        if (r0 != null) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0039, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x003a, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001e A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:5:0x0009] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0035 A[SYNTHETIC, Splitter:B:26:0x0035] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.salmon.sdk.b.c> b() {
        /*
            r6 = this;
            r0 = 0
            monitor-enter(r6)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x002c }
            r1.<init>()     // Catch:{ all -> 0x002c }
            java.lang.String r2 = "SELECT * FROM campaign"
            android.database.sqlite.SQLiteDatabase r3 = r6.f6019b     // Catch:{ Exception -> 0x001e, all -> 0x002f }
            r4 = 0
            android.database.Cursor r0 = r3.rawQuery(r2, r4)     // Catch:{ Exception -> 0x001e, all -> 0x002f }
        L_0x0010:
            boolean r2 = r0.moveToNext()     // Catch:{ Exception -> 0x001e, all -> 0x0039 }
            if (r2 == 0) goto L_0x0026
            com.salmon.sdk.b.c r2 = r6.a(r0)     // Catch:{ Exception -> 0x001e, all -> 0x0039 }
            r1.add(r2)     // Catch:{ Exception -> 0x001e, all -> 0x0039 }
            goto L_0x0010
        L_0x001e:
            r2 = move-exception
            if (r0 == 0) goto L_0x0024
            r0.close()     // Catch:{ all -> 0x002c }
        L_0x0024:
            monitor-exit(r6)
            return r1
        L_0x0026:
            if (r0 == 0) goto L_0x0024
            r0.close()     // Catch:{ all -> 0x002c }
            goto L_0x0024
        L_0x002c:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x002f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0033:
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ all -> 0x002c }
        L_0x0038:
            throw r0     // Catch:{ all -> 0x002c }
        L_0x0039:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.c.b():java.util.List");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b(long r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.String r1 = "get_time<="
            r0.<init>(r1)     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            android.database.sqlite.SQLiteDatabase r1 = r5.f6019b     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
            java.lang.String r2 = "campaign"
            r3 = 0
            r1.delete(r2, r0, r3)     // Catch:{ Exception -> 0x001f, Error -> 0x001d, all -> 0x001a }
        L_0x0018:
            monitor-exit(r5)
            return
        L_0x001a:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x001d:
            r0 = move-exception
            goto L_0x0018
        L_0x001f:
            r0 = move-exception
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.c.b(long):void");
    }
}
