package com.salmon.sdk.a;

import com.salmon.sdk.b.b;
import com.salmon.sdk.d.i;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

public class d {

    /* renamed from: c  reason: collision with root package name */
    private static final String f6023c = d.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    private Map<String, b> f6024a;

    /* renamed from: b  reason: collision with root package name */
    private b f6025b;

    /* renamed from: d  reason: collision with root package name */
    private b f6026d;

    private d() {
        this.f6024a = new Hashtable(10);
    }

    /* synthetic */ d(byte b2) {
        this();
    }

    public static final d a() {
        return e.f6027a;
    }

    public final void a(b bVar) {
        this.f6025b = bVar;
        this.f6024a.put(bVar.f(), bVar);
    }

    public final void a(String str) {
        if (this.f6025b != null) {
            this.f6025b.b(str);
            this.f6026d = this.f6025b;
            this.f6025b = null;
            i.b(f6023c, "保存副本--" + str);
            return;
        }
        i.b(f6023c, "没有下载信息");
    }

    public final void a(boolean z) {
        if (z) {
            this.f6025b = null;
        }
        this.f6026d = null;
        try {
            if (this.f6024a.size() > 10) {
                Iterator<String> it = this.f6024a.keySet().iterator();
                long currentTimeMillis = System.currentTimeMillis();
                while (it.hasNext()) {
                    if (currentTimeMillis - this.f6024a.get(it.next()).a() > 3600000) {
                        it.remove();
                    }
                }
            }
        } catch (Exception e2) {
        }
    }

    public final b b() {
        return this.f6025b;
    }

    public final boolean b(String str) {
        return !this.f6024a.containsKey(str);
    }

    public final b c() {
        return this.f6026d;
    }
}
