package com.salmon.sdk.a;

import android.database.Cursor;
import com.salmon.sdk.b.e;

public final class f extends a {

    /* renamed from: c  reason: collision with root package name */
    private static f f6028c;

    private f(g gVar) {
        super(gVar);
    }

    public static f a(g gVar) {
        if (f6028c == null) {
            f6028c = new f(gVar);
        }
        return f6028c;
    }

    private synchronized e a(Cursor cursor) {
        e eVar;
        boolean z = true;
        synchronized (this) {
            eVar = new e();
            eVar.a(cursor.getString(cursor.getColumnIndex("packagename")));
            eVar.b(cursor.getString(cursor.getColumnIndex("refer")));
            eVar.a(cursor.getLong(cursor.getColumnIndex("clicktime")));
            eVar.b(cursor.getLong(cursor.getColumnIndex("campaignid")));
            if (cursor.getInt(cursor.getColumnIndex("is_realtime")) != 1) {
                z = false;
            }
            eVar.a(z);
        }
        return eVar;
    }

    private synchronized boolean b(String str) {
        Cursor cursor = null;
        boolean z = false;
        synchronized (this) {
            try {
                cursor = this.f6019b.rawQuery("SELECT packagename FROM rushrefer WHERE campaignid='" + str + "'", null);
                if (cursor.getCount() > 0) {
                    cursor.close();
                    if (cursor != null) {
                        cursor.close();
                    }
                    z = true;
                } else {
                    cursor.close();
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            } catch (Exception e2) {
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0058 A[Catch:{ Exception -> 0x004a, all -> 0x0052 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.salmon.sdk.b.e a(java.lang.String r7, long r8) {
        /*
            r6 = this;
            r0 = 0
            monitor-enter(r6)
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x004a, all -> 0x0052 }
            long r2 = r2 - r8
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004a, all -> 0x0052 }
            java.lang.String r4 = "SELECT * FROM rushrefer WHERE packagename = '"
            r1.<init>(r4)     // Catch:{ Exception -> 0x004a, all -> 0x0052 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x004a, all -> 0x0052 }
            java.lang.String r4 = "' and clicktime>="
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x004a, all -> 0x0052 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x004a, all -> 0x0052 }
            java.lang.String r2 = " order by clicktime desc "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x004a, all -> 0x0052 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x004a, all -> 0x0052 }
            android.database.sqlite.SQLiteDatabase r2 = r6.f6019b     // Catch:{ Exception -> 0x004a, all -> 0x0052 }
            r3 = 0
            android.database.Cursor r1 = r2.rawQuery(r1, r3)     // Catch:{ Exception -> 0x004a, all -> 0x0052 }
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x005e, all -> 0x005c }
            if (r2 == 0) goto L_0x003e
            com.salmon.sdk.b.e r0 = r6.a(r1)     // Catch:{ Exception -> 0x005e, all -> 0x005c }
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ all -> 0x0047 }
        L_0x003c:
            monitor-exit(r6)
            return r0
        L_0x003e:
            r1.close()     // Catch:{ Exception -> 0x005e, all -> 0x005c }
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ all -> 0x0047 }
            goto L_0x003c
        L_0x0047:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x004a:
            r1 = move-exception
            r1 = r0
        L_0x004c:
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ all -> 0x0047 }
            goto L_0x003c
        L_0x0052:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0056:
            if (r1 == 0) goto L_0x005b
            r1.close()     // Catch:{ all -> 0x0047 }
        L_0x005b:
            throw r0     // Catch:{ all -> 0x0047 }
        L_0x005c:
            r0 = move-exception
            goto L_0x0056
        L_0x005e:
            r2 = move-exception
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.f.a(java.lang.String, long):com.salmon.sdk.b.e");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.String a(com.salmon.sdk.b.e r7) {
        /*
            r6 = this;
            r0 = 0
            monitor-enter(r6)
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            r2.<init>()     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r1 = "packagename"
            java.lang.String r3 = r7.a()     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            r2.put(r1, r3)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r1 = "refer"
            java.lang.String r3 = r7.b()     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            r2.put(r1, r3)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r1 = "clicktime"
            long r4 = r7.c()     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.Long r3 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            r2.put(r1, r3)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r1 = "campaignid"
            long r4 = r7.d()     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.Long r3 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            r2.put(r1, r3)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r3 = "is_realtime"
            boolean r1 = r7.e()     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            if (r1 == 0) goto L_0x0078
            r1 = 1
        L_0x003c:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            r2.put(r3, r1)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            long r4 = r7.d()     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r1 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            boolean r1 = r6.b(r1)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            if (r1 == 0) goto L_0x007a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r3 = "campaignid = '"
            r1.<init>(r3)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            long r4 = r7.d()     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r3 = "'"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            android.database.sqlite.SQLiteDatabase r3 = r6.f6019b     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r4 = "rushrefer"
            r5 = 0
            r3.update(r4, r2, r1, r5)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
        L_0x0072:
            java.lang.String r0 = r7.a()     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
        L_0x0076:
            monitor-exit(r6)
            return r0
        L_0x0078:
            r1 = 0
            goto L_0x003c
        L_0x007a:
            android.database.sqlite.SQLiteDatabase r1 = r6.f6019b     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            java.lang.String r3 = "rushrefer"
            r4 = 0
            r1.insert(r3, r4, r2)     // Catch:{ Exception -> 0x0083, Error -> 0x0088, all -> 0x0085 }
            goto L_0x0072
        L_0x0083:
            r1 = move-exception
            goto L_0x0076
        L_0x0085:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0088:
            r1 = move-exception
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.f.a(com.salmon.sdk.b.e):java.lang.String");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0025, Error -> 0x0023, all -> 0x0020 }
            java.lang.String r1 = "campaignid='"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0025, Error -> 0x0023, all -> 0x0020 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x0025, Error -> 0x0023, all -> 0x0020 }
            java.lang.String r1 = "'"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0025, Error -> 0x0023, all -> 0x0020 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0025, Error -> 0x0023, all -> 0x0020 }
            android.database.sqlite.SQLiteDatabase r1 = r4.f6019b     // Catch:{ Exception -> 0x0025, Error -> 0x0023, all -> 0x0020 }
            java.lang.String r2 = "rushrefer"
            r3 = 0
            r1.delete(r2, r0, r3)     // Catch:{ Exception -> 0x0025, Error -> 0x0023, all -> 0x0020 }
        L_0x001e:
            monitor-exit(r4)
            return
        L_0x0020:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0023:
            r0 = move-exception
            goto L_0x001e
        L_0x0025:
            r0 = move-exception
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.f.a(java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        if (r0 != null) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0032, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0033, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001e A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:5:0x0009] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0038 A[SYNTHETIC, Splitter:B:28:0x0038] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.salmon.sdk.b.e> b() {
        /*
            r6 = this;
            r0 = 0
            monitor-enter(r6)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x002f }
            r1.<init>()     // Catch:{ all -> 0x002f }
            java.lang.String r2 = "SELECT * FROM rushrefer"
            android.database.sqlite.SQLiteDatabase r3 = r6.f6019b     // Catch:{ Exception -> 0x001e, all -> 0x0032 }
            r4 = 0
            android.database.Cursor r0 = r3.rawQuery(r2, r4)     // Catch:{ Exception -> 0x001e, all -> 0x0032 }
        L_0x0010:
            boolean r2 = r0.moveToNext()     // Catch:{ Exception -> 0x001e, all -> 0x003c }
            if (r2 == 0) goto L_0x0026
            com.salmon.sdk.b.e r2 = r6.a(r0)     // Catch:{ Exception -> 0x001e, all -> 0x003c }
            r1.add(r2)     // Catch:{ Exception -> 0x001e, all -> 0x003c }
            goto L_0x0010
        L_0x001e:
            r2 = move-exception
            if (r0 == 0) goto L_0x0024
            r0.close()     // Catch:{ all -> 0x002f }
        L_0x0024:
            monitor-exit(r6)
            return r1
        L_0x0026:
            r0.close()     // Catch:{ Exception -> 0x001e, all -> 0x003c }
            if (r0 == 0) goto L_0x0024
            r0.close()     // Catch:{ all -> 0x002f }
            goto L_0x0024
        L_0x002f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0032:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0036:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ all -> 0x002f }
        L_0x003b:
            throw r0     // Catch:{ all -> 0x002f }
        L_0x003c:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.f.b():java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0041, code lost:
        if (r0 != null) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0054, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0055, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0040 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:4:0x0007] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005a A[SYNTHETIC, Splitter:B:27:0x005a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.util.List<com.salmon.sdk.b.e> b(java.lang.String r9, long r10) {
        /*
            r8 = this;
            r0 = 0
            monitor-enter(r8)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0051 }
            r1.<init>()     // Catch:{ all -> 0x0051 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0040, all -> 0x0054 }
            long r2 = r2 - r10
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0040, all -> 0x0054 }
            java.lang.String r5 = "SELECT * FROM rushrefer WHERE packagename = '"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0040, all -> 0x0054 }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ Exception -> 0x0040, all -> 0x0054 }
            java.lang.String r5 = "' and clicktime>="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0040, all -> 0x0054 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x0040, all -> 0x0054 }
            java.lang.String r3 = " order by clicktime desc "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0040, all -> 0x0054 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0040, all -> 0x0054 }
            android.database.sqlite.SQLiteDatabase r3 = r8.f6019b     // Catch:{ Exception -> 0x0040, all -> 0x0054 }
            r4 = 0
            android.database.Cursor r0 = r3.rawQuery(r2, r4)     // Catch:{ Exception -> 0x0040, all -> 0x0054 }
        L_0x0032:
            boolean r2 = r0.moveToNext()     // Catch:{ Exception -> 0x0040, all -> 0x005e }
            if (r2 == 0) goto L_0x0048
            com.salmon.sdk.b.e r2 = r8.a(r0)     // Catch:{ Exception -> 0x0040, all -> 0x005e }
            r1.add(r2)     // Catch:{ Exception -> 0x0040, all -> 0x005e }
            goto L_0x0032
        L_0x0040:
            r2 = move-exception
            if (r0 == 0) goto L_0x0046
            r0.close()     // Catch:{ all -> 0x0051 }
        L_0x0046:
            monitor-exit(r8)
            return r1
        L_0x0048:
            r0.close()     // Catch:{ Exception -> 0x0040, all -> 0x005e }
            if (r0 == 0) goto L_0x0046
            r0.close()     // Catch:{ all -> 0x0051 }
            goto L_0x0046
        L_0x0051:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x0054:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0058:
            if (r1 == 0) goto L_0x005d
            r1.close()     // Catch:{ all -> 0x0051 }
        L_0x005d:
            throw r0     // Catch:{ all -> 0x0051 }
        L_0x005e:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.a.f.b(java.lang.String, long):java.util.List");
    }
}
