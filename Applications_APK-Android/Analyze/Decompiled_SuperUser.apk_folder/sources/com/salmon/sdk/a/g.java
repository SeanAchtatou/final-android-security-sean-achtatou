package com.salmon.sdk.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class g {

    /* renamed from: a  reason: collision with root package name */
    public static Context f6029a;

    /* renamed from: c  reason: collision with root package name */
    private static int f6030c = 5;

    /* renamed from: d  reason: collision with root package name */
    private static g f6031d;

    /* renamed from: f  reason: collision with root package name */
    private static SQLiteDatabase f6032f = null;

    /* renamed from: b  reason: collision with root package name */
    private String f6033b = g.class.getSimpleName();

    /* renamed from: e  reason: collision with root package name */
    private h f6034e;

    private g(Context context) {
        f6029a = context;
        this.f6034e = new h(this, f6029a, "salmon.sdk.db", f6030c);
    }

    public static synchronized g a(Context context) {
        g gVar;
        synchronized (g.class) {
            if (f6031d == null) {
                f6031d = new g(context);
            }
            gVar = f6031d;
        }
        return gVar;
    }

    static /* synthetic */ void a(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS rushrefer (campaignid INTEGER,packagename TEXT,refer TEXT,clicktime BIGINT,is_realtime INTEGER,PRIMARY KEY (campaignid))");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS campaign (id INTEGER,ad_id INTEGER,impression_url TEXT,click_url TEXT,notice_url TEXT,package_name TEXT,icon_url TEXT,pkg_url TEXT,app_name TEXT,app_desc TEXT,app_score TEXT,image_url TEXT,image_size TEXT,pre_click TEXT,get_time BIGINT,PRIMARY KEY (ad_id))");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS campaign_click (campaign_id TEXT,package_name TEXT,version TEXT,last_click_time TEXT,click_times INTEGER,result TEXT,click_ruls TEXT )");
        } catch (Exception e2) {
        }
    }

    static /* synthetic */ void b(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS 'rushrefer'");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS 'campaign'");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS 'campaign_click'");
        } catch (Exception e2) {
        }
    }

    public final synchronized SQLiteDatabase a() {
        SQLiteDatabase sQLiteDatabase;
        if (f6032f != null) {
            sQLiteDatabase = f6032f;
        } else {
            try {
                if (this.f6034e == null) {
                    this.f6034e = new h(this, f6029a, "salmon.sdk.db", f6030c);
                }
                f6032f = this.f6034e.getWritableDatabase();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            sQLiteDatabase = f6032f;
        }
        return sQLiteDatabase;
    }
}
