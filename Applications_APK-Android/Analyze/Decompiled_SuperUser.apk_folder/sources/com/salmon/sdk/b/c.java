package com.salmon.sdk.b;

import java.io.Serializable;

public final class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f6050a;

    /* renamed from: b  reason: collision with root package name */
    private long f6051b;

    /* renamed from: c  reason: collision with root package name */
    private String f6052c;

    /* renamed from: d  reason: collision with root package name */
    private String f6053d;

    /* renamed from: e  reason: collision with root package name */
    private String f6054e;

    /* renamed from: f  reason: collision with root package name */
    private String f6055f;

    /* renamed from: g  reason: collision with root package name */
    private String f6056g;

    /* renamed from: h  reason: collision with root package name */
    private String f6057h;
    private String i;
    private String j;
    private double k;
    private String l;
    private String m;
    private boolean n;

    public final long a() {
        return this.f6050a;
    }

    public final void a(double d2) {
        this.k = d2;
    }

    public final void a(long j2) {
        this.f6050a = j2;
    }

    public final void a(String str) {
        this.f6052c = str;
    }

    public final void a(boolean z) {
        this.n = z;
    }

    public final long b() {
        return this.f6051b;
    }

    public final void b(long j2) {
        this.f6051b = j2;
    }

    public final void b(String str) {
        this.f6053d = str;
    }

    public final String c() {
        return this.f6052c;
    }

    public final void c(String str) {
        this.f6054e = str;
    }

    public final String d() {
        return this.f6053d;
    }

    public final void d(String str) {
        this.f6055f = str;
    }

    public final String e() {
        return this.f6054e;
    }

    public final void e(String str) {
        this.f6056g = str;
    }

    public final String f() {
        return this.f6055f;
    }

    public final void f(String str) {
        this.i = str;
    }

    public final String g() {
        return this.f6056g;
    }

    public final void g(String str) {
        this.j = str;
    }

    public final String h() {
        return this.i;
    }

    public final void h(String str) {
        this.l = str;
    }

    public final String i() {
        return this.j;
    }

    public final void i(String str) {
        this.m = str;
    }

    public final double j() {
        return this.k;
    }

    public final void j(String str) {
        this.f6057h = str;
    }

    public final String k() {
        return this.l;
    }

    public final String l() {
        return this.m;
    }

    public final boolean m() {
        return this.n;
    }

    public final String n() {
        return this.f6057h;
    }
}
