package com.salmon.sdk.b;

import java.util.List;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    String f6044a;

    /* renamed from: b  reason: collision with root package name */
    String f6045b;

    /* renamed from: c  reason: collision with root package name */
    List<String> f6046c = null;

    /* renamed from: d  reason: collision with root package name */
    String f6047d;

    /* renamed from: e  reason: collision with root package name */
    String f6048e;

    /* renamed from: f  reason: collision with root package name */
    long f6049f;

    public b(String str, String str2, String str3) {
        this.f6047d = str;
        this.f6045b = str2;
        this.f6044a = str3;
        this.f6049f = System.currentTimeMillis();
    }

    public final long a() {
        return this.f6049f;
    }

    public final void a(String str) {
        this.f6045b = str;
    }

    public final void a(List<String> list) {
        this.f6046c = list;
    }

    public final String b() {
        return this.f6044a;
    }

    public final void b(String str) {
        this.f6048e = str;
    }

    public final String c() {
        return this.f6045b;
    }

    public final List<String> d() {
        return this.f6046c;
    }

    public final String e() {
        return this.f6048e;
    }

    public final String f() {
        return this.f6047d;
    }
}
