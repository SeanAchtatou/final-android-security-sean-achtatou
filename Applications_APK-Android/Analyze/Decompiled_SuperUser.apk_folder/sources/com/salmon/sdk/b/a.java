package com.salmon.sdk.b;

import java.io.Serializable;
import java.util.List;

public final class a implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static int f6036a = 1;

    /* renamed from: b  reason: collision with root package name */
    public static int f6037b = 1;

    /* renamed from: c  reason: collision with root package name */
    private long f6038c;

    /* renamed from: d  reason: collision with root package name */
    private String f6039d;

    /* renamed from: e  reason: collision with root package name */
    private String f6040e;

    /* renamed from: f  reason: collision with root package name */
    private String f6041f;

    /* renamed from: g  reason: collision with root package name */
    private int f6042g;

    /* renamed from: h  reason: collision with root package name */
    private String f6043h;
    private int i;
    private long j;
    private List<c> k;
    private String l;

    public final void a() {
        this.f6038c = 1;
    }

    public final void a(int i2) {
        this.f6042g = i2;
    }

    public final void a(long j2) {
        this.j = j2;
    }

    public final void a(String str) {
        this.f6039d = str;
    }

    public final void a(List<c> list) {
        this.k = list;
    }

    public final List<c> b() {
        return this.k;
    }

    public final void b(int i2) {
        this.i = i2;
    }

    public final void b(String str) {
        this.f6040e = str;
    }

    public final void c(String str) {
        this.f6041f = str;
    }

    public final void d(String str) {
        this.f6043h = str;
    }

    public final void e(String str) {
        this.l = str;
    }
}
