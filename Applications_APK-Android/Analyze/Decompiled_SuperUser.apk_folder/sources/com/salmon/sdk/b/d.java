package com.salmon.sdk.b;

import java.util.Set;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private String f6058a;

    /* renamed from: b  reason: collision with root package name */
    private String f6059b;

    /* renamed from: c  reason: collision with root package name */
    private long f6060c;

    public d() {
    }

    public d(String str, String str2, long j) {
        this.f6058a = str;
        this.f6059b = str2;
        this.f6060c = j;
    }

    public static String a(Set<d> set) {
        if (set != null) {
            try {
                if (set.size() > 0) {
                    StringBuffer stringBuffer = new StringBuffer();
                    for (d next : set) {
                        stringBuffer.append("{\"campaignId\":").append(next.f6058a + ",").append("\"packageName\":").append(next.f6059b + ",").append("\"updateTime\":").append(next.f6060c + "},");
                    }
                    return "[{$native_info}]".replace("{$native_info}", stringBuffer.subSequence(0, stringBuffer.lastIndexOf(",")));
                }
            } catch (Exception e2) {
            }
        }
        return null;
    }

    public final String a() {
        return this.f6059b;
    }

    public final void a(long j) {
        this.f6060c = j;
    }

    public final void a(String str) {
        this.f6058a = str;
    }

    public final long b() {
        return this.f6060c;
    }

    public final void b(String str) {
        this.f6059b = str;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        d dVar = (d) obj;
        if (this.f6058a == null) {
            if (dVar.f6058a != null) {
                return false;
            }
        } else if (!this.f6058a.equals(dVar.f6058a)) {
            return false;
        }
        return this.f6059b == null ? dVar.f6059b == null : this.f6059b.equals(dVar.f6059b);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.f6058a == null ? 0 : this.f6058a.hashCode()) + 31) * 31;
        if (this.f6059b != null) {
            i = this.f6059b.hashCode();
        }
        return hashCode + i;
    }
}
