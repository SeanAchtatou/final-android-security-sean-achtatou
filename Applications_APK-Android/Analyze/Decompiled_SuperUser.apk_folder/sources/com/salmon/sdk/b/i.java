package com.salmon.sdk.b;

import java.io.Serializable;
import java.util.List;

public final class i implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static int f6078a = 1;

    /* renamed from: b  reason: collision with root package name */
    public static int f6079b = 0;

    /* renamed from: c  reason: collision with root package name */
    List<f> f6080c;

    /* renamed from: d  reason: collision with root package name */
    j f6081d;

    /* renamed from: e  reason: collision with root package name */
    int f6082e;

    /* renamed from: f  reason: collision with root package name */
    String f6083f;

    public final f a(String str) {
        for (f next : this.f6080c) {
            if (str.equals(next.f6067b)) {
                return next;
            }
        }
        return null;
    }

    public final j a() {
        return this.f6081d;
    }

    public final void a(int i) {
        this.f6082e = i;
    }

    public final void a(j jVar) {
        this.f6081d = jVar;
    }

    public final void a(List<f> list) {
        this.f6080c = list;
    }

    public final String b() {
        return this.f6083f;
    }

    public final void b(String str) {
        this.f6083f = str;
    }
}
