package com.salmon.sdk.b;

import java.io.Serializable;
import java.util.Map;

public final class g implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, String> f6074a;

    /* renamed from: b  reason: collision with root package name */
    private String f6075b;

    public final String a() {
        return this.f6075b;
    }

    public final void a(String str) {
        this.f6075b = str;
    }

    public final void a(Map<String, String> map) {
        this.f6074a = map;
    }

    public final Map<String, String> b() {
        return this.f6074a;
    }
}
