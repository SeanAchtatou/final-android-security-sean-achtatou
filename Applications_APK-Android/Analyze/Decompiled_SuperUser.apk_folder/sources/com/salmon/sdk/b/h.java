package com.salmon.sdk.b;

import org.json.JSONArray;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private int f6076a;

    /* renamed from: b  reason: collision with root package name */
    private int f6077b;

    public final void a(int i) {
        this.f6076a = i;
    }

    public final void b(int i) {
        this.f6077b = i;
    }

    public final String toString() {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(this.f6076a);
        jSONArray.put(this.f6077b);
        return jSONArray.toString();
    }
}
