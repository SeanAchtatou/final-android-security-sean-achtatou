package com.salmon.sdk.c;

import android.content.Context;
import com.salmon.sdk.d.c.c;
import com.salmon.sdk.d.i;
import java.util.List;
import java.util.Map;

public abstract class a<T> {
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public static final String f6088d = a.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    protected boolean f6089a;

    /* renamed from: b  reason: collision with root package name */
    protected Context f6090b;

    /* renamed from: c  reason: collision with root package name */
    boolean f6091c = true;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public k f6092e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public c f6093f;

    public a(Context context) {
        this.f6090b = context;
    }

    /* access modifiers changed from: protected */
    public abstract int a();

    /* access modifiers changed from: protected */
    public abstract T a(Map<String, List<String>> map, byte[] bArr);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.c.a.a(com.salmon.sdk.c.k, boolean):void
     arg types: [com.salmon.sdk.c.k, int]
     candidates:
      com.salmon.sdk.c.a.a(java.util.Map<java.lang.String, java.util.List<java.lang.String>>, byte[]):T
      com.salmon.sdk.c.a.a(com.salmon.sdk.c.k, boolean):void */
    public final void a(k kVar) {
        a(kVar, true);
    }

    public final void a(k kVar, boolean z) {
        i.a("agent", "HTTP=========>  1  --  " + System.currentTimeMillis());
        this.f6089a = false;
        if (kVar != null) {
            this.f6092e = kVar;
        }
        this.f6091c = z;
        if (z && this.f6093f == null) {
            this.f6093f = new c(this.f6092e);
        }
        i.a("agent", "HTTP=========>  2  --  " + System.currentTimeMillis());
        com.salmon.sdk.d.c.a.a().a((c) new b(this, 0));
    }

    /* access modifiers changed from: protected */
    public abstract String b();

    /* access modifiers changed from: protected */
    public abstract Map<String, String> c();

    /* access modifiers changed from: protected */
    public abstract byte[] d();

    /* access modifiers changed from: protected */
    public abstract boolean e();
}
