package com.salmon.sdk.c;

import android.os.Handler;
import android.os.Message;
import java.lang.ref.WeakReference;

final class c extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<k> f6097a = null;

    public c(k kVar) {
        this.f6097a = new WeakReference<>(kVar);
    }

    public final void handleMessage(Message message) {
        k kVar = this.f6097a.get();
        if (kVar != null) {
            switch (message.what) {
                case 100:
                    int i = message.arg1;
                    kVar.a(message.obj);
                    break;
                case 101:
                    int i2 = message.arg1;
                    kVar.a();
                    break;
                case 110:
                    int i3 = message.arg1;
                    kVar.b();
                    break;
                case 111:
                    if (message.obj == null) {
                        int i4 = message.arg1;
                        kVar.b();
                        break;
                    } else {
                        int i5 = message.arg1;
                        message.obj.toString();
                        kVar.b();
                        break;
                    }
                case 112:
                    int i6 = message.arg1;
                    new StringBuilder("[").append(message.arg2).append("] Http status code.");
                    kVar.b();
                    break;
                case 113:
                    int i7 = message.arg1;
                    new StringBuilder("[").append(message.arg2).append("]request api error ! ").append(message.obj.toString());
                    kVar.b();
                    break;
                case 114:
                    int i8 = message.arg1;
                    kVar.c();
                    break;
            }
            super.handleMessage(message);
        }
    }
}
