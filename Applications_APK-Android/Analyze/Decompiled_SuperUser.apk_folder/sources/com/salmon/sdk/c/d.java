package com.salmon.sdk.c;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import com.kingouser.com.entity.UninstallAppInfo;
import com.salmon.sdk.a.b;
import com.salmon.sdk.a.g;
import com.salmon.sdk.b.a;
import com.salmon.sdk.b.c;
import com.salmon.sdk.b.e;
import com.salmon.sdk.d.f;
import com.salmon.sdk.d.h;
import com.salmon.sdk.d.i;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class d extends a<a> {

    /* renamed from: d  reason: collision with root package name */
    private static final String f6098d = d.class.getSimpleName();

    /* renamed from: e  reason: collision with root package name */
    private int f6099e = a.f6036a;

    /* renamed from: f  reason: collision with root package name */
    private String f6100f = "lite";

    /* renamed from: g  reason: collision with root package name */
    private String f6101g;

    /* renamed from: h  reason: collision with root package name */
    private String f6102h;
    private String i;
    private int j;
    private int k = 1;
    private String l;
    private int m = 0;
    private int n = 0;
    private Context o;
    private g p;
    private String q = "";
    private String r = "";

    public d(Context context, g gVar, String str, int i2) {
        super(context);
        this.o = context;
        this.f6100f = str;
        this.f6099e = i2;
        this.p = gVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public a a(Map<String, List<String>> map, byte[] bArr) {
        a aVar;
        List list;
        try {
            this.j = 1;
            Iterator<String> it = map.keySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                String next = it.next();
                if ("status".equalsIgnoreCase(next) && (list = map.get(next)) != null && list.size() > 0) {
                    this.j = Integer.parseInt((String) list.get(0));
                    break;
                }
            }
            if (this.j == 1) {
                String str = new String(bArr);
                if (TextUtils.isEmpty(str)) {
                    return null;
                }
                String b2 = f.b(str);
                i.a(f6098d, "RESPONSE: " + b2);
                aVar = f(b2);
            } else {
                if (this.j == -1) {
                    i.e(f6098d, "RESPONSE: " + f.b(new String(bArr)));
                }
                aVar = null;
            }
        } catch (Exception e2) {
            aVar = null;
        }
        return aVar;
    }

    private a f(String str) {
        a aVar = new a();
        aVar.a();
        aVar.b(a.f6037b);
        aVar.a(System.currentTimeMillis());
        try {
            JSONObject jSONObject = new JSONObject(str);
            this.j = jSONObject.optInt("status");
            if (this.j == 1) {
                ArrayList arrayList = new ArrayList();
                JSONObject optJSONObject = jSONObject.optJSONObject("data");
                aVar.a(optJSONObject.getString("scenario"));
                aVar.b(optJSONObject.getString("adType"));
                aVar.c(optJSONObject.getString("onlyImpressionUrl"));
                aVar.a(optJSONObject.getInt("orientation"));
                aVar.d(optJSONObject.optString("htmlUrl"));
                JSONArray optJSONArray = optJSONObject.optJSONArray("rows");
                for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                    JSONObject jSONObject2 = (JSONObject) optJSONArray.get(i2);
                    c cVar = new c();
                    cVar.a(jSONObject2.optLong("campaignId"));
                    cVar.b(jSONObject2.optLong("campaignId"));
                    cVar.a(jSONObject2.optString("impressionUrl"));
                    cVar.b(jSONObject2.optString("clickUrl"));
                    cVar.c(jSONObject2.optString("noticeUrl"));
                    cVar.d(jSONObject2.optString("packageName"));
                    cVar.e(jSONObject2.optString("iconUrl"));
                    cVar.f(jSONObject2.optString("appName"));
                    cVar.g(jSONObject2.optString("appDesc"));
                    cVar.a(jSONObject2.optDouble("appScore"));
                    cVar.h(jSONObject2.optString("imageUrl"));
                    cVar.i(jSONObject2.optString("imageSize"));
                    cVar.a(jSONObject2.optBoolean("preClick"));
                    cVar.j(jSONObject2.optString("dataUrl"));
                    arrayList.add(cVar);
                }
                aVar.a(arrayList);
                aVar.e(optJSONObject.toString());
            }
        } catch (JSONException e2) {
        }
        return aVar;
    }

    private String h() {
        JSONObject jSONObject = new JSONObject();
        try {
            String substring = new StringBuilder().append(System.currentTimeMillis()).toString().substring(0, 10);
            jSONObject.put("pf", io.fabric.sdk.android.services.common.a.ANDROID_CLIENT_TYPE);
            jSONObject.put("ov", Build.VERSION.RELEASE);
            jSONObject.put("sv", "SA_SR_3.3.0");
            jSONObject.put("pn", h.l(this.o));
            jSONObject.put("vn", h.i(this.o));
            jSONObject.put("vc", h.h(this.o));
            jSONObject.put("im", h.a(this.o));
            jSONObject.put("mac", h.g(this.o));
            jSONObject.put("did", h.d(this.o));
            jSONObject.put("dm", Build.MODEL);
            jSONObject.put("ss", h.j(this.o) + "x" + h.k(this.o));
            jSONObject.put("ot", this.f6099e);
            jSONObject.put("mnc", h.c(this.o));
            jSONObject.put("mcc", h.b(this.o));
            jSONObject.put("nt", h.m(this.o));
            jSONObject.put("l", h.f(this.o));
            jSONObject.put("ua", h.a());
            jSONObject.put("gpv", h.n(this.o));
            jSONObject.put("gpsv", h.o(this.o));
            jSONObject.put("adid", h.p(this.o));
            jSONObject.put("t", substring);
            if (this.m == 0) {
                this.m = 1;
            }
            jSONObject.put("aid", this.m);
            jSONObject.put("snr", this.f6100f);
            jSONObject.put("at", this.f6102h);
            jSONObject.put("tz", h.b());
            jSONObject.put("an", this.k);
            jSONObject.put(UninstallAppInfo.COLUMN_PKG, this.q);
            jSONObject.put("utid", TextUtils.isEmpty(this.r) ? "" : this.r);
            if (this.n > 0) {
                jSONObject.put("offset", this.n);
            }
            if (!TextUtils.isEmpty(this.i)) {
                jSONObject.put("cat", this.i);
            }
            jSONObject.put("ext", this.f6101g);
            if (!TextUtils.isEmpty(this.f6100f) && !this.f6100f.equals("lite")) {
                JSONArray jSONArray = new JSONArray();
                List<e> b2 = com.salmon.sdk.a.f.a(this.p).b();
                if (b2.size() > 0) {
                    for (e d2 : b2) {
                        jSONArray.put(new StringBuilder().append(d2.d()).toString());
                    }
                }
                List<c> b3 = com.salmon.sdk.a.c.a(this.p).b();
                if (b3.size() > 0) {
                    for (c a2 : b3) {
                        jSONArray.put(new StringBuilder().append(a2.a()).toString());
                    }
                }
                List<String> c2 = b.a(this.p).c();
                if (c2.size() > 0) {
                    for (String put : c2) {
                        jSONArray.put(put);
                    }
                }
                if (jSONArray.length() > 0 && TextUtils.isEmpty(this.q)) {
                    jSONObject.put("ei", jSONArray);
                }
            }
            jSONObject.put("sn", com.salmon.sdk.d.g.b(this.m + this.l + this.f6100f + substring));
            return URLEncoder.encode(new String(Base64.encode(jSONObject.toString().getBytes(), 0)), "utf-8");
        } catch (JSONException e2) {
            return null;
        } catch (UnsupportedEncodingException e3) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return 2;
    }

    public final void a(int i2) {
        this.m = i2;
    }

    public final void a(String str) {
        this.f6100f = str;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return com.salmon.sdk.core.c.f6239a + "?p=" + h();
    }

    public final void b(int i2) {
        this.k = i2;
    }

    public final void b(String str) {
        this.f6102h = str;
    }

    /* access modifiers changed from: protected */
    public final Map<String, String> c() {
        return null;
    }

    public final void c(String str) {
        this.l = str;
    }

    public final void d(String str) {
        this.q = str;
    }

    /* access modifiers changed from: protected */
    public final byte[] d() {
        return null;
    }

    public final void e(String str) {
        this.r = str;
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return false;
    }

    public final void g() {
        this.n = 0;
    }
}
