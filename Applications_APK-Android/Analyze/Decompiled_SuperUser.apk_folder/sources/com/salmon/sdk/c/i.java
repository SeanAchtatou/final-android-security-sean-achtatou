package com.salmon.sdk.c;

import android.content.Context;
import android.os.Build;
import com.salmon.sdk.b.b;
import com.salmon.sdk.core.a;
import com.salmon.sdk.core.c;
import com.salmon.sdk.d.f;
import com.salmon.sdk.d.h;
import com.salmon.sdk.d.l;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class i extends a<Object> {

    /* renamed from: d  reason: collision with root package name */
    private final String f6120d = i.class.getSimpleName();

    /* renamed from: e  reason: collision with root package name */
    private int f6121e = 1;

    /* renamed from: f  reason: collision with root package name */
    private b f6122f;

    /* renamed from: g  reason: collision with root package name */
    private Context f6123g;

    public i(Context context, b bVar) {
        super(context);
        this.f6123g = context;
        this.f6122f = bVar;
        this.f6121e = l.b(context, a.f6148a, "APPID", Integer.parseInt(a.j));
    }

    private String g() {
        int i = 2;
        StringBuilder sb = new StringBuilder();
        try {
            String c2 = f.c(h());
            com.salmon.sdk.d.i.b("downing", "clever=" + c2);
            sb.append("appid=").append(String.valueOf(this.f6121e)).append("&bgt=").append(URLEncoder.encode(c2, "UTF-8"));
            HashMap hashMap = new HashMap();
            hashMap.put("system", "1");
            hashMap.put("os_v", Build.VERSION.RELEASE);
            hashMap.put("app_pname", h.l(this.f6123g));
            hashMap.put("app_vn", h.i(this.f6090b));
            hashMap.put("app_vc", String.valueOf(h.h(this.f6090b)));
            if (this.f6090b.getResources().getConfiguration().orientation == 2) {
                i = 1;
            }
            hashMap.put("direction", Integer.valueOf(i));
            hashMap.put("brand", Build.BRAND);
            hashMap.put("model", Build.MODEL);
            hashMap.put("adid", h.p(this.f6090b));
            hashMap.put("mnc", h.c(this.f6090b));
            hashMap.put("mcc", h.b(this.f6090b));
            hashMap.put("network", String.valueOf(h.m(this.f6090b)));
            hashMap.put("language", h.f(this.f6090b));
            hashMap.put("timezone", h.b());
            hashMap.put("ua", h.a());
            hashMap.put("sdkversion", "SA_SR_3.3.0");
            hashMap.put("screen_size", h.j(this.f6090b) + "x" + h.k(this.f6090b));
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("imei", h.a(this.f6090b));
            jSONObject.put("mac", h.g(this.f6090b));
            jSONObject.put("android_id", h.d(this.f6090b));
            hashMap.put("ima", f.c(jSONObject.toString()));
            for (String str : hashMap.keySet()) {
                sb.append("&");
                sb.append(str);
                sb.append("=");
                sb.append(URLEncoder.encode(hashMap.get(str).toString(), "UTF-8"));
            }
            com.salmon.sdk.d.i.b(this.f6120d, sb.toString());
        } catch (Exception e2) {
        } catch (OutOfMemoryError e3) {
            System.gc();
        }
        com.salmon.sdk.d.i.b("downing", sb.toString());
        return sb.toString();
    }

    private String h() {
        if (this.f6122f == null) {
            return "";
        }
        try {
            JSONArray jSONArray = new JSONArray();
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("p", this.f6122f.e());
            if (this.f6122f.d() != null) {
                jSONObject.putOpt("ul", new JSONArray((Collection) this.f6122f.d()));
            }
            jSONObject.put("fl", this.f6122f.c());
            jSONObject.put("kw", this.f6122f.b());
            jSONObject.put("p", "");
            jSONArray.put(jSONObject);
            com.salmon.sdk.d.i.b("downing", jSONArray.toString());
            return jSONArray.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public final Object a(Map<String, List<String>> map, byte[] bArr) {
        try {
            String str = new String(bArr);
            ArrayList arrayList = new ArrayList();
            String b2 = f.b(str);
            com.salmon.sdk.d.i.b(this.f6120d, "data:" + b2);
            JSONObject jSONObject = new JSONObject(b2);
            if (jSONObject.optInt("status") == 1) {
                JSONArray optJSONArray = jSONObject.optJSONObject("data").optJSONArray("pnlist");
                if (optJSONArray == null) {
                    return arrayList;
                }
                for (int i = 0; i < optJSONArray.length(); i++) {
                    arrayList.add(optJSONArray.optJSONObject(i).optString("p"));
                }
                com.salmon.sdk.d.i.b(this.f6120d, "data:" + optJSONArray.toString());
                return arrayList;
            }
        } catch (Throwable th) {
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return c.f6245g;
    }

    /* access modifiers changed from: protected */
    public final Map<String, String> c() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final byte[] d() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("s=").append(URLEncoder.encode(f.a(g()), "UTF-8"));
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return false;
    }
}
