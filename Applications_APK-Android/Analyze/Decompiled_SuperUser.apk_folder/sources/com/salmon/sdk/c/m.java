package com.salmon.sdk.c;

import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.salmon.sdk.d.i;

final class m extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f6133a;

    m(l lVar) {
        this.f6133a = lVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        try {
            i.b(l.f6125a, "onPageFinished--------->" + str);
            if (!TextUtils.isEmpty(str) && str.equalsIgnoreCase(this.f6133a.j)) {
                this.f6133a.f6129e.removeCallbacksAndMessages(null);
                this.f6133a.f6129e.postDelayed(new n(this, webView), 8000);
            } else if (this.f6133a.f6126b) {
                this.f6133a.f6129e.removeCallbacksAndMessages(null);
                this.f6133a.f6129e.postDelayed(this.f6133a.p, 8000);
            }
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.c.l.a(com.salmon.sdk.c.l, boolean):boolean
     arg types: [com.salmon.sdk.c.l, int]
     candidates:
      com.salmon.sdk.c.l.a(com.salmon.sdk.c.l, android.net.Uri):android.net.Uri
      com.salmon.sdk.c.l.a(com.salmon.sdk.b.c, com.salmon.sdk.c.p):boolean
      com.salmon.sdk.c.l.a(com.salmon.sdk.c.l, boolean):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x013f, code lost:
        java.lang.System.gc();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x013e A[ExcHandler: OutOfMemoryError (e java.lang.OutOfMemoryError), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x01a5 A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean shouldOverrideUrlLoading(android.webkit.WebView r8, java.lang.String r9) {
        /*
            r7 = this;
            r6 = 1
            com.salmon.sdk.c.l r0 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean r0 = r0.f6126b     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r0 == 0) goto L_0x0052
            com.salmon.sdk.c.l r0 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.os.Handler r0 = r0.f6129e     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r0 == 0) goto L_0x0052
            java.lang.String r0 = com.salmon.sdk.c.l.f6125a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r1 = "=============parseRefer start2"
            com.salmon.sdk.d.i.b(r0, r1)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r0 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.os.Handler r0 = r0.f6129e     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r1 = 0
            r0.removeCallbacksAndMessages(r1)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.net.Uri r0 = android.net.Uri.parse(r9)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.net.Uri r1 = r1.f6131g     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r1 == 0) goto L_0x0053
            java.lang.String r0 = com.salmon.sdk.c.l.f6125a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r1 = "=============callback"
            com.salmon.sdk.d.i.b(r0, r1)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r0 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r1 = 1
            boolean unused = r0.m = r1     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r0 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.os.Handler r0 = r0.f6129e     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.Runnable r1 = r1.p     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r0.post(r1)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
        L_0x0052:
            return r6
        L_0x0053:
            boolean r1 = com.salmon.sdk.c.l.b(r0)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r1 == 0) goto L_0x00c1
            java.lang.String r1 = com.salmon.sdk.c.l.f6125a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = "=============callback"
            com.salmon.sdk.d.i.b(r1, r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r1 = com.salmon.sdk.c.l.f6125a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r3 = "isGooglePlayUri > "
            r2.<init>(r3)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.d.i.a(r1, r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.p r1 = r1.f6132h     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = "market"
            java.lang.String r3 = r0.getScheme()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r2 == 0) goto L_0x00a5
            android.net.Uri$Builder r0 = r0.buildUpon()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = "https"
            r0.scheme(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = "play.google.com"
            r0.authority(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = "/store/apps/details"
            r0.path(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.net.Uri r0 = r0.build()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
        L_0x00a5:
            com.salmon.sdk.c.l r2 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean unused = r2.f6126b = false     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r2 = 1
            com.salmon.sdk.c.l r3 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean unused = r3.k     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r3 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean unused = r3.l     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r3 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean r3 = r3.m     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r1.a(r2, r0, r3)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            goto L_0x0052
        L_0x00bf:
            r0 = move-exception
            goto L_0x0052
        L_0x00c1:
            java.lang.String r1 = "intent"
            java.lang.String r2 = r0.getScheme()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r1 == 0) goto L_0x01a8
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean unused = r1.f6126b = false     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r1 = "adjust_reftag"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r1 >= 0) goto L_0x0144
            java.util.Map r1 = com.salmon.sdk.c.l.a(r0)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r0 = "package"
            java.lang.Object r0 = r1.get(r0)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = "referrer"
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r2 = r7.f6133a     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.p r2 = r2.f6132h     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r3 = 1
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r4.<init>()     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r5 = com.salmon.sdk.core.a.l     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r4 = "&referrer="
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean unused = r1.k     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean unused = r1.l     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean r1 = r1.m     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r2.a(r3, r0, r1)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            goto L_0x0052
        L_0x012f:
            r0 = move-exception
            com.salmon.sdk.c.l r0 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.p r0 = r0.f6132h     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r1 = 0
            r2 = 0
            r3 = 1
            r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            goto L_0x0052
        L_0x013e:
            r0 = move-exception
            java.lang.System.gc()
            goto L_0x0052
        L_0x0144:
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.p r1 = r1.f6132h     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r2 = 1
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r3.<init>()     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r4 = com.salmon.sdk.core.a.l     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r4 = "package="
            int r4 = r0.indexOf(r4)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            int r4 = r4 + 8
            java.lang.String r5 = ";end"
            int r5 = r0.indexOf(r5)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r4 = r0.substring(r4, r5)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r4 = "&referrer="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r4 = "adjust_reftag="
            int r4 = r0.indexOf(r4)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            int r4 = r4 + 14
            java.lang.String r5 = "#"
            int r5 = r0.indexOf(r5)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r0 = r0.substring(r4, r5)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r3 = r7.f6133a     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean unused = r3.k     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r3 = r7.f6133a     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean unused = r3.l     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r3 = r7.f6133a     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean r3 = r3.m     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r1.a(r2, r0, r3)     // Catch:{ Exception -> 0x012f, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            goto L_0x0052
        L_0x01a5:
            r0 = move-exception
            goto L_0x0052
        L_0x01a8:
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.net.Uri unused = r1.f6131g = r0     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r0 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.content.Context r0 = r0.f6128d     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.core.a.a r0 = com.salmon.sdk.core.a.a.a(r0)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.b.g r0 = r0.b()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r0 == 0) goto L_0x0231
            java.util.Map r1 = r0.b()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r1 == 0) goto L_0x0231
            java.util.Map r1 = r0.b()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            int r1 = r1.size()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r1 <= 0) goto L_0x0231
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.content.Context r1 = r1.f6128d     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r0 = com.salmon.sdk.core.a.a.a(r1, r9, r0)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            if (r1 != 0) goto L_0x0248
            r1 = 1003204(0xf4ec4, float:1.405788E-39)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r3 = "session_id=0&campaign_id="
            r2.<init>(r3)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r3 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.b.c r3 = r3.i     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            long r4 = r3.a()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r3 = "&type=rush&pkg="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r3 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.b.c r3 = r3.i     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r3 = r3.f()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r3 = "&msg="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r3 = "&dummy=1"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.d.a.a.a(r1, r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r1.<init>()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r1 = r1.append(r9)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r9 = r0.toString()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
        L_0x0231:
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r1 = 19
            if (r0 < r1) goto L_0x028c
            com.salmon.sdk.c.l r0 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.webkit.WebView r0 = r0.f6127c     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r1 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.util.Map r1 = r1.f6130f     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r0.loadUrl(r9, r1)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            goto L_0x0052
        L_0x0248:
            r0 = 1003204(0xf4ec4, float:1.405788E-39)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = "session_id=0&campaign_id="
            r1.<init>(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r2 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.b.c r2 = r2.i     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            long r2 = r2.a()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = "&type=rush&pkg="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.c.l r2 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.b.c r2 = r2.i     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = r2.f()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = "&msg="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.StringBuilder r1 = r1.append(r9)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r2 = "&dummy=0"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            com.salmon.sdk.d.a.a.a(r0, r1)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            goto L_0x0231
        L_0x028c:
            com.salmon.sdk.c.l r0 = r7.f6133a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            android.webkit.WebView r0 = r0.f6127c     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            r0.loadUrl(r9)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x013e, Throwable -> 0x01a5 }
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.c.m.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
    }
}
