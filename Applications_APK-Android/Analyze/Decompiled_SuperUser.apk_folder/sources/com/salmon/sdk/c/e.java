package com.salmon.sdk.c;

import android.content.Context;
import com.salmon.sdk.a.d;
import com.salmon.sdk.b.b;
import com.salmon.sdk.core.a;
import com.salmon.sdk.core.c;
import com.salmon.sdk.d.f;
import com.salmon.sdk.d.g;
import com.salmon.sdk.d.i;
import com.salmon.sdk.d.l;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class e extends a<Object> {
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public static final String f6103d = e.class.getSimpleName();

    /* renamed from: e  reason: collision with root package name */
    private String f6104e;

    /* renamed from: f  reason: collision with root package name */
    private String f6105f;

    /* renamed from: g  reason: collision with root package name */
    private String f6106g;

    /* renamed from: h  reason: collision with root package name */
    private String f6107h;
    private String i;
    private List<String> j;
    private int k;

    public e(Context context, b bVar, int i2) {
        super(context);
        this.f6090b = context;
        this.f6104e = new StringBuilder().append(l.b(context, a.f6148a, "APPID", Integer.parseInt(a.j))).toString();
        this.f6106g = l.b(context, a.f6148a, "APPKEY", a.k);
        this.f6105f = bVar.c();
        this.f6107h = bVar.b();
        this.k = i2;
        i.b(f6103d, "size....." + i2);
        i.b(f6103d, "getUrllist....." + bVar.d().size());
        if (bVar.d() == null || bVar.d().size() <= i2) {
            this.j = bVar.d();
        } else {
            this.j = new ArrayList(i2);
            for (int i3 = 0; i3 < i2; i3++) {
                this.j.add(bVar.d().get(i3));
            }
        }
        i.b(f6103d, "mUrlList....." + this.j.size());
        this.i = bVar.e();
    }

    public static void a(Context context, boolean z, boolean z2, String str, int i2, long j2, int i3) {
        i.b(f6103d, "上报 init.....need_report....---->" + z2);
        i.b(f6103d, "上报 init.....isFromDownload....---->" + z);
        i.b(f6103d, "上报 init.....packName....---->" + str);
        i.b(f6103d, "上报 init.....gprul302JumpMaxCount....---->" + i2);
        i.b(f6103d, "上报 init.....downloadDiffTimeMax....---->" + j2);
        i.b(f6103d, "上报 init.....downloadDiffSizeMax....---->" + i3);
        i.b(f6103d, "上报 init.....DowingInfoMap....---->" + (d.a().b() != null));
        if (i2 > 0) {
            com.salmon.sdk.d.c.a.a().a(new f(z, context, str, j2, i3, z2, i2));
        }
    }

    private String h() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.j != null) {
                jSONObject.putOpt("url_list", new JSONArray((Collection) this.j));
            }
            jSONObject.put("file_length", this.f6105f);
            jSONObject.put("app_id", this.f6104e);
            jSONObject.put("app_title", this.f6107h);
            jSONObject.put("package_name", this.i);
            i.b("AdPackageLoader", "para_p_json:" + jSONObject.toString());
            String a2 = f.a(jSONObject.toString());
            i.b("AdPackageLoader", "para_p_json:" + a2);
            String b2 = g.b(a2 + this.f6106g);
            HashMap hashMap = new HashMap();
            hashMap.put("p", a2);
            hashMap.put("sign", b2);
            Set<String> keySet = hashMap.keySet();
            StringBuilder sb = new StringBuilder();
            for (String str : keySet) {
                if (sb.length() > 0) {
                    sb.append("&");
                }
                sb.append(str);
                sb.append("=");
                sb.append(URLEncoder.encode(String.valueOf(hashMap.get(str)), "utf-8"));
            }
            return sb.toString();
        } catch (Exception e2) {
        } catch (OutOfMemoryError e3) {
            System.gc();
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public final Object a(Map map, byte[] bArr) {
        try {
            String b2 = f.b(new String(bArr));
            i.b(f6103d, "data:" + b2);
            return new Integer(new JSONObject(b2).optInt(a.f6150c));
        } catch (Throwable th) {
            return new Integer(-1);
        }
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return c.f6244f + "?" + h();
    }

    /* access modifiers changed from: protected */
    public final Map<String, String> c() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final byte[] d() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return false;
    }
}
