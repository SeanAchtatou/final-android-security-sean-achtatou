package com.salmon.sdk.c;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.salmon.sdk.b.c;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class l {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f6125a = l.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f6126b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public WebView f6127c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public Context f6128d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public Handler f6129e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public Map<String, String> f6130f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public Uri f6131g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public p f6132h;
    /* access modifiers changed from: private */
    public c i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public boolean n = false;
    private WebViewClient o = new m(this);
    /* access modifiers changed from: private */
    public Runnable p = new o(this);

    private l(Context context) {
        try {
            this.f6128d = context;
            this.f6126b = false;
            this.f6129e = new Handler(Looper.getMainLooper());
            if (Build.VERSION.SDK_INT >= 19) {
                this.f6130f = new HashMap();
                ResolveInfo resolveActivity = this.f6128d.getPackageManager().resolveActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://")), 65536);
                if (resolveActivity != null) {
                    this.f6130f.put("X-Requested-With", resolveActivity.activityInfo.packageName);
                } else {
                    this.f6130f.put("X-Requested-With", "com.android.browser");
                }
            }
        } catch (Exception e2) {
        }
    }

    public static l a(Context context) {
        return new l(context);
    }

    public static Map<String, String> a(String str) {
        HashMap hashMap = new HashMap();
        try {
            Matcher matcher = Pattern.compile("=market://details(.+);").matcher(str);
            loop0:
            while (matcher.find()) {
                String[] split = matcher.group(1).split(";")[0].split("&");
                int length = split.length;
                int i2 = 0;
                while (true) {
                    if (i2 < length) {
                        String str2 = split[i2];
                        if (str2.indexOf("id=") >= 0) {
                            hashMap.put(ServiceManagerNative.PACKAGE, str2.split("=")[1]);
                        }
                        if (str2.indexOf("referrer=") >= 0) {
                            hashMap.put("referrer", str2.split("=")[1]);
                        }
                        if (hashMap.containsKey(ServiceManagerNative.PACKAGE) && hashMap.containsKey("referrer")) {
                            break loop0;
                        }
                        i2++;
                    }
                }
            }
        } catch (Exception e2) {
        }
        return hashMap;
    }

    /* access modifiers changed from: private */
    public static boolean b(Uri uri) {
        return "market".equals(uri.getScheme()) || "play.google.com".equals(uri.getHost());
    }

    public final void a() {
        try {
            if (this.f6127c != null) {
                this.f6127c.clearHistory();
                this.f6127c.clearCache(true);
                this.f6127c.freeMemory();
                this.f6127c.pauseTimers();
                this.f6127c = null;
            }
        } catch (Exception e2) {
        }
    }

    public final synchronized boolean a(c cVar, p pVar) {
        boolean z = true;
        boolean z2 = false;
        synchronized (this) {
            if (cVar != null) {
                synchronized (this) {
                    try {
                        this.i = cVar;
                        this.j = cVar.d();
                        String d2 = cVar.d();
                        if (d2 != null && !this.f6126b && Thread.currentThread() == Looper.getMainLooper().getThread()) {
                            this.f6132h = pVar;
                            this.f6131g = Uri.parse(d2);
                            if (!b(this.f6131g)) {
                                this.f6126b = true;
                                if (this.f6127c == null) {
                                    this.f6127c = new WebView(this.f6128d);
                                    this.f6127c.setWebViewClient(this.o);
                                    WebSettings settings = this.f6127c.getSettings();
                                    settings.setUseWideViewPort(false);
                                    this.f6127c.addJavascriptInterface(new q(this), "local_obj");
                                    settings.setJavaScriptEnabled(true);
                                }
                                this.f6127c.setInitialScale(100);
                                DisplayMetrics displayMetrics = this.f6128d.getResources().getDisplayMetrics();
                                this.f6127c.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                                if (Build.VERSION.SDK_INT >= 19) {
                                    this.f6127c.loadUrl(d2, this.f6130f);
                                } else {
                                    this.f6127c.loadUrl(d2);
                                }
                                z2 = z;
                            }
                        }
                        z = false;
                        z2 = z;
                    } catch (Exception e2) {
                    }
                }
            }
        }
        return z2;
    }
}
