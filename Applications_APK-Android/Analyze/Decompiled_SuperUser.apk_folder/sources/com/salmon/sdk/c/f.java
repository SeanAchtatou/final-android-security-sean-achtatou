package com.salmon.sdk.c;

import android.content.Context;
import com.salmon.sdk.a.d;
import com.salmon.sdk.b.b;
import com.salmon.sdk.core.a;
import com.salmon.sdk.d.i;
import java.io.File;

final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f6108a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Context f6109b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ String f6110c;

    /* renamed from: d  reason: collision with root package name */
    final /* synthetic */ long f6111d;

    /* renamed from: e  reason: collision with root package name */
    final /* synthetic */ int f6112e;

    /* renamed from: f  reason: collision with root package name */
    final /* synthetic */ boolean f6113f;

    /* renamed from: g  reason: collision with root package name */
    final /* synthetic */ int f6114g;

    f(boolean z, Context context, String str, long j, int i, boolean z2, int i2) {
        this.f6108a = z;
        this.f6109b = context;
        this.f6110c = str;
        this.f6111d = j;
        this.f6112e = i;
        this.f6113f = z2;
        this.f6114g = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.c.a.a(com.salmon.sdk.c.k, boolean):void
     arg types: [com.salmon.sdk.c.g, int]
     candidates:
      com.salmon.sdk.c.e.a(java.util.Map, byte[]):java.lang.Object
      com.salmon.sdk.c.a.a(java.util.Map<java.lang.String, java.util.List<java.lang.String>>, byte[]):T
      com.salmon.sdk.c.a.a(com.salmon.sdk.c.k, boolean):void */
    public final void run() {
        b bVar;
        if (this.f6108a) {
            bVar = d.a().b();
            if (bVar == null) {
                return;
            }
        } else {
            bVar = d.a().c();
            if (bVar != null) {
                try {
                    if (!a.f6152e.equals(this.f6109b.getPackageManager().getInstallerPackageName(this.f6110c))) {
                        i.b(e.f6103d, "不是gp安装的");
                        return;
                    } else if (System.currentTimeMillis() - bVar.a() <= this.f6111d) {
                        File file = new File(this.f6109b.getPackageManager().getApplicationInfo(this.f6110c, 0).sourceDir);
                        long length = file.exists() ? file.length() : 0;
                        if (length > 0 && Math.abs(length - Long.parseLong(bVar.c())) > (length * ((long) this.f6112e)) / 100) {
                            i.b(e.f6103d, "curr_size - Appsiz 误差在 curr_size 40% 之外");
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Exception e2) {
                }
            } else {
                return;
            }
        }
        if (bVar != null) {
            if (this.f6113f && this.f6110c.equals(bVar.e()) && bVar.d() != null && bVar.d().size() > 0) {
                e eVar = new e(this.f6109b, bVar, this.f6114g);
                i.b(e.f6103d, "上报 开始上报.........---->");
                eVar.a((k) new g(this), false);
            }
            i.b(e.f6103d, "上报结束.........");
        }
    }
}
