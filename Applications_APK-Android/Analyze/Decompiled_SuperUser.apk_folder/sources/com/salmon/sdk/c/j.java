package com.salmon.sdk.c;

import android.content.Context;
import java.util.Map;

public final class j extends a<String> {

    /* renamed from: d  reason: collision with root package name */
    private String f6124d;

    public j(String str, Context context) {
        super(context);
        this.f6124d = str;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Map map, byte[] bArr) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return this.f6124d;
    }

    /* access modifiers changed from: protected */
    public final Map<String, String> c() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final byte[] d() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return false;
    }
}
