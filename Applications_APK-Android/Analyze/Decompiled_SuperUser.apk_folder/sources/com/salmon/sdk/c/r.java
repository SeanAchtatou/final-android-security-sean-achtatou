package com.salmon.sdk.c;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.salmon.sdk.a.g;
import com.salmon.sdk.b.i;
import com.salmon.sdk.core.c;
import com.salmon.sdk.d.h;
import io.fabric.sdk.android.services.common.a;
import java.net.URLEncoder;
import java.util.Map;
import org.json.JSONObject;

public class r extends a<i> {

    /* renamed from: d  reason: collision with root package name */
    private final String f6138d = r.class.getSimpleName();

    /* renamed from: e  reason: collision with root package name */
    private g f6139e;

    /* renamed from: f  reason: collision with root package name */
    private Context f6140f;

    /* renamed from: g  reason: collision with root package name */
    private int f6141g = 1;

    /* renamed from: h  reason: collision with root package name */
    private String f6142h;

    public r(g gVar, Context context, int i, String str) {
        super(context);
        this.f6139e = gVar;
        this.f6140f = context;
        this.f6141g = i;
        this.f6142h = str;
    }

    private i a(String str) {
        com.salmon.sdk.d.i.a(this.f6138d, "RESPONSE: " + str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            int optInt = jSONObject.optInt("status");
            String optString = jSONObject.optString("msg");
            if (optInt == 1) {
                i iVar = new i();
                try {
                    iVar.b(jSONObject.optJSONObject("data").toString());
                    return iVar;
                } catch (Exception e2) {
                    return iVar;
                }
            } else {
                Log.w(this.f6138d, "StrategyLoader error: " + optString);
                return null;
            }
        } catch (Exception e3) {
            return null;
        }
    }

    private String g() {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        try {
            String sb = new StringBuilder().append(System.currentTimeMillis()).toString();
            jSONObject.put("pf", a.ANDROID_CLIENT_TYPE);
            jSONObject.put("ov", Build.VERSION.RELEASE);
            jSONObject.put("sv", "SA_SR_3.3.0");
            jSONObject.put("pn", h.l(this.f6140f));
            jSONObject.put("vn", h.i(this.f6140f));
            jSONObject.put("vc", h.h(this.f6140f));
            jSONObject.put("im", h.a(this.f6140f));
            jSONObject.put("mac", h.g(this.f6140f));
            jSONObject.put("did", h.d(this.f6140f));
            jSONObject.put("dm", Build.MODEL);
            jSONObject.put("ss", h.j(this.f6140f) + "x" + h.k(this.f6140f));
            jSONObject.put("ot", 1);
            jSONObject.put("mnc", h.c(this.f6140f));
            jSONObject.put("mcc", h.b(this.f6140f));
            jSONObject.put("nt", h.m(this.f6140f));
            jSONObject.put("l", h.f(this.f6140f));
            jSONObject.put("tz", h.b());
            jSONObject.put("ua", h.a());
            jSONObject.put("gaid", com.salmon.sdk.d.b.a.a(this.f6140f));
            jSONObject.put("gpv", h.n(this.f6140f));
            jSONObject.put("gpsv", h.o(this.f6140f));
            jSONObject.put("ss", h.j(this.f6140f) + "x" + h.k(this.f6140f));
            String jSONObject3 = jSONObject.toString();
            String b2 = com.salmon.sdk.d.g.b(this.f6141g + this.f6142h + sb);
            jSONObject2.put("m_device_info", jSONObject3);
            jSONObject2.put("appid", this.f6141g);
            jSONObject2.put("sign", b2);
            jSONObject2.put("ts", sb);
            return URLEncoder.encode(new String(Base64.encode(jSONObject2.toString().getBytes(), 0)), "utf-8");
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Map map, byte[] bArr) {
        if (bArr != null && bArr.length > 0) {
            String str = new String(bArr);
            if (!TextUtils.isEmpty(str)) {
                return a(str);
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return c.f6241c + "?p=" + g();
    }

    /* access modifiers changed from: protected */
    public final Map<String, String> c() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final byte[] d() {
        return new byte[0];
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return false;
    }
}
