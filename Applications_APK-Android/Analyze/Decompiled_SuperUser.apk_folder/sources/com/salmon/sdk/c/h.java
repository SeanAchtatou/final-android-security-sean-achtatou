package com.salmon.sdk.c;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.salmon.sdk.b.g;
import com.salmon.sdk.core.c;
import com.salmon.sdk.d.i;
import java.util.Map;
import org.json.JSONObject;

public class h extends a<g> {

    /* renamed from: d  reason: collision with root package name */
    private final String f6116d = h.class.getSimpleName();

    /* renamed from: e  reason: collision with root package name */
    private String f6117e;

    /* renamed from: f  reason: collision with root package name */
    private int f6118f = 1;

    /* renamed from: g  reason: collision with root package name */
    private Context f6119g;

    public h(Context context, int i, String str) {
        super(context);
        this.f6119g = context;
        this.f6117e = str;
        this.f6118f = i;
    }

    private g a(String str) {
        i.a(this.f6116d, "RESPONSE: " + str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            int optInt = jSONObject.optInt("status");
            String optString = jSONObject.optString("msg");
            if (optInt == 1) {
                g gVar = new g();
                try {
                    gVar.a(jSONObject.optJSONObject("data").toString());
                    return gVar;
                } catch (Exception e2) {
                    return gVar;
                }
            } else {
                Log.w(this.f6116d, "appSetting error: " + optString);
                return null;
            }
        } catch (Exception e3) {
            return null;
        }
    }

    private String g() {
        try {
            return "app_id=" + this.f6118f + "&sign=" + com.salmon.sdk.d.g.b(this.f6118f + this.f6117e);
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(Map map, byte[] bArr) {
        if (bArr != null && bArr.length > 0) {
            String str = new String(bArr);
            if (!TextUtils.isEmpty(str)) {
                return a(str);
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return c.f6242d + "?" + g();
    }

    /* access modifiers changed from: protected */
    public final Map<String, String> c() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final byte[] d() {
        return new byte[0];
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return false;
    }
}
