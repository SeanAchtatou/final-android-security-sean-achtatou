package com.stripe.android;

import com.kingouser.com.R;

/* compiled from: R */
public final class a {

    /* renamed from: com.stripe.android.a$a  reason: collision with other inner class name */
    /* compiled from: R */
    public static final class C0095a {
        public static final int abc_background_cache_hint_selector_material_dark = 2131558579;
        public static final int abc_background_cache_hint_selector_material_light = 2131558580;
        public static final int abc_btn_colored_borderless_text_material = 2131558581;
        public static final int abc_btn_colored_text_material = 2131558582;
        public static final int abc_color_highlight_material = 2131558583;
        public static final int abc_hint_foreground_material_dark = 2131558584;
        public static final int abc_hint_foreground_material_light = 2131558585;
        public static final int abc_input_method_navigation_guard = 2131558401;
        public static final int abc_primary_text_disable_only_material_dark = 2131558586;
        public static final int abc_primary_text_disable_only_material_light = 2131558587;
        public static final int abc_primary_text_material_dark = 2131558588;
        public static final int abc_primary_text_material_light = 2131558589;
        public static final int abc_search_url_text = 2131558590;
        public static final int abc_search_url_text_normal = 2131558402;
        public static final int abc_search_url_text_pressed = 2131558403;
        public static final int abc_search_url_text_selected = 2131558404;
        public static final int abc_secondary_text_material_dark = 2131558591;
        public static final int abc_secondary_text_material_light = 2131558592;
        public static final int abc_tint_btn_checkable = 2131558593;
        public static final int abc_tint_default = 2131558594;
        public static final int abc_tint_edittext = 2131558595;
        public static final int abc_tint_seek_thumb = 2131558596;
        public static final int abc_tint_spinner = 2131558597;
        public static final int abc_tint_switch_thumb = 2131558598;
        public static final int abc_tint_switch_track = 2131558599;
        public static final int accent_material_dark = 2131558409;
        public static final int accent_material_light = 2131558410;
        public static final int background_floating_material_dark = 2131558411;
        public static final int background_floating_material_light = 2131558412;
        public static final int background_material_dark = 2131558413;
        public static final int background_material_light = 2131558414;
        public static final int bright_foreground_disabled_material_dark = 2131558429;
        public static final int bright_foreground_disabled_material_light = 2131558430;
        public static final int bright_foreground_inverse_material_dark = 2131558431;
        public static final int bright_foreground_inverse_material_light = 2131558432;
        public static final int bright_foreground_material_dark = 2131558433;
        public static final int bright_foreground_material_light = 2131558434;
        public static final int button_material_dark = 2131558437;
        public static final int button_material_light = 2131558438;
        public static final int dim_foreground_disabled_material_dark = 2131558477;
        public static final int dim_foreground_disabled_material_light = 2131558478;
        public static final int dim_foreground_material_dark = 2131558479;
        public static final int dim_foreground_material_light = 2131558480;

        /* renamed from: error_text_dark_theme */
        public static final int bi = 2131558482;

        /* renamed from: error_text_light_theme */
        public static final int bj = 2131558483;
        public static final int foreground_material_dark = 2131558484;
        public static final int foreground_material_light = 2131558485;
        public static final int highlighted_text_material_dark = 2131558486;
        public static final int highlighted_text_material_light = 2131558487;
        public static final int material_blue_grey_800 = 2131558508;
        public static final int material_blue_grey_900 = 2131558509;
        public static final int material_blue_grey_950 = 2131558510;
        public static final int material_deep_teal_200 = 2131558511;
        public static final int material_deep_teal_500 = 2131558512;
        public static final int material_grey_100 = 2131558513;
        public static final int material_grey_300 = 2131558514;
        public static final int material_grey_50 = 2131558515;
        public static final int material_grey_600 = 2131558516;
        public static final int material_grey_800 = 2131558517;
        public static final int material_grey_850 = 2131558518;
        public static final int material_grey_900 = 2131558519;
        public static final int notification_action_color_filter = 2131558400;
        public static final int notification_icon_bg_color = 2131558523;
        public static final int notification_material_background_media_default_color = 2131558526;
        public static final int primary_dark_material_dark = 2131558536;
        public static final int primary_dark_material_light = 2131558537;
        public static final int primary_material_dark = 2131558538;
        public static final int primary_material_light = 2131558539;
        public static final int primary_text_default_material_dark = 2131558540;
        public static final int primary_text_default_material_light = 2131558541;
        public static final int primary_text_disabled_material_dark = 2131558542;
        public static final int primary_text_disabled_material_light = 2131558543;
        public static final int ripple_material_dark = 2131558545;
        public static final int ripple_material_light = 2131558546;
        public static final int secondary_text_default_material_dark = 2131558549;
        public static final int secondary_text_default_material_light = 2131558550;
        public static final int secondary_text_disabled_material_dark = 2131558551;
        public static final int secondary_text_disabled_material_light = 2131558552;
        public static final int switch_thumb_disabled_material_dark = 2131558563;
        public static final int switch_thumb_disabled_material_light = 2131558564;
        public static final int switch_thumb_material_dark = 2131558609;
        public static final int switch_thumb_material_light = 2131558610;
        public static final int switch_thumb_normal_material_dark = 2131558565;
        public static final int switch_thumb_normal_material_light = 2131558566;
    }

    /* compiled from: R */
    public static final class b {
        public static final int abc_action_bar_content_inset_material = 2131230758;
        public static final int abc_action_bar_content_inset_with_nav = 2131230759;
        public static final int abc_action_bar_default_height_material = 2131230747;
        public static final int abc_action_bar_default_padding_end_material = 2131230760;
        public static final int abc_action_bar_default_padding_start_material = 2131230761;
        public static final int abc_action_bar_elevation_material = 2131230800;
        public static final int abc_action_bar_icon_vertical_padding_material = 2131230801;
        public static final int abc_action_bar_overflow_padding_end_material = 2131230802;
        public static final int abc_action_bar_overflow_padding_start_material = 2131230803;
        public static final int abc_action_bar_progress_bar_size = 2131230748;
        public static final int abc_action_bar_stacked_max_height = 2131230804;
        public static final int abc_action_bar_stacked_tab_max_width = 2131230805;
        public static final int abc_action_bar_subtitle_bottom_margin_material = 2131230806;
        public static final int abc_action_bar_subtitle_top_margin_material = 2131230807;
        public static final int abc_action_button_min_height_material = 2131230808;
        public static final int abc_action_button_min_width_material = 2131230809;
        public static final int abc_action_button_min_width_overflow_material = 2131230810;
        public static final int abc_alert_dialog_button_bar_height = 2131230745;
        public static final int abc_button_inset_horizontal_material = 2131230811;
        public static final int abc_button_inset_vertical_material = 2131230812;
        public static final int abc_button_padding_horizontal_material = 2131230813;
        public static final int abc_button_padding_vertical_material = 2131230814;
        public static final int abc_cascading_menus_min_smallest_width = 2131230815;
        public static final int abc_config_prefDialogWidth = 2131230751;
        public static final int abc_control_corner_material = 2131230816;
        public static final int abc_control_inset_material = 2131230817;
        public static final int abc_control_padding_material = 2131230818;
        public static final int abc_dialog_fixed_height_major = 2131230752;
        public static final int abc_dialog_fixed_height_minor = 2131230753;
        public static final int abc_dialog_fixed_width_major = 2131230754;
        public static final int abc_dialog_fixed_width_minor = 2131230755;
        public static final int abc_dialog_list_padding_bottom_no_buttons = 2131230819;
        public static final int abc_dialog_list_padding_top_no_title = 2131230820;
        public static final int abc_dialog_min_width_major = 2131230756;
        public static final int abc_dialog_min_width_minor = 2131230757;
        public static final int abc_dialog_padding_material = 2131230821;
        public static final int abc_dialog_padding_top_material = 2131230822;
        public static final int abc_dialog_title_divider_material = 2131230823;
        public static final int abc_disabled_alpha_material_dark = 2131230824;
        public static final int abc_disabled_alpha_material_light = 2131230825;
        public static final int abc_dropdownitem_icon_width = 2131230826;
        public static final int abc_dropdownitem_text_padding_left = 2131230827;
        public static final int abc_dropdownitem_text_padding_right = 2131230828;
        public static final int abc_edit_text_inset_bottom_material = 2131230829;
        public static final int abc_edit_text_inset_horizontal_material = 2131230830;
        public static final int abc_edit_text_inset_top_material = 2131230831;
        public static final int abc_floating_window_z = 2131230832;
        public static final int abc_list_item_padding_horizontal_material = 2131230833;
        public static final int abc_panel_menu_list_width = 2131230834;
        public static final int abc_progress_bar_height_material = 2131230835;
        public static final int abc_search_view_preferred_height = 2131230836;
        public static final int abc_search_view_preferred_width = 2131230837;
        public static final int abc_seekbar_track_background_height_material = 2131230838;
        public static final int abc_seekbar_track_progress_height_material = 2131230839;
        public static final int abc_select_dialog_padding_start_material = 2131230840;
        public static final int abc_switch_padding = 2131230771;
        public static final int abc_text_size_body_1_material = 2131230841;
        public static final int abc_text_size_body_2_material = 2131230842;
        public static final int abc_text_size_button_material = 2131230843;
        public static final int abc_text_size_caption_material = 2131230844;
        public static final int abc_text_size_display_1_material = 2131230845;
        public static final int abc_text_size_display_2_material = 2131230846;
        public static final int abc_text_size_display_3_material = 2131230847;
        public static final int abc_text_size_display_4_material = 2131230848;
        public static final int abc_text_size_headline_material = 2131230849;
        public static final int abc_text_size_large_material = 2131230850;
        public static final int abc_text_size_medium_material = 2131230851;
        public static final int abc_text_size_menu_header_material = 2131230852;
        public static final int abc_text_size_menu_material = 2131230853;
        public static final int abc_text_size_small_material = 2131230854;
        public static final int abc_text_size_subhead_material = 2131230855;
        public static final int abc_text_size_subtitle_material_toolbar = 2131230749;
        public static final int abc_text_size_title_material = 2131230856;
        public static final int abc_text_size_title_material_toolbar = 2131230750;

        /* renamed from: android_pay_button_layout_margin */
        public static final int cr = 2131230861;

        /* renamed from: android_pay_button_separation */
        public static final int cs = 2131230862;

        /* renamed from: android_pay_confirmation_margin */
        public static final int ct = 2131230863;

        /* renamed from: card_cvc_initial_margin */
        public static final int d1 = 2131230871;

        /* renamed from: card_expiry_initial_margin */
        public static final int d2 = 2131230872;

        /* renamed from: card_icon_padding */
        public static final int d3 = 2131230873;

        /* renamed from: card_widget_min_width */
        public static final int d4 = 2131230874;
        public static final int disabled_alpha_material_dark = 2131230912;
        public static final int disabled_alpha_material_light = 2131230913;
        public static final int highlight_alpha_material_colored = 2131230918;
        public static final int highlight_alpha_material_dark = 2131230919;
        public static final int highlight_alpha_material_light = 2131230920;
        public static final int hint_alpha_material_dark = 2131230921;
        public static final int hint_alpha_material_light = 2131230922;
        public static final int hint_pressed_alpha_material_dark = 2131230923;
        public static final int hint_pressed_alpha_material_light = 2131230924;
        public static final int notification_action_icon_size = 2131230944;
        public static final int notification_action_text_size = 2131230945;
        public static final int notification_big_circle_margin = 2131230946;
        public static final int notification_content_margin_start = 2131230774;
        public static final int notification_large_icon_height = 2131230949;
        public static final int notification_large_icon_width = 2131230950;
        public static final int notification_main_column_padding_top = 2131230775;
        public static final int notification_media_narrow_margin = 2131230776;
        public static final int notification_right_icon_size = 2131230957;
        public static final int notification_right_side_padding_top = 2131230770;
        public static final int notification_small_icon_background_padding = 2131230960;
        public static final int notification_small_icon_size_as_large = 2131230961;
        public static final int notification_subtext_size = 2131230962;
        public static final int notification_top_pad = 2131230966;
        public static final int notification_top_pad_large_text = 2131230967;
    }

    /* compiled from: R */
    public static final class c {
        public static final int abc_ab_share_pack_mtrl_alpha = 2130837504;
        public static final int abc_action_bar_item_background_material = 2130837505;
        public static final int abc_btn_borderless_material = 2130837506;
        public static final int abc_btn_check_material = 2130837507;
        public static final int abc_btn_check_to_on_mtrl_000 = 2130837508;
        public static final int abc_btn_check_to_on_mtrl_015 = 2130837509;
        public static final int abc_btn_colored_material = 2130837510;
        public static final int abc_btn_default_mtrl_shape = 2130837511;
        public static final int abc_btn_radio_material = 2130837512;
        public static final int abc_btn_radio_to_on_mtrl_000 = 2130837513;
        public static final int abc_btn_radio_to_on_mtrl_015 = 2130837514;
        public static final int abc_btn_switch_to_on_mtrl_00001 = 2130837515;
        public static final int abc_btn_switch_to_on_mtrl_00012 = 2130837516;
        public static final int abc_cab_background_internal_bg = 2130837517;
        public static final int abc_cab_background_top_material = 2130837518;
        public static final int abc_cab_background_top_mtrl_alpha = 2130837519;
        public static final int abc_control_background_material = 2130837520;
        public static final int abc_dialog_material_background = 2130837521;
        public static final int abc_edit_text_material = 2130837522;
        public static final int abc_ic_ab_back_material = 2130837523;
        public static final int abc_ic_arrow_drop_right_black_24dp = 2130837524;
        public static final int abc_ic_clear_material = 2130837525;
        public static final int abc_ic_commit_search_api_mtrl_alpha = 2130837526;
        public static final int abc_ic_go_search_api_material = 2130837527;
        public static final int abc_ic_menu_copy_mtrl_am_alpha = 2130837528;
        public static final int abc_ic_menu_cut_mtrl_alpha = 2130837529;
        public static final int abc_ic_menu_overflow_material = 2130837530;
        public static final int abc_ic_menu_paste_mtrl_am_alpha = 2130837531;
        public static final int abc_ic_menu_selectall_mtrl_alpha = 2130837532;
        public static final int abc_ic_menu_share_mtrl_alpha = 2130837533;
        public static final int abc_ic_search_api_material = 2130837534;
        public static final int abc_ic_star_black_16dp = 2130837535;
        public static final int abc_ic_star_black_36dp = 2130837536;
        public static final int abc_ic_star_black_48dp = 2130837537;
        public static final int abc_ic_star_half_black_16dp = 2130837538;
        public static final int abc_ic_star_half_black_36dp = 2130837539;
        public static final int abc_ic_star_half_black_48dp = 2130837540;
        public static final int abc_ic_voice_search_api_material = 2130837541;
        public static final int abc_item_background_holo_dark = 2130837542;
        public static final int abc_item_background_holo_light = 2130837543;
        public static final int abc_list_divider_mtrl_alpha = 2130837544;
        public static final int abc_list_focused_holo = 2130837545;
        public static final int abc_list_longpressed_holo = 2130837546;
        public static final int abc_list_pressed_holo_dark = 2130837547;
        public static final int abc_list_pressed_holo_light = 2130837548;
        public static final int abc_list_selector_background_transition_holo_dark = 2130837549;
        public static final int abc_list_selector_background_transition_holo_light = 2130837550;
        public static final int abc_list_selector_disabled_holo_dark = 2130837551;
        public static final int abc_list_selector_disabled_holo_light = 2130837552;
        public static final int abc_list_selector_holo_dark = 2130837553;
        public static final int abc_list_selector_holo_light = 2130837554;
        public static final int abc_menu_hardkey_panel_mtrl_mult = 2130837555;
        public static final int abc_popup_background_mtrl_mult = 2130837556;
        public static final int abc_ratingbar_indicator_material = 2130837557;
        public static final int abc_ratingbar_material = 2130837558;
        public static final int abc_ratingbar_small_material = 2130837559;
        public static final int abc_scrubber_control_off_mtrl_alpha = 2130837560;
        public static final int abc_scrubber_control_to_pressed_mtrl_000 = 2130837561;
        public static final int abc_scrubber_control_to_pressed_mtrl_005 = 2130837562;
        public static final int abc_scrubber_primary_mtrl_alpha = 2130837563;
        public static final int abc_scrubber_track_mtrl_alpha = 2130837564;
        public static final int abc_seekbar_thumb_material = 2130837565;
        public static final int abc_seekbar_tick_mark_material = 2130837566;
        public static final int abc_seekbar_track_material = 2130837567;
        public static final int abc_spinner_mtrl_am_alpha = 2130837568;
        public static final int abc_spinner_textfield_background_material = 2130837569;
        public static final int abc_switch_thumb_material = 2130837570;
        public static final int abc_switch_track_mtrl_alpha = 2130837571;
        public static final int abc_tab_indicator_material = 2130837572;
        public static final int abc_tab_indicator_mtrl_alpha = 2130837573;
        public static final int abc_text_cursor_material = 2130837574;
        public static final int abc_text_select_handle_left_mtrl_dark = 2130837575;
        public static final int abc_text_select_handle_left_mtrl_light = 2130837576;
        public static final int abc_text_select_handle_middle_mtrl_dark = 2130837577;
        public static final int abc_text_select_handle_middle_mtrl_light = 2130837578;
        public static final int abc_text_select_handle_right_mtrl_dark = 2130837579;
        public static final int abc_text_select_handle_right_mtrl_light = 2130837580;
        public static final int abc_textfield_activated_mtrl_alpha = 2130837581;
        public static final int abc_textfield_default_mtrl_alpha = 2130837582;
        public static final int abc_textfield_search_activated_mtrl_alpha = 2130837583;
        public static final int abc_textfield_search_default_mtrl_alpha = 2130837584;
        public static final int abc_textfield_search_material = 2130837585;
        public static final int abc_vector_test = 2130837586;

        /* renamed from: ic_amex  reason: collision with root package name */
        public static final int f6787ic_amex = 2130837681;

        /* renamed from: ic_cvc  reason: collision with root package name */
        public static final int f6788ic_cvc = 2130837692;

        /* renamed from: ic_cvc_amex  reason: collision with root package name */
        public static final int f6789ic_cvc_amex = 2130837693;

        /* renamed from: ic_diners  reason: collision with root package name */
        public static final int f6790ic_diners = 2130837694;

        /* renamed from: ic_discover  reason: collision with root package name */
        public static final int f6791ic_discover = 2130837695;

        /* renamed from: ic_error  reason: collision with root package name */
        public static final int f6792ic_error = 2130837696;

        /* renamed from: ic_error_amex */
        public static final int i = 2130837697;

        /* renamed from: ic_jcb */
        public static final int j = 2130837698;

        /* renamed from: ic_mastercard */
        public static final int k = 2130837700;

        /* renamed from: ic_unknown */
        public static final int l = 2130837712;

        /* renamed from: ic_visa */
        public static final int m = 2130837713;
        public static final int notification_action_background = 2130837740;
        public static final int notification_bg = 2130837741;
        public static final int notification_bg_low = 2130837742;
        public static final int notification_bg_low_normal = 2130837743;
        public static final int notification_bg_low_pressed = 2130837744;
        public static final int notification_bg_normal = 2130837745;
        public static final int notification_bg_normal_pressed = 2130837746;
        public static final int notification_icon_background = 2130837753;
        public static final int notification_template_icon_bg = 2130837834;
        public static final int notification_template_icon_low_bg = 2130837835;
        public static final int notification_tile_bg = 2130837762;
        public static final int notify_panel_notification_icon_bg = 2130837767;
    }

    /* compiled from: R */
    public static final class d {
        public static final int action0 = 2131624325;
        public static final int action_bar = 2131624046;
        public static final int action_bar_activity_content = 2131623936;
        public static final int action_bar_container = 2131624045;
        public static final int action_bar_root = 2131624041;
        public static final int action_bar_spinner = 2131623937;
        public static final int action_bar_subtitle = 2131624012;
        public static final int action_bar_title = 2131624011;
        public static final int action_container = 2131624314;
        public static final int action_context_bar = 2131624047;
        public static final int action_divider = 2131624333;
        public static final int action_image = 2131624315;
        public static final int action_menu_divider = 2131623938;
        public static final int action_menu_presenter = 2131623939;
        public static final int action_mode_bar = 2131624043;
        public static final int action_mode_bar_stub = 2131624042;
        public static final int action_mode_close_button = 2131624013;
        public static final int action_text = 2131624316;
        public static final int actions = 2131624341;
        public static final int activity_chooser_view_content = 2131624014;
        public static final int add = 2131623967;
        public static final int alertTitle = 2131624034;
        public static final int always = 2131623999;
        public static final int beginning = 2131623993;
        public static final int bottom = 2131623975;
        public static final int buttonPanel = 2131624021;
        public static final int cancel_action = 2131624326;

        /* renamed from: card_number_label */
        public static final int f9 = 2131624156;
        public static final int checkbox = 2131624037;
        public static final int chronometer = 2131624337;
        public static final int collapseActionView = 2131624000;
        public static final int contentPanel = 2131624024;
        public static final int custom = 2131624031;
        public static final int customPanel = 2131624030;
        public static final int decor_content_parent = 2131624044;
        public static final int default_activity_button = 2131624017;
        public static final int disableHome = 2131623955;
        public static final int edit_query = 2131624048;
        public static final int end = 2131623979;
        public static final int end_padder = 2131624346;

        /* renamed from: et_card_number */
        public static final int f_ = 2131624157;

        /* renamed from: et_cvc_number */
        public static final int fc = 2131624160;

        /* renamed from: et_expiry_date */
        public static final int fb = 2131624159;
        public static final int expand_activities_button = 2131624015;
        public static final int expanded_menu = 2131624036;

        /* renamed from: expiry_date_label */
        public static final int fa = 2131624158;

        /* renamed from: frame_container */
        public static final int f8 = 2131624155;
        public static final int home = 2131623940;
        public static final int homeAsUp = 2131623956;
        public static final int icon = 2131624019;
        public static final int icon_group = 2131624342;
        public static final int ifRoom = 2131624001;
        public static final int image = 2131624016;
        public static final int info = 2131624338;

        /* renamed from: iv_card_icon */
        public static final int f7 = 2131624154;
        public static final int line1 = 2131624343;
        public static final int line3 = 2131624345;
        public static final int listMode = 2131623952;
        public static final int list_item = 2131624018;
        public static final int media_actions = 2131624332;
        public static final int middle = 2131623994;
        public static final int multiply = 2131623968;
        public static final int never = 2131624002;
        public static final int none = 2131623957;
        public static final int normal = 2131623953;
        public static final int notification_background = 2131624340;
        public static final int notification_main_column = 2131624335;
        public static final int notification_main_column_container = 2131624334;
        public static final int parentPanel = 2131624023;
        public static final int progress_circular = 2131623943;
        public static final int progress_horizontal = 2131623944;
        public static final int radio = 2131624039;
        public static final int right_icon = 2131624339;
        public static final int right_side = 2131624336;
        public static final int screen = 2131623969;
        public static final int scrollIndicatorDown = 2131624029;
        public static final int scrollIndicatorUp = 2131624025;
        public static final int scrollView = 2131624026;
        public static final int search_badge = 2131624050;
        public static final int search_bar = 2131624049;
        public static final int search_button = 2131624051;
        public static final int search_close_btn = 2131624056;
        public static final int search_edit_frame = 2131624052;
        public static final int search_go_btn = 2131624058;
        public static final int search_mag_icon = 2131624053;
        public static final int search_plate = 2131624054;
        public static final int search_src_text = 2131624055;
        public static final int search_voice_btn = 2131624059;
        public static final int select_dialog_listview = 2131624060;
        public static final int shortcut = 2131624038;
        public static final int showCustom = 2131623958;
        public static final int showHome = 2131623959;
        public static final int showTitle = 2131623960;
        public static final int spacer = 2131624022;
        public static final int split_action_bar = 2131623945;
        public static final int src_atop = 2131623970;
        public static final int src_in = 2131623971;
        public static final int src_over = 2131623972;
        public static final int status_bar_latest_event_content = 2131624331;
        public static final int submenuarrow = 2131624040;
        public static final int submit_area = 2131624057;
        public static final int tabMode = 2131623954;
        public static final int text = 2131624101;
        public static final int text2 = 2131624344;
        public static final int textSpacerNoButtons = 2131624028;
        public static final int textSpacerNoTitle = 2131624027;
        public static final int time = 2131624321;
        public static final int title = 2131624020;
        public static final int titleDividerNoCustom = 2131624035;
        public static final int title_template = 2131624033;
        public static final int top = 2131623984;
        public static final int topPanel = 2131624032;
        public static final int up = 2131623950;
        public static final int useLogo = 2131623961;
        public static final int withText = 2131624003;
        public static final int wrap_content = 2131623973;
    }

    /* compiled from: R */
    public static final class e {
        public static final int abc_action_bar_title_item = 2130968576;
        public static final int abc_action_bar_up_container = 2130968577;
        public static final int abc_action_bar_view_list_nav_layout = 2130968578;
        public static final int abc_action_menu_item_layout = 2130968579;
        public static final int abc_action_menu_layout = 2130968580;
        public static final int abc_action_mode_bar = 2130968581;
        public static final int abc_action_mode_close_item_material = 2130968582;
        public static final int abc_activity_chooser_view = 2130968583;
        public static final int abc_activity_chooser_view_list_item = 2130968584;
        public static final int abc_alert_dialog_button_bar_material = 2130968585;
        public static final int abc_alert_dialog_material = 2130968586;
        public static final int abc_alert_dialog_title_material = 2130968587;
        public static final int abc_dialog_title_material = 2130968588;
        public static final int abc_expanded_menu_layout = 2130968589;
        public static final int abc_list_menu_item_checkbox = 2130968590;
        public static final int abc_list_menu_item_icon = 2130968591;
        public static final int abc_list_menu_item_layout = 2130968592;
        public static final int abc_list_menu_item_radio = 2130968593;
        public static final int abc_popup_menu_header_item_layout = 2130968594;
        public static final int abc_popup_menu_item_layout = 2130968595;
        public static final int abc_screen_content_include = 2130968596;
        public static final int abc_screen_simple = 2130968597;
        public static final int abc_screen_simple_overlay_action_mode = 2130968598;
        public static final int abc_screen_toolbar = 2130968599;
        public static final int abc_search_dropdown_item_icons_2line = 2130968600;
        public static final int abc_search_view = 2130968601;
        public static final int abc_select_dialog_material = 2130968602;

        /* renamed from: card_input_widget */
        public static final int ak = 2130968623;
        public static final int notification_action = 2130968672;
        public static final int notification_action_tombstone = 2130968673;
        public static final int notification_media_action = 2130968678;
        public static final int notification_media_cancel_action = 2130968679;
        public static final int notification_template_big_media = 2130968682;
        public static final int notification_template_big_media_custom = 2130968683;
        public static final int notification_template_big_media_narrow = 2130968684;
        public static final int notification_template_big_media_narrow_custom = 2130968685;
        public static final int notification_template_custom_big = 2130968686;
        public static final int notification_template_icon_group = 2130968687;
        public static final int notification_template_lines_media = 2130968688;
        public static final int notification_template_media = 2130968689;
        public static final int notification_template_media_custom = 2130968690;
        public static final int notification_template_part_chronometer = 2130968691;
        public static final int notification_template_part_time = 2130968692;
        public static final int select_dialog_item_material = 2130968703;
        public static final int select_dialog_multichoice_material = 2130968704;
        public static final int select_dialog_singlechoice_material = 2130968705;
        public static final int support_simple_spinner_dropdown_item = 2130968712;
    }

    /* compiled from: R */
    public static final class f {
        public static final int abc_action_bar_home_description = 2131296256;
        public static final int abc_action_bar_home_description_format = 2131296257;
        public static final int abc_action_bar_home_subtitle_description_format = 2131296258;
        public static final int abc_action_bar_up_description = 2131296259;
        public static final int abc_action_menu_overflow_description = 2131296260;
        public static final int abc_action_mode_done = 2131296261;
        public static final int abc_activity_chooser_view_see_all = 2131296262;
        public static final int abc_activitychooserview_choose_application = 2131296263;
        public static final int abc_capital_off = 2131296264;
        public static final int abc_capital_on = 2131296265;
        public static final int abc_font_family_body_1_material = 2131296495;
        public static final int abc_font_family_body_2_material = 2131296496;
        public static final int abc_font_family_button_material = 2131296497;
        public static final int abc_font_family_caption_material = 2131296498;
        public static final int abc_font_family_display_1_material = 2131296499;
        public static final int abc_font_family_display_2_material = 2131296500;
        public static final int abc_font_family_display_3_material = 2131296501;
        public static final int abc_font_family_display_4_material = 2131296502;
        public static final int abc_font_family_headline_material = 2131296503;
        public static final int abc_font_family_menu_material = 2131296504;
        public static final int abc_font_family_subhead_material = 2131296505;
        public static final int abc_font_family_title_material = 2131296506;
        public static final int abc_search_hint = 2131296266;
        public static final int abc_searchview_description_clear = 2131296267;
        public static final int abc_searchview_description_query = 2131296268;
        public static final int abc_searchview_description_search = 2131296269;
        public static final int abc_searchview_description_submit = 2131296270;
        public static final int abc_searchview_description_voice = 2131296271;
        public static final int abc_shareactionprovider_share_with = 2131296272;
        public static final int abc_shareactionprovider_share_with_application = 2131296273;
        public static final int abc_toolbar_collapse_description = 2131296274;

        /* renamed from: acc_label_card_number */
        public static final int af = 2131296298;

        /* renamed from: acc_label_expiry_date */
        public static final int ag = 2131296299;

        /* renamed from: card_number_hint */
        public static final int g7 = 2131296511;

        /* renamed from: cvc_amex_hint */
        public static final int ga = 2131296516;

        /* renamed from: cvc_number_hint */
        public static final int gb = 2131296517;

        /* renamed from: expiry_date_hint */
        public static final int c6 = 2131296362;
        public static final int search_menu_title = 2131296292;
        public static final int status_bar_notification_info_overflow = 2131296293;
    }

    /* compiled from: R */
    public static final class g {
        public static final int[] ActionBar = {R.attr.f8249f, R.attr.w, R.attr.y, R.attr.z, R.attr.a0, R.attr.a1, R.attr.a2, R.attr.a3, R.attr.a4, R.attr.a5, R.attr.a6, R.attr.a7, R.attr.a8, R.attr.a9, R.attr.a_, R.attr.aa, R.attr.ab, R.attr.ac, R.attr.ad, R.attr.ae, R.attr.af, R.attr.ag, R.attr.ah, R.attr.ai, R.attr.aj, R.attr.ak, R.attr.al, R.attr.am, R.attr.cf};
        public static final int[] ActionBarLayout = {16842931};
        public static final int ActionBarLayout_android_layout_gravity = 0;
        public static final int ActionBar_background = 10;
        public static final int ActionBar_backgroundSplit = 12;
        public static final int ActionBar_backgroundStacked = 11;
        public static final int ActionBar_contentInsetEnd = 21;
        public static final int ActionBar_contentInsetEndWithActions = 25;
        public static final int ActionBar_contentInsetLeft = 22;
        public static final int ActionBar_contentInsetRight = 23;
        public static final int ActionBar_contentInsetStart = 20;
        public static final int ActionBar_contentInsetStartWithNavigation = 24;
        public static final int ActionBar_customNavigationLayout = 13;
        public static final int ActionBar_displayOptions = 3;
        public static final int ActionBar_divider = 9;
        public static final int ActionBar_elevation = 26;
        public static final int ActionBar_height = 0;
        public static final int ActionBar_hideOnContentScroll = 19;
        public static final int ActionBar_homeAsUpIndicator = 28;
        public static final int ActionBar_homeLayout = 14;
        public static final int ActionBar_icon = 7;
        public static final int ActionBar_indeterminateProgressStyle = 16;
        public static final int ActionBar_itemPadding = 18;
        public static final int ActionBar_logo = 8;
        public static final int ActionBar_navigationMode = 2;
        public static final int ActionBar_popupTheme = 27;
        public static final int ActionBar_progressBarPadding = 17;
        public static final int ActionBar_progressBarStyle = 15;
        public static final int ActionBar_subtitle = 4;
        public static final int ActionBar_subtitleTextStyle = 6;
        public static final int ActionBar_title = 1;
        public static final int ActionBar_titleTextStyle = 5;
        public static final int[] ActionMenuItemView = {16843071};
        public static final int ActionMenuItemView_android_minWidth = 0;
        public static final int[] ActionMenuView = new int[0];
        public static final int[] ActionMode = {R.attr.f8249f, R.attr.a1, R.attr.a2, R.attr.a6, R.attr.a8, R.attr.an};
        public static final int ActionMode_background = 3;
        public static final int ActionMode_backgroundSplit = 4;
        public static final int ActionMode_closeItemLayout = 5;
        public static final int ActionMode_height = 0;
        public static final int ActionMode_subtitleTextStyle = 2;
        public static final int ActionMode_titleTextStyle = 1;
        public static final int[] ActivityChooserView = {R.attr.ao, R.attr.ap};
        public static final int ActivityChooserView_expandActivityOverflowButtonDrawable = 1;
        public static final int ActivityChooserView_initialActivityCount = 0;
        public static final int[] AlertDialog = {16842994, R.attr.aq, R.attr.ar, R.attr.as, R.attr.at, R.attr.au, R.attr.av};
        public static final int AlertDialog_android_layout = 0;
        public static final int AlertDialog_buttonPanelSideLayout = 1;
        public static final int AlertDialog_listItemLayout = 5;
        public static final int AlertDialog_listLayout = 2;
        public static final int AlertDialog_multiChoiceItemLayout = 3;
        public static final int AlertDialog_showTitle = 6;
        public static final int AlertDialog_singleChoiceItemLayout = 4;
        public static final int[] AppCompatImageView = {16843033, R.attr.b1};
        public static final int AppCompatImageView_android_src = 0;
        public static final int AppCompatImageView_srcCompat = 1;
        public static final int[] AppCompatSeekBar = {16843074, R.attr.b2, R.attr.b3, R.attr.b4};
        public static final int AppCompatSeekBar_android_thumb = 0;
        public static final int AppCompatSeekBar_tickMark = 1;
        public static final int AppCompatSeekBar_tickMarkTint = 2;
        public static final int AppCompatSeekBar_tickMarkTintMode = 3;
        public static final int[] AppCompatTextHelper = {16842804, 16843117, 16843118, 16843119, 16843120, 16843666, 16843667};
        public static final int AppCompatTextHelper_android_drawableBottom = 2;
        public static final int AppCompatTextHelper_android_drawableEnd = 6;
        public static final int AppCompatTextHelper_android_drawableLeft = 3;
        public static final int AppCompatTextHelper_android_drawableRight = 4;
        public static final int AppCompatTextHelper_android_drawableStart = 5;
        public static final int AppCompatTextHelper_android_drawableTop = 1;
        public static final int AppCompatTextHelper_android_textAppearance = 0;
        public static final int[] AppCompatTextView = {16842804, R.attr.b5};
        public static final int AppCompatTextView_android_textAppearance = 0;
        public static final int AppCompatTextView_textAllCaps = 1;
        public static final int[] AppCompatTheme = {16842839, 16842926, R.attr.b6, R.attr.b7, R.attr.b8, R.attr.b9, R.attr.b_, R.attr.ba, R.attr.bb, R.attr.bc, R.attr.bd, R.attr.be, R.attr.bf, R.attr.bg, R.attr.bh, R.attr.bi, R.attr.bj, R.attr.bk, R.attr.bl, R.attr.bm, R.attr.bn, R.attr.bo, R.attr.bp, R.attr.bq, R.attr.br, R.attr.bs, R.attr.bt, R.attr.bu, R.attr.bv, R.attr.bw, R.attr.bx, R.attr.by, R.attr.bz, R.attr.c0, R.attr.c1, R.attr.c2, R.attr.c3, R.attr.c4, R.attr.c5, R.attr.c6, R.attr.c7, R.attr.c8, R.attr.c9, R.attr.c_, R.attr.ca, R.attr.cb, R.attr.cc, R.attr.cd, R.attr.ce, R.attr.cf, R.attr.cg, R.attr.ch, R.attr.ci, R.attr.cj, R.attr.ck, R.attr.cl, R.attr.cm, R.attr.cn, R.attr.co, R.attr.cp, R.attr.cq, R.attr.cr, R.attr.cs, R.attr.ct, R.attr.cu, R.attr.cv, R.attr.cw, R.attr.cx, R.attr.cy, R.attr.cz, R.attr.d0, R.attr.d1, R.attr.d2, R.attr.d3, R.attr.d4, R.attr.d5, R.attr.d6, R.attr.d7, R.attr.d8, R.attr.d9, R.attr.d_, R.attr.da, R.attr.db, R.attr.dc, R.attr.dd, R.attr.de, R.attr.df, R.attr.dg, R.attr.dh, R.attr.di, R.attr.dj, R.attr.dk, R.attr.dl, R.attr.dm, R.attr.dn, R.attr.f0do, R.attr.dp, R.attr.dq, R.attr.dr, R.attr.ds, R.attr.dt, R.attr.du, R.attr.dv, R.attr.dw, R.attr.dx, R.attr.dy, R.attr.dz, R.attr.e0, R.attr.e1, R.attr.e2, R.attr.e3, R.attr.e4, R.attr.e5, R.attr.e6, R.attr.e7};
        public static final int AppCompatTheme_actionBarDivider = 23;
        public static final int AppCompatTheme_actionBarItemBackground = 24;
        public static final int AppCompatTheme_actionBarPopupTheme = 17;
        public static final int AppCompatTheme_actionBarSize = 22;
        public static final int AppCompatTheme_actionBarSplitStyle = 19;
        public static final int AppCompatTheme_actionBarStyle = 18;
        public static final int AppCompatTheme_actionBarTabBarStyle = 13;
        public static final int AppCompatTheme_actionBarTabStyle = 12;
        public static final int AppCompatTheme_actionBarTabTextStyle = 14;
        public static final int AppCompatTheme_actionBarTheme = 20;
        public static final int AppCompatTheme_actionBarWidgetTheme = 21;
        public static final int AppCompatTheme_actionButtonStyle = 50;
        public static final int AppCompatTheme_actionDropDownStyle = 46;
        public static final int AppCompatTheme_actionMenuTextAppearance = 25;
        public static final int AppCompatTheme_actionMenuTextColor = 26;
        public static final int AppCompatTheme_actionModeBackground = 29;
        public static final int AppCompatTheme_actionModeCloseButtonStyle = 28;
        public static final int AppCompatTheme_actionModeCloseDrawable = 31;
        public static final int AppCompatTheme_actionModeCopyDrawable = 33;
        public static final int AppCompatTheme_actionModeCutDrawable = 32;
        public static final int AppCompatTheme_actionModeFindDrawable = 37;
        public static final int AppCompatTheme_actionModePasteDrawable = 34;
        public static final int AppCompatTheme_actionModePopupWindowStyle = 39;
        public static final int AppCompatTheme_actionModeSelectAllDrawable = 35;
        public static final int AppCompatTheme_actionModeShareDrawable = 36;
        public static final int AppCompatTheme_actionModeSplitBackground = 30;
        public static final int AppCompatTheme_actionModeStyle = 27;
        public static final int AppCompatTheme_actionModeWebSearchDrawable = 38;
        public static final int AppCompatTheme_actionOverflowButtonStyle = 15;
        public static final int AppCompatTheme_actionOverflowMenuStyle = 16;
        public static final int AppCompatTheme_activityChooserViewStyle = 58;
        public static final int AppCompatTheme_alertDialogButtonGroupStyle = 94;
        public static final int AppCompatTheme_alertDialogCenterButtons = 95;
        public static final int AppCompatTheme_alertDialogStyle = 93;
        public static final int AppCompatTheme_alertDialogTheme = 96;
        public static final int AppCompatTheme_android_windowAnimationStyle = 1;
        public static final int AppCompatTheme_android_windowIsFloating = 0;
        public static final int AppCompatTheme_autoCompleteTextViewStyle = 101;
        public static final int AppCompatTheme_borderlessButtonStyle = 55;
        public static final int AppCompatTheme_buttonBarButtonStyle = 52;
        public static final int AppCompatTheme_buttonBarNegativeButtonStyle = 99;
        public static final int AppCompatTheme_buttonBarNeutralButtonStyle = 100;
        public static final int AppCompatTheme_buttonBarPositiveButtonStyle = 98;
        public static final int AppCompatTheme_buttonBarStyle = 51;
        public static final int AppCompatTheme_buttonStyle = 102;
        public static final int AppCompatTheme_buttonStyleSmall = 103;
        public static final int AppCompatTheme_checkboxStyle = 104;
        public static final int AppCompatTheme_checkedTextViewStyle = 105;
        public static final int AppCompatTheme_colorAccent = 85;
        public static final int AppCompatTheme_colorBackgroundFloating = 92;
        public static final int AppCompatTheme_colorButtonNormal = 89;
        public static final int AppCompatTheme_colorControlActivated = 87;
        public static final int AppCompatTheme_colorControlHighlight = 88;
        public static final int AppCompatTheme_colorControlNormal = 86;
        public static final int AppCompatTheme_colorPrimary = 83;
        public static final int AppCompatTheme_colorPrimaryDark = 84;
        public static final int AppCompatTheme_colorSwitchThumbNormal = 90;
        public static final int AppCompatTheme_controlBackground = 91;
        public static final int AppCompatTheme_dialogPreferredPadding = 44;
        public static final int AppCompatTheme_dialogTheme = 43;
        public static final int AppCompatTheme_dividerHorizontal = 57;
        public static final int AppCompatTheme_dividerVertical = 56;
        public static final int AppCompatTheme_dropDownListViewStyle = 75;
        public static final int AppCompatTheme_dropdownListPreferredItemHeight = 47;
        public static final int AppCompatTheme_editTextBackground = 64;
        public static final int AppCompatTheme_editTextColor = 63;
        public static final int AppCompatTheme_editTextStyle = 106;
        public static final int AppCompatTheme_homeAsUpIndicator = 49;
        public static final int AppCompatTheme_imageButtonStyle = 65;
        public static final int AppCompatTheme_listChoiceBackgroundIndicator = 82;
        public static final int AppCompatTheme_listDividerAlertDialog = 45;
        public static final int AppCompatTheme_listMenuViewStyle = 114;
        public static final int AppCompatTheme_listPopupWindowStyle = 76;
        public static final int AppCompatTheme_listPreferredItemHeight = 70;
        public static final int AppCompatTheme_listPreferredItemHeightLarge = 72;
        public static final int AppCompatTheme_listPreferredItemHeightSmall = 71;
        public static final int AppCompatTheme_listPreferredItemPaddingLeft = 73;
        public static final int AppCompatTheme_listPreferredItemPaddingRight = 74;
        public static final int AppCompatTheme_panelBackground = 79;
        public static final int AppCompatTheme_panelMenuListTheme = 81;
        public static final int AppCompatTheme_panelMenuListWidth = 80;
        public static final int AppCompatTheme_popupMenuStyle = 61;
        public static final int AppCompatTheme_popupWindowStyle = 62;
        public static final int AppCompatTheme_radioButtonStyle = 107;
        public static final int AppCompatTheme_ratingBarStyle = 108;
        public static final int AppCompatTheme_ratingBarStyleIndicator = 109;
        public static final int AppCompatTheme_ratingBarStyleSmall = 110;
        public static final int AppCompatTheme_searchViewStyle = 69;
        public static final int AppCompatTheme_seekBarStyle = 111;
        public static final int AppCompatTheme_selectableItemBackground = 53;
        public static final int AppCompatTheme_selectableItemBackgroundBorderless = 54;
        public static final int AppCompatTheme_spinnerDropDownItemStyle = 48;
        public static final int AppCompatTheme_spinnerStyle = 112;
        public static final int AppCompatTheme_switchStyle = 113;
        public static final int AppCompatTheme_textAppearanceLargePopupMenu = 40;
        public static final int AppCompatTheme_textAppearanceListItem = 77;
        public static final int AppCompatTheme_textAppearanceListItemSmall = 78;
        public static final int AppCompatTheme_textAppearancePopupMenuHeader = 42;
        public static final int AppCompatTheme_textAppearanceSearchResultSubtitle = 67;
        public static final int AppCompatTheme_textAppearanceSearchResultTitle = 66;
        public static final int AppCompatTheme_textAppearanceSmallPopupMenu = 41;
        public static final int AppCompatTheme_textColorAlertDialogListItem = 97;
        public static final int AppCompatTheme_textColorSearchUrl = 68;
        public static final int AppCompatTheme_toolbarNavigationButtonStyle = 60;
        public static final int AppCompatTheme_toolbarStyle = 59;
        public static final int AppCompatTheme_windowActionBar = 2;
        public static final int AppCompatTheme_windowActionBarOverlay = 4;
        public static final int AppCompatTheme_windowActionModeOverlay = 5;
        public static final int AppCompatTheme_windowFixedHeightMajor = 9;
        public static final int AppCompatTheme_windowFixedHeightMinor = 7;
        public static final int AppCompatTheme_windowFixedWidthMajor = 6;
        public static final int AppCompatTheme_windowFixedWidthMinor = 8;
        public static final int AppCompatTheme_windowMinWidthMajor = 10;
        public static final int AppCompatTheme_windowMinWidthMinor = 11;
        public static final int AppCompatTheme_windowNoTitle = 3;
        public static final int[] ButtonBarLayout = {R.attr.eh};
        public static final int ButtonBarLayout_allowStacking = 0;
        public static final int[] CardInputView = {R.attr.ei, R.attr.ej, R.attr.ek};
        public static final int CardInputView_cardHintText = 0;
        public static final int CardInputView_cardTextErrorColor = 2;
        public static final int CardInputView_cardTint = 1;
        public static final int[] ColorStateListItem = {16843173, 16843551, R.attr.fc};
        public static final int ColorStateListItem_alpha = 2;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_android_color = 0;
        public static final int[] CompoundButton = {16843015, R.attr.fd, R.attr.fe};
        public static final int CompoundButton_android_button = 0;
        public static final int CompoundButton_buttonTint = 1;
        public static final int CompoundButton_buttonTintMode = 2;
        public static final int[] DrawerArrowToggle = {R.attr.fq, R.attr.fr, R.attr.fs, R.attr.ft, R.attr.fu, R.attr.fv, R.attr.fw, R.attr.fx};
        public static final int DrawerArrowToggle_arrowHeadLength = 4;
        public static final int DrawerArrowToggle_arrowShaftLength = 5;
        public static final int DrawerArrowToggle_barLength = 6;
        public static final int DrawerArrowToggle_color = 0;
        public static final int DrawerArrowToggle_drawableSize = 2;
        public static final int DrawerArrowToggle_gapBetweenBars = 3;
        public static final int DrawerArrowToggle_spinBars = 1;
        public static final int DrawerArrowToggle_thickness = 7;
        public static final int[] LinearLayoutCompat = {16842927, 16842948, 16843046, 16843047, 16843048, R.attr.a5, R.attr.g6, R.attr.g7, R.attr.g8};
        public static final int[] LinearLayoutCompat_Layout = {16842931, 16842996, 16842997, 16843137};
        public static final int LinearLayoutCompat_Layout_android_layout_gravity = 0;
        public static final int LinearLayoutCompat_Layout_android_layout_height = 2;
        public static final int LinearLayoutCompat_Layout_android_layout_weight = 3;
        public static final int LinearLayoutCompat_Layout_android_layout_width = 1;
        public static final int LinearLayoutCompat_android_baselineAligned = 2;
        public static final int LinearLayoutCompat_android_baselineAlignedChildIndex = 3;
        public static final int LinearLayoutCompat_android_gravity = 0;
        public static final int LinearLayoutCompat_android_orientation = 1;
        public static final int LinearLayoutCompat_android_weightSum = 4;
        public static final int LinearLayoutCompat_divider = 5;
        public static final int LinearLayoutCompat_dividerPadding = 8;
        public static final int LinearLayoutCompat_measureWithLargestChild = 6;
        public static final int LinearLayoutCompat_showDividers = 7;
        public static final int[] ListPopupWindow = {16843436, 16843437};
        public static final int ListPopupWindow_android_dropDownHorizontalOffset = 0;
        public static final int ListPopupWindow_android_dropDownVerticalOffset = 1;
        public static final int[] MenuGroup = {16842766, 16842960, 16843156, 16843230, 16843231, 16843232};
        public static final int MenuGroup_android_checkableBehavior = 5;
        public static final int MenuGroup_android_enabled = 0;
        public static final int MenuGroup_android_id = 1;
        public static final int MenuGroup_android_menuCategory = 3;
        public static final int MenuGroup_android_orderInCategory = 4;
        public static final int MenuGroup_android_visible = 2;
        public static final int[] MenuItem = {16842754, 16842766, 16842960, 16843014, 16843156, 16843230, 16843231, 16843233, 16843234, 16843235, 16843236, 16843237, 16843375, R.attr.gk, R.attr.gl, R.attr.gm, R.attr.gn};
        public static final int MenuItem_actionLayout = 14;
        public static final int MenuItem_actionProviderClass = 16;
        public static final int MenuItem_actionViewClass = 15;
        public static final int MenuItem_android_alphabeticShortcut = 9;
        public static final int MenuItem_android_checkable = 11;
        public static final int MenuItem_android_checked = 3;
        public static final int MenuItem_android_enabled = 1;
        public static final int MenuItem_android_icon = 0;
        public static final int MenuItem_android_id = 2;
        public static final int MenuItem_android_menuCategory = 5;
        public static final int MenuItem_android_numericShortcut = 10;
        public static final int MenuItem_android_onClick = 12;
        public static final int MenuItem_android_orderInCategory = 6;
        public static final int MenuItem_android_title = 7;
        public static final int MenuItem_android_titleCondensed = 8;
        public static final int MenuItem_android_visible = 4;
        public static final int MenuItem_showAsAction = 13;
        public static final int[] MenuView = {16842926, 16843052, 16843053, 16843054, 16843055, 16843056, 16843057, R.attr.go, R.attr.gp};
        public static final int MenuView_android_headerBackground = 4;
        public static final int MenuView_android_horizontalDivider = 2;
        public static final int MenuView_android_itemBackground = 5;
        public static final int MenuView_android_itemIconDisabledAlpha = 6;
        public static final int MenuView_android_itemTextAppearance = 1;
        public static final int MenuView_android_verticalDivider = 3;
        public static final int MenuView_android_windowAnimationStyle = 0;
        public static final int MenuView_preserveIconSpacing = 7;
        public static final int MenuView_subMenuArrow = 8;
        public static final int[] PopupWindow = {16843126, 16843465, R.attr.gw};
        public static final int[] PopupWindowBackgroundState = {R.attr.gx};
        public static final int PopupWindowBackgroundState_state_above_anchor = 0;
        public static final int PopupWindow_android_popupAnimationStyle = 1;
        public static final int PopupWindow_android_popupBackground = 0;
        public static final int PopupWindow_overlapAnchor = 2;
        public static final int[] RecycleListView = {R.attr.gy, R.attr.gz};
        public static final int RecycleListView_paddingBottomNoButtons = 0;
        public static final int RecycleListView_paddingTopNoTitle = 1;
        public static final int[] SearchView = {16842970, 16843039, 16843296, 16843364, R.attr.h_, R.attr.ha, R.attr.hb, R.attr.hc, R.attr.hd, R.attr.he, R.attr.hf, R.attr.hg, R.attr.hh, R.attr.hi, R.attr.hj, R.attr.hk, R.attr.hl};
        public static final int SearchView_android_focusable = 0;
        public static final int SearchView_android_imeOptions = 3;
        public static final int SearchView_android_inputType = 2;
        public static final int SearchView_android_maxWidth = 1;
        public static final int SearchView_closeIcon = 8;
        public static final int SearchView_commitIcon = 13;
        public static final int SearchView_defaultQueryHint = 7;
        public static final int SearchView_goIcon = 9;
        public static final int SearchView_iconifiedByDefault = 5;
        public static final int SearchView_layout = 4;
        public static final int SearchView_queryBackground = 15;
        public static final int SearchView_queryHint = 6;
        public static final int SearchView_searchHintIcon = 11;
        public static final int SearchView_searchIcon = 10;
        public static final int SearchView_submitBackground = 16;
        public static final int SearchView_suggestionRowLayout = 14;
        public static final int SearchView_voiceIcon = 12;
        public static final int[] Spinner = {16842930, 16843126, 16843131, 16843362, R.attr.am};
        public static final int Spinner_android_dropDownWidth = 3;
        public static final int Spinner_android_entries = 0;
        public static final int Spinner_android_popupBackground = 1;
        public static final int Spinner_android_prompt = 2;
        public static final int Spinner_popupTheme = 4;
        public static final int[] SwitchCompat = {16843044, 16843045, 16843074, R.attr.ib, R.attr.ic, R.attr.id, R.attr.ie, R.attr.f1if, R.attr.ig, R.attr.ih, R.attr.ii, R.attr.ij, R.attr.ik, R.attr.il};
        public static final int SwitchCompat_android_textOff = 1;
        public static final int SwitchCompat_android_textOn = 0;
        public static final int SwitchCompat_android_thumb = 2;
        public static final int SwitchCompat_showText = 13;
        public static final int SwitchCompat_splitTrack = 12;
        public static final int SwitchCompat_switchMinWidth = 10;
        public static final int SwitchCompat_switchPadding = 11;
        public static final int SwitchCompat_switchTextAppearance = 9;
        public static final int SwitchCompat_thumbTextPadding = 8;
        public static final int SwitchCompat_thumbTint = 3;
        public static final int SwitchCompat_thumbTintMode = 4;
        public static final int SwitchCompat_track = 5;
        public static final int SwitchCompat_trackTint = 6;
        public static final int SwitchCompat_trackTintMode = 7;
        public static final int[] TextAppearance = {16842901, 16842902, 16842903, 16842904, 16842906, 16843105, 16843106, 16843107, 16843108, R.attr.b5};
        public static final int TextAppearance_android_shadowColor = 5;
        public static final int TextAppearance_android_shadowDx = 6;
        public static final int TextAppearance_android_shadowDy = 7;
        public static final int TextAppearance_android_shadowRadius = 8;
        public static final int TextAppearance_android_textColor = 3;
        public static final int TextAppearance_android_textColorHint = 4;
        public static final int TextAppearance_android_textSize = 0;
        public static final int TextAppearance_android_textStyle = 2;
        public static final int TextAppearance_android_typeface = 1;
        public static final int TextAppearance_textAllCaps = 9;
        public static final int[] Toolbar = {16842927, 16843072, R.attr.w, R.attr.a0, R.attr.a4, R.attr.af, R.attr.ag, R.attr.ah, R.attr.ai, R.attr.aj, R.attr.ak, R.attr.am, R.attr.jf, R.attr.jg, R.attr.jh, R.attr.ji, R.attr.jj, R.attr.jk, R.attr.jl, R.attr.jm, R.attr.jn, R.attr.jo, R.attr.f8253jp, R.attr.jq, R.attr.jr, R.attr.js, R.attr.jt, R.attr.ju, R.attr.jv};
        public static final int Toolbar_android_gravity = 0;
        public static final int Toolbar_android_minHeight = 1;
        public static final int Toolbar_buttonGravity = 21;
        public static final int Toolbar_collapseContentDescription = 23;
        public static final int Toolbar_collapseIcon = 22;
        public static final int Toolbar_contentInsetEnd = 6;
        public static final int Toolbar_contentInsetEndWithActions = 10;
        public static final int Toolbar_contentInsetLeft = 7;
        public static final int Toolbar_contentInsetRight = 8;
        public static final int Toolbar_contentInsetStart = 5;
        public static final int Toolbar_contentInsetStartWithNavigation = 9;
        public static final int Toolbar_logo = 4;
        public static final int Toolbar_logoDescription = 26;
        public static final int Toolbar_maxButtonHeight = 20;
        public static final int Toolbar_navigationContentDescription = 25;
        public static final int Toolbar_navigationIcon = 24;
        public static final int Toolbar_popupTheme = 11;
        public static final int Toolbar_subtitle = 3;
        public static final int Toolbar_subtitleTextAppearance = 13;
        public static final int Toolbar_subtitleTextColor = 28;
        public static final int Toolbar_title = 2;
        public static final int Toolbar_titleMargin = 14;
        public static final int Toolbar_titleMarginBottom = 18;
        public static final int Toolbar_titleMarginEnd = 16;
        public static final int Toolbar_titleMarginStart = 15;
        public static final int Toolbar_titleMarginTop = 17;
        public static final int Toolbar_titleMargins = 19;
        public static final int Toolbar_titleTextAppearance = 12;
        public static final int Toolbar_titleTextColor = 27;
        public static final int[] View = {16842752, 16842970, R.attr.jw, R.attr.jx, R.attr.jy};
        public static final int[] ViewBackgroundHelper = {16842964, R.attr.jz, R.attr.k0};
        public static final int ViewBackgroundHelper_android_background = 0;
        public static final int ViewBackgroundHelper_backgroundTint = 1;
        public static final int ViewBackgroundHelper_backgroundTintMode = 2;
        public static final int[] ViewStubCompat = {16842960, 16842994, 16842995};
        public static final int ViewStubCompat_android_id = 0;
        public static final int ViewStubCompat_android_inflatedId = 2;
        public static final int ViewStubCompat_android_layout = 1;
        public static final int View_android_focusable = 1;
        public static final int View_android_theme = 0;
        public static final int View_paddingEnd = 3;
        public static final int View_paddingStart = 2;
        public static final int View_theme = 4;
    }
}
