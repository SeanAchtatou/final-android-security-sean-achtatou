package com.stripe.android;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stripe.android.a.j;
import com.stripe.android.b.d;
import com.stripe.android.d.e;
import com.stripe.android.exception.StripeException;
import java.util.Map;
import java.util.concurrent.Executor;

public class Stripe {

    /* renamed from: a  reason: collision with root package name */
    b f6745a = new b() {
    };

    /* renamed from: b  reason: collision with root package name */
    c f6746b = new c() {
        public void a(Map<String, Object> map, String str, String str2, String str3, Executor executor, b bVar) {
            final String str4 = str;
            final String str5 = str2;
            final Map<String, Object> map2 = map;
            final String str6 = str3;
            final b bVar2 = bVar;
            Stripe.this.a(executor, new AsyncTask<Void, Void, a>() {
                /* access modifiers changed from: protected */
                /* renamed from: a */
                public a doInBackground(Void... voidArr) {
                    try {
                        return new a(d.a(Stripe.this.f6747c, map2, com.stripe.android.b.c.a(str4, str5, FirebaseAnalytics.Param.SOURCE).a(), str6, Stripe.this.f6748d));
                    } catch (StripeException e2) {
                        return new a(e2);
                    }
                }

                /* access modifiers changed from: protected */
                /* renamed from: a */
                public void onPostExecute(a aVar) {
                    Stripe.this.a(aVar, bVar2);
                }
            });
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Context f6747c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public d.a f6748d;

    /* renamed from: e  reason: collision with root package name */
    private String f6749e;

    /* renamed from: f  reason: collision with root package name */
    private String f6750f;

    interface b {
    }

    interface c {
        void a(Map<String, Object> map, String str, String str2, String str3, Executor executor, b bVar);
    }

    public Stripe(Context context) {
        this.f6747c = context;
    }

    public Stripe(Context context, String str) {
        this.f6747c = context;
        a(str);
    }

    public void a(com.stripe.android.a.b bVar, b bVar2) {
        a(bVar, this.f6749e, bVar2);
    }

    public void a(com.stripe.android.a.b bVar, String str, b bVar2) {
        a(bVar, str, null, bVar2);
    }

    public void a(com.stripe.android.a.b bVar, String str, Executor executor, b bVar2) {
        if (bVar == null) {
            throw new RuntimeException("Required Parameter: 'card' is required to create a token");
        }
        a(e.a(this.f6747c, bVar), str, "card", executor, bVar2);
    }

    public void a(String str) {
        b(str);
        this.f6749e = str;
    }

    private void a(Map<String, Object> map, String str, String str2, Executor executor, b bVar) {
        if (bVar == null) {
            throw new RuntimeException("Required Parameter: 'callback' is required to use the created token and handle errors");
        }
        b(str);
        this.f6746b.a(map, str, this.f6750f, str2, executor, bVar);
    }

    private void b(String str) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Invalid Publishable Key: You must use a valid publishable key to create a token.  For more info, see https://stripe.com/docs/stripe.js.");
        } else if (str.startsWith("sk_")) {
            throw new IllegalArgumentException("Invalid Publishable Key: You are using a secret key to create a token, instead of the publishable one. For more info, see https://stripe.com/docs/stripe.js");
        }
    }

    /* access modifiers changed from: private */
    public void a(a aVar, b bVar) {
        if (aVar.f6760b != null) {
            bVar.a(aVar.f6760b);
        } else if (aVar.f6761c != null) {
            bVar.a(aVar.f6761c);
        } else {
            bVar.a(new RuntimeException("Somehow got neither a token response or an error response"));
        }
    }

    /* access modifiers changed from: private */
    public void a(Executor executor, AsyncTask<Void, Void, a> asyncTask) {
        if (executor == null || Build.VERSION.SDK_INT <= 11) {
            asyncTask.execute(new Void[0]);
        } else {
            asyncTask.executeOnExecutor(executor, new Void[0]);
        }
    }

    private class a {

        /* renamed from: a  reason: collision with root package name */
        final com.stripe.android.a.c f6759a;

        /* renamed from: b  reason: collision with root package name */
        final j f6760b;

        /* renamed from: c  reason: collision with root package name */
        final Exception f6761c;

        private a(j jVar) {
            this.f6760b = jVar;
            this.f6759a = null;
            this.f6761c = null;
        }

        private a(Exception exc) {
            this.f6761c = exc;
            this.f6759a = null;
            this.f6760b = null;
        }
    }
}
