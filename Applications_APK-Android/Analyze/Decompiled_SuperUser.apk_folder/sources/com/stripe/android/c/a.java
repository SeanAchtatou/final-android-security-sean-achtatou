package com.stripe.android.c;

import java.util.Calendar;

/* compiled from: Clock */
public class a {

    /* renamed from: b  reason: collision with root package name */
    private static a f6858b;

    /* renamed from: a  reason: collision with root package name */
    protected Calendar f6859a;

    protected static a a() {
        if (f6858b == null) {
            f6858b = new a();
        }
        return f6858b;
    }

    private Calendar c() {
        return this.f6859a != null ? (Calendar) this.f6859a.clone() : Calendar.getInstance();
    }

    public static Calendar b() {
        return a().c();
    }
}
