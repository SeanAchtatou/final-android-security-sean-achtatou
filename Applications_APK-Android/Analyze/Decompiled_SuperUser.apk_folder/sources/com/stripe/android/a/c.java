package com.stripe.android.a;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.stripe.android.d.d;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Source */
public class c extends i {

    /* renamed from: a  reason: collision with root package name */
    public static final Set<String> f6793a = new HashSet();

    /* renamed from: b  reason: collision with root package name */
    private String f6794b;

    /* renamed from: c  reason: collision with root package name */
    private Long f6795c;

    /* renamed from: d  reason: collision with root package name */
    private String f6796d;

    /* renamed from: e  reason: collision with root package name */
    private e f6797e;

    /* renamed from: f  reason: collision with root package name */
    private Long f6798f;

    /* renamed from: g  reason: collision with root package name */
    private String f6799g;

    /* renamed from: h  reason: collision with root package name */
    private String f6800h;
    private String i;
    private Boolean j;
    private Map<String, String> k;
    private f l;
    private g m;
    private h n;
    private String o;
    private Map<String, Object> p;
    private String q;

    static {
        f6793a.add("card");
        f6793a.add("sepa_debit");
    }

    public JSONObject v() {
        JSONObject jSONObject = new JSONObject();
        try {
            d.a(jSONObject, "id", this.f6794b);
            jSONObject.put("object", FirebaseAnalytics.Param.SOURCE);
            jSONObject.put("amount", this.f6795c);
            d.a(jSONObject, "client_secret", this.f6796d);
            a(jSONObject, "code_verification", this.f6797e);
            jSONObject.put("created", this.f6798f);
            d.a(jSONObject, FirebaseAnalytics.Param.CURRENCY, this.f6799g);
            d.a(jSONObject, "flow", this.i);
            jSONObject.put("livemode", this.j);
            JSONObject a2 = d.a(this.k);
            if (a2 != null) {
                jSONObject.put("metadata", a2);
            }
            JSONObject a3 = d.a(this.p);
            if (a3 != null) {
                jSONObject.put(this.f6800h, a3);
            }
            a(jSONObject, "owner", this.l);
            a(jSONObject, "receiver", this.m);
            a(jSONObject, "redirect", this.n);
            d.a(jSONObject, "status", this.o);
            d.a(jSONObject, "type", this.f6800h);
            d.a(jSONObject, "usage", this.q);
        } catch (JSONException e2) {
        }
        return jSONObject;
    }
}
