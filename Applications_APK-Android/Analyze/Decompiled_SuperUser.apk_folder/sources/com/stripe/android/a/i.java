package com.stripe.android.a;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StripeJsonModel */
public abstract class i {
    public abstract JSONObject v();

    public String toString() {
        return v().toString();
    }

    static void a(JSONObject jSONObject, String str, i iVar) {
        if (iVar != null) {
            try {
                jSONObject.put(str, iVar.v());
            } catch (JSONException e2) {
            }
        }
    }
}
