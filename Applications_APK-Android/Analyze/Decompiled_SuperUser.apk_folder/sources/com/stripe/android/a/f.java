package com.stripe.android.a;

import com.google.android.gms.common.Scopes;
import com.stripe.android.d.d;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SourceOwner */
public class f extends i {

    /* renamed from: a  reason: collision with root package name */
    private d f6809a;

    /* renamed from: b  reason: collision with root package name */
    private String f6810b;

    /* renamed from: c  reason: collision with root package name */
    private String f6811c;

    /* renamed from: d  reason: collision with root package name */
    private String f6812d;

    /* renamed from: e  reason: collision with root package name */
    private d f6813e;

    /* renamed from: f  reason: collision with root package name */
    private String f6814f;

    /* renamed from: g  reason: collision with root package name */
    private String f6815g;

    /* renamed from: h  reason: collision with root package name */
    private String f6816h;

    public JSONObject v() {
        JSONObject jSONObject = null;
        JSONObject jSONObject2 = new JSONObject();
        JSONObject v = this.f6809a == null ? null : this.f6809a.v();
        if (this.f6813e != null) {
            jSONObject = this.f6813e.v();
        }
        if (v != null) {
            try {
                if (v.length() > 0) {
                    jSONObject2.put("address", v);
                }
            } catch (JSONException e2) {
            }
        }
        d.a(jSONObject2, Scopes.EMAIL, this.f6810b);
        d.a(jSONObject2, "name", this.f6811c);
        d.a(jSONObject2, "phone", this.f6812d);
        if (jSONObject != null && jSONObject.length() > 0) {
            jSONObject2.put("verified_address", jSONObject);
        }
        d.a(jSONObject2, "verified_email", this.f6814f);
        d.a(jSONObject2, "verified_name", this.f6815g);
        d.a(jSONObject2, "verified_phone", this.f6816h);
        return jSONObject2;
    }
}
