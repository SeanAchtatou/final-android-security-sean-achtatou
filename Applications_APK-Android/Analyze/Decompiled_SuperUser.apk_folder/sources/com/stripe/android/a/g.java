package com.stripe.android.a;

import com.stripe.android.d.d;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SourceReceiver */
public class g extends i {

    /* renamed from: a  reason: collision with root package name */
    private String f6817a;

    /* renamed from: b  reason: collision with root package name */
    private long f6818b;

    /* renamed from: c  reason: collision with root package name */
    private long f6819c;

    /* renamed from: d  reason: collision with root package name */
    private long f6820d;

    public JSONObject v() {
        JSONObject jSONObject = new JSONObject();
        d.a(jSONObject, "address", this.f6817a);
        try {
            jSONObject.put("amount_charged", this.f6818b);
            jSONObject.put("amount_received", this.f6819c);
            jSONObject.put("amount_returned", this.f6820d);
        } catch (JSONException e2) {
        }
        return jSONObject;
    }
}
