package com.stripe.android.a;

import java.util.Date;

/* compiled from: Token */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private final String f6824a;

    /* renamed from: b  reason: collision with root package name */
    private final String f6825b;

    /* renamed from: c  reason: collision with root package name */
    private final Date f6826c;

    /* renamed from: d  reason: collision with root package name */
    private final boolean f6827d;

    /* renamed from: e  reason: collision with root package name */
    private final boolean f6828e;

    /* renamed from: f  reason: collision with root package name */
    private final a f6829f;

    /* renamed from: g  reason: collision with root package name */
    private final b f6830g;

    public j(String str, boolean z, Date date, Boolean bool, b bVar) {
        this.f6824a = str;
        this.f6825b = "card";
        this.f6826c = date;
        this.f6827d = z;
        this.f6830g = bVar;
        this.f6828e = bool.booleanValue();
        this.f6829f = null;
    }

    public j(String str, boolean z, Date date, Boolean bool, a aVar) {
        this.f6824a = str;
        this.f6825b = "bank_account";
        this.f6826c = date;
        this.f6827d = z;
        this.f6830g = null;
        this.f6828e = bool.booleanValue();
        this.f6829f = aVar;
    }

    public j(String str, boolean z, Date date, Boolean bool) {
        this.f6824a = str;
        this.f6825b = "pii";
        this.f6826c = date;
        this.f6830g = null;
        this.f6829f = null;
        this.f6828e = bool.booleanValue();
        this.f6827d = z;
    }

    public String a() {
        return this.f6824a;
    }
}
