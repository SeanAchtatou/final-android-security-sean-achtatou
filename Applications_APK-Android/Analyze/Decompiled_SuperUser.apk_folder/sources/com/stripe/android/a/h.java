package com.stripe.android.a;

import com.stripe.android.d.d;
import org.json.JSONObject;

/* compiled from: SourceRedirect */
public class h extends i {

    /* renamed from: a  reason: collision with root package name */
    private String f6821a;

    /* renamed from: b  reason: collision with root package name */
    private String f6822b;

    /* renamed from: c  reason: collision with root package name */
    private String f6823c;

    public JSONObject v() {
        JSONObject jSONObject = new JSONObject();
        d.a(jSONObject, "return_url", this.f6821a);
        d.a(jSONObject, "status", this.f6822b);
        d.a(jSONObject, "url", this.f6823c);
        return jSONObject;
    }
}
