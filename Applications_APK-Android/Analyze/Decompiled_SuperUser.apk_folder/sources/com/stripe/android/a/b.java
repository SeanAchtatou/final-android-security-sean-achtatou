package com.stripe.android.a;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.stripe.android.d.d;
import com.stripe.android.d.f;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

/* compiled from: Card */
public class b extends i {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f6771a = {"34", "37"};

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f6772b = {"60", "62", "64", "65"};

    /* renamed from: c  reason: collision with root package name */
    public static final String[] f6773c = {"35"};

    /* renamed from: d  reason: collision with root package name */
    public static final String[] f6774d = {"300", "301", "302", "303", "304", "305", "309", "36", "38", "39"};

    /* renamed from: e  reason: collision with root package name */
    public static final String[] f6775e = {"4"};

    /* renamed from: f  reason: collision with root package name */
    public static final String[] f6776f = {"2221", "2222", "2223", "2224", "2225", "2226", "2227", "2228", "2229", "223", "224", "225", "226", "227", "228", "229", "23", "24", "25", "26", "270", "271", "2720", "50", "51", "52", "53", "54", "55"};
    private String A;
    private String B;
    private List<String> C;

    /* renamed from: g  reason: collision with root package name */
    private String f6777g;

    /* renamed from: h  reason: collision with root package name */
    private String f6778h;
    private Integer i;
    private Integer j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private String y;
    private String z;

    /* compiled from: Card */
    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final String f6779a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final String f6780b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public final Integer f6781c;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public final Integer f6782d;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public String f6783e;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public String f6784f;
        /* access modifiers changed from: private */

        /* renamed from: g  reason: collision with root package name */
        public String f6785g;
        /* access modifiers changed from: private */

        /* renamed from: h  reason: collision with root package name */
        public String f6786h;
        /* access modifiers changed from: private */
        public String i;
        /* access modifiers changed from: private */
        public String j;
        /* access modifiers changed from: private */
        public String k;
        /* access modifiers changed from: private */
        public String l;
        /* access modifiers changed from: private */
        public String m;
        /* access modifiers changed from: private */
        public String n;
        /* access modifiers changed from: private */
        public String o;
        /* access modifiers changed from: private */
        public String p;
        /* access modifiers changed from: private */
        public String q;
        /* access modifiers changed from: private */
        public String r;
        /* access modifiers changed from: private */
        public String s;
        /* access modifiers changed from: private */
        public String t;
        /* access modifiers changed from: private */
        public String u;
        /* access modifiers changed from: private */
        public String v;

        public a(String str, Integer num, Integer num2, String str2) {
            this.f6779a = str;
            this.f6781c = num;
            this.f6782d = num2;
            this.f6780b = str2;
        }

        public a a(String str) {
            this.f6783e = str;
            return this;
        }

        public a b(String str) {
            this.f6784f = str;
            return this;
        }

        public a c(String str) {
            this.f6785g = str;
            return this;
        }

        public a d(String str) {
            this.f6786h = str;
            return this;
        }

        public a e(String str) {
            this.i = str;
            return this;
        }

        public a f(String str) {
            this.j = str;
            return this;
        }

        public a g(String str) {
            this.k = str;
            return this;
        }

        public a h(String str) {
            this.l = str;
            return this;
        }

        public a i(String str) {
            this.m = str;
            return this;
        }

        public a j(String str) {
            this.n = str;
            return this;
        }

        public a k(String str) {
            this.q = str;
            return this;
        }

        public a l(String str) {
            this.o = str;
            return this;
        }

        public a m(String str) {
            this.r = str;
            return this;
        }

        public a n(String str) {
            this.s = str;
            return this;
        }

        public a o(String str) {
            this.t = str;
            return this;
        }

        public a p(String str) {
            this.u = str;
            return this;
        }

        public a q(String str) {
            this.p = str;
            return this;
        }

        public a r(String str) {
            this.v = str;
            return this;
        }

        public b a() {
            return new b(this);
        }
    }

    public static b a(JSONObject jSONObject) {
        if (jSONObject == null || !"card".equals(jSONObject.optString("object"))) {
            return null;
        }
        Integer b2 = d.b(jSONObject, "exp_month");
        Integer b3 = d.b(jSONObject, "exp_year");
        if (b2 != null && (b2.intValue() < 1 || b2.intValue() > 12)) {
            b2 = null;
        }
        if (b3 != null && b3.intValue() < 0) {
            b3 = null;
        }
        a aVar = new a(null, b2, b3, null);
        aVar.e(d.c(jSONObject, "address_city"));
        aVar.b(d.c(jSONObject, "address_line1"));
        aVar.c(d.c(jSONObject, "address_line1_check"));
        aVar.d(d.c(jSONObject, "address_line2"));
        aVar.i(d.c(jSONObject, "address_country"));
        aVar.f(d.c(jSONObject, "address_state"));
        aVar.g(d.c(jSONObject, "address_zip"));
        aVar.h(d.c(jSONObject, "address_zip_check"));
        aVar.j(f.f(d.c(jSONObject, "brand")));
        aVar.m(d.d(jSONObject, "country"));
        aVar.o(d.c(jSONObject, "customer"));
        aVar.n(d.e(jSONObject, FirebaseAnalytics.Param.CURRENCY));
        aVar.p(d.c(jSONObject, "cvc_check"));
        aVar.l(f.g(d.c(jSONObject, "funding")));
        aVar.k(d.c(jSONObject, "fingerprint"));
        aVar.r(d.c(jSONObject, "id"));
        aVar.q(d.c(jSONObject, "last4"));
        aVar.a(d.c(jSONObject, "name"));
        return aVar.a();
    }

    public b(String str, Integer num, Integer num2, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16) {
        this.C = new ArrayList();
        this.f6777g = f.b(b(str));
        this.i = num;
        this.j = num2;
        this.f6778h = f.b(str2);
        this.k = f.b(str3);
        this.l = f.b(str4);
        this.n = f.b(str5);
        this.o = f.b(str6);
        this.p = f.b(str7);
        this.q = f.b(str8);
        this.s = f.b(str9);
        this.u = f.f(str10) == null ? u() : str10;
        this.t = f.b(str11) == null ? t() : str11;
        this.w = f.b(str12);
        this.v = f.g(str13);
        this.x = f.b(str14);
        this.y = f.b(str15);
        this.B = f.b(str16);
    }

    public b(String str, Integer num, Integer num2, String str2) {
        this(str, num, num2, str2, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    public boolean a() {
        if (this.f6778h == null) {
            if (!b() || !c()) {
                return false;
            }
            return true;
        } else if (!b() || !c() || !d()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean b() {
        return com.stripe.android.d.a.a(this.f6777g);
    }

    public boolean c() {
        if (e() && f() && !com.stripe.android.d.b.a(this.j.intValue(), this.i.intValue())) {
            return true;
        }
        return false;
    }

    public boolean d() {
        boolean z2;
        boolean z3 = true;
        if (f.c(this.f6778h)) {
            return false;
        }
        String trim = this.f6778h.trim();
        String u2 = u();
        if ((u2 != null || trim.length() < 3 || trim.length() > 4) && ((!"American Express".equals(u2) || trim.length() != 4) && trim.length() != 3)) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (!f.a(trim) || !z2) {
            z3 = false;
        }
        return z3;
    }

    public boolean e() {
        return this.i != null && this.i.intValue() >= 1 && this.i.intValue() <= 12;
    }

    public boolean f() {
        return this.j != null && !com.stripe.android.d.b.a(this.j.intValue());
    }

    public String g() {
        return this.f6777g;
    }

    public List<String> h() {
        return this.C;
    }

    public b a(String str) {
        this.C.add(str);
        return this;
    }

    public String i() {
        return this.f6778h;
    }

    public Integer j() {
        return this.i;
    }

    public Integer k() {
        return this.j;
    }

    public String l() {
        return this.k;
    }

    public String m() {
        return this.l;
    }

    public String n() {
        return this.n;
    }

    public String o() {
        return this.o;
    }

    public String p() {
        return this.q;
    }

    public String q() {
        return this.p;
    }

    public String r() {
        return this.s;
    }

    public String s() {
        return this.y;
    }

    public String t() {
        if (!f.c(this.t)) {
            return this.t;
        }
        if (this.f6777g == null || this.f6777g.length() <= 4) {
            return null;
        }
        this.t = this.f6777g.substring(this.f6777g.length() - 4, this.f6777g.length());
        return this.t;
    }

    public String u() {
        String str;
        if (f.c(this.u) && !f.c(this.f6777g)) {
            if (f.a(this.f6777g, f6771a)) {
                str = "American Express";
            } else if (f.a(this.f6777g, f6772b)) {
                str = "Discover";
            } else if (f.a(this.f6777g, f6773c)) {
                str = "JCB";
            } else if (f.a(this.f6777g, f6774d)) {
                str = "Diners Club";
            } else if (f.a(this.f6777g, f6775e)) {
                str = "Visa";
            } else if (f.a(this.f6777g, f6776f)) {
                str = "MasterCard";
            } else {
                str = "Unknown";
            }
            this.u = str;
        }
        return this.u;
    }

    public JSONObject v() {
        JSONObject jSONObject = new JSONObject();
        d.a(jSONObject, "name", this.k);
        d.a(jSONObject, "address_city", this.o);
        d.a(jSONObject, "address_country", this.s);
        d.a(jSONObject, "address_line1", this.l);
        d.a(jSONObject, "address_line1_check", this.m);
        d.a(jSONObject, "address_line2", this.n);
        d.a(jSONObject, "address_state", this.p);
        d.a(jSONObject, "address_zip", this.q);
        d.a(jSONObject, "address_zip_check", this.r);
        d.a(jSONObject, "brand", this.u);
        d.a(jSONObject, FirebaseAnalytics.Param.CURRENCY, this.y);
        d.a(jSONObject, "country", this.x);
        d.a(jSONObject, "customer", this.z);
        d.a(jSONObject, "exp_month", this.i);
        d.a(jSONObject, "exp_year", this.j);
        d.a(jSONObject, "fingerprint", this.w);
        d.a(jSONObject, "funding", this.v);
        d.a(jSONObject, "cvc_check", this.A);
        d.a(jSONObject, "last4", this.t);
        d.a(jSONObject, "id", this.B);
        d.a(jSONObject, "object", "card");
        return jSONObject;
    }

    private b(a aVar) {
        String n2;
        String o2;
        this.C = new ArrayList();
        this.f6777g = f.b(b(aVar.f6779a));
        this.i = aVar.f6781c;
        this.j = aVar.f6782d;
        this.f6778h = f.b(aVar.f6780b);
        this.k = f.b(aVar.f6783e);
        this.l = f.b(aVar.f6784f);
        this.m = f.b(aVar.f6785g);
        this.n = f.b(aVar.f6786h);
        this.o = f.b(aVar.i);
        this.p = f.b(aVar.j);
        this.q = f.b(aVar.k);
        this.r = f.b(aVar.l);
        this.s = f.b(aVar.m);
        if (f.b(aVar.p) == null) {
            n2 = t();
        } else {
            n2 = aVar.p;
        }
        this.t = n2;
        if (f.f(aVar.n) == null) {
            o2 = u();
        } else {
            o2 = aVar.n;
        }
        this.u = o2;
        this.w = f.b(aVar.q);
        this.v = f.g(aVar.o);
        this.x = f.b(aVar.r);
        this.y = f.b(aVar.s);
        this.z = f.b(aVar.t);
        this.A = f.b(aVar.u);
        this.B = f.b(aVar.v);
    }

    private String b(String str) {
        if (str == null) {
            return null;
        }
        return str.trim().replaceAll("\\s+|-", "");
    }
}
