package com.stripe.android.a;

import org.json.JSONObject;

/* compiled from: SourceAddress */
public class d extends i {

    /* renamed from: a  reason: collision with root package name */
    private String f6801a;

    /* renamed from: b  reason: collision with root package name */
    private String f6802b;

    /* renamed from: c  reason: collision with root package name */
    private String f6803c;

    /* renamed from: d  reason: collision with root package name */
    private String f6804d;

    /* renamed from: e  reason: collision with root package name */
    private String f6805e;

    /* renamed from: f  reason: collision with root package name */
    private String f6806f;

    public JSONObject v() {
        JSONObject jSONObject = new JSONObject();
        com.stripe.android.d.d.a(jSONObject, "city", this.f6801a);
        com.stripe.android.d.d.a(jSONObject, "country", this.f6802b);
        com.stripe.android.d.d.a(jSONObject, "line1", this.f6803c);
        com.stripe.android.d.d.a(jSONObject, "line2", this.f6804d);
        com.stripe.android.d.d.a(jSONObject, "postal_code", this.f6805e);
        com.stripe.android.d.d.a(jSONObject, "state", this.f6806f);
        return jSONObject;
    }
}
