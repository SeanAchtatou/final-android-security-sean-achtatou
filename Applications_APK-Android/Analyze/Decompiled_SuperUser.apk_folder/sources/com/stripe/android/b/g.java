package com.stripe.android.b;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import com.stripe.android.d.f;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/* compiled from: TelemetryClientUtil */
class g {
    static Map<String, Object> a(Context context) {
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        HashMap hashMap3 = new HashMap();
        hashMap.put("v2", 1);
        hashMap.put("tag", "4.1.5");
        hashMap.put("src", "android-sdk");
        hashMap2.put("c", a(Locale.getDefault().toString()));
        hashMap2.put("d", a(b()));
        hashMap2.put("f", a(c(context)));
        hashMap2.put("g", a(a()));
        hashMap.put("a", hashMap2);
        hashMap3.put("d", d(context));
        String e2 = e(context);
        hashMap3.put("k", e2);
        hashMap3.put("o", Build.VERSION.RELEASE);
        hashMap3.put("p", Integer.valueOf(Build.VERSION.SDK_INT));
        hashMap3.put("q", Build.MANUFACTURER);
        hashMap3.put("r", Build.BRAND);
        hashMap3.put("s", Build.MODEL);
        hashMap3.put("t", Build.TAGS);
        if (context.getPackageName() != null) {
            try {
                hashMap3.put("l", context.getPackageManager().getPackageInfo(e2, 0).versionName);
            } catch (PackageManager.NameNotFoundException e3) {
            }
        }
        hashMap.put("b", hashMap3);
        return hashMap;
    }

    private static Map<String, Object> a(Object obj) {
        HashMap hashMap = new HashMap();
        hashMap.put("v", obj);
        return hashMap;
    }

    private static String a() {
        int convert = (int) TimeUnit.MINUTES.convert((long) TimeZone.getDefault().getRawOffset(), TimeUnit.MILLISECONDS);
        if (convert % 60 == 0) {
            return String.valueOf(convert / 60);
        }
        return new BigDecimal(convert).setScale(2, 6).divide(new BigDecimal(60), new MathContext(2)).setScale(2, 6).toString();
    }

    private static String c(Context context) {
        if (context.getResources() == null) {
            return "";
        }
        int i = context.getResources().getDisplayMetrics().widthPixels;
        int i2 = context.getResources().getDisplayMetrics().heightPixels;
        int i3 = context.getResources().getDisplayMetrics().densityDpi;
        return String.format(Locale.ENGLISH, "%dw_%dh_%ddpi", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3));
    }

    private static String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("Android").append(" ").append(Build.VERSION.RELEASE).append(" ").append(Build.VERSION.CODENAME).append(" ").append(Build.VERSION.SDK_INT);
        return sb.toString();
    }

    static String b(Context context) {
        String i;
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        return (!f.c(string) && (i = f.i(string)) != null) ? i : "";
    }

    private static String d(Context context) {
        String i = f.i(e(context) + b(context));
        return i == null ? "" : i;
    }

    private static String e(Context context) {
        if (context.getApplicationContext() == null || context.getApplicationContext().getPackageName() == null) {
            return "";
        }
        return context.getApplicationContext().getPackageName();
    }
}
