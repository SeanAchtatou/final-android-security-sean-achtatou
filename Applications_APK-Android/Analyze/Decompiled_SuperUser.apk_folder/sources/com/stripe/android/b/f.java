package com.stripe.android.b;

import java.net.InetAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashSet;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: StripeSSLSocketFactory */
public class f extends SSLSocketFactory {

    /* renamed from: a  reason: collision with root package name */
    private final SSLSocketFactory f6855a = HttpsURLConnection.getDefaultSSLSocketFactory();

    /* renamed from: b  reason: collision with root package name */
    private final boolean f6856b;

    /* renamed from: c  reason: collision with root package name */
    private final boolean f6857c;

    public f() {
        String[] strArr;
        boolean z = false;
        try {
            strArr = SSLContext.getDefault().getSupportedSSLParameters().getProtocols();
        } catch (NoSuchAlgorithmException e2) {
            strArr = new String[0];
        }
        boolean z2 = false;
        for (String str : strArr) {
            if (str.equals("TLSv1.1")) {
                z2 = true;
            } else if (str.equals("TLSv1.2")) {
                z = true;
            }
        }
        this.f6856b = z2;
        this.f6857c = z;
    }

    public String[] getDefaultCipherSuites() {
        return this.f6855a.getDefaultCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        return this.f6855a.getSupportedCipherSuites();
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) {
        return a(this.f6855a.createSocket(socket, str, i, z));
    }

    public Socket createSocket(String str, int i) {
        return a(this.f6855a.createSocket(str, i));
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        return a(this.f6855a.createSocket(str, i, inetAddress, i2));
    }

    public Socket createSocket(InetAddress inetAddress, int i) {
        return a(this.f6855a.createSocket(inetAddress, i));
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        return a(this.f6855a.createSocket(inetAddress, i, inetAddress2, i2));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private Socket a(Socket socket) {
        boolean z = socket instanceof SSLSocket;
        SSLSocket sSLSocket = socket;
        if (z) {
            SSLSocket sSLSocket2 = (SSLSocket) socket;
            HashSet hashSet = new HashSet(Arrays.asList(sSLSocket2.getEnabledProtocols()));
            if (this.f6856b) {
                hashSet.add("TLSv1.1");
            }
            if (this.f6857c) {
                hashSet.add("TLSv1.2");
            }
            sSLSocket2.setEnabledProtocols((String[]) hashSet.toArray(new String[0]));
            sSLSocket = sSLSocket2;
        }
        return sSLSocket;
    }
}
