package com.stripe.android.b;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.stripe.android.d.d;
import com.stripe.android.d.f;
import org.json.JSONObject;

/* compiled from: BankAccountParser */
class a {
    static com.stripe.android.a.a a(JSONObject jSONObject) {
        return new com.stripe.android.a.a(d.c(jSONObject, "account_holder_name"), f.d(d.c(jSONObject, "account_holder_type")), d.c(jSONObject, "bank_name"), d.d(jSONObject, "country"), d.e(jSONObject, FirebaseAnalytics.Param.CURRENCY), d.c(jSONObject, "fingerprint"), d.c(jSONObject, "last4"), d.c(jSONObject, "routing_number"));
    }
}
