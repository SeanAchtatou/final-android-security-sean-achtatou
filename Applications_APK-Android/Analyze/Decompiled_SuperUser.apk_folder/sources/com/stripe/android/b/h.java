package com.stripe.android.b;

import com.stripe.android.a.b;
import com.stripe.android.a.j;
import com.stripe.android.d.d;
import com.stripe.android.d.f;
import java.util.Date;
import org.json.JSONObject;

/* compiled from: TokenParser */
public class h {
    public static j a(String str) {
        JSONObject jSONObject = new JSONObject(str);
        String a2 = d.a(jSONObject, "id");
        Long valueOf = Long.valueOf(jSONObject.getLong("created"));
        Boolean valueOf2 = Boolean.valueOf(jSONObject.getBoolean("livemode"));
        String h2 = f.h(d.a(jSONObject, "type"));
        Boolean valueOf3 = Boolean.valueOf(jSONObject.getBoolean("used"));
        Date date = new Date(valueOf.longValue() * 1000);
        if ("bank_account".equals(h2)) {
            return new j(a2, valueOf2.booleanValue(), date, valueOf3, a.a(jSONObject.getJSONObject("bank_account")));
        } else if ("card".equals(h2)) {
            return new j(a2, valueOf2.booleanValue(), date, valueOf3, b.a(jSONObject.getJSONObject("card")));
        } else if ("pii".equals(h2)) {
            return new j(a2, valueOf2.booleanValue(), date, valueOf3);
        } else {
            return null;
        }
    }
}
