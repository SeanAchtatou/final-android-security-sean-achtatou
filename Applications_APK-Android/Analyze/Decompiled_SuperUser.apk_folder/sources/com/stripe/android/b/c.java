package com.stripe.android.b;

/* compiled from: RequestOptions */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private final String f6837a;

    /* renamed from: b  reason: collision with root package name */
    private final String f6838b;

    /* renamed from: c  reason: collision with root package name */
    private final String f6839c;

    /* renamed from: d  reason: collision with root package name */
    private final String f6840d;

    /* renamed from: e  reason: collision with root package name */
    private final String f6841e;

    /* renamed from: f  reason: collision with root package name */
    private final String f6842f;

    private c(String str, String str2, String str3, String str4, String str5, String str6) {
        this.f6837a = str;
        this.f6838b = str2;
        this.f6839c = str3;
        this.f6840d = str4;
        this.f6841e = str5;
        this.f6842f = str6;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f6837a;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.f6838b;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.f6839c;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.f6840d;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.f6841e;
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.f6842f;
    }

    public static a a(String str, String str2, String str3) {
        return new a(str, str3).b(str2);
    }

    public static a a(String str, String str2) {
        return new a(str, str2);
    }

    /* compiled from: RequestOptions */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        private String f6843a;

        /* renamed from: b  reason: collision with root package name */
        private String f6844b;

        /* renamed from: c  reason: collision with root package name */
        private String f6845c;

        /* renamed from: d  reason: collision with root package name */
        private String f6846d;

        /* renamed from: e  reason: collision with root package name */
        private String f6847e;

        /* renamed from: f  reason: collision with root package name */
        private String f6848f;

        a(String str, String str2) {
            this.f6846d = str;
            this.f6847e = str2;
        }

        /* access modifiers changed from: package-private */
        public a a(String str) {
            this.f6844b = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a b(String str) {
            this.f6848f = str;
            return this;
        }

        public c a() {
            return new c(this.f6843a, this.f6844b, this.f6845c, this.f6846d, this.f6847e, this.f6848f);
        }
    }
}
