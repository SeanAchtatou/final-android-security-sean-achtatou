package com.stripe.android.b;

import com.stripe.android.d.d;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ErrorParser */
class b {
    static a a(String str) {
        a aVar = new a();
        try {
            JSONObject jSONObject = new JSONObject(str).getJSONObject("error");
            aVar.f6836f = d.c(jSONObject, "charge");
            aVar.f6833c = d.c(jSONObject, "code");
            aVar.f6835e = d.c(jSONObject, "decline_code");
            aVar.f6832b = d.c(jSONObject, "message");
            aVar.f6834d = d.c(jSONObject, "param");
            aVar.f6831a = d.c(jSONObject, "type");
        } catch (JSONException e2) {
            aVar.f6832b = "An improperly formatted error response was found.";
        }
        return aVar;
    }

    /* compiled from: ErrorParser */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        public String f6831a;

        /* renamed from: b  reason: collision with root package name */
        public String f6832b;

        /* renamed from: c  reason: collision with root package name */
        public String f6833c;

        /* renamed from: d  reason: collision with root package name */
        public String f6834d;

        /* renamed from: e  reason: collision with root package name */
        public String f6835e;

        /* renamed from: f  reason: collision with root package name */
        public String f6836f;

        a() {
        }
    }
}
