package com.stripe.android.b;

import java.util.List;
import java.util.Map;

/* compiled from: StripeResponse */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private String f6852a;

    /* renamed from: b  reason: collision with root package name */
    private int f6853b;

    /* renamed from: c  reason: collision with root package name */
    private Map<String, List<String>> f6854c;

    public e(int i, String str, Map<String, List<String>> map) {
        this.f6853b = i;
        this.f6852a = str;
        this.f6854c = map;
    }

    public int a() {
        return this.f6853b;
    }

    public String b() {
        return this.f6852a;
    }

    public Map<String, List<String>> c() {
        return this.f6854c;
    }
}
