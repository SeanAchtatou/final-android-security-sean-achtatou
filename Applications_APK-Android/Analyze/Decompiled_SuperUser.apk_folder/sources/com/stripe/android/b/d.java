package com.stripe.android.b;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.stripe.android.a.j;
import com.stripe.android.b.b;
import com.stripe.android.d.c;
import com.stripe.android.d.e;
import com.stripe.android.d.f;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.exception.CardException;
import com.stripe.android.exception.InvalidRequestException;
import com.stripe.android.exception.PermissionException;
import com.stripe.android.exception.RateLimitException;
import com.stripe.android.exception.StripeException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.Security;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StripeApiHandler */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final SSLSocketFactory f6849a = new f();

    /* compiled from: StripeApiHandler */
    public interface a {
        void a(e eVar);

        void a(StripeException stripeException);

        boolean a();
    }

    public static void a(Context context, a aVar) {
        Map<String, Object> a2 = g.a(context);
        e.a(a2);
        if (aVar == null || aVar.a()) {
            a(a2, "https://m.stripe.com/4", "POST", c.a(null, "json_data").a(g.b(context)).a(), aVar);
        }
    }

    public static j a(Context context, Map<String, Object> map, c cVar, String str, a aVar) {
        try {
            String d2 = cVar.d();
            if (f.c(d2)) {
                return null;
            }
            map.remove("product_usage");
            a(context, aVar);
            a(c.a((List) map.get("product_usage"), d2, str), cVar, aVar);
            return b("POST", a(), map, cVar);
        } catch (ClassCastException e2) {
            map.remove("product_usage");
        }
    }

    static String a(Map<String, Object> map) {
        StringBuilder sb = new StringBuilder();
        for (b next : b(map)) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(b(next.f6850a, next.f6851b));
        }
        return sb.toString();
    }

    static Map<String, String> a(c cVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("Accept-Charset", "UTF-8");
        hashMap.put(io.fabric.sdk.android.services.common.a.HEADER_ACCEPT, io.fabric.sdk.android.services.common.a.ACCEPT_JSON_VALUE);
        hashMap.put(io.fabric.sdk.android.services.common.a.HEADER_USER_AGENT, String.format("Stripe/v1 AndroidBindings/%s", "4.1.5"));
        if (cVar != null) {
            hashMap.put("Authorization", String.format(Locale.ENGLISH, "Bearer %s", cVar.d()));
        }
        HashMap hashMap2 = new HashMap();
        hashMap2.put("java.version", System.getProperty("java.version"));
        hashMap2.put("os.name", io.fabric.sdk.android.services.common.a.ANDROID_CLIENT_TYPE);
        hashMap2.put("os.version", String.valueOf(Build.VERSION.SDK_INT));
        hashMap2.put("bindings.version", "4.1.5");
        hashMap2.put("lang", "Java");
        hashMap2.put("publisher", "Stripe");
        hashMap.put("X-Stripe-Client-User-Agent", new JSONObject(hashMap2).toString());
        if (!(cVar == null || cVar.a() == null)) {
            hashMap.put("Stripe-Version", cVar.a());
        }
        if (!(cVar == null || cVar.c() == null)) {
            hashMap.put("Idempotency-Key", cVar.c());
        }
        if (!(cVar == null || cVar.f() == null)) {
            hashMap.put("Stripe-Account", cVar.f());
        }
        return hashMap;
    }

    static String a() {
        return String.format(Locale.ENGLISH, "%s/v1/%s", "https://api.stripe.com", "tokens");
    }

    private static String a(String str, String str2) {
        if (str2 == null || str2.isEmpty()) {
            return str;
        }
        return String.format("%s%s%s", str, str.contains("?") ? "&" : "?", str2);
    }

    private static HttpURLConnection a(String str, String str2, c cVar) {
        HttpURLConnection a2 = a(a(str, str2), cVar);
        a2.setRequestMethod("GET");
        return a2;
    }

    private static HttpURLConnection a(String str, Map<String, Object> map, c cVar) {
        HttpURLConnection a2 = a(str, cVar);
        a2.setDoOutput(true);
        a2.setRequestMethod("POST");
        a2.setRequestProperty("Content-Type", b(cVar));
        OutputStream outputStream = null;
        try {
            outputStream = a2.getOutputStream();
            outputStream.write(a(map, cVar));
            return a2;
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    private static String b(c cVar) {
        if ("json_data".equals(cVar.e())) {
            return String.format("application/json; charset=%s", "UTF-8");
        }
        return String.format("application/x-www-form-urlencoded;charset=%s", "UTF-8");
    }

    private static byte[] a(Map<String, Object> map, c cVar) {
        try {
            if (!"json_data".equals(cVar.e())) {
                return a(map).getBytes("UTF-8");
            }
            JSONObject a2 = com.stripe.android.d.d.a(map);
            if (a2 != null) {
                return a2.toString().getBytes("UTF-8");
            }
            throw new InvalidRequestException("Unable to create JSON data from parameters. Please contact support@stripe.com for assistance.", null, null, 0, null);
        } catch (UnsupportedEncodingException e2) {
            throw new InvalidRequestException("Unable to encode parameters to UTF-8. Please contact support@stripe.com for assistance.", null, null, 0, e2);
        }
    }

    private static HttpURLConnection a(String str, c cVar) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(80000);
        httpURLConnection.setUseCaches(false);
        if (a(str)) {
            for (Map.Entry next : a(cVar).entrySet()) {
                httpURLConnection.setRequestProperty((String) next.getKey(), (String) next.getValue());
            }
        }
        if (b(str)) {
            a(httpURLConnection, cVar);
        }
        if (httpURLConnection instanceof HttpsURLConnection) {
            ((HttpsURLConnection) httpURLConnection).setSSLSocketFactory(f6849a);
        }
        return httpURLConnection;
    }

    private static boolean a(String str) {
        return str.startsWith("https://api.stripe.com") || str.startsWith("https://q.stripe.com");
    }

    private static boolean b(String str) {
        return str.startsWith("https://m.stripe.com/4");
    }

    private static void a(HttpURLConnection httpURLConnection, c cVar) {
        if (cVar.b() != null && !TextUtils.isEmpty(cVar.b())) {
            httpURLConnection.setRequestProperty("Cookie", "m=" + cVar.b());
        }
    }

    public static void a(Map<String, Object> map, c cVar, a aVar) {
        if (cVar != null) {
            if ((aVar == null || aVar.a()) && !cVar.d().trim().isEmpty()) {
                a(map, "https://q.stripe.com", "GET", cVar, aVar);
            }
        }
    }

    private static void a(Map<String, Object> map, String str, String str2, c cVar, a aVar) {
        String str3;
        String str4 = null;
        Boolean bool = true;
        try {
            str4 = Security.getProperty("networkaddress.cache.ttl");
            Security.setProperty("networkaddress.cache.ttl", "0");
            str3 = str4;
        } catch (SecurityException e2) {
            bool = false;
            str3 = str4;
        }
        try {
            e c2 = c(str2, str, map, cVar);
            if (aVar != null) {
                aVar.a(c2);
            }
            if (!bool.booleanValue()) {
                return;
            }
            if (str3 == null) {
                Security.setProperty("networkaddress.cache.ttl", "-1");
            } else {
                Security.setProperty("networkaddress.cache.ttl", str3);
            }
        } catch (StripeException e3) {
            if (aVar != null) {
                aVar.a(e3);
            }
            if (!bool.booleanValue()) {
                return;
            }
            if (str3 == null) {
                Security.setProperty("networkaddress.cache.ttl", "-1");
            } else {
                Security.setProperty("networkaddress.cache.ttl", str3);
            }
        } catch (Throwable th) {
            if (bool.booleanValue()) {
                if (str3 == null) {
                    Security.setProperty("networkaddress.cache.ttl", "-1");
                } else {
                    Security.setProperty("networkaddress.cache.ttl", str3);
                }
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.stripe.android.b.e a(java.lang.String r9, java.lang.String r10, java.util.Map<java.lang.String, java.lang.Object> r11, com.stripe.android.b.c r12) {
        /*
            r8 = 0
            r3 = 0
            if (r12 != 0) goto L_0x0006
            r0 = r3
        L_0x0005:
            return r0
        L_0x0006:
            r0 = 1
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            java.lang.String r1 = "networkaddress.cache.ttl"
            java.lang.String r1 = java.security.Security.getProperty(r1)     // Catch:{ SecurityException -> 0x0034 }
            java.lang.String r2 = "networkaddress.cache.ttl"
            java.lang.String r4 = "0"
            java.security.Security.setProperty(r2, r4)     // Catch:{ SecurityException -> 0x008a }
            r2 = r1
            r1 = r0
        L_0x001a:
            java.lang.String r0 = r12.d()
            java.lang.String r0 = r0.trim()
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x003d
            com.stripe.android.exception.AuthenticationException r0 = new com.stripe.android.exception.AuthenticationException
            java.lang.String r1 = "No API key provided. (HINT: set your API key using 'Stripe.apiKey = <API-KEY>'. You can generate API keys from the Stripe web interface. See https://stripe.com/api for details or email support@stripe.com if you have questions."
            java.lang.Integer r2 = java.lang.Integer.valueOf(r8)
            r0.<init>(r1, r3, r2)
            throw r0
        L_0x0034:
            r0 = move-exception
            r1 = r3
        L_0x0036:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r8)
            r2 = r1
            r1 = r0
            goto L_0x001a
        L_0x003d:
            com.stripe.android.b.e r4 = c(r9, r10, r11, r12)
            int r5 = r4.a()
            java.lang.String r6 = r4.b()
            java.util.Map r0 = r4.c()
            if (r0 != 0) goto L_0x007b
            r0 = r3
        L_0x0050:
            if (r0 == 0) goto L_0x005f
            int r7 = r0.size()
            if (r7 <= 0) goto L_0x005f
            java.lang.Object r0 = r0.get(r8)
            java.lang.String r0 = (java.lang.String) r0
            r3 = r0
        L_0x005f:
            r0 = 200(0xc8, float:2.8E-43)
            if (r5 < r0) goto L_0x0067
            r0 = 300(0x12c, float:4.2E-43)
            if (r5 < r0) goto L_0x006a
        L_0x0067:
            a(r6, r5, r3)
        L_0x006a:
            boolean r0 = r1.booleanValue()
            if (r0 == 0) goto L_0x0079
            if (r2 != 0) goto L_0x0084
            java.lang.String r0 = "networkaddress.cache.ttl"
            java.lang.String r1 = "-1"
            java.security.Security.setProperty(r0, r1)
        L_0x0079:
            r0 = r4
            goto L_0x0005
        L_0x007b:
            java.lang.String r7 = "Request-Id"
            java.lang.Object r0 = r0.get(r7)
            java.util.List r0 = (java.util.List) r0
            goto L_0x0050
        L_0x0084:
            java.lang.String r0 = "networkaddress.cache.ttl"
            java.security.Security.setProperty(r0, r2)
            goto L_0x0079
        L_0x008a:
            r0 = move-exception
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.stripe.android.b.d.a(java.lang.String, java.lang.String, java.util.Map, com.stripe.android.b.c):com.stripe.android.b.e");
    }

    private static j b(String str, String str2, Map<String, Object> map, c cVar) {
        try {
            return h.a(a(str, str2, map, cVar).b());
        } catch (JSONException e2) {
            return null;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static e c(String str, String str2, Map<String, Object> map, c cVar) {
        HttpURLConnection a2;
        String a3;
        boolean z = false;
        HttpURLConnection httpURLConnection = null;
        try {
            switch (str.hashCode()) {
                case 70454:
                    if (str.equals("GET")) {
                        break;
                    }
                    z = true;
                    break;
                case 2461856:
                    if (str.equals("POST")) {
                        z = true;
                        break;
                    }
                    z = true;
                    break;
                default:
                    z = true;
                    break;
            }
            switch (z) {
                case false:
                    a2 = a(str2, a(map), cVar);
                    break;
                case true:
                    a2 = a(str2, map, cVar);
                    break;
                default:
                    throw new APIConnectionException(String.format("Unrecognized HTTP method %s. This indicates a bug in the Stripe bindings. Please contact support@stripe.com for assistance.", str));
            }
            int responseCode = a2.getResponseCode();
            if (responseCode < 200 || responseCode >= 300) {
                a3 = a(a2.getErrorStream());
            } else {
                a3 = a(a2.getInputStream());
            }
            e eVar = new e(responseCode, a3, a2.getHeaderFields());
            if (a2 != null) {
                a2.disconnect();
            }
            return eVar;
        } catch (IOException e2) {
            throw new APIConnectionException(String.format("IOException during API request to Stripe (%s): %s Please check your internet connection and try again. If this problem persists, you should check Stripe's service status at https://twitter.com/stripestatus, or let us know at support@stripe.com.", a(), e2.getMessage()), e2);
        } catch (Throwable th) {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    private static List<b> b(Map<String, Object> map) {
        return a(map, (String) null);
    }

    private static List<b> a(List<Object> list, String str) {
        LinkedList linkedList = new LinkedList();
        String format = String.format("%s[]", str);
        if (list.isEmpty()) {
            linkedList.add(new b(str, ""));
        } else {
            for (Object a2 : list) {
                linkedList.addAll(a(a2, format));
            }
        }
        return linkedList;
    }

    private static List<b> a(Map<String, Object> map, String str) {
        LinkedList linkedList = new LinkedList();
        if (map == null) {
            return linkedList;
        }
        for (Map.Entry next : map.entrySet()) {
            String str2 = (String) next.getKey();
            Object value = next.getValue();
            if (str != null) {
                str2 = String.format("%s[%s]", str, str2);
            }
            linkedList.addAll(a(value, str2));
        }
        return linkedList;
    }

    private static List<b> a(Object obj, String str) {
        if (obj instanceof Map) {
            return a((Map<String, Object>) ((Map) obj), str);
        }
        if (obj instanceof List) {
            return a((List<Object>) ((List) obj), str);
        }
        if ("".equals(obj)) {
            throw new InvalidRequestException("You cannot set '" + str + "' to an empty string. We interpret empty strings as null in requests. You may set '" + str + "' to null to delete the property.", str, null, 0, null);
        } else if (obj == null) {
            LinkedList linkedList = new LinkedList();
            linkedList.add(new b(str, ""));
            return linkedList;
        } else {
            LinkedList linkedList2 = new LinkedList();
            linkedList2.add(new b(str, obj.toString()));
            return linkedList2;
        }
    }

    private static void a(String str, int i, String str2) {
        b.a a2 = b.a(str);
        switch (i) {
            case 400:
                throw new InvalidRequestException(a2.f6832b, a2.f6834d, str2, Integer.valueOf(i), null);
            case 401:
                throw new AuthenticationException(a2.f6832b, str2, Integer.valueOf(i));
            case 402:
                throw new CardException(a2.f6832b, str2, a2.f6833c, a2.f6834d, a2.f6835e, a2.f6836f, Integer.valueOf(i), null);
            case 403:
                throw new PermissionException(a2.f6832b, str2, Integer.valueOf(i));
            case 404:
                throw new InvalidRequestException(a2.f6832b, a2.f6834d, str2, Integer.valueOf(i), null);
            case 429:
                throw new RateLimitException(a2.f6832b, a2.f6834d, str2, Integer.valueOf(i), null);
            default:
                throw new APIException(a2.f6832b, str2, Integer.valueOf(i), null);
        }
    }

    private static String b(String str, String str2) {
        return String.format("%s=%s", c(str), c(str2));
    }

    private static String c(String str) {
        if (str == null) {
            return null;
        }
        return URLEncoder.encode(str, "UTF-8");
    }

    private static String a(InputStream inputStream) {
        String next = new Scanner(inputStream, "UTF-8").useDelimiter("\\A").next();
        inputStream.close();
        return next;
    }

    /* compiled from: StripeApiHandler */
    private static final class b {

        /* renamed from: a  reason: collision with root package name */
        public final String f6850a;

        /* renamed from: b  reason: collision with root package name */
        public final String f6851b;

        public b(String str, String str2) {
            this.f6850a = str;
            this.f6851b = str2;
        }
    }
}
