package com.stripe.android.d;

import com.stripe.android.c.a;
import java.util.Calendar;
import java.util.Locale;

/* compiled from: DateUtils */
public class b {
    public static boolean a(int i) {
        if (c(i) < a.b().get(1)) {
            return true;
        }
        return false;
    }

    public static boolean a(int i, int i2) {
        if (a(i)) {
            return true;
        }
        Calendar b2 = a.b();
        if (c(i) != b2.get(1) || i2 >= b2.get(2) + 1) {
            return false;
        }
        return true;
    }

    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        try {
            int parseInt = Integer.parseInt(str);
            if (parseInt <= 0 || parseInt > 12) {
                return false;
            }
            return true;
        } catch (NumberFormatException e2) {
            return false;
        }
    }

    public static String[] b(String str) {
        String[] strArr = new String[2];
        if (str.length() >= 2) {
            strArr[0] = str.substring(0, 2);
            strArr[1] = str.substring(2);
        } else {
            strArr[0] = str;
            strArr[1] = "";
        }
        return strArr;
    }

    public static boolean b(int i, int i2) {
        return a(i, i2, Calendar.getInstance());
    }

    static boolean a(int i, int i2, Calendar calendar) {
        int i3;
        boolean z = true;
        if (i < 1 || i > 12 || i2 < 0 || i2 > 9980 || i2 < (i3 = calendar.get(1))) {
            return false;
        }
        if (i2 > i3) {
            return true;
        }
        if (i < calendar.get(2) + 1) {
            z = false;
        }
        return z;
    }

    public static int b(int i) {
        return a(i, Calendar.getInstance());
    }

    static int a(int i, Calendar calendar) {
        int i2 = calendar.get(1);
        int i3 = i2 / 100;
        if (i2 % 100 > 80 && i < 20) {
            i3++;
        } else if (i2 % 100 < 20 && i > 80) {
            i3--;
        }
        return (i3 * 100) + i;
    }

    private static int c(int i) {
        if (i >= 100 || i < 0) {
            return i;
        }
        String valueOf = String.valueOf(a.b().get(1));
        String substring = valueOf.substring(0, valueOf.length() - 2);
        return Integer.parseInt(String.format(Locale.US, "%s%02d", substring, Integer.valueOf(i)));
    }
}
