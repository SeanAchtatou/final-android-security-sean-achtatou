package com.stripe.android.d;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stripe.android.a.b;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* compiled from: StripeNetworkUtils */
public class e {

    /* compiled from: StripeNetworkUtils */
    public interface a {
        String a();

        String b();
    }

    public static Map<String, Object> a(Context context, b bVar) {
        return a((a) null, context, bVar);
    }

    private static Map<String, Object> a(a aVar, Context context, b bVar) {
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        hashMap2.put("number", f.b(bVar.g()));
        hashMap2.put("cvc", f.b(bVar.i()));
        hashMap2.put("exp_month", bVar.j());
        hashMap2.put("exp_year", bVar.k());
        hashMap2.put("name", f.b(bVar.l()));
        hashMap2.put(FirebaseAnalytics.Param.CURRENCY, f.b(bVar.s()));
        hashMap2.put("address_line1", f.b(bVar.m()));
        hashMap2.put("address_line2", f.b(bVar.n()));
        hashMap2.put("address_city", f.b(bVar.o()));
        hashMap2.put("address_zip", f.b(bVar.p()));
        hashMap2.put("address_state", f.b(bVar.q()));
        hashMap2.put("address_country", f.b(bVar.r()));
        a(hashMap2);
        hashMap.put("product_usage", bVar.h());
        hashMap.put("card", hashMap2);
        a(aVar, context, hashMap);
        return hashMap;
    }

    public static void a(Map<String, Object> map) {
        Iterator it = new HashSet(map.keySet()).iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (map.get(str) == null) {
                map.remove(str);
            }
            if ((map.get(str) instanceof CharSequence) && TextUtils.isEmpty((CharSequence) map.get(str))) {
                map.remove(str);
            }
            if (map.get(str) instanceof Map) {
                a((Map) map.get(str));
            }
        }
    }

    public static void a(a aVar, Context context, Map<String, Object> map) {
        String a2;
        String str;
        if (aVar == null) {
            a2 = Settings.Secure.getString(context.getContentResolver(), "android_id");
        } else {
            a2 = aVar.a();
        }
        if (!f.c(a2)) {
            String i = f.i(a2);
            if (aVar == null) {
                str = context.getApplicationContext().getPackageName() + a2;
            } else {
                str = aVar.b() + a2;
            }
            String i2 = f.i(str);
            if (!f.c(i)) {
                map.put("guid", i);
            }
            if (!f.c(i2)) {
                map.put("muid", i2);
            }
        }
    }
}
