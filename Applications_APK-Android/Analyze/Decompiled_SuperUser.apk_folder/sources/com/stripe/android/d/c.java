package com.stripe.android.d;

import android.os.Build;
import com.google.android.gms.tagmanager.DataLayer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: LoggingUtils */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public static final Set<String> f6860a = new HashSet();

    /* renamed from: b  reason: collision with root package name */
    static final Set<String> f6861b = new HashSet();

    static {
        f6860a.add("AndroidPay");
        f6860a.add("CardInputView");
        f6861b.add("analytics_ua");
        f6861b.add("bindings_version");
        f6861b.add("device_type");
        f6861b.add(DataLayer.EVENT_KEY);
        f6861b.add("os_version");
        f6861b.add("os_name");
        f6861b.add("os_release");
        f6861b.add("product_usage");
        f6861b.add("publishable_key");
        f6861b.add("source_type");
        f6861b.add("token_type");
    }

    public static Map<String, Object> a(List<String> list, String str, String str2) {
        return a(list, null, str2, str, "token_creation");
    }

    public static Map<String, Object> a(List<String> list, String str, String str2, String str3, String str4) {
        HashMap hashMap = new HashMap();
        hashMap.put("analytics_ua", b());
        hashMap.put(DataLayer.EVENT_KEY, a(str4));
        hashMap.put("publishable_key", str3);
        hashMap.put("os_name", Build.VERSION.CODENAME);
        hashMap.put("os_release", Build.VERSION.RELEASE);
        hashMap.put("os_version", Integer.valueOf(Build.VERSION.SDK_INT));
        hashMap.put("device_type", a());
        hashMap.put("bindings_version", "4.1.5");
        if (list != null) {
            hashMap.put("product_usage", list);
        }
        if (str != null) {
            hashMap.put("source_type", str);
        }
        if (str2 != null) {
            hashMap.put("token_type", str2);
        } else if (str == null) {
            hashMap.put("token_type", "unknown");
        }
        return hashMap;
    }

    static String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(Build.MANUFACTURER).append('_').append(Build.BRAND).append('_').append(Build.MODEL);
        return sb.toString();
    }

    static String b() {
        return "analytics.stripe_android-1.0";
    }

    static String a(String str) {
        return "stripe_android." + str;
    }
}
