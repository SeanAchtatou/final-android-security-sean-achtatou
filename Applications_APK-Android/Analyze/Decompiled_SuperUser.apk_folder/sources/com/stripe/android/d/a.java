package com.stripe.android.d;

import com.stripe.android.a.b;

/* compiled from: CardUtils */
public class a {
    public static boolean a(String str) {
        String e2 = f.e(str);
        return b(e2) && c(e2);
    }

    public static boolean b(String str) {
        if (str == null) {
            return false;
        }
        int i = 0;
        boolean z = true;
        for (int length = str.length() - 1; length >= 0; length--) {
            char charAt = str.charAt(length);
            if (!Character.isDigit(charAt)) {
                return false;
            }
            int numericValue = Character.getNumericValue(charAt);
            if (!z) {
                z = true;
            } else {
                z = false;
            }
            if (z) {
                numericValue *= 2;
            }
            if (numericValue > 9) {
                numericValue -= 9;
            }
            i += numericValue;
        }
        return i % 10 == 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.stripe.android.d.a.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.stripe.android.d.a.a(java.lang.String, java.lang.String):boolean
      com.stripe.android.d.a.a(java.lang.String, boolean):java.lang.String */
    public static boolean c(String str) {
        if (str == null) {
            return false;
        }
        return a(str, a(str, false));
    }

    public static boolean a(String str, String str2) {
        if (str == null || "Unknown".equals(str2)) {
            return false;
        }
        int length = str.length();
        char c2 = 65535;
        switch (str2.hashCode()) {
            case -993787207:
                if (str2.equals("Diners Club")) {
                    c2 = 1;
                    break;
                }
                break;
            case -298759312:
                if (str2.equals("American Express")) {
                    c2 = 0;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                if (length != 15) {
                    return false;
                }
                return true;
            case 1:
                if (length != 14) {
                    return false;
                }
                return true;
            default:
                if (length != 16) {
                    return false;
                }
                return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.stripe.android.d.a.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.stripe.android.d.a.a(java.lang.String, java.lang.String):boolean
      com.stripe.android.d.a.a(java.lang.String, boolean):java.lang.String */
    public static String d(String str) {
        return a(str, true);
    }

    private static String a(String str, boolean z) {
        if (f.c(str)) {
            return "Unknown";
        }
        if (z) {
            str = f.e(str);
        }
        if (f.a(str, b.f6771a)) {
            return "American Express";
        }
        if (f.a(str, b.f6772b)) {
            return "Discover";
        }
        if (f.a(str, b.f6773c)) {
            return "JCB";
        }
        if (f.a(str, b.f6774d)) {
            return "Diners Club";
        }
        if (f.a(str, b.f6775e)) {
            return "Visa";
        }
        if (f.a(str, b.f6776f)) {
            return "MasterCard";
        }
        return "Unknown";
    }

    public static String[] b(String str, String str2) {
        int i;
        int i2 = 10;
        int i3 = 0;
        if (str2.equals("American Express")) {
            String[] strArr = new String[3];
            int length = str.length();
            if (length > 4) {
                strArr[0] = str.substring(0, 4);
                i = 4;
            } else {
                i = 0;
            }
            if (length > 10) {
                strArr[1] = str.substring(4, 10);
            } else {
                i2 = i;
            }
            while (true) {
                if (i3 < 3) {
                    if (strArr[i3] == null) {
                        strArr[i3] = str.substring(i2);
                        break;
                    }
                    i3++;
                } else {
                    break;
                }
            }
            return strArr;
        }
        String[] strArr2 = new String[4];
        int i4 = 0;
        while ((i3 + 1) * 4 < str.length()) {
            strArr2[i3] = str.substring(i4, (i3 + 1) * 4);
            i4 = (i3 + 1) * 4;
            i3++;
        }
        strArr2[i3] = str.substring(i4);
        return strArr2;
    }
}
