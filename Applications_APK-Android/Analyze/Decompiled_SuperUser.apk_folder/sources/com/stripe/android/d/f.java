package com.stripe.android.d;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: StripeTextUtils */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private static int[] f6862a = {7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 76};

    /* renamed from: b  reason: collision with root package name */
    private static final char[] f6863b = "0123456789ABCDEF".toCharArray();

    public static boolean a(String str, String... strArr) {
        if (str == null) {
            return false;
        }
        for (String startsWith : strArr) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String b(String str) {
        if (c(str)) {
            return null;
        }
        return str;
    }

    public static boolean c(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static String d(String str) {
        if ("company".equals(str)) {
            return "company";
        }
        if ("individual".equals(str)) {
            return "individual";
        }
        return null;
    }

    public static String e(String str) {
        if (c(str)) {
            return null;
        }
        return str.replaceAll("\\s|-", "");
    }

    public static String f(String str) {
        if (c(str)) {
            return null;
        }
        if ("American Express".equalsIgnoreCase(str)) {
            return "American Express";
        }
        if ("MasterCard".equalsIgnoreCase(str)) {
            return "MasterCard";
        }
        if ("Diners Club".equalsIgnoreCase(str)) {
            return "Diners Club";
        }
        if ("Discover".equalsIgnoreCase(str)) {
            return "Discover";
        }
        if ("JCB".equalsIgnoreCase(str)) {
            return "JCB";
        }
        if ("Visa".equalsIgnoreCase(str)) {
            return "Visa";
        }
        return "Unknown";
    }

    public static String g(String str) {
        if (c(str)) {
            return null;
        }
        if ("credit".equalsIgnoreCase(str)) {
            return "credit";
        }
        if ("debit".equalsIgnoreCase(str)) {
            return "debit";
        }
        if ("prepaid".equalsIgnoreCase(str)) {
            return "prepaid";
        }
        return "unknown";
    }

    public static String h(String str) {
        if (c(str)) {
            return null;
        }
        if ("card".equals(str)) {
            return "card";
        }
        if ("bank_account".equals(str)) {
            return "bank_account";
        }
        if ("pii".equals(str)) {
            return "pii";
        }
        return null;
    }

    public static String i(String str) {
        if (c(str)) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            byte[] bytes = str.getBytes("UTF-8");
            instance.update(bytes, 0, bytes.length);
            return a(instance.digest());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e2) {
            return null;
        }
    }

    private static String a(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            byte b2 = bArr[i] & 255;
            cArr[i * 2] = f6863b[b2 >>> 4];
            cArr[(i * 2) + 1] = f6863b[b2 & 15];
        }
        return new String(cArr);
    }
}
