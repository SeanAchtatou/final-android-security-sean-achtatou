package com.stripe.android.d;

import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StripeJsonUtils */
public class d {
    public static String a(JSONObject jSONObject, String str) {
        return a(jSONObject.getString(str));
    }

    public static Integer b(JSONObject jSONObject, String str) {
        if (!jSONObject.has(str)) {
            return null;
        }
        return Integer.valueOf(jSONObject.optInt(str));
    }

    public static String c(JSONObject jSONObject, String str) {
        return a(jSONObject.optString(str));
    }

    public static String d(JSONObject jSONObject, String str) {
        String a2 = a(jSONObject.optString(str));
        if (a2 == null || a2.length() != 2) {
            return null;
        }
        return a2;
    }

    public static String e(JSONObject jSONObject, String str) {
        String a2 = a(jSONObject.optString(str));
        if (a2 == null || a2.length() != 3) {
            return null;
        }
        return a2;
    }

    public static JSONObject a(Map<String, ? extends Object> map) {
        if (map == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        for (String next : map.keySet()) {
            Object obj = map.get(next);
            if (obj != null) {
                try {
                    if (obj instanceof Map) {
                        try {
                            jSONObject.put(next, a((Map) obj));
                        } catch (ClassCastException e2) {
                        }
                    } else if (obj instanceof List) {
                        jSONObject.put(next, a((List) obj));
                    } else if ((obj instanceof Number) || (obj instanceof Boolean)) {
                        jSONObject.put(next, obj);
                    } else {
                        jSONObject.put(next, obj.toString());
                    }
                } catch (JSONException e3) {
                }
            }
        }
        return jSONObject;
    }

    public static JSONArray a(List list) {
        if (list == null) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        for (Object next : list) {
            if (next instanceof Map) {
                try {
                    jSONArray.put(a((Map) next));
                } catch (ClassCastException e2) {
                }
            } else if (next instanceof List) {
                jSONArray.put(a((List) next));
            } else if ((next instanceof Number) || (next instanceof Boolean)) {
                jSONArray.put(next);
            } else {
                jSONArray.put(next.toString());
            }
        }
        return jSONArray;
    }

    public static void a(JSONObject jSONObject, String str, String str2) {
        if (!f.c(str2)) {
            try {
                jSONObject.put(str, str2);
            } catch (JSONException e2) {
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, Integer num) {
        if (num != null) {
            try {
                jSONObject.put(str, num.intValue());
            } catch (JSONException e2) {
            }
        }
    }

    public static String a(String str) {
        if ("null".equals(str) || "".equals(str)) {
            return null;
        }
        return str;
    }
}
