package com.stripe.android.exception;

public class CardException extends StripeException {

    /* renamed from: a  reason: collision with root package name */
    private String f6864a;

    /* renamed from: b  reason: collision with root package name */
    private String f6865b;

    /* renamed from: c  reason: collision with root package name */
    private String f6866c;

    /* renamed from: d  reason: collision with root package name */
    private String f6867d;

    public CardException(String str, String str2, String str3, String str4, String str5, String str6, Integer num, Throwable th) {
        super(str, str2, num, th);
        this.f6864a = str3;
        this.f6865b = str4;
        this.f6866c = str5;
        this.f6867d = str6;
    }
}
