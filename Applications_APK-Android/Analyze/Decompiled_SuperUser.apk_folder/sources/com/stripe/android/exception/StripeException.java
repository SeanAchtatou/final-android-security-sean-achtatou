package com.stripe.android.exception;

public abstract class StripeException extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private String f6869a;

    /* renamed from: b  reason: collision with root package name */
    private Integer f6870b;

    public StripeException(String str, String str2, Integer num) {
        super(str, null);
        this.f6869a = str2;
        this.f6870b = num;
    }

    public StripeException(String str, String str2, Integer num, Throwable th) {
        super(str, th);
        this.f6870b = num;
        this.f6869a = str2;
    }

    public String toString() {
        String str = "";
        if (this.f6869a != null) {
            str = "; request-id: " + this.f6869a;
        }
        return super.toString() + str;
    }
}
