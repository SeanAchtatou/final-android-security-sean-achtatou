package com.stripe.android.exception;

public class InvalidRequestException extends StripeException {

    /* renamed from: a  reason: collision with root package name */
    private final String f6868a;

    public InvalidRequestException(String str, String str2, String str3, Integer num, Throwable th) {
        super(str, str3, num, th);
        this.f6868a = str2;
    }
}
