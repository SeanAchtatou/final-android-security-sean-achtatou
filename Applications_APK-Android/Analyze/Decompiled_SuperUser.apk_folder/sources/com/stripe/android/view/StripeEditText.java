package com.stripe.android.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import com.stripe.android.a;

public class StripeEditText extends AppCompatEditText {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public a f6934a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public b f6935b;

    /* renamed from: c  reason: collision with root package name */
    private ColorStateList f6936c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f6937d;

    /* renamed from: e  reason: collision with root package name */
    private int f6938e;

    /* renamed from: f  reason: collision with root package name */
    private int f6939f;

    public interface a {
        void a(String str);
    }

    public interface b {
        void a();
    }

    public StripeEditText(Context context) {
        super(context);
        a();
    }

    public StripeEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public StripeEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        return new c(super.onCreateInputConnection(editorInfo), true);
    }

    public void setAfterTextChangedListener(a aVar) {
        this.f6934a = aVar;
    }

    public void setDeleteEmptyListener(b bVar) {
        this.f6935b = bVar;
    }

    public void setShouldShowError(boolean z) {
        this.f6937d = z;
        if (this.f6937d) {
            setTextColor(this.f6939f);
        } else {
            setTextColor(this.f6936c);
        }
        refreshDrawableState();
    }

    public ColorStateList getCachedColorStateList() {
        return this.f6936c;
    }

    public boolean getShouldShowError() {
        return this.f6937d;
    }

    public int getDefaultErrorColorInt() {
        b();
        if (Build.VERSION.SDK_INT >= 23) {
            return getResources().getColor(this.f6938e, null);
        }
        return getResources().getColor(this.f6938e);
    }

    public void setErrorColor(int i) {
        this.f6939f = i;
    }

    static boolean a(int i) {
        if ((((0.299d * ((double) Color.red(i))) + (0.587d * ((double) Color.green(i)))) + (0.114d * ((double) Color.blue(i)))) / 255.0d > 0.5d) {
            return false;
        }
        return true;
    }

    private void a() {
        c();
        d();
        b();
        this.f6936c = getTextColors();
    }

    private void b() {
        this.f6936c = getTextColors();
        if (a(this.f6936c.getDefaultColor())) {
            this.f6938e = a.C0095a.bj;
        } else {
            this.f6938e = a.C0095a.bi;
        }
    }

    private void c() {
        addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
                if (StripeEditText.this.f6934a != null) {
                    StripeEditText.this.f6934a.a(editable.toString());
                }
            }
        });
    }

    private void d() {
        setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i != 67 || keyEvent.getAction() != 0 || StripeEditText.this.f6935b == null || StripeEditText.this.length() != 0) {
                    return false;
                }
                StripeEditText.this.f6935b.a();
                return false;
            }
        });
    }

    private class c extends InputConnectionWrapper {
        public c(InputConnection inputConnection, boolean z) {
            super(inputConnection, z);
        }

        public boolean deleteSurroundingText(int i, int i2) {
            if (getTextBeforeCursor(1, 0).length() == 0 && StripeEditText.this.f6935b != null) {
                StripeEditText.this.f6935b.a();
            }
            return super.deleteSurroundingText(i, i2);
        }
    }
}
