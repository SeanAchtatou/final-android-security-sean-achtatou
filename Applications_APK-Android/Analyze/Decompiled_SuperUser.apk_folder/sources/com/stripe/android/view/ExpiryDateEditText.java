package com.stripe.android.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import com.stripe.android.d.b;

public class ExpiryDateEditText extends StripeEditText {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public a f6925a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f6926b;

    interface a {
        void a();
    }

    public ExpiryDateEditText(Context context) {
        super(context);
        a();
    }

    public ExpiryDateEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public ExpiryDateEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    public int[] getValidDateFields() {
        if (!this.f6926b) {
            return null;
        }
        int[] iArr = new int[2];
        String[] b2 = b.b(getText().toString().replaceAll("/", ""));
        try {
            iArr[0] = Integer.parseInt(b2[0]);
            iArr[1] = b.b(Integer.parseInt(b2[1]));
            return iArr;
        } catch (NumberFormatException e2) {
            return null;
        }
    }

    public void setExpiryDateEditListener(a aVar) {
        this.f6925a = aVar;
    }

    private void a() {
        addTextChangedListener(new TextWatcher() {

            /* renamed from: a  reason: collision with root package name */
            boolean f6927a = false;

            /* renamed from: b  reason: collision with root package name */
            int f6928b;

            /* renamed from: c  reason: collision with root package name */
            int f6929c;

            /* renamed from: d  reason: collision with root package name */
            String[] f6930d = new String[2];

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!this.f6927a) {
                    this.f6928b = i;
                    this.f6929c = i3;
                }
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                boolean z;
                if (!this.f6927a) {
                    String replaceAll = charSequence.toString().replaceAll("/", "");
                    if (replaceAll.length() == 1 && this.f6928b == 0 && this.f6929c == 1) {
                        char charAt = replaceAll.charAt(0);
                        if (!(charAt == '0' || charAt == '1')) {
                            replaceAll = "0" + replaceAll;
                            this.f6929c++;
                        }
                    } else if (replaceAll.length() == 2 && this.f6928b == 2 && this.f6929c == 0) {
                        replaceAll = replaceAll.substring(0, 1);
                    }
                    this.f6930d = b.b(replaceAll);
                    if (!b.a(this.f6930d[0])) {
                        z = true;
                    } else {
                        z = false;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.f6930d[0]);
                    if ((this.f6930d[0].length() == 2 && this.f6929c > 0 && !z) || replaceAll.length() > 2) {
                        sb.append("/");
                    }
                    sb.append(this.f6930d[1]);
                    String sb2 = sb.toString();
                    int a2 = ExpiryDateEditText.this.a(sb2.length(), this.f6928b, this.f6929c);
                    this.f6927a = true;
                    ExpiryDateEditText.this.setText(sb2);
                    ExpiryDateEditText.this.setSelection(a2);
                    this.f6927a = false;
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.stripe.android.view.ExpiryDateEditText.a(com.stripe.android.view.ExpiryDateEditText, boolean):boolean
             arg types: [com.stripe.android.view.ExpiryDateEditText, int]
             candidates:
              com.stripe.android.view.ExpiryDateEditText.a(com.stripe.android.view.ExpiryDateEditText, java.lang.String[]):void
              com.stripe.android.view.ExpiryDateEditText.a(com.stripe.android.view.ExpiryDateEditText, boolean):boolean */
            public void afterTextChanged(Editable editable) {
                boolean z;
                boolean z2;
                if (this.f6930d[0].length() != 2 || b.a(this.f6930d[0])) {
                    z = false;
                } else {
                    z = true;
                }
                if (this.f6930d[0].length() == 2 && this.f6930d[1].length() == 2) {
                    boolean a2 = ExpiryDateEditText.this.f6926b;
                    ExpiryDateEditText.this.a(this.f6930d);
                    z2 = !ExpiryDateEditText.this.f6926b;
                    if (!a2 && ExpiryDateEditText.this.f6926b && ExpiryDateEditText.this.f6925a != null) {
                        ExpiryDateEditText.this.f6925a.a();
                    }
                } else {
                    boolean unused = ExpiryDateEditText.this.f6926b = false;
                }
                ExpiryDateEditText.this.setShouldShowError(z2);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public int a(int i, int i2, int i3) {
        int i4;
        boolean z = false;
        if (i2 > 2 || i2 + i3 < 2) {
            i4 = 0;
        } else {
            i4 = 1;
        }
        if (i3 == 0 && i2 == 3) {
            z = true;
        }
        int i5 = i2 + i3 + i4;
        if (z && i5 > 0) {
            i5--;
        }
        return i5 <= i ? i5 : i;
    }

    /* access modifiers changed from: private */
    public void a(String[] strArr) {
        int i;
        int i2 = -1;
        if (strArr[0].length() != 2) {
            i = -1;
        } else {
            try {
                i = Integer.parseInt(strArr[0]);
            } catch (NumberFormatException e2) {
                i = -1;
            }
        }
        if (strArr[1].length() == 2) {
            try {
                i2 = b.b(Integer.parseInt(strArr[1]));
            } catch (NumberFormatException e3) {
            }
        }
        this.f6926b = b.b(i, i2);
    }
}
