package com.stripe.android.view;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import com.stripe.android.d.f;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CardNumberEditText extends StripeEditText {

    /* renamed from: b  reason: collision with root package name */
    private static final Integer[] f6914b = {4, 9, 14};

    /* renamed from: c  reason: collision with root package name */
    private static final Set<Integer> f6915c = new HashSet(Arrays.asList(f6914b));

    /* renamed from: d  reason: collision with root package name */
    private static final Integer[] f6916d = {4, 11};

    /* renamed from: e  reason: collision with root package name */
    private static final Set<Integer> f6917e = new HashSet(Arrays.asList(f6916d));

    /* renamed from: a  reason: collision with root package name */
    String f6918a = "Unknown";

    /* renamed from: f  reason: collision with root package name */
    private a f6919f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public b f6920g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public int f6921h = 19;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = false;

    interface a {
        void a(String str);
    }

    interface b {
        void a();
    }

    public CardNumberEditText(Context context) {
        super(context);
        b();
    }

    public CardNumberEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
    }

    public CardNumberEditText(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        b();
    }

    public String getCardBrand() {
        return this.f6918a;
    }

    public String getCardNumber() {
        if (this.j) {
            return f.e(getText().toString());
        }
        return null;
    }

    public int getLengthMax() {
        return this.f6921h;
    }

    public boolean a() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void setCardNumberCompleteListener(b bVar) {
        this.f6920g = bVar;
    }

    /* access modifiers changed from: package-private */
    public void setCardBrandChangeListener(a aVar) {
        this.f6919f = aVar;
        this.f6919f.a(this.f6918a);
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3, int i4) {
        boolean z = false;
        int i5 = 0;
        for (Integer next : "American Express".equals(this.f6918a) ? f6917e : f6915c) {
            if (i3 <= next.intValue() && i3 + i4 > next.intValue()) {
                i5++;
            }
            if (i4 == 0 && i3 == next.intValue() + 1) {
                z = true;
            }
            z = z;
        }
        int i6 = i3 + i4 + i5;
        if (z && i6 > 0) {
            i6--;
        }
        return i6 <= i2 ? i6 : i2;
    }

    private void b() {
        addTextChangedListener(new TextWatcher() {

            /* renamed from: a  reason: collision with root package name */
            int f6922a;

            /* renamed from: b  reason: collision with root package name */
            int f6923b;

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!CardNumberEditText.this.i) {
                    this.f6922a = i;
                    this.f6923b = i3;
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.stripe.android.view.CardNumberEditText.a(com.stripe.android.view.CardNumberEditText, boolean):boolean
             arg types: [com.stripe.android.view.CardNumberEditText, int]
             candidates:
              com.stripe.android.view.CardNumberEditText.a(com.stripe.android.view.CardNumberEditText, java.lang.String):void
              com.stripe.android.view.CardNumberEditText.a(com.stripe.android.view.CardNumberEditText, boolean):boolean */
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                String e2;
                if (!CardNumberEditText.this.i) {
                    if (i < 4) {
                        CardNumberEditText.this.b(charSequence.toString());
                    }
                    if (i <= 16 && (e2 = f.e(charSequence.toString())) != null) {
                        String[] b2 = com.stripe.android.d.a.b(e2, CardNumberEditText.this.f6918a);
                        StringBuilder sb = new StringBuilder();
                        int i4 = 0;
                        while (i4 < b2.length && b2[i4] != null) {
                            if (i4 != 0) {
                                sb.append(' ');
                            }
                            sb.append(b2[i4]);
                            i4++;
                        }
                        String sb2 = sb.toString();
                        int a2 = CardNumberEditText.this.a(sb2.length(), this.f6922a, this.f6923b);
                        boolean unused = CardNumberEditText.this.i = true;
                        CardNumberEditText.this.setText(sb2);
                        CardNumberEditText.this.setSelection(a2);
                        boolean unused2 = CardNumberEditText.this.i = false;
                    }
                }
            }

            public void afterTextChanged(Editable editable) {
                boolean z = true;
                if (editable.length() == CardNumberEditText.this.f6921h) {
                    boolean c2 = CardNumberEditText.this.j;
                    boolean unused = CardNumberEditText.this.j = com.stripe.android.d.a.a(editable.toString());
                    CardNumberEditText cardNumberEditText = CardNumberEditText.this;
                    if (CardNumberEditText.this.j) {
                        z = false;
                    }
                    cardNumberEditText.setShouldShowError(z);
                    if (!c2 && CardNumberEditText.this.j && CardNumberEditText.this.f6920g != null) {
                        CardNumberEditText.this.f6920g.a();
                        return;
                    }
                    return;
                }
                CardNumberEditText cardNumberEditText2 = CardNumberEditText.this;
                if (CardNumberEditText.this.getText() == null || !com.stripe.android.d.a.a(CardNumberEditText.this.getText().toString())) {
                    z = false;
                }
                boolean unused2 = cardNumberEditText2.j = z;
                CardNumberEditText.this.setShouldShowError(false);
            }
        });
    }

    private void a(String str) {
        if (!this.f6918a.equals(str)) {
            this.f6918a = str;
            if (this.f6919f != null) {
                this.f6919f.a(this.f6918a);
            }
            int i2 = this.f6921h;
            this.f6921h = c(this.f6918a);
            if (i2 != this.f6921h) {
                setFilters(new InputFilter[]{new InputFilter.LengthFilter(this.f6921h)});
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        a(com.stripe.android.d.a.d(str));
    }

    private static int c(String str) {
        if ("American Express".equals(str) || "Diners Club".equals(str)) {
            return 17;
        }
        return 19;
    }
}
