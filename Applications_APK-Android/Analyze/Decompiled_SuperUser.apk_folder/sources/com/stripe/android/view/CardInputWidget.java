package com.stripe.android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.InputFilter;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.stripe.android.a;
import com.stripe.android.d.f;
import com.stripe.android.view.CardNumberEditText;
import com.stripe.android.view.ExpiryDateEditText;
import com.stripe.android.view.StripeEditText;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CardInputWidget extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<String, Integer> f6871a = new HashMap<String, Integer>() {
        {
            put("American Express", Integer.valueOf(a.c.f6787ic_amex));
            put("Diners Club", Integer.valueOf(a.c.f6790ic_diners));
            put("Discover", Integer.valueOf(a.c.f6791ic_discover));
            put("JCB", Integer.valueOf(a.c.j));
            put("MasterCard", Integer.valueOf(a.c.k));
            put("Visa", Integer.valueOf(a.c.m));
            put("Unknown", Integer.valueOf(a.c.l));
        }
    };

    /* renamed from: b  reason: collision with root package name */
    private ImageView f6872b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public c f6873c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public CardNumberEditText f6874d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f6875e = true;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public StripeEditText f6876f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public ExpiryDateEditText f6877g;

    /* renamed from: h  reason: collision with root package name */
    private FrameLayout f6878h;
    private String i;
    private int j;
    private int k;
    /* access modifiers changed from: private */
    public boolean l;
    private boolean m;
    private int n;
    private d o;
    /* access modifiers changed from: private */
    public e p;

    public interface c {
        void a();

        void a(String str);

        void b();

        void c();
    }

    interface d {
        int a();

        int a(String str, EditText editText);
    }

    public CardInputWidget(Context context) {
        super(context);
        a((AttributeSet) null);
    }

    public CardInputWidget(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet);
    }

    public CardInputWidget(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(attributeSet);
    }

    public com.stripe.android.a.b getCard() {
        String cardNumber = this.f6874d.getCardNumber();
        int[] validDateFields = this.f6877g.getValidDateFields();
        if (cardNumber == null || validDateFields == null || validDateFields.length != 2) {
            return null;
        }
        int i2 = this.l ? 4 : 3;
        String obj = this.f6876f.getText().toString();
        if (f.c(obj) || obj.length() != i2) {
            return null;
        }
        return new com.stripe.android.a.b(cardNumber, Integer.valueOf(validDateFields[0]), Integer.valueOf(validDateFields[1]), obj).a("CardInputView");
    }

    public void setCardInputListener(c cVar) {
        this.f6873c = cVar;
    }

    public void setCardNumber(String str) {
        this.f6874d.setText(str);
        setCardNumberIsViewed(!this.f6874d.a());
    }

    public void setCvcCode(String str) {
        this.f6876f.setText(str);
    }

    public void setEnabled(boolean z) {
        this.f6874d.setEnabled(z);
        this.f6877g.setEnabled(z);
        this.f6876f.setEnabled(z);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        StripeEditText a2 = a((int) motionEvent.getX());
        if (a2 == null) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        a2.requestFocus();
        return true;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("extra_super_state", super.onSaveInstanceState());
        bundle.putBoolean("extra_card_viewed", this.f6875e);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        int i2;
        int i3;
        int i4;
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.f6875e = bundle.getBoolean("extra_card_viewed", true);
            a(this.f6875e);
            this.n = getFrameWidth();
            if (this.f6875e) {
                i2 = 0;
                i3 = this.p.f6909d + this.p.f6906a;
                i4 = this.n;
            } else {
                i2 = this.p.f6907b * -1;
                i3 = this.p.f6909d + this.p.f6908c;
                i4 = this.p.f6910e + i3 + this.p.f6911f;
            }
            a(this.p.f6906a, i2, this.f6874d);
            a(this.p.f6910e, i3, this.f6877g);
            a(this.p.f6912g, i4, this.f6876f);
            super.onRestoreInstanceState(bundle.getParcelable("extra_super_state"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: package-private */
    public StripeEditText a(int i2) {
        int left = this.f6878h.getLeft();
        if (this.f6875e) {
            if (i2 < left + this.p.f6906a) {
                return null;
            }
            if (i2 < this.p.f6913h) {
                return this.f6874d;
            }
            if (i2 < this.p.i) {
                return this.f6877g;
            }
            return null;
        } else if (i2 < left + this.p.f6908c) {
            return null;
        } else {
            if (i2 < this.p.f6913h) {
                return this.f6874d;
            }
            if (i2 < this.p.i) {
                return this.f6877g;
            }
            if (i2 < this.p.i + this.p.f6910e) {
                return null;
            }
            if (i2 < this.p.j) {
                return this.f6877g;
            }
            if (i2 < this.p.k) {
                return this.f6876f;
            }
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void setDimensionOverrideSettings(d dVar) {
        this.o = dVar;
    }

    /* access modifiers changed from: package-private */
    public void setCardNumberIsViewed(boolean z) {
        this.f6875e = z;
    }

    /* access modifiers changed from: package-private */
    public e getPlacementParameters() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        int frameWidth = getFrameWidth();
        int left = this.f6878h.getLeft();
        if (frameWidth != 0) {
            this.p.f6906a = a("4242 4242 4242 4242", this.f6874d);
            this.p.f6910e = a("MM/MM", this.f6877g);
            String cardBrand = this.f6874d.getCardBrand();
            this.p.f6907b = a(a(cardBrand), this.f6874d);
            this.p.f6912g = a(b(cardBrand), this.f6876f);
            this.p.f6908c = a(c(cardBrand), this.f6874d);
            if (z) {
                this.p.f6909d = (frameWidth - this.p.f6906a) - this.p.f6910e;
                this.p.f6913h = this.p.f6906a + left + (this.p.f6909d / 2);
                this.p.i = left + this.p.f6906a + this.p.f6909d;
                return;
            }
            this.p.f6909d = ((frameWidth / 2) - this.p.f6908c) - (this.p.f6910e / 2);
            this.p.f6911f = (((frameWidth - this.p.f6908c) - this.p.f6909d) - this.p.f6910e) - this.p.f6912g;
            this.p.f6913h = this.p.f6908c + left + (this.p.f6909d / 2);
            this.p.i = left + this.p.f6908c + this.p.f6909d;
            this.p.j = this.p.i + this.p.f6910e + (this.p.f6911f / 2);
            this.p.k = this.p.i + this.p.f6910e + this.p.f6911f;
        }
    }

    private void a(int i2, int i3, StripeEditText stripeEditText) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) stripeEditText.getLayoutParams();
        layoutParams.width = i2;
        layoutParams.leftMargin = i3;
        stripeEditText.setLayoutParams(layoutParams);
    }

    private int a(String str, StripeEditText stripeEditText) {
        if (this.o == null) {
            return (int) Layout.getDesiredWidth(str, stripeEditText.getPaint());
        }
        return this.o.a(str, stripeEditText);
    }

    private int getFrameWidth() {
        if (this.o == null) {
            return this.f6878h.getWidth();
        }
        return this.o.a();
    }

    private void a(AttributeSet attributeSet) {
        inflate(getContext(), a.e.ak, this);
        if (getId() == -1) {
            setId(42424242);
        }
        setOrientation(0);
        setMinimumWidth(getResources().getDimensionPixelSize(a.b.d4));
        this.p = new e();
        this.f6872b = (ImageView) findViewById(a.d.f7);
        this.f6874d = (CardNumberEditText) findViewById(a.d.f_);
        this.f6877g = (ExpiryDateEditText) findViewById(a.d.fb);
        this.f6876f = (StripeEditText) findViewById(a.d.fc);
        this.f6875e = true;
        this.f6878h = (FrameLayout) findViewById(a.d.f8);
        this.j = this.f6874d.getDefaultErrorColorInt();
        this.k = this.f6874d.getHintTextColors().getDefaultColor();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, a.g.CardInputView, 0, 0);
            try {
                this.j = obtainStyledAttributes.getColor(a.g.CardInputView_cardTextErrorColor, this.j);
                this.k = obtainStyledAttributes.getColor(a.g.CardInputView_cardTint, this.k);
                this.i = obtainStyledAttributes.getString(a.g.CardInputView_cardHintText);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        if (this.i != null) {
            this.f6874d.setHint(this.i);
        }
        this.f6874d.setErrorColor(this.j);
        this.f6877g.setErrorColor(this.j);
        this.f6876f.setErrorColor(this.j);
        this.f6874d.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (z) {
                    CardInputWidget.this.a();
                    if (CardInputWidget.this.f6873c != null) {
                        CardInputWidget.this.f6873c.a("focus_card");
                    }
                }
            }
        });
        this.f6877g.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (z) {
                    CardInputWidget.this.b();
                    if (CardInputWidget.this.f6873c != null) {
                        CardInputWidget.this.f6873c.a("focus_expiry");
                    }
                }
            }
        });
        this.f6877g.setDeleteEmptyListener(new b(this.f6874d));
        this.f6876f.setDeleteEmptyListener(new b(this.f6877g));
        this.f6876f.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (z) {
                    CardInputWidget.this.b();
                    if (CardInputWidget.this.f6873c != null) {
                        CardInputWidget.this.f6873c.a("focus_cvc");
                    }
                }
                CardInputWidget.this.b(CardInputWidget.this.f6874d.getCardBrand(), z, CardInputWidget.this.f6876f.getText().toString());
            }
        });
        this.f6876f.setAfterTextChangedListener(new StripeEditText.a() {
            public void a(String str) {
                if (CardInputWidget.this.f6873c != null && CardInputWidget.b(CardInputWidget.this.f6874d.getCardBrand(), str)) {
                    CardInputWidget.this.f6873c.c();
                }
                CardInputWidget.this.b(CardInputWidget.this.f6874d.getCardBrand(), CardInputWidget.this.f6876f.hasFocus(), str);
            }
        });
        this.f6874d.setCardNumberCompleteListener(new CardNumberEditText.b() {
            public void a() {
                CardInputWidget.this.b();
                if (CardInputWidget.this.f6873c != null) {
                    CardInputWidget.this.f6873c.a();
                }
            }
        });
        this.f6874d.setCardBrandChangeListener(new CardNumberEditText.a() {
            public void a(String str) {
                boolean unused = CardInputWidget.this.l = "American Express".equals(str);
                CardInputWidget.this.e(str);
                CardInputWidget.this.d(str);
            }
        });
        this.f6877g.setExpiryDateEditListener(new ExpiryDateEditText.a() {
            public void a() {
                CardInputWidget.this.f6876f.requestFocus();
                if (CardInputWidget.this.f6873c != null) {
                    CardInputWidget.this.f6873c.b();
                }
            }
        });
        this.f6874d.requestFocus();
    }

    /* access modifiers changed from: private */
    public void a() {
        if (!this.f6875e && this.m) {
            final int i2 = this.p.f6909d + this.p.f6908c;
            final int i3 = this.p.f6911f + this.p.f6910e + i2;
            a(true);
            final int i4 = ((FrameLayout.LayoutParams) this.f6874d.getLayoutParams()).leftMargin;
            AnonymousClass16 r3 = new Animation() {
                /* access modifiers changed from: protected */
                public void applyTransformation(float f2, Transformation transformation) {
                    super.applyTransformation(f2, transformation);
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) CardInputWidget.this.f6874d.getLayoutParams();
                    layoutParams.leftMargin = (int) (((float) i4) * (1.0f - f2));
                    CardInputWidget.this.f6874d.setLayoutParams(layoutParams);
                }
            };
            final int i5 = this.p.f6906a + this.p.f6909d;
            AnonymousClass2 r4 = new Animation() {
                /* access modifiers changed from: protected */
                public void applyTransformation(float f2, Transformation transformation) {
                    super.applyTransformation(f2, transformation);
                    int i = (int) ((((float) i5) * f2) + ((1.0f - f2) * ((float) i2)));
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) CardInputWidget.this.f6877g.getLayoutParams();
                    layoutParams.leftMargin = i;
                    CardInputWidget.this.f6877g.setLayoutParams(layoutParams);
                }
            };
            final int i6 = (i5 - i2) + i3;
            AnonymousClass3 r1 = new Animation() {
                /* access modifiers changed from: protected */
                public void applyTransformation(float f2, Transformation transformation) {
                    super.applyTransformation(f2, transformation);
                    int i = (int) ((((float) i6) * f2) + ((1.0f - f2) * ((float) i3)));
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) CardInputWidget.this.f6876f.getLayoutParams();
                    layoutParams.leftMargin = i;
                    layoutParams.rightMargin = 0;
                    layoutParams.width = CardInputWidget.this.p.f6912g;
                    CardInputWidget.this.f6876f.setLayoutParams(layoutParams);
                }
            };
            r3.setAnimationListener(new a() {
                public void onAnimationEnd(Animation animation) {
                    CardInputWidget.this.f6874d.requestFocus();
                }
            });
            r3.setDuration(150);
            r4.setDuration(150);
            r1.setDuration(150);
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(r3);
            animationSet.addAnimation(r4);
            animationSet.addAnimation(r1);
            this.f6878h.startAnimation(animationSet);
            this.f6875e = true;
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.f6875e && this.m) {
            final int i2 = this.p.f6906a + this.p.f6909d;
            a(false);
            AnonymousClass5 r1 = new Animation() {
                /* access modifiers changed from: protected */
                public void applyTransformation(float f2, Transformation transformation) {
                    super.applyTransformation(f2, transformation);
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) CardInputWidget.this.f6874d.getLayoutParams();
                    layoutParams.leftMargin = (int) (((float) (CardInputWidget.this.p.f6907b * -1)) * f2);
                    CardInputWidget.this.f6874d.setLayoutParams(layoutParams);
                }
            };
            final int i3 = this.p.f6908c + this.p.f6909d;
            AnonymousClass6 r3 = new Animation() {
                /* access modifiers changed from: protected */
                public void applyTransformation(float f2, Transformation transformation) {
                    super.applyTransformation(f2, transformation);
                    int i = (int) ((((float) i3) * f2) + ((1.0f - f2) * ((float) i2)));
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) CardInputWidget.this.f6877g.getLayoutParams();
                    layoutParams.leftMargin = i;
                    CardInputWidget.this.f6877g.setLayoutParams(layoutParams);
                }
            };
            final int i4 = this.p.f6908c + this.p.f6909d + this.p.f6910e + this.p.f6911f;
            final int i5 = (i2 - i3) + i4;
            AnonymousClass7 r2 = new Animation() {
                /* access modifiers changed from: protected */
                public void applyTransformation(float f2, Transformation transformation) {
                    super.applyTransformation(f2, transformation);
                    int i = (int) ((((float) i4) * f2) + ((1.0f - f2) * ((float) i5)));
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) CardInputWidget.this.f6876f.getLayoutParams();
                    layoutParams.leftMargin = i;
                    layoutParams.rightMargin = 0;
                    layoutParams.width = CardInputWidget.this.p.f6912g;
                    CardInputWidget.this.f6876f.setLayoutParams(layoutParams);
                }
            };
            r1.setDuration(150);
            r3.setDuration(150);
            r2.setDuration(150);
            r1.setAnimationListener(new a() {
                public void onAnimationEnd(Animation animation) {
                    CardInputWidget.this.f6877g.requestFocus();
                }
            });
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(r1);
            animationSet.addAnimation(r3);
            animationSet.addAnimation(r2);
            this.f6878h.startAnimation(animationSet);
            this.f6875e = false;
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            b(false);
        }
    }

    static boolean a(String str, boolean z, String str2) {
        if (!z) {
            return true;
        }
        return b(str, str2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (!this.m && getWidth() != 0) {
            this.m = true;
            this.n = getFrameWidth();
            a(this.f6875e);
            a(this.p.f6906a, this.f6875e ? 0 : this.p.f6907b * -1, this.f6874d);
            a(this.p.f6910e, this.f6875e ? this.p.f6906a + this.p.f6909d : this.p.f6908c + this.p.f6909d, this.f6877g);
            a(this.p.f6912g, this.f6875e ? this.n : this.p.f6908c + this.p.f6909d + this.p.f6910e + this.p.f6911f, this.f6876f);
        }
    }

    /* access modifiers changed from: private */
    public static boolean b(String str, String str2) {
        if (str2 == null) {
            return false;
        }
        if ("American Express".equals(str)) {
            if (str2.length() != 4) {
                return false;
            }
            return true;
        } else if (str2.length() != 3) {
            return false;
        } else {
            return true;
        }
    }

    private String a(String str) {
        if ("American Express".equals(str)) {
            return "3434 343434 ";
        }
        return "4242 4242 4242 ";
    }

    private String b(String str) {
        if ("American Express".equals(str)) {
            return "2345";
        }
        return "CVC";
    }

    private String c(String str) {
        if ("American Express".equals(str)) {
            return "34343";
        }
        if ("Diners Club".equals(str)) {
            return "88";
        }
        return "4242";
    }

    private void b(boolean z) {
        if (z || "Unknown".equals(this.f6874d.getCardBrand())) {
            Drawable g2 = android.support.v4.b.a.a.g(this.f6872b.getDrawable());
            android.support.v4.b.a.a.a(g2.mutate(), this.k);
            this.f6872b.setImageDrawable(android.support.v4.b.a.a.h(g2));
        }
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        if ("American Express".equals(str)) {
            this.f6876f.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
            this.f6876f.setHint(a.f.ga);
            return;
        }
        this.f6876f.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
        this.f6876f.setHint(a.f.gb);
    }

    /* access modifiers changed from: private */
    public void e(String str) {
        if ("Unknown".equals(str)) {
            this.f6872b.setImageDrawable(getResources().getDrawable(a.c.l));
            b(false);
            return;
        }
        this.f6872b.setImageResource(f6871a.get(str).intValue());
    }

    /* access modifiers changed from: private */
    public void b(String str, boolean z, String str2) {
        if (a(str, z, str2)) {
            e(str);
        } else {
            c("American Express".equals(str));
        }
    }

    private void c(boolean z) {
        if (z) {
            this.f6872b.setImageResource(a.c.f6789ic_cvc_amex);
        } else {
            this.f6872b.setImageResource(a.c.f6788ic_cvc);
        }
        b(true);
    }

    private class b implements StripeEditText.b {

        /* renamed from: b  reason: collision with root package name */
        private StripeEditText f6905b;

        b(StripeEditText stripeEditText) {
            this.f6905b = stripeEditText;
        }

        public void a() {
            String obj = this.f6905b.getText().toString();
            if (obj.length() > 1) {
                this.f6905b.setText(obj.substring(0, obj.length() - 1));
            }
            this.f6905b.requestFocus();
            this.f6905b.setSelection(this.f6905b.length());
        }
    }

    class e {

        /* renamed from: a  reason: collision with root package name */
        int f6906a;

        /* renamed from: b  reason: collision with root package name */
        int f6907b;

        /* renamed from: c  reason: collision with root package name */
        int f6908c;

        /* renamed from: d  reason: collision with root package name */
        int f6909d;

        /* renamed from: e  reason: collision with root package name */
        int f6910e;

        /* renamed from: f  reason: collision with root package name */
        int f6911f;

        /* renamed from: g  reason: collision with root package name */
        int f6912g;

        /* renamed from: h  reason: collision with root package name */
        int f6913h;
        int i;
        int j;
        int k;

        e() {
        }

        public String toString() {
            return String.format(Locale.ENGLISH, "CardWidth = %d\nHiddenCardWidth = %d\nPeekCardWidth = %d\nCardDateSeparation = %d\nDateWidth = %d\nDateCvcSeparation = %d\nCvcWidth = %d\n", Integer.valueOf(this.f6906a), Integer.valueOf(this.f6907b), Integer.valueOf(this.f6908c), Integer.valueOf(this.f6909d), Integer.valueOf(this.f6910e), Integer.valueOf(this.f6911f), Integer.valueOf(this.f6912g)) + String.format(Locale.ENGLISH, "Touch Buffer Data:\nCardTouchBufferLimit = %d\nDateStartPosition = %d\nDateRightTouchBufferLimit = %d\nCvcStartPosition = %d", Integer.valueOf(this.f6913h), Integer.valueOf(this.i), Integer.valueOf(this.j), Integer.valueOf(this.k));
        }
    }

    private abstract class a implements Animation.AnimationListener {
        private a() {
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }
}
