package com.stripe.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

public class LockableHorizontalScrollView extends HorizontalScrollView {

    /* renamed from: a  reason: collision with root package name */
    private a f6932a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f6933b;

    interface a {
    }

    public LockableHorizontalScrollView(Context context) {
        super(context);
    }

    public LockableHorizontalScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public LockableHorizontalScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setScrollable(boolean z) {
        this.f6933b = z;
        setSmoothScrollingEnabled(z);
    }

    public void scrollTo(int i, int i2) {
        if (this.f6933b) {
            super.scrollTo(i, i2);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return super.onTouchEvent(motionEvent);
        }
        return this.f6933b && super.onTouchEvent(motionEvent);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.f6933b && super.onInterceptTouchEvent(motionEvent);
    }

    /* access modifiers changed from: package-private */
    public void setScrollChangedListener(a aVar) {
        this.f6932a = aVar;
    }
}
