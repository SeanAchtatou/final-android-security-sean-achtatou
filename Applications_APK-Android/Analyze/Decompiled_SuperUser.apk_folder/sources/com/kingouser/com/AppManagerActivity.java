package com.kingouser.com;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.q;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnPageChange;
import com.kingouser.com.entity.UninstallAppInfo;
import com.kingouser.com.fragment.AppManagerFeagment;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.pureapps.cleaner.util.f;
import java.util.ArrayList;
import java.util.List;
import me.everything.android.ui.overscroll.g;

public class AppManagerActivity extends BaseActivity {
    @BindView(R.id.cw)
    ViewPager mAppViewPager;
    @BindView(R.id.cv)
    TabLayout mTabLayout;
    boolean n = false;
    private Context p;
    private final int q = 0;
    private final int r = 1;
    /* access modifiers changed from: private */
    public AppManagerFeagment s;
    /* access modifiers changed from: private */
    public AppManagerFeagment t;
    private b u = null;
    private Handler v = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (!AppManagerActivity.this.n) {
                switch (message.what) {
                    case 101:
                        AppManagerActivity.this.e(message);
                        return;
                    case 102:
                        AppManagerActivity.this.d(message);
                        return;
                    case 103:
                        AppManagerActivity.this.c(message);
                        return;
                    case 104:
                        AppManagerActivity.this.b(message);
                        return;
                    case 105:
                        AppManagerActivity.this.a(message);
                        return;
                    case 106:
                        AppManagerActivity.this.t.a();
                        AppManagerActivity.this.s.a();
                        return;
                    default:
                        return;
                }
            }
        }
    };

    /* access modifiers changed from: private */
    public void a(Message message) {
        if (message != null) {
            Bundle bundle = (Bundle) message.obj;
            UninstallAppInfo uninstallAppInfo = (UninstallAppInfo) bundle.getSerializable("appinfoone");
            bundle.getBoolean("isOver");
            boolean z = bundle.getBoolean("isSys");
            bundle.getInt("index");
            if (z) {
                this.t.a(uninstallAppInfo);
            } else {
                this.s.a(uninstallAppInfo);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(Message message) {
        this.s.b();
        this.t.b();
    }

    /* access modifiers changed from: private */
    public void c(Message message) {
        if (message != null) {
            Toast.makeText(this.p, this.p.getResources().getString(R.string.d0), 0).show();
        }
    }

    /* access modifiers changed from: private */
    public void d(Message message) {
        if (message != null) {
            boolean booleanValue = ((Boolean) message.obj).booleanValue();
            Toast.makeText(this.p, this.p.getResources().getString(R.string.f3, "" + booleanValue), 0).show();
            if (booleanValue) {
                l();
            }
        }
    }

    /* access modifiers changed from: private */
    public void e(Message message) {
        if (message != null) {
            Toast.makeText(this.p, this.p.getResources().getString(R.string.f3, ((UninstallAppInfo) message.obj).title), 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        o();
        getWindow().getDecorView().setBackgroundColor(-1);
        setContentView((int) R.layout.a2);
        this.p = getApplicationContext();
        m();
        k();
    }

    private void k() {
        l();
        this.s = AppManagerFeagment.a(0, this.p, this);
        this.t = AppManagerFeagment.a(1, this.p, this);
        a aVar = new a(e());
        aVar.a(this.s, this.p.getResources().getString(R.string.c5));
        aVar.a(this.t, this.p.getResources().getString(R.string.c4));
        this.mAppViewPager.setAdapter(aVar);
        this.mTabLayout.setupWithViewPager(this.mAppViewPager);
    }

    private void l() {
        new com.kingouser.com.c.a(this.p, this.v).start();
    }

    private void m() {
        g.a(this.mAppViewPager);
        a((Toolbar) findViewById(R.id.cu));
        ActionBar f2 = f();
        f2.d(true);
        f2.a(true);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.n = false;
        if (this.mAppViewPager != null) {
            f.a("xxxxxx onResume " + this.mAppViewPager.getCurrentItem());
            if (this.mAppViewPager.getCurrentItem() == 0) {
                com.pureapps.cleaner.analytic.a.a(this.p).b(this.o, "FragmentAppManagerUser");
            } else if (this.mAppViewPager.getCurrentItem() == 1) {
                com.pureapps.cleaner.analytic.a.a(this.p).b(this.o, "FragmentAppManagerSystem");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.n = true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        n();
        if (this.mAppViewPager != null) {
            this.mAppViewPager = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    static class a extends FragmentPagerAdapter {

        /* renamed from: a  reason: collision with root package name */
        private final List<Fragment> f3930a = new ArrayList();

        /* renamed from: b  reason: collision with root package name */
        private final List<String> f3931b = new ArrayList();

        public a(q qVar) {
            super(qVar);
        }

        public void a(Fragment fragment, String str) {
            this.f3930a.add(fragment);
            this.f3931b.add(str);
        }

        public Fragment a(int i) {
            return this.f3930a.get(i);
        }

        public int b() {
            return this.f3930a.size();
        }

        public CharSequence c(int i) {
            return this.f3931b.get(i);
        }
    }

    private void n() {
        if (this.u != null) {
            unregisterReceiver(this.u);
        }
    }

    private void o() {
        if (this.u == null) {
            this.u = new b();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addDataScheme(ServiceManagerNative.PACKAGE);
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        registerReceiver(this.u, intentFilter);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        f.a("case onKeyDown:" + System.currentTimeMillis());
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        f.a("case ItemSe:" + System.currentTimeMillis());
        switch (menuItem.getItemId()) {
            case 16908332:
                f.a("caseHome:" + System.currentTimeMillis());
                f.a("casePressed:" + System.currentTimeMillis());
                finish();
                f.a("casefinish:" + System.currentTimeMillis());
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public Handler j() {
        return this.v;
    }

    @OnPageChange({2131624069})
    public void onPageSelected(int i) {
        if (i == 0) {
            com.pureapps.cleaner.analytic.a.a(this.p).b(this.o, "FragmentAppManagerUser");
        } else if (1 == i) {
            com.pureapps.cleaner.analytic.a.a(this.p).b(this.o, "FragmentAppManagerSystem");
        }
    }

    private class b extends BroadcastReceiver {
        private b() {
        }

        public void onReceive(Context context, Intent intent) {
            f.a("UninstallAppReceiver" + intent.getAction());
            if (intent != null && intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
                com.pureapps.cleaner.b.a.a(33, 0, intent.getData().getSchemeSpecificPart());
            }
        }
    }
}
