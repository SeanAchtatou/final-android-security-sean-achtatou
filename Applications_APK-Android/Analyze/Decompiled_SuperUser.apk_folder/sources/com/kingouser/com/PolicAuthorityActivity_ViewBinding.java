package com.kingouser.com;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class PolicAuthorityActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private PolicAuthorityActivity f4007a;

    public PolicAuthorityActivity_ViewBinding(PolicAuthorityActivity policAuthorityActivity, View view) {
        this.f4007a = policAuthorityActivity;
        policAuthorityActivity.listView = (ListView) Utils.findRequiredViewAsType(view, R.id.ea, "field 'listView'", ListView.class);
        policAuthorityActivity.bgImageView = (ImageView) Utils.findRequiredViewAsType(view, R.id.e9, "field 'bgImageView'", ImageView.class);
        policAuthorityActivity.adContainer = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.e_, "field 'adContainer'", FrameLayout.class);
    }

    public void unbind() {
        PolicAuthorityActivity policAuthorityActivity = this.f4007a;
        if (policAuthorityActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4007a = null;
        policAuthorityActivity.listView = null;
        policAuthorityActivity.bgImageView = null;
        policAuthorityActivity.adContainer = null;
    }
}
