package com.kingouser.com;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class PaymentCompleteActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private PaymentCompleteActivity f3983a;

    /* renamed from: b  reason: collision with root package name */
    private View f3984b;

    public PaymentCompleteActivity_ViewBinding(final PaymentCompleteActivity paymentCompleteActivity, View view) {
        this.f3983a = paymentCompleteActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.kv, "method 'onClick'");
        this.f3984b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                paymentCompleteActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        if (this.f3983a == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f3983a = null;
        this.f3984b.setOnClickListener(null);
        this.f3984b = null;
    }
}
