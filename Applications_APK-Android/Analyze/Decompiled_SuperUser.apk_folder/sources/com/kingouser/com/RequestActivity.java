package com.kingouser.com;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.kingouser.com.db.KingoDatabaseHelper;
import com.kingouser.com.db.b;
import com.kingouser.com.entity.UidPolicy;
import com.kingouser.com.util.DeviceInfoUtils;
import com.kingouser.com.util.LanguageUtils;
import com.kingouser.com.util.MySharedPreference;
import com.kingouser.com.util.PermissionUtils;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;

public class RequestActivity extends Activity {
    private static final HashMap<String, Integer> l = new HashMap<String, Integer>() {
        {
            put("command", Integer.valueOf((int) FileUtils.FileMode.MODE_ISUID));
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f4008a;
    @BindView(R.id.lx)
    TextView applicationTitle;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f4009b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public long f4010c;

    /* renamed from: d  reason: collision with root package name */
    private ArrayList<String> f4011d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public boolean f4012e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f4013f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public LocalSocket f4014g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public String f4015h;
    private Activity i;
    @BindView(R.id.lw)
    ImageView imageView;
    private a j;
    /* access modifiers changed from: private */
    public Handler k = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 92:
                    int intValue = ((Integer) message.obj).intValue();
                    RequestActivity.this.progressBar.setProgress(intValue);
                    if (intValue == 100 && !RequestActivity.this.isFinishing()) {
                        RequestActivity.this.finish();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    @BindView(R.id.m3)
    Button mAllow;
    @BindView(R.id.m2)
    Button mDeny;
    @BindView(R.id.d6)
    ProgressBar progressBar;
    @BindView(R.id.lv)
    TextView tvAppTitle;
    @BindView(R.id.lz)
    TextView tvManufacturer;
    @BindView(R.id.m1)
    TextView tvRequestPermission;
    @BindView(R.id.ly)
    TextView tvSecurityLevel;
    @BindView(R.id.ld)
    TextView tvShadow;
    @BindView(R.id.m0)
    TextView tvValue;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.i = this;
        this.f4011d = b.a(this.i, b.f4342a);
        LanguageUtils.getLocalLanguage();
        LanguageUtils.changeLocalLanguage(this.i, MySharedPreference.getAboutActivityLocalLanguage(this.i, ""));
        requestWindowFeature(1);
        setContentView((int) R.layout.cp);
        ButterKnife.bind(this);
        Long l2 = 100L;
        this.f4010c = 1000 / (l2.longValue() / ((long) MySharedPreference.getRequestDialogTimes(getApplicationContext(), 15)));
        if (Build.VERSION.SDK_INT >= 11) {
            new Object() {
                public void a(Activity activity) {
                    activity.setFinishOnTouchOutside(false);
                }
            }.a(this);
        }
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        this.f4015h = intent.getStringExtra("socket");
        if (this.f4015h == null) {
            finish();
            return;
        }
        g();
        d();
        if (this.f4012e) {
            c();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        a();
    }

    private void a() {
        this.progressBar.setMax(100);
        this.progressBar.setProgress(0);
        this.j = new a();
        this.j.f4021a = false;
        this.j.start();
    }

    private class a extends Thread {

        /* renamed from: a  reason: collision with root package name */
        public boolean f4021a;

        private a() {
        }

        public void run() {
            super.run();
            int i = 1;
            while (!this.f4021a && i <= 100) {
                Message message = new Message();
                message.what = 92;
                message.obj = Integer.valueOf(i);
                RequestActivity.this.k.sendMessage(message);
                try {
                    Thread.sleep(RequestActivity.this.f4010c);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                i++;
            }
        }
    }

    private int b() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.RequestActivity.a(boolean, java.lang.Integer):void
     arg types: [int, int]
     candidates:
      com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, int):int
      com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, android.net.LocalSocket):android.net.LocalSocket
      com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, boolean):boolean
      com.kingouser.com.RequestActivity.a(boolean, java.lang.Integer):void */
    /* access modifiers changed from: private */
    public void c() {
        PackageManager packageManager = getPackageManager();
        String[] packagesForUid = packageManager.getPackagesForUid(this.f4008a);
        if (packagesForUid == null || packagesForUid.length <= 0) {
            String string = getString(R.string.bp);
            this.imageView.setImageResource(R.drawable.et);
            this.applicationTitle.setText(this.i.getResources().getString(R.string.cz, string));
            return;
        }
        int length = packagesForUid.length;
        int i2 = 0;
        while (i2 < length) {
            String str = packagesForUid[i2];
            boolean z = KingoDatabaseHelper.a(this, str) == null;
            if (!this.f4011d.contains(str) || !z) {
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo(str, (int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
                    PackageInfo packageInfo2 = packageManager.getPackageInfo(str, 64);
                    if ("Shell".equals((String) packageInfo.applicationInfo.loadLabel(packageManager))) {
                        this.applicationTitle.setText(this.i.getResources().getString(R.string.cz, "ADB Shell"));
                        this.imageView.setImageResource(R.drawable.et);
                    } else {
                        this.applicationTitle.setText(this.i.getResources().getString(R.string.cz, packageInfo.applicationInfo.loadLabel(packageManager)));
                        this.imageView.setImageDrawable(packageInfo.applicationInfo.loadIcon(packageManager));
                    }
                    try {
                        String[] split = ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(packageInfo2.signatures[0].toByteArray()))).getIssuerX500Principal().getName("RFC1779").split(",");
                        String str2 = "";
                        for (String str3 : split) {
                            if (str3.contains("O=")) {
                                str2 = str3.substring(str3.indexOf("=") + 1);
                            }
                        }
                        this.tvValue.setText(" : " + str2);
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                    i2++;
                }
            } else {
                a(true, (Integer) 0);
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.RequestActivity.a(boolean, java.lang.Integer):void
     arg types: [int, int]
     candidates:
      com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, int):int
      com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, android.net.LocalSocket):android.net.LocalSocket
      com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, boolean):boolean
      com.kingouser.com.RequestActivity.a(boolean, java.lang.Integer):void */
    @OnClick({2131624409, 2131624408})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.m2 /*2131624408*/:
                a(false, (Integer) 0);
                return;
            case R.id.m3 /*2131624409*/:
                a(true, (Integer) 0);
                return;
            default:
                return;
        }
    }

    private void d() {
        f();
        e();
    }

    private void e() {
        this.tvAppTitle.setTypeface(Typeface.createFromAsset(this.i.getAssets(), "fonts/Arial.ttf"));
    }

    private void f() {
        this.tvAppTitle.setTextSize(DeviceInfoUtils.getTextSize(this.i, 62));
        this.applicationTitle.setTextSize(DeviceInfoUtils.getTextSize(this.i, 48));
        this.tvSecurityLevel.setTextSize(DeviceInfoUtils.getTextSize(this.i, 36));
        this.tvManufacturer.setTextSize(DeviceInfoUtils.getTextSize(this.i, 36));
        this.tvValue.setTextSize(DeviceInfoUtils.getTextSize(this.i, 36));
        this.tvShadow.setTextSize(DeviceInfoUtils.getTextSize(this.i, 36));
        this.tvRequestPermission.setTextSize(DeviceInfoUtils.getTextSize(this.i, 46));
        this.mAllow.setTextSize(DeviceInfoUtils.getTextSize(this.i, 36));
        this.mDeny.setTextSize(DeviceInfoUtils.getTextSize(this.i, 36));
    }

    /* access modifiers changed from: private */
    public static int b(String str) {
        Integer num = l.get(str);
        if (num == null) {
            return FileUtils.FileMode.MODE_IRUSR;
        }
        return num.intValue();
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z, Integer num) {
        String str;
        this.f4013f = true;
        try {
            this.f4014g.getOutputStream().write((z ? "socket:ALLOW" : "socket:DENY").getBytes());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            if (num.intValue() != -1) {
                UidPolicy uidPolicy = new UidPolicy();
                if (num.intValue() != 0) {
                    uidPolicy.policy = UidPolicy.INTERACTIVE;
                } else {
                    if (z) {
                        str = UidPolicy.ALLOW;
                    } else {
                        str = UidPolicy.DENY;
                    }
                    uidPolicy.policy = str;
                }
                uidPolicy.uid = this.f4008a;
                uidPolicy.command = null;
                uidPolicy.until = num.intValue();
                uidPolicy.desiredUid = this.f4009b;
                KingoDatabaseHelper.a(this, uidPolicy);
                if (z) {
                    if (com.kingouser.com.util.FileUtils.checkFileExist(this.i, this.i.getFilesDir() + "/" + "supersu.cfg")) {
                        PermissionUtils.addApp2Cfg(this.i, uidPolicy.packageName, this.f4008a, 1);
                    } else {
                        PermissionUtils.createPrePermission(this.i, MySharedPreference.getRequestDialogTimes(this.i, 15));
                        PermissionUtils.addApp2Cfg(this.i, uidPolicy.packageName, this.f4008a, 1);
                    }
                } else if (com.kingouser.com.util.FileUtils.checkFileExist(this.i, this.i.getFilesDir() + "/" + "supersu.cfg")) {
                    PermissionUtils.RemoveAppFromCfg(this.i, uidPolicy.packageName);
                } else {
                    PermissionUtils.createPrePermission(this.i, MySharedPreference.getRequestDialogTimes(this.i, 15));
                }
                if (UidPolicy.INTERACTIVE.equalsIgnoreCase(uidPolicy.policy)) {
                    MySharedPreference.setPermissionState(this, true);
                }
            }
        } catch (Exception e3) {
        }
        finish();
    }

    private void g() {
        new Thread() {
            public void run() {
                try {
                    LocalSocket unused = RequestActivity.this.f4014g = new LocalSocket();
                    RequestActivity.this.f4014g.connect(new LocalSocketAddress(RequestActivity.this.f4015h, LocalSocketAddress.Namespace.FILESYSTEM));
                    DataInputStream dataInputStream = new DataInputStream(RequestActivity.this.f4014g.getInputStream());
                    ContentValues contentValues = new ContentValues();
                    for (int i = 0; i < 20; i++) {
                        int readInt = dataInputStream.readInt();
                        if (readInt > 20) {
                            throw new IllegalArgumentException("name length too long: " + readInt);
                        }
                        byte[] bArr = new byte[readInt];
                        dataInputStream.readFully(bArr);
                        String str = new String(bArr);
                        int readInt2 = dataInputStream.readInt();
                        if (readInt2 > RequestActivity.b(str)) {
                            throw new IllegalArgumentException(str + " data length too long: " + readInt2);
                        }
                        byte[] bArr2 = new byte[readInt2];
                        dataInputStream.readFully(bArr2);
                        contentValues.put(str, new String(bArr2));
                        if ("eof".equals(str)) {
                            break;
                        }
                    }
                    int unused2 = RequestActivity.this.f4008a = contentValues.getAsInteger("from.uid").intValue();
                    int unused3 = RequestActivity.this.f4009b = contentValues.getAsByte("to.uid").byteValue();
                    RequestActivity.this.runOnUiThread(new Runnable() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, boolean):boolean
                         arg types: [com.kingouser.com.RequestActivity, int]
                         candidates:
                          com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, int):int
                          com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, android.net.LocalSocket):android.net.LocalSocket
                          com.kingouser.com.RequestActivity.a(boolean, java.lang.Integer):void
                          com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, boolean):boolean */
                        public void run() {
                            boolean unused = RequestActivity.this.f4012e = true;
                            RequestActivity.this.c();
                        }
                    });
                } catch (Exception e2) {
                    e2.printStackTrace();
                    try {
                        RequestActivity.this.f4014g.close();
                    } catch (Exception e3) {
                    }
                    RequestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            RequestActivity.this.finish();
                        }
                    });
                }
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.j.f4021a = true;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        finish();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 || i2 == 3) {
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.RequestActivity.a(boolean, java.lang.Integer):void
     arg types: [int, java.lang.Integer]
     candidates:
      com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, int):int
      com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, android.net.LocalSocket):android.net.LocalSocket
      com.kingouser.com.RequestActivity.a(com.kingouser.com.RequestActivity, boolean):boolean
      com.kingouser.com.RequestActivity.a(boolean, java.lang.Integer):void */
    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.f4013f) {
            a(false, Integer.valueOf(b()));
        }
        try {
            if (this.f4014g != null) {
                this.f4014g.close();
            }
        } catch (Exception e2) {
        }
        new File(this.f4015h).delete();
        Intent intent = new Intent();
        intent.setAction("com.kingouser.com.sqlchange");
        sendBroadcast(intent);
    }
}
