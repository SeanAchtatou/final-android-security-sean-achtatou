package com.kingouser.com;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class RequestActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private RequestActivity f4023a;

    /* renamed from: b  reason: collision with root package name */
    private View f4024b;

    /* renamed from: c  reason: collision with root package name */
    private View f4025c;

    public RequestActivity_ViewBinding(final RequestActivity requestActivity, View view) {
        this.f4023a = requestActivity;
        requestActivity.imageView = (ImageView) Utils.findRequiredViewAsType(view, R.id.lw, "field 'imageView'", ImageView.class);
        requestActivity.applicationTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.lx, "field 'applicationTitle'", TextView.class);
        requestActivity.tvAppTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.lv, "field 'tvAppTitle'", TextView.class);
        requestActivity.tvSecurityLevel = (TextView) Utils.findRequiredViewAsType(view, R.id.ly, "field 'tvSecurityLevel'", TextView.class);
        requestActivity.tvManufacturer = (TextView) Utils.findRequiredViewAsType(view, R.id.lz, "field 'tvManufacturer'", TextView.class);
        requestActivity.tvRequestPermission = (TextView) Utils.findRequiredViewAsType(view, R.id.m1, "field 'tvRequestPermission'", TextView.class);
        requestActivity.tvValue = (TextView) Utils.findRequiredViewAsType(view, R.id.m0, "field 'tvValue'", TextView.class);
        requestActivity.tvShadow = (TextView) Utils.findRequiredViewAsType(view, R.id.ld, "field 'tvShadow'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.m3, "field 'mAllow' and method 'OnClick'");
        requestActivity.mAllow = (Button) Utils.castView(findRequiredView, R.id.m3, "field 'mAllow'", Button.class);
        this.f4024b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                requestActivity.OnClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.m2, "field 'mDeny' and method 'OnClick'");
        requestActivity.mDeny = (Button) Utils.castView(findRequiredView2, R.id.m2, "field 'mDeny'", Button.class);
        this.f4025c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                requestActivity.OnClick(view);
            }
        });
        requestActivity.progressBar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.d6, "field 'progressBar'", ProgressBar.class);
    }

    public void unbind() {
        RequestActivity requestActivity = this.f4023a;
        if (requestActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4023a = null;
        requestActivity.imageView = null;
        requestActivity.applicationTitle = null;
        requestActivity.tvAppTitle = null;
        requestActivity.tvSecurityLevel = null;
        requestActivity.tvManufacturer = null;
        requestActivity.tvRequestPermission = null;
        requestActivity.tvValue = null;
        requestActivity.tvShadow = null;
        requestActivity.mAllow = null;
        requestActivity.mDeny = null;
        requestActivity.progressBar = null;
        this.f4024b.setOnClickListener(null);
        this.f4024b = null;
        this.f4025c.setOnClickListener(null);
        this.f4025c = null;
    }
}
