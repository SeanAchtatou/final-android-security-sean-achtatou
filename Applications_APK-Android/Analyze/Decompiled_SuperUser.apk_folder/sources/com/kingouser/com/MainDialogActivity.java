package com.kingouser.com;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainDialogActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private String f3970a;
    @BindView(R.id.ff)
    Button cancel;
    @BindView(R.id.je)
    Button ok;
    @BindView(R.id.fd)
    TextView tvTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.bs);
        ButterKnife.bind(this);
        b();
        a();
    }

    private void a() {
        this.tvTitle.setText(this.f3970a);
        this.ok.setText((int) R.string.bw);
        this.cancel.setText((int) R.string.bu);
    }

    private void b() {
        this.f3970a = getIntent().getStringExtra("wifi_state");
    }

    @OnClick({2131624163, 2131624310})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ff /*2131624163*/:
                finish();
                return;
            case R.id.je /*2131624310*/:
                setResult(-1);
                Intent intent = new Intent();
                intent.setAction("com.kingouser.com.receiver.startDownloadReceiver");
                sendBroadcast(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 || i == 3) {
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
