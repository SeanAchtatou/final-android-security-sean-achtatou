package com.kingouser.com;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class MainDialogActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private MainDialogActivity f3971a;

    /* renamed from: b  reason: collision with root package name */
    private View f3972b;

    /* renamed from: c  reason: collision with root package name */
    private View f3973c;

    public MainDialogActivity_ViewBinding(final MainDialogActivity mainDialogActivity, View view) {
        this.f3971a = mainDialogActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.je, "field 'ok' and method 'onClick'");
        mainDialogActivity.ok = (Button) Utils.castView(findRequiredView, R.id.je, "field 'ok'", Button.class);
        this.f3972b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                mainDialogActivity.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.ff, "field 'cancel' and method 'onClick'");
        mainDialogActivity.cancel = (Button) Utils.castView(findRequiredView2, R.id.ff, "field 'cancel'", Button.class);
        this.f3973c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                mainDialogActivity.onClick(view);
            }
        });
        mainDialogActivity.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.fd, "field 'tvTitle'", TextView.class);
    }

    public void unbind() {
        MainDialogActivity mainDialogActivity = this.f3971a;
        if (mainDialogActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f3971a = null;
        mainDialogActivity.ok = null;
        mainDialogActivity.cancel = null;
        mainDialogActivity.tvTitle = null;
        this.f3972b.setOnClickListener(null);
        this.f3972b = null;
        this.f3973c.setOnClickListener(null);
        this.f3973c = null;
    }
}
