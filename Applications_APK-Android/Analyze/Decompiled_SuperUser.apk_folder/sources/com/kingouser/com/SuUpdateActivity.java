package com.kingouser.com;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SuUpdateActivity extends Activity {
    @BindView(R.id.lu)
    Button update;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.cq);
        ButterKnife.bind(this);
        a();
    }

    private void a() {
        if (Build.VERSION.SDK_INT >= 11) {
            new Object() {
                public void a(Activity activity) {
                    activity.setFinishOnTouchOutside(false);
                }
            }.a(this);
        }
    }

    @OnClick({2131624400})
    public void OnClick() {
        Intent intent = new Intent();
        intent.setAction("com.kingouser.com.updateloading");
        sendBroadcast(intent);
        finish();
    }

    public static void a(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, SuUpdateActivity.class);
        intent.setFlags(268435456);
        context.startActivity(intent);
    }
}
