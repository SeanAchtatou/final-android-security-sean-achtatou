package com.kingouser.com;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.kingouser.com.entity.DeleteAppItem;

public class UninstallDialogActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private DeleteAppItem f4140a;
    @BindView(R.id.fo)
    ImageView ivNo;
    @BindView(R.id.fp)
    ImageView ivYes;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.cv);
        ButterKnife.bind(this);
        b();
        a();
    }

    private void a() {
    }

    @OnClick({2131624172, 2131624173})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.fo /*2131624172*/:
                finish();
                return;
            case R.id.fp /*2131624173*/:
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("deleteAppItem", this.f4140a);
                intent.putExtras(bundle);
                intent.setAction("com.kingouser.com.uninstallappreceiver");
                sendBroadcast(intent);
                finish();
                return;
            default:
                return;
        }
    }

    private void b() {
        this.f4140a = (DeleteAppItem) getIntent().getSerializableExtra("deleteAppItem");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 || i == 3) {
        }
        return super.onKeyDown(i, keyEvent);
    }
}
