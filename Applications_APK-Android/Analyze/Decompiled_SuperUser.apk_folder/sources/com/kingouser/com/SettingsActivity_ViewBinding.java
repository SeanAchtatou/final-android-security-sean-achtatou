package com.kingouser.com;

import android.content.res.Resources;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.customview.MyDrawbleText;
import com.kingouser.com.customview.MySeekBar;

public class SettingsActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private SettingsActivity f4055a;

    /* renamed from: b  reason: collision with root package name */
    private View f4056b;

    /* renamed from: c  reason: collision with root package name */
    private View f4057c;

    /* renamed from: d  reason: collision with root package name */
    private View f4058d;

    /* renamed from: e  reason: collision with root package name */
    private View f4059e;

    /* renamed from: f  reason: collision with root package name */
    private View f4060f;

    /* renamed from: g  reason: collision with root package name */
    private View f4061g;

    public SettingsActivity_ViewBinding(final SettingsActivity settingsActivity, View view) {
        this.f4055a = settingsActivity;
        settingsActivity.tvToastNotification = (MyDrawbleText) Utils.findRequiredViewAsType(view, R.id.eb, "field 'tvToastNotification'", MyDrawbleText.class);
        settingsActivity.tvPromptTimespan = (MyDrawbleText) Utils.findRequiredViewAsType(view, R.id.eh, "field 'tvPromptTimespan'", MyDrawbleText.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.ek, "field 'tvLanguage' and method 'onClick'");
        settingsActivity.tvLanguage = (MyDrawbleText) Utils.castView(findRequiredView, R.id.ek, "field 'tvLanguage'", MyDrawbleText.class);
        this.f4056b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                settingsActivity.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.el, "field 'tvRemovePermission' and method 'onClick'");
        settingsActivity.tvRemovePermission = (MyDrawbleText) Utils.castView(findRequiredView2, R.id.el, "field 'tvRemovePermission'", MyDrawbleText.class);
        this.f4057c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                settingsActivity.onClick(view);
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.eg, "field 'tvNotificationTool' and method 'onClick'");
        settingsActivity.tvNotificationTool = (MyDrawbleText) Utils.castView(findRequiredView3, R.id.eg, "field 'tvNotificationTool'", MyDrawbleText.class);
        this.f4058d = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                settingsActivity.onClick(view);
            }
        });
        settingsActivity.tvSwipeText = (MyDrawbleText) Utils.findRequiredViewAsType(view, R.id.ed, "field 'tvSwipeText'", MyDrawbleText.class);
        settingsActivity.tv_time = (TextView) Utils.findRequiredViewAsType(view, R.id.ei, "field 'tv_time'", TextView.class);
        View findRequiredView4 = Utils.findRequiredView(view, R.id.ec, "field 'ivToast' and method 'OnCheckedChange'");
        settingsActivity.ivToast = (SwitchCompat) Utils.castView(findRequiredView4, R.id.ec, "field 'ivToast'", SwitchCompat.class);
        this.f4059e = findRequiredView4;
        ((CompoundButton) findRequiredView4).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                settingsActivity.OnCheckedChange(compoundButton, z);
            }
        });
        View findRequiredView5 = Utils.findRequiredView(view, R.id.ee, "field 'ivSwipe' and method 'OnCheckedChange'");
        settingsActivity.ivSwipe = (SwitchCompat) Utils.castView(findRequiredView5, R.id.ee, "field 'ivSwipe'", SwitchCompat.class);
        this.f4060f = findRequiredView5;
        ((CompoundButton) findRequiredView5).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                settingsActivity.OnCheckedChange(compoundButton, z);
            }
        });
        View findRequiredView6 = Utils.findRequiredView(view, R.id.ef, "field 'ivWeather' and method 'OnCheckedChange'");
        settingsActivity.ivWeather = (SwitchCompat) Utils.castView(findRequiredView6, R.id.ef, "field 'ivWeather'", SwitchCompat.class);
        this.f4061g = findRequiredView6;
        ((CompoundButton) findRequiredView6).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                settingsActivity.OnCheckedChange(compoundButton, z);
            }
        });
        settingsActivity.mySeeekBar = (MySeekBar) Utils.findRequiredViewAsType(view, R.id.ej, "field 'mySeeekBar'", MySeekBar.class);
        settingsActivity.mScrollview = (ScrollView) Utils.findRequiredViewAsType(view, R.id.co, "field 'mScrollview'", ScrollView.class);
        Resources resources = view.getContext().getResources();
        settingsActivity.drawbleRightWidth = resources.getDimensionPixelSize(R.dimen.f8285d);
        settingsActivity.drawbleRightHeight = resources.getDimensionPixelSize(R.dimen.f8285d);
        settingsActivity.drawbleBottomWidth = resources.getDimensionPixelSize(R.dimen.f8284c);
        settingsActivity.bgWidth = resources.getDimensionPixelSize(R.dimen.f8284c);
        settingsActivity.bgHeight = resources.getDimensionPixelSize(R.dimen.f8283b);
        settingsActivity.rightMargin = resources.getDimensionPixelSize(R.dimen.f8286e);
    }

    public void unbind() {
        SettingsActivity settingsActivity = this.f4055a;
        if (settingsActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4055a = null;
        settingsActivity.tvToastNotification = null;
        settingsActivity.tvPromptTimespan = null;
        settingsActivity.tvLanguage = null;
        settingsActivity.tvRemovePermission = null;
        settingsActivity.tvNotificationTool = null;
        settingsActivity.tvSwipeText = null;
        settingsActivity.tv_time = null;
        settingsActivity.ivToast = null;
        settingsActivity.ivSwipe = null;
        settingsActivity.ivWeather = null;
        settingsActivity.mySeeekBar = null;
        settingsActivity.mScrollview = null;
        this.f4056b.setOnClickListener(null);
        this.f4056b = null;
        this.f4057c.setOnClickListener(null);
        this.f4057c = null;
        this.f4058d.setOnClickListener(null);
        this.f4058d = null;
        ((CompoundButton) this.f4059e).setOnCheckedChangeListener(null);
        this.f4059e = null;
        ((CompoundButton) this.f4060f).setOnCheckedChangeListener(null);
        this.f4060f = null;
        ((CompoundButton) this.f4061g).setOnCheckedChangeListener(null);
        this.f4061g = null;
    }
}
