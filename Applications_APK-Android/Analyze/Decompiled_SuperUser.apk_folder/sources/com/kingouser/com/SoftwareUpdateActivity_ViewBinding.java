package com.kingouser.com;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class SoftwareUpdateActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private SoftwareUpdateActivity f4105a;

    /* renamed from: b  reason: collision with root package name */
    private View f4106b;

    /* renamed from: c  reason: collision with root package name */
    private View f4107c;

    public SoftwareUpdateActivity_ViewBinding(final SoftwareUpdateActivity softwareUpdateActivity, View view) {
        this.f4105a = softwareUpdateActivity;
        softwareUpdateActivity.tvContent = (TextView) Utils.findRequiredViewAsType(view, R.id.gp, "field 'tvContent'", TextView.class);
        softwareUpdateActivity.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.fd, "field 'tvTitle'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.ff, "field 'btCancel' and method 'OnClick'");
        softwareUpdateActivity.btCancel = (Button) Utils.castView(findRequiredView, R.id.ff, "field 'btCancel'", Button.class);
        this.f4106b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                softwareUpdateActivity.OnClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.lu, "field 'btUpdate' and method 'OnClick'");
        softwareUpdateActivity.btUpdate = (Button) Utils.castView(findRequiredView2, R.id.lu, "field 'btUpdate'", Button.class);
        this.f4107c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                softwareUpdateActivity.OnClick(view);
            }
        });
    }

    public void unbind() {
        SoftwareUpdateActivity softwareUpdateActivity = this.f4105a;
        if (softwareUpdateActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4105a = null;
        softwareUpdateActivity.tvContent = null;
        softwareUpdateActivity.tvTitle = null;
        softwareUpdateActivity.btCancel = null;
        softwareUpdateActivity.btUpdate = null;
        this.f4106b.setOnClickListener(null);
        this.f4106b = null;
        this.f4107c.setOnClickListener(null);
        this.f4107c = null;
    }
}
