package com.kingouser.com.c;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.kingouser.com.entity.DeleteAppItem;
import com.kingouser.com.entity.UninstallAppInfo;
import com.kingouser.com.util.HttpUtils;
import com.lody.virtual.helper.utils.FileUtils;
import com.pureapps.cleaner.util.f;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NewGetAppListThread */
public class a extends Thread {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f4240a;

    /* renamed from: b  reason: collision with root package name */
    public static boolean f4241b;

    /* renamed from: f  reason: collision with root package name */
    private static ArrayList<String> f4242f = new ArrayList<>();

    /* renamed from: c  reason: collision with root package name */
    private Context f4243c;

    /* renamed from: d  reason: collision with root package name */
    private Handler f4244d;

    /* renamed from: e  reason: collision with root package name */
    private ArrayList<DeleteAppItem> f4245e = new ArrayList<>();

    static {
        f4242f.add("kingoroot.supersu");
        f4242f.add("com.kingouser.com");
    }

    public a(Context context, Handler handler) {
        f4240a = false;
        f4241b = false;
        this.f4243c = context;
        this.f4244d = handler;
    }

    public void run() {
        List<PackageInfo> list;
        PackageManager packageManager;
        boolean z;
        super.run();
        f.a("NewGetAppListThread Run.........：" + System.currentTimeMillis());
        this.f4245e.clear();
        PackageManager packageManager2 = null;
        try {
            packageManager2 = this.f4243c.getPackageManager();
            list = packageManager2.getInstalledPackages(0);
            packageManager = packageManager2;
        } catch (Exception e2) {
            try {
                packageManager2 = this.f4243c.getPackageManager();
                list = packageManager2.getInstalledPackages(0);
                packageManager = packageManager2;
            } catch (Exception e3) {
                e3.printStackTrace();
                list = null;
                packageManager = packageManager2;
            }
        }
        long currentTimeMillis = System.currentTimeMillis();
        f.a("bianliwanbi getInstalledPackages over:\t" + currentTimeMillis + "\tsize:" + list.size());
        if (list.size() > 0) {
            this.f4244d.sendEmptyMessage(104);
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (list != null && list.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= list.size()) {
                    break;
                }
                PackageInfo packageInfo = list.get(i2);
                String str = packageInfo.packageName;
                if (i2 == list.size() - 1) {
                    z = true;
                } else {
                    z = false;
                }
                if (!a(str)) {
                    boolean z2 = false;
                    if ((packageInfo.applicationInfo.flags & FileUtils.FileMode.MODE_IWUSR) != 0) {
                        z2 = true;
                    } else if ((packageInfo.applicationInfo.flags & 1) == 0) {
                        z2 = true;
                    }
                    DeleteAppItem a2 = a(packageInfo, packageManager);
                    if (a(packageInfo) == 0) {
                        f.a("packageInfo is 0byte : | " + packageInfo.packageName);
                    } else {
                        if (z2) {
                            this.f4245e.add(a2);
                        }
                        a(b(packageInfo, packageManager), b(packageInfo), z, i2);
                    }
                }
                i = i2 + 1;
            }
            this.f4244d.sendEmptyMessage(106);
            f.a("bianliwanbi : | " + arrayList.size());
            f.a("bianliwanbi : | " + arrayList2.size());
            f.a("bianliwanbi : | " + (System.currentTimeMillis() - currentTimeMillis));
        }
        HttpUtils.saveAppItemInfo(this.f4243c, this.f4245e, "list");
    }

    private DeleteAppItem a(PackageInfo packageInfo, PackageManager packageManager) {
        DeleteAppItem deleteAppItem = new DeleteAppItem();
        deleteAppItem.setAppPackage(packageInfo.packageName);
        deleteAppItem.setAppName(packageInfo.applicationInfo.loadLabel(packageManager).toString());
        deleteAppItem.setVersionName(packageInfo.versionName);
        deleteAppItem.setDataDir(packageInfo.applicationInfo.dataDir);
        if (Build.VERSION.SDK_INT >= 9) {
            deleteAppItem.setNativeLibraryDir(packageInfo.applicationInfo.nativeLibraryDir);
        } else {
            deleteAppItem.setNativeLibraryDir("");
        }
        return deleteAppItem;
    }

    private void a(UninstallAppInfo uninstallAppInfo, boolean z, boolean z2, int i) {
        Message obtainMessage = this.f4244d.obtainMessage(105);
        if (obtainMessage == null) {
            obtainMessage = new Message();
            obtainMessage.what = 105;
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable("appinfoone", uninstallAppInfo);
        bundle.putBoolean("isSys", z);
        bundle.putBoolean("isOver", z2);
        bundle.putInt("index", i);
        obtainMessage.obj = bundle;
        this.f4244d.sendMessage(obtainMessage);
    }

    private boolean a(String str) {
        if (!f4242f.contains(str) && !str.equals("system") && !str.equals("com.android.phone")) {
            return false;
        }
        return true;
    }

    private UninstallAppInfo b(PackageInfo packageInfo, PackageManager packageManager) {
        UninstallAppInfo uninstallAppInfo = new UninstallAppInfo();
        uninstallAppInfo.checked = false;
        uninstallAppInfo.pkg = packageInfo.packageName;
        uninstallAppInfo.icon = packageInfo.applicationInfo.loadIcon(packageManager);
        uninstallAppInfo.title = packageInfo.applicationInfo.loadLabel(packageManager).toString();
        uninstallAppInfo.size = a(packageInfo);
        uninstallAppInfo.datadir = packageInfo.applicationInfo.dataDir;
        uninstallAppInfo.sourceDir = packageInfo.applicationInfo.sourceDir;
        if (Build.VERSION.SDK_INT >= 9) {
            uninstallAppInfo.nativeLibraryDir = packageInfo.applicationInfo.nativeLibraryDir;
        } else {
            uninstallAppInfo.nativeLibraryDir = "";
        }
        return uninstallAppInfo;
    }

    private long a(PackageInfo packageInfo) {
        String str = packageInfo.applicationInfo.sourceDir;
        if (str != null) {
            return new File(str).length();
        }
        return 0;
    }

    private boolean b(PackageInfo packageInfo) {
        return (packageInfo.applicationInfo.flags & 1) > 0;
    }
}
