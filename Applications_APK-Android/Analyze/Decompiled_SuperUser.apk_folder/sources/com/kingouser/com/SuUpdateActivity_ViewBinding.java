package com.kingouser.com;

import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class SuUpdateActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private SuUpdateActivity f4126a;

    /* renamed from: b  reason: collision with root package name */
    private View f4127b;

    public SuUpdateActivity_ViewBinding(final SuUpdateActivity suUpdateActivity, View view) {
        this.f4126a = suUpdateActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.lu, "field 'update' and method 'OnClick'");
        suUpdateActivity.update = (Button) Utils.castView(findRequiredView, R.id.lu, "field 'update'", Button.class);
        this.f4127b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                suUpdateActivity.OnClick();
            }
        });
    }

    public void unbind() {
        SuUpdateActivity suUpdateActivity = this.f4126a;
        if (suUpdateActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4126a = null;
        suUpdateActivity.update = null;
        this.f4127b.setOnClickListener(null);
        this.f4127b = null;
    }
}
