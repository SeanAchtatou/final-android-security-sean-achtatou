package com.kingouser.com;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.util.AuthorizeUtils;
import com.pureapps.cleaner.analytic.a;
import com.pureapps.cleaner.manager.g;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.view.ShadowLayout;
import com.squareup.okhttp.u;
import com.stripe.android.Stripe;
import com.stripe.android.a.b;
import com.stripe.android.a.j;

public class StripeActivity extends BaseActivity {
    @BindView(R.id.em)
    ProgressBar mProgressBar;
    @BindView(R.id.en)
    LinearLayout mRootLinearLayout;
    @BindView(R.id.eq)
    EditText mStripeCardCvc;
    @BindView(R.id.ep)
    EditText mStripeCardMonthAndYears;
    @BindView(R.id.eo)
    EditText mStripeCardNum;
    @BindView(R.id.er)
    ShadowLayout mSubmitButton;
    AuthorizeUtils.CARDBIND n = null;
    private Context p;
    private String q = "";
    /* access modifiers changed from: private */
    public String r = "";
    /* access modifiers changed from: private */
    public String s = "";
    /* access modifiers changed from: private */
    public String t = "";
    private Double u = Double.valueOf(0.0d);
    private boolean v = true;
    /* access modifiers changed from: private */
    public Handler w = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 114:
                    StripeActivity.this.o();
                    return;
                case 115:
                    StripeActivity.this.n();
                    return;
                case 116:
                    StripeActivity.this.l();
                    return;
                case 117:
                    StripeActivity.this.m();
                    return;
                case 118:
                    StripeActivity.this.k();
                    return;
                case 119:
                    StripeActivity.this.j();
                    return;
                default:
                    return;
            }
        }
    };

    /* access modifiers changed from: private */
    public void j() {
        if (this.mStripeCardMonthAndYears != null) {
            this.mStripeCardMonthAndYears.setText(this.mStripeCardMonthAndYears.getText().append((CharSequence) "/"));
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        Drawable drawable;
        if (this.mStripeCardNum != null) {
            if (this.n == AuthorizeUtils.CARDBIND.Maestro) {
                drawable = getResources().getDrawable(R.drawable.cm);
            } else if (this.n == AuthorizeUtils.CARDBIND.Mastercard) {
                drawable = getResources().getDrawable(R.drawable.cn);
            } else {
                drawable = this.n == AuthorizeUtils.CARDBIND.Visa ? getResources().getDrawable(R.drawable.co) : null;
            }
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            this.mStripeCardNum.setCompoundDrawables(null, null, drawable, null);
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        Intent intent = new Intent();
        intent.putExtra("stripe_result_state", 0);
        intent.putExtra("stripe_result_auth", "");
        setResult(-1, intent);
        finish();
    }

    /* access modifiers changed from: private */
    public void m() {
        Intent intent = new Intent();
        intent.putExtra("stripe_result_state", 1);
        intent.putExtra("stripe_result_auth", this.s);
        setResult(-1, intent);
        finish();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || this.v) {
            return super.onKeyDown(i, keyEvent);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void n() {
        g.a().a(this.p, AuthorizeUtils.getInstance().getStripePayByToken(this.p, this.r, this.q, this.u), new g.a() {
            public void onBack(u uVar) {
                if (uVar != null) {
                    AuthorizeUtils.StripInfo parseStripeInfo = AuthorizeUtils.getInstance().parseStripeInfo(uVar.g());
                    if (parseStripeInfo == null || TextUtils.isEmpty(parseStripeInfo.auth)) {
                        StripeActivity.this.w.sendEmptyMessage(116);
                        return;
                    }
                    f.a("stripePayByToken:" + parseStripeInfo.toString());
                    String unused = StripeActivity.this.s = parseStripeInfo.auth;
                    String unused2 = StripeActivity.this.t = "1";
                    StripeActivity.this.w.sendEmptyMessage(117);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void o() {
        l();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Bundle extras;
        super.onCreate(bundle);
        setContentView((int) R.layout.ad);
        ActionBar f2 = f();
        f.a("supportActionBar is null " + (f2 == null));
        if (f2 != null) {
            f2.a(true);
        }
        this.p = getApplicationContext();
        Intent intent = getIntent();
        if (!(intent == null || (extras = intent.getExtras()) == null)) {
            Double valueOf = Double.valueOf(extras.getDouble("stripe_publick_amount"));
            if (valueOf.doubleValue() > 0.0d) {
                this.u = valueOf;
            }
            String string = extras.getString("stripe_publick_key");
            if (!TextUtils.isEmpty(string)) {
                this.q = string;
            }
        }
        f.a("mPayAmount:" + this.u);
        f.a("mPublickKey:" + this.q);
        p();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.v = true;
        a.a(this).b(this.o, "Stripe");
    }

    private void p() {
        this.mStripeCardNum.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
                StripeActivity.this.a(editable.toString());
            }
        });
        this.mStripeCardMonthAndYears.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                f.a("beforeTextChanged: s:" + charSequence.toString() + "\tstart:" + i + "\tcount:" + i2 + "\tafter:" + i3);
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                f.a("onTextChanged:s:" + charSequence.toString() + "\tstart:" + i + "\tbefore:" + i2 + "\tcount:" + i3);
            }

            public void afterTextChanged(Editable editable) {
                f.a("afterTextChanged:" + editable.toString());
                editable.toString();
            }
        });
        f.a("");
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            AuthorizeUtils.CARDBIND cardBIN = AuthorizeUtils.getInstance().getCardBIN(str);
            f.a("cardbind:" + cardBIN);
            if (cardBIN != null && this.n != cardBIN) {
                this.n = cardBIN;
                this.w.sendEmptyMessage(118);
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    @OnClick({2131624138})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.er /*2131624138*/:
                q();
                return;
            default:
                return;
        }
    }

    private void q() {
        String str;
        Integer num;
        Integer num2;
        String[] strArr;
        String str2 = null;
        f.a("check card num....");
        if (this.mStripeCardNum != null) {
            str = this.mStripeCardNum.getText().toString();
            if (TextUtils.isEmpty(str) || str.length() < 15) {
                f.a("CARDNUM IS ERROE");
                a((View) this.mStripeCardNum);
                return;
            }
        } else {
            str = null;
        }
        if (this.mStripeCardMonthAndYears != null) {
            String obj = this.mStripeCardMonthAndYears.getText().toString();
            if (TextUtils.isEmpty(obj)) {
                f.a("cardmonth IS erroe");
                a((View) this.mStripeCardMonthAndYears);
                return;
            }
            if (obj.contains("/")) {
                strArr = obj.split("/");
            } else if (obj.length() != 4) {
                a((View) this.mStripeCardMonthAndYears);
                return;
            } else {
                strArr = new String[]{obj.substring(0, 2), obj.substring(2, 4)};
            }
            if (strArr == null || strArr.length != 2) {
                f.a("cardmonth IS split erroe");
                a((View) this.mStripeCardMonthAndYears);
                return;
            }
            try {
                int parseInt = Integer.parseInt(strArr[0]);
                int parseInt2 = Integer.parseInt(strArr[1]);
                if (parseInt > 12 || parseInt2 < 16) {
                    f.a("cardmonth IS parseInt erroe");
                    a((View) this.mStripeCardMonthAndYears);
                    return;
                }
                num2 = Integer.valueOf(parseInt);
                num = Integer.valueOf(parseInt2);
            } catch (Exception e2) {
                f.a("cardmonth IS erroe" + e2.toString());
                a((View) this.mStripeCardMonthAndYears);
                return;
            }
        } else {
            num = null;
            num2 = null;
        }
        if (this.mStripeCardCvc != null) {
            str2 = this.mStripeCardCvc.getText().toString();
            if (TextUtils.isEmpty(str2)) {
                f.a("cardCVC IS erroe");
                a((View) this.mStripeCardCvc);
                return;
            }
        }
        b bVar = new b(str, num2, num, str2);
        if (!bVar.a()) {
            f.b("validateCard fale");
            a((View) this.mSubmitButton);
        } else if (TextUtils.isEmpty(this.q)) {
            f.b("mPublickKey fale");
        } else {
            s();
            this.v = false;
            r();
            f.b("start request token :" + this.q + " | " + this.u);
            final long currentTimeMillis = System.currentTimeMillis();
            a.a(this).c(FirebaseAnalytics.getInstance(this), "BtnStripePageRequestTokenClick");
            new Stripe(this, this.q).a(bVar, new com.stripe.android.b() {
                public void a(Exception exc) {
                    f.b("onError:" + exc);
                    f.b("onError:time;" + (System.currentTimeMillis() - currentTimeMillis));
                    a.a(StripeActivity.this).c(FirebaseAnalytics.getInstance(StripeActivity.this), "BtnStripePageRequestTokenFailClick");
                    StripeActivity.this.t();
                    String unused = StripeActivity.this.r = null;
                    if (StripeActivity.this.w != null) {
                        a.a(StripeActivity.this).c(FirebaseAnalytics.getInstance(StripeActivity.this), "BtnStripePageRequestTokenFailClick");
                        StripeActivity.this.w.sendEmptyMessage(114);
                    }
                }

                public void a(j jVar) {
                    f.b("onSuccess:" + jVar.a());
                    String a2 = jVar.a();
                    if (!TextUtils.isEmpty(a2)) {
                        String unused = StripeActivity.this.r = a2;
                        f.a("onSuccess:time;" + (System.currentTimeMillis() - currentTimeMillis));
                        if (StripeActivity.this.w != null) {
                            a.a(StripeActivity.this).c(FirebaseAnalytics.getInstance(StripeActivity.this), "BtnStripePageRequestTokenSuccessClick");
                            StripeActivity.this.w.sendEmptyMessage(115);
                            return;
                        }
                        return;
                    }
                    String unused2 = StripeActivity.this.r = null;
                    if (StripeActivity.this.w != null) {
                        a.a(StripeActivity.this).c(FirebaseAnalytics.getInstance(StripeActivity.this), "BtnStripePageRequestTokenFailClick");
                        StripeActivity.this.w.sendEmptyMessage(114);
                    }
                }
            });
        }
    }

    private void a(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.x));
    }

    private void r() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        boolean isActive = inputMethodManager.isActive();
        f.a("isOpen:" + isActive);
        if (isActive) {
            inputMethodManager.hideSoftInputFromWindow(this.mStripeCardCvc.getWindowToken(), 0);
        }
    }

    private void s() {
        if (this.mProgressBar != null) {
            this.mProgressBar.setVisibility(0);
        }
        if (this.mRootLinearLayout != null) {
            this.mRootLinearLayout.setVisibility(4);
            this.mRootLinearLayout.setEnabled(false);
            this.mRootLinearLayout.setFocusable(false);
        }
    }

    /* access modifiers changed from: private */
    public void t() {
        if (this.mProgressBar != null) {
            this.mProgressBar.setVisibility(8);
        }
        if (this.mRootLinearLayout != null) {
            this.mRootLinearLayout.setVisibility(0);
            this.mRootLinearLayout.setEnabled(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        f.a("onDestroy");
    }
}
