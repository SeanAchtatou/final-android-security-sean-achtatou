package com.kingouser.com;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.kingouser.com.util.MySharedPreference;

public class SuUpdatingActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Animation f4130a;

    /* renamed from: b  reason: collision with root package name */
    private a f4131b = new a();
    @BindView(R.id.m4)
    ImageView ivLoading;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.cr);
        ButterKnife.bind(this);
        a();
        b();
    }

    private void a() {
        this.f4130a = AnimationUtils.loadAnimation(this, R.anim.v);
    }

    private void b() {
        this.ivLoading.startAnimation(this.f4130a);
        if (Build.VERSION.SDK_INT >= 11) {
            new Object() {
                public void a(Activity activity) {
                    activity.setFinishOnTouchOutside(false);
                }
            }.a(this);
        }
    }

    public static void a(Context context) {
        if (MySharedPreference.getWheaterOnResume(context, false)) {
            Intent intent = new Intent();
            intent.setClass(context, SuUpdatingActivity.class);
            intent.setFlags(268435456);
            context.startActivity(intent);
        }
    }

    private class a extends BroadcastReceiver {
        private a() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("com.kingouser.com.finishloading".equalsIgnoreCase(intent.getAction())) {
                SuUpdatingActivity.this.finish();
            }
        }
    }

    private void b(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.kingouser.com.finishloading");
        context.registerReceiver(this.f4131b, intentFilter);
    }

    private void c(Context context) {
        context.unregisterReceiver(this.f4131b);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        b(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            c(this);
        } catch (Exception e2) {
        }
        this.ivLoading.clearAnimation();
    }
}
