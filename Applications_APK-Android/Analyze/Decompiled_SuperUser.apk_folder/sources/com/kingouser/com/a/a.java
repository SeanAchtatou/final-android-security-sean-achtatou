package com.kingouser.com.a;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/* compiled from: ExpandCollapseAnimation */
public class a extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private View f4161a;

    /* renamed from: b  reason: collision with root package name */
    private int f4162b = this.f4161a.getMeasuredHeight();

    /* renamed from: c  reason: collision with root package name */
    private int f4163c;

    /* renamed from: d  reason: collision with root package name */
    private LinearLayout.LayoutParams f4164d;

    public a(View view, int i) {
        this.f4161a = view;
        this.f4164d = (LinearLayout.LayoutParams) view.getLayoutParams();
        this.f4163c = i;
        if (this.f4163c == 0) {
            this.f4164d.bottomMargin = -this.f4162b;
        } else {
            this.f4164d.bottomMargin = 0;
        }
        view.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        super.applyTransformation(f2, transformation);
        if (f2 < 1.0f) {
            if (this.f4163c == 0) {
                this.f4164d.bottomMargin = (-this.f4162b) + ((int) (((float) this.f4162b) * f2));
            } else {
                this.f4164d.bottomMargin = -((int) (((float) this.f4162b) * f2));
            }
            this.f4161a.requestLayout();
        } else if (this.f4163c == 0) {
            this.f4164d.bottomMargin = 0;
            this.f4161a.requestLayout();
        } else {
            this.f4164d.bottomMargin = -this.f4162b;
            this.f4161a.setVisibility(8);
            this.f4161a.requestLayout();
        }
    }
}
