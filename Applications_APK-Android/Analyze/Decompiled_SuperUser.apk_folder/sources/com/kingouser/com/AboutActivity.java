package com.kingouser.com;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.OnClick;
import com.kingouser.com.customview.MyDrawbleText;
import com.kingouser.com.util.NetworkUtils;
import com.kingouser.com.util.PackageUtils;
import com.kingouser.com.util.RC4EncodeUtils;
import com.pureapps.cleaner.analytic.a;
import com.pureapps.cleaner.util.f;
import me.everything.android.ui.overscroll.g;

public class AboutActivity extends BaseActivity {
    @BindView(R.id.cr)
    MyDrawbleText about_version;
    @BindDimen(R.dimen.f8283b)
    int bgHeight;
    @BindDimen(R.dimen.f8284c)
    int bgWidth;
    @BindDimen(R.dimen.f8284c)
    int drawbleBottomWidth;
    @BindDimen(R.dimen.f8285d)
    int drawbleRightHeight;
    @BindDimen(R.dimen.f8285d)
    int drawbleRightWidth;
    @BindView(R.id.cp)
    MyDrawbleText face_book;
    @BindView(R.id.co)
    ScrollView mScrollview;
    int n = 1;
    private Context p;
    @BindDimen(R.dimen.f8286e)
    int rightMargin;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.a1);
        g.a(this.mScrollview);
        ActionBar f2 = f();
        f.a("supportActionBar is null " + (f2 == null));
        if (f2 != null) {
            f2.a(true);
        }
        this.p = getApplicationContext();
        a(this.face_book, this.drawbleRightWidth, this.drawbleRightHeight, this.drawbleBottomWidth, this.n, this.bgWidth, this.bgHeight, this.rightMargin);
        this.about_version.a();
        j();
    }

    public boolean g() {
        finish();
        return super.g();
    }

    private void j() {
        String appVersion = PackageUtils.getAppVersion(this.p);
        if (this.about_version != null) {
            String k = k();
            this.about_version.setText(this.p.getResources().getString(R.string.ae, appVersion + k));
        }
    }

    private String k() {
        if (TextUtils.isEmpty("OffcialSite")) {
            return "N";
        }
        if ("PCKingoRoot".equalsIgnoreCase("OffcialSite")) {
            return "P";
        }
        if ("MobileKingoRoot".equalsIgnoreCase("OffcialSite")) {
            return "M";
        }
        if ("OfficialSite".equalsIgnoreCase("OffcialSite") || "OffcialSite".equalsIgnoreCase("OffcialSite")) {
            return "W";
        }
        if ("TestChannel".equalsIgnoreCase("OffcialSite")) {
            return "T";
        }
        return "N";
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a.a(this).b(this.o, "About");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick({2131624063, 2131624062, 2131624065})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cp /*2131624062*/:
                m();
                return;
            case R.id.cq /*2131624063*/:
                n();
                return;
            case R.id.cr /*2131624064*/:
            default:
                return;
            case R.id.cs /*2131624065*/:
                l();
                return;
        }
    }

    private void l() {
        Intent intent = new Intent();
        intent.setClass(this.p, PaypalActivity.class);
        intent.setFlags(268435456);
        this.p.startActivity(intent);
    }

    private void m() {
        a.a(this).c(this.o, "BtnAboutFacebookClick");
        NetworkUtils.openURL(RC4EncodeUtils.decry_RC4("c9b07620b6ff1214690ac544aa08ff8001ca8feb00dd5dd2db5414a1a7221acc04d7", "string_key"), this.p);
        finish();
    }

    private void n() {
        a.a(this).c(this.o, "BtnAboutPrivacypolicyClick");
        NetworkUtils.openURL(RC4EncodeUtils.decry_RC4("c9b07620ffea12507713d505bf1cec8011d093e55c9051d099100dbda93b0edc18d5b66fc98fbd90a195a911", "string_key"), this.p);
        finish();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void a(MyDrawbleText myDrawbleText, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        myDrawbleText.setRightDrawbleId(R.drawable.ex);
        myDrawbleText.setBgHeight(i6);
        myDrawbleText.setDrawbleRightWidth(i);
        myDrawbleText.setDrawbleRightHeight(i2);
        myDrawbleText.setDrawbleBottomWidth(i3);
        myDrawbleText.setDrawbleBottomHegith(i4);
        myDrawbleText.setRightMargin(i7);
        myDrawbleText.a();
    }
}
