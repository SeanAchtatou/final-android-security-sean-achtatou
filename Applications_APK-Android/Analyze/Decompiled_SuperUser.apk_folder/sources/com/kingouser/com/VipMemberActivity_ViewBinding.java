package com.kingouser.com;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.airbnb.lottie.LottieAnimationView;

public class VipMemberActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private VipMemberActivity f4149a;

    public VipMemberActivity_ViewBinding(VipMemberActivity vipMemberActivity, View view) {
        this.f4149a = vipMemberActivity;
        vipMemberActivity.tv_vipmembership = (TextView) Utils.findRequiredViewAsType(view, R.id.ex, "field 'tv_vipmembership'", TextView.class);
        vipMemberActivity.anim_view = (LottieAnimationView) Utils.findRequiredViewAsType(view, R.id.ew, "field 'anim_view'", LottieAnimationView.class);
    }

    public void unbind() {
        VipMemberActivity vipMemberActivity = this.f4149a;
        if (vipMemberActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4149a = null;
        vipMemberActivity.tv_vipmembership = null;
        vipMemberActivity.anim_view = null;
    }
}
