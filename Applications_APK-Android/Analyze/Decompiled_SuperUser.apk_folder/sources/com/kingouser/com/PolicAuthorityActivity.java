package com.kingouser.com;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import butterknife.BindView;
import com.duapps.ad.DuNativeAd;
import com.kingouser.com.adapter.PolicyAdatper;
import com.kingouser.com.application.App;
import com.kingouser.com.db.KingoDatabaseHelper;
import com.kingouser.com.entity.UidPolicy;
import com.kingouser.com.receiver.SqlChangedReceiver;
import com.kingouser.com.util.DeviceInfoUtils;
import com.kingouser.com.util.Helper;
import com.kingouser.com.util.HttpUtils;
import com.kingouser.com.util.MyLog;
import com.pureapps.cleaner.manager.a;
import com.pureapps.cleaner.util.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

public class PolicAuthorityActivity extends BaseActivity {
    private SqlChangedReceiver A = new SqlChangedReceiver() {
        public void onReceive(Context context, Intent intent) {
            PolicAuthorityActivity.this.k();
        }
    };
    @BindView(R.id.e_)
    FrameLayout adContainer;
    @BindView(R.id.e9)
    ImageView bgImageView;
    @BindView(R.id.ea)
    ListView listView;
    ArrayList<UidPolicy> n = new ArrayList<>();
    /* access modifiers changed from: private */
    public Context p;
    /* access modifiers changed from: private */
    public PolicyAdatper q;
    private int r;
    /* access modifiers changed from: private */
    public float s = 4.0f;
    private IntentFilter t;
    /* access modifiers changed from: private */
    public ArrayList<UidPolicy> u = new ArrayList<>();
    /* access modifiers changed from: private */
    public TreeSet<Integer> v = new TreeSet<>();
    /* access modifiers changed from: private */
    public ArrayList<UidPolicy> w = new ArrayList<>();
    /* access modifiers changed from: private */
    public TreeSet<Integer> x = new TreeSet<>();
    private a y = new a();
    /* access modifiers changed from: private */
    public Handler z = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 72:
                    if (PolicAuthorityActivity.this.p != null) {
                        PolicAuthorityActivity.this.w.clear();
                        PolicAuthorityActivity.this.x.clear();
                        PolicAuthorityActivity.this.q.notifyDataSetChanged();
                        PolicAuthorityActivity.this.w.addAll(PolicAuthorityActivity.this.u);
                        PolicAuthorityActivity.this.x.addAll(PolicAuthorityActivity.this.v);
                        MyLog.e("TAG", "长度是。。。。。。。。。。。。。。。。。" + PolicAuthorityActivity.this.n.size());
                        HttpUtils.weatherShowGcAd(PolicAuthorityActivity.this.p, this, PolicAuthorityActivity.this.n.size());
                        PolicAuthorityActivity.this.q.notifyDataSetChanged();
                        if (PolicAuthorityActivity.this.w.size() >= 1) {
                            PolicAuthorityActivity.this.bgImageView.setVisibility(8);
                            PolicAuthorityActivity.this.listView.setVisibility(0);
                            return;
                        }
                        PolicAuthorityActivity.this.bgImageView.setVisibility(0);
                        PolicAuthorityActivity.this.listView.setVisibility(8);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.p = getApplicationContext();
        setContentView((int) R.layout.ab);
        ActionBar f2 = f();
        if (f2 != null) {
            f2.a(true);
        }
        l();
        n();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.pureapps.cleaner.analytic.a.a(this).b(this.o, "PoliceAuthority");
    }

    private void j() {
        float f2 = getResources().getDisplayMetrics().density;
        this.adContainer.setBackgroundColor(getResources().getColor(R.color.cy));
        this.adContainer.removeAllViews();
        com.pureapps.cleaner.manager.a.a().a(10, this.adContainer, new a.C0089a() {
            public void a(DuNativeAd duNativeAd) {
                f.a("initAdByManager onSuccess");
                PolicAuthorityActivity.this.adContainer.setVisibility(0);
                com.pureapps.cleaner.util.a.a(PolicAuthorityActivity.this.adContainer, true, 400, App.a().getResources().getDisplayMetrics().heightPixels / 4);
            }

            public void a() {
                f.a("initAdByManager error");
                PolicAuthorityActivity.this.adContainer.removeAllViews();
            }

            public void b() {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        q();
        p();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            k();
        } catch (Exception e2) {
        }
        o();
        this.listView.setAdapter((ListAdapter) this.q);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(str, context, attributeSet);
    }

    private class a extends BroadcastReceiver {
        private a() {
        }

        public void onReceive(Context context, Intent intent) {
            PolicAuthorityActivity.this.k();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        new Thread(new Runnable() {
            public void run() {
                MyLog.e("PermissionService", "加载授权界面数据。。。。。。。。。。。。。。。。。。。");
                ArrayList<UidPolicy> a2 = KingoDatabaseHelper.a(PolicAuthorityActivity.this.p);
                PolicAuthorityActivity.this.n.clear();
                PolicAuthorityActivity.this.n.addAll(a2);
                PolicAuthorityActivity.this.u.clear();
                PolicAuthorityActivity.this.v.clear();
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                Iterator<UidPolicy> it = a2.iterator();
                while (it.hasNext()) {
                    UidPolicy next = it.next();
                    Drawable loadPackageIcon = Helper.loadPackageIcon(PolicAuthorityActivity.this.p, next.packageName);
                    if (loadPackageIcon != null) {
                        next.drawable = loadPackageIcon;
                    } else if (!TextUtils.isEmpty(next.name)) {
                        next.Icon = R.drawable.et;
                    } else {
                        next.Icon = R.drawable.et;
                    }
                    if (next.until != 0) {
                        arrayList2.add(next);
                    } else if (R.string.al == next.getPolicyResource()) {
                        next.allow = true;
                        arrayList.add(next);
                    } else {
                        arrayList3.add(next);
                    }
                }
                if (arrayList.size() > 0) {
                    UidPolicy uidPolicy = new UidPolicy();
                    uidPolicy.label = PolicAuthorityActivity.this.p.getResources().getString(R.string.am);
                    PolicAuthorityActivity.this.u.add(uidPolicy);
                    PolicAuthorityActivity.this.u.addAll(arrayList);
                    PolicAuthorityActivity.this.v.add(Integer.valueOf((PolicAuthorityActivity.this.u.size() - arrayList.size()) - 1));
                }
                if (arrayList3.size() > 0) {
                    UidPolicy uidPolicy2 = new UidPolicy();
                    uidPolicy2.label = PolicAuthorityActivity.this.p.getResources().getString(R.string.bs);
                    PolicAuthorityActivity.this.u.add(uidPolicy2);
                    PolicAuthorityActivity.this.u.addAll(arrayList3);
                    PolicAuthorityActivity.this.v.add(Integer.valueOf((PolicAuthorityActivity.this.u.size() - arrayList3.size()) - 1));
                }
                if (arrayList2.size() > 0) {
                    UidPolicy uidPolicy3 = new UidPolicy();
                    uidPolicy3.label = PolicAuthorityActivity.this.p.getResources().getString(R.string.b3);
                    PolicAuthorityActivity.this.u.add(uidPolicy3);
                    PolicAuthorityActivity.this.u.addAll(arrayList2);
                    PolicAuthorityActivity.this.v.add(Integer.valueOf((PolicAuthorityActivity.this.u.size() - arrayList2.size()) - 1));
                }
                Message message = new Message();
                message.what = 72;
                PolicAuthorityActivity.this.z.sendMessage(message);
            }
        }).start();
    }

    private void l() {
        this.r = DeviceInfoUtils.getDeviceWidth(this.p);
        this.q = new PolicyAdatper(this.p, this.w, this.x);
        m();
        j();
    }

    private void m() {
        this.t = new IntentFilter("com.kingouser.com.sqlchange");
        this.p.registerReceiver(this.A, this.t);
    }

    private void n() {
        if (Build.VERSION.SDK_INT >= 11) {
            new Object() {
                public void a(Activity activity) {
                    PolicAuthorityActivity.this.listView.setFriction(ViewConfiguration.getScrollFriction() * PolicAuthorityActivity.this.s);
                }
            }.a(this);
        }
    }

    private void o() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.kingouser.com.reload.policies");
        this.p.registerReceiver(this.y, intentFilter);
    }

    private void p() {
        this.p.unregisterReceiver(this.y);
    }

    private void q() {
        this.p.unregisterReceiver(this.A);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
