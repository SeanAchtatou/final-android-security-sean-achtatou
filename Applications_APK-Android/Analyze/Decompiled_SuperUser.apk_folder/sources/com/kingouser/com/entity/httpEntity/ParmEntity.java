package com.kingouser.com.entity.httpEntity;

import java.io.Serializable;

public class ParmEntity implements Serializable {
    String action;
    String key;
    int perm_list_nums;

    public String getKey() {
        return this.key;
    }

    public void setKey(String str) {
        this.key = str;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String str) {
        this.action = str;
    }

    public int getPerm_list_nums() {
        return this.perm_list_nums;
    }

    public void setPerm_list_nums(int i) {
        this.perm_list_nums = i;
    }
}
