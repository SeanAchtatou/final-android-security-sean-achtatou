package com.kingouser.com.entity.rootEntity;

public class PackageInfo {
    public String codePath;
    public String name;
    public String nativeLibraryPath;

    public String toString() {
        try {
            return "PackageInfo{name='" + this.name + '\'' + ", codePath='" + this.codePath + '\'' + ", nativeLibraryPath='" + this.nativeLibraryPath + '\'' + '}';
        } catch (Exception e2) {
            return "";
        }
    }
}
