package com.kingouser.com.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class RequestsEntity implements Serializable {
    private ArrayList<RequestEntity> requestEntities;

    public ArrayList<RequestEntity> getRequestEntities() {
        return this.requestEntities;
    }

    public void setRequestEntities(ArrayList<RequestEntity> arrayList) {
        this.requestEntities = arrayList;
    }
}
