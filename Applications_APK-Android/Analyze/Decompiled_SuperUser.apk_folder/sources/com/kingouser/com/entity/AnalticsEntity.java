package com.kingouser.com.entity;

import java.io.Serializable;
import java.util.List;

public class AnalticsEntity implements Serializable {
    private List<String[]> action;
    private String android_version;
    private String app_version;
    private String id;
    private String model;
    private String pid;
    private String request_time;

    public String getId() {
        return this.id;
    }

    public void setId(String str) {
        this.id = str;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String str) {
        this.model = str;
    }

    public String getAndroid_version() {
        return this.android_version;
    }

    public void setAndroid_version(String str) {
        this.android_version = str;
    }

    public String getPid() {
        return this.pid;
    }

    public void setPid(String str) {
        this.pid = str;
    }

    public String getApp_version() {
        return this.app_version;
    }

    public void setApp_version(String str) {
        this.app_version = str;
    }

    public String getRequest_time() {
        return this.request_time;
    }

    public void setRequest_time(String str) {
        this.request_time = str;
    }

    public List<String[]> getAction() {
        return this.action;
    }

    public void setAction(List<String[]> list) {
        this.action = list;
    }
}
