package com.kingouser.com.entity;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.kingouser.com.R;
import com.kingouser.com.db.UidCommand;
import java.util.Date;

public class UidPolicy extends UidCommand {
    public static final String ALLOW = "allow";
    public static final String DENY = "deny";
    public static final String INTERACTIVE = "interactive";
    public int Icon;
    public boolean allow;
    public Drawable drawable;
    private View fbNativeAdView;
    public String label;
    public boolean logging = true;
    public String nameText;
    public boolean notification = true;
    public String policy;
    public int textId;
    public int type = 75;
    public int until;

    public Date getUntilDate() {
        return new Date(((long) this.until) * 1000);
    }

    public View getFbNativeAdView() {
        return this.fbNativeAdView;
    }

    public void setFbNativeAdView(View view) {
        this.fbNativeAdView = view;
    }

    public int getPolicyResource() {
        if (ALLOW.equals(this.policy)) {
            return R.string.al;
        }
        if (INTERACTIVE.equals(this.policy)) {
            return R.string.cb;
        }
        return R.string.br;
    }
}
