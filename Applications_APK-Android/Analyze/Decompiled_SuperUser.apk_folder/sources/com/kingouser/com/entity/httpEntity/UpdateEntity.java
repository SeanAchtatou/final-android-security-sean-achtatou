package com.kingouser.com.entity.httpEntity;

public class UpdateEntity {
    public String msg;
    public int status;
    public upgrade upgrade = new upgrade();

    public class upgrade {
        public String channel;
        public String clientversion;
        public String crc32;
        public String downloadurl;
        public String iconurl;
        public int id;
        public String key_msg;
        public String langagestr;
        public String md5;
        public String packagename;
        public String releasenode;
        public String signature;
        public int version;
        public String versionname;

        public upgrade() {
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("msg='" + this.msg);
        if (this.upgrade != null) {
            sb.append("clientversion='" + this.upgrade.clientversion);
            sb.append("signature='" + this.upgrade.signature);
            sb.append("channel='" + this.upgrade.channel);
            sb.append("downloadurl='" + this.upgrade.downloadurl);
            sb.append("releasenode='" + this.upgrade.releasenode);
            sb.append("version='" + this.upgrade.version);
            sb.append("iconurl='" + this.upgrade.iconurl);
            sb.append("md5='" + this.upgrade.md5);
            sb.append("crc32='" + this.upgrade.crc32);
            sb.append("langagestr='" + this.upgrade.langagestr);
            sb.append("id='" + this.upgrade.id);
        }
        return sb.toString();
    }
}
