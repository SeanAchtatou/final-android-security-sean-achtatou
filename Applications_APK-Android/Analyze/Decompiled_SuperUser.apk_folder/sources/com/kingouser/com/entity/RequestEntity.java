package com.kingouser.com.entity;

import java.io.Serializable;

public class RequestEntity implements Serializable {
    private String packageName;
    private int requestNum;
    private long requestTime;

    public long getRequestTime() {
        return this.requestTime;
    }

    public void setRequestTime(long j) {
        this.requestTime = j;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String str) {
        this.packageName = str;
    }

    public int getRequestNum() {
        return this.requestNum;
    }

    public void setRequestNum(int i) {
        this.requestNum = i;
    }
}
