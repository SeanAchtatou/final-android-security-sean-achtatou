package com.kingouser.com.entity;

import com.google.gson.annotations.Expose;
import java.io.Serializable;

public class RecommandEntity implements Serializable {
    @Expose
    private String app_icon;
    @Expose
    private String app_name;
    @Expose
    private String app_short_desc;
    @Expose
    private String download_url;
    @Expose
    private String package_id;

    public String getPackage_id() {
        return this.package_id;
    }

    public void setPackage_id(String str) {
        this.package_id = str;
    }

    public String getApp_short_desc() {
        return this.app_short_desc;
    }

    public void setApp_short_desc(String str) {
        this.app_short_desc = str;
    }

    public String getApp_name() {
        return this.app_name;
    }

    public void setApp_name(String str) {
        this.app_name = str;
    }

    public String getDownload_url() {
        return this.download_url;
    }

    public void setDownload_url(String str) {
        this.download_url = str;
    }

    public String getApp_icon() {
        return this.app_icon;
    }

    public void setApp_icon(String str) {
        this.app_icon = str;
    }
}
