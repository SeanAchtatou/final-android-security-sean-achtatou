package com.kingouser.com.entity;

import java.io.Serializable;

public class EnableUninstallEntity implements Serializable {
    private boolean apus_ads;
    private boolean enable_uninstall;

    public boolean isEnable_uninstall() {
        return this.enable_uninstall;
    }

    public void setEnable_uninstall(boolean z) {
        this.enable_uninstall = z;
    }

    public void setApus_ads(boolean z) {
        this.apus_ads = z;
    }

    public boolean isApus_ads() {
        return this.apus_ads;
    }
}
