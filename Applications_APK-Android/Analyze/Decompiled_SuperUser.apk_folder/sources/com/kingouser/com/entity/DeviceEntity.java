package com.kingouser.com.entity;

import android.os.Build;
import com.google.gson.annotations.SerializedName;
import com.kingouser.com.util.PackageUtils;
import io.fabric.sdk.android.services.b.b;

public class DeviceEntity {
    private String action;
    @SerializedName("android-version")
    private String android_version;
    @SerializedName("android-sdk-code")
    private int apiVersion = Build.VERSION.SDK_INT;
    @SerializedName("board_platform")
    private String boardPlatform;
    @SerializedName("client-version")
    private int clientVersion;
    @SerializedName("context-package-id")
    private String contextPackageId;
    @SerializedName("cpu-abi1")
    private String cpu_abi1;
    @SerializedName("device-id")
    private String device_id;
    @SerializedName("display-version")
    private String display_version;
    @SerializedName("exploit-category")
    private String exploitCategory;
    @SerializedName("exploit-name")
    private String exploitName;
    @SerializedName("hardware")
    private String hardWare = PackageUtils.getCpuInfo();
    @SerializedName("kernel-version")
    private String kernel_version;
    @SerializedName("manufacturer")
    private String manufacturer;
    @SerializedName("model-id")
    private String model_id;
    @SerializedName("new-model-key-md5")
    private String model_key_md5;
    @SerializedName("exploit-result")
    private String result;
    @SerializedName("token")
    private String token;

    public String getExploitCategory() {
        return this.exploitCategory;
    }

    public void setExploitCategory(String str) {
        this.exploitCategory = str;
    }

    public String getHardWare() {
        return this.hardWare;
    }

    public void setHardWare(String str) {
        this.hardWare = str;
    }

    public String getBoardPlatform() {
        return this.boardPlatform;
    }

    public void setBoardPlatform(String str) {
        this.boardPlatform = str;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String str) {
        this.token = str;
    }

    public String getContextPackageId() {
        return this.contextPackageId;
    }

    public void setContextPackageId(String str) {
        this.contextPackageId = str;
    }

    public int getClientVersion() {
        return this.clientVersion;
    }

    public void setClientVersion(int i) {
        this.clientVersion = i;
    }

    public String getExploitName() {
        return this.exploitName;
    }

    public void setExploitName(String str) {
        this.exploitName = str;
    }

    public String getKernel_version() {
        return this.kernel_version;
    }

    public void setKernel_version(String str) {
        this.kernel_version = str;
    }

    public String getCpu_abi1() {
        return this.cpu_abi1;
    }

    public void setCpu_abi1(String str) {
        this.cpu_abi1 = str;
    }

    public String getModel_key_md5() {
        return this.model_key_md5;
    }

    public void setModel_key_md5(String str) {
        this.model_key_md5 = str;
    }

    public String getResult() {
        return this.result;
    }

    public void setResult(String str) {
        this.result = str;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String str) {
        this.action = str;
    }

    public String getAndroid_version() {
        return this.android_version;
    }

    public void setAndroid_version(String str) {
        this.android_version = str;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    public String getModel_id() {
        return this.model_id;
    }

    public void setModel_id(String str) {
        this.model_id = str;
    }

    public String getDisplay_version() {
        return this.display_version;
    }

    public void setDisplay_version(String str) {
        this.display_version = str;
    }

    public String toString() {
        return "DeviceEntity{manufacturer='" + this.manufacturer + '\'' + ", model_id='" + this.model_id + '\'' + ", android_version='" + this.android_version + '\'' + ", display_version='" + this.display_version + '\'' + ", device_id='" + this.device_id + '\'' + ", kernel_version='" + this.kernel_version + '\'' + ", cpu_abi1='" + this.cpu_abi1 + '\'' + ", model_key_md5='" + this.model_key_md5 + '\'' + ", action='" + this.action + '\'' + ", result='" + this.result + '\'' + ", exploitName='" + this.exploitName + '\'' + ", clientVersion=" + this.clientVersion + ", token='" + this.token + '\'' + ", contextPackageId='" + this.contextPackageId + '\'' + ", hardWare='" + this.hardWare + '\'' + ", boardPlatform='" + this.boardPlatform + '\'' + ", apiVersion=" + this.apiVersion + ", exploitCategory='" + this.exploitCategory + '\'' + '}';
    }

    public String getDevice_id() {
        return this.device_id;
    }

    public String getModelKey() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.manufacturer.toUpperCase());
        stringBuffer.append(b.ROLL_OVER_FILE_NAME_SEPARATOR);
        stringBuffer.append(this.model_id);
        stringBuffer.append(b.ROLL_OVER_FILE_NAME_SEPARATOR);
        stringBuffer.append(this.android_version);
        stringBuffer.append(b.ROLL_OVER_FILE_NAME_SEPARATOR);
        stringBuffer.append(this.display_version);
        stringBuffer.append(b.ROLL_OVER_FILE_NAME_SEPARATOR);
        stringBuffer.append(PackageUtils.getKernelVersion());
        return stringBuffer.toString();
    }

    public void setDevice_id(String str) {
        this.device_id = str;
    }

    public static DeviceEntity getDeviceInfo() {
        DeviceEntity deviceEntity = new DeviceEntity();
        deviceEntity.setDisplay_version(Build.DISPLAY);
        deviceEntity.setManufacturer(Build.MANUFACTURER);
        deviceEntity.setModel_id(Build.MODEL);
        deviceEntity.setDevice_id(Build.DEVICE);
        deviceEntity.setAndroid_version(Build.VERSION.RELEASE);
        String phoneInfo = PackageUtils.getPhoneInfo("getprop ro.board.platform");
        if (phoneInfo == null || "".equals(phoneInfo)) {
            phoneInfo = PackageUtils.getPhoneInfo("getprop ro.mediatek.platform");
        }
        deviceEntity.setBoardPlatform(phoneInfo);
        deviceEntity.setCpu_abi1(PackageUtils.getPhoneInfo("getprop ro.product.cpu.abi"));
        deviceEntity.setKernel_version(PackageUtils.getKernelVersion());
        return deviceEntity;
    }
}
