package com.kingouser.com.entity;

import java.io.Serializable;

public class WeatherShowGcAd implements Serializable {
    private boolean show;
    private int when_quantity_gt;

    public int getWhen_quantity_gt() {
        return this.when_quantity_gt;
    }

    public void setWhen_quantity_gt(int i) {
        this.when_quantity_gt = i;
    }

    public boolean isShow() {
        return this.show;
    }

    public void setShow(boolean z) {
        this.show = z;
    }
}
