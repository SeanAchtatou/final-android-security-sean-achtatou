package com.kingouser.com.entity.httpEntity;

import java.io.Serializable;

public class ResultEntity implements Serializable {
    private boolean success;

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean z) {
        this.success = z;
    }
}
