package com.kingouser.com.entity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PlugEntity {
    public String action;
    public String apkname;
    public String channel;
    public String download = "";
    public String fileMd5;
    public String iconurl;
    public int id;
    public String main;
    public int operation;
    public String packagename;
    public String signMd5;
    public int status = 1;
    public int versioncode;

    public PlugEntity(String str, int i) {
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.has("plugins")) {
                    JSONArray jSONArray = jSONObject.getJSONArray("plugins");
                    int i2 = 0;
                    while (jSONArray != null && i2 < jSONArray.length()) {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                        if (jSONObject2.has("operation") && jSONObject2.getInt("operation") == i) {
                            this.id = jSONObject2.getInt("id");
                            this.action = jSONObject2.getString("action");
                            this.main = jSONObject2.getString("main");
                            this.packagename = jSONObject2.getString("packagename");
                            this.versioncode = jSONObject2.getInt("versioncode");
                            this.download = jSONObject2.getString("download");
                            this.apkname = jSONObject2.getString("apkname");
                            this.operation = jSONObject2.getInt("operation");
                            this.iconurl = jSONObject2.getString("iconurl");
                            this.signMd5 = jSONObject2.getString("signMd5");
                            this.fileMd5 = jSONObject2.getString("fileMd5");
                            this.channel = jSONObject2.getString("channel");
                            this.status = jSONObject2.getInt("status");
                        }
                        i2++;
                    }
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }
}
