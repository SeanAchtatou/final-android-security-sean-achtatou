package com.kingouser.com.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class AppsEntity implements Serializable {
    private String android_version;
    private ArrayList<AppEntity> apps;
    private String manufacturer;
    private String model_id;
    private String user_id;

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    public String getModel_id() {
        return this.model_id;
    }

    public void setModel_id(String str) {
        this.model_id = str;
    }

    public String getAndroid_version() {
        return this.android_version;
    }

    public void setAndroid_version(String str) {
        this.android_version = str;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String str) {
        this.user_id = str;
    }

    public ArrayList<AppEntity> getApps() {
        return this.apps;
    }

    public void setApps(ArrayList<AppEntity> arrayList) {
        this.apps = arrayList;
    }
}
