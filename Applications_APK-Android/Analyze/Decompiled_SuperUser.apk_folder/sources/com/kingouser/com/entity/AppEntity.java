package com.kingouser.com.entity;

import java.io.Serializable;

public class AppEntity implements Serializable {
    private String package_id;

    public String getPackage_id() {
        return this.package_id;
    }

    public void setPackage_id(String str) {
        this.package_id = str;
    }
}
