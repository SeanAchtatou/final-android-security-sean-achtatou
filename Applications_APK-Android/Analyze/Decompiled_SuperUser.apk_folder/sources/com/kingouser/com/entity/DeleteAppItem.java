package com.kingouser.com.entity;

import android.content.pm.ApplicationInfo;
import java.io.Serializable;

public class DeleteAppItem implements Serializable {
    private int appCode;
    private String appName;
    private String appPackage;
    private String appVersion;
    private int appVersionCode;
    private ApplicationInfo applicationInfo;
    private long cacheSize;
    public boolean checked;
    private String codePath;
    private long codeSize;
    private String dataDir;
    private String filePath;
    private boolean install;
    private long installTime;
    private boolean isChecked;
    private int location;
    private String nativeLibraryDir;
    private String odexPath;
    private boolean systemApp;
    private String versionName;

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String str) {
        this.appName = str;
    }

    public String getAppPackage() {
        return this.appPackage;
    }

    public void setAppPackage(String str) {
        this.appPackage = str;
    }

    public String getCodePath() {
        return this.codePath;
    }

    public void setCodePath(String str) {
        this.codePath = str;
    }

    public String getOdexPath() {
        return this.odexPath;
    }

    public void setOdexPath(String str) {
        this.odexPath = str;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public void setVersionName(String str) {
        this.versionName = str;
    }

    public long getCodeSize() {
        return this.codeSize;
    }

    public void setCodeSize(long j) {
        this.codeSize = j;
    }

    public int getAppCode() {
        return this.appCode;
    }

    public void setAppCode(int i) {
        this.appCode = i;
    }

    public long getCacheSize() {
        return this.cacheSize;
    }

    public void setCacheSize(long j) {
        this.cacheSize = j;
    }

    public String getAppVersion() {
        return this.appVersion;
    }

    public void setAppVersion(String str) {
        this.appVersion = str;
    }

    public int getAppVersionCode() {
        return this.appVersionCode;
    }

    public void setAppVersionCode(int i) {
        this.appVersionCode = i;
    }

    public boolean isChecked() {
        return this.isChecked;
    }

    public void setChecked(boolean z) {
        this.isChecked = z;
    }

    public int getLocation() {
        return this.location;
    }

    public void setLocation(int i) {
        this.location = i;
    }

    public String getFilePath() {
        return this.filePath;
    }

    public void setFilePath(String str) {
        this.filePath = str;
    }

    public ApplicationInfo getApplicationInfo() {
        return this.applicationInfo;
    }

    public void setApplicationInfo(ApplicationInfo applicationInfo2) {
        this.applicationInfo = applicationInfo2;
    }

    public boolean isSystemApp() {
        return this.systemApp;
    }

    public void setSystemApp(boolean z) {
        this.systemApp = z;
    }

    public long getInstallTime() {
        return this.installTime;
    }

    public void setInstallTime(long j) {
        this.installTime = j;
    }

    public boolean isInstall() {
        return this.install;
    }

    public void setInstall(boolean z) {
        this.install = z;
    }

    public String getNativeLibraryDir() {
        return this.nativeLibraryDir;
    }

    public void setNativeLibraryDir(String str) {
        this.nativeLibraryDir = str;
    }

    public String getDataDir() {
        return this.dataDir;
    }

    public void setDataDir(String str) {
        this.dataDir = str;
    }
}
