package com.kingouser.com.entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.pureapps.cleaner.db.b;
import com.pureapps.cleaner.db.c;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class UninstallAppInfo extends b implements Serializable {
    public static final String COLUMN_PKG = "pkg";
    public static final String[] CONTENT_PROJECTION = {b.RECORD_ID, COLUMN_PKG};
    public static final Uri CONTENT_URI = Uri.parse("content://com.kingouser.com.database/uninstalllist");
    public boolean checked;
    public String datadir;
    public Drawable icon;
    public String nativeLibraryDir;
    public String pkg;
    public long size;
    public String sourceDir;
    public String title;

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PKG, this.pkg);
        return contentValues;
    }

    public UninstallAppInfo restore(Cursor cursor) {
        this.mId = cursor.getLong(0);
        this.pkg = cursor.getString(1);
        return this;
    }

    public String[] getContentProjection() {
        return CONTENT_PROJECTION;
    }

    public static ArrayList<UninstallAppInfo> getAll(Context context) {
        Cursor cursor;
        ArrayList<UninstallAppInfo> arrayList = new ArrayList<>();
        try {
            cursor = context.getContentResolver().query(CONTENT_URI, null, null, null, null);
            while (cursor.moveToNext()) {
                try {
                    UninstallAppInfo uninstallAppInfo = new UninstallAppInfo();
                    uninstallAppInfo.restore(cursor);
                    arrayList.add(uninstallAppInfo);
                } catch (Exception e2) {
                    e = e2;
                    try {
                        e.printStackTrace();
                        c.a(cursor);
                        return arrayList;
                    } catch (Throwable th) {
                        th = th;
                        c.a(cursor);
                        throw th;
                    }
                }
            }
            c.a(cursor);
        } catch (Exception e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            c.a(cursor);
            throw th;
        }
        return arrayList;
    }

    public static void addItems(Context context, ArrayList<UninstallAppInfo> arrayList) {
        try {
            Iterator<UninstallAppInfo> it = arrayList.iterator();
            while (it.hasNext()) {
                context.getContentResolver().insert(CONTENT_URI, it.next().toContentValues());
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void removeItems(Context context, ArrayList<UninstallAppInfo> arrayList) {
        try {
            Iterator<UninstallAppInfo> it = arrayList.iterator();
            while (it.hasNext()) {
                context.getContentResolver().delete(CONTENT_URI, "pkg = ? ", new String[]{it.next().pkg});
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
