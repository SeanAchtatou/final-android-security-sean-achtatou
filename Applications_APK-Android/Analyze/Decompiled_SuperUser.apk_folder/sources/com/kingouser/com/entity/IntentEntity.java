package com.kingouser.com.entity;

import java.io.Serializable;

public class IntentEntity implements Serializable {
    private String packageName;
    private long requestTime;
    private int suCode;
    private int suFromuid;
    private int suTouid;

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String str) {
        this.packageName = str;
    }

    public int getSuCode() {
        return this.suCode;
    }

    public void setSuCode(int i) {
        this.suCode = i;
    }

    public int getSuFromuid() {
        return this.suFromuid;
    }

    public void setSuFromuid(int i) {
        this.suFromuid = i;
    }

    public int getSuTouid() {
        return this.suTouid;
    }

    public void setSuTouid(int i) {
        this.suTouid = i;
    }

    public long getRequestTime() {
        return this.requestTime;
    }

    public void setRequestTime(long j) {
        this.requestTime = j;
    }
}
