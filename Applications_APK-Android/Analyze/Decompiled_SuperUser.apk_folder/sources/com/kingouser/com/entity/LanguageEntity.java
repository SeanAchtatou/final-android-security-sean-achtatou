package com.kingouser.com.entity;

import java.io.Serializable;

public class LanguageEntity implements Serializable {
    private String language;
    private String languageCode;

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String str) {
        this.language = str;
    }

    public String getLanguageCode() {
        return this.languageCode;
    }

    public void setLanguageCode(String str) {
        this.languageCode = str;
    }
}
