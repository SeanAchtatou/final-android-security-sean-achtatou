package com.kingouser.com.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class DeleteJsonEntity implements Serializable {
    private String android_version;
    private String app_version;
    private ArrayList<ArrayList<String>> data;
    private String device_id;
    private String display_version;
    private String id;
    private String manufacturer;
    private String model_id;

    public String getId() {
        return this.id;
    }

    public void setId(String str) {
        this.id = str;
    }

    public String getApp_version() {
        return this.app_version;
    }

    public void setApp_version(String str) {
        this.app_version = str;
    }

    public String getModel_id() {
        return this.model_id;
    }

    public void setModel_id(String str) {
        this.model_id = str;
    }

    public String getDevice_id() {
        return this.device_id;
    }

    public void setDevice_id(String str) {
        this.device_id = str;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    public String getAndroid_version() {
        return this.android_version;
    }

    public void setAndroid_version(String str) {
        this.android_version = str;
    }

    public String getDisplay_version() {
        return this.display_version;
    }

    public void setDisplay_version(String str) {
        this.display_version = str;
    }

    public ArrayList<ArrayList<String>> getData() {
        return this.data;
    }

    public void setData(ArrayList<ArrayList<String>> arrayList) {
        this.data = arrayList;
    }
}
