package com.kingouser.com.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UpdatePushEntity implements Serializable {
    private String button_text;
    private List<String> channels = new ArrayList();
    private String click_action;
    private String content;
    private String icon;
    private boolean isOffLineMsg;
    private String push_id;
    private String title;
    private String url;
    private String version_code;
    private String visible_time;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String str) {
        this.icon = str;
    }

    public String getButton_text() {
        return this.button_text;
    }

    public void setButton_text(String str) {
        this.button_text = str;
    }

    public String getClick_action() {
        return this.click_action;
    }

    public void setClick_action(String str) {
        this.click_action = str;
    }

    public List<String> getChannels() {
        return this.channels;
    }

    public void setChannels(List<String> list) {
        this.channels = list;
    }

    public String getVersion_code() {
        return this.version_code;
    }

    public void setVersion_code(String str) {
        this.version_code = str;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public String getVisible_time() {
        return this.visible_time;
    }

    public void setVisible_time(String str) {
        this.visible_time = str;
    }

    public boolean isOffLineMsg() {
        return this.isOffLineMsg;
    }

    public void setIsOffLineMsg(boolean z) {
        this.isOffLineMsg = z;
    }

    public String getPush_id() {
        return this.push_id;
    }

    public void setPush_id(String str) {
        this.push_id = str;
    }
}
