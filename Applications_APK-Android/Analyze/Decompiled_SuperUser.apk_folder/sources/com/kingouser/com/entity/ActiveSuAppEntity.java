package com.kingouser.com.entity;

import java.io.Serializable;

public class ActiveSuAppEntity extends InstallAppEntity implements Serializable {
    private int request_nums;

    public int getRequest_nums() {
        return this.request_nums;
    }

    public void setRequest_nums(int i) {
        this.request_nums = i;
    }
}
