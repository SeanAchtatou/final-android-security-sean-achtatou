package com.kingouser.com.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class InstallAppEntity implements Serializable {
    private ArrayList<ArrayList<Object>> data;
    private String id;
    private String pid;
    private ArrayList<Long> time;
    private String user_id;

    public String getId() {
        return this.id;
    }

    public void setId(String str) {
        this.id = str;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String str) {
        this.user_id = str;
    }

    public String getPid() {
        return this.pid;
    }

    public void setPid(String str) {
        this.pid = str;
    }

    public ArrayList<ArrayList<Object>> getData() {
        return this.data;
    }

    public void setData(ArrayList<ArrayList<Object>> arrayList) {
        this.data = arrayList;
    }

    public ArrayList<Long> getTime() {
        return this.time;
    }

    public void setTime(ArrayList<Long> arrayList) {
        this.time = arrayList;
    }
}
