package com.kingouser.com.entity;

import java.io.Serializable;

public class SuAndUpdateEntity implements Serializable {
    private String daemon_su_download_url;
    private String daemon_su_md5;
    private boolean daemon_su_upgrade;
    private boolean force_update;
    private String kingouser_download_url;
    private String kingouser_md5;
    private boolean kingouser_upgrade;
    private String recovery_kingouser_download_url;
    private String recovery_kingouser_md5;
    private String release_note;
    private String su_download_url;
    private String su_md5;
    private boolean su_upgrade;
    private String version_code;
    private String version_name;

    public boolean isKingouser_upgrade() {
        return this.kingouser_upgrade;
    }

    public void setKingouser_upgrade(boolean z) {
        this.kingouser_upgrade = z;
    }

    public String getVersion_name() {
        return this.version_name;
    }

    public void setVersion_name(String str) {
        this.version_name = str;
    }

    public String getVersion_code() {
        return this.version_code;
    }

    public void setVersion_code(String str) {
        this.version_code = str;
    }

    public String getRelease_note() {
        return this.release_note;
    }

    public void setRelease_note(String str) {
        this.release_note = str;
    }

    public boolean isForce_update() {
        return this.force_update;
    }

    public void setForce_update(boolean z) {
        this.force_update = z;
    }

    public String getKingouser_md5() {
        return this.kingouser_md5;
    }

    public void setKingouser_md5(String str) {
        this.kingouser_md5 = str;
    }

    public String getKingouser_download_url() {
        return this.kingouser_download_url;
    }

    public void setKingouser_download_url(String str) {
        this.kingouser_download_url = str;
    }

    public String getRecovery_kingouser_md5() {
        return this.recovery_kingouser_md5;
    }

    public void setRecovery_kingouser_md5(String str) {
        this.recovery_kingouser_md5 = str;
    }

    public String getRecovery_kingouser_download_url() {
        return this.recovery_kingouser_download_url;
    }

    public void setRecovery_kingouser_download_url(String str) {
        this.recovery_kingouser_download_url = str;
    }

    public boolean isSu_upgrade() {
        return this.su_upgrade;
    }

    public void setSu_upgrade(boolean z) {
        this.su_upgrade = z;
    }

    public String getSu_md5() {
        return this.su_md5;
    }

    public void setSu_md5(String str) {
        this.su_md5 = str;
    }

    public String getSu_download_url() {
        return this.su_download_url;
    }

    public void setSu_download_url(String str) {
        this.su_download_url = str;
    }

    public boolean isDaemon_su_upgrade() {
        return this.daemon_su_upgrade;
    }

    public void setDaemon_su_upgrade(boolean z) {
        this.daemon_su_upgrade = z;
    }

    public String getDaemon_su_md5() {
        return this.daemon_su_md5;
    }

    public void setDaemon_su_md5(String str) {
        this.daemon_su_md5 = str;
    }

    public String getDaemon_su_download_url() {
        return this.daemon_su_download_url;
    }

    public void setDaemon_su_download_url(String str) {
        this.daemon_su_download_url = str;
    }
}
