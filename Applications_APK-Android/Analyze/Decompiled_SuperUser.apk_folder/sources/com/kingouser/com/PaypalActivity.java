package com.kingouser.com;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.util.AuthorizeUtils;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.pureapps.cleaner.analytic.a;
import com.pureapps.cleaner.manager.g;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.view.BBaseActivity;
import com.squareup.okhttp.u;
import java.math.BigDecimal;
import org.json.JSONException;

public class PaypalActivity extends BBaseActivity {
    static boolean n = false;
    public static int o = 5;
    public static int p = 10;
    @BindView(R.id.l4)
    LinearLayout ll_msg;
    @BindView(R.id.l2)
    LinearLayout ll_originalprice;
    @BindView(R.id.l0)
    FrameLayout ll_payment_price;
    @BindView(R.id.ky)
    ProgressBar progressbar_top;
    AuthorizeUtils.VerifyInfo q = null;
    AuthorizeUtils.VerifyPayResult r = null;
    @BindView(R.id.l5)
    LinearLayout rl_creditcard;
    @BindView(R.id.l7)
    LinearLayout rl_paypal;
    AuthorizeUtils.PayResultInfo s = null;
    @BindView(R.id.l1)
    TextView tv_currentprice;
    @BindView(R.id.l3)
    TextView tv_originalprice;
    private PayPalConfiguration u = null;
    /* access modifiers changed from: private */
    public AuthorizeUtils.PAYPALWAY v = null;
    private String w = null;
    /* access modifiers changed from: private */
    public Handler x = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 99:
                    PaypalActivity.this.o();
                    return;
                case 100:
                    PaypalActivity.this.w();
                    return;
                case 101:
                    PaypalActivity.this.u();
                    return;
                case 102:
                case 112:
                default:
                    return;
                case 103:
                case 104:
                case 106:
                    PaypalActivity.this.n();
                    return;
                case 105:
                    PaypalActivity.this.l();
                    return;
                case 107:
                    PaypalActivity.this.D();
                    if (PaypalActivity.this.v == AuthorizeUtils.PAYPALWAY.PAYPAL) {
                        PaypalActivity.this.s = (AuthorizeUtils.PayResultInfo) message.obj;
                        PaypalActivity.this.a(PaypalActivity.this.s);
                        return;
                    }
                    return;
                case 108:
                    PaypalActivity.this.p();
                    return;
                case 109:
                    PaypalActivity.this.t();
                    return;
                case 110:
                    PaypalActivity.this.k();
                    return;
                case 111:
                    PaypalActivity.this.r();
                    return;
                case 113:
                    PaypalActivity.this.p();
                    return;
            }
        }
    };

    /* access modifiers changed from: private */
    public void k() {
        f.a("验证订单剩余尝试次数:" + o);
        if (o <= 0) {
            this.x.sendEmptyMessage(108);
        } else if (this.v == AuthorizeUtils.PAYPALWAY.PAYPAL && this.s != null) {
            a(this.s);
            o--;
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        a.a(this).c(FirebaseAnalytics.getInstance(this), "BtnPayPagePaycanceledClick");
        j();
    }

    /* access modifiers changed from: private */
    public void n() {
        a.a(this).c(FirebaseAnalytics.getInstance(this), "BtnPayPagePayFailClick");
        q();
    }

    /* access modifiers changed from: private */
    public void o() {
        AuthorizeUtils.getInstance().saveLastCheckNeedPayDate(this);
        AuthorizeUtils.getInstance().saveServiceNeedPayState(this, false);
    }

    /* access modifiers changed from: private */
    public void p() {
        q();
        j();
        a.a(this).c(FirebaseAnalytics.getInstance(this), "BtnpPayVerifyResultFail");
    }

    private void q() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), PaymentFailedActivity.class);
        intent.setFlags(268435456);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void r() {
        s();
    }

    private void s() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), PaymentCompleteActivity.class);
        intent.setFlags(268435456);
        startActivity(intent);
        finish();
    }

    /* access modifiers changed from: private */
    public void t() {
        f.a("验证授权码 是否有效");
        if (this.r != null && !TextUtils.isEmpty(this.r.authorization)) {
            String str = this.r.authorization;
            f.a("授权码:" + str);
            if (AuthorizeUtils.getInstance().verifyAuthorization(this, str)) {
                f.a("验证授权码有效");
                this.x.sendEmptyMessage(111);
                AuthorizeUtils.getInstance().saveAuthorization(this, this.r.authorization);
                f.a("二次校验" + AuthorizeUtils.getInstance().checkAuthorization(this));
                return;
            }
            f.a("验证授权码无效");
            this.x.sendEmptyMessage(113);
        }
    }

    /* access modifiers changed from: private */
    public void u() {
        v();
    }

    private void v() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), MainActivity.class);
        intent.setFlags(268435456);
        startActivity(intent);
        finish();
    }

    /* access modifiers changed from: private */
    public void w() {
        a.a(this).c(FirebaseAnalytics.getInstance(this), "BtnpPayPageShow");
        f.a("更新商品订单信息");
        if (this.q != null) {
            if (this.tv_currentprice != null) {
                this.tv_currentprice.setText("$" + this.q.amount);
            }
            if (this.tv_originalprice != null) {
                this.tv_originalprice.setText("$" + this.q.oriPrice);
            }
            this.rl_paypal.setEnabled(true);
            this.rl_creditcard.setEnabled(true);
        }
        j();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.ce);
        f.a("PaypalActivity onCreate");
        B();
        x();
        y();
    }

    private void x() {
        ((TextView) findViewById(R.id.kz)).setTypeface(Typeface.createFromAsset(getAssets(), "fonts/booster_number_font.otf"));
    }

    private void y() {
        f.a("开始检查是否存在授权码.....");
        if (AuthorizeUtils.getInstance().checkAuthorization(getApplicationContext())) {
            f.a("授权码存在.....去首页");
            z();
            return;
        }
        f.a("授权码不存在.....是否需要付费");
        A();
    }

    private void z() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), VipMemberActivity.class);
        intent.setFlags(268435456);
        startActivity(intent);
        finish();
    }

    private void A() {
        a.a(this).c(FirebaseAnalytics.getInstance(this), "BtnPayCheckIsNeedpay");
        String replaceAll = (com.kingouser.com.b.a.f4238g + AuthorizeUtils.getInstance().getVerify(this)).replaceAll(" ", "");
        f.a("request:" + replaceAll);
        AuthorizeUtils.getInstance().getDevicesInfo(this);
        g.a().a(replaceAll, new g.a() {
            public void onBack(u uVar) {
                if (uVar == null) {
                    PaypalActivity.this.x.sendEmptyMessage(101);
                } else if (!uVar.d()) {
                    f.a("response: erroe");
                    f.a("检查是否需要付费失败");
                    PaypalActivity.this.x.sendEmptyMessage(101);
                } else {
                    AuthorizeUtils.VerifyInfo parseVerify = AuthorizeUtils.getInstance().parseVerify(uVar.g());
                    if (parseVerify != null) {
                        f.a("检查是否需要付费 是的 需要付费");
                        if (parseVerify.paystatus == 0) {
                            PaypalActivity.this.x.sendEmptyMessageDelayed(99, 0);
                            return;
                        }
                        PaypalActivity.this.q = parseVerify;
                        PaypalActivity.this.x.sendEmptyMessageDelayed(100, 0);
                        return;
                    }
                    f.a("检查是否需要付费 失败");
                    PaypalActivity.this.x.sendEmptyMessage(101);
                }
            }
        });
    }

    private void B() {
        this.tv_originalprice.getPaint().setFlags(16);
        this.ll_msg.setVisibility(4);
        this.ll_payment_price.setVisibility(4);
        this.rl_paypal.setVisibility(4);
        this.rl_creditcard.setVisibility(4);
        this.progressbar_top.setVisibility(0);
        if (Build.VERSION.SDK_INT < 16) {
            C();
        }
    }

    private void C() {
        this.rl_paypal.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void D() {
        if (this.progressbar_top != null) {
            this.progressbar_top.setVisibility(0);
        }
        this.ll_msg.setVisibility(4);
        this.ll_payment_price.setVisibility(4);
        if (Build.VERSION.SDK_INT < 16) {
            this.rl_paypal.setVisibility(8);
        } else {
            this.rl_paypal.setVisibility(4);
        }
        this.rl_creditcard.setVisibility(4);
    }

    public void j() {
        if (this.progressbar_top != null) {
            this.progressbar_top.setVisibility(8);
        }
        this.ll_msg.setVisibility(0);
        this.ll_payment_price.setVisibility(0);
        if (Build.VERSION.SDK_INT < 16) {
            this.rl_paypal.setVisibility(8);
        } else {
            this.rl_paypal.setVisibility(0);
        }
        this.rl_creditcard.setVisibility(0);
    }

    @OnClick({2131624377, 2131624375})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.l6 /*2131624375*/:
                this.v = AuthorizeUtils.PAYPALWAY.STRIPE;
                E();
                return;
            case R.id.l7 /*2131624376*/:
            default:
                return;
            case R.id.l8 /*2131624377*/:
                this.v = AuthorizeUtils.PAYPALWAY.PAYPAL;
                F();
                return;
        }
    }

    private void E() {
        double d2;
        a.a(this).c(FirebaseAnalytics.getInstance(this), "BtnPayPageStripeClick");
        f.a("StripeCreditCard");
        String str = "pk_test_h7DDCLmhtE3UD7kIJ9ixrbun";
        Intent intent = new Intent();
        intent.setClass(this, StripeActivity.class);
        if (!TextUtils.isEmpty(this.q.stripe_pub)) {
            str = this.q.stripe_pub;
        }
        intent.putExtra("stripe_publick_key", str);
        if (this.q.amount > 0.0d) {
            d2 = this.q.amount;
        } else {
            d2 = 9.95d;
        }
        intent.putExtra("stripe_publick_amount", d2);
        startActivityForResult(intent, p);
    }

    private void F() {
        f.a("paypal准备支付....");
        a.a(this).c(FirebaseAnalytics.getInstance(this), "BtnPayPagePaypalClick");
        H();
        G();
    }

    private void G() {
        f.a("开始支付....");
        String str = "SUPER USER VIP";
        if (!TextUtils.isEmpty(this.q.goodsName)) {
            str = this.q.goodsName;
        }
        float f2 = (float) this.q.amount;
        f.a("" + f2);
        a.a(this).b(FirebaseAnalytics.getInstance(this), "PayPalPage");
        f.a("" + f2);
        PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal((double) f2), "USD", str, "sale");
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra("com.paypal.android.sdk.paypalConfiguration", this.u);
        intent.putExtra("com.paypal.android.sdk.payment", payPalPayment);
        startActivityForResult(intent, 0);
    }

    private void H() {
        o = 5;
        if (this.q != null && !TextUtils.isEmpty(this.q.client_id)) {
            com.kingouser.com.b.a.l = this.q.client_id;
            f.a("change client_id ..........");
        }
        this.u = new PayPalConfiguration().a(n ? "sandbox" : "live").b(n ? com.kingouser.com.b.a.k : com.kingouser.com.b.a.l);
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra("com.paypal.android.sdk.paypalConfiguration", this.u);
        startService(intent);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        f.a("wyy", "onActivityResult" + i + " " + i2);
        if (i == 0) {
            b(i, i2, intent);
        } else if (i == p) {
            a(i, i2, intent);
        }
    }

    private void a(int i, int i2, Intent intent) {
        f.a("handStripeCardid:" + i);
        f.a("handStripeCardid:" + i2);
        f.a("handStripeCardid:" + (intent == null));
        if (i2 != -1) {
            if (i2 == 0) {
            }
        } else if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            int i3 = extras.getInt("stripe_result_state");
            f.a("state:" + i3);
            if (i3 == 1) {
                String string = extras.getString("stripe_result_auth");
                f.a("auth:" + string);
                if (AuthorizeUtils.getInstance().verifyAuthorization(this, string)) {
                    f.a("验证授权码有效");
                    AuthorizeUtils.getInstance().saveAuthorization(this, string);
                    this.x.sendEmptyMessage(111);
                    f.a("二次校验" + AuthorizeUtils.getInstance().checkAuthorization(this));
                    return;
                }
                f.a("验证授权码无效");
                this.x.sendEmptyMessage(113);
                return;
            }
            f.a("无授权码");
            this.x.sendEmptyMessage(113);
        }
    }

    private void b(int i, int i2, Intent intent) {
        if (i2 == -1) {
            PaymentConfirmation paymentConfirmation = (PaymentConfirmation) intent.getParcelableExtra("com.paypal.android.sdk.paymentConfirmation");
            if (paymentConfirmation != null) {
                try {
                    f.a("支付正常..结果检查....");
                    this.s = AuthorizeUtils.getInstance().parsePayResult(paymentConfirmation.a().toString(4));
                    if (this.s == null) {
                        this.x.sendEmptyMessage(103);
                        return;
                    }
                    a.a(this).c(FirebaseAnalytics.getInstance(this), "BtnPayPageVerifypayidClick");
                    f.a("结果检查:" + this.s.response_id);
                    Message message = new Message();
                    message.what = 107;
                    message.obj = this.s;
                    this.x.sendMessageDelayed(message, 0);
                } catch (JSONException e2) {
                    this.x.sendEmptyMessage(104);
                    f.a("支付解析失败....");
                    f.a("wyy", "an extremely unlikely failure occurred: " + e2.toString());
                }
            }
        } else if (i2 == 0) {
            f.a("wyy", "The user canceled.");
            f.a("支付取消....");
            this.x.sendEmptyMessage(105);
        } else if (i2 == 2) {
            f.a("wyy", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            f.a("支付异常....");
            a.a(this).c(FirebaseAnalytics.getInstance(this), "BtnPayPagePayInvalidClick");
            this.x.sendEmptyMessage(106);
        }
    }

    /* access modifiers changed from: private */
    public void a(AuthorizeUtils.PayResultInfo payResultInfo) {
        f.a("支付单号校验开始....");
        f.a("支付单号校验开始....：" + payResultInfo.response_id);
        String str = com.kingouser.com.b.a.f4239h;
        String payPalResultVerify = AuthorizeUtils.getInstance().getPayPalResultVerify(this, payResultInfo, this.q.client_id);
        f.a("request:" + payPalResultVerify);
        g.a().b(payPalResultVerify, str, new g.a() {
            public void onBack(u uVar) {
                if (uVar == null) {
                    f.a("支付单号校验异常....");
                    PaypalActivity.this.x.sendEmptyMessage(108);
                } else if (!uVar.d()) {
                    f.a("response: erroe");
                    f.a("支付单号校验失败....");
                    PaypalActivity.this.x.sendEmptyMessage(108);
                } else {
                    AuthorizeUtils.VerifyPayResult parseVerifyPayResultPaypal = AuthorizeUtils.getInstance().parseVerifyPayResultPaypal(uVar.g());
                    f.a("支付单号校验完毕....");
                    if (parseVerifyPayResultPaypal != null && !TextUtils.isEmpty(parseVerifyPayResultPaypal.pay_id)) {
                        PaypalActivity.this.r = parseVerifyPayResultPaypal;
                        f.a("获得授权码....");
                        PaypalActivity.this.x.sendEmptyMessage(109);
                    } else if (parseVerifyPayResultPaypal == null || parseVerifyPayResultPaypal.paystate != 0) {
                        PaypalActivity.this.x.sendEmptyMessage(108);
                    } else {
                        PaypalActivity.this.x.sendEmptyMessage(110);
                    }
                }
            }
        });
    }

    public void onStart() {
        super.onStart();
        a.a(this).b(FirebaseAnalytics.getInstance(this), "PayCustomizPage");
    }

    public void onStop() {
        super.onStop();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        finish();
        return false;
    }
}
