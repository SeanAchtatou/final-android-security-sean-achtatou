package com.kingouser.com.fragment;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.MainActivity;
import com.kingouser.com.PolicAuthorityActivity;
import com.kingouser.com.R;
import com.kingouser.com.a.b;
import com.kingouser.com.customview.RootStatusRound;
import com.kingouser.com.util.AuthorizeUtils;
import com.kingouser.com.util.ShellUtils;
import com.pureapps.cleaner.analytic.a;
import com.pureapps.cleaner.view.FlashButton;

public class PolicyFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4412a = false;

    /* renamed from: b  reason: collision with root package name */
    private MainActivity f4413b;

    /* renamed from: c  reason: collision with root package name */
    private View f4414c;

    /* renamed from: d  reason: collision with root package name */
    private ValueAnimator f4415d;

    /* renamed from: e  reason: collision with root package name */
    private Handler f4416e = new Handler();

    /* renamed from: f  reason: collision with root package name */
    private Handler f4417f = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 100:
                case 101:
                    PolicyFragment.this.a(message.what);
                    return;
                default:
                    return;
            }
        }
    };

    /* renamed from: g  reason: collision with root package name */
    private Runnable f4418g = new Runnable() {
        public void run() {
            PolicyFragment.this.policAuthority.a(true);
        }
    };
    @BindView(R.id.l9)
    RootStatusRound mStatusRound;
    @BindView(R.id.la)
    FlashButton policAuthority;
    @BindView(R.id.l_)
    TextView policdescription;

    public static PolicyFragment a() {
        return new PolicyFragment();
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        if (i == 100) {
            this.f4412a = true;
            this.mStatusRound.a(this.f4412a);
            this.policAuthority.setText((int) R.string.ay);
            this.policdescription.setText((int) R.string.aw);
            AuthorizeUtils.getInstance().grantPermission(this.f4413b);
        } else if (i == 101) {
            this.f4412a = false;
            this.mStatusRound.a(this.f4412a);
            this.policAuthority.setText((int) R.string.g4);
            this.policdescription.setText((int) R.string.ax);
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f4413b = (MainActivity) getActivity();
        a(layoutInflater, viewGroup, bundle);
        ButterKnife.bind(this, this.f4414c);
        this.policAuthority.setRepeatCount(0);
        return this.f4414c;
    }

    public void onResume() {
        super.onResume();
        if (isAdded()) {
            e();
        }
    }

    private void e() {
        boolean z;
        try {
            z = ShellUtils.checkSuVerison();
        } catch (Exception e2) {
            z = false;
        }
        if (z) {
            this.f4417f.sendEmptyMessage(100);
        } else {
            this.f4417f.sendEmptyMessage(101);
        }
    }

    public void onStart() {
        super.onStart();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f4414c = layoutInflater.inflate((int) R.layout.cf, viewGroup, false);
    }

    private int b(int i) {
        if (isAdded()) {
            return Math.round(getResources().getDisplayMetrics().density * ((float) i));
        }
        return 10;
    }

    public void b() {
        if (this.policdescription != null) {
            this.policdescription.setAlpha(0.0f);
        }
    }

    public void c() {
        if (isAdded()) {
            if (this.policAuthority != null) {
                this.f4416e.removeCallbacks(this.f4418g);
                this.f4416e.postDelayed(this.f4418g, 300);
            }
            int i = -b(30);
            if (this.f4415d == null) {
                this.f4415d = ValueAnimator.ofFloat((float) i, 0.0f);
                this.f4415d.setDuration(375L);
                this.f4415d.setInterpolator(new b());
                this.f4415d.setStartDelay(50);
                this.f4415d.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                        PolicyFragment.this.policdescription.setTranslationX(((Float) valueAnimator.getAnimatedValue()).floatValue());
                        PolicyFragment.this.policdescription.setAlpha(valueAnimator.getAnimatedFraction());
                    }
                });
            }
            this.f4415d.start();
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick({2131624380, 2131624378})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.l9 /*2131624378*/:
                d();
                return;
            case R.id.l_ /*2131624379*/:
            default:
                return;
            case R.id.la /*2131624380*/:
                f();
                return;
        }
    }

    private void f() {
        if (this.f4412a) {
            a.a(this.f4413b).c(FirebaseAnalytics.getInstance(this.f4413b), "BtnMainPolicyClick");
            g();
            return;
        }
        a.a(this.f4413b).c(FirebaseAnalytics.getInstance(this.f4413b), "BtnMainNoRootPolicyClick");
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(com.kingouser.com.b.a.f4235d));
            startActivity(intent);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.f4412a) {
            a.a(this.f4413b).c(FirebaseAnalytics.getInstance(this.f4413b), "BtnMainPolicyCenterClick");
            g();
        }
    }

    private void g() {
        Intent intent = new Intent();
        intent.setClass(this.f4413b, PolicAuthorityActivity.class);
        intent.setFlags(268435456);
        this.f4413b.startActivity(intent);
    }

    public void onStop() {
        super.onStop();
    }
}
