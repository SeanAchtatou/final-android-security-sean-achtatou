package com.kingouser.com.fragment;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.kingouser.com.customview.ProgressieRound;
import com.pureapps.cleaner.view.FlashButton;

public class CleanFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private CleanFragment f4405a;

    /* renamed from: b  reason: collision with root package name */
    private View f4406b;

    /* renamed from: c  reason: collision with root package name */
    private View f4407c;

    public CleanFragment_ViewBinding(final CleanFragment cleanFragment, View view) {
        this.f4405a = cleanFragment;
        View findRequiredView = Utils.findRequiredView(view, R.id.fl, "field 'mBtCleanStart' and method 'onClick'");
        cleanFragment.mBtCleanStart = (FlashButton) Utils.castView(findRequiredView, R.id.fl, "field 'mBtCleanStart'", FlashButton.class);
        this.f4406b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                cleanFragment.onClick(view);
            }
        });
        cleanFragment.tvStoragedUsage = (TextView) Utils.findRequiredViewAsType(view, R.id.fk, "field 'tvStoragedUsage'", TextView.class);
        cleanFragment.tvStorageTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.fj, "field 'tvStorageTitle'", TextView.class);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.fi, "field 'mBtCleanProgressie' and method 'onClick'");
        cleanFragment.mBtCleanProgressie = (ProgressieRound) Utils.castView(findRequiredView2, R.id.fi, "field 'mBtCleanProgressie'", ProgressieRound.class);
        this.f4407c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                cleanFragment.onClick(view);
            }
        });
    }

    public void unbind() {
        CleanFragment cleanFragment = this.f4405a;
        if (cleanFragment == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4405a = null;
        cleanFragment.mBtCleanStart = null;
        cleanFragment.tvStoragedUsage = null;
        cleanFragment.tvStorageTitle = null;
        cleanFragment.mBtCleanProgressie = null;
        this.f4406b.setOnClickListener(null);
        this.f4406b = null;
        this.f4407c.setOnClickListener(null);
        this.f4407c = null;
    }
}
