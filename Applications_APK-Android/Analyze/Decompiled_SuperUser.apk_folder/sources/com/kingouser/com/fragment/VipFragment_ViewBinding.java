package com.kingouser.com.fragment;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.pureapps.cleaner.view.ShadowLayout;

public class VipFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private VipFragment f4444a;

    /* renamed from: b  reason: collision with root package name */
    private View f4445b;

    /* renamed from: c  reason: collision with root package name */
    private View f4446c;

    /* renamed from: d  reason: collision with root package name */
    private View f4447d;

    public VipFragment_ViewBinding(final VipFragment vipFragment, View view) {
        this.f4444a = vipFragment;
        vipFragment.mPeopleImg = (ImageView) Utils.findRequiredViewAsType(view, R.id.mc, "field 'mPeopleImg'", ImageView.class);
        vipFragment.mCloudImg1 = (ImageView) Utils.findRequiredViewAsType(view, R.id.m_, "field 'mCloudImg1'", ImageView.class);
        vipFragment.mCloudImg2 = (ImageView) Utils.findRequiredViewAsType(view, R.id.ma, "field 'mCloudImg2'", ImageView.class);
        vipFragment.mCloudImg3 = (ImageView) Utils.findRequiredViewAsType(view, R.id.mb, "field 'mCloudImg3'", ImageView.class);
        vipFragment.mStarImg1 = (ImageView) Utils.findRequiredViewAsType(view, R.id.md, "field 'mStarImg1'", ImageView.class);
        vipFragment.mStarImg2 = (ImageView) Utils.findRequiredViewAsType(view, R.id.f8340me, "field 'mStarImg2'", ImageView.class);
        vipFragment.mProgress = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.fr, "field 'mProgress'", ProgressBar.class);
        vipFragment.progressbar_top = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.ky, "field 'progressbar_top'", ProgressBar.class);
        vipFragment.mMessageLayout = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.mf, "field 'mMessageLayout'", LinearLayout.class);
        vipFragment.mVipMessageText = (TextView) Utils.findRequiredViewAsType(view, R.id.mg, "field 'mVipMessageText'", TextView.class);
        vipFragment.mVipDiscount = (TextView) Utils.findRequiredViewAsType(view, R.id.mh, "field 'mVipDiscount'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.mk, "field 'mPaypalBtn' and method 'onClick'");
        vipFragment.mPaypalBtn = (TextView) Utils.castView(findRequiredView, R.id.mk, "field 'mPaypalBtn'", TextView.class);
        this.f4445b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                vipFragment.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.mi, "field 'mCreditCardBtn' and method 'onClick'");
        vipFragment.mCreditCardBtn = (TextView) Utils.castView(findRequiredView2, R.id.mi, "field 'mCreditCardBtn'", TextView.class);
        this.f4446c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                vipFragment.onClick(view);
            }
        });
        vipFragment.mMainVipHeadLayout = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.m9, "field 'mMainVipHeadLayout'", FrameLayout.class);
        vipFragment.mGiftVipHeadLayout = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.m8, "field 'mGiftVipHeadLayout'", FrameLayout.class);
        vipFragment.content_layout = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.f8338io, "field 'content_layout'", FrameLayout.class);
        vipFragment.mShadowlayoutVipPaypal = (ShadowLayout) Utils.findRequiredViewAsType(view, R.id.mj, "field 'mShadowlayoutVipPaypal'", ShadowLayout.class);
        View findRequiredView3 = Utils.findRequiredView(view, R.id.is, "method 'onClick'");
        this.f4447d = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                vipFragment.onClick(view);
            }
        });
    }

    public void unbind() {
        VipFragment vipFragment = this.f4444a;
        if (vipFragment == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4444a = null;
        vipFragment.mPeopleImg = null;
        vipFragment.mCloudImg1 = null;
        vipFragment.mCloudImg2 = null;
        vipFragment.mCloudImg3 = null;
        vipFragment.mStarImg1 = null;
        vipFragment.mStarImg2 = null;
        vipFragment.mProgress = null;
        vipFragment.progressbar_top = null;
        vipFragment.mMessageLayout = null;
        vipFragment.mVipMessageText = null;
        vipFragment.mVipDiscount = null;
        vipFragment.mPaypalBtn = null;
        vipFragment.mCreditCardBtn = null;
        vipFragment.mMainVipHeadLayout = null;
        vipFragment.mGiftVipHeadLayout = null;
        vipFragment.content_layout = null;
        vipFragment.mShadowlayoutVipPaypal = null;
        this.f4445b.setOnClickListener(null);
        this.f4445b = null;
        this.f4446c.setOnClickListener(null);
        this.f4446c = null;
        this.f4447d.setOnClickListener(null);
        this.f4447d = null;
    }
}
