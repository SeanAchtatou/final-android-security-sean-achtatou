package com.kingouser.com.fragment;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.kingouser.com.customview.RootStatusRound;
import com.pureapps.cleaner.view.FlashButton;

public class PolicyFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private PolicyFragment f4422a;

    /* renamed from: b  reason: collision with root package name */
    private View f4423b;

    /* renamed from: c  reason: collision with root package name */
    private View f4424c;

    public PolicyFragment_ViewBinding(final PolicyFragment policyFragment, View view) {
        this.f4422a = policyFragment;
        View findRequiredView = Utils.findRequiredView(view, R.id.la, "field 'policAuthority' and method 'onClick'");
        policyFragment.policAuthority = (FlashButton) Utils.castView(findRequiredView, R.id.la, "field 'policAuthority'", FlashButton.class);
        this.f4423b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                policyFragment.onClick(view);
            }
        });
        policyFragment.policdescription = (TextView) Utils.findRequiredViewAsType(view, R.id.l_, "field 'policdescription'", TextView.class);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.l9, "field 'mStatusRound' and method 'onClick'");
        policyFragment.mStatusRound = (RootStatusRound) Utils.castView(findRequiredView2, R.id.l9, "field 'mStatusRound'", RootStatusRound.class);
        this.f4424c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                policyFragment.onClick(view);
            }
        });
    }

    public void unbind() {
        PolicyFragment policyFragment = this.f4422a;
        if (policyFragment == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4422a = null;
        policyFragment.policAuthority = null;
        policyFragment.policdescription = null;
        policyFragment.mStatusRound = null;
        this.f4423b.setOnClickListener(null);
        this.f4423b = null;
        this.f4424c.setOnClickListener(null);
        this.f4424c = null;
    }
}
