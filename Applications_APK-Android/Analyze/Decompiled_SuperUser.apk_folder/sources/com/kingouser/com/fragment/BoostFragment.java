package com.kingouser.com.fragment;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.MainActivity;
import com.kingouser.com.R;
import com.kingouser.com.a.b;
import com.kingouser.com.customview.ProgressieRound;
import com.pureapps.cleaner.MemoryBoostActivity;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.util.k;
import com.pureapps.cleaner.util.l;
import com.pureapps.cleaner.view.FlashButton;

public class BoostFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public MainActivity f4374a;

    /* renamed from: b  reason: collision with root package name */
    private Thread f4375b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public a f4376c = new a();

    /* renamed from: d  reason: collision with root package name */
    private ValueAnimator f4377d;

    /* renamed from: e  reason: collision with root package name */
    private ValueAnimator f4378e;

    /* renamed from: f  reason: collision with root package name */
    private ValueAnimator f4379f;

    /* renamed from: g  reason: collision with root package name */
    private ValueAnimator f4380g;

    /* renamed from: h  reason: collision with root package name */
    private Runnable f4381h = new Runnable() {
        public void run() {
            BoostFragment.this.mBtBoostStart.a(true);
        }
    };
    @BindView(R.id.f1)
    ProgressieRound mBtBoostProgressie;
    @BindView(R.id.f4)
    FlashButton mBtBoostStart;
    @BindView(R.id.f2)
    TextView tvMemoryTitle;
    @BindView(R.id.f3)
    TextView tvRamUsage;

    public static BoostFragment a() {
        return new BoostFragment();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.ai, (ViewGroup) null);
        ButterKnife.bind(this, inflate);
        this.f4374a = (MainActivity) getActivity();
        a(inflate);
        return inflate;
    }

    private void a(View view) {
        this.mBtBoostStart.setAutoAnim(false);
        this.mBtBoostStart.setRepeatCount(0);
    }

    public void b() {
        if (this.tvMemoryTitle != null) {
            this.tvMemoryTitle.setAlpha(0.0f);
        }
        if (this.tvRamUsage != null) {
            this.tvRamUsage.setAlpha(0.0f);
        }
    }

    private int c(int i) {
        if (isAdded()) {
            return Math.round(getResources().getDisplayMetrics().density * ((float) i));
        }
        return 10;
    }

    public void a(int i) {
        if (isAdded()) {
            if (!(i == 1 || this.mBtBoostStart == null)) {
                this.f4376c.removeCallbacks(this.f4381h);
                this.f4376c.postDelayed(this.f4381h, 300);
            }
            if (i == 0) {
                int c2 = c(30);
                if (this.f4379f == null) {
                    this.f4379f = a(c2, this.tvMemoryTitle, 50);
                }
                if (this.f4380g == null) {
                    this.f4380g = a(c2, this.tvRamUsage, 80);
                }
                this.f4379f.start();
                this.f4380g.start();
            } else if (i == 2) {
                int i2 = -c(30);
                if (this.f4377d == null) {
                    this.f4377d = a(i2, this.tvMemoryTitle, 50);
                }
                if (this.f4378e == null) {
                    this.f4378e = a(i2, this.tvRamUsage, 80);
                }
                this.f4377d.start();
                this.f4378e.start();
            }
        }
    }

    private ValueAnimator a(int i, final View view, int i2) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat((float) i, 0.0f);
        ofFloat.setDuration(375L);
        ofFloat.setInterpolator(new b());
        ofFloat.setStartDelay((long) i2);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                view.setTranslationX(((Float) valueAnimator.getAnimatedValue()).floatValue());
                view.setAlpha(valueAnimator.getAnimatedFraction());
            }
        });
        return ofFloat;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    public void onStart() {
        super.onStart();
        d();
    }

    private void d() {
        e();
    }

    private void e() {
        k.a();
        k.a(this.f4375b);
        this.f4375b = new Thread(new Runnable() {
            public void run() {
                long i = l.i(BoostFragment.this.f4374a);
                long h2 = l.h(BoostFragment.this.f4374a);
                int i2 = (int) (((i - h2) * 100) / i);
                f.a("mHandler:" + (BoostFragment.this.f4376c == null));
                Message obtainMessage = BoostFragment.this.f4376c.obtainMessage(1);
                obtainMessage.getData().putLong("totalRAM", i);
                obtainMessage.getData().putLong("availableRAM", h2);
                obtainMessage.getData().putInt("ramPrecent", i2);
                BoostFragment.this.f4376c.sendMessage(obtainMessage);
            }
        });
        this.f4375b.start();
    }

    public void onResume() {
        super.onResume();
        if (this.f4376c == null) {
            this.f4376c = new a();
        }
        if (this.f4374a == null) {
            this.f4374a = (MainActivity) getActivity();
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onDestroy() {
        super.onDestroy();
        k.a();
        k.a(this.f4375b);
    }

    @OnClick({2131624151, 2131624148})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.f1 /*2131624148*/:
                f();
                return;
            case R.id.f2 /*2131624149*/:
            case R.id.f3 /*2131624150*/:
            default:
                return;
            case R.id.f4 /*2131624151*/:
                g();
                return;
        }
    }

    private void f() {
        com.pureapps.cleaner.analytic.a.a(this.f4374a).c(FirebaseAnalytics.getInstance(this.f4374a), "BtnMainBoostCenterClick");
        g();
    }

    public void c() {
        e();
    }

    public void b(int i) {
        if (this.mBtBoostProgressie != null) {
            this.mBtBoostProgressie.a(i);
        }
    }

    private void g() {
        com.pureapps.cleaner.analytic.a.a(this.f4374a).c(FirebaseAnalytics.getInstance(this.f4374a), "BtnMainBoostClick");
        Intent intent = new Intent();
        intent.setClass(this.f4374a, MemoryBoostActivity.class);
        intent.setFlags(268435456);
        this.f4374a.startActivity(intent);
    }

    class a extends Handler {
        a() {
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    long j = message.getData().getLong("totalRAM");
                    long j2 = message.getData().getLong("availableRAM");
                    BoostFragment.this.b(message.getData().getInt("ramPrecent"));
                    String formatFileSize = Formatter.formatFileSize(BoostFragment.this.f4374a, j - j2);
                    String formatFileSize2 = Formatter.formatFileSize(BoostFragment.this.f4374a, j);
                    if (BoostFragment.this.isAdded()) {
                        BoostFragment.this.tvRamUsage.setText(BoostFragment.this.getString(R.string.aq, formatFileSize, formatFileSize2));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
