package com.kingouser.com.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;

public class AppManagerFeagment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private AppManagerFeagment f4360a;

    /* renamed from: b  reason: collision with root package name */
    private View f4361b;

    public AppManagerFeagment_ViewBinding(final AppManagerFeagment appManagerFeagment, View view) {
        this.f4360a = appManagerFeagment;
        appManagerFeagment.progressBar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.d6, "field 'progressBar'", ProgressBar.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.iv, "field 'mUninstall' and method 'onClick'");
        appManagerFeagment.mUninstall = (TextView) Utils.castView(findRequiredView, R.id.iv, "field 'mUninstall'", TextView.class);
        this.f4361b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                appManagerFeagment.onClick(view);
            }
        });
        appManagerFeagment.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.iu, "field 'mRecyclerView'", RecyclerView.class);
        appManagerFeagment.mTvShow = (TextView) Utils.findRequiredViewAsType(view, R.id.it, "field 'mTvShow'", TextView.class);
    }

    public void unbind() {
        AppManagerFeagment appManagerFeagment = this.f4360a;
        if (appManagerFeagment == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4360a = null;
        appManagerFeagment.progressBar = null;
        appManagerFeagment.mUninstall = null;
        appManagerFeagment.mRecyclerView = null;
        appManagerFeagment.mTvShow = null;
        this.f4361b.setOnClickListener(null);
        this.f4361b = null;
    }
}
