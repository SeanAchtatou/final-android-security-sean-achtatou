package com.kingouser.com.fragment;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.kingouser.com.customview.ProgressieRound;
import com.pureapps.cleaner.view.FlashButton;

public class BoostFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private BoostFragment f4387a;

    /* renamed from: b  reason: collision with root package name */
    private View f4388b;

    /* renamed from: c  reason: collision with root package name */
    private View f4389c;

    public BoostFragment_ViewBinding(final BoostFragment boostFragment, View view) {
        this.f4387a = boostFragment;
        View findRequiredView = Utils.findRequiredView(view, R.id.f4, "field 'mBtBoostStart' and method 'onClick'");
        boostFragment.mBtBoostStart = (FlashButton) Utils.castView(findRequiredView, R.id.f4, "field 'mBtBoostStart'", FlashButton.class);
        this.f4388b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                boostFragment.onClick(view);
            }
        });
        boostFragment.tvRamUsage = (TextView) Utils.findRequiredViewAsType(view, R.id.f3, "field 'tvRamUsage'", TextView.class);
        boostFragment.tvMemoryTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.f2, "field 'tvMemoryTitle'", TextView.class);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.f1, "field 'mBtBoostProgressie' and method 'onClick'");
        boostFragment.mBtBoostProgressie = (ProgressieRound) Utils.castView(findRequiredView2, R.id.f1, "field 'mBtBoostProgressie'", ProgressieRound.class);
        this.f4389c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                boostFragment.onClick(view);
            }
        });
    }

    public void unbind() {
        BoostFragment boostFragment = this.f4387a;
        if (boostFragment == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4387a = null;
        boostFragment.mBtBoostStart = null;
        boostFragment.tvRamUsage = null;
        boostFragment.tvMemoryTitle = null;
        boostFragment.mBtBoostProgressie = null;
        this.f4388b.setOnClickListener(null);
        this.f4388b = null;
        this.f4389c.setOnClickListener(null);
        this.f4389c = null;
    }
}
