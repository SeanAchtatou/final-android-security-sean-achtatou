package com.kingouser.com.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.AppManagerActivity;
import com.kingouser.com.R;
import com.kingouser.com.application.App;
import com.kingouser.com.customview.CommonDialog;
import com.kingouser.com.entity.UninstallAppInfo;
import com.kingouser.com.util.AppManagerUtils;
import com.kingouser.com.util.FormatUtils;
import com.kingouser.com.util.ShellUtils;
import com.pureapps.cleaner.adapter.AppManagerAdapter;
import com.pureapps.cleaner.b.c;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.util.k;
import java.util.ArrayList;
import java.util.Iterator;
import me.everything.android.ui.overscroll.g;

public class AppManagerFeagment extends Fragment implements c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f4344a;

    /* renamed from: b  reason: collision with root package name */
    private AppManagerActivity f4345b;

    /* renamed from: c  reason: collision with root package name */
    private int f4346c = 0;

    /* renamed from: d  reason: collision with root package name */
    private String[] f4347d = {"USER", "SYSTEM"};

    /* renamed from: e  reason: collision with root package name */
    private int[] f4348e = {R.color.am, R.color.ah};

    /* renamed from: f  reason: collision with root package name */
    private View f4349f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public ArrayList<UninstallAppInfo> f4350g = new ArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public AppManagerAdapter f4351h;
    /* access modifiers changed from: private */
    public int i = 0;
    /* access modifiers changed from: private */
    public int j = 0;
    private Handler k = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            String str = (String) message.obj;
            int i = 0;
            while (true) {
                int i2 = i;
                if (AppManagerFeagment.this.f4350g == null || i2 >= AppManagerFeagment.this.f4350g.size()) {
                    break;
                } else if (((UninstallAppInfo) AppManagerFeagment.this.f4350g.get(i2)).pkg.equals(str)) {
                    AppManagerFeagment.this.f4350g.remove(i2);
                    AppManagerFeagment.this.f4351h.a(i2);
                    break;
                } else {
                    i = i2 + 1;
                }
            }
            AppManagerFeagment.this.c();
        }
    };
    @BindView(R.id.iu)
    RecyclerView mRecyclerView;
    @BindView(R.id.it)
    TextView mTvShow;
    @BindView(R.id.iv)
    TextView mUninstall;
    @BindView(R.id.d6)
    ProgressBar progressBar;

    static /* synthetic */ int a(AppManagerFeagment appManagerFeagment) {
        int i2 = appManagerFeagment.j;
        appManagerFeagment.j = i2 + 1;
        return i2;
    }

    public static AppManagerFeagment a(int i2, Context context, AppManagerActivity appManagerActivity) {
        return new AppManagerFeagment(i2, context, appManagerActivity);
    }

    @SuppressLint({"ValidFragment"})
    public AppManagerFeagment() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            this.f4344a = activity.getApplicationContext();
        } else {
            this.f4344a = App.a().getApplicationContext();
        }
    }

    @SuppressLint({"ValidFragment"})
    public AppManagerFeagment(int i2, Context context, AppManagerActivity appManagerActivity) {
        this.f4346c = i2;
        this.f4344a = context;
        this.f4345b = appManagerActivity;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.pureapps.cleaner.b.a.a(this);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f4349f = layoutInflater.inflate((int) R.layout.bg, (ViewGroup) null);
        ButterKnife.bind(this, this.f4349f);
        this.mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.f4344a);
        linearLayoutManager.b(1);
        this.mRecyclerView.setLayoutManager(linearLayoutManager);
        this.mRecyclerView.setHasFixedSize(true);
        g.a(this.mRecyclerView, 0);
        this.f4351h = new AppManagerAdapter(getActivity(), this, this.f4346c, this.mRecyclerView);
        this.mRecyclerView.setAdapter(this.f4351h);
        return this.f4349f;
    }

    public void a() {
        this.progressBar.setVisibility(8);
    }

    public void onStart() {
        super.onStart();
        if (this.mTvShow != null) {
            this.mTvShow.setText(this.f4347d[this.f4346c]);
            this.f4349f.setBackgroundColor(this.f4348e[this.f4346c]);
        }
    }

    public void a(CheckBox checkBox, int i2) {
        UninstallAppInfo b2 = this.f4351h.b(i2);
        b2.checked = !b2.checked;
        checkBox.setChecked(b2.checked);
        c();
        f.b("checked:" + b2.checked);
    }

    /* access modifiers changed from: private */
    public void c() {
        if (e() > 0) {
            this.mUninstall.setSelected(true);
            this.mUninstall.setEnabled(true);
            this.mUninstall.setTextColor(getResources().getColor(R.color.a_));
            this.mUninstall.setText(getResources().getString(R.string.bc) + "(" + d() + ")");
            return;
        }
        this.mUninstall.setTextColor(getResources().getColor(R.color.a_));
        this.mUninstall.setText(getResources().getString(R.string.bc));
        this.mUninstall.setSelected(false);
        this.mUninstall.setEnabled(false);
    }

    private String d() {
        long j2;
        Iterator<UninstallAppInfo> it = this.f4351h.a().iterator();
        long j3 = 0;
        while (it.hasNext()) {
            UninstallAppInfo next = it.next();
            if (next.checked) {
                j2 = next.size + j3;
            } else {
                j2 = j3;
            }
            j3 = j2;
        }
        if (j3 == 0) {
            return "";
        }
        return FormatUtils.formatBytesInByte(j3);
    }

    private int e() {
        int i2 = 0;
        Iterator<UninstallAppInfo> it = this.f4351h.a().iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                return i3;
            }
            if (it.next().checked) {
                i2 = i3 + 1;
            } else {
                i2 = i3;
            }
        }
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroy() {
        super.onDestroy();
        com.pureapps.cleaner.b.a.b(this);
        if (this.f4350g != null) {
            this.f4350g.clear();
            this.f4350g = null;
        }
        if (this.f4351h != null) {
            this.f4351h = null;
        }
    }

    public void b() {
        if (this.f4350g == null) {
            this.f4350g = new ArrayList<>();
        }
        this.f4350g.clear();
        this.f4351h.a(this.f4350g);
    }

    public void a(UninstallAppInfo uninstallAppInfo) {
        this.progressBar.setVisibility(0);
        if (this.f4350g != null) {
            this.f4350g.add(uninstallAppInfo);
            this.f4351h.a(this.f4350g);
        }
    }

    @OnClick({2131624290})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv /*2131624290*/:
                f();
                return;
            default:
                return;
        }
    }

    private void f() {
        f.a("uninstallClick" + e());
        if (e() == 0) {
            Toast.makeText(this.f4344a, (int) R.string.ey, 0).show();
            return;
        }
        ArrayList<UninstallAppInfo> g2 = g();
        if (this.f4346c == 0) {
            com.pureapps.cleaner.analytic.a.a(this.f4344a).c(FirebaseAnalytics.getInstance(this.f4344a), "BtnAppManagerUserUninstallClick");
            this.i = g2.size();
            this.j = 0;
            for (int i2 = 0; i2 < this.i; i2++) {
                UninstallAppInfo uninstallAppInfo = g2.get(i2);
                if (ShellUtils.checkRootPermission()) {
                    new a().executeOnExecutor(k.a().b(), uninstallAppInfo.pkg);
                } else {
                    AppManagerUtils.unInstallUserApp(this.f4344a, uninstallAppInfo.pkg);
                }
            }
            return;
        }
        com.pureapps.cleaner.analytic.a.a(this.f4344a).c(FirebaseAnalytics.getInstance(this.f4344a), "BtnAppManagerSystemUninstallClick");
        a(g2);
    }

    class a extends AsyncTask<String, Void, Boolean> {

        /* renamed from: b  reason: collision with root package name */
        private String f4359b;

        a() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            AppManagerFeagment.this.progressBar.setVisibility(0);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            this.f4359b = strArr[0];
            return Boolean.valueOf(AppManagerUtils.uninstallUserApp(this.f4359b));
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            AppManagerFeagment.a(AppManagerFeagment.this);
            if (AppManagerFeagment.this.j == AppManagerFeagment.this.i) {
                AppManagerFeagment.this.progressBar.setVisibility(8);
            }
            if (!bool.booleanValue()) {
                AppManagerUtils.unInstallUserApp(AppManagerFeagment.this.f4344a, this.f4359b);
            }
        }
    }

    private void a(final ArrayList<UninstallAppInfo> arrayList) {
        final CommonDialog commonDialog = new CommonDialog(this.f4345b);
        commonDialog.a(getResources().getString(R.string.f2));
        commonDialog.a((int) R.drawable.gu);
        commonDialog.b(getResources().getString(R.string.f1));
        commonDialog.a(true);
        commonDialog.a(new CommonDialog.a() {
            public void a() {
                AppManagerFeagment.this.b(arrayList);
                commonDialog.dismiss();
            }

            public void b() {
                commonDialog.dismiss();
            }
        });
        commonDialog.show();
    }

    /* access modifiers changed from: private */
    public void b(final ArrayList<UninstallAppInfo> arrayList) {
        new Thread(new Runnable() {
            public void run() {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    AppManagerFeagment.this.b((UninstallAppInfo) it.next());
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void b(UninstallAppInfo uninstallAppInfo) {
        Handler j2 = this.f4345b.j();
        if (j2 != null) {
            Message message = new Message();
            message.what = 101;
            message.obj = uninstallAppInfo;
            this.f4345b.j().sendMessage(message);
        }
        if (ShellUtils.checkRoot(this.f4344a)) {
            boolean uninstallSystemApp = AppManagerUtils.uninstallSystemApp(uninstallAppInfo, this.f4344a);
            f.a("uninstallSystemApp:" + uninstallSystemApp);
            if (j2 != null) {
                Message message2 = new Message();
                message2.what = 102;
                message2.obj = Boolean.valueOf(uninstallSystemApp);
                this.f4345b.j().sendMessage(message2);
            }
        } else if (j2 != null) {
            Message message3 = new Message();
            message3.what = 103;
            this.f4345b.j().sendMessage(message3);
        }
    }

    private ArrayList<UninstallAppInfo> g() {
        ArrayList<UninstallAppInfo> arrayList = new ArrayList<>();
        Iterator<UninstallAppInfo> it = this.f4351h.a().iterator();
        while (it.hasNext()) {
            UninstallAppInfo next = it.next();
            if (next.checked) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public void a(int i2, long j2, Object obj) {
        switch (i2) {
            case 33:
                Message obtainMessage = this.k.obtainMessage();
                obtainMessage.obj = obj;
                this.k.sendMessageDelayed(obtainMessage, 500);
                return;
            default:
                return;
        }
    }
}
