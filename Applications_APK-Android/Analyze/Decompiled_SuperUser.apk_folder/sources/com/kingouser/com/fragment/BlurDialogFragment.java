package com.kingouser.com.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.pureapps.cleaner.util.m;
import com.pureapps.cleaner.view.etsyblur.c;
import com.pureapps.cleaner.view.etsyblur.d;
import com.pureapps.cleaner.view.etsyblur.f;

public abstract class BlurDialogFragment extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4364a = BlurDialogFragment.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private c f4365b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ViewGroup f4366c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public ImageView f4367d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f4368e = true;

    /* renamed from: f  reason: collision with root package name */
    private final ViewTreeObserver.OnPreDrawListener f4369f = new ViewTreeObserver.OnPreDrawListener() {
        public boolean onPreDraw() {
            BlurDialogFragment.this.f4366c.getViewTreeObserver().removeOnPreDrawListener(this);
            BlurDialogFragment.this.f4366c.post(new Runnable() {
                public void run() {
                    BlurDialogFragment.this.d();
                    BlurDialogFragment.this.e();
                }
            });
            return true;
        }
    };

    public void a(boolean z) {
        this.f4368e = z;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.f4365b = new c(context, a());
        if (!(context instanceof Activity) || !this.f4368e) {
            Log.w(f4364a, "onAttach(Context context) - context is not type of Activity. Currently Not supported.");
            return;
        }
        this.f4366c = (ViewGroup) ((Activity) context).getWindow().getDecorView();
        if (this.f4366c.isShown()) {
            d();
            e();
            return;
        }
        this.f4366c.getViewTreeObserver().addOnPreDrawListener(this.f4369f);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
    }

    public void onStart() {
        Dialog dialog = getDialog();
        if (dialog != null && !b()) {
            dialog.getWindow().clearFlags(2);
        }
        super.onStart();
    }

    public void onDismiss(DialogInterface dialogInterface) {
        if (this.f4368e) {
            f();
        }
        super.onDismiss(dialogInterface);
    }

    public void onDetach() {
        if (this.f4366c != null) {
            this.f4366c.getViewTreeObserver().removeOnPreDrawListener(this.f4369f);
        }
        if (this.f4365b != null) {
            this.f4365b.a();
        }
        super.onDetach();
    }

    /* access modifiers changed from: protected */
    public d a() {
        return d.f5978b;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return false;
    }

    /* access modifiers changed from: protected */
    public int c() {
        return 400;
    }

    /* access modifiers changed from: private */
    public void d() {
        Rect rect = new Rect();
        this.f4366c.getWindowVisibleDisplayFrame(rect);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(rect.right - rect.left, rect.bottom - rect.top);
        layoutParams.setMargins(rect.left, rect.top, 0, 0);
        this.f4367d = new ImageView(this.f4366c.getContext());
        this.f4367d.setLayoutParams(layoutParams);
        this.f4367d.setAlpha(0.0f);
        this.f4366c.addView(this.f4367d);
        this.f4365b.a(m.a(this.f4366c, rect.right, rect.bottom, (float) rect.left, (float) rect.top, a().b(), a().c()), true, new f.a() {
            public void a(Bitmap bitmap) {
                BlurDialogFragment.this.f4367d.setImageBitmap(bitmap);
            }
        });
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.f4367d != null) {
            m.a(this.f4367d, 0.0f, 1.0f, c(), null);
        }
    }

    private void f() {
        if (this.f4367d != null) {
            m.a(this.f4367d, 1.0f, 0.0f, c(), new Runnable() {
                public void run() {
                    BlurDialogFragment.this.f4366c.removeView(BlurDialogFragment.this.f4367d);
                }
            });
        }
    }
}
