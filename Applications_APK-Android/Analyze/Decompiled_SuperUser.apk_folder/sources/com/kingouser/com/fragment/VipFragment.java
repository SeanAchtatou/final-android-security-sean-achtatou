package com.kingouser.com.fragment;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.PaymentCompleteActivity;
import com.kingouser.com.PaymentFailedActivity;
import com.kingouser.com.PaypalActivity;
import com.kingouser.com.R;
import com.kingouser.com.StripeActivity;
import com.kingouser.com.VipMemberActivity;
import com.kingouser.com.application.App;
import com.kingouser.com.util.AuthorizeUtils;
import com.kingouser.com.util.ShellUtils;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.pureapps.cleaner.b.a;
import com.pureapps.cleaner.manager.g;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.view.ShadowLayout;
import com.squareup.okhttp.u;
import explosionfield.ExplosionField;
import java.math.BigDecimal;
import org.json.JSONException;

public class VipFragment extends BlurDialogFragment {

    /* renamed from: d  reason: collision with root package name */
    static boolean f4429d = false;

    /* renamed from: e  reason: collision with root package name */
    public static int f4430e = 5;

    /* renamed from: a  reason: collision with root package name */
    AuthorizeUtils.VerifyInfo f4431a = null;

    /* renamed from: b  reason: collision with root package name */
    AuthorizeUtils.VerifyPayResult f4432b = null;

    /* renamed from: c  reason: collision with root package name */
    AuthorizeUtils.PayResultInfo f4433c = null;
    @BindView(R.id.f8338io)
    FrameLayout content_layout;

    /* renamed from: f  reason: collision with root package name */
    private final String f4434f = "VipFragment";

    /* renamed from: g  reason: collision with root package name */
    private Context f4435g = null;

    /* renamed from: h  reason: collision with root package name */
    private int f4436h = 1;
    private AnimatorSet i;
    private ObjectAnimator j;
    private AnimatorSet k;
    private ObjectAnimator l;
    private AnimatorSet m;
    @BindView(R.id.m_)
    ImageView mCloudImg1;
    @BindView(R.id.ma)
    ImageView mCloudImg2;
    @BindView(R.id.mb)
    ImageView mCloudImg3;
    @BindView(R.id.mi)
    TextView mCreditCardBtn;
    @BindView(R.id.m8)
    FrameLayout mGiftVipHeadLayout;
    @BindView(R.id.m9)
    FrameLayout mMainVipHeadLayout;
    @BindView(R.id.mf)
    LinearLayout mMessageLayout;
    @BindView(R.id.mk)
    TextView mPaypalBtn;
    @BindView(R.id.mc)
    ImageView mPeopleImg;
    @BindView(R.id.fr)
    ProgressBar mProgress;
    @BindView(R.id.mj)
    ShadowLayout mShadowlayoutVipPaypal;
    @BindView(R.id.md)
    ImageView mStarImg1;
    @BindView(R.id.f8340me)
    ImageView mStarImg2;
    @BindView(R.id.mh)
    TextView mVipDiscount;
    @BindView(R.id.mg)
    TextView mVipMessageText;
    private ObjectAnimator n;
    /* access modifiers changed from: private */
    public AuthorizeUtils.PAYPALWAY o = null;
    private String p = null;
    @BindView(R.id.ky)
    ProgressBar progressbar_top;
    private PayPalConfiguration q = null;
    private ExplosionField r;
    /* access modifiers changed from: private */
    public Handler s = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 99:
                    VipFragment.this.mProgress.setVisibility(8);
                    VipFragment.this.mMessageLayout.setVisibility(8);
                    VipFragment.this.f();
                    return;
                case 100:
                    if (VipFragment.this.f4431a != null && VipFragment.this.isAdded()) {
                        VipFragment.this.mProgress.setVisibility(8);
                        VipFragment.this.mMessageLayout.setVisibility(0);
                        VipFragment.this.mPaypalBtn.setEnabled(true);
                        VipFragment.this.mCreditCardBtn.setEnabled(true);
                        if (VipFragment.this.mVipMessageText != null) {
                            VipFragment.this.mVipMessageText.setText(String.format(VipFragment.this.getString(R.string.f9), "$:" + VipFragment.this.f4431a.amount));
                            VipFragment.this.mVipDiscount.setText(VipFragment.this.a(VipFragment.this.f4431a));
                        }
                        VipFragment.this.s();
                        return;
                    }
                    return;
                case 101:
                    VipFragment.this.dismissAllowingStateLoss();
                    return;
                case 102:
                case 103:
                case 104:
                case 105:
                case 106:
                case 114:
                case 115:
                case 116:
                case 117:
                case 118:
                case 119:
                default:
                    return;
                case 107:
                    VipFragment.this.t();
                    if (VipFragment.this.o == AuthorizeUtils.PAYPALWAY.PAYPAL) {
                        VipFragment.this.f4433c = (AuthorizeUtils.PayResultInfo) message.obj;
                        VipFragment.this.a(VipFragment.this.f4433c);
                        return;
                    }
                    return;
                case 108:
                    VipFragment.this.l();
                    return;
                case 109:
                    VipFragment.this.k();
                    return;
                case 110:
                    VipFragment.this.j();
                    return;
                case 111:
                case 112:
                    VipFragment.this.h();
                    return;
                case 113:
                    VipFragment.this.g();
                    return;
                case 120:
                    VipFragment.this.i();
                    return;
            }
        }
    };
    private Runnable t = new Runnable() {
        public void run() {
            VipFragment.this.dismissAllowingStateLoss();
        }
    };

    public static VipFragment a(int i2) {
        VipFragment vipFragment = new VipFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("style", i2);
        vipFragment.setArguments(bundle);
        return vipFragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        getDialog().requestWindowFeature(1);
        View inflate = layoutInflater.inflate((int) R.layout.cx, viewGroup, false);
        ButterKnife.bind(this, inflate);
        return inflate;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, 16973840);
    }

    public void onStart() {
        super.onStart();
        try {
            Window window = getDialog().getWindow();
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.dimAmount = 0.0f;
            window.setAttributes(attributes);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onAttach(Context context) {
        boolean z = true;
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey("style")) {
            this.f4436h = arguments.getInt("style");
        }
        if (this.f4436h != 1) {
            z = false;
        }
        a(z);
        super.onAttach(context);
        this.r = ExplosionField.a(getActivity());
    }

    public void onActivityCreated(Bundle bundle) {
        int i2;
        int i3 = 0;
        super.onActivityCreated(bundle);
        try {
            getDialog().getWindow().getAttributes().windowAnimations = 16973826;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.f4435g = getActivity().getApplicationContext();
        this.mGiftVipHeadLayout.getLayoutParams().height = getResources().getDisplayMetrics().heightPixels / 5;
        FrameLayout frameLayout = this.mMainVipHeadLayout;
        if (this.f4436h == 1) {
            i2 = 0;
        } else {
            i2 = 8;
        }
        frameLayout.setVisibility(i2);
        FrameLayout frameLayout2 = this.mGiftVipHeadLayout;
        if (this.f4436h != 2) {
            i3 = 8;
        }
        frameLayout2.setVisibility(i3);
        n();
        this.content_layout.getLayoutParams().width = (int) (((float) getResources().getDisplayMetrics().widthPixels) * 0.92f);
        d();
    }

    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        a.a(34, 0, null);
    }

    private void d() {
        t();
        final Context applicationContext = getActivity().getApplicationContext();
        AuthorizeUtils instance = AuthorizeUtils.getInstance();
        if (instance.checkAuthorization(applicationContext)) {
            com.pureapps.cleaner.analytic.a.a(applicationContext).c(FirebaseAnalytics.getInstance(applicationContext), "BtnPayCheckAuthorOK");
            f.a("检查是否需要付费，已经存在授权码...");
            i();
        } else if (!ShellUtils.checkSuVerison()) {
            f.a("检查是否需要付费，无Root不付费...");
        } else {
            if (Build.VERSION.SDK_INT < 16) {
                e();
            }
            instance.grantPermission(applicationContext);
            g.a().a(com.kingouser.com.b.a.f4236e, AuthorizeUtils.getInstance().getPayerInfo(applicationContext), new g.a() {
                public void onBack(u uVar) {
                    AuthorizeUtils instance = AuthorizeUtils.getInstance();
                    if (uVar != null) {
                        AuthorizeUtils.PayerInfo parsePayerInfo = instance.parsePayerInfo(uVar.g());
                        if (parsePayerInfo == null) {
                            instance.saveServiceNeedPayState(applicationContext, true);
                        } else if (parsePayerInfo.status == 0) {
                            instance.saveServiceNeedPayState(applicationContext, true);
                            VipFragment.this.m();
                        } else if (AuthorizeUtils.getInstance().verifyAuthorization(applicationContext, parsePayerInfo.auth)) {
                            f.a("验证授权码有效");
                            AuthorizeUtils.getInstance().saveAuthorization(applicationContext, parsePayerInfo.auth);
                            VipFragment.this.s.sendEmptyMessage(120);
                        }
                    }
                }
            });
        }
    }

    private void e() {
        this.mShadowlayoutVipPaypal.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void f() {
        AuthorizeUtils.getInstance().saveLastCheckNeedPayDate(getActivity());
        AuthorizeUtils.getInstance().saveServiceNeedPayState(getActivity(), false);
        dismissAllowingStateLoss();
    }

    /* access modifiers changed from: private */
    public void g() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), PaymentFailedActivity.class);
        intent.setFlags(268435456);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void h() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), PaymentCompleteActivity.class);
        intent.setFlags(268435456);
        startActivity(intent);
        dismiss();
    }

    /* access modifiers changed from: private */
    public String a(AuthorizeUtils.VerifyInfo verifyInfo) {
        String str = verifyInfo.oriPrice;
        double d2 = 0.0d;
        try {
            double doubleValue = new BigDecimal(1.0d - (verifyInfo.amount / Double.parseDouble(str))).setScale(2, 4).doubleValue();
            f.a("discount:" + doubleValue);
            d2 = doubleValue * 100.0d;
            f.a("discount:" + d2);
        } catch (Exception e2) {
        }
        return (d2 + "").substring(0, 2) + "%" + " OFF";
    }

    /* access modifiers changed from: private */
    public void i() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), VipMemberActivity.class);
        intent.setFlags(268435456);
        getActivity().startActivity(intent);
        dismissAllowingStateLoss();
    }

    /* access modifiers changed from: private */
    public void j() {
        f.a("验证订单剩余尝试次数:" + f4430e);
        if (f4430e <= 0) {
            this.s.sendEmptyMessage(108);
        } else if (this.o == AuthorizeUtils.PAYPALWAY.PAYPAL && this.f4433c != null) {
            a(this.f4433c);
            f4430e--;
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        f.a("验证授权码 是否有效");
        if (this.f4432b != null && !TextUtils.isEmpty(this.f4432b.authorization)) {
            String str = this.f4432b.authorization;
            f.a("授权码:" + str);
            if (AuthorizeUtils.getInstance().verifyAuthorization(App.a(), str)) {
                f.a("验证授权码有效");
                this.s.sendEmptyMessage(112);
                AuthorizeUtils.getInstance().saveAuthorization(App.a(), this.f4432b.authorization);
                f.a("二次校验" + AuthorizeUtils.getInstance().checkAuthorization(App.a()));
                return;
            }
            f.a("验证授权码无效");
            this.s.sendEmptyMessage(113);
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        if (isAdded()) {
            com.pureapps.cleaner.analytic.a.a(getActivity()).c(FirebaseAnalytics.getInstance(getActivity()), "BtnpPayVerifyResultFail");
            s();
        }
    }

    /* access modifiers changed from: private */
    public void a(AuthorizeUtils.PayResultInfo payResultInfo) {
        f.a("支付单号校验开始....");
        f.a("支付单号校验开始....：" + payResultInfo.response_id);
        String str = com.kingouser.com.b.a.f4239h;
        String payPalResultVerify = AuthorizeUtils.getInstance().getPayPalResultVerify(App.a(), payResultInfo, this.f4431a.client_id);
        f.a("request:" + payPalResultVerify);
        g.a().b(payPalResultVerify, str, new g.a() {
            public void onBack(u uVar) {
                if (uVar == null) {
                    f.a("支付单号校验异常....");
                    VipFragment.this.s.sendEmptyMessage(108);
                } else if (!uVar.d()) {
                    f.a("response: erroe");
                    f.a("支付单号校验失败....");
                    VipFragment.this.s.sendEmptyMessage(108);
                } else {
                    AuthorizeUtils.VerifyPayResult parseVerifyPayResultPaypal = AuthorizeUtils.getInstance().parseVerifyPayResultPaypal(uVar.g());
                    f.a("支付单号校验完毕....");
                    if (parseVerifyPayResultPaypal != null && !TextUtils.isEmpty(parseVerifyPayResultPaypal.pay_id)) {
                        VipFragment.this.f4432b = parseVerifyPayResultPaypal;
                        f.a("获得授权码....");
                        VipFragment.this.s.sendEmptyMessage(109);
                    } else if (parseVerifyPayResultPaypal == null || parseVerifyPayResultPaypal.paystate != 0) {
                        VipFragment.this.s.sendEmptyMessage(108);
                    } else {
                        VipFragment.this.s.sendEmptyMessage(110);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void m() {
        String replaceAll = (com.kingouser.com.b.a.f4238g + AuthorizeUtils.getInstance().getVerify(getActivity())).replaceAll(" ", "");
        f.a("request:" + replaceAll);
        AuthorizeUtils.getInstance().getDevicesInfo(getActivity());
        g.a().a(replaceAll, new g.a() {
            public void onBack(u uVar) {
                if (uVar == null) {
                    VipFragment.this.s.sendEmptyMessage(101);
                } else if (!uVar.d()) {
                    f.a("response: erroe");
                    VipFragment.this.s.sendEmptyMessage(101);
                } else {
                    AuthorizeUtils.VerifyInfo parseVerify = AuthorizeUtils.getInstance().parseVerify(uVar.g());
                    if (parseVerify != null) {
                        f.a("检查是否需要付费 是的 需要付费");
                        if (parseVerify.paystatus == 0) {
                            VipFragment.this.s.sendEmptyMessageDelayed(99, 0);
                            return;
                        }
                        VipFragment.this.f4431a = parseVerify;
                        VipFragment.this.s.sendEmptyMessageDelayed(100, 0);
                        return;
                    }
                    f.a("检查是否需要付费 失败");
                    VipFragment.this.s.sendEmptyMessage(101);
                }
            }
        });
    }

    private void n() {
        float f2 = getResources().getDisplayMetrics().density;
        float f3 = (float) getResources().getDisplayMetrics().widthPixels;
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.mPeopleImg, "translationY", this.mPeopleImg.getTranslationY() + (100.0f * f2), this.mPeopleImg.getTranslationY());
        ofFloat.setInterpolator(new LinearInterpolator());
        ofFloat.setDuration(800L);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.mPeopleImg, "translationX", this.mPeopleImg.getTranslationX() - (100.0f * f2), this.mPeopleImg.getTranslationX());
        ofFloat2.setInterpolator(new LinearInterpolator());
        ofFloat2.setDuration(800L);
        this.m = new AnimatorSet();
        this.m.setStartDelay(0);
        this.m.play(ofFloat).with(ofFloat2);
        this.m.addListener(new Animator.AnimatorListener() {
            public void onAnimationStart(Animator animator) {
                VipFragment.this.mPeopleImg.setVisibility(0);
            }

            public void onAnimationEnd(Animator animator) {
            }

            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }
        });
        this.n = ObjectAnimator.ofFloat(this.mCloudImg1, "translationX", this.mCloudImg1.getTranslationX() - (50.0f * f2), f3);
        this.n.setInterpolator(new LinearInterpolator());
        this.n.setDuration(15000L);
        this.n.setRepeatCount(-1);
        this.l = ObjectAnimator.ofFloat(this.mCloudImg2, "translationX", this.mCloudImg2.getTranslationX() - (50.0f * f2), f3);
        this.l.setInterpolator(new LinearInterpolator());
        this.l.setDuration(17000L);
        this.l.setStartDelay(3000);
        this.l.setRepeatCount(-1);
        this.j = ObjectAnimator.ofFloat(this.mCloudImg3, "translationX", this.mCloudImg3.getTranslationX() - (f2 * 50.0f), f3);
        this.j.setInterpolator(new LinearInterpolator());
        this.j.setDuration(19000L);
        this.j.setStartDelay(6000);
        this.j.setRepeatCount(-1);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.mStarImg1, "rotation", 0.0f, 360.0f);
        ofFloat3.setDuration(5000L);
        ofFloat3.setRepeatCount(-1);
        ofFloat3.setRepeatMode(2);
        ofFloat3.setInterpolator(new LinearInterpolator());
        ObjectAnimator ofFloat4 = ObjectAnimator.ofFloat(this.mStarImg1, "scaleX", 0.5f, 1.4f);
        ofFloat4.setDuration(5000L);
        ofFloat4.setRepeatCount(-1);
        ofFloat4.setRepeatMode(2);
        ofFloat4.setInterpolator(new LinearInterpolator());
        ObjectAnimator ofFloat5 = ObjectAnimator.ofFloat(this.mStarImg1, "scaleY", 0.5f, 1.4f);
        ofFloat5.setDuration(5000L);
        ofFloat5.setRepeatCount(-1);
        ofFloat5.setRepeatMode(2);
        ofFloat5.setInterpolator(new LinearInterpolator());
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ofFloat4).with(ofFloat5);
        this.k = new AnimatorSet();
        this.k.play(ofFloat3).with(animatorSet);
        ObjectAnimator ofFloat6 = ObjectAnimator.ofFloat(this.mStarImg2, "rotation", 0.0f, 360.0f);
        ofFloat6.setDuration(8000L);
        ofFloat6.setRepeatCount(-1);
        ofFloat6.setRepeatMode(2);
        ofFloat6.setInterpolator(new LinearInterpolator());
        ObjectAnimator ofFloat7 = ObjectAnimator.ofFloat(this.mStarImg2, "scaleX", 0.5f, 2.0f);
        ofFloat7.setDuration(8000L);
        ofFloat7.setRepeatCount(-1);
        ofFloat7.setRepeatMode(2);
        ofFloat7.setInterpolator(new LinearInterpolator());
        ObjectAnimator ofFloat8 = ObjectAnimator.ofFloat(this.mStarImg2, "scaleY", 0.5f, 2.0f);
        ofFloat8.setDuration(8000L);
        ofFloat8.setRepeatCount(-1);
        ofFloat8.setRepeatMode(2);
        ofFloat8.setInterpolator(new LinearInterpolator());
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.play(ofFloat7).with(ofFloat8);
        this.i = new AnimatorSet();
        this.i.setStartDelay(4000);
        this.i.play(ofFloat6).with(animatorSet2);
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.m.cancel();
        this.n.cancel();
        this.l.cancel();
        this.j.cancel();
        this.k.cancel();
        this.i.cancel();
    }

    private void o() {
        f4430e = 5;
        if (this.f4431a != null && !TextUtils.isEmpty(this.f4431a.client_id)) {
            com.kingouser.com.b.a.l = this.f4431a.client_id;
            f.a("change client_id ..........");
        }
        this.q = new PayPalConfiguration().a(f4429d ? "sandbox" : "live").b(f4429d ? com.kingouser.com.b.a.k : com.kingouser.com.b.a.l);
        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra("com.paypal.android.sdk.paypalConfiguration", this.q);
        getActivity().startService(intent);
    }

    private void p() {
        f.a("开始支付....");
        String str = "SUPER USER VIP";
        if (!TextUtils.isEmpty(this.f4431a.goodsName)) {
            str = this.f4431a.goodsName;
        }
        float f2 = (float) this.f4431a.amount;
        f.a("" + f2);
        com.pureapps.cleaner.analytic.a.a(getActivity()).b(FirebaseAnalytics.getInstance(getActivity()), "PayPalPage");
        f.a("" + f2);
        PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal((double) f2), "USD", str, "sale");
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra("com.paypal.android.sdk.paypalConfiguration", this.q);
        intent.putExtra("com.paypal.android.sdk.payment", payPalPayment);
        startActivityForResult(intent, 0);
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        f.a("VipFragment", "onActivityResult:" + i2 + " | " + i3 + " | isAdded:" + isAdded());
        if (i2 == 0 && isAdded()) {
            b(i2, i3, intent);
        } else if (i2 == PaypalActivity.p) {
            a(i2, i3, intent);
        }
    }

    private void a(int i2, int i3, Intent intent) {
        f.a("handStripeCardid:" + i2);
        f.a("handStripeCardid:" + i3);
        f.a("handStripeCardid:" + (intent == null));
        if (i3 != -1) {
            if (i3 == 0) {
            }
        } else if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            int i4 = extras.getInt("stripe_result_state");
            f.a("state:" + i4);
            if (i4 == 1) {
                String string = extras.getString("stripe_result_auth");
                f.a("auth:" + string);
                if (AuthorizeUtils.getInstance().verifyAuthorization(getActivity(), string)) {
                    f.a("验证授权码有效");
                    AuthorizeUtils.getInstance().saveAuthorization(getActivity(), string);
                    this.s.sendEmptyMessage(111);
                    f.a("二次校验" + AuthorizeUtils.getInstance().checkAuthorization(getActivity()));
                    return;
                }
                f.a("验证授权码无效");
                this.s.sendEmptyMessage(113);
                return;
            }
            f.a("无授权码");
            this.s.sendEmptyMessage(113);
        }
    }

    private void b(int i2, int i3, Intent intent) {
        if (i3 == -1) {
            PaymentConfirmation paymentConfirmation = (PaymentConfirmation) intent.getParcelableExtra("com.paypal.android.sdk.paymentConfirmation");
            if (paymentConfirmation != null) {
                try {
                    f.a("支付正常..结果检查....");
                    this.f4433c = AuthorizeUtils.getInstance().parsePayResult(paymentConfirmation.a().toString(4));
                    if (this.f4433c == null) {
                        this.s.sendEmptyMessage(103);
                        return;
                    }
                    com.pureapps.cleaner.analytic.a.a(getActivity()).c(FirebaseAnalytics.getInstance(getActivity()), "BtnPayPageVerifypayidClick");
                    f.a("结果检查:" + this.f4433c.response_id);
                    Message message = new Message();
                    message.what = 107;
                    message.obj = this.f4433c;
                    this.s.sendMessageDelayed(message, 0);
                } catch (JSONException e2) {
                    this.s.sendEmptyMessage(104);
                    f.a("支付解析失败....");
                    f.a("VipFragment", "an extremely unlikely failure occurred: " + e2.toString());
                }
            }
        } else if (i3 == 0) {
            f.a("VipFragment", "The user canceled.");
            f.a("支付取消....");
            this.s.sendEmptyMessage(105);
        } else if (i3 == 2) {
            f.a("VipFragment", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            f.a("支付异常....");
            com.pureapps.cleaner.analytic.a.a(getActivity()).c(FirebaseAnalytics.getInstance(getActivity()), "BtnPayPagePayInvalidClick");
            this.s.sendEmptyMessage(106);
        }
    }

    private void q() {
        f.a("准备支付....gotopaypal");
        com.pureapps.cleaner.analytic.a.a(getActivity()).c(FirebaseAnalytics.getInstance(getActivity()), "BtnPayPagePaypalClick");
        o();
        p();
    }

    @OnClick({2131624427, 2131624425, 2131624287})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.is /*2131624287*/:
                this.r.a(this.content_layout);
                this.s.postDelayed(this.t, 1024);
                return;
            case R.id.mi /*2131624425*/:
                this.o = AuthorizeUtils.PAYPALWAY.STRIPE;
                r();
                return;
            case R.id.mk /*2131624427*/:
                this.o = AuthorizeUtils.PAYPALWAY.PAYPAL;
                q();
                return;
            default:
                return;
        }
    }

    private void r() {
        double d2;
        f.a("StripeCreditCard");
        com.pureapps.cleaner.analytic.a.a(this.f4435g).c(FirebaseAnalytics.getInstance(this.f4435g), "BtnVipPageStripeClick");
        String str = "pk_test_h7DDCLmhtE3UD7kIJ9ixrbun";
        Intent intent = new Intent();
        intent.setClass(getActivity(), StripeActivity.class);
        if (!TextUtils.isEmpty(this.f4431a.stripe_pub)) {
            str = this.f4431a.stripe_pub;
        }
        intent.putExtra("stripe_publick_key", str);
        if (this.f4431a.amount > 0.0d) {
            d2 = this.f4431a.amount;
        } else {
            d2 = 9.95d;
        }
        intent.putExtra("stripe_publick_amount", d2);
        startActivityForResult(intent, PaypalActivity.p);
    }

    /* access modifiers changed from: private */
    public void s() {
        if (this.progressbar_top != null) {
            this.progressbar_top.setVisibility(8);
            this.m.start();
            this.n.start();
            this.l.start();
            this.j.start();
            this.k.start();
            this.i.start();
        }
    }

    /* access modifiers changed from: private */
    public void t() {
        if (this.progressbar_top != null) {
            this.progressbar_top.setVisibility(0);
        }
    }
}
