package com.kingouser.com.fragment;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.MainActivity;
import com.kingouser.com.R;
import com.kingouser.com.a.b;
import com.kingouser.com.customview.ProgressieRound;
import com.pureapps.cleaner.JunkClearActivity;
import com.pureapps.cleaner.util.k;
import com.pureapps.cleaner.util.l;
import com.pureapps.cleaner.view.FlashButton;

public class CleanFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public MainActivity f4394a;

    /* renamed from: b  reason: collision with root package name */
    private Thread f4395b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public a f4396c;

    /* renamed from: d  reason: collision with root package name */
    private ValueAnimator f4397d;

    /* renamed from: e  reason: collision with root package name */
    private ValueAnimator f4398e;

    /* renamed from: f  reason: collision with root package name */
    private Runnable f4399f = new Runnable() {
        public void run() {
            CleanFragment.this.mBtCleanStart.a(true);
        }
    };
    @BindView(R.id.fi)
    ProgressieRound mBtCleanProgressie;
    @BindView(R.id.fl)
    FlashButton mBtCleanStart;
    @BindView(R.id.fj)
    TextView tvStorageTitle;
    @BindView(R.id.fk)
    TextView tvStoragedUsage;

    public static CleanFragment a() {
        return new CleanFragment();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f4396c = new a();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.ap, (ViewGroup) null);
        ButterKnife.bind(this, inflate);
        this.f4394a = (MainActivity) getActivity();
        a(inflate);
        return inflate;
    }

    private void a(View view) {
        this.mBtCleanStart.setAutoAnim(false);
        this.mBtCleanStart.setRepeatCount(0);
    }

    public void b() {
        if (this.tvStorageTitle != null) {
            this.tvStorageTitle.setAlpha(0.0f);
        }
        if (this.tvStoragedUsage != null) {
            this.tvStoragedUsage.setAlpha(0.0f);
        }
    }

    private int c(int i) {
        if (isAdded()) {
            return Math.round(getResources().getDisplayMetrics().density * ((float) i));
        }
        return 10;
    }

    public void a(int i) {
        if (i != 2 && isAdded()) {
            if (this.mBtCleanStart != null) {
                this.f4396c.removeCallbacks(this.f4399f);
                this.f4396c.postDelayed(this.f4399f, 300);
            }
            int c2 = c(30);
            if (this.f4397d == null) {
                this.f4397d = a(c2, this.tvStorageTitle, 50);
            }
            if (this.f4398e == null) {
                this.f4398e = a(c2, this.tvStoragedUsage, 80);
            }
            this.f4397d.start();
            this.f4398e.start();
        }
    }

    private ValueAnimator a(int i, final View view, int i2) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat((float) i, 0.0f);
        ofFloat.setDuration(375L);
        ofFloat.setInterpolator(new b());
        ofFloat.setStartDelay((long) i2);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                view.setTranslationX(((Float) valueAnimator.getAnimatedValue()).floatValue());
                view.setAlpha(valueAnimator.getAnimatedFraction());
            }
        });
        return ofFloat;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    public void onStart() {
        super.onStart();
        d();
    }

    private void d() {
        e();
    }

    private void e() {
        k.a();
        k.a(this.f4395b);
        this.f4395b = new Thread(new Runnable() {
            public void run() {
                if (CleanFragment.this.isAdded() && CleanFragment.this.f4396c != null) {
                    long f2 = l.f(CleanFragment.this.f4394a) + l.d();
                    long g2 = l.g(CleanFragment.this.f4394a) + l.c();
                    int i = (int) (((f2 - g2) * 100) / f2);
                    Message obtainMessage = CleanFragment.this.f4396c.obtainMessage(1);
                    if (obtainMessage == null) {
                        obtainMessage = new Message();
                        obtainMessage.what = 1;
                    }
                    obtainMessage.getData().putLong("totalStorage", f2);
                    obtainMessage.getData().putLong("availableStorage", g2);
                    obtainMessage.getData().putInt("storagePrecent", i);
                    CleanFragment.this.f4396c.sendMessage(obtainMessage);
                }
            }
        });
        this.f4395b.start();
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick({2131624169, 2131624166})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fi /*2131624166*/:
                f();
                return;
            case R.id.fj /*2131624167*/:
            case R.id.fk /*2131624168*/:
            default:
                return;
            case R.id.fl /*2131624169*/:
                g();
                return;
        }
    }

    private void f() {
        com.pureapps.cleaner.analytic.a.a(this.f4394a).c(FirebaseAnalytics.getInstance(this.f4394a), "BtnMainCleanCenterClick");
        g();
    }

    public void b(int i) {
        if (this.mBtCleanProgressie != null) {
            this.mBtCleanProgressie.a(i);
        }
    }

    public void c() {
        e();
    }

    private void g() {
        com.pureapps.cleaner.analytic.a.a(this.f4394a).c(FirebaseAnalytics.getInstance(this.f4394a), "BtnMainCleanClick");
        Intent intent = new Intent();
        intent.setClass(this.f4394a, JunkClearActivity.class);
        intent.setFlags(268435456);
        this.f4394a.startActivity(intent);
    }

    class a extends Handler {
        a() {
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    long j = message.getData().getLong("totalStorage");
                    long j2 = message.getData().getLong("availableStorage");
                    CleanFragment.this.b(message.getData().getInt("storagePrecent"));
                    String formatFileSize = Formatter.formatFileSize(CleanFragment.this.f4394a, j - j2);
                    String formatFileSize2 = Formatter.formatFileSize(CleanFragment.this.f4394a, j);
                    CleanFragment.this.tvStoragedUsage.setText(CleanFragment.this.getString(R.string.at, formatFileSize, formatFileSize2));
                    return;
                default:
                    return;
            }
        }
    }
}
