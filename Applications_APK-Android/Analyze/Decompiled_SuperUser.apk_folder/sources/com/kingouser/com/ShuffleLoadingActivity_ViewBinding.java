package com.kingouser.com;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class ShuffleLoadingActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private ShuffleLoadingActivity f4098a;

    /* renamed from: b  reason: collision with root package name */
    private View f4099b;

    public ShuffleLoadingActivity_ViewBinding(final ShuffleLoadingActivity shuffleLoadingActivity, View view) {
        this.f4098a = shuffleLoadingActivity;
        shuffleLoadingActivity.relativeLayout = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.jc, "field 'relativeLayout'", RelativeLayout.class);
        shuffleLoadingActivity.adLayout = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.lq, "field 'adLayout'", FrameLayout.class);
        shuffleLoadingActivity.failed_layout = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.ls, "field 'failed_layout'", RelativeLayout.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.lt, "method 'onClick'");
        this.f4099b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                shuffleLoadingActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        ShuffleLoadingActivity shuffleLoadingActivity = this.f4098a;
        if (shuffleLoadingActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4098a = null;
        shuffleLoadingActivity.relativeLayout = null;
        shuffleLoadingActivity.adLayout = null;
        shuffleLoadingActivity.failed_layout = null;
        this.f4099b.setOnClickListener(null);
        this.f4099b = null;
    }
}
