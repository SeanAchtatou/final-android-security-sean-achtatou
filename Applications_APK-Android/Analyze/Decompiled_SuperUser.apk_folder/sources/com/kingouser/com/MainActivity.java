package com.kingouser.com;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.q;
import android.support.v4.app.t;
import android.support.v4.view.ViewPager;
import android.support.v4.view.z;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnPageChange;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.bd;
import com.airbnb.lottie.bm;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.application.App;
import com.kingouser.com.entity.SuAndUpdateEntity;
import com.kingouser.com.entity.httpEntity.UpdateEntity;
import com.kingouser.com.fragment.BoostFragment;
import com.kingouser.com.fragment.CleanFragment;
import com.kingouser.com.fragment.PolicyFragment;
import com.kingouser.com.fragment.VipFragment;
import com.kingouser.com.util.AuthorizeUtils;
import com.kingouser.com.util.CacheManager;
import com.kingouser.com.util.DeviceInfoUtils;
import com.kingouser.com.util.FileUtils;
import com.kingouser.com.util.HttpUtils;
import com.kingouser.com.util.LanguageUtils;
import com.kingouser.com.util.MyLog;
import com.kingouser.com.util.MySharedPreference;
import com.kingouser.com.util.PermissionUtils;
import com.kingouser.com.util.ResultUtils;
import com.kingouser.com.util.ShellUtils;
import com.kingouser.com.util.SuHelper;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.pureapps.cleaner.IgnoreListActivity;
import com.pureapps.cleaner.NotificationGuideActivity;
import com.pureapps.cleaner.NotificationManagerActivity;
import com.pureapps.cleaner.b.c;
import com.pureapps.cleaner.manager.e;
import com.pureapps.cleaner.manager.h;
import com.pureapps.cleaner.service.NotificationMonitorService;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.l;
import com.pureapps.cleaner.view.etsyblur.g;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends BaseActivity implements c {
    /* access modifiers changed from: private */
    public SuAndUpdateEntity A;
    private PolicyFragment B;
    private BoostFragment C;
    private CleanFragment D;
    private a E = null;
    /* access modifiers changed from: private */
    public List<Fragment> F = new ArrayList();
    private ImageView G;
    private com.pureapps.cleaner.a.c H = null;
    private ImageView I;
    private AnimatorSet J = null;
    private ImageView K;
    private ObjectAnimator L = null;
    private ImageView M = null;
    private ValueAnimator N;
    /* access modifiers changed from: private */
    public com.pureapps.cleaner.view.jumpingbeans.a O;
    private b P = new b();
    private int Q = 0;
    /* access modifiers changed from: private */
    public String R;
    /* access modifiers changed from: private */
    public String S;
    /* access modifiers changed from: private */
    public String T;
    /* access modifiers changed from: private */
    public String U;
    private TextView V = null;
    /* access modifiers changed from: private */
    public Runnable W = new Runnable() {
        public void run() {
            if (MainActivity.this.r.isShown()) {
                MainActivity.this.r.c();
                MainActivity.this.X.postDelayed(MainActivity.this.W, 7000);
            }
        }
    };
    /* access modifiers changed from: private */
    public Handler X = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 36:
                    if (!MainActivity.this.isFinishing()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.dj);
                        builder.a(true);
                        builder.a(17039380);
                        builder.c(17301543);
                        builder.a(17039370, (DialogInterface.OnClickListener) null);
                        builder.b((int) R.string.be);
                        builder.b().setCanceledOnTouchOutside(true);
                        builder.c();
                        return;
                    }
                    return;
                case 80:
                    CheckSuDialgoActivity.a(MainActivity.this);
                    return;
                case 88:
                    SuUpdateActivity.a(MainActivity.this.w);
                    return;
                case 89:
                    SuUpdatingActivity.a(MainActivity.this.w);
                    return;
                case 90:
                    MainActivity.this.a(MainActivity.this.A);
                    return;
                case 91:
                    MainActivity.this.y();
                    SuHelper.checkSu(MainActivity.this, MainActivity.this.X);
                    return;
                case 93:
                    String fileMd5 = DeviceInfoUtils.getFileMd5(MainActivity.this.w, MainActivity.this.w.getFilesDir() + "/su");
                    if (!TextUtils.isEmpty(fileMd5) || !fileMd5.equalsIgnoreCase(MainActivity.this.A.getSu_md5())) {
                        HttpUtils.downloadSu(MainActivity.this.X, MainActivity.this.w, MainActivity.this.A.getSu_download_url(), false);
                        return;
                    }
                    Message message2 = new Message();
                    message2.what = 90;
                    MainActivity.this.X.sendMessage(message2);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Runnable Y = new Runnable() {
        public void run() {
            VipFragment.a(1).show(MainActivity.this.e(), "VipFragment");
        }
    };
    @BindView(R.id.di)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.dk)
    NavigationView mNavigationView;
    TextView n;
    int[] p = {R.string.cv, R.string.ct, R.string.cu};
    int[] q = {R.color.eu, R.color.er, R.color.es};
    LottieAnimationView r;
    TextView s;
    TextView t;
    TextView u;
    ImageButton v;
    @BindView(R.id.d2)
    ViewPager viewPager;
    /* access modifiers changed from: private */
    public Context w;
    /* access modifiers changed from: private */
    public q x;
    private CacheManager y;
    private ExecutorService z;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.pureapps.cleaner.b.a.a(this);
        this.w = getApplicationContext();
        setContentView((int) R.layout.a7);
        m();
        k();
        u();
        h.a(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.pureapps.cleaner.b.a.b(this);
        if (this.O != null) {
            this.O.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MySharedPreference.setWheaterOnResume(this, true);
        w();
        if (!MySharedPreference.getMainActivityLocalLanguage(this.w, "").equalsIgnoreCase(LanguageUtils.getLocalLanguage())) {
            MySharedPreference.setMainActivityLocalLanguage(this.w, LanguageUtils.getLocalLanguage());
        }
        if (this.viewPager != null) {
            f.a("xxxxxx onResume " + this.viewPager.getCurrentItem());
            if (this.viewPager.getCurrentItem() == 0) {
                com.pureapps.cleaner.analytic.a.a(this.w).b(this.o, "FragmentPolice");
            } else if (this.viewPager.getCurrentItem() == 1) {
                com.pureapps.cleaner.analytic.a.a(this.w).b(this.o, "FragmentBoost");
            } else if (this.viewPager.getCurrentItem() == 2) {
                com.pureapps.cleaner.analytic.a.a(this.w).b(this.o, "FragmentClean");
            }
        }
        if (!i.a(this).f()) {
            this.X.postDelayed(this.W, 100);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        MySharedPreference.setWheaterOnResume(this, false);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent == null || ServiceManagerNative.NOTIFICATION.equalsIgnoreCase(intent.getStringExtra("extra"))) {
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || this.mDrawerLayout == null || !this.mDrawerLayout.f(8388611)) {
            return super.onKeyDown(i, keyEvent);
        }
        this.mDrawerLayout.b();
        return true;
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.mNavigationView != null) {
            Menu menu = this.mNavigationView.getMenu();
            for (int i = 0; i < menu.size(); i++) {
                MenuItem item = menu.getItem(i);
                if (item.isChecked()) {
                    item.setChecked(false);
                }
            }
        }
    }

    private void k() {
        App.f4220b = 0;
        v();
        this.y = CacheManager.newInstance(this.w);
        if (this.viewPager != null) {
            a(this.viewPager);
        }
        TabLayout tabLayout = (TabLayout) findViewById(R.id.d1);
        tabLayout.setupWithViewPager(this.viewPager);
        tabLayout.getTabAt(0).setCustomView(d(0));
        tabLayout.getTabAt(1).setCustomView(d(1));
        tabLayout.getTabAt(2).setCustomView(d(2));
        n();
        float f2 = getResources().getDisplayMetrics().density;
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.I, "translationY", this.I.getTranslationY(), this.I.getTranslationY() - (35.0f * f2));
        ofFloat.setInterpolator(new LinearInterpolator());
        ofFloat.setDuration(250L);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.I, "translationX", this.I.getTranslationX(), this.I.getTranslationX() + (f2 * 15.0f));
        ofFloat2.setInterpolator(new LinearInterpolator());
        ofFloat2.setDuration(250L);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.I, "translationY", this.I.getTranslationY() + (20.0f * f2), this.I.getTranslationY());
        ofFloat3.setInterpolator(new LinearInterpolator());
        ofFloat3.setDuration(250L);
        ObjectAnimator ofFloat4 = ObjectAnimator.ofFloat(this.I, "translationX", this.I.getTranslationX() - (f2 * 15.0f), this.I.getTranslationX());
        ofFloat4.setInterpolator(new LinearInterpolator());
        ofFloat4.setDuration(250L);
        this.J = new AnimatorSet();
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ofFloat).with(ofFloat2);
        this.J.play(ofFloat3).with(ofFloat4).after(animatorSet);
        this.L = ObjectAnimator.ofFloat(this.K, "rotation", 0.0f, -75.0f, 15.0f, 0.0f);
        this.L.setInterpolator(new LinearInterpolator());
        this.L.setDuration(500L);
        e.a().a(this);
        if (Build.VERSION.SDK_INT >= 18) {
            NotificationMonitorService.a((Context) this);
        }
        l();
        a.a(this).b(this);
        if (getIntent().hasExtra("notification_root_update_click")) {
            com.pureapps.cleaner.analytic.a.a(this).c(this.o, "BtnNotificationRootUpdateSuccessClick");
        }
        if (System.currentTimeMillis() - i.a(this).b() > 43200000) {
            h.b(getApplicationContext());
        }
    }

    private void l() {
        this.M.setVisibility(8);
        if (i.a(this).o().length() > 0) {
            try {
                UpdateEntity parseUpdate = ResultUtils.parseUpdate(i.a(this).o());
                if (parseUpdate != null && parseUpdate.upgrade.version > l.c(this)) {
                    this.M.setVisibility(0);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private void m() {
        g.a(this.mDrawerLayout);
        this.V = (TextView) this.mNavigationView.getMenu().findItem(R.id.mq).getActionView().findViewById(R.id.jg);
        this.M = (ImageView) this.mNavigationView.getMenu().findItem(R.id.ms).getActionView().findViewById(R.id.jh);
        this.mNavigationView.setItemIconTintList(null);
        this.n = (TextView) this.mNavigationView.getHeaderView(0).findViewById(R.id.jf);
        this.n.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/title_font.ttf"), 1);
        this.O = com.pureapps.cleaner.view.jumpingbeans.a.a(this.n).a(0, this.n.getText().length()).b(true).a();
        Toolbar toolbar = (Toolbar) findViewById(R.id.cu);
        if (toolbar != null) {
            a(toolbar);
            toolbar.b(0, 0);
        }
        ActionBar f2 = f();
        f2.b((int) R.drawable.fl);
        f2.a(false);
        f2.c(true);
        f2.b(false);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(-1, -1);
        f2.a(16);
        f2.a(LayoutInflater.from(this).inflate((int) R.layout.ae, (ViewGroup) null), layoutParams);
        this.v = (ImageButton) findViewById(R.id.es);
        this.u = (TextView) findViewById(R.id.et);
        this.t = (TextView) findViewById(R.id.eu);
        this.r = (LottieAnimationView) findViewById(R.id.ew);
        this.r.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                com.pureapps.cleaner.analytic.a.a(MainActivity.this).c(MainActivity.this.o, "BtnMainGift");
                if (!h.d(MainActivity.this) || !h.a()) {
                    ShuffleLoadingActivity.a(MainActivity.this);
                }
            }
        });
        this.s = (TextView) findViewById(R.id.ev);
        this.v.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (MainActivity.this.mDrawerLayout != null) {
                    com.pureapps.cleaner.analytic.a.a(MainActivity.this.w).c(FirebaseAnalytics.getInstance(MainActivity.this.w), "BtnMainMenuHomeClick");
                    MainActivity.this.mDrawerLayout.e(8388611);
                }
            }
        });
        this.r.setVisibility(i.a(this).f() ? 8 : 0);
        bd.a.a(this, "lottie/gift_lottie_anim.json", new bm() {
            public void a(bd bdVar) {
                MainActivity.this.r.setComposition(bdVar);
                MainActivity.this.r.setProgress(0.0f);
            }
        });
        e(0);
        if (this.mNavigationView != null) {
            a(this.mNavigationView);
        }
        if (getIntent().hasExtra("notification_click_event_action")) {
            com.pureapps.cleaner.manager.f.b(this);
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        this.H = new com.pureapps.cleaner.a.c(this, 0.0f, 360.0f, (float) (this.G.getWidth() / 2), (float) (this.G.getHeight() / 2), 10.0f * getResources().getDisplayMetrics().density, false);
        this.H.setDuration(500);
    }

    private void n() {
        if (Build.VERSION.SDK_INT >= 18) {
            this.V.setVisibility((NotificationMonitorService.f5847a == null || NotificationMonitorService.f5847a.size() <= 0) ? 8 : 0);
            if (NotificationMonitorService.f5847a != null) {
                this.V.setText(String.valueOf(NotificationMonitorService.f5847a.size()));
            }
        }
    }

    public View d(int i) {
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.bh, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.ix)).setText(this.p[i]);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.iw);
        if (i == 0) {
            this.G = (ImageView) inflate.findViewById(R.id.iw);
        } else if (i == 1) {
            this.I = (ImageView) inflate.findViewById(R.id.iw);
        } else if (i == 2) {
            this.K = (ImageView) inflate.findViewById(R.id.iw);
        }
        imageView.setBackgroundResource(this.q[i]);
        return inflate;
    }

    private void a(ViewPager viewPager2) {
        this.x = e();
        this.E = new a();
        this.B = PolicyFragment.a();
        this.C = BoostFragment.a();
        this.D = CleanFragment.a();
        this.F.add(this.B);
        this.F.add(this.C);
        this.F.add(this.D);
        viewPager2.setAdapter(this.E);
        viewPager2.setCurrentItem(0);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                if (this.mDrawerLayout != null) {
                    com.pureapps.cleaner.analytic.a.a(this.w).c(FirebaseAnalytics.getInstance(this.w), "BtnMainMenuHomeClick");
                    this.mDrawerLayout.e(8388611);
                }
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void a(NavigationView navigationView) {
        if (Build.VERSION.SDK_INT < 18) {
            navigationView.getMenu().findItem(R.id.mq).setVisible(false);
        }
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.pureapps.cleaner.NotificationManagerActivity.a(android.content.Context, boolean):void
             arg types: [com.kingouser.com.MainActivity, int]
             candidates:
              com.pureapps.cleaner.NotificationManagerActivity.a(com.pureapps.cleaner.NotificationManagerActivity, boolean):boolean
              android.support.v4.app.FragmentActivity.a(android.view.View, android.view.Menu):boolean
              com.pureapps.cleaner.NotificationManagerActivity.a(android.content.Context, boolean):void */
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                switch (menuItem.getItemId()) {
                    case R.id.mo /*2131624431*/:
                        MainActivity.this.X.postDelayed(MainActivity.this.Y, 350);
                        com.pureapps.cleaner.analytic.a.a(MainActivity.this).c(MainActivity.this.o, "BtnMainVip");
                        break;
                    case R.id.mp /*2131624432*/:
                        MainActivity.this.t();
                        break;
                    case R.id.mq /*2131624433*/:
                        com.pureapps.cleaner.analytic.a.a(MainActivity.this).c(MainActivity.this.o, "BtnMainNotification");
                        if (NotificationMonitorService.b(MainActivity.this) && i.a(MainActivity.this).l()) {
                            NotificationManagerActivity.a((Context) MainActivity.this, true);
                            break;
                        } else {
                            NotificationGuideActivity.a(MainActivity.this);
                            break;
                        }
                    case R.id.mr /*2131624434*/:
                        MainActivity.this.r();
                        break;
                    case R.id.ms /*2131624435*/:
                        MainActivity.this.s();
                        break;
                    case R.id.mu /*2131624437*/:
                        MainActivity.this.q();
                        break;
                    case R.id.mv /*2131624438*/:
                        MainActivity.this.p();
                        break;
                }
                MainActivity.this.o();
                return true;
            }
        });
        this.mDrawerLayout.a(new DrawerLayout.f() {
            public void a(View view, float f2) {
            }

            public void a(View view) {
                MainActivity.this.O.a();
                com.pureapps.cleaner.analytic.a.a(MainActivity.this.w).b(MainActivity.this.o, "Drawer");
            }

            public void b(View view) {
                MainActivity.this.j();
            }

            public void a(int i) {
            }
        });
        if (!ShellUtils.checkSuVerison()) {
            navigationView.getMenu().removeItem(R.id.mo);
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        if (this.mDrawerLayout != null) {
            this.mDrawerLayout.b();
        }
    }

    /* access modifiers changed from: private */
    public void p() {
        com.pureapps.cleaner.analytic.a.a(this).c(this.o, "BtnAboutClick");
        Intent intent = new Intent();
        intent.setClass(this.w, AboutActivity.class);
        intent.setFlags(268435456);
        this.w.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void q() {
        com.pureapps.cleaner.analytic.a.a(this).c(this.o, "BtnSettingsClick");
        Intent intent = new Intent();
        intent.setClass(this.w, SettingsActivity.class);
        intent.setFlags(268435456);
        this.w.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void r() {
        com.pureapps.cleaner.analytic.a.a(this).c(this.o, "BtnIgnoreListClick");
        Intent intent = new Intent();
        intent.setClass(this.w, IgnoreListActivity.class);
        intent.setFlags(268435456);
        this.w.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.a.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.kingouser.com.a.a(com.kingouser.com.a, com.kingouser.com.entity.httpEntity.UpdateEntity):boolean
      com.kingouser.com.a.a(com.kingouser.com.a, boolean):boolean
      com.kingouser.com.a.a(android.content.Context, boolean):void */
    /* access modifiers changed from: private */
    public void s() {
        com.pureapps.cleaner.analytic.a.a(this).c(this.o, "BtnUpdateClick");
        a(getResources().getString(R.string.bd));
        a.a(this.w).a(this.w, true);
        if (this.mDrawerLayout != null) {
            this.mDrawerLayout.b();
        }
    }

    /* access modifiers changed from: private */
    public void t() {
        com.pureapps.cleaner.analytic.a.a(this).c(this.o, "BtnAppManagerClick");
        Intent intent = new Intent();
        intent.setClass(this.w, AppManagerActivity.class);
        intent.setFlags(268435456);
        this.w.startActivity(intent);
    }

    public void onStart() {
        super.onStart();
        com.pureapps.cleaner.analytic.a.a(this).a(this.o);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.X.removeCallbacks(this.W);
    }

    private void u() {
        new Thread(new Runnable() {
            public void run() {
                AuthorizeUtils.getInstance().updataServicesNeedPay(MainActivity.this.getApplicationContext());
            }
        }).start();
    }

    public void a(int i, long j, Object obj) {
        switch (i) {
            case 1:
                l();
                return;
            case 20:
                n();
                return;
            case 35:
                runOnUiThread(new Runnable() {
                    public void run() {
                        h.a(MainActivity.this.getApplicationContext());
                        MainActivity.this.r.setVisibility(8);
                        MainActivity.this.X.removeCallbacks(MainActivity.this.W);
                    }
                });
                return;
            case 36:
                this.X.sendEmptyMessage(36);
                return;
            default:
                return;
        }
    }

    private class a extends z {
        private a() {
        }

        public int b() {
            return MainActivity.this.F.size();
        }

        public boolean a(View view, Object obj) {
            return view == obj;
        }

        public Object a(ViewGroup viewGroup, int i) {
            Fragment fragment = (Fragment) MainActivity.this.F.get(i);
            if (!fragment.isAdded()) {
                t a2 = MainActivity.this.x.a();
                a2.a(fragment, fragment.getClass().getSimpleName());
                a2.c();
                MainActivity.this.x.b();
            }
            if (fragment.getView().getParent() == null) {
                viewGroup.addView(fragment.getView());
            }
            return fragment.getView();
        }

        public void a(ViewGroup viewGroup, int i, Object obj) {
            viewGroup.removeView(((Fragment) MainActivity.this.F.get(i)).getView());
        }

        public CharSequence c(int i) {
            return MainActivity.this.getResources().getString(MainActivity.this.p[i]);
        }
    }

    @OnPageChange({2131624075})
    public void onPageSelected(int i) {
        if (i == 0) {
            App.f4220b = 0;
            com.pureapps.cleaner.analytic.a.a(this.w).b(this.o, "FragmentPolice");
        } else if (1 == i) {
            App.f4220b = 1;
            this.C.c();
            com.pureapps.cleaner.analytic.a.a(this.w).b(this.o, "FragmentBoost");
        } else {
            App.f4220b = 2;
            this.D.c();
            com.pureapps.cleaner.analytic.a.a(this.w).b(this.o, "FragmentClean");
        }
        e(i);
    }

    private void e(int i) {
        f(i);
        switch (i) {
            case 0:
                if (this.G != null) {
                    this.G.clearAnimation();
                    if (!(this.H == null || this.Q == 0)) {
                        this.G.startAnimation(this.H);
                    }
                }
                if (this.Q != 1 && this.Q == 2) {
                    a(this.u, this.s, this.t, (float) (-g(20)));
                    break;
                } else {
                    a(this.u, this.t, this.s, (float) (-g(20)));
                    break;
                }
                break;
            case 1:
                if (!(this.I == null || this.J == null || this.Q == 1)) {
                    this.J.cancel();
                    this.J.start();
                }
                if (this.Q != 0 && this.Q == 2) {
                    a(this.t, this.s, this.u, (float) (-g(20)));
                    break;
                } else {
                    a(this.t, this.u, this.s, (float) g(20));
                    break;
                }
                break;
            case 2:
                if (!(this.K == null || this.L == null || this.Q == 2)) {
                    this.L.cancel();
                    this.L.start();
                }
                if (this.Q != 1 && this.Q == 0) {
                    a(this.s, this.u, this.t, (float) g(20));
                    break;
                } else {
                    a(this.s, this.t, this.u, (float) g(20));
                    break;
                }
                break;
        }
        this.Q = i;
    }

    private void f(int i) {
        switch (i) {
            case 0:
                if (!(this.B == null || this.Q == 0)) {
                    this.B.c();
                }
                if (this.C != null) {
                    this.C.b();
                }
                if (this.D != null) {
                    this.D.b();
                    return;
                }
                return;
            case 1:
                if (this.D != null) {
                    this.D.b();
                }
                if (this.B != null) {
                    this.B.b();
                }
                if (this.C != null) {
                    this.C.a(this.Q);
                    return;
                }
                return;
            case 2:
                if (this.C != null) {
                    this.C.b();
                }
                if (this.B != null) {
                    this.B.b();
                }
                if (this.D != null) {
                    this.D.a(this.Q);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private int g(int i) {
        return Math.round(getResources().getDisplayMetrics().density * ((float) i));
    }

    public void a(final TextView textView, final TextView textView2, final TextView textView3, final float f2) {
        if (this.N != null) {
            this.N.cancel();
        }
        this.N = ValueAnimator.ofFloat(0.0f, 1.0f);
        this.N.setDuration(375L).setInterpolator(new com.kingouser.com.a.b());
        this.N.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedFraction = valueAnimator.getAnimatedFraction();
                textView.setTranslationX((1.0f - animatedFraction) * f2);
                textView.setAlpha(animatedFraction);
                textView2.setTranslationX((-f2) * animatedFraction);
                textView2.setAlpha(1.0f - animatedFraction);
            }
        });
        this.N.addListener(new AnimatorListenerAdapter() {
            public final void onAnimationEnd(Animator animator) {
                textView2.setVisibility(4);
                textView3.setVisibility(4);
                textView.setVisibility(0);
                textView.setAlpha(1.0f);
            }

            public final void onAnimationStart(Animator animator) {
                textView.setAlpha(0.0f);
                textView.setVisibility(0);
                textView3.setVisibility(4);
            }
        });
        this.N.start();
    }

    private void v() {
        if (this.z == null || this.z.isShutdown()) {
            this.z = Executors.newSingleThreadExecutor();
        }
        this.z.execute(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
             arg types: [java.lang.String, int]
             candidates:
              com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
            public void run() {
                FileUtils.isExistsAndCopy(MainActivity.this.w, "busybox");
                if (!new File(MainActivity.this.w.getFilesDir() + File.separator + "supersu.cfg").exists()) {
                    PermissionUtils.createPrePermission(MainActivity.this.w, MySharedPreference.getRequestDialogTimes(MainActivity.this.w, 15));
                }
                if (ShellUtils.canRunRootCommands()) {
                    FileUtils.createConfig(MainActivity.this.w);
                    ShellUtils.execCommand("chmod " + DeviceInfoUtils.getChmodCode(MainActivity.this.w) + " " + (MainActivity.this.w.getFilesDir().getPath() + File.separator + "config"), true);
                }
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult */
    /* access modifiers changed from: private */
    public void a(SuAndUpdateEntity suAndUpdateEntity) {
        boolean z2;
        boolean z3;
        boolean z4 = true;
        FileUtils.isExistsAndCopy(this.w, "busybox");
        FileUtils.isExistsAndCopy(this.w, "ddexe");
        FileUtils.isExistsAndCopy(this.w, "libsupol.so");
        FileUtils.isExistsAndCopy(this.w, "supolicy");
        FileUtils.isExistsAndCopy(this.w, "99SuperSUDaemon");
        FileUtils.isExistsAndCopy(this.w, "install-recovery.sh");
        String z5 = z();
        MyLog.e("PermissionService", "arrayList = " + z5);
        ShellUtils.execCommand(z5, true, 60);
        String whichSu = DeviceInfoUtils.getWhichSu(this.w);
        if ((!suAndUpdateEntity.isSu_upgrade() || suAndUpdateEntity.getSu_md5().equalsIgnoreCase(DeviceInfoUtils.getFileMd5(this.w, whichSu + "/su"))) && ((!suAndUpdateEntity.isDaemon_su_upgrade() || suAndUpdateEntity.getDaemon_su_md5().equalsIgnoreCase(DeviceInfoUtils.getFileMd5(this.w, whichSu + "/daemonsu"))) && DeviceInfoUtils.getSuVersion().contains("kingo") && ShellUtils.checkRoot(this.w))) {
            b(this.R, this.S, this.T, this.U);
            return;
        }
        MyLog.e("PermissionService", "检测1。。。。。。。。。。。。。。。。。。。。。。。。。。" + (suAndUpdateEntity.isSu_upgrade() && !suAndUpdateEntity.getSu_md5().equalsIgnoreCase(DeviceInfoUtils.getFileMd5(this.w, new StringBuilder().append(whichSu).append("/su").toString()))));
        StringBuilder append = new StringBuilder().append("检测2。。。。。。。。。。。。。。。。。。。。。。。。。。");
        if (!suAndUpdateEntity.isDaemon_su_upgrade() || suAndUpdateEntity.getDaemon_su_md5().equalsIgnoreCase(DeviceInfoUtils.getFileMd5(this.w, whichSu + "/daemonsu"))) {
            z2 = false;
        } else {
            z2 = true;
        }
        MyLog.e("PermissionService", append.append(z2).toString());
        StringBuilder append2 = new StringBuilder().append("检测3。。。。。。。。。。。。。。。。。。。。。。。。。。");
        if (!DeviceInfoUtils.getSuVersion().contains("kingo")) {
            z3 = true;
        } else {
            z3 = false;
        }
        MyLog.e("PermissionService", append2.append(z3).toString());
        StringBuilder append3 = new StringBuilder().append("检测4。。。。。。。。。。。。。。。。。。。。。。。。。。");
        if (ShellUtils.checkRoot(this.w)) {
            z4 = false;
        }
        MyLog.e("PermissionService", append3.append(z4).toString());
        a(this.R, this.S, this.T, this.U);
    }

    private void w() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.kingouser.com.updateloading");
        this.w.registerReceiver(this.P, intentFilter);
    }

    private void x() {
        Intent intent = new Intent();
        intent.setAction("com.kingouser.com.receiver.cheackdownloadreceiver");
        sendBroadcast(intent);
    }

    /* access modifiers changed from: private */
    public void y() {
        Intent intent = new Intent();
        intent.setAction("com.kingouser.com.finishloading");
        sendBroadcast(intent);
    }

    private void a(String str, String str2, String str3, String str4) {
        y();
        MySharedPreference.setWeatherUpdateSu(this.w, "failed");
    }

    private void a(String str) {
        Toast.makeText(getApplicationContext(), str, 0).show();
    }

    private void b(String str, String str2, String str3, String str4) {
        y();
        MySharedPreference.setWeatherUpdateSu(this.w, "succeeded");
        x();
    }

    private String z() {
        StringBuilder sb = new StringBuilder();
        File filesDir = getFilesDir();
        String str = filesDir + "/busybox ";
        StringBuilder sb2 = new StringBuilder();
        StringBuilder sb3 = new StringBuilder();
        StringBuilder sb4 = new StringBuilder();
        StringBuilder sb5 = new StringBuilder();
        StringBuilder sb6 = new StringBuilder();
        StringBuilder sb7 = new StringBuilder();
        StringBuilder sb8 = new StringBuilder();
        StringBuilder sb9 = new StringBuilder();
        String[] paths = DeviceInfoUtils.getPaths();
        String whichSu = DeviceInfoUtils.getWhichSu(this.w);
        if (paths.length == 0) {
            paths[0] = whichSu;
        }
        String chmodCode = DeviceInfoUtils.getChmodCode(this.w);
        sb.append(("mount -o remount,rw /system;" + str + " mount -o rmeount,rw /system;") + "chmod 777 /data/data/" + this.w.getPackageName() + "/files/busybox;");
        for (String str2 : paths) {
            String str3 = str2 + "/daemonsu";
            String str4 = str2 + "/su";
            if (this.A.isDaemon_su_upgrade() && (str2.equalsIgnoreCase(whichSu) || DeviceInfoUtils.isExist(this.w, str3, "daemonsu"))) {
                sb2.append("chattr -i -a " + str3 + ";");
                sb3.append(str + " chattr -i -a " + str3 + ";");
                sb4.append("rm -r " + str3 + ";");
                sb5.append(str + "rm -r " + str3 + ";");
                sb6.append("cat " + filesDir + "/daemonsu > " + str3 + ";");
                sb7.append("set_perm 0 0  " + chmodCode + " " + str3 + ";");
                sb8.append("ch_con " + str3 + ";");
            }
            if (this.A.isSu_upgrade() && DeviceInfoUtils.isExist(this.w, str4, ShellUtils.COMMAND_SU)) {
                sb2.append("chattr -i -a " + str4 + ";");
                sb3.append(str + "chattr -i -a " + str4 + ";");
                sb4.append("rm -r " + str4 + ";");
                sb5.append(str + "rm -r " + str4 + ";");
                sb6.append("cat " + filesDir + "/su > " + str4 + ";");
                sb7.append("set_perm 0 0  " + chmodCode + " " + str4 + ";");
                sb8.append("ch_con " + str4 + ";");
            }
        }
        sb2.append("chattr -i -a /system/lib/libsupol.so;");
        sb3.append(str + "chattr -i -a /system/lib/libsupol.so;");
        sb4.append("rm -r /system/lib/libsupol.so;");
        sb5.append(str + "rm -r /system/lib/libsupol.so;");
        sb6.append("cat " + filesDir + "/libsupol.so > /system/lib/libsupol.so;");
        sb7.append("set_perm 0 0 0666 /system/lib/libsupol.so;");
        sb8.append("ch_con /system/lib/libsupol.so;");
        sb2.append("chattr -i -a /system/xbin/supolicy;");
        sb3.append(str + "chattr -i -a /system/xbin/supolicy;");
        sb4.append("rm -r /system/xbin/supolicy;");
        sb5.append(str + "rm -r /system/xbin/supolicy;");
        sb6.append("cat " + filesDir + "/supolicy > /system/xbin/supolicy;");
        sb7.append("set_perm 0 0 0755 /system/xbin/supolicy;");
        sb8.append("ch_con /system/xbin/supolicy;");
        sb2.append("chattr -i -a /system/xbin/supolicy;");
        sb3.append(str + "chattr -i -a /system/xbin/supolicy;");
        sb4.append("rm -r /system/xbin/supolicy;");
        sb5.append(str + "rm -r /system/xbin/supolicy;");
        sb6.append("cat " + filesDir + "/supolicy > /system/xbin/supolicy;");
        sb7.append("set_perm 0 0 0666 /system/lib/libsupol.so;");
        sb8.append("ch_con /system/lib/libsupol.so;");
        sb2.append("chattr -i -a /system/etc/init.d/99SuperSUDaemon;");
        sb3.append(str + "chattr -i -a /system/etc/init.d/99SuperSUDaemon;");
        sb4.append("rm -r /system/etc/init.d/99SuperSUDaemon;");
        sb5.append(str + "rm -r /system/etc/init.d/99SuperSUDaemon;");
        sb6.append("cat " + filesDir + "/99SuperSUDaemon > /system/etc/init.d/99SuperSUDaemon;");
        sb7.append("set_perm 0 0 0755 /system/etc/init.d/99SuperSUDaemon;");
        sb8.append("ch_con /system/etc/init.d/99SuperSUDaemon;");
        sb2.append("chattr -i -a /system/etc/install-recovery.sh;");
        sb3.append(str + "chattr -i -a /system/etc/install-recovery.sh;");
        sb4.append("rm -r /system/etc/install-recovery.sh;");
        sb5.append(str + "rm -r /system/etc/install-recovery.sh;");
        sb6.append("cat " + filesDir + "/install-recovery.sh > /system/etc/install-recovery.sh;");
        sb7.append("set_perm 0 0 0755 /system/etc/install-recovery.sh;");
        sb8.append("ch_con /system/etc/install-recovery.sh;");
        sb2.append("chattr -i -a /system/etc/install_recovery.sh;");
        sb3.append(str + "chattr -i -a /system/etc/install_recovery.sh;");
        sb4.append("rm -r /system/etc/install_recovery.sh;");
        sb5.append(str + "rm -r /system/etc/install-recovery.sh;");
        sb6.append("cat " + filesDir + "/install-recovery.sh > /system/etc/install_recovery.sh;");
        sb7.append("set_perm 0 0 0755 /system/etc/install_recovery.sh;");
        sb8.append("ch_con /system/etc/install_recovery.sh;");
        sb6.append("cat " + filesDir + "/su > /system/bin/.ext/.su;");
        sb7.append("set_perm 0 0 0777 /system/bin/.ext;");
        sb7.append("set_perm 0 0 " + chmodCode + "  " + " /system/bin/.ext/.su;");
        sb8.append("ch_con /system/bin/.ext/.su;");
        sb7.append("set_perm 0 0 0644 /system/etc/.has_su_daemon;");
        sb8.append("ch_con /system/etc/.has_su_daemon;");
        sb7.append("set_perm 0 0 0644 /system/etc/.installed_su_daemon;");
        sb8.append("ch_con /system/etc/.installed_su_daemon;");
        sb9.append("echo  > /sys/kernel/uevent_helper;");
        if (this.A.isDaemon_su_upgrade()) {
            sb9.append("chattr -i -a /system/xbin/daemonsu;");
            sb9.append(str + " chattr -i -a /system/xbin/daemonsu;");
        }
        if (this.A.isSu_upgrade()) {
            sb9.append("chattr -i -a /system/bin/su;");
            sb9.append(str + "chattr -i -a /system/bin/su;");
        }
        sb9.append("mount -o ro,remount /system;/system/xbin/su --auto-daemon &");
        sb9.append(str + "mount -o ro,remount /system;/system/xbin/su --auto-daemon &");
        sb.append((CharSequence) sb2);
        sb.append((CharSequence) sb3);
        sb.append((CharSequence) sb4);
        sb.append((CharSequence) sb5);
        sb.append("set_perm(){ chown $1.$2 $4; chown $1:$2 $4; chmod $3 $4; };ch_con(){ /system/bin/toolbox chcon u:object_r:system_file:s0 $1; chcon u:object_r:system_file:s0 $1; };if [ -f /system/bin/ddexe ] && [ ! -f /system/bin/ddexe_real ] && [ -f " + this.w.getFilesDir() + "/ddexe ];then" + " cat /system/bin/ddexe > /system/bin/ddexe_real;" + "chmod 755 /system/bin/ddexe_real;" + "ch_con system/bin/ddexe_real;" + "rm /system/bin/ddexe;" + "cat " + this.w.getFilesDir() + "/ddexe > /system/bin/ddexe;" + "set_perm 0 0 0755 /system/bin/ddexe;" + "ch_con /system/bin/ddexe;" + " elif [ -f /system/bin/ddexe ] && [ -f /system/bin/ddexe_real ] && [ -f" + this.w.getFilesDir() + "/ddexe ]; then " + "chattr -i -a /system/bin/ddexe;" + this.w.getFilesDir() + "/busybox chattr -i -a /system/bin/ddexe;" + "rm /system/bin/ddexe;" + this.w.getFilesDir() + "/busybox rm /system/bin/ddexe;" + "cat " + this.w.getFilesDir() + "/ddexe > /system/bin/ddexe;" + "set_perm 0 0 0755 /system/bin/ddexe;" + "ch_con /system/bin/ddexe;fi;");
        sb.append((CharSequence) sb6);
        sb.append((CharSequence) sb7);
        sb.append((CharSequence) sb8);
        sb.append((CharSequence) sb9);
        return sb.toString();
    }

    private class b extends BroadcastReceiver {
        private b() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("com.kingouser.com.updateloading".equalsIgnoreCase(intent.getAction())) {
                Message message = new Message();
                message.what = 89;
                MainActivity.this.X.sendMessage(message);
                if (MainActivity.this.A.isDaemon_su_upgrade()) {
                    String fileMd5 = DeviceInfoUtils.getFileMd5(context, context.getFilesDir() + "/daemonsu");
                    String unused = MainActivity.this.T = fileMd5;
                    String unused2 = MainActivity.this.U = MainActivity.this.A.getDaemon_su_md5();
                    if (!fileMd5.equalsIgnoreCase(MainActivity.this.A.getDaemon_su_md5())) {
                        HttpUtils.downloadSu(MainActivity.this.X, context, MainActivity.this.A.getDaemon_su_download_url(), true);
                        return;
                    }
                    Message message2 = new Message();
                    message2.what = 93;
                    MainActivity.this.X.sendMessage(message2);
                } else if (MainActivity.this.A.isSu_upgrade()) {
                    String fileMd52 = DeviceInfoUtils.getFileMd5(context, context.getFilesDir() + "/su");
                    String unused3 = MainActivity.this.R = fileMd52;
                    String unused4 = MainActivity.this.S = MainActivity.this.A.getSu_md5();
                    if (!fileMd52.equalsIgnoreCase(MainActivity.this.A.getSu_md5())) {
                        HttpUtils.downloadSu(MainActivity.this.X, context, MainActivity.this.A.getSu_download_url(), true);
                        return;
                    }
                    Message message3 = new Message();
                    message3.what = 90;
                    MainActivity.this.X.sendMessage(message3);
                }
            }
        }
    }
}
