package com.kingouser.com;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class SuperUserOpenServiceActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private SuperUserOpenServiceActivity f4136a;

    /* renamed from: b  reason: collision with root package name */
    private View f4137b;

    public SuperUserOpenServiceActivity_ViewBinding(final SuperUserOpenServiceActivity superUserOpenServiceActivity, View view) {
        this.f4136a = superUserOpenServiceActivity;
        superUserOpenServiceActivity.mTitleText = (TextView) Utils.findRequiredViewAsType(view, R.id.m5, "field 'mTitleText'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.m6, "method 'OnClick'");
        this.f4137b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                superUserOpenServiceActivity.OnClick(view);
            }
        });
    }

    public void unbind() {
        SuperUserOpenServiceActivity superUserOpenServiceActivity = this.f4136a;
        if (superUserOpenServiceActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4136a = null;
        superUserOpenServiceActivity.mTitleText = null;
        this.f4137b.setOnClickListener(null);
        this.f4137b = null;
    }
}
