package com.kingouser.com;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.widget.TextView;
import butterknife.BindView;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.bd;
import com.airbnb.lottie.bm;
import com.pureapps.cleaner.util.f;

public class VipMemberActivity extends BaseActivity {
    @BindView(R.id.ew)
    LottieAnimationView anim_view;
    private Context n;
    @BindView(R.id.ex)
    TextView tv_vipmembership;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.af);
        ActionBar f2 = f();
        f.a("supportActionBar is null " + (f2 == null));
        if (f2 != null) {
            f2.a(true);
        }
        this.n = getApplicationContext();
        j();
        bd.a.a(this, "lottie/like.json", new bm() {
            public void a(bd bdVar) {
                VipMemberActivity.this.anim_view.setComposition(bdVar);
                VipMemberActivity.this.anim_view.setProgress(0.0f);
            }
        });
        this.anim_view.c();
    }

    private void j() {
        Typeface createFromAsset = Typeface.createFromAsset(getAssets(), "fonts/booster_number_font.otf");
        this.tv_vipmembership.setTypeface(createFromAsset);
        this.tv_vipmembership.setTypeface(createFromAsset);
    }

    public boolean g() {
        finish();
        return super.g();
    }
}
