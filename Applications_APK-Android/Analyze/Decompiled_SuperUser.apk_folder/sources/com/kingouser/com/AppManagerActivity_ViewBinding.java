package com.kingouser.com;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class AppManagerActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private AppManagerActivity f3933a;

    /* renamed from: b  reason: collision with root package name */
    private View f3934b;

    /* renamed from: c  reason: collision with root package name */
    private ViewPager.e f3935c;

    public AppManagerActivity_ViewBinding(final AppManagerActivity appManagerActivity, View view) {
        this.f3933a = appManagerActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.cw, "field 'mAppViewPager' and method 'onPageSelected'");
        appManagerActivity.mAppViewPager = (ViewPager) Utils.castView(findRequiredView, R.id.cw, "field 'mAppViewPager'", ViewPager.class);
        this.f3934b = findRequiredView;
        this.f3935c = new ViewPager.e() {
            public void onPageSelected(int i) {
                appManagerActivity.onPageSelected(i);
            }

            public void onPageScrolled(int i, float f2, int i2) {
            }

            public void onPageScrollStateChanged(int i) {
            }
        };
        ((ViewPager) findRequiredView).a(this.f3935c);
        appManagerActivity.mTabLayout = (TabLayout) Utils.findRequiredViewAsType(view, R.id.cv, "field 'mTabLayout'", TabLayout.class);
    }

    public void unbind() {
        AppManagerActivity appManagerActivity = this.f3933a;
        if (appManagerActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f3933a = null;
        appManagerActivity.mAppViewPager = null;
        appManagerActivity.mTabLayout = null;
        ((ViewPager) this.f3934b).b(this.f3935c);
        this.f3935c = null;
        this.f3934b = null;
    }
}
