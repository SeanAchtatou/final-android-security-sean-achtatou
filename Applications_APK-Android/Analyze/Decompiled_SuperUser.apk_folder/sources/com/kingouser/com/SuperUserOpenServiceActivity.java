package com.kingouser.com;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.android.gms.common.ConnectionResult;
import com.pureapps.cleaner.view.jumpingbeans.a;

public class SuperUserOpenServiceActivity extends BaseActivity {
    @BindView(R.id.m5)
    TextView mTitleText;
    /* access modifiers changed from: private */
    public a n;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.cs);
        SpannableString spannableString = new SpannableString(getString(R.string.b0));
        spannableString.setSpan(new com.pureapps.cleaner.view.a(this, "title_font.ttf"), 0, spannableString.length(), 33);
        ActionBar f2 = f();
        f2.a(spannableString);
        f2.a(0.0f);
        this.n = a.a(this.mTitleText).a(0, this.mTitleText.getText().length()).b(true).a(true).a((int) ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED).a();
        this.mTitleText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (SuperUserOpenServiceActivity.this.mTitleText.isShown()) {
                    SuperUserOpenServiceActivity.this.n.a();
                }
            }
        });
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    @OnClick({2131624412})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.m6 /*2131624412*/:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.n != null) {
            this.n.b();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.f8342b, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.mw) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
