package com.kingouser.com.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.kingouser.com.util.SPUtil;
import com.pureapps.cleaner.util.f;

public class ScreenReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
            f.a("锁屏了");
            SPUtil.getInstant(context).save("SCREEN_OFF", true);
        }
        if (intent.getAction().equals("android.intent.action.USER_PRESENT")) {
            f.a("点亮了 SCREEN_OFF ");
            SPUtil.getInstant(context).save("SCREEN_OFF", false);
        }
    }
}
