package com.kingouser.com.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.kingouser.com.util.MyLog;
import com.kingouser.com.util.Settings;
import com.pureapps.cleaner.service.BackService;
import com.pureapps.cleaner.service.CommonService;

public class SuCheckerReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction()) || "internal.superuser.BOOT_TEST".equals(intent.getAction())) {
                int checkSuQuietCounter = Settings.getCheckSuQuietCounter(context);
                if (checkSuQuietCounter > 0) {
                    MyLog.i("KingoUser", "Not bothering user... su counter set.");
                    Settings.setCheckSuQuietCounter(context, checkSuQuietCounter - 1);
                    return;
                }
                BackService.a(context.getApplicationContext());
                CommonService.a(context, "bootComplete");
            } else if ("internal.superuser.ACTION_CHECK_DELETED".equals(intent.getAction())) {
                MyLog.i("KingoUser", "Will not bother the user in the future... su counter set.");
                Settings.setCheckSuQuietCounter(context, 3);
            }
        }
    }
}
