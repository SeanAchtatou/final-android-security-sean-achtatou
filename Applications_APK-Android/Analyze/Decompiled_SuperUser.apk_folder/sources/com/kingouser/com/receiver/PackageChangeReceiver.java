package com.kingouser.com.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.text.TextUtils;
import com.duapps.ad.base.DuAdNetwork;
import com.kingouser.com.db.KingoDatabaseHelper;
import com.kingouser.com.db.a;
import com.kingouser.com.entity.UidPolicy;
import com.kingouser.com.util.FileUtils;
import com.kingouser.com.util.MyLog;
import com.kingouser.com.util.MySharedPreference;
import com.kingouser.com.util.PermissionUtils;
import com.kingouser.com.util.RC4EncodeUtils;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.ArrayList;
import java.util.Iterator;

public class PackageChangeReceiver extends BroadcastReceiver {
    public void onReceive(final Context context, Intent intent) {
        if (intent != null) {
            try {
                DuAdNetwork.onPackageAddReceived(context, intent);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
                final String substring = intent.getDataString().substring(8);
                if (!TextUtils.isEmpty(substring) && !"com.kingouser.com".equalsIgnoreCase(substring)) {
                    new Thread() {
                        public void run() {
                            ArrayList<UidPolicy> a2 = KingoDatabaseHelper.a(context);
                            if (FileUtils.checkFileExist(context, context.getFilesDir() + "/" + "supersu.cfg")) {
                                PermissionUtils.RemoveAppFromCfg(context, substring);
                            } else {
                                PermissionUtils.createPrePermission(context, MySharedPreference.getRequestDialogTimes(context, 15));
                            }
                            a.a(context, substring, "user_application");
                            a.a(context, substring, "system_application");
                            Iterator<UidPolicy> it = a2.iterator();
                            while (it.hasNext()) {
                                UidPolicy next = it.next();
                                if (!TextUtils.isEmpty(next.packageName) && next.uid != 2000) {
                                    try {
                                        if (substring.equalsIgnoreCase(next.packageName)) {
                                            KingoDatabaseHelper.b(context, next);
                                            PackageChangeReceiver.this.a(context);
                                            MyLog.e("PermissionService", "移除数据。。。。。。。。。。。。。。。。。。。");
                                            return;
                                        }
                                    } catch (Exception e2) {
                                        e2.printStackTrace();
                                    }
                                }
                            }
                        }
                    }.start();
                }
            } else if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
                final String substring2 = intent.getDataString().substring(8);
                if (!TextUtils.isEmpty(substring2) && !"com.kingouser.com".equalsIgnoreCase(substring2)) {
                    new Thread(new Runnable() {
                        public void run() {
                            boolean z = true;
                            try {
                                ApplicationInfo applicationInfo = context.getPackageManager().getPackageInfo(substring2, 0).applicationInfo;
                                if ((applicationInfo.flags & FileUtils.FileMode.MODE_IWUSR) == 0) {
                                    if ((applicationInfo.flags & 1) != 0) {
                                        z = false;
                                    }
                                }
                                Intent intent = new Intent();
                                intent.setAction("com.kingouser.com.installappreceiver.succeed");
                                context.sendBroadcast(intent);
                                try {
                                    String decry_RC4 = RC4EncodeUtils.decry_RC4(com.kingouser.com.util.FileUtils.readFile(context.getFilesDir() + "/app/list"), "s7JK&@NL");
                                    if (z && !decry_RC4.contains(substring2)) {
                                        com.kingouser.com.util.FileUtils.write(context.getFilesDir() + "/app/list", RC4EncodeUtils.encry_RC4_string(decry_RC4 + "," + substring2.trim(), "s7JK&@NL"), false);
                                        com.kingouser.com.util.FileUtils.write(context.getFilesDir() + "/app/" + System.currentTimeMillis(), RC4EncodeUtils.encry_RC4_string(substring2.trim(), "s7JK&@NL"), false);
                                    }
                                } catch (Exception e2) {
                                    if (z) {
                                        com.kingouser.com.util.FileUtils.write(context.getFilesDir() + "/app/" + System.currentTimeMillis(), RC4EncodeUtils.encry_RC4_string(substring2.trim(), "s7JK&@NL"), false);
                                    }
                                }
                            } catch (Exception e3) {
                                e3.printStackTrace();
                            }
                        }
                    }).start();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.kingouser.com.reload.policies");
        context.sendBroadcast(intent);
    }
}
