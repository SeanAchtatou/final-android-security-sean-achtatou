package com.kingouser.com;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.kingouser.com.util.DeviceInfoUtils;

public class SoftwareUpdateActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private String f4102a;

    /* renamed from: b  reason: collision with root package name */
    private String f4103b;
    @BindView(R.id.ff)
    Button btCancel;
    @BindView(R.id.lu)
    Button btUpdate;

    /* renamed from: c  reason: collision with root package name */
    private String f4104c;
    @BindView(R.id.gp)
    TextView tvContent;
    @BindView(R.id.fd)
    TextView tvTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.co);
        ButterKnife.bind(this);
        c();
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    private void a() {
        String fileMd5 = DeviceInfoUtils.getFileMd5(getApplicationContext(), getApplicationContext().getFilesDir() + "/KingoUser.apk");
        if (TextUtils.isEmpty(fileMd5) || !fileMd5.equalsIgnoreCase(this.f4104c)) {
            finish();
        }
    }

    private void b() {
        this.tvContent.setText(this.f4102a);
        this.tvTitle.setText(getResources().getString(R.string.fc, this.f4103b));
    }

    @OnClick({2131624163, 2131624400})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.ff /*2131624163*/:
                finish();
                return;
            case R.id.lu /*2131624400*/:
                Intent intent = new Intent();
                intent.setAction("com.kingouser.com.receiver");
                sendBroadcast(intent);
                finish();
                return;
            default:
                return;
        }
    }

    private void c() {
        Intent intent = getIntent();
        this.f4102a = intent.getStringExtra("wifi_state");
        this.f4103b = intent.getStringExtra("version");
        this.f4104c = intent.getStringExtra("apk_md5");
        new Intent().setAction("com.kingouser.com.cancel.update.notification");
        sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 || i == 3) {
        }
        return super.onKeyDown(i, keyEvent);
    }
}
