package com.kingouser.com.util;

import com.lody.virtual.helper.utils.FileUtils;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

public class StreamUtility {
    private static final String LOGTAG = StreamUtility.class.getSimpleName();

    public static void fastChannelCopy(ReadableByteChannel readableByteChannel, WritableByteChannel writableByteChannel) {
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(131072);
        while (readableByteChannel.read(allocateDirect) != -1) {
            allocateDirect.flip();
            writableByteChannel.write(allocateDirect);
            allocateDirect.compact();
        }
        allocateDirect.flip();
        while (allocateDirect.hasRemaining()) {
            writableByteChannel.write(allocateDirect);
        }
    }

    public static void copyStream(InputStream inputStream, OutputStream outputStream) {
        fastChannelCopy(Channels.newChannel(inputStream), Channels.newChannel(outputStream));
    }

    public static byte[] readToEndAsArray(InputStream inputStream) {
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = dataInputStream.read(bArr);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                inputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
        }
    }

    public static void eat(InputStream inputStream) {
        do {
        } while (inputStream.read(new byte[FileUtils.FileMode.MODE_ISGID]) != -1);
    }

    public static String readToEnd(InputStream inputStream) {
        return new String(readToEndAsArray(inputStream));
    }

    public static String readFile(String str) {
        return readFile(new File(str));
    }

    public static String readFile(File file) {
        byte[] bArr = new byte[((int) file.length())];
        new DataInputStream(new FileInputStream(file)).readFully(bArr);
        return new String(bArr);
    }

    public static void writeFile(File file, String str) {
        writeFile(file.getAbsolutePath(), str);
    }

    public static void writeFile(String str, String str2) {
        File file = new File(str);
        file.getParentFile().mkdirs();
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));
        dataOutputStream.write(str2.getBytes());
        dataOutputStream.close();
    }
}
