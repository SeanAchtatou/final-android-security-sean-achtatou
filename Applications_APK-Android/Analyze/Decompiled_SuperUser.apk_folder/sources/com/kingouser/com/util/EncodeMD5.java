package com.kingouser.com.util;

public class EncodeMD5 {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v10, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getMD5To32String(java.lang.String r5) {
        /*
            r0 = 0
            java.lang.String r1 = "MD5"
            java.security.MessageDigest r1 = java.security.MessageDigest.getInstance(r1)     // Catch:{ Exception -> 0x003b }
            byte[] r2 = r5.getBytes()     // Catch:{ Exception -> 0x003b }
            r1.update(r2)     // Catch:{ Exception -> 0x003b }
            byte[] r2 = r1.digest()     // Catch:{ Exception -> 0x003b }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x003b }
            java.lang.String r1 = ""
            r3.<init>(r1)     // Catch:{ Exception -> 0x003b }
            r1 = r0
        L_0x001a:
            int r0 = r2.length     // Catch:{ Exception -> 0x003b }
            if (r1 >= r0) goto L_0x0036
            byte r0 = r2[r1]     // Catch:{ Exception -> 0x003b }
            if (r0 >= 0) goto L_0x0023
            int r0 = r0 + 256
        L_0x0023:
            r4 = 16
            if (r0 >= r4) goto L_0x002b
            r4 = 0
            r3.append(r4)     // Catch:{ Exception -> 0x003b }
        L_0x002b:
            java.lang.String r0 = java.lang.Integer.toHexString(r0)     // Catch:{ Exception -> 0x003b }
            r3.append(r0)     // Catch:{ Exception -> 0x003b }
            int r0 = r1 + 1
            r1 = r0
            goto L_0x001a
        L_0x0036:
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x003b }
        L_0x003a:
            return r0
        L_0x003b:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = ""
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.EncodeMD5.getMD5To32String(java.lang.String):java.lang.String");
    }
}
