package com.kingouser.com.util;

import android.content.Context;
import com.kingouser.com.R;
import java.text.DecimalFormat;

public class FormatUtils {
    public static String formatBytesInByte2(long j) {
        if (j > 1073741824) {
            return new DecimalFormat("#0.0").format((double) (((float) j) / 1.07374182E9f)) + "GB";
        } else if (j > 1048576) {
            return new DecimalFormat("#0").format((double) (((float) j) / 1048576.0f)) + "MB";
        } else if (j > 1024) {
            return new DecimalFormat("#0").format(j / 1024) + "KB";
        } else {
            return new DecimalFormat("#0").format(j) + " B";
        }
    }

    public static String formatBytesInByte(long j) {
        DecimalFormat decimalFormat = new DecimalFormat("#0.0");
        if (j > 1073741824) {
            return decimalFormat.format((double) (((float) j) / 1.07374182E9f)) + "GB";
        }
        if (j > 1048576) {
            return decimalFormat.format((double) (((float) j) / 1048576.0f)) + "MB";
        }
        if (j > 1024) {
            return decimalFormat.format(j / 1024) + "KB";
        }
        return decimalFormat.format(j) + "B";
    }

    public static String formatBytesInRoundByte(long j) {
        DecimalFormat decimalFormat = new DecimalFormat("#0");
        if (j > 1073741824) {
            return decimalFormat.format((double) (((float) j) / 1.07374182E9f)) + "GB";
        }
        if (j > 1048576) {
            return decimalFormat.format((double) (((float) j) / 1048576.0f)) + "MB";
        }
        if (j > 1024) {
            return decimalFormat.format(j / 1024) + "KB";
        }
        return decimalFormat.format(j) + "B";
    }

    public static boolean compareSize(long j, long j2) {
        int parseInt;
        int parseInt2;
        DecimalFormat decimalFormat = new DecimalFormat("#0");
        if (j > 1073741824) {
            parseInt = Integer.parseInt(decimalFormat.format((double) (((float) j) / 1.07374182E9f)));
        } else if (j > 1048576) {
            parseInt = Integer.parseInt(decimalFormat.format((double) (((float) j) / 1048576.0f)));
        } else if (j > 1024) {
            parseInt = Integer.parseInt(decimalFormat.format(j / 1024));
        } else {
            parseInt = Integer.parseInt(decimalFormat.format(j));
        }
        if (j2 > 1073741824) {
            parseInt2 = Integer.parseInt(decimalFormat.format((double) (((float) j2) / 1.07374182E9f)));
        } else if (j2 > 1048576) {
            parseInt2 = Integer.parseInt(decimalFormat.format((double) (((float) j2) / 1048576.0f)));
        } else if (j2 > 1024) {
            parseInt2 = Integer.parseInt(decimalFormat.format(j2 / 1024));
        } else {
            parseInt2 = Integer.parseInt(decimalFormat.format(j2));
        }
        if (parseInt == parseInt2) {
            return true;
        }
        return false;
    }

    public static String formatGBBytesInByte(long j) {
        return new DecimalFormat("#0.00").format((double) (((float) j) / 1024.0f)) + "GB";
    }

    public static float formatFloatRound(float f2) {
        return ((float) Math.round(f2 * 10.0f)) / 10.0f;
    }

    public static String formatDateByLong(Context context, long j) {
        return context.getString(R.string.e5, Integer.valueOf((int) (j / 86400000)));
    }
}
