package com.kingouser.com.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class QueryString {
    private StringBuffer query = new StringBuffer();
    private String url;

    public QueryString(String str) {
        this.url = str;
    }

    public QueryString(String str, String str2) {
        encode(str, str2);
    }

    public void add(String str, String str2) {
        this.query.append("&");
        encode(str, str2);
    }

    private void encode(String str, String str2) {
        try {
            this.query.append(URLEncoder.encode(str, "UTF-8"));
            this.query.append("=");
            this.query.append(URLEncoder.encode(str2, "UTF-8"));
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException("Broken VM does not support UTF-8");
        }
    }

    public String getQuery() {
        return this.query.toString();
    }

    public String toString() {
        return this.url + "?" + getQuery();
    }
}
