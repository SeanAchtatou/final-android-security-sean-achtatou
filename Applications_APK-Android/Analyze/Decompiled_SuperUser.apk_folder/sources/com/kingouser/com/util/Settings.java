package com.kingouser.com.util;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.db.KingouserDatabaseHelper;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;

public class Settings {
    public static final int AUTOMATIC_RESPONSE_ALLOW = 1;
    public static final int AUTOMATIC_RESPONSE_DEFAULT = 0;
    public static final int AUTOMATIC_RESPONSE_DENY = 2;
    public static final int AUTOMATIC_RESPONSE_PROMPT = 0;
    private static final String CHECK_SU_QUIET = "check_su_quiet";
    private static final String KEY_AUTOMATIC_RESPONSE = "automatic_response";
    private static final String KEY_LOGGING = "logging";
    private static final String KEY_NOTIFICATION = "notification";
    public static final String KEY_PIN = "pin";
    private static final String KEY_REQUIRE_PREMISSION = "require_permission";
    private static final String KEY_THEME = "theme";
    private static final String KEY_TIMEOUT = "timeout";
    public static final int MULTIUSER_MODE_NONE = 3;
    public static final int MULTIUSER_MODE_OWNER_MANAGED = 1;
    public static final int MULTIUSER_MODE_OWNER_ONLY = 0;
    public static final int MULTIUSER_MODE_USER = 2;
    private static final String MULTIUSER_VALUE_OWNER_MANAGED = "managed";
    private static final String MULTIUSER_VALUE_OWNER_ONLY = "owner";
    private static final String MULTIUSER_VALUE_USER = "user";
    public static final int NOTIFICATION_TYPE_DEFAULT = 1;
    public static final int NOTIFICATION_TYPE_NONE = 0;
    public static final int NOTIFICATION_TYPE_NOTIFICATION = 2;
    public static final int NOTIFICATION_TYPE_TOAST = 1;
    public static final int REQUEST_TIMEOUT_DEFAULT = 30;
    public static final int SUPERUSER_ACCESS_ADB_ONLY = 2;
    public static final int SUPERUSER_ACCESS_APPS_AND_ADB = 3;
    public static final int SUPERUSER_ACCESS_APPS_ONLY = 1;
    public static final int SUPERUSER_ACCESS_DISABLED = 0;
    public static final int THEME_DARK = 1;
    public static final int THEME_LIGHT = 0;
    Context mContext;
    SQLiteDatabase mDatabase;

    public static void setString(Context context, String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("key", str);
        contentValues.put(FirebaseAnalytics.Param.VALUE, str2);
        try {
            new KingouserDatabaseHelper(context).getWritableDatabase().replace("settings", null, contentValues);
        } catch (Exception e2) {
        }
    }

    public static String getString(Context context, String str) {
        return getString(context, str, null);
    }

    public static String getString(Context context, String str, String str2) {
        Cursor query = new KingouserDatabaseHelper(context).getReadableDatabase().query("settings", new String[]{FirebaseAnalytics.Param.VALUE}, "key='" + str + "'", null, null, null, null);
        try {
            if (query.moveToNext()) {
                str2 = query.getString(0);
            } else {
                query.close();
            }
            return str2;
        } finally {
            query.close();
        }
    }

    public static void setInt(Context context, String str, int i) {
        setString(context, str, Integer.valueOf(i).toString());
    }

    public static int getInt(Context context, String str, int i) {
        try {
            return Integer.parseInt(getString(context, str, null));
        } catch (Exception e2) {
            return i;
        }
    }

    public static void setLong(Context context, String str, long j) {
        setString(context, str, Long.valueOf(j).toString());
    }

    public static long getLong(Context context, String str, long j) {
        try {
            return Long.parseLong(getString(context, str, null));
        } catch (Exception e2) {
            return j;
        }
    }

    public static void setBoolean(Context context, String str, boolean z) {
        setString(context, str, Boolean.valueOf(z).toString());
    }

    public static boolean getBoolean(Context context, String str, boolean z) {
        try {
            return Boolean.parseBoolean(getString(context, str, Boolean.valueOf(z).toString()));
        } catch (Exception e2) {
            e2.printStackTrace();
            return z;
        }
    }

    public static boolean getLogging(Context context) {
        return getBoolean(context, KEY_LOGGING, true);
    }

    public static void setLogging(Context context, boolean z) {
        setBoolean(context, KEY_LOGGING, z);
    }

    public static int getRequestTimeout(Context context) {
        return getInt(context, KEY_TIMEOUT, 30);
    }

    public static void setTimeout(Context context, int i) {
        setInt(context, KEY_TIMEOUT, i);
    }

    public static int getNotificationType(Context context) {
        switch (getInt(context, "notification", 1)) {
            case 0:
                return 0;
            case 1:
            default:
                return 1;
            case 2:
                return 2;
        }
    }

    public static void setNotificationType(Context context, int i) {
        setInt(context, "notification", i);
    }

    public static final boolean isPinProtected(Context context) {
        return getString(context, KEY_PIN) != null;
    }

    private static String digest(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return Base64.encodeToString(MessageDigest.getInstance("MD5").digest(str.getBytes()), 0);
        } catch (Exception e2) {
            return str;
        }
    }

    public static void setPin(Context context, String str) {
        setString(context, KEY_PIN, digest(str));
    }

    public static boolean checkPin(Context context, String str) {
        String digest = digest(str);
        String string = getString(context, KEY_PIN);
        if (TextUtils.isEmpty(digest)) {
            return TextUtils.isEmpty(string);
        }
        return digest.equals(string);
    }

    public static boolean getRequirePermission(Context context) {
        return getBoolean(context, KEY_REQUIRE_PREMISSION, false);
    }

    public static void setRequirePermission(Context context, boolean z) {
        setBoolean(context, KEY_REQUIRE_PREMISSION, z);
    }

    public static int getAutomaticResponse(Context context) {
        switch (getInt(context, KEY_AUTOMATIC_RESPONSE, 0)) {
            case 0:
            default:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
        }
    }

    public static void setAutomaticResponse(Context context, int i) {
        setInt(context, KEY_AUTOMATIC_RESPONSE, i);
    }

    public static String readFile(String str) {
        return readFile(new File(str));
    }

    public static String readFile(File file) {
        byte[] bArr = new byte[((int) file.length())];
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
        dataInputStream.readFully(bArr);
        dataInputStream.close();
        return new String(bArr);
    }

    public static void writeFile(File file, String str) {
        writeFile(file.getAbsolutePath(), str);
    }

    public static void writeFile(String str, String str2) {
        File file = new File(str);
        file.getParentFile().mkdirs();
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));
        dataOutputStream.write(str2.getBytes());
        dataOutputStream.close();
    }

    public static byte[] readToEndAsArray(InputStream inputStream) {
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = dataInputStream.read(bArr);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                inputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
        }
    }

    public static String readToEnd(InputStream inputStream) {
        return new String(readToEndAsArray(inputStream));
    }

    public static final int getMultiuserMode(Context context) {
        if (Build.VERSION.SDK_INT < 17) {
            return 3;
        }
        return 0;
    }

    public static int getSuperuserAccess() {
        try {
            int intValue = Integer.valueOf((String) Class.forName("android.os.SystemProperties").getMethod("get", String.class).invoke(null, "persist.sys.root_access")).intValue();
            switch (intValue) {
                case 0:
                case 1:
                case 2:
                case 3:
                    return intValue;
                default:
                    return 3;
            }
        } catch (Exception e2) {
            return 3;
        }
    }

    public static void setSuperuserAccess(int i) {
        try {
            Process exec = Runtime.getRuntime().exec(ShellUtils.COMMAND_SU);
            exec.getOutputStream().write(("setprop persist.sys.root_access " + i).getBytes());
            exec.getOutputStream().close();
            exec.waitFor();
        } catch (Exception e2) {
        }
    }

    public static final int getCheckSuQuietCounter(Context context) {
        return getInt(context, CHECK_SU_QUIET, 0);
    }

    public static final void setCheckSuQuietCounter(Context context, int i) {
        setInt(context, CHECK_SU_QUIET, i);
    }

    public static void applyDarkThemeSetting(Activity activity, int i) {
        if ("com.kingouser.com".equals(activity.getPackageName())) {
            try {
                if (getTheme(activity) == 1) {
                    activity.setTheme(i);
                }
            } catch (Exception e2) {
            }
        }
    }

    public static final int getTheme(Context context) {
        return getInt(context, KEY_THEME, 0);
    }

    public static final void setTheme(Context context, int i) {
        setInt(context, KEY_THEME, i);
    }
}
