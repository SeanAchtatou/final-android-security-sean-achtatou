package com.kingouser.com.util;

import android.graphics.Bitmap;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

public class MemoryCache {
    private static final String TAG = "MemoryCache";
    private Map<String, Bitmap> cache = Collections.synchronizedMap(new WeakHashMap(10, 1.5f));
    private long limit = 1000000;
    private long size = 0;

    public MemoryCache() {
        setLimit(Runtime.getRuntime().maxMemory() / 10);
    }

    public void setLimit(long j) {
        this.limit = j;
        MyLog.i(TAG, "MemoryCache will use up to " + ((((double) this.limit) / 1024.0d) / 1024.0d) + "MB");
    }

    public Bitmap get(String str) {
        try {
            if (!this.cache.containsKey(str)) {
                return null;
            }
            return this.cache.get(str);
        } catch (NullPointerException e2) {
            return null;
        }
    }

    public void put(String str, Bitmap bitmap) {
        try {
            if (this.cache.containsKey(str)) {
                this.size -= getSizeInBytes(this.cache.get(str));
            }
            this.cache.put(str, bitmap);
            this.size += getSizeInBytes(bitmap);
            checkSize();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void checkSize() {
        MyLog.i(TAG, "cache size=" + this.size + " length=" + this.cache.size());
        if (this.size > this.limit) {
            Iterator<Map.Entry<String, Bitmap>> it = this.cache.entrySet().iterator();
            while (it.hasNext()) {
                this.size -= getSizeInBytes((Bitmap) it.next().getValue());
                it.remove();
                if (this.size <= this.limit) {
                    break;
                }
            }
            MyLog.i(TAG, "Clean cache. New size " + this.cache.size());
        }
    }

    public void clear() {
        this.cache.clear();
    }

    /* access modifiers changed from: package-private */
    public long getSizeInBytes(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        return (long) (bitmap.getRowBytes() * bitmap.getHeight());
    }
}
