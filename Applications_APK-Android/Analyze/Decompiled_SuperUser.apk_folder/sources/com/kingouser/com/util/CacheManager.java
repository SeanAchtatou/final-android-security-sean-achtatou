package com.kingouser.com.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.util.f;
import com.kingouser.com.R;

public class CacheManager {
    private static CacheManager INSTANCE;
    private f<String, Bitmap> mMemoryCache;

    @TargetApi(12)
    private CacheManager(Context context) {
        this.mMemoryCache = null;
        this.mMemoryCache = new f<String, Bitmap>(((int) (Runtime.getRuntime().maxMemory() / 1024)) / 8) {
            /* access modifiers changed from: protected */
            public int sizeOf(String str, Bitmap bitmap) {
                if (Integer.valueOf(Build.VERSION.SDK_INT).intValue() >= 12) {
                    return bitmap.getByteCount();
                }
                return bitmap.getRowBytes() * bitmap.getHeight();
            }
        };
    }

    public static CacheManager newInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new CacheManager(context);
        }
        return INSTANCE;
    }

    @TargetApi(12)
    public void addBitmapToMemoryCache(String str, Bitmap bitmap) {
        if (getBitmapFromMemCache(str) == null) {
            this.mMemoryCache.put(str, bitmap);
        }
    }

    @TargetApi(12)
    public Bitmap getBitmapFromMemCache(String str) {
        return this.mMemoryCache.get(str);
    }

    public Bitmap getVPBitmap(Context context, int i) {
        String valueOf = String.valueOf(i);
        Bitmap bitmapFromMemCache = getBitmapFromMemCache(valueOf);
        if (bitmapFromMemCache != null) {
            return bitmapFromMemCache;
        }
        switch (i) {
            case 99:
                Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), R.drawable.gs);
                addBitmapToMemoryCache(valueOf, decodeResource);
                return decodeResource;
            case 100:
                Bitmap decodeResource2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.gt);
                addBitmapToMemoryCache(valueOf, decodeResource2);
                return decodeResource2;
            case 101:
                Bitmap decodeResource3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.f9);
                addBitmapToMemoryCache(valueOf, decodeResource3);
                return decodeResource3;
            case 102:
                Bitmap decodeResource4 = BitmapFactory.decodeResource(context.getResources(), R.drawable.f_);
                addBitmapToMemoryCache(valueOf, decodeResource4);
                return decodeResource4;
            case 103:
            case 104:
            default:
                return bitmapFromMemCache;
            case 105:
                Bitmap decodeResource5 = BitmapFactory.decodeResource(context.getResources(), R.drawable.eu);
                addBitmapToMemoryCache(valueOf, decodeResource5);
                return decodeResource5;
            case 106:
                Bitmap decodeResource6 = BitmapFactory.decodeResource(context.getResources(), R.drawable.ev);
                addBitmapToMemoryCache(valueOf, decodeResource6);
                return decodeResource6;
        }
    }
}
