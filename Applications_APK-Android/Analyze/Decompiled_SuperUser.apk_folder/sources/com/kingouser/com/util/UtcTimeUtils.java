package com.kingouser.com.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class UtcTimeUtils {
    public static String getUtcTimeDirectly() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone timeZone = TimeZone.getTimeZone("Etc/GMT+0");
        Date date = new Date();
        simpleDateFormat.setTimeZone(timeZone);
        return getTime(simpleDateFormat.format(date));
    }

    public static String getTime(String str) {
        try {
            return String.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str).getTime() / 1000);
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }
}
