package com.kingouser.com.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.kingouser.com.AppManagerActivity;
import com.kingouser.com.R;
import com.kingouser.com.entity.DeleteAppItem;
import com.kingouser.com.entity.UninstallAppInfo;
import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.helper.utils.FileUtils;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.util.h;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AppManagerUtils {
    public static boolean fillApkModel(Context context, DeleteAppItem deleteAppItem) {
        String filePath = deleteAppItem.getFilePath();
        File file = new File(filePath);
        if (file == null || !file.exists()) {
            return false;
        }
        deleteAppItem.setInstallTime(file.lastModified());
        deleteAppItem.setCodePath(filePath);
        deleteAppItem.setCodeSize(file.length());
        try {
            Class<?> cls = Class.forName("android.content.pm.PackageParser");
            Object newInstance = cls.getConstructor(String.class).newInstance(filePath);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            displayMetrics.setToDefaults();
            Object invoke = cls.getDeclaredMethod("parsePackage", File.class, String.class, DisplayMetrics.class, Integer.TYPE).invoke(newInstance, new File(filePath), filePath, displayMetrics, 0);
            if (invoke == null) {
                return false;
            }
            Field declaredField = invoke.getClass().getDeclaredField("applicationInfo");
            if (declaredField.get(invoke) == null) {
                return false;
            }
            ApplicationInfo applicationInfo = (ApplicationInfo) declaredField.get(invoke);
            Class<?> cls2 = Class.forName("android.content.res.AssetManager");
            Object newInstance2 = cls2.newInstance();
            cls2.getDeclaredMethod("addAssetPath", String.class).invoke(newInstance2, filePath);
            Resources resources = context.getResources();
            Resources newInstance3 = Resources.class.getConstructor(newInstance2.getClass(), resources.getDisplayMetrics().getClass(), resources.getConfiguration().getClass()).newInstance(newInstance2, resources.getDisplayMetrics(), resources.getConfiguration());
            if (applicationInfo == null) {
                return false;
            }
            if (applicationInfo.labelRes != 0) {
                deleteAppItem.setAppName((String) newInstance3.getText(applicationInfo.labelRes));
            } else {
                String name = file.getName();
                deleteAppItem.setAppName(name.substring(0, name.lastIndexOf(".")));
            }
            deleteAppItem.setAppPackage(applicationInfo.packageName);
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageArchiveInfo = packageManager.getPackageArchiveInfo(filePath, 1);
            if (packageArchiveInfo != null) {
                deleteAppItem.setAppVersion(packageArchiveInfo.versionName);
                deleteAppItem.setAppVersionCode(packageArchiveInfo.versionCode);
            }
            PackageInfo packageInfo = null;
            try {
                packageInfo = packageManager.getPackageInfo(deleteAppItem.getAppPackage(), 0);
            } catch (PackageManager.NameNotFoundException e2) {
            }
            if (packageInfo != null) {
                deleteAppItem.setInstall(true);
                deleteAppItem.setChecked(true);
                if (deleteAppItem.getAppVersionCode() <= packageInfo.versionCode) {
                    deleteAppItem.setChecked(true);
                }
            } else {
                deleteAppItem.setInstall(false);
                deleteAppItem.setChecked(false);
                deleteAppItem.setAppVersion(context.getString(R.string.av));
            }
            return true;
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public static boolean isAliveable(Context context, String str) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        new ArrayList();
        if (installedPackages == null) {
            return false;
        }
        for (int i = 0; i < installedPackages.size(); i++) {
            if (str.equalsIgnoreCase(installedPackages.get(i).packageName)) {
                return true;
            }
        }
        return false;
    }

    public static void unInstallUserApp(Context context, String str) {
        openUninstaller(context, str);
    }

    public static void unInstallUserApp(AppManagerActivity appManagerActivity, final DeleteAppItem deleteAppItem, final Context context, Handler handler) {
        new Thread(new Runnable() {
            public void run() {
                for (boolean z = true; z; z = false) {
                    AppManagerUtils.openUninstaller(context, deleteAppItem.getAppPackage());
                }
            }
        }).start();
    }

    public static void uninstallSysApp(AppManagerActivity appManagerActivity, final DeleteAppItem deleteAppItem, final Context context, final Handler handler) {
        new Thread(new Runnable() {
            public void run() {
                for (boolean z = true; z; z = false) {
                    if (ShellUtils.checkRoot(context)) {
                        Message message = new Message();
                        message.what = 83;
                        message.obj = deleteAppItem;
                        handler.sendMessage(message);
                        AppManagerUtils.uninstallSystemApp(deleteAppItem, context);
                        Message message2 = new Message();
                        message2.what = 84;
                        handler.sendMessage(message2);
                    } else {
                        Message message3 = new Message();
                        message3.obj = deleteAppItem;
                        message3.what = 85;
                        handler.sendMessage(message3);
                    }
                }
            }
        }).start();
    }

    public static void runApp(Context context, String str) {
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(str);
        if (launchIntentForPackage != null) {
            context.startActivity(launchIntentForPackage);
        }
    }

    public static Intent getLaunchIntent(Context context, String str) {
        return context.getPackageManager().getLaunchIntentForPackage(str);
    }

    public static void openInstalledDetail(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            intent.putExtra("com.android.settings.ApplicationPkgName", str);
            intent.putExtra(UninstallAppInfo.COLUMN_PKG, str);
            context.startActivity(intent);
        } catch (Exception e2) {
            try {
                context.startActivity(new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.fromParts(ServiceManagerNative.PACKAGE, str, null)));
            } catch (Exception e3) {
                openUninstaller(context, str);
            }
        }
    }

    public static void openUninstaller(Context context, String str) {
        Intent intent = new Intent("android.intent.action.DELETE", Uri.parse("package:" + str));
        intent.setFlags(268435456);
        context.startActivity(intent);
    }

    public static void openInstaller(Context context, String str) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        File file = new File(str);
        if (file.exists() && file.isAbsolute()) {
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            context.startActivity(intent);
        }
    }

    public static boolean backupApp(DeleteAppItem deleteAppItem, String str) {
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
        String substring = deleteAppItem.getCodePath().substring(deleteAppItem.getCodePath().lastIndexOf("/") + 1, deleteAppItem.getCodePath().length());
        String str2 = str + substring;
        if (!str2.endsWith(".apk")) {
            str2 = str2 + ".apk";
        }
        File file2 = new File(str2);
        if (file2.exists()) {
            file2.delete();
        }
        File file3 = new File(str + substring.replace(".apk", ".odex"));
        if (file3.exists()) {
            file3.delete();
        }
        try {
            file2.createNewFile();
            FileInputStream fileInputStream = new FileInputStream(new File(deleteAppItem.getCodePath()));
            FileOutputStream fileOutputStream = new FileOutputStream(str2);
            byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
            int i = 0;
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                i += read;
                fileOutputStream.write(bArr, 0, read);
            }
            if (((long) i) != new File(deleteAppItem.getCodePath()).length()) {
                file2.delete();
                return false;
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            fileInputStream.close();
            if (new File(deleteAppItem.getOdexPath()).exists()) {
                file3.createNewFile();
                FileInputStream fileInputStream2 = new FileInputStream(new File(deleteAppItem.getOdexPath()));
                FileOutputStream fileOutputStream2 = new FileOutputStream(file3);
                int i2 = 0;
                while (true) {
                    int read2 = fileInputStream2.read(bArr);
                    if (read2 <= 0) {
                        break;
                    }
                    i2 += read2;
                    fileOutputStream2.write(bArr, 0, read2);
                }
                if (((long) i2) != new File(deleteAppItem.getOdexPath()).length()) {
                    file3.delete();
                    return false;
                }
                fileOutputStream2.flush();
                fileOutputStream2.close();
                fileInputStream2.close();
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean uninstallSystemApp(UninstallAppInfo uninstallAppInfo, Context context) {
        LinkedList linkedList = new LinkedList();
        String str = context.getFilesDir() + File.separator + "busybox " + "mount -o remount,rw /system";
        linkedList.add(str);
        linkedList.add(str);
        linkedList.add("pm disable " + uninstallAppInfo.pkg);
        if (new File(uninstallAppInfo.sourceDir).exists()) {
            linkedList.add("rm -R " + uninstallAppInfo.sourceDir);
        }
        if (new File(uninstallAppInfo.datadir).exists()) {
            linkedList.add("rm -R " + uninstallAppInfo.datadir);
        }
        String[] strArr = new String[linkedList.size()];
        linkedList.toArray(strArr);
        for (int i = 0; i < strArr.length; i++) {
            f.a("comman:" + strArr[i]);
        }
        ShellUtils.CommandResult execCommand = ShellUtils.execCommand(strArr, true, true, 60);
        f.a("result:" + execCommand.result);
        f.a("resuls:" + execCommand.successMsg);
        f.a("resule:" + execCommand.errorMsg);
        boolean a2 = h.a(context, uninstallAppInfo.pkg);
        f.a("resule:" + a2);
        if (!a2) {
            return true;
        }
        return false;
    }

    public static void uninstallSystemApp(DeleteAppItem deleteAppItem, Context context) {
        ShellUtils.execCommand(new String[]{context.getFilesDir() + "/busybox " + "mount -o remount,rw /system", "mount -o remount,rw /system", "rm " + deleteAppItem.getCodePath(), "rm " + deleteAppItem.getDataDir(), "rm " + deleteAppItem.getNativeLibraryDir(), "pm uninstall " + deleteAppItem.getAppPackage()}, true, true, 60);
    }

    public static boolean restoreApp(DeleteAppItem deleteAppItem) {
        String substring = deleteAppItem.getFilePath().substring(deleteAppItem.getFilePath().lastIndexOf("/") + 1, deleteAppItem.getFilePath().length());
        executeRootCommand("mount -o remount rw /system");
        executeRootCommand("PATH='/system/bin';'mount' '-o' 'remount,rw' '' '/system'");
        String replace = substring.replace(".apk", ".odex");
        if (new File(deleteAppItem.getOdexPath()).exists()) {
            executeRootCommand((("touch /system/app/" + replace + ShellUtils.COMMAND_LINE_END) + "chmod 0644 /system/app/" + replace + ShellUtils.COMMAND_LINE_END) + "cat " + deleteAppItem.getOdexPath() + " > " + "/system/app/" + replace);
        }
        return executeRootCommand(new StringBuilder().append(new StringBuilder().append(new StringBuilder().append("touch /system/app/").append(substring).append(ShellUtils.COMMAND_LINE_END).toString()).append("chmod 0644 /system/app/").append(substring).append(ShellUtils.COMMAND_LINE_END).toString()).append("cat ").append(deleteAppItem.getFilePath()).append(" > ").append("/system/app/").append(substring).toString()) == 0;
    }

    public static boolean appIsInstall(Context context, String str, boolean z) {
        boolean z2;
        boolean z3 = true;
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(str, 0);
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            if ((applicationInfo.flags & FileUtils.FileMode.MODE_IWUSR) != 0) {
                z2 = true;
            } else if ((applicationInfo.flags & 1) == 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (z && !z2) {
                return false;
            }
            if (!z && z2) {
                return false;
            }
            if (packageInfo == null) {
                z3 = false;
            }
            return z3;
        } catch (PackageManager.NameNotFoundException e2) {
        }
    }

    public static boolean appIsRunning(Context context, String str) {
        boolean z = true;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getPackageInfo(str, 0).applicationInfo;
            z = applicationInfo.enabled;
            MyLog.i("debug", "enable:" + applicationInfo.enabled);
            MyLog.i("debug", applicationInfo.packageName);
            return z;
        } catch (Exception e2) {
            return z;
        }
    }

    public static int executeRootCommand(String str) {
        Exception e2;
        Process process;
        try {
            process = Runtime.getRuntime().exec(ShellUtils.COMMAND_SU);
            try {
                DataOutputStream dataOutputStream = new DataOutputStream(process.getOutputStream());
                dataOutputStream.writeBytes(str + ShellUtils.COMMAND_LINE_END);
                dataOutputStream.writeBytes(ShellUtils.COMMAND_EXIT);
                dataOutputStream.flush();
                dataOutputStream.close();
                process.waitFor();
            } catch (Exception e3) {
                e2 = e3;
                e2.printStackTrace();
                return process.exitValue();
            }
        } catch (Exception e4) {
            Exception exc = e4;
            process = null;
            e2 = exc;
        }
        return process.exitValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static boolean uninstallUserApp(String str) {
        if (ShellUtils.execCommand("pm uninstall " + str, true, true).result == 0) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static boolean uninstallUserApp(DeleteAppItem deleteAppItem) {
        if (ShellUtils.execCommand("pm uninstall " + deleteAppItem.getAppPackage(), true, true).result == 0) {
            return true;
        }
        return false;
    }

    public static int executeRootCommands(String str) {
        try {
            Process exec = Runtime.getRuntime().exec(ShellUtils.COMMAND_SU);
            OutputStream outputStream = exec.getOutputStream();
            outputStream.write(str.getBytes());
            outputStream.close();
            exec.waitFor();
            return exec.exitValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public static boolean grantRoot(Context context) {
        long currentTimeMillis = System.currentTimeMillis();
        executeRootCommand("mount -o remount,rw /system; echo 'a' > /system/xbin/" + currentTimeMillis + ";");
        executeRootCommand("PATH='/system/xbin';'mount' '-o' 'remount,rw' '' '/system'; echo 'a' > /system/xbin/" + currentTimeMillis + ";");
        if (new File("/system/xbin/" + currentTimeMillis) == null || !new File("/system/xbin/" + currentTimeMillis).exists() || new File("/system/xbin/" + currentTimeMillis).length() <= 0) {
            MySharedPreference.setWheaterRoot(context, false);
            return false;
        }
        executeRootCommand("mount -o remount,rw /system;rm /system/xbin/" + currentTimeMillis);
        MySharedPreference.setWheaterRoot(context, true);
        return true;
    }

    public static boolean isForeground(Context context, String str) {
        if (context == null || TextUtils.isEmpty(str)) {
            return false;
        }
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) context.getSystemService(ServiceManagerNative.ACTIVITY)).getRunningTasks(1);
        if (runningTasks == null || runningTasks.size() <= 0 || !str.equals(runningTasks.get(0).topActivity.getClassName())) {
            return false;
        }
        return true;
    }

    public static byte[] getApkSign(Context context) {
        for (PackageInfo next : context.getPackageManager().getInstalledPackages(64)) {
            if (next.packageName.equals(context.getPackageName())) {
                return next.signatures[0].toByteArray();
            }
        }
        return null;
    }

    public static byte[] getApkSignature(Context context, String str) {
        Signature signature;
        Class<?> cls = Class.forName("android.content.pm.PackageParser");
        Method method = cls.getMethod("parsePackage", File.class, String.class, DisplayMetrics.class, Integer.TYPE);
        Object newInstance = cls.getConstructor(String.class).newInstance("");
        Object invoke = method.invoke(newInstance, new File(str), null, context.getResources().getDisplayMetrics(), 4);
        cls.getMethod("collectCertificates", Class.forName("android.content.pm.PackageParser$Package"), Integer.TYPE).invoke(newInstance, invoke, 64);
        Signature[] signatureArr = (Signature[]) invoke.getClass().getField("mSignatures").get(invoke);
        if (signatureArr.length > 0) {
            signature = signatureArr[0];
        } else {
            signature = null;
        }
        if (signature != null) {
            return signature.toByteArray();
        }
        return null;
    }

    public static String getPublicKey(byte[] bArr) {
        try {
            String obj = ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(bArr))).getPublicKey().toString();
            return obj.substring(obj.indexOf("modulus: ") + 9, obj.indexOf(ShellUtils.COMMAND_LINE_END, obj.indexOf("modulus:")));
        } catch (CertificateException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static void getSingInfo(Context context) {
        try {
            parseSignature(context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toByteArray());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static void parseSignature(byte[] bArr) {
        try {
            X509Certificate x509Certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(bArr));
            x509Certificate.getPublicKey().toString();
            x509Certificate.getSerialNumber().toString();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            simpleDateFormat.format(x509Certificate.getNotAfter());
            simpleDateFormat.format(x509Certificate.getNotBefore());
            x509Certificate.getVersion();
            x509Certificate.getType();
        } catch (CertificateException e2) {
            e2.printStackTrace();
        }
    }

    public static void getInstallApkSign(Context context) {
        PackageManager packageManager = context.getPackageManager();
        ArrayList arrayList = new ArrayList();
        int i = 0;
        for (PackageInfo next : packageManager.getInstalledPackages(64)) {
            try {
                String str = next.packageName;
                String str2 = next.versionName;
                String str3 = "" + next.versionCode;
                String charSequence = next.applicationInfo.loadLabel(packageManager).toString();
                X509Certificate x509Certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(next.signatures[0].toByteArray()));
                String obj = x509Certificate.getPublicKey().toString();
                String bigInteger = x509Certificate.getSerialNumber().toString();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String format = simpleDateFormat.format(x509Certificate.getNotAfter());
                String format2 = simpleDateFormat.format(x509Certificate.getNotBefore());
                int version = x509Certificate.getVersion();
                String type = x509Certificate.getType();
                String name = x509Certificate.getIssuerX500Principal().getName("RFC1779");
                StringBuilder sb = new StringBuilder();
                sb.append("==============").append("\r\n");
                sb.append("==============").append("\r\n");
                sb.append("第" + i + "个,游戏名字:【" + charSequence + "】__packageName:【" + str + "】___versionName:" + str2 + "____versionCode:" + str3).append("\r\n");
                sb.append("parseSignature==========开始日期:" + format2 + "___有效结束日期:" + format).append("\r\n");
                sb.append("parseSignature==========version:" + version + "___type:" + type).append("\r\n");
                sb.append("parseSignature==========signName:" + x509Certificate.getSigAlgName()).append("\r\n");
                sb.append("parseSignature==========pubKey:" + obj).append("\r\n");
                sb.append("parseSignature==========signNumber:" + bigInteger).append("\r\n");
                sb.append("parseSignature==========issuer:" + name).append("\r\n");
                sb.append("parseSignature==========subjectDN:" + x509Certificate.getSubjectDN().toString()).append("\r\n");
                arrayList.add(sb.toString());
                i++;
            } catch (Exception e2) {
            }
        }
    }
}
