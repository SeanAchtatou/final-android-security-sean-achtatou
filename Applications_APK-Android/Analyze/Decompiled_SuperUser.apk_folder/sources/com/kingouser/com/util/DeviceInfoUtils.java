package com.kingouser.com.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.kingoapp.uts.util.DeviceIdGenerator;
import com.kingouser.com.R;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

public class DeviceInfoUtils {
    private static final String[] BAD_SERIAL_PATTERNS = {"1234567", "abcdef", "dead00beef"};
    private static final String EMULATOR_ANDROID_ID = "9774d56d682e549c";

    public static boolean externalMemoryAvailable() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static int getDeviceWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & 15) >= 3;
    }

    private boolean isPad(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        float width = (float) defaultDisplay.getWidth();
        float height = (float) defaultDisplay.getHeight();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getMetrics(displayMetrics);
        if (Math.sqrt(Math.pow((double) (((float) displayMetrics.heightPixels) / displayMetrics.ydpi), 2.0d) + Math.pow((double) (((float) displayMetrics.widthPixels) / displayMetrics.xdpi), 2.0d)) >= 6.0d) {
            return true;
        }
        return false;
    }

    public static int getDeviceHeight(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static String readDeviceId(Context context) {
        String str = null;
        try {
            str = Build.SERIAL;
        } catch (NoSuchFieldError e2) {
        }
        if (TextUtils.isEmpty(str) || "unknown".equals(str) || isBadSerial(str)) {
            str = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (TextUtils.isEmpty(str) || EMULATOR_ANDROID_ID.equals(str) || isBadDeviceId(str) || str.length() != EMULATOR_ANDROID_ID.length()) {
                str = SoftInstallationId.id(context);
            }
        }
        return UUID.nameUUIDFromBytes(str.getBytes()).toString();
    }

    public static String getImei(Context context) {
        List<String> imei = new Imei().getIMEI(context);
        if (imei.size() > 0) {
            return imei.get(0);
        }
        return DeviceIdGenerator.readDeviceId(context);
    }

    private static boolean isBadDeviceId(String str) {
        return TextUtils.isEmpty(str) || TextUtils.isEmpty(str.replace('0', ' ').replace('-', ' ').trim());
    }

    private static boolean isBadSerial(String str) {
        if (TextUtils.isEmpty(str)) {
            return true;
        }
        String lowerCase = str.toLowerCase();
        for (String contains : BAD_SERIAL_PATTERNS) {
            if (lowerCase.contains(contains)) {
                return true;
            }
        }
        return false;
    }

    private static class SoftInstallationId {
        private static final String INSTALLATION = "INSTALLATION";
        private static String sID = null;

        private SoftInstallationId() {
        }

        public static synchronized String id(Context context) {
            String str;
            synchronized (SoftInstallationId.class) {
                if (sID == null) {
                    File file = new File(context.getFilesDir(), INSTALLATION);
                    try {
                        if (!file.exists()) {
                            writeInstallationFile(file);
                        }
                        sID = readInstallationFile(file);
                    } catch (Exception e2) {
                        throw new RuntimeException(e2);
                    }
                }
                str = sID;
            }
            return str;
        }

        /* JADX INFO: finally extract failed */
        private static String readInstallationFile(File file) {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
            try {
                byte[] bArr = new byte[((int) randomAccessFile.length())];
                randomAccessFile.readFully(bArr);
                randomAccessFile.close();
                String str = new String(bArr);
                randomAccessFile.close();
                return str;
            } catch (Throwable th) {
                randomAccessFile.close();
                throw th;
            }
        }

        private static void writeInstallationFile(File file) {
            new FileOutputStream(file).write(UUID.randomUUID().toString().getBytes());
        }
    }

    public static String getAndroidid(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static String getMacAddress(Context context) {
        WifiInfo connectionInfo;
        if (Build.VERSION.SDK_INT < 23 && (connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo()) != null && connectionInfo.getMacAddress() != null) {
            return connectionInfo.getMacAddress().toUpperCase();
        }
        StringBuffer stringBuffer = new StringBuffer();
        try {
            NetworkInterface byName = NetworkInterface.getByName("eth1");
            if (byName == null) {
                byName = NetworkInterface.getByName("wlan0");
            }
            if (byName == null) {
                return "02:00:00:00:00:02";
            }
            byte[] hardwareAddress = byName.getHardwareAddress();
            int length = hardwareAddress.length;
            for (int i = 0; i < length; i++) {
                stringBuffer.append(String.format("%02X:", Byte.valueOf(hardwareAddress[i])));
            }
            if (stringBuffer.length() > 0) {
                stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            }
            return stringBuffer.toString().toUpperCase();
        } catch (Exception e2) {
            e2.printStackTrace();
            return "02:00:00:00:00:02";
        }
    }

    public static String getTelNumber(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
        } catch (Exception e2) {
            return null;
        }
    }

    public static String getManuFacture() {
        return Build.MANUFACTURER;
    }

    public static String getSystemVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String getSDKVersion() {
        return Build.VERSION.SDK;
    }

    public static String getDisplayId() {
        return Build.DISPLAY;
    }

    public static String getDeviceId() {
        return Build.PRODUCT;
    }

    public static String getScreenPic(Activity activity) {
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        Bitmap.createBitmap(defaultDisplay.getWidth(), defaultDisplay.getHeight(), Bitmap.Config.ARGB_8888);
        View decorView = activity.getWindow().getDecorView();
        decorView.setDrawingCacheEnabled(true);
        Bitmap drawingCache = decorView.getDrawingCache();
        Bitmap extractThumbnail = ThumbnailUtils.extractThumbnail(drawingCache, 96, 144, 2);
        if (drawingCache != null && !drawingCache.isRecycled()) {
            drawingCache.recycle();
        }
        decorView.destroyDrawingCache();
        try {
            FileOutputStream openFileOutput = activity.openFileOutput("screen.png", 0);
            extractThumbnail.compress(Bitmap.CompressFormat.PNG, 70, openFileOutput);
            openFileOutput.flush();
            openFileOutput.close();
            if (extractThumbnail == null || extractThumbnail.isRecycled()) {
                return "screen.png";
            }
            extractThumbnail.recycle();
            return "screen.png";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "screen.png";
        }
    }

    public static String getDeviceUUID(Context context) {
        UUID uuid;
        String deviceId;
        if (context == null) {
            return null;
        }
        if (0 != 0 || (deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId()) == null) {
            uuid = null;
        } else {
            uuid = UUID.nameUUIDFromBytes(deviceId.getBytes());
        }
        return uuid.toString();
    }

    private static String getV(Map<String, String> map, String str) {
        String str2 = map.get(str);
        if (TextUtils.isEmpty(str2)) {
            return "";
        }
        return str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e A[SYNTHETIC, Splitter:B:11:0x002e] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003d A[SYNTHETIC, Splitter:B:20:0x003d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<java.lang.String> runComm(java.lang.String r5) {
        /*
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r2 = 0
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0048, all -> 0x003a }
            java.lang.Process r0 = r0.exec(r5)     // Catch:{ IOException -> 0x0048, all -> 0x003a }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x0048, all -> 0x003a }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0048, all -> 0x003a }
            r4.<init>(r0)     // Catch:{ IOException -> 0x0048, all -> 0x003a }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0048, all -> 0x003a }
            r0 = 8192(0x2000, float:1.14794E-41)
            r1.<init>(r4, r0)     // Catch:{ IOException -> 0x0048, all -> 0x003a }
        L_0x001e:
            java.lang.String r0 = r1.readLine()     // Catch:{ IOException -> 0x0028 }
            if (r0 == 0) goto L_0x0032
            r3.add(r0)     // Catch:{ IOException -> 0x0028 }
            goto L_0x001e
        L_0x0028:
            r0 = move-exception
        L_0x0029:
            r0.printStackTrace()     // Catch:{ all -> 0x0045 }
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0041 }
        L_0x0031:
            return r3
        L_0x0032:
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0038 }
            goto L_0x0031
        L_0x0038:
            r0 = move-exception
            goto L_0x0031
        L_0x003a:
            r0 = move-exception
        L_0x003b:
            if (r2 == 0) goto L_0x0040
            r2.close()     // Catch:{ IOException -> 0x0043 }
        L_0x0040:
            throw r0
        L_0x0041:
            r0 = move-exception
            goto L_0x0031
        L_0x0043:
            r1 = move-exception
            goto L_0x0040
        L_0x0045:
            r0 = move-exception
            r2 = r1
            goto L_0x003b
        L_0x0048:
            r0 = move-exception
            r1 = r2
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.DeviceInfoUtils.runComm(java.lang.String):java.util.ArrayList");
    }

    public static boolean matchChinaese(String str) {
        if (Pattern.compile("[\\u4e00-\\u9fa5]", 2).matcher(str).find()) {
            return true;
        }
        return false;
    }

    public static float getTextSize(Context context, int i) {
        return ((float) DensityUtil.px2sp(context, (float) i)) * (context.getResources().getDisplayMetrics().density / 3.0f);
    }

    public static float getNomalTextSize(Context context, int i) {
        return ((float) DensityUtil.px2sp(context, (float) i)) * (context.getResources().getDisplayMetrics().density / 2.0f);
    }

    public static int getPix(Context context, int i) {
        return (int) (context.getResources().getDisplayMetrics().density * ((float) i));
    }

    public static String getLocalLanguage() {
        return LanguageUtils.getLocalLanguage();
    }

    public static String getLocalDefault() {
        return LanguageUtils.getLocalDefault();
    }

    public static String getVersionName(Context context) {
        return PackageUtils.getAppVersion(context);
    }

    public static int getVersionCode(Context context) {
        return PackageUtils.getAppversionCode(context);
    }

    public static int getSDKVersion(Context context) {
        return Build.VERSION.SDK_INT;
    }

    public static String getModelId() {
        return Build.MODEL;
    }

    public static String getManufacturer() {
        try {
            return Build.MANUFACTURER;
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getDisplayVersion() {
        return Build.DISPLAY;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static String getSuVersion() {
        return ShellUtils.execCommand("su -v", false, true).successMsg;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static String getDaemonSuVersion() {
        return ShellUtils.execCommand("su -v", false, true).successMsg;
    }

    public static String getFileMd5(Context context, String str) {
        FileUtils.isExistsAndCopy(context, "busybox");
        String str2 = ShellUtils.execCommand(new String[]{"chmod 755 " + context.getFilesDir() + "/busybox", context.getFilesDir() + "/busybox" + " md5sum " + str}, true, true, 60).successMsg;
        if (TextUtils.isEmpty(str2)) {
            return str2;
        }
        String[] split = str2.split(" ");
        if (!TextUtils.isEmpty(split[0])) {
            return split[0];
        }
        return str2;
    }

    public static String getWhichSu(Context context) {
        FileUtils.isExistsAndCopy(context, "busybox");
        String str = ShellUtils.execCommand(new String[]{"chmod 755 " + context.getFilesDir() + "/busybox", context.getFilesDir() + "/busybox which su"}, true, true, 60).successMsg;
        if (!TextUtils.isEmpty(str)) {
            return str.substring(0, str.lastIndexOf("/"));
        }
        return str;
    }

    public static String getChmodCode(Context context) {
        int intValue = Integer.valueOf(getSDKVersion()).intValue();
        if ((16 > intValue || intValue >= 18 || (!"YuLong".equalsIgnoreCase(Build.MODEL) && !"COOLPAD".equalsIgnoreCase(Build.MODEL))) && Integer.valueOf(getSDKVersion()).intValue() < 18) {
            return "6755";
        }
        return "755";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static String[] getPaths() {
        String[] strArr = new String[0];
        return ShellUtils.execCommand("echo $PATH", false).successMsg.split(":");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static boolean isExist(Context context, String str, String str2) {
        FileUtils.isExistsAndCopy(context, "busybox");
        if (ShellUtils.execCommand((("chmod 755 " + context.getFilesDir() + "/busybox;") + context.getFilesDir() + "/busybox " + "ls -l " + str + ";") + "ls -l " + str + ";", true).successMsg.contains(str2)) {
            return true;
        }
        return false;
    }

    public static int getUid(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 64).applicationInfo.uid;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static void initTintBar(Activity activity) {
        if (Build.VERSION.SDK_INT >= 19) {
            setTranslucentStatus(activity, true);
        }
        SystemBarTintManager systemBarTintManager = new SystemBarTintManager(activity);
        systemBarTintManager.setStatusBarTintEnabled(true);
        systemBarTintManager.setNavigationBarTintEnabled(true);
        systemBarTintManager.setTintColor(activity.getResources().getColor(R.color.am));
    }

    @TargetApi(19)
    public static void setTranslucentStatus(Activity activity, boolean z) {
        Window window = activity.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        if (z) {
            attributes.flags |= 67108864;
        } else {
            attributes.flags &= -67108865;
        }
        window.setAttributes(attributes);
    }

    public static Drawable getDrawble(Context context, String str) {
        PackageManager packageManager = context.getPackageManager();
        try {
            return packageManager.getApplicationInfo(str, FileUtils.FileMode.MODE_IWUSR).loadIcon(packageManager);
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    public static String getProcessName(Context context, int i) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService(ServiceManagerNative.ACTIVITY)).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.pid == i) {
                    return next.processName;
                }
            }
        }
        return null;
    }

    private static String getRrandomDevicesID(Context context) {
        StringBuilder sb = new StringBuilder();
        String uuid = UUID.randomUUID().toString();
        if (!TextUtils.isEmpty(uuid)) {
            sb.append(uuid);
        }
        String str = Build.SERIAL;
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        return encryptSHA(sb.toString()).replaceAll("\r|\n", "");
    }

    public static String encryptSHA(String str) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA");
        } catch (NoSuchAlgorithmException e2) {
        }
        messageDigest.update(str.getBytes());
        byte[] digest = messageDigest.digest();
        String str2 = "";
        for (int i = 0; i < messageDigest.digest().length; i++) {
            str2 = str2 + Integer.toString((digest[i] & 255) + 256, 16).substring(1);
        }
        return str2.toLowerCase();
    }
}
