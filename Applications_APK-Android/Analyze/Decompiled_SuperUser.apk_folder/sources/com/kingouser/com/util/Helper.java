package com.kingouser.com.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

public class Helper {
    public static Drawable loadPackageIcon(Context context, String str) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 0);
            Drawable drawable = (Drawable) ImageCache.getInstance().get(str);
            if (drawable != null) {
                return drawable;
            }
            ImageCache instance = ImageCache.getInstance();
            Drawable loadIcon = packageInfo.applicationInfo.loadIcon(packageManager);
            instance.put(str, loadIcon);
            return loadIcon;
        } catch (Exception e2) {
            return null;
        }
    }
}
