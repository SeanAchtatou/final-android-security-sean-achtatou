package com.kingouser.com.util;

import android.annotation.SuppressLint;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

@SuppressLint({"NewApi"})
public class NetUtil {
    public static final int MEHOD_GET = 1;
    public static final int MEHOD_POST = 0;
    private static NetUtil netWorkUtil;

    public static NetUtil getInstant() {
        if (netWorkUtil == null) {
            netWorkUtil = new NetUtil();
        }
        return netWorkUtil;
    }

    public byte[] submitPostData(String str, byte[] bArr, HashMap<String, String> hashMap, int i) {
        return submitPostDataByte(str, bArr, hashMap, i);
    }

    public byte[] submitPostData(String str, String str2, HashMap<String, String> hashMap, int i) {
        return submitPostDataByte(str, str2.getBytes(), hashMap, i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0050 A[SYNTHETIC, Splitter:B:22:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0055 A[Catch:{ Exception -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005a A[Catch:{ Exception -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00c5 A[SYNTHETIC, Splitter:B:68:0x00c5] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00ca A[Catch:{ Exception -> 0x00d3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00cf A[Catch:{ Exception -> 0x00d3 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] submitPostDataByte(java.lang.String r8, byte[] r9, java.util.HashMap<java.lang.String, java.lang.String> r10, int r11) {
        /*
            r7 = this;
            r3 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x00f7, all -> 0x00c0 }
            r0.<init>(r8)     // Catch:{ IOException -> 0x00f7, all -> 0x00c0 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x00f7, all -> 0x00c0 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x00f7, all -> 0x00c0 }
            r1 = 3000(0xbb8, float:4.204E-42)
            r0.setConnectTimeout(r1)     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            if (r11 != 0) goto L_0x001b
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            r1 = 0
            r0.setUseCaches(r1)     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
        L_0x001b:
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            if (r11 != 0) goto L_0x005f
            java.lang.String r1 = "POST"
        L_0x0023:
            r0.setRequestMethod(r1)     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            if (r10 == 0) goto L_0x0062
            java.util.Set r1 = r10.keySet()     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            java.util.Iterator r4 = r1.iterator()     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
        L_0x0030:
            boolean r1 = r4.hasNext()     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            if (r1 == 0) goto L_0x0062
            java.lang.Object r1 = r4.next()     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            java.lang.Object r2 = r10.get(r1)     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            r0.addRequestProperty(r1, r2)     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            goto L_0x0030
        L_0x0046:
            r1 = move-exception
            r2 = r3
            r4 = r0
            r0 = r1
            r1 = r3
        L_0x004b:
            r0.printStackTrace()     // Catch:{ all -> 0x00f2 }
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ Exception -> 0x00bd }
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ Exception -> 0x00bd }
        L_0x0058:
            if (r4 == 0) goto L_0x005d
            r4.disconnect()     // Catch:{ Exception -> 0x00bd }
        L_0x005d:
            r0 = r3
        L_0x005e:
            return r0
        L_0x005f:
            java.lang.String r1 = "GET"
            goto L_0x0023
        L_0x0062:
            int r1 = r9.length     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            if (r1 <= 0) goto L_0x006f
            java.lang.String r1 = "Content-Length"
            int r2 = r9.length     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            r0.addRequestProperty(r1, r2)     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
        L_0x006f:
            r1 = 0
            r0.setInstanceFollowRedirects(r1)     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            r0.connect()     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            if (r11 != 0) goto L_0x011d
            java.io.OutputStream r2 = r0.getOutputStream()     // Catch:{ IOException -> 0x0046, all -> 0x00d5 }
            r2.write(r9)     // Catch:{ IOException -> 0x00fd, all -> 0x00db }
            r4 = r2
        L_0x0080:
            int r1 = r0.getResponseCode()     // Catch:{ IOException -> 0x0103, all -> 0x00e1 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x011a
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ IOException -> 0x0103, all -> 0x00e1 }
            java.lang.String r1 = r0.getContentEncoding()     // Catch:{ IOException -> 0x010a, all -> 0x00e6 }
            if (r1 == 0) goto L_0x00a5
            java.lang.String r5 = "gzip"
            boolean r1 = r1.equalsIgnoreCase(r5)     // Catch:{ IOException -> 0x010a, all -> 0x00e6 }
            if (r1 == 0) goto L_0x00a5
            java.util.zip.GZIPInputStream r1 = new java.util.zip.GZIPInputStream     // Catch:{ IOException -> 0x010a, all -> 0x00e6 }
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x010a, all -> 0x00e6 }
            r5.<init>(r2)     // Catch:{ IOException -> 0x010a, all -> 0x00e6 }
            r1.<init>(r5)     // Catch:{ IOException -> 0x010a, all -> 0x00e6 }
            r2 = r1
        L_0x00a5:
            byte[] r1 = r7.dealResponseResult(r2)     // Catch:{ IOException -> 0x0112, all -> 0x00ec }
        L_0x00a9:
            if (r4 == 0) goto L_0x00ae
            r4.close()     // Catch:{ Exception -> 0x00ba }
        L_0x00ae:
            if (r2 == 0) goto L_0x00b3
            r2.close()     // Catch:{ Exception -> 0x00ba }
        L_0x00b3:
            if (r0 == 0) goto L_0x00b8
            r0.disconnect()     // Catch:{ Exception -> 0x00ba }
        L_0x00b8:
            r0 = r1
            goto L_0x005e
        L_0x00ba:
            r0 = move-exception
            r0 = r1
            goto L_0x005e
        L_0x00bd:
            r0 = move-exception
            r0 = r3
            goto L_0x005e
        L_0x00c0:
            r0 = move-exception
            r4 = r3
            r1 = r3
        L_0x00c3:
            if (r4 == 0) goto L_0x00c8
            r4.close()     // Catch:{ Exception -> 0x00d3 }
        L_0x00c8:
            if (r3 == 0) goto L_0x00cd
            r3.close()     // Catch:{ Exception -> 0x00d3 }
        L_0x00cd:
            if (r1 == 0) goto L_0x00d2
            r1.disconnect()     // Catch:{ Exception -> 0x00d3 }
        L_0x00d2:
            throw r0
        L_0x00d3:
            r1 = move-exception
            goto L_0x00d2
        L_0x00d5:
            r1 = move-exception
            r4 = r3
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00c3
        L_0x00db:
            r1 = move-exception
            r4 = r2
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00c3
        L_0x00e1:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00c3
        L_0x00e6:
            r1 = move-exception
            r3 = r2
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00c3
        L_0x00ec:
            r1 = move-exception
            r3 = r2
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00c3
        L_0x00f2:
            r0 = move-exception
            r3 = r1
            r1 = r4
            r4 = r2
            goto L_0x00c3
        L_0x00f7:
            r0 = move-exception
            r1 = r3
            r2 = r3
            r4 = r3
            goto L_0x004b
        L_0x00fd:
            r1 = move-exception
            r4 = r0
            r0 = r1
            r1 = r3
            goto L_0x004b
        L_0x0103:
            r1 = move-exception
            r2 = r4
            r4 = r0
            r0 = r1
            r1 = r3
            goto L_0x004b
        L_0x010a:
            r1 = move-exception
            r6 = r1
            r1 = r2
            r2 = r4
            r4 = r0
            r0 = r6
            goto L_0x004b
        L_0x0112:
            r1 = move-exception
            r6 = r1
            r1 = r2
            r2 = r4
            r4 = r0
            r0 = r6
            goto L_0x004b
        L_0x011a:
            r1 = r3
            r2 = r3
            goto L_0x00a9
        L_0x011d:
            r4 = r3
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.NetUtil.submitPostDataByte(java.lang.String, byte[], java.util.HashMap, int):byte[]");
    }

    public byte[] dealResponseResult(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
        while (true) {
            try {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return byteArrayOutputStream.toByteArray();
    }
}
