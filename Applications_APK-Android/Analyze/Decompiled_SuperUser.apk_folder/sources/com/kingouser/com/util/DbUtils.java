package com.kingouser.com.util;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.kingouser.com.entity.DeleteAppItem;
import java.util.ArrayList;
import java.util.Iterator;

public class DbUtils {
    public static void saveDeleteApp(SQLiteDatabase sQLiteDatabase, String str, ArrayList<DeleteAppItem> arrayList) {
        synchronized (DbUtils.class) {
            Iterator<DeleteAppItem> it = arrayList.iterator();
            while (it.hasNext()) {
                DeleteAppItem next = it.next();
                ContentValues contentValues = new ContentValues();
                contentValues.put("app_name", next.getAppName());
                contentValues.put("app_package", next.getAppPackage());
                contentValues.put("size", Long.valueOf(next.getCodeSize()));
                contentValues.put("source_dir", next.getCodePath());
                contentValues.put("data_dir", next.getDataDir());
                contentValues.put("native_library_dir", next.getNativeLibraryDir());
                sQLiteDatabase.insert(str, null, contentValues);
            }
        }
    }

    public static void deleteTableInfo(SQLiteDatabase sQLiteDatabase, String str) {
        sQLiteDatabase.delete(str, null, null);
    }
}
