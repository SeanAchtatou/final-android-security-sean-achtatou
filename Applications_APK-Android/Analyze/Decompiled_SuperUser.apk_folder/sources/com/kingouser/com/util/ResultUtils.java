package com.kingouser.com.util;

import android.text.TextUtils;
import com.kingouser.com.entity.httpEntity.UpdateEntity;
import com.pureapps.cleaner.util.f;
import org.json.JSONException;
import org.json.JSONObject;

public class ResultUtils {
    public static UpdateEntity parseUpdate(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        f.a("UpdateEntity:" + str);
        if (str.startsWith("null")) {
            str = str.replace("null", "");
        }
        f.a("UpdateEntity:" + str);
        UpdateEntity updateEntity = new UpdateEntity();
        try {
            JSONObject jSONObject = new JSONObject(str);
            updateEntity.msg = jSONObject.getString("msg");
            JSONObject jSONObject2 = jSONObject.getJSONObject("upgrade");
            if (jSONObject2 != null) {
                updateEntity.upgrade.clientversion = jSONObject2.getString("clientVersion");
                updateEntity.upgrade.signature = jSONObject2.getString("signMd5");
                updateEntity.upgrade.channel = jSONObject2.getString("channel");
                updateEntity.upgrade.downloadurl = jSONObject2.getString("downloadUrl");
                updateEntity.upgrade.packagename = jSONObject2.getString("packagename");
                updateEntity.upgrade.releasenode = jSONObject2.getString("releaseNode");
                updateEntity.upgrade.key_msg = jSONObject2.getString("key_msg");
                updateEntity.upgrade.version = jSONObject2.getInt("version");
                updateEntity.upgrade.langagestr = jSONObject2.getString("langageStr");
                updateEntity.upgrade.iconurl = jSONObject2.getString("iconUrl");
                updateEntity.upgrade.crc32 = jSONObject2.getString("crc32");
                updateEntity.upgrade.id = jSONObject2.getInt("id");
                updateEntity.upgrade.versionname = jSONObject2.getString("versionName");
                updateEntity.upgrade.md5 = jSONObject2.getString("md5");
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
            updateEntity.upgrade = null;
            updateEntity = null;
        }
        return updateEntity;
    }
}
