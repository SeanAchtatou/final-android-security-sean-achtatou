package com.kingouser.com.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.kingouser.com.application.App;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.ArrayList;
import java.util.concurrent.Executors;

public class SuHelper {
    public static String CURRENT_VERSION = "14";

    public static void checkSu(Context context, Handler handler) {
        try {
            Process exec = Runtime.getRuntime().exec("su -v");
            exec.waitFor();
            String readToEnd = Settings.readToEnd(exec.getInputStream());
            if (exec.waitFor() != 0) {
                throw new Exception("non zero result");
            } else if (readToEnd == null) {
                throw new Exception("no data");
            } else if (!readToEnd.contains("kingo")) {
                throw new Exception("unknown su");
            } else {
                if (!CURRENT_VERSION.equals(readToEnd.split(" ")[0])) {
                    throw new Exception("binary is old");
                } else if (!App.f4219a) {
                    throw new Exception("app no permission");
                }
            }
        } catch (Exception e2) {
            Message message = new Message();
            message.what = 80;
            handler.sendMessage(message);
        }
    }

    public static void getRemoveSuCommandSpecial(final Context context, final Handler handler, final ArrayList<String> arrayList, final String str) {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
             arg types: [java.lang.String, int]
             candidates:
              com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
            public void run() {
                boolean z;
                String str;
                boolean z2;
                int i = 0;
                try {
                    ApplicationInfo applicationInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 64).applicationInfo;
                    if ((applicationInfo.flags & FileUtils.FileMode.MODE_IWUSR) != 0) {
                        z2 = true;
                    } else if ((applicationInfo.flags & 1) == 0) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    z = z2;
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                    z = false;
                }
                String str2 = context.getApplicationInfo().nativeLibraryDir;
                FileUtils.copyFileFromAssets(context, str + "/busybox", "busybox");
                String str3 = str + "/busybox ";
                String str4 = "chmod 755 " + str3 + ";" + "mount -o remount,rw /system;" + str3 + " mount -o remount,rw /system;";
                while (true) {
                    str = str4;
                    if (i >= arrayList.size()) {
                        break;
                    }
                    String str5 = (String) arrayList.get(i);
                    if (TextUtils.isEmpty(str5)) {
                        str4 = str;
                    } else {
                        str4 = (((str + "chattr -i -a " + str5 + ";") + str3 + " chattr -i -a " + str5 + ";") + "rm -r " + str5 + ";") + str3 + " rm -r " + str5 + ";";
                    }
                    i++;
                }
                if (z) {
                    str = (str + "rm -r /system/app/Kingo*;") + str3 + "rm -r /system/app/Kingo*;";
                }
                ShellUtils.execCommand((((str + "pm uninstall com.kingouser.com;") + "rm -r " + str2 + ";rm -r /system/app/Kingo*;") + str3 + "rm -r " + str2 + ";" + str3 + "rm -r /system/app/Kingo*;") + str3 + "rm -r " + str2 + ";" + "rm -r " + str2 + ";pm uninstall com.kingouser.com;", true);
                Message message = new Message();
                message.what = 107;
                handler.sendMessage(message);
            }
        });
    }

    /* access modifiers changed from: private */
    public static String restoreAppProcess(String str, String str2) {
        return (((((str2 + "mv /system/bin/app_process64_original /system/bin/app_process64;") + "rm /system/bin/app_process_init /system/bin/app_process;") + str + " ln -s /system/bin/app_process64 /system/bin/app_process;") + "mv /system/bin/app_process32_original /system/bin/app_process32;") + "rm /system/bin/app_process_init /system/bin/app_process;") + str + " ln -s /system/bin/app_process32 /system/bin/app_process;";
    }

    public static void getRemoveSuCommandSpecialQ505(final Context context, final Handler handler, final ArrayList<String> arrayList, final String str) {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
             arg types: [java.lang.String, int]
             candidates:
              com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
            public void run() {
                int i = 0;
                String str = context.getApplicationInfo().nativeLibraryDir;
                FileUtils.copyFileFromAssets(context, str + "/busybox", "busybox");
                String str2 = str + "/busybox ";
                String str3 = "chmod 755 " + str2 + ";" + "mount -o remount,rw /system;" + str2 + " mount -o remount,rw /system;";
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    String str4 = (String) arrayList.get(i2);
                    if (!TextUtils.isEmpty(str4)) {
                        str3 = (str3 + "chattr -i -a " + str4 + ";") + str2 + " chattr -i -a " + str4 + ";";
                    }
                }
                String str5 = (((str3 + "pm uninstall com.kingouser.com;") + "rm -r /system/app/Kingo*;") + str2 + "rm -r /system/app/Kingo*;") + "pm uninstall com.kingouser.com;";
                while (true) {
                    String str6 = str5;
                    if (i < arrayList.size()) {
                        String str7 = (String) arrayList.get(i);
                        if (TextUtils.isEmpty(str7)) {
                            str5 = str6;
                        } else {
                            str5 = (str6 + "rm -r " + str7 + ";") + str2 + " rm -r " + str7 + ";";
                        }
                        i++;
                    } else {
                        ShellUtils.execCommand(SuHelper.restoreAppProcess(str2, str6), true);
                        Message message = new Message();
                        message.what = 107;
                        handler.sendMessage(message);
                        return;
                    }
                }
            }
        });
    }

    public static void testMkdevsh(final Context context) {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
             arg types: [java.lang.String, int]
             candidates:
              com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
            public void run() {
                FileUtils.isExistsAndCopy(context, "busybox");
                FileUtils.isExistsAndCopy(context, "mkdevsh");
                ShellUtils.execCommand("chmod 6755 /data/local/tmp/busybox;cat " + (context.getFilesDir() + "/mkdevsh") + "> /data/local/tmp/mkdevsh;" + "chmod 755 /data/local/tmp/mkdevsh;/data/local/tmp/mkdevsh;", true);
            }
        });
    }

    public static void getRemoveSuCommand(final Context context, final Handler handler, ArrayList<String> arrayList, final String str) {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
             arg types: [java.lang.String, int]
             candidates:
              com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
            public void run() {
                if (Integer.valueOf(DeviceInfoUtils.getSDKVersion()).intValue() > 20) {
                    ShellUtils.execCommand("pm uninstall com.kingouser.com", true);
                }
                String str = context.getApplicationInfo().nativeLibraryDir;
                FileUtils.copyFileFromAssets(context, str + "/busybox", "busybox");
                String str2 = str + "/busybox ";
                String str3 = "chmod 755 " + str2 + ";" + "mount -o remount,rw /system;" + str2 + " mount -o remount,rw /system;";
                MyLog.e("PermissionService", "removeSuCommand0 = " + str3);
                System.out.println("removeSuCommand0 = " + str3);
                ShellUtils.execCommand(str3, true);
                String str4 = ((("" + str2 + " mount -o remount,rw /system;") + str2 + " rm -r /system/app/KingoUser*;") + str2 + " rm -r " + str + ";") + "pm uninstall com.kingouser.com;";
                if (Integer.valueOf(DeviceInfoUtils.getSDKVersion()).intValue() > 20) {
                    str4 = str4 + "reboot";
                }
                MyLog.e("PermissionService", "removeSuCommand1 = " + str4);
                System.out.println("removeSuCommand1 = " + str4);
                ShellUtils.execCommand(str4, true);
                Message message = new Message();
                message.what = 107;
                handler.sendMessage(message);
            }
        });
    }

    private ArrayList<String> getRemovePath() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("/system/xbin/su");
        arrayList.add("/system/xbin/daemonsu");
        arrayList.add("/system/bin/su");
        arrayList.add("/system/sbin/su");
        arrayList.add("/system/xbin/supolicy");
        arrayList.add("/system/lib/libsupol.so");
        arrayList.add("/system/xbin/su");
        arrayList.add("/system/xbin/su");
        arrayList.add("/system/app/Kingo*");
        return arrayList;
    }
}
