package com.kingouser.com.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.kingouser.com.entity.InstallAppEntity;
import com.kingouser.com.entity.RequestEntity;
import com.kingouser.com.entity.RequestsEntity;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class FileUtils {
    private static ArrayList<String> pathList;
    private static long totalSize;

    public static boolean isEmpty(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null && listFiles.length > 0) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    if (!isEmpty(file2)) {
                        return false;
                    }
                } else if (file2.isFile()) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void listFile(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null && listFiles.length > 0) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    listFile(file2);
                } else if (isLogFile(file2)) {
                    pathList.add(file2.getAbsolutePath());
                    totalSize += file2.length();
                }
            }
        }
    }

    public static ArrayList<File> listFiles(File file) {
        ArrayList<File> arrayList = new ArrayList<>();
        try {
            File[] listFiles = file.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (File file2 : listFiles) {
                    if (file2.isDirectory()) {
                        listFile(file2);
                    } else {
                        arrayList.add(file2);
                    }
                }
            }
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
        }
        return arrayList;
    }

    private static boolean isLogFile(File file) {
        if (file.getName().toLowerCase().endsWith(".log")) {
            return true;
        }
        return false;
    }

    public static void deleteFileByPath(String str) {
        File file = new File(str);
        if (file != null && file.exists()) {
            if (file.isDirectory()) {
                File[] listFiles = file.listFiles();
                if (listFiles != null) {
                    for (File absolutePath : listFiles) {
                        deleteFileByPath(absolutePath.getAbsolutePath());
                    }
                }
                file.delete();
            } else if (file.isFile()) {
                file.delete();
            }
        }
    }

    public static void deleteFileByList(ArrayList<String> arrayList) {
        if (arrayList != null) {
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                deleteFileByPath(it.next());
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003b A[SYNTHETIC, Splitter:B:22:0x003b] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0044 A[SYNTHETIC, Splitter:B:27:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void write(java.lang.String r3, java.lang.String r4, boolean r5) {
        /*
            java.io.File r0 = new java.io.File
            r0.<init>(r3)
            java.io.File r1 = r0.getParentFile()
            boolean r2 = r1.exists()
            if (r2 != 0) goto L_0x0012
            r1.mkdir()
        L_0x0012:
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x001b
            r0.createNewFile()     // Catch:{ IOException -> 0x002f }
        L_0x001b:
            r2 = 0
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x0034, all -> 0x0041 }
            java.io.FileWriter r0 = new java.io.FileWriter     // Catch:{ Exception -> 0x0034, all -> 0x0041 }
            r0.<init>(r3, r5)     // Catch:{ Exception -> 0x0034, all -> 0x0041 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0034, all -> 0x0041 }
            r1.write(r4)     // Catch:{ Exception -> 0x004f }
            if (r1 == 0) goto L_0x002e
            r1.close()     // Catch:{ IOException -> 0x0048 }
        L_0x002e:
            return
        L_0x002f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x001b
        L_0x0034:
            r0 = move-exception
            r1 = r2
        L_0x0036:
            r0.printStackTrace()     // Catch:{ all -> 0x004c }
            if (r1 == 0) goto L_0x002e
            r1.close()     // Catch:{ IOException -> 0x003f }
            goto L_0x002e
        L_0x003f:
            r0 = move-exception
            goto L_0x002e
        L_0x0041:
            r0 = move-exception
        L_0x0042:
            if (r2 == 0) goto L_0x0047
            r2.close()     // Catch:{ IOException -> 0x004a }
        L_0x0047:
            throw r0
        L_0x0048:
            r0 = move-exception
            goto L_0x002e
        L_0x004a:
            r1 = move-exception
            goto L_0x0047
        L_0x004c:
            r0 = move-exception
            r2 = r1
            goto L_0x0042
        L_0x004f:
            r0 = move-exception
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.FileUtils.write(java.lang.String, java.lang.String, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    public static void writeContent(String str, String str2) {
        BufferedWriter bufferedWriter;
        BufferedWriter bufferedWriter2 = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(str, true));
            try {
                bufferedWriter.write(str2);
                try {
                    bufferedWriter.close();
                } catch (Exception e2) {
                }
            } catch (Exception e3) {
                try {
                    bufferedWriter.close();
                } catch (Exception e4) {
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                bufferedWriter2 = bufferedWriter;
                th = th2;
                try {
                    bufferedWriter2.close();
                } catch (Exception e5) {
                }
                throw th;
            }
        } catch (Exception e6) {
            bufferedWriter = null;
            bufferedWriter.close();
        } catch (Throwable th3) {
            th = th3;
            bufferedWriter2.close();
            throw th;
        }
    }

    public static void saveInstallChannel(Context context, String str, String str2) {
        MyLog.e("TAG", "保存了安装渠道。。。。。。。。。。。。。。。。。。。。。。。。。");
        String str3 = "";
        Gson gson = new Gson();
        ArrayList arrayList = new ArrayList();
        try {
            str3 = context.getPackageManager().getPackageInfo(context.getPackageName(), (int) CodedOutputStream.DEFAULT_BUFFER_SIZE).versionName;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(Long.valueOf(UtcTimeUtils.getUtcTimeDirectly()));
        arrayList2.add(str);
        arrayList2.add("KingoUser");
        if (TextUtils.isEmpty(str2)) {
            arrayList2.add("OffcialSite");
            arrayList2.add(str3);
            arrayList2.add(null);
        } else {
            arrayList2.add(str3);
            arrayList2.add("OffcialSite");
            arrayList2.add(str2);
        }
        arrayList.add(arrayList2);
        InstallAppEntity installAppEntity = new InstallAppEntity();
        installAppEntity.setData(arrayList);
        installAppEntity.setPid(context.getPackageName());
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(Long.valueOf(UtcTimeUtils.getUtcTimeDirectly()));
        installAppEntity.setTime(arrayList3);
        String imei = DeviceInfoUtils.getImei(context);
        String readDeviceId = DeviceInfoUtils.readDeviceId(context);
        if (TextUtils.isEmpty(imei)) {
            installAppEntity.setId("");
        } else {
            installAppEntity.setId(EncodeMD5.getMD5To32String(imei));
        }
        installAppEntity.setUser_id(EncodeMD5.getMD5To32String(readDeviceId));
        write(context.getFilesDir() + "/systemoruser/" + System.currentTimeMillis(), RC4EncodeUtils.encry_RC4_string(gson.toJson(installAppEntity), "SJK@9x.1"), false);
    }

    public static void saveActive(Context context, String str) {
        MyLog.e("TAG", "保存了apk活跃。。。。。。。。。。。。。。。。。。。。。。。。。");
        Gson gson = new Gson();
        ArrayList arrayList = new ArrayList();
        if (TextUtils.isEmpty(str)) {
            try {
                str = context.getPackageManager().getPackageInfo(context.getPackageName(), (int) CodedOutputStream.DEFAULT_BUFFER_SIZE).versionName;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        ArrayList arrayList2 = new ArrayList();
        try {
            arrayList2.add(Long.valueOf(UtcTimeUtils.getUtcTimeDirectly()));
        } catch (Exception e3) {
            arrayList2.add(0);
        }
        arrayList2.add("Active");
        arrayList2.add("KingoUser");
        MyLog.e("Tag", str);
        arrayList2.add(str);
        arrayList2.add("OffcialSite");
        arrayList2.add(null);
        arrayList.add(arrayList2);
        InstallAppEntity installAppEntity = new InstallAppEntity();
        installAppEntity.setData(arrayList);
        installAppEntity.setPid(context.getPackageName());
        ArrayList arrayList3 = new ArrayList();
        try {
            arrayList3.add(Long.valueOf(UtcTimeUtils.getUtcTimeDirectly()));
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        installAppEntity.setTime(arrayList3);
        String imei = DeviceInfoUtils.getImei(context);
        String readDeviceId = DeviceInfoUtils.readDeviceId(context);
        if (TextUtils.isEmpty(imei)) {
            installAppEntity.setId("");
        } else {
            installAppEntity.setId(EncodeMD5.getMD5To32String(imei));
        }
        installAppEntity.setUser_id(EncodeMD5.getMD5To32String(readDeviceId));
        write(context.getFilesDir() + "/systemoruser/" + System.currentTimeMillis(), RC4EncodeUtils.encry_RC4_string(gson.toJson(installAppEntity), "SJK@9x.1"), false);
        MySharedPreference.setActiveTime(context, System.currentTimeMillis());
    }

    public static void saveSuRequest(Context context, String str) {
        int i;
        MyLog.e("TAG", "保存了su请求。。。。。。。。。。。。。。。。。。。。。。。。。");
        String str2 = "";
        Gson gson = new Gson();
        ArrayList arrayList = new ArrayList();
        try {
            str2 = context.getPackageManager().getPackageInfo(context.getPackageName(), (int) CodedOutputStream.DEFAULT_BUFFER_SIZE).versionName;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            Process exec = Runtime.getRuntime().exec("su -v");
            exec.waitFor();
            i = Integer.valueOf(Settings.readToEnd(exec.getInputStream()).split(" ")[0]).intValue();
        } catch (Exception e3) {
            i = 0;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(Long.valueOf(UtcTimeUtils.getUtcTimeDirectly()));
        arrayList2.add("Active");
        arrayList2.add("SU");
        if (ShellUtils.checkRoot(context)) {
            arrayList2.add("success");
        } else {
            arrayList2.add("failed");
        }
        arrayList2.add(Integer.valueOf(i));
        arrayList2.add(str);
        arrayList2.add(str2);
        arrayList.add(arrayList2);
        InstallAppEntity installAppEntity = new InstallAppEntity();
        installAppEntity.setData(arrayList);
        installAppEntity.setPid(context.getPackageName());
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(Long.valueOf(UtcTimeUtils.getUtcTimeDirectly()));
        installAppEntity.setTime(arrayList3);
        String imei = DeviceInfoUtils.getImei(context);
        String readDeviceId = DeviceInfoUtils.readDeviceId(context);
        if (TextUtils.isEmpty(imei)) {
            installAppEntity.setId("");
        } else {
            installAppEntity.setId(EncodeMD5.getMD5To32String(imei));
        }
        installAppEntity.setUser_id(EncodeMD5.getMD5To32String(readDeviceId));
        write(context.getFilesDir() + "/systemoruser/" + System.currentTimeMillis(), RC4EncodeUtils.encry_RC4_string(gson.toJson(installAppEntity), "SJK@9x.1"), false);
        MySharedPreference.setActiveTime(context, System.currentTimeMillis());
    }

    public static void saveActiveSu(Context context, String str) {
        String str2 = context.getFilesDir() + "/activesu";
        ArrayList<File> listFiles = listFiles(new File(str2));
        try {
            Gson gson = new Gson();
            if (listFiles.size() > 0) {
                for (int i = 0; i < listFiles.size(); i++) {
                    File file = listFiles.get(i);
                    String name = file.getName();
                    if (Math.abs(MySharedPreference.getActiveTime(context, 0) - System.currentTimeMillis()) <= 86400000) {
                        String decry_RC4 = RC4EncodeUtils.decry_RC4(readFile(file), "SJK@9x.1");
                        ArrayList arrayList = new ArrayList();
                        RequestsEntity requestsEntity = new RequestsEntity();
                        JSONArray jSONArray = new JSONObject(decry_RC4).getJSONArray("requestEntities");
                        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                            JSONObject jSONObject = jSONArray.getJSONObject(i2);
                            RequestEntity requestEntity = new RequestEntity();
                            requestEntity.setPackageName(jSONObject.getString("packageName"));
                            requestEntity.setRequestNum(jSONObject.getInt("requestNum"));
                            requestEntity.setRequestTime(jSONObject.getLong("requestTime"));
                            arrayList.add(requestEntity);
                        }
                        for (int i3 = 0; i3 < arrayList.size(); i3++) {
                            RequestEntity requestEntity2 = (RequestEntity) arrayList.get(i3);
                            if (requestEntity2.getPackageName().equalsIgnoreCase(str)) {
                                requestEntity2.setRequestNum(requestEntity2.getRequestNum() + 1);
                                requestsEntity.setRequestEntities(arrayList);
                                write(str2 + "/" + name, RC4EncodeUtils.encry_RC4_string(gson.toJson(requestsEntity), "SJK@9x.1"), false);
                                return;
                            }
                        }
                        RequestEntity requestEntity3 = new RequestEntity();
                        requestEntity3.setPackageName(str);
                        requestEntity3.setRequestNum(1);
                        requestEntity3.setRequestTime(Long.valueOf(UtcTimeUtils.getUtcTimeDirectly()).longValue());
                        arrayList.add(requestEntity3);
                        requestsEntity.setRequestEntities(arrayList);
                        write(str2 + "/" + name, RC4EncodeUtils.encry_RC4_string(gson.toJson(requestsEntity), "SJK@9x.1"), false);
                    } else {
                        RequestsEntity requestsEntity2 = new RequestsEntity();
                        ArrayList arrayList2 = new ArrayList();
                        RequestEntity requestEntity4 = new RequestEntity();
                        requestEntity4.setPackageName(str);
                        requestEntity4.setRequestTime(Long.valueOf(UtcTimeUtils.getUtcTimeDirectly()).longValue());
                        requestEntity4.setRequestNum(1);
                        arrayList2.add(requestEntity4);
                        requestsEntity2.setRequestEntities(arrayList2);
                        write(str2 + "/" + System.currentTimeMillis(), RC4EncodeUtils.encry_RC4_string(gson.toJson(requestsEntity2), "SJK@9x.1"), false);
                    }
                }
                return;
            }
            RequestsEntity requestsEntity3 = new RequestsEntity();
            ArrayList arrayList3 = new ArrayList();
            RequestEntity requestEntity5 = new RequestEntity();
            requestEntity5.setPackageName(str);
            requestEntity5.setRequestTime(Long.valueOf(UtcTimeUtils.getUtcTimeDirectly()).longValue());
            requestEntity5.setRequestNum(1);
            arrayList3.add(requestEntity5);
            requestsEntity3.setRequestEntities(arrayList3);
            write(str2 + "/" + System.currentTimeMillis(), RC4EncodeUtils.encry_RC4_string(gson.toJson(requestsEntity3), "SJK@9x.1"), false);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0040 A[SYNTHETIC, Splitter:B:16:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0045 A[Catch:{ Exception -> 0x005c }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0065 A[SYNTHETIC, Splitter:B:34:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006a A[Catch:{ Exception -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void copyAssetFile(android.content.Context r5, java.lang.String r6) {
        /*
            r2 = 0
            android.content.res.AssetManager r0 = r5.getAssets()     // Catch:{ IOException -> 0x007c, all -> 0x0061 }
            java.io.InputStream r3 = r0.open(r6)     // Catch:{ IOException -> 0x007c, all -> 0x0061 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x007f, all -> 0x0073 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x007f, all -> 0x0073 }
            r0.<init>()     // Catch:{ IOException -> 0x007f, all -> 0x0073 }
            java.io.File r4 = r5.getFilesDir()     // Catch:{ IOException -> 0x007f, all -> 0x0073 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ IOException -> 0x007f, all -> 0x0073 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ IOException -> 0x007f, all -> 0x0073 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ IOException -> 0x007f, all -> 0x0073 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x007f, all -> 0x0073 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x007f, all -> 0x0073 }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x0039, all -> 0x0075 }
        L_0x002d:
            int r2 = r3.read(r0)     // Catch:{ IOException -> 0x0039, all -> 0x0075 }
            r4 = -1
            if (r2 == r4) goto L_0x0049
            r4 = 0
            r1.write(r0, r4, r2)     // Catch:{ IOException -> 0x0039, all -> 0x0075 }
            goto L_0x002d
        L_0x0039:
            r0 = move-exception
            r2 = r3
        L_0x003b:
            r0.printStackTrace()     // Catch:{ all -> 0x0078 }
            if (r2 == 0) goto L_0x0043
            r2.close()     // Catch:{ Exception -> 0x005c }
        L_0x0043:
            if (r1 == 0) goto L_0x0048
            r1.close()     // Catch:{ Exception -> 0x005c }
        L_0x0048:
            return
        L_0x0049:
            r1.flush()     // Catch:{ IOException -> 0x0039, all -> 0x0075 }
            if (r3 == 0) goto L_0x0051
            r3.close()     // Catch:{ Exception -> 0x0057 }
        L_0x0051:
            if (r1 == 0) goto L_0x0048
            r1.close()     // Catch:{ Exception -> 0x0057 }
            goto L_0x0048
        L_0x0057:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0048
        L_0x005c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0048
        L_0x0061:
            r0 = move-exception
            r3 = r2
        L_0x0063:
            if (r3 == 0) goto L_0x0068
            r3.close()     // Catch:{ Exception -> 0x006e }
        L_0x0068:
            if (r2 == 0) goto L_0x006d
            r2.close()     // Catch:{ Exception -> 0x006e }
        L_0x006d:
            throw r0
        L_0x006e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006d
        L_0x0073:
            r0 = move-exception
            goto L_0x0063
        L_0x0075:
            r0 = move-exception
            r2 = r1
            goto L_0x0063
        L_0x0078:
            r0 = move-exception
            r3 = r2
            r2 = r1
            goto L_0x0063
        L_0x007c:
            r0 = move-exception
            r1 = r2
            goto L_0x003b
        L_0x007f:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.FileUtils.copyAssetFile(android.content.Context, java.lang.String):void");
    }

    public static long getFileSize(File file) {
        long length;
        long j = 0;
        if (!file.exists() || file == null || !file.exists()) {
            return 0;
        }
        if (!file.isDirectory()) {
            return file.length();
        }
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return 0;
        }
        for (int i = 0; i < listFiles.length; i++) {
            if (listFiles[i].isDirectory()) {
                length = getFileSize(listFiles[i]);
            } else {
                length = listFiles[i].length();
            }
            j += length;
        }
        return j;
    }

    public static long getMediaImageTotalSize(Context context) {
        long j = 0;
        Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_size"}, null, null, null);
        if (query != null) {
            long j2 = 0;
            while (query.moveToNext()) {
                j2 += query.getLong(0);
            }
            query.close();
            j = j2;
        }
        Cursor query2 = context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, new String[]{"_size"}, null, null, null);
        if (query2 != null) {
            while (query2.moveToNext()) {
                j += query2.getLong(0);
            }
            query2.close();
        }
        return j;
    }

    public static long getMediaAudioTotalSize(Context context) {
        long j = 0;
        Cursor query = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_size"}, null, null, null);
        if (query != null) {
            long j2 = 0;
            while (query.moveToNext()) {
                j2 += query.getLong(0);
            }
            query.close();
            j = j2;
        }
        Cursor query2 = context.getContentResolver().query(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, new String[]{"_size"}, null, null, null);
        if (query2 != null) {
            while (query2.moveToNext()) {
                j += query2.getLong(0);
            }
            query2.close();
        }
        return j;
    }

    public static long getMediaVideoTotalSize(Context context) {
        long j = 0;
        Cursor query = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, new String[]{"_size"}, null, null, null);
        if (query != null) {
            long j2 = 0;
            while (query.moveToNext()) {
                j2 += query.getLong(0);
            }
            query.close();
            j = j2;
        }
        Cursor query2 = context.getContentResolver().query(MediaStore.Video.Media.INTERNAL_CONTENT_URI, new String[]{"_size"}, null, null, null);
        if (query2 != null) {
            while (query2.moveToNext()) {
                j += query2.getLong(0);
            }
            query2.close();
        }
        return j;
    }

    public static String readFile(String str) {
        File file = new File(str);
        byte[] bArr = new byte[((int) file.length())];
        new DataInputStream(new FileInputStream(file)).readFully(bArr);
        return new String(bArr);
    }

    public static String readFile(File file) {
        byte[] bArr = new byte[((int) file.length())];
        new DataInputStream(new FileInputStream(file)).readFully(bArr);
        return new String(bArr);
    }

    public static void copyFileFromAssets(Context context, String str, String str2) {
        if (!new File(str).exists()) {
            AssetManager assets = context.getAssets();
            File file = new File(str);
            try {
                InputStream open = assets.open(str2);
                file.createNewFile();
                byte[] bArr = new byte[open.available()];
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                while (true) {
                    int read = open.read(bArr, 0, bArr.length);
                    if (read != -1) {
                        fileOutputStream.write(bArr, 0, read);
                    } else {
                        fileOutputStream.close();
                        open.close();
                        return;
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                file.delete();
                try {
                    FileOutputStream fileOutputStream2 = new FileOutputStream(new File(context.getFilesDir(), "mylog"));
                    fileOutputStream2.write(e2.toString().getBytes());
                    fileOutputStream2.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static void isExistsAndCopy(Context context, String str) {
        String str2 = context.getFilesDir() + File.separator + str;
        File file = new File(str2);
        int i = 0;
        try {
            i = context.getResources().getAssets().open(str).available();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (!file.exists()) {
            copyFileFromAssets(context, str2, str);
        } else if (((long) i) != file.length()) {
            copyFileFromAssets(context, str2, str);
        }
        if (str.equals("busybox")) {
            ShellUtils.execCommand("chmod 755 " + str2, true);
        }
    }

    public static void isExistsAndCopy(Context context, String str, String str2) {
        String str3 = context.getFilesDir() + File.separator + str;
        File file = new File(str3);
        int i = 0;
        try {
            i = context.getResources().getAssets().open(str2).available();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (!file.exists()) {
            copyFileFromAssets(context, str3, str2);
        } else if (((long) i) != file.length()) {
            copyFileFromAssets(context, str3, str2);
        }
    }

    public static boolean checkFileExist(Context context, String str) {
        return new File(str).exists();
    }

    public static void createConfig(Context context) {
        String str;
        if (Integer.valueOf(DeviceInfoUtils.getSDKVersion()).intValue() < 18) {
            str = "config_nonpie";
        } else {
            str = "config_pie";
        }
        isExistsAndCopy(context, "config", str);
    }

    public static Boolean CopyAssetsFile(Context context, String str, String str2) {
        try {
            InputStream open = context.getAssets().open(str);
            FileOutputStream fileOutputStream = new FileOutputStream(str2 + "/" + str);
            byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
            while (true) {
                int read = open.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    open.close();
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    return true;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static Boolean CopyAssetsDir(Context context, String str, String str2) {
        Boolean bool = true;
        try {
            String[] list = context.getResources().getAssets().list(str);
            if (list.length == 0) {
                Boolean CopyAssetsFile = CopyAssetsFile(context, str, str2);
                if (!CopyAssetsFile.booleanValue()) {
                }
                return CopyAssetsFile;
            }
            File file = new File(str2 + "/" + str);
            if (file.exists()) {
                return bool;
            }
            if (!file.mkdir()) {
                return false;
            }
            for (int i = 0; i < list.length; i++) {
                bool = CopyAssetsDir(context, str + "/" + list[i], str2);
                if (!bool.booleanValue()) {
                    return bool;
                }
            }
            return bool;
        } catch (IOException e2) {
            return false;
        }
    }

    public static void checkConfigFile(Context context) {
        String str;
        if (Integer.valueOf(DeviceInfoUtils.getSDKVersion()).intValue() < 18) {
            str = "config_nonpie";
        } else {
            str = "config_pie";
        }
        isExistsAndCopy(context, "config", str);
    }
}
