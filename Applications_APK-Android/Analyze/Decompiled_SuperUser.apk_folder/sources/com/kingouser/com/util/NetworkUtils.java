package com.kingouser.com.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NetworkUtils {
    public static final int NETWORK_TYPE_2G = 1;
    public static final int NETWORK_TYPE_3G = 2;
    public static final int NETWORK_TYPE_MOBILE = 3;
    public static final int NETWORK_TYPE_NONE = 0;
    public static final int NETWORK_TYPE_OTHER = 5;
    public static final int NETWORK_TYPE_WIFI = 4;
    private static ConnectivityManager connectivityMgr;
    private static TelephonyManager telMgr;

    public static int getNetWorkType(Context context) {
        if (connectivityMgr == null) {
            connectivityMgr = (ConnectivityManager) context.getSystemService("connectivity");
        }
        if (connectivityMgr == null) {
            return 0;
        }
        NetworkInfo activeNetworkInfo = connectivityMgr.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return 0;
        }
        if (activeNetworkInfo.getType() == 0) {
            if (telMgr == null) {
                telMgr = (TelephonyManager) context.getSystemService("phone");
            }
            if (telMgr == null) {
                return 3;
            }
            switch (telMgr.getNetworkType()) {
                case 1:
                case 2:
                    return 1;
                case 3:
                case 8:
                case 9:
                case 10:
                    return 2;
                case 4:
                case 5:
                case 6:
                case 7:
                default:
                    return 3;
            }
        } else if (1 == activeNetworkInfo.getType()) {
            return 4;
        } else {
            return 5;
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        if (connectivityMgr == null) {
            connectivityMgr = (ConnectivityManager) context.getSystemService("connectivity");
        }
        if (connectivityMgr == null) {
            return false;
        }
        try {
            NetworkInfo activeNetworkInfo = connectivityMgr.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public static void openAppByWeb(Activity activity, String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(str));
            activity.startActivity(intent);
        } catch (Exception e2) {
            Toast.makeText(activity, "Please install a browser first", 0).show();
        }
    }

    public static void openAppByGooglePlay(Activity activity, String str) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.imangi.templerun"));
        intent.setClassName("com.android.vending", "com.android.vending.AssetBrowserActivity");
        intent.setFlags(268435456);
        activity.startActivity(intent);
    }

    public static void openURL(String str, Context context) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setFlags(402653184);
        intent.setData(Uri.parse(str));
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        ArrayList<ResolveInfo> arrayList = new ArrayList<>(Arrays.asList(new ResolveInfo[queryIntentActivities.size()]));
        Collections.copy(arrayList, queryIntentActivities);
        if (queryIntentActivities != null) {
            for (ResolveInfo resolveInfo : arrayList) {
                if (!isSystemApp(resolveInfo.activityInfo.applicationInfo)) {
                    queryIntentActivities.remove(resolveInfo);
                }
            }
            if (queryIntentActivities.size() > 0) {
                ResolveInfo resolveInfo2 = queryIntentActivities.get(0);
                intent.setClassName(resolveInfo2.activityInfo.packageName, resolveInfo2.activityInfo.name);
            }
            context.startActivity(intent);
            return;
        }
        context.startActivity(intent);
    }

    public static void showMarket(Context context, String str) {
        if (AppManagerUtils.appIsInstall(context, "com.android.vending", true) || AppManagerUtils.appIsInstall(context, "com.android.vending", false)) {
            try {
                Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage("com.android.vending");
                launchIntentForPackage.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.activities.LaunchUrlHandlerActivity"));
                launchIntentForPackage.setData(Uri.parse("market://details?id=" + str));
                context.startActivity(launchIntentForPackage);
            } catch (ActivityNotFoundException e2) {
                openURL("http://play.google.com/store/apps/details?id=\" + packageName", context);
            }
        } else {
            openURL("http://play.google.com/store/apps/details?id=\" + packageName", context);
        }
    }

    public static boolean isSystemApp(ApplicationInfo applicationInfo) {
        if ((applicationInfo.flags & 1) == 1) {
            return true;
        }
        return false;
    }
}
