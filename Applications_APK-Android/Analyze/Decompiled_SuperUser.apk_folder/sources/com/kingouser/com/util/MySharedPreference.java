package com.kingouser.com.util;

import android.content.Context;

public class MySharedPreference {
    public static void setDownloadVersion(Context context, String str) {
        context.getSharedPreferences("download_version", 0).edit().putString("download_version", str).commit();
    }

    public static String getDownloadVersion(Context context, String str) {
        return context.getSharedPreferences("download_version", 0).getString("download_version", str);
    }

    public static void setGoogleAnalyticsPeriodTime(Context context, long j) {
        context.getSharedPreferences("google_analytics_period_time", 0).edit().putLong("google_analytics_period_time", j);
    }

    public static void setGoogleAnalyticsServiceRigist(Context context, boolean z) {
        context.getSharedPreferences("google_analytics_service_regist", 0).edit().putBoolean("google_analytics_service_regist", z).commit();
    }

    public static void setDownloadProgress(Context context, int i) {
        context.getSharedPreferences("download_progress", 0).edit().putInt("download_progress", i).commit();
    }

    public static int getDownLoadProgress(Context context, int i) {
        return context.getSharedPreferences("download_progress", 0).getInt("download_progress", 0);
    }

    public static void setWheatherFirstRun(Context context, boolean z) {
        context.getSharedPreferences("weather_first_run", 0).edit().putBoolean("weather_first_run", z).commit();
    }

    public static boolean getWheaterFirstRun(Context context, boolean z) {
        return context.getSharedPreferences("weather_first_run", 0).getBoolean("weather_first_run", z);
    }

    public static boolean getWheaterRoot(Context context, boolean z) {
        return context.getSharedPreferences("weather_root", 0).getBoolean("weather_root", z);
    }

    public static void setWheaterRoot(Context context, boolean z) {
        context.getSharedPreferences("weather_root", 0).edit().putBoolean("weather_root", z).commit();
    }

    public static boolean getWheaterOnResume(Context context, boolean z) {
        return context.getSharedPreferences("weather_on_resume", 0).getBoolean("weather_on_resume", z);
    }

    public static void setWheaterOnResume(Context context, boolean z) {
        context.getSharedPreferences("weather_on_resume", 0).edit().putBoolean("weather_on_resume", z).commit();
    }

    public static boolean getWheaterPolicyFragmentOnResume(Context context, boolean z) {
        return context.getSharedPreferences("weather_policyfragment_on_resume", 0).getBoolean("weather_policyfragment_on_resume", z);
    }

    public static void setWheaterPolicyFragmentOnResume(Context context, boolean z) {
        context.getSharedPreferences("weather_policyfragment_on_resume", 0).edit().putBoolean("weather_policyfragment_on_resume", z).commit();
    }

    public static void setRequestDialogTime(Context context, int i) {
        context.getSharedPreferences("request_dialog_time", 0).edit().putInt("request_dialog_time", i).commit();
    }

    public static int getRequestDialogTimes(Context context, int i) {
        return context.getSharedPreferences("request_dialog_time", 0).getInt("request_dialog_time", i);
    }

    public static boolean getWheaterToast(Context context, boolean z) {
        return context.getSharedPreferences("weather_toast", 0).getBoolean("weather_toast", z);
    }

    public static void setWheaterToast(Context context, boolean z) {
        context.getSharedPreferences("weather_toast", 0).edit().putBoolean("weather_toast", z).commit();
    }

    public static boolean getWheaterSend(Context context, boolean z) {
        return context.getSharedPreferences("weather_send", 0).getBoolean("weather_send", z);
    }

    public static void setWheaterSend(Context context, boolean z) {
        context.getSharedPreferences("weather_send", 0).edit().putBoolean("weather_send", z).commit();
    }

    public static long getActiveTime(Context context, long j) {
        return context.getSharedPreferences("active_time", 0).getLong("active_time", j);
    }

    public static void setActiveTime(Context context, long j) {
        context.getSharedPreferences("active_time", 0).edit().putLong("active_time", j).commit();
    }

    public static String getMainActivityLocalLanguage(Context context, String str) {
        return context.getSharedPreferences("mainactivity_local_language", 0).getString("mainactivity_local_language", str);
    }

    public static void setMainActivityLocalLanguage(Context context, String str) {
        context.getSharedPreferences("mainactivity_local_language", 0).edit().putString("mainactivity_local_language", str).commit();
    }

    public static String getAboutActivityLocalLanguage(Context context, String str) {
        return context.getSharedPreferences("aboutactivity_local_language", 0).getString("aboutactivity_local_language", str);
    }

    public static void setAboutActivityLocalLanguage(Context context, String str) {
        context.getSharedPreferences("aboutactivity_local_language", 0).edit().putString("aboutactivity_local_language", str).commit();
    }

    public static String getPolicyFragmentLocalLanguage(Context context, String str) {
        return context.getSharedPreferences("policyfragment_local_language", 0).getString("policyfragment_local_language", str);
    }

    public static void setPolicyFragmentLocalLanguage(Context context, String str) {
        context.getSharedPreferences("policyfragment_local_language", 0).edit().putString("policyfragment_local_language", str).commit();
    }

    public static String getDeleteFragmentLocalLanguage(Context context, String str) {
        return context.getSharedPreferences("deletefragment_local_language", 0).getString("deletefragment_local_language", str);
    }

    public static void setDeleteFragmentLocalLanguage(Context context, String str) {
        context.getSharedPreferences("deletefragment_local_language", 0).edit().putString("deletefragment_local_language", str).commit();
    }

    public static String getRecommandFragmentLocalLanguage(Context context, String str) {
        return context.getSharedPreferences("recommandfragment_local_language", 0).getString("recommandfragment_local_language", str);
    }

    public static void setRecommandFragmentLocalLanguage(Context context, String str) {
        context.getSharedPreferences("recommandfragment_local_language", 0).edit().putString("recommandfragment_local_language", str).commit();
    }

    public static boolean getWeatherSystemLanguageChanged(Context context, boolean z) {
        return context.getSharedPreferences("system_language_changed", 0).getBoolean("system_language_changed", z);
    }

    public static void setWeatherSystemLanguageChanged(Context context, boolean z) {
        context.getSharedPreferences("system_language_changed", 0).edit().putBoolean("system_language_changed", z).commit();
    }

    public static void setWeatherUpdateApk(Context context, boolean z) {
        context.getSharedPreferences("weather_update", 0).edit().putBoolean("weather_update", z).commit();
    }

    public static boolean getWeatherUpdateApk(Context context, boolean z) {
        return context.getSharedPreferences("weather_update", 0).getBoolean("weather_update", z);
    }

    public static void setWeatherUpdateSu(Context context, String str) {
        context.getSharedPreferences("weather_update_su", 0).edit().putString("weather_update_su", str).commit();
    }

    public static String getWeatherUpdateSu(Context context, String str) {
        return context.getSharedPreferences("weather_update_su", 0).getString("weather_update_su", str);
    }

    public static void setPermissionState(Context context, boolean z) {
        context.getSharedPreferences("permission_state_ask", 0).edit().putBoolean("permission_state_ask", z).commit();
    }

    public static boolean getPermissionState(Context context, boolean z) {
        return context.getSharedPreferences("permission_state_ask", 0).getBoolean("permission_state_ask", z);
    }

    public static long getDeleShowTime(Context context, long j) {
        return context.getSharedPreferences("delete_fragment_resume_time", 0).getLong("delete_fragment_resume_time", j);
    }

    public static void setDeleShowTime(Context context, long j) {
        context.getSharedPreferences("delete_fragment_resume_time", 0).edit().putLong("delete_fragment_resume_time", j).commit();
    }

    public static long getPermissionTostTime(Context context, long j) {
        return context.getSharedPreferences("permission_toast", 0).getLong("permission_toast_time", j);
    }

    public static void setPermissionToastTime(Context context, long j) {
        context.getSharedPreferences("permission_toast", 0).edit().putLong("permission_toast_time", j).commit();
    }

    public static String getPermissionTostPackageName(Context context, String str) {
        return context.getSharedPreferences("permission_toast", 0).getString("permission_toast_package_name", str);
    }

    public static void setPermissionTostPackageName(Context context, String str) {
        context.getSharedPreferences("permission_toast", 0).edit().putString("permission_toast_package_name", str).commit();
    }

    public static int getPermissionTostsuAccess(Context context, int i) {
        return context.getSharedPreferences("permission_toast", 0).getInt("permission_toast_su_access", i);
    }

    public static void setPermissionTostsuAccess(Context context, int i) {
        context.getSharedPreferences("permission_toast", 0).edit().putInt("permission_toast_su_access", i).commit();
    }

    public static void setWeatherShowGcAd(Context context, boolean z) {
        context.getSharedPreferences("custom_gc_ad", 0).edit().putBoolean("weather_show_ad", z).commit();
    }

    public static boolean getWeatherShowGcAd(Context context, boolean z) {
        return context.getSharedPreferences("custom_gc_ad", 5).getBoolean("weather_show_ad", z);
    }

    public static void setShowAdCount(Context context, int i) {
        context.getSharedPreferences("custom_gc_ad", 1).edit().putInt("show_ad_count", i).commit();
    }

    public static int getShowAdCount(Context context, int i) {
        return context.getSharedPreferences("custom_gc_ad", 5).getInt("show_ad_count", i);
    }

    public static void setWheaterReportInstallChannel(Context context, boolean z) {
        context.getSharedPreferences("weather_report_install_channel", 0).edit().putBoolean("weather_report_install_channel", z).commit();
    }

    public static boolean getWheaterReportInstallChannel(Context context, boolean z) {
        return context.getSharedPreferences("weather_report_install_channel", 0).getBoolean("weather_report_install_channel", z);
    }

    public static void setWheaterRegistPush(Context context, boolean z) {
        context.getSharedPreferences("weather_regist_push", 0).edit().putBoolean("weather_regist_push", z).commit();
    }

    public static boolean getWheaterRegistPush(Context context, boolean z) {
        return context.getSharedPreferences("weather_regist_push", 0).getBoolean("weather_regist_push", z);
    }

    public static void setLongValue(Context context, String str, String str2, long j) {
        context.getSharedPreferences(str, 0).edit().putLong(str2, j).commit();
    }

    public static long getLongValue(Context context, String str, String str2, long j) {
        return context.getSharedPreferences(str, 0).getLong(str2, j);
    }
}
