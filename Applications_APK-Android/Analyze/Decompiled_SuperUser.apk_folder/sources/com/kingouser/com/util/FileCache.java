package com.kingouser.com.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCache {
    private File cacheDir;

    public FileCache(Context context) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            this.cacheDir = new File(Environment.getExternalStorageDirectory(), "image_cache");
        } else {
            this.cacheDir = context.getCacheDir();
        }
        if (!this.cacheDir.exists()) {
            this.cacheDir.mkdirs();
        }
    }

    public File getCacheDirFile() {
        return this.cacheDir;
    }

    public File getFile(String str) {
        File file = new File(this.cacheDir, str);
        if (file.exists()) {
            return file;
        }
        return null;
    }

    public void put(String str, Bitmap bitmap) {
        File file = new File(this.cacheDir, str);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public void clear() {
        for (File delete : this.cacheDir.listFiles()) {
            delete.delete();
        }
    }
}
