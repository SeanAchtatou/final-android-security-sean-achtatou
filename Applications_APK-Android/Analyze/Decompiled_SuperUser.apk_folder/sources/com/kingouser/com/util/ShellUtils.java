package com.kingouser.com.util;

import android.content.Context;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.List;

public class ShellUtils {
    public static final String COMMAND_EXIT = "exit\n";
    public static final String COMMAND_LINE_END = "\n";
    public static final String COMMAND_SH = "sh";
    public static final String COMMAND_SU = "su";
    private static final String TAG = "ShellUtils";

    private ShellUtils() {
        throw new AssertionError();
    }

    public static boolean checkRootPermission() {
        return execCommand("echo root", true, false).result == 0;
    }

    public static synchronized CommandResult execCommand(String str, boolean z) {
        CommandResult execCommand;
        synchronized (ShellUtils.class) {
            execCommand = execCommand(new String[]{str}, z, true, 0);
        }
        return execCommand;
    }

    public static synchronized CommandResult execCommand(String str, boolean z, int i) {
        CommandResult execCommand;
        synchronized (ShellUtils.class) {
            execCommand = execCommand(new String[]{str}, z, true, i);
        }
        return execCommand;
    }

    public static CommandResult execCommand(List<String> list, boolean z) {
        return execCommand(list == null ? null : (String[]) list.toArray(new String[0]), z, true, 0);
    }

    public static CommandResult execCommand(String[] strArr, boolean z) {
        return execCommand(strArr, z, true, 0);
    }

    public static CommandResult execCommand(String str, boolean z, boolean z2) {
        return execCommand(new String[]{str}, z, z2, 0);
    }

    public static CommandResult execCommand(List<String> list, boolean z, boolean z2) {
        return execCommand(list == null ? null : (String[]) list.toArray(new String[0]), z, z2, 0);
    }

    public static synchronized boolean canRunRootCommands() {
        boolean z;
        boolean z2 = true;
        synchronized (ShellUtils.class) {
            try {
                Process exec = Runtime.getRuntime().exec(COMMAND_SU);
                DataOutputStream dataOutputStream = new DataOutputStream(exec.getOutputStream());
                DataInputStream dataInputStream = new DataInputStream(exec.getInputStream());
                if (dataOutputStream == null || dataInputStream == null) {
                    z2 = false;
                } else {
                    dataOutputStream.writeBytes("id\n");
                    dataOutputStream.flush();
                    String readLine = dataInputStream.readLine();
                    if (readLine == null) {
                        MyLog.d(TAG, "Can't get root access or denied by user");
                        z = false;
                        z2 = false;
                    } else if (true == readLine.contains("uid=0")) {
                        MyLog.d(TAG, "Root access granted");
                        z = true;
                    } else {
                        MyLog.d(TAG, "Root access rejected: " + readLine);
                        z = true;
                        z2 = false;
                    }
                    if (z) {
                        dataOutputStream.writeBytes(COMMAND_EXIT);
                        dataOutputStream.flush();
                    }
                }
                dataOutputStream.close();
            } catch (Exception e2) {
                MyLog.d(TAG, "Root access rejected [" + e2.getClass().getName() + "] : " + e2.getMessage());
                z2 = false;
            }
            MyLog.d(TAG, "retval:" + z2);
        }
        return z2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static boolean checkRoot(Context context) {
        boolean z = true;
        synchronized (ShellUtils.class) {
            execCommand("echo root > /data/kingoRoot", true, false);
            execCommand("sync", true, false);
            CommandResult execCommand = execCommand("cat /data/kingoRoot", true, true);
            if (execCommand.successMsg == null || !"root".equals(execCommand.successMsg.trim())) {
                z = false;
            } else {
                execCommand("rm -r /data/kingoRoot", true, true);
                execCommand("sync", true, false);
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0148, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x014d, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        r10.destroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x017e, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x017f, code lost:
        r3.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0197, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x01c7, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x01c8, code lost:
        r3 = r4;
        r6 = r8;
        r4 = null;
        r8 = r10;
        r5 = r7;
        r7 = r9;
        r9 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x01d1, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x01d2, code lost:
        r5 = r7;
        r6 = r8;
        r7 = r9;
        r8 = r10;
        r9 = r11;
        r16 = r3;
        r3 = r4;
        r4 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0065, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0066, code lost:
        r3 = r4;
        r4 = null;
        r5 = null;
        r6 = r8;
        r8 = r10;
        r16 = r9;
        r9 = -1;
        r7 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0148 A[Catch:{ IOException -> 0x017e }] */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x014d A[Catch:{ IOException -> 0x017e }] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0152 A[SYNTHETIC, Splitter:B:105:0x0152] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0183 A[Catch:{ IOException -> 0x012f }] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x018a A[Catch:{ IOException -> 0x012f }] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0197 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:20:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x01eb  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0075 A[SYNTHETIC, Splitter:B:34:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x007a A[Catch:{ IOException -> 0x0178 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x007f A[Catch:{ IOException -> 0x0178 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0084 A[SYNTHETIC, Splitter:B:41:0x0084] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008e A[Catch:{ IOException -> 0x012f }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0092 A[Catch:{ IOException -> 0x012f }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0143 A[SYNTHETIC, Splitter:B:98:0x0143] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized com.kingouser.com.util.ShellUtils.CommandResult execCommand(java.lang.String[] r18, boolean r19, boolean r20, int r21) {
        /*
            java.lang.Class<com.kingouser.com.util.ShellUtils> r12 = com.kingouser.com.util.ShellUtils.class
            monitor-enter(r12)
            r7 = -1
            if (r18 == 0) goto L_0x000b
            r0 = r18
            int r2 = r0.length     // Catch:{ all -> 0x0098 }
            if (r2 != 0) goto L_0x0014
        L_0x000b:
            com.kingouser.com.util.ShellUtils$CommandResult r2 = new com.kingouser.com.util.ShellUtils$CommandResult     // Catch:{ all -> 0x0098 }
            r3 = 0
            r4 = 0
            r2.<init>(r7, r3, r4)     // Catch:{ all -> 0x0098 }
        L_0x0012:
            monitor-exit(r12)
            return r2
        L_0x0014:
            r4 = 0
            r9 = 0
            r8 = 0
            r6 = 0
            r5 = 0
            r3 = 0
            java.lang.Runtime r10 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x01a2, all -> 0x0190 }
            if (r19 == 0) goto L_0x0052
            java.lang.String r2 = "su"
        L_0x0022:
            java.lang.Process r10 = r10.exec(r2)     // Catch:{ Exception -> 0x01a2, all -> 0x0190 }
            java.io.DataOutputStream r4 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x01b1, all -> 0x0194 }
            java.io.OutputStream r2 = r10.getOutputStream()     // Catch:{ Exception -> 0x01b1, all -> 0x0194 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x0194 }
            r0 = r18
            int r3 = r0.length     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            r2 = 0
        L_0x0033:
            if (r2 >= r3) goto L_0x009b
            r11 = r18[r2]     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            r13.<init>()     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            java.lang.String r14 = "fkkkkkkcmd:"
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            java.lang.StringBuilder r13 = r13.append(r11)     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            com.pureapps.cleaner.util.f.a(r13)     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            if (r11 != 0) goto L_0x0055
        L_0x004f:
            int r2 = r2 + 1
            goto L_0x0033
        L_0x0052:
            java.lang.String r2 = "sh"
            goto L_0x0022
        L_0x0055:
            byte[] r11 = r11.getBytes()     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            r4.write(r11)     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            java.lang.String r11 = "\n"
            r4.writeBytes(r11)     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            r4.flush()     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            goto L_0x004f
        L_0x0065:
            r2 = move-exception
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r8
            r8 = r10
            r16 = r9
            r9 = r7
            r7 = r16
        L_0x0070:
            r2.printStackTrace()     // Catch:{ all -> 0x019c }
            if (r3 == 0) goto L_0x0078
            r3.close()     // Catch:{ IOException -> 0x0178 }
        L_0x0078:
            if (r7 == 0) goto L_0x007d
            r7.close()     // Catch:{ IOException -> 0x0178 }
        L_0x007d:
            if (r6 == 0) goto L_0x0082
            r6.close()     // Catch:{ IOException -> 0x0178 }
        L_0x0082:
            if (r8 == 0) goto L_0x01eb
            r8.destroy()     // Catch:{ all -> 0x0098 }
            r3 = r5
            r11 = r9
            r5 = r4
        L_0x008a:
            com.kingouser.com.util.ShellUtils$CommandResult r2 = new com.kingouser.com.util.ShellUtils$CommandResult     // Catch:{ all -> 0x0098 }
            if (r3 != 0) goto L_0x0183
            r3 = 0
            r4 = r3
        L_0x0090:
            if (r5 != 0) goto L_0x018a
            r3 = 0
        L_0x0093:
            r2.<init>(r11, r4, r3)     // Catch:{ all -> 0x0098 }
            goto L_0x0012
        L_0x0098:
            r2 = move-exception
            monitor-exit(r12)
            throw r2
        L_0x009b:
            java.lang.String r2 = "exit\n"
            r4.writeBytes(r2)     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            r4.flush()     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            if (r21 <= 0) goto L_0x00ba
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            r0 = r21
            int r11 = r0 * 1000
            long r14 = (long) r11     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            long r2 = r2 + r14
        L_0x00af:
            r14 = 300(0x12c, double:1.48E-321)
            java.lang.Thread.sleep(r14)     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            boolean r11 = isProcessAlive(r10)     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            if (r11 != 0) goto L_0x0100
        L_0x00ba:
            int r11 = r10.waitFor()     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            if (r20 == 0) goto L_0x01f7
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bd, all -> 0x0197 }
            r7.<init>()     // Catch:{ Exception -> 0x01bd, all -> 0x0197 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c7, all -> 0x0197 }
            r3.<init>()     // Catch:{ Exception -> 0x01c7, all -> 0x0197 }
            java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ Exception -> 0x01d1, all -> 0x0197 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x01d1, all -> 0x0197 }
            java.io.InputStream r5 = r10.getInputStream()     // Catch:{ Exception -> 0x01d1, all -> 0x0197 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x01d1, all -> 0x0197 }
            r6.<init>(r2)     // Catch:{ Exception -> 0x01d1, all -> 0x0197 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x01de, all -> 0x0199 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x01de, all -> 0x0199 }
            java.io.InputStream r9 = r10.getErrorStream()     // Catch:{ Exception -> 0x01de, all -> 0x0199 }
            r2.<init>(r9)     // Catch:{ Exception -> 0x01de, all -> 0x0199 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x01de, all -> 0x0199 }
        L_0x00e6:
            java.lang.String r2 = r6.readLine()     // Catch:{ Exception -> 0x00f0, all -> 0x013e }
            if (r2 == 0) goto L_0x0134
            r7.append(r2)     // Catch:{ Exception -> 0x00f0, all -> 0x013e }
            goto L_0x00e6
        L_0x00f0:
            r2 = move-exception
            r8 = r10
            r9 = r11
            r16 = r7
            r7 = r6
            r6 = r5
            r5 = r16
            r17 = r4
            r4 = r3
            r3 = r17
            goto L_0x0070
        L_0x0100:
            long r14 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            int r11 = (r14 > r2 ? 1 : (r14 == r2 ? 0 : -1))
            if (r11 <= 0) goto L_0x00af
            java.lang.String r2 = "ShellUtils"
            java.lang.String r3 = "Process doesn't seem to stop on it's own, assuming it's hanging"
            com.kingouser.com.util.MyLog.e(r2, r3)     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            com.kingouser.com.util.ShellUtils$CommandResult r2 = new com.kingouser.com.util.ShellUtils$CommandResult     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            r3 = -1
            java.lang.String r11 = ""
            java.lang.String r13 = "cmd timeout"
            r2.<init>(r3, r11, r13)     // Catch:{ Exception -> 0x0065, all -> 0x0197 }
            if (r4 == 0) goto L_0x011e
            r4.close()     // Catch:{ IOException -> 0x012f }
        L_0x011e:
            if (r9 == 0) goto L_0x0123
            r9.close()     // Catch:{ IOException -> 0x012f }
        L_0x0123:
            if (r8 == 0) goto L_0x0128
            r8.close()     // Catch:{ IOException -> 0x012f }
        L_0x0128:
            if (r10 == 0) goto L_0x0012
            r10.destroy()     // Catch:{ all -> 0x0098 }
            goto L_0x0012
        L_0x012f:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x0098 }
            goto L_0x0128
        L_0x0134:
            java.lang.String r2 = r5.readLine()     // Catch:{ Exception -> 0x00f0, all -> 0x013e }
            if (r2 == 0) goto L_0x0156
            r3.append(r2)     // Catch:{ Exception -> 0x00f0, all -> 0x013e }
            goto L_0x0134
        L_0x013e:
            r2 = move-exception
            r8 = r5
            r9 = r6
        L_0x0141:
            if (r4 == 0) goto L_0x0146
            r4.close()     // Catch:{ IOException -> 0x017e }
        L_0x0146:
            if (r9 == 0) goto L_0x014b
            r9.close()     // Catch:{ IOException -> 0x017e }
        L_0x014b:
            if (r8 == 0) goto L_0x0150
            r8.close()     // Catch:{ IOException -> 0x017e }
        L_0x0150:
            if (r10 == 0) goto L_0x0155
            r10.destroy()     // Catch:{ all -> 0x0098 }
        L_0x0155:
            throw r2     // Catch:{ all -> 0x0098 }
        L_0x0156:
            r8 = r5
            r5 = r7
        L_0x0158:
            if (r4 == 0) goto L_0x015d
            r4.close()     // Catch:{ IOException -> 0x0173 }
        L_0x015d:
            if (r6 == 0) goto L_0x0162
            r6.close()     // Catch:{ IOException -> 0x0173 }
        L_0x0162:
            if (r8 == 0) goto L_0x0167
            r8.close()     // Catch:{ IOException -> 0x0173 }
        L_0x0167:
            if (r10 == 0) goto L_0x01f0
            r10.destroy()     // Catch:{ all -> 0x0098 }
            r16 = r3
            r3 = r5
            r5 = r16
            goto L_0x008a
        L_0x0173:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x0098 }
            goto L_0x0167
        L_0x0178:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x0098 }
            goto L_0x0082
        L_0x017e:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x0098 }
            goto L_0x0150
        L_0x0183:
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0098 }
            r4 = r3
            goto L_0x0090
        L_0x018a:
            java.lang.String r3 = r5.toString()     // Catch:{ all -> 0x0098 }
            goto L_0x0093
        L_0x0190:
            r2 = move-exception
            r10 = r4
            r4 = r3
            goto L_0x0141
        L_0x0194:
            r2 = move-exception
            r4 = r3
            goto L_0x0141
        L_0x0197:
            r2 = move-exception
            goto L_0x0141
        L_0x0199:
            r2 = move-exception
            r9 = r6
            goto L_0x0141
        L_0x019c:
            r2 = move-exception
            r4 = r3
            r9 = r7
            r10 = r8
            r8 = r6
            goto L_0x0141
        L_0x01a2:
            r2 = move-exception
            r16 = r5
            r5 = r6
            r6 = r8
            r8 = r4
            r4 = r16
            r17 = r9
            r9 = r7
            r7 = r17
            goto L_0x0070
        L_0x01b1:
            r2 = move-exception
            r4 = r5
            r5 = r6
            r6 = r8
            r8 = r10
            r16 = r9
            r9 = r7
            r7 = r16
            goto L_0x0070
        L_0x01bd:
            r2 = move-exception
            r3 = r4
            r7 = r9
            r4 = r5
            r9 = r11
            r5 = r6
            r6 = r8
            r8 = r10
            goto L_0x0070
        L_0x01c7:
            r2 = move-exception
            r3 = r4
            r6 = r8
            r4 = r5
            r8 = r10
            r5 = r7
            r7 = r9
            r9 = r11
            goto L_0x0070
        L_0x01d1:
            r2 = move-exception
            r5 = r7
            r6 = r8
            r7 = r9
            r8 = r10
            r9 = r11
            r16 = r3
            r3 = r4
            r4 = r16
            goto L_0x0070
        L_0x01de:
            r2 = move-exception
            r5 = r7
            r9 = r11
            r7 = r6
            r6 = r8
            r8 = r10
            r16 = r4
            r4 = r3
            r3 = r16
            goto L_0x0070
        L_0x01eb:
            r3 = r5
            r11 = r9
            r5 = r4
            goto L_0x008a
        L_0x01f0:
            r16 = r3
            r3 = r5
            r5 = r16
            goto L_0x008a
        L_0x01f7:
            r3 = r5
            r5 = r6
            r6 = r9
            goto L_0x0158
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult");
    }

    public static boolean isProcessAlive(Process process) {
        try {
            process.exitValue();
            return false;
        } catch (IllegalThreadStateException e2) {
            return true;
        }
    }

    public static void sleep(long j) {
        try {
            Thread.sleep(j);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0035, code lost:
        if (r3.successMsg.trim().startsWith("kingo") != false) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean checkSuVerison() {
        /*
            r0 = 1
            r1 = 0
            java.lang.Class<com.kingouser.com.util.ShellUtils> r2 = com.kingouser.com.util.ShellUtils.class
            monitor-enter(r2)
            java.lang.String r3 = "su -v"
            r4 = 1
            r5 = 1
            com.kingouser.com.util.ShellUtils$CommandResult r3 = execCommand(r3, r4, r5)     // Catch:{ all -> 0x0039 }
            java.lang.String r4 = r3.successMsg     // Catch:{ all -> 0x0039 }
            if (r4 == 0) goto L_0x003c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0039 }
            r4.<init>()     // Catch:{ all -> 0x0039 }
            java.lang.String r5 = "hasRoot:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0039 }
            java.lang.String r5 = r3.successMsg     // Catch:{ all -> 0x0039 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0039 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0039 }
            com.pureapps.cleaner.util.f.a(r4)     // Catch:{ all -> 0x0039 }
            java.lang.String r3 = r3.successMsg     // Catch:{ all -> 0x0039 }
            java.lang.String r3 = r3.trim()     // Catch:{ all -> 0x0039 }
            java.lang.String r4 = "kingo"
            boolean r3 = r3.startsWith(r4)     // Catch:{ all -> 0x0039 }
            if (r3 == 0) goto L_0x003c
        L_0x0037:
            monitor-exit(r2)     // Catch:{ all -> 0x0039 }
            return r0
        L_0x0039:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0039 }
            throw r0
        L_0x003c:
            r0 = r1
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.ShellUtils.checkSuVerison():boolean");
    }

    public static class CommandResult {
        public String errorMsg;
        public int result;
        public String successMsg = "";

        public CommandResult(int i) {
            this.result = i;
        }

        public CommandResult(int i, String str, String str2) {
            this.result = i;
            this.successMsg = str;
            this.errorMsg = str2;
        }

        public String toString() {
            return "CommandResult{result=" + this.result + ", successMsg='" + this.successMsg + '\'' + ", errorMsg='" + this.errorMsg + '\'' + '}';
        }
    }
}
