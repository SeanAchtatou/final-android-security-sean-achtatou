package com.kingouser.com.util;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public class Stub extends Binder implements NvRAMAgent {

    class Proxy implements NvRAMAgent {
        private IBinder mRemote;

        Proxy(IBinder iBinder) {
            this.mRemote = iBinder;
        }

        public IBinder asBinder() {
            return this.mRemote;
        }

        public byte[] readFile(int i) {
            byte[] bArr = null;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("NvRAMAgent");
                obtain.writeInt(i);
                this.mRemote.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                bArr = obtain2.createByteArray();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            obtain2.recycle();
            obtain.recycle();
            return bArr;
        }

        public byte[] readFileByName(String str) {
            byte[] bArr = null;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("NvRAMAgent");
                obtain.writeString(str);
                this.mRemote.transact(3, obtain, obtain2, 0);
                obtain2.readException();
                bArr = obtain2.createByteArray();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            obtain2.recycle();
            obtain.recycle();
            return bArr;
        }

        public int writeFile(int i, byte[] bArr) {
            int i2 = 0;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("NvRAMAgent");
                obtain.writeInt(i);
                obtain.writeByteArray(bArr);
                this.mRemote.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                i2 = obtain2.readInt();
            } catch (Throwable th) {
            }
            obtain2.recycle();
            obtain.recycle();
            return i2;
        }

        public int writeFileByName(String str, byte[] bArr) {
            int i = 0;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("NvRAMAgent");
                obtain.writeString(str);
                obtain.writeByteArray(bArr);
                this.mRemote.transact(4, obtain, obtain2, 0);
                obtain2.readException();
                i = obtain2.readInt();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            obtain2.recycle();
            obtain.recycle();
            return i;
        }
    }

    public static NvRAMAgent asInterface(IBinder iBinder) {
        Proxy proxy = null;
        if (iBinder != null) {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("NvRAMAgent");
            if (queryLocalInterface != null && (queryLocalInterface instanceof NvRAMAgent)) {
                return (NvRAMAgent) queryLocalInterface;
            }
            Stub stub = new Stub();
            stub.getClass();
            proxy = new Proxy(iBinder);
        }
        return proxy;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("NvRAMAgent");
                byte[] readFile = readFile(parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeByteArray(readFile);
                return true;
            case 2:
                parcel.enforceInterface("NvRAMAgent");
                int writeFile = writeFile(parcel.readInt(), parcel.createByteArray());
                parcel2.writeNoException();
                parcel2.writeInt(writeFile);
                return true;
            case 1598968902:
                parcel2.writeString("NvRAMAgent");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }

    public IBinder asBinder() {
        return null;
    }

    public byte[] readFile(int i) {
        return null;
    }

    public byte[] readFileByName(String str) {
        return null;
    }

    public int writeFile(int i, byte[] bArr) {
        return 0;
    }

    public int writeFileByName(String str, byte[] bArr) {
        return 0;
    }
}
