package com.kingouser.com.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SPUtil {
    private static final String CONFIG = "KIDDIE_COONFIG";
    private static SharedPreferences.Editor editor;
    private static SharedPreferences sp;
    private static SPUtil spUtil;

    public static SPUtil getInstant(Context context) {
        if (spUtil == null) {
            spUtil = new SPUtil();
        }
        if (sp == null) {
            sp = context.getSharedPreferences(CONFIG, 0);
        }
        if (editor == null) {
            editor = sp.edit();
        }
        return spUtil;
    }

    public SPUtil save(String str, Object obj) {
        if (obj instanceof String) {
            editor.putString(str, obj.toString());
        }
        if (obj instanceof Boolean) {
            editor.putBoolean(str, Boolean.valueOf(obj.toString()).booleanValue());
        }
        if (obj instanceof Long) {
            editor.putLong(str, Long.valueOf(obj.toString()).longValue());
        }
        if (obj instanceof Float) {
            editor.putFloat(str, Float.valueOf(obj.toString()).floatValue());
        }
        if (obj instanceof Integer) {
            editor.putInt(str, Integer.valueOf(obj.toString()).intValue());
        }
        editor.commit();
        return spUtil;
    }

    public Object get(String str, Object obj) {
        if (obj instanceof String) {
            return sp.getString(str, obj.toString());
        }
        if (obj instanceof Boolean) {
            return Boolean.valueOf(sp.getBoolean(str, Boolean.parseBoolean(obj.toString())));
        }
        if (obj instanceof Long) {
            return Long.valueOf(sp.getLong(str, Long.valueOf(obj.toString()).longValue()));
        }
        if (obj instanceof Float) {
            return Float.valueOf(sp.getFloat(str, -1.0f));
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(sp.getInt(str, Integer.parseInt(obj.toString())));
        }
        return null;
    }
}
