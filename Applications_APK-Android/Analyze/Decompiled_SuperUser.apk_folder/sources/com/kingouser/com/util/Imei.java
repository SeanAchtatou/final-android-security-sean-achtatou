package com.kingouser.com.util;

import android.content.Context;
import android.os.Build;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class Imei {
    public List<String> getIMEI(Context context) {
        HashSet hashSet = new HashSet();
        String str = Build.MODEL;
        Build.BRAND.toLowerCase().trim();
        ArrayList arrayList = new ArrayList();
        try {
            String iMEI_Stand = getIMEI_Stand(context);
            if (iMEI_Stand != null && iMEI_Stand.indexOf("0000000000") > -1) {
                iMEI_Stand = "";
            }
            if (!(iMEI_Stand == null || iMEI_Stand == "")) {
                hashSet.add(iMEI_Stand);
            }
            try {
                getIMEI_MtkDoubleSim(context, hashSet);
            } catch (Exception e2) {
            }
            try {
                getIMEI_MtkSecondDoubleSim(context, hashSet);
            } catch (Exception e3) {
            }
            try {
                getIMEI_SpreadDoubleSim(context, hashSet);
            } catch (Exception e4) {
            }
            try {
                getIMEI_QualcommDoubleSim(context, hashSet);
            } catch (Exception e5) {
            }
            try {
                getIMEI2_SMG3812(context, hashSet);
            } catch (Exception e6) {
            }
            try {
                if (!str.contains("WP-S") && !str.contains("D5012T") && !str.contains("K-Touch K3")) {
                    getIMEI2_LenovoA3580(context, hashSet);
                }
                try {
                    getIMEI2_SM7000(context, hashSet);
                } catch (Exception e7) {
                }
                try {
                    getMeid_Vivod(context, hashSet);
                } catch (Exception e8) {
                }
                try {
                    getIMEI_Huawei(context, hashSet);
                } catch (Exception e9) {
                }
                try {
                    getMeid_Huawei(context, hashSet);
                } catch (Exception e10) {
                }
                try {
                    getMeid_HuaweiTAGAL00(context, hashSet);
                } catch (Exception e11) {
                }
                try {
                    getMeid_VivoX5p(context, hashSet);
                } catch (Exception e12) {
                }
                try {
                    getMEIDMi4c(context, hashSet);
                } catch (Exception e13) {
                }
                try {
                    getMEIDMEIKIWAL10(context, hashSet);
                } catch (Exception e14) {
                }
                try {
                    getTITAL00Imei2(context, hashSet, true);
                } catch (Exception e15) {
                }
                try {
                    getTITAL00Imei2(context, hashSet, false);
                } catch (Exception e16) {
                }
                if (hashSet.size() > 0) {
                    Iterator it = hashSet.iterator();
                    while (it.hasNext()) {
                        String str2 = (String) it.next();
                        if (!TextUtils.isEmpty(str2)) {
                            arrayList.add(str2);
                        }
                    }
                    Collections.sort(arrayList, Collections.reverseOrder());
                }
            } catch (Exception e17) {
            }
        } catch (Exception e18) {
        }
        return arrayList;
    }

    private void getTITAL00Imei2(Context context, HashSet<String> hashSet, boolean z) {
        Method method;
        try {
            Class<?> cls = Class.forName("android.os.ServiceManager");
            if (cls != null && (method = cls.getMethod("getService", String.class)) != null) {
                method.invoke(null, "NvRAMAgent").getClass().getName();
                NvRAMAgent asInterface = Stub.asInterface((IBinder) method.invoke(null, "NvRAMAgent"));
                byte[] bArr = new byte[8];
                byte[] bArr2 = new byte[15];
                if (asInterface != null) {
                    byte[] readFileByName = asInterface.readFileByName("/data/nvram/APCFG/APRDEB/PRODUCT_INFO");
                    if (z) {
                        for (int i = 64; i < 72; i++) {
                            bArr[i - 64] = readFileByName[i];
                        }
                    } else {
                        for (int i2 = 74; i2 < 82; i2++) {
                            bArr[i2 - 74] = readFileByName[i2];
                        }
                    }
                    for (int i3 = 0; i3 < 7; i3++) {
                        bArr2[i3 * 2] = (byte) ((bArr[i3] & 15) + 48);
                        bArr2[(i3 * 2) + 1] = (byte) (((bArr[i3] & 240) >> 4) + 48);
                    }
                    bArr2[14] = (byte) ((bArr[7] & 15) + 48);
                    String str = new String(bArr2);
                    if (isIMEI(str)) {
                        hashSet.add(str);
                        return;
                    }
                    return;
                }
                Log.d("wyy", "setFlag readFile is null ");
            }
        } catch (Exception e2) {
        }
    }

    private void getMEIDMEIKIWAL10(Context context, HashSet<String> hashSet) {
        try {
            Class<?> cls = Class.forName("android.telephony.HwTelephonyManager");
            Method method = cls.getMethod("getDefault", null);
            if (method != null) {
                String str = (String) cls.getMethod("getMeid", null).invoke(method.invoke(null, null), null);
                if (isIMEI(str)) {
                    hashSet.add(str);
                }
            }
        } catch (Exception e2) {
        }
        try {
            Class<?> cls2 = Class.forName("android.telephony.TelephonyManager");
            Method method2 = cls2.getMethod("getDefault", null);
            if (method2 != null) {
                String str2 = (String) cls2.getMethod("getImei", null).invoke(method2.invoke(null, null), null);
                Log.d("wyy", "imei:" + str2);
                if (isIMEI(str2)) {
                    hashSet.add(str2);
                }
            }
        } catch (Exception e3) {
        }
    }

    private void getMEIDMi4c(Context context, HashSet<String> hashSet) {
        try {
            Class<?> cls = Class.forName("miui.telephony.TelephonyManager");
            Method method = cls.getMethod("getDefault", null);
            if (method != null) {
                method.setAccessible(true);
                Method declaredMethod = cls.getDeclaredMethod("getMeidForSlot", Integer.TYPE);
                if (declaredMethod != null) {
                    declaredMethod.setAccessible(true);
                    for (int i = 0; i < 2; i++) {
                        String str = (String) declaredMethod.invoke(method.invoke(null, null), Integer.valueOf(i));
                        if (isIMEI(str)) {
                            hashSet.add(str);
                        }
                    }
                }
            }
        } catch (Exception e2) {
        }
    }

    private void getMeid_VivoX5p(Context context, HashSet<String> hashSet) {
        try {
            Class<?> cls = Class.forName("com.android.internal.telephony.ITelephony$Stub");
            cls.getMethods();
            Method declaredMethod = cls.getDeclaredMethod("asInterface", Class.forName("android.os.IBinder"));
            declaredMethod.setAccessible(true);
            Method declaredMethod2 = Class.forName("android.os.ServiceManager").getDeclaredMethod("getService", String.class);
            Class<?> cls2 = declaredMethod.invoke(null, declaredMethod2.invoke(null, "phone")).getClass();
            Class<?> cls3 = Class.forName("com.android.internal.telephony.VivoTelephonyApiParams");
            Constructor<?> declaredConstructor = cls3.getDeclaredConstructor(String.class);
            declaredConstructor.setAccessible(true);
            Method declaredMethod3 = cls2.getDeclaredMethod("vivoTelephonyApi", cls3);
            Object invoke = declaredMethod3.invoke(declaredMethod.invoke(null, declaredMethod2.invoke(null, "phone")), declaredConstructor.newInstance("API_TAG_getMeid"));
            Method declaredMethod4 = invoke.getClass().getDeclaredMethod("getAsString", String.class);
            declaredMethod4.setAccessible(true);
            String str = (String) declaredMethod4.invoke(invoke, "meid");
            if (isIMEI(str)) {
                hashSet.add(str);
            }
            Object invoke2 = declaredMethod3.invoke(declaredMethod.invoke(null, declaredMethod2.invoke(null, "phone")), declaredConstructor.newInstance("API_TAG_getImei"));
            Method declaredMethod5 = invoke2.getClass().getDeclaredMethod("getAsString", String.class);
            declaredMethod5.setAccessible(true);
            String str2 = (String) declaredMethod5.invoke(invoke2, "imei");
            Log.e("wyy", "imei:" + str2);
            if (isIMEI(str2)) {
                hashSet.add(str2);
            }
        } catch (Exception e2) {
        }
    }

    private void getMeid_HuaweiTAGAL00(Context context, HashSet<String> hashSet) {
        int i = 0;
        try {
            Class<?> cls = Class.forName("android.os.Build");
            Method[] declaredMethods = cls.getDeclaredMethods();
            while (true) {
                int i2 = i;
                if (i2 < declaredMethods.length) {
                    Method method = declaredMethods[i2];
                    if (method.getName().equals("getString")) {
                        method.setAccessible(true);
                        String str = (String) method.invoke(cls, "cdma.meid");
                        String str2 = (String) method.invoke(cls, "gsm.imei1");
                        String str3 = (String) method.invoke(cls, "gsm.imei2");
                        if (isIMEI(str)) {
                            hashSet.add(str);
                        }
                        if (isIMEI(str3)) {
                            hashSet.add(str3);
                        }
                        if (isIMEI(str2)) {
                            hashSet.add(str2);
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void getIMEI2_SMG3812(Context context, HashSet<String> hashSet) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            Method method = Class.forName("android.telephony.TelephonyManager").getMethod("getDeviceIdDs", Integer.TYPE);
            if (method != null) {
                String str = (String) method.invoke(telephonyManager, 0);
                String str2 = (String) method.invoke(telephonyManager, 1);
                if (isIMEI(str)) {
                    hashSet.add(str);
                }
                if (isIMEI(str2)) {
                    hashSet.add(str2);
                }
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public String getIMEI_Stand(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return null;
        }
        if (CheckPermission(context, "android.permission.READ_PHONE_STATE")) {
            return telephonyManager.getDeviceId();
        }
        return null;
    }

    public static boolean CheckPermission(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    /* access modifiers changed from: package-private */
    public void getIMEI_MtkDoubleSim(Context context, HashSet<String> hashSet) {
        int i;
        int i2;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        Class<?> cls = Class.forName("com.android.internal.telephony.Phone");
        try {
            Field field = cls.getField("GEMINI_SIM_1");
            field.setAccessible(true);
            int parseInt = Integer.parseInt(field.get(null).toString());
            Field field2 = cls.getField("GEMINI_SIM_2");
            field2.setAccessible(true);
            int parseInt2 = Integer.parseInt(field2.get(null).toString());
            i2 = parseInt;
            i = parseInt2;
        } catch (Exception e2) {
            i = 1;
            i2 = 0;
        }
        Method declaredMethod = TelephonyManager.class.getDeclaredMethod("getDeviceIdGemini", Integer.TYPE);
        String trim = ((String) declaredMethod.invoke(telephonyManager, Integer.valueOf(i2))).trim();
        String trim2 = ((String) declaredMethod.invoke(telephonyManager, Integer.valueOf(i))).trim();
        if (!hashSet.contains(trim) && trim != null && trim.indexOf("0000000000") == -1) {
            hashSet.add(trim);
        }
        if (!hashSet.contains(trim2) && trim2 != null && trim2.indexOf("0000000000") == -1) {
            hashSet.add(trim2);
        }
    }

    /* access modifiers changed from: package-private */
    public void getIMEI_MtkSecondDoubleSim(Context context, HashSet<String> hashSet) {
        int i;
        int i2;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        Class<?> cls = Class.forName("com.android.internal.telephony.Phone");
        try {
            Field field = cls.getField("GEMINI_SIM_1");
            field.setAccessible(true);
            int parseInt = Integer.parseInt(field.get(null).toString());
            Field field2 = cls.getField("GEMINI_SIM_2");
            field2.setAccessible(true);
            int parseInt2 = Integer.parseInt(field2.get(null).toString());
            i2 = parseInt;
            i = parseInt2;
        } catch (Exception e2) {
            i = 1;
            i2 = 0;
        }
        Method method = TelephonyManager.class.getMethod("getDefault", Integer.TYPE);
        Object[] objArr = {Integer.valueOf(i2)};
        Object[] objArr2 = {Integer.valueOf(i)};
        String trim = ((TelephonyManager) method.invoke(telephonyManager, objArr)).getDeviceId().trim();
        String trim2 = ((TelephonyManager) method.invoke(telephonyManager, objArr2)).getDeviceId().trim();
        if (!hashSet.contains(trim) && trim != null && trim.indexOf("0000000000") == -1) {
            hashSet.add(trim);
        }
        if (!hashSet.contains(trim2) && trim2 != null && trim2.indexOf("0000000000") == -1) {
            hashSet.add(trim2);
        }
    }

    /* access modifiers changed from: package-private */
    public void getIMEI_SpreadDoubleSim(Context context, HashSet<String> hashSet) {
        boolean z;
        try {
            Class<?> cls = Class.forName("com.android.internal.telephony.PhoneFactory");
            String trim = ((TelephonyManager) context.getSystemService("phone")).getDeviceId().trim();
            String trim2 = ((TelephonyManager) context.getSystemService((String) cls.getMethod("getServiceName", String.class, Integer.TYPE).invoke(cls, "phone", 1))).getDeviceId().trim();
            if ((!hashSet.contains(trim)) && isIMEI(trim)) {
                hashSet.add(trim);
            }
            if (!hashSet.contains(trim)) {
                z = true;
            } else {
                z = false;
            }
            if (z && isIMEI(trim2)) {
                hashSet.add(trim2);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void getIMEI_QualcommDoubleSim(Context context, HashSet<String> hashSet) {
        Class<?> cls = Class.forName("android.telephony.MSimTelephonyManager");
        Object systemService = context.getSystemService("phone_msim");
        Method method = cls.getMethod("getDeviceId", Integer.TYPE);
        String trim = ((String) method.invoke(systemService, 0)).trim();
        String trim2 = ((String) method.invoke(systemService, 1)).trim();
        if (!hashSet.contains(trim) && trim != null && trim.indexOf("0000000000") == -1) {
            hashSet.add(trim);
        }
        if (!hashSet.contains(trim2) && trim2 != null && trim2.indexOf("0000000000") == -1) {
            hashSet.add(trim2);
        }
    }

    private void getIMEI2_LenovoA3580(Context context, HashSet<String> hashSet) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager.getPhoneType() == 1) {
            }
            Method method = telephonyManager.getClass().getMethod("getDeviceId", Integer.TYPE);
            if (method != null) {
                String str = (String) method.invoke(telephonyManager, 0);
                if (!TextUtils.isEmpty(str)) {
                    hashSet.add(str);
                }
                String str2 = (String) method.invoke(telephonyManager, 1);
                if (!TextUtils.isEmpty(str2)) {
                    hashSet.add(str2);
                }
                String str3 = (String) method.invoke(telephonyManager, 2);
                if (!TextUtils.isEmpty(str3)) {
                    hashSet.add(str3);
                }
                String str4 = (String) method.invoke(telephonyManager, -1);
                if (!TextUtils.isEmpty(str4)) {
                    hashSet.add(str4);
                }
            }
        } catch (Exception e2) {
        }
    }

    private void getIMEI2_SM7000(Context context, HashSet<String> hashSet) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone2");
            Method method = context.getSystemService("phone2").getClass().getMethod("getImeiInCDMAGSMPhone", null);
            if (method != null) {
                String obj = method.invoke(telephonyManager, null).toString();
                if (isIMEI(obj)) {
                    hashSet.add(obj);
                }
            }
        } catch (Exception e2) {
        }
    }

    private void getMeid_Vivod(Context context, HashSet<String> hashSet) {
        String str;
        try {
            Object invoke = Class.forName("com.android.internal.telephony.ITelephony$Stub").getMethod("asInterface", IBinder.class).invoke(null, (IBinder) Class.forName("android.os.ServiceManager").getMethod("getService", String.class).invoke(null, "phone"));
            try {
                str = invoke.getClass().getMethod("getMeid", new Class[0]).invoke(invoke, new Object[0]).toString();
            } catch (Exception e2) {
                e2.toString();
                str = "";
            }
        } catch (Exception e3) {
            str = "";
        }
        if (isIMEI(str)) {
            hashSet.add(str);
        }
    }

    private void getIMEI_Huawei(Context context, HashSet<String> hashSet) {
        String cdmaimei;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        try {
            Method method = context.getSystemService("phone").getClass().getMethod("getCurrentPhoneType", null);
            if (method != null) {
                int intValue = ((Integer) method.invoke(telephonyManager, null)).intValue();
                if (intValue != 1 && intValue == 2 && (cdmaimei = getCDMAIMEI()) != null && cdmaimei.length() > 0) {
                    hashSet.add(cdmaimei);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private String getCDMAIMEI() {
        String str;
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            Method method = cls.getMethod("get", String.class);
            if (method != null) {
                if (((String) method.invoke(cls.newInstance(), "telephony.lteOnCdmaDevice")).equals("1")) {
                    Method method2 = Class.forName("com.huawei.android.hwnv.HWNVFuncation").getMethod("getNVIMEI", null);
                    if (method2 == null) {
                        return "";
                    }
                    method2.setAccessible(true);
                    str = (String) method2.invoke(null, null);
                    return str;
                }
            }
            str = "";
        } catch (Exception e2) {
            str = "";
        }
        return str;
    }

    private void getMeid_Huawei(Context context, HashSet<String> hashSet) {
        String str;
        try {
            str = (String) Class.forName("com.huawei.android.hwnv.HWNVFuncation").getMethod("getNVMEID", new Class[0]).invoke(null, null);
        } catch (Exception e2) {
            str = "";
        }
        if (hashSet != null && !hashSet.contains(str) && isIMEI(str)) {
            hashSet.add(str);
        }
    }

    public static boolean isIMEI(String str) {
        Pattern compile = Pattern.compile("^[0-9A-Fa-f]{13,18}+$");
        if (str != null && str.length() != 0 && compile.matcher(str).matches() && str.indexOf("000000000") == -1 && str.indexOf("111111111") == -1 && str.indexOf("222222222") == -1 && str.indexOf("333333333") == -1 && str.indexOf("444444444") == -1 && str.indexOf("555555555") == -1 && str.indexOf("666666666") == -1 && str.indexOf("777777777") == -1 && str.indexOf("888888888") == -1 && str.indexOf("999999999") == -1) {
            return true;
        }
        return false;
    }
}
