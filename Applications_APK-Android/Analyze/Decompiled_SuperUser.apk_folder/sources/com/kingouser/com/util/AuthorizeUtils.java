package com.kingouser.com.util;

import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.util.rsa.RSAUtils;
import com.pureapps.cleaner.b.a;
import com.pureapps.cleaner.manager.g;
import com.pureapps.cleaner.service.NotificationMonitorService;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.util.i;
import com.squareup.okhttp.u;
import com.squareup.okhttp.v;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONObject;

public class AuthorizeUtils {
    private static volatile AuthorizeUtils instance = null;

    public enum CARDBIND {
        Visa,
        Maestro,
        Mastercard,
        JCB
    }

    public enum PAYPALWAY {
        PAYPAL,
        PAYSSION,
        STRIPE
    }

    private AuthorizeUtils() {
    }

    public static AuthorizeUtils getInstance() {
        try {
            if (instance == null) {
                Thread.sleep(300);
                synchronized (AuthorizeUtils.class) {
                    if (instance == null) {
                        instance = new AuthorizeUtils();
                    }
                }
            }
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        return instance;
    }

    public BaseEnvInfo getDevicesInfo(Context context) {
        String str = Build.BRAND;
        String str2 = Build.MODEL;
        int i = Build.VERSION.SDK_INT;
        String str3 = Build.VERSION.RELEASE;
        String imei = DeviceInfoUtils.getImei(context);
        String macAddress = DeviceInfoUtils.getMacAddress(context);
        BaseEnvInfo baseEnvInfo = new BaseEnvInfo();
        baseEnvInfo.brand = str;
        baseEnvInfo.model = str2;
        baseEnvInfo.sdkInt = "" + i;
        baseEnvInfo.release = str3;
        baseEnvInfo.imei = imei;
        baseEnvInfo.mac = macAddress;
        return baseEnvInfo;
    }

    public String getVerify(Context context) {
        StringBuilder sb = new StringBuilder();
        String country = Locale.getDefault().getCountry();
        String str = Build.BRAND;
        String str2 = Build.MODEL;
        "" + Build.VERSION.SDK_INT;
        String str3 = Build.VERSION.RELEASE;
        if (TextUtils.isEmpty(country)) {
            country = "CN";
        }
        sb.append("channel=" + "OffcialSite" + "&");
        sb.append("country=" + country + "&");
        sb.append("manufacturer=" + str + "&");
        sb.append("model_id=" + str2 + "&");
        sb.append("pid=" + context.getPackageName());
        return sb.toString();
    }

    public VerifyInfo parseVerify(v vVar) {
        VerifyInfo verifyInfo;
        Exception e2;
        if (vVar == null) {
            return null;
        }
        try {
            String e3 = vVar.e();
            f.a("bodytoString:" + e3.length());
            f.a("bodytoString:" + e3);
            JSONObject jSONObject = new JSONObject(JSONTokener(e3));
            int i = jSONObject.getInt("status");
            verifyInfo = new VerifyInfo();
            try {
                verifyInfo.paystatus = i;
                if (i == 0) {
                    verifyInfo.paystatus = i;
                    return verifyInfo;
                }
                JSONObject jSONObject2 = jSONObject.getJSONObject("pay_info");
                if (jSONObject2 == null) {
                    return verifyInfo;
                }
                verifyInfo.amount = jSONObject2.getDouble("amount");
                verifyInfo.createTime = jSONObject2.getLong("createTime");
                verifyInfo.modelId = jSONObject2.getString("modelId");
                verifyInfo.ccountry = jSONObject2.getString("ccountry");
                verifyInfo.channel = jSONObject2.getString("channel");
                verifyInfo.goodsName = jSONObject2.getString("goodsName");
                verifyInfo.oriPrice = jSONObject2.getString("oriPrice");
                verifyInfo.updateTime = jSONObject2.getLong("updateTime");
                verifyInfo.id = jSONObject2.getInt("id");
                verifyInfo.manufacturer = jSONObject2.getString("manufacturer");
                verifyInfo.client_id = jSONObject2.getString("client_id");
                verifyInfo.stripe_pub = jSONObject2.getString("stripe_pub");
                verifyInfo.status = jSONObject2.getInt("status");
                return verifyInfo;
            } catch (Exception e4) {
                e2 = e4;
                e2.printStackTrace();
                return verifyInfo;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            verifyInfo = null;
            e2 = exc;
            e2.printStackTrace();
            return verifyInfo;
        }
    }

    public static String JSONTokener(String str) {
        if (str == null || !str.startsWith("﻿")) {
            return str;
        }
        return str.substring(1);
    }

    public PayResultInfo parsePayResult(String str) {
        PayResultInfo payResultInfo;
        Exception e2;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            f.a("parsePayResult:" + str.length());
            f.a("parsePayResult:" + str);
            String JSONTokener = JSONTokener(str);
            payResultInfo = new PayResultInfo();
            try {
                JSONObject jSONObject = new JSONObject(JSONTokener);
                payResultInfo.response_type = jSONObject.getString("response_type");
                JSONObject jSONObject2 = jSONObject.getJSONObject("client");
                if (jSONObject2 != null) {
                    payResultInfo.client_environment = jSONObject2.getString("environment");
                    payResultInfo.cliepaypal_paypal_sdk_version = jSONObject2.getString("paypal_sdk_version");
                    payResultInfo.client_platform = jSONObject2.getString("platform");
                    payResultInfo.client_product_name = jSONObject2.getString("product_name");
                }
                JSONObject jSONObject3 = jSONObject.getJSONObject("response");
                if (jSONObject3 == null) {
                    return payResultInfo;
                }
                payResultInfo.response_create_time = jSONObject3.getString("create_time");
                payResultInfo.response_id = jSONObject3.getString("id");
                payResultInfo.response_intent = jSONObject3.getString("intent");
                payResultInfo.response_state = jSONObject3.getString("state");
                return payResultInfo;
            } catch (Exception e3) {
                e2 = e3;
                e2.printStackTrace();
                return payResultInfo;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            payResultInfo = null;
            e2 = exc;
            e2.printStackTrace();
            return payResultInfo;
        }
    }

    public String getPayPalResultVerify(Context context, PayResultInfo payResultInfo, String str) {
        String str2 = null;
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("pay_id", payResultInfo.response_id);
            jSONObject.put("channel", "OffcialSite");
            jSONObject.put("imei", DeviceInfoUtils.getImei(context));
            jSONObject.put("mac", DeviceInfoUtils.getMacAddress(context));
            jSONObject.put("android_id", DeviceInfoUtils.getAndroidid(context));
            jSONObject.put("pid", context.getPackageName());
            jSONObject.put("client_id", str);
            f.a("加密前:\r\n" + jSONObject.toString());
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("pay_id", RSAUtils.encrypt(payResultInfo.response_id));
            jSONObject2.put("channel", "OffcialSite");
            jSONObject2.put("imei", RSAUtils.encrypt(DeviceInfoUtils.getImei(context)));
            jSONObject2.put("mac", RSAUtils.encrypt(DeviceInfoUtils.getMacAddress(context)));
            jSONObject2.put("android_id", RSAUtils.encrypt(DeviceInfoUtils.getAndroidid(context)));
            jSONObject2.put("pid", context.getPackageName());
            jSONObject2.put("client_id", str);
            str2 = jSONObject2.toString();
            f.a("加密后:\r\n" + str2);
            return str2;
        } catch (Exception e2) {
            return str2;
        }
    }

    public String getPayssionResultVerify(Context context, String str) {
        String str2 = null;
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("order_id", str);
            jSONObject.put("channel", "OffcialSite");
            jSONObject.put("imei", DeviceInfoUtils.getImei(context));
            jSONObject.put("mac", DeviceInfoUtils.getMacAddress(context));
            jSONObject.put("android_id", DeviceInfoUtils.getAndroidid(context));
            jSONObject.put("pid", context.getPackageName());
            f.a("加密前:\r\n" + jSONObject.toString());
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("order_id", RSAUtils.encrypt(str));
            jSONObject2.put("channel", "OffcialSite");
            jSONObject2.put("imei", RSAUtils.encrypt(DeviceInfoUtils.getImei(context)));
            jSONObject2.put("mac", RSAUtils.encrypt(DeviceInfoUtils.getMacAddress(context)));
            jSONObject2.put("android_id", RSAUtils.encrypt(DeviceInfoUtils.getAndroidid(context)));
            jSONObject2.put("pid", context.getPackageName());
            str2 = jSONObject2.toString();
            f.a("加密后:\r\n" + str2);
            return str2;
        } catch (Exception e2) {
            return str2;
        }
    }

    public VerifyPayResult parseVerifyPayResultPayssion(v vVar) {
        VerifyPayResult verifyPayResult;
        Exception e2;
        if (vVar == null) {
            return null;
        }
        try {
            String e3 = vVar.e();
            f.a("parseVerifyPayResultPaypal:" + e3);
            f.a("bodytoString:" + e3.length());
            String JSONTokener = JSONTokener(e3);
            verifyPayResult = new VerifyPayResult();
            try {
                JSONObject jSONObject = new JSONObject(JSONTokener);
                int i = jSONObject.getInt("status");
                if (i == 1) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("order");
                    if (jSONObject2 == null) {
                        return verifyPayResult;
                    }
                    verifyPayResult.authorization = jSONObject2.getString("authorization");
                    verifyPayResult.total = jSONObject2.getString("total");
                    verifyPayResult.createTime = jSONObject2.getString("createTime");
                    verifyPayResult.description = jSONObject2.getString("description");
                    verifyPayResult.currency = jSONObject2.getString(FirebaseAnalytics.Param.CURRENCY);
                    verifyPayResult.state = jSONObject2.getString("state");
                    verifyPayResult.pay_id = jSONObject2.getString("order_id");
                    verifyPayResult.intent = jSONObject2.getString("intent");
                    return verifyPayResult;
                }
                verifyPayResult.paystate = i;
                f.a("bodytoString:" + i);
                return verifyPayResult;
            } catch (Exception e4) {
                e2 = e4;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            verifyPayResult = null;
            e2 = exc;
            e2.printStackTrace();
            return verifyPayResult;
        }
    }

    public VerifyPayResult parseVerifyPayResultPaypal(v vVar) {
        VerifyPayResult verifyPayResult;
        Exception e2;
        if (vVar == null) {
            return null;
        }
        try {
            String e3 = vVar.e();
            f.a("parseVerifyPayResultPaypal:" + e3);
            f.a("bodytoString:" + e3.length());
            String JSONTokener = JSONTokener(e3);
            verifyPayResult = new VerifyPayResult();
            try {
                JSONObject jSONObject = new JSONObject(JSONTokener);
                int i = jSONObject.getInt("status");
                if (i == 1) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("order");
                    if (jSONObject2 == null) {
                        return verifyPayResult;
                    }
                    verifyPayResult.authorization = jSONObject2.getString("authorization");
                    verifyPayResult.total = jSONObject2.getString("total");
                    verifyPayResult.createTime = jSONObject2.getString("createTime");
                    verifyPayResult.description = jSONObject2.getString("description");
                    verifyPayResult.currency = jSONObject2.getString(FirebaseAnalytics.Param.CURRENCY);
                    verifyPayResult.state = jSONObject2.getString("state");
                    verifyPayResult.pay_id = jSONObject2.getString("pay_id");
                    verifyPayResult.intent = jSONObject2.getString("intent");
                    return verifyPayResult;
                }
                verifyPayResult.paystate = i;
                f.a("bodytoString:" + i);
                return verifyPayResult;
            } catch (Exception e4) {
                e2 = e4;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            verifyPayResult = null;
            e2 = exc;
            e2.printStackTrace();
            return verifyPayResult;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.util.ArrayList, int, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public void saveAuthorization(Context context, String str) {
        String[] verifyLocal = getVerifyLocal(context);
        for (int i = 0; i < verifyLocal.length; i++) {
            if (ShellUtils.checkSuVerison()) {
                ArrayList arrayList = new ArrayList();
                arrayList.add("mount -o remount,rw /system");
                arrayList.add("echo " + str + " > " + verifyLocal[i]);
                arrayList.add("chmod 755 " + verifyLocal[i]);
                ShellUtils.execCommand((List<String>) arrayList, true, true);
            } else {
                writeContent(verifyLocal[i], str);
            }
        }
    }

    private String[] getVerifyLocal(Context context) {
        return new String[]{File.separator + "data" + File.separator + "kogauthor", context.getFilesDir() + File.separator + "kogauthor"};
    }

    public boolean verifyAuthorization(Context context, String str) {
        int i;
        boolean z = false;
        if (!TextUtils.isEmpty(str)) {
            f.a("验证授权码...");
            try {
                String decrypt = RSAUtils.decrypt(str);
                if (!TextUtils.isEmpty(decrypt)) {
                    String imei = DeviceInfoUtils.getImei(context);
                    if (decrypt.contains(imei)) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    String macAddress = DeviceInfoUtils.getMacAddress(context);
                    if (decrypt.contains(macAddress)) {
                        i++;
                    }
                    String androidid = DeviceInfoUtils.getAndroidid(context);
                    if (decrypt.contains(androidid)) {
                        i++;
                    }
                    f.a("macth imei:" + imei);
                    f.a("macth macAddress:" + macAddress);
                    f.a("macth androidid:" + androidid);
                    f.a("macth count:" + i);
                    if (i >= 2) {
                        z = true;
                    }
                    i.a(context).b(z);
                    if (z) {
                        a.a(35, 0, null);
                    }
                    DoAdSu(context, z);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return z;
    }

    public boolean checkAuthorization(Context context) {
        String[] verifyLocal = getVerifyLocal(context);
        boolean z = false;
        for (String str : verifyLocal) {
            f.a("文件校验:" + str);
            if (new File(str).exists()) {
                try {
                    String readFile = readFile(str);
                    f.a("文件内容:" + readFile);
                    z = verifyAuthorization(context, readFile);
                    if (z) {
                        break;
                    }
                } catch (IOException e2) {
                    e2.printStackTrace();
                    z = false;
                }
            }
        }
        return z;
    }

    public void saveServiceNeedPayState(Context context, boolean z) {
        SPUtil.getInstant(context).save("serviceneedpay", Boolean.valueOf(z));
        saveLastCheckNeedPayDate(context);
    }

    public boolean getServiceNeedPayState(Context context) {
        return ((Boolean) SPUtil.getInstant(context).get("serviceneedpay", true)).booleanValue();
    }

    public void saveLastCheckNeedPayDate(Context context) {
        SPUtil.getInstant(context).save("lastcheckpay", Long.valueOf(System.currentTimeMillis()));
    }

    public boolean getLastCheckNeedPayDate(Context context) {
        if (Math.abs(System.currentTimeMillis() - ((Long) SPUtil.getInstant(context).get("lastcheckpay", Long.valueOf(System.currentTimeMillis()))).longValue()) > 21600000) {
            return true;
        }
        return false;
    }

    private void DoAdSu(Context context, boolean z) {
        i.a(context).b(z);
        if (z) {
            a.a(35, 0, null);
        }
    }

    public void updataServicesNeedPay(Context context) {
        boolean checkAuthorization = checkAuthorization(context);
        i.a(context).b(checkAuthorization);
        if (checkAuthorization) {
            a.a(35, 0, null);
        }
        if (checkAuthorization) {
            com.pureapps.cleaner.analytic.a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnPayCheckAuthorOK");
            f.a("检查是否需要付费，已经存在授权码...");
            return;
        }
        DoAdSu(context, checkAuthorization);
        if (!ShellUtils.checkSuVerison()) {
            com.pureapps.cleaner.analytic.a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnPayCheckNoRoot");
        } else if (Build.VERSION.SDK_INT < 11) {
            com.pureapps.cleaner.analytic.a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnPayCheckBelowJelly");
        } else {
            grantPermission(context);
            com.pureapps.cleaner.analytic.a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnPayCheckHaveRoot");
            startCheckIsPaySwichOn(context);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.util.ArrayList, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String[], int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public void grantPermission(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= 18 && !NotificationMonitorService.b(context)) {
                ShellUtils.execCommand("settings put secure enabled_notification_listeners com.kingouser.com/com.pureapps.cleaner.service.NotificationMonitorService:com.pureapps.cleaner/com.pureapps.cleaner.service.NotificationMonitorService", true);
            }
        } catch (Exception e2) {
        }
        if (Build.VERSION.SDK_INT >= 23) {
            HashSet hashSet = new HashSet();
            hashSet.add("android.permission.READ_PHONE_STATE");
            hashSet.add("android.permission.ACCESS_WIFI_STATE");
            hashSet.add("android.permission.SYSTEM_ALERT_WINDOW");
            hashSet.add("android.permission.ACCESS_FINE_LOCATION");
            hashSet.add("android.permission.PACKAGE_USAGE_STATS");
            ArrayList arrayList = new ArrayList();
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                if (context.getPackageManager().checkPermission(str, context.getPackageName()) != 0) {
                    arrayList.add("pm grant " + context.getPackageName() + " " + str);
                }
            }
            try {
                ShellUtils.execCommand((List<String>) arrayList, true);
            } catch (Exception e3) {
            }
            Log.d("wyy", "checkPermission:" + context.getPackageManager().checkPermission("android.permission.PACKAGE_USAGE_STATS", context.getPackageName()));
            UsageStatsManager usageStatsManager = (UsageStatsManager) context.getSystemService("usagestats");
            long currentTimeMillis = System.currentTimeMillis();
            if (usageStatsManager == null) {
                f.a("mUsageStatsManager is null ? " + (usageStatsManager == null));
                return;
            }
            List<UsageStats> queryUsageStats = usageStatsManager.queryUsageStats(4, currentTimeMillis - 2000, currentTimeMillis);
            f.a("usageStatsList usageStatsList" + queryUsageStats.size());
            if (queryUsageStats == null || queryUsageStats.size() <= 1) {
                f.a("usageStatsList is error ,start open appops");
                File file = new File(context.getFilesDir().getAbsolutePath() + File.separator + "kt.db");
                if (!file.exists()) {
                    FileUtils.copyAssetFile(context, "kt.db");
                }
                ShellUtils.execCommand(new String[]{"chmod 777 " + file.getAbsolutePath(), "export CLASSPATH=" + file.getAbsolutePath(), "app_process " + file.getAbsolutePath() + " " + "kingo.nativetool.Main" + " setappops " + context.getPackageName() + " " + ("" + context.getApplicationInfo().uid) + " " + "0"}, true);
            }
        }
    }

    private void startCheckIsPaySwichOn(final Context context) {
        f.a("检查支付开关是否打开，开始检查...");
        com.pureapps.cleaner.analytic.a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnPayCheckSwich");
        f.a("request:" + com.kingouser.com.b.a.f4237f);
        g.a().a(com.kingouser.com.b.a.f4237f, new g.a() {
            public void onBack(u uVar) {
                if (uVar == null) {
                    AuthorizeUtils.this.saveServiceNeedPayState(context, false);
                } else if (!uVar.d()) {
                    f.a("response: erroe");
                    f.a("检查支付开关是否打开 失败");
                    AuthorizeUtils.this.saveServiceNeedPayState(context, false);
                } else {
                    f.a("解析结果");
                    PayerSwitchOn access$000 = AuthorizeUtils.getInstance().parsePaySwitchOn(uVar.g());
                    if (access$000 == null) {
                        f.a("检查是否需要付费 失败");
                        AuthorizeUtils.this.saveServiceNeedPayState(context, false);
                    } else if (access$000.status == 0) {
                        f.a("检测失败，忽略掉不需要付费");
                        AuthorizeUtils.this.saveServiceNeedPayState(context, false);
                        com.pureapps.cleaner.analytic.a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnPayCheckSwichOff");
                    } else if (access$000.usr_pay.equals("1")) {
                        f.a("检查是否需要付费 是的 需要付费");
                        AuthorizeUtils.this.saveServiceNeedPayState(context, true);
                        com.pureapps.cleaner.analytic.a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnPayCheckSwichON");
                        AuthorizeUtils.this.startCheckPayerIsExit(context);
                    } else {
                        f.a("检查是否需要付费 不需要付费");
                        com.pureapps.cleaner.analytic.a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnPayCheckSwichOff");
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public PayerSwitchOn parsePaySwitchOn(v vVar) {
        PayerSwitchOn payerSwitchOn;
        Exception e2;
        JSONObject jSONObject;
        try {
            String e3 = vVar.e();
            f.a("bodytoString:" + e3.length());
            f.a("bodytoString:" + e3);
            JSONObject jSONObject2 = new JSONObject(JSONTokener(e3));
            int i = jSONObject2.getInt("status");
            payerSwitchOn = new PayerSwitchOn();
            if (i == 0) {
                try {
                    payerSwitchOn.status = 0;
                } catch (Exception e4) {
                    e2 = e4;
                    e2.printStackTrace();
                    return payerSwitchOn;
                }
            } else if (i == 1 && (jSONObject = jSONObject2.getJSONObject("switch")) != null) {
                payerSwitchOn.status = 1;
                payerSwitchOn.usr_pay = jSONObject.getString("usr_pay");
            }
        } catch (Exception e5) {
            Exception exc = e5;
            payerSwitchOn = null;
            e2 = exc;
            e2.printStackTrace();
            return payerSwitchOn;
        }
        return payerSwitchOn;
    }

    /* access modifiers changed from: private */
    public void startCheckPayerIsExit(final Context context) {
        f.a("检查是否已经付费，开始检查...");
        String payerInfo = getInstance().getPayerInfo(context);
        f.a("检查是否已经付费 开始....");
        f.a("参数:" + payerInfo);
        g.a().a(com.kingouser.com.b.a.f4236e, payerInfo, new g.a() {
            public void onBack(u uVar) {
                if (uVar != null) {
                    if (!uVar.d()) {
                        f.a("response: erroe");
                        return;
                    }
                    PayerInfo parsePayerInfo = AuthorizeUtils.getInstance().parsePayerInfo(uVar.g());
                    if (parsePayerInfo == null) {
                        AuthorizeUtils.this.saveServiceNeedPayState(context, true);
                    } else if (parsePayerInfo.status == 0) {
                        f.a("检查是否是付费用户:非付费用户 需要付款!!!!");
                        AuthorizeUtils.this.saveServiceNeedPayState(context, true);
                        AuthorizeUtils.this.startCustomizePaypal(context);
                    } else {
                        f.a("已经付过了，保存授权码");
                        AuthorizeUtils.this.verifyAuthorizeAndSaveResult(context, parsePayerInfo);
                    }
                }
                AuthorizeUtils.this.saveServiceNeedPayState(context, true);
            }
        });
    }

    public void verifyAuthorizeAndSaveResult(Context context, PayerInfo payerInfo) {
        if (payerInfo != null) {
            String str = payerInfo.auth;
            f.a("授权码:" + str);
            if (getInstance().verifyAuthorization(context, str)) {
                f.a("验证授权码有效");
                getInstance().saveAuthorization(context, str);
                f.a("二次校验" + getInstance().checkAuthorization(context));
                return;
            }
            f.a("验证授权码无效");
            startCustomizePaypal(context);
        }
    }

    public PayerInfo parsePayerInfo(v vVar) {
        PayerInfo payerInfo;
        Exception e2;
        if (vVar == null) {
            return null;
        }
        try {
            String e3 = vVar.e();
            f.a("bodytoString:" + e3.length());
            f.a("bodytoString:" + e3);
            JSONObject jSONObject = new JSONObject(JSONTokener(e3));
            int i = jSONObject.getInt("status");
            payerInfo = new PayerInfo();
            if (i == 0) {
                try {
                    payerInfo.status = 0;
                    return payerInfo;
                } catch (Exception e4) {
                    e2 = e4;
                }
            } else if (i != 1) {
                return payerInfo;
            } else {
                JSONObject jSONObject2 = jSONObject.getJSONObject("payer_info");
                if (jSONObject2 == null) {
                    return payerInfo;
                }
                payerInfo.status = 1;
                payerInfo.auth = jSONObject2.getString("auth");
                payerInfo.channel = jSONObject2.getString("channel");
                payerInfo.androidId = jSONObject2.getString("androidId");
                payerInfo.mac = jSONObject2.getString("mac");
                return payerInfo;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            payerInfo = null;
            e2 = exc;
            e2.printStackTrace();
            return payerInfo;
        }
    }

    public String getPayerInfo(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("imei", RSAUtils.encrypt(DeviceInfoUtils.getImei(context)));
            jSONObject.put("mac", RSAUtils.encrypt(DeviceInfoUtils.getMacAddress(context)));
            jSONObject.put("android_id", RSAUtils.encrypt(DeviceInfoUtils.getAndroidid(context)));
        } catch (Exception e2) {
            e2.printStackTrace();
            jSONObject = null;
        }
        if (jSONObject == null) {
            return "";
        }
        return jSONObject.toString();
    }

    public boolean isStartPaypalActivity(Context context) {
        if (checkAuthorization(context)) {
            return false;
        }
        return getServiceNeedPayState(context);
    }

    public String getStripePayByToken(Context context, String str, String str2, Double d2) {
        f.a("getStripePayByToken:\r\n");
        String str3 = null;
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("pid", context.getPackageName());
            jSONObject.put("channel", "OffcialSite");
            jSONObject.put("imei", DeviceInfoUtils.getImei(context));
            jSONObject.put("mac", DeviceInfoUtils.getMacAddress(context));
            jSONObject.put("android_id", DeviceInfoUtils.getAndroidid(context));
            jSONObject.put("stripe_pub", str2);
            jSONObject.put("token", str);
            jSONObject.put("amount", d2);
            f.a("加密前:\r\n" + jSONObject.toString());
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("pid", context.getPackageName());
            jSONObject2.put("channel", "OffcialSite");
            jSONObject2.put("imei", RSAUtils.encrypt(DeviceInfoUtils.getImei(context)));
            jSONObject2.put("mac", RSAUtils.encrypt(DeviceInfoUtils.getMacAddress(context)));
            jSONObject2.put("android_id", RSAUtils.encrypt(DeviceInfoUtils.getAndroidid(context)));
            jSONObject2.put("stripe_pub", str2);
            jSONObject2.put("token", str);
            jSONObject2.put("amount", d2);
            str3 = jSONObject2.toString();
            f.a("加密后:\r\n" + str3);
            return str3;
        } catch (Exception e2) {
            return str3;
        }
    }

    public StripInfo parseStripeInfo(v vVar) {
        Exception e2;
        StripInfo stripInfo;
        if (vVar == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(JSONTokener(vVar.e()));
            if (jSONObject.getInt("status") != 1 || !jSONObject.getString("msg").equals("success")) {
                stripInfo = null;
                return stripInfo;
            }
            JSONObject jSONObject2 = jSONObject.getJSONObject("pay_info");
            stripInfo = new StripInfo();
            if (jSONObject2 != null) {
                try {
                    stripInfo.status = 1;
                    stripInfo.msg = "success";
                    stripInfo.auth = jSONObject2.getString("auth");
                    stripInfo.channel = jSONObject2.getString("channel");
                    stripInfo.description = jSONObject2.getString("description");
                    stripInfo.order_id = jSONObject2.getString("order_id");
                } catch (Exception e3) {
                    e2 = e3;
                    e2.printStackTrace();
                    return stripInfo;
                }
            }
            return stripInfo;
        } catch (Exception e4) {
            Exception exc = e4;
            stripInfo = null;
            e2 = exc;
            e2.printStackTrace();
            return stripInfo;
        }
    }

    public CARDBIND getCardBIN(String str) {
        if (str.startsWith("51") || str.startsWith("52") || str.startsWith("53") || str.startsWith("54") || str.startsWith("55")) {
            f.a("mastercard");
            return CARDBIND.Mastercard;
        } else if (str.startsWith("5018") || str.startsWith("5020") || str.startsWith("5038") || str.startsWith("6304") || str.startsWith("6759") || str.startsWith("6761") || str.startsWith("6762") || str.startsWith("6763")) {
            f.a("Maestro");
            return CARDBIND.Maestro;
        } else if (!str.startsWith("4")) {
            return null;
        } else {
            f.a("Visa");
            return CARDBIND.Visa;
        }
    }

    public class PayerSwitchOn {
        public int status;
        public String usr_pay;

        public PayerSwitchOn() {
        }
    }

    public class PayerInfo {
        public String androidId;
        public String auth;
        public String channel;
        public String imei;
        public String mac;
        public int status;

        public PayerInfo() {
        }
    }

    public class VerifyPayResult {
        public String authorization;
        public String createTime;
        public String currency;
        public String description;
        public String intent;
        public String pay_id;
        public int paystate;
        public String state;
        public String total;

        public VerifyPayResult() {
        }
    }

    public class PayResultInfo {
        public String client_environment;
        public String client_platform;
        public String client_product_name;
        public String cliepaypal_paypal_sdk_version;
        public String response_create_time;
        public String response_id;
        public String response_intent;
        public String response_state;
        public String response_type;

        public PayResultInfo() {
        }
    }

    public class VerifyInfo {
        public double amount;
        public String ccountry;
        public String channel;
        public String client_id;
        public long createTime;
        public String goodsName;
        public int id;
        public String manufacturer;
        public String modelId;
        public String oriPrice;
        public int paystatus;
        public int status;
        public String stripe_pub;
        public long updateTime;

        public VerifyInfo() {
        }
    }

    public class StripInfo {
        public String auth;
        public String channel;
        public String description;
        public String msg;
        public String order_id;
        public String pid;
        public String state;
        public int status;

        public StripInfo() {
        }

        public String toString() {
            return "StripInfo{msg='" + this.msg + '\'' + ", auth='" + this.auth + '\'' + ", channel='" + this.channel + '\'' + ", description='" + this.description + '\'' + ", pid='" + this.pid + '\'' + ", state='" + this.state + '\'' + ", order_id='" + this.order_id + '\'' + ", status=" + this.status + '}';
        }
    }

    public class BaseEnvInfo {
        public String brand;
        public String channel;
        public String country;
        public String imei;
        public String mac;
        public String model;
        public String release;
        public String sdkInt;

        public BaseEnvInfo() {
        }

        public String toString() {
            return "BaseEnvInfo{brand='" + this.brand + '\'' + ", model='" + this.model + '\'' + ", sdkInt='" + this.sdkInt + '\'' + ", release='" + this.release + '\'' + ", imei='" + this.imei + '\'' + ", mac='" + this.mac + '\'' + '}';
        }
    }

    public static String readFile(String str) {
        File file = new File(str);
        byte[] bArr = new byte[((int) file.length())];
        new DataInputStream(new FileInputStream(file)).readFully(bArr);
        return new String(bArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    public static void writeContent(String str, String str2) {
        BufferedWriter bufferedWriter;
        Throwable th;
        BufferedWriter bufferedWriter2 = null;
        try {
            if (new File(str).exists()) {
                new File(str).delete();
            }
            bufferedWriter = new BufferedWriter(new FileWriter(str, false));
            try {
                bufferedWriter.write(str2);
                try {
                    bufferedWriter.close();
                } catch (Exception e2) {
                }
            } catch (Exception e3) {
                bufferedWriter2 = bufferedWriter;
                try {
                    bufferedWriter2.close();
                } catch (Exception e4) {
                }
            } catch (Throwable th2) {
                th = th2;
                try {
                    bufferedWriter.close();
                } catch (Exception e5) {
                }
                throw th;
            }
        } catch (Exception e6) {
            bufferedWriter2.close();
        } catch (Throwable th3) {
            Throwable th4 = th3;
            bufferedWriter = null;
            th = th4;
            bufferedWriter.close();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public void startCustomizePaypal(Context context) {
    }
}
