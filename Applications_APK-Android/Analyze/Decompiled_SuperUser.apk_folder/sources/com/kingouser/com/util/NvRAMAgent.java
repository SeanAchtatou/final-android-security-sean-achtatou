package com.kingouser.com.util;

import android.os.IInterface;

public interface NvRAMAgent extends IInterface {
    byte[] readFile(int i);

    byte[] readFileByName(String str);

    int writeFile(int i, byte[] bArr);

    int writeFileByName(String str, byte[] bArr);
}
