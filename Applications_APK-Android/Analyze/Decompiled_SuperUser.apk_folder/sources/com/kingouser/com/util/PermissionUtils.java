package com.kingouser.com.util;

import android.content.Context;
import com.duapps.ad.AdError;
import com.kingouser.com.db.KingoDatabaseHelper;
import com.kingouser.com.entity.UidPolicy;
import com.pureapps.cleaner.util.f;
import io.fabric.sdk.android.services.common.a;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class PermissionUtils {
    public static void createPrePermission(Context context, int i) {
        f.a("createPrePermission v1 \t:" + System.currentTimeMillis());
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(String.format("[%s]%s", "default", ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s=%d%s", "notify", 1, ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s=%d%s", "log", 1, ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s=%d%s", "wait", Integer.valueOf(i), ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s=%d%s", "access", 2, ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s=%d%s", "respectcm", 0, ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s=%d%s", "trustsystem", 0, ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s=%d%s", "enablemultiuser", 0, ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s=%d%s", "enableduringboot", 0, ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s=%d%s", "enablemountnamespaceseparation", 1, ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s", ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("[%s]%s", "ADB shell", ShellUtils.COMMAND_LINE_END));
        stringBuffer.append(String.format("%s=%d%s", "access", 1, ShellUtils.COMMAND_LINE_END));
        String str = context.getFilesDir() + "/supersu.cfg";
        FileUtils.write(str, stringBuffer.toString(), false);
        f.a("createPrePermission v2 \t:" + System.currentTimeMillis());
        saveAdbShellToDb(context);
        f.a("createPrePermission v3 \t:" + System.currentTimeMillis());
        chmodRootPermission(context, str);
    }

    private static void saveAdbShellToDb(Context context) {
        UidPolicy uidPolicy = new UidPolicy();
        uidPolicy.policy = UidPolicy.ALLOW;
        uidPolicy.uid = AdError.SERVER_ERROR_CODE;
        uidPolicy.command = null;
        uidPolicy.until = 0;
        uidPolicy.desiredUid = AdError.SERVER_ERROR_CODE;
        KingoDatabaseHelper.a(context, uidPolicy);
    }

    public static void deny(Context context, String str, int i) {
        if (i != -1) {
            String packageName = context.getPackageName();
            try {
                new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("/system/xbin/su -v").getInputStream())).readLine();
                String[] strArr = {String.format("mkdir %s", "/data/data/" + packageName + "/requests/"), String.format("chmod 600 %s", "/data/data/" + packageName + "/requests/"), String.format("echo '%s' > %s%d", "0", "/data/data/" + packageName + "/requests/", Integer.valueOf(i)), String.format("chmod 600 %s%d", "/data/data/" + packageName + "/requests/", Integer.valueOf(i))};
                Process exec = Runtime.getRuntime().exec("/system/xbin/su");
                DataOutputStream dataOutputStream = new DataOutputStream(exec.getOutputStream());
                new DataInputStream(exec.getInputStream());
                new DataInputStream(exec.getErrorStream());
                for (int i2 = 0; i2 < strArr.length; i2++) {
                    dataOutputStream.writeBytes(String.valueOf(strArr[i2]) + ShellUtils.COMMAND_LINE_END);
                    dataOutputStream.flush();
                }
                if (FileUtils.checkFileExist(context, context.getFilesDir() + "/" + "supersu.cfg")) {
                    RemoveAppFromCfg(context, str);
                } else {
                    createPrePermission(context, MySharedPreference.getRequestDialogTimes(context, 15));
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void allow(Context context, String str, int i, int i2, int i3) {
        if (i3 != -1) {
            String packageName = context.getPackageName();
            try {
                new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("su -v").getInputStream())).readLine();
                String[] strArr = {String.format("mkdir %s", "/data/data/" + packageName + "/requests/"), String.format("chmod 600 %s", "/data/data/" + packageName + "/requests/"), String.format("echo '%s' > %s%d", "1:1:::", "/data/data/" + packageName + "/requests/", Integer.valueOf(i3)), String.format("chmod 600 %s%d", "/data/data/" + packageName + "/requests/", Integer.valueOf(i3))};
                Process exec = Runtime.getRuntime().exec(ShellUtils.COMMAND_SU);
                DataOutputStream dataOutputStream = new DataOutputStream(exec.getOutputStream());
                new DataInputStream(exec.getInputStream());
                new DataInputStream(exec.getErrorStream());
                for (int i4 = 0; i4 < strArr.length; i4++) {
                    dataOutputStream.writeBytes(String.valueOf(strArr[i4]) + ShellUtils.COMMAND_LINE_END);
                    dataOutputStream.flush();
                }
                context.getFilesDir() + "/supersu.cfg";
                if (FileUtils.checkFileExist(context, context.getFilesDir() + "/" + "supersu.cfg")) {
                    addApp2Cfg(context, str, i % a.DEFAULT_TIMEOUT, i2);
                } else {
                    createPrePermission(context, MySharedPreference.getRequestDialogTimes(context, 15));
                    addApp2Cfg(context, str, i % a.DEFAULT_TIMEOUT, i2);
                }
                if ("ADB shell".equalsIgnoreCase(str)) {
                    saveAdbShellToDb(context);
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void handleAction(Context context, boolean z, boolean z2, Integer num, int i, int i2) {
        String str;
        try {
            if (num.intValue() != -1) {
                UidPolicy uidPolicy = new UidPolicy();
                if (num.intValue() != 0) {
                    uidPolicy.policy = UidPolicy.INTERACTIVE;
                } else {
                    if (z2) {
                        str = UidPolicy.ALLOW;
                    } else {
                        str = UidPolicy.DENY;
                    }
                    uidPolicy.policy = str;
                }
                uidPolicy.uid = i;
                uidPolicy.command = null;
                uidPolicy.until = num.intValue();
                uidPolicy.desiredUid = i2;
                KingoDatabaseHelper.a(context, uidPolicy);
                if (UidPolicy.INTERACTIVE.equalsIgnoreCase(uidPolicy.policy)) {
                    MySharedPreference.setPermissionState(context, true);
                }
            }
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String[], int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static void addApp2Cfg(Context context, String str, int i, int i2) {
        FileUtils.createConfig(context);
        String str2 = context.getFilesDir().getPath() + "/config";
        String[] strArr = {"chmod " + DeviceInfoUtils.getChmodCode(context) + " " + str2, str2 + " -a \"" + str + "\" uid " + i, str2 + " -a \"" + str + "\" access " + i2};
        for (int i3 = 0; i3 < strArr.length; i3++) {
            f.a("addApp2Cfg:" + strArr[i3]);
        }
        ShellUtils.execCommand(strArr, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String[], int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static void chmodRootPermission(Context context, String str) {
        f.a("chmodRootPermission v1 \t:" + System.currentTimeMillis());
        String str2 = context.getFilesDir() + "/busybox ";
        f.a("chmodRootPermission v2 \t:" + System.currentTimeMillis());
        ShellUtils.execCommand(new String[]{"chown 0.0  " + str, str2 + "chown 0.0  " + str, "chown 0:0  " + str, str2 + "chown 0:0  " + str, "chown root.root " + str, str2 + "chown root.root " + str, "chown root:root " + str, str2 + "chown root:root " + str, "chmod 600  " + str, str2 + "chmod 600  " + str}, true);
        f.a("chmodRootPermission v3 \t:" + System.currentTimeMillis());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String[], int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static void RemoveAppFromCfg(Context context, String str) {
        String str2 = context.getFilesDir().getPath() + "/config";
        FileUtils.checkConfigFile(context);
        String[] strArr = {"chmod " + DeviceInfoUtils.getChmodCode(context) + "  " + str2, str2 + " -d " + str};
        for (int i = 0; i < strArr.length; i++) {
            f.a("RemoveAppFromCfg:" + strArr[i]);
        }
        ShellUtils.execCommand(strArr, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String[], int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static void udeAppFromeCfg(Context context, String str, String str2, String str3) {
        String str4 = context.getFilesDir().getPath() + "/config";
        FileUtils.isExistsAndCopy(context, "config");
        ShellUtils.execCommand(new String[]{"chmod 6755 " + str4, str4 + " -w " + str + " " + str2 + " " + str3}, true);
    }
}
