package com.kingouser.com.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.Signature;
import android.net.Uri;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipFile;

public class PackageUtils {
    public static String getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static int getAppversionCode(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 16384).versionCode;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static String getSoftWareName(Context context) {
        context.getPackageManager();
        return context.getApplicationInfo().name;
    }

    public static File extractSu(Activity activity) {
        String str = "armeabi";
        if (System.getProperty("os.arch").contains("x86") || System.getProperty("os.arch").contains("i686") || System.getProperty("os.arch").contains("i386")) {
            str = "x86";
        }
        ZipFile zipFile = new ZipFile(activity.getPackageCodePath());
        InputStream inputStream = zipFile.getInputStream(zipFile.getEntry("assets/" + str + "/su"));
        File fileStreamPath = activity.getFileStreamPath(ShellUtils.COMMAND_SU);
        FileOutputStream fileOutputStream = new FileOutputStream(fileStreamPath);
        StreamUtility.copyStream(inputStream, fileOutputStream);
        inputStream.close();
        zipFile.close();
        fileOutputStream.close();
        return fileStreamPath;
    }

    public static void silenceInstall(Context context, String str) {
        try {
            slientInstall(context, new File(str));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static boolean slientInstall(Context context, File file) {
        InterruptedException e2;
        IOException e3;
        boolean z = true;
        try {
            Process exec = Runtime.getRuntime().exec(ShellUtils.COMMAND_SU);
            OutputStream outputStream = exec.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            dataOutputStream.writeBytes("chmod 777 " + file.getPath() + ShellUtils.COMMAND_LINE_END);
            dataOutputStream.writeBytes("LD_LIBRARY_PATH=/vendor/lib:/system/lib pm install -r " + file.getPath());
            dataOutputStream.flush();
            dataOutputStream.close();
            outputStream.close();
            int waitFor = exec.waitFor();
            if (waitFor == 0) {
                try {
                    file.delete();
                    return true;
                } catch (IOException e4) {
                    e3 = e4;
                } catch (InterruptedException e5) {
                    e2 = e5;
                    e2.printStackTrace();
                    return z;
                }
            } else if (waitFor == 1) {
                return false;
            } else {
                return false;
            }
        } catch (IOException e6) {
            IOException iOException = e6;
            z = false;
            e3 = iOException;
        } catch (InterruptedException e7) {
            InterruptedException interruptedException = e7;
            z = false;
            e2 = interruptedException;
            e2.printStackTrace();
            return z;
        }
        e3.printStackTrace();
        return z;
    }

    public static void authority(String str) {
        try {
            Runtime.getRuntime().exec("chmod 777 " + str);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static String getChannelName(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), FileUtils.FileMode.MODE_IWUSR).metaData.getString("CHANNEL");
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getAppVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:62:0x00db A[SYNTHETIC, Splitter:B:62:0x00db] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00e0 A[SYNTHETIC, Splitter:B:65:0x00e0] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00f3 A[SYNTHETIC, Splitter:B:74:0x00f3] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00f8 A[SYNTHETIC, Splitter:B:77:0x00f8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getSDCardPath() {
        /*
            r2 = 0
            r7 = 1
            java.lang.String r0 = "cat /proc/mounts"
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()
            java.lang.Process r0 = r1.exec(r0)     // Catch:{ Exception -> 0x00d4, all -> 0x00ee }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00d4, all -> 0x00ee }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x00d4, all -> 0x00ee }
            r3.<init>(r1)     // Catch:{ Exception -> 0x00d4, all -> 0x00ee }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x010e, all -> 0x0106 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x010e, all -> 0x0106 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x010e, all -> 0x0106 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x010e, all -> 0x0106 }
        L_0x001f:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            if (r2 == 0) goto L_0x00b7
            java.lang.String r4 = "CommonUitls"
            com.kingouser.com.util.MyLog.i(r4, r2)     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            java.lang.String r4 = r2.toLowerCase()     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            java.lang.String r5 = "sdcard"
            java.lang.String r5 = r5.toLowerCase()     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            boolean r4 = r4.contains(r5)     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            if (r4 == 0) goto L_0x0073
            java.lang.String r4 = ".android_secure"
            boolean r4 = r2.contains(r4)     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            if (r4 == 0) goto L_0x0073
            java.lang.String r4 = " "
            java.lang.String[] r4 = r2.split(r4)     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            if (r4 == 0) goto L_0x0073
            int r5 = r4.length     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            r6 = 5
            if (r5 < r6) goto L_0x0073
            r0 = 1
            r0 = r4[r0]     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            java.lang.String r2 = "/.android_secure"
            java.lang.String r4 = ""
            java.lang.String r0 = r0.replace(r2, r4)     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            java.lang.String r2 = "CommonUitls"
            com.kingouser.com.util.MyLog.i(r2, r0)     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            if (r3 == 0) goto L_0x0063
            r3.close()     // Catch:{ IOException -> 0x0069 }
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ IOException -> 0x006e }
        L_0x0068:
            return r0
        L_0x0069:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0063
        L_0x006e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0068
        L_0x0073:
            java.lang.String r4 = r2.toLowerCase()     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            java.lang.String r5 = "extsd"
            java.lang.String r5 = r5.toLowerCase()     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            boolean r4 = r4.contains(r5)     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            if (r4 == 0) goto L_0x00a9
            java.lang.String r0 = " "
            java.lang.String[] r0 = r2.split(r0)     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            java.lang.String r2 = "CommonUitls"
            r4 = 1
            r4 = r0[r4]     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            com.kingouser.com.util.MyLog.i(r2, r4)     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            r2 = 1
            r0 = r0[r2]     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            if (r3 == 0) goto L_0x0099
            r3.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x0099:
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ IOException -> 0x009f }
            goto L_0x0068
        L_0x009f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0068
        L_0x00a4:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0099
        L_0x00a9:
            int r2 = r0.waitFor()     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            if (r2 == 0) goto L_0x001f
            int r2 = r0.exitValue()     // Catch:{ Exception -> 0x0112, all -> 0x0109 }
            if (r2 != r7) goto L_0x001f
            goto L_0x001f
        L_0x00b7:
            if (r3 == 0) goto L_0x00bc
            r3.close()     // Catch:{ IOException -> 0x00ca }
        L_0x00bc:
            if (r1 == 0) goto L_0x00c1
            r1.close()     // Catch:{ IOException -> 0x00cf }
        L_0x00c1:
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r0 = r0.getPath()
            goto L_0x0068
        L_0x00ca:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00bc
        L_0x00cf:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00c1
        L_0x00d4:
            r0 = move-exception
            r1 = r2
        L_0x00d6:
            r0.printStackTrace()     // Catch:{ all -> 0x010b }
            if (r2 == 0) goto L_0x00de
            r2.close()     // Catch:{ IOException -> 0x00e9 }
        L_0x00de:
            if (r1 == 0) goto L_0x00c1
            r1.close()     // Catch:{ IOException -> 0x00e4 }
            goto L_0x00c1
        L_0x00e4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00c1
        L_0x00e9:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00de
        L_0x00ee:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x00f1:
            if (r3 == 0) goto L_0x00f6
            r3.close()     // Catch:{ IOException -> 0x00fc }
        L_0x00f6:
            if (r1 == 0) goto L_0x00fb
            r1.close()     // Catch:{ IOException -> 0x0101 }
        L_0x00fb:
            throw r0
        L_0x00fc:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00f6
        L_0x0101:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00fb
        L_0x0106:
            r0 = move-exception
            r1 = r2
            goto L_0x00f1
        L_0x0109:
            r0 = move-exception
            goto L_0x00f1
        L_0x010b:
            r0 = move-exception
            r3 = r2
            goto L_0x00f1
        L_0x010e:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x00d6
        L_0x0112:
            r0 = move-exception
            r2 = r3
            goto L_0x00d6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.PackageUtils.getSDCardPath():java.lang.String");
    }

    public static void installApk(Context context, String str, String str2) {
        File file = new File(str, str2);
        if (file.exists()) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.addFlags(268435456);
            intent.setDataAndType(Uri.parse("file://" + file.toString()), "application/vnd.android.package-archive");
            context.startActivity(intent);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003c A[SYNTHETIC, Splitter:B:22:0x003c] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041 A[Catch:{ IOException -> 0x0045 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x004e A[SYNTHETIC, Splitter:B:31:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0053 A[Catch:{ IOException -> 0x0057 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getPhoneInfo(java.lang.String r5) {
        /*
            r1 = 0
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0031, all -> 0x004a }
            java.lang.Process r0 = r0.exec(r5)     // Catch:{ IOException -> 0x0031, all -> 0x004a }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0031, all -> 0x004a }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x0031, all -> 0x004a }
            r3.<init>(r0)     // Catch:{ IOException -> 0x0031, all -> 0x004a }
            java.io.LineNumberReader r2 = new java.io.LineNumberReader     // Catch:{ IOException -> 0x0061, all -> 0x005c }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0061, all -> 0x005c }
            java.lang.String r0 = r2.readLine()     // Catch:{ IOException -> 0x0067 }
            r3.close()     // Catch:{ IOException -> 0x006c }
            r2.close()     // Catch:{ IOException -> 0x006c }
            if (r3 == 0) goto L_0x0026
            r3.close()     // Catch:{ IOException -> 0x002c }
        L_0x0026:
            if (r2 == 0) goto L_0x002b
            r2.close()     // Catch:{ IOException -> 0x002c }
        L_0x002b:
            return r0
        L_0x002c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002b
        L_0x0031:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x0037:
            r1.printStackTrace()     // Catch:{ all -> 0x005e }
            if (r3 == 0) goto L_0x003f
            r3.close()     // Catch:{ IOException -> 0x0045 }
        L_0x003f:
            if (r2 == 0) goto L_0x002b
            r2.close()     // Catch:{ IOException -> 0x0045 }
            goto L_0x002b
        L_0x0045:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002b
        L_0x004a:
            r0 = move-exception
            r3 = r1
        L_0x004c:
            if (r3 == 0) goto L_0x0051
            r3.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0056:
            throw r0
        L_0x0057:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0056
        L_0x005c:
            r0 = move-exception
            goto L_0x004c
        L_0x005e:
            r0 = move-exception
            r1 = r2
            goto L_0x004c
        L_0x0061:
            r0 = move-exception
            r2 = r1
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x0037
        L_0x0067:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x0037
        L_0x006c:
            r1 = move-exception
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.PackageUtils.getPhoneInfo(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003e A[SYNTHETIC, Splitter:B:22:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0043 A[Catch:{ IOException -> 0x0047 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0050 A[SYNTHETIC, Splitter:B:31:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0055 A[Catch:{ IOException -> 0x0059 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getKernelVersion() {
        /*
            r1 = 0
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0033, all -> 0x004c }
            java.lang.String r2 = "cat proc/version"
            java.lang.Process r0 = r0.exec(r2)     // Catch:{ IOException -> 0x0033, all -> 0x004c }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0033, all -> 0x004c }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x0033, all -> 0x004c }
            r3.<init>(r0)     // Catch:{ IOException -> 0x0033, all -> 0x004c }
            java.io.LineNumberReader r2 = new java.io.LineNumberReader     // Catch:{ IOException -> 0x0063, all -> 0x005e }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0063, all -> 0x005e }
            java.lang.String r0 = r2.readLine()     // Catch:{ IOException -> 0x0069 }
            r3.close()     // Catch:{ IOException -> 0x006e }
            r2.close()     // Catch:{ IOException -> 0x006e }
            if (r3 == 0) goto L_0x0028
            r3.close()     // Catch:{ IOException -> 0x002e }
        L_0x0028:
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ IOException -> 0x002e }
        L_0x002d:
            return r0
        L_0x002e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002d
        L_0x0033:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x0039:
            r1.printStackTrace()     // Catch:{ all -> 0x0060 }
            if (r3 == 0) goto L_0x0041
            r3.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0041:
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ IOException -> 0x0047 }
            goto L_0x002d
        L_0x0047:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002d
        L_0x004c:
            r0 = move-exception
            r3 = r1
        L_0x004e:
            if (r3 == 0) goto L_0x0053
            r3.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0058:
            throw r0
        L_0x0059:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0058
        L_0x005e:
            r0 = move-exception
            goto L_0x004e
        L_0x0060:
            r0 = move-exception
            r1 = r2
            goto L_0x004e
        L_0x0063:
            r0 = move-exception
            r2 = r1
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x0039
        L_0x0069:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x0039
        L_0x006e:
            r1 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.util.PackageUtils.getKernelVersion():java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, int):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public static String getCpuInfo() {
        Matcher matcher = Pattern.compile("Hardware\\s+: (.+)").matcher(ShellUtils.execCommand("cat proc/cpuinfo", false, true).successMsg);
        if (matcher.find()) {
        }
        return matcher.group(1);
    }

    public static void getHashKey(Context context) {
        try {
            for (Signature byteArray : context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures) {
                MessageDigest.getInstance("SHA").update(byteArray.toByteArray());
            }
        } catch (Exception e2) {
        }
    }
}
