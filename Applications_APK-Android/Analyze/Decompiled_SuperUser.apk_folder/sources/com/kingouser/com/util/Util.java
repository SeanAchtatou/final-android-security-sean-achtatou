package com.kingouser.com.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.lody.virtual.helper.utils.FileUtils;

public class Util {
    public static boolean appIsInstall(Context context, String str, boolean z) {
        boolean z2;
        boolean z3 = true;
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(str, 0);
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            if ((applicationInfo.flags & FileUtils.FileMode.MODE_IWUSR) != 0) {
                z2 = true;
            } else if ((applicationInfo.flags & 1) == 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (z && !z2) {
                return false;
            }
            if (!z && z2) {
                return false;
            }
            if (packageInfo == null) {
                z3 = false;
            }
            return z3;
        } catch (PackageManager.NameNotFoundException e2) {
        }
    }
}
