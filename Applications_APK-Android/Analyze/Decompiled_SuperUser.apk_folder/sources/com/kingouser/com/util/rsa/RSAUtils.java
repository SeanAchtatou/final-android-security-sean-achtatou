package com.kingouser.com.util.rsa;

import com.lody.virtual.helper.utils.FileUtils;
import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.Cipher;

public class RSAUtils {
    public static final String KEY_ALGORITHM = "RSA";
    public static final String KEY_ALGORITHM_ANDROID = "RSA/ECB/PKCS1Padding";
    private static final int MAX_DECRYPT_BLOCK = 128;
    private static final int MAX_ENCRYPT_BLOCK = 117;
    private static final String PRIVATE_KEY = "RSAPrivateKey";
    private static final String PUBLIC_KEY = "RSAPublicKey";
    public static final String SIGNATURE_ALGORITHM = "MD5withRSA";
    public static final String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCIdoxCQy5O9UFLRrThxRQzFC/G2vjTHlcyxk5NCJwivKg0fLarZz2XOsN8kRTISnzfEFMpY+GAsb6UiD1H89xXTvetX4ESxPs0HEItOq26enjPfDqPK6TplB/ng6X/dpkRoHhkXg9yLMcGQf7LRjDwiG7Ho77+Zmc6A1wMt7xJLQIDAQAB";

    public static Map<String, Object> genKeyPair() {
        KeyPairGenerator instance = KeyPairGenerator.getInstance(KEY_ALGORITHM);
        instance.initialize((int) FileUtils.FileMode.MODE_ISGID);
        KeyPair generateKeyPair = instance.generateKeyPair();
        HashMap hashMap = new HashMap(2);
        hashMap.put(PUBLIC_KEY, (RSAPublicKey) generateKeyPair.getPublic());
        hashMap.put(PRIVATE_KEY, (RSAPrivateKey) generateKeyPair.getPrivate());
        return hashMap;
    }

    public static String sign(byte[] bArr, String str) {
        PrivateKey generatePrivate = KeyFactory.getInstance(KEY_ALGORITHM).generatePrivate(new PKCS8EncodedKeySpec(Base64Utils.decode(str)));
        Signature instance = Signature.getInstance(SIGNATURE_ALGORITHM);
        instance.initSign(generatePrivate);
        instance.update(bArr);
        return Base64Utils.encode(instance.sign());
    }

    public static boolean verify(byte[] bArr, String str, String str2) {
        PublicKey generatePublic = KeyFactory.getInstance(KEY_ALGORITHM).generatePublic(new X509EncodedKeySpec(Base64Utils.decode(str)));
        Signature instance = Signature.getInstance(SIGNATURE_ALGORITHM);
        instance.initVerify(generatePublic);
        instance.update(bArr);
        return instance.verify(Base64Utils.decode(str2));
    }

    public static byte[] decryptByPrivateKey(byte[] bArr, String str) {
        byte[] doFinal;
        PrivateKey generatePrivate = KeyFactory.getInstance(KEY_ALGORITHM).generatePrivate(new PKCS8EncodedKeySpec(Base64Utils.decode(str)));
        Cipher instance = Cipher.getInstance(KEY_ALGORITHM_ANDROID);
        instance.init(2, generatePrivate);
        int length = bArr.length;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        int i2 = 0;
        while (length - i2 > 0) {
            if (length - i2 > 128) {
                doFinal = instance.doFinal(bArr, i2, 128);
            } else {
                doFinal = instance.doFinal(bArr, i2, length - i2);
            }
            byteArrayOutputStream.write(doFinal, 0, doFinal.length);
            int i3 = i + 1;
            int i4 = i3;
            i2 = i3 * 128;
            i = i4;
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return byteArray;
    }

    public static byte[] decryptByPublicKey(byte[] bArr, String str) {
        byte[] doFinal;
        PublicKey generatePublic = KeyFactory.getInstance(KEY_ALGORITHM).generatePublic(new X509EncodedKeySpec(Base64Utils.decode(str)));
        Cipher instance = Cipher.getInstance(KEY_ALGORITHM_ANDROID);
        instance.init(2, generatePublic);
        int length = bArr.length;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        int i2 = 0;
        while (length - i2 > 0) {
            if (length - i2 > 128) {
                doFinal = instance.doFinal(bArr, i2, 128);
            } else {
                doFinal = instance.doFinal(bArr, i2, length - i2);
            }
            byteArrayOutputStream.write(doFinal, 0, doFinal.length);
            int i3 = i + 1;
            int i4 = i3;
            i2 = i3 * 128;
            i = i4;
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return byteArray;
    }

    public static byte[] encryptByPublicKey(byte[] bArr, String str) {
        byte[] doFinal;
        PublicKey generatePublic = KeyFactory.getInstance(KEY_ALGORITHM).generatePublic(new X509EncodedKeySpec(Base64Utils.decode(str)));
        Cipher instance = Cipher.getInstance(KEY_ALGORITHM_ANDROID);
        instance.init(1, generatePublic);
        int length = bArr.length;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        int i2 = 0;
        while (length - i2 > 0) {
            if (length - i2 > MAX_ENCRYPT_BLOCK) {
                doFinal = instance.doFinal(bArr, i2, MAX_ENCRYPT_BLOCK);
            } else {
                doFinal = instance.doFinal(bArr, i2, length - i2);
            }
            byteArrayOutputStream.write(doFinal, 0, doFinal.length);
            int i3 = i + 1;
            int i4 = i3;
            i2 = i3 * MAX_ENCRYPT_BLOCK;
            i = i4;
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return byteArray;
    }

    public static byte[] encryptByPrivateKey(byte[] bArr, String str) {
        byte[] doFinal;
        PrivateKey generatePrivate = KeyFactory.getInstance(KEY_ALGORITHM).generatePrivate(new PKCS8EncodedKeySpec(Base64Utils.decode(str)));
        Cipher instance = Cipher.getInstance(KEY_ALGORITHM_ANDROID);
        instance.init(1, generatePrivate);
        int length = bArr.length;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        int i2 = 0;
        while (length - i2 > 0) {
            if (length - i2 > MAX_ENCRYPT_BLOCK) {
                doFinal = instance.doFinal(bArr, i2, MAX_ENCRYPT_BLOCK);
            } else {
                doFinal = instance.doFinal(bArr, i2, length - i2);
            }
            byteArrayOutputStream.write(doFinal, 0, doFinal.length);
            int i3 = i + 1;
            int i4 = i3;
            i2 = i3 * MAX_ENCRYPT_BLOCK;
            i = i4;
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return byteArray;
    }

    public static String getPrivateKey(Map<String, Object> map) {
        return Base64Utils.encode(((Key) map.get(PRIVATE_KEY)).getEncoded());
    }

    public static String getPublicKey(Map<String, Object> map) {
        return Base64Utils.encode(((Key) map.get(PUBLIC_KEY)).getEncoded());
    }

    public static String encrypt(String str) {
        return Base64.encode(encryptByPublicKey(str.getBytes(), publicKey));
    }

    public static String decrypt(String str) {
        return new String(decryptByPublicKey(Base64.decode(str), publicKey));
    }
}
