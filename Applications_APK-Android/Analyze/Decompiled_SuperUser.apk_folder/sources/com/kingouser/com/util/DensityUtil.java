package com.kingouser.com.util;

import android.content.Context;

public class DensityUtil {
    public static int dip2px(Context context, float f2) {
        return (int) ((context.getResources().getDisplayMetrics().density * f2) + 0.5f);
    }

    public static int px2dip(Context context, float f2) {
        return (int) ((f2 / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static int dpToPx(Context context, int i) {
        return Math.round((context.getResources().getDisplayMetrics().xdpi / 160.0f) * ((float) i));
    }

    public static int pxToDp(Context context, int i) {
        return Math.round(((float) i) / (context.getResources().getDisplayMetrics().xdpi / 160.0f));
    }

    public static int px2sp(Context context, float f2) {
        return (int) ((f2 / context.getResources().getDisplayMetrics().scaledDensity) + 0.5f);
    }

    public static int sp2px(Context context, float f2) {
        return (int) ((context.getResources().getDisplayMetrics().scaledDensity * f2) + 0.5f);
    }
}
