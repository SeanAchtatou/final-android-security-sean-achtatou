package com.kingouser.com.util;

import android.content.Context;
import android.content.res.Configuration;
import java.util.Locale;

public class LanguageUtils {
    public static String getLocalLanguage() {
        return Locale.getDefault().getLanguage();
    }

    public static String getLocalCountry() {
        return Locale.getDefault().getCountry();
    }

    public static String getLocalDefault() {
        return Locale.getDefault().toString();
    }

    public static void changeLocalLanguage(Context context, String str) {
        Locale locale = new Locale(str);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        try {
            context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
