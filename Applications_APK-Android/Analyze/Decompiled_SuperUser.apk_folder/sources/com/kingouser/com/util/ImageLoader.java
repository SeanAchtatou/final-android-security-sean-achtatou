package com.kingouser.com.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader {
    private ExecutorService executorService;
    private FileCache fileCache;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap());
    /* access modifiers changed from: private */
    public MemoryCache memoryCache = new MemoryCache();

    public ImageLoader(Context context) {
        this.fileCache = new FileCache(context);
        this.executorService = Executors.newFixedThreadPool(5);
    }

    public void DisplayImage(String str, ImageView imageView, boolean z) {
        this.imageViews.put(imageView, str);
        Bitmap bitmap = this.memoryCache.get(str.substring(str.lastIndexOf("/") + 1, str.length()));
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else if (!z) {
            queuePhoto(str, imageView);
        }
    }

    private void queuePhoto(String str, ImageView imageView) {
        this.executorService.submit(new PhotosLoader(new PhotoToLoad(str, imageView)));
    }

    public Bitmap getBitmap(String str) {
        String substring = str.substring(str.lastIndexOf("/") + 1, str.length());
        File file = this.fileCache.getFile(substring);
        if (file == null || !file.exists()) {
            File file2 = new File(this.fileCache.getCacheDirFile(), substring);
            try {
                file2.createNewFile();
                file = file2;
            } catch (IOException e2) {
                e2.printStackTrace();
                file = file2;
            }
        } else {
            Bitmap decodeFile = decodeFile(file);
            if (decodeFile != null) {
                return decodeFile;
            }
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(5000);
            InputStream inputStream = httpURLConnection.getInputStream();
            if (httpURLConnection.getResponseCode() != 200) {
                return null;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            CopyStream(inputStream, fileOutputStream);
            fileOutputStream.close();
            return decodeFile(file);
        } catch (Exception e3) {
            return null;
        }
    }

    private Bitmap decodeFile(File file) {
        int i = 1;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(file), null, options);
            int i2 = options.outWidth;
            int i3 = options.outHeight;
            while (i2 / 2 >= 100 && i3 / 2 >= 100) {
                i2 /= 2;
                i3 /= 2;
                i *= 2;
            }
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inSampleSize = i;
            return BitmapFactory.decodeStream(new FileInputStream(file), null, options2);
        } catch (FileNotFoundException e2) {
            return null;
        }
    }

    private class PhotoToLoad {
        public ImageView imageView;
        public String url;

        public PhotoToLoad(String str, ImageView imageView2) {
            this.url = str;
            this.imageView = imageView2;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad2) {
            this.photoToLoad = photoToLoad2;
        }

        public void run() {
            if (!ImageLoader.this.imageViewReused(this.photoToLoad)) {
                String str = this.photoToLoad.url;
                Bitmap bitmap = ImageLoader.this.getBitmap(str);
                ImageLoader.this.memoryCache.put(str.substring(str.lastIndexOf("/") + 1, str.length()), bitmap);
                if (!ImageLoader.this.imageViewReused(this.photoToLoad)) {
                    ((Activity) this.photoToLoad.imageView.getContext()).runOnUiThread(new BitmapDisplayer(bitmap, this.photoToLoad));
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean imageViewReused(PhotoToLoad photoToLoad) {
        String str = this.imageViews.get(photoToLoad.imageView);
        if (str == null || !str.equals(photoToLoad.url)) {
            return true;
        }
        return false;
    }

    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap bitmap2, PhotoToLoad photoToLoad2) {
            this.bitmap = bitmap2;
            this.photoToLoad = photoToLoad2;
        }

        public void run() {
            if (!ImageLoader.this.imageViewReused(this.photoToLoad) && this.bitmap != null) {
                this.photoToLoad.imageView.setImageBitmap(this.bitmap);
            }
        }
    }

    public void clearCache() {
        this.memoryCache.clear();
        this.fileCache.clear();
    }

    public static void CopyStream(InputStream inputStream, OutputStream outputStream) {
        try {
            byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
            while (true) {
                int read = inputStream.read(bArr, 0, FileUtils.FileMode.MODE_ISGID);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        } catch (Exception e2) {
        }
    }
}
