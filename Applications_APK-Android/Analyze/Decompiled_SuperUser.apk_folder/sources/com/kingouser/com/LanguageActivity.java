package com.kingouser.com;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import butterknife.BindView;
import com.kingouser.com.adapter.LanguageAdapter;
import com.kingouser.com.entity.LanguageEntity;
import com.kingouser.com.util.LanguageUtils;
import com.pureapps.cleaner.analytic.a;
import com.pureapps.cleaner.util.f;
import java.util.ArrayList;
import me.everything.android.ui.overscroll.g;

public class LanguageActivity extends BaseActivity {
    @BindView(R.id.iz)
    ListView lvLanguage;
    private LanguageActivity n;
    private ArrayList<LanguageEntity> p = new ArrayList<>();
    private LanguageAdapter q;
    private String r;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.bj);
        this.n = this;
        m();
        k();
        j();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a.a(this).b(this.o, "Language");
    }

    private void j() {
        this.lvLanguage.setAdapter((ListAdapter) this.q);
    }

    private void k() {
        this.p.clear();
        this.p = l();
        this.q = new LanguageAdapter(this, this.p);
        this.r = LanguageUtils.getLocalLanguage();
    }

    private ArrayList<LanguageEntity> l() {
        ArrayList<LanguageEntity> arrayList = new ArrayList<>();
        arrayList.add(a((int) R.string.cl, "en"));
        arrayList.add(a((int) R.string.cm, "de"));
        arrayList.add(a((int) R.string.ck, "es"));
        arrayList.add(a((int) R.string.cj, "fr"));
        arrayList.add(a((int) R.string.co, "it"));
        arrayList.add(a((int) R.string.cq, "ru"));
        arrayList.add(a((int) R.string.cr, "tr"));
        arrayList.add(a((int) R.string.cp, "pt"));
        arrayList.add(a((int) R.string.cn, "in"));
        arrayList.add(a((int) R.string.cs, "vi"));
        return arrayList;
    }

    private LanguageEntity a(int i, String str) {
        LanguageEntity languageEntity = new LanguageEntity();
        languageEntity.setLanguage(this.n.getResources().getString(i));
        languageEntity.setLanguageCode(str);
        return languageEntity;
    }

    private void m() {
        g.a(this.lvLanguage);
        ActionBar f2 = f();
        f.a("supportActionBar is null " + (f2 == null));
        if (f2 != null) {
            f2.d(true);
            f2.a(true);
        }
    }

    public static void a(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, LanguageActivity.class);
        intent.setFlags(268435456);
        context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
