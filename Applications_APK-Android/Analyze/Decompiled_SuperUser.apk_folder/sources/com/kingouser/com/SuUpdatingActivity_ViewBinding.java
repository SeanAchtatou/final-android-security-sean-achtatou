package com.kingouser.com;

import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SuUpdatingActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private SuUpdatingActivity f4134a;

    public SuUpdatingActivity_ViewBinding(SuUpdatingActivity suUpdatingActivity, View view) {
        this.f4134a = suUpdatingActivity;
        suUpdatingActivity.ivLoading = (ImageView) Utils.findRequiredViewAsType(view, R.id.m4, "field 'ivLoading'", ImageView.class);
    }

    public void unbind() {
        SuUpdatingActivity suUpdatingActivity = this.f4134a;
        if (suUpdatingActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4134a = null;
        suUpdatingActivity.ivLoading = null;
    }
}
