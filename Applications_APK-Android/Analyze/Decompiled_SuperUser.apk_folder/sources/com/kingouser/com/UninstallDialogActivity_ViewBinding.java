package com.kingouser.com;

import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class UninstallDialogActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private UninstallDialogActivity f4141a;

    /* renamed from: b  reason: collision with root package name */
    private View f4142b;

    /* renamed from: c  reason: collision with root package name */
    private View f4143c;

    public UninstallDialogActivity_ViewBinding(final UninstallDialogActivity uninstallDialogActivity, View view) {
        this.f4141a = uninstallDialogActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.fo, "field 'ivNo' and method 'OnClick'");
        uninstallDialogActivity.ivNo = (ImageView) Utils.castView(findRequiredView, R.id.fo, "field 'ivNo'", ImageView.class);
        this.f4142b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                uninstallDialogActivity.OnClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.fp, "field 'ivYes' and method 'OnClick'");
        uninstallDialogActivity.ivYes = (ImageView) Utils.castView(findRequiredView2, R.id.fp, "field 'ivYes'", ImageView.class);
        this.f4143c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                uninstallDialogActivity.OnClick(view);
            }
        });
    }

    public void unbind() {
        UninstallDialogActivity uninstallDialogActivity = this.f4141a;
        if (uninstallDialogActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4141a = null;
        uninstallDialogActivity.ivNo = null;
        uninstallDialogActivity.ivYes = null;
        this.f4142b.setOnClickListener(null);
        this.f4142b = null;
        this.f4143c.setOnClickListener(null);
        this.f4143c = null;
    }
}
