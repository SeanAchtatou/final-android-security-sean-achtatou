package com.kingouser.com;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.pureapps.cleaner.view.ShadowLayout;

public class StripeActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private StripeActivity f4118a;

    /* renamed from: b  reason: collision with root package name */
    private View f4119b;

    public StripeActivity_ViewBinding(final StripeActivity stripeActivity, View view) {
        this.f4118a = stripeActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.er, "field 'mSubmitButton' and method 'onClick'");
        stripeActivity.mSubmitButton = (ShadowLayout) Utils.castView(findRequiredView, R.id.er, "field 'mSubmitButton'", ShadowLayout.class);
        this.f4119b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                stripeActivity.onClick(view);
            }
        });
        stripeActivity.mStripeCardNum = (EditText) Utils.findRequiredViewAsType(view, R.id.eo, "field 'mStripeCardNum'", EditText.class);
        stripeActivity.mStripeCardMonthAndYears = (EditText) Utils.findRequiredViewAsType(view, R.id.ep, "field 'mStripeCardMonthAndYears'", EditText.class);
        stripeActivity.mStripeCardCvc = (EditText) Utils.findRequiredViewAsType(view, R.id.eq, "field 'mStripeCardCvc'", EditText.class);
        stripeActivity.mProgressBar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.em, "field 'mProgressBar'", ProgressBar.class);
        stripeActivity.mRootLinearLayout = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.en, "field 'mRootLinearLayout'", LinearLayout.class);
    }

    public void unbind() {
        StripeActivity stripeActivity = this.f4118a;
        if (stripeActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4118a = null;
        stripeActivity.mSubmitButton = null;
        stripeActivity.mStripeCardNum = null;
        stripeActivity.mStripeCardMonthAndYears = null;
        stripeActivity.mStripeCardCvc = null;
        stripeActivity.mProgressBar = null;
        stripeActivity.mRootLinearLayout = null;
        this.f4119b.setOnClickListener(null);
        this.f4119b = null;
    }
}
