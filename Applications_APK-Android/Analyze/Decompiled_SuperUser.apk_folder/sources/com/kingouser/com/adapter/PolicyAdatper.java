package com.kingouser.com.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.R;
import com.kingouser.com.customview.a;
import com.kingouser.com.db.KingoDatabaseHelper;
import com.kingouser.com.entity.UidPolicy;
import com.kingouser.com.util.DeviceInfoUtils;
import com.kingouser.com.util.FileUtils;
import com.kingouser.com.util.MyLog;
import com.kingouser.com.util.MySharedPreference;
import com.kingouser.com.util.PermissionUtils;
import com.pureapps.cleaner.util.f;
import java.io.File;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.TreeSet;

public class PolicyAdatper extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f4176a = "PolicyAdatper";

    /* renamed from: b  reason: collision with root package name */
    private int f4177b = 0;

    /* renamed from: c  reason: collision with root package name */
    private int f4178c = 1;

    /* renamed from: d  reason: collision with root package name */
    private int f4179d = (this.f4178c + 1);

    /* renamed from: e  reason: collision with root package name */
    private View f4180e = null;

    /* renamed from: f  reason: collision with root package name */
    private int f4181f = -1;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public int f4182g = 300;

    /* renamed from: h  reason: collision with root package name */
    private BitSet f4183h = new BitSet();
    private final SparseIntArray i = new SparseIntArray(10);
    /* access modifiers changed from: private */
    public Context j;
    private LayoutInflater k;
    /* access modifiers changed from: private */
    public ArrayList<UidPolicy> l;
    private TreeSet<Integer> m;
    /* access modifiers changed from: private */
    public a n;
    private Animation o;
    /* access modifiers changed from: private */
    public long p;
    /* access modifiers changed from: private */
    public boolean q;

    public class HeadViewHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private HeadViewHolder f4218a;

        public HeadViewHolder_ViewBinding(HeadViewHolder headViewHolder, View view) {
            this.f4218a = headViewHolder;
            headViewHolder.label = (TextView) Utils.findRequiredViewAsType(view, R.id.iy, "field 'label'", TextView.class);
        }

        public void unbind() {
            HeadViewHolder headViewHolder = this.f4218a;
            if (headViewHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f4218a = null;
            headViewHolder.label = null;
        }
    }

    public class ContentViewHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private ContentViewHolder f4216a;

        public ContentViewHolder_ViewBinding(ContentViewHolder contentViewHolder, View view) {
            this.f4216a = contentViewHolder;
            contentViewHolder.cIcon = (ImageView) Utils.findRequiredViewAsType(view, R.id.bg, "field 'cIcon'", ImageView.class);
            contentViewHolder.ivState = (ImageView) Utils.findRequiredViewAsType(view, R.id.lf, "field 'ivState'", ImageView.class);
            contentViewHolder.cTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.bk, "field 'cTitle'", TextView.class);
            contentViewHolder.tvSecurity = (TextView) Utils.findRequiredViewAsType(view, R.id.lc, "field 'tvSecurity'", TextView.class);
            contentViewHolder.weatherAllow = (TextView) Utils.findRequiredViewAsType(view, R.id.le, "field 'weatherAllow'", TextView.class);
            contentViewHolder.tvDeny = (TextView) Utils.findRequiredViewAsType(view, R.id.li, "field 'tvDeny'", TextView.class);
            contentViewHolder.tvAsk = (TextView) Utils.findRequiredViewAsType(view, R.id.lk, "field 'tvAsk'", TextView.class);
            contentViewHolder.tvAllow = (TextView) Utils.findRequiredViewAsType(view, R.id.lm, "field 'tvAllow'", TextView.class);
            contentViewHolder.tvShadow = (TextView) Utils.findRequiredViewAsType(view, R.id.ld, "field 'tvShadow'", TextView.class);
            contentViewHolder.linearLayout = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.lg, "field 'linearLayout'", LinearLayout.class);
            contentViewHolder.lvRight = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.lb, "field 'lvRight'", LinearLayout.class);
            contentViewHolder.lvDeny = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.lh, "field 'lvDeny'", LinearLayout.class);
            contentViewHolder.lvAsk = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.lj, "field 'lvAsk'", LinearLayout.class);
            contentViewHolder.lvAllow = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.ll, "field 'lvAllow'", LinearLayout.class);
            contentViewHolder.lvAd = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.e_, "field 'lvAd'", LinearLayout.class);
            contentViewHolder.adLine = Utils.findRequiredView(view, R.id.ln, "field 'adLine'");
        }

        public void unbind() {
            ContentViewHolder contentViewHolder = this.f4216a;
            if (contentViewHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f4216a = null;
            contentViewHolder.cIcon = null;
            contentViewHolder.ivState = null;
            contentViewHolder.cTitle = null;
            contentViewHolder.tvSecurity = null;
            contentViewHolder.weatherAllow = null;
            contentViewHolder.tvDeny = null;
            contentViewHolder.tvAsk = null;
            contentViewHolder.tvAllow = null;
            contentViewHolder.tvShadow = null;
            contentViewHolder.linearLayout = null;
            contentViewHolder.lvRight = null;
            contentViewHolder.lvDeny = null;
            contentViewHolder.lvAsk = null;
            contentViewHolder.lvAllow = null;
            contentViewHolder.lvAd = null;
            contentViewHolder.adLine = null;
        }
    }

    public PolicyAdatper(Context context, ArrayList<UidPolicy> arrayList, TreeSet<Integer> treeSet) {
        this.j = context;
        this.l = arrayList;
        this.m = treeSet;
        this.o = AnimationUtils.loadAnimation(context, R.anim.w);
        this.k = LayoutInflater.from(context);
    }

    public int getItemViewType(int i2) {
        return this.m.contains(Integer.valueOf(i2)) ? this.f4178c : this.f4177b;
    }

    public int getViewTypeCount() {
        return this.f4179d;
    }

    public int getCount() {
        return this.l.size();
    }

    public Object getItem(int i2) {
        return this.l.get(i2);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private void a(ViewGroup viewGroup, View view, ContentViewHolder contentViewHolder, int i2) {
        UidPolicy uidPolicy = this.l.get(i2);
        MyLog.e(this.f4176a, "执行了PolicyAdapter的getView()方法。。。。。。。。。。。。。。。。。。。。。。。。。。。");
        a(view, contentViewHolder.lvRight, contentViewHolder.lvAllow, contentViewHolder.lvAsk, contentViewHolder.lvDeny, contentViewHolder.linearLayout, contentViewHolder.f4214a, i2, uidPolicy, viewGroup, contentViewHolder.ivState);
        contentViewHolder.ivState.setTag(Integer.valueOf(i2));
        View fbNativeAdView = uidPolicy.getFbNativeAdView();
        if (fbNativeAdView != null) {
            contentViewHolder.lvAd.setVisibility(0);
            contentViewHolder.lvAd.removeAllViews();
            try {
                contentViewHolder.lvAd.addView(fbNativeAdView, 0);
                contentViewHolder.lvAd.setBackgroundColor(0);
            } catch (Exception e2) {
                MyLog.e("PermissionService", "异常是。。。。。。。。。。。。。" + e2.toString());
            }
            contentViewHolder.adLine.setVisibility(0);
        } else {
            contentViewHolder.lvAd.setVisibility(8);
            contentViewHolder.lvAd.removeAllViews();
            contentViewHolder.adLine.setVisibility(8);
        }
        String str = uidPolicy.name;
        if (!TextUtils.isEmpty(str)) {
            if (2000 == uidPolicy.uid) {
                str = this.j.getResources().getString(R.string.bp);
            }
        } else if (2000 == uidPolicy.uid) {
            str = this.j.getResources().getString(R.string.bp);
        } else {
            str = this.j.getResources().getString(R.string.bq, Integer.valueOf(uidPolicy.uid));
        }
        contentViewHolder.cTitle.setText(str);
        uidPolicy.nameText = str;
        if (2000 == uidPolicy.uid) {
            contentViewHolder.cIcon.setImageResource(R.drawable.et);
        } else if (uidPolicy.drawable != null) {
            contentViewHolder.cIcon.setImageDrawable(uidPolicy.drawable);
        } else {
            contentViewHolder.cIcon.setImageResource(uidPolicy.Icon);
        }
        if (-1 == this.f4181f || i2 != this.f4181f) {
            contentViewHolder.linearLayout.setVisibility(8);
        } else {
            contentViewHolder.linearLayout.setVisibility(0);
        }
        if (uidPolicy.until != 0) {
            contentViewHolder.weatherAllow.setText((int) R.string.b2);
            contentViewHolder.weatherAllow.setTextColor(this.j.getResources().getColor(R.color.e4));
            contentViewHolder.weatherAllow.setText((int) R.string.b2);
            contentViewHolder.weatherAllow.setTextColor(this.j.getResources().getColor(R.color.e4));
            uidPolicy.textId = R.string.b2;
        } else if (uidPolicy.allow) {
            contentViewHolder.weatherAllow.setText((int) R.string.al);
            contentViewHolder.weatherAllow.setTextColor(this.j.getResources().getColor(R.color.y));
            contentViewHolder.weatherAllow.setText((int) R.string.al);
            contentViewHolder.weatherAllow.setTextColor(this.j.getResources().getColor(R.color.y));
            uidPolicy.textId = R.string.al;
        } else {
            contentViewHolder.weatherAllow.setText((int) R.string.br);
            contentViewHolder.weatherAllow.setTextColor(this.j.getResources().getColor(R.color.cu));
            contentViewHolder.weatherAllow.setText((int) R.string.br);
            contentViewHolder.weatherAllow.setTextColor(this.j.getResources().getColor(R.color.cu));
            uidPolicy.textId = R.string.br;
        }
        contentViewHolder.ivState.setAnimation(null);
        if (i2 == this.f4181f) {
            contentViewHolder.ivState.setImageResource(R.drawable.ew);
        } else {
            contentViewHolder.ivState.setImageResource(R.drawable.fb);
        }
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        HeadViewHolder headViewHolder;
        ContentViewHolder contentViewHolder;
        if (getItemViewType(i2) == this.f4177b) {
            if (view == null) {
                view = this.k.inflate((int) R.layout.cg, (ViewGroup) null);
                contentViewHolder = new ContentViewHolder(view);
                view.setTag(contentViewHolder);
            } else {
                contentViewHolder = (ContentViewHolder) view.getTag();
            }
            a(viewGroup, view, contentViewHolder, i2);
        } else {
            if (view == null) {
                view = this.k.inflate((int) R.layout.bi, (ViewGroup) null);
                HeadViewHolder headViewHolder2 = new HeadViewHolder(view);
                view.setTag(headViewHolder2);
                headViewHolder = headViewHolder2;
            } else {
                headViewHolder = (HeadViewHolder) view.getTag();
            }
            headViewHolder.label.setText(this.l.get(i2).label);
        }
        return view;
    }

    /* access modifiers changed from: private */
    public int a() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    class HeadViewHolder {
        @BindView(R.id.iy)
        TextView label;

        public HeadViewHolder(View view) {
            ButterKnife.bind(this, view);
            this.label.setTextSize(DeviceInfoUtils.getTextSize(PolicyAdatper.this.j, 36));
        }
    }

    class ContentViewHolder {

        /* renamed from: a  reason: collision with root package name */
        View f4214a;
        @BindView(R.id.ln)
        View adLine;
        @BindView(R.id.bg)
        ImageView cIcon;
        @BindView(R.id.bk)
        TextView cTitle;
        @BindView(R.id.lf)
        ImageView ivState;
        @BindView(R.id.lg)
        LinearLayout linearLayout;
        @BindView(R.id.e_)
        LinearLayout lvAd;
        @BindView(R.id.ll)
        LinearLayout lvAllow;
        @BindView(R.id.lj)
        LinearLayout lvAsk;
        @BindView(R.id.lh)
        LinearLayout lvDeny;
        @BindView(R.id.lb)
        LinearLayout lvRight;
        @BindView(R.id.lm)
        TextView tvAllow;
        @BindView(R.id.lk)
        TextView tvAsk;
        @BindView(R.id.li)
        TextView tvDeny;
        @BindView(R.id.lc)
        TextView tvSecurity;
        @BindView(R.id.ld)
        TextView tvShadow;
        @BindView(R.id.le)
        TextView weatherAllow;

        public ContentViewHolder(View view) {
            ButterKnife.bind(this, view);
            this.cTitle.setTextSize(DeviceInfoUtils.getTextSize(PolicyAdatper.this.j, 46));
            this.tvSecurity.setTextSize(DeviceInfoUtils.getTextSize(PolicyAdatper.this.j, 36));
            this.weatherAllow.setTextSize(DeviceInfoUtils.getTextSize(PolicyAdatper.this.j, 30));
            this.tvShadow.setTextSize(DeviceInfoUtils.getTextSize(PolicyAdatper.this.j, 30));
            this.tvAllow.setTextSize(DeviceInfoUtils.getTextSize(PolicyAdatper.this.j, 30));
            this.tvAsk.setTextSize(DeviceInfoUtils.getTextSize(PolicyAdatper.this.j, 30));
            this.tvDeny.setTextSize(DeviceInfoUtils.getTextSize(PolicyAdatper.this.j, 30));
            this.weatherAllow.setTextSize(DeviceInfoUtils.getTextSize(PolicyAdatper.this.j, 30));
        }
    }

    private void a(View view, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, LinearLayout linearLayout4, LinearLayout linearLayout5, View view2, int i2, UidPolicy uidPolicy, ViewGroup viewGroup, ImageView imageView) {
        linearLayout5.measure(view.getWidth(), view.getHeight());
        a(view, linearLayout, linearLayout2, linearLayout3, linearLayout4, linearLayout5, i2, uidPolicy, viewGroup, imageView);
        linearLayout5.requestLayout();
    }

    private void a(View view, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, LinearLayout linearLayout4, View view2, int i2, UidPolicy uidPolicy, ViewGroup viewGroup, ImageView imageView) {
        if (view2 == this.f4180e && i2 != this.f4181f) {
            this.f4180e = null;
        }
        if (i2 == this.f4181f) {
            this.f4180e = view2;
        }
        if (this.i.get(i2, -1) == -1) {
            this.i.put(i2, view2.getMeasuredHeight());
            a(view2, i2);
        } else {
            a(view2, i2);
        }
        final View view3 = view;
        final View view4 = view2;
        final int i3 = i2;
        final ImageView imageView2 = imageView;
        linearLayout.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, boolean):boolean
             arg types: [com.kingouser.com.adapter.PolicyAdatper, int]
             candidates:
              com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, long):long
              com.kingouser.com.adapter.PolicyAdatper.a(android.view.View, int):void
              com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, boolean):boolean */
            public void onClick(View view) {
                if (!PolicyAdatper.this.q) {
                    boolean unused = PolicyAdatper.this.q = true;
                    long unused2 = PolicyAdatper.this.p = System.currentTimeMillis();
                    PolicyAdatper.this.a(view3, view4, i3, imageView2);
                    com.pureapps.cleaner.analytic.a.a(PolicyAdatper.this.j).a(FirebaseAnalytics.getInstance(PolicyAdatper.this.j), "BtnPolicySelectClick", ((UidPolicy) PolicyAdatper.this.l.get(i3)).packageName);
                }
            }
        });
        final UidPolicy uidPolicy2 = uidPolicy;
        final ViewGroup viewGroup2 = viewGroup;
        final View view5 = view;
        final View view6 = view2;
        final int i4 = i2;
        final ImageView imageView3 = imageView;
        linearLayout2.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, boolean):boolean
             arg types: [com.kingouser.com.adapter.PolicyAdatper, int]
             candidates:
              com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, long):long
              com.kingouser.com.adapter.PolicyAdatper.a(android.view.View, int):void
              com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, boolean):boolean */
            public void onClick(View view) {
                if (!PolicyAdatper.this.q) {
                    boolean unused = PolicyAdatper.this.q = true;
                    f.a("textId:" + uidPolicy2.textId + " | deny:" + ((int) R.string.al));
                    if (R.string.al != uidPolicy2.textId) {
                        if (!new File(PolicyAdatper.this.j.getFilesDir() + "/supersu.cfg").exists()) {
                            PermissionUtils.createPrePermission(PolicyAdatper.this.j, MySharedPreference.getRequestDialogTimes(PolicyAdatper.this.j, 15));
                        }
                        if (FileUtils.checkFileExist(PolicyAdatper.this.j, PolicyAdatper.this.j.getFilesDir() + "/" + "supersu.cfg")) {
                            PermissionUtils.addApp2Cfg(PolicyAdatper.this.j, uidPolicy2.packageName, uidPolicy2.uid % io.fabric.sdk.android.services.common.a.DEFAULT_TIMEOUT, 1);
                        } else {
                            PermissionUtils.createPrePermission(PolicyAdatper.this.j, MySharedPreference.getRequestDialogTimes(PolicyAdatper.this.j, 15));
                            PermissionUtils.addApp2Cfg(PolicyAdatper.this.j, uidPolicy2.packageName, uidPolicy2.uid % io.fabric.sdk.android.services.common.a.DEFAULT_TIMEOUT, 1);
                        }
                        Context a2 = PolicyAdatper.this.j;
                        UidPolicy uidPolicy = uidPolicy2;
                        KingoDatabaseHelper.a(uidPolicy2.uid + "", 0, a2, UidPolicy.ALLOW);
                    }
                    uidPolicy2.until = 0;
                    uidPolicy2.allow = true;
                    a unused2 = PolicyAdatper.this.n;
                    a.a(PolicyAdatper.this.j, viewGroup2, PolicyAdatper.this.j.getString(R.string.an, uidPolicy2.nameText));
                    PolicyAdatper.this.a(view5, view6, i4, imageView3);
                    com.pureapps.cleaner.analytic.a.a(PolicyAdatper.this.j).a(FirebaseAnalytics.getInstance(PolicyAdatper.this.j), ((UidPolicy) PolicyAdatper.this.l.get(i4)).packageName, "Allow");
                }
            }
        });
        final UidPolicy uidPolicy3 = uidPolicy;
        final ViewGroup viewGroup3 = viewGroup;
        final View view7 = view;
        final View view8 = view2;
        final int i5 = i2;
        final ImageView imageView4 = imageView;
        linearLayout3.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, boolean):boolean
             arg types: [com.kingouser.com.adapter.PolicyAdatper, int]
             candidates:
              com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, long):long
              com.kingouser.com.adapter.PolicyAdatper.a(android.view.View, int):void
              com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, boolean):boolean */
            public void onClick(View view) {
                if (!PolicyAdatper.this.q) {
                    boolean unused = PolicyAdatper.this.q = true;
                    f.a("textId:" + uidPolicy3.textId + " | ask:" + ((int) R.string.b2));
                    if (R.string.b2 != uidPolicy3.textId) {
                        if (FileUtils.checkFileExist(PolicyAdatper.this.j, PolicyAdatper.this.j.getFilesDir() + "/" + "supersu.cfg")) {
                            PermissionUtils.RemoveAppFromCfg(PolicyAdatper.this.j, uidPolicy3.packageName);
                        } else {
                            PermissionUtils.createPrePermission(PolicyAdatper.this.j, MySharedPreference.getRequestDialogTimes(PolicyAdatper.this.j, 15));
                        }
                        int e2 = PolicyAdatper.this.a();
                        Context a2 = PolicyAdatper.this.j;
                        UidPolicy uidPolicy = uidPolicy3;
                        KingoDatabaseHelper.a(uidPolicy3.uid + "", e2, a2, UidPolicy.DENY);
                    }
                    uidPolicy3.until = PolicyAdatper.this.a();
                    a unused2 = PolicyAdatper.this.n;
                    a.a(PolicyAdatper.this.j, viewGroup3, PolicyAdatper.this.j.getString(R.string.b4, uidPolicy3.nameText));
                    PolicyAdatper.this.a(view7, view8, i5, imageView4);
                    PermissionUtils.RemoveAppFromCfg(PolicyAdatper.this.j, ((UidPolicy) PolicyAdatper.this.l.get(i5)).packageName);
                    com.pureapps.cleaner.analytic.a.a(PolicyAdatper.this.j).a(FirebaseAnalytics.getInstance(PolicyAdatper.this.j), ((UidPolicy) PolicyAdatper.this.l.get(i5)).packageName, "Ask");
                }
            }
        });
        final UidPolicy uidPolicy4 = uidPolicy;
        final View view9 = view;
        final View view10 = view2;
        final int i6 = i2;
        final ImageView imageView5 = imageView;
        final ViewGroup viewGroup4 = viewGroup;
        linearLayout4.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, boolean):boolean
             arg types: [com.kingouser.com.adapter.PolicyAdatper, int]
             candidates:
              com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, long):long
              com.kingouser.com.adapter.PolicyAdatper.a(android.view.View, int):void
              com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, boolean):boolean */
            public void onClick(View view) {
                if (!PolicyAdatper.this.q) {
                    boolean unused = PolicyAdatper.this.q = true;
                    f.a("textId:" + uidPolicy4.textId + " | deny:" + ((int) R.string.b2));
                    if (R.string.br != uidPolicy4.textId) {
                        if (FileUtils.checkFileExist(PolicyAdatper.this.j, PolicyAdatper.this.j.getFilesDir() + "/" + "supersu.cfg")) {
                            PermissionUtils.RemoveAppFromCfg(PolicyAdatper.this.j, uidPolicy4.packageName);
                        } else {
                            PermissionUtils.createPrePermission(PolicyAdatper.this.j, MySharedPreference.getRequestDialogTimes(PolicyAdatper.this.j, 15));
                        }
                        Context a2 = PolicyAdatper.this.j;
                        UidPolicy uidPolicy = uidPolicy4;
                        KingoDatabaseHelper.a(uidPolicy4.uid + "", 0, a2, UidPolicy.DENY);
                    }
                    uidPolicy4.until = 0;
                    uidPolicy4.allow = false;
                    PolicyAdatper.this.a(view9, view10, i6, imageView5);
                    a unused2 = PolicyAdatper.this.n;
                    a.a(PolicyAdatper.this.j, viewGroup4, PolicyAdatper.this.j.getString(R.string.bt, uidPolicy4.nameText));
                    com.pureapps.cleaner.analytic.a.a(PolicyAdatper.this.j).a(FirebaseAnalytics.getInstance(PolicyAdatper.this.j), ((UidPolicy) PolicyAdatper.this.l.get(i6)).packageName, "Deny");
                }
            }
        });
    }

    private void a(View view, int i2) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        if (this.f4183h.get(i2)) {
            view.setVisibility(0);
            layoutParams.bottomMargin = 0;
            return;
        }
        view.setVisibility(8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    /* access modifiers changed from: private */
    public void a(View view, View view2, int i2, ImageView imageView) {
        int i3 = view2.getVisibility() == 0 ? 1 : 0;
        if (i3 == 0) {
            this.f4183h.set(i2, true);
        } else {
            this.f4183h.set(i2, false);
        }
        if (i3 == 0) {
            if (!(this.f4181f == -1 || this.f4181f == i2)) {
                if (this.f4180e != null) {
                    a(view, this.f4180e, 1, this.f4181f, imageView);
                }
                this.f4183h.set(this.f4181f, false);
            }
            this.f4180e = view2;
            this.f4181f = i2;
        } else if (this.f4181f == i2) {
            this.f4181f = -1;
        }
        a(view, view2, i3, i2, imageView);
    }

    private void a(final View view, final View view2, final int i2, int i3, ImageView imageView) {
        com.kingouser.com.a.a aVar = new com.kingouser.com.a.a(view2, i2);
        aVar.setDuration((long) this.f4182g);
        aVar.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, boolean):boolean
             arg types: [com.kingouser.com.adapter.PolicyAdatper, int]
             candidates:
              com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, long):long
              com.kingouser.com.adapter.PolicyAdatper.a(android.view.View, int):void
              com.kingouser.com.adapter.PolicyAdatper.a(com.kingouser.com.adapter.PolicyAdatper, boolean):boolean */
            public void onAnimationEnd(Animation animation) {
                if (i2 == 0 && (view instanceof ListView)) {
                    ListView listView = (ListView) view;
                    int bottom = view2.getBottom();
                    Rect rect = new Rect();
                    boolean globalVisibleRect = view2.getGlobalVisibleRect(rect);
                    Rect rect2 = new Rect();
                    listView.getGlobalVisibleRect(rect2);
                    if (!globalVisibleRect) {
                        listView.smoothScrollBy(bottom, PolicyAdatper.this.f4182g);
                    } else if (rect2.bottom == rect.bottom) {
                        listView.smoothScrollBy(bottom, PolicyAdatper.this.f4182g);
                    }
                }
                MyLog.e(PolicyAdatper.this.f4176a, "动画结束。。。。。。。。。。。。。。。。。。。。。。。。。" + (System.currentTimeMillis() - PolicyAdatper.this.p));
                PolicyAdatper.this.notifyDataSetChanged();
                boolean unused = PolicyAdatper.this.q = false;
            }
        });
        view2.startAnimation(aVar);
        if (((Integer) imageView.getTag()).intValue() == i3) {
            imageView.startAnimation(this.o);
        } else {
            imageView.clearAnimation();
        }
    }
}
