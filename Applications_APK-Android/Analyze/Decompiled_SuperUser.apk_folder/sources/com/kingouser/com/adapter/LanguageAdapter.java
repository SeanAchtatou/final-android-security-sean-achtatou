package com.kingouser.com.adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kingouser.com.LanguageActivity;
import com.kingouser.com.R;
import com.kingouser.com.customview.MyDrawbleText;
import com.kingouser.com.entity.LanguageEntity;
import com.kingouser.com.util.LanguageUtils;
import com.pureapps.cleaner.analytic.a;
import java.util.ArrayList;

public class LanguageAdapter extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public LanguageActivity f4167a;

    /* renamed from: b  reason: collision with root package name */
    private ArrayList<LanguageEntity> f4168b;

    /* renamed from: c  reason: collision with root package name */
    private LayoutInflater f4169c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public int f4170d;

    public class ContentViewHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private ContentViewHolder f4175a;

        public ContentViewHolder_ViewBinding(ContentViewHolder contentViewHolder, View view) {
            this.f4175a = contentViewHolder;
            contentViewHolder.tvLanguage = (MyDrawbleText) Utils.findRequiredViewAsType(view, R.id.j0, "field 'tvLanguage'", MyDrawbleText.class);
        }

        public void unbind() {
            ContentViewHolder contentViewHolder = this.f4175a;
            if (contentViewHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f4175a = null;
            contentViewHolder.tvLanguage = null;
        }
    }

    public LanguageAdapter(LanguageActivity languageActivity, ArrayList<LanguageEntity> arrayList) {
        this.f4167a = languageActivity;
        this.f4168b = arrayList;
        this.f4169c = LayoutInflater.from(languageActivity);
        String localLanguage = LanguageUtils.getLocalLanguage();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                if (localLanguage.equalsIgnoreCase(arrayList.get(i2).getLanguageCode())) {
                    this.f4170d = i2;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public int getCount() {
        return this.f4168b.size();
    }

    public Object getItem(int i) {
        return this.f4168b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(final int i, View view, ViewGroup viewGroup) {
        ContentViewHolder contentViewHolder;
        if (view == null) {
            view = this.f4169c.inflate((int) R.layout.bk, (ViewGroup) null);
            ContentViewHolder contentViewHolder2 = new ContentViewHolder(view);
            view.setTag(contentViewHolder2);
            contentViewHolder = contentViewHolder2;
        } else {
            contentViewHolder = (ContentViewHolder) view.getTag();
        }
        final LanguageEntity languageEntity = this.f4168b.get(i);
        contentViewHolder.tvLanguage.setText(languageEntity.getLanguage());
        if (this.f4170d == i) {
            contentViewHolder.tvLanguage.setRightDrawbleId(R.drawable.f2);
        } else {
            contentViewHolder.tvLanguage.setRightDrawbleId(R.drawable.f4);
        }
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int unused = LanguageAdapter.this.f4170d = i;
                LanguageAdapter.this.notifyDataSetChanged();
                String languageCode = languageEntity.getLanguageCode();
                LanguageUtils.changeLocalLanguage(LanguageAdapter.this.f4167a, languageCode);
                a.a(LanguageAdapter.this.f4167a).e(LanguageAdapter.this.f4167a.o, languageCode);
            }
        });
        return view;
    }

    class ContentViewHolder {
        @BindView(R.id.j0)
        MyDrawbleText tvLanguage;

        public ContentViewHolder(View view) {
            ButterKnife.bind(this, view);
            this.tvLanguage.setBgWidth((int) LanguageAdapter.this.f4167a.getResources().getDimension(R.dimen.f8284c));
            this.tvLanguage.setBgHeight((int) LanguageAdapter.this.f4167a.getResources().getDimension(R.dimen.r));
            this.tvLanguage.setDrawbleRightWidth((int) LanguageAdapter.this.f4167a.getResources().getDimension(R.dimen.o));
            this.tvLanguage.setDrawbleRightHeight((int) LanguageAdapter.this.f4167a.getResources().getDimension(R.dimen.o));
            this.tvLanguage.setDrawbleBottomWidth((int) LanguageAdapter.this.f4167a.getResources().getDimension(R.dimen.f8284c));
            this.tvLanguage.setDrawbleBottomHegith(1);
            this.tvLanguage.setRightMargin((int) LanguageAdapter.this.f4167a.getResources().getDimension(R.dimen.f8286e));
            this.tvLanguage.setTypeface(Typeface.createFromAsset(LanguageAdapter.this.f4167a.getAssets(), "fonts/Arial.ttf"));
        }
    }
}
