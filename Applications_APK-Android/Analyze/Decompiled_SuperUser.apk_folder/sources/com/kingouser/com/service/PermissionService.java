package com.kingouser.com.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import com.kingouser.com.RequestTranActivity;
import com.kingouser.com.application.App;
import com.kingouser.com.db.KingoDatabaseHelper;
import com.kingouser.com.db.b;
import com.kingouser.com.entity.IntentEntity;
import com.kingouser.com.entity.UidPolicy;
import com.kingouser.com.util.DeviceInfoUtils;
import com.kingouser.com.util.FileUtils;
import com.kingouser.com.util.MyLog;
import com.kingouser.com.util.PermissionUtils;
import com.kingouser.com.util.ShellUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PermissionService extends Service {

    /* renamed from: a  reason: collision with root package name */
    String f4460a = "PermissionService";
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Context f4461b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public boolean f4462c = true;

    /* renamed from: d  reason: collision with root package name */
    private Lock f4463d = new ReentrantLock();

    /* renamed from: e  reason: collision with root package name */
    private a f4464e = new a();

    public void onCreate() {
        super.onCreate();
        this.f4461b = getApplicationContext();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        b((Context) this);
        a();
    }

    private synchronized void a() {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
             arg types: [java.lang.String, int]
             candidates:
              com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
            public void run() {
                IntentEntity intentEntity;
                while (!App.f4222d.isEmpty()) {
                    if (PermissionService.this.f4462c) {
                        boolean unused = PermissionService.this.f4462c = false;
                        FileUtils.createConfig(PermissionService.this.f4461b);
                        ShellUtils.execCommand("chmod " + DeviceInfoUtils.getChmodCode(PermissionService.this.f4461b) + " " + (PermissionService.this.f4461b.getFilesDir().getPath() + "/config"), true);
                        LinkedList<IntentEntity> linkedList = App.f4222d;
                        if (!(linkedList == null || linkedList.size() == 0)) {
                            try {
                                intentEntity = App.f4222d.getFirst();
                                if (intentEntity == null) {
                                }
                            } catch (Exception e2) {
                                intentEntity = null;
                            }
                            App.f4222d.removeFirst();
                            String packageName = intentEntity.getPackageName();
                            int suCode = intentEntity.getSuCode();
                            int suFromuid = intentEntity.getSuFromuid();
                            int suTouid = intentEntity.getSuTouid();
                            intentEntity.getRequestTime();
                            MyLog.e(PermissionService.this.f4460a, "0................，，。。。。。。。。。。。。。。。。。。。。。。。。。" + suFromuid);
                            if (packageName != null && App.f4223e.contains(packageName)) {
                                PermissionUtils.deny(PermissionService.this.f4461b, packageName, suCode);
                                boolean unused2 = PermissionService.this.f4462c = true;
                                MyLog.e("PermissionService", "申请权限的是" + packageName);
                            } else if (App.f4221c == 0 || App.f4221c != suFromuid) {
                                UidPolicy a2 = KingoDatabaseHelper.a(PermissionService.this.f4461b, packageName);
                                if (a2 == null || packageName == null) {
                                    MyLog.e(PermissionService.this.f4460a, "7................，，。。。。。。。。。。。。。。。。。。。。。。。。。");
                                    ArrayList<String> a3 = b.a(PermissionService.this.f4461b, b.f4342a);
                                    MyLog.e(PermissionService.this.f4460a, "8................，，。。。。。。。。。。。。。。。。。。。。。。。。。");
                                    if (a3 == null || !a3.contains(packageName)) {
                                        MyLog.e(PermissionService.this.f4460a, "12................，，。。。。。。。。。。。。。。。。。。。。。。。。。");
                                        PermissionService.this.a(packageName, suCode, suFromuid, suTouid);
                                    } else {
                                        MyLog.e(PermissionService.this.f4460a, "9................，，。。。。。。。。。。。。。。。。。。。。。。。。。");
                                        PermissionUtils.allow(PermissionService.this.f4461b, packageName, suFromuid, 1, suCode);
                                        MyLog.e(PermissionService.this.f4460a, "10................，，。。。。。。。。。。。。。。。。。。。。。。。。。");
                                        UidPolicy uidPolicy = new UidPolicy();
                                        uidPolicy.policy = UidPolicy.ALLOW;
                                        uidPolicy.uid = suFromuid;
                                        uidPolicy.command = null;
                                        uidPolicy.until = 0;
                                        uidPolicy.desiredUid = suTouid;
                                        KingoDatabaseHelper.a(PermissionService.this.f4461b, uidPolicy);
                                        MyLog.e(PermissionService.this.f4460a, "11................，，。。。。。。。。。。。。。。。。。。。。。。。。。");
                                        boolean unused3 = PermissionService.this.f4462c = true;
                                    }
                                } else {
                                    String str = a2.policy;
                                    int i = a2.until;
                                    MyLog.e(PermissionService.this.f4460a, "3................，，。。。。。。。。。。。。。。。。。。。。。。。。。");
                                    if (i == 0 && str.equalsIgnoreCase(UidPolicy.ALLOW)) {
                                        PermissionUtils.allow(PermissionService.this.f4461b, packageName, suFromuid, 1, suCode);
                                        MyLog.e(PermissionService.this.f4460a, "1所要处理的队列长度是。。。。。。。。。。。。。。。。。。。。。。。。。" + App.f4222d.size());
                                        boolean unused4 = PermissionService.this.f4462c = true;
                                        MyLog.e(PermissionService.this.f4460a, "4................，，。。。。。。。。。。。。。。。。。。。。。。。。。");
                                    } else if (i != 0 || !str.equalsIgnoreCase(UidPolicy.DENY)) {
                                        PermissionService.this.a(packageName, suCode, suFromuid, suTouid);
                                        MyLog.e(PermissionService.this.f4460a, "6................，，。。。。。。。。。。。。。。。。。。。。。。。。。");
                                    } else {
                                        PermissionUtils.deny(PermissionService.this.f4461b, packageName, suCode);
                                        boolean unused5 = PermissionService.this.f4462c = true;
                                        MyLog.e(PermissionService.this.f4460a, "5................，，。。。。。。。。。。。。。。。。。。。。。。。。。");
                                    }
                                }
                            } else {
                                PermissionUtils.allow(PermissionService.this.f4461b, packageName, suFromuid, 1, suCode);
                                boolean unused6 = PermissionService.this.f4462c = true;
                            }
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(String str, int i, int i2, int i3) {
        Intent intent = new Intent();
        intent.setClass(this, RequestTranActivity.class);
        intent.putExtra("package_name", str);
        intent.putExtra("su_code", i);
        intent.putExtra("su_fromuid", i2);
        intent.putExtra("su_touid", i3);
        intent.setFlags(268435456);
        intent.addFlags(8388608);
        try {
            this.f4461b.startActivity(intent);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        c(this);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void a(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, PermissionService.class);
        context.startService(intent);
    }

    private class a extends BroadcastReceiver {
        private a() {
        }

        public void onReceive(Context context, Intent intent) {
            boolean unused = PermissionService.this.f4462c = true;
        }
    }

    private void b(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.kingouser.com.finish.permission");
        context.registerReceiver(this.f4464e, intentFilter);
    }

    private void c(Context context) {
        context.unregisterReceiver(this.f4464e);
    }
}
