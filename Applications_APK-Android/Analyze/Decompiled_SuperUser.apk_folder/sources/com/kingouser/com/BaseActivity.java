package com.kingouser.com;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import butterknife.ButterKnife;
import com.google.firebase.analytics.FirebaseAnalytics;

public class BaseActivity extends AppCompatActivity {
    public FirebaseAnalytics o;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        try {
            super.onCreate(bundle);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.o = FirebaseAnalytics.getInstance(this);
        c(R.color.dj);
    }

    public void setContentView(int i) {
        super.setContentView(i);
        ButterKnife.bind(this);
    }

    public void c(int i) {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(Integer.MIN_VALUE);
                window.setStatusBarColor(getResources().getColor(i));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            System.gc();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
