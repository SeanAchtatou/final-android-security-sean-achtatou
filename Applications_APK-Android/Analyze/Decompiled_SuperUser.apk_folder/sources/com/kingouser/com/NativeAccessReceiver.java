package com.kingouser.com;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.kingouser.com.application.App;
import com.kingouser.com.entity.IntentEntity;
import com.kingouser.com.service.PermissionService;
import com.kingouser.com.util.FileUtils;
import com.kingouser.com.util.MySharedPreference;
import com.pureapps.cleaner.util.f;
import java.util.HashMap;
import java.util.concurrent.Executors;

public class NativeAccessReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    String f3978a = "NativeAccessReceiver";

    public void onReceive(final Context context, Intent intent) {
        final String stringExtra = intent.getStringExtra("su_appname");
        intent.getAction();
        intent.getStringExtra("su_cmd");
        intent.getIntExtra("--user", -1);
        int intExtra = intent.getIntExtra("su_fromuid", -1);
        final int intExtra2 = intent.getIntExtra("su_access", -1);
        int intExtra3 = intent.getIntExtra("su_touid", -1);
        int intExtra4 = intent.getIntExtra("su_code", -1);
        intent.getIntExtra("su_uid_mismatch", -1);
        f.a(this.f3978a + "\t|\t" + "onReceive" + stringExtra + "........." + intExtra2 + "......." + intExtra);
        if (!MySharedPreference.getPermissionTostPackageName(context, "").equalsIgnoreCase(stringExtra) || (MySharedPreference.getPermissionTostPackageName(context, "").equalsIgnoreCase(stringExtra) && Math.abs(System.currentTimeMillis() - MySharedPreference.getPermissionTostTime(context, 0)) >= 60000)) {
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                public void run() {
                    FileUtils.saveActiveSu(context, stringExtra);
                    if (intExtra2 == 1) {
                    }
                    if (MySharedPreference.getPermissionState(context, false)) {
                        MySharedPreference.setPermissionState(context, false);
                    }
                    if (Math.abs(MySharedPreference.getActiveTime(context, 0) - System.currentTimeMillis()) >= 43200000) {
                        FileUtils.saveActive(context, "");
                        new HashMap();
                        FileUtils.saveSuRequest(context, context.getPackageName());
                    }
                }
            });
            if (!App.f4223e.contains(stringExtra)) {
                if (intExtra2 == 1 && MySharedPreference.getWheaterToast(context, true)) {
                    Toast.makeText(context, context.getString(R.string.ci, stringExtra), 0).show();
                    MySharedPreference.setPermissionToastTime(context, System.currentTimeMillis());
                    MySharedPreference.setPermissionTostPackageName(context, stringExtra);
                    MySharedPreference.setPermissionTostsuAccess(context, 1);
                } else if (intExtra2 == 0 && MySharedPreference.getWheaterToast(context, true)) {
                    Toast.makeText(context, context.getString(R.string.ch, stringExtra), 0).show();
                    MySharedPreference.setPermissionToastTime(context, System.currentTimeMillis());
                    MySharedPreference.setPermissionTostPackageName(context, stringExtra);
                    MySharedPreference.setPermissionTostsuAccess(context, 0);
                }
            }
        }
        if (intExtra2 == 2 && App.f4222d.size() <= 100) {
            IntentEntity intentEntity = new IntentEntity();
            intentEntity.setPackageName(stringExtra);
            intentEntity.setSuCode(intExtra4);
            intentEntity.setSuFromuid(intExtra);
            intentEntity.setSuTouid(intExtra3);
            intentEntity.setRequestTime(System.currentTimeMillis());
            try {
                App.f4222d.addLast(intentEntity);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            PermissionService.a(context);
        }
    }
}
