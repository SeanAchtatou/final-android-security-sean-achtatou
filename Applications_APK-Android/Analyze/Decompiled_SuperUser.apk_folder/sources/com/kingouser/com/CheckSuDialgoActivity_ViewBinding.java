package com.kingouser.com;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class CheckSuDialgoActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private CheckSuDialgoActivity f3939a;

    /* renamed from: b  reason: collision with root package name */
    private View f3940b;

    public CheckSuDialgoActivity_ViewBinding(final CheckSuDialgoActivity checkSuDialgoActivity, View view) {
        this.f3939a = checkSuDialgoActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.ff, "field 'button' and method 'onClick'");
        checkSuDialgoActivity.button = (Button) Utils.castView(findRequiredView, R.id.ff, "field 'button'", Button.class);
        this.f3940b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                checkSuDialgoActivity.onClick(view);
            }
        });
        checkSuDialgoActivity.tvInfo = (TextView) Utils.findRequiredViewAsType(view, R.id.fe, "field 'tvInfo'", TextView.class);
        checkSuDialgoActivity.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.fd, "field 'tvTitle'", TextView.class);
    }

    public void unbind() {
        CheckSuDialgoActivity checkSuDialgoActivity = this.f3939a;
        if (checkSuDialgoActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f3939a = null;
        checkSuDialgoActivity.button = null;
        checkSuDialgoActivity.tvInfo = null;
        checkSuDialgoActivity.tvTitle = null;
        this.f3940b.setOnClickListener(null);
        this.f3940b = null;
    }
}
