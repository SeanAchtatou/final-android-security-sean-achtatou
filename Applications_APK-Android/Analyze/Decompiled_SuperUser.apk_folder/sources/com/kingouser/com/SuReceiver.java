package com.kingouser.com;

import android.content.BroadcastReceiver;

public class SuReceiver extends BroadcastReceiver {
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:78:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(final android.content.Context r14, android.content.Intent r15) {
        /*
            r13 = this;
            r11 = 2131296374(0x7f090076, float:1.8210663E38)
            r9 = 2131296313(0x7f090039, float:1.821054E38)
            r3 = -1
            r8 = 1
            r10 = 0
            if (r15 != 0) goto L_0x000c
        L_0x000b:
            return
        L_0x000c:
            java.lang.String r0 = "command"
            java.lang.String r0 = r15.getStringExtra(r0)
            if (r0 == 0) goto L_0x000b
            java.lang.String r1 = "uid"
            int r1 = r15.getIntExtra(r1, r3)
            if (r1 == r3) goto L_0x000b
            java.lang.String r2 = "desired_uid"
            int r2 = r15.getIntExtra(r2, r3)
            if (r2 == r3) goto L_0x000b
            java.lang.String r3 = "action"
            java.lang.String r3 = r15.getStringExtra(r3)
            if (r3 == 0) goto L_0x000b
            java.lang.String r4 = "from_name"
            java.lang.String r4 = r15.getStringExtra(r4)
            java.lang.String r5 = "desired_name"
            java.lang.String r5 = r15.getStringExtra(r5)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "fromName:"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r4)
            java.lang.String r6 = r6.toString()
            com.pureapps.cleaner.util.f.a(r6)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "desiredName:"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r5)
            java.lang.String r6 = r6.toString()
            com.pureapps.cleaner.util.f.a(r6)
            com.kingouser.com.db.LogEntry r6 = new com.kingouser.com.db.LogEntry
            r6.<init>()
            r6.uid = r1
            r6.command = r0
            r6.f4337b = r3
            r6.desiredUid = r2
            r6.desiredName = r5
            r6.username = r4
            long r0 = java.lang.System.currentTimeMillis()
            r4 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 / r4
            int r0 = (int) r0
            r6.f4338c = r0
            r6.getPackageInfo(r14)
            com.kingouser.com.entity.UidPolicy r2 = com.kingouser.com.db.KingouserDatabaseHelper.a(r14, r6)
            java.lang.String r0 = ""
            android.content.res.Resources r1 = r14.getResources()
            java.lang.String r1 = r1.getString(r9)
            java.lang.String r4 = r6.getName()
            boolean r1 = r1.equalsIgnoreCase(r4)
            if (r1 != 0) goto L_0x000b
            java.lang.String r1 = r6.packageName
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x00f2
            java.lang.String r1 = r6.packageName
            java.lang.String r4 = "com.kingoapp.root"
            boolean r1 = r1.equalsIgnoreCase(r4)
            if (r1 == 0) goto L_0x00f2
            java.lang.String r1 = "com.kingoapp.root"
            com.kingouser.com.entity.UidPolicy r1 = com.kingouser.com.db.KingoDatabaseHelper.a(r14, r1)     // Catch:{ Exception -> 0x01f2 }
            r4 = 0
            boolean r4 = com.kingouser.com.util.MySharedPreference.getWheaterFirstRun(r14, r4)     // Catch:{ Exception -> 0x01f2 }
            if (r4 != 0) goto L_0x00f2
            if (r1 == 0) goto L_0x015c
            java.lang.String r4 = "allow"
            java.lang.String r1 = r1.policy     // Catch:{ Exception -> 0x01f2 }
            boolean r1 = r4.equalsIgnoreCase(r1)     // Catch:{ Exception -> 0x01f2 }
            if (r1 == 0) goto L_0x015c
            r1 = 2131296375(0x7f090077, float:1.8210665E38)
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x01f2 }
            r5 = 0
            java.lang.String r7 = r6.getName()     // Catch:{ Exception -> 0x01f2 }
            r4[r5] = r7     // Catch:{ Exception -> 0x01f2 }
            java.lang.String r1 = r14.getString(r1, r4)     // Catch:{ Exception -> 0x01f2 }
        L_0x00d6:
            r0 = 1
            com.kingouser.com.util.MySharedPreference.setWheatherFirstRun(r14, r0)     // Catch:{ Exception -> 0x00eb }
            r0 = 1
            boolean r0 = com.kingouser.com.util.MySharedPreference.getWheaterToast(r14, r0)     // Catch:{ Exception -> 0x00eb }
            if (r0 == 0) goto L_0x000b
            r0 = 0
            android.widget.Toast r0 = android.widget.Toast.makeText(r14, r1, r0)     // Catch:{ Exception -> 0x00eb }
            r0.show()     // Catch:{ Exception -> 0x00eb }
            goto L_0x000b
        L_0x00eb:
            r0 = move-exception
            r12 = r0
            r0 = r1
            r1 = r12
        L_0x00ef:
            r1.printStackTrace()
        L_0x00f2:
            java.lang.String r1 = "allow"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x016f
            r0 = 2131296375(0x7f090077, float:1.8210665E38)
            java.lang.Object[] r1 = new java.lang.Object[r8]
            java.lang.String r3 = r6.getName()
            r1[r10] = r3
            java.lang.String r0 = r14.getString(r0, r1)
        L_0x0109:
            if (r2 == 0) goto L_0x010f
            boolean r1 = r2.notification
            if (r1 == 0) goto L_0x000b
        L_0x010f:
            int r1 = com.kingouser.com.util.Settings.getNotificationType(r14)
            switch(r1) {
                case 1: goto L_0x0118;
                case 2: goto L_0x0184;
                default: goto L_0x0116;
            }
        L_0x0116:
            goto L_0x000b
        L_0x0118:
            java.lang.String r1 = "kingouser"
            java.lang.String r2 = r6.getName()
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 != 0) goto L_0x000b
            boolean r1 = com.kingouser.com.util.MySharedPreference.getWheaterToast(r14, r8)
            if (r1 == 0) goto L_0x000b
            long r2 = java.lang.System.currentTimeMillis()
            java.lang.String r1 = ""
            java.lang.String r1 = com.kingouser.com.util.MySharedPreference.getPermissionTostPackageName(r14, r1)
            java.lang.String r4 = r6.packageName
            boolean r1 = r1.equalsIgnoreCase(r4)
            if (r1 == 0) goto L_0x01c3
            long r4 = java.lang.System.currentTimeMillis()
            r8 = 0
            long r8 = com.kingouser.com.util.MySharedPreference.getPermissionTostTime(r14, r8)
            long r4 = r4 - r8
            long r4 = java.lang.Math.abs(r4)
            r8 = 60000(0xea60, double:2.9644E-319)
            int r1 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r1 > 0) goto L_0x01c3
            com.kingouser.com.util.MySharedPreference.setPermissionToastTime(r14, r2)
            java.lang.String r0 = r6.packageName
            com.kingouser.com.util.MySharedPreference.setPermissionTostPackageName(r14, r0)
            goto L_0x000b
        L_0x015c:
            r1 = 2131296374(0x7f090076, float:1.8210663E38)
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x01f2 }
            r5 = 0
            java.lang.String r7 = r6.getName()     // Catch:{ Exception -> 0x01f2 }
            r4[r5] = r7     // Catch:{ Exception -> 0x01f2 }
            java.lang.String r1 = r14.getString(r1, r4)     // Catch:{ Exception -> 0x01f2 }
            goto L_0x00d6
        L_0x016f:
            java.lang.String r1 = "deny"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x0109
            java.lang.Object[] r0 = new java.lang.Object[r8]
            java.lang.String r1 = r6.getName()
            r0[r10] = r1
            java.lang.String r0 = r14.getString(r11, r0)
            goto L_0x0109
        L_0x0184:
            android.support.v4.app.NotificationCompat$Builder r1 = new android.support.v4.app.NotificationCompat$Builder
            r1.<init>(r14)
            android.support.v4.app.NotificationCompat$Builder r2 = r1.c(r0)
            android.support.v4.app.NotificationCompat$Builder r2 = r2.b(r8)
            android.content.Intent r3 = new android.content.Intent
            r3.<init>()
            android.app.PendingIntent r3 = android.app.PendingIntent.getActivity(r14, r10, r3, r10)
            android.support.v4.app.NotificationCompat$Builder r2 = r2.a(r3)
            java.lang.String r3 = r14.getString(r9)
            android.support.v4.app.NotificationCompat$Builder r2 = r2.a(r3)
            android.support.v4.app.NotificationCompat$Builder r0 = r2.b(r0)
            r2 = 2130837699(0x7f0200c3, float:1.728036E38)
            r0.a(r2)
            java.lang.String r0 = "notification"
            java.lang.Object r0 = r14.getSystemService(r0)
            android.app.NotificationManager r0 = (android.app.NotificationManager) r0
            r2 = 4545(0x11c1, float:6.369E-42)
            android.app.Notification r1 = r1.a()
            r0.notify(r2, r1)
            goto L_0x000b
        L_0x01c3:
            com.kingouser.com.util.MySharedPreference.setPermissionToastTime(r14, r2)
            java.lang.String r1 = r6.packageName
            com.kingouser.com.util.MySharedPreference.setPermissionTostPackageName(r14, r1)
            android.widget.Toast r0 = android.widget.Toast.makeText(r14, r0, r10)
            r0.show()
            java.lang.String r0 = "com.android.shell"
            java.lang.String r1 = r6.packageName
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 != 0) goto L_0x000b
            java.lang.String r0 = r6.packageName
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x000b
            java.util.concurrent.ExecutorService r0 = java.util.concurrent.Executors.newSingleThreadExecutor()
            com.kingouser.com.SuReceiver$1 r1 = new com.kingouser.com.SuReceiver$1
            r1.<init>(r14, r6)
            r0.execute(r1)
            goto L_0x000b
        L_0x01f2:
            r1 = move-exception
            goto L_0x00ef
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.SuReceiver.onReceive(android.content.Context, android.content.Intent):void");
    }
}
