package com.kingouser.com;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class RequestTranActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private RequestTranActivity f4043a;

    /* renamed from: b  reason: collision with root package name */
    private View f4044b;

    /* renamed from: c  reason: collision with root package name */
    private View f4045c;

    public RequestTranActivity_ViewBinding(final RequestTranActivity requestTranActivity, View view) {
        this.f4043a = requestTranActivity;
        requestTranActivity.imageView = (ImageView) Utils.findRequiredViewAsType(view, R.id.lw, "field 'imageView'", ImageView.class);
        requestTranActivity.applicationTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.lx, "field 'applicationTitle'", TextView.class);
        requestTranActivity.tvAppTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.lv, "field 'tvAppTitle'", TextView.class);
        requestTranActivity.tvSecurityLevel = (TextView) Utils.findRequiredViewAsType(view, R.id.ly, "field 'tvSecurityLevel'", TextView.class);
        requestTranActivity.tvManufacturer = (TextView) Utils.findRequiredViewAsType(view, R.id.lz, "field 'tvManufacturer'", TextView.class);
        requestTranActivity.tvRequestPermission = (TextView) Utils.findRequiredViewAsType(view, R.id.m1, "field 'tvRequestPermission'", TextView.class);
        requestTranActivity.tvValue = (TextView) Utils.findRequiredViewAsType(view, R.id.m0, "field 'tvValue'", TextView.class);
        requestTranActivity.tvShadow = (TextView) Utils.findRequiredViewAsType(view, R.id.ld, "field 'tvShadow'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.m3, "field 'mAllow' and method 'OnClick'");
        requestTranActivity.mAllow = (Button) Utils.castView(findRequiredView, R.id.m3, "field 'mAllow'", Button.class);
        this.f4044b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                requestTranActivity.OnClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.m2, "field 'mDeny' and method 'OnClick'");
        requestTranActivity.mDeny = (Button) Utils.castView(findRequiredView2, R.id.m2, "field 'mDeny'", Button.class);
        this.f4045c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                requestTranActivity.OnClick(view);
            }
        });
        requestTranActivity.progressBar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.d6, "field 'progressBar'", ProgressBar.class);
    }

    public void unbind() {
        RequestTranActivity requestTranActivity = this.f4043a;
        if (requestTranActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f4043a = null;
        requestTranActivity.imageView = null;
        requestTranActivity.applicationTitle = null;
        requestTranActivity.tvAppTitle = null;
        requestTranActivity.tvSecurityLevel = null;
        requestTranActivity.tvManufacturer = null;
        requestTranActivity.tvRequestPermission = null;
        requestTranActivity.tvValue = null;
        requestTranActivity.tvShadow = null;
        requestTranActivity.mAllow = null;
        requestTranActivity.mDeny = null;
        requestTranActivity.progressBar = null;
        this.f4044b.setOnClickListener(null);
        this.f4044b = null;
        this.f4045c.setOnClickListener(null);
        this.f4045c = null;
    }
}
