package com.kingouser.com;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class PaymentFailedActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private PaymentFailedActivity f3987a;

    /* renamed from: b  reason: collision with root package name */
    private View f3988b;

    public PaymentFailedActivity_ViewBinding(final PaymentFailedActivity paymentFailedActivity, View view) {
        this.f3987a = paymentFailedActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.kx, "method 'onClick'");
        this.f3988b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                paymentFailedActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        if (this.f3987a == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f3987a = null;
        this.f3988b.setOnClickListener(null);
        this.f3988b = null;
    }
}
