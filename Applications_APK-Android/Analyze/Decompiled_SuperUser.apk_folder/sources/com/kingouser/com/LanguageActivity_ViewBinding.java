package com.kingouser.com;

import android.view.View;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class LanguageActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private LanguageActivity f3943a;

    public LanguageActivity_ViewBinding(LanguageActivity languageActivity, View view) {
        this.f3943a = languageActivity;
        languageActivity.lvLanguage = (ListView) Utils.findRequiredViewAsType(view, R.id.iz, "field 'lvLanguage'", ListView.class);
    }

    public void unbind() {
        LanguageActivity languageActivity = this.f3943a;
        if (languageActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f3943a = null;
        languageActivity.lvLanguage = null;
    }
}
