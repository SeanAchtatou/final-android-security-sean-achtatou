package com.kingouser.com;

import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class MainActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private MainActivity f3965a;

    /* renamed from: b  reason: collision with root package name */
    private View f3966b;

    /* renamed from: c  reason: collision with root package name */
    private ViewPager.e f3967c;

    public MainActivity_ViewBinding(final MainActivity mainActivity, View view) {
        this.f3965a = mainActivity;
        mainActivity.mDrawerLayout = (DrawerLayout) Utils.findRequiredViewAsType(view, R.id.di, "field 'mDrawerLayout'", DrawerLayout.class);
        mainActivity.mNavigationView = (NavigationView) Utils.findRequiredViewAsType(view, R.id.dk, "field 'mNavigationView'", NavigationView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.d2, "field 'viewPager' and method 'onPageSelected'");
        mainActivity.viewPager = (ViewPager) Utils.castView(findRequiredView, R.id.d2, "field 'viewPager'", ViewPager.class);
        this.f3966b = findRequiredView;
        this.f3967c = new ViewPager.e() {
            public void onPageSelected(int i) {
                mainActivity.onPageSelected(i);
            }

            public void onPageScrolled(int i, float f2, int i2) {
            }

            public void onPageScrollStateChanged(int i) {
            }
        };
        ((ViewPager) findRequiredView).a(this.f3967c);
    }

    public void unbind() {
        MainActivity mainActivity = this.f3965a;
        if (mainActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f3965a = null;
        mainActivity.mDrawerLayout = null;
        mainActivity.mNavigationView = null;
        mainActivity.viewPager = null;
        ((ViewPager) this.f3966b).b(this.f3967c);
        this.f3967c = null;
        this.f3966b = null;
    }
}
