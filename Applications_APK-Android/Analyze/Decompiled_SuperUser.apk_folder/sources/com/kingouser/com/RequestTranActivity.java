package com.kingouser.com;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.kingouser.com.util.DeviceInfoUtils;
import com.kingouser.com.util.FileUtils;
import com.kingouser.com.util.MyLog;
import com.kingouser.com.util.MySharedPreference;
import com.kingouser.com.util.PermissionUtils;
import java.io.ByteArrayInputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class RequestTranActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public long f4030a;
    @BindView(R.id.lx)
    TextView applicationTitle;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Activity f4031b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public int f4032c = 0;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public int f4033d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public int f4034e;

    /* renamed from: f  reason: collision with root package name */
    private b f4035f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public String f4036g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public boolean f4037h;
    private a i = new a();
    @BindView(R.id.lw)
    ImageView imageView;
    /* access modifiers changed from: private */
    public Handler j = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 92:
                    int intValue = ((Integer) message.obj).intValue();
                    RequestTranActivity.this.progressBar.setProgress(intValue);
                    if (intValue == 100 && !RequestTranActivity.this.isFinishing()) {
                        boolean unused = RequestTranActivity.this.f4037h = true;
                        PermissionUtils.deny(RequestTranActivity.this.f4031b, RequestTranActivity.this.f4036g, RequestTranActivity.this.f4032c);
                        PermissionUtils.handleAction(RequestTranActivity.this.f4031b, RequestTranActivity.this.f4037h, false, Integer.valueOf(RequestTranActivity.this.c()), RequestTranActivity.this.f4033d, RequestTranActivity.this.f4034e);
                        RequestTranActivity.this.finish();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    @BindView(R.id.m3)
    Button mAllow;
    @BindView(R.id.m2)
    Button mDeny;
    @BindView(R.id.d6)
    ProgressBar progressBar;
    @BindView(R.id.lv)
    TextView tvAppTitle;
    @BindView(R.id.lz)
    TextView tvManufacturer;
    @BindView(R.id.m1)
    TextView tvRequestPermission;
    @BindView(R.id.ly)
    TextView tvSecurityLevel;
    @BindView(R.id.ld)
    TextView tvShadow;
    @BindView(R.id.m0)
    TextView tvValue;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f4031b = this;
        requestWindowFeature(1);
        a(this.f4031b);
        setContentView((int) R.layout.cp);
        ButterKnife.bind(this);
        int requestDialogTimes = MySharedPreference.getRequestDialogTimes(getApplicationContext(), 15);
        Long l = 100L;
        this.f4030a = 1000 / (l.longValue() / ((long) requestDialogTimes));
        if (Build.VERSION.SDK_INT >= 11) {
            new Object() {
                public void a(Activity activity) {
                    activity.setFinishOnTouchOutside(false);
                }
            }.a(this);
        }
        d();
        a();
        a(this.f4033d);
        if (!FileUtils.checkFileExist(this.f4031b, this.f4031b.getFilesDir() + "/supersu.cfg")) {
            PermissionUtils.createPrePermission(this.f4031b, requestDialogTimes);
        }
    }

    private void a() {
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        this.f4036g = intent.getStringExtra("package_name");
        this.f4032c = intent.getIntExtra("su_code", 0);
        this.f4033d = intent.getIntExtra("su_fromuid", 0);
        this.f4034e = intent.getIntExtra("su_touid", 0);
        if (!TextUtils.isEmpty(this.f4036g)) {
            this.applicationTitle.setText(this.f4036g);
        } else {
            this.applicationTitle.setText("");
        }
        if ("ADB shell".equalsIgnoreCase(this.f4036g)) {
            PermissionUtils.allow(this.f4031b, this.f4036g, this.f4033d, 1, this.f4032c);
            this.f4037h = true;
            finish();
        }
    }

    private void a(int i2) {
        PackageManager packageManager = getPackageManager();
        String[] packagesForUid = packageManager.getPackagesForUid(i2);
        if (packagesForUid != null && packagesForUid.length > 0) {
            for (String str : packagesForUid) {
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo(str, (int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
                    PackageInfo packageInfo2 = packageManager.getPackageInfo(str, 64);
                    this.applicationTitle.setText(this.f4031b.getResources().getString(R.string.cz, (String) packageInfo.applicationInfo.loadLabel(packageManager)));
                    this.imageView.setImageDrawable(packageInfo.applicationInfo.loadIcon(packageManager));
                    try {
                        String[] split = ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(packageInfo2.signatures[0].toByteArray()))).getIssuerX500Principal().getName("RFC1779").split(",");
                        String str2 = "";
                        for (String str3 : split) {
                            if (str3.contains("O=")) {
                                str2 = str3.substring(str3.indexOf("=") + 1);
                            }
                        }
                        this.tvValue.setText(" : " + str2);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                } catch (Exception e3) {
                    String string = getString(R.string.bp);
                    this.imageView.setImageResource(R.drawable.et);
                    this.applicationTitle.setText(this.f4031b.getResources().getString(R.string.cz, string));
                }
            }
        }
    }

    private class a extends BroadcastReceiver {
        private a() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            MyLog.e("PermissionService", "收到了home键的广播。。。。。。。。。。。。。。。。。。。。。。。。0");
            if ("android.intent.action.CLOSE_SYSTEM_DIALOGS".equalsIgnoreCase(action)) {
                MyLog.e("PermissionService", "收到了home键的广播。。。。。。。。。。。。。。。。。。。。。。。。1");
                RequestTranActivity.this.finish();
            }
        }
    }

    private void a(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        context.registerReceiver(this.i, intentFilter);
    }

    private void b(Context context) {
        context.unregisterReceiver(this.i);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        b();
        a(this.f4031b);
    }

    private void b() {
        this.progressBar.setMax(100);
        this.progressBar.setProgress(0);
        this.f4035f = new b();
        this.f4035f.f4041a = false;
        this.f4035f.start();
    }

    private class b extends Thread {

        /* renamed from: a  reason: collision with root package name */
        public boolean f4041a;

        private b() {
        }

        public void run() {
            super.run();
            int i = 1;
            while (!this.f4041a && i <= 100) {
                Message message = new Message();
                message.what = 92;
                message.obj = Integer.valueOf(i);
                RequestTranActivity.this.j.sendMessage(message);
                try {
                    Thread.sleep(RequestTranActivity.this.f4030a);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                i++;
            }
        }
    }

    /* access modifiers changed from: private */
    public int c() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    @OnClick({2131624409, 2131624408})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.m2 /*2131624408*/:
                this.f4037h = true;
                PermissionUtils.deny(this.f4031b, this.f4036g, this.f4032c);
                PermissionUtils.handleAction(this.f4031b, this.f4037h, false, 0, this.f4033d, this.f4034e);
                finish();
                return;
            case R.id.m3 /*2131624409*/:
                this.f4037h = true;
                PermissionUtils.allow(this.f4031b, this.f4036g, this.f4033d, 1, this.f4032c);
                PermissionUtils.handleAction(this.f4031b, this.f4037h, true, 0, this.f4033d, this.f4034e);
                finish();
                return;
            default:
                return;
        }
    }

    private void d() {
        f();
        e();
    }

    private void e() {
        this.tvAppTitle.setTypeface(Typeface.createFromAsset(this.f4031b.getAssets(), "fonts/Arial.ttf"));
    }

    private void f() {
        this.tvAppTitle.setTextSize(DeviceInfoUtils.getTextSize(this.f4031b, 62));
        this.applicationTitle.setTextSize(DeviceInfoUtils.getTextSize(this.f4031b, 48));
        this.tvSecurityLevel.setTextSize(DeviceInfoUtils.getTextSize(this.f4031b, 36));
        this.tvManufacturer.setTextSize(DeviceInfoUtils.getTextSize(this.f4031b, 36));
        this.tvValue.setTextSize(DeviceInfoUtils.getTextSize(this.f4031b, 36));
        this.tvShadow.setTextSize(DeviceInfoUtils.getTextSize(this.f4031b, 36));
        this.tvRequestPermission.setTextSize(DeviceInfoUtils.getTextSize(this.f4031b, 46));
        this.mAllow.setTextSize(DeviceInfoUtils.getTextSize(this.f4031b, 36));
        this.mDeny.setTextSize(DeviceInfoUtils.getTextSize(this.f4031b, 36));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.f4035f.f4041a = true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.f4037h) {
            PermissionUtils.deny(this.f4031b, this.f4036g, this.f4032c);
            PermissionUtils.handleAction(this.f4031b, this.f4037h, false, Integer.valueOf(c()), this.f4033d, this.f4034e);
        }
        Intent intent = new Intent();
        intent.setAction("com.kingouser.com.finish.permission");
        this.f4031b.sendBroadcast(intent);
        try {
            b(this.f4031b);
        } catch (Exception e2) {
        }
    }
}
