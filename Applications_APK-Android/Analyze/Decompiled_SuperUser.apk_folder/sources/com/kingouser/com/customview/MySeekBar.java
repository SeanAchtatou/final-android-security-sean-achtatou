package com.kingouser.com.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.kingouser.com.R;

public class MySeekBar extends View implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4264a = true;

    /* renamed from: b  reason: collision with root package name */
    private float f4265b;

    /* renamed from: c  reason: collision with root package name */
    private float f4266c;

    /* renamed from: d  reason: collision with root package name */
    private float f4267d;

    /* renamed from: e  reason: collision with root package name */
    private float f4268e;

    /* renamed from: f  reason: collision with root package name */
    private float f4269f;

    /* renamed from: g  reason: collision with root package name */
    private int f4270g;

    /* renamed from: h  reason: collision with root package name */
    private int f4271h;
    private a i;
    private Paint j;
    private Paint k;
    private Paint l;
    private Paint m;

    public interface a {
        void a(int i);

        void b(int i);
    }

    public MySeekBar(Context context) {
        super(context);
        a(context);
    }

    public MySeekBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public MySeekBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    private void a(Context context) {
        this.j = new Paint(1);
        this.j.setColor(context.getResources().getColor(R.color.f8329h));
        this.k = new Paint(1);
        this.k.setColor(context.getResources().getColor(R.color.am));
        this.l = new Paint(1);
        this.l.setColor(context.getResources().getColor(R.color.am));
        this.m = new Paint(1);
        this.m.setStrokeWidth(0.1f);
        this.m.setStyle(Paint.Style.STROKE);
        this.m.setColor(context.getResources().getColor(R.color.i));
        setOnTouchListener(this);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (View.MeasureSpec.getMode(i2) == 1073741824) {
            this.f4267d = (float) View.MeasureSpec.getSize(i2);
            this.f4269f = (float) View.MeasureSpec.getSize(i3);
        }
        setMeasuredDimension((int) this.f4267d, (int) this.f4269f);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f4264a) {
            this.f4268e = (((float) this.f4270g) * (this.f4267d - this.f4265b)) / ((float) this.f4271h);
        } else if (this.f4268e <= 0.0f) {
            this.f4268e = 0.0f;
        } else if (this.f4268e >= this.f4267d - this.f4265b) {
            this.f4268e = this.f4267d - this.f4265b;
        }
        int i2 = (int) ((this.f4269f - this.f4266c) / 2.0f);
        RectF rectF = new RectF(0.0f, (float) i2, this.f4267d, this.f4266c + ((float) i2));
        RectF rectF2 = new RectF(0.0f, (float) i2, this.f4268e + 12.0f, ((float) i2) + this.f4266c);
        canvas.drawRect(rectF, this.j);
        canvas.drawRect(rectF2, this.k);
        canvas.translate(this.f4268e, 0.0f);
        canvas.drawCircle(this.f4265b / 2.0f, this.f4269f / 2.0f, this.f4265b / 2.0f, this.l);
        canvas.drawCircle(this.f4265b / 2.0f, this.f4269f / 2.0f, this.f4265b / 2.0f, this.m);
        if (this.i != null) {
            this.i.b((int) ((this.f4268e * ((float) this.f4271h)) / (this.f4267d - this.f4265b)));
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f4264a = false;
                return true;
            case 1:
            case 3:
                if (this.f4268e <= 0.0f) {
                    this.f4268e = 0.0f;
                } else if (this.f4268e >= this.f4267d - this.f4265b) {
                    this.f4268e = this.f4267d - this.f4265b;
                }
                if (this.i == null) {
                    return true;
                }
                this.i.a((int) ((this.f4268e * ((float) this.f4271h)) / (this.f4267d - this.f4265b)));
                return true;
            case 2:
                this.f4268e = (float) ((int) motionEvent.getX());
                invalidate();
                return true;
            default:
                return true;
        }
    }

    public float getBtDiameter() {
        return this.f4265b;
    }

    public void setBtDiameter(float f2) {
        this.f4265b = f2;
    }

    public float getBackgroundHeight() {
        return this.f4266c;
    }

    public void setBackgroundHeight(float f2) {
        this.f4266c = f2;
    }

    public float getBackgroundWidth() {
        return this.f4267d;
    }

    public void setBackgroundWidth(float f2) {
        this.f4267d = f2;
    }

    public int getMaxProgress() {
        return this.f4271h;
    }

    public void setMaxProgress(int i2) {
        this.f4271h = i2;
    }

    public int getProgress() {
        return this.f4270g;
    }

    public void setProgress(int i2) {
        this.f4270g = i2;
    }

    public void setOnSeekBarChangedListener(a aVar) {
        this.i = aVar;
    }

    public float getOffsetHeight() {
        return this.f4269f;
    }

    public void setOffsetHeight(float f2) {
        this.f4269f = f2;
    }
}
