package com.kingouser.com.customview;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.kingouser.com.R;

/* compiled from: MySingleToast */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static PopupWindow f4333a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static Handler f4334b = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 77:
                    a.a();
                    return;
                default:
                    return;
            }
        }
    };

    public static void a(Context context, View view, String str) {
        if (f4333a == null || !f4333a.isShowing()) {
            a();
            View inflate = View.inflate(context, R.layout.aw, null);
            ((TextView) inflate.findViewById(R.id.gp)).setText(str);
            f4333a = new PopupWindow(inflate, -2, -2);
            f4333a.setBackgroundDrawable(new ColorDrawable(0));
            f4333a.showAtLocation(view, 80, 0, 0);
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                    Message message = new Message();
                    message.what = 77;
                    a.f4334b.sendMessage(message);
                }
            }).start();
            return;
        }
        a();
    }

    public static void a() {
        if (f4333a != null && f4333a.isShowing()) {
            f4333a.dismiss();
            f4333a = null;
        }
    }
}
