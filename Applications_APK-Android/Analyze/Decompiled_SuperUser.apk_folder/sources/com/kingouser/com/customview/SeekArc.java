package com.kingouser.com.customview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.kingouser.com.R;
import com.kingouser.com.util.MyLog;
import h.a.a;

public class SeekArc extends View {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4308a = SeekArc.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static int f4309b = -1;

    /* renamed from: c  reason: collision with root package name */
    private final int f4310c = -90;

    /* renamed from: d  reason: collision with root package name */
    private int f4311d = 100;

    /* renamed from: e  reason: collision with root package name */
    private int f4312e = 0;

    /* renamed from: f  reason: collision with root package name */
    private int f4313f = 3;

    /* renamed from: g  reason: collision with root package name */
    private int f4314g = 28;

    /* renamed from: h  reason: collision with root package name */
    private int f4315h = 0;
    private int i = 360;
    private int j = 0;
    private boolean k = false;
    private boolean l = true;
    private boolean m = true;
    private int n = 0;
    private float o = 0.0f;
    private RectF p = new RectF();
    private Paint q;
    private Paint r;
    private int s;
    private int t;
    private int u;
    private int v;

    public SeekArc(Context context) {
        super(context);
        a(context, null, 0);
    }

    public SeekArc(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet, R.attr.i0);
    }

    public SeekArc(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet, i2);
    }

    private void a(Context context, AttributeSet attributeSet, int i2) {
        int i3 = 0;
        MyLog.d(f4308a, "Initialising SeekArc");
        Resources resources = getResources();
        float f2 = context.getResources().getDisplayMetrics().density;
        int color = resources.getColor(R.color.d7);
        int color2 = resources.getColor(17170450);
        this.f4313f = (int) (f2 * ((float) this.f4313f));
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.C0102a.SeekArc, i2, 0);
            this.f4311d = obtainStyledAttributes.getInteger(2, this.f4311d);
            this.f4312e = obtainStyledAttributes.getInteger(5, this.f4312e);
            this.f4313f = (int) obtainStyledAttributes.getDimension(3, (float) this.f4313f);
            this.f4314g = (int) obtainStyledAttributes.getDimension(4, (float) this.f4314g);
            this.f4315h = obtainStyledAttributes.getInt(7, this.f4315h);
            this.i = obtainStyledAttributes.getInt(8, this.i);
            this.j = obtainStyledAttributes.getInt(6, this.j);
            this.k = obtainStyledAttributes.getBoolean(11, this.k);
            this.l = obtainStyledAttributes.getBoolean(12, this.l);
            this.m = obtainStyledAttributes.getBoolean(13, this.m);
            color = obtainStyledAttributes.getColor(9, color);
            color2 = obtainStyledAttributes.getColor(10, color2);
            obtainStyledAttributes.recycle();
        }
        this.f4312e = this.f4312e > this.f4311d ? this.f4311d : this.f4312e;
        this.f4312e = this.f4312e < 0 ? 0 : this.f4312e;
        this.i = this.i > 360 ? 360 : this.i;
        this.i = this.i < 0 ? 0 : this.i;
        this.f4315h = this.f4315h > 360 ? 0 : this.f4315h;
        if (this.f4315h >= 0) {
            i3 = this.f4315h;
        }
        this.f4315h = i3;
        this.q = new Paint();
        this.q.setColor(color);
        this.q.setAntiAlias(true);
        this.q.setStyle(Paint.Style.STROKE);
        this.q.setStrokeWidth((float) this.f4313f);
        this.q.setAlpha(60);
        this.r = new Paint();
        this.r.setColor(color2);
        this.r.setAntiAlias(true);
        this.r.setStyle(Paint.Style.STROKE);
        this.r.setStrokeWidth((float) this.f4313f);
        this.q.setStrokeCap(Paint.Cap.ROUND);
        this.r.setStrokeCap(Paint.Cap.ROUND);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (!this.m) {
            canvas.scale(-1.0f, 1.0f, this.p.centerX(), this.p.centerY());
        }
        int i2 = (this.f4315h - 90) + this.j;
        Canvas canvas2 = canvas;
        canvas2.drawArc(this.p, (float) i2, (float) this.i, false, this.q);
        canvas.drawArc(this.p, (float) i2, this.o, false, this.r);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int defaultSize = getDefaultSize(getSuggestedMinimumHeight(), i3);
        int defaultSize2 = getDefaultSize(getSuggestedMinimumWidth(), i2);
        int min = Math.min(defaultSize2, defaultSize);
        this.s = (int) (((float) defaultSize2) * 0.5f);
        this.t = (int) (((float) defaultSize) * 0.5f);
        int paddingLeft = min - getPaddingLeft();
        this.n = paddingLeft / 2;
        float f2 = (float) ((defaultSize / 2) - (paddingLeft / 2));
        float f3 = (float) ((defaultSize2 / 2) - (paddingLeft / 2));
        this.p.set(f3, f2, ((float) paddingLeft) + f3, ((float) paddingLeft) + f2);
        int i4 = ((int) this.o) + this.f4315h + this.j + 90;
        this.u = (int) (((double) this.n) * Math.cos(Math.toRadians((double) i4)));
        this.v = (int) (Math.sin(Math.toRadians((double) i4)) * ((double) this.n));
        super.onMeasure(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

    private void a() {
        int i2 = (int) (((float) this.f4315h) + this.o + ((float) this.j) + 90.0f);
        this.u = (int) (((double) this.n) * Math.cos(Math.toRadians((double) i2)));
        this.v = (int) (Math.sin(Math.toRadians((double) i2)) * ((double) this.n));
    }

    private void a(int i2, boolean z) {
        if (i2 != f4309b) {
            if (i2 > this.f4311d) {
                i2 = this.f4311d;
            }
            if (this.f4312e < 0) {
                i2 = 0;
            }
            this.f4312e = i2;
            this.o = (((float) i2) / ((float) this.f4311d)) * ((float) this.i);
            a();
            invalidate();
        }
    }

    public void setProgress(int i2) {
        a(i2, false);
    }

    public int getProgressWidth() {
        return this.f4313f;
    }

    public void setProgressWidth(int i2) {
        this.f4313f = i2;
        this.r.setStrokeWidth((float) i2);
    }

    public int getArcWidth() {
        return this.f4314g;
    }

    public void setArcWidth(int i2) {
        this.f4314g = i2;
        this.q.setStrokeWidth((float) i2);
    }

    public int getArcRotation() {
        return this.j;
    }

    public void setArcRotation(int i2) {
        this.j = i2;
        a();
    }

    public int getStartAngle() {
        return this.f4315h;
    }

    public void setStartAngle(int i2) {
        this.f4315h = i2;
        a();
    }

    public int getSweepAngle() {
        return this.i;
    }

    public void setSweepAngle(int i2) {
        this.i = i2;
        a();
    }

    public void setRoundedEdges(boolean z) {
        this.k = z;
        if (this.k) {
            this.q.setStrokeCap(Paint.Cap.ROUND);
            this.r.setStrokeCap(Paint.Cap.ROUND);
            return;
        }
        this.q.setStrokeCap(Paint.Cap.SQUARE);
        this.r.setStrokeCap(Paint.Cap.SQUARE);
    }

    public void setClockwise(boolean z) {
        this.m = z;
    }
}
