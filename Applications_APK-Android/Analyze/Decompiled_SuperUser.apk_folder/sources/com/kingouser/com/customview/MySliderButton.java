package com.kingouser.com.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.kingouser.com.R;

public class MySliderButton extends View implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f4272a;

    /* renamed from: b  reason: collision with root package name */
    private c f4273b;

    /* renamed from: c  reason: collision with root package name */
    private int f4274c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public int f4275d;

    /* renamed from: e  reason: collision with root package name */
    private int f4276e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public int f4277f;

    /* renamed from: g  reason: collision with root package name */
    private int f4278g;

    /* renamed from: h  reason: collision with root package name */
    private int f4279h;
    private int i;
    private int j;
    private long k;
    private boolean l = false;
    /* access modifiers changed from: private */
    public boolean m = true;
    private Paint n;
    private Paint o;
    private Paint p;

    public interface c {
        void a(MySliderButton mySliderButton, boolean z);
    }

    public MySliderButton(Context context) {
        super(context);
        a(context);
    }

    public MySliderButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public MySliderButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    private void a(Context context) {
        this.f4272a = context;
        this.n = new Paint(1);
        this.o = new Paint(1);
        this.o.setStyle(Paint.Style.STROKE);
        this.o.setColor(context.getResources().getColor(R.color.am));
        this.p = new Paint(1);
        setOnTouchListener(this);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        setMeasuredDimension(this.f4279h, this.i);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.f4276e = (this.f4279h - this.i) / 2;
        this.f4277f = this.f4279h - this.i;
        this.n.setColor(this.f4272a.getResources().getColor(R.color.am));
        if (!this.m) {
            if (this.f4275d <= 0) {
                this.f4275d = 0;
                this.n.setColor(this.f4272a.getResources().getColor(R.color.f8329h));
                this.p.setColor(this.f4272a.getResources().getColor(R.color.f8329h));
            } else if (this.f4275d >= this.f4277f) {
                this.f4275d = this.f4277f;
                this.n.setColor(this.f4272a.getResources().getColor(R.color.am));
                this.p.setColor(this.f4272a.getResources().getColor(R.color.am));
            } else if (this.f4275d <= this.f4276e) {
                this.n.setColor(this.f4272a.getResources().getColor(R.color.f8329h));
                this.p.setColor(this.f4272a.getResources().getColor(R.color.f8329h));
            } else if (this.f4275d >= this.f4276e) {
                this.n.setColor(this.f4272a.getResources().getColor(R.color.am));
                this.p.setColor(this.f4272a.getResources().getColor(R.color.am));
            }
        } else if (this.l) {
            this.f4275d = this.f4277f;
            this.n.setColor(this.f4272a.getResources().getColor(R.color.am));
            this.p.setColor(this.f4272a.getResources().getColor(R.color.am));
        } else {
            this.f4275d = 0;
            this.n.setColor(this.f4272a.getResources().getColor(R.color.f8329h));
            this.p.setColor(this.f4272a.getResources().getColor(R.color.f8329h));
        }
        int i2 = (this.i - this.f4278g) / 2;
        Rect rect = new Rect();
        rect.left = 0;
        rect.top = i2;
        rect.right = this.f4279h;
        rect.bottom = this.f4278g + i2;
        canvas.drawRect(new RectF(0.0f, (float) i2, (float) this.f4279h, (float) (i2 + this.f4278g)), this.p);
        canvas.translate((float) this.f4275d, 0.0f);
        canvas.drawCircle((float) (this.i / 2), (float) (this.i / 2), (float) (this.i / 2), this.n);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.k = System.currentTimeMillis();
                this.m = false;
                this.f4274c = (int) motionEvent.getX();
                break;
            case 1:
            case 3:
                int x = (int) motionEvent.getX();
                long currentTimeMillis = System.currentTimeMillis();
                if (x - this.f4274c <= 8 && currentTimeMillis - this.k <= 500) {
                    if (this.l) {
                        this.l = false;
                        new a().start();
                    } else {
                        this.l = true;
                        new b().start();
                    }
                    if (this.f4273b != null) {
                        this.f4273b.a(this, this.l);
                        break;
                    }
                } else {
                    this.f4275d = x;
                    if (x <= this.f4276e) {
                        this.l = false;
                        new a().start();
                    } else {
                        this.l = true;
                        new b().start();
                    }
                    if (this.f4273b != null) {
                        this.f4273b.a(this, this.l);
                        break;
                    }
                }
                break;
            case 2:
                this.f4275d = (int) motionEvent.getX();
                invalidate();
                break;
        }
        return true;
    }

    private class b extends Thread {
        private b() {
        }

        public void run() {
            super.run();
            if (!MySliderButton.this.m) {
                while (MySliderButton.this.f4275d <= MySliderButton.this.f4277f) {
                    int unused = MySliderButton.this.f4275d = MySliderButton.this.f4275d + 3;
                    MySliderButton.this.postInvalidate();
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    private class a extends Thread {
        private a() {
        }

        public void run() {
            super.run();
            if (!MySliderButton.this.m) {
                while (MySliderButton.this.f4275d >= 0) {
                    int unused = MySliderButton.this.f4275d = MySliderButton.this.f4275d - 3;
                    MySliderButton.this.postInvalidate();
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    public void setOnChangedListener(c cVar) {
        this.f4273b = cVar;
    }

    public void setChecked(boolean z) {
        this.l = z;
    }

    public boolean getChecked() {
        return this.l;
    }

    public int getBackgroudHeight() {
        return this.f4278g;
    }

    public void setBackgroudHeight(int i2) {
        this.f4278g = i2;
    }

    public int getBackgroudWidth() {
        return this.f4279h;
    }

    public void setBackgroudWidth(int i2) {
        this.f4279h = i2;
    }

    public int getBtDiameter() {
        return this.i;
    }

    public void setBtDiameter(int i2) {
        this.i = i2;
    }

    public int getCircleStrokeWidth() {
        return this.j;
    }

    public void setCircleStrokeWidth(int i2) {
        this.j = i2;
    }
}
