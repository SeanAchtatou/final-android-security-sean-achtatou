package com.kingouser.com.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.v4.content.c;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import com.kingouser.com.R;
import com.pureapps.cleaner.util.f;

public class RootStatusRound extends View {

    /* renamed from: a  reason: collision with root package name */
    private float f4292a = 0.86499995f;

    /* renamed from: b  reason: collision with root package name */
    private float f4293b = 0.8f;

    /* renamed from: c  reason: collision with root package name */
    private String f4294c = "Have Root";

    /* renamed from: d  reason: collision with root package name */
    private String f4295d = "No Root";

    /* renamed from: e  reason: collision with root package name */
    private boolean f4296e = false;

    /* renamed from: f  reason: collision with root package name */
    private boolean f4297f = false;

    /* renamed from: g  reason: collision with root package name */
    private Context f4298g;

    /* renamed from: h  reason: collision with root package name */
    private float f4299h;
    private float i;
    private float j = 25.0f;
    private float k = 1.0f;
    private Typeface l;
    private Paint m;
    private Paint n;
    private Paint o;
    private Paint p;
    private Paint q;
    private Paint r;
    private Paint s;
    private Paint t;

    public RootStatusRound(Context context) {
        super(context);
        this.f4298g = context;
        f.a("1");
    }

    public RootStatusRound(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f4298g = context;
        f.a("2");
        a();
    }

    private void a() {
        f.a("initAtt");
        Point point = new Point();
        ((WindowManager) this.f4298g.getSystemService("window")).getDefaultDisplay().getSize(point);
        this.f4299h = (float) point.x;
        this.i = (float) point.y;
        this.j = (this.f4299h * this.j) / 720.0f;
        this.m = new Paint();
        this.m.setColor(c.c(this.f4298g, R.color.c1));
        this.m.setStrokeWidth(this.j);
        this.m.setAntiAlias(true);
        this.m.setStyle(Paint.Style.STROKE);
        this.n = new Paint();
        this.n.setColor(c.c(this.f4298g, R.color.c4));
        this.o = new Paint();
        this.o.setColor(c.c(this.f4298g, R.color.c2));
        this.o.setStrokeWidth(this.j);
        this.o.setAntiAlias(true);
        this.o.setStyle(Paint.Style.STROKE);
        this.p = new Paint();
        this.p.setColor(c.c(this.f4298g, R.color.c5));
        this.q = new Paint();
        this.q.setColor(c.c(this.f4298g, R.color.bx));
        this.q.setTextSize(50.0f);
        this.q.setTypeface(Typeface.DEFAULT_BOLD);
        this.s = new Paint();
        this.s.setColor(c.c(this.f4298g, R.color.bx));
        this.s.setTextSize(15.0f);
        this.s.setAntiAlias(true);
        this.r = new Paint();
        this.r.setColor(c.c(this.f4298g, R.color.c7));
        this.r.setTextSize(15.0f);
        this.r.setAntiAlias(true);
        this.l = Typeface.createFromAsset(this.f4298g.getAssets(), "fonts/booster_number_font.otf");
        this.t = new Paint();
        this.t.setColor(c.c(this.f4298g, R.color.bx));
        this.t.setTypeface(this.l);
        this.t.setTextSize(45.0f);
        this.t.setAntiAlias(true);
        f.a("initAtt2");
    }

    public RootStatusRound(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        f.a("3");
        this.f4298g = context;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f4296e) {
            b(canvas);
        } else {
            a(canvas);
        }
    }

    private void a(Canvas canvas) {
        canvas.drawColor(c.c(this.f4298g, R.color.c3));
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (width == 0.0f) {
            width = 300.0f;
        }
        float f2 = width / 2.0f;
        float f3 = width * this.f4292a;
        float f4 = f3 / 2.0f;
        RectF rectF = new RectF(-f4, -f4, f4, f4);
        RectF rectF2 = new RectF((-30.0f) / 2.0f, -f4, 30.0f / 2.0f, f4);
        canvas.save();
        canvas.translate(f2, f2);
        canvas.drawArc(rectF, 0.0f, 360.0f, false, this.p);
        canvas.rotate(45.0f);
        canvas.drawRect(rectF2, this.r);
        canvas.rotate(-45.0f);
        canvas.drawArc(rectF, 0.0f, 360.0f, false, this.o);
        a(canvas, this.f4295d, f3, this.t);
        canvas.restore();
    }

    private void b(Canvas canvas) {
        canvas.drawColor(c.c(this.f4298g, R.color.c3));
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (width == 0.0f) {
            width = 300.0f;
        }
        float f2 = width / 2.0f;
        float f3 = width * this.f4292a;
        float f4 = f3 / 2.0f;
        RectF rectF = new RectF(-f4, -f4, f4, f4);
        float f5 = 0.5f * f4;
        new RectF((-30.0f) / 2.0f, -f4, 30.0f / 2.0f, f4);
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.g1);
        RectF rectF2 = new RectF(-f5, ((-f5) / 110.0f) * 135.0f, f5, (f5 / 110.0f) * 135.0f);
        canvas.save();
        canvas.translate(f2, f2);
        canvas.drawArc(rectF, 0.0f, 360.0f, false, this.n);
        canvas.drawArc(rectF, 0.0f, 360.0f, false, this.m);
        canvas.drawBitmap(decodeResource, (Rect) null, rectF2, this.s);
        a(canvas, this.f4294c, f3, this.t);
        canvas.restore();
    }

    private void a(Canvas canvas, String str, float f2, Paint paint) {
        this.k = 1.0f;
        paint.setTextSize(this.k);
        float measureText = paint.measureText(str);
        while (measureText < 0.8f * f2) {
            this.k += 1.0f;
            paint.setTextSize(this.k);
            measureText = paint.measureText(str);
        }
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        canvas.drawText(str, -(measureText / 2.0f), -((fontMetrics.ascent + fontMetrics.descent) / 2.0f), paint);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        f.a("RootStatusRound onMeasure: windowWidth: " + this.f4299h + " | windowHeigth:" + this.i);
        if (this.f4299h > 0.0f) {
            int i4 = (int) (this.f4299h < this.i ? this.f4299h : this.i);
            setMeasuredDimension((int) (((float) i4) * this.f4293b), (int) (((float) i4) * this.f4293b));
        }
    }

    public void a(boolean z) {
        this.f4296e = z;
        postInvalidate();
    }
}
