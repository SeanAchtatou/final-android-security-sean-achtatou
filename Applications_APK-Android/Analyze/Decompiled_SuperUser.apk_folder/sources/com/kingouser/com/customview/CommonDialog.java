package com.kingouser.com.customview;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.kingouser.com.R;

public class CommonDialog extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    public a f4246a;

    /* renamed from: b  reason: collision with root package name */
    private ImageView f4247b;

    /* renamed from: c  reason: collision with root package name */
    private TextView f4248c;

    /* renamed from: d  reason: collision with root package name */
    private TextView f4249d;

    /* renamed from: e  reason: collision with root package name */
    private ImageView f4250e;

    /* renamed from: f  reason: collision with root package name */
    private ImageView f4251f;

    /* renamed from: g  reason: collision with root package name */
    private String f4252g;

    /* renamed from: h  reason: collision with root package name */
    private String f4253h;
    private int i = -1;
    private boolean j = false;

    public interface a {
        void a();

        void b();
    }

    public CommonDialog(Context context) {
        super(context, R.style.f8);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.aq);
        setCanceledOnTouchOutside(false);
        c();
        b();
        a();
    }

    private void a() {
        this.f4251f.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (CommonDialog.this.f4246a != null) {
                    CommonDialog.this.f4246a.a();
                }
            }
        });
        this.f4250e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (CommonDialog.this.f4246a != null) {
                    CommonDialog.this.f4246a.b();
                }
            }
        });
    }

    private void b() {
        if (!TextUtils.isEmpty(this.f4253h)) {
            this.f4248c.setText(this.f4253h);
            this.f4248c.setVisibility(0);
        } else {
            this.f4248c.setVisibility(8);
        }
        if (!TextUtils.isEmpty(this.f4252g)) {
            this.f4249d.setText(this.f4252g);
        }
        if (this.i != -1) {
            this.f4247b.setImageResource(this.i);
            this.f4247b.setVisibility(0);
            return;
        }
        this.f4247b.setVisibility(8);
    }

    public void show() {
        super.show();
        b();
    }

    private void c() {
        this.f4250e = (ImageView) findViewById(R.id.fo);
        this.f4251f = (ImageView) findViewById(R.id.fp);
        this.f4248c = (TextView) findViewById(R.id.fd);
        this.f4249d = (TextView) findViewById(R.id.fn);
        this.f4247b = (ImageView) findViewById(R.id.fm);
    }

    public CommonDialog a(a aVar) {
        this.f4246a = aVar;
        return this;
    }

    public CommonDialog a(String str) {
        this.f4252g = str;
        return this;
    }

    public CommonDialog b(String str) {
        this.f4253h = str;
        return this;
    }

    public CommonDialog a(boolean z) {
        this.j = z;
        return this;
    }

    public CommonDialog a(int i2) {
        this.i = i2;
        return this;
    }
}
