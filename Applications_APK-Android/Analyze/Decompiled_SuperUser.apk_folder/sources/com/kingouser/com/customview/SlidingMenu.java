package com.kingouser.com.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Scroller;

public class SlidingMenu extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    private View f4316a;

    /* renamed from: b  reason: collision with root package name */
    private View f4317b;

    /* renamed from: c  reason: collision with root package name */
    private int f4318c;

    /* renamed from: d  reason: collision with root package name */
    private float f4319d;

    /* renamed from: e  reason: collision with root package name */
    private float f4320e;

    /* renamed from: f  reason: collision with root package name */
    private Scroller f4321f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f4322g;

    public SlidingMenu(Context context) {
        this(context, null);
    }

    public SlidingMenu(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f4322g = false;
        this.f4321f = new Scroller(context);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.f4316a = getChildAt(0);
        this.f4317b = getChildAt(1);
        this.f4318c = this.f4316a.getLayoutParams().width;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.f4316a.measure(View.MeasureSpec.makeMeasureSpec(this.f4318c, 1073741824), i2);
        this.f4317b.measure(i, i2);
        setMeasuredDimension(View.MeasureSpec.getSize(i), View.MeasureSpec.getSize(i2));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth = this.f4316a.getMeasuredWidth();
        int measuredHeight = this.f4316a.getMeasuredHeight();
        Log.d("SlidingMenu", "width : " + measuredWidth);
        Log.d("SlidingMenu", "height : " + measuredHeight);
        this.f4316a.layout(-measuredWidth, 0, 0, measuredHeight);
        this.f4317b.layout(0, 0, this.f4317b.getMeasuredWidth(), this.f4317b.getMeasuredHeight());
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f4319d = motionEvent.getX();
                this.f4320e = motionEvent.getY();
                break;
            case 2:
                if (Math.abs(motionEvent.getX() - this.f4319d) > Math.abs(motionEvent.getY() - this.f4320e)) {
                    return true;
                }
                break;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        switch (motionEvent.getAction()) {
            case 0:
                this.f4319d = motionEvent.getX();
                this.f4320e = motionEvent.getY();
                break;
            case 1:
                if (((float) getScrollX()) <= ((float) (-this.f4316a.getMeasuredWidth())) / 2.0f) {
                    z = true;
                }
                a(z);
                break;
            case 2:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int i = (int) ((this.f4319d - x) + 0.5f);
                int scrollX = getScrollX() + i;
                if (scrollX < 0 && scrollX < (-this.f4316a.getMeasuredWidth())) {
                    scrollTo(-this.f4316a.getMeasuredWidth(), 0);
                } else if (scrollX > 0) {
                    scrollTo(0, 0);
                } else {
                    scrollBy(i, 0);
                }
                this.f4319d = x;
                this.f4320e = y;
                break;
        }
        return true;
    }

    private void a(boolean z) {
        int i = 600;
        this.f4322g = z;
        int measuredWidth = this.f4316a.getMeasuredWidth();
        int scrollX = getScrollX();
        if (!z) {
            int i2 = 0 - scrollX;
            int abs = Math.abs(i2) * 10;
            if (abs < 600) {
                i = abs;
            }
            this.f4321f.startScroll(scrollX, 0, i2, 0, i);
        } else {
            int i3 = (-measuredWidth) - scrollX;
            int abs2 = Math.abs(i3) * 10;
            if (abs2 < 600) {
                i = abs2;
            }
            this.f4321f.startScroll(scrollX, 0, i3, 0, i);
        }
        invalidate();
    }

    public void computeScroll() {
        if (this.f4321f.computeScrollOffset()) {
            scrollTo(this.f4321f.getCurrX(), 0);
            invalidate();
        }
    }
}
