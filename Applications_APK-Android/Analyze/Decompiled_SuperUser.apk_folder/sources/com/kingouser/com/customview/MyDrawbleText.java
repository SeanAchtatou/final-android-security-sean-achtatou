package com.kingouser.com.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import com.kingouser.com.R;

public class MyDrawbleText extends AppCompatTextView {

    /* renamed from: a  reason: collision with root package name */
    private int f4256a;

    /* renamed from: b  reason: collision with root package name */
    private int f4257b;

    /* renamed from: c  reason: collision with root package name */
    private int f4258c;

    /* renamed from: d  reason: collision with root package name */
    private int f4259d;

    /* renamed from: e  reason: collision with root package name */
    private int f4260e;

    /* renamed from: f  reason: collision with root package name */
    private int f4261f;

    /* renamed from: g  reason: collision with root package name */
    private int f4262g;

    /* renamed from: h  reason: collision with root package name */
    private int f4263h;
    private Bitmap i;
    private Context j;
    private boolean k = false;
    private Paint l = new Paint();
    private Paint m = new Paint();
    private Paint n = new Paint();

    public MyDrawbleText(Context context) {
        super(context);
        this.j = context;
        a(context);
    }

    public MyDrawbleText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public MyDrawbleText(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    private void a(Context context) {
        this.l.setAntiAlias(true);
        this.l.setColor(context.getResources().getColor(R.color.f8327f));
        this.m.setAntiAlias(true);
        this.m.setColor(context.getResources().getColor(R.color.bw));
        this.n.setAntiAlias(true);
        this.n.setColor(context.getResources().getColor(R.color.c2));
        setTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/Arial.ttf"));
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        this.f4261f = View.MeasureSpec.getSize(i2);
        super.onMeasure(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!(this.f4258c == 0 || this.f4257b == 0)) {
            this.i = BitmapFactory.decodeResource(getResources(), this.f4256a);
            Rect rect = new Rect();
            rect.left = (this.f4261f - this.f4263h) - this.f4257b;
            rect.top = (this.f4262g / 2) - (this.f4258c / 2);
            rect.right = this.f4261f - this.f4263h;
            rect.bottom = (this.f4262g / 2) + (this.f4258c / 2);
            canvas.drawBitmap(this.i, (Rect) null, rect, (Paint) null);
        }
        if (this.k) {
            CharSequence text = getText();
            if (!TextUtils.isEmpty(text)) {
                text.toString();
            }
        }
    }

    public int getDrawbleRightWidth() {
        return this.f4257b;
    }

    public void setDrawbleRightWidth(int i2) {
        this.f4257b = i2;
    }

    public int getDrawbleRightHeight() {
        return this.f4258c;
    }

    public void setDrawbleRightHeight(int i2) {
        this.f4258c = i2;
    }

    public int getDrawbleBottomWidth() {
        return this.f4259d;
    }

    public void setDrawbleBottomWidth(int i2) {
        this.f4259d = i2;
    }

    public int getDrawbleBottomHegith() {
        return this.f4260e;
    }

    public void setDrawbleBottomHegith(int i2) {
        this.f4260e = i2;
    }

    public int getBgWidth() {
        return this.f4261f;
    }

    public void setBgWidth(int i2) {
        this.f4261f = i2;
    }

    public int getBgHeight() {
        return this.f4262g;
    }

    public void setBgHeight(int i2) {
        this.f4262g = i2;
    }

    public int getRightMargin() {
        return this.f4263h;
    }

    public void setRightMargin(int i2) {
        this.f4263h = i2;
    }

    public int getRightDrawbleId() {
        return this.f4256a;
    }

    public void setRightDrawbleId(int i2) {
        this.f4256a = i2;
    }

    public void a() {
        this.k = true;
    }
}
