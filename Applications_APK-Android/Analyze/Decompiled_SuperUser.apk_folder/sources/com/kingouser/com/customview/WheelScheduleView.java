package com.kingouser.com.customview;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.duapps.ad.AdError;
import com.kingouser.com.R;
import java.util.Random;

public class WheelScheduleView extends View {

    /* renamed from: a  reason: collision with root package name */
    float f4323a = 0.0f;

    /* renamed from: b  reason: collision with root package name */
    float f4324b = 0.0f;

    /* renamed from: c  reason: collision with root package name */
    float f4325c = 0.8f;

    /* renamed from: d  reason: collision with root package name */
    int[] f4326d = {R.color.bv, R.color.b_, R.color.bq, R.color.bq, R.color.bp};

    /* renamed from: e  reason: collision with root package name */
    boolean f4327e = false;

    /* renamed from: f  reason: collision with root package name */
    private Paint f4328f;

    /* renamed from: g  reason: collision with root package name */
    private Paint f4329g;

    /* renamed from: h  reason: collision with root package name */
    private Paint f4330h;
    private Paint i;
    private Context j;
    private Canvas k;
    /* access modifiers changed from: private */
    public float l = 0.0f;
    /* access modifiers changed from: private */
    public float m = 0.0f;

    public WheelScheduleView(Context context) {
        super(context);
        this.j = context;
    }

    public WheelScheduleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.j = context;
    }

    private void a() {
        b();
        d();
    }

    public WheelScheduleView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.j = context;
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.k == null) {
            this.k = canvas;
            c();
            a();
        }
        Log.d("wyy", "mCanvas == canvas: " + (this.k == canvas));
        a(canvas);
    }

    private void a(Canvas canvas) {
        long currentTimeMillis = System.currentTimeMillis();
        float f2 = this.m;
        float f3 = this.l;
        Log.d("wyy", "drawRing sweepAngle:" + f2);
        a(canvas, ((float) getWidth()) * 0.8f, -90.0f, f2, f3, this.f4326d[0]);
        Log.d("wyy", "drawRing:" + (System.currentTimeMillis() - currentTimeMillis));
    }

    private void b() {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 360.0f);
        ofFloat.setDuration(1000L);
        ofFloat.setRepeatCount(-1);
        ofFloat.setInterpolator(new LinearInterpolator());
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float unused = WheelScheduleView.this.l = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                WheelScheduleView.this.postInvalidate();
            }
        });
        ofFloat.start();
    }

    private void a(Canvas canvas, float f2, float f3, float f4, float f5, int i2) {
        Log.d("wyy", "drawCustomizeArc: \t" + f3 + "\t|" + f4 + "\t|rotate");
        int width = getWidth();
        float f6 = f2 / 2.0f;
        RectF rectF = new RectF(-f6, -f6, f6, f6);
        Log.d("wyy", " RectF :" + rectF.toString());
        this.f4329g.setColor(a(i2));
        canvas.save();
        canvas.translate((float) (width / 2), (float) (width / 2));
        canvas.rotate(f5);
        canvas.drawArc(rectF, f3, f4, false, this.f4329g);
        canvas.restore();
    }

    private void c() {
        Log.d("wyy", "mCanvas init ");
        this.f4323a = (float) getWidth();
        this.f4324b = (float) getHeight();
        this.f4328f = new Paint();
        this.f4328f.setAntiAlias(true);
        this.f4328f.setColor(a((int) R.color.bo));
        this.f4328f.setStrokeWidth(10.0f);
        this.f4328f.setStyle(Paint.Style.STROKE);
        this.f4329g = new Paint();
        this.f4329g.setAntiAlias(true);
        this.f4329g.setColor(a((int) R.color.bb));
        this.f4329g.setStrokeWidth(30.0f);
        this.f4329g.setStyle(Paint.Style.STROKE);
        this.i = new Paint();
        this.i.setTextSize(15.0f);
        this.i.setAntiAlias(true);
        this.i.setColor(a((int) R.color.dy));
        this.f4330h = new Paint();
        this.f4330h.setTextSize(15.0f);
        this.f4330h.setAntiAlias(true);
        this.f4330h.setColor(a((int) R.color.br));
    }

    private int a(int i2) {
        if (this.j != null) {
            return this.j.getResources().getColor(i2);
        }
        return -16777216;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        setMeasuredDimension(AdError.NETWORK_ERROR_CODE, AdError.NETWORK_ERROR_CODE);
    }

    public int getRandom() {
        return (new Random().nextInt(500) % 451) + 50;
    }

    private void d() {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 180.0f);
        ofFloat.setDuration(4000L);
        ofFloat.setRepeatCount(0);
        ofFloat.setInterpolator(new LinearInterpolator());
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float unused = WheelScheduleView.this.m = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                Log.d("wyy", "ArcLenthAdd :" + WheelScheduleView.this.m);
                WheelScheduleView.this.postInvalidate();
            }
        });
        ofFloat.start();
    }
}
