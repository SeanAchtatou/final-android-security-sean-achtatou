package com.kingouser.com.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import h.a.a;

public class RotateLoading extends View {

    /* renamed from: a  reason: collision with root package name */
    private Paint f4300a;

    /* renamed from: b  reason: collision with root package name */
    private RectF f4301b;

    /* renamed from: c  reason: collision with root package name */
    private RectF f4302c;

    /* renamed from: d  reason: collision with root package name */
    private int f4303d = 10;

    /* renamed from: e  reason: collision with root package name */
    private int f4304e = 190;

    /* renamed from: f  reason: collision with root package name */
    private float f4305f;

    /* renamed from: g  reason: collision with root package name */
    private int f4306g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f4307h = true;
    private int i;
    private boolean j = false;
    private int k;
    private int l;
    private float m;

    public RotateLoading(Context context) {
        super(context);
        a(context, (AttributeSet) null);
    }

    public RotateLoading(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public RotateLoading(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        this.k = -1;
        this.f4306g = a(context, 6.0f);
        this.i = a(getContext(), 2.0f);
        this.l = 10;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.C0102a.RotateLoading);
            this.k = obtainStyledAttributes.getColor(1, -1);
            this.f4306g = obtainStyledAttributes.getDimensionPixelSize(0, a(context, 6.0f));
            this.i = obtainStyledAttributes.getInt(2, 2);
            this.l = obtainStyledAttributes.getInt(3, 10);
            obtainStyledAttributes.recycle();
        }
        this.m = (float) (this.l / 4);
        this.f4300a = new Paint();
        this.f4300a.setColor(this.k);
        this.f4300a.setAntiAlias(true);
        this.f4300a.setStyle(Paint.Style.STROKE);
        this.f4300a.setStrokeWidth((float) this.f4306g);
        this.f4300a.setStrokeCap(Paint.Cap.ROUND);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.f4305f = 10.0f;
        this.f4301b = new RectF((float) (this.f4306g * 2), (float) (this.f4306g * 2), (float) (i2 - (this.f4306g * 2)), (float) (i3 - (this.f4306g * 2)));
        this.f4302c = new RectF((float) ((this.f4306g * 2) + this.i), (float) ((this.f4306g * 2) + this.i), (float) ((i2 - (this.f4306g * 2)) + this.i), (float) ((i3 - (this.f4306g * 2)) + this.i));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        boolean z = false;
        super.onDraw(canvas);
        if (this.j) {
            this.f4300a.setColor(Color.parseColor("#1a000000"));
            canvas.drawArc(this.f4302c, (float) this.f4303d, this.f4305f, false, this.f4300a);
            canvas.drawArc(this.f4302c, (float) this.f4304e, this.f4305f, false, this.f4300a);
            this.f4300a.setColor(this.k);
            canvas.drawArc(this.f4301b, (float) this.f4303d, this.f4305f, false, this.f4300a);
            canvas.drawArc(this.f4301b, (float) this.f4304e, this.f4305f, false, this.f4300a);
            this.f4303d += this.l;
            this.f4304e += this.l;
            if (this.f4303d > 360) {
                this.f4303d -= 360;
            }
            if (this.f4304e > 360) {
                this.f4304e -= 360;
            }
            if (this.f4307h) {
                if (this.f4305f < 160.0f) {
                    this.f4305f += this.m;
                    invalidate();
                }
            } else if (this.f4305f > ((float) this.l)) {
                this.f4305f -= 2.0f * this.m;
                invalidate();
            }
            if (this.f4305f >= 160.0f || this.f4305f <= 10.0f) {
                if (!this.f4307h) {
                    z = true;
                }
                this.f4307h = z;
                invalidate();
            }
        }
    }

    public void setLoadingColor(int i2) {
        this.k = i2;
    }

    public int getLoadingColor() {
        return this.k;
    }

    public int a(Context context, float f2) {
        return (int) TypedValue.applyDimension(1, f2, context.getResources().getDisplayMetrics());
    }
}
