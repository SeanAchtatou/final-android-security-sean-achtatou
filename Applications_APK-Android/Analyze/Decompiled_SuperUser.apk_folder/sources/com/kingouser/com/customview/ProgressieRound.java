package com.kingouser.com.customview;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.v4.content.c;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import com.kingouser.com.R;
import com.pureapps.cleaner.util.f;

public class ProgressieRound extends View {
    private Paint A;
    private Paint B;
    /* access modifiers changed from: private */
    public float C = 0.0f;

    /* renamed from: a  reason: collision with root package name */
    boolean f4282a = false;

    /* renamed from: b  reason: collision with root package name */
    boolean f4283b = false;

    /* renamed from: c  reason: collision with root package name */
    private Context f4284c;

    /* renamed from: d  reason: collision with root package name */
    private float f4285d;

    /* renamed from: e  reason: collision with root package name */
    private float f4286e;

    /* renamed from: f  reason: collision with root package name */
    private float f4287f = 25.0f;

    /* renamed from: g  reason: collision with root package name */
    private float f4288g = 1.0f;

    /* renamed from: h  reason: collision with root package name */
    private float f4289h = 0.86499995f;
    private float i = 0.8f;
    private float j;
    private float k;
    private float l;
    private float m;
    private float n;
    private float o = -1.0f;
    private float p = -1.0f;
    private float q = 0.25f;
    private float[] r = {-90.0f, 360.0f};
    private RectF s;
    private Typeface t;
    private Paint u;
    private Paint v;
    private Paint w;
    private Paint x;
    private Paint y;
    private Paint z;

    public ProgressieRound(Context context) {
        super(context);
        this.f4284c = context;
        f.a("1");
    }

    public ProgressieRound(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f4284c = context;
        f.a("2");
        a();
    }

    private void a() {
        Point point = new Point();
        ((WindowManager) this.f4284c.getSystemService("window")).getDefaultDisplay().getSize(point);
        this.f4285d = (float) point.x;
        this.f4286e = (float) point.y;
        this.f4287f = (this.f4285d * this.f4287f) / 720.0f;
        this.u = new Paint();
        this.u.setColor(c.c(this.f4284c, R.color.by));
        this.u.setStrokeWidth(this.f4287f);
        this.u.setAntiAlias(true);
        this.u.setStyle(Paint.Style.STROKE);
        this.v = new Paint();
        this.v.setColor(c.c(this.f4284c, R.color.am));
        this.v.setStrokeWidth(this.f4287f);
        this.v.setAntiAlias(true);
        this.v.setStyle(Paint.Style.STROKE);
        this.w = new Paint();
        this.w.setColor(c.c(this.f4284c, R.color.bz));
        this.w.setStrokeWidth(this.f4287f);
        this.w.setAntiAlias(true);
        this.x = new Paint();
        this.x.setColor(c.c(this.f4284c, R.color.bx));
        this.x.setTextSize(50.0f);
        this.x.setTypeface(Typeface.DEFAULT_BOLD);
        this.z = new Paint();
        this.z.setColor(c.c(this.f4284c, R.color.bx));
        this.z.setTextSize(15.0f);
        this.z.setAntiAlias(true);
        this.y = new Paint();
        this.y.setColor(c.c(this.f4284c, R.color.c7));
        this.y.setTextSize(15.0f);
        this.y.setAntiAlias(true);
        this.t = Typeface.createFromAsset(this.f4284c.getAssets(), "fonts/booster_number_font.otf");
        this.A = new Paint();
        this.A.setColor(c.c(this.f4284c, R.color.bx));
        this.A.setTypeface(this.t);
        this.A.setTextSize(45.0f);
        this.A.setAntiAlias(true);
        this.B = new Paint();
        this.B.setColor(c.c(this.f4284c, R.color.bx));
        this.B.setTypeface(this.t);
        this.B.setAntiAlias(true);
    }

    public ProgressieRound(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        f.a("3");
        this.f4284c = context;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f4283b) {
            b();
            this.f4283b = true;
        }
        a(canvas);
    }

    private void b() {
        float width = (float) getWidth();
        float height = (float) getHeight();
        this.k = 300.0f;
        if (width != 0.0f) {
            this.k = width;
        }
        this.n = this.k / 2.0f;
        this.l = this.k * this.f4289h;
        this.m = this.l / 2.0f;
        this.s = new RectF(-this.m, -this.m, this.m, this.m);
        f.a("ovalRectF: " + this.s);
    }

    private void a(Canvas canvas) {
        System.currentTimeMillis();
        float f2 = (this.C / 100.0f) * 360.0f;
        int i2 = (int) f2;
        int i3 = (int) f2;
        int i4 = (int) this.C;
        canvas.save();
        canvas.drawColor(c.c(this.f4284c, R.color.c3));
        canvas.translate(this.n, this.n);
        canvas.drawArc(this.s, this.r[0], this.r[1], false, this.u);
        canvas.drawArc(this.s, this.r[0], (float) i3, true, this.w);
        canvas.drawArc(this.s, this.r[0], (float) i2, false, this.v);
        a(canvas, "" + i4, this.l, this.A);
        canvas.restore();
        System.currentTimeMillis();
    }

    private void a(Canvas canvas, String str, float f2, Paint paint) {
        System.currentTimeMillis();
        float[] a2 = a(str, f2, paint);
        float f3 = a2[1];
        canvas.drawText(str, -(f3 / 2.0f), -(a2[2] / 2.0f), paint);
        canvas.drawText("%", (-(f3 / 2.0f)) + (this.o * ((float) str.length())) + 5.0f, -(this.p / 2.0f), this.B);
        System.currentTimeMillis();
    }

    private float[] a(String str, float f2, Paint paint) {
        float f3 = 0.6f * f2;
        paint.setTextSize(f3);
        this.B.setTextSize(this.q * f3);
        if (this.o == -1.0f || this.p == -1.0f) {
            this.o = paint.measureText("1");
            Paint.FontMetrics fontMetrics = paint.getFontMetrics();
            this.p = fontMetrics.ascent + fontMetrics.descent;
        }
        return new float[]{f3, this.o * ((float) str.length()), this.p};
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.f4285d > 0.0f) {
            this.j = ((float) ((int) (this.f4285d < this.f4286e ? this.f4285d : this.f4286e))) * this.i;
            setMeasuredDimension((int) this.j, (int) this.j);
        }
    }

    public boolean a(int i2) {
        if (this.f4282a) {
            return false;
        }
        b(i2);
        postInvalidate();
        return true;
    }

    private void b(final int i2) {
        this.f4282a = true;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, (float) i2);
        ofFloat.setDuration(500L);
        ofFloat.setRepeatCount(0);
        ofFloat.setInterpolator(new LinearInterpolator());
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                float unused = ProgressieRound.this.C = floatValue;
                ProgressieRound.this.postInvalidate();
                if (floatValue == ((float) i2)) {
                    ProgressieRound.this.f4282a = false;
                }
            }
        });
        ofFloat.start();
    }
}
