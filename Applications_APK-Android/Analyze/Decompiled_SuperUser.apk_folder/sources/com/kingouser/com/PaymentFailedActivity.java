package com.kingouser.com;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import butterknife.OnClick;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.pureapps.cleaner.analytic.a;

public class PaymentFailedActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.cd);
        ActionBar f2 = f();
        if (f2 != null) {
            f2.a(true);
        }
        j();
    }

    private void j() {
        TextView textView = (TextView) findViewById(R.id.kw);
        Typeface createFromAsset = Typeface.createFromAsset(getAssets(), "fonts/booster_number_font.otf");
        textView.setTypeface(createFromAsset);
        textView.setTypeface(createFromAsset);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a.a(this).b(FirebaseAnalytics.getInstance(this), "PaymentFailedPage");
    }

    @OnClick({2131624366})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.kx /*2131624366*/:
                k();
                return;
            default:
                return;
        }
    }

    private void k() {
        a.a(this).c(this.o, "BtnPaymentFaliedTryAgainClick");
        finish();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
