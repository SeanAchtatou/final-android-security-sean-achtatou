package com.kingouser.com;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import com.daps.weather.notification.DapWeatherNotification;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.kingouser.com.customview.MyDrawbleText;
import com.kingouser.com.customview.MySeekBar;
import com.kingouser.com.entity.EnableUninstallEntity;
import com.kingouser.com.util.DeviceInfoUtils;
import com.kingouser.com.util.HttpUtils;
import com.kingouser.com.util.LanguageUtils;
import com.kingouser.com.util.MyLog;
import com.kingouser.com.util.MySharedPreference;
import com.kingouser.com.util.PackageUtils;
import com.kingouser.com.util.PermissionUtils;
import com.kingouser.com.util.RC4EncodeUtils;
import com.kingouser.com.util.ShellUtils;
import com.kingouser.com.util.SuHelper;
import com.lody.virtual.helper.utils.FileUtils;
import com.pureapps.cleaner.NotificationThemeActivity;
import com.pureapps.cleaner.analytic.a;
import com.pureapps.cleaner.manager.h;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.util.i;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.Executors;
import me.everything.android.ui.overscroll.g;

public class SettingsActivity extends BaseActivity {
    @BindDimen(R.dimen.f8283b)
    int bgHeight;
    @BindDimen(R.dimen.f8284c)
    int bgWidth;
    @BindDimen(R.dimen.f8284c)
    int drawbleBottomWidth;
    @BindDimen(R.dimen.f8285d)
    int drawbleRightHeight;
    @BindDimen(R.dimen.f8285d)
    int drawbleRightWidth;
    @BindView(R.id.ee)
    SwitchCompat ivSwipe;
    @BindView(R.id.ec)
    SwitchCompat ivToast;
    @BindView(R.id.ef)
    SwitchCompat ivWeather;
    @BindView(R.id.co)
    ScrollView mScrollview;
    @BindView(R.id.ej)
    MySeekBar mySeeekBar;
    int n = 1;
    private final int p = FileUtils.FileMode.MODE_IRUSR;
    /* access modifiers changed from: private */
    public Context q;
    /* access modifiers changed from: private */
    public Dialog r;
    @BindDimen(R.dimen.f8286e)
    int rightMargin;
    private Button s;
    private Button t;
    @BindView(R.id.ek)
    MyDrawbleText tvLanguage;
    @BindView(R.id.eg)
    MyDrawbleText tvNotificationTool;
    @BindView(R.id.eh)
    MyDrawbleText tvPromptTimespan;
    @BindView(R.id.el)
    MyDrawbleText tvRemovePermission;
    @BindView(R.id.ed)
    MyDrawbleText tvSwipeText;
    @BindView(R.id.eb)
    MyDrawbleText tvToastNotification;
    @BindView(R.id.ei)
    TextView tv_time;
    private TextView u;
    private TextView v;
    private final int w = 1;
    /* access modifiers changed from: private */
    public Handler x = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 96:
                    SettingsActivity.this.tvRemovePermission.setVisibility(0);
                    return;
                case 97:
                    SettingsActivity.this.tvRemovePermission.setVisibility(8);
                    return;
                default:
                    return;
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.ac);
        this.q = getApplicationContext();
        a.a(this).b(this.o, "Settings");
        j();
        o();
        p();
    }

    private void j() {
        ActionBar f2 = f();
        f.a("supportActionBar is null " + (f2 == null));
        if (f2 != null) {
            f2.a(true);
        }
        g.a(this.mScrollview);
        this.mySeeekBar.setOnSeekBarChangedListener(new MySeekBar.a() {
            public void a(int i) {
                int i2 = i + 5;
                MySharedPreference.setRequestDialogTime(SettingsActivity.this.q, i2);
                if (com.kingouser.com.util.FileUtils.checkFileExist(SettingsActivity.this.q, SettingsActivity.this.q.getFilesDir() + "/" + "supersu.cfg")) {
                    PermissionUtils.udeAppFromeCfg(SettingsActivity.this.q, "default", "wait", i2 + "");
                } else {
                    PermissionUtils.createPrePermission(SettingsActivity.this.q, MySharedPreference.getRequestDialogTimes(SettingsActivity.this.q, 15));
                    PermissionUtils.udeAppFromeCfg(SettingsActivity.this.q, "default", "wait", i2 + "");
                }
                a.a(SettingsActivity.this.q).d(FirebaseAnalytics.getInstance(SettingsActivity.this.q), "" + i2);
            }

            public void b(int i) {
                SettingsActivity.this.tv_time.setText((i + 5) + "s");
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MyLog.e("PermissionService", "执行了AboutFragment的onCreateView()方法。。。。。。。。。。。。。。。。。。");
        if (!MySharedPreference.getAboutActivityLocalLanguage(this.q, "").equalsIgnoreCase(LanguageUtils.getLocalLanguage())) {
            MySharedPreference.setAboutActivityLocalLanguage(this.q, LanguageUtils.getLocalLanguage());
        }
        n();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick({2131624131, 2131624132, 2131624127})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.eg /*2131624127*/:
                NotificationThemeActivity.a(this);
                return;
            case R.id.eh /*2131624128*/:
            case R.id.ei /*2131624129*/:
            case R.id.ej /*2131624130*/:
            default:
                return;
            case R.id.ek /*2131624131*/:
                LanguageActivity.a(this.q);
                a.a(this.q).c(FirebaseAnalytics.getInstance(this.q), "BtnSettingsLanguageClick");
                return;
            case R.id.el /*2131624132*/:
                a.a(this.q).c(this.o, "BtnSettingsRemoveClick");
                if (ShellUtils.checkSuVerison()) {
                    a.a(this.q).c(FirebaseAnalytics.getInstance(this.q), "BtnSettingsRemoveYesPermissionClick");
                    k();
                    return;
                }
                a.a(this.q).c(FirebaseAnalytics.getInstance(this.q), "BtnSettingsRemoveNoPermissionClick");
                Toast.makeText(this.q, this.q.getResources().getString(R.string.d0), 0).show();
                return;
        }
    }

    private void k() {
        this.r = new Dialog(this, R.style.lu);
        View inflate = LayoutInflater.from(this.q).inflate((int) R.layout.ch, (ViewGroup) null);
        this.r.requestWindowFeature(1);
        this.r.setContentView(inflate);
        this.r.setCancelable(true);
        a(inflate);
        m();
        l();
        this.r.show();
    }

    private void a(View view) {
        this.s = (Button) view.findViewById(R.id.ff);
        this.t = (Button) view.findViewById(R.id.lo);
        this.u = (TextView) view.findViewById(R.id.fd);
        this.v = (TextView) view.findViewById(R.id.gp);
    }

    private void l() {
        com.kingouser.com.util.FileUtils.copyFileFromAssets(this.q, this.q.getFilesDir() + "/busybox", "busybox");
        com.kingouser.com.util.FileUtils.copyFileFromAssets(this.q, this.q.getFilesDir() + "/ddexe", "ddexe");
        com.kingouser.com.util.FileUtils.copyFileFromAssets(this.q, this.q.getFilesDir() + "/libsupol.so", "libsupol.so");
        com.kingouser.com.util.FileUtils.copyFileFromAssets(this.q, this.q.getFilesDir() + "/supolicy", "supolicy");
        com.kingouser.com.util.FileUtils.copyFileFromAssets(this.q, this.q.getFilesDir() + "/99SuperSUDaemon", "99SuperSUDaemon");
        com.kingouser.com.util.FileUtils.copyFileFromAssets(this.q, this.q.getFilesDir() + "/install-recovery.sh", "install-recovery.sh");
        this.u.setTypeface(Typeface.createFromAsset(this.q.getAssets(), "fonts/Arial.ttf"));
        this.v.setTypeface(Typeface.createFromAsset(this.q.getAssets(), "fonts/Arial.ttf"));
        this.s.setTypeface(Typeface.createFromAsset(this.q.getAssets(), "fonts/Arial.ttf"));
        this.t.setTypeface(Typeface.createFromAsset(this.q.getAssets(), "fonts/Arial.ttf"));
        this.u.setTextSize(DeviceInfoUtils.getNomalTextSize(this.q, 28));
        this.v.setTextSize(DeviceInfoUtils.getNomalTextSize(this.q, 24));
        this.s.setTextSize(DeviceInfoUtils.getNomalTextSize(this.q, 20));
        this.t.setTextSize(DeviceInfoUtils.getNomalTextSize(this.q, 20));
    }

    private void m() {
        this.s.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                a.a(SettingsActivity.this.q).c(SettingsActivity.this.o, "BtnSettingsDialogRemovePermissionCancel");
                SettingsActivity.this.r.dismiss();
            }
        });
        this.t.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                a.a(SettingsActivity.this.q).c(SettingsActivity.this.o, "BtnSettingsDialogRemovePermissionContinue");
                SuHelper.testMkdevsh(SettingsActivity.this.q);
            }
        });
    }

    private void n() {
        this.tvToastNotification.setText(this.q.getResources().getString(R.string.ez));
        this.tvPromptTimespan.setText(this.q.getResources().getString(R.string.dt));
        this.tvLanguage.setText(this.q.getResources().getString(R.string.ab));
        this.tvRemovePermission.setText(this.q.getResources().getString(R.string.ad));
    }

    private void o() {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            public void run() {
                String str;
                URL url;
                String localLanguage = LanguageUtils.getLocalLanguage();
                String localDefault = LanguageUtils.getLocalDefault();
                String appVersion = PackageUtils.getAppVersion(SettingsActivity.this.q);
                if ("official".equalsIgnoreCase("official")) {
                    str = RC4EncodeUtils.decry_RC4("c9b07620ffea12487b0fc403af0cb28e0acb87ef4fce4291975010e0a13d06900accb764cf99b7dbbbcea712dc8e0ba0", "string_key") + "?channel=" + "OffcialSite" + "&lang-str=" + localDefault + "&lang=" + localLanguage + "&client-version=" + appVersion;
                } else {
                    str = RC4EncodeUtils.decry_RC4("c9b07620ffea120a274f9c5bfa51b2d44d94d2b01486028fc4101cbfa96204d60fc2b676d389b691aa8eaa1bdb8f", "string_key") + "?channel=" + "OffcialSite" + "&lang-str=" + localDefault + "&lang=" + localLanguage + "&client-version=" + appVersion;
                }
                try {
                    url = new URL(str);
                } catch (MalformedURLException e2) {
                    e2.printStackTrace();
                    url = null;
                }
                HashMap hashMap = new HashMap();
                hashMap.put("key", "enable_uninstall");
                hashMap.put("action", "GET");
                String checkStatePost = HttpUtils.checkStatePost(SettingsActivity.this.q, url, hashMap, "utf-8");
                if (!TextUtils.isEmpty(checkStatePost)) {
                    Message message = new Message();
                    if (((EnableUninstallEntity) new Gson().fromJson(checkStatePost, EnableUninstallEntity.class)).isEnable_uninstall()) {
                        message.what = 96;
                    } else {
                        message.what = 97;
                    }
                    SettingsActivity.this.x.sendMessage(message);
                }
            }
        });
    }

    @OnCheckedChanged({2131624125, 2131624126, 2131624123})
    public void OnCheckedChange(CompoundButton compoundButton, boolean z) {
        boolean z2 = true;
        switch (compoundButton.getId()) {
            case R.id.ec /*2131624123*/:
                MySharedPreference.setWheaterToast(this, z);
                return;
            case R.id.ed /*2131624124*/:
            default:
                return;
            case R.id.ee /*2131624125*/:
                if (this.ivSwipe.isPressed()) {
                    if (!z) {
                        h.a(new Intent("com.plug.swipe.close"));
                        i.a(this).c(false);
                    } else if (h.c(this)) {
                        if (Build.VERSION.SDK_INT < 23 || Settings.canDrawOverlays(this)) {
                            i.a(this).c(true);
                            h.a(new Intent("com.plug.swipe.open"));
                        } else {
                            startActivityForResult(new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.parse("package:" + getPackageName())), 1);
                        }
                    } else if (h.b()) {
                        this.ivSwipe.setChecked(false);
                        Toast.makeText(this, (int) R.string.gn, 0).show();
                    } else {
                        h.b(this);
                        this.ivSwipe.setChecked(false);
                        Toast.makeText(this, (int) R.string.gn, 0).show();
                    }
                    a.a(this.q).b(FirebaseAnalytics.getInstance(this.q), z);
                    return;
                }
                return;
            case R.id.ef /*2131624126*/:
                if (!this.ivWeather.isPressed()) {
                    return;
                }
                if (z) {
                    if (Build.VERSION.SDK_INT >= 23 && checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") != 0) {
                        requestPermissions(new String[]{"android.permission.ACCESS_FINE_LOCATION"}, FileUtils.FileMode.MODE_IRUSR);
                        Toast.makeText(this, (int) R.string.fq, 0).show();
                        z2 = false;
                    }
                    if (z2) {
                        i.a(this).a(z);
                        DapWeatherNotification.getInstance(this).setPushElevatingTemperature(z);
                        DapWeatherNotification.getInstance(this).setPushTodayTomorrowWeather(z);
                        return;
                    }
                    this.ivWeather.setChecked(false);
                    return;
                }
                i.a(this).a(z);
                DapWeatherNotification.getInstance(this).setPushElevatingTemperature(z);
                DapWeatherNotification.getInstance(this).setPushTodayTomorrowWeather(z);
                return;
        }
    }

    private void p() {
        if (MySharedPreference.getWheaterToast(this.q, true)) {
            this.ivToast.setChecked(true);
        } else {
            this.ivToast.setChecked(false);
        }
        int requestDialogTimes = MySharedPreference.getRequestDialogTimes(this.q, 15);
        this.tv_time.setText(requestDialogTimes + "s");
        q();
        if (!i.a(this).h() || !h.c(this)) {
            this.ivSwipe.setChecked(false);
        } else {
            this.ivSwipe.setChecked(true);
        }
        this.ivWeather.setChecked(i.a(this).c());
        this.mySeeekBar.setBackgroundHeight(this.q.getResources().getDimension(R.dimen.p));
        this.mySeeekBar.setBtDiameter((float) ((int) this.q.getResources().getDimension(R.dimen.o)));
        this.mySeeekBar.setMaxProgress(25);
        this.mySeeekBar.setProgress(requestDialogTimes - 5);
        a(this.tvRemovePermission, this.drawbleRightWidth, this.drawbleRightHeight, this.drawbleBottomWidth, this.n, this.bgWidth, this.bgHeight, this.rightMargin);
        a(this.tvLanguage, this.drawbleRightWidth, this.drawbleRightHeight, this.drawbleBottomWidth, this.n, this.bgWidth, this.bgHeight, this.rightMargin);
        a(this.tvNotificationTool, this.drawbleRightWidth, this.drawbleRightHeight, this.drawbleBottomWidth, this.n, this.bgWidth, this.bgHeight, this.rightMargin);
        a(this.tvToastNotification, 0, 0, 0, 0, (int) this.q.getResources().getDimension(R.dimen.co), (int) this.q.getResources().getDimension(R.dimen.f8287f), this.rightMargin);
        a(this.tvPromptTimespan, 0, 0, 0, 0, (int) this.q.getResources().getDimension(R.dimen.co), (int) this.q.getResources().getDimension(R.dimen.f8287f), this.rightMargin);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        switch (i) {
            case FileUtils.FileMode.MODE_IRUSR /*256*/:
                if (iArr[0] == 0) {
                    this.ivWeather.setChecked(true);
                    i.a(this).a(true);
                    DapWeatherNotification.getInstance(this).setPushElevatingTemperature(true);
                    DapWeatherNotification.getInstance(this).setPushTodayTomorrowWeather(true);
                    return;
                }
                return;
            default:
                super.onRequestPermissionsResult(i, strArr, iArr);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1 && Build.VERSION.SDK_INT >= 23) {
            if (Settings.canDrawOverlays(this)) {
                this.ivSwipe.setChecked(true);
                i.a(this).c(true);
                h.a(new Intent("com.plug.swipe.open"));
                return;
            }
            i.a(this).c(false);
            this.ivSwipe.setChecked(false);
        }
    }

    private void q() {
        if (!DeviceInfoUtils.isTablet(this.q)) {
            this.tvToastNotification.setTextSize(DeviceInfoUtils.getTextSize(this.q, 42));
            this.tvPromptTimespan.setTextSize(DeviceInfoUtils.getTextSize(this.q, 42));
            this.tvRemovePermission.setTextSize(DeviceInfoUtils.getTextSize(this.q, 42));
            this.tvLanguage.setTextSize(DeviceInfoUtils.getTextSize(this.q, 42));
        }
    }

    public void a(MyDrawbleText myDrawbleText, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        myDrawbleText.setRightDrawbleId(R.drawable.ex);
        myDrawbleText.setBgHeight(i6);
        myDrawbleText.setDrawbleRightWidth(i);
        myDrawbleText.setDrawbleRightHeight(i2);
        myDrawbleText.setDrawbleBottomWidth(i3);
        myDrawbleText.setDrawbleBottomHegith(i4);
        myDrawbleText.setRightMargin(i7);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
