package com.kingouser.com.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.entity.UidPolicy;
import com.kingouser.com.util.FileUtils;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import java.util.ArrayList;
import java.util.Iterator;

public class KingoDatabaseHelper extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private Context f4335a;

    public KingoDatabaseHelper(Context context) {
        super(context, "su.sqlite", (SQLiteDatabase.CursorFactory) null, 6);
        this.f4335a = context;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        onUpgrade(sQLiteDatabase, 0, 6);
        FileUtils.write(this.f4335a.getFilesDir() + "/request_log", System.currentTimeMillis() + ":init the su.sqlite;", true);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        int i3;
        FileUtils.write(this.f4335a.getFilesDir() + "/request_log", System.currentTimeMillis() + ":onUpgrade;", true);
        if (i == 0) {
            sQLiteDatabase.execSQL("create table if not exists uid_policy (logging integer, desired_name text, username text, policy text, until integer, command text not null, uid integer, desired_uid integer, package_name text, name text, primary key(uid, command, desired_uid))");
            i3 = 4;
        } else {
            i3 = i;
        }
        if (i3 == 1 || i3 == 2) {
            sQLiteDatabase.execSQL("create table if not exists settings (key TEXT PRIMARY KEY, value TEXT)");
            i3 = 3;
        }
        if (i3 == 3) {
            SQLiteDatabase writableDatabase = new KingouserDatabaseHelper(this.f4335a).getWritableDatabase();
            ArrayList<LogEntry> a2 = KingouserDatabaseHelper.a(this.f4335a, sQLiteDatabase);
            writableDatabase.beginTransaction();
            try {
                Iterator<LogEntry> it = a2.iterator();
                while (it.hasNext()) {
                    KingouserDatabaseHelper.a(writableDatabase, it.next());
                }
                Cursor query = sQLiteDatabase.query("settings", null, null, null, null, null, null);
                while (query.moveToNext()) {
                    String string = query.getString(query.getColumnIndex("key"));
                    String string2 = query.getString(query.getColumnIndex(FirebaseAnalytics.Param.VALUE));
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("key", string);
                    contentValues.put(FirebaseAnalytics.Param.VALUE, string2);
                    writableDatabase.replace("settings", null, contentValues);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            } finally {
                writableDatabase.setTransactionSuccessful();
                writableDatabase.endTransaction();
            }
            sQLiteDatabase.execSQL("drop table if exists log");
            sQLiteDatabase.execSQL("drop table if exists settings");
            i3 = 4;
        }
        if (i3 == 4) {
            sQLiteDatabase.execSQL("alter table uid_policy add column notification integer");
            sQLiteDatabase.execSQL("update uid_policy set notification = 1");
            i3 = 5;
        }
        if (i3 == 5) {
            Iterator<UidPolicy> it2 = a(sQLiteDatabase).iterator();
            while (it2.hasNext()) {
                a(sQLiteDatabase, it2.next(), this.f4335a);
            }
        }
    }

    public static void a(Context context, UidPolicy uidPolicy) {
        uidPolicy.getPackageInfo(context);
        if (2000 != uidPolicy.uid) {
            a(new KingoDatabaseHelper(context).getWritableDatabase(), uidPolicy, context);
        }
    }

    public static void a(SQLiteDatabase sQLiteDatabase, UidPolicy uidPolicy, Context context) {
        if (2000 != uidPolicy.uid) {
            String str = uidPolicy.name;
            String str2 = uidPolicy.packageName;
            String str3 = uidPolicy.username;
            String str4 = uidPolicy.desiredName;
            int i = uidPolicy.uid;
            int i2 = uidPolicy.until;
            int i3 = uidPolicy.desiredUid;
            if (a(context, i) != null) {
                a(i + "", i2, context, uidPolicy.policy);
                return;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("logging", Boolean.valueOf(uidPolicy.logging));
            contentValues.put(ServiceManagerNative.NOTIFICATION, Boolean.valueOf(uidPolicy.notification));
            contentValues.put("uid", Integer.valueOf(i));
            if (uidPolicy.command == null) {
                uidPolicy.command = "";
            }
            contentValues.put("command", uidPolicy.command);
            contentValues.put("policy", uidPolicy.policy);
            contentValues.put("until", Integer.valueOf(i2));
            contentValues.put("name", str);
            contentValues.put("package_name", str2);
            contentValues.put("desired_uid", Integer.valueOf(i3));
            contentValues.put("desired_name", str4);
            contentValues.put("username", str3);
            sQLiteDatabase.replace("uid_policy", null, contentValues);
        }
    }

    public static void a(String str, int i, Context context, String str2) {
        if (2000 != Integer.valueOf(str).intValue()) {
            SQLiteDatabase writableDatabase = new KingoDatabaseHelper(context).getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            try {
                contentValues.put("policy", str2);
                contentValues.put("until", Integer.valueOf(i));
                writableDatabase.update("uid_policy", contentValues, "uid = ?", new String[]{str});
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    static UidPolicy a(Cursor cursor) {
        boolean z;
        boolean z2 = true;
        UidPolicy uidPolicy = new UidPolicy();
        uidPolicy.getUidCommand(cursor);
        uidPolicy.policy = cursor.getString(cursor.getColumnIndex("policy"));
        uidPolicy.until = cursor.getInt(cursor.getColumnIndex("until"));
        if (cursor.getInt(cursor.getColumnIndex("logging")) != 0) {
            z = true;
        } else {
            z = false;
        }
        uidPolicy.logging = z;
        if (cursor.getInt(cursor.getColumnIndex(ServiceManagerNative.NOTIFICATION)) == 0) {
            z2 = false;
        }
        uidPolicy.notification = z2;
        return uidPolicy;
    }

    public static ArrayList<UidPolicy> a(SQLiteDatabase sQLiteDatabase) {
        ArrayList<UidPolicy> arrayList = new ArrayList<>();
        Cursor query = sQLiteDatabase.query("uid_policy", null, null, null, null, null, null);
        while (query.moveToNext()) {
            try {
                arrayList.add(a(query));
            } catch (Exception e2) {
                e2.printStackTrace();
            } finally {
                query.close();
            }
        }
        return arrayList;
    }

    public static UidPolicy a(Context context, int i) {
        UidPolicy uidPolicy = null;
        Cursor query = new KingoDatabaseHelper(context).getReadableDatabase().query("uid_policy", null, "uid = ? ", new String[]{String.valueOf(i)}, null, null, null);
        try {
            if (query.moveToNext()) {
                uidPolicy = a(query);
            } else {
                query.close();
            }
            return uidPolicy;
        } finally {
            query.close();
        }
    }

    public static UidPolicy a(Context context, String str) {
        UidPolicy uidPolicy = null;
        SQLiteDatabase readableDatabase = new KingoDatabaseHelper(context).getReadableDatabase();
        FileUtils.write(context.getFilesDir() + "/request_log", System.currentTimeMillis() + ":getpackagename info;" + str + ";", true);
        Cursor query = readableDatabase.query("uid_policy", null, "package_name = ? ", new String[]{str}, null, null, null);
        try {
            if (query.moveToNext()) {
                FileUtils.write(context.getFilesDir() + "/request_log", System.currentTimeMillis() + ":getPolicy(c) !=null =" + (a(query) != null), true);
                uidPolicy = a(query);
            } else {
                query.close();
            }
            return uidPolicy;
        } finally {
            query.close();
        }
    }

    public static ArrayList<UidPolicy> a(Context context) {
        return a(new KingoDatabaseHelper(context).getWritableDatabase());
    }

    public static void b(Context context, UidPolicy uidPolicy) {
        new KingoDatabaseHelper(context).getWritableDatabase().delete("uid_policy", "uid = ?", new String[]{String.valueOf(uidPolicy.uid)});
    }
}
