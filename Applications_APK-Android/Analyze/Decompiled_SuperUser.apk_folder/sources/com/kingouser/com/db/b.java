package com.kingouser.com.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import com.kingouser.com.entity.AppEntity;
import java.util.ArrayList;
import java.util.List;

/* compiled from: WhiteListDatabaseHelper */
public class b extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    public static String f4342a = "white_list";

    /* renamed from: b  reason: collision with root package name */
    private static b f4343b = null;

    private b(Context context) {
        super(context, "whitelist.sqlite", (SQLiteDatabase.CursorFactory) null, 1);
    }

    public static b a(Context context) {
        if (f4343b == null) {
            synchronized (b.class) {
                if (f4343b == null) {
                    f4343b = new b(context);
                }
            }
        }
        return f4343b;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("create table if not exists " + f4342a + " (_id integer primary key,app_package text)");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public static ArrayList<String> a(Context context, String str) {
        SQLiteDatabase readableDatabase = a(context).getReadableDatabase();
        ArrayList<String> arrayList = new ArrayList<>();
        Cursor query = readableDatabase.query(str, null, null, null, null, null, null);
        while (query.moveToNext()) {
            String string = query.getString(query.getColumnIndex("app_package"));
            if (!TextUtils.isEmpty(string)) {
                arrayList.add(string);
            }
        }
        return arrayList;
    }

    public static void a(Context context, List<AppEntity> list, String str) {
        SQLiteDatabase readableDatabase = a(context).getReadableDatabase();
        if (list != null && list.size() > 0) {
            for (AppEntity package_id : list) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("app_package", package_id.getPackage_id());
                readableDatabase.insert(str, null, contentValues);
            }
        }
    }

    public static void b(Context context) {
        ArrayList arrayList = new ArrayList();
        AppEntity appEntity = new AppEntity();
        appEntity.setPackage_id("com.kingoapp.root");
        arrayList.add(appEntity);
        AppEntity appEntity2 = new AppEntity();
        appEntity2.setPackage_id("com.kingoapp.superbattery");
        arrayList.add(appEntity2);
        AppEntity appEntity3 = new AppEntity();
        appEntity3.setPackage_id("com.kingoapp.battery");
        arrayList.add(appEntity3);
        AppEntity appEntity4 = new AppEntity();
        appEntity4.setPackage_id("com.kingoapp.apk");
        arrayList.add(appEntity4);
        AppEntity appEntity5 = new AppEntity();
        appEntity5.setPackage_id("com.clean.boost.phone.booster.cleaner");
        arrayList.add(appEntity5);
        AppEntity appEntity6 = new AppEntity();
        appEntity6.setPackage_id("kingoroot.supersu");
        arrayList.add(appEntity6);
        AppEntity appEntity7 = new AppEntity();
        appEntity7.setPackage_id("com.kg.apk");
        arrayList.add(appEntity7);
        AppEntity appEntity8 = new AppEntity();
        appEntity8.setPackage_id("com.kingpapp.link");
        arrayList.add(appEntity8);
        AppEntity appEntity9 = new AppEntity();
        appEntity9.setPackage_id("com.pureapps.cleaner");
        arrayList.add(appEntity9);
        AppEntity appEntity10 = new AppEntity();
        appEntity10.setPackage_id("com.kingoapp.keyapp");
        arrayList.add(appEntity10);
        a(context, arrayList, f4342a);
    }
}
