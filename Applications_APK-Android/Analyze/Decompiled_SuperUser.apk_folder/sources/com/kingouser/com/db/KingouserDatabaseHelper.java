package com.kingouser.com.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.kingouser.com.entity.UidPolicy;
import java.util.ArrayList;

public class KingouserDatabaseHelper extends SQLiteOpenHelper {
    public KingouserDatabaseHelper(Context context) {
        super(context, "superuser.sqlite", (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        onUpgrade(sQLiteDatabase, 0, 1);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i == 0) {
            sQLiteDatabase.execSQL("create table if not exists log (id integer primary key autoincrement, desired_name text, username text, uid integer, desired_uid integer, command text not null, date integer, action text, package_name text, name text)");
            sQLiteDatabase.execSQL("create index if not exists log_uid_index on log(uid)");
            sQLiteDatabase.execSQL("create index if not exists log_desired_uid_index on log(desired_uid)");
            sQLiteDatabase.execSQL("create index if not exists log_command_index on log(command)");
            sQLiteDatabase.execSQL("create index if not exists log_date_index on log(date)");
            sQLiteDatabase.execSQL("create table if not exists settings (key text primary key not null, value text)");
        }
    }

    public static ArrayList<LogEntry> a(Context context, SQLiteDatabase sQLiteDatabase) {
        ArrayList<LogEntry> arrayList = new ArrayList<>();
        Cursor query = sQLiteDatabase.query("log", null, null, null, null, null, "date DESC");
        while (query.moveToNext()) {
            try {
                LogEntry logEntry = new LogEntry();
                arrayList.add(logEntry);
                logEntry.getUidCommand(query);
                logEntry.uid = query.getInt(query.getColumnIndex("uid"));
                logEntry.f4336a = query.getLong(query.getColumnIndex("id"));
                logEntry.f4338c = query.getInt(query.getColumnIndex("date"));
                logEntry.f4337b = query.getString(query.getColumnIndex("action"));
            } catch (Exception e2) {
            } finally {
                query.close();
            }
        }
        return arrayList;
    }

    static void a(SQLiteDatabase sQLiteDatabase, LogEntry logEntry) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("uid", Integer.valueOf(logEntry.uid));
        if (logEntry.command == null) {
            logEntry.command = "";
        }
        contentValues.put("command", logEntry.command);
        contentValues.put("action", logEntry.f4337b);
        contentValues.put("date", Integer.valueOf(logEntry.f4338c));
        contentValues.put("name", logEntry.name);
        contentValues.put("desired_uid", Integer.valueOf(logEntry.desiredUid));
        contentValues.put("package_name", logEntry.packageName);
        contentValues.put("desired_name", logEntry.desiredName);
        contentValues.put("username", logEntry.username);
        sQLiteDatabase.insert("log", null, contentValues);
    }

    public static UidPolicy a(Context context, LogEntry logEntry) {
        UidPolicy uidPolicy = null;
        if (logEntry.command == null) {
            logEntry.command = "";
        }
        Cursor query = new KingoDatabaseHelper(context).getReadableDatabase().query("uid_policy", null, "uid = ? and (command = ? or command = ?) and desired_uid = ?", new String[]{String.valueOf(logEntry.uid), logEntry.command, "", String.valueOf(logEntry.desiredUid)}, null, null, null, null);
        try {
            if (query.moveToNext()) {
                uidPolicy = KingoDatabaseHelper.a(query);
            }
            if (uidPolicy == null || uidPolicy.logging) {
                SQLiteDatabase writableDatabase = new KingouserDatabaseHelper(context).getWritableDatabase();
                writableDatabase.delete("log", "date < ?", new String[]{String.valueOf((System.currentTimeMillis() - 1209600000) / 1000)});
                a(writableDatabase, logEntry);
            }
            return uidPolicy;
        } finally {
            query.close();
        }
    }
}
