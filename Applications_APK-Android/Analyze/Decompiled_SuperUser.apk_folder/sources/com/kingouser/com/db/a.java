package com.kingouser.com.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.text.TextUtils;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: AppDatabaseHelper */
public class a extends SQLiteOpenHelper {

    /* renamed from: b  reason: collision with root package name */
    private static a f4339b = null;

    /* renamed from: c  reason: collision with root package name */
    private static Lock f4340c = new ReentrantLock();

    /* renamed from: a  reason: collision with root package name */
    private Context f4341a;

    private a(Context context) {
        super(context, "app.sqlite", (SQLiteDatabase.CursorFactory) null, 2);
        this.f4341a = context;
    }

    public static a a(Context context) {
        if (f4339b == null) {
            synchronized (a.class) {
                if (f4339b == null) {
                    f4339b = new a(context);
                }
            }
        }
        return f4339b;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("create table if not exists user_application (_id integer primary key,app_name text, app_package text, size long, source_dir text,data_dir text,native_library_dir text, sort_letter text,app_icon blob)");
        sQLiteDatabase.execSQL("create table if not exists system_application (_id integer primary key,app_name text, app_package text, size long, source_dir text,data_dir text,native_library_dir text, sort_letter text,app_icon blob)");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i != 1) {
            return;
        }
        if (Build.VERSION.SDK_INT <= 20) {
            sQLiteDatabase.execSQL("ALTER TABLE user_application RENAME TO user_application_temp");
            sQLiteDatabase.execSQL("create table if not exists user_application (_id integer primary key,app_name text, app_package text, size long, source_dir text,data_dir text,native_library_dir text, sort_letter text,app_icon blob)");
            sQLiteDatabase.execSQL("insert into user_application(app_name, app_package, size, source_dir, sort_letter, app_icon)select app_name, app_package, size, source_dir, sort_letter, app_icon from user_application_temp");
            sQLiteDatabase.execSQL("DROP TABLE user_application_temp");
            sQLiteDatabase.execSQL("ALTER TABLE system_application RENAME TO system_application_temp");
            sQLiteDatabase.execSQL("create table if not exists system_application  (_id integer primary key,app_name text, app_package text, size long, source_dir text,data_dir text,native_library_dir text, sort_letter text,app_icon blob)");
            sQLiteDatabase.execSQL("insert into system_application(app_name, app_package, size, source_dir, sort_letter, app_icon)select app_name, app_package, size, source_dir, sort_letter, app_icon from system_application_temp");
            sQLiteDatabase.execSQL("DROP TABLE system_application_temp");
            return;
        }
        sQLiteDatabase.execSQL("DROP TABLE user_application if exists");
        sQLiteDatabase.execSQL("DROP TABLE system_application if exists");
        sQLiteDatabase.execSQL("create table if not exists user_application (_id integer primary key,app_name text, app_package text, size long, source_dir text,data_dir text,native_library_dir text, sort_letter text,app_icon blob)");
        sQLiteDatabase.execSQL("create table if not exists system_application (_id integer primary key,app_name text, app_package text, size long, source_dir text,data_dir text,native_library_dir text, sort_letter text,app_icon blob)");
    }

    public static void a(Context context, String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            try {
                a(context).getWritableDatabase().delete(str2, "app_package = ?", new String[]{str});
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
