package com.kingouser.com.db;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import java.io.Serializable;

public class UidCommand implements Serializable {
    public String command;
    public String desiredName;
    public int desiredUid;
    public String name;
    public String packageName;
    public int uid;
    public String username;

    public String getName() {
        if (this.name != null) {
            return this.name;
        }
        if (this.packageName != null) {
            return this.packageName;
        }
        if (this.username == null || this.username.length() <= 0) {
            return String.valueOf(this.uid);
        }
        return this.username;
    }

    public void getPackageInfo(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageManager.getPackagesForUid(this.uid)[0], 0);
            this.name = packageInfo.applicationInfo.loadLabel(packageManager).toString();
            this.packageName = packageInfo.packageName;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void getUidCommand(Cursor cursor) {
        this.uid = cursor.getInt(cursor.getColumnIndex("uid"));
        this.command = cursor.getString(cursor.getColumnIndex("command"));
        this.name = cursor.getString(cursor.getColumnIndex("name"));
        this.packageName = cursor.getString(cursor.getColumnIndex("package_name"));
        this.desiredUid = cursor.getInt(cursor.getColumnIndex("desired_uid"));
        this.desiredName = cursor.getString(cursor.getColumnIndex("desired_name"));
        this.username = cursor.getString(cursor.getColumnIndex("username"));
    }
}
