package com.kingouser.com;

import android.content.res.Resources;
import android.view.View;
import android.widget.ScrollView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.customview.MyDrawbleText;

public class AboutActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private AboutActivity f3919a;

    /* renamed from: b  reason: collision with root package name */
    private View f3920b;

    /* renamed from: c  reason: collision with root package name */
    private View f3921c;

    /* renamed from: d  reason: collision with root package name */
    private View f3922d;

    public AboutActivity_ViewBinding(final AboutActivity aboutActivity, View view) {
        this.f3919a = aboutActivity;
        aboutActivity.about_version = (MyDrawbleText) Utils.findRequiredViewAsType(view, R.id.cr, "field 'about_version'", MyDrawbleText.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.cp, "field 'face_book' and method 'onClick'");
        aboutActivity.face_book = (MyDrawbleText) Utils.castView(findRequiredView, R.id.cp, "field 'face_book'", MyDrawbleText.class);
        this.f3920b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                aboutActivity.onClick(view);
            }
        });
        aboutActivity.mScrollview = (ScrollView) Utils.findRequiredViewAsType(view, R.id.co, "field 'mScrollview'", ScrollView.class);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.cq, "method 'onClick'");
        this.f3921c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                aboutActivity.onClick(view);
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.cs, "method 'onClick'");
        this.f3922d = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                aboutActivity.onClick(view);
            }
        });
        Resources resources = view.getContext().getResources();
        aboutActivity.drawbleRightWidth = resources.getDimensionPixelSize(R.dimen.f8285d);
        aboutActivity.drawbleRightHeight = resources.getDimensionPixelSize(R.dimen.f8285d);
        aboutActivity.drawbleBottomWidth = resources.getDimensionPixelSize(R.dimen.f8284c);
        aboutActivity.bgWidth = resources.getDimensionPixelSize(R.dimen.f8284c);
        aboutActivity.bgHeight = resources.getDimensionPixelSize(R.dimen.f8283b);
        aboutActivity.rightMargin = resources.getDimensionPixelSize(R.dimen.f8286e);
    }

    public void unbind() {
        AboutActivity aboutActivity = this.f3919a;
        if (aboutActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f3919a = null;
        aboutActivity.about_version = null;
        aboutActivity.face_book = null;
        aboutActivity.mScrollview = null;
        this.f3920b.setOnClickListener(null);
        this.f3920b = null;
        this.f3921c.setOnClickListener(null);
        this.f3921c = null;
        this.f3922d.setOnClickListener(null);
        this.f3922d = null;
    }
}
