package com.kingouser.com;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class PaypalActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private PaypalActivity f3994a;

    /* renamed from: b  reason: collision with root package name */
    private View f3995b;

    /* renamed from: c  reason: collision with root package name */
    private View f3996c;

    public PaypalActivity_ViewBinding(final PaypalActivity paypalActivity, View view) {
        this.f3994a = paypalActivity;
        paypalActivity.tv_currentprice = (TextView) Utils.findRequiredViewAsType(view, R.id.l1, "field 'tv_currentprice'", TextView.class);
        paypalActivity.tv_originalprice = (TextView) Utils.findRequiredViewAsType(view, R.id.l3, "field 'tv_originalprice'", TextView.class);
        paypalActivity.rl_paypal = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.l7, "field 'rl_paypal'", LinearLayout.class);
        paypalActivity.rl_creditcard = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.l5, "field 'rl_creditcard'", LinearLayout.class);
        paypalActivity.ll_payment_price = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.l0, "field 'll_payment_price'", FrameLayout.class);
        paypalActivity.progressbar_top = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.ky, "field 'progressbar_top'", ProgressBar.class);
        paypalActivity.ll_msg = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.l4, "field 'll_msg'", LinearLayout.class);
        paypalActivity.ll_originalprice = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.l2, "field 'll_originalprice'", LinearLayout.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.l8, "method 'onClick'");
        this.f3995b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                paypalActivity.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.l6, "method 'onClick'");
        this.f3996c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                paypalActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        PaypalActivity paypalActivity = this.f3994a;
        if (paypalActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f3994a = null;
        paypalActivity.tv_currentprice = null;
        paypalActivity.tv_originalprice = null;
        paypalActivity.rl_paypal = null;
        paypalActivity.rl_creditcard = null;
        paypalActivity.ll_payment_price = null;
        paypalActivity.progressbar_top = null;
        paypalActivity.ll_msg = null;
        paypalActivity.ll_originalprice = null;
        this.f3995b.setOnClickListener(null);
        this.f3995b = null;
        this.f3996c.setOnClickListener(null);
        this.f3996c = null;
    }
}
