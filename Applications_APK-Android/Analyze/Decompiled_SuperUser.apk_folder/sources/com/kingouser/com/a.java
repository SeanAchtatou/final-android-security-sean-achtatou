package com.kingouser.com;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.application.App;
import com.kingouser.com.entity.httpEntity.UpdateEntity;
import com.kingouser.com.receiver.ScreenReceiver;
import com.kingouser.com.util.NetworkUtils;
import com.kingouser.com.util.RC4EncodeUtils;
import com.kingouser.com.util.ResultUtils;
import com.kingouser.com.util.SPUtil;
import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.util.g;
import com.pureapps.cleaner.util.h;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.l;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: UpdateManager */
public class a {
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static Context f4150c;

    /* renamed from: e  reason: collision with root package name */
    private static a f4151e = null;

    /* renamed from: a  reason: collision with root package name */
    Handler f4152a = new Handler();

    /* renamed from: b  reason: collision with root package name */
    Runnable f4153b = new Runnable() {
        public void run() {
            f.a("nameBelowAndroidL:" + a.this.d(a.f4150c));
            f.a("isScreenOff:" + a.this.d());
            if (!a.this.f() || a.this.d()) {
                a.this.e();
            } else {
                a.this.f4152a.postDelayed(a.this.f4153b, 5000);
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public boolean f4154d = false;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public boolean f4155f = false;

    /* renamed from: g  reason: collision with root package name */
    private Runnable f4156g = new Runnable() {
        public void run() {
            AlertDialog.Builder builder = new AlertDialog.Builder(a.f4150c, R.style.dj);
            builder.a(true);
            builder.c(17301543);
            builder.a((int) R.string.f6);
            UpdateEntity parseUpdate = ResultUtils.parseUpdate(i.a(App.a()).o());
            if (parseUpdate == null || parseUpdate.upgrade != null) {
            }
            if (parseUpdate != null && parseUpdate.upgrade != null && parseUpdate.upgrade.version == l.c(a.f4150c)) {
                builder.b(parseUpdate.upgrade.releasenode);
                builder.a(17039370, (DialogInterface.OnClickListener) null);
                builder.b().setCanceledOnTouchOutside(false);
                builder.c();
                i.a(a.f4150c).a(l.c(a.f4150c));
            }
        }
    };

    public static a a(Context context) {
        if (f4151e == null) {
            synchronized (a.class) {
                if (f4151e == null) {
                    f4151e = new a();
                }
            }
        }
        if (f4150c == null) {
            f4150c = context;
        }
        return f4151e;
    }

    public void b(Context context) {
        f4150c = context;
        if (l.c(context) > i.a(context).g()) {
            this.f4152a.postDelayed(this.f4156g, 1000);
        }
    }

    public void a(Context context, boolean z) {
        this.f4155f = z;
        c(context);
    }

    public void c(Context context) {
        f.a("belowAndroidL:" + d(f4150c));
        if (this.f4154d) {
            f.a("startUpdate is  Runing");
            return;
        }
        this.f4154d = true;
        if (!NetworkUtils.isNetworkAvailable(context)) {
            f.a("NetworkAvailable NO");
            this.f4154d = false;
            return;
        }
        f.a("NetworkAvailable yes");
        new C0084a().execute(new String[0]);
    }

    /* renamed from: com.kingouser.com.a$a  reason: collision with other inner class name */
    /* compiled from: UpdateManager */
    class C0084a extends AsyncTask<String, String, Boolean> {

        /* renamed from: a  reason: collision with root package name */
        UpdateEntity f4159a;

        C0084a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kingouser.com.a.a(com.kingouser.com.a, boolean):boolean
         arg types: [com.kingouser.com.a, int]
         candidates:
          com.kingouser.com.a.a(com.kingouser.com.a, com.kingouser.com.entity.httpEntity.UpdateEntity):boolean
          com.kingouser.com.a.a(android.content.Context, boolean):void
          com.kingouser.com.a.a(com.kingouser.com.a, boolean):boolean */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            boolean z;
            String a2;
            boolean z2;
            String str = a.this.h() + "?" + a.this.i();
            f.a("updateUrl:" + str);
            try {
                if (i.a(App.a()).p() > l.c(App.a())) {
                    a2 = i.a(App.a()).o();
                    this.f4159a = ResultUtils.parseUpdate(a2);
                } else {
                    a2 = cli.a.a.a(str);
                    this.f4159a = ResultUtils.parseUpdate(a2);
                    if (this.f4159a != null) {
                        i.a(App.a()).b(a2);
                        i.a(App.a()).c(this.f4159a.upgrade.version);
                    }
                }
                f.a("requestGet:" + a2);
                if (this.f4159a == null || this.f4159a.upgrade.version <= l.c(App.a())) {
                    f.a("no upupdate");
                    if (a.this.f4155f) {
                        com.pureapps.cleaner.b.a.a(36, 0, null);
                    }
                    z2 = false;
                } else {
                    f.a("parseUpdate" + this.f4159a.toString());
                    com.pureapps.cleaner.analytic.a.a(a.f4150c).b(FirebaseAnalytics.getInstance(a.f4150c));
                    if (!a.this.a(this.f4159a)) {
                        int i = 5;
                        boolean z3 = false;
                        while (!z3 && i > 0) {
                            z3 = cli.a.a.a(this.f4159a.upgrade.downloadurl, a.this.g());
                            i--;
                            f.a("maxdown:" + i);
                        }
                        z = a.this.a(this.f4159a);
                        try {
                            f.a("loadlfile:" + z3);
                        } catch (Exception e2) {
                            e = e2;
                            e.printStackTrace();
                            boolean unused = a.this.f4154d = false;
                            return Boolean.valueOf(z);
                        }
                    }
                    z2 = true;
                }
                return Boolean.valueOf(z2);
            } catch (Exception e3) {
                e = e3;
                z = false;
                e.printStackTrace();
                boolean unused2 = a.this.f4154d = false;
                return Boolean.valueOf(z);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            f.a("onPreExecute");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kingouser.com.a.a(com.kingouser.com.a, boolean):boolean
         arg types: [com.kingouser.com.a, int]
         candidates:
          com.kingouser.com.a.a(com.kingouser.com.a, com.kingouser.com.entity.httpEntity.UpdateEntity):boolean
          com.kingouser.com.a.a(android.content.Context, boolean):void
          com.kingouser.com.a.a(com.kingouser.com.a, boolean):boolean */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            f.a("onPostExecute:" + bool);
            if (!bool.booleanValue()) {
                f.b("hasDownloadlfile : false");
            } else if (ShellUtils.checkSuVerison()) {
                a.this.c();
            } else if (a.this.f4155f) {
                a.this.a();
            }
            boolean unused = a.this.f4154d = false;
            com.pureapps.cleaner.b.a.a(1, a.this.f4155f ? 1 : 0, bool);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String[], int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public void a() {
        f.b("installWithOutRoot");
        ShellUtils.execCommand(new String[]{"chmod 777 " + g()}, false);
        h.c(f4150c, g());
    }

    /* access modifiers changed from: private */
    public void c() {
        f.b("installWithRoot");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        App.a().getApplicationContext().registerReceiver(new ScreenReceiver(), intentFilter);
        if (h.b(f4150c, f4150c.getPackageName())) {
            if (!f() || d()) {
                e();
            } else {
                this.f4152a.postDelayed(this.f4153b, 5000);
            }
        } else if (!f() || d()) {
            e();
        } else {
            this.f4152a.postDelayed(this.f4153b, 5000);
        }
    }

    /* access modifiers changed from: private */
    public boolean d() {
        return ((Boolean) SPUtil.getInstant(App.a().getApplicationContext()).get("SCREEN_OFF", false)).booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String[], int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    /* access modifiers changed from: private */
    public void e() {
        String[] strArr = {"chmod 777 " + g(), "pm install -r " + g()};
        f.a("installReplace:" + strArr[0]);
        f.a("installReplace:" + strArr[1]);
        ShellUtils.execCommand(strArr, true);
    }

    /* access modifiers changed from: private */
    public boolean f() {
        String d2 = d(f4150c);
        if (TextUtils.isEmpty(d2)) {
            return false;
        }
        return d2.contains(f4150c.getPackageName());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    public String d(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ServiceManagerNative.ACTIVITY);
        if (Build.VERSION.SDK_INT >= 21) {
            String str = "dumpsys window | " + "/data/data/kingoroot.supersu/files/busybox grep" + " Top | " + "/data/data/kingoroot.supersu/files/busybox grep" + " " + context.getPackageName();
            ShellUtils.CommandResult execCommand = ShellUtils.execCommand(str, true);
            f.a("command:" + str);
            f.a("result:" + execCommand.result);
            f.a("result:" + execCommand.successMsg);
            f.a("result:" + execCommand.errorMsg);
            if (execCommand.result == 0) {
                Matcher matcher = Pattern.compile("\\w+\\..+/").matcher(execCommand.successMsg);
                if (matcher.find()) {
                    String group = matcher.group();
                    return group.substring(0, group.length() - 1);
                }
            }
        } else {
            try {
                return activityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return "";
    }

    /* access modifiers changed from: private */
    public boolean a(UpdateEntity updateEntity) {
        String g2 = g();
        File file = new File(g2);
        if (!file.exists()) {
            f.a("downLoaderCahe updateCache is not exists");
            return false;
        }
        String str = updateEntity.upgrade.md5;
        String b2 = g.b(g2);
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(b2)) {
            f.a("fileMD5 is null u:" + TextUtils.isEmpty(str) + " f:" + TextUtils.isEmpty(b2));
            return false;
        }
        if (!str.equals(b2) && file.exists()) {
            file.deleteOnExit();
        }
        return str.equals(b2);
    }

    /* access modifiers changed from: private */
    public String g() {
        File filesDir = f4150c.getFilesDir();
        if (filesDir.exists() && filesDir.isDirectory() && filesDir.canRead()) {
            return filesDir.getAbsoluteFile() + File.separator + "update.apk";
        }
        File file = new File("/data/local/tmp/");
        if (!file.exists() || !file.isDirectory() || !file.canRead()) {
            return null;
        }
        return file.getAbsoluteFile() + File.separator + "update.apk";
    }

    /* access modifiers changed from: private */
    public String h() {
        return RC4EncodeUtils.decry_RC4("c9b07620ffea125a6e149c08a31ab28e04c88fe2479051d0991016a8b3281dc908c6bc2cce89b3cbba84b608c28c4da07e7ebb02b6", "string_key");
    }

    /* access modifiers changed from: private */
    public String i() {
        if (f4150c == null) {
            f4150c = App.a().getApplicationContext();
        }
        String str = "";
        try {
            str = "" + f4150c.getPackageManager().getPackageInfo(f4150c.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        return "current_version=" + str + "&channel=" + "OffcialSite" + "&packagename=" + f4150c.getPackageName();
    }
}
