package com.kingouser.com.application;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import com.daps.weather.DapWeather;
import com.daps.weather.floatdisplay.FloatDisplayController;
import com.daps.weather.notification.DapWeatherNotification;
import com.duapps.ad.base.DuAdNetwork;
import com.google.firebase.FirebaseApp;
import com.kingoapp.uts.api.RxAndroidPlugins;
import com.kingouser.com.db.b;
import com.kingouser.com.entity.DeviceEntity;
import com.kingouser.com.entity.IntentEntity;
import com.kingouser.com.util.DeviceInfoUtils;
import com.kingouser.com.util.EncodeMD5;
import com.kingouser.com.util.FileUtils;
import com.kingouser.com.util.LanguageUtils;
import com.kingouser.com.util.MySharedPreference;
import com.kingouser.com.util.PermissionUtils;
import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.client.core.VirtualCore;
import com.pureapps.cleaner.manager.h;
import com.pureapps.cleaner.service.BackService;
import com.pureapps.cleaner.util.i;
import io.fabric.sdk.android.Fabric;
import io.reactivex.BackpressureStrategy;
import io.reactivex.c.a;
import io.reactivex.d;
import io.reactivex.e;
import io.reactivex.f;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App extends MultiDexApplication {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f4219a;

    /* renamed from: b  reason: collision with root package name */
    public static int f4220b = 0;

    /* renamed from: c  reason: collision with root package name */
    public static int f4221c;

    /* renamed from: d  reason: collision with root package name */
    public static LinkedList<IntentEntity> f4222d = new LinkedList<>();

    /* renamed from: e  reason: collision with root package name */
    public static ArrayList<String> f4223e = new ArrayList<>();

    /* renamed from: h  reason: collision with root package name */
    private static App f4224h;

    /* renamed from: f  reason: collision with root package name */
    ExecutorService f4225f = Executors.newSingleThreadExecutor();

    /* renamed from: g  reason: collision with root package name */
    Handler f4226g = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Toast.makeText(App.this, String.valueOf(message.obj), 0).show();
                    return;
                case 2:
                    ImageView imageView = new ImageView(App.this);
                    imageView.setImageBitmap((Bitmap) message.obj);
                    Toast toast = new Toast(App.this);
                    toast.setView(imageView);
                    toast.show();
                    return;
                default:
                    return;
            }
        }
    };
    private boolean i = true;

    public static App a() {
        return f4224h;
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        File file = new File(getApplicationInfo().nativeLibraryDir + "/" + "libva-native.so");
        if (ShellUtils.checkSuVerison() && !file.exists()) {
            b();
        }
        try {
            VirtualCore.get().startup(context);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void b() {
        d.a(new f<Boolean>() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
             arg types: [java.util.ArrayList, int]
             candidates:
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
              com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
            public void subscribe(e<Boolean> eVar) {
                File dir = App.this.getDir("jniLibs", 0);
                Log.e("copySo", "开始copy:" + dir.getAbsolutePath());
                if (FileUtils.CopyAssetsFile(App.this, "libva-native.so", dir.getAbsolutePath()).booleanValue()) {
                    Log.e("copySo", "true");
                    ArrayList arrayList = new ArrayList();
                    String str = App.this.getApplicationInfo().nativeLibraryDir;
                    arrayList.add("busybox mkdir " + str);
                    arrayList.add("busybox chmod 755 " + str);
                    arrayList.add("busybox chown 1000:1000 " + str);
                    arrayList.add("busybox  cp  " + dir.getAbsolutePath() + "/" + "libva-native.so" + "  " + str);
                    arrayList.add("busybox chmod 755 " + str + "/" + "libva-native.so");
                    arrayList.add("busybox chown 1000:1000 " + str + "/" + "libva-native.so");
                    Log.e("copySo", ShellUtils.execCommand((List<String>) arrayList, true).toString());
                    eVar.a((Boolean) true);
                    eVar.a();
                    return;
                }
                Log.e("copySo", "copy assets失败");
                eVar.a((Boolean) false);
                eVar.a();
            }
        }, BackpressureStrategy.ERROR).b(a.a()).a(RxAndroidPlugins.onMainThreadScheduler(h.f5805b)).a(new io.reactivex.a.d<Boolean>() {
            /* renamed from: a */
            public void accept(Boolean bool) {
            }
        }, new io.reactivex.a.d<Throwable>() {
            /* renamed from: a */
            public void accept(Throwable th) {
            }
        });
    }

    public void onCreate() {
        super.onCreate();
        f4224h = this;
        try {
            if (b.a(this, b.f4342a).size() == 0) {
                b.b(this);
            }
        } catch (Exception e2) {
        }
        e();
        c();
        h.b(this);
    }

    private void c() {
        Fabric.a(new Fabric.Builder(this).a(new Crashlytics()).a(false).a());
        try {
            FirebaseApp.initializeApp(this);
            com.salmon.sdk.a.a(this, 92139, "76f1ccd30ab01551310b54264f2810e4");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        d();
    }

    private void d() {
        try {
            DuAdNetwork.init(this, b("baidnativeupid"));
            DapWeather.init(this, 140822);
            DapWeatherNotification.getInstance(this).setOngoing(false);
            FloatDisplayController.setFloatSerachWindowIsShow(this, false);
            DapWeatherNotification.getInstance(this).setPushElevatingTemperature(i.a(this).c());
            DapWeatherNotification.getInstance(this).setPushTodayTomorrowWeather(i.a(this).c());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041 A[SYNTHETIC, Splitter:B:14:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0055 A[SYNTHETIC, Splitter:B:25:0x0055] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String b(java.lang.String r7) {
        /*
            r6 = this;
            r2 = 0
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x005f, all -> 0x0051 }
            android.content.res.AssetManager r0 = r6.getAssets()     // Catch:{ IOException -> 0x005f, all -> 0x0051 }
            java.io.InputStream r0 = r0.open(r7)     // Catch:{ IOException -> 0x005f, all -> 0x0051 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x005f, all -> 0x0051 }
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x0022 }
        L_0x0017:
            int r2 = r1.read(r0)     // Catch:{ IOException -> 0x0022 }
            if (r2 <= 0) goto L_0x0049
            r4 = 0
            r3.write(r0, r4, r2)     // Catch:{ IOException -> 0x0022 }
            goto L_0x0017
        L_0x0022:
            r0 = move-exception
        L_0x0023:
            java.lang.String r2 = "wyy"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x005d }
            r4.<init>()     // Catch:{ all -> 0x005d }
            java.lang.String r5 = "IOException :"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x005d }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x005d }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x005d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x005d }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x005d }
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0044:
            java.lang.String r0 = r3.toString()
            return r0
        L_0x0049:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ IOException -> 0x004f }
            goto L_0x0044
        L_0x004f:
            r0 = move-exception
            goto L_0x0044
        L_0x0051:
            r0 = move-exception
            r1 = r2
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x005b }
        L_0x0058:
            throw r0
        L_0x0059:
            r0 = move-exception
            goto L_0x0044
        L_0x005b:
            r1 = move-exception
            goto L_0x0058
        L_0x005d:
            r0 = move-exception
            goto L_0x0053
        L_0x005f:
            r0 = move-exception
            r1 = r2
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kingouser.com.application.App.b(java.lang.String):java.lang.String");
    }

    private void e() {
        this.f4225f.execute(new Runnable() {
            public void run() {
                App.f4221c = DeviceInfoUtils.getUid(App.this);
                App.this.g();
                App.this.i();
                App.this.h();
                App.this.f();
                BackService.a(App.this.getApplicationContext());
            }
        });
    }

    /* access modifiers changed from: private */
    public void f() {
        f4223e.add("eu.chainfire.supersu");
        f4223e.add("com.kingroot.kinguser");
    }

    /* access modifiers changed from: private */
    public void g() {
        String aboutActivityLocalLanguage = MySharedPreference.getAboutActivityLocalLanguage(this, "");
        if (!TextUtils.isEmpty(aboutActivityLocalLanguage)) {
            LanguageUtils.changeLocalLanguage(this, aboutActivityLocalLanguage);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult
     arg types: [java.lang.String, int]
     candidates:
      com.kingouser.com.util.ShellUtils.execCommand(java.util.List<java.lang.String>, boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String[], boolean):com.kingouser.com.util.ShellUtils$CommandResult
      com.kingouser.com.util.ShellUtils.execCommand(java.lang.String, boolean):com.kingouser.com.util.ShellUtils$CommandResult */
    /* access modifiers changed from: private */
    public void h() {
        if (!new File(getFilesDir() + "/supersu.cfg").exists()) {
            PermissionUtils.createPrePermission(this, MySharedPreference.getRequestDialogTimes(this, 15));
        }
        FileUtils.createConfig(this);
        ShellUtils.execCommand("chmod " + DeviceInfoUtils.getChmodCode(this) + " " + (getFilesDir().getPath() + "/config"), true);
    }

    /* access modifiers changed from: private */
    public void i() {
        try {
            f4219a = ShellUtils.checkSuVerison();
            String modelKey = DeviceEntity.getDeviceInfo().getModelKey();
            if (TextUtils.isEmpty(modelKey)) {
                modelKey = "";
            }
            HashMap hashMap = new HashMap();
            hashMap.put("su_version", DeviceInfoUtils.getSuVersion());
            hashMap.put("model_id", DeviceInfoUtils.getModelId());
            hashMap.put("android_sdk", DeviceInfoUtils.getSDKVersion());
            hashMap.put("model_key", EncodeMD5.getMD5To32String(modelKey));
        } catch (Exception e2) {
        }
    }

    public void a(int i2) {
        a(getResources().getString(i2));
    }

    public void a(String str) {
        Message obtainMessage = this.f4226g.obtainMessage(1);
        obtainMessage.obj = str;
        this.f4226g.sendMessage(obtainMessage);
    }
}
