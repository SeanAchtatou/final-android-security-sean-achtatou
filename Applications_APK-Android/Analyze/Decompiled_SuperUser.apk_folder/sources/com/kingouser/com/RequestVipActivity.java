package com.kingouser.com;

import android.os.Bundle;
import com.kingouser.com.fragment.VipFragment;
import com.kingouser.com.util.ShellUtils;
import com.pureapps.cleaner.b.a;
import com.pureapps.cleaner.b.c;

public class RequestVipActivity extends BaseActivity implements c {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a.a(this);
        if (ShellUtils.checkSuVerison()) {
            VipFragment.a(2).show(e(), "VipFragment");
        } else {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a.b(this);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.pureapps.cleaner.analytic.a.a(this).b(this.o, "AdlibRequestVip");
    }

    public void a(int i, long j, Object obj) {
        switch (i) {
            case 34:
                finish();
                return;
            default:
                return;
        }
    }
}
