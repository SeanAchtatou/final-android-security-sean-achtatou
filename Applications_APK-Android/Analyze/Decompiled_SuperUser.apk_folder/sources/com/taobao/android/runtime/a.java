package com.taobao.android.runtime;

import android.os.Build;
import android.util.Log;
import com.taobao.android.dex.interpret.ARTUtils;

/* compiled from: AndroidRuntime */
public class a {

    /* renamed from: c  reason: collision with root package name */
    private static volatile a f6952c;

    /* renamed from: a  reason: collision with root package name */
    private boolean f6953a;

    /* renamed from: b  reason: collision with root package name */
    private c f6954b;

    private a() {
    }

    public static a a() {
        if (f6952c == null) {
            synchronized (a.class) {
                if (f6952c == null) {
                    f6952c = new a();
                }
            }
        }
        return f6952c;
    }

    public void a(boolean z) {
        Boolean b2;
        if (!this.f6953a) {
            Log.e("RuntimeUtils", "- RuntimeUtils setVerificationEnabled disabled.");
            return;
        }
        if (d.f6955a) {
            b2 = ARTUtils.b(z);
        } else {
            b2 = DalvikUtils.b(z ? 3 : 1);
        }
        Log.e("RuntimeUtils", "- RuntimeUtils setVerificationEnabled: enabled=" + z + ", success=" + b2);
        a("setVerificationEnabled", b2);
    }

    private void a(String str, Boolean bool) {
        if (this.f6954b != null) {
            this.f6954b.a(str, "typeID=" + str + ", success=" + bool + ", model=" + Build.MODEL + ", version=" + Build.VERSION.RELEASE, bool == null ? false : bool.booleanValue());
        }
    }
}
