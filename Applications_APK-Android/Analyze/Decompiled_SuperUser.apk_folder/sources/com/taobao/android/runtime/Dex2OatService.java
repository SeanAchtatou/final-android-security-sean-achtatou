package com.taobao.android.runtime;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import com.taobao.android.dex.interpret.ARTUtils;
import dalvik.system.DexFile;
import java.util.ArrayList;
import java.util.List;

public class Dex2OatService extends IntentService {

    /* renamed from: a  reason: collision with root package name */
    static boolean f6946a = true;

    /* renamed from: b  reason: collision with root package name */
    private static List<Runnable> f6947b = new ArrayList();

    /* renamed from: c  reason: collision with root package name */
    private final Boolean f6948c = ARTUtils.a(true);

    public Dex2OatService() {
        super("Dex2OatService");
        a.a().a(true);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        if (this.f6948c != null && this.f6948c.booleanValue() && intent != null) {
            String stringExtra = intent.getStringExtra("sourcePathName");
            String stringExtra2 = intent.getStringExtra("outputPathName");
            try {
                long currentTimeMillis = System.currentTimeMillis();
                DexFile.loadDex(stringExtra, stringExtra2, 0);
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
            } catch (Exception e2) {
                Log.e("Dex2OatService", "- DexFile loadDex fail: sourcePathName=" + stringExtra + ", outputPathName=" + stringExtra2, e2);
            }
        }
    }
}
