package com.taobao.android.runtime;

import android.util.Log;

public class DalvikUtils {

    /* renamed from: a  reason: collision with root package name */
    private static final String f6944a = DalvikUtils.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static boolean f6945b;

    private static native boolean nativeInit();

    private static native boolean setClassVerifyModeNative(int i);

    private static native boolean setDexOptModeNative(int i);

    public static boolean a() {
        try {
            System.loadLibrary("dalvikhack");
            f6945b = nativeInit();
            return f6945b;
        } catch (UnsatisfiedLinkError e2) {
            Log.e(f6944a, e2.getMessage(), e2);
            return false;
        }
    }

    public static Boolean a(int i) {
        if (!d.f6955a && f6945b) {
            return Boolean.valueOf(setDexOptModeNative(i));
        }
        return null;
    }

    public static Boolean b(int i) {
        if (!d.f6955a && f6945b) {
            return Boolean.valueOf(setClassVerifyModeNative(i));
        }
        return null;
    }
}
