package com.taobao.android.runtime;

public class OatFile extends b {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f6950a = {Byte.MAX_VALUE, 69, 76, 70};

    /* renamed from: b  reason: collision with root package name */
    private static final byte[] f6951b = {111, 97, 116, 10};

    public static class InvalidOatFileException extends RuntimeException {
    }

    public static class NotAnOatFileException extends RuntimeException {
    }
}
