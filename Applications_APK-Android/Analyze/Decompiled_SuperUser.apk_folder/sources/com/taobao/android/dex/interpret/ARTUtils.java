package com.taobao.android.dex.interpret;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.Log;

@TargetApi(21)
public class ARTUtils {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f6943a = false;

    private static native boolean nativeInit(boolean z, int i);

    private static native boolean setIsDex2oatEnabledNative(boolean z);

    private static native boolean setSignalCatcherHaltFlagNative(boolean z);

    private static native boolean setVerificationEnabledNative(boolean z);

    public static boolean a(Context context) {
        return a(context, false);
    }

    public static boolean a(Context context, boolean z) {
        try {
            System.loadLibrary("dexinterpret");
            nativeInit(z, context.getApplicationInfo().targetSdkVersion);
            f6943a = true;
        } catch (UnsatisfiedLinkError e2) {
            Log.e("ARTUtils", "Couldn't initialize.", e2);
        } catch (NoSuchMethodError e3) {
            Log.e("ARTUtils", "Couldn't initialize.", e3);
        } catch (Throwable th) {
            Log.e("ARTUtils", "Couldn't initialize.", th);
        }
        return f6943a;
    }

    public static Boolean a(boolean z) {
        if (!f6943a) {
            return null;
        }
        return Boolean.valueOf(setIsDex2oatEnabledNative(z));
    }

    public static Boolean b(boolean z) {
        if (!f6943a) {
            return null;
        }
        boolean verificationEnabledNative = setVerificationEnabledNative(z);
        if (verificationEnabledNative && z) {
            c(false);
        } else if (verificationEnabledNative && !z) {
            c(true);
        }
        return Boolean.valueOf(verificationEnabledNative);
    }

    public static Boolean c(boolean z) {
        if (!f6943a) {
            return null;
        }
        return Boolean.valueOf(setSignalCatcherHaltFlagNative(z));
    }
}
