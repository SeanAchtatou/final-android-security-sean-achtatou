package com.bumptech.glide.request;

/* compiled from: ThumbnailRequestCoordinator */
public class e implements a, b {

    /* renamed from: a  reason: collision with root package name */
    private a f3345a;

    /* renamed from: b  reason: collision with root package name */
    private a f3346b;

    /* renamed from: c  reason: collision with root package name */
    private b f3347c;

    public e() {
        this(null);
    }

    public e(b bVar) {
        this.f3347c = bVar;
    }

    public void a(a aVar, a aVar2) {
        this.f3345a = aVar;
        this.f3346b = aVar2;
    }

    public boolean a(a aVar) {
        return j() && (aVar.equals(this.f3345a) || !this.f3345a.h());
    }

    private boolean j() {
        return this.f3347c == null || this.f3347c.a(this);
    }

    public boolean b(a aVar) {
        return k() && aVar.equals(this.f3345a) && !c();
    }

    private boolean k() {
        return this.f3347c == null || this.f3347c.b(this);
    }

    public boolean c() {
        return l() || h();
    }

    public void c(a aVar) {
        if (!aVar.equals(this.f3346b)) {
            if (this.f3347c != null) {
                this.f3347c.c(this);
            }
            if (!this.f3346b.g()) {
                this.f3346b.d();
            }
        }
    }

    private boolean l() {
        return this.f3347c != null && this.f3347c.c();
    }

    public void b() {
        if (!this.f3346b.f()) {
            this.f3346b.b();
        }
        if (!this.f3345a.f()) {
            this.f3345a.b();
        }
    }

    public void e() {
        this.f3345a.e();
        this.f3346b.e();
    }

    public void d() {
        this.f3346b.d();
        this.f3345a.d();
    }

    public boolean f() {
        return this.f3345a.f();
    }

    public boolean g() {
        return this.f3345a.g() || this.f3346b.g();
    }

    public boolean h() {
        return this.f3345a.h() || this.f3346b.h();
    }

    public boolean i() {
        return this.f3345a.i();
    }

    public void a() {
        this.f3345a.a();
        this.f3346b.a();
    }
}
