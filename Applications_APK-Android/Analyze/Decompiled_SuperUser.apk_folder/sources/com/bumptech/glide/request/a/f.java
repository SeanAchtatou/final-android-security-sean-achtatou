package com.bumptech.glide.request.a;

import android.view.View;
import android.view.animation.Animation;
import com.bumptech.glide.request.a.c;

/* compiled from: ViewAnimation */
public class f<R> implements c<R> {

    /* renamed from: a  reason: collision with root package name */
    private final a f3323a;

    /* compiled from: ViewAnimation */
    interface a {
        Animation a();
    }

    f(a aVar) {
        this.f3323a = aVar;
    }

    public boolean a(R r, c.a aVar) {
        View a2 = aVar.a();
        if (a2 == null) {
            return false;
        }
        a2.clearAnimation();
        a2.startAnimation(this.f3323a.a());
        return false;
    }
}
