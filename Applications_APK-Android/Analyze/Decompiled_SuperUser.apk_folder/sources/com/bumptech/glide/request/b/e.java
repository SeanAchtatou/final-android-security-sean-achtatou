package com.bumptech.glide.request.b;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.request.a.c;

/* compiled from: ImageViewTarget */
public abstract class e<Z> extends k<ImageView, Z> implements c.a {
    /* access modifiers changed from: protected */
    public abstract void a(Object obj);

    public e(ImageView imageView) {
        super(imageView);
    }

    public Drawable b() {
        return ((ImageView) this.f3338a).getDrawable();
    }

    public void a(Drawable drawable) {
        ((ImageView) this.f3338a).setImageDrawable(drawable);
    }

    public void c(Drawable drawable) {
        ((ImageView) this.f3338a).setImageDrawable(drawable);
    }

    public void a(Exception exc, Drawable drawable) {
        ((ImageView) this.f3338a).setImageDrawable(drawable);
    }

    public void b(Drawable drawable) {
        ((ImageView) this.f3338a).setImageDrawable(drawable);
    }

    public void a(Z z, c<? super Z> cVar) {
        if (cVar == null || !cVar.a(z, this)) {
            a((Object) z);
        }
    }
}
