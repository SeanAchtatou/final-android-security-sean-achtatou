package com.bumptech.glide.request.a;

import android.graphics.drawable.Drawable;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import com.bumptech.glide.request.a.f;

/* compiled from: DrawableCrossFadeFactory */
public class a<T extends Drawable> implements d<T> {

    /* renamed from: a  reason: collision with root package name */
    private final g<T> f3314a;

    /* renamed from: b  reason: collision with root package name */
    private final int f3315b;

    /* renamed from: c  reason: collision with root package name */
    private b<T> f3316c;

    /* renamed from: d  reason: collision with root package name */
    private b<T> f3317d;

    public a() {
        this(300);
    }

    public a(int i) {
        this(new g(new C0042a(i)), i);
    }

    a(g<T> gVar, int i) {
        this.f3314a = gVar;
        this.f3315b = i;
    }

    public c<T> a(boolean z, boolean z2) {
        if (z) {
            return e.b();
        }
        if (z2) {
            return a();
        }
        return b();
    }

    private c<T> a() {
        if (this.f3316c == null) {
            this.f3316c = new b<>(this.f3314a.a(false, true), this.f3315b);
        }
        return this.f3316c;
    }

    private c<T> b() {
        if (this.f3317d == null) {
            this.f3317d = new b<>(this.f3314a.a(false, false), this.f3315b);
        }
        return this.f3317d;
    }

    /* renamed from: com.bumptech.glide.request.a.a$a  reason: collision with other inner class name */
    /* compiled from: DrawableCrossFadeFactory */
    private static class C0042a implements f.a {

        /* renamed from: a  reason: collision with root package name */
        private final int f3318a;

        C0042a(int i) {
            this.f3318a = i;
        }

        public Animation a() {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration((long) this.f3318a);
            return alphaAnimation;
        }
    }
}
