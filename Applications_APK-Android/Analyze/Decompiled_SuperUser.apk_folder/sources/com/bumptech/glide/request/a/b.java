package com.bumptech.glide.request.a;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import com.bumptech.glide.request.a.c;

/* compiled from: DrawableCrossFadeViewAnimation */
public class b<T extends Drawable> implements c<T> {

    /* renamed from: a  reason: collision with root package name */
    private final c<T> f3319a;

    /* renamed from: b  reason: collision with root package name */
    private final int f3320b;

    public b(c<T> cVar, int i) {
        this.f3319a = cVar;
        this.f3320b = i;
    }

    public boolean a(T t, c.a aVar) {
        Drawable b2 = aVar.b();
        if (b2 != null) {
            TransitionDrawable transitionDrawable = new TransitionDrawable(new Drawable[]{b2, t});
            transitionDrawable.setCrossFadeEnabled(true);
            transitionDrawable.startTransition(this.f3320b);
            aVar.a(transitionDrawable);
            return true;
        }
        this.f3319a.a(t, aVar);
        return false;
    }
}
