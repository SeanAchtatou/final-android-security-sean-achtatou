package com.bumptech.glide.request.b;

import android.widget.ImageView;
import com.bumptech.glide.load.resource.a.b;
import com.bumptech.glide.request.a.c;

/* compiled from: GlideDrawableImageViewTarget */
public class d extends e<b> {

    /* renamed from: b  reason: collision with root package name */
    private int f3327b;

    /* renamed from: c  reason: collision with root package name */
    private b f3328c;

    public /* bridge */ /* synthetic */ void a(Object obj, c cVar) {
        a((b) obj, (c<? super b>) cVar);
    }

    public d(ImageView imageView) {
        this(imageView, -1);
    }

    public d(ImageView imageView, int i) {
        super(imageView);
        this.f3327b = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.request.b.e.a(java.lang.Object, com.bumptech.glide.request.a.c):void
     arg types: [com.bumptech.glide.load.resource.a.b, com.bumptech.glide.request.a.c<? super com.bumptech.glide.load.resource.a.b>]
     candidates:
      com.bumptech.glide.request.b.d.a(com.bumptech.glide.load.resource.a.b, com.bumptech.glide.request.a.c<? super com.bumptech.glide.load.resource.a.b>):void
      com.bumptech.glide.request.b.e.a(java.lang.Exception, android.graphics.drawable.Drawable):void
      com.bumptech.glide.request.b.a.a(java.lang.Exception, android.graphics.drawable.Drawable):void
      com.bumptech.glide.request.b.j.a(java.lang.Exception, android.graphics.drawable.Drawable):void
      com.bumptech.glide.request.b.e.a(java.lang.Object, com.bumptech.glide.request.a.c):void */
    public void a(b bVar, c<? super b> cVar) {
        if (!bVar.a()) {
            float width = ((float) ((ImageView) this.f3338a).getWidth()) / ((float) ((ImageView) this.f3338a).getHeight());
            float intrinsicWidth = ((float) bVar.getIntrinsicWidth()) / ((float) bVar.getIntrinsicHeight());
            if (Math.abs(width - 1.0f) <= 0.05f && Math.abs(intrinsicWidth - 1.0f) <= 0.05f) {
                bVar = new i(bVar, ((ImageView) this.f3338a).getWidth());
            }
        }
        super.a((Object) bVar, (c) cVar);
        this.f3328c = bVar;
        bVar.a(this.f3327b);
        bVar.start();
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        ((ImageView) this.f3338a).setImageDrawable(bVar);
    }

    public void d() {
        if (this.f3328c != null) {
            this.f3328c.start();
        }
    }

    public void e() {
        if (this.f3328c != null) {
            this.f3328c.stop();
        }
    }
}
