package com.bumptech.glide.request.b;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.resource.a.b;

/* compiled from: SquaringDrawable */
public class i extends b {

    /* renamed from: a  reason: collision with root package name */
    private b f3331a;

    /* renamed from: b  reason: collision with root package name */
    private a f3332b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f3333c;

    public i(b bVar, int i) {
        this(new a(bVar.getConstantState(), i), bVar, null);
    }

    i(a aVar, b bVar, Resources resources) {
        this.f3332b = aVar;
        if (bVar != null) {
            this.f3331a = bVar;
        } else if (resources != null) {
            this.f3331a = (b) aVar.f3334a.newDrawable(resources);
        } else {
            this.f3331a = (b) aVar.f3334a.newDrawable();
        }
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i, i2, i3, i4);
        this.f3331a.setBounds(i, i2, i3, i4);
    }

    public void setBounds(Rect rect) {
        super.setBounds(rect);
        this.f3331a.setBounds(rect);
    }

    public void setChangingConfigurations(int i) {
        this.f3331a.setChangingConfigurations(i);
    }

    public int getChangingConfigurations() {
        return this.f3331a.getChangingConfigurations();
    }

    public void setDither(boolean z) {
        this.f3331a.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f3331a.setFilterBitmap(z);
    }

    @TargetApi(11)
    public Drawable.Callback getCallback() {
        return this.f3331a.getCallback();
    }

    @TargetApi(19)
    public int getAlpha() {
        return this.f3331a.getAlpha();
    }

    public void setColorFilter(int i, PorterDuff.Mode mode) {
        this.f3331a.setColorFilter(i, mode);
    }

    public void clearColorFilter() {
        this.f3331a.clearColorFilter();
    }

    public Drawable getCurrent() {
        return this.f3331a.getCurrent();
    }

    public boolean setVisible(boolean z, boolean z2) {
        return this.f3331a.setVisible(z, z2);
    }

    public int getIntrinsicWidth() {
        return this.f3332b.f3335b;
    }

    public int getIntrinsicHeight() {
        return this.f3332b.f3335b;
    }

    public int getMinimumWidth() {
        return this.f3331a.getMinimumWidth();
    }

    public int getMinimumHeight() {
        return this.f3331a.getMinimumHeight();
    }

    public boolean getPadding(Rect rect) {
        return this.f3331a.getPadding(rect);
    }

    public void invalidateSelf() {
        super.invalidateSelf();
        this.f3331a.invalidateSelf();
    }

    public void unscheduleSelf(Runnable runnable) {
        super.unscheduleSelf(runnable);
        this.f3331a.unscheduleSelf(runnable);
    }

    public void scheduleSelf(Runnable runnable, long j) {
        super.scheduleSelf(runnable, j);
        this.f3331a.scheduleSelf(runnable, j);
    }

    public void draw(Canvas canvas) {
        this.f3331a.draw(canvas);
    }

    public void setAlpha(int i) {
        this.f3331a.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f3331a.setColorFilter(colorFilter);
    }

    public int getOpacity() {
        return this.f3331a.getOpacity();
    }

    public boolean a() {
        return this.f3331a.a();
    }

    public void a(int i) {
        this.f3331a.a(i);
    }

    public void start() {
        this.f3331a.start();
    }

    public void stop() {
        this.f3331a.stop();
    }

    public boolean isRunning() {
        return this.f3331a.isRunning();
    }

    public Drawable mutate() {
        if (!this.f3333c && super.mutate() == this) {
            this.f3331a = (b) this.f3331a.mutate();
            this.f3332b = new a(this.f3332b);
            this.f3333c = true;
        }
        return this;
    }

    public Drawable.ConstantState getConstantState() {
        return this.f3332b;
    }

    /* compiled from: SquaringDrawable */
    static class a extends Drawable.ConstantState {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final Drawable.ConstantState f3334a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final int f3335b;

        a(a aVar) {
            this(aVar.f3334a, aVar.f3335b);
        }

        a(Drawable.ConstantState constantState, int i) {
            this.f3334a = constantState;
            this.f3335b = i;
        }

        public Drawable newDrawable() {
            return newDrawable(null);
        }

        public Drawable newDrawable(Resources resources) {
            return new i(this, null, resources);
        }

        public int getChangingConfigurations() {
            return 0;
        }
    }
}
