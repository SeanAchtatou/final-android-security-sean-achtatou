package com.bumptech.glide.request.a;

import com.bumptech.glide.request.a.f;

/* compiled from: ViewAnimationFactory */
public class g<R> implements d<R> {

    /* renamed from: a  reason: collision with root package name */
    private final f.a f3324a;

    /* renamed from: b  reason: collision with root package name */
    private c<R> f3325b;

    g(f.a aVar) {
        this.f3324a = aVar;
    }

    public c<R> a(boolean z, boolean z2) {
        if (z || !z2) {
            return e.b();
        }
        if (this.f3325b == null) {
            this.f3325b = new f(this.f3324a);
        }
        return this.f3325b;
    }
}
