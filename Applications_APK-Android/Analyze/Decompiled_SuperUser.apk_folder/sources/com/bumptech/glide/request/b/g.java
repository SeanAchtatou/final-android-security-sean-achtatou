package com.bumptech.glide.request.b;

import com.bumptech.glide.f.h;

/* compiled from: SimpleTarget */
public abstract class g<Z> extends a<Z> {

    /* renamed from: a  reason: collision with root package name */
    private final int f3329a;

    /* renamed from: b  reason: collision with root package name */
    private final int f3330b;

    public g() {
        this(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    public g(int i, int i2) {
        this.f3329a = i;
        this.f3330b = i2;
    }

    public final void a(h hVar) {
        if (!h.a(this.f3329a, this.f3330b)) {
            throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + this.f3329a + " and height: " + this.f3330b + ", either provide dimensions in the constructor" + " or call override()");
        }
        hVar.a(this.f3329a, this.f3330b);
    }
}
