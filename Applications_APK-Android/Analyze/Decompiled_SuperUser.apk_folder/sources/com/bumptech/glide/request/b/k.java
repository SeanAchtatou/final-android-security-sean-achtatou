package com.bumptech.glide.request.b;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ViewTarget */
public abstract class k<T extends View, Z> extends a<Z> {

    /* renamed from: b  reason: collision with root package name */
    private static boolean f3336b = false;

    /* renamed from: c  reason: collision with root package name */
    private static Integer f3337c = null;

    /* renamed from: a  reason: collision with root package name */
    protected final T f3338a;

    /* renamed from: d  reason: collision with root package name */
    private final a f3339d;

    public k(T t) {
        if (t == null) {
            throw new NullPointerException("View must not be null!");
        }
        this.f3338a = t;
        this.f3339d = new a(t);
    }

    public T a() {
        return this.f3338a;
    }

    public void a(h hVar) {
        this.f3339d.a(hVar);
    }

    public void a(com.bumptech.glide.request.a aVar) {
        a((Object) aVar);
    }

    public com.bumptech.glide.request.a c() {
        Object g2 = g();
        if (g2 == null) {
            return null;
        }
        if (g2 instanceof com.bumptech.glide.request.a) {
            return (com.bumptech.glide.request.a) g2;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void a(java.lang.Object r3) {
        /*
            r2 = this;
            java.lang.Integer r0 = com.bumptech.glide.request.b.k.f3337c
            if (r0 != 0) goto L_0x000d
            r0 = 1
            com.bumptech.glide.request.b.k.f3336b = r0
            T r0 = r2.f3338a
            r0.setTag(r3)
        L_0x000c:
            return
        L_0x000d:
            T r0 = r2.f3338a
            java.lang.Integer r1 = com.bumptech.glide.request.b.k.f3337c
            int r1 = r1.intValue()
            r0.setTag(r1, r3)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.b.k.a(java.lang.Object):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private java.lang.Object g() {
        /*
            r2 = this;
            java.lang.Integer r0 = com.bumptech.glide.request.b.k.f3337c
            if (r0 != 0) goto L_0x000b
            T r0 = r2.f3338a
            java.lang.Object r0 = r0.getTag()
        L_0x000a:
            return r0
        L_0x000b:
            T r0 = r2.f3338a
            java.lang.Integer r1 = com.bumptech.glide.request.b.k.f3337c
            int r1 = r1.intValue()
            java.lang.Object r0 = r0.getTag(r1)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.b.k.g():java.lang.Object");
    }

    public String toString() {
        return "Target for: " + ((Object) this.f3338a);
    }

    /* compiled from: ViewTarget */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        private final View f3340a;

        /* renamed from: b  reason: collision with root package name */
        private final List<h> f3341b = new ArrayList();

        /* renamed from: c  reason: collision with root package name */
        private C0043a f3342c;

        /* renamed from: d  reason: collision with root package name */
        private Point f3343d;

        public a(View view) {
            this.f3340a = view;
        }

        private void a(int i, int i2) {
            for (h a2 : this.f3341b) {
                a2.a(i, i2);
            }
            this.f3341b.clear();
        }

        /* access modifiers changed from: private */
        public void a() {
            if (!this.f3341b.isEmpty()) {
                int c2 = c();
                int b2 = b();
                if (a(c2) && a(b2)) {
                    a(c2, b2);
                    ViewTreeObserver viewTreeObserver = this.f3340a.getViewTreeObserver();
                    if (viewTreeObserver.isAlive()) {
                        viewTreeObserver.removeOnPreDrawListener(this.f3342c);
                    }
                    this.f3342c = null;
                }
            }
        }

        public void a(h hVar) {
            int c2 = c();
            int b2 = b();
            if (!a(c2) || !a(b2)) {
                if (!this.f3341b.contains(hVar)) {
                    this.f3341b.add(hVar);
                }
                if (this.f3342c == null) {
                    ViewTreeObserver viewTreeObserver = this.f3340a.getViewTreeObserver();
                    this.f3342c = new C0043a(this);
                    viewTreeObserver.addOnPreDrawListener(this.f3342c);
                    return;
                }
                return;
            }
            hVar.a(c2, b2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.bumptech.glide.request.b.k.a.a(int, boolean):int
         arg types: [int, int]
         candidates:
          com.bumptech.glide.request.b.k.a.a(int, int):void
          com.bumptech.glide.request.b.k.a.a(int, boolean):int */
        private int b() {
            ViewGroup.LayoutParams layoutParams = this.f3340a.getLayoutParams();
            if (a(this.f3340a.getHeight())) {
                return this.f3340a.getHeight();
            }
            if (layoutParams != null) {
                return a(layoutParams.height, true);
            }
            return 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.bumptech.glide.request.b.k.a.a(int, boolean):int
         arg types: [int, int]
         candidates:
          com.bumptech.glide.request.b.k.a.a(int, int):void
          com.bumptech.glide.request.b.k.a.a(int, boolean):int */
        private int c() {
            ViewGroup.LayoutParams layoutParams = this.f3340a.getLayoutParams();
            if (a(this.f3340a.getWidth())) {
                return this.f3340a.getWidth();
            }
            if (layoutParams != null) {
                return a(layoutParams.width, false);
            }
            return 0;
        }

        private int a(int i, boolean z) {
            if (i != -2) {
                return i;
            }
            Point d2 = d();
            return z ? d2.y : d2.x;
        }

        @TargetApi(13)
        private Point d() {
            if (this.f3343d != null) {
                return this.f3343d;
            }
            Display defaultDisplay = ((WindowManager) this.f3340a.getContext().getSystemService("window")).getDefaultDisplay();
            if (Build.VERSION.SDK_INT >= 13) {
                this.f3343d = new Point();
                defaultDisplay.getSize(this.f3343d);
            } else {
                this.f3343d = new Point(defaultDisplay.getWidth(), defaultDisplay.getHeight());
            }
            return this.f3343d;
        }

        private boolean a(int i) {
            return i > 0 || i == -2;
        }

        /* renamed from: com.bumptech.glide.request.b.k$a$a  reason: collision with other inner class name */
        /* compiled from: ViewTarget */
        private static class C0043a implements ViewTreeObserver.OnPreDrawListener {

            /* renamed from: a  reason: collision with root package name */
            private final WeakReference<a> f3344a;

            public C0043a(a aVar) {
                this.f3344a = new WeakReference<>(aVar);
            }

            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called listener=" + this);
                }
                a aVar = this.f3344a.get();
                if (aVar == null) {
                    return true;
                }
                aVar.a();
                return true;
            }
        }
    }
}
