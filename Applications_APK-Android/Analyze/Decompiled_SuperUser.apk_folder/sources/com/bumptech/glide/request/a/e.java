package com.bumptech.glide.request.a;

import com.bumptech.glide.request.a.c;

/* compiled from: NoAnimation */
public class e<R> implements c<R> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final e<?> f3321a = new e<>();

    /* renamed from: b  reason: collision with root package name */
    private static final d<?> f3322b = new a();

    /* compiled from: NoAnimation */
    public static class a<R> implements d<R> {
        public c<R> a(boolean z, boolean z2) {
            return e.f3321a;
        }
    }

    public static <R> d<R> a() {
        return f3322b;
    }

    public static <R> c<R> b() {
        return f3321a;
    }

    public boolean a(Object obj, c.a aVar) {
        return false;
    }
}
