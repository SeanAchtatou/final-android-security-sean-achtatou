package com.bumptech.glide.request;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.a.c;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.b;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.f;
import com.bumptech.glide.request.a.d;
import com.bumptech.glide.request.b.h;
import com.bumptech.glide.request.b.j;
import java.util.Queue;

public final class GenericRequest<A, T, Z, R> implements a, h, d {

    /* renamed from: a  reason: collision with root package name */
    private static final Queue<GenericRequest<?, ?, ?, ?>> f3298a = com.bumptech.glide.f.h.a(0);
    private i<?> A;
    private b.c B;
    private long C;
    private Status D;

    /* renamed from: b  reason: collision with root package name */
    private final String f3299b = String.valueOf(hashCode());

    /* renamed from: c  reason: collision with root package name */
    private com.bumptech.glide.load.b f3300c;

    /* renamed from: d  reason: collision with root package name */
    private Drawable f3301d;

    /* renamed from: e  reason: collision with root package name */
    private int f3302e;

    /* renamed from: f  reason: collision with root package name */
    private int f3303f;

    /* renamed from: g  reason: collision with root package name */
    private int f3304g;

    /* renamed from: h  reason: collision with root package name */
    private Context f3305h;
    private f<Z> i;
    private com.bumptech.glide.d.f<A, T, Z, R> j;
    private b k;
    private A l;
    private Class<R> m;
    private boolean n;
    private Priority o;
    private j<R> p;
    private c<? super A, R> q;
    private float r;
    private b s;
    private d<R> t;
    private int u;
    private int v;
    private DiskCacheStrategy w;
    private Drawable x;
    private Drawable y;
    private boolean z;

    private enum Status {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CANCELLED,
        CLEARED,
        PAUSED
    }

    public static <A, T, Z, R> GenericRequest<A, T, Z, R> a(com.bumptech.glide.d.f<A, T, Z, R> fVar, A a2, com.bumptech.glide.load.b bVar, Context context, Priority priority, j<R> jVar, float f2, Drawable drawable, int i2, Drawable drawable2, int i3, Drawable drawable3, int i4, c<? super A, R> cVar, b bVar2, b bVar3, f<Z> fVar2, Class<R> cls, boolean z2, d<R> dVar, int i5, int i6, DiskCacheStrategy diskCacheStrategy) {
        GenericRequest<A, T, Z, R> poll = f3298a.poll();
        if (poll == null) {
            poll = new GenericRequest<>();
        }
        poll.b(fVar, a2, bVar, context, priority, jVar, f2, drawable, i2, drawable2, i3, drawable3, i4, cVar, bVar2, bVar3, fVar2, cls, z2, dVar, i5, i6, diskCacheStrategy);
        return poll;
    }

    private GenericRequest() {
    }

    public void a() {
        this.j = null;
        this.l = null;
        this.f3305h = null;
        this.p = null;
        this.x = null;
        this.y = null;
        this.f3301d = null;
        this.q = null;
        this.k = null;
        this.i = null;
        this.t = null;
        this.z = false;
        this.B = null;
        f3298a.offer(this);
    }

    private void b(com.bumptech.glide.d.f<A, T, Z, R> fVar, A a2, com.bumptech.glide.load.b bVar, Context context, Priority priority, j<R> jVar, float f2, Drawable drawable, int i2, Drawable drawable2, int i3, Drawable drawable3, int i4, c<? super A, R> cVar, b bVar2, b bVar3, f<Z> fVar2, Class<R> cls, boolean z2, d<R> dVar, int i5, int i6, DiskCacheStrategy diskCacheStrategy) {
        this.j = fVar;
        this.l = a2;
        this.f3300c = bVar;
        this.f3301d = drawable3;
        this.f3302e = i4;
        this.f3305h = context.getApplicationContext();
        this.o = priority;
        this.p = jVar;
        this.r = f2;
        this.x = drawable;
        this.f3303f = i2;
        this.y = drawable2;
        this.f3304g = i3;
        this.q = cVar;
        this.k = bVar2;
        this.s = bVar3;
        this.i = fVar2;
        this.m = cls;
        this.n = z2;
        this.t = dVar;
        this.u = i5;
        this.v = i6;
        this.w = diskCacheStrategy;
        this.D = Status.PENDING;
        if (a2 != null) {
            a("ModelLoader", fVar.e(), "try .using(ModelLoader)");
            a("Transcoder", fVar.f(), "try .as*(Class).transcode(ResourceTranscoder)");
            a("Transformation", fVar2, "try .transform(UnitTransformation.get())");
            if (diskCacheStrategy.a()) {
                a("SourceEncoder", fVar.c(), "try .sourceEncoder(Encoder) or .diskCacheStrategy(NONE/RESULT)");
            } else {
                a("SourceDecoder", fVar.b(), "try .decoder/.imageDecoder/.videoDecoder(ResourceDecoder) or .diskCacheStrategy(ALL/SOURCE)");
            }
            if (diskCacheStrategy.a() || diskCacheStrategy.b()) {
                a("CacheDecoder", fVar.a(), "try .cacheDecoder(ResouceDecoder) or .diskCacheStrategy(NONE)");
            }
            if (diskCacheStrategy.b()) {
                a("Encoder", fVar.d(), "try .encode(ResourceEncoder) or .diskCacheStrategy(NONE/SOURCE)");
            }
        }
    }

    private static void a(String str, Object obj, String str2) {
        if (obj == null) {
            StringBuilder sb = new StringBuilder(str);
            sb.append(" must not be null");
            if (str2 != null) {
                sb.append(", ");
                sb.append(str2);
            }
            throw new NullPointerException(sb.toString());
        }
    }

    public void b() {
        this.C = com.bumptech.glide.f.d.a();
        if (this.l == null) {
            a((Exception) null);
            return;
        }
        this.D = Status.WAITING_FOR_SIZE;
        if (com.bumptech.glide.f.h.a(this.u, this.v)) {
            a(this.u, this.v);
        } else {
            this.p.a((h) this);
        }
        if (!g() && !j() && o()) {
            this.p.c(m());
        }
        if (Log.isLoggable("GenericRequest", 2)) {
            a("finished run method in " + com.bumptech.glide.f.d.a(this.C));
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.D = Status.CANCELLED;
        if (this.B != null) {
            this.B.a();
            this.B = null;
        }
    }

    public void d() {
        com.bumptech.glide.f.h.a();
        if (this.D != Status.CLEARED) {
            c();
            if (this.A != null) {
                b(this.A);
            }
            if (o()) {
                this.p.b(m());
            }
            this.D = Status.CLEARED;
        }
    }

    public void e() {
        d();
        this.D = Status.PAUSED;
    }

    private void b(i iVar) {
        this.s.a(iVar);
        this.A = null;
    }

    public boolean f() {
        return this.D == Status.RUNNING || this.D == Status.WAITING_FOR_SIZE;
    }

    public boolean g() {
        return this.D == Status.COMPLETE;
    }

    public boolean h() {
        return g();
    }

    public boolean i() {
        return this.D == Status.CANCELLED || this.D == Status.CLEARED;
    }

    public boolean j() {
        return this.D == Status.FAILED;
    }

    private Drawable k() {
        if (this.f3301d == null && this.f3302e > 0) {
            this.f3301d = this.f3305h.getResources().getDrawable(this.f3302e);
        }
        return this.f3301d;
    }

    private void b(Exception exc) {
        if (o()) {
            Drawable k2 = this.l == null ? k() : null;
            if (k2 == null) {
                k2 = l();
            }
            if (k2 == null) {
                k2 = m();
            }
            this.p.a(exc, k2);
        }
    }

    private Drawable l() {
        if (this.y == null && this.f3304g > 0) {
            this.y = this.f3305h.getResources().getDrawable(this.f3304g);
        }
        return this.y;
    }

    private Drawable m() {
        if (this.x == null && this.f3303f > 0) {
            this.x = this.f3305h.getResources().getDrawable(this.f3303f);
        }
        return this.x;
    }

    public void a(int i2, int i3) {
        if (Log.isLoggable("GenericRequest", 2)) {
            a("Got onSizeReady in " + com.bumptech.glide.f.d.a(this.C));
        }
        if (this.D == Status.WAITING_FOR_SIZE) {
            this.D = Status.RUNNING;
            int round = Math.round(this.r * ((float) i2));
            int round2 = Math.round(this.r * ((float) i3));
            c<T> a2 = this.j.e().a(this.l, round, round2);
            if (a2 == null) {
                a(new Exception("Failed to load model: '" + ((Object) this.l) + "'"));
                return;
            }
            com.bumptech.glide.load.resource.transcode.b<Z, R> f2 = this.j.f();
            if (Log.isLoggable("GenericRequest", 2)) {
                a("finished setup for calling load in " + com.bumptech.glide.f.d.a(this.C));
            }
            this.z = true;
            this.B = this.s.a(this.f3300c, round, round2, a2, this.j, this.i, f2, this.o, this.n, this.w, this);
            this.z = this.A != null;
            if (Log.isLoggable("GenericRequest", 2)) {
                a("finished onSizeReady in " + com.bumptech.glide.f.d.a(this.C));
            }
        }
    }

    private boolean n() {
        return this.k == null || this.k.a(this);
    }

    private boolean o() {
        return this.k == null || this.k.b(this);
    }

    private boolean p() {
        return this.k == null || !this.k.c();
    }

    private void q() {
        if (this.k != null) {
            this.k.c(this);
        }
    }

    public void a(i<?> iVar) {
        if (iVar == null) {
            a(new Exception("Expected to receive a Resource<R> with an object of " + this.m + " inside, but instead got null."));
            return;
        }
        Object b2 = iVar.b();
        if (b2 == null || !this.m.isAssignableFrom(b2.getClass())) {
            b(iVar);
            a(new Exception("Expected to receive an object of " + this.m + " but instead got " + (b2 != null ? b2.getClass() : "") + "{" + b2 + "}" + " inside Resource{" + iVar + "}." + (b2 != null ? "" : " To indicate failure return a null Resource object, rather than a Resource object containing null data.")));
        } else if (!n()) {
            b(iVar);
            this.D = Status.COMPLETE;
        } else {
            a(iVar, b2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001b, code lost:
        if (r6.q.a(r8, r6.l, r6.p, r6.z, r5) == false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.bumptech.glide.load.engine.i<?> r7, R r8) {
        /*
            r6 = this;
            boolean r5 = r6.p()
            com.bumptech.glide.request.GenericRequest$Status r0 = com.bumptech.glide.request.GenericRequest.Status.COMPLETE
            r6.D = r0
            r6.A = r7
            com.bumptech.glide.request.c<? super A, R> r0 = r6.q
            if (r0 == 0) goto L_0x001d
            com.bumptech.glide.request.c<? super A, R> r0 = r6.q
            A r2 = r6.l
            com.bumptech.glide.request.b.j<R> r3 = r6.p
            boolean r4 = r6.z
            r1 = r8
            boolean r0 = r0.a(r1, r2, r3, r4, r5)
            if (r0 != 0) goto L_0x002a
        L_0x001d:
            com.bumptech.glide.request.a.d<R> r0 = r6.t
            boolean r1 = r6.z
            com.bumptech.glide.request.a.c r0 = r0.a(r1, r5)
            com.bumptech.glide.request.b.j<R> r1 = r6.p
            r1.a(r8, r0)
        L_0x002a:
            r6.q()
            java.lang.String r0 = "GenericRequest"
            r1 = 2
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x0070
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Resource ready in "
            java.lang.StringBuilder r0 = r0.append(r1)
            long r2 = r6.C
            double r2 = com.bumptech.glide.f.d.a(r2)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = " size: "
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r7.c()
            double r2 = (double) r1
            r4 = 4517110426252607488(0x3eb0000000000000, double:9.5367431640625E-7)
            double r2 = r2 * r4
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = " fromCache: "
            java.lang.StringBuilder r0 = r0.append(r1)
            boolean r1 = r6.z
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r6.a(r0)
        L_0x0070:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.request.GenericRequest.a(com.bumptech.glide.load.engine.i, java.lang.Object):void");
    }

    public void a(Exception exc) {
        if (Log.isLoggable("GenericRequest", 3)) {
            Log.d("GenericRequest", "load failed", exc);
        }
        this.D = Status.FAILED;
        if (this.q == null || !this.q.a(exc, this.l, this.p, p())) {
            b(exc);
        }
    }

    private void a(String str) {
        Log.v("GenericRequest", str + " this: " + this.f3299b);
    }
}
