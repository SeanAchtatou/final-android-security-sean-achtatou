package com.bumptech.glide;

import android.content.Context;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.d.e;
import com.bumptech.glide.f;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.resource.c.a;
import com.bumptech.glide.manager.g;
import com.bumptech.glide.manager.m;
import java.io.InputStream;

/* compiled from: DrawableTypeRequest */
public class b<ModelType> extends a<ModelType> {

    /* renamed from: g  reason: collision with root package name */
    private final k<ModelType, InputStream> f2790g;

    /* renamed from: h  reason: collision with root package name */
    private final k<ModelType, ParcelFileDescriptor> f2791h;
    private final f.c i;

    private static <A, Z, R> e<A, com.bumptech.glide.load.model.f, Z, R> a(e eVar, k<A, InputStream> kVar, k<A, ParcelFileDescriptor> kVar2, Class<Z> cls, Class<R> cls2, com.bumptech.glide.load.resource.transcode.b<Z, R> bVar) {
        if (kVar == null && kVar2 == null) {
            return null;
        }
        if (bVar == null) {
            bVar = eVar.a(cls, cls2);
        }
        return new e<>(new com.bumptech.glide.load.model.e(kVar, kVar2), bVar, eVar.b(com.bumptech.glide.load.model.f.class, cls));
    }

    b(Class<ModelType> cls, k<ModelType, InputStream> kVar, k<ModelType, ParcelFileDescriptor> kVar2, Context context, e eVar, m mVar, g gVar, f.c cVar) {
        super(context, cls, a(eVar, kVar, kVar2, a.class, com.bumptech.glide.load.resource.a.b.class, null), eVar, mVar, gVar);
        this.f2790g = kVar;
        this.f2791h = kVar2;
        this.i = cVar;
    }
}
