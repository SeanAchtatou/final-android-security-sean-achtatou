package com.bumptech.glide;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.manager.c;
import com.bumptech.glide.manager.g;
import com.bumptech.glide.manager.h;
import com.bumptech.glide.manager.l;
import com.bumptech.glide.manager.m;
import java.io.InputStream;

/* compiled from: RequestManager */
public class f implements h {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Context f2876a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final g f2877b;

    /* renamed from: c  reason: collision with root package name */
    private final l f2878c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final m f2879d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final e f2880e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final c f2881f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public a f2882g;

    /* compiled from: RequestManager */
    public interface a {
        <T> void a(c<T, ?, ?, ?> cVar);
    }

    public f(Context context, g gVar, l lVar) {
        this(context, gVar, lVar, new m(), new com.bumptech.glide.manager.d());
    }

    f(Context context, final g gVar, l lVar, m mVar, com.bumptech.glide.manager.d dVar) {
        this.f2876a = context.getApplicationContext();
        this.f2877b = gVar;
        this.f2878c = lVar;
        this.f2879d = mVar;
        this.f2880e = e.a(context);
        this.f2881f = new c();
        com.bumptech.glide.manager.c a2 = dVar.a(context, new d(mVar));
        if (com.bumptech.glide.f.h.c()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    gVar.a(f.this);
                }
            });
        } else {
            gVar.a(this);
        }
        gVar.a(a2);
    }

    public void a(int i) {
        this.f2880e.a(i);
    }

    public void a() {
        this.f2880e.e();
    }

    public void b() {
        com.bumptech.glide.f.h.a();
        this.f2879d.a();
    }

    public void c() {
        com.bumptech.glide.f.h.a();
        this.f2879d.b();
    }

    public void d() {
        c();
    }

    public void e() {
        b();
    }

    public void f() {
        this.f2879d.c();
    }

    public <A, T> b<A, T> a(k<A, T> kVar, Class<T> cls) {
        return new b<>(kVar, cls);
    }

    public b<String> a(String str) {
        return (b) g().b(str);
    }

    public b<String> g() {
        return a(String.class);
    }

    private <T> b<T> a(Class<T> cls) {
        k<T, InputStream> a2 = e.a(cls, this.f2876a);
        k<T, ParcelFileDescriptor> b2 = e.b(cls, this.f2876a);
        if (cls != null && a2 == null && b2 == null) {
            throw new IllegalArgumentException("Unknown type " + cls + ". You must provide a Model of a type for" + " which there is a registered ModelLoader, if you are using a custom model, you must first call" + " Glide#register with a ModelLoaderFactory for your custom model class");
        }
        return (b) this.f2881f.a(new b(cls, a2, b2, this.f2876a, this.f2880e, this.f2879d, this.f2877b, this.f2881f));
    }

    /* access modifiers changed from: private */
    public static <T> Class<T> b(T t) {
        if (t != null) {
            return t.getClass();
        }
        return null;
    }

    /* compiled from: RequestManager */
    public final class b<A, T> {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final k<A, T> f2888b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public final Class<T> f2889c;

        b(k<A, T> kVar, Class<T> cls) {
            this.f2888b = kVar;
            this.f2889c = cls;
        }

        public b<A, T>.a a(A a2) {
            return new a(a2);
        }

        /* compiled from: RequestManager */
        public final class a {

            /* renamed from: b  reason: collision with root package name */
            private final A f2893b;

            /* renamed from: c  reason: collision with root package name */
            private final Class<A> f2894c;

            /* renamed from: d  reason: collision with root package name */
            private final boolean f2895d = true;

            a(A a2) {
                this.f2893b = a2;
                this.f2894c = f.b((Object) a2);
            }

            public <Z> d<A, T, Z> a(Class<Z> cls) {
                d<A, T, Z> dVar = (d) f.this.f2881f.a(new d(f.this.f2876a, f.this.f2880e, this.f2894c, b.this.f2888b, b.this.f2889c, cls, f.this.f2879d, f.this.f2877b, f.this.f2881f));
                if (this.f2895d) {
                    dVar.b((Object) this.f2893b);
                }
                return dVar;
            }
        }
    }

    /* compiled from: RequestManager */
    class c {
        c() {
        }

        public <A, X extends c<A, ?, ?, ?>> X a(X x) {
            if (f.this.f2882g != null) {
                f.this.f2882g.a(x);
            }
            return x;
        }
    }

    /* compiled from: RequestManager */
    private static class d implements c.a {

        /* renamed from: a  reason: collision with root package name */
        private final m f2900a;

        public d(m mVar) {
            this.f2900a = mVar;
        }

        public void a(boolean z) {
            if (z) {
                this.f2900a.d();
            }
        }
    }
}
