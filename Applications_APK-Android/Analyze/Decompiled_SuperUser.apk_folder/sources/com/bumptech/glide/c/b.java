package com.bumptech.glide.c;

import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.os.VUserInfo;
import java.io.OutputStream;

/* compiled from: LZWEncoder */
class b {

    /* renamed from: a  reason: collision with root package name */
    int f2837a;

    /* renamed from: b  reason: collision with root package name */
    int f2838b = 12;

    /* renamed from: c  reason: collision with root package name */
    int f2839c;

    /* renamed from: d  reason: collision with root package name */
    int f2840d = CodedOutputStream.DEFAULT_BUFFER_SIZE;

    /* renamed from: e  reason: collision with root package name */
    int[] f2841e = new int[5003];

    /* renamed from: f  reason: collision with root package name */
    int[] f2842f = new int[5003];

    /* renamed from: g  reason: collision with root package name */
    int f2843g = 5003;

    /* renamed from: h  reason: collision with root package name */
    int f2844h = 0;
    boolean i = false;
    int j;
    int k;
    int l;
    int m = 0;
    int n = 0;
    int[] o = {0, 1, 3, 7, 15, 31, 63, 127, VUserInfo.FLAG_MASK_USER_TYPE, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535};
    int p;
    byte[] q = new byte[FileUtils.FileMode.MODE_IRUSR];
    private int r;
    private int s;
    private byte[] t;
    private int u;
    private int v;
    private int w;

    b(int i2, int i3, byte[] bArr, int i4) {
        this.r = i2;
        this.s = i3;
        this.t = bArr;
        this.u = Math.max(2, i4);
    }

    /* access modifiers changed from: package-private */
    public void a(byte b2, OutputStream outputStream) {
        byte[] bArr = this.q;
        int i2 = this.p;
        this.p = i2 + 1;
        bArr[i2] = b2;
        if (this.p >= 254) {
            c(outputStream);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(OutputStream outputStream) {
        a(this.f2843g);
        this.f2844h = this.k + 2;
        this.i = true;
        b(this.k, outputStream);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            this.f2841e[i3] = -1;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, OutputStream outputStream) {
        int i3 = 0;
        this.j = i2;
        this.i = false;
        this.f2837a = this.j;
        this.f2839c = b(this.f2837a);
        this.k = 1 << (i2 - 1);
        this.l = this.k + 1;
        this.f2844h = this.k + 2;
        this.p = 0;
        int a2 = a();
        for (int i4 = this.f2843g; i4 < 65536; i4 *= 2) {
            i3++;
        }
        int i5 = 8 - i3;
        int i6 = this.f2843g;
        a(i6);
        b(this.k, outputStream);
        while (true) {
            int a3 = a();
            if (a3 != -1) {
                int i7 = (a3 << this.f2838b) + a2;
                int i8 = (a3 << i5) ^ a2;
                if (this.f2841e[i8] == i7) {
                    a2 = this.f2842f[i8];
                } else if (this.f2841e[i8] >= 0) {
                    int i9 = i6 - i8;
                    if (i8 == 0) {
                        i9 = 1;
                    }
                    while (true) {
                        i8 -= i9;
                        if (i8 < 0) {
                            i8 += i6;
                        }
                        if (this.f2841e[i8] != i7) {
                            if (this.f2841e[i8] < 0) {
                                break;
                            }
                        } else {
                            a2 = this.f2842f[i8];
                            break;
                        }
                    }
                } else {
                    b(a2, outputStream);
                    if (this.f2844h < this.f2840d) {
                        int[] iArr = this.f2842f;
                        int i10 = this.f2844h;
                        this.f2844h = i10 + 1;
                        iArr[i8] = i10;
                        this.f2841e[i8] = i7;
                        a2 = a3;
                    } else {
                        a(outputStream);
                        a2 = a3;
                    }
                }
            } else {
                b(a2, outputStream);
                b(this.l, outputStream);
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(OutputStream outputStream) {
        outputStream.write(this.u);
        this.v = this.r * this.s;
        this.w = 0;
        a(this.u + 1, outputStream);
        outputStream.write(0);
    }

    /* access modifiers changed from: package-private */
    public void c(OutputStream outputStream) {
        if (this.p > 0) {
            outputStream.write(this.p);
            outputStream.write(this.q, 0, this.p);
            this.p = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public final int b(int i2) {
        return (1 << i2) - 1;
    }

    private int a() {
        if (this.v == 0) {
            return -1;
        }
        this.v--;
        byte[] bArr = this.t;
        int i2 = this.w;
        this.w = i2 + 1;
        return bArr[i2] & 255;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, OutputStream outputStream) {
        this.m &= this.o[this.n];
        if (this.n > 0) {
            this.m |= i2 << this.n;
        } else {
            this.m = i2;
        }
        this.n += this.f2837a;
        while (this.n >= 8) {
            a((byte) (this.m & VUserInfo.FLAG_MASK_USER_TYPE), outputStream);
            this.m >>= 8;
            this.n -= 8;
        }
        if (this.f2844h > this.f2839c || this.i) {
            if (this.i) {
                int i3 = this.j;
                this.f2837a = i3;
                this.f2839c = b(i3);
                this.i = false;
            } else {
                this.f2837a++;
                if (this.f2837a == this.f2838b) {
                    this.f2839c = this.f2840d;
                } else {
                    this.f2839c = b(this.f2837a);
                }
            }
        }
        if (i2 == this.l) {
            while (this.n > 0) {
                a((byte) (this.m & VUserInfo.FLAG_MASK_USER_TYPE), outputStream);
                this.m >>= 8;
                this.n -= 8;
            }
            c(outputStream);
        }
    }
}
