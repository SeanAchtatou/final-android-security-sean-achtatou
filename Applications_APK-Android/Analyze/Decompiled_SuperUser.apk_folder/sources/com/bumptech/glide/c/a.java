package com.bumptech.glide.c;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.os.VUserInfo;
import java.io.IOException;
import java.io.OutputStream;

/* compiled from: AnimatedGifEncoder */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private int f2829a;

    /* renamed from: b  reason: collision with root package name */
    private int f2830b;

    /* renamed from: c  reason: collision with root package name */
    private Integer f2831c = null;

    /* renamed from: d  reason: collision with root package name */
    private int f2832d;

    /* renamed from: e  reason: collision with root package name */
    private int f2833e = -1;

    /* renamed from: f  reason: collision with root package name */
    private int f2834f = 0;

    /* renamed from: g  reason: collision with root package name */
    private boolean f2835g = false;

    /* renamed from: h  reason: collision with root package name */
    private OutputStream f2836h;
    private Bitmap i;
    private byte[] j;
    private byte[] k;
    private int l;
    private byte[] m;
    private boolean[] n = new boolean[FileUtils.FileMode.MODE_IRUSR];
    private int o = 7;
    private int p = -1;
    private boolean q = false;
    private boolean r = true;
    private boolean s = false;
    private int t = 10;
    private boolean u;

    public void a(int i2) {
        this.f2834f = Math.round(((float) i2) / 10.0f);
    }

    public boolean a(Bitmap bitmap) {
        if (bitmap == null || !this.f2835g) {
            return false;
        }
        try {
            if (!this.s) {
                a(bitmap.getWidth(), bitmap.getHeight());
            }
            this.i = bitmap;
            c();
            b();
            if (this.r) {
                f();
                h();
                if (this.f2833e >= 0) {
                    g();
                }
            }
            d();
            e();
            if (!this.r) {
                h();
            }
            i();
            this.r = false;
            return true;
        } catch (IOException e2) {
            return false;
        }
    }

    public boolean a() {
        boolean z;
        if (!this.f2835g) {
            return false;
        }
        this.f2835g = false;
        try {
            this.f2836h.write(59);
            this.f2836h.flush();
            if (this.q) {
                this.f2836h.close();
            }
            z = true;
        } catch (IOException e2) {
            z = false;
        }
        this.f2832d = 0;
        this.f2836h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.m = null;
        this.q = false;
        this.r = true;
        return z;
    }

    public void a(int i2, int i3) {
        if (!this.f2835g || this.r) {
            this.f2829a = i2;
            this.f2830b = i3;
            if (this.f2829a < 1) {
                this.f2829a = 320;
            }
            if (this.f2830b < 1) {
                this.f2830b = 240;
            }
            this.s = true;
        }
    }

    public boolean a(OutputStream outputStream) {
        if (outputStream == null) {
            return false;
        }
        boolean z = true;
        this.q = false;
        this.f2836h = outputStream;
        try {
            a("GIF89a");
        } catch (IOException e2) {
            z = false;
        }
        this.f2835g = z;
        return z;
    }

    private void b() {
        int length = this.j.length;
        int i2 = length / 3;
        this.k = new byte[i2];
        c cVar = new c(this.j, length, this.t);
        this.m = cVar.d();
        for (int i3 = 0; i3 < this.m.length; i3 += 3) {
            byte b2 = this.m[i3];
            this.m[i3] = this.m[i3 + 2];
            this.m[i3 + 2] = b2;
            this.n[i3 / 3] = false;
        }
        int i4 = 0;
        for (int i5 = 0; i5 < i2; i5++) {
            int i6 = i4 + 1;
            int i7 = i6 + 1;
            i4 = i7 + 1;
            int a2 = cVar.a(this.j[i4] & 255, this.j[i6] & 255, this.j[i7] & 255);
            this.n[a2] = true;
            this.k[i5] = (byte) a2;
        }
        this.j = null;
        this.l = 8;
        this.o = 7;
        if (this.f2831c != null) {
            this.f2832d = b(this.f2831c.intValue());
        } else if (this.u) {
            this.f2832d = b(0);
        }
    }

    private int b(int i2) {
        int i3;
        int i4 = 0;
        if (this.m == null) {
            return -1;
        }
        int red = Color.red(i2);
        int green = Color.green(i2);
        int blue = Color.blue(i2);
        int i5 = 16777216;
        int length = this.m.length;
        int i6 = 0;
        while (i4 < length) {
            int i7 = i4 + 1;
            int i8 = red - (this.m[i4] & 255);
            int i9 = i7 + 1;
            int i10 = green - (this.m[i7] & 255);
            int i11 = blue - (this.m[i9] & 255);
            int i12 = (i8 * i8) + (i10 * i10) + (i11 * i11);
            int i13 = i9 / 3;
            if (!this.n[i13] || i12 >= i5) {
                i12 = i5;
                i3 = i6;
            } else {
                i3 = i13;
            }
            i6 = i3;
            i5 = i12;
            i4 = i9 + 1;
        }
        return i6;
    }

    private void c() {
        boolean z = false;
        int width = this.i.getWidth();
        int height = this.i.getHeight();
        if (!(width == this.f2829a && height == this.f2830b)) {
            Bitmap createBitmap = Bitmap.createBitmap(this.f2829a, this.f2830b, Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(createBitmap, 0.0f, 0.0f, (Paint) null);
            this.i = createBitmap;
        }
        int[] iArr = new int[(width * height)];
        this.i.getPixels(iArr, 0, width, 0, 0, width, height);
        this.j = new byte[(iArr.length * 3)];
        this.u = false;
        int i2 = 0;
        int i3 = 0;
        for (int i4 : iArr) {
            if (i4 == 0) {
                i2++;
            }
            int i5 = i3 + 1;
            this.j[i3] = (byte) (i4 & VUserInfo.FLAG_MASK_USER_TYPE);
            int i6 = i5 + 1;
            this.j[i5] = (byte) ((i4 >> 8) & VUserInfo.FLAG_MASK_USER_TYPE);
            i3 = i6 + 1;
            this.j[i6] = (byte) ((i4 >> 16) & VUserInfo.FLAG_MASK_USER_TYPE);
        }
        double length = ((double) (i2 * 100)) / ((double) iArr.length);
        if (length > 4.0d) {
            z = true;
        }
        this.u = z;
        if (Log.isLoggable("AnimatedGifEncoder", 3)) {
            Log.d("AnimatedGifEncoder", "got pixels for frame with " + length + "% transparent pixels");
        }
    }

    private void d() {
        int i2;
        int i3;
        this.f2836h.write(33);
        this.f2836h.write(249);
        this.f2836h.write(4);
        if (this.f2831c != null || this.u) {
            i2 = 1;
            i3 = 2;
        } else {
            i3 = 0;
            i2 = 0;
        }
        if (this.p >= 0) {
            i3 = this.p & 7;
        }
        this.f2836h.write((i3 << 2) | 0 | 0 | i2);
        c(this.f2834f);
        this.f2836h.write(this.f2832d);
        this.f2836h.write(0);
    }

    private void e() {
        this.f2836h.write(44);
        c(0);
        c(0);
        c(this.f2829a);
        c(this.f2830b);
        if (this.r) {
            this.f2836h.write(0);
        } else {
            this.f2836h.write(this.o | FileUtils.FileMode.MODE_IWUSR);
        }
    }

    private void f() {
        c(this.f2829a);
        c(this.f2830b);
        this.f2836h.write(this.o | 240);
        this.f2836h.write(0);
        this.f2836h.write(0);
    }

    private void g() {
        this.f2836h.write(33);
        this.f2836h.write((int) VUserInfo.FLAG_MASK_USER_TYPE);
        this.f2836h.write(11);
        a("NETSCAPE2.0");
        this.f2836h.write(3);
        this.f2836h.write(1);
        c(this.f2833e);
        this.f2836h.write(0);
    }

    private void h() {
        this.f2836h.write(this.m, 0, this.m.length);
        int length = 768 - this.m.length;
        for (int i2 = 0; i2 < length; i2++) {
            this.f2836h.write(0);
        }
    }

    private void i() {
        new b(this.f2829a, this.f2830b, this.k, this.l).b(this.f2836h);
    }

    private void c(int i2) {
        this.f2836h.write(i2 & VUserInfo.FLAG_MASK_USER_TYPE);
        this.f2836h.write((i2 >> 8) & VUserInfo.FLAG_MASK_USER_TYPE);
    }

    private void a(String str) {
        for (int i2 = 0; i2 < str.length(); i2++) {
            this.f2836h.write((byte) str.charAt(i2));
        }
    }
}
