package com.bumptech.glide.a;

import com.kingouser.com.util.ShellUtils;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: DiskLruCache */
public final class a implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    final ThreadPoolExecutor f2756a = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue());
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final File f2757b;

    /* renamed from: c  reason: collision with root package name */
    private final File f2758c;

    /* renamed from: d  reason: collision with root package name */
    private final File f2759d;

    /* renamed from: e  reason: collision with root package name */
    private final File f2760e;

    /* renamed from: f  reason: collision with root package name */
    private final int f2761f;

    /* renamed from: g  reason: collision with root package name */
    private long f2762g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public final int f2763h;
    private long i = 0;
    /* access modifiers changed from: private */
    public Writer j;
    private final LinkedHashMap<String, b> k = new LinkedHashMap<>(0, 0.75f, true);
    /* access modifiers changed from: private */
    public int l;
    private long m = 0;
    private final Callable<Void> n = new Callable<Void>() {
        /* renamed from: a */
        public Void call() {
            synchronized (a.this) {
                if (a.this.j != null) {
                    a.this.g();
                    if (a.this.e()) {
                        a.this.d();
                        int unused = a.this.l = 0;
                    }
                }
            }
            return null;
        }
    };

    private a(File file, int i2, int i3, long j2) {
        this.f2757b = file;
        this.f2761f = i2;
        this.f2758c = new File(file, "journal");
        this.f2759d = new File(file, "journal.tmp");
        this.f2760e = new File(file, "journal.bkp");
        this.f2763h = i3;
        this.f2762g = j2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.a.a.a(java.io.File, java.io.File, boolean):void
     arg types: [java.io.File, java.io.File, int]
     candidates:
      com.bumptech.glide.a.a.a(com.bumptech.glide.a.a, com.bumptech.glide.a.a$a, boolean):void
      com.bumptech.glide.a.a.a(java.io.File, java.io.File, boolean):void */
    public static a a(File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 <= 0) {
            throw new IllegalArgumentException("valueCount <= 0");
        } else {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    a(file2, file3, false);
                }
            }
            a aVar = new a(file, i2, i3, j2);
            if (aVar.f2758c.exists()) {
                try {
                    aVar.b();
                    aVar.c();
                    return aVar;
                } catch (IOException e2) {
                    System.out.println("DiskLruCache " + file + " is corrupt: " + e2.getMessage() + ", removing");
                    aVar.a();
                }
            }
            file.mkdirs();
            a aVar2 = new a(file, i2, i3, j2);
            aVar2.d();
            return aVar2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    private void b() {
        int i2;
        b bVar = new b(new FileInputStream(this.f2758c), c.f2788a);
        try {
            String a2 = bVar.a();
            String a3 = bVar.a();
            String a4 = bVar.a();
            String a5 = bVar.a();
            String a6 = bVar.a();
            if (!"libcore.io.DiskLruCache".equals(a2) || !"1".equals(a3) || !Integer.toString(this.f2761f).equals(a4) || !Integer.toString(this.f2763h).equals(a5) || !"".equals(a6)) {
                throw new IOException("unexpected journal header: [" + a2 + ", " + a3 + ", " + a5 + ", " + a6 + "]");
            }
            i2 = 0;
            while (true) {
                d(bVar.a());
                i2++;
            }
        } catch (EOFException e2) {
            this.l = i2 - this.k.size();
            if (bVar.b()) {
                d();
            } else {
                this.j = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f2758c, true), c.f2788a));
            }
            c.a(bVar);
        } catch (Throwable th) {
            c.a(bVar);
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.a.a.b.a(com.bumptech.glide.a.a$b, boolean):boolean
     arg types: [com.bumptech.glide.a.a$b, int]
     candidates:
      com.bumptech.glide.a.a.b.a(com.bumptech.glide.a.a$b, long):long
      com.bumptech.glide.a.a.b.a(com.bumptech.glide.a.a$b, com.bumptech.glide.a.a$a):com.bumptech.glide.a.a$a
      com.bumptech.glide.a.a.b.a(com.bumptech.glide.a.a$b, java.lang.String[]):void
      com.bumptech.glide.a.a.b.a(com.bumptech.glide.a.a$b, boolean):boolean */
    private void d(String str) {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf == -1) {
            throw new IOException("unexpected journal line: " + str);
        }
        int i2 = indexOf + 1;
        int indexOf2 = str.indexOf(32, i2);
        if (indexOf2 == -1) {
            String substring = str.substring(i2);
            if (indexOf != "REMOVE".length() || !str.startsWith("REMOVE")) {
                str2 = substring;
            } else {
                this.k.remove(substring);
                return;
            }
        } else {
            str2 = str.substring(i2, indexOf2);
        }
        b bVar = this.k.get(str2);
        if (bVar == null) {
            bVar = new b(str2);
            this.k.put(str2, bVar);
        }
        if (indexOf2 != -1 && indexOf == "CLEAN".length() && str.startsWith("CLEAN")) {
            String[] split = str.substring(indexOf2 + 1).split(" ");
            boolean unused = bVar.f2774f = true;
            C0035a unused2 = bVar.f2775g = (C0035a) null;
            bVar.a(split);
        } else if (indexOf2 == -1 && indexOf == "DIRTY".length() && str.startsWith("DIRTY")) {
            C0035a unused3 = bVar.f2775g = new C0035a(bVar);
        } else if (indexOf2 != -1 || indexOf != "READ".length() || !str.startsWith("READ")) {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    private void c() {
        a(this.f2759d);
        Iterator<b> it = this.k.values().iterator();
        while (it.hasNext()) {
            b next = it.next();
            if (next.f2775g == null) {
                for (int i2 = 0; i2 < this.f2763h; i2++) {
                    this.i += next.f2773e[i2];
                }
            } else {
                C0035a unused = next.f2775g = (C0035a) null;
                for (int i3 = 0; i3 < this.f2763h; i3++) {
                    a(next.a(i3));
                    a(next.b(i3));
                }
                it.remove();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.a.a.a(java.io.File, java.io.File, boolean):void
     arg types: [java.io.File, java.io.File, int]
     candidates:
      com.bumptech.glide.a.a.a(com.bumptech.glide.a.a, com.bumptech.glide.a.a$a, boolean):void
      com.bumptech.glide.a.a.a(java.io.File, java.io.File, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* access modifiers changed from: private */
    public synchronized void d() {
        if (this.j != null) {
            this.j.close();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f2759d), c.f2788a));
        try {
            bufferedWriter.write("libcore.io.DiskLruCache");
            bufferedWriter.write(ShellUtils.COMMAND_LINE_END);
            bufferedWriter.write("1");
            bufferedWriter.write(ShellUtils.COMMAND_LINE_END);
            bufferedWriter.write(Integer.toString(this.f2761f));
            bufferedWriter.write(ShellUtils.COMMAND_LINE_END);
            bufferedWriter.write(Integer.toString(this.f2763h));
            bufferedWriter.write(ShellUtils.COMMAND_LINE_END);
            bufferedWriter.write(ShellUtils.COMMAND_LINE_END);
            for (b next : this.k.values()) {
                if (next.f2775g != null) {
                    bufferedWriter.write("DIRTY " + next.f2772d + 10);
                } else {
                    bufferedWriter.write("CLEAN " + next.f2772d + next.a() + 10);
                }
            }
            bufferedWriter.close();
            if (this.f2758c.exists()) {
                a(this.f2758c, this.f2760e, true);
            }
            a(this.f2759d, this.f2758c, false);
            this.f2760e.delete();
            this.j = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f2758c, true), c.f2788a));
        } catch (Throwable th) {
            bufferedWriter.close();
            throw th;
        }
    }

    private static void a(File file) {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    private static void a(File file, File file2, boolean z) {
        if (z) {
            a(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    public synchronized c a(String str) {
        c cVar = null;
        synchronized (this) {
            f();
            b bVar = this.k.get(str);
            if (bVar != null) {
                if (bVar.f2774f) {
                    File[] fileArr = bVar.f2769a;
                    int length = fileArr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 < length) {
                            if (!fileArr[i2].exists()) {
                                break;
                            }
                            i2++;
                        } else {
                            this.l++;
                            this.j.append((CharSequence) "READ");
                            this.j.append(' ');
                            this.j.append((CharSequence) str);
                            this.j.append(10);
                            if (e()) {
                                this.f2756a.submit(this.n);
                            }
                            cVar = new c(str, bVar.f2776h, bVar.f2769a, bVar.f2773e);
                        }
                    }
                }
            }
        }
        return cVar;
    }

    public C0035a b(String str) {
        return a(str, -1);
    }

    private synchronized C0035a a(String str, long j2) {
        b bVar;
        C0035a aVar;
        f();
        b bVar2 = this.k.get(str);
        if (j2 == -1 || (bVar2 != null && bVar2.f2776h == j2)) {
            if (bVar2 == null) {
                b bVar3 = new b(str);
                this.k.put(str, bVar3);
                bVar = bVar3;
            } else if (bVar2.f2775g != null) {
                aVar = null;
            } else {
                bVar = bVar2;
            }
            aVar = new C0035a(bVar);
            C0035a unused = bVar.f2775g = aVar;
            this.j.append((CharSequence) "DIRTY");
            this.j.append(' ');
            this.j.append((CharSequence) str);
            this.j.append(10);
            this.j.flush();
        } else {
            aVar = null;
        }
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.a.a.b.a(com.bumptech.glide.a.a$b, boolean):boolean
     arg types: [com.bumptech.glide.a.a$b, int]
     candidates:
      com.bumptech.glide.a.a.b.a(com.bumptech.glide.a.a$b, long):long
      com.bumptech.glide.a.a.b.a(com.bumptech.glide.a.a$b, com.bumptech.glide.a.a$a):com.bumptech.glide.a.a$a
      com.bumptech.glide.a.a.b.a(com.bumptech.glide.a.a$b, java.lang.String[]):void
      com.bumptech.glide.a.a.b.a(com.bumptech.glide.a.a$b, boolean):boolean */
    /* access modifiers changed from: private */
    public synchronized void a(C0035a aVar, boolean z) {
        synchronized (this) {
            b a2 = aVar.f2766b;
            if (a2.f2775g != aVar) {
                throw new IllegalStateException();
            }
            if (z) {
                if (!a2.f2774f) {
                    int i2 = 0;
                    while (true) {
                        if (i2 < this.f2763h) {
                            if (!aVar.f2767c[i2]) {
                                aVar.b();
                                throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                            } else if (!a2.b(i2).exists()) {
                                aVar.b();
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
            }
            for (int i3 = 0; i3 < this.f2763h; i3++) {
                File b2 = a2.b(i3);
                if (!z) {
                    a(b2);
                } else if (b2.exists()) {
                    File a3 = a2.a(i3);
                    b2.renameTo(a3);
                    long j2 = a2.f2773e[i3];
                    long length = a3.length();
                    a2.f2773e[i3] = length;
                    this.i = (this.i - j2) + length;
                }
            }
            this.l++;
            C0035a unused = a2.f2775g = (C0035a) null;
            if (a2.f2774f || z) {
                boolean unused2 = a2.f2774f = true;
                this.j.append((CharSequence) "CLEAN");
                this.j.append(' ');
                this.j.append((CharSequence) a2.f2772d);
                this.j.append((CharSequence) a2.a());
                this.j.append(10);
                if (z) {
                    long j3 = this.m;
                    this.m = 1 + j3;
                    long unused3 = a2.f2776h = j3;
                }
            } else {
                this.k.remove(a2.f2772d);
                this.j.append((CharSequence) "REMOVE");
                this.j.append(' ');
                this.j.append((CharSequence) a2.f2772d);
                this.j.append(10);
            }
            this.j.flush();
            if (this.i > this.f2762g || e()) {
                this.f2756a.submit(this.n);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean e() {
        return this.l >= 2000 && this.l >= this.k.size();
    }

    public synchronized boolean c(String str) {
        boolean z;
        int i2 = 0;
        synchronized (this) {
            f();
            b bVar = this.k.get(str);
            if (bVar == null || bVar.f2775g != null) {
                z = false;
            } else {
                while (i2 < this.f2763h) {
                    File a2 = bVar.a(i2);
                    if (!a2.exists() || a2.delete()) {
                        this.i -= bVar.f2773e[i2];
                        bVar.f2773e[i2] = 0;
                        i2++;
                    } else {
                        throw new IOException("failed to delete " + a2);
                    }
                }
                this.l++;
                this.j.append((CharSequence) "REMOVE");
                this.j.append(' ');
                this.j.append((CharSequence) str);
                this.j.append(10);
                this.k.remove(str);
                if (e()) {
                    this.f2756a.submit(this.n);
                }
                z = true;
            }
        }
        return z;
    }

    private void f() {
        if (this.j == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    public synchronized void close() {
        if (this.j != null) {
            Iterator it = new ArrayList(this.k.values()).iterator();
            while (it.hasNext()) {
                b bVar = (b) it.next();
                if (bVar.f2775g != null) {
                    bVar.f2775g.b();
                }
            }
            g();
            this.j.close();
            this.j = null;
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        while (this.i > this.f2762g) {
            c((String) this.k.entrySet().iterator().next().getKey());
        }
    }

    public void a() {
        close();
        c.a(this.f2757b);
    }

    /* compiled from: DiskLruCache */
    public final class c {

        /* renamed from: b  reason: collision with root package name */
        private final String f2778b;

        /* renamed from: c  reason: collision with root package name */
        private final long f2779c;

        /* renamed from: d  reason: collision with root package name */
        private final long[] f2780d;

        /* renamed from: e  reason: collision with root package name */
        private final File[] f2781e;

        private c(String str, long j, File[] fileArr, long[] jArr) {
            this.f2778b = str;
            this.f2779c = j;
            this.f2781e = fileArr;
            this.f2780d = jArr;
        }

        public File a(int i) {
            return this.f2781e[i];
        }
    }

    /* renamed from: com.bumptech.glide.a.a$a  reason: collision with other inner class name */
    /* compiled from: DiskLruCache */
    public final class C0035a {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final b f2766b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public final boolean[] f2767c;

        /* renamed from: d  reason: collision with root package name */
        private boolean f2768d;

        private C0035a(b bVar) {
            this.f2766b = bVar;
            this.f2767c = bVar.f2774f ? null : new boolean[a.this.f2763h];
        }

        public File a(int i) {
            File b2;
            synchronized (a.this) {
                if (this.f2766b.f2775g != this) {
                    throw new IllegalStateException();
                }
                if (!this.f2766b.f2774f) {
                    this.f2767c[i] = true;
                }
                b2 = this.f2766b.b(i);
                if (!a.this.f2757b.exists()) {
                    a.this.f2757b.mkdirs();
                }
            }
            return b2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.bumptech.glide.a.a.a(com.bumptech.glide.a.a, com.bumptech.glide.a.a$a, boolean):void
         arg types: [com.bumptech.glide.a.a, com.bumptech.glide.a.a$a, int]
         candidates:
          com.bumptech.glide.a.a.a(java.io.File, java.io.File, boolean):void
          com.bumptech.glide.a.a.a(com.bumptech.glide.a.a, com.bumptech.glide.a.a$a, boolean):void */
        public void a() {
            a.this.a(this, true);
            this.f2768d = true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.bumptech.glide.a.a.a(com.bumptech.glide.a.a, com.bumptech.glide.a.a$a, boolean):void
         arg types: [com.bumptech.glide.a.a, com.bumptech.glide.a.a$a, int]
         candidates:
          com.bumptech.glide.a.a.a(java.io.File, java.io.File, boolean):void
          com.bumptech.glide.a.a.a(com.bumptech.glide.a.a, com.bumptech.glide.a.a$a, boolean):void */
        public void b() {
            a.this.a(this, false);
        }

        public void c() {
            if (!this.f2768d) {
                try {
                    b();
                } catch (IOException e2) {
                }
            }
        }
    }

    /* compiled from: DiskLruCache */
    private final class b {

        /* renamed from: a  reason: collision with root package name */
        File[] f2769a;

        /* renamed from: b  reason: collision with root package name */
        File[] f2770b;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public final String f2772d;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public final long[] f2773e;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public boolean f2774f;
        /* access modifiers changed from: private */

        /* renamed from: g  reason: collision with root package name */
        public C0035a f2775g;
        /* access modifiers changed from: private */

        /* renamed from: h  reason: collision with root package name */
        public long f2776h;

        private b(String str) {
            this.f2772d = str;
            this.f2773e = new long[a.this.f2763h];
            this.f2769a = new File[a.this.f2763h];
            this.f2770b = new File[a.this.f2763h];
            StringBuilder append = new StringBuilder(str).append('.');
            int length = append.length();
            for (int i = 0; i < a.this.f2763h; i++) {
                append.append(i);
                this.f2769a[i] = new File(a.this.f2757b, append.toString());
                append.append(".tmp");
                this.f2770b[i] = new File(a.this.f2757b, append.toString());
                append.setLength(length);
            }
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            for (long append : this.f2773e) {
                sb.append(' ').append(append);
            }
            return sb.toString();
        }

        /* access modifiers changed from: private */
        public void a(String[] strArr) {
            if (strArr.length != a.this.f2763h) {
                throw b(strArr);
            }
            int i = 0;
            while (i < strArr.length) {
                try {
                    this.f2773e[i] = Long.parseLong(strArr[i]);
                    i++;
                } catch (NumberFormatException e2) {
                    throw b(strArr);
                }
            }
        }

        private IOException b(String[] strArr) {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        public File a(int i) {
            return this.f2769a[i];
        }

        public File b(int i) {
            return this.f2770b[i];
        }
    }
}
