package com.bumptech.glide.a;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/* compiled from: StrictLineReader */
class b implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    private final InputStream f2782a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final Charset f2783b;

    /* renamed from: c  reason: collision with root package name */
    private byte[] f2784c;

    /* renamed from: d  reason: collision with root package name */
    private int f2785d;

    /* renamed from: e  reason: collision with root package name */
    private int f2786e;

    public b(InputStream inputStream, Charset charset) {
        this(inputStream, 8192, charset);
    }

    public b(InputStream inputStream, int i, Charset charset) {
        if (inputStream == null || charset == null) {
            throw new NullPointerException();
        } else if (i < 0) {
            throw new IllegalArgumentException("capacity <= 0");
        } else if (!charset.equals(c.f2788a)) {
            throw new IllegalArgumentException("Unsupported encoding");
        } else {
            this.f2782a = inputStream;
            this.f2783b = charset;
            this.f2784c = new byte[i];
        }
    }

    public void close() {
        synchronized (this.f2782a) {
            if (this.f2784c != null) {
                this.f2784c = null;
                this.f2782a.close();
            }
        }
    }

    public String a() {
        int i;
        String byteArrayOutputStream;
        synchronized (this.f2782a) {
            if (this.f2784c == null) {
                throw new IOException("LineReader is closed");
            }
            if (this.f2785d >= this.f2786e) {
                c();
            }
            int i2 = this.f2785d;
            while (true) {
                if (i2 == this.f2786e) {
                    AnonymousClass1 r1 = new ByteArrayOutputStream((this.f2786e - this.f2785d) + 80) {
                        public String toString() {
                            try {
                                return new String(this.buf, 0, (this.count <= 0 || this.buf[this.count + -1] != 13) ? this.count : this.count - 1, b.this.f2783b.name());
                            } catch (UnsupportedEncodingException e2) {
                                throw new AssertionError(e2);
                            }
                        }
                    };
                    loop1:
                    while (true) {
                        r1.write(this.f2784c, this.f2785d, this.f2786e - this.f2785d);
                        this.f2786e = -1;
                        c();
                        i = this.f2785d;
                        while (true) {
                            if (i != this.f2786e) {
                                if (this.f2784c[i] == 10) {
                                    break loop1;
                                }
                                i++;
                            }
                        }
                    }
                    if (i != this.f2785d) {
                        r1.write(this.f2784c, this.f2785d, i - this.f2785d);
                    }
                    this.f2785d = i + 1;
                    byteArrayOutputStream = r1.toString();
                } else if (this.f2784c[i2] == 10) {
                    byteArrayOutputStream = new String(this.f2784c, this.f2785d, ((i2 == this.f2785d || this.f2784c[i2 + -1] != 13) ? i2 : i2 - 1) - this.f2785d, this.f2783b.name());
                    this.f2785d = i2 + 1;
                } else {
                    i2++;
                }
            }
        }
        return byteArrayOutputStream;
    }

    public boolean b() {
        return this.f2786e == -1;
    }

    private void c() {
        int read = this.f2782a.read(this.f2784c, 0, this.f2784c.length);
        if (read == -1) {
            throw new EOFException();
        }
        this.f2785d = 0;
        this.f2786e = read;
    }
}
