package com.bumptech.glide.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.bumptech.glide.manager.c;

/* compiled from: DefaultConnectivityMonitor */
class e implements c {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3277a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final c.a f3278b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public boolean f3279c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f3280d;

    /* renamed from: e  reason: collision with root package name */
    private final BroadcastReceiver f3281e = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            boolean a2 = e.this.f3279c;
            boolean unused = e.this.f3279c = e.this.a(context);
            if (a2 != e.this.f3279c) {
                e.this.f3278b.a(e.this.f3279c);
            }
        }
    };

    public e(Context context, c.a aVar) {
        this.f3277a = context.getApplicationContext();
        this.f3278b = aVar;
    }

    private void a() {
        if (!this.f3280d) {
            this.f3279c = a(this.f3277a);
            this.f3277a.registerReceiver(this.f3281e, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.f3280d = true;
        }
    }

    private void b() {
        if (this.f3280d) {
            this.f3277a.unregisterReceiver(this.f3281e);
            this.f3280d = false;
        }
    }

    /* access modifiers changed from: private */
    public boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void d() {
        a();
    }

    public void e() {
        b();
    }

    public void f() {
    }
}
