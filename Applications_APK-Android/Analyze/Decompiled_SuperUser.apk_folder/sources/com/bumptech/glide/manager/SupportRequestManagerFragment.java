package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v4.app.Fragment;
import com.bumptech.glide.f;
import java.util.HashSet;

public class SupportRequestManagerFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private f f3268a;

    /* renamed from: b  reason: collision with root package name */
    private final a f3269b;

    /* renamed from: c  reason: collision with root package name */
    private final l f3270c;

    /* renamed from: d  reason: collision with root package name */
    private final HashSet<SupportRequestManagerFragment> f3271d;

    /* renamed from: e  reason: collision with root package name */
    private SupportRequestManagerFragment f3272e;

    public SupportRequestManagerFragment() {
        this(new a());
    }

    @SuppressLint({"ValidFragment"})
    public SupportRequestManagerFragment(a aVar) {
        this.f3270c = new a();
        this.f3271d = new HashSet<>();
        this.f3269b = aVar;
    }

    public void a(f fVar) {
        this.f3268a = fVar;
    }

    /* access modifiers changed from: package-private */
    public a a() {
        return this.f3269b;
    }

    public f b() {
        return this.f3268a;
    }

    public l c() {
        return this.f3270c;
    }

    private void a(SupportRequestManagerFragment supportRequestManagerFragment) {
        this.f3271d.add(supportRequestManagerFragment);
    }

    private void b(SupportRequestManagerFragment supportRequestManagerFragment) {
        this.f3271d.remove(supportRequestManagerFragment);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.f3272e = k.a().a(getActivity().e());
        if (this.f3272e != this) {
            this.f3272e.a(this);
        }
    }

    public void onDetach() {
        super.onDetach();
        if (this.f3272e != null) {
            this.f3272e.b(this);
            this.f3272e = null;
        }
    }

    public void onStart() {
        super.onStart();
        this.f3269b.a();
    }

    public void onStop() {
        super.onStop();
        this.f3269b.b();
    }

    public void onDestroy() {
        super.onDestroy();
        this.f3269b.c();
    }

    public void onLowMemory() {
        super.onLowMemory();
        if (this.f3268a != null) {
            this.f3268a.a();
        }
    }

    private class a implements l {
        private a() {
        }
    }
}
