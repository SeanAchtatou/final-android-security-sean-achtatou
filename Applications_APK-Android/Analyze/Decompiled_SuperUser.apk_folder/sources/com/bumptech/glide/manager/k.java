package com.bumptech.glide.manager;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.q;
import android.util.Log;
import com.bumptech.glide.f;
import com.bumptech.glide.f.h;
import java.util.HashMap;
import java.util.Map;

/* compiled from: RequestManagerRetriever */
public class k implements Handler.Callback {

    /* renamed from: c  reason: collision with root package name */
    private static final k f3289c = new k();

    /* renamed from: a  reason: collision with root package name */
    final Map<FragmentManager, j> f3290a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    final Map<q, SupportRequestManagerFragment> f3291b = new HashMap();

    /* renamed from: d  reason: collision with root package name */
    private volatile f f3292d;

    /* renamed from: e  reason: collision with root package name */
    private final Handler f3293e = new Handler(Looper.getMainLooper(), this);

    public static k a() {
        return f3289c;
    }

    k() {
    }

    private f b(Context context) {
        if (this.f3292d == null) {
            synchronized (this) {
                if (this.f3292d == null) {
                    this.f3292d = new f(context.getApplicationContext(), new b(), new f());
                }
            }
        }
        return this.f3292d;
    }

    public f a(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("You cannot start a load on a null Context");
        }
        if (h.b() && !(context instanceof Application)) {
            if (context instanceof FragmentActivity) {
                return a((FragmentActivity) context);
            }
            if (context instanceof Activity) {
                return a((Activity) context);
            }
            if (context instanceof ContextWrapper) {
                return a(((ContextWrapper) context).getBaseContext());
            }
        }
        return b(context);
    }

    public f a(FragmentActivity fragmentActivity) {
        if (h.c()) {
            return a(fragmentActivity.getApplicationContext());
        }
        b((Activity) fragmentActivity);
        return a(fragmentActivity, fragmentActivity.e());
    }

    @TargetApi(11)
    public f a(Activity activity) {
        if (h.c() || Build.VERSION.SDK_INT < 11) {
            return a(activity.getApplicationContext());
        }
        b(activity);
        return a(activity, activity.getFragmentManager());
    }

    @TargetApi(17)
    private static void b(Activity activity) {
        if (Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    /* access modifiers changed from: package-private */
    @TargetApi(17)
    public j a(FragmentManager fragmentManager) {
        j jVar = (j) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (jVar != null) {
            return jVar;
        }
        j jVar2 = this.f3290a.get(fragmentManager);
        if (jVar2 != null) {
            return jVar2;
        }
        j jVar3 = new j();
        this.f3290a.put(fragmentManager, jVar3);
        fragmentManager.beginTransaction().add(jVar3, "com.bumptech.glide.manager").commitAllowingStateLoss();
        this.f3293e.obtainMessage(1, fragmentManager).sendToTarget();
        return jVar3;
    }

    /* access modifiers changed from: package-private */
    @TargetApi(11)
    public f a(Context context, FragmentManager fragmentManager) {
        j a2 = a(fragmentManager);
        f b2 = a2.b();
        if (b2 != null) {
            return b2;
        }
        f fVar = new f(context, a2.a(), a2.c());
        a2.a(fVar);
        return fVar;
    }

    /* access modifiers changed from: package-private */
    public SupportRequestManagerFragment a(q qVar) {
        SupportRequestManagerFragment supportRequestManagerFragment = (SupportRequestManagerFragment) qVar.a("com.bumptech.glide.manager");
        if (supportRequestManagerFragment != null) {
            return supportRequestManagerFragment;
        }
        SupportRequestManagerFragment supportRequestManagerFragment2 = this.f3291b.get(qVar);
        if (supportRequestManagerFragment2 != null) {
            return supportRequestManagerFragment2;
        }
        SupportRequestManagerFragment supportRequestManagerFragment3 = new SupportRequestManagerFragment();
        this.f3291b.put(qVar, supportRequestManagerFragment3);
        qVar.a().a(supportRequestManagerFragment3, "com.bumptech.glide.manager").c();
        this.f3293e.obtainMessage(2, qVar).sendToTarget();
        return supportRequestManagerFragment3;
    }

    /* access modifiers changed from: package-private */
    public f a(Context context, q qVar) {
        SupportRequestManagerFragment a2 = a(qVar);
        f b2 = a2.b();
        if (b2 != null) {
            return b2;
        }
        f fVar = new f(context, a2.a(), a2.c());
        a2.a(fVar);
        return fVar;
    }

    public boolean handleMessage(Message message) {
        Object remove;
        Object obj = null;
        boolean z = true;
        switch (message.what) {
            case 1:
                obj = (FragmentManager) message.obj;
                remove = this.f3290a.remove(obj);
                break;
            case 2:
                obj = (q) message.obj;
                remove = this.f3291b.remove(obj);
                break;
            default:
                z = false;
                remove = null;
                break;
        }
        if (z && remove == null && Log.isLoggable("RMRetriever", 5)) {
            Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj);
        }
        return z;
    }
}
