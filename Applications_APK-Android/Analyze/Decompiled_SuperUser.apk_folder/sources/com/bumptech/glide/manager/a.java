package com.bumptech.glide.manager;

import com.bumptech.glide.f.h;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/* compiled from: ActivityFragmentLifecycle */
class a implements g {

    /* renamed from: a  reason: collision with root package name */
    private final Set<h> f3274a = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: b  reason: collision with root package name */
    private boolean f3275b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f3276c;

    a() {
    }

    public void a(h hVar) {
        this.f3274a.add(hVar);
        if (this.f3276c) {
            hVar.f();
        } else if (this.f3275b) {
            hVar.d();
        } else {
            hVar.e();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f3275b = true;
        for (h d2 : h.a(this.f3274a)) {
            d2.d();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f3275b = false;
        for (h e2 : h.a(this.f3274a)) {
            e2.e();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.f3276c = true;
        for (h f2 : h.a(this.f3274a)) {
            f2.f();
        }
    }
}
