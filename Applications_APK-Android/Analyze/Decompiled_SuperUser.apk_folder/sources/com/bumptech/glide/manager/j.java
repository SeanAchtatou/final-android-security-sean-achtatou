package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import com.bumptech.glide.f;
import java.util.HashSet;

@TargetApi(11)
/* compiled from: RequestManagerFragment */
public class j extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private final a f3283a;

    /* renamed from: b  reason: collision with root package name */
    private final l f3284b;

    /* renamed from: c  reason: collision with root package name */
    private f f3285c;

    /* renamed from: d  reason: collision with root package name */
    private final HashSet<j> f3286d;

    /* renamed from: e  reason: collision with root package name */
    private j f3287e;

    public j() {
        this(new a());
    }

    @SuppressLint({"ValidFragment"})
    j(a aVar) {
        this.f3284b = new a();
        this.f3286d = new HashSet<>();
        this.f3283a = aVar;
    }

    public void a(f fVar) {
        this.f3285c = fVar;
    }

    /* access modifiers changed from: package-private */
    public a a() {
        return this.f3283a;
    }

    public f b() {
        return this.f3285c;
    }

    public l c() {
        return this.f3284b;
    }

    private void a(j jVar) {
        this.f3286d.add(jVar);
    }

    private void b(j jVar) {
        this.f3286d.remove(jVar);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.f3287e = k.a().a(getActivity().getFragmentManager());
        if (this.f3287e != this) {
            this.f3287e.a(this);
        }
    }

    public void onDetach() {
        super.onDetach();
        if (this.f3287e != null) {
            this.f3287e.b(this);
            this.f3287e = null;
        }
    }

    public void onStart() {
        super.onStart();
        this.f3283a.a();
    }

    public void onStop() {
        super.onStop();
        this.f3283a.b();
    }

    public void onDestroy() {
        super.onDestroy();
        this.f3283a.c();
    }

    public void onTrimMemory(int i) {
        if (this.f3285c != null) {
            this.f3285c.a(i);
        }
    }

    public void onLowMemory() {
        if (this.f3285c != null) {
            this.f3285c.a();
        }
    }

    /* compiled from: RequestManagerFragment */
    private class a implements l {
        private a() {
        }
    }
}
