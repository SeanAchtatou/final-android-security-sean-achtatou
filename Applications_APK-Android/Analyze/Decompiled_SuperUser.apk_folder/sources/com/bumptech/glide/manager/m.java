package com.bumptech.glide.manager;

import com.bumptech.glide.f.h;
import com.bumptech.glide.request.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

/* compiled from: RequestTracker */
public class m {

    /* renamed from: a  reason: collision with root package name */
    private final Set<a> f3294a = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: b  reason: collision with root package name */
    private final List<a> f3295b = new ArrayList();

    /* renamed from: c  reason: collision with root package name */
    private boolean f3296c;

    public void a(a aVar) {
        this.f3294a.add(aVar);
        if (!this.f3296c) {
            aVar.b();
        } else {
            this.f3295b.add(aVar);
        }
    }

    public void b(a aVar) {
        this.f3294a.remove(aVar);
        this.f3295b.remove(aVar);
    }

    public void a() {
        this.f3296c = true;
        for (a aVar : h.a(this.f3294a)) {
            if (aVar.f()) {
                aVar.e();
                this.f3295b.add(aVar);
            }
        }
    }

    public void b() {
        this.f3296c = false;
        for (a aVar : h.a(this.f3294a)) {
            if (!aVar.g() && !aVar.i() && !aVar.f()) {
                aVar.b();
            }
        }
        this.f3295b.clear();
    }

    public void c() {
        for (a d2 : h.a(this.f3294a)) {
            d2.d();
        }
        this.f3295b.clear();
    }

    public void d() {
        for (a aVar : h.a(this.f3294a)) {
            if (!aVar.g() && !aVar.i()) {
                aVar.e();
                if (!this.f3296c) {
                    aVar.b();
                } else {
                    this.f3295b.add(aVar);
                }
            }
        }
    }
}
