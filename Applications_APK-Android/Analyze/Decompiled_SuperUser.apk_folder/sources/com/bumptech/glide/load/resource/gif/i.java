package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.b.a;
import com.bumptech.glide.f.d;
import com.bumptech.glide.load.e;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.f;
import java.io.IOException;
import java.io.OutputStream;

/* compiled from: GifResourceEncoder */
public class i implements e<b> {

    /* renamed from: a  reason: collision with root package name */
    private static final a f3257a = new a();

    /* renamed from: b  reason: collision with root package name */
    private final a.C0036a f3258b;

    /* renamed from: c  reason: collision with root package name */
    private final c f3259c;

    /* renamed from: d  reason: collision with root package name */
    private final a f3260d;

    public /* bridge */ /* synthetic */ boolean a(Object obj, OutputStream outputStream) {
        return a((com.bumptech.glide.load.engine.i<b>) ((com.bumptech.glide.load.engine.i) obj), outputStream);
    }

    public i(c cVar) {
        this(cVar, f3257a);
    }

    i(c cVar, a aVar) {
        this.f3259c = cVar;
        this.f3258b = new a(cVar);
        this.f3260d = aVar;
    }

    /* JADX INFO: finally extract failed */
    public boolean a(com.bumptech.glide.load.engine.i<b> iVar, OutputStream outputStream) {
        long a2 = d.a();
        b b2 = iVar.b();
        f<Bitmap> c2 = b2.c();
        if (c2 instanceof com.bumptech.glide.load.resource.d) {
            return a(b2.d(), outputStream);
        }
        com.bumptech.glide.b.a a3 = a(b2.d());
        com.bumptech.glide.c.a b3 = this.f3260d.b();
        if (!b3.a(outputStream)) {
            return false;
        }
        int i = 0;
        while (i < a3.c()) {
            com.bumptech.glide.load.engine.i<Bitmap> a4 = a(a3.f(), c2, b2);
            try {
                if (!b3.a(a4.b())) {
                    a4.d();
                    return false;
                }
                b3.a(a3.a(a3.d()));
                a3.a();
                a4.d();
                i++;
            } catch (Throwable th) {
                a4.d();
                throw th;
            }
        }
        boolean a5 = b3.a();
        if (!Log.isLoggable("GifEncoder", 2)) {
            return a5;
        }
        Log.v("GifEncoder", "Encoded gif with " + a3.c() + " frames and " + b2.d().length + " bytes in " + d.a(a2) + " ms");
        return a5;
    }

    private boolean a(byte[] bArr, OutputStream outputStream) {
        try {
            outputStream.write(bArr);
            return true;
        } catch (IOException e2) {
            if (Log.isLoggable("GifEncoder", 3)) {
                Log.d("GifEncoder", "Failed to write data to output stream in GifResourceEncoder", e2);
            }
            return false;
        }
    }

    private com.bumptech.glide.b.a a(byte[] bArr) {
        com.bumptech.glide.b.d a2 = this.f3260d.a();
        a2.a(bArr);
        com.bumptech.glide.b.c b2 = a2.b();
        com.bumptech.glide.b.a a3 = this.f3260d.a(this.f3258b);
        a3.a(b2, bArr);
        a3.a();
        return a3;
    }

    private com.bumptech.glide.load.engine.i<Bitmap> a(Bitmap bitmap, f<Bitmap> fVar, b bVar) {
        com.bumptech.glide.load.engine.i<Bitmap> a2 = this.f3260d.a(bitmap, this.f3259c);
        com.bumptech.glide.load.engine.i<Bitmap> a3 = fVar.a(a2, bVar.getIntrinsicWidth(), bVar.getIntrinsicHeight());
        if (!a2.equals(a3)) {
            a2.d();
        }
        return a3;
    }

    public String a() {
        return "";
    }

    /* compiled from: GifResourceEncoder */
    static class a {
        a() {
        }

        public com.bumptech.glide.b.a a(a.C0036a aVar) {
            return new com.bumptech.glide.b.a(aVar);
        }

        public com.bumptech.glide.b.d a() {
            return new com.bumptech.glide.b.d();
        }

        public com.bumptech.glide.c.a b() {
            return new com.bumptech.glide.c.a();
        }

        public com.bumptech.glide.load.engine.i<Bitmap> a(Bitmap bitmap, c cVar) {
            return new com.bumptech.glide.load.resource.bitmap.c(bitmap, cVar);
        }
    }
}
