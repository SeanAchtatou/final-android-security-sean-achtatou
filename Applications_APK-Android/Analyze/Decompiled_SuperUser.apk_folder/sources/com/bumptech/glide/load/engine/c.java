package com.bumptech.glide.load.engine;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.bumptech.glide.f.h;
import com.bumptech.glide.load.engine.EngineRunnable;
import com.bumptech.glide.request.d;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/* compiled from: EngineJob */
class c implements EngineRunnable.a {

    /* renamed from: a  reason: collision with root package name */
    private static final a f3014a = new a();

    /* renamed from: b  reason: collision with root package name */
    private static final Handler f3015b = new Handler(Looper.getMainLooper(), new b());

    /* renamed from: c  reason: collision with root package name */
    private final List<d> f3016c;

    /* renamed from: d  reason: collision with root package name */
    private final a f3017d;

    /* renamed from: e  reason: collision with root package name */
    private final d f3018e;

    /* renamed from: f  reason: collision with root package name */
    private final com.bumptech.glide.load.b f3019f;

    /* renamed from: g  reason: collision with root package name */
    private final ExecutorService f3020g;

    /* renamed from: h  reason: collision with root package name */
    private final ExecutorService f3021h;
    private final boolean i;
    private boolean j;
    private i<?> k;
    private boolean l;
    private Exception m;
    private boolean n;
    private Set<d> o;
    private EngineRunnable p;
    private g<?> q;
    private volatile Future<?> r;

    public c(com.bumptech.glide.load.b bVar, ExecutorService executorService, ExecutorService executorService2, boolean z, d dVar) {
        this(bVar, executorService, executorService2, z, dVar, f3014a);
    }

    public c(com.bumptech.glide.load.b bVar, ExecutorService executorService, ExecutorService executorService2, boolean z, d dVar, a aVar) {
        this.f3016c = new ArrayList();
        this.f3019f = bVar;
        this.f3020g = executorService;
        this.f3021h = executorService2;
        this.i = z;
        this.f3018e = dVar;
        this.f3017d = aVar;
    }

    public void a(EngineRunnable engineRunnable) {
        this.p = engineRunnable;
        this.r = this.f3020g.submit(engineRunnable);
    }

    public void b(EngineRunnable engineRunnable) {
        this.r = this.f3021h.submit(engineRunnable);
    }

    public void a(d dVar) {
        h.a();
        if (this.l) {
            dVar.a(this.q);
        } else if (this.n) {
            dVar.a(this.m);
        } else {
            this.f3016c.add(dVar);
        }
    }

    public void b(d dVar) {
        h.a();
        if (this.l || this.n) {
            c(dVar);
            return;
        }
        this.f3016c.remove(dVar);
        if (this.f3016c.isEmpty()) {
            a();
        }
    }

    private void c(d dVar) {
        if (this.o == null) {
            this.o = new HashSet();
        }
        this.o.add(dVar);
    }

    private boolean d(d dVar) {
        return this.o != null && this.o.contains(dVar);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (!this.n && !this.l && !this.j) {
            this.p.a();
            Future<?> future = this.r;
            if (future != null) {
                future.cancel(true);
            }
            this.j = true;
            this.f3018e.a(this, this.f3019f);
        }
    }

    public void a(i<?> iVar) {
        this.k = iVar;
        f3015b.obtainMessage(1, this).sendToTarget();
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.j) {
            this.k.d();
        } else if (this.f3016c.isEmpty()) {
            throw new IllegalStateException("Received a resource without any callbacks to notify");
        } else {
            this.q = this.f3017d.a(this.k, this.i);
            this.l = true;
            this.q.e();
            this.f3018e.a(this.f3019f, this.q);
            for (d next : this.f3016c) {
                if (!d(next)) {
                    this.q.e();
                    next.a(this.q);
                }
            }
            this.q.f();
        }
    }

    public void a(Exception exc) {
        this.m = exc;
        f3015b.obtainMessage(2, this).sendToTarget();
    }

    /* access modifiers changed from: private */
    public void c() {
        if (!this.j) {
            if (this.f3016c.isEmpty()) {
                throw new IllegalStateException("Received an exception without any callbacks to notify");
            }
            this.n = true;
            this.f3018e.a(this.f3019f, (g<?>) null);
            for (d next : this.f3016c) {
                if (!d(next)) {
                    next.a(this.m);
                }
            }
        }
    }

    /* compiled from: EngineJob */
    static class a {
        a() {
        }

        public <R> g<R> a(i<R> iVar, boolean z) {
            return new g<>(iVar, z);
        }
    }

    /* compiled from: EngineJob */
    private static class b implements Handler.Callback {
        private b() {
        }

        public boolean handleMessage(Message message) {
            if (1 != message.what && 2 != message.what) {
                return false;
            }
            c cVar = (c) message.obj;
            if (1 == message.what) {
                cVar.b();
            } else {
                cVar.c();
            }
            return true;
        }
    }
}
