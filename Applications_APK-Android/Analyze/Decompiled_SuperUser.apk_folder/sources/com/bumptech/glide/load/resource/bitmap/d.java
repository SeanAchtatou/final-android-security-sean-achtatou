package com.bumptech.glide.load.resource.bitmap;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import com.bumptech.glide.f.a;
import com.bumptech.glide.f.f;
import com.bumptech.glide.f.h;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.resource.bitmap.ImageHeaderParser;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.EnumSet;
import java.util.Queue;
import java.util.Set;

/* compiled from: Downsampler */
public abstract class d implements a<InputStream> {

    /* renamed from: a  reason: collision with root package name */
    public static final d f3153a = new d() {
        /* access modifiers changed from: protected */
        public int a(int i, int i2, int i3, int i4) {
            return Math.min(i2 / i4, i / i3);
        }

        public String a() {
            return "AT_LEAST.com.bumptech.glide.load.data.bitmap";
        }
    };

    /* renamed from: b  reason: collision with root package name */
    public static final d f3154b = new d() {
        /* access modifiers changed from: protected */
        public int a(int i, int i2, int i3, int i4) {
            int i5 = 1;
            int ceil = (int) Math.ceil((double) Math.max(((float) i2) / ((float) i4), ((float) i) / ((float) i3)));
            int max = Math.max(1, Integer.highestOneBit(ceil));
            if (max >= ceil) {
                i5 = 0;
            }
            return max << i5;
        }

        public String a() {
            return "AT_MOST.com.bumptech.glide.load.data.bitmap";
        }
    };

    /* renamed from: c  reason: collision with root package name */
    public static final d f3155c = new d() {
        /* access modifiers changed from: protected */
        public int a(int i, int i2, int i3, int i4) {
            return 0;
        }

        public String a() {
            return "NONE.com.bumptech.glide.load.data.bitmap";
        }
    };

    /* renamed from: d  reason: collision with root package name */
    private static final Set<ImageHeaderParser.ImageType> f3156d = EnumSet.of(ImageHeaderParser.ImageType.JPEG, ImageHeaderParser.ImageType.PNG_A, ImageHeaderParser.ImageType.PNG);

    /* renamed from: e  reason: collision with root package name */
    private static final Queue<BitmapFactory.Options> f3157e = h.a(0);

    /* access modifiers changed from: protected */
    public abstract int a(int i, int i2, int i3, int i4);

    public Bitmap a(InputStream inputStream, c cVar, int i, int i2, DecodeFormat decodeFormat) {
        int i3;
        int c2;
        a a2 = a.a();
        byte[] b2 = a2.b();
        byte[] b3 = a2.b();
        BitmapFactory.Options b4 = b();
        RecyclableBufferedInputStream recyclableBufferedInputStream = new RecyclableBufferedInputStream(inputStream, b3);
        com.bumptech.glide.f.c a3 = com.bumptech.glide.f.c.a(recyclableBufferedInputStream);
        f fVar = new f(a3);
        try {
            a3.mark(5242880);
            try {
                c2 = new ImageHeaderParser(a3).c();
                a3.reset();
                i3 = c2;
            } catch (IOException e2) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot determine the image orientation from header", e2);
                }
                try {
                    a3.reset();
                    i3 = 0;
                } catch (IOException e3) {
                    if (Log.isLoggable("Downsampler", 5)) {
                        Log.w("Downsampler", "Cannot reset the input stream", e3);
                    }
                    i3 = 0;
                }
            } catch (Throwable th) {
                try {
                    a3.reset();
                } catch (IOException e4) {
                    if (Log.isLoggable("Downsampler", 5)) {
                        Log.w("Downsampler", "Cannot reset the input stream", e4);
                    }
                }
                throw th;
            }
        } catch (IOException e5) {
            if (Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Cannot reset the input stream", e5);
            }
            i3 = c2;
        } catch (Throwable th2) {
            a2.a(b2);
            a2.a(b3);
            a3.b();
            a(b4);
            throw th2;
        }
        b4.inTempStorage = b2;
        int[] a4 = a(fVar, recyclableBufferedInputStream, b4);
        int i4 = a4[0];
        int i5 = a4[1];
        Bitmap a5 = a(fVar, recyclableBufferedInputStream, b4, cVar, i4, i5, a(k.a(i3), i4, i5, i, i2), decodeFormat);
        IOException a6 = a3.a();
        if (a6 != null) {
            throw new RuntimeException(a6);
        }
        Bitmap bitmap = null;
        if (a5 != null) {
            bitmap = k.a(a5, cVar, i3);
            if (!a5.equals(bitmap) && !cVar.a(a5)) {
                a5.recycle();
            }
        }
        a2.a(b2);
        a2.a(b3);
        a3.b();
        a(b4);
        return bitmap;
    }

    private int a(int i, int i2, int i3, int i4, int i5) {
        int i6;
        if (i5 == Integer.MIN_VALUE) {
            i5 = i3;
        }
        if (i4 == Integer.MIN_VALUE) {
            i4 = i2;
        }
        if (i == 90 || i == 270) {
            i6 = a(i3, i2, i4, i5);
        } else {
            i6 = a(i2, i3, i4, i5);
        }
        return Math.max(1, i6 == 0 ? 0 : Integer.highestOneBit(i6));
    }

    private Bitmap a(f fVar, RecyclableBufferedInputStream recyclableBufferedInputStream, BitmapFactory.Options options, c cVar, int i, int i2, int i3, DecodeFormat decodeFormat) {
        Bitmap.Config a2 = a(fVar, decodeFormat);
        options.inSampleSize = i3;
        options.inPreferredConfig = a2;
        if ((options.inSampleSize == 1 || 19 <= Build.VERSION.SDK_INT) && a(fVar)) {
            a(options, cVar.b((int) Math.ceil(((double) i) / ((double) i3)), (int) Math.ceil(((double) i2) / ((double) i3)), a2));
        }
        return b(fVar, recyclableBufferedInputStream, options);
    }

    private static boolean a(InputStream inputStream) {
        if (19 <= Build.VERSION.SDK_INT) {
            return true;
        }
        inputStream.mark(FileUtils.FileMode.MODE_ISGID);
        try {
            boolean contains = f3156d.contains(new ImageHeaderParser(inputStream).b());
            try {
                inputStream.reset();
                return contains;
            } catch (IOException e2) {
                if (!Log.isLoggable("Downsampler", 5)) {
                    return contains;
                }
                Log.w("Downsampler", "Cannot reset the input stream", e2);
                return contains;
            }
        } catch (IOException e3) {
            if (Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Cannot determine the image type from header", e3);
            }
            try {
                inputStream.reset();
            } catch (IOException e4) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot reset the input stream", e4);
                }
            }
            return false;
        } catch (Throwable th) {
            try {
                inputStream.reset();
            } catch (IOException e5) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot reset the input stream", e5);
                }
            }
            throw th;
        }
    }

    private static Bitmap.Config a(InputStream inputStream, DecodeFormat decodeFormat) {
        boolean z;
        if (decodeFormat == DecodeFormat.ALWAYS_ARGB_8888 || decodeFormat == DecodeFormat.PREFER_ARGB_8888 || Build.VERSION.SDK_INT == 16) {
            return Bitmap.Config.ARGB_8888;
        }
        inputStream.mark(FileUtils.FileMode.MODE_ISGID);
        try {
            z = new ImageHeaderParser(inputStream).a();
            try {
                inputStream.reset();
            } catch (IOException e2) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot reset the input stream", e2);
                }
            }
        } catch (IOException e3) {
            if (Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Cannot determine whether the image has alpha or not from header for format " + decodeFormat, e3);
            }
            try {
                inputStream.reset();
                z = false;
            } catch (IOException e4) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot reset the input stream", e4);
                }
                z = false;
            }
        } catch (Throwable th) {
            try {
                inputStream.reset();
            } catch (IOException e5) {
                if (Log.isLoggable("Downsampler", 5)) {
                    Log.w("Downsampler", "Cannot reset the input stream", e5);
                }
            }
            throw th;
        }
        if (z) {
            return Bitmap.Config.ARGB_8888;
        }
        return Bitmap.Config.RGB_565;
    }

    public int[] a(f fVar, RecyclableBufferedInputStream recyclableBufferedInputStream, BitmapFactory.Options options) {
        options.inJustDecodeBounds = true;
        b(fVar, recyclableBufferedInputStream, options);
        options.inJustDecodeBounds = false;
        return new int[]{options.outWidth, options.outHeight};
    }

    private static Bitmap b(f fVar, RecyclableBufferedInputStream recyclableBufferedInputStream, BitmapFactory.Options options) {
        if (options.inJustDecodeBounds) {
            fVar.mark(5242880);
        } else {
            recyclableBufferedInputStream.a();
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(fVar, null, options);
        try {
            if (options.inJustDecodeBounds) {
                fVar.reset();
            }
        } catch (IOException e2) {
            if (Log.isLoggable("Downsampler", 6)) {
                Log.e("Downsampler", "Exception loading inDecodeBounds=" + options.inJustDecodeBounds + " sample=" + options.inSampleSize, e2);
            }
        }
        return decodeStream;
    }

    @TargetApi(11)
    private static void a(BitmapFactory.Options options, Bitmap bitmap) {
        if (11 <= Build.VERSION.SDK_INT) {
            options.inBitmap = bitmap;
        }
    }

    @TargetApi(11)
    private static synchronized BitmapFactory.Options b() {
        BitmapFactory.Options poll;
        synchronized (d.class) {
            synchronized (f3157e) {
                poll = f3157e.poll();
            }
            if (poll == null) {
                poll = new BitmapFactory.Options();
                b(poll);
            }
        }
        return poll;
    }

    private static void a(BitmapFactory.Options options) {
        b(options);
        synchronized (f3157e) {
            f3157e.offer(options);
        }
    }

    @TargetApi(11)
    private static void b(BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        if (11 <= Build.VERSION.SDK_INT) {
            options.inBitmap = null;
            options.inMutable = true;
        }
    }
}
