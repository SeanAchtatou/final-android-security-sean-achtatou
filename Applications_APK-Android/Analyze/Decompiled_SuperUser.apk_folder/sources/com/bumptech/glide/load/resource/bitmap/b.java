package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.f.d;
import com.bumptech.glide.f.h;
import com.bumptech.glide.load.e;
import com.bumptech.glide.load.engine.i;
import java.io.OutputStream;

/* compiled from: BitmapEncoder */
public class b implements e<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap.CompressFormat f3149a;

    /* renamed from: b  reason: collision with root package name */
    private int f3150b;

    public /* bridge */ /* synthetic */ boolean a(Object obj, OutputStream outputStream) {
        return a((i<Bitmap>) ((i) obj), outputStream);
    }

    public b() {
        this(null, 90);
    }

    public b(Bitmap.CompressFormat compressFormat, int i) {
        this.f3149a = compressFormat;
        this.f3150b = i;
    }

    public boolean a(i<Bitmap> iVar, OutputStream outputStream) {
        Bitmap b2 = iVar.b();
        long a2 = d.a();
        Bitmap.CompressFormat a3 = a(b2);
        b2.compress(a3, this.f3150b, outputStream);
        if (!Log.isLoggable("BitmapEncoder", 2)) {
            return true;
        }
        Log.v("BitmapEncoder", "Compressed with type: " + a3 + " of size " + h.a(b2) + " in " + d.a(a2));
        return true;
    }

    public String a() {
        return "BitmapEncoder.com.bumptech.glide.load.resource.bitmap";
    }

    private Bitmap.CompressFormat a(Bitmap bitmap) {
        if (this.f3149a != null) {
            return this.f3149a;
        }
        if (bitmap.hasAlpha()) {
            return Bitmap.CompressFormat.PNG;
        }
        return Bitmap.CompressFormat.JPEG;
    }
}
