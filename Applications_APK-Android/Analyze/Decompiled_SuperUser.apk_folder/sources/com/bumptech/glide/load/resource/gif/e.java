package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.f;

/* compiled from: GifDrawableTransformation */
public class e implements f<b> {

    /* renamed from: a  reason: collision with root package name */
    private final f<Bitmap> f3239a;

    /* renamed from: b  reason: collision with root package name */
    private final c f3240b;

    public e(f<Bitmap> fVar, c cVar) {
        this.f3239a = fVar;
        this.f3240b = cVar;
    }

    public i<b> a(i<b> iVar, int i, int i2) {
        b b2 = iVar.b();
        Bitmap b3 = iVar.b().b();
        Bitmap b4 = this.f3239a.a(new com.bumptech.glide.load.resource.bitmap.c(b3, this.f3240b), i, i2).b();
        if (!b4.equals(b3)) {
            return new d(new b(b2, b4, this.f3239a));
        }
        return iVar;
    }

    public String a() {
        return this.f3239a.a();
    }
}
