package com.bumptech.glide.load.engine.a;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import com.bumptech.glide.f.h;
import com.bumptech.glide.load.engine.a.a;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

@TargetApi(19)
/* compiled from: SizeConfigStrategy */
public class i implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final Bitmap.Config[] f2981a = {Bitmap.Config.ARGB_8888, null};

    /* renamed from: b  reason: collision with root package name */
    private static final Bitmap.Config[] f2982b = {Bitmap.Config.RGB_565};

    /* renamed from: c  reason: collision with root package name */
    private static final Bitmap.Config[] f2983c = {Bitmap.Config.ARGB_4444};

    /* renamed from: d  reason: collision with root package name */
    private static final Bitmap.Config[] f2984d = {Bitmap.Config.ALPHA_8};

    /* renamed from: e  reason: collision with root package name */
    private final b f2985e = new b();

    /* renamed from: f  reason: collision with root package name */
    private final e<a, Bitmap> f2986f = new e<>();

    /* renamed from: g  reason: collision with root package name */
    private final Map<Bitmap.Config, NavigableMap<Integer, Integer>> f2987g = new HashMap();

    public void a(Bitmap bitmap) {
        a a2 = this.f2985e.a(h.a(bitmap), bitmap.getConfig());
        this.f2986f.a(a2, bitmap);
        NavigableMap<Integer, Integer> a3 = a(bitmap.getConfig());
        Integer num = (Integer) a3.get(Integer.valueOf(a2.f2990b));
        a3.put(Integer.valueOf(a2.f2990b), Integer.valueOf(num == null ? 1 : num.intValue() + 1));
    }

    public Bitmap a(int i, int i2, Bitmap.Config config) {
        int a2 = h.a(i, i2, config);
        Bitmap a3 = this.f2986f.a((a.C0038a) a(this.f2985e.a(a2, config), a2, config));
        if (a3 != null) {
            a(Integer.valueOf(h.a(a3)), a3.getConfig());
            a3.reconfigure(i, i2, a3.getConfig() != null ? a3.getConfig() : Bitmap.Config.ARGB_8888);
        }
        return a3;
    }

    private a a(a aVar, int i, Bitmap.Config config) {
        Bitmap.Config[] b2 = b(config);
        int length = b2.length;
        int i2 = 0;
        while (i2 < length) {
            Bitmap.Config config2 = b2[i2];
            Integer ceilingKey = a(config2).ceilingKey(Integer.valueOf(i));
            if (ceilingKey == null || ceilingKey.intValue() > i * 8) {
                i2++;
            } else {
                if (ceilingKey.intValue() == i) {
                    if (config2 == null) {
                        if (config == null) {
                            return aVar;
                        }
                    } else if (config2.equals(config)) {
                        return aVar;
                    }
                }
                this.f2985e.a(aVar);
                return this.f2985e.a(ceilingKey.intValue(), config2);
            }
        }
        return aVar;
    }

    public Bitmap a() {
        Bitmap a2 = this.f2986f.a();
        if (a2 != null) {
            a(Integer.valueOf(h.a(a2)), a2.getConfig());
        }
        return a2;
    }

    private void a(Integer num, Bitmap.Config config) {
        NavigableMap<Integer, Integer> a2 = a(config);
        Integer num2 = (Integer) a2.get(num);
        if (num2.intValue() == 1) {
            a2.remove(num);
        } else {
            a2.put(num, Integer.valueOf(num2.intValue() - 1));
        }
    }

    private NavigableMap<Integer, Integer> a(Bitmap.Config config) {
        NavigableMap<Integer, Integer> navigableMap = this.f2987g.get(config);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.f2987g.put(config, treeMap);
        return treeMap;
    }

    public String b(Bitmap bitmap) {
        return b(h.a(bitmap), bitmap.getConfig());
    }

    public String b(int i, int i2, Bitmap.Config config) {
        return b(h.a(i, i2, config), config);
    }

    public int c(Bitmap bitmap) {
        return h.a(bitmap);
    }

    public String toString() {
        StringBuilder append = new StringBuilder().append("SizeConfigStrategy{groupedMap=").append(this.f2986f).append(", sortedSizes=(");
        for (Map.Entry next : this.f2987g.entrySet()) {
            append.append(next.getKey()).append('[').append(next.getValue()).append("], ");
        }
        if (!this.f2987g.isEmpty()) {
            append.replace(append.length() - 2, append.length(), "");
        }
        return append.append(")}").toString();
    }

    /* compiled from: SizeConfigStrategy */
    static class b extends b<a> {
        b() {
        }

        public a a(int i, Bitmap.Config config) {
            a aVar = (a) c();
            aVar.a(i, config);
            return aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public a b() {
            return new a(this);
        }
    }

    /* compiled from: SizeConfigStrategy */
    static final class a implements h {

        /* renamed from: a  reason: collision with root package name */
        private final b f2989a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public int f2990b;

        /* renamed from: c  reason: collision with root package name */
        private Bitmap.Config f2991c;

        public a(b bVar) {
            this.f2989a = bVar;
        }

        public void a(int i, Bitmap.Config config) {
            this.f2990b = i;
            this.f2991c = config;
        }

        public void a() {
            this.f2989a.a(this);
        }

        public String toString() {
            return i.b(this.f2990b, this.f2991c);
        }

        /* JADX WARNING: Removed duplicated region for block: B:8:0x0015 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                r0 = 0
                boolean r1 = r4 instanceof com.bumptech.glide.load.engine.a.i.a
                if (r1 == 0) goto L_0x0016
                com.bumptech.glide.load.engine.a.i$a r4 = (com.bumptech.glide.load.engine.a.i.a) r4
                int r1 = r3.f2990b
                int r2 = r4.f2990b
                if (r1 != r2) goto L_0x0016
                android.graphics.Bitmap$Config r1 = r3.f2991c
                if (r1 != 0) goto L_0x0017
                android.graphics.Bitmap$Config r1 = r4.f2991c
                if (r1 != 0) goto L_0x0016
            L_0x0015:
                r0 = 1
            L_0x0016:
                return r0
            L_0x0017:
                android.graphics.Bitmap$Config r1 = r3.f2991c
                android.graphics.Bitmap$Config r2 = r4.f2991c
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x0016
                goto L_0x0015
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.engine.a.i.a.equals(java.lang.Object):boolean");
        }

        public int hashCode() {
            return (this.f2991c != null ? this.f2991c.hashCode() : 0) + (this.f2990b * 31);
        }
    }

    /* access modifiers changed from: private */
    public static String b(int i, Bitmap.Config config) {
        return "[" + i + "](" + config + ")";
    }

    /* renamed from: com.bumptech.glide.load.engine.a.i$1  reason: invalid class name */
    /* compiled from: SizeConfigStrategy */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2988a = new int[Bitmap.Config.values().length];

        static {
            try {
                f2988a[Bitmap.Config.ARGB_8888.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f2988a[Bitmap.Config.RGB_565.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f2988a[Bitmap.Config.ARGB_4444.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f2988a[Bitmap.Config.ALPHA_8.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    private static Bitmap.Config[] b(Bitmap.Config config) {
        switch (AnonymousClass1.f2988a[config.ordinal()]) {
            case 1:
                return f2981a;
            case 2:
                return f2982b;
            case 3:
                return f2983c;
            case 4:
                return f2984d;
            default:
                return new Bitmap.Config[]{config};
        }
    }
}
