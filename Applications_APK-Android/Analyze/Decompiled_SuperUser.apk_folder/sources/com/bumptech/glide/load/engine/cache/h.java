package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.f.e;
import com.bumptech.glide.load.b;
import com.duapps.ad.AdError;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: SafeKeyGenerator */
class h {

    /* renamed from: a  reason: collision with root package name */
    private final e<b, String> f3044a = new e<>(AdError.NETWORK_ERROR_CODE);

    h() {
    }

    public String a(b bVar) {
        String b2;
        synchronized (this.f3044a) {
            b2 = this.f3044a.b(bVar);
        }
        if (b2 == null) {
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-256");
                bVar.a(instance);
                b2 = com.bumptech.glide.f.h.a(instance.digest());
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            } catch (NoSuchAlgorithmException e3) {
                e3.printStackTrace();
            }
            synchronized (this.f3044a) {
                this.f3044a.b(bVar, b2);
            }
        }
        return b2;
    }
}
