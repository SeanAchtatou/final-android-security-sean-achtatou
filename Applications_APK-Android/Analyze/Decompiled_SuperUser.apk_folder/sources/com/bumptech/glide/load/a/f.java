package com.bumptech.glide.load.a;

import android.text.TextUtils;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.model.c;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

/* compiled from: HttpUrlFetcher */
public class f implements c<InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private static final b f2923a = new a();

    /* renamed from: b  reason: collision with root package name */
    private final c f2924b;

    /* renamed from: c  reason: collision with root package name */
    private final b f2925c;

    /* renamed from: d  reason: collision with root package name */
    private HttpURLConnection f2926d;

    /* renamed from: e  reason: collision with root package name */
    private InputStream f2927e;

    /* renamed from: f  reason: collision with root package name */
    private volatile boolean f2928f;

    /* compiled from: HttpUrlFetcher */
    interface b {
        HttpURLConnection a(URL url);
    }

    public f(c cVar) {
        this(cVar, f2923a);
    }

    f(c cVar, b bVar) {
        this.f2924b = cVar;
        this.f2925c = bVar;
    }

    /* renamed from: b */
    public InputStream a(Priority priority) {
        return a(this.f2924b.a(), 0, null, this.f2924b.b());
    }

    private InputStream a(URL url, int i, URL url2, Map<String, String> map) {
        if (i >= 5) {
            throw new IOException("Too many (> 5) redirects!");
        }
        if (url2 != null) {
            try {
                if (url.toURI().equals(url2.toURI())) {
                    throw new IOException("In re-direct loop");
                }
            } catch (URISyntaxException e2) {
            }
        }
        this.f2926d = this.f2925c.a(url);
        for (Map.Entry next : map.entrySet()) {
            this.f2926d.addRequestProperty((String) next.getKey(), (String) next.getValue());
        }
        this.f2926d.setConnectTimeout(2500);
        this.f2926d.setReadTimeout(2500);
        this.f2926d.setUseCaches(false);
        this.f2926d.setDoInput(true);
        this.f2926d.connect();
        if (this.f2928f) {
            return null;
        }
        int responseCode = this.f2926d.getResponseCode();
        if (responseCode / 100 == 2) {
            return a(this.f2926d);
        }
        if (responseCode / 100 == 3) {
            String headerField = this.f2926d.getHeaderField("Location");
            if (!TextUtils.isEmpty(headerField)) {
                return a(new URL(url, headerField), i + 1, url, map);
            }
            throw new IOException("Received empty or null redirect url");
        } else if (responseCode == -1) {
            throw new IOException("Unable to retrieve response code from HttpUrlConnection.");
        } else {
            throw new IOException("Request failed " + responseCode + ": " + this.f2926d.getResponseMessage());
        }
    }

    private InputStream a(HttpURLConnection httpURLConnection) {
        if (TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
            this.f2927e = com.bumptech.glide.f.b.a(httpURLConnection.getInputStream(), (long) httpURLConnection.getContentLength());
        } else {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Got non empty content encoding: " + httpURLConnection.getContentEncoding());
            }
            this.f2927e = httpURLConnection.getInputStream();
        }
        return this.f2927e;
    }

    public void a() {
        if (this.f2927e != null) {
            try {
                this.f2927e.close();
            } catch (IOException e2) {
            }
        }
        if (this.f2926d != null) {
            this.f2926d.disconnect();
        }
    }

    public String b() {
        return this.f2924b.c();
    }

    public void c() {
        this.f2928f = true;
    }

    /* compiled from: HttpUrlFetcher */
    private static class a implements b {
        private a() {
        }

        public HttpURLConnection a(URL url) {
            return (HttpURLConnection) url.openConnection();
        }
    }
}
