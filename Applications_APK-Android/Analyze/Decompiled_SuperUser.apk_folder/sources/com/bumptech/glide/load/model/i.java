package com.bumptech.glide.load.model;

import android.text.TextUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: LazyHeaders */
public final class i implements d {

    /* renamed from: c  reason: collision with root package name */
    private final Map<String, List<h>> f3095c;

    /* renamed from: d  reason: collision with root package name */
    private volatile Map<String, String> f3096d;

    i(Map<String, List<h>> map) {
        this.f3095c = Collections.unmodifiableMap(map);
    }

    public Map<String, String> a() {
        if (this.f3096d == null) {
            synchronized (this) {
                if (this.f3096d == null) {
                    this.f3096d = Collections.unmodifiableMap(b());
                }
            }
        }
        return this.f3096d;
    }

    private Map<String, String> b() {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.f3095c.entrySet()) {
            StringBuilder sb = new StringBuilder();
            List list = (List) next.getValue();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= list.size()) {
                    break;
                }
                sb.append(((h) list.get(i2)).a());
                if (i2 != list.size() - 1) {
                    sb.append(',');
                }
                i = i2 + 1;
            }
            hashMap.put(next.getKey(), sb.toString());
        }
        return hashMap;
    }

    public String toString() {
        return "LazyHeaders{headers=" + this.f3095c + '}';
    }

    public boolean equals(Object obj) {
        if (obj instanceof i) {
            return this.f3095c.equals(((i) obj).f3095c);
        }
        return false;
    }

    public int hashCode() {
        return this.f3095c.hashCode();
    }

    /* compiled from: LazyHeaders */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        private static final String f3097a = System.getProperty("http.agent");

        /* renamed from: b  reason: collision with root package name */
        private static final Map<String, List<h>> f3098b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f3099c = true;

        /* renamed from: d  reason: collision with root package name */
        private Map<String, List<h>> f3100d = f3098b;

        /* renamed from: e  reason: collision with root package name */
        private boolean f3101e = true;

        /* renamed from: f  reason: collision with root package name */
        private boolean f3102f = true;

        static {
            HashMap hashMap = new HashMap(2);
            if (!TextUtils.isEmpty(f3097a)) {
                hashMap.put(io.fabric.sdk.android.services.common.a.HEADER_USER_AGENT, Collections.singletonList(new b(f3097a)));
            }
            hashMap.put("Accept-Encoding", Collections.singletonList(new b("identity")));
            f3098b = Collections.unmodifiableMap(hashMap);
        }

        public i a() {
            this.f3099c = true;
            return new i(this.f3100d);
        }
    }

    /* compiled from: LazyHeaders */
    static final class b implements h {

        /* renamed from: a  reason: collision with root package name */
        private final String f3103a;

        b(String str) {
            this.f3103a = str;
        }

        public String a() {
            return this.f3103a;
        }

        public String toString() {
            return "StringHeaderFactory{value='" + this.f3103a + '\'' + '}';
        }

        public boolean equals(Object obj) {
            if (obj instanceof b) {
                return this.f3103a.equals(((b) obj).f3103a);
            }
            return false;
        }

        public int hashCode() {
            return this.f3103a.hashCode();
        }
    }
}
