package com.bumptech.glide.load.resource.c;

import android.graphics.Bitmap;
import com.bumptech.glide.load.e;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.resource.gif.b;
import java.io.OutputStream;

/* compiled from: GifBitmapWrapperResourceEncoder */
public class d implements e<a> {

    /* renamed from: a  reason: collision with root package name */
    private final e<Bitmap> f3198a;

    /* renamed from: b  reason: collision with root package name */
    private final e<b> f3199b;

    /* renamed from: c  reason: collision with root package name */
    private String f3200c;

    public /* bridge */ /* synthetic */ boolean a(Object obj, OutputStream outputStream) {
        return a((i<a>) ((i) obj), outputStream);
    }

    public d(e<Bitmap> eVar, e<b> eVar2) {
        this.f3198a = eVar;
        this.f3199b = eVar2;
    }

    public boolean a(i<a> iVar, OutputStream outputStream) {
        a b2 = iVar.b();
        i<Bitmap> b3 = b2.b();
        if (b3 != null) {
            return this.f3198a.a(b3, outputStream);
        }
        return this.f3199b.a(b2.c(), outputStream);
    }

    public String a() {
        if (this.f3200c == null) {
            this.f3200c = this.f3198a.a() + this.f3199b.a();
        }
        return this.f3200c;
    }
}
