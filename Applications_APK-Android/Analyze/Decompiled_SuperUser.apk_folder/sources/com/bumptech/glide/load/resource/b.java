package com.bumptech.glide.load.resource;

import com.bumptech.glide.load.e;
import com.bumptech.glide.load.engine.i;
import java.io.OutputStream;

/* compiled from: NullResourceEncoder */
public class b<T> implements e<T> {

    /* renamed from: a  reason: collision with root package name */
    private static final b<?> f3123a = new b<>();

    public static <T> b<T> b() {
        return f3123a;
    }

    public boolean a(i<T> iVar, OutputStream outputStream) {
        return false;
    }

    public String a() {
        return "";
    }
}
