package com.bumptech.glide.load.engine.cache;

import android.annotation.SuppressLint;
import com.bumptech.glide.f.e;
import com.bumptech.glide.load.b;
import com.bumptech.glide.load.engine.cache.g;
import com.bumptech.glide.load.engine.i;

/* compiled from: LruResourceCache */
public class f extends e<b, i<?>> implements g {

    /* renamed from: a  reason: collision with root package name */
    private g.a f3043a;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ int a(Object obj) {
        return a((i<?>) ((i) obj));
    }

    public /* synthetic */ i a(b bVar) {
        return (i) super.c(bVar);
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void a(Object obj, Object obj2) {
        a((b) obj, (i<?>) ((i) obj2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.f.e.b(java.lang.Object, java.lang.Object):Y
     arg types: [com.bumptech.glide.load.b, com.bumptech.glide.load.engine.i]
     candidates:
      com.bumptech.glide.load.engine.cache.f.b(com.bumptech.glide.load.b, com.bumptech.glide.load.engine.i):com.bumptech.glide.load.engine.i
      com.bumptech.glide.load.engine.cache.g.b(com.bumptech.glide.load.b, com.bumptech.glide.load.engine.i<?>):com.bumptech.glide.load.engine.i<?>
      com.bumptech.glide.f.e.b(java.lang.Object, java.lang.Object):Y */
    public /* bridge */ /* synthetic */ i b(b bVar, i iVar) {
        return (i) super.b((Object) bVar, (Object) iVar);
    }

    public f(int i) {
        super(i);
    }

    public void a(g.a aVar) {
        this.f3043a = aVar;
    }

    /* access modifiers changed from: protected */
    public void a(b bVar, i<?> iVar) {
        if (this.f3043a != null) {
            this.f3043a.b(iVar);
        }
    }

    /* access modifiers changed from: protected */
    public int a(i<?> iVar) {
        return iVar.c();
    }

    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (i >= 60) {
            a();
        } else if (i >= 40) {
            b(b() / 2);
        }
    }
}
