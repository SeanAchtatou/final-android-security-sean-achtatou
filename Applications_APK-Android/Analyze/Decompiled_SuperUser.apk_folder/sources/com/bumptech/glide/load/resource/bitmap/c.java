package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.f.h;
import com.bumptech.glide.load.engine.i;

/* compiled from: BitmapResource */
public class c implements i<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final Bitmap f3151a;

    /* renamed from: b  reason: collision with root package name */
    private final com.bumptech.glide.load.engine.a.c f3152b;

    public static c a(Bitmap bitmap, com.bumptech.glide.load.engine.a.c cVar) {
        if (bitmap == null) {
            return null;
        }
        return new c(bitmap, cVar);
    }

    public c(Bitmap bitmap, com.bumptech.glide.load.engine.a.c cVar) {
        if (bitmap == null) {
            throw new NullPointerException("Bitmap must not be null");
        } else if (cVar == null) {
            throw new NullPointerException("BitmapPool must not be null");
        } else {
            this.f3151a = bitmap;
            this.f3152b = cVar;
        }
    }

    /* renamed from: a */
    public Bitmap b() {
        return this.f3151a;
    }

    public int c() {
        return h.a(this.f3151a);
    }

    public void d() {
        if (!this.f3152b.a(this.f3151a)) {
            this.f3151a.recycle();
        }
    }
}
