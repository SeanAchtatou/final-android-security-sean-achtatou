package com.bumptech.glide.load.model.stream;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.e;
import com.bumptech.glide.load.a.h;
import com.bumptech.glide.load.a.i;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.c;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.model.l;
import com.bumptech.glide.load.model.p;
import java.io.InputStream;

public class StreamUriLoader extends p<InputStream> implements c<Uri> {

    public static class a implements l<Uri, InputStream> {
        public k<Uri, InputStream> a(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new StreamUriLoader(context, genericLoaderFactory.a(c.class, InputStream.class));
        }

        public void a() {
        }
    }

    public StreamUriLoader(Context context) {
        this(context, e.a(c.class, context));
    }

    public StreamUriLoader(Context context, k<c, InputStream> kVar) {
        super(context, kVar);
    }

    /* access modifiers changed from: protected */
    public com.bumptech.glide.load.a.c<InputStream> a(Context context, Uri uri) {
        return new i(context, uri);
    }

    /* access modifiers changed from: protected */
    public com.bumptech.glide.load.a.c<InputStream> a(Context context, String str) {
        return new h(context.getApplicationContext().getAssets(), str);
    }
}
