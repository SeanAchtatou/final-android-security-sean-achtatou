package com.bumptech.glide.load.engine;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.bumptech.glide.f.h;

/* compiled from: ResourceRecycler */
class j {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3071a;

    /* renamed from: b  reason: collision with root package name */
    private final Handler f3072b = new Handler(Looper.getMainLooper(), new a());

    j() {
    }

    public void a(i<?> iVar) {
        h.a();
        if (this.f3071a) {
            this.f3072b.obtainMessage(1, iVar).sendToTarget();
            return;
        }
        this.f3071a = true;
        iVar.d();
        this.f3071a = false;
    }

    /* compiled from: ResourceRecycler */
    private static class a implements Handler.Callback {
        private a() {
        }

        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((i) message.obj).d();
            return true;
        }
    }
}
