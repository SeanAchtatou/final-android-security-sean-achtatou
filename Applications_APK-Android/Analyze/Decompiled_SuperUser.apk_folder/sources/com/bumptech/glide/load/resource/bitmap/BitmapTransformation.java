package com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.e;
import com.bumptech.glide.f.h;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.f;

public abstract class BitmapTransformation implements f<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private c f3130a;

    /* access modifiers changed from: protected */
    public abstract Bitmap a(c cVar, Bitmap bitmap, int i, int i2);

    public BitmapTransformation(Context context) {
        this(e.a(context).a());
    }

    public BitmapTransformation(c cVar) {
        this.f3130a = cVar;
    }

    public final i<Bitmap> a(i<Bitmap> iVar, int i, int i2) {
        if (!h.a(i, i2)) {
            throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
        }
        Bitmap b2 = iVar.b();
        if (i == Integer.MIN_VALUE) {
            i = b2.getWidth();
        }
        if (i2 == Integer.MIN_VALUE) {
            i2 = b2.getHeight();
        }
        Bitmap a2 = a(this.f3130a, b2, i, i2);
        if (b2.equals(a2)) {
            return iVar;
        }
        return c.a(a2, this.f3130a);
    }
}
