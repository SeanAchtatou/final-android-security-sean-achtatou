package com.bumptech.glide.load.engine.cache;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: DiskCacheWriteLocker */
final class c {

    /* renamed from: a  reason: collision with root package name */
    private final Map<com.bumptech.glide.load.b, a> f3030a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    private final b f3031b = new b();

    c() {
    }

    /* access modifiers changed from: package-private */
    public void a(com.bumptech.glide.load.b bVar) {
        a aVar;
        synchronized (this) {
            aVar = this.f3030a.get(bVar);
            if (aVar == null) {
                aVar = this.f3031b.a();
                this.f3030a.put(bVar, aVar);
            }
            aVar.f3033b++;
        }
        aVar.f3032a.lock();
    }

    /* access modifiers changed from: package-private */
    public void b(com.bumptech.glide.load.b bVar) {
        a aVar;
        int i;
        synchronized (this) {
            aVar = this.f3030a.get(bVar);
            if (aVar == null || aVar.f3033b <= 0) {
                StringBuilder append = new StringBuilder().append("Cannot release a lock that is not held, key: ").append(bVar).append(", interestedThreads: ");
                if (aVar == null) {
                    i = 0;
                } else {
                    i = aVar.f3033b;
                }
                throw new IllegalArgumentException(append.append(i).toString());
            }
            int i2 = aVar.f3033b - 1;
            aVar.f3033b = i2;
            if (i2 == 0) {
                a remove = this.f3030a.remove(bVar);
                if (!remove.equals(aVar)) {
                    throw new IllegalStateException("Removed the wrong lock, expected to remove: " + aVar + ", but actually removed: " + remove + ", key: " + bVar);
                }
                this.f3031b.a(remove);
            }
        }
        aVar.f3032a.unlock();
    }

    /* compiled from: DiskCacheWriteLocker */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        final Lock f3032a;

        /* renamed from: b  reason: collision with root package name */
        int f3033b;

        private a() {
            this.f3032a = new ReentrantLock();
        }
    }

    /* compiled from: DiskCacheWriteLocker */
    private static class b {

        /* renamed from: a  reason: collision with root package name */
        private final Queue<a> f3034a;

        private b() {
            this.f3034a = new ArrayDeque();
        }

        /* access modifiers changed from: package-private */
        public a a() {
            a poll;
            synchronized (this.f3034a) {
                poll = this.f3034a.poll();
            }
            if (poll == null) {
                return new a();
            }
            return poll;
        }

        /* access modifiers changed from: package-private */
        public void a(a aVar) {
            synchronized (this.f3034a) {
                if (this.f3034a.size() < 10) {
                    this.f3034a.offer(aVar);
                }
            }
        }
    }
}
