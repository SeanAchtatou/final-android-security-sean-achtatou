package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.a;
import com.bumptech.glide.load.b;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.f;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

/* compiled from: EngineKey */
class e implements b {

    /* renamed from: a  reason: collision with root package name */
    private final String f3045a;

    /* renamed from: b  reason: collision with root package name */
    private final int f3046b;

    /* renamed from: c  reason: collision with root package name */
    private final int f3047c;

    /* renamed from: d  reason: collision with root package name */
    private final d f3048d;

    /* renamed from: e  reason: collision with root package name */
    private final d f3049e;

    /* renamed from: f  reason: collision with root package name */
    private final f f3050f;

    /* renamed from: g  reason: collision with root package name */
    private final com.bumptech.glide.load.e f3051g;

    /* renamed from: h  reason: collision with root package name */
    private final com.bumptech.glide.load.resource.transcode.b f3052h;
    private final a i;
    private final b j;
    private String k;
    private int l;
    private b m;

    public e(String str, b bVar, int i2, int i3, d dVar, d dVar2, f fVar, com.bumptech.glide.load.e eVar, com.bumptech.glide.load.resource.transcode.b bVar2, a aVar) {
        this.f3045a = str;
        this.j = bVar;
        this.f3046b = i2;
        this.f3047c = i3;
        this.f3048d = dVar;
        this.f3049e = dVar2;
        this.f3050f = fVar;
        this.f3051g = eVar;
        this.f3052h = bVar2;
        this.i = aVar;
    }

    public b a() {
        if (this.m == null) {
            this.m = new h(this.f3045a, this.j);
        }
        return this.m;
    }

    public boolean equals(Object obj) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        if (!this.f3045a.equals(eVar.f3045a) || !this.j.equals(eVar.j) || this.f3047c != eVar.f3047c || this.f3046b != eVar.f3046b) {
            return false;
        }
        if ((this.f3050f == null) ^ (eVar.f3050f == null)) {
            return false;
        }
        if (this.f3050f != null && !this.f3050f.a().equals(eVar.f3050f.a())) {
            return false;
        }
        if (this.f3049e == null) {
            z = true;
        } else {
            z = false;
        }
        if (z ^ (eVar.f3049e == null)) {
            return false;
        }
        if (this.f3049e != null && !this.f3049e.a().equals(eVar.f3049e.a())) {
            return false;
        }
        if (this.f3048d == null) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2 ^ (eVar.f3048d == null)) {
            return false;
        }
        if (this.f3048d != null && !this.f3048d.a().equals(eVar.f3048d.a())) {
            return false;
        }
        if (this.f3051g == null) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (z3 ^ (eVar.f3051g == null)) {
            return false;
        }
        if (this.f3051g != null && !this.f3051g.a().equals(eVar.f3051g.a())) {
            return false;
        }
        if (this.f3052h == null) {
            z4 = true;
        } else {
            z4 = false;
        }
        if (z4 ^ (eVar.f3052h == null)) {
            return false;
        }
        if (this.f3052h != null && !this.f3052h.a().equals(eVar.f3052h.a())) {
            return false;
        }
        if (this.i == null) {
            z5 = true;
        } else {
            z5 = false;
        }
        if (z5 ^ (eVar.i == null)) {
            return false;
        }
        if (this.i == null || this.i.a().equals(eVar.i.a())) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        if (this.l == 0) {
            this.l = this.f3045a.hashCode();
            this.l = (this.l * 31) + this.j.hashCode();
            this.l = (this.l * 31) + this.f3046b;
            this.l = (this.l * 31) + this.f3047c;
            this.l = (this.f3048d != null ? this.f3048d.a().hashCode() : 0) + (this.l * 31);
            int i7 = this.l * 31;
            if (this.f3049e != null) {
                i2 = this.f3049e.a().hashCode();
            } else {
                i2 = 0;
            }
            this.l = i2 + i7;
            int i8 = this.l * 31;
            if (this.f3050f != null) {
                i3 = this.f3050f.a().hashCode();
            } else {
                i3 = 0;
            }
            this.l = i3 + i8;
            int i9 = this.l * 31;
            if (this.f3051g != null) {
                i4 = this.f3051g.a().hashCode();
            } else {
                i4 = 0;
            }
            this.l = i4 + i9;
            int i10 = this.l * 31;
            if (this.f3052h != null) {
                i5 = this.f3052h.a().hashCode();
            } else {
                i5 = 0;
            }
            this.l = i5 + i10;
            int i11 = this.l * 31;
            if (this.i != null) {
                i6 = this.i.a().hashCode();
            }
            this.l = i11 + i6;
        }
        return this.l;
    }

    public String toString() {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        if (this.k == null) {
            StringBuilder append = new StringBuilder().append("EngineKey{").append(this.f3045a).append('+').append(this.j).append("+[").append(this.f3046b).append('x').append(this.f3047c).append("]+").append('\'');
            if (this.f3048d != null) {
                str = this.f3048d.a();
            } else {
                str = "";
            }
            StringBuilder append2 = append.append(str).append('\'').append('+').append('\'');
            if (this.f3049e != null) {
                str2 = this.f3049e.a();
            } else {
                str2 = "";
            }
            StringBuilder append3 = append2.append(str2).append('\'').append('+').append('\'');
            if (this.f3050f != null) {
                str3 = this.f3050f.a();
            } else {
                str3 = "";
            }
            StringBuilder append4 = append3.append(str3).append('\'').append('+').append('\'');
            if (this.f3051g != null) {
                str4 = this.f3051g.a();
            } else {
                str4 = "";
            }
            StringBuilder append5 = append4.append(str4).append('\'').append('+').append('\'');
            if (this.f3052h != null) {
                str5 = this.f3052h.a();
            } else {
                str5 = "";
            }
            StringBuilder append6 = append5.append(str5).append('\'').append('+').append('\'');
            if (this.i != null) {
                str6 = this.i.a();
            } else {
                str6 = "";
            }
            this.k = append6.append(str6).append('\'').append('}').toString();
        }
        return this.k;
    }

    public void a(MessageDigest messageDigest) {
        byte[] array = ByteBuffer.allocate(8).putInt(this.f3046b).putInt(this.f3047c).array();
        this.j.a(messageDigest);
        messageDigest.update(this.f3045a.getBytes("UTF-8"));
        messageDigest.update(array);
        messageDigest.update((this.f3048d != null ? this.f3048d.a() : "").getBytes("UTF-8"));
        messageDigest.update((this.f3049e != null ? this.f3049e.a() : "").getBytes("UTF-8"));
        messageDigest.update((this.f3050f != null ? this.f3050f.a() : "").getBytes("UTF-8"));
        messageDigest.update((this.f3051g != null ? this.f3051g.a() : "").getBytes("UTF-8"));
        messageDigest.update((this.i != null ? this.i.a() : "").getBytes("UTF-8"));
    }
}
