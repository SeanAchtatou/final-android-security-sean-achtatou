package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.b;
import java.security.MessageDigest;

/* compiled from: OriginalKey */
class h implements b {

    /* renamed from: a  reason: collision with root package name */
    private final String f3069a;

    /* renamed from: b  reason: collision with root package name */
    private final b f3070b;

    public h(String str, b bVar) {
        this.f3069a = str;
        this.f3070b = bVar;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        h hVar = (h) obj;
        if (!this.f3069a.equals(hVar.f3069a)) {
            return false;
        }
        if (!this.f3070b.equals(hVar.f3070b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.f3069a.hashCode() * 31) + this.f3070b.hashCode();
    }

    public void a(MessageDigest messageDigest) {
        messageDigest.update(this.f3069a.getBytes("UTF-8"));
        this.f3070b.a(messageDigest);
    }
}
