package com.bumptech.glide.load.resource.c;

import com.bumptech.glide.load.d;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.model.f;
import java.io.InputStream;

/* compiled from: GifBitmapWrapperStreamResourceDecoder */
public class e implements d<InputStream, a> {

    /* renamed from: a  reason: collision with root package name */
    private final d<f, a> f3201a;

    public e(d<f, a> dVar) {
        this.f3201a = dVar;
    }

    public i<a> a(InputStream inputStream, int i, int i2) {
        return this.f3201a.a(new f(inputStream, null), i, i2);
    }

    public String a() {
        return this.f3201a.a();
    }
}
