package com.bumptech.glide.load.resource.c;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.resource.gif.b;

/* compiled from: GifBitmapWrapper */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private final i<b> f3187a;

    /* renamed from: b  reason: collision with root package name */
    private final i<Bitmap> f3188b;

    public a(i<Bitmap> iVar, i<b> iVar2) {
        if (iVar != null && iVar2 != null) {
            throw new IllegalArgumentException("Can only contain either a bitmap resource or a gif resource, not both");
        } else if (iVar == null && iVar2 == null) {
            throw new IllegalArgumentException("Must contain either a bitmap resource or a gif resource");
        } else {
            this.f3188b = iVar;
            this.f3187a = iVar2;
        }
    }

    public int a() {
        if (this.f3188b != null) {
            return this.f3188b.c();
        }
        return this.f3187a.c();
    }

    public i<Bitmap> b() {
        return this.f3188b;
    }

    public i<b> c() {
        return this.f3187a;
    }
}
