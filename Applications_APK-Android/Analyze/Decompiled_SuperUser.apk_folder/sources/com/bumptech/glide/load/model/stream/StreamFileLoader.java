package com.bumptech.glide.load.model.stream;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.e;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.b;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.model.l;
import java.io.File;
import java.io.InputStream;

public class StreamFileLoader extends b<InputStream> implements c<File> {

    public static class a implements l<File, InputStream> {
        public k<File, InputStream> a(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new StreamFileLoader(genericLoaderFactory.a(Uri.class, InputStream.class));
        }

        public void a() {
        }
    }

    public StreamFileLoader(Context context) {
        this(e.a(Uri.class, context));
    }

    public StreamFileLoader(k<Uri, InputStream> kVar) {
        super(kVar);
    }
}
