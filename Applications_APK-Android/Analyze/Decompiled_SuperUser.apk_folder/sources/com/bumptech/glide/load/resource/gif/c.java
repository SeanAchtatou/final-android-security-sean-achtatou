package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import com.bumptech.glide.d.b;
import com.bumptech.glide.load.a;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.e;
import com.bumptech.glide.load.model.n;
import java.io.File;
import java.io.InputStream;

/* compiled from: GifDrawableLoadProvider */
public class c implements b<InputStream, b> {

    /* renamed from: a  reason: collision with root package name */
    private final GifResourceDecoder f3235a;

    /* renamed from: b  reason: collision with root package name */
    private final i f3236b;

    /* renamed from: c  reason: collision with root package name */
    private final n f3237c;

    /* renamed from: d  reason: collision with root package name */
    private final com.bumptech.glide.load.resource.b.c<b> f3238d = new com.bumptech.glide.load.resource.b.c<>(this.f3235a);

    public c(Context context, com.bumptech.glide.load.engine.a.c cVar) {
        this.f3235a = new GifResourceDecoder(context, cVar);
        this.f3236b = new i(cVar);
        this.f3237c = new n();
    }

    public d<File, b> a() {
        return this.f3238d;
    }

    public d<InputStream, b> b() {
        return this.f3235a;
    }

    public a<InputStream> c() {
        return this.f3237c;
    }

    public e<b> d() {
        return this.f3236b;
    }
}
