package com.bumptech.glide.load.model;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import com.bumptech.glide.load.a.c;

/* compiled from: ResourceLoader */
public class m<T> implements k<Integer, T> {

    /* renamed from: a  reason: collision with root package name */
    private final k<Uri, T> f3110a;

    /* renamed from: b  reason: collision with root package name */
    private final Resources f3111b;

    public m(Context context, k kVar) {
        this(context.getResources(), kVar);
    }

    public m(Resources resources, k kVar) {
        this.f3111b = resources;
        this.f3110a = kVar;
    }

    public c<T> a(Integer num, int i, int i2) {
        Uri uri;
        try {
            uri = Uri.parse("android.resource://" + this.f3111b.getResourcePackageName(num.intValue()) + '/' + this.f3111b.getResourceTypeName(num.intValue()) + '/' + this.f3111b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e2) {
            if (Log.isLoggable("ResourceLoader", 5)) {
                Log.w("ResourceLoader", "Received invalid resource id: " + num, e2);
            }
            uri = null;
        }
        if (uri != null) {
            return this.f3110a.a(uri, i, i2);
        }
        return null;
    }
}
