package com.bumptech.glide.load.resource.c;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.resource.gif.b;
import com.bumptech.glide.load.resource.gif.e;

/* compiled from: GifBitmapWrapperTransformation */
public class f implements com.bumptech.glide.load.f<a> {

    /* renamed from: a  reason: collision with root package name */
    private final com.bumptech.glide.load.f<Bitmap> f3202a;

    /* renamed from: b  reason: collision with root package name */
    private final com.bumptech.glide.load.f<b> f3203b;

    public f(c cVar, com.bumptech.glide.load.f<Bitmap> fVar) {
        this(fVar, new e(fVar, cVar));
    }

    f(com.bumptech.glide.load.f<Bitmap> fVar, com.bumptech.glide.load.f<b> fVar2) {
        this.f3202a = fVar;
        this.f3203b = fVar2;
    }

    public i<a> a(i<a> iVar, int i, int i2) {
        i<Bitmap> b2 = iVar.b().b();
        i<b> c2 = iVar.b().c();
        if (b2 != null && this.f3202a != null) {
            i<Bitmap> a2 = this.f3202a.a(b2, i, i2);
            if (!b2.equals(a2)) {
                return new b(new a(a2, iVar.b().c()));
            }
            return iVar;
        } else if (c2 == null || this.f3203b == null) {
            return iVar;
        } else {
            i<b> a3 = this.f3203b.a(c2, i, i2);
            if (!c2.equals(a3)) {
                return new b(new a(iVar.b().b(), a3));
            }
            return iVar;
        }
    }

    public String a() {
        return this.f3202a.a();
    }
}
