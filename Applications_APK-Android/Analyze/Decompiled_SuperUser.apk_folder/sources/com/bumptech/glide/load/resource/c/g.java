package com.bumptech.glide.load.resource.c;

import android.graphics.Bitmap;
import com.bumptech.glide.d.b;
import com.bumptech.glide.load.a;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.e;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.model.f;
import java.io.File;
import java.io.InputStream;

/* compiled from: ImageVideoGifDrawableLoadProvider */
public class g implements b<f, a> {

    /* renamed from: a  reason: collision with root package name */
    private final d<File, a> f3204a;

    /* renamed from: b  reason: collision with root package name */
    private final d<f, a> f3205b;

    /* renamed from: c  reason: collision with root package name */
    private final e<a> f3206c;

    /* renamed from: d  reason: collision with root package name */
    private final a<f> f3207d;

    public g(b<f, Bitmap> bVar, b<InputStream, com.bumptech.glide.load.resource.gif.b> bVar2, c cVar) {
        c cVar2 = new c(bVar.b(), bVar2.b(), cVar);
        this.f3204a = new com.bumptech.glide.load.resource.b.c(new e(cVar2));
        this.f3205b = cVar2;
        this.f3206c = new d(bVar.d(), bVar2.d());
        this.f3207d = bVar.c();
    }

    public d<File, a> a() {
        return this.f3204a;
    }

    public d<f, a> b() {
        return this.f3205b;
    }

    public a<f> c() {
        return this.f3207d;
    }

    public e<a> d() {
        return this.f3206c;
    }
}
