package com.bumptech.glide.load.a;

import com.bumptech.glide.Priority;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/* compiled from: ByteArrayFetcher */
public class b implements c<InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f2921a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2922b;

    public b(byte[] bArr, String str) {
        this.f2921a = bArr;
        this.f2922b = str;
    }

    /* renamed from: b */
    public InputStream a(Priority priority) {
        return new ByteArrayInputStream(this.f2921a);
    }

    public void a() {
    }

    public String b() {
        return this.f2922b;
    }

    public void c() {
    }
}
