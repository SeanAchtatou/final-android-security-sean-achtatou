package com.bumptech.glide.load.model.file_descriptor;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.e;
import com.bumptech.glide.load.a.d;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.c;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.model.l;
import com.bumptech.glide.load.model.p;

public class FileDescriptorUriLoader extends p<ParcelFileDescriptor> implements a<Uri> {

    public static class a implements l<Uri, ParcelFileDescriptor> {
        public k<Uri, ParcelFileDescriptor> a(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new FileDescriptorUriLoader(context, genericLoaderFactory.a(c.class, ParcelFileDescriptor.class));
        }

        public void a() {
        }
    }

    public FileDescriptorUriLoader(Context context) {
        this(context, e.b(c.class, context));
    }

    public FileDescriptorUriLoader(Context context, k<c, ParcelFileDescriptor> kVar) {
        super(context, kVar);
    }

    /* access modifiers changed from: protected */
    public com.bumptech.glide.load.a.c<ParcelFileDescriptor> a(Context context, Uri uri) {
        return new com.bumptech.glide.load.a.e(context, uri);
    }

    /* access modifiers changed from: protected */
    public com.bumptech.glide.load.a.c<ParcelFileDescriptor> a(Context context, String str) {
        return new d(context.getApplicationContext().getAssets(), str);
    }
}
