package com.bumptech.glide.load.model;

import com.bumptech.glide.f.e;
import com.bumptech.glide.f.h;
import java.util.Queue;

/* compiled from: ModelCache */
public class j<A, B> {

    /* renamed from: a  reason: collision with root package name */
    private final e<a<A>, B> f3104a;

    public j() {
        this(250);
    }

    public j(int i) {
        this.f3104a = new e<a<A>, B>(i) {
            /* access modifiers changed from: protected */
            public void a(a<A> aVar, B b2) {
                aVar.a();
            }
        };
    }

    public B a(A a2, int i, int i2) {
        a a3 = a.a(a2, i, i2);
        B b2 = this.f3104a.b(a3);
        a3.a();
        return b2;
    }

    public void a(A a2, int i, int i2, B b2) {
        this.f3104a.b(a.a(a2, i, i2), b2);
    }

    /* compiled from: ModelCache */
    static final class a<A> {

        /* renamed from: a  reason: collision with root package name */
        private static final Queue<a<?>> f3106a = h.a(0);

        /* renamed from: b  reason: collision with root package name */
        private int f3107b;

        /* renamed from: c  reason: collision with root package name */
        private int f3108c;

        /* renamed from: d  reason: collision with root package name */
        private A f3109d;

        static <A> a<A> a(A a2, int i, int i2) {
            a<A> poll = f3106a.poll();
            if (poll == null) {
                poll = new a<>();
            }
            poll.b(a2, i, i2);
            return poll;
        }

        private a() {
        }

        private void b(A a2, int i, int i2) {
            this.f3109d = a2;
            this.f3108c = i;
            this.f3107b = i2;
        }

        public void a() {
            f3106a.offer(this);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                r0 = 0
                boolean r1 = r4 instanceof com.bumptech.glide.load.model.j.a
                if (r1 == 0) goto L_0x001e
                com.bumptech.glide.load.model.j$a r4 = (com.bumptech.glide.load.model.j.a) r4
                int r1 = r3.f3108c
                int r2 = r4.f3108c
                if (r1 != r2) goto L_0x001e
                int r1 = r3.f3107b
                int r2 = r4.f3107b
                if (r1 != r2) goto L_0x001e
                A r1 = r3.f3109d
                A r2 = r4.f3109d
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001e
                r0 = 1
            L_0x001e:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.model.j.a.equals(java.lang.Object):boolean");
        }

        public int hashCode() {
            return (((this.f3107b * 31) + this.f3108c) * 31) + this.f3109d.hashCode();
        }
    }
}
