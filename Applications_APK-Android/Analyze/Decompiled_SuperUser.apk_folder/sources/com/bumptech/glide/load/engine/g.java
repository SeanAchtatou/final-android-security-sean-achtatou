package com.bumptech.glide.load.engine;

import android.os.Looper;
import com.bumptech.glide.load.b;

/* compiled from: EngineResource */
class g<Z> implements i<Z> {

    /* renamed from: a  reason: collision with root package name */
    private final i<Z> f3063a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f3064b;

    /* renamed from: c  reason: collision with root package name */
    private a f3065c;

    /* renamed from: d  reason: collision with root package name */
    private b f3066d;

    /* renamed from: e  reason: collision with root package name */
    private int f3067e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f3068f;

    /* compiled from: EngineResource */
    interface a {
        void b(b bVar, g<?> gVar);
    }

    g(i<Z> iVar, boolean z) {
        if (iVar == null) {
            throw new NullPointerException("Wrapped resource must not be null");
        }
        this.f3063a = iVar;
        this.f3064b = z;
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar, a aVar) {
        this.f3066d = bVar;
        this.f3065c = aVar;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.f3064b;
    }

    public Z b() {
        return this.f3063a.b();
    }

    public int c() {
        return this.f3063a.c();
    }

    public void d() {
        if (this.f3067e > 0) {
            throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
        } else if (this.f3068f) {
            throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
        } else {
            this.f3068f = true;
            this.f3063a.d();
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.f3068f) {
            throw new IllegalStateException("Cannot acquire a recycled resource");
        } else if (!Looper.getMainLooper().equals(Looper.myLooper())) {
            throw new IllegalThreadStateException("Must call acquire on the main thread");
        } else {
            this.f3067e++;
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.f3067e <= 0) {
            throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
        } else if (!Looper.getMainLooper().equals(Looper.myLooper())) {
            throw new IllegalThreadStateException("Must call release on the main thread");
        } else {
            int i = this.f3067e - 1;
            this.f3067e = i;
            if (i == 0) {
                this.f3065c.b(this.f3066d, this);
            }
        }
    }
}
