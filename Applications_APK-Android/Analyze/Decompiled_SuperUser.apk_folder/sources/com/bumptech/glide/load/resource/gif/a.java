package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import com.bumptech.glide.b.a;
import com.bumptech.glide.load.engine.a.c;

/* compiled from: GifBitmapProvider */
class a implements a.C0036a {

    /* renamed from: a  reason: collision with root package name */
    private final c f3218a;

    public a(c cVar) {
        this.f3218a = cVar;
    }

    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.f3218a.b(i, i2, config);
    }

    public void a(Bitmap bitmap) {
        if (!this.f3218a.a(bitmap)) {
            bitmap.recycle();
        }
    }
}
