package com.bumptech.glide.load.model;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.a.c;
import com.google.firebase.analytics.FirebaseAnalytics;

/* compiled from: UriLoader */
public abstract class p<T> implements k<Uri, T> {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3113a;

    /* renamed from: b  reason: collision with root package name */
    private final k<c, T> f3114b;

    /* access modifiers changed from: protected */
    public abstract c<T> a(Context context, Uri uri);

    /* access modifiers changed from: protected */
    public abstract c<T> a(Context context, String str);

    public p(Context context, k<c, T> kVar) {
        this.f3113a = context;
        this.f3114b = kVar;
    }

    public final c<T> a(Uri uri, int i, int i2) {
        String scheme = uri.getScheme();
        if (a(scheme)) {
            if (!a.a(uri)) {
                return a(this.f3113a, uri);
            }
            return a(this.f3113a, a.b(uri));
        } else if (this.f3114b == null) {
            return null;
        } else {
            if ("http".equals(scheme) || "https".equals(scheme)) {
                return this.f3114b.a(new c(uri.toString()), i, i2);
            }
            return null;
        }
    }

    private static boolean a(String str) {
        return "file".equals(str) || FirebaseAnalytics.Param.CONTENT.equals(str) || "android.resource".equals(str);
    }
}
