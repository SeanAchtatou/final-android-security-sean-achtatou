package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.load.engine.cache.a;
import java.io.File;

/* compiled from: DiskLruCacheFactory */
public class d implements a.C0040a {

    /* renamed from: a  reason: collision with root package name */
    private final int f3035a;

    /* renamed from: b  reason: collision with root package name */
    private final a f3036b;

    /* compiled from: DiskLruCacheFactory */
    public interface a {
        File a();
    }

    public d(a aVar, int i) {
        this.f3035a = i;
        this.f3036b = aVar;
    }

    public a a() {
        File a2 = this.f3036b.a();
        if (a2 == null) {
            return null;
        }
        if (a2.mkdirs() || (a2.exists() && a2.isDirectory())) {
            return e.a(a2, this.f3035a);
        }
        return null;
    }
}
