package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.a.c;

/* compiled from: VideoBitmapDecoder */
public class l implements a<ParcelFileDescriptor> {

    /* renamed from: a  reason: collision with root package name */
    private static final a f3183a = new a();

    /* renamed from: b  reason: collision with root package name */
    private a f3184b;

    /* renamed from: c  reason: collision with root package name */
    private int f3185c;

    public l() {
        this(f3183a, -1);
    }

    l(a aVar, int i) {
        this.f3184b = aVar;
        this.f3185c = i;
    }

    public Bitmap a(ParcelFileDescriptor parcelFileDescriptor, c cVar, int i, int i2, DecodeFormat decodeFormat) {
        Bitmap frameAtTime;
        MediaMetadataRetriever a2 = this.f3184b.a();
        a2.setDataSource(parcelFileDescriptor.getFileDescriptor());
        if (this.f3185c >= 0) {
            frameAtTime = a2.getFrameAtTime((long) this.f3185c);
        } else {
            frameAtTime = a2.getFrameAtTime();
        }
        a2.release();
        parcelFileDescriptor.close();
        return frameAtTime;
    }

    public String a() {
        return "VideoBitmapDecoder.com.bumptech.glide.load.resource.bitmap";
    }

    /* compiled from: VideoBitmapDecoder */
    static class a {
        a() {
        }

        public MediaMetadataRetriever a() {
            return new MediaMetadataRetriever();
        }
    }
}
