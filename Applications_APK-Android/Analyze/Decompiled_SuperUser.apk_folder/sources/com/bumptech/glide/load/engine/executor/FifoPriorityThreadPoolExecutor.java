package com.bumptech.glide.load.engine.executor;

import android.os.Process;
import android.util.Log;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class FifoPriorityThreadPoolExecutor extends ThreadPoolExecutor {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f3053a;

    /* renamed from: b  reason: collision with root package name */
    private final UncaughtThrowableStrategy f3054b;

    public enum UncaughtThrowableStrategy {
        IGNORE,
        LOG {
            /* access modifiers changed from: protected */
            public void a(Throwable th) {
                if (Log.isLoggable("PriorityExecutor", 6)) {
                    Log.e("PriorityExecutor", "Request threw uncaught throwable", th);
                }
            }
        },
        THROW {
            /* access modifiers changed from: protected */
            public void a(Throwable th) {
                super.a(th);
                throw new RuntimeException(th);
            }
        };

        /* access modifiers changed from: protected */
        public void a(Throwable th) {
        }
    }

    public FifoPriorityThreadPoolExecutor(int i) {
        this(i, UncaughtThrowableStrategy.LOG);
    }

    public FifoPriorityThreadPoolExecutor(int i, UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        this(i, i, 0, TimeUnit.MILLISECONDS, new a(), uncaughtThrowableStrategy);
    }

    public FifoPriorityThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, ThreadFactory threadFactory, UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        super(i, i2, j, timeUnit, new PriorityBlockingQueue(), threadFactory);
        this.f3053a = new AtomicInteger();
        this.f3054b = uncaughtThrowableStrategy;
    }

    /* access modifiers changed from: protected */
    public <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new b(runnable, t, this.f3053a.getAndIncrement());
    }

    /* access modifiers changed from: protected */
    public void afterExecute(Runnable runnable, Throwable th) {
        super.afterExecute(runnable, th);
        if (th == null && (runnable instanceof Future)) {
            Future future = (Future) runnable;
            if (future.isDone() && !future.isCancelled()) {
                try {
                    future.get();
                } catch (InterruptedException e2) {
                    this.f3054b.a(e2);
                } catch (ExecutionException e3) {
                    this.f3054b.a(e3);
                }
            }
        }
    }

    public static class a implements ThreadFactory {

        /* renamed from: a  reason: collision with root package name */
        int f3059a = 0;

        public Thread newThread(Runnable runnable) {
            AnonymousClass1 r0 = new Thread(runnable, "fifo-pool-thread-" + this.f3059a) {
                public void run() {
                    Process.setThreadPriority(10);
                    super.run();
                }
            };
            this.f3059a++;
            return r0;
        }
    }

    static class b<T> extends FutureTask<T> implements Comparable<b<?>> {

        /* renamed from: a  reason: collision with root package name */
        private final int f3061a;

        /* renamed from: b  reason: collision with root package name */
        private final int f3062b;

        public b(Runnable runnable, T t, int i) {
            super(runnable, t);
            if (!(runnable instanceof a)) {
                throw new IllegalArgumentException("FifoPriorityThreadPoolExecutor must be given Runnables that implement Prioritized");
            }
            this.f3061a = ((a) runnable).b();
            this.f3062b = i;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.f3062b == bVar.f3062b && this.f3061a == bVar.f3061a) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (this.f3061a * 31) + this.f3062b;
        }

        /* renamed from: a */
        public int compareTo(b<?> bVar) {
            int i = this.f3061a - bVar.f3061a;
            if (i == 0) {
                return this.f3062b - bVar.f3062b;
            }
            return i;
        }
    }
}
