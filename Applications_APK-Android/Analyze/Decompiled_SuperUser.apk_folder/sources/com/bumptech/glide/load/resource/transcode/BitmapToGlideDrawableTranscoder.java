package com.bumptech.glide.load.resource.transcode;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.resource.a.b;

public class BitmapToGlideDrawableTranscoder implements b<Bitmap, b> {

    /* renamed from: a  reason: collision with root package name */
    private final GlideBitmapDrawableTranscoder f3261a;

    public BitmapToGlideDrawableTranscoder(Context context) {
        this(new GlideBitmapDrawableTranscoder(context));
    }

    public BitmapToGlideDrawableTranscoder(GlideBitmapDrawableTranscoder glideBitmapDrawableTranscoder) {
        this.f3261a = glideBitmapDrawableTranscoder;
    }

    public i<b> a(i<Bitmap> iVar) {
        return this.f3261a.a(iVar);
    }

    public String a() {
        return this.f3261a.a();
    }
}
