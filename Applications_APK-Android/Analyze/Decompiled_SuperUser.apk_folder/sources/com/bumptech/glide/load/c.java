package com.bumptech.glide.load;

import com.bumptech.glide.load.engine.i;
import java.util.Arrays;
import java.util.Collection;

/* compiled from: MultiTransformation */
public class c<T> implements f<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Collection<? extends f<T>> f2932a;

    /* renamed from: b  reason: collision with root package name */
    private String f2933b;

    @SafeVarargs
    public c(f<T>... fVarArr) {
        if (fVarArr.length < 1) {
            throw new IllegalArgumentException("MultiTransformation must contain at least one Transformation");
        }
        this.f2932a = Arrays.asList(fVarArr);
    }

    public i<T> a(i<T> iVar, int i, int i2) {
        i<T> iVar2 = iVar;
        for (f a2 : this.f2932a) {
            i<T> a3 = a2.a(iVar2, i, i2);
            if (iVar2 != null && !iVar2.equals(iVar) && !iVar2.equals(a3)) {
                iVar2.d();
            }
            iVar2 = a3;
        }
        return iVar2;
    }

    public String a() {
        if (this.f2933b == null) {
            StringBuilder sb = new StringBuilder();
            for (f a2 : this.f2932a) {
                sb.append(a2.a());
            }
            this.f2933b = sb.toString();
        }
        return this.f2933b;
    }
}
