package com.bumptech.glide.load.model;

import android.net.Uri;
import android.text.TextUtils;
import java.net.URL;
import java.util.Map;

/* compiled from: GlideUrl */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private final URL f3079a;

    /* renamed from: b  reason: collision with root package name */
    private final d f3080b;

    /* renamed from: c  reason: collision with root package name */
    private final String f3081c;

    /* renamed from: d  reason: collision with root package name */
    private String f3082d;

    /* renamed from: e  reason: collision with root package name */
    private URL f3083e;

    public c(URL url) {
        this(url, d.f3085b);
    }

    public c(String str) {
        this(str, d.f3085b);
    }

    public c(URL url, d dVar) {
        if (url == null) {
            throw new IllegalArgumentException("URL must not be null!");
        } else if (dVar == null) {
            throw new IllegalArgumentException("Headers must not be null");
        } else {
            this.f3079a = url;
            this.f3081c = null;
            this.f3080b = dVar;
        }
    }

    public c(String str, d dVar) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("String url must not be empty or null: " + str);
        } else if (dVar == null) {
            throw new IllegalArgumentException("Headers must not be null");
        } else {
            this.f3081c = str;
            this.f3079a = null;
            this.f3080b = dVar;
        }
    }

    public URL a() {
        return d();
    }

    private URL d() {
        if (this.f3083e == null) {
            this.f3083e = new URL(e());
        }
        return this.f3083e;
    }

    private String e() {
        if (TextUtils.isEmpty(this.f3082d)) {
            String str = this.f3081c;
            if (TextUtils.isEmpty(str)) {
                str = this.f3079a.toString();
            }
            this.f3082d = Uri.encode(str, "@#&=*+-_.,:!?()/~'%");
        }
        return this.f3082d;
    }

    public Map<String, String> b() {
        return this.f3080b.a();
    }

    public String c() {
        return this.f3081c != null ? this.f3081c : this.f3079a.toString();
    }

    public String toString() {
        return c() + 10 + this.f3080b.toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (!c().equals(cVar.c()) || !this.f3080b.equals(cVar.f3080b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (c().hashCode() * 31) + this.f3080b.hashCode();
    }
}
