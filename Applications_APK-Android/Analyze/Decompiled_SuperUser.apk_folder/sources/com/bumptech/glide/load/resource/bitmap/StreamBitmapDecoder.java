package com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.e;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import java.io.InputStream;

public class StreamBitmapDecoder implements d<InputStream, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final d f3145a;

    /* renamed from: b  reason: collision with root package name */
    private c f3146b;

    /* renamed from: c  reason: collision with root package name */
    private DecodeFormat f3147c;

    /* renamed from: d  reason: collision with root package name */
    private String f3148d;

    public StreamBitmapDecoder(Context context) {
        this(e.a(context).a());
    }

    public StreamBitmapDecoder(c cVar) {
        this(cVar, DecodeFormat.f2916d);
    }

    public StreamBitmapDecoder(c cVar, DecodeFormat decodeFormat) {
        this(d.f3153a, cVar, decodeFormat);
    }

    public StreamBitmapDecoder(d dVar, c cVar, DecodeFormat decodeFormat) {
        this.f3145a = dVar;
        this.f3146b = cVar;
        this.f3147c = decodeFormat;
    }

    public i<Bitmap> a(InputStream inputStream, int i, int i2) {
        return c.a(this.f3145a.a(inputStream, this.f3146b, i, i2, this.f3147c), this.f3146b);
    }

    public String a() {
        if (this.f3148d == null) {
            this.f3148d = "StreamBitmapDecoder.com.bumptech.glide.load.resource.bitmap" + this.f3145a.a() + this.f3147c.name();
        }
        return this.f3148d;
    }
}
