package com.bumptech.glide.load.model.stream;

import android.content.Context;
import com.bumptech.glide.load.a.f;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.c;
import com.bumptech.glide.load.model.j;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.model.l;
import java.io.InputStream;

/* compiled from: HttpUrlGlideUrlLoader */
public class a implements k<c, InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final j<c, c> f3118a;

    /* renamed from: com.bumptech.glide.load.model.stream.a$a  reason: collision with other inner class name */
    /* compiled from: HttpUrlGlideUrlLoader */
    public static class C0041a implements l<c, InputStream> {

        /* renamed from: a  reason: collision with root package name */
        private final j<c, c> f3119a = new j<>(500);

        public k<c, InputStream> a(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new a(this.f3119a);
        }

        public void a() {
        }
    }

    public a() {
        this(null);
    }

    public a(j<c, c> jVar) {
        this.f3118a = jVar;
    }

    public com.bumptech.glide.load.a.c<InputStream> a(c cVar, int i, int i2) {
        if (this.f3118a != null) {
            c a2 = this.f3118a.a(cVar, 0, 0);
            if (a2 == null) {
                this.f3118a.a(cVar, 0, 0, cVar);
            } else {
                cVar = a2;
            }
        }
        return new f(cVar);
    }
}
