package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.model.f;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: ImageVideoBitmapDecoder */
public class h implements d<f, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final d<InputStream, Bitmap> f3173a;

    /* renamed from: b  reason: collision with root package name */
    private final d<ParcelFileDescriptor, Bitmap> f3174b;

    public h(d<InputStream, Bitmap> dVar, d<ParcelFileDescriptor, Bitmap> dVar2) {
        this.f3173a = dVar;
        this.f3174b = dVar2;
    }

    public i<Bitmap> a(f fVar, int i, int i2) {
        i<Bitmap> iVar;
        ParcelFileDescriptor b2;
        InputStream a2 = fVar.a();
        if (a2 != null) {
            try {
                iVar = this.f3173a.a(a2, i, i2);
            } catch (IOException e2) {
                if (Log.isLoggable("ImageVideoDecoder", 2)) {
                    Log.v("ImageVideoDecoder", "Failed to load image from stream, trying FileDescriptor", e2);
                }
            }
            if (iVar != null && (b2 = fVar.b()) != null) {
                return this.f3174b.a(b2, i, i2);
            }
        }
        iVar = null;
        return iVar != null ? iVar : iVar;
    }

    public String a() {
        return "ImageVideoBitmapDecoder.com.bumptech.glide.load.resource.bitmap";
    }
}
