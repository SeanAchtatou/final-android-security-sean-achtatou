package com.bumptech.glide.load.engine.b;

import android.os.Handler;
import android.os.Looper;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.cache.g;

/* compiled from: BitmapPreFiller */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final g f3003a;

    /* renamed from: b  reason: collision with root package name */
    private final c f3004b;

    /* renamed from: c  reason: collision with root package name */
    private final DecodeFormat f3005c;

    /* renamed from: d  reason: collision with root package name */
    private final Handler f3006d = new Handler(Looper.getMainLooper());

    public a(g gVar, c cVar, DecodeFormat decodeFormat) {
        this.f3003a = gVar;
        this.f3004b = cVar;
        this.f3005c = decodeFormat;
    }
}
