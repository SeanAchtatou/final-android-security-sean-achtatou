package com.bumptech.glide.load.engine.a;

import com.bumptech.glide.load.engine.a.h;
import java.util.Queue;

/* compiled from: BaseKeyPool */
abstract class b<T extends h> {

    /* renamed from: a  reason: collision with root package name */
    private final Queue<T> f2963a = com.bumptech.glide.f.h.a(20);

    /* access modifiers changed from: protected */
    public abstract T b();

    b() {
    }

    /* access modifiers changed from: protected */
    public T c() {
        T t = (h) this.f2963a.poll();
        if (t == null) {
            return b();
        }
        return t;
    }

    public void a(T t) {
        if (this.f2963a.size() < 20) {
            this.f2963a.offer(t);
        }
    }
}
