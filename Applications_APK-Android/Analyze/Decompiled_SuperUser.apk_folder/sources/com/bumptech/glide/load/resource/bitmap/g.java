package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.f.h;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.resource.a.a;

/* compiled from: GlideBitmapDrawableResource */
public class g extends a<f> {

    /* renamed from: b  reason: collision with root package name */
    private final c f3172b;

    public g(f fVar, c cVar) {
        super(fVar);
        this.f3172b = cVar;
    }

    public int c() {
        return h.a(((f) this.f3122a).b());
    }

    public void d() {
        this.f3172b.a(((f) this.f3122a).b());
    }
}
