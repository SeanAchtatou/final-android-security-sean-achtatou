package com.bumptech.glide.load.resource.c;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.i;

/* compiled from: GifBitmapWrapperResource */
public class b implements i<a> {

    /* renamed from: a  reason: collision with root package name */
    private final a f3189a;

    public b(a aVar) {
        if (aVar == null) {
            throw new NullPointerException("Data must not be null");
        }
        this.f3189a = aVar;
    }

    /* renamed from: a */
    public a b() {
        return this.f3189a;
    }

    public int c() {
        return this.f3189a.a();
    }

    public void d() {
        i<Bitmap> b2 = this.f3189a.b();
        if (b2 != null) {
            b2.d();
        }
        i<com.bumptech.glide.load.resource.gif.b> c2 = this.f3189a.c();
        if (c2 != null) {
            c2.d();
        }
    }
}
