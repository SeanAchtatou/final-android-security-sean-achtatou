package com.bumptech.glide.load.engine;

import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.d;

class EngineRunnable implements com.bumptech.glide.load.engine.executor.a, Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Priority f2941a;

    /* renamed from: b  reason: collision with root package name */
    private final a f2942b;

    /* renamed from: c  reason: collision with root package name */
    private final a<?, ?, ?> f2943c;

    /* renamed from: d  reason: collision with root package name */
    private Stage f2944d = Stage.CACHE;

    /* renamed from: e  reason: collision with root package name */
    private volatile boolean f2945e;

    private enum Stage {
        CACHE,
        SOURCE
    }

    interface a extends d {
        void b(EngineRunnable engineRunnable);
    }

    public EngineRunnable(a aVar, a<?, ?, ?> aVar2, Priority priority) {
        this.f2942b = aVar;
        this.f2943c = aVar2;
        this.f2941a = priority;
    }

    public void a() {
        this.f2945e = true;
        this.f2943c.d();
    }

    public void run() {
        i<?> iVar;
        Exception exc = null;
        if (!this.f2945e) {
            try {
                iVar = d();
            } catch (Exception e2) {
                if (Log.isLoggable("EngineRunnable", 2)) {
                    Log.v("EngineRunnable", "Exception decoding", e2);
                }
                exc = e2;
                iVar = null;
            }
            if (this.f2945e) {
                if (iVar != null) {
                    iVar.d();
                }
            } else if (iVar == null) {
                a(exc);
            } else {
                a(iVar);
            }
        }
    }

    private boolean c() {
        return this.f2944d == Stage.CACHE;
    }

    private void a(i iVar) {
        this.f2942b.a(iVar);
    }

    private void a(Exception exc) {
        if (c()) {
            this.f2944d = Stage.SOURCE;
            this.f2942b.b(this);
            return;
        }
        this.f2942b.a(exc);
    }

    private i<?> d() {
        if (c()) {
            return e();
        }
        return f();
    }

    private i<?> e() {
        i<?> iVar;
        try {
            iVar = this.f2943c.a();
        } catch (Exception e2) {
            if (Log.isLoggable("EngineRunnable", 3)) {
                Log.d("EngineRunnable", "Exception decoding result from cache: " + e2);
            }
            iVar = null;
        }
        if (iVar == null) {
            return this.f2943c.b();
        }
        return iVar;
    }

    private i<?> f() {
        return this.f2943c.c();
    }

    public int b() {
        return this.f2941a.ordinal();
    }
}
