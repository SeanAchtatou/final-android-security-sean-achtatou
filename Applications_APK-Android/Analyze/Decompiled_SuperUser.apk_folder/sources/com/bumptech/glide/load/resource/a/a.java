package com.bumptech.glide.load.resource.a;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.engine.i;

/* compiled from: DrawableResource */
public abstract class a<T extends Drawable> implements i<T> {

    /* renamed from: a  reason: collision with root package name */
    protected final T f3122a;

    public a(T t) {
        if (t == null) {
            throw new NullPointerException("Drawable must not be null!");
        }
        this.f3122a = t;
    }

    /* renamed from: a */
    public final T b() {
        return this.f3122a.getConstantState().newDrawable();
    }
}
