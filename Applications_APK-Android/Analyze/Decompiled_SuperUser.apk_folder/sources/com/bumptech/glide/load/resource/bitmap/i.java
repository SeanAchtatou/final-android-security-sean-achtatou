package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.d.b;
import com.bumptech.glide.load.a;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.e;
import com.bumptech.glide.load.model.f;
import com.bumptech.glide.load.model.g;
import java.io.File;
import java.io.InputStream;

/* compiled from: ImageVideoDataLoadProvider */
public class i implements b<f, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final h f3175a;

    /* renamed from: b  reason: collision with root package name */
    private final d<File, Bitmap> f3176b;

    /* renamed from: c  reason: collision with root package name */
    private final e<Bitmap> f3177c;

    /* renamed from: d  reason: collision with root package name */
    private final g f3178d;

    public i(b<InputStream, Bitmap> bVar, b<ParcelFileDescriptor, Bitmap> bVar2) {
        this.f3177c = bVar.d();
        this.f3178d = new g(bVar.c(), bVar2.c());
        this.f3176b = bVar.a();
        this.f3175a = new h(bVar.b(), bVar2.b());
    }

    public d<File, Bitmap> a() {
        return this.f3176b;
    }

    public d<f, Bitmap> b() {
        return this.f3175a;
    }

    public a<f> c() {
        return this.f3178d;
    }

    public e<Bitmap> d() {
        return this.f3177c;
    }
}
