package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.d.b;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.a;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.engine.a.c;
import java.io.File;

/* compiled from: FileDescriptorBitmapDataLoadProvider */
public class e implements b<ParcelFileDescriptor, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final d<File, Bitmap> f3158a;

    /* renamed from: b  reason: collision with root package name */
    private final FileDescriptorBitmapDecoder f3159b;

    /* renamed from: c  reason: collision with root package name */
    private final b f3160c = new b();

    /* renamed from: d  reason: collision with root package name */
    private final a<ParcelFileDescriptor> f3161d = com.bumptech.glide.load.resource.a.b();

    public e(c cVar, DecodeFormat decodeFormat) {
        this.f3158a = new com.bumptech.glide.load.resource.b.c(new StreamBitmapDecoder(cVar, decodeFormat));
        this.f3159b = new FileDescriptorBitmapDecoder(cVar, decodeFormat);
    }

    public d<File, Bitmap> a() {
        return this.f3158a;
    }

    public d<ParcelFileDescriptor, Bitmap> b() {
        return this.f3159b;
    }

    public a<ParcelFileDescriptor> c() {
        return this.f3161d;
    }

    public com.bumptech.glide.load.e<Bitmap> d() {
        return this.f3160c;
    }
}
