package com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.a.c;

public class FitCenter extends BitmapTransformation {
    public FitCenter(Context context) {
        super(context);
    }

    public FitCenter(c cVar) {
        super(cVar);
    }

    /* access modifiers changed from: protected */
    public Bitmap a(c cVar, Bitmap bitmap, int i, int i2) {
        return k.a(bitmap, cVar, i, i2);
    }

    public String a() {
        return "FitCenter.com.bumptech.glide.load.resource.bitmap";
    }
}
