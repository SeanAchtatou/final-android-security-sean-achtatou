package com.bumptech.glide.load.engine;

public enum DiskCacheStrategy {
    ALL(true, true),
    NONE(false, false),
    SOURCE(true, false),
    RESULT(false, true);
    

    /* renamed from: e  reason: collision with root package name */
    private final boolean f2939e;

    /* renamed from: f  reason: collision with root package name */
    private final boolean f2940f;

    private DiskCacheStrategy(boolean z, boolean z2) {
        this.f2939e = z;
        this.f2940f = z2;
    }

    public boolean a() {
        return this.f2939e;
    }

    public boolean b() {
        return this.f2940f;
    }
}
