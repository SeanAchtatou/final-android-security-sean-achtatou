package com.bumptech.glide.load.a;

import android.content.res.AssetManager;
import android.util.Log;
import com.bumptech.glide.Priority;
import java.io.IOException;

/* compiled from: AssetPathFetcher */
public abstract class a<T> implements c<T> {

    /* renamed from: a  reason: collision with root package name */
    private final String f2918a;

    /* renamed from: b  reason: collision with root package name */
    private final AssetManager f2919b;

    /* renamed from: c  reason: collision with root package name */
    private T f2920c;

    /* access modifiers changed from: protected */
    public abstract T a(AssetManager assetManager, String str);

    /* access modifiers changed from: protected */
    public abstract void a(T t);

    public a(AssetManager assetManager, String str) {
        this.f2919b = assetManager;
        this.f2918a = str;
    }

    public T a(Priority priority) {
        this.f2920c = a(this.f2919b, this.f2918a);
        return this.f2920c;
    }

    public void a() {
        if (this.f2920c != null) {
            try {
                a((Object) this.f2920c);
            } catch (IOException e2) {
                if (Log.isLoggable("AssetUriFetcher", 2)) {
                    Log.v("AssetUriFetcher", "Failed to close data", e2);
                }
            }
        }
    }

    public String b() {
        return this.f2918a;
    }

    public void c() {
    }
}
