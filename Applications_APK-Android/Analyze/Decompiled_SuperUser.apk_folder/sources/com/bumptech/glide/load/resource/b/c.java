package com.bumptech.glide.load.resource.b;

import com.bumptech.glide.load.d;
import com.bumptech.glide.load.engine.i;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: FileToStreamDecoder */
public class c<T> implements d<File, T> {

    /* renamed from: a  reason: collision with root package name */
    private static final a f3124a = new a();

    /* renamed from: b  reason: collision with root package name */
    private d<InputStream, T> f3125b;

    /* renamed from: c  reason: collision with root package name */
    private final a f3126c;

    public c(d<InputStream, T> dVar) {
        this(dVar, f3124a);
    }

    c(d<InputStream, T> dVar, a aVar) {
        this.f3125b = dVar;
        this.f3126c = aVar;
    }

    public i<T> a(File file, int i, int i2) {
        InputStream inputStream = null;
        try {
            inputStream = this.f3126c.a(file);
            return this.f3125b.a(inputStream, i, i2);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    public String a() {
        return "";
    }

    /* compiled from: FileToStreamDecoder */
    static class a {
        a() {
        }

        public InputStream a(File file) {
            return new FileInputStream(file);
        }
    }
}
