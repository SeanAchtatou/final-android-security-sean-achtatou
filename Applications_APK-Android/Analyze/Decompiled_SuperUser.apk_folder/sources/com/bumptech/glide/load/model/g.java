package com.bumptech.glide.load.model;

import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.a;
import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: ImageVideoWrapperEncoder */
public class g implements a<f> {

    /* renamed from: a  reason: collision with root package name */
    private final a<InputStream> f3092a;

    /* renamed from: b  reason: collision with root package name */
    private final a<ParcelFileDescriptor> f3093b;

    /* renamed from: c  reason: collision with root package name */
    private String f3094c;

    public g(a<InputStream> aVar, a<ParcelFileDescriptor> aVar2) {
        this.f3092a = aVar;
        this.f3093b = aVar2;
    }

    public boolean a(f fVar, OutputStream outputStream) {
        if (fVar.a() != null) {
            return this.f3092a.a(fVar.a(), outputStream);
        }
        return this.f3093b.a(fVar.b(), outputStream);
    }

    public String a() {
        if (this.f3094c == null) {
            this.f3094c = this.f3092a.a() + this.f3093b.a();
        }
        return this.f3094c;
    }
}
