package com.bumptech.glide.load.model;

import android.net.Uri;
import com.bumptech.glide.load.a.c;
import java.io.File;

/* compiled from: FileLoader */
public class b<T> implements k<File, T> {

    /* renamed from: a  reason: collision with root package name */
    private final k<Uri, T> f3078a;

    public b(k<Uri, T> kVar) {
        this.f3078a = kVar;
    }

    public c<T> a(File file, int i, int i2) {
        return this.f3078a.a(Uri.fromFile(file), i, i2);
    }
}
