package com.bumptech.glide.load.a;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.bumptech.glide.Priority;
import java.io.IOException;

/* compiled from: LocalUriFetcher */
public abstract class g<T> implements c<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Uri f2929a;

    /* renamed from: b  reason: collision with root package name */
    private final Context f2930b;

    /* renamed from: c  reason: collision with root package name */
    private T f2931c;

    /* access modifiers changed from: protected */
    public abstract void a(Object obj);

    /* access modifiers changed from: protected */
    public abstract T b(Uri uri, ContentResolver contentResolver);

    public g(Context context, Uri uri) {
        this.f2930b = context.getApplicationContext();
        this.f2929a = uri;
    }

    public final T a(Priority priority) {
        this.f2931c = b(this.f2929a, this.f2930b.getContentResolver());
        return this.f2931c;
    }

    public void a() {
        if (this.f2931c != null) {
            try {
                a((Object) this.f2931c);
            } catch (IOException e2) {
                if (Log.isLoggable("LocalUriFetcher", 2)) {
                    Log.v("LocalUriFetcher", "failed to close data", e2);
                }
            }
        }
    }

    public void c() {
    }

    public String b() {
        return this.f2929a.toString();
    }
}
