package com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.e;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;

public class FileDescriptorBitmapDecoder implements d<ParcelFileDescriptor, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final l f3131a;

    /* renamed from: b  reason: collision with root package name */
    private final c f3132b;

    /* renamed from: c  reason: collision with root package name */
    private DecodeFormat f3133c;

    public FileDescriptorBitmapDecoder(Context context) {
        this(e.a(context).a(), DecodeFormat.f2916d);
    }

    public FileDescriptorBitmapDecoder(c cVar, DecodeFormat decodeFormat) {
        this(new l(), cVar, decodeFormat);
    }

    public FileDescriptorBitmapDecoder(l lVar, c cVar, DecodeFormat decodeFormat) {
        this.f3131a = lVar;
        this.f3132b = cVar;
        this.f3133c = decodeFormat;
    }

    public i<Bitmap> a(ParcelFileDescriptor parcelFileDescriptor, int i, int i2) {
        return c.a(this.f3131a.a(parcelFileDescriptor, this.f3132b, i, i2, this.f3133c), this.f3132b);
    }

    public String a() {
        return "FileDescriptorBitmapDecoder.com.bumptech.glide.load.data.bitmap";
    }
}
