package com.bumptech.glide.load.engine.cache;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.helper.utils.FileUtils;

public class MemorySizeCalculator {

    /* renamed from: a  reason: collision with root package name */
    private final int f3026a;

    /* renamed from: b  reason: collision with root package name */
    private final int f3027b;

    /* renamed from: c  reason: collision with root package name */
    private final Context f3028c;

    interface b {
        int a();

        int b();
    }

    public MemorySizeCalculator(Context context) {
        this(context, (ActivityManager) context.getSystemService(ServiceManagerNative.ACTIVITY), new a(context.getResources().getDisplayMetrics()));
    }

    MemorySizeCalculator(Context context, ActivityManager activityManager, b bVar) {
        this.f3028c = context;
        int a2 = a(activityManager);
        int a3 = bVar.a() * bVar.b() * 4;
        int i = a3 * 4;
        int i2 = a3 * 2;
        if (i2 + i <= a2) {
            this.f3027b = i2;
            this.f3026a = i;
        } else {
            int round = Math.round(((float) a2) / 6.0f);
            this.f3027b = round * 2;
            this.f3026a = round * 4;
        }
        if (Log.isLoggable("MemorySizeCalculator", 3)) {
            Log.d("MemorySizeCalculator", "Calculated memory cache size: " + a(this.f3027b) + " pool size: " + a(this.f3026a) + " memory class limited? " + (i2 + i > a2) + " max size: " + a(a2) + " memoryClass: " + activityManager.getMemoryClass() + " isLowMemoryDevice: " + b(activityManager));
        }
    }

    public int a() {
        return this.f3027b;
    }

    public int b() {
        return this.f3026a;
    }

    private static int a(ActivityManager activityManager) {
        return Math.round((b(activityManager) ? 0.33f : 0.4f) * ((float) (activityManager.getMemoryClass() * FileUtils.FileMode.MODE_ISGID * FileUtils.FileMode.MODE_ISGID)));
    }

    private String a(int i) {
        return Formatter.formatFileSize(this.f3028c, (long) i);
    }

    @TargetApi(19)
    private static boolean b(ActivityManager activityManager) {
        if (Build.VERSION.SDK_INT >= 19) {
            return activityManager.isLowRamDevice();
        }
        return Build.VERSION.SDK_INT < 11;
    }

    private static class a implements b {

        /* renamed from: a  reason: collision with root package name */
        private final DisplayMetrics f3029a;

        public a(DisplayMetrics displayMetrics) {
            this.f3029a = displayMetrics;
        }

        public int a() {
            return this.f3029a.widthPixels;
        }

        public int b() {
            return this.f3029a.heightPixels;
        }
    }
}
