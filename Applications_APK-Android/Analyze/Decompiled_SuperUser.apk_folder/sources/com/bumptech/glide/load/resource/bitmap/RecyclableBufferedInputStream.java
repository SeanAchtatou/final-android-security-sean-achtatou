package com.bumptech.glide.load.resource.bitmap;

import android.util.Log;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class RecyclableBufferedInputStream extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private volatile byte[] f3140a;

    /* renamed from: b  reason: collision with root package name */
    private int f3141b;

    /* renamed from: c  reason: collision with root package name */
    private int f3142c;

    /* renamed from: d  reason: collision with root package name */
    private int f3143d = -1;

    /* renamed from: e  reason: collision with root package name */
    private int f3144e;

    public RecyclableBufferedInputStream(InputStream inputStream, byte[] bArr) {
        super(inputStream);
        if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("buffer is null or empty");
        }
        this.f3140a = bArr;
    }

    public synchronized int available() {
        InputStream inputStream;
        inputStream = this.in;
        if (this.f3140a == null || inputStream == null) {
            throw b();
        }
        return inputStream.available() + (this.f3141b - this.f3144e);
    }

    private static IOException b() {
        throw new IOException("BufferedInputStream is closed");
    }

    public synchronized void a() {
        this.f3142c = this.f3140a.length;
    }

    public void close() {
        this.f3140a = null;
        InputStream inputStream = this.in;
        this.in = null;
        if (inputStream != null) {
            inputStream.close();
        }
    }

    private int a(InputStream inputStream, byte[] bArr) {
        if (this.f3143d == -1 || this.f3144e - this.f3143d >= this.f3142c) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                return read;
            }
            this.f3143d = -1;
            this.f3144e = 0;
            this.f3141b = read;
            return read;
        }
        if (this.f3143d == 0 && this.f3142c > bArr.length && this.f3141b == bArr.length) {
            int length = bArr.length * 2;
            if (length > this.f3142c) {
                length = this.f3142c;
            }
            if (Log.isLoggable("BufferedIs", 3)) {
                Log.d("BufferedIs", "allocate buffer of length: " + length);
            }
            byte[] bArr2 = new byte[length];
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            this.f3140a = bArr2;
            bArr = bArr2;
        } else if (this.f3143d > 0) {
            System.arraycopy(bArr, this.f3143d, bArr, 0, bArr.length - this.f3143d);
        }
        this.f3144e -= this.f3143d;
        this.f3143d = 0;
        this.f3141b = 0;
        int read2 = inputStream.read(bArr, this.f3144e, bArr.length - this.f3144e);
        this.f3141b = read2 <= 0 ? this.f3144e : this.f3144e + read2;
        return read2;
    }

    public synchronized void mark(int i) {
        this.f3142c = Math.max(this.f3142c, i);
        this.f3143d = this.f3144e;
    }

    public boolean markSupported() {
        return true;
    }

    public synchronized int read() {
        byte b2 = -1;
        synchronized (this) {
            byte[] bArr = this.f3140a;
            InputStream inputStream = this.in;
            if (bArr == null || inputStream == null) {
                throw b();
            } else if (this.f3144e < this.f3141b || a(inputStream, bArr) != -1) {
                if (bArr != this.f3140a && (bArr = this.f3140a) == null) {
                    throw b();
                } else if (this.f3141b - this.f3144e > 0) {
                    int i = this.f3144e;
                    this.f3144e = i + 1;
                    b2 = bArr[i] & 255;
                }
            }
        }
        return b2;
    }

    public synchronized int read(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5 = -1;
        synchronized (this) {
            byte[] bArr2 = this.f3140a;
            if (bArr2 == null) {
                throw b();
            } else if (i2 == 0) {
                i5 = 0;
            } else {
                InputStream inputStream = this.in;
                if (inputStream == null) {
                    throw b();
                }
                if (this.f3144e < this.f3141b) {
                    int i6 = this.f3141b - this.f3144e >= i2 ? i2 : this.f3141b - this.f3144e;
                    System.arraycopy(bArr2, this.f3144e, bArr, i, i6);
                    this.f3144e += i6;
                    if (i6 == i2 || inputStream.available() == 0) {
                        i5 = i6;
                    } else {
                        i += i6;
                        i3 = i2 - i6;
                    }
                } else {
                    i3 = i2;
                }
                while (true) {
                    if (this.f3143d == -1 && i3 >= bArr2.length) {
                        i4 = inputStream.read(bArr, i, i3);
                        if (i4 == -1) {
                            if (i3 != i2) {
                                i5 = i2 - i3;
                            }
                        }
                    } else if (a(inputStream, bArr2) == -1) {
                        if (i3 != i2) {
                            i5 = i2 - i3;
                        }
                    } else if (bArr2 == this.f3140a || (bArr2 = this.f3140a) != null) {
                        i4 = this.f3141b - this.f3144e >= i3 ? i3 : this.f3141b - this.f3144e;
                        System.arraycopy(bArr2, this.f3144e, bArr, i, i4);
                        this.f3144e += i4;
                    } else {
                        throw b();
                    }
                    i3 -= i4;
                    if (i3 == 0) {
                        i5 = i2;
                        break;
                    } else if (inputStream.available() == 0) {
                        i5 = i2 - i3;
                        break;
                    } else {
                        i += i4;
                    }
                }
            }
        }
        return i5;
    }

    public synchronized void reset() {
        if (this.f3140a == null) {
            throw new IOException("Stream is closed");
        } else if (-1 == this.f3143d) {
            throw new InvalidMarkException("Mark has been invalidated");
        } else {
            this.f3144e = this.f3143d;
        }
    }

    public synchronized long skip(long j) {
        byte[] bArr = this.f3140a;
        InputStream inputStream = this.in;
        if (bArr == null) {
            throw b();
        } else if (j < 1) {
            j = 0;
        } else if (inputStream == null) {
            throw b();
        } else if (((long) (this.f3141b - this.f3144e)) >= j) {
            this.f3144e = (int) (((long) this.f3144e) + j);
        } else {
            long j2 = (long) (this.f3141b - this.f3144e);
            this.f3144e = this.f3141b;
            if (this.f3143d == -1 || j > ((long) this.f3142c)) {
                j = j2 + inputStream.skip(j - j2);
            } else if (a(inputStream, bArr) == -1) {
                j = j2;
            } else if (((long) (this.f3141b - this.f3144e)) >= j - j2) {
                this.f3144e = (int) ((j - j2) + ((long) this.f3144e));
            } else {
                j = (j2 + ((long) this.f3141b)) - ((long) this.f3144e);
                this.f3144e = this.f3141b;
            }
        }
        return j;
    }

    public static class InvalidMarkException extends RuntimeException {
        public InvalidMarkException(String str) {
            super(str);
        }
    }
}
