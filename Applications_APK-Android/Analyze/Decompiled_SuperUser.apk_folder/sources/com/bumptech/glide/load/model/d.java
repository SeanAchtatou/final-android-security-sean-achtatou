package com.bumptech.glide.load.model;

import com.bumptech.glide.load.model.i;
import java.util.Collections;
import java.util.Map;

/* compiled from: Headers */
public interface d {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    public static final d f3084a = new d() {
        public Map<String, String> a() {
            return Collections.emptyMap();
        }
    };

    /* renamed from: b  reason: collision with root package name */
    public static final d f3085b = new i.a().a();

    Map<String, String> a();
}
