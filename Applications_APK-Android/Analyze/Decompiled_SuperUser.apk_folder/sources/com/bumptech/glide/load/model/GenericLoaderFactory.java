package com.bumptech.glide.load.model;

import android.content.Context;
import com.bumptech.glide.load.a.c;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GenericLoaderFactory {

    /* renamed from: c  reason: collision with root package name */
    private static final k f3073c = new k() {
        public c a(Object obj, int i, int i2) {
            throw new NoSuchMethodError("This should never be called!");
        }

        public String toString() {
            return "NULL_MODEL_LOADER";
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Map<Class, Map<Class, l>> f3074a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    private final Map<Class, Map<Class, k>> f3075b = new HashMap();

    /* renamed from: d  reason: collision with root package name */
    private final Context f3076d;

    public GenericLoaderFactory(Context context) {
        this.f3076d = context.getApplicationContext();
    }

    public synchronized <T, Y> l<T, Y> a(Class cls, Class cls2, l lVar) {
        l<T, Y> lVar2;
        this.f3075b.clear();
        Object obj = this.f3074a.get(cls);
        if (obj == null) {
            obj = new HashMap();
            this.f3074a.put(cls, obj);
        }
        lVar2 = (l) obj.put(cls2, lVar);
        if (lVar2 != null) {
            Iterator<Map<Class, l>> it = this.f3074a.values().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().containsValue(lVar2)) {
                        lVar2 = null;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return lVar2;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.Class<Y>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized <T, Y> com.bumptech.glide.load.model.k<T, Y> a(java.lang.Class<T> r3, java.lang.Class<Y> r4) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.bumptech.glide.load.model.k r0 = r2.c(r3, r4)     // Catch:{ all -> 0x0022 }
            if (r0 == 0) goto L_0x0012
            com.bumptech.glide.load.model.k r1 = com.bumptech.glide.load.model.GenericLoaderFactory.f3073c     // Catch:{ all -> 0x0022 }
            boolean r1 = r1.equals(r0)     // Catch:{ all -> 0x0022 }
            if (r1 == 0) goto L_0x0010
            r0 = 0
        L_0x0010:
            monitor-exit(r2)
            return r0
        L_0x0012:
            com.bumptech.glide.load.model.l r1 = r2.d(r3, r4)     // Catch:{ all -> 0x0022 }
            if (r1 == 0) goto L_0x0025
            android.content.Context r0 = r2.f3076d     // Catch:{ all -> 0x0022 }
            com.bumptech.glide.load.model.k r0 = r1.a(r0, r2)     // Catch:{ all -> 0x0022 }
            r2.a(r3, r4, r0)     // Catch:{ all -> 0x0022 }
            goto L_0x0010
        L_0x0022:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0025:
            r2.b(r3, r4)     // Catch:{ all -> 0x0022 }
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.model.GenericLoaderFactory.a(java.lang.Class, java.lang.Class):com.bumptech.glide.load.model.k");
    }

    private <T, Y> void b(Class<T> cls, Class<Y> cls2) {
        a(cls, cls2, f3073c);
    }

    private <T, Y> void a(Class cls, Class cls2, k kVar) {
        Object obj = this.f3075b.get(cls);
        if (obj == null) {
            obj = new HashMap();
            this.f3075b.put(cls, obj);
        }
        obj.put(cls2, kVar);
    }

    private <T, Y> k<T, Y> c(Class<T> cls, Class<Y> cls2) {
        Map map = this.f3075b.get(cls);
        if (map != null) {
            return (k) map.get(cls2);
        }
        return null;
    }

    private <T, Y> l<T, Y> d(Class<T> cls, Class<Y> cls2) {
        l<T, Y> lVar;
        l<T, Y> lVar2;
        Map map;
        Map map2 = this.f3074a.get(cls);
        if (map2 != null) {
            lVar = (l) map2.get(cls2);
        } else {
            lVar = null;
        }
        if (lVar2 != null) {
            return lVar2;
        }
        Iterator<Class> it = this.f3074a.keySet().iterator();
        while (true) {
            l<T, Y> lVar3 = lVar2;
            if (!it.hasNext()) {
                return lVar3;
            }
            Class next = it.next();
            if (!next.isAssignableFrom(cls) || (map = this.f3074a.get(next)) == null) {
                lVar2 = lVar3;
            } else {
                lVar2 = (l) map.get(cls2);
                if (lVar2 != null) {
                    return lVar2;
                }
            }
        }
    }
}
