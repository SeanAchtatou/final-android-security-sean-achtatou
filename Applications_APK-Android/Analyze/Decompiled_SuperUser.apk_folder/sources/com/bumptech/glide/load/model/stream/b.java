package com.bumptech.glide.load.model.stream;

import android.content.Context;
import com.bumptech.glide.load.a.c;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.model.l;
import java.io.InputStream;

/* compiled from: StreamByteArrayLoader */
public class b implements c<byte[]> {

    /* renamed from: a  reason: collision with root package name */
    private final String f3120a;

    public b() {
        this("");
    }

    @Deprecated
    public b(String str) {
        this.f3120a = str;
    }

    public c<InputStream> a(byte[] bArr, int i, int i2) {
        return new com.bumptech.glide.load.a.b(bArr, this.f3120a);
    }

    /* compiled from: StreamByteArrayLoader */
    public static class a implements l<byte[], InputStream> {
        public k<byte[], InputStream> a(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new b();
        }

        public void a() {
        }
    }
}
