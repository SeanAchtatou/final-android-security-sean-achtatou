package com.bumptech.glide.load.resource.transcode;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.resource.a.b;
import com.bumptech.glide.load.resource.bitmap.f;

/* compiled from: GifBitmapWrapperDrawableTranscoder */
public class a implements b<com.bumptech.glide.load.resource.c.a, b> {

    /* renamed from: a  reason: collision with root package name */
    private final b<Bitmap, f> f3264a;

    public a(b<Bitmap, f> bVar) {
        this.f3264a = bVar;
    }

    public i<b> a(i<com.bumptech.glide.load.resource.c.a> iVar) {
        com.bumptech.glide.load.resource.c.a b2 = iVar.b();
        i<Bitmap> b3 = b2.b();
        if (b3 != null) {
            return this.f3264a.a(b3);
        }
        return b2.c();
    }

    public String a() {
        return "GifBitmapWrapperDrawableTranscoder.com.bumptech.glide.load.resource.transcode";
    }
}
