package com.bumptech.glide.load.model.stream;

import android.content.Context;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.c;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.model.l;
import com.bumptech.glide.load.model.q;
import java.io.InputStream;
import java.net.URL;

/* compiled from: StreamUrlLoader */
public class d extends q<InputStream> {

    /* compiled from: StreamUrlLoader */
    public static class a implements l<URL, InputStream> {
        public k<URL, InputStream> a(Context context, GenericLoaderFactory genericLoaderFactory) {
            return new d(genericLoaderFactory.a(c.class, InputStream.class));
        }

        public void a() {
        }
    }

    public d(k<c, InputStream> kVar) {
        super(kVar);
    }
}
