package com.bumptech.glide.load.resource.c;

import android.graphics.Bitmap;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.model.f;
import com.bumptech.glide.load.resource.bitmap.ImageHeaderParser;
import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.InputStream;

/* compiled from: GifBitmapWrapperResourceDecoder */
public class c implements d<f, a> {

    /* renamed from: a  reason: collision with root package name */
    private static final b f3190a = new b();

    /* renamed from: b  reason: collision with root package name */
    private static final a f3191b = new a();

    /* renamed from: c  reason: collision with root package name */
    private final d<f, Bitmap> f3192c;

    /* renamed from: d  reason: collision with root package name */
    private final d<InputStream, com.bumptech.glide.load.resource.gif.b> f3193d;

    /* renamed from: e  reason: collision with root package name */
    private final com.bumptech.glide.load.engine.a.c f3194e;

    /* renamed from: f  reason: collision with root package name */
    private final b f3195f;

    /* renamed from: g  reason: collision with root package name */
    private final a f3196g;

    /* renamed from: h  reason: collision with root package name */
    private String f3197h;

    public c(d<f, Bitmap> dVar, d<InputStream, com.bumptech.glide.load.resource.gif.b> dVar2, com.bumptech.glide.load.engine.a.c cVar) {
        this(dVar, dVar2, cVar, f3190a, f3191b);
    }

    c(d<f, Bitmap> dVar, d<InputStream, com.bumptech.glide.load.resource.gif.b> dVar2, com.bumptech.glide.load.engine.a.c cVar, b bVar, a aVar) {
        this.f3192c = dVar;
        this.f3193d = dVar2;
        this.f3194e = cVar;
        this.f3195f = bVar;
        this.f3196g = aVar;
    }

    public i<a> a(f fVar, int i, int i2) {
        com.bumptech.glide.f.a a2 = com.bumptech.glide.f.a.a();
        byte[] b2 = a2.b();
        try {
            a a3 = a(fVar, i, i2, b2);
            if (a3 != null) {
                return new b(a3);
            }
            return null;
        } finally {
            a2.a(b2);
        }
    }

    private a a(f fVar, int i, int i2, byte[] bArr) {
        if (fVar.a() != null) {
            return b(fVar, i, i2, bArr);
        }
        return b(fVar, i, i2);
    }

    private a b(f fVar, int i, int i2, byte[] bArr) {
        InputStream a2 = this.f3196g.a(fVar.a(), bArr);
        a2.mark(FileUtils.FileMode.MODE_ISUID);
        ImageHeaderParser.ImageType a3 = this.f3195f.a(a2);
        a2.reset();
        a aVar = null;
        if (a3 == ImageHeaderParser.ImageType.GIF) {
            aVar = a(a2, i, i2);
        }
        if (aVar == null) {
            return b(new f(a2, fVar.b()), i, i2);
        }
        return aVar;
    }

    private a a(InputStream inputStream, int i, int i2) {
        i<com.bumptech.glide.load.resource.gif.b> a2 = this.f3193d.a(inputStream, i, i2);
        if (a2 == null) {
            return null;
        }
        com.bumptech.glide.load.resource.gif.b b2 = a2.b();
        if (b2.e() > 1) {
            return new a(null, a2);
        }
        return new a(new com.bumptech.glide.load.resource.bitmap.c(b2.b(), this.f3194e), null);
    }

    private a b(f fVar, int i, int i2) {
        i<Bitmap> a2 = this.f3192c.a(fVar, i, i2);
        if (a2 != null) {
            return new a(a2, null);
        }
        return null;
    }

    public String a() {
        if (this.f3197h == null) {
            this.f3197h = this.f3193d.a() + this.f3192c.a();
        }
        return this.f3197h;
    }

    /* compiled from: GifBitmapWrapperResourceDecoder */
    static class a {
        a() {
        }

        public InputStream a(InputStream inputStream, byte[] bArr) {
            return new RecyclableBufferedInputStream(inputStream, bArr);
        }
    }

    /* compiled from: GifBitmapWrapperResourceDecoder */
    static class b {
        b() {
        }

        public ImageHeaderParser.ImageType a(InputStream inputStream) {
            return new ImageHeaderParser(inputStream).b();
        }
    }
}
