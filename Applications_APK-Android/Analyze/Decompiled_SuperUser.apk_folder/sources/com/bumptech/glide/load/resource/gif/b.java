package com.bumptech.glide.load.resource.gif;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Gravity;
import com.bumptech.glide.b.a;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.resource.gif.f;

/* compiled from: GifDrawable */
public class b extends com.bumptech.glide.load.resource.a.b implements f.b {

    /* renamed from: a  reason: collision with root package name */
    private final Paint f3219a;

    /* renamed from: b  reason: collision with root package name */
    private final Rect f3220b;

    /* renamed from: c  reason: collision with root package name */
    private final a f3221c;

    /* renamed from: d  reason: collision with root package name */
    private final com.bumptech.glide.b.a f3222d;

    /* renamed from: e  reason: collision with root package name */
    private final f f3223e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f3224f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f3225g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f3226h;
    private boolean i;
    private int j;
    private int k;
    private boolean l;

    public b(Context context, a.C0036a aVar, c cVar, com.bumptech.glide.load.f<Bitmap> fVar, int i2, int i3, com.bumptech.glide.b.c cVar2, byte[] bArr, Bitmap bitmap) {
        this(new a(cVar2, bArr, context, fVar, i2, i3, aVar, cVar, bitmap));
    }

    public b(b bVar, Bitmap bitmap, com.bumptech.glide.load.f<Bitmap> fVar) {
        this(new a(bVar.f3221c.f3227a, bVar.f3221c.f3228b, bVar.f3221c.f3229c, fVar, bVar.f3221c.f3231e, bVar.f3221c.f3232f, bVar.f3221c.f3233g, bVar.f3221c.f3234h, bitmap));
    }

    b(a aVar) {
        this.f3220b = new Rect();
        this.i = true;
        this.k = -1;
        if (aVar == null) {
            throw new NullPointerException("GifState must not be null");
        }
        this.f3221c = aVar;
        this.f3222d = new com.bumptech.glide.b.a(aVar.f3233g);
        this.f3219a = new Paint();
        this.f3222d.a(aVar.f3227a, aVar.f3228b);
        this.f3223e = new f(aVar.f3229c, this, this.f3222d, aVar.f3231e, aVar.f3232f);
        this.f3223e.a(aVar.f3230d);
    }

    public Bitmap b() {
        return this.f3221c.i;
    }

    public com.bumptech.glide.load.f<Bitmap> c() {
        return this.f3221c.f3230d;
    }

    public byte[] d() {
        return this.f3221c.f3228b;
    }

    public int e() {
        return this.f3222d.c();
    }

    private void g() {
        this.j = 0;
    }

    public void start() {
        this.f3225g = true;
        g();
        if (this.i) {
            i();
        }
    }

    public void stop() {
        this.f3225g = false;
        j();
        if (Build.VERSION.SDK_INT < 11) {
            h();
        }
    }

    private void h() {
        this.f3223e.c();
        invalidateSelf();
    }

    private void i() {
        if (this.f3222d.c() == 1) {
            invalidateSelf();
        } else if (!this.f3224f) {
            this.f3224f = true;
            this.f3223e.a();
            invalidateSelf();
        }
    }

    private void j() {
        this.f3224f = false;
        this.f3223e.b();
    }

    public boolean setVisible(boolean z, boolean z2) {
        this.i = z;
        if (!z) {
            j();
        } else if (this.f3225g) {
            i();
        }
        return super.setVisible(z, z2);
    }

    public int getIntrinsicWidth() {
        return this.f3221c.i.getWidth();
    }

    public int getIntrinsicHeight() {
        return this.f3221c.i.getHeight();
    }

    public boolean isRunning() {
        return this.f3224f;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.l = true;
    }

    public void draw(Canvas canvas) {
        if (!this.f3226h) {
            if (this.l) {
                Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), this.f3220b);
                this.l = false;
            }
            Bitmap d2 = this.f3223e.d();
            if (d2 == null) {
                d2 = this.f3221c.i;
            }
            canvas.drawBitmap(d2, (Rect) null, this.f3220b, this.f3219a);
        }
    }

    public void setAlpha(int i2) {
        this.f3219a.setAlpha(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f3219a.setColorFilter(colorFilter);
    }

    public int getOpacity() {
        return -2;
    }

    @TargetApi(11)
    public void b(int i2) {
        if (Build.VERSION.SDK_INT < 11 || getCallback() != null) {
            invalidateSelf();
            if (i2 == this.f3222d.c() - 1) {
                this.j++;
            }
            if (this.k != -1 && this.j >= this.k) {
                stop();
                return;
            }
            return;
        }
        stop();
        h();
    }

    public Drawable.ConstantState getConstantState() {
        return this.f3221c;
    }

    public void f() {
        this.f3226h = true;
        this.f3221c.f3234h.a(this.f3221c.i);
        this.f3223e.c();
        this.f3223e.b();
    }

    public boolean a() {
        return true;
    }

    public void a(int i2) {
        if (i2 <= 0 && i2 != -1 && i2 != 0) {
            throw new IllegalArgumentException("Loop count must be greater than 0, or equal to GlideDrawable.LOOP_FOREVER, or equal to GlideDrawable.LOOP_INTRINSIC");
        } else if (i2 == 0) {
            this.k = this.f3222d.e();
        } else {
            this.k = i2;
        }
    }

    /* compiled from: GifDrawable */
    static class a extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        com.bumptech.glide.b.c f3227a;

        /* renamed from: b  reason: collision with root package name */
        byte[] f3228b;

        /* renamed from: c  reason: collision with root package name */
        Context f3229c;

        /* renamed from: d  reason: collision with root package name */
        com.bumptech.glide.load.f<Bitmap> f3230d;

        /* renamed from: e  reason: collision with root package name */
        int f3231e;

        /* renamed from: f  reason: collision with root package name */
        int f3232f;

        /* renamed from: g  reason: collision with root package name */
        a.C0036a f3233g;

        /* renamed from: h  reason: collision with root package name */
        c f3234h;
        Bitmap i;

        public a(com.bumptech.glide.b.c cVar, byte[] bArr, Context context, com.bumptech.glide.load.f<Bitmap> fVar, int i2, int i3, a.C0036a aVar, c cVar2, Bitmap bitmap) {
            if (bitmap == null) {
                throw new NullPointerException("The first frame of the GIF must not be null");
            }
            this.f3227a = cVar;
            this.f3228b = bArr;
            this.f3234h = cVar2;
            this.i = bitmap;
            this.f3229c = context.getApplicationContext();
            this.f3230d = fVar;
            this.f3231e = i2;
            this.f3232f = i3;
            this.f3233g = aVar;
        }

        public Drawable newDrawable(Resources resources) {
            return newDrawable();
        }

        public Drawable newDrawable() {
            return new b(this);
        }

        public int getChangingConfigurations() {
            return 0;
        }
    }
}
