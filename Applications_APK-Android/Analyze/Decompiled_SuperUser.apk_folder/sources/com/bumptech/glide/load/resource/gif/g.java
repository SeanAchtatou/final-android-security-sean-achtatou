package com.bumptech.glide.load.resource.gif;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.a.c;
import com.bumptech.glide.load.model.k;

/* compiled from: GifFrameModelLoader */
class g implements k<com.bumptech.glide.b.a, com.bumptech.glide.b.a> {
    g() {
    }

    public c<com.bumptech.glide.b.a> a(com.bumptech.glide.b.a aVar, int i, int i2) {
        return new a(aVar);
    }

    /* compiled from: GifFrameModelLoader */
    private static class a implements c<com.bumptech.glide.b.a> {

        /* renamed from: a  reason: collision with root package name */
        private final com.bumptech.glide.b.a f3255a;

        public a(com.bumptech.glide.b.a aVar) {
            this.f3255a = aVar;
        }

        /* renamed from: b */
        public com.bumptech.glide.b.a a(Priority priority) {
            return this.f3255a;
        }

        public void a() {
        }

        public String b() {
            return String.valueOf(this.f3255a.d());
        }

        public void c() {
        }
    }
}
