package com.bumptech.glide.load.resource;

import com.bumptech.glide.load.engine.i;

/* compiled from: SimpleResource */
public class c<T> implements i<T> {

    /* renamed from: a  reason: collision with root package name */
    protected final T f3186a;

    public c(T t) {
        if (t == null) {
            throw new NullPointerException("Data must not be null");
        }
        this.f3186a = t;
    }

    public final T b() {
        return this.f3186a;
    }

    public final int c() {
        return 1;
    }

    public void d() {
    }
}
