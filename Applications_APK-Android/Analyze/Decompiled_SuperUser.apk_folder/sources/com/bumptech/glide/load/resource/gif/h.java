package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import com.bumptech.glide.b.a;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;

/* compiled from: GifFrameResourceDecoder */
class h implements d<a, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final c f3256a;

    public h(c cVar) {
        this.f3256a = cVar;
    }

    public i<Bitmap> a(a aVar, int i, int i2) {
        return com.bumptech.glide.load.resource.bitmap.c.a(aVar.f(), this.f3256a);
    }

    public String a() {
        return "GifFrameResourceDecoder.com.bumptech.glide.load.resource.gif";
    }
}
