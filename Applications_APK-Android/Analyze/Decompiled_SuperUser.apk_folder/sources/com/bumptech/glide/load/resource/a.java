package com.bumptech.glide.load.resource;

import java.io.OutputStream;

/* compiled from: NullEncoder */
public class a<T> implements com.bumptech.glide.load.a<T> {

    /* renamed from: a  reason: collision with root package name */
    private static final a<?> f3121a = new a<>();

    public static <T> com.bumptech.glide.load.a<T> b() {
        return f3121a;
    }

    public boolean a(T t, OutputStream outputStream) {
        return false;
    }

    public String a() {
        return "";
    }
}
