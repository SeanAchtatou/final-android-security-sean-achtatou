package com.bumptech.glide.load.model;

import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.a.c;
import java.io.InputStream;

/* compiled from: ImageVideoModelLoader */
public class e<A> implements k<A, f> {

    /* renamed from: a  reason: collision with root package name */
    private final k<A, InputStream> f3086a;

    /* renamed from: b  reason: collision with root package name */
    private final k<A, ParcelFileDescriptor> f3087b;

    public e(k<A, InputStream> kVar, k<A, ParcelFileDescriptor> kVar2) {
        if (kVar == null && kVar2 == null) {
            throw new NullPointerException("At least one of streamLoader and fileDescriptorLoader must be non null");
        }
        this.f3086a = kVar;
        this.f3087b = kVar2;
    }

    public c<f> a(A a2, int i, int i2) {
        c<InputStream> cVar;
        c<ParcelFileDescriptor> cVar2;
        if (this.f3086a != null) {
            cVar = this.f3086a.a(a2, i, i2);
        } else {
            cVar = null;
        }
        if (this.f3087b != null) {
            cVar2 = this.f3087b.a(a2, i, i2);
        } else {
            cVar2 = null;
        }
        if (cVar == null && cVar2 == null) {
            return null;
        }
        return new a(cVar, cVar2);
    }

    /* compiled from: ImageVideoModelLoader */
    static class a implements c<f> {

        /* renamed from: a  reason: collision with root package name */
        private final c<InputStream> f3088a;

        /* renamed from: b  reason: collision with root package name */
        private final c<ParcelFileDescriptor> f3089b;

        public a(c<InputStream> cVar, c<ParcelFileDescriptor> cVar2) {
            this.f3088a = cVar;
            this.f3089b = cVar2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:7:0x0013 A[SYNTHETIC, Splitter:B:7:0x0013] */
        /* renamed from: b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.bumptech.glide.load.model.f a(com.bumptech.glide.Priority r6) {
            /*
                r5 = this;
                r2 = 0
                r4 = 2
                com.bumptech.glide.load.a.c<java.io.InputStream> r0 = r5.f3088a
                if (r0 == 0) goto L_0x004a
                com.bumptech.glide.load.a.c<java.io.InputStream> r0 = r5.f3088a     // Catch:{ Exception -> 0x0022 }
                java.lang.Object r0 = r0.a(r6)     // Catch:{ Exception -> 0x0022 }
                java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ Exception -> 0x0022 }
                r1 = r0
            L_0x000f:
                com.bumptech.glide.load.a.c<android.os.ParcelFileDescriptor> r0 = r5.f3089b
                if (r0 == 0) goto L_0x001c
                com.bumptech.glide.load.a.c<android.os.ParcelFileDescriptor> r0 = r5.f3089b     // Catch:{ Exception -> 0x0037 }
                java.lang.Object r0 = r0.a(r6)     // Catch:{ Exception -> 0x0037 }
                android.os.ParcelFileDescriptor r0 = (android.os.ParcelFileDescriptor) r0     // Catch:{ Exception -> 0x0037 }
                r2 = r0
            L_0x001c:
                com.bumptech.glide.load.model.f r0 = new com.bumptech.glide.load.model.f
                r0.<init>(r1, r2)
                return r0
            L_0x0022:
                r0 = move-exception
                java.lang.String r1 = "IVML"
                boolean r1 = android.util.Log.isLoggable(r1, r4)
                if (r1 == 0) goto L_0x0032
                java.lang.String r1 = "IVML"
                java.lang.String r3 = "Exception fetching input stream, trying ParcelFileDescriptor"
                android.util.Log.v(r1, r3, r0)
            L_0x0032:
                com.bumptech.glide.load.a.c<android.os.ParcelFileDescriptor> r1 = r5.f3089b
                if (r1 != 0) goto L_0x004a
                throw r0
            L_0x0037:
                r0 = move-exception
                java.lang.String r3 = "IVML"
                boolean r3 = android.util.Log.isLoggable(r3, r4)
                if (r3 == 0) goto L_0x0047
                java.lang.String r3 = "IVML"
                java.lang.String r4 = "Exception fetching ParcelFileDescriptor"
                android.util.Log.v(r3, r4, r0)
            L_0x0047:
                if (r1 != 0) goto L_0x001c
                throw r0
            L_0x004a:
                r1 = r2
                goto L_0x000f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.model.e.a.a(com.bumptech.glide.Priority):com.bumptech.glide.load.model.f");
        }

        public void a() {
            if (this.f3088a != null) {
                this.f3088a.a();
            }
            if (this.f3089b != null) {
                this.f3089b.a();
            }
        }

        public String b() {
            if (this.f3088a != null) {
                return this.f3088a.b();
            }
            return this.f3089b.b();
        }

        public void c() {
            if (this.f3088a != null) {
                this.f3088a.c();
            }
            if (this.f3089b != null) {
                this.f3089b.c();
            }
        }
    }
}
