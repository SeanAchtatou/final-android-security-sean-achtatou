package com.bumptech.glide.load.resource.transcode;

import com.bumptech.glide.f.g;
import java.util.HashMap;
import java.util.Map;

/* compiled from: TranscoderRegistry */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final g f3265a = new g();

    /* renamed from: b  reason: collision with root package name */
    private final Map<g, b<?, ?>> f3266b = new HashMap();

    public <Z, R> void a(Class<Z> cls, Class<R> cls2, b<Z, R> bVar) {
        this.f3266b.put(new g(cls, cls2), bVar);
    }

    public <Z, R> b<Z, R> a(Class<Z> cls, Class<R> cls2) {
        b<Z, R> bVar;
        if (cls.equals(cls2)) {
            return d.b();
        }
        synchronized (f3265a) {
            f3265a.a(cls, cls2);
            bVar = this.f3266b.get(f3265a);
        }
        if (bVar != null) {
            return bVar;
        }
        throw new IllegalArgumentException("No transcoder registered for " + cls + " and " + cls2);
    }
}
