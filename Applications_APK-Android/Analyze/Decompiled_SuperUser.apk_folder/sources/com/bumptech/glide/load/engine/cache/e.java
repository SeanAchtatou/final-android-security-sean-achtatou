package com.bumptech.glide.load.engine.cache;

import android.util.Log;
import com.bumptech.glide.a.a;
import com.bumptech.glide.load.b;
import com.bumptech.glide.load.engine.cache.a;
import java.io.File;
import java.io.IOException;

/* compiled from: DiskLruCacheWrapper */
public class e implements a {

    /* renamed from: a  reason: collision with root package name */
    private static e f3037a = null;

    /* renamed from: b  reason: collision with root package name */
    private final c f3038b = new c();

    /* renamed from: c  reason: collision with root package name */
    private final h f3039c;

    /* renamed from: d  reason: collision with root package name */
    private final File f3040d;

    /* renamed from: e  reason: collision with root package name */
    private final int f3041e;

    /* renamed from: f  reason: collision with root package name */
    private a f3042f;

    public static synchronized a a(File file, int i) {
        e eVar;
        synchronized (e.class) {
            if (f3037a == null) {
                f3037a = new e(file, i);
            }
            eVar = f3037a;
        }
        return eVar;
    }

    protected e(File file, int i) {
        this.f3040d = file;
        this.f3041e = i;
        this.f3039c = new h();
    }

    private synchronized a a() {
        if (this.f3042f == null) {
            this.f3042f = a.a(this.f3040d, 1, 1, (long) this.f3041e);
        }
        return this.f3042f;
    }

    public File a(b bVar) {
        try {
            a.c a2 = a().a(this.f3039c.a(bVar));
            if (a2 != null) {
                return a2.a(0);
            }
            return null;
        } catch (IOException e2) {
            if (!Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e2);
            return null;
        }
    }

    public void a(b bVar, a.b bVar2) {
        a.C0035a b2;
        String a2 = this.f3039c.a(bVar);
        this.f3038b.a(bVar);
        try {
            b2 = a().b(a2);
            if (b2 != null) {
                if (bVar2.a(b2.a(0))) {
                    b2.a();
                }
                b2.c();
            }
            this.f3038b.b(bVar);
        } catch (IOException e2) {
            try {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e2);
                }
            } finally {
                this.f3038b.b(bVar);
            }
        } catch (Throwable th) {
            b2.c();
            throw th;
        }
    }

    public void b(b bVar) {
        try {
            a().c(this.f3039c.a(bVar));
        } catch (IOException e2) {
            if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                Log.w("DiskLruCacheWrapper", "Unable to delete from disk cache", e2);
            }
        }
    }
}
