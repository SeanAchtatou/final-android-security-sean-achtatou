package com.bumptech.glide.load.resource.bitmap;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import com.bumptech.glide.load.resource.a.b;

/* compiled from: GlideBitmapDrawable */
public class f extends b {

    /* renamed from: a  reason: collision with root package name */
    private final Rect f3162a;

    /* renamed from: b  reason: collision with root package name */
    private int f3163b;

    /* renamed from: c  reason: collision with root package name */
    private int f3164c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f3165d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f3166e;

    /* renamed from: f  reason: collision with root package name */
    private a f3167f;

    public f(Resources resources, Bitmap bitmap) {
        this(resources, new a(bitmap));
    }

    f(Resources resources, a aVar) {
        int i;
        this.f3162a = new Rect();
        if (aVar == null) {
            throw new NullPointerException("BitmapState must not be null");
        }
        this.f3167f = aVar;
        if (resources != null) {
            i = resources.getDisplayMetrics().densityDpi;
            i = i == 0 ? 160 : i;
            aVar.f3170b = i;
        } else {
            i = aVar.f3170b;
        }
        this.f3163b = aVar.f3169a.getScaledWidth(i);
        this.f3164c = aVar.f3169a.getScaledHeight(i);
    }

    public int getIntrinsicWidth() {
        return this.f3163b;
    }

    public int getIntrinsicHeight() {
        return this.f3164c;
    }

    public boolean a() {
        return false;
    }

    public void a(int i) {
    }

    public void start() {
    }

    public void stop() {
    }

    public boolean isRunning() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.f3165d = true;
    }

    public Drawable.ConstantState getConstantState() {
        return this.f3167f;
    }

    public void draw(Canvas canvas) {
        if (this.f3165d) {
            Gravity.apply(119, this.f3163b, this.f3164c, getBounds(), this.f3162a);
            this.f3165d = false;
        }
        canvas.drawBitmap(this.f3167f.f3169a, (Rect) null, this.f3162a, this.f3167f.f3171c);
    }

    public void setAlpha(int i) {
        if (this.f3167f.f3171c.getAlpha() != i) {
            this.f3167f.a(i);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f3167f.a(colorFilter);
        invalidateSelf();
    }

    public int getOpacity() {
        Bitmap bitmap = this.f3167f.f3169a;
        return (bitmap == null || bitmap.hasAlpha() || this.f3167f.f3171c.getAlpha() < 255) ? -3 : -1;
    }

    public Drawable mutate() {
        if (!this.f3166e && super.mutate() == this) {
            this.f3167f = new a(this.f3167f);
            this.f3166e = true;
        }
        return this;
    }

    public Bitmap b() {
        return this.f3167f.f3169a;
    }

    /* compiled from: GlideBitmapDrawable */
    static class a extends Drawable.ConstantState {

        /* renamed from: d  reason: collision with root package name */
        private static final Paint f3168d = new Paint(6);

        /* renamed from: a  reason: collision with root package name */
        final Bitmap f3169a;

        /* renamed from: b  reason: collision with root package name */
        int f3170b;

        /* renamed from: c  reason: collision with root package name */
        Paint f3171c;

        public a(Bitmap bitmap) {
            this.f3171c = f3168d;
            this.f3169a = bitmap;
        }

        a(a aVar) {
            this(aVar.f3169a);
            this.f3170b = aVar.f3170b;
        }

        /* access modifiers changed from: package-private */
        public void a(ColorFilter colorFilter) {
            a();
            this.f3171c.setColorFilter(colorFilter);
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            a();
            this.f3171c.setAlpha(i);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (f3168d == this.f3171c) {
                this.f3171c = new Paint(6);
            }
        }

        public Drawable newDrawable() {
            return new f((Resources) null, this);
        }

        public Drawable newDrawable(Resources resources) {
            return new f(resources, this);
        }

        public int getChangingConfigurations() {
            return 0;
        }
    }
}
