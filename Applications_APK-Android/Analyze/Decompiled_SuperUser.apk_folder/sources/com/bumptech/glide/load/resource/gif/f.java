package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.b.g;
import java.security.MessageDigest;
import java.util.UUID;

/* compiled from: GifFrameLoader */
class f {

    /* renamed from: a  reason: collision with root package name */
    private final b f3241a;

    /* renamed from: b  reason: collision with root package name */
    private final com.bumptech.glide.b.a f3242b;

    /* renamed from: c  reason: collision with root package name */
    private final Handler f3243c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f3244d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f3245e;

    /* renamed from: f  reason: collision with root package name */
    private com.bumptech.glide.c<com.bumptech.glide.b.a, com.bumptech.glide.b.a, Bitmap, Bitmap> f3246f;

    /* renamed from: g  reason: collision with root package name */
    private a f3247g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f3248h;

    /* compiled from: GifFrameLoader */
    public interface b {
        void b(int i);
    }

    public f(Context context, b bVar, com.bumptech.glide.b.a aVar, int i, int i2) {
        this(bVar, aVar, null, a(context, aVar, i, i2, e.a(context).a()));
    }

    f(b bVar, com.bumptech.glide.b.a aVar, Handler handler, com.bumptech.glide.c<com.bumptech.glide.b.a, com.bumptech.glide.b.a, Bitmap, Bitmap> cVar) {
        this.f3244d = false;
        this.f3245e = false;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new c()) : handler;
        this.f3241a = bVar;
        this.f3242b = aVar;
        this.f3243c = handler;
        this.f3246f = cVar;
    }

    public void a(com.bumptech.glide.load.f<Bitmap> fVar) {
        if (fVar == null) {
            throw new NullPointerException("Transformation must not be null");
        }
        this.f3246f = this.f3246f.b(fVar);
    }

    public void a() {
        if (!this.f3244d) {
            this.f3244d = true;
            this.f3248h = false;
            e();
        }
    }

    public void b() {
        this.f3244d = false;
    }

    public void c() {
        b();
        if (this.f3247g != null) {
            e.a(this.f3247g);
            this.f3247g = null;
        }
        this.f3248h = true;
    }

    public Bitmap d() {
        if (this.f3247g != null) {
            return this.f3247g.a();
        }
        return null;
    }

    private void e() {
        if (this.f3244d && !this.f3245e) {
            this.f3245e = true;
            this.f3242b.a();
            this.f3246f.b((com.bumptech.glide.load.b) new d()).a(new a(this.f3243c, this.f3242b.d(), SystemClock.uptimeMillis() + ((long) this.f3242b.b())));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        if (this.f3248h) {
            this.f3243c.obtainMessage(2, aVar).sendToTarget();
            return;
        }
        a aVar2 = this.f3247g;
        this.f3247g = aVar;
        this.f3241a.b(aVar.f3250b);
        if (aVar2 != null) {
            this.f3243c.obtainMessage(2, aVar2).sendToTarget();
        }
        this.f3245e = false;
        e();
    }

    /* compiled from: GifFrameLoader */
    private class c implements Handler.Callback {
        private c() {
        }

        public boolean handleMessage(Message message) {
            if (message.what == 1) {
                f.this.a((a) message.obj);
                return true;
            }
            if (message.what == 2) {
                e.a((a) message.obj);
            }
            return false;
        }
    }

    /* compiled from: GifFrameLoader */
    static class a extends g<Bitmap> {

        /* renamed from: a  reason: collision with root package name */
        private final Handler f3249a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final int f3250b;

        /* renamed from: c  reason: collision with root package name */
        private final long f3251c;

        /* renamed from: d  reason: collision with root package name */
        private Bitmap f3252d;

        public /* bridge */ /* synthetic */ void a(Object obj, com.bumptech.glide.request.a.c cVar) {
            a((Bitmap) obj, (com.bumptech.glide.request.a.c<? super Bitmap>) cVar);
        }

        public a(Handler handler, int i, long j) {
            this.f3249a = handler;
            this.f3250b = i;
            this.f3251c = j;
        }

        public Bitmap a() {
            return this.f3252d;
        }

        public void a(Bitmap bitmap, com.bumptech.glide.request.a.c<? super Bitmap> cVar) {
            this.f3252d = bitmap;
            this.f3249a.sendMessageAtTime(this.f3249a.obtainMessage(1, this), this.f3251c);
        }
    }

    private static com.bumptech.glide.c<com.bumptech.glide.b.a, com.bumptech.glide.b.a, Bitmap, Bitmap> a(Context context, com.bumptech.glide.b.a aVar, int i, int i2, com.bumptech.glide.load.engine.a.c cVar) {
        h hVar = new h(cVar);
        g gVar = new g();
        return e.b(context).a(gVar, com.bumptech.glide.b.a.class).a(aVar).a(Bitmap.class).b(com.bumptech.glide.load.resource.a.b()).b((com.bumptech.glide.load.d) hVar).b(true).b(DiskCacheStrategy.NONE).b(i, i2);
    }

    /* compiled from: GifFrameLoader */
    static class d implements com.bumptech.glide.load.b {

        /* renamed from: a  reason: collision with root package name */
        private final UUID f3254a;

        public d() {
            this(UUID.randomUUID());
        }

        d(UUID uuid) {
            this.f3254a = uuid;
        }

        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return ((d) obj).f3254a.equals(this.f3254a);
            }
            return false;
        }

        public int hashCode() {
            return this.f3254a.hashCode();
        }

        public void a(MessageDigest messageDigest) {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}
