package com.bumptech.glide.load.engine.a;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.a.a;
import com.bumptech.glide.load.engine.a.h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: GroupedLinkedMap */
class e<K extends h, V> {

    /* renamed from: a  reason: collision with root package name */
    private final a<K, V> f2967a = new a<>();

    /* renamed from: b  reason: collision with root package name */
    private final Map<K, a<K, V>> f2968b = new HashMap();

    e() {
    }

    public void a(K k, V v) {
        a aVar = this.f2968b.get(k);
        if (aVar == null) {
            aVar = new a(k);
            b(aVar);
            this.f2968b.put(k, aVar);
        } else {
            k.a();
        }
        aVar.a((Object) v);
    }

    public V a(a.C0038a aVar) {
        a aVar2 = this.f2968b.get(aVar);
        if (aVar2 == null) {
            aVar2 = new a(aVar);
            this.f2968b.put(aVar, aVar2);
        } else {
            aVar.a();
        }
        a(aVar2);
        return aVar2.a();
    }

    public V a() {
        a<K, V> aVar = this.f2967a.f2970b;
        while (true) {
            a<K, V> aVar2 = aVar;
            if (aVar2.equals(this.f2967a)) {
                return null;
            }
            V a2 = aVar2.a();
            if (a2 != null) {
                return a2;
            }
            d(aVar2);
            this.f2968b.remove(((a) aVar2).f2971c);
            ((h) ((a) aVar2).f2971c).a();
            aVar = aVar2.f2970b;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("GroupedLinkedMap( ");
        boolean z = false;
        for (a<K, V> aVar = this.f2967a.f2969a; !aVar.equals(this.f2967a); aVar = aVar.f2969a) {
            z = true;
            sb.append('{').append(((a) aVar).f2971c).append(':').append(aVar.b()).append("}, ");
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        return sb.append(" )").toString();
    }

    private void a(a<a.C0038a, Bitmap> aVar) {
        d(aVar);
        aVar.f2970b = this.f2967a;
        aVar.f2969a = this.f2967a.f2969a;
        c(aVar);
    }

    private void b(a<K, V> aVar) {
        d(aVar);
        aVar.f2970b = this.f2967a.f2970b;
        aVar.f2969a = this.f2967a;
        c(aVar);
    }

    private static <K, V> void c(a<K, V> aVar) {
        aVar.f2969a.f2970b = aVar;
        aVar.f2970b.f2969a = aVar;
    }

    private static <K, V> void d(a<K, V> aVar) {
        aVar.f2970b.f2969a = aVar.f2969a;
        aVar.f2969a.f2970b = aVar.f2970b;
    }

    /* compiled from: GroupedLinkedMap */
    private static class a<K, V> {

        /* renamed from: a  reason: collision with root package name */
        a<K, V> f2969a;

        /* renamed from: b  reason: collision with root package name */
        a<K, V> f2970b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public final K f2971c;

        /* renamed from: d  reason: collision with root package name */
        private List<V> f2972d;

        public a() {
            this(null);
        }

        public a(K k) {
            this.f2970b = this;
            this.f2969a = this;
            this.f2971c = k;
        }

        public V a() {
            int b2 = b();
            if (b2 > 0) {
                return this.f2972d.remove(b2 - 1);
            }
            return null;
        }

        public int b() {
            if (this.f2972d != null) {
                return this.f2972d.size();
            }
            return 0;
        }

        public void a(V v) {
            if (this.f2972d == null) {
                this.f2972d = new ArrayList();
            }
            this.f2972d.add(v);
        }
    }
}
