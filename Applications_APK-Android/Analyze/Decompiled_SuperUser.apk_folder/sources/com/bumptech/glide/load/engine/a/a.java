package com.bumptech.glide.load.engine.a;

import android.graphics.Bitmap;
import com.bumptech.glide.f.h;

/* compiled from: AttributeStrategy */
class a implements g {

    /* renamed from: a  reason: collision with root package name */
    private final b f2957a = new b();

    /* renamed from: b  reason: collision with root package name */
    private final e<C0038a, Bitmap> f2958b = new e<>();

    a() {
    }

    public void a(Bitmap bitmap) {
        this.f2958b.a(this.f2957a.a(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig()), bitmap);
    }

    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.f2958b.a(this.f2957a.a(i, i2, config));
    }

    public Bitmap a() {
        return this.f2958b.a();
    }

    public String b(Bitmap bitmap) {
        return d(bitmap);
    }

    public String b(int i, int i2, Bitmap.Config config) {
        return d(i, i2, config);
    }

    public int c(Bitmap bitmap) {
        return h.a(bitmap);
    }

    public String toString() {
        return "AttributeStrategy:\n  " + this.f2958b;
    }

    private static String d(Bitmap bitmap) {
        return d(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    }

    /* access modifiers changed from: private */
    public static String d(int i, int i2, Bitmap.Config config) {
        return "[" + i + "x" + i2 + "], " + config;
    }

    /* compiled from: AttributeStrategy */
    static class b extends b<C0038a> {
        b() {
        }

        public C0038a a(int i, int i2, Bitmap.Config config) {
            C0038a aVar = (C0038a) c();
            aVar.a(i, i2, config);
            return aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public C0038a b() {
            return new C0038a(this);
        }
    }

    /* renamed from: com.bumptech.glide.load.engine.a.a$a  reason: collision with other inner class name */
    /* compiled from: AttributeStrategy */
    static class C0038a implements h {

        /* renamed from: a  reason: collision with root package name */
        private final b f2959a;

        /* renamed from: b  reason: collision with root package name */
        private int f2960b;

        /* renamed from: c  reason: collision with root package name */
        private int f2961c;

        /* renamed from: d  reason: collision with root package name */
        private Bitmap.Config f2962d;

        public C0038a(b bVar) {
            this.f2959a = bVar;
        }

        public void a(int i, int i2, Bitmap.Config config) {
            this.f2960b = i;
            this.f2961c = i2;
            this.f2962d = config;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C0038a)) {
                return false;
            }
            C0038a aVar = (C0038a) obj;
            if (this.f2960b == aVar.f2960b && this.f2961c == aVar.f2961c && this.f2962d == aVar.f2962d) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (this.f2962d != null ? this.f2962d.hashCode() : 0) + (((this.f2960b * 31) + this.f2961c) * 31);
        }

        public String toString() {
            return a.d(this.f2960b, this.f2961c, this.f2962d);
        }

        public void a() {
            this.f2959a.a(this);
        }
    }
}
