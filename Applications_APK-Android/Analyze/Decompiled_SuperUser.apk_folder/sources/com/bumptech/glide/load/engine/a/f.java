package com.bumptech.glide.load.engine.a;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: LruBitmapPool */
public class f implements c {

    /* renamed from: a  reason: collision with root package name */
    private static final Bitmap.Config f2973a = Bitmap.Config.ARGB_8888;

    /* renamed from: b  reason: collision with root package name */
    private final g f2974b;

    /* renamed from: c  reason: collision with root package name */
    private final Set<Bitmap.Config> f2975c;

    /* renamed from: d  reason: collision with root package name */
    private final int f2976d;

    /* renamed from: e  reason: collision with root package name */
    private final a f2977e;

    /* renamed from: f  reason: collision with root package name */
    private int f2978f;

    /* renamed from: g  reason: collision with root package name */
    private int f2979g;

    /* renamed from: h  reason: collision with root package name */
    private int f2980h;
    private int i;
    private int j;
    private int k;

    /* compiled from: LruBitmapPool */
    private interface a {
        void a(Bitmap bitmap);

        void b(Bitmap bitmap);
    }

    f(int i2, g gVar, Set<Bitmap.Config> set) {
        this.f2976d = i2;
        this.f2978f = i2;
        this.f2974b = gVar;
        this.f2975c = set;
        this.f2977e = new b();
    }

    public f(int i2) {
        this(i2, e(), f());
    }

    public synchronized boolean a(Bitmap bitmap) {
        boolean z;
        if (bitmap == null) {
            throw new NullPointerException("Bitmap must not be null");
        } else if (!bitmap.isMutable() || this.f2974b.c(bitmap) > this.f2978f || !this.f2975c.contains(bitmap.getConfig())) {
            if (Log.isLoggable("LruBitmapPool", 2)) {
                Log.v("LruBitmapPool", "Reject bitmap from pool, bitmap: " + this.f2974b.b(bitmap) + ", is mutable: " + bitmap.isMutable() + ", is allowed config: " + this.f2975c.contains(bitmap.getConfig()));
            }
            z = false;
        } else {
            int c2 = this.f2974b.c(bitmap);
            this.f2974b.a(bitmap);
            this.f2977e.a(bitmap);
            this.j++;
            this.f2979g = c2 + this.f2979g;
            if (Log.isLoggable("LruBitmapPool", 2)) {
                Log.v("LruBitmapPool", "Put bitmap in pool=" + this.f2974b.b(bitmap));
            }
            c();
            b();
            z = true;
        }
        return z;
    }

    private void b() {
        b(this.f2978f);
    }

    public synchronized Bitmap a(int i2, int i3, Bitmap.Config config) {
        Bitmap b2;
        b2 = b(i2, i3, config);
        if (b2 != null) {
            b2.eraseColor(0);
        }
        return b2;
    }

    @TargetApi(12)
    public synchronized Bitmap b(int i2, int i3, Bitmap.Config config) {
        Bitmap a2;
        a2 = this.f2974b.a(i2, i3, config != null ? config : f2973a);
        if (a2 == null) {
            if (Log.isLoggable("LruBitmapPool", 3)) {
                Log.d("LruBitmapPool", "Missing bitmap=" + this.f2974b.b(i2, i3, config));
            }
            this.i++;
        } else {
            this.f2980h++;
            this.f2979g -= this.f2974b.c(a2);
            this.f2977e.b(a2);
            if (Build.VERSION.SDK_INT >= 12) {
                a2.setHasAlpha(true);
            }
        }
        if (Log.isLoggable("LruBitmapPool", 2)) {
            Log.v("LruBitmapPool", "Get bitmap=" + this.f2974b.b(i2, i3, config));
        }
        c();
        return a2;
    }

    public void a() {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "clearMemory");
        }
        b(0);
    }

    @SuppressLint({"InlinedApi"})
    public void a(int i2) {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "trimMemory, level=" + i2);
        }
        if (i2 >= 60) {
            a();
        } else if (i2 >= 40) {
            b(this.f2978f / 2);
        }
    }

    private synchronized void b(int i2) {
        while (true) {
            if (this.f2979g <= i2) {
                break;
            }
            Bitmap a2 = this.f2974b.a();
            if (a2 == null) {
                if (Log.isLoggable("LruBitmapPool", 5)) {
                    Log.w("LruBitmapPool", "Size mismatch, resetting");
                    d();
                }
                this.f2979g = 0;
            } else {
                this.f2977e.b(a2);
                this.f2979g -= this.f2974b.c(a2);
                a2.recycle();
                this.k++;
                if (Log.isLoggable("LruBitmapPool", 3)) {
                    Log.d("LruBitmapPool", "Evicting bitmap=" + this.f2974b.b(a2));
                }
                c();
            }
        }
    }

    private void c() {
        if (Log.isLoggable("LruBitmapPool", 2)) {
            d();
        }
    }

    private void d() {
        Log.v("LruBitmapPool", "Hits=" + this.f2980h + ", misses=" + this.i + ", puts=" + this.j + ", evictions=" + this.k + ", currentSize=" + this.f2979g + ", maxSize=" + this.f2978f + "\nStrategy=" + this.f2974b);
    }

    private static g e() {
        if (Build.VERSION.SDK_INT >= 19) {
            return new i();
        }
        return new a();
    }

    private static Set<Bitmap.Config> f() {
        HashSet hashSet = new HashSet();
        hashSet.addAll(Arrays.asList(Bitmap.Config.values()));
        if (Build.VERSION.SDK_INT >= 19) {
            hashSet.add(null);
        }
        return Collections.unmodifiableSet(hashSet);
    }

    /* compiled from: LruBitmapPool */
    private static class b implements a {
        private b() {
        }

        public void a(Bitmap bitmap) {
        }

        public void b(Bitmap bitmap) {
        }
    }
}
