package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.b.a;
import com.bumptech.glide.e;
import com.bumptech.glide.f.h;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.engine.a.c;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

public class GifResourceDecoder implements d<InputStream, b> {

    /* renamed from: a  reason: collision with root package name */
    private static final b f3209a = new b();

    /* renamed from: b  reason: collision with root package name */
    private static final a f3210b = new a();

    /* renamed from: c  reason: collision with root package name */
    private final Context f3211c;

    /* renamed from: d  reason: collision with root package name */
    private final b f3212d;

    /* renamed from: e  reason: collision with root package name */
    private final c f3213e;

    /* renamed from: f  reason: collision with root package name */
    private final a f3214f;

    /* renamed from: g  reason: collision with root package name */
    private final a f3215g;

    public GifResourceDecoder(Context context) {
        this(context, e.a(context).a());
    }

    public GifResourceDecoder(Context context, c cVar) {
        this(context, cVar, f3209a, f3210b);
    }

    GifResourceDecoder(Context context, c cVar, b bVar, a aVar) {
        this.f3211c = context;
        this.f3213e = cVar;
        this.f3214f = aVar;
        this.f3215g = new a(cVar);
        this.f3212d = bVar;
    }

    public d a(InputStream inputStream, int i, int i2) {
        byte[] a2 = a(inputStream);
        com.bumptech.glide.b.d a3 = this.f3212d.a(a2);
        com.bumptech.glide.b.a a4 = this.f3214f.a(this.f3215g);
        try {
            return a(a2, i, i2, a3, a4);
        } finally {
            this.f3212d.a(a3);
            this.f3214f.a(a4);
        }
    }

    private d a(byte[] bArr, int i, int i2, com.bumptech.glide.b.d dVar, com.bumptech.glide.b.a aVar) {
        Bitmap a2;
        com.bumptech.glide.b.c b2 = dVar.b();
        if (b2.a() <= 0 || b2.b() != 0 || (a2 = a(aVar, b2, bArr)) == null) {
            return null;
        }
        return new d(new b(this.f3211c, this.f3215g, this.f3213e, com.bumptech.glide.load.resource.d.b(), i, i2, b2, bArr, a2));
    }

    private Bitmap a(com.bumptech.glide.b.a aVar, com.bumptech.glide.b.c cVar, byte[] bArr) {
        aVar.a(cVar, bArr);
        aVar.a();
        return aVar.f();
    }

    public String a() {
        return "";
    }

    private static byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        try {
            byte[] bArr = new byte[16384];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byteArrayOutputStream.flush();
        } catch (IOException e2) {
            Log.w("GifResourceDecoder", "Error reading data from stream", e2);
        }
        return byteArrayOutputStream.toByteArray();
    }

    static class a {

        /* renamed from: a  reason: collision with root package name */
        private final Queue<com.bumptech.glide.b.a> f3216a = h.a(0);

        a() {
        }

        public synchronized com.bumptech.glide.b.a a(a.C0036a aVar) {
            com.bumptech.glide.b.a poll;
            poll = this.f3216a.poll();
            if (poll == null) {
                poll = new com.bumptech.glide.b.a(aVar);
            }
            return poll;
        }

        public synchronized void a(com.bumptech.glide.b.a aVar) {
            aVar.g();
            this.f3216a.offer(aVar);
        }
    }

    static class b {

        /* renamed from: a  reason: collision with root package name */
        private final Queue<com.bumptech.glide.b.d> f3217a = h.a(0);

        b() {
        }

        public synchronized com.bumptech.glide.b.d a(byte[] bArr) {
            com.bumptech.glide.b.d poll;
            poll = this.f3217a.poll();
            if (poll == null) {
                poll = new com.bumptech.glide.b.d();
            }
            return poll.a(bArr);
        }

        public synchronized void a(com.bumptech.glide.b.d dVar) {
            dVar.a();
            this.f3217a.offer(dVar);
        }
    }
}
