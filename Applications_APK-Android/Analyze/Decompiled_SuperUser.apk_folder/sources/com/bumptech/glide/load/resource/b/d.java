package com.bumptech.glide.load.resource.b;

import com.bumptech.glide.d.b;
import com.bumptech.glide.load.e;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.model.n;
import java.io.File;
import java.io.InputStream;

/* compiled from: StreamFileDataLoadProvider */
public class d implements b<InputStream, File> {

    /* renamed from: a  reason: collision with root package name */
    private static final a f3127a = new a();

    /* renamed from: b  reason: collision with root package name */
    private final com.bumptech.glide.load.d<File, File> f3128b = new a();

    /* renamed from: c  reason: collision with root package name */
    private final com.bumptech.glide.load.a<InputStream> f3129c = new n();

    public com.bumptech.glide.load.d<File, File> a() {
        return this.f3128b;
    }

    public com.bumptech.glide.load.d<InputStream, File> b() {
        return f3127a;
    }

    public com.bumptech.glide.load.a<InputStream> c() {
        return this.f3129c;
    }

    public e<File> d() {
        return com.bumptech.glide.load.resource.b.b();
    }

    /* compiled from: StreamFileDataLoadProvider */
    private static class a implements com.bumptech.glide.load.d<InputStream, File> {
        private a() {
        }

        public i<File> a(InputStream inputStream, int i, int i2) {
            throw new Error("You cannot decode a File from an InputStream by default, try either #diskCacheStratey(DiskCacheStrategy.SOURCE) to avoid this call or #decoder(ResourceDecoder) to replace this Decoder");
        }

        public String a() {
            return "";
        }
    }
}
