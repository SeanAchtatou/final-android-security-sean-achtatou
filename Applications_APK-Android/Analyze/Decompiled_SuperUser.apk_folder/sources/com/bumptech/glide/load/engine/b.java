package com.bumptech.glide.load.engine;

import android.os.Looper;
import android.os.MessageQueue;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.f.h;
import com.bumptech.glide.load.engine.a;
import com.bumptech.glide.load.engine.cache.a;
import com.bumptech.glide.load.engine.cache.g;
import com.bumptech.glide.load.engine.g;
import com.bumptech.glide.load.f;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/* compiled from: Engine */
public class b implements g.a, d, g.a {

    /* renamed from: a  reason: collision with root package name */
    private final Map<com.bumptech.glide.load.b, c> f2992a;

    /* renamed from: b  reason: collision with root package name */
    private final f f2993b;

    /* renamed from: c  reason: collision with root package name */
    private final com.bumptech.glide.load.engine.cache.g f2994c;

    /* renamed from: d  reason: collision with root package name */
    private final a f2995d;

    /* renamed from: e  reason: collision with root package name */
    private final Map<com.bumptech.glide.load.b, WeakReference<g<?>>> f2996e;

    /* renamed from: f  reason: collision with root package name */
    private final j f2997f;

    /* renamed from: g  reason: collision with root package name */
    private final C0039b f2998g;

    /* renamed from: h  reason: collision with root package name */
    private ReferenceQueue<g<?>> f2999h;

    /* compiled from: Engine */
    public static class c {

        /* renamed from: a  reason: collision with root package name */
        private final c f3009a;

        /* renamed from: b  reason: collision with root package name */
        private final com.bumptech.glide.request.d f3010b;

        public c(com.bumptech.glide.request.d dVar, c cVar) {
            this.f3010b = dVar;
            this.f3009a = cVar;
        }

        public void a() {
            this.f3009a.b(this.f3010b);
        }
    }

    public b(com.bumptech.glide.load.engine.cache.g gVar, a.C0040a aVar, ExecutorService executorService, ExecutorService executorService2) {
        this(gVar, aVar, executorService, executorService2, null, null, null, null, null);
    }

    b(com.bumptech.glide.load.engine.cache.g gVar, a.C0040a aVar, ExecutorService executorService, ExecutorService executorService2, Map<com.bumptech.glide.load.b, c> map, f fVar, Map<com.bumptech.glide.load.b, WeakReference<g<?>>> map2, a aVar2, j jVar) {
        this.f2994c = gVar;
        this.f2998g = new C0039b(aVar);
        this.f2996e = map2 == null ? new HashMap<>() : map2;
        this.f2993b = fVar == null ? new f() : fVar;
        this.f2992a = map == null ? new HashMap<>() : map;
        this.f2995d = aVar2 == null ? new a(executorService, executorService2, this) : aVar2;
        this.f2997f = jVar == null ? new j() : jVar;
        gVar.a(this);
    }

    public <T, Z, R> c a(com.bumptech.glide.load.b bVar, int i, int i2, com.bumptech.glide.load.a.c<T> cVar, com.bumptech.glide.d.b<T, Z> bVar2, f<Z> fVar, com.bumptech.glide.load.resource.transcode.b<Z, R> bVar3, Priority priority, boolean z, DiskCacheStrategy diskCacheStrategy, com.bumptech.glide.request.d dVar) {
        h.a();
        long a2 = com.bumptech.glide.f.d.a();
        e a3 = this.f2993b.a(cVar.b(), bVar, i, i2, bVar2.a(), bVar2.b(), fVar, bVar2.d(), bVar3, bVar2.c());
        g<?> b2 = b(a3, z);
        if (b2 != null) {
            dVar.a(b2);
            if (Log.isLoggable("Engine", 2)) {
                a("Loaded resource from cache", a2, a3);
            }
            return null;
        }
        g<?> a4 = a(a3, z);
        if (a4 != null) {
            dVar.a(a4);
            if (Log.isLoggable("Engine", 2)) {
                a("Loaded resource from active resources", a2, a3);
            }
            return null;
        }
        c cVar2 = this.f2992a.get(a3);
        if (cVar2 != null) {
            cVar2.a(dVar);
            if (Log.isLoggable("Engine", 2)) {
                a("Added to existing load", a2, a3);
            }
            return new c(dVar, cVar2);
        }
        c a5 = this.f2995d.a(a3, z);
        EngineRunnable engineRunnable = new EngineRunnable(a5, new a(a3, i, i2, cVar, bVar2, fVar, bVar3, this.f2998g, diskCacheStrategy, priority), priority);
        this.f2992a.put(a3, a5);
        a5.a(dVar);
        a5.a(engineRunnable);
        if (Log.isLoggable("Engine", 2)) {
            a("Started new load", a2, a3);
        }
        return new c(dVar, a5);
    }

    private static void a(String str, long j, com.bumptech.glide.load.b bVar) {
        Log.v("Engine", str + " in " + com.bumptech.glide.f.d.a(j) + "ms, key: " + bVar);
    }

    private g<?> a(com.bumptech.glide.load.b bVar, boolean z) {
        g<?> gVar;
        if (!z) {
            return null;
        }
        WeakReference weakReference = this.f2996e.get(bVar);
        if (weakReference != null) {
            gVar = (g) weakReference.get();
            if (gVar != null) {
                gVar.e();
            } else {
                this.f2996e.remove(bVar);
            }
        } else {
            gVar = null;
        }
        return gVar;
    }

    private g<?> b(com.bumptech.glide.load.b bVar, boolean z) {
        if (!z) {
            return null;
        }
        g<?> a2 = a(bVar);
        if (a2 == null) {
            return a2;
        }
        a2.e();
        this.f2996e.put(bVar, new e(bVar, a2, a()));
        return a2;
    }

    private g<?> a(com.bumptech.glide.load.b bVar) {
        i<?> a2 = this.f2994c.a(bVar);
        if (a2 == null) {
            return null;
        }
        if (a2 instanceof g) {
            return (g) a2;
        }
        return new g<>(a2, true);
    }

    public void a(i iVar) {
        h.a();
        if (iVar instanceof g) {
            ((g) iVar).f();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    public void a(com.bumptech.glide.load.b bVar, g<?> gVar) {
        h.a();
        if (gVar != null) {
            gVar.a(bVar, this);
            if (gVar.a()) {
                this.f2996e.put(bVar, new e(bVar, gVar, a()));
            }
        }
        this.f2992a.remove(bVar);
    }

    public void a(c cVar, com.bumptech.glide.load.b bVar) {
        h.a();
        if (cVar.equals(this.f2992a.get(bVar))) {
            this.f2992a.remove(bVar);
        }
    }

    public void b(i<?> iVar) {
        h.a();
        this.f2997f.a(iVar);
    }

    public void b(com.bumptech.glide.load.b bVar, g gVar) {
        h.a();
        this.f2996e.remove(bVar);
        if (gVar.a()) {
            this.f2994c.b(bVar, gVar);
        } else {
            this.f2997f.a(gVar);
        }
    }

    private ReferenceQueue<g<?>> a() {
        if (this.f2999h == null) {
            this.f2999h = new ReferenceQueue<>();
            Looper.myQueue().addIdleHandler(new d(this.f2996e, this.f2999h));
        }
        return this.f2999h;
    }

    /* renamed from: com.bumptech.glide.load.engine.b$b  reason: collision with other inner class name */
    /* compiled from: Engine */
    private static class C0039b implements a.C0037a {

        /* renamed from: a  reason: collision with root package name */
        private final a.C0040a f3007a;

        /* renamed from: b  reason: collision with root package name */
        private volatile com.bumptech.glide.load.engine.cache.a f3008b;

        public C0039b(a.C0040a aVar) {
            this.f3007a = aVar;
        }

        public com.bumptech.glide.load.engine.cache.a a() {
            if (this.f3008b == null) {
                synchronized (this) {
                    if (this.f3008b == null) {
                        this.f3008b = this.f3007a.a();
                    }
                    if (this.f3008b == null) {
                        this.f3008b = new com.bumptech.glide.load.engine.cache.b();
                    }
                }
            }
            return this.f3008b;
        }
    }

    /* compiled from: Engine */
    private static class e extends WeakReference<g<?>> {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final com.bumptech.glide.load.b f3013a;

        public e(com.bumptech.glide.load.b bVar, g<?> gVar, ReferenceQueue<? super g<?>> referenceQueue) {
            super(gVar, referenceQueue);
            this.f3013a = bVar;
        }
    }

    /* compiled from: Engine */
    private static class d implements MessageQueue.IdleHandler {

        /* renamed from: a  reason: collision with root package name */
        private final Map<com.bumptech.glide.load.b, WeakReference<g<?>>> f3011a;

        /* renamed from: b  reason: collision with root package name */
        private final ReferenceQueue<g<?>> f3012b;

        public d(Map<com.bumptech.glide.load.b, WeakReference<g<?>>> map, ReferenceQueue<g<?>> referenceQueue) {
            this.f3011a = map;
            this.f3012b = referenceQueue;
        }

        public boolean queueIdle() {
            e eVar = (e) this.f3012b.poll();
            if (eVar == null) {
                return true;
            }
            this.f3011a.remove(eVar.f3013a);
            return true;
        }
    }

    /* compiled from: Engine */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        private final ExecutorService f3000a;

        /* renamed from: b  reason: collision with root package name */
        private final ExecutorService f3001b;

        /* renamed from: c  reason: collision with root package name */
        private final d f3002c;

        public a(ExecutorService executorService, ExecutorService executorService2, d dVar) {
            this.f3000a = executorService;
            this.f3001b = executorService2;
            this.f3002c = dVar;
        }

        public c a(com.bumptech.glide.load.b bVar, boolean z) {
            return new c(bVar, this.f3000a, this.f3001b, z, this.f3002c);
        }
    }
}
