package com.bumptech.glide.load.a;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import java.io.InputStream;

/* compiled from: StreamLocalUriFetcher */
public class i extends g<InputStream> {
    public i(Context context, Uri uri) {
        super(context, uri);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public InputStream b(Uri uri, ContentResolver contentResolver) {
        return contentResolver.openInputStream(uri);
    }

    /* access modifiers changed from: protected */
    public void a(InputStream inputStream) {
        inputStream.close();
    }
}
