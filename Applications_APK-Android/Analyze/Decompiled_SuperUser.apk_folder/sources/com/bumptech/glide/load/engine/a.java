package com.bumptech.glide.load.engine;

import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.f.d;
import com.bumptech.glide.load.engine.cache.a;
import com.bumptech.glide.load.f;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/* compiled from: DecodeJob */
class a<A, T, Z> {

    /* renamed from: a  reason: collision with root package name */
    private static final b f2949a = new b();

    /* renamed from: b  reason: collision with root package name */
    private final e f2950b;

    /* renamed from: c  reason: collision with root package name */
    private final int f2951c;

    /* renamed from: d  reason: collision with root package name */
    private final int f2952d;

    /* renamed from: e  reason: collision with root package name */
    private final com.bumptech.glide.load.a.c<A> f2953e;

    /* renamed from: f  reason: collision with root package name */
    private final com.bumptech.glide.d.b<A, T> f2954f;

    /* renamed from: g  reason: collision with root package name */
    private final f<T> f2955g;

    /* renamed from: h  reason: collision with root package name */
    private final com.bumptech.glide.load.resource.transcode.b<T, Z> f2956h;
    private final C0037a i;
    private final DiskCacheStrategy j;
    private final Priority k;
    /* access modifiers changed from: private */
    public final b l;
    private volatile boolean m;

    /* renamed from: com.bumptech.glide.load.engine.a$a  reason: collision with other inner class name */
    /* compiled from: DecodeJob */
    interface C0037a {
        com.bumptech.glide.load.engine.cache.a a();
    }

    public a(e eVar, int i2, int i3, com.bumptech.glide.load.a.c<A> cVar, com.bumptech.glide.d.b<A, T> bVar, f<T> fVar, com.bumptech.glide.load.resource.transcode.b<T, Z> bVar2, C0037a aVar, DiskCacheStrategy diskCacheStrategy, Priority priority) {
        this(eVar, i2, i3, cVar, bVar, fVar, bVar2, aVar, diskCacheStrategy, priority, f2949a);
    }

    a(e eVar, int i2, int i3, com.bumptech.glide.load.a.c<A> cVar, com.bumptech.glide.d.b<A, T> bVar, f<T> fVar, com.bumptech.glide.load.resource.transcode.b<T, Z> bVar2, C0037a aVar, DiskCacheStrategy diskCacheStrategy, Priority priority, b bVar3) {
        this.f2950b = eVar;
        this.f2951c = i2;
        this.f2952d = i3;
        this.f2953e = cVar;
        this.f2954f = bVar;
        this.f2955g = fVar;
        this.f2956h = bVar2;
        this.i = aVar;
        this.j = diskCacheStrategy;
        this.k = priority;
        this.l = bVar3;
    }

    public i<Z> a() {
        if (!this.j.b()) {
            return null;
        }
        long a2 = d.a();
        i a3 = a((com.bumptech.glide.load.b) this.f2950b);
        if (Log.isLoggable("DecodeJob", 2)) {
            a("Decoded transformed from cache", a2);
        }
        long a4 = d.a();
        i<Z> d2 = d(a3);
        if (!Log.isLoggable("DecodeJob", 2)) {
            return d2;
        }
        a("Transcoded transformed from cache", a4);
        return d2;
    }

    public i<Z> b() {
        if (!this.j.a()) {
            return null;
        }
        long a2 = d.a();
        i a3 = a(this.f2950b.a());
        if (Log.isLoggable("DecodeJob", 2)) {
            a("Decoded source from cache", a2);
        }
        return a(a3);
    }

    public i<Z> c() {
        return a(e());
    }

    public void d() {
        this.m = true;
        this.f2953e.c();
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [com.bumptech.glide.load.engine.i, com.bumptech.glide.load.engine.i<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.bumptech.glide.load.engine.i<Z> a(com.bumptech.glide.load.engine.i<T> r6) {
        /*
            r5 = this;
            r4 = 2
            long r0 = com.bumptech.glide.f.d.a()
            com.bumptech.glide.load.engine.i r2 = r5.c(r6)
            java.lang.String r3 = "DecodeJob"
            boolean r3 = android.util.Log.isLoggable(r3, r4)
            if (r3 == 0) goto L_0x0016
            java.lang.String r3 = "Transformed resource from source"
            r5.a(r3, r0)
        L_0x0016:
            r5.b(r2)
            long r0 = com.bumptech.glide.f.d.a()
            com.bumptech.glide.load.engine.i r2 = r5.d(r2)
            java.lang.String r3 = "DecodeJob"
            boolean r3 = android.util.Log.isLoggable(r3, r4)
            if (r3 == 0) goto L_0x002e
            java.lang.String r3 = "Transcoded transformed from source"
            r5.a(r3, r0)
        L_0x002e:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.engine.a.a(com.bumptech.glide.load.engine.i):com.bumptech.glide.load.engine.i");
    }

    private void b(i<T> iVar) {
        if (iVar != null && this.j.b()) {
            long a2 = d.a();
            this.i.a().a(this.f2950b, new c(this.f2954f.d(), iVar));
            if (Log.isLoggable("DecodeJob", 2)) {
                a("Wrote transformed from source to cache", a2);
            }
        }
    }

    private i<T> e() {
        try {
            long a2 = d.a();
            A a3 = this.f2953e.a(this.k);
            if (Log.isLoggable("DecodeJob", 2)) {
                a("Fetched data", a2);
            }
            if (this.m) {
                return null;
            }
            i<T> a4 = a((Object) a3);
            this.f2953e.a();
            return a4;
        } finally {
            this.f2953e.a();
        }
    }

    private i<T> a(A a2) {
        if (this.j.a()) {
            return b((Object) a2);
        }
        long a3 = d.a();
        i<T> a4 = this.f2954f.b().a(a2, this.f2951c, this.f2952d);
        if (!Log.isLoggable("DecodeJob", 2)) {
            return a4;
        }
        a("Decoded from source", a3);
        return a4;
    }

    private i<T> b(A a2) {
        long a3 = d.a();
        this.i.a().a(this.f2950b.a(), new c(this.f2954f.c(), a2));
        if (Log.isLoggable("DecodeJob", 2)) {
            a("Wrote source to cache", a3);
        }
        long a4 = d.a();
        i<T> a5 = a(this.f2950b.a());
        if (Log.isLoggable("DecodeJob", 2) && a5 != null) {
            a("Decoded source from cache", a4);
        }
        return a5;
    }

    private i<T> a(com.bumptech.glide.load.b bVar) {
        i<T> iVar = null;
        File a2 = this.i.a().a(bVar);
        if (a2 != null) {
            try {
                iVar = this.f2954f.a().a(a2, this.f2951c, this.f2952d);
                if (iVar == null) {
                    this.i.a().b(bVar);
                }
            } catch (Throwable th) {
                if (0 == 0) {
                    this.i.a().b(bVar);
                }
                throw th;
            }
        }
        return iVar;
    }

    private i<T> c(i<T> iVar) {
        if (iVar == null) {
            return null;
        }
        i<T> a2 = this.f2955g.a(iVar, this.f2951c, this.f2952d);
        if (iVar.equals(a2)) {
            return a2;
        }
        iVar.d();
        return a2;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.bumptech.glide.load.engine.i, com.bumptech.glide.load.engine.i<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.bumptech.glide.load.engine.i<Z> d(com.bumptech.glide.load.engine.i<T> r2) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0004
            r0 = 0
        L_0x0003:
            return r0
        L_0x0004:
            com.bumptech.glide.load.resource.transcode.b<T, Z> r0 = r1.f2956h
            com.bumptech.glide.load.engine.i r0 = r0.a(r2)
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.engine.a.d(com.bumptech.glide.load.engine.i):com.bumptech.glide.load.engine.i");
    }

    private void a(String str, long j2) {
        Log.v("DecodeJob", str + " in " + d.a(j2) + ", key: " + this.f2950b);
    }

    /* compiled from: DecodeJob */
    class c<DataType> implements a.b {

        /* renamed from: b  reason: collision with root package name */
        private final com.bumptech.glide.load.a<DataType> f2965b;

        /* renamed from: c  reason: collision with root package name */
        private final DataType f2966c;

        public c(com.bumptech.glide.load.a<DataType> aVar, DataType datatype) {
            this.f2965b = aVar;
            this.f2966c = datatype;
        }

        public boolean a(File file) {
            boolean z = false;
            OutputStream outputStream = null;
            try {
                outputStream = a.this.l.a(file);
                z = this.f2965b.a(this.f2966c, outputStream);
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e2) {
                    }
                }
            } catch (FileNotFoundException e3) {
                if (Log.isLoggable("DecodeJob", 3)) {
                    Log.d("DecodeJob", "Failed to find file to write to disk cache", e3);
                }
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e4) {
                    }
                }
            } catch (Throwable th) {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e5) {
                    }
                }
                throw th;
            }
            return z;
        }
    }

    /* compiled from: DecodeJob */
    static class b {
        b() {
        }

        public OutputStream a(File file) {
            return new BufferedOutputStream(new FileOutputStream(file));
        }
    }
}
