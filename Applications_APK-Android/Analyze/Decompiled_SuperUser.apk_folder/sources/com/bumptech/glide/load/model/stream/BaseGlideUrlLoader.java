package com.bumptech.glide.load.model.stream;

import android.content.Context;
import com.bumptech.glide.e;
import com.bumptech.glide.load.model.c;
import com.bumptech.glide.load.model.j;
import com.bumptech.glide.load.model.k;
import java.io.InputStream;

public abstract class BaseGlideUrlLoader<T> implements c<T> {

    /* renamed from: a  reason: collision with root package name */
    private final k<c, InputStream> f3116a;

    /* renamed from: b  reason: collision with root package name */
    private final j<T, c> f3117b;

    public BaseGlideUrlLoader(Context context) {
        this(context, (j) null);
    }

    public BaseGlideUrlLoader(Context context, j<T, c> jVar) {
        this(e.a(c.class, InputStream.class, context), jVar);
    }

    public BaseGlideUrlLoader(k<c, InputStream> kVar, j<T, c> jVar) {
        this.f3116a = kVar;
        this.f3117b = jVar;
    }
}
