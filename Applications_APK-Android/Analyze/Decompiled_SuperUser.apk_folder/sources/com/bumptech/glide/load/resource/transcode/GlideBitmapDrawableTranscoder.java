package com.bumptech.glide.load.resource.transcode;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.resource.bitmap.f;
import com.bumptech.glide.load.resource.bitmap.g;

public class GlideBitmapDrawableTranscoder implements b<Bitmap, f> {

    /* renamed from: a  reason: collision with root package name */
    private final Resources f3262a;

    /* renamed from: b  reason: collision with root package name */
    private final c f3263b;

    public GlideBitmapDrawableTranscoder(Context context) {
        this(context.getResources(), e.a(context).a());
    }

    public GlideBitmapDrawableTranscoder(Resources resources, c cVar) {
        this.f3262a = resources;
        this.f3263b = cVar;
    }

    public i<f> a(i<Bitmap> iVar) {
        return new g(new f(this.f3262a, iVar.b()), this.f3263b);
    }

    public String a() {
        return "GlideBitmapDrawableTranscoder.com.bumptech.glide.load.resource.transcode";
    }
}
