package com.bumptech.glide.load;

public enum DecodeFormat {
    ALWAYS_ARGB_8888,
    PREFER_ARGB_8888,
    PREFER_RGB_565;
    

    /* renamed from: d  reason: collision with root package name */
    public static final DecodeFormat f2916d = PREFER_RGB_565;
}
