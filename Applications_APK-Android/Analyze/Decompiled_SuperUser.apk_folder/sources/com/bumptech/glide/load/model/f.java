package com.bumptech.glide.load.model;

import android.os.ParcelFileDescriptor;
import java.io.InputStream;

/* compiled from: ImageVideoWrapper */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private final InputStream f3090a;

    /* renamed from: b  reason: collision with root package name */
    private final ParcelFileDescriptor f3091b;

    public f(InputStream inputStream, ParcelFileDescriptor parcelFileDescriptor) {
        this.f3090a = inputStream;
        this.f3091b = parcelFileDescriptor;
    }

    public InputStream a() {
        return this.f3090a;
    }

    public ParcelFileDescriptor b() {
        return this.f3091b;
    }
}
