package com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.a.c;

public class CenterCrop extends BitmapTransformation {
    public CenterCrop(Context context) {
        super(context);
    }

    public CenterCrop(c cVar) {
        super(cVar);
    }

    /* access modifiers changed from: protected */
    public Bitmap a(c cVar, Bitmap bitmap, int i, int i2) {
        Bitmap a2 = cVar.a(i, i2, bitmap.getConfig() != null ? bitmap.getConfig() : Bitmap.Config.ARGB_8888);
        Bitmap a3 = k.a(a2, bitmap, i, i2);
        if (!(a2 == null || a2 == a3 || cVar.a(a2))) {
            a2.recycle();
        }
        return a3;
    }

    public String a() {
        return "CenterCrop.com.bumptech.glide.load.resource.bitmap";
    }
}
