package com.bumptech.glide.load.model;

import android.util.Log;
import com.bumptech.glide.load.a;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: StreamEncoder */
public class n implements a<InputStream> {
    public boolean a(InputStream inputStream, OutputStream outputStream) {
        byte[] b2 = com.bumptech.glide.f.a.a().b();
        while (true) {
            try {
                int read = inputStream.read(b2);
                if (read != -1) {
                    outputStream.write(b2, 0, read);
                } else {
                    return true;
                }
            } catch (IOException e2) {
                if (Log.isLoggable("StreamEncoder", 3)) {
                    Log.d("StreamEncoder", "Failed to encode data onto the OutputStream", e2);
                }
                return false;
            } finally {
                com.bumptech.glide.f.a.a().a(b2);
            }
        }
    }

    public String a() {
        return "";
    }
}
