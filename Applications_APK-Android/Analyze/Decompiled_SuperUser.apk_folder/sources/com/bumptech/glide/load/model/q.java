package com.bumptech.glide.load.model;

import com.bumptech.glide.load.a.c;
import java.net.URL;

/* compiled from: UrlLoader */
public class q<T> implements k<URL, T> {

    /* renamed from: a  reason: collision with root package name */
    private final k<c, T> f3115a;

    public q(k<c, T> kVar) {
        this.f3115a = kVar;
    }

    public c<T> a(URL url, int i, int i2) {
        return this.f3115a.a(new c(url), i, i2);
    }
}
