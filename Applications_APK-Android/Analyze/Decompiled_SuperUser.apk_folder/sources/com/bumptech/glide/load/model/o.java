package com.bumptech.glide.load.model;

import android.net.Uri;
import android.text.TextUtils;
import com.bumptech.glide.load.a.c;
import java.io.File;

/* compiled from: StringLoader */
public class o<T> implements k<String, T> {

    /* renamed from: a  reason: collision with root package name */
    private final k<Uri, T> f3112a;

    public o(k<Uri, T> kVar) {
        this.f3112a = kVar;
    }

    public c<T> a(String str, int i, int i2) {
        Uri parse;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.startsWith("/")) {
            parse = a(str);
        } else {
            parse = Uri.parse(str);
            if (parse.getScheme() == null) {
                parse = a(str);
            }
        }
        return this.f3112a.a(parse, i, i2);
    }

    private static Uri a(String str) {
        return Uri.fromFile(new File(str));
    }
}
