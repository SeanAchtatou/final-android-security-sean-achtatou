package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import com.bumptech.glide.d.b;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.a;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.e;
import com.bumptech.glide.load.model.n;
import com.bumptech.glide.load.resource.b.c;
import java.io.File;
import java.io.InputStream;

/* compiled from: StreamBitmapDataLoadProvider */
public class j implements b<InputStream, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private final StreamBitmapDecoder f3179a;

    /* renamed from: b  reason: collision with root package name */
    private final b f3180b;

    /* renamed from: c  reason: collision with root package name */
    private final n f3181c = new n();

    /* renamed from: d  reason: collision with root package name */
    private final c<Bitmap> f3182d;

    public j(com.bumptech.glide.load.engine.a.c cVar, DecodeFormat decodeFormat) {
        this.f3179a = new StreamBitmapDecoder(cVar, decodeFormat);
        this.f3180b = new b();
        this.f3182d = new c<>(this.f3179a);
    }

    public d<File, Bitmap> a() {
        return this.f3182d;
    }

    public d<InputStream, Bitmap> b() {
        return this.f3179a;
    }

    public a<InputStream> c() {
        return this.f3181c;
    }

    public e<Bitmap> d() {
        return this.f3180b;
    }
}
