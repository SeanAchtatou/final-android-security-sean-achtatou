package com.bumptech.glide.b;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;

/* compiled from: GifDecoder */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2792a = a.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static final Bitmap.Config f2793b = Bitmap.Config.ARGB_8888;

    /* renamed from: c  reason: collision with root package name */
    private int[] f2794c;

    /* renamed from: d  reason: collision with root package name */
    private ByteBuffer f2795d;

    /* renamed from: e  reason: collision with root package name */
    private final byte[] f2796e = new byte[FileUtils.FileMode.MODE_IRUSR];

    /* renamed from: f  reason: collision with root package name */
    private short[] f2797f;

    /* renamed from: g  reason: collision with root package name */
    private byte[] f2798g;

    /* renamed from: h  reason: collision with root package name */
    private byte[] f2799h;
    private byte[] i;
    private int[] j;
    private int k;
    private byte[] l;
    private c m;
    private C0036a n;
    private Bitmap o;
    private boolean p;
    private int q;

    /* renamed from: com.bumptech.glide.b.a$a  reason: collision with other inner class name */
    /* compiled from: GifDecoder */
    public interface C0036a {
        Bitmap a(int i, int i2, Bitmap.Config config);

        void a(Bitmap bitmap);
    }

    public a(C0036a aVar) {
        this.n = aVar;
        this.m = new c();
    }

    public void a() {
        this.k = (this.k + 1) % this.m.f2810c;
    }

    public int a(int i2) {
        if (i2 < 0 || i2 >= this.m.f2810c) {
            return -1;
        }
        return this.m.f2812e.get(i2).i;
    }

    public int b() {
        if (this.m.f2810c <= 0 || this.k < 0) {
            return -1;
        }
        return a(this.k);
    }

    public int c() {
        return this.m.f2810c;
    }

    public int d() {
        return this.k;
    }

    public int e() {
        return this.m.m;
    }

    public synchronized Bitmap f() {
        Bitmap bitmap;
        b bVar;
        int i2 = 0;
        synchronized (this) {
            if (this.m.f2810c <= 0 || this.k < 0) {
                if (Log.isLoggable(f2792a, 3)) {
                    Log.d(f2792a, "unable to decode frame, frameCount=" + this.m.f2810c + " framePointer=" + this.k);
                }
                this.q = 1;
            }
            if (this.q == 1 || this.q == 2) {
                if (Log.isLoggable(f2792a, 3)) {
                    Log.d(f2792a, "Unable to decode frame, status=" + this.q);
                }
                bitmap = null;
            } else {
                this.q = 0;
                b bVar2 = this.m.f2812e.get(this.k);
                int i3 = this.k - 1;
                if (i3 >= 0) {
                    bVar = this.m.f2812e.get(i3);
                } else {
                    bVar = null;
                }
                if (bVar2.k == null) {
                    this.f2794c = this.m.f2808a;
                } else {
                    this.f2794c = bVar2.k;
                    if (this.m.j == bVar2.f2807h) {
                        this.m.l = 0;
                    }
                }
                if (bVar2.f2805f) {
                    int i4 = this.f2794c[bVar2.f2807h];
                    this.f2794c[bVar2.f2807h] = 0;
                    i2 = i4;
                }
                if (this.f2794c == null) {
                    if (Log.isLoggable(f2792a, 3)) {
                        Log.d(f2792a, "No Valid Color Table");
                    }
                    this.q = 1;
                    bitmap = null;
                } else {
                    Bitmap a2 = a(bVar2, bVar);
                    if (bVar2.f2805f) {
                        this.f2794c[bVar2.f2807h] = i2;
                    }
                    bitmap = a2;
                }
            }
        }
        return bitmap;
    }

    public void g() {
        this.m = null;
        this.l = null;
        this.i = null;
        this.j = null;
        if (this.o != null) {
            this.n.a(this.o);
        }
        this.o = null;
        this.f2795d = null;
    }

    public void a(c cVar, byte[] bArr) {
        this.m = cVar;
        this.l = bArr;
        this.q = 0;
        this.k = -1;
        this.f2795d = ByteBuffer.wrap(bArr);
        this.f2795d.rewind();
        this.f2795d.order(ByteOrder.LITTLE_ENDIAN);
        this.p = false;
        Iterator<b> it = cVar.f2812e.iterator();
        while (true) {
            if (it.hasNext()) {
                if (it.next().f2806g == 3) {
                    this.p = true;
                    break;
                }
            } else {
                break;
            }
        }
        this.i = new byte[(cVar.f2813f * cVar.f2814g)];
        this.j = new int[(cVar.f2813f * cVar.f2814g)];
    }

    private Bitmap a(b bVar, b bVar2) {
        int i2;
        int i3 = this.m.f2813f;
        int i4 = this.m.f2814g;
        int[] iArr = this.j;
        if (bVar2 != null && bVar2.f2806g > 0) {
            if (bVar2.f2806g == 2) {
                int i5 = 0;
                if (!bVar.f2805f) {
                    i5 = this.m.l;
                }
                Arrays.fill(iArr, i5);
            } else if (bVar2.f2806g == 3 && this.o != null) {
                this.o.getPixels(iArr, 0, i3, 0, 0, i3, i4);
            }
        }
        a(bVar);
        int i6 = 1;
        int i7 = 8;
        int i8 = 0;
        for (int i9 = 0; i9 < bVar.f2803d; i9++) {
            if (bVar.f2804e) {
                if (i8 >= bVar.f2803d) {
                    i6++;
                    switch (i6) {
                        case 2:
                            i8 = 4;
                            break;
                        case 3:
                            i8 = 2;
                            i7 = 4;
                            break;
                        case 4:
                            i8 = 1;
                            i7 = 2;
                            break;
                    }
                }
                int i10 = i8;
                i8 += i7;
                i2 = i10;
            } else {
                i2 = i9;
            }
            int i11 = i2 + bVar.f2801b;
            if (i11 < this.m.f2814g) {
                int i12 = this.m.f2813f * i11;
                int i13 = i12 + bVar.f2800a;
                int i14 = bVar.f2802c + i13;
                if (this.m.f2813f + i12 < i14) {
                    i14 = this.m.f2813f + i12;
                }
                int i15 = bVar.f2802c * i9;
                int i16 = i13;
                while (i16 < i14) {
                    int i17 = i15 + 1;
                    int i18 = this.f2794c[this.i[i15] & 255];
                    if (i18 != 0) {
                        iArr[i16] = i18;
                    }
                    i16++;
                    i15 = i17;
                }
            }
        }
        if (this.p && (bVar.f2806g == 0 || bVar.f2806g == 1)) {
            if (this.o == null) {
                this.o = j();
            }
            this.o.setPixels(iArr, 0, i3, 0, 0, i3, i4);
        }
        Bitmap j2 = j();
        j2.setPixels(iArr, 0, i3, 0, 0, i3, i4);
        return j2;
    }

    /* JADX WARN: Type inference failed for: r8v15, types: [short[]] */
    /* JADX WARN: Type inference failed for: r8v16, types: [short] */
    private void a(b bVar) {
        int i2;
        byte b2;
        if (bVar != null) {
            this.f2795d.position(bVar.j);
        }
        if (bVar == null) {
            i2 = this.m.f2813f * this.m.f2814g;
        } else {
            i2 = bVar.f2802c * bVar.f2803d;
        }
        if (this.i == null || this.i.length < i2) {
            this.i = new byte[i2];
        }
        if (this.f2797f == null) {
            this.f2797f = new short[CodedOutputStream.DEFAULT_BUFFER_SIZE];
        }
        if (this.f2798g == null) {
            this.f2798g = new byte[CodedOutputStream.DEFAULT_BUFFER_SIZE];
        }
        if (this.f2799h == null) {
            this.f2799h = new byte[4097];
        }
        int h2 = h();
        int i3 = 1 << h2;
        int i4 = i3 + 1;
        int i5 = i3 + 2;
        byte b3 = -1;
        int i6 = h2 + 1;
        int i7 = (1 << i6) - 1;
        for (int i8 = 0; i8 < i3; i8++) {
            this.f2797f[i8] = 0;
            this.f2798g[i8] = (byte) i8;
        }
        byte b4 = 0;
        int i9 = 0;
        int i10 = 0;
        byte b5 = 0;
        byte b6 = 0;
        int i11 = i6;
        int i12 = i7;
        int i13 = i5;
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        while (true) {
            if (i9 < i2) {
                if (i15 == 0) {
                    i15 = i();
                    if (i15 <= 0) {
                        this.q = 3;
                        break;
                    }
                    i14 = 0;
                }
                int i17 = b4 + ((this.f2796e[i14] & 255) << b6);
                int i18 = i14 + 1;
                int i19 = i15 - 1;
                int i20 = i11;
                int i21 = i12;
                byte b7 = b5;
                int i22 = b6 + 8;
                byte b8 = i17;
                int i23 = i16;
                int i24 = i13;
                int i25 = i22;
                while (true) {
                    if (i25 < i20) {
                        b5 = b7;
                        i12 = i21;
                        i15 = i19;
                        i11 = i20;
                        i14 = i18;
                        byte b9 = i25;
                        i13 = i24;
                        i16 = i23;
                        b4 = b8;
                        b6 = b9;
                        break;
                    }
                    byte b10 = b8 & i21;
                    int i26 = b8 >> i20;
                    i25 -= i20;
                    if (b10 == i3) {
                        i20 = h2 + 1;
                        i21 = (1 << i20) - 1;
                        i24 = i3 + 2;
                        b8 = i26;
                        b3 = -1;
                    } else if (b10 > i24) {
                        this.q = 3;
                        b5 = b7;
                        b6 = i25;
                        i11 = i20;
                        i13 = i24;
                        i14 = i18;
                        i16 = i23;
                        b4 = i26;
                        i12 = i21;
                        i15 = i19;
                        break;
                    } else if (b10 == i4) {
                        b5 = b7;
                        b6 = i25;
                        i11 = i20;
                        i13 = i24;
                        i14 = i18;
                        i16 = i23;
                        b4 = i26;
                        i12 = i21;
                        i15 = i19;
                        break;
                    } else if (b3 == -1) {
                        this.f2799h[i10] = this.f2798g[b10];
                        i10++;
                        b7 = b10;
                        b3 = b10;
                        b8 = i26;
                    } else {
                        if (b10 >= i24) {
                            this.f2799h[i10] = (byte) b7;
                            i10++;
                            b2 = b3;
                        } else {
                            b2 = b10;
                        }
                        while (b2 >= i3) {
                            this.f2799h[i10] = this.f2798g[b2];
                            b2 = this.f2797f[b2];
                            i10++;
                        }
                        b7 = this.f2798g[b2] & 255;
                        int i27 = i10 + 1;
                        this.f2799h[i10] = (byte) b7;
                        if (i24 < 4096) {
                            this.f2797f[i24] = (short) b3;
                            this.f2798g[i24] = (byte) b7;
                            i24++;
                            if ((i24 & i21) == 0 && i24 < 4096) {
                                i20++;
                                i21 += i24;
                            }
                        }
                        int i28 = i9;
                        while (i27 > 0) {
                            int i29 = i27 - 1;
                            this.i[i23] = this.f2799h[i29];
                            i28++;
                            i23++;
                            i27 = i29;
                        }
                        i9 = i28;
                        b3 = b10;
                        i10 = i27;
                        b8 = i26;
                    }
                }
            } else {
                break;
            }
        }
        for (int i30 = i16; i30 < i2; i30++) {
            this.i[i30] = 0;
        }
    }

    private int h() {
        try {
            return this.f2795d.get() & 255;
        } catch (Exception e2) {
            this.q = 1;
            return 0;
        }
    }

    private int i() {
        int h2 = h();
        int i2 = 0;
        if (h2 > 0) {
            while (i2 < h2) {
                int i3 = h2 - i2;
                try {
                    this.f2795d.get(this.f2796e, i2, i3);
                    i2 += i3;
                } catch (Exception e2) {
                    Log.w(f2792a, "Error Reading Block", e2);
                    this.q = 1;
                }
            }
        }
        return i2;
    }

    private Bitmap j() {
        Bitmap a2 = this.n.a(this.m.f2813f, this.m.f2814g, f2793b);
        if (a2 == null) {
            a2 = Bitmap.createBitmap(this.m.f2813f, this.m.f2814g, f2793b);
        }
        a(a2);
        return a2;
    }

    @TargetApi(12)
    private static void a(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 12) {
            bitmap.setHasAlpha(true);
        }
    }
}
