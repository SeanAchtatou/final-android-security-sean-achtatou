package com.bumptech.glide.b;

import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.os.VUserInfo;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/* compiled from: GifHeaderParser */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f2816a = new byte[FileUtils.FileMode.MODE_IRUSR];

    /* renamed from: b  reason: collision with root package name */
    private ByteBuffer f2817b;

    /* renamed from: c  reason: collision with root package name */
    private c f2818c;

    /* renamed from: d  reason: collision with root package name */
    private int f2819d = 0;

    public d a(byte[] bArr) {
        c();
        if (bArr != null) {
            this.f2817b = ByteBuffer.wrap(bArr);
            this.f2817b.rewind();
            this.f2817b.order(ByteOrder.LITTLE_ENDIAN);
        } else {
            this.f2817b = null;
            this.f2818c.f2809b = 2;
        }
        return this;
    }

    public void a() {
        this.f2817b = null;
        this.f2818c = null;
    }

    private void c() {
        this.f2817b = null;
        Arrays.fill(this.f2816a, (byte) 0);
        this.f2818c = new c();
        this.f2819d = 0;
    }

    public c b() {
        if (this.f2817b == null) {
            throw new IllegalStateException("You must call setData() before parseHeader()");
        } else if (o()) {
            return this.f2818c;
        } else {
            h();
            if (!o()) {
                d();
                if (this.f2818c.f2810c < 0) {
                    this.f2818c.f2809b = 1;
                }
            }
            return this.f2818c;
        }
    }

    private void d() {
        boolean z = false;
        while (!z && !o()) {
            switch (m()) {
                case 33:
                    switch (m()) {
                        case 1:
                            k();
                            continue;
                        case 249:
                            this.f2818c.f2811d = new b();
                            e();
                            continue;
                        case 254:
                            k();
                            continue;
                        case VUserInfo.FLAG_MASK_USER_TYPE /*255*/:
                            l();
                            String str = "";
                            for (int i = 0; i < 11; i++) {
                                str = str + ((char) this.f2816a[i]);
                            }
                            if (!str.equals("NETSCAPE2.0")) {
                                k();
                                break;
                            } else {
                                g();
                                continue;
                            }
                        default:
                            k();
                            continue;
                    }
                case 44:
                    if (this.f2818c.f2811d == null) {
                        this.f2818c.f2811d = new b();
                    }
                    f();
                    break;
                case 59:
                    z = true;
                    break;
                default:
                    this.f2818c.f2809b = 1;
                    break;
            }
        }
    }

    private void e() {
        boolean z = true;
        m();
        int m = m();
        this.f2818c.f2811d.f2806g = (m & 28) >> 2;
        if (this.f2818c.f2811d.f2806g == 0) {
            this.f2818c.f2811d.f2806g = 1;
        }
        b bVar = this.f2818c.f2811d;
        if ((m & 1) == 0) {
            z = false;
        }
        bVar.f2805f = z;
        int n = n();
        if (n < 3) {
            n = 10;
        }
        this.f2818c.f2811d.i = n * 10;
        this.f2818c.f2811d.f2807h = m();
        m();
    }

    private void f() {
        boolean z = true;
        this.f2818c.f2811d.f2800a = n();
        this.f2818c.f2811d.f2801b = n();
        this.f2818c.f2811d.f2802c = n();
        this.f2818c.f2811d.f2803d = n();
        int m = m();
        boolean z2 = (m & FileUtils.FileMode.MODE_IWUSR) != 0;
        int pow = (int) Math.pow(2.0d, (double) ((m & 7) + 1));
        b bVar = this.f2818c.f2811d;
        if ((m & 64) == 0) {
            z = false;
        }
        bVar.f2804e = z;
        if (z2) {
            this.f2818c.f2811d.k = a(pow);
        } else {
            this.f2818c.f2811d.k = null;
        }
        this.f2818c.f2811d.j = this.f2817b.position();
        j();
        if (!o()) {
            this.f2818c.f2810c++;
            this.f2818c.f2812e.add(this.f2818c.f2811d);
        }
    }

    private void g() {
        do {
            l();
            if (this.f2816a[0] == 1) {
                this.f2818c.m = (this.f2816a[1] & 255) | ((this.f2816a[2] & 255) << 8);
            }
            if (this.f2819d <= 0) {
                return;
            }
        } while (!o());
    }

    private void h() {
        String str = "";
        for (int i = 0; i < 6; i++) {
            str = str + ((char) m());
        }
        if (!str.startsWith("GIF")) {
            this.f2818c.f2809b = 1;
            return;
        }
        i();
        if (this.f2818c.f2815h && !o()) {
            this.f2818c.f2808a = a(this.f2818c.i);
            this.f2818c.l = this.f2818c.f2808a[this.f2818c.j];
        }
    }

    private void i() {
        this.f2818c.f2813f = n();
        this.f2818c.f2814g = n();
        int m = m();
        this.f2818c.f2815h = (m & FileUtils.FileMode.MODE_IWUSR) != 0;
        this.f2818c.i = 2 << (m & 7);
        this.f2818c.j = m();
        this.f2818c.k = m();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int[] a(int r11) {
        /*
            r10 = this;
            r2 = 0
            int r0 = r11 * 3
            r1 = 0
            byte[] r4 = new byte[r0]
            java.nio.ByteBuffer r0 = r10.f2817b     // Catch:{ BufferUnderflowException -> 0x0033 }
            r0.get(r4)     // Catch:{ BufferUnderflowException -> 0x0033 }
            r0 = 256(0x100, float:3.59E-43)
            int[] r0 = new int[r0]     // Catch:{ BufferUnderflowException -> 0x0033 }
            r1 = r2
        L_0x0010:
            if (r2 >= r11) goto L_0x004c
            int r3 = r1 + 1
            byte r1 = r4[r1]     // Catch:{ BufferUnderflowException -> 0x004d }
            r5 = r1 & 255(0xff, float:3.57E-43)
            int r6 = r3 + 1
            byte r1 = r4[r3]     // Catch:{ BufferUnderflowException -> 0x004d }
            r7 = r1 & 255(0xff, float:3.57E-43)
            int r1 = r6 + 1
            byte r3 = r4[r6]     // Catch:{ BufferUnderflowException -> 0x004d }
            r6 = r3 & 255(0xff, float:3.57E-43)
            int r3 = r2 + 1
            r8 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            int r5 = r5 << 16
            r5 = r5 | r8
            int r7 = r7 << 8
            r5 = r5 | r7
            r5 = r5 | r6
            r0[r2] = r5     // Catch:{ BufferUnderflowException -> 0x004d }
            r2 = r3
            goto L_0x0010
        L_0x0033:
            r0 = move-exception
            r9 = r0
            r0 = r1
            r1 = r9
        L_0x0037:
            java.lang.String r2 = "GifHeaderParser"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)
            if (r2 == 0) goto L_0x0047
            java.lang.String r2 = "GifHeaderParser"
            java.lang.String r3 = "Format Error Reading Color Table"
            android.util.Log.d(r2, r3, r1)
        L_0x0047:
            com.bumptech.glide.b.c r1 = r10.f2818c
            r2 = 1
            r1.f2809b = r2
        L_0x004c:
            return r0
        L_0x004d:
            r1 = move-exception
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.b.d.a(int):int[]");
    }

    private void j() {
        m();
        k();
    }

    private void k() {
        int m;
        do {
            m = m();
            this.f2817b.position(this.f2817b.position() + m);
        } while (m > 0);
    }

    private int l() {
        int i = 0;
        this.f2819d = m();
        if (this.f2819d > 0) {
            int i2 = 0;
            while (i < this.f2819d) {
                try {
                    i2 = this.f2819d - i;
                    this.f2817b.get(this.f2816a, i, i2);
                    i += i2;
                } catch (Exception e2) {
                    if (Log.isLoggable("GifHeaderParser", 3)) {
                        Log.d("GifHeaderParser", "Error Reading Block n: " + i + " count: " + i2 + " blockSize: " + this.f2819d, e2);
                    }
                    this.f2818c.f2809b = 1;
                }
            }
        }
        return i;
    }

    private int m() {
        try {
            return this.f2817b.get() & 255;
        } catch (Exception e2) {
            this.f2818c.f2809b = 1;
            return 0;
        }
    }

    private int n() {
        return this.f2817b.getShort();
    }

    private boolean o() {
        return this.f2818c.f2809b != 0;
    }
}
