package com.bumptech.glide.f;

/* compiled from: MultiClassKey */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private Class<?> f2907a;

    /* renamed from: b  reason: collision with root package name */
    private Class<?> f2908b;

    public g() {
    }

    public g(Class<?> cls, Class<?> cls2) {
        a(cls, cls2);
    }

    public void a(Class<?> cls, Class<?> cls2) {
        this.f2907a = cls;
        this.f2908b = cls2;
    }

    public String toString() {
        return "MultiClassKey{first=" + this.f2907a + ", second=" + this.f2908b + '}';
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        if (!this.f2907a.equals(gVar.f2907a)) {
            return false;
        }
        if (!this.f2908b.equals(gVar.f2908b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.f2907a.hashCode() * 31) + this.f2908b.hashCode();
    }
}
