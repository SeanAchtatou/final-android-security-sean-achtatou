package com.bumptech.glide.f;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: ContentLengthInputStream */
public final class b extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private final long f2890a;

    /* renamed from: b  reason: collision with root package name */
    private int f2891b;

    public static InputStream a(InputStream inputStream, long j) {
        return new b(inputStream, j);
    }

    b(InputStream inputStream, long j) {
        super(inputStream);
        this.f2890a = j;
    }

    public synchronized int available() {
        return (int) Math.max(this.f2890a - ((long) this.f2891b), (long) this.in.available());
    }

    public synchronized int read() {
        return a(super.read());
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public synchronized int read(byte[] bArr, int i, int i2) {
        return a(super.read(bArr, i, i2));
    }

    private int a(int i) {
        if (i >= 0) {
            this.f2891b += i;
        } else if (this.f2890a - ((long) this.f2891b) > 0) {
            throw new IOException("Failed to read all expected data, expected: " + this.f2890a + ", but read: " + this.f2891b);
        }
        return i;
    }
}
