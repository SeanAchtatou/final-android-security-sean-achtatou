package com.bumptech.glide.f;

import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: LruCache */
public class e<T, Y> {

    /* renamed from: a  reason: collision with root package name */
    private final LinkedHashMap<T, Y> f2902a = new LinkedHashMap<>(100, 0.75f, true);

    /* renamed from: b  reason: collision with root package name */
    private int f2903b;

    /* renamed from: c  reason: collision with root package name */
    private final int f2904c;

    /* renamed from: d  reason: collision with root package name */
    private int f2905d = 0;

    public e(int i) {
        this.f2904c = i;
        this.f2903b = i;
    }

    /* access modifiers changed from: protected */
    public int a(Object obj) {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object obj2) {
    }

    public int b() {
        return this.f2905d;
    }

    public Y b(Object obj) {
        return this.f2902a.get(obj);
    }

    public Y b(Object obj, Object obj2) {
        if (a(obj2) >= this.f2903b) {
            a(obj, obj2);
            return null;
        }
        Y put = this.f2902a.put(obj, obj2);
        if (obj2 != null) {
            this.f2905d += a(obj2);
        }
        if (put != null) {
            this.f2905d -= a(put);
        }
        c();
        return put;
    }

    public Y c(T t) {
        Y remove = this.f2902a.remove(t);
        if (remove != null) {
            this.f2905d -= a(remove);
        }
        return remove;
    }

    public void a() {
        b(0);
    }

    /* access modifiers changed from: protected */
    public void b(int i) {
        while (this.f2905d > i) {
            Map.Entry next = this.f2902a.entrySet().iterator().next();
            Object value = next.getValue();
            this.f2905d -= a(value);
            Object key = next.getKey();
            this.f2902a.remove(key);
            a(key, value);
        }
    }

    private void c() {
        b(this.f2903b);
    }
}
