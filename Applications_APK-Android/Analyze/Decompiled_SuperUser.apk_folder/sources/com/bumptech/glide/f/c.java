package com.bumptech.glide.f;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

/* compiled from: ExceptionCatchingInputStream */
public class c extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private static final Queue<c> f2897a = h.a(0);

    /* renamed from: b  reason: collision with root package name */
    private InputStream f2898b;

    /* renamed from: c  reason: collision with root package name */
    private IOException f2899c;

    public static c a(InputStream inputStream) {
        c poll;
        synchronized (f2897a) {
            poll = f2897a.poll();
        }
        if (poll == null) {
            poll = new c();
        }
        poll.b(inputStream);
        return poll;
    }

    c() {
    }

    /* access modifiers changed from: package-private */
    public void b(InputStream inputStream) {
        this.f2898b = inputStream;
    }

    public int available() {
        return this.f2898b.available();
    }

    public void close() {
        this.f2898b.close();
    }

    public void mark(int i) {
        this.f2898b.mark(i);
    }

    public boolean markSupported() {
        return this.f2898b.markSupported();
    }

    public int read(byte[] bArr) {
        try {
            return this.f2898b.read(bArr);
        } catch (IOException e2) {
            this.f2899c = e2;
            return -1;
        }
    }

    public int read(byte[] bArr, int i, int i2) {
        try {
            return this.f2898b.read(bArr, i, i2);
        } catch (IOException e2) {
            this.f2899c = e2;
            return -1;
        }
    }

    public synchronized void reset() {
        this.f2898b.reset();
    }

    public long skip(long j) {
        try {
            return this.f2898b.skip(j);
        } catch (IOException e2) {
            this.f2899c = e2;
            return 0;
        }
    }

    public int read() {
        try {
            return this.f2898b.read();
        } catch (IOException e2) {
            this.f2899c = e2;
            return -1;
        }
    }

    public IOException a() {
        return this.f2899c;
    }

    public void b() {
        this.f2899c = null;
        this.f2898b = null;
        synchronized (f2897a) {
            f2897a.offer(this);
        }
    }
}
