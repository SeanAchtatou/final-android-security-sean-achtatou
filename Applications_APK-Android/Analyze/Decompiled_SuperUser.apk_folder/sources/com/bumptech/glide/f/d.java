package com.bumptech.glide.f;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.SystemClock;

/* compiled from: LogTime */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final double f2901a;

    static {
        double d2 = 1.0d;
        if (17 <= Build.VERSION.SDK_INT) {
            d2 = 1.0d / Math.pow(10.0d, 6.0d);
        }
        f2901a = d2;
    }

    @TargetApi(17)
    public static long a() {
        if (17 <= Build.VERSION.SDK_INT) {
            return SystemClock.elapsedRealtimeNanos();
        }
        return System.currentTimeMillis();
    }

    public static double a(long j) {
        return ((double) (a() - j)) * f2901a;
    }
}
