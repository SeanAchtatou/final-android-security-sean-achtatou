package com.bumptech.glide.f;

import java.io.FilterInputStream;
import java.io.InputStream;

/* compiled from: MarkEnforcingInputStream */
public class f extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private int f2906a = Integer.MIN_VALUE;

    public f(InputStream inputStream) {
        super(inputStream);
    }

    public void mark(int i) {
        super.mark(i);
        this.f2906a = i;
    }

    public int read() {
        if (a(1) == -1) {
            return -1;
        }
        int read = super.read();
        b(1);
        return read;
    }

    public int read(byte[] bArr, int i, int i2) {
        int a2 = (int) a((long) i2);
        if (a2 == -1) {
            return -1;
        }
        int read = super.read(bArr, i, a2);
        b((long) read);
        return read;
    }

    public void reset() {
        super.reset();
        this.f2906a = Integer.MIN_VALUE;
    }

    public long skip(long j) {
        long a2 = a(j);
        if (a2 == -1) {
            return -1;
        }
        long skip = super.skip(a2);
        b(skip);
        return skip;
    }

    public int available() {
        return this.f2906a == Integer.MIN_VALUE ? super.available() : Math.min(this.f2906a, super.available());
    }

    private long a(long j) {
        if (this.f2906a == 0) {
            return -1;
        }
        if (this.f2906a == Integer.MIN_VALUE || j <= ((long) this.f2906a)) {
            return j;
        }
        return (long) this.f2906a;
    }

    private void b(long j) {
        if (this.f2906a != Integer.MIN_VALUE && j != -1) {
            this.f2906a = (int) (((long) this.f2906a) - j);
        }
    }
}
