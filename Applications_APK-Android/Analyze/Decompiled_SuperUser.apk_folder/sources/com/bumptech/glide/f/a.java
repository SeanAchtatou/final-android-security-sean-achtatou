package com.bumptech.glide.f;

import android.util.Log;
import java.util.Queue;

/* compiled from: ByteArrayPool */
public final class a {

    /* renamed from: b  reason: collision with root package name */
    private static final a f2885b = new a();

    /* renamed from: a  reason: collision with root package name */
    private final Queue<byte[]> f2886a = h.a(0);

    public static a a() {
        return f2885b;
    }

    private a() {
    }

    public byte[] b() {
        byte[] poll;
        synchronized (this.f2886a) {
            poll = this.f2886a.poll();
        }
        if (poll == null) {
            poll = new byte[65536];
            if (Log.isLoggable("ByteArrayPool", 3)) {
                Log.d("ByteArrayPool", "Created temp bytes");
            }
        }
        return poll;
    }

    public boolean a(byte[] bArr) {
        boolean z = false;
        if (bArr.length == 65536) {
            synchronized (this.f2886a) {
                if (this.f2886a.size() < 32) {
                    z = true;
                    this.f2886a.offer(bArr);
                }
            }
        }
        return z;
    }
}
