package com.bumptech.glide;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.f.h;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.b;
import com.bumptech.glide.load.engine.b.a;
import com.bumptech.glide.load.engine.cache.g;
import com.bumptech.glide.load.model.GenericLoaderFactory;
import com.bumptech.glide.load.model.file_descriptor.FileDescriptorFileLoader;
import com.bumptech.glide.load.model.file_descriptor.FileDescriptorResourceLoader;
import com.bumptech.glide.load.model.file_descriptor.FileDescriptorStringLoader;
import com.bumptech.glide.load.model.file_descriptor.FileDescriptorUriLoader;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.model.l;
import com.bumptech.glide.load.model.stream.StreamFileLoader;
import com.bumptech.glide.load.model.stream.StreamResourceLoader;
import com.bumptech.glide.load.model.stream.StreamStringLoader;
import com.bumptech.glide.load.model.stream.StreamUriLoader;
import com.bumptech.glide.load.model.stream.a;
import com.bumptech.glide.load.model.stream.b;
import com.bumptech.glide.load.model.stream.d;
import com.bumptech.glide.load.resource.b.d;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.i;
import com.bumptech.glide.load.resource.bitmap.j;
import com.bumptech.glide.load.resource.transcode.GlideBitmapDrawableTranscoder;
import com.bumptech.glide.module.ManifestParser;
import com.bumptech.glide.request.b.f;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

/* compiled from: Glide */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private static volatile e f2867a;

    /* renamed from: b  reason: collision with root package name */
    private final GenericLoaderFactory f2868b;

    /* renamed from: c  reason: collision with root package name */
    private final b f2869c;

    /* renamed from: d  reason: collision with root package name */
    private final c f2870d;

    /* renamed from: e  reason: collision with root package name */
    private final g f2871e;

    /* renamed from: f  reason: collision with root package name */
    private final DecodeFormat f2872f;

    /* renamed from: g  reason: collision with root package name */
    private final f f2873g = new f();

    /* renamed from: h  reason: collision with root package name */
    private final com.bumptech.glide.load.resource.transcode.c f2874h = new com.bumptech.glide.load.resource.transcode.c();
    private final com.bumptech.glide.d.c i;
    private final CenterCrop j;
    private final com.bumptech.glide.load.resource.c.f k;
    private final FitCenter l;
    private final com.bumptech.glide.load.resource.c.f m;
    private final Handler n;
    private final a o;

    public static e a(Context context) {
        if (f2867a == null) {
            synchronized (e.class) {
                if (f2867a == null) {
                    Context applicationContext = context.getApplicationContext();
                    List<com.bumptech.glide.module.a> a2 = new ManifestParser(applicationContext).a();
                    GlideBuilder glideBuilder = new GlideBuilder(applicationContext);
                    for (com.bumptech.glide.module.a a3 : a2) {
                        a3.a(applicationContext, glideBuilder);
                    }
                    f2867a = glideBuilder.a();
                    for (com.bumptech.glide.module.a a4 : a2) {
                        a4.a(applicationContext, f2867a);
                    }
                }
            }
        }
        return f2867a;
    }

    e(b bVar, g gVar, c cVar, Context context, DecodeFormat decodeFormat) {
        this.f2869c = bVar;
        this.f2870d = cVar;
        this.f2871e = gVar;
        this.f2872f = decodeFormat;
        this.f2868b = new GenericLoaderFactory(context);
        this.n = new Handler(Looper.getMainLooper());
        this.o = new a(gVar, cVar, decodeFormat);
        this.i = new com.bumptech.glide.d.c();
        j jVar = new j(cVar, decodeFormat);
        this.i.a(InputStream.class, Bitmap.class, jVar);
        com.bumptech.glide.load.resource.bitmap.e eVar = new com.bumptech.glide.load.resource.bitmap.e(cVar, decodeFormat);
        this.i.a(ParcelFileDescriptor.class, Bitmap.class, eVar);
        i iVar = new i(jVar, eVar);
        this.i.a(com.bumptech.glide.load.model.f.class, Bitmap.class, iVar);
        com.bumptech.glide.load.resource.gif.c cVar2 = new com.bumptech.glide.load.resource.gif.c(context, cVar);
        this.i.a(InputStream.class, com.bumptech.glide.load.resource.gif.b.class, cVar2);
        this.i.a(com.bumptech.glide.load.model.f.class, com.bumptech.glide.load.resource.c.a.class, new com.bumptech.glide.load.resource.c.g(iVar, cVar2, cVar));
        this.i.a(InputStream.class, File.class, new d());
        a(File.class, ParcelFileDescriptor.class, new FileDescriptorFileLoader.a());
        a(File.class, InputStream.class, new StreamFileLoader.a());
        a(Integer.TYPE, ParcelFileDescriptor.class, new FileDescriptorResourceLoader.a());
        a(Integer.TYPE, InputStream.class, new StreamResourceLoader.a());
        a(Integer.class, ParcelFileDescriptor.class, new FileDescriptorResourceLoader.a());
        a(Integer.class, InputStream.class, new StreamResourceLoader.a());
        a(String.class, ParcelFileDescriptor.class, new FileDescriptorStringLoader.a());
        a(String.class, InputStream.class, new StreamStringLoader.a());
        a(Uri.class, ParcelFileDescriptor.class, new FileDescriptorUriLoader.a());
        a(Uri.class, InputStream.class, new StreamUriLoader.a());
        a(URL.class, InputStream.class, new d.a());
        a(com.bumptech.glide.load.model.c.class, InputStream.class, new a.C0041a());
        a(byte[].class, InputStream.class, new b.a());
        this.f2874h.a(Bitmap.class, com.bumptech.glide.load.resource.bitmap.f.class, new GlideBitmapDrawableTranscoder(context.getResources(), cVar));
        this.f2874h.a(com.bumptech.glide.load.resource.c.a.class, com.bumptech.glide.load.resource.a.b.class, new com.bumptech.glide.load.resource.transcode.a(new GlideBitmapDrawableTranscoder(context.getResources(), cVar)));
        this.j = new CenterCrop(cVar);
        this.k = new com.bumptech.glide.load.resource.c.f(cVar, this.j);
        this.l = new FitCenter(cVar);
        this.m = new com.bumptech.glide.load.resource.c.f(cVar, this.l);
    }

    public c a() {
        return this.f2870d;
    }

    /* access modifiers changed from: package-private */
    public <Z, R> com.bumptech.glide.load.resource.transcode.b<Z, R> a(Class<Z> cls, Class<R> cls2) {
        return this.f2874h.a(cls, cls2);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Class<Z>, java.lang.Class] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T, Z> com.bumptech.glide.d.b<T, Z> b(java.lang.Class<T> r2, java.lang.Class<Z> r3) {
        /*
            r1 = this;
            com.bumptech.glide.d.c r0 = r1.i
            com.bumptech.glide.d.b r0 = r0.a(r2, r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.e.b(java.lang.Class, java.lang.Class):com.bumptech.glide.d.b");
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Class, java.lang.Class<R>] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <R> com.bumptech.glide.request.b.j<R> a(android.widget.ImageView r2, java.lang.Class<R> r3) {
        /*
            r1 = this;
            com.bumptech.glide.request.b.f r0 = r1.f2873g
            com.bumptech.glide.request.b.j r0 = r0.a(r2, r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.e.a(android.widget.ImageView, java.lang.Class):com.bumptech.glide.request.b.j");
    }

    /* access modifiers changed from: package-private */
    public com.bumptech.glide.load.engine.b b() {
        return this.f2869c;
    }

    /* access modifiers changed from: package-private */
    public com.bumptech.glide.load.resource.c.f c() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public com.bumptech.glide.load.resource.c.f d() {
        return this.m;
    }

    private GenericLoaderFactory f() {
        return this.f2868b;
    }

    public void e() {
        h.a();
        this.f2871e.a();
        this.f2870d.a();
    }

    public void a(int i2) {
        h.a();
        this.f2871e.a(i2);
        this.f2870d.a(i2);
    }

    public static void a(com.bumptech.glide.request.b.j<?> jVar) {
        h.a();
        com.bumptech.glide.request.a c2 = jVar.c();
        if (c2 != null) {
            c2.d();
            jVar.a((com.bumptech.glide.request.a) null);
        }
    }

    public <T, Y> void a(Class<T> cls, Class<Y> cls2, l<T, Y> lVar) {
        l<T, Y> a2 = this.f2868b.a(cls, cls2, lVar);
        if (a2 != null) {
            a2.a();
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Class<Y>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T, Y> com.bumptech.glide.load.model.k<T, Y> a(java.lang.Class<T> r2, java.lang.Class<Y> r3, android.content.Context r4) {
        /*
            if (r2 != 0) goto L_0x0014
            java.lang.String r0 = "Glide"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x0012
            java.lang.String r0 = "Glide"
            java.lang.String r1 = "Unable to load null model, setting placeholder only"
            android.util.Log.d(r0, r1)
        L_0x0012:
            r0 = 0
        L_0x0013:
            return r0
        L_0x0014:
            com.bumptech.glide.e r0 = a(r4)
            com.bumptech.glide.load.model.GenericLoaderFactory r0 = r0.f()
            com.bumptech.glide.load.model.k r0 = r0.a(r2, r3)
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.e.a(java.lang.Class, java.lang.Class, android.content.Context):com.bumptech.glide.load.model.k");
    }

    public static <T> k<T, InputStream> a(Class<T> cls, Context context) {
        return a(cls, InputStream.class, context);
    }

    public static <T> k<T, ParcelFileDescriptor> b(Class<T> cls, Context context) {
        return a(cls, ParcelFileDescriptor.class, context);
    }

    public static f b(Context context) {
        return com.bumptech.glide.manager.k.a().a(context);
    }
}
