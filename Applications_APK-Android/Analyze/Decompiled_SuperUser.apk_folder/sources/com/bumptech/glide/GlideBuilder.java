package com.bumptech.glide;

import android.content.Context;
import android.os.Build;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.a.d;
import com.bumptech.glide.load.engine.a.f;
import com.bumptech.glide.load.engine.b;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.load.engine.cache.a;
import com.bumptech.glide.load.engine.cache.g;
import com.bumptech.glide.load.engine.executor.FifoPriorityThreadPoolExecutor;
import java.util.concurrent.ExecutorService;

public class GlideBuilder {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2742a;

    /* renamed from: b  reason: collision with root package name */
    private b f2743b;

    /* renamed from: c  reason: collision with root package name */
    private c f2744c;

    /* renamed from: d  reason: collision with root package name */
    private g f2745d;

    /* renamed from: e  reason: collision with root package name */
    private ExecutorService f2746e;

    /* renamed from: f  reason: collision with root package name */
    private ExecutorService f2747f;

    /* renamed from: g  reason: collision with root package name */
    private DecodeFormat f2748g;

    /* renamed from: h  reason: collision with root package name */
    private a.C0040a f2749h;

    public GlideBuilder(Context context) {
        this.f2742a = context.getApplicationContext();
    }

    /* access modifiers changed from: package-private */
    public e a() {
        if (this.f2746e == null) {
            this.f2746e = new FifoPriorityThreadPoolExecutor(Math.max(1, Runtime.getRuntime().availableProcessors()));
        }
        if (this.f2747f == null) {
            this.f2747f = new FifoPriorityThreadPoolExecutor(1);
        }
        MemorySizeCalculator memorySizeCalculator = new MemorySizeCalculator(this.f2742a);
        if (this.f2744c == null) {
            if (Build.VERSION.SDK_INT >= 11) {
                this.f2744c = new f(memorySizeCalculator.b());
            } else {
                this.f2744c = new d();
            }
        }
        if (this.f2745d == null) {
            this.f2745d = new com.bumptech.glide.load.engine.cache.f(memorySizeCalculator.a());
        }
        if (this.f2749h == null) {
            this.f2749h = new InternalCacheDiskCacheFactory(this.f2742a);
        }
        if (this.f2743b == null) {
            this.f2743b = new b(this.f2745d, this.f2749h, this.f2747f, this.f2746e);
        }
        if (this.f2748g == null) {
            this.f2748g = DecodeFormat.f2916d;
        }
        return new e(this.f2743b, this.f2745d, this.f2744c, this.f2742a, this.f2748g);
    }
}
