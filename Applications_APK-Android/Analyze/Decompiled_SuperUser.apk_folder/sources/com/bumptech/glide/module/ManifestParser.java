package com.bumptech.glide.module;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.ArrayList;
import java.util.List;

public final class ManifestParser {

    /* renamed from: a  reason: collision with root package name */
    private final Context f3297a;

    public ManifestParser(Context context) {
        this.f3297a = context;
    }

    public List<a> a() {
        ArrayList arrayList = new ArrayList();
        try {
            ApplicationInfo applicationInfo = this.f3297a.getPackageManager().getApplicationInfo(this.f3297a.getPackageName(), FileUtils.FileMode.MODE_IWUSR);
            if (applicationInfo.metaData != null) {
                for (String next : applicationInfo.metaData.keySet()) {
                    if ("GlideModule".equals(applicationInfo.metaData.get(next))) {
                        arrayList.add(a(next));
                    }
                }
            }
            return arrayList;
        } catch (PackageManager.NameNotFoundException e2) {
            throw new RuntimeException("Unable to find metadata to parse GlideModules", e2);
        }
    }

    private static a a(String str) {
        Object obj;
        Object obj2;
        try {
            try {
                Object newInstance = Class.forName(str).newInstance();
                if (newInstance instanceof a) {
                    return (a) newInstance;
                }
                throw new RuntimeException("Expected instanceof GlideModule, but found: " + newInstance);
            } catch (InstantiationException e2) {
                throw new RuntimeException("Unable to instantiate GlideModule implementation for " + obj2, e2);
            } catch (IllegalAccessException e3) {
                throw new RuntimeException("Unable to instantiate GlideModule implementation for " + obj, e3);
            }
        } catch (ClassNotFoundException e4) {
            throw new IllegalArgumentException("Unable to find GlideModule implementation", e4);
        }
    }
}
