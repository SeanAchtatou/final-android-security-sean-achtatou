package com.bumptech.glide;

import android.content.Context;
import android.widget.ImageView;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.f;
import com.bumptech.glide.load.resource.a.b;
import com.bumptech.glide.manager.g;
import com.bumptech.glide.manager.m;
import com.bumptech.glide.request.b.j;

/* compiled from: DrawableRequestBuilder */
public class a<ModelType> extends c<ModelType, f, com.bumptech.glide.load.resource.c.a, b> {
    public /* synthetic */ c b(com.bumptech.glide.load.a aVar) {
        return a((com.bumptech.glide.load.a<f>) aVar);
    }

    public /* synthetic */ c b(d dVar) {
        return a((d<f, com.bumptech.glide.load.resource.c.a>) dVar);
    }

    public /* synthetic */ c b(com.bumptech.glide.load.f[] fVarArr) {
        return a((com.bumptech.glide.load.f<com.bumptech.glide.load.resource.c.a>[]) fVarArr);
    }

    a(Context context, Class<ModelType> cls, com.bumptech.glide.d.f<ModelType, f, com.bumptech.glide.load.resource.c.a, b> fVar, e eVar, m mVar, g gVar) {
        super(context, cls, fVar, b.class, eVar, mVar, gVar);
        c();
    }

    public a<ModelType> a(d<f, com.bumptech.glide.load.resource.c.a> dVar) {
        super.b((d) dVar);
        return this;
    }

    public a<ModelType> a() {
        return a((com.bumptech.glide.load.f<com.bumptech.glide.load.resource.c.a>[]) new com.bumptech.glide.load.f[]{this.f2822c.c()});
    }

    public a<ModelType> b() {
        return a((com.bumptech.glide.load.f<com.bumptech.glide.load.resource.c.a>[]) new com.bumptech.glide.load.f[]{this.f2822c.d()});
    }

    public a<ModelType> a(com.bumptech.glide.load.f<com.bumptech.glide.load.resource.c.a>... fVarArr) {
        super.b((com.bumptech.glide.load.f[]) fVarArr);
        return this;
    }

    public final a<ModelType> c() {
        super.a((com.bumptech.glide.request.a.d) new com.bumptech.glide.request.a.a());
        return this;
    }

    /* renamed from: a */
    public a<ModelType> b(DiskCacheStrategy diskCacheStrategy) {
        super.b(diskCacheStrategy);
        return this;
    }

    /* renamed from: a */
    public a<ModelType> b(boolean z) {
        super.b(z);
        return this;
    }

    /* renamed from: a */
    public a<ModelType> b(int i, int i2) {
        super.b(i, i2);
        return this;
    }

    public a<ModelType> a(com.bumptech.glide.load.a<f> aVar) {
        super.b((com.bumptech.glide.load.a) aVar);
        return this;
    }

    /* renamed from: a */
    public a<ModelType> b(com.bumptech.glide.load.b bVar) {
        super.b(bVar);
        return this;
    }

    /* renamed from: a */
    public a<ModelType> b(ModelType modeltype) {
        super.b((Object) modeltype);
        return this;
    }

    /* renamed from: d */
    public a<ModelType> g() {
        return (a) super.clone();
    }

    public j<b> a(ImageView imageView) {
        return super.a(imageView);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        b();
    }

    /* access modifiers changed from: package-private */
    public void f() {
        a();
    }
}
