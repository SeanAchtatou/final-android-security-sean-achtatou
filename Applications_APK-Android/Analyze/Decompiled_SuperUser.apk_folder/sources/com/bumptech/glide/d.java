package com.bumptech.glide;

import android.content.Context;
import com.bumptech.glide.d.e;
import com.bumptech.glide.f;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.resource.transcode.b;
import com.bumptech.glide.manager.g;
import com.bumptech.glide.manager.m;

/* compiled from: GenericTranscodeRequest */
public class d<ModelType, DataType, ResourceType> extends c<ModelType, DataType, ResourceType, ResourceType> {

    /* renamed from: g  reason: collision with root package name */
    private final k<ModelType, DataType> f2853g;

    /* renamed from: h  reason: collision with root package name */
    private final Class<DataType> f2854h;
    private final Class<ResourceType> i;
    private final f.c j;

    private static <A, T, Z, R> com.bumptech.glide.d.f<A, T, Z, R> a(e eVar, k<A, T> kVar, Class<T> cls, Class<Z> cls2, b<Z, R> bVar) {
        return new e(kVar, bVar, eVar.b(cls, cls2));
    }

    d(Context context, e eVar, Class<ModelType> cls, k<ModelType, DataType> kVar, Class<DataType> cls2, Class<ResourceType> cls3, m mVar, g gVar, f.c cVar) {
        super(context, cls, a(eVar, kVar, cls2, cls3, com.bumptech.glide.load.resource.transcode.d.b()), cls3, eVar, mVar, gVar);
        this.f2853g = kVar;
        this.f2854h = cls2;
        this.i = cls3;
        this.j = cVar;
    }
}
