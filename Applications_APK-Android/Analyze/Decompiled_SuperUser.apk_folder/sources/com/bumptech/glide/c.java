package com.bumptech.glide;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.d.a;
import com.bumptech.glide.f.h;
import com.bumptech.glide.load.b;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.f;
import com.bumptech.glide.manager.g;
import com.bumptech.glide.manager.m;
import com.bumptech.glide.request.GenericRequest;
import com.bumptech.glide.request.a.d;
import com.bumptech.glide.request.a.e;
import com.bumptech.glide.request.b.j;

/* compiled from: GenericRequestBuilder */
public class c<ModelType, DataType, ResourceType, TranscodeType> implements Cloneable {
    private boolean A;
    private Drawable B;
    private int C;

    /* renamed from: a  reason: collision with root package name */
    protected final Class<ModelType> f2820a;

    /* renamed from: b  reason: collision with root package name */
    protected final Context f2821b;

    /* renamed from: c  reason: collision with root package name */
    protected final e f2822c;

    /* renamed from: d  reason: collision with root package name */
    protected final Class<TranscodeType> f2823d;

    /* renamed from: e  reason: collision with root package name */
    protected final m f2824e;

    /* renamed from: f  reason: collision with root package name */
    protected final g f2825f;

    /* renamed from: g  reason: collision with root package name */
    private a<ModelType, DataType, ResourceType, TranscodeType> f2826g;

    /* renamed from: h  reason: collision with root package name */
    private ModelType f2827h;
    private b i = com.bumptech.glide.e.a.a();
    private boolean j;
    private int k;
    private int l;
    private com.bumptech.glide.request.c<? super ModelType, TranscodeType> m;
    private Float n;
    private c<?, ?, ?, TranscodeType> o;
    private Float p = Float.valueOf(1.0f);
    private Drawable q;
    private Drawable r;
    private Priority s = null;
    private boolean t = true;
    private d<TranscodeType> u = e.a();
    private int v = -1;
    private int w = -1;
    private DiskCacheStrategy x = DiskCacheStrategy.RESULT;
    private f<ResourceType> y = com.bumptech.glide.load.resource.d.b();
    private boolean z;

    c(Context context, Class<ModelType> cls, com.bumptech.glide.d.f<ModelType, DataType, ResourceType, TranscodeType> fVar, Class<TranscodeType> cls2, e eVar, m mVar, g gVar) {
        a<ModelType, DataType, ResourceType, TranscodeType> aVar = null;
        this.f2821b = context;
        this.f2820a = cls;
        this.f2823d = cls2;
        this.f2822c = eVar;
        this.f2824e = mVar;
        this.f2825f = gVar;
        this.f2826g = fVar != null ? new a<>(fVar) : aVar;
        if (context == null) {
            throw new NullPointerException("Context can't be null");
        } else if (cls != null && fVar == null) {
            throw new NullPointerException("LoadProvider must not be null");
        }
    }

    public c<ModelType, DataType, ResourceType, TranscodeType> b(com.bumptech.glide.load.d dVar) {
        if (this.f2826g != null) {
            this.f2826g.a(dVar);
        }
        return this;
    }

    public c<ModelType, DataType, ResourceType, TranscodeType> b(com.bumptech.glide.load.a aVar) {
        if (this.f2826g != null) {
            this.f2826g.a(aVar);
        }
        return this;
    }

    public c<ModelType, DataType, ResourceType, TranscodeType> b(DiskCacheStrategy diskCacheStrategy) {
        this.x = diskCacheStrategy;
        return this;
    }

    public c<ModelType, DataType, ResourceType, TranscodeType> b(f... fVarArr) {
        this.z = true;
        if (fVarArr.length == 1) {
            this.y = fVarArr[0];
        } else {
            this.y = new com.bumptech.glide.load.c(fVarArr);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public c<ModelType, DataType, ResourceType, TranscodeType> a(d dVar) {
        if (dVar == null) {
            throw new NullPointerException("Animation factory must not be null!");
        }
        this.u = dVar;
        return this;
    }

    public c<ModelType, DataType, ResourceType, TranscodeType> b(boolean z2) {
        this.t = !z2;
        return this;
    }

    public c<ModelType, DataType, ResourceType, TranscodeType> b(int i2, int i3) {
        if (!h.a(i2, i3)) {
            throw new IllegalArgumentException("Width and height must be Target#SIZE_ORIGINAL or > 0");
        }
        this.w = i2;
        this.v = i3;
        return this;
    }

    public c<ModelType, DataType, ResourceType, TranscodeType> b(b bVar) {
        if (bVar == null) {
            throw new NullPointerException("Signature must not be null");
        }
        this.i = bVar;
        return this;
    }

    public c<ModelType, DataType, ResourceType, TranscodeType> b(Object obj) {
        this.f2827h = obj;
        this.j = true;
        return this;
    }

    /* renamed from: g */
    public c<ModelType, DataType, ResourceType, TranscodeType> clone() {
        try {
            c<ModelType, DataType, ResourceType, TranscodeType> cVar = (c) super.clone();
            cVar.f2826g = this.f2826g != null ? this.f2826g.clone() : null;
            return cVar;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    public <Y extends j<TranscodeType>> Y a(j jVar) {
        h.a();
        if (jVar == null) {
            throw new IllegalArgumentException("You must pass in a non null Target");
        } else if (!this.j) {
            throw new IllegalArgumentException("You must first set a model (try #load())");
        } else {
            com.bumptech.glide.request.a c2 = jVar.c();
            if (c2 != null) {
                c2.d();
                this.f2824e.b(c2);
                c2.a();
            }
            com.bumptech.glide.request.a b2 = b(jVar);
            jVar.a(b2);
            this.f2825f.a(jVar);
            this.f2824e.a(b2);
            return jVar;
        }
    }

    public j<TranscodeType> a(ImageView imageView) {
        h.a();
        if (imageView == null) {
            throw new IllegalArgumentException("You must pass in a non null View");
        }
        if (!this.z && imageView.getScaleType() != null) {
            switch (AnonymousClass1.f2828a[imageView.getScaleType().ordinal()]) {
                case 1:
                    f();
                    break;
                case 2:
                case 3:
                case 4:
                    e();
                    break;
            }
        }
        return a(this.f2822c.a(imageView, this.f2823d));
    }

    /* renamed from: com.bumptech.glide.c$1  reason: invalid class name */
    /* compiled from: GenericRequestBuilder */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2828a = new int[ImageView.ScaleType.values().length];

        static {
            try {
                f2828a[ImageView.ScaleType.CENTER_CROP.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f2828a[ImageView.ScaleType.FIT_CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f2828a[ImageView.ScaleType.FIT_START.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f2828a[ImageView.ScaleType.FIT_END.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
    }

    /* access modifiers changed from: package-private */
    public void e() {
    }

    private Priority a() {
        if (this.s == Priority.LOW) {
            return Priority.NORMAL;
        }
        if (this.s == Priority.NORMAL) {
            return Priority.HIGH;
        }
        return Priority.IMMEDIATE;
    }

    private com.bumptech.glide.request.a b(j jVar) {
        if (this.s == null) {
            this.s = Priority.NORMAL;
        }
        return a(jVar, null);
    }

    private com.bumptech.glide.request.a a(j jVar, com.bumptech.glide.request.e eVar) {
        if (this.o != null) {
            if (this.A) {
                throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
            }
            if (this.o.u.equals(e.a())) {
                this.o.u = this.u;
            }
            if (this.o.s == null) {
                this.o.s = a();
            }
            if (h.a(this.w, this.v) && !h.a(this.o.w, this.o.v)) {
                this.o.b(this.w, this.v);
            }
            com.bumptech.glide.request.e eVar2 = new com.bumptech.glide.request.e(eVar);
            com.bumptech.glide.request.a a2 = a(jVar, this.p.floatValue(), this.s, eVar2);
            this.A = true;
            com.bumptech.glide.request.a a3 = this.o.a(jVar, eVar2);
            this.A = false;
            eVar2.a(a2, a3);
            return eVar2;
        } else if (this.n == null) {
            return a(jVar, this.p.floatValue(), this.s, eVar);
        } else {
            com.bumptech.glide.request.e eVar3 = new com.bumptech.glide.request.e(eVar);
            eVar3.a(a(jVar, this.p.floatValue(), this.s, eVar3), a(jVar, this.n.floatValue(), a(), eVar3));
            return eVar3;
        }
    }

    private com.bumptech.glide.request.a a(j<TranscodeType> jVar, float f2, Priority priority, com.bumptech.glide.request.b bVar) {
        return GenericRequest.a(this.f2826g, this.f2827h, this.i, this.f2821b, priority, jVar, f2, this.q, this.k, this.r, this.l, this.B, this.C, this.m, bVar, this.f2822c.b(), this.y, this.f2823d, this.t, this.u, this.w, this.v, this.x);
    }
}
