package com.bumptech.glide.d;

import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.resource.transcode.b;

/* compiled from: LoadProvider */
public interface f<A, T, Z, R> extends b<T, Z> {
    k<A, T> e();

    b<Z, R> f();
}
