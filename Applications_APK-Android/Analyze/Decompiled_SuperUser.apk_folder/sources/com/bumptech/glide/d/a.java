package com.bumptech.glide.d;

import com.bumptech.glide.load.d;
import com.bumptech.glide.load.e;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.resource.transcode.b;
import java.io.File;

/* compiled from: ChildLoadProvider */
public class a<A, T, Z, R> implements f<A, T, Z, R>, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final f<A, T, Z, R> f2855a;

    /* renamed from: b  reason: collision with root package name */
    private d<File, Z> f2856b;

    /* renamed from: c  reason: collision with root package name */
    private d<T, Z> f2857c;

    /* renamed from: d  reason: collision with root package name */
    private e<Z> f2858d;

    /* renamed from: e  reason: collision with root package name */
    private b<Z, R> f2859e;

    /* renamed from: f  reason: collision with root package name */
    private com.bumptech.glide.load.a<T> f2860f;

    public a(f<A, T, Z, R> fVar) {
        this.f2855a = fVar;
    }

    public k<A, T> e() {
        return this.f2855a.e();
    }

    public void a(d dVar) {
        this.f2857c = dVar;
    }

    public void a(com.bumptech.glide.load.a aVar) {
        this.f2860f = aVar;
    }

    public d<File, Z> a() {
        if (this.f2856b != null) {
            return this.f2856b;
        }
        return this.f2855a.a();
    }

    public d<T, Z> b() {
        if (this.f2857c != null) {
            return this.f2857c;
        }
        return this.f2855a.b();
    }

    public com.bumptech.glide.load.a<T> c() {
        if (this.f2860f != null) {
            return this.f2860f;
        }
        return this.f2855a.c();
    }

    public e<Z> d() {
        if (this.f2858d != null) {
            return this.f2858d;
        }
        return this.f2855a.d();
    }

    public b<Z, R> f() {
        if (this.f2859e != null) {
            return this.f2859e;
        }
        return this.f2855a.f();
    }

    /* renamed from: g */
    public a<A, T, Z, R> clone() {
        try {
            return (a) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }
}
