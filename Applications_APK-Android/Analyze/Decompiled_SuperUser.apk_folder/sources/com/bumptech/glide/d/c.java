package com.bumptech.glide.d;

import com.bumptech.glide.f.g;
import java.util.HashMap;
import java.util.Map;

/* compiled from: DataLoadProviderRegistry */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final g f2861a = new g();

    /* renamed from: b  reason: collision with root package name */
    private final Map<g, b<?, ?>> f2862b = new HashMap();

    public <T, Z> void a(Class<T> cls, Class<Z> cls2, b<T, Z> bVar) {
        this.f2862b.put(new g(cls, cls2), bVar);
    }

    public <T, Z> b<T, Z> a(Class<T> cls, Class<Z> cls2) {
        b<T, Z> bVar;
        synchronized (f2861a) {
            f2861a.a(cls, cls2);
            bVar = this.f2862b.get(f2861a);
        }
        if (bVar == null) {
            return d.e();
        }
        return bVar;
    }
}
