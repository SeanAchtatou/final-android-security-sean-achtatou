package com.bumptech.glide.d;

import com.bumptech.glide.load.a;
import com.bumptech.glide.load.d;
import com.bumptech.glide.load.model.k;
import com.bumptech.glide.load.resource.transcode.b;
import java.io.File;

/* compiled from: FixedLoadProvider */
public class e<A, T, Z, R> implements f<A, T, Z, R> {

    /* renamed from: a  reason: collision with root package name */
    private final k<A, T> f2864a;

    /* renamed from: b  reason: collision with root package name */
    private final b<Z, R> f2865b;

    /* renamed from: c  reason: collision with root package name */
    private final b<T, Z> f2866c;

    public e(k<A, T> kVar, b<Z, R> bVar, b<T, Z> bVar2) {
        if (kVar == null) {
            throw new NullPointerException("ModelLoader must not be null");
        }
        this.f2864a = kVar;
        if (bVar == null) {
            throw new NullPointerException("Transcoder must not be null");
        }
        this.f2865b = bVar;
        if (bVar2 == null) {
            throw new NullPointerException("DataLoadProvider must not be null");
        }
        this.f2866c = bVar2;
    }

    public k<A, T> e() {
        return this.f2864a;
    }

    public b<Z, R> f() {
        return this.f2865b;
    }

    public d<File, Z> a() {
        return this.f2866c.a();
    }

    public d<T, Z> b() {
        return this.f2866c.b();
    }

    public a<T> c() {
        return this.f2866c.c();
    }

    public com.bumptech.glide.load.e<Z> d() {
        return this.f2866c.d();
    }
}
