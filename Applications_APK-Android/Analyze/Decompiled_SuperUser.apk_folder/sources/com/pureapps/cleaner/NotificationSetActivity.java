package com.pureapps.cleaner;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;
import butterknife.BindView;
import com.kingouser.com.BaseActivity;
import com.kingouser.com.R;
import com.lody.virtual.helper.utils.FileUtils;
import com.pureapps.cleaner.adapter.NotificationSetAdapter;
import com.pureapps.cleaner.b.c;
import com.pureapps.cleaner.bean.l;
import com.pureapps.cleaner.bean.m;
import com.pureapps.cleaner.service.NotificationMonitorService;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.k;
import com.pureapps.cleaner.view.CustomLinearLayoutManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import me.everything.android.ui.overscroll.g;

public class NotificationSetActivity extends BaseActivity implements c {
    @BindView(R.id.e1)
    TextView floatView;
    @BindView(R.id.dy)
    RecyclerView mRecyclerView;
    private a n = null;
    /* access modifiers changed from: private */
    public NotificationSetAdapter p = null;
    private CustomLinearLayoutManager q;

    public static void a(Context context) {
        context.startActivity(new Intent(context, NotificationSetActivity.class));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.pureapps.cleaner.analytic.a.a(this).b(this.o, "NotificationSetView");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.pureapps.cleaner.b.a.a(this);
        setContentView((int) R.layout.a_);
        ActionBar f2 = f();
        if (f2 != null) {
            f2.d(true);
            f2.a(true);
        }
        this.q = new CustomLinearLayoutManager(this);
        this.q.b(1);
        this.mRecyclerView.setLayoutManager(this.q);
        this.mRecyclerView.setHasFixedSize(true);
        g.a(this.mRecyclerView, 0);
        this.n = new a();
        this.n.executeOnExecutor(k.a().b(), new Void[0]);
        this.p = new NotificationSetAdapter(null, this);
        this.mRecyclerView.setAdapter(this.p);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.pureapps.cleaner.b.a.b(this);
        k.a(this.n, true);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void a(int i, long j, Object obj) {
        switch (i) {
            case 22:
                this.floatView.setVisibility(i.a(this).l() ? 8 : 0);
                this.q.d(i.a(this).l());
                return;
            default:
                return;
        }
    }

    class a extends AsyncTask<Void, Void, ArrayList<l>> {
        a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ArrayList<l> doInBackground(Void... voidArr) {
            boolean z;
            ArrayList<l> arrayList = new ArrayList<>();
            PackageManager packageManager = NotificationSetActivity.this.getPackageManager();
            List<PackageInfo> installedPackages = packageManager.getInstalledPackages(0);
            HashSet<String> a2 = NotificationMonitorService.a(i.a(NotificationSetActivity.this).m());
            NotificationSetActivity.this.j();
            for (int i = 0; i < installedPackages.size(); i++) {
                PackageInfo packageInfo = installedPackages.get(i);
                if ((packageInfo.applicationInfo.flags & 1) <= 0 || (packageInfo.applicationInfo.flags & FileUtils.FileMode.MODE_IWUSR) != 0) {
                    z = true;
                } else {
                    z = false;
                }
                String str = packageInfo.packageName;
                if (z && !str.equals(NotificationSetActivity.this.getPackageName())) {
                    String charSequence = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
                    l lVar = new l();
                    lVar.f5650a = charSequence;
                    lVar.f5651b = str;
                    lVar.f5652c = false;
                    if (a2.contains(str)) {
                        lVar.f5652c = true;
                    }
                    arrayList.add(lVar);
                }
            }
            return arrayList;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(ArrayList<l> arrayList) {
            super.onPostExecute(arrayList);
            Collections.sort(arrayList, new Comparator<l>() {
                /* renamed from: a */
                public int compare(l lVar, l lVar2) {
                    if (lVar.f5652c != lVar2.f5652c && !lVar.f5652c) {
                        return 1;
                    }
                    return -1;
                }
            });
            if (NotificationSetActivity.this.p != null) {
                NotificationSetActivity.this.p.a(arrayList);
            }
        }
    }

    public List j() {
        ArrayList arrayList = new ArrayList();
        m a2 = m.a();
        a2.a(this);
        arrayList.addAll(a2.f5659g);
        arrayList.addAll(a2.f5655c);
        return arrayList;
    }
}
