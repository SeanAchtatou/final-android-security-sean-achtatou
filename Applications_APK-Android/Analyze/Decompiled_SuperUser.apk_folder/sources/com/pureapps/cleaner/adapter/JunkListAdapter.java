package com.pureapps.cleaner.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.kingouser.com.application.App;
import com.pureapps.cleaner.JunkClearActivity;
import com.pureapps.cleaner.bean.d;
import com.pureapps.cleaner.bean.e;
import com.pureapps.cleaner.bean.g;
import com.pureapps.cleaner.bean.h;
import com.pureapps.cleaner.util.l;
import com.pureapps.cleaner.view.AnimatedExpandableListView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class JunkListAdapter extends AnimatedExpandableListView.a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final WeakReference<JunkClearActivity> f5505a;

    /* renamed from: b  reason: collision with root package name */
    private LayoutInflater f5506b;

    /* renamed from: c  reason: collision with root package name */
    private ArrayList<h> f5507c = new ArrayList<>();

    /* renamed from: d  reason: collision with root package name */
    private boolean f5508d = true;

    /* renamed from: e  reason: collision with root package name */
    private boolean f5509e = false;

    /* renamed from: f  reason: collision with root package name */
    private h f5510f = null;

    /* renamed from: g  reason: collision with root package name */
    private h f5511g = null;

    /* renamed from: h  reason: collision with root package name */
    private h f5512h = null;
    /* access modifiers changed from: private */
    public AnimatedExpandableListView i = null;

    public class ChildHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private ChildHolder f5516a;

        public ChildHolder_ViewBinding(ChildHolder childHolder, View view) {
            this.f5516a = childHolder;
            childHolder.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.fd, "field 'tvTitle'", TextView.class);
            childHolder.ivIcon = (ImageView) Utils.findRequiredViewAsType(view, R.id.j3, "field 'ivIcon'", ImageView.class);
            childHolder.tvSize = (TextView) Utils.findRequiredViewAsType(view, R.id.j4, "field 'tvSize'", TextView.class);
            childHolder.mSelected = (CheckBox) Utils.findRequiredViewAsType(view, R.id.j5, "field 'mSelected'", CheckBox.class);
        }

        public void unbind() {
            ChildHolder childHolder = this.f5516a;
            if (childHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f5516a = null;
            childHolder.tvTitle = null;
            childHolder.ivIcon = null;
            childHolder.tvSize = null;
            childHolder.mSelected = null;
        }
    }

    public class GroupHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private GroupHolder f5518a;

        /* renamed from: b  reason: collision with root package name */
        private View f5519b;

        /* renamed from: c  reason: collision with root package name */
        private View f5520c;

        public GroupHolder_ViewBinding(final GroupHolder groupHolder, View view) {
            this.f5518a = groupHolder;
            groupHolder.ivArrow = (ImageView) Utils.findRequiredViewAsType(view, R.id.j9, "field 'ivArrow'", ImageView.class);
            groupHolder.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.fd, "field 'tvTitle'", TextView.class);
            groupHolder.tvSize = (TextView) Utils.findRequiredViewAsType(view, R.id.j4, "field 'tvSize'", TextView.class);
            View findRequiredView = Utils.findRequiredView(view, R.id.j_, "field 'mSelectedAll' and method 'onClick'");
            groupHolder.mSelectedAll = (CheckBox) Utils.castView(findRequiredView, R.id.j_, "field 'mSelectedAll'", CheckBox.class);
            this.f5519b = findRequiredView;
            findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
                public void doClick(View view) {
                    groupHolder.onClick(view);
                }
            });
            groupHolder.mProgressBar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.d6, "field 'mProgressBar'", ProgressBar.class);
            View findRequiredView2 = Utils.findRequiredView(view, R.id.j8, "field 'mExpandLayout' and method 'onClick'");
            groupHolder.mExpandLayout = (LinearLayout) Utils.castView(findRequiredView2, R.id.j8, "field 'mExpandLayout'", LinearLayout.class);
            this.f5520c = findRequiredView2;
            findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
                public void doClick(View view) {
                    groupHolder.onClick(view);
                }
            });
            groupHolder.mBomLine = Utils.findRequiredView(view, R.id.ja, "field 'mBomLine'");
        }

        public void unbind() {
            GroupHolder groupHolder = this.f5518a;
            if (groupHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f5518a = null;
            groupHolder.ivArrow = null;
            groupHolder.tvTitle = null;
            groupHolder.tvSize = null;
            groupHolder.mSelectedAll = null;
            groupHolder.mProgressBar = null;
            groupHolder.mExpandLayout = null;
            groupHolder.mBomLine = null;
            this.f5519b.setOnClickListener(null);
            this.f5519b = null;
            this.f5520c.setOnClickListener(null);
            this.f5520c = null;
        }
    }

    public JunkListAdapter(JunkClearActivity junkClearActivity, AnimatedExpandableListView animatedExpandableListView) {
        this.f5505a = new WeakReference<>(junkClearActivity);
        this.i = animatedExpandableListView;
        this.f5506b = LayoutInflater.from(junkClearActivity);
        this.f5510f = new h(0);
        this.f5510f.f5633b = junkClearActivity.getString(R.string.cf);
        this.f5507c.add(this.f5510f);
        this.f5511g = new h(1);
        this.f5511g.f5633b = junkClearActivity.getString(R.string.cd);
        this.f5507c.add(this.f5511g);
        this.f5512h = new h(3);
        this.f5512h.f5633b = junkClearActivity.getString(R.string.ce);
        this.f5507c.add(this.f5512h);
    }

    public void a(h hVar, h hVar2, h hVar3) {
        this.f5510f = hVar;
        this.f5511g = hVar2;
        this.f5512h = hVar3;
        if (hVar != null && hVar.f5636e.size() >= this.f5507c.get(0).f5636e.size()) {
            e a2 = a(hVar);
            e a3 = a(this.f5507c.get(0));
            if (a2 == null || a3 == null || a2.f5629e >= a3.f5629e) {
                this.f5507c.set(0, hVar);
            }
        }
        if (hVar2 != null && hVar2.f5636e.size() >= this.f5507c.get(1).f5636e.size()) {
            this.f5507c.set(1, hVar2);
        }
        if (hVar3 != null && hVar3.f5636e.size() >= this.f5507c.get(2).f5636e.size()) {
            this.f5507c.set(2, hVar3);
        }
        notifyDataSetChanged();
    }

    private e a(h hVar) {
        String string = App.a().getString(R.string.cg);
        Iterator<g> it = hVar.f5636e.iterator();
        while (it.hasNext()) {
            g next = it.next();
            if ((next instanceof e) && string.equals(((e) next).f5618a)) {
                return (e) next;
            }
        }
        return null;
    }

    public void notifyDataSetChanged() {
        int i2 = 0;
        int i3 = 0;
        while (this.f5510f != null && this.f5510f.f5636e != null && i3 < this.f5510f.f5636e.size() && this.f5510f.f5636e.size() >= 2) {
            if (this.f5510f.f5636e.get(i3).f5629e <= 0) {
                this.f5510f.f5636e.remove(i3);
            }
            i3++;
        }
        int i4 = 0;
        while (this.f5511g != null && this.f5511g.f5636e != null && i4 < this.f5511g.f5636e.size() && this.f5511g.f5636e.size() >= 2) {
            if (this.f5511g.f5636e.get(i4).f5629e <= 0) {
                this.f5511g.f5636e.remove(i4);
            }
            i4++;
        }
        while (this.f5512h != null && this.f5512h.f5636e != null && i2 < this.f5512h.f5636e.size() && this.f5512h.f5636e.size() >= 2) {
            if (this.f5512h.f5636e.get(i2).f5629e <= 0) {
                this.f5512h.f5636e.remove(i2);
            }
            i2++;
        }
        super.notifyDataSetChanged();
    }

    public h a() {
        Iterator<h> it = this.f5507c.iterator();
        while (it.hasNext()) {
            h next = it.next();
            if (next.f5632a == 0) {
                return next;
            }
        }
        return null;
    }

    public h b() {
        Iterator<h> it = this.f5507c.iterator();
        while (it.hasNext()) {
            h next = it.next();
            if (next.f5632a == 1) {
                return next;
            }
        }
        return null;
    }

    public void c() {
        this.f5509e = true;
        notifyDataSetChanged();
    }

    public void a(boolean z) {
        this.f5508d = z;
        notifyDataSetChanged();
    }

    public ArrayList<h> d() {
        ArrayList<h> arrayList = new ArrayList<>();
        arrayList.addAll(this.f5507c);
        return arrayList;
    }

    public int getGroupCount() {
        return this.f5507c.size();
    }

    /* renamed from: a */
    public h getGroup(int i2) {
        return this.f5507c.get(i2);
    }

    /* renamed from: a */
    public g getChild(int i2, int i3) {
        if (i2 >= this.f5507c.size() || i3 >= this.f5507c.get(i2).f5636e.size()) {
            return new e();
        }
        return this.f5507c.get(i2).f5636e.get(i3);
    }

    public long getGroupId(int i2) {
        return 0;
    }

    public long getChildId(int i2, int i3) {
        return 0;
    }

    public boolean hasStableIds() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        GroupHolder groupHolder;
        Activity activity = this.f5505a.get();
        if (view == null) {
            view = this.f5506b.inflate((int) R.layout.bo, viewGroup, false);
            groupHolder = new GroupHolder(view);
            view.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) view.getTag();
        }
        if (this.f5508d) {
            groupHolder.tvSize.setVisibility(8);
            groupHolder.mProgressBar.setVisibility(0);
            groupHolder.mSelectedAll.setVisibility(8);
        } else {
            groupHolder.tvSize.setVisibility(0);
            groupHolder.mProgressBar.setVisibility(8);
            groupHolder.mSelectedAll.setVisibility(0);
        }
        groupHolder.mExpandLayout.setTag(Integer.valueOf(i2));
        if (z) {
            groupHolder.ivArrow.setImageResource(R.drawable.fi);
        } else {
            groupHolder.ivArrow.setImageResource(R.drawable.by);
        }
        h a2 = getGroup(i2);
        groupHolder.tvTitle.setText(a2.f5633b);
        if (activity != null) {
            groupHolder.tvSize.setText(l.a(activity, a2.f5635d));
        }
        groupHolder.mSelectedAll.setTag(Integer.valueOf(i2));
        if (a2.f5637f > 0) {
            groupHolder.mSelectedAll.setChecked(true);
            if (a2.f5637f < a2.f5636e.size()) {
                groupHolder.mSelectedAll.setButtonDrawable((int) R.drawable.f3);
            } else {
                groupHolder.mSelectedAll.setButtonDrawable((int) R.drawable.cg);
            }
        } else {
            groupHolder.mSelectedAll.setButtonDrawable((int) R.drawable.cg);
            groupHolder.mSelectedAll.setChecked(false);
        }
        if (a2.f5636e.size() == 0) {
            groupHolder.mBomLine.setVisibility(0);
        } else {
            groupHolder.mBomLine.setVisibility(this.i.isGroupExpanded(i2) ? 8 : 0);
        }
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.adapter.JunkListAdapter.a(int, int):com.pureapps.cleaner.bean.g
     arg types: [int, int]
     candidates:
      com.pureapps.cleaner.view.AnimatedExpandableListView.a.a(int, int):void
      com.pureapps.cleaner.view.AnimatedExpandableListView.a.a(com.pureapps.cleaner.view.AnimatedExpandableListView$a, int):void
      com.pureapps.cleaner.view.AnimatedExpandableListView.a.a(com.pureapps.cleaner.view.AnimatedExpandableListView$a, com.pureapps.cleaner.view.AnimatedExpandableListView):void
      com.pureapps.cleaner.adapter.JunkListAdapter.a(int, int):com.pureapps.cleaner.bean.g */
    public View a(int i2, int i3, boolean z, final View view, ViewGroup viewGroup) {
        ChildHolder childHolder;
        int i4;
        int i5;
        int i6 = 0;
        Activity activity = this.f5505a.get();
        if (view == null) {
            view = this.f5506b.inflate((int) R.layout.bm, viewGroup, false);
            ChildHolder childHolder2 = new ChildHolder(view);
            view.setTag(childHolder2);
            if (this.f5509e || activity == null) {
                childHolder = childHolder2;
            } else {
                Animation loadAnimation = AnimationUtils.loadAnimation(activity, R.anim.y);
                loadAnimation.setDuration((long) ((i3 * 100) + 400));
                view.startAnimation(loadAnimation);
                childHolder = childHolder2;
            }
        } else {
            childHolder = (ChildHolder) view.getTag();
        }
        if (activity != null) {
            i6 = activity.getResources().getDimensionPixelOffset(R.dimen.g3);
            i4 = activity.getResources().getDimensionPixelOffset(R.dimen.g4);
        } else {
            i4 = 0;
        }
        if (z) {
            i5 = i6;
        } else {
            i5 = i4;
        }
        if (i3 == 0) {
            i4 = i6;
        }
        view.setPadding(i6, i4, i6, i5);
        g a2 = getChild(i2, i3);
        childHolder.ivIcon.setImageResource(R.drawable.bx);
        childHolder.tvTitle.setText(a2.f5628d);
        if (activity != null) {
            childHolder.tvSize.setText(l.a(activity, a2.f5629e));
        }
        childHolder.mSelected.setChecked(a2.f5631g);
        if (a2 instanceof e) {
            e eVar = (e) a2;
            if (eVar.f5619b != null) {
                childHolder.ivIcon.setImageDrawable(eVar.f5619b);
            }
        } else if (a2 instanceof d) {
            d dVar = (d) a2;
            if (dVar.f5616b != null) {
                childHolder.ivIcon.setImageDrawable(dVar.f5616b);
            }
        }
        if (this.f5509e && a2.f5631g && activity != null) {
            Animation loadAnimation2 = AnimationUtils.loadAnimation(activity, R.anim.z);
            loadAnimation2.setDuration((long) ((i3 * 100) + 400));
            loadAnimation2.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(4);
                }

                public void onAnimationRepeat(Animation animation) {
                }
            });
            view.startAnimation(loadAnimation2);
        }
        return view;
    }

    public int b(int i2) {
        return this.f5507c.get(i2).f5636e.size();
    }

    public void e() {
        System.gc();
    }

    public boolean isChildSelectable(int i2, int i3) {
        return true;
    }

    public void onGroupExpanded(int i2) {
    }

    public void onGroupCollapsed(int i2) {
    }

    class GroupHolder {
        @BindView(R.id.j9)
        ImageView ivArrow;
        @BindView(R.id.ja)
        View mBomLine;
        @BindView(R.id.j8)
        LinearLayout mExpandLayout;
        @BindView(R.id.d6)
        ProgressBar mProgressBar;
        @BindView(R.id.j_)
        CheckBox mSelectedAll;
        @BindView(R.id.j4)
        TextView tvSize;
        @BindView(R.id.fd)
        TextView tvTitle;

        public GroupHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @OnClick({2131624303, 2131624305})
        public void onClick(View view) {
            boolean z;
            long j;
            switch (view.getId()) {
                case R.id.j8 /*2131624303*/:
                    int intValue = ((Integer) view.getTag()).intValue();
                    ImageView imageView = (ImageView) view.findViewById(R.id.j9);
                    if (JunkListAdapter.this.i.isGroupExpanded(intValue)) {
                        JunkListAdapter.this.i.b(intValue);
                        imageView.setImageResource(R.drawable.by);
                        return;
                    }
                    JunkListAdapter.this.i.a(intValue);
                    imageView.setImageResource(R.drawable.fi);
                    return;
                case R.id.j9 /*2131624304*/:
                default:
                    return;
                case R.id.j_ /*2131624305*/:
                    h a2 = JunkListAdapter.this.getGroup(((Integer) view.getTag()).intValue());
                    if (a2.f5637f > 0) {
                        a2.f5637f = 0;
                    } else {
                        a2.f5637f = a2.f5636e.size();
                    }
                    Iterator<g> it = a2.f5636e.iterator();
                    while (it.hasNext()) {
                        g next = it.next();
                        if (a2.f5637f == 0) {
                            z = false;
                        } else {
                            z = true;
                        }
                        next.f5631g = z;
                        if (a2.f5637f == 0) {
                            j = 0;
                        } else {
                            j = next.f5629e + a2.f5635d;
                        }
                        a2.f5635d = j;
                    }
                    JunkListAdapter.this.notifyDataSetChanged();
                    JunkClearActivity junkClearActivity = (JunkClearActivity) JunkListAdapter.this.f5505a.get();
                    if (junkClearActivity != null) {
                        junkClearActivity.b(true);
                        return;
                    }
                    return;
            }
        }
    }

    class ChildHolder {
        @BindView(R.id.j3)
        ImageView ivIcon;
        @BindView(R.id.j5)
        CheckBox mSelected;
        @BindView(R.id.j4)
        TextView tvSize;
        @BindView(R.id.fd)
        TextView tvTitle;

        public ChildHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
