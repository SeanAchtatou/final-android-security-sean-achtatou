package com.pureapps.cleaner.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.pureapps.cleaner.MemoryBoostActivity;
import com.pureapps.cleaner.bean.j;
import com.pureapps.cleaner.bean.k;
import com.pureapps.cleaner.util.Const;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.l;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class MemoryBoostListAdapter extends BaseExpandableListAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public WeakReference<MemoryBoostActivity> f5525a;

    /* renamed from: b  reason: collision with root package name */
    private LayoutInflater f5526b;

    /* renamed from: c  reason: collision with root package name */
    private ArrayList<k> f5527c = new ArrayList<>();

    /* renamed from: d  reason: collision with root package name */
    private int f5528d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f5529e = true;

    /* renamed from: f  reason: collision with root package name */
    private boolean f5530f = true;

    public class ChildHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private ChildHolder f5533a;

        public ChildHolder_ViewBinding(ChildHolder childHolder, View view) {
            this.f5533a = childHolder;
            childHolder.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.fd, "field 'tvTitle'", TextView.class);
            childHolder.ivIcon = (ImageView) Utils.findRequiredViewAsType(view, R.id.j3, "field 'ivIcon'", ImageView.class);
            childHolder.tvRuntime = (TextView) Utils.findRequiredViewAsType(view, R.id.j6, "field 'tvRuntime'", TextView.class);
            childHolder.tvSize = (TextView) Utils.findRequiredViewAsType(view, R.id.j7, "field 'tvSize'", TextView.class);
            childHolder.mSelected = (CheckBox) Utils.findRequiredViewAsType(view, R.id.j5, "field 'mSelected'", CheckBox.class);
        }

        public void unbind() {
            ChildHolder childHolder = this.f5533a;
            if (childHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f5533a = null;
            childHolder.tvTitle = null;
            childHolder.ivIcon = null;
            childHolder.tvRuntime = null;
            childHolder.tvSize = null;
            childHolder.mSelected = null;
        }
    }

    public class GroupHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private GroupHolder f5535a;

        /* renamed from: b  reason: collision with root package name */
        private View f5536b;

        public GroupHolder_ViewBinding(final GroupHolder groupHolder, View view) {
            this.f5535a = groupHolder;
            groupHolder.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.fd, "field 'tvTitle'", TextView.class);
            groupHolder.tvChildCount = (TextView) Utils.findRequiredViewAsType(view, R.id.jb, "field 'tvChildCount'", TextView.class);
            View findRequiredView = Utils.findRequiredView(view, R.id.j_, "field 'mSelectedAll' and method 'onClick'");
            groupHolder.mSelectedAll = (CheckBox) Utils.castView(findRequiredView, R.id.j_, "field 'mSelectedAll'", CheckBox.class);
            this.f5536b = findRequiredView;
            findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
                public void doClick(View view) {
                    groupHolder.onClick(view);
                }
            });
        }

        public void unbind() {
            GroupHolder groupHolder = this.f5535a;
            if (groupHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f5535a = null;
            groupHolder.tvTitle = null;
            groupHolder.tvChildCount = null;
            groupHolder.mSelectedAll = null;
            this.f5536b.setOnClickListener(null);
            this.f5536b = null;
        }
    }

    public MemoryBoostListAdapter(MemoryBoostActivity memoryBoostActivity) {
        this.f5525a = new WeakReference<>(memoryBoostActivity);
        this.f5526b = LayoutInflater.from(memoryBoostActivity);
        this.f5528d = i.a(memoryBoostActivity).t();
        k kVar = new k();
        kVar.f5646a = 1;
        kVar.f5647b = memoryBoostActivity.getString(R.string.ew);
        ArrayList<j> arrayList = new ArrayList<>();
        arrayList.add(a(kVar, memoryBoostActivity));
        kVar.f5648c = arrayList;
        this.f5527c.add(kVar);
        k kVar2 = new k();
        kVar2.f5646a = 0;
        kVar2.f5647b = memoryBoostActivity.getString(R.string.ex);
        this.f5527c.add(kVar2);
    }

    private j a(k kVar, Activity activity) {
        j jVar = new j();
        jVar.f5638a = 1;
        jVar.f5640c = activity.getString(R.string.bo);
        jVar.f5645h = 35;
        kVar.f5648c.add(jVar);
        return jVar;
    }

    public void a(k kVar) {
        this.f5527c.set(1, kVar);
    }

    public void a(int i) {
        Iterator<j> it = a().f5648c.iterator();
        while (it.hasNext()) {
            j next = it.next();
            if (this.f5525a.get() != null && next.f5638a == 1) {
                next.f5645h = i;
                return;
            }
        }
    }

    public void a(boolean z) {
        this.f5529e = z;
        notifyDataSetChanged();
    }

    public k a() {
        Iterator<k> it = this.f5527c.iterator();
        while (it.hasNext()) {
            k next = it.next();
            if (next.f5646a == 1) {
                return next;
            }
        }
        return null;
    }

    public k b() {
        if (this.f5527c.size() > 0) {
            Iterator<k> it = this.f5527c.iterator();
            while (it.hasNext()) {
                k next = it.next();
                if (next.f5646a == 0) {
                    return next;
                }
            }
        }
        return null;
    }

    public int getGroupCount() {
        return this.f5527c.size();
    }

    public int getChildrenCount(int i) {
        return this.f5527c.get(i).f5648c.size();
    }

    /* renamed from: b */
    public k getGroup(int i) {
        return this.f5527c.get(i);
    }

    /* renamed from: a */
    public j getChild(int i, int i2) {
        return this.f5527c.get(i).f5648c.get(i2);
    }

    public long getGroupId(int i) {
        return 0;
    }

    public long getChildId(int i, int i2) {
        return 0;
    }

    public boolean hasStableIds() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        GroupHolder groupHolder;
        if (view == null) {
            view = this.f5526b.inflate((int) R.layout.bp, viewGroup, false);
            groupHolder = new GroupHolder(view);
            view.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) view.getTag();
        }
        k b2 = getGroup(i);
        groupHolder.tvTitle.setText(b2.f5647b);
        if (b2.f5646a == 0) {
            if (this.f5529e) {
                groupHolder.mSelectedAll.setVisibility(8);
                groupHolder.tvChildCount.setVisibility(8);
            } else {
                groupHolder.mSelectedAll.setVisibility(0);
                groupHolder.tvChildCount.setVisibility(0);
            }
            groupHolder.tvChildCount.setText(String.valueOf(b2.f5648c.size()));
            groupHolder.mSelectedAll.setTag(Integer.valueOf(i));
            if (b2.f5649d > 0) {
                groupHolder.mSelectedAll.setChecked(true);
                if (b2.f5649d < b2.f5648c.size()) {
                    groupHolder.mSelectedAll.setButtonDrawable((int) R.drawable.f3);
                } else {
                    groupHolder.mSelectedAll.setButtonDrawable((int) R.drawable.cg);
                }
            } else {
                groupHolder.mSelectedAll.setButtonDrawable((int) R.drawable.cg);
                groupHolder.mSelectedAll.setChecked(false);
            }
        } else {
            groupHolder.mSelectedAll.setVisibility(8);
            groupHolder.tvChildCount.setVisibility(8);
        }
        return view;
    }

    public int getChildTypeCount() {
        return 2;
    }

    public int getChildType(int i, int i2) {
        if (getChild(i, i2).f5638a == 1) {
            return 1;
        }
        return 0;
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        if (getChildType(i, i2) == 1) {
            return a(i, i2, z, view, viewGroup);
        }
        return b(i, i2, z, view, viewGroup);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.l.a(android.content.Context, int):com.pureapps.cleaner.util.Const$CpuTempStyle
     arg types: [android.app.Activity, int]
     candidates:
      com.pureapps.cleaner.util.l.a(android.content.Context, java.lang.String):android.content.res.Resources
      com.pureapps.cleaner.util.l.a(android.content.Context, long):java.lang.String
      com.pureapps.cleaner.util.l.a(android.content.Context, int):com.pureapps.cleaner.util.Const$CpuTempStyle */
    private View a(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        Activity activity = this.f5525a.get();
        if (view == null) {
            view = this.f5526b.inflate((int) R.layout.bl, viewGroup, false);
        }
        j a2 = getChild(i, i2);
        TextView textView = (TextView) view.findViewById(R.id.fd);
        textView.setText(a2.f5640c);
        TextView textView2 = (TextView) view.findViewById(R.id.j1);
        String str = null;
        if (activity != null) {
            if (this.f5528d == 0) {
                str = a2.f5645h + activity.getString(R.string.ee);
            } else {
                str = a2.f5645h + activity.getString(R.string.ef);
            }
        }
        if (activity != null) {
            if (l.a((Context) activity, a2.f5645h) == Const.CpuTempStyle.FINE) {
                textView.setText((int) R.string.bo);
            } else if (l.a((Context) activity, a2.f5645h) == Const.CpuTempStyle.HIGH) {
                textView.setText((int) R.string.bn);
            } else {
                textView.setText((int) R.string.bm);
            }
        }
        textView2.setText(str);
        ((Button) view.findViewById(R.id.j2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MemoryBoostActivity memoryBoostActivity = (MemoryBoostActivity) MemoryBoostListAdapter.this.f5525a.get();
                if (memoryBoostActivity != null) {
                    memoryBoostActivity.k();
                }
            }
        });
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View b(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        ChildHolder childHolder;
        int i3 = 0;
        Activity activity = this.f5525a.get();
        if (view == null) {
            view = this.f5526b.inflate((int) R.layout.bn, viewGroup, false);
            childHolder = new ChildHolder(view);
            view.setTag(childHolder);
            if (activity != null) {
                Animation loadAnimation = AnimationUtils.loadAnimation(activity, R.anim.y);
                loadAnimation.setDuration(loadAnimation.getDuration() + ((long) (i2 * 100)));
                view.startAnimation(loadAnimation);
            }
        } else {
            childHolder = (ChildHolder) view.getTag();
        }
        j a2 = getChild(i, i2);
        childHolder.ivIcon.setImageDrawable(a2.f5641d);
        childHolder.tvTitle.setText(a2.f5640c);
        TextView textView = childHolder.tvRuntime;
        if (a2.f5643f <= 0) {
            i3 = 8;
        }
        textView.setVisibility(i3);
        childHolder.tvRuntime.setText(l.b(activity, a2.f5643f));
        childHolder.tvSize.setText(l.a(activity, a2.f5642e));
        childHolder.mSelected.setChecked(a2.f5644g);
        return view;
    }

    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    public void onGroupExpanded(int i) {
    }

    public void onGroupCollapsed(int i) {
    }

    class GroupHolder {
        @BindView(R.id.j_)
        CheckBox mSelectedAll;
        @BindView(R.id.jb)
        TextView tvChildCount;
        @BindView(R.id.fd)
        TextView tvTitle;

        public GroupHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @OnClick({2131624305})
        public void onClick(View view) {
            MemoryBoostActivity memoryBoostActivity = (MemoryBoostActivity) MemoryBoostListAdapter.this.f5525a.get();
            k b2 = MemoryBoostListAdapter.this.getGroup(((Integer) view.getTag()).intValue());
            if (b2.f5649d > 0) {
                b2.f5649d = 0;
            } else {
                b2.f5649d = b2.f5648c.size();
            }
            Iterator<j> it = b2.f5648c.iterator();
            while (it.hasNext()) {
                it.next().f5644g = b2.f5649d != 0;
            }
            MemoryBoostListAdapter.this.notifyDataSetChanged();
            if (memoryBoostActivity != null) {
                memoryBoostActivity.j();
            }
        }
    }

    class ChildHolder {
        @BindView(R.id.j3)
        ImageView ivIcon;
        @BindView(R.id.j5)
        CheckBox mSelected;
        @BindView(R.id.j6)
        TextView tvRuntime;
        @BindView(R.id.j7)
        TextView tvSize;
        @BindView(R.id.fd)
        TextView tvTitle;

        public ChildHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
