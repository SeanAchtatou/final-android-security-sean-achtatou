package com.pureapps.cleaner.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.duapps.ad.DuNativeAd;
import com.kingouser.com.R;
import com.kingouser.com.application.App;
import com.kingouser.com.entity.UninstallAppInfo;
import com.kingouser.com.fragment.AppManagerFeagment;
import com.kingouser.com.util.FormatUtils;
import com.pureapps.cleaner.manager.a;
import com.pureapps.cleaner.util.f;
import explosionfield.ExplosionField;
import java.util.ArrayList;

public class AppManagerAdapter extends RecyclerView.a<RecyclerView.u> {

    /* renamed from: a  reason: collision with root package name */
    public final int f5477a = 0;

    /* renamed from: b  reason: collision with root package name */
    public final int f5478b = 1;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Context f5479c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public AppManagerFeagment f5480d;

    /* renamed from: e  reason: collision with root package name */
    private LayoutInflater f5481e;

    /* renamed from: f  reason: collision with root package name */
    private ArrayList<UninstallAppInfo> f5482f = new ArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public int f5483g = 0;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public int f5484h = 0;
    private RecyclerView i;
    private ExplosionField j;
    private Handler k = new Handler();

    public class BannerViewHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private BannerViewHolder f5490a;

        public BannerViewHolder_ViewBinding(BannerViewHolder bannerViewHolder, View view) {
            this.f5490a = bannerViewHolder;
            bannerViewHolder.mAdLayout = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.ey, "field 'mAdLayout'", FrameLayout.class);
        }

        public void unbind() {
            BannerViewHolder bannerViewHolder = this.f5490a;
            if (bannerViewHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f5490a = null;
            bannerViewHolder.mAdLayout = null;
        }
    }

    public class ViewHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private ViewHolder f5492a;

        /* renamed from: b  reason: collision with root package name */
        private View f5493b;

        public ViewHolder_ViewBinding(final ViewHolder viewHolder, View view) {
            this.f5492a = viewHolder;
            viewHolder.ivIcon = (ImageView) Utils.findRequiredViewAsType(view, R.id.j3, "field 'ivIcon'", ImageView.class);
            viewHolder.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.fd, "field 'tvTitle'", TextView.class);
            viewHolder.tvFileSize = (TextView) Utils.findRequiredViewAsType(view, R.id.jd, "field 'tvFileSize'", TextView.class);
            viewHolder.mCheckbox = (CheckBox) Utils.findRequiredViewAsType(view, R.id.c1, "field 'mCheckbox'", CheckBox.class);
            View findRequiredView = Utils.findRequiredView(view, R.id.jc, "field 'mContent' and method 'onClick'");
            viewHolder.mContent = findRequiredView;
            this.f5493b = findRequiredView;
            findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
                public void doClick(View view) {
                    viewHolder.onClick(view);
                }
            });
        }

        public void unbind() {
            ViewHolder viewHolder = this.f5492a;
            if (viewHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f5492a = null;
            viewHolder.ivIcon = null;
            viewHolder.tvTitle = null;
            viewHolder.tvFileSize = null;
            viewHolder.mCheckbox = null;
            viewHolder.mContent = null;
            this.f5493b.setOnClickListener(null);
            this.f5493b = null;
        }
    }

    public AppManagerAdapter(Activity activity, AppManagerFeagment appManagerFeagment, int i2, RecyclerView recyclerView) {
        this.f5479c = activity;
        this.f5480d = appManagerFeagment;
        this.f5481e = LayoutInflater.from(this.f5479c);
        this.f5484h = i2;
        this.i = recyclerView;
        this.j = ExplosionField.a(activity);
    }

    public void a(ArrayList<UninstallAppInfo> arrayList) {
        if (arrayList != null) {
            this.f5482f.clear();
            this.f5482f = (ArrayList) arrayList.clone();
            notifyDataSetChanged();
        }
    }

    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        return 1;
    }

    public void a(int i2) {
        if (this.i.getChildCount() > i2 + 1) {
            this.j.a(this.i.getChildAt(i2 + 1));
        }
        this.f5482f.remove(i2);
        notifyItemRemoved(i2 + 1);
        notifyItemRangeChanged(0, this.f5482f.size());
    }

    public ArrayList<UninstallAppInfo> a() {
        ArrayList<UninstallAppInfo> arrayList = new ArrayList<>();
        arrayList.addAll(this.f5482f);
        return arrayList;
    }

    public UninstallAppInfo b(int i2) {
        if (i2 < 0) {
            return null;
        }
        return this.f5482f.get(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.u onCreateViewHolder(ViewGroup viewGroup, int i2) {
        if (i2 == 0) {
            return new BannerViewHolder(this.f5481e.inflate((int) R.layout.ag, viewGroup, false));
        }
        return new ViewHolder(this.f5481e.inflate((int) R.layout.br, viewGroup, false));
    }

    public void onBindViewHolder(RecyclerView.u uVar, int i2) {
        if (uVar instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) uVar;
            UninstallAppInfo b2 = b(i2 - 1);
            viewHolder.ivIcon.setImageDrawable(b2.icon);
            viewHolder.tvTitle.setText(b2.title);
            viewHolder.tvFileSize.setText(FormatUtils.formatBytesInByte(b2.size));
            viewHolder.mCheckbox.setChecked(b2.checked);
            viewHolder.mContent.setTag(viewHolder);
        }
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    public int getItemCount() {
        if (this.f5482f == null) {
            return 1;
        }
        return this.f5482f.size() + 1;
    }

    class ViewHolder extends RecyclerView.u {
        @BindView(R.id.j3)
        ImageView ivIcon;
        @BindView(R.id.c1)
        CheckBox mCheckbox;
        @BindView(R.id.jc)
        View mContent;
        @BindView(R.id.jd)
        TextView tvFileSize;
        @BindView(R.id.fd)
        TextView tvTitle;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick({2131624308})
        public void onClick(View view) {
            if (AppManagerAdapter.this.f5480d instanceof AppManagerFeagment) {
                ViewHolder viewHolder = (ViewHolder) view.getTag();
                AppManagerAdapter.this.f5480d.a(viewHolder.mCheckbox, viewHolder.getAdapterPosition() - 1);
            }
        }
    }

    class BannerViewHolder extends RecyclerView.u {
        @BindView(R.id.ey)
        FrameLayout mAdLayout;

        public BannerViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.mAdLayout.setVisibility(8);
            float f2 = AppManagerAdapter.this.f5479c.getResources().getDisplayMetrics().density;
            if (AppManagerAdapter.this.f5484h == AppManagerAdapter.this.f5483g) {
                a.a().a(11, this.mAdLayout, new a.C0089a(AppManagerAdapter.this) {
                    public void a(DuNativeAd duNativeAd) {
                        f.a("initAdByManager onSuccess");
                        BannerViewHolder.this.mAdLayout.setVisibility(0);
                        com.pureapps.cleaner.util.a.a(BannerViewHolder.this.mAdLayout, true, 400, App.a().getResources().getDisplayMetrics().heightPixels / 4);
                    }

                    public void a() {
                        f.a("initAdByManager error");
                        BannerViewHolder.this.mAdLayout.removeAllViews();
                    }

                    public void b() {
                    }
                });
            } else {
                a.a().a(12, this.mAdLayout, new a.C0089a(AppManagerAdapter.this) {
                    public void a(DuNativeAd duNativeAd) {
                        f.a("initAdByManager onSuccess");
                        BannerViewHolder.this.mAdLayout.setVisibility(0);
                        com.pureapps.cleaner.util.a.a(BannerViewHolder.this.mAdLayout, true, 400, App.a().getResources().getDisplayMetrics().heightPixels / 4);
                    }

                    public void a() {
                        f.a("initAdByManager error");
                        BannerViewHolder.this.mAdLayout.removeAllViews();
                    }

                    public void b() {
                    }
                });
            }
        }
    }
}
