package com.pureapps.cleaner.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.pureapps.cleaner.IgnoreListActivity;
import com.pureapps.cleaner.IgnoreListAddActivity;
import com.pureapps.cleaner.bean.a;
import java.util.ArrayList;
import java.util.HashMap;

public class IgnoreListAdapter extends RecyclerView.a<ViewHolder> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Activity f5496a;

    /* renamed from: b  reason: collision with root package name */
    private LayoutInflater f5497b;

    /* renamed from: c  reason: collision with root package name */
    private ArrayList<a> f5498c = new ArrayList<>();

    /* renamed from: d  reason: collision with root package name */
    private HashMap<Integer, ViewHolder> f5499d = new HashMap<>();

    public class ViewHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private ViewHolder f5501a;

        /* renamed from: b  reason: collision with root package name */
        private View f5502b;

        public ViewHolder_ViewBinding(final ViewHolder viewHolder, View view) {
            this.f5501a = viewHolder;
            viewHolder.ivIcon = (ImageView) Utils.findRequiredViewAsType(view, R.id.j3, "field 'ivIcon'", ImageView.class);
            viewHolder.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.fd, "field 'tvTitle'", TextView.class);
            viewHolder.mCheckbox = (CheckBox) Utils.findRequiredViewAsType(view, R.id.c1, "field 'mCheckbox'", CheckBox.class);
            View findRequiredView = Utils.findRequiredView(view, R.id.jc, "field 'mContent' and method 'onClick'");
            viewHolder.mContent = findRequiredView;
            this.f5502b = findRequiredView;
            findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
                public void doClick(View view) {
                    viewHolder.onClick(view);
                }
            });
        }

        public void unbind() {
            ViewHolder viewHolder = this.f5501a;
            if (viewHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f5501a = null;
            viewHolder.ivIcon = null;
            viewHolder.tvTitle = null;
            viewHolder.mCheckbox = null;
            viewHolder.mContent = null;
            this.f5502b.setOnClickListener(null);
            this.f5502b = null;
        }
    }

    public IgnoreListAdapter(Activity activity) {
        this.f5496a = activity;
        this.f5497b = LayoutInflater.from(this.f5496a);
    }

    public void a(ArrayList<a> arrayList) {
        this.f5498c.clear();
        this.f5498c = (ArrayList) arrayList.clone();
        notifyDataSetChanged();
    }

    public void a(int i) {
        this.f5498c.remove(i);
        notifyItemRemoved(i);
        notifyItemRangeChanged(0, this.f5498c.size());
    }

    public ArrayList<a> a() {
        ArrayList<a> arrayList = new ArrayList<>();
        arrayList.addAll(this.f5498c);
        return arrayList;
    }

    public a b(int i) {
        return this.f5498c.get(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: a */
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(this.f5497b.inflate((int) R.layout.bq, viewGroup, false));
    }

    /* renamed from: a */
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        a b2 = b(i);
        viewHolder.ivIcon.setImageDrawable(b2.f5609d);
        viewHolder.tvTitle.setText(b2.f5610e);
        viewHolder.mCheckbox.setTag(Integer.valueOf(i));
        viewHolder.mCheckbox.setChecked(b2.f5611f);
        viewHolder.mContent.setTag(viewHolder);
        if (!this.f5499d.containsValue(viewHolder)) {
            this.f5499d.put(Integer.valueOf(i), viewHolder);
        }
    }

    public long getItemId(int i) {
        return 0;
    }

    public int getItemCount() {
        if (this.f5498c == null) {
            return 0;
        }
        return this.f5498c.size();
    }

    class ViewHolder extends RecyclerView.u {
        @BindView(R.id.j3)
        ImageView ivIcon;
        @BindView(R.id.c1)
        CheckBox mCheckbox;
        @BindView(R.id.jc)
        View mContent;
        @BindView(R.id.fd)
        TextView tvTitle;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick({2131624308})
        public void onClick(View view) {
            if (IgnoreListAdapter.this.f5496a instanceof IgnoreListActivity) {
                ViewHolder viewHolder = (ViewHolder) view.getTag();
                ((IgnoreListActivity) IgnoreListAdapter.this.f5496a).a(viewHolder.mCheckbox, viewHolder.getAdapterPosition());
            } else if (IgnoreListAdapter.this.f5496a instanceof IgnoreListAddActivity) {
                ViewHolder viewHolder2 = (ViewHolder) view.getTag();
                ((IgnoreListAddActivity) IgnoreListAdapter.this.f5496a).a(viewHolder2.mCheckbox, viewHolder2.getAdapterPosition());
            }
        }
    }
}
