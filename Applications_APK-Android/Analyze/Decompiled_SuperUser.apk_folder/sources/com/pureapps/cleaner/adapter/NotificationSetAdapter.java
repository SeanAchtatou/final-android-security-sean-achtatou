package com.pureapps.cleaner.adapter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.pureapps.cleaner.b.a;
import com.pureapps.cleaner.bean.l;
import com.pureapps.cleaner.manager.f;
import com.pureapps.cleaner.service.NotificationMonitorService;
import com.pureapps.cleaner.util.i;
import java.util.ArrayList;

public class NotificationSetAdapter extends RecyclerView.a<RecyclerView.u> {

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<l> f5552a;

    /* renamed from: b  reason: collision with root package name */
    public Context f5553b;

    /* renamed from: c  reason: collision with root package name */
    private PackageManager f5554c;

    /* renamed from: d  reason: collision with root package name */
    private final int f5555d = 0;

    /* renamed from: e  reason: collision with root package name */
    private final int f5556e = 1;

    public class HeadViewHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private HeadViewHolder f5560a;

        /* renamed from: b  reason: collision with root package name */
        private View f5561b;

        public HeadViewHolder_ViewBinding(final HeadViewHolder headViewHolder, View view) {
            this.f5560a = headViewHolder;
            View findRequiredView = Utils.findRequiredView(view, R.id.jv, "field 'content' and method 'onClick'");
            headViewHolder.content = (LinearLayout) Utils.castView(findRequiredView, R.id.jv, "field 'content'", LinearLayout.class);
            this.f5561b = findRequiredView;
            findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
                public void doClick(View view) {
                    headViewHolder.onClick(view);
                }
            });
            headViewHolder.checkbox = (SwitchCompat) Utils.findRequiredViewAsType(view, R.id.jx, "field 'checkbox'", SwitchCompat.class);
            headViewHolder.stateText = (TextView) Utils.findRequiredViewAsType(view, R.id.jw, "field 'stateText'", TextView.class);
        }

        public void unbind() {
            HeadViewHolder headViewHolder = this.f5560a;
            if (headViewHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f5560a = null;
            headViewHolder.content = null;
            headViewHolder.checkbox = null;
            headViewHolder.stateText = null;
            this.f5561b.setOnClickListener(null);
            this.f5561b = null;
        }
    }

    public class ViewHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private ViewHolder f5565a;

        /* renamed from: b  reason: collision with root package name */
        private View f5566b;

        public ViewHolder_ViewBinding(final ViewHolder viewHolder, View view) {
            this.f5565a = viewHolder;
            viewHolder.icon = (ImageView) Utils.findRequiredViewAsType(view, R.id.bj, "field 'icon'", ImageView.class);
            viewHolder.name = (TextView) Utils.findRequiredViewAsType(view, R.id.jy, "field 'name'", TextView.class);
            viewHolder.checkbox = (CheckBox) Utils.findRequiredViewAsType(view, R.id.c1, "field 'checkbox'", CheckBox.class);
            View findRequiredView = Utils.findRequiredView(view, R.id.jc, "field 'content' and method 'onClick'");
            viewHolder.content = (LinearLayout) Utils.castView(findRequiredView, R.id.jc, "field 'content'", LinearLayout.class);
            this.f5566b = findRequiredView;
            findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
                public void doClick(View view) {
                    viewHolder.onClick(view);
                }
            });
        }

        public void unbind() {
            ViewHolder viewHolder = this.f5565a;
            if (viewHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f5565a = null;
            viewHolder.icon = null;
            viewHolder.name = null;
            viewHolder.checkbox = null;
            viewHolder.content = null;
            this.f5566b.setOnClickListener(null);
            this.f5566b = null;
        }
    }

    public NotificationSetAdapter(ArrayList<l> arrayList, Context context) {
        this.f5552a = arrayList;
        this.f5553b = context;
        this.f5554c = context.getPackageManager();
    }

    public void a(ArrayList<l> arrayList) {
        this.f5552a = arrayList;
        notifyDataSetChanged();
    }

    public int getItemViewType(int i) {
        if (i == 0) {
            return 0;
        }
        return 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.u onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 1) {
            return new ViewHolder(LayoutInflater.from(this.f5553b).inflate((int) R.layout.c3, viewGroup, false));
        }
        return new HeadViewHolder(LayoutInflater.from(this.f5553b).inflate((int) R.layout.c2, viewGroup, false));
    }

    public void onBindViewHolder(RecyclerView.u uVar, int i) {
        if (uVar instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) uVar;
            l lVar = this.f5552a.get(i - 1);
            if (lVar != null) {
                try {
                    Drawable loadIcon = this.f5554c.getPackageInfo(lVar.f5651b, 8192).applicationInfo.loadIcon(this.f5554c);
                    if (loadIcon != null) {
                        viewHolder.icon.setImageDrawable(loadIcon);
                    } else {
                        viewHolder.icon.setImageDrawable(this.f5554c.getDefaultActivityIcon());
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                    viewHolder.icon.setImageDrawable(this.f5554c.getDefaultActivityIcon());
                }
                viewHolder.name.setText(lVar.f5650a);
                viewHolder.checkbox.setChecked(lVar.f5652c);
                viewHolder.content.setTag(Integer.valueOf(i));
                return;
            }
            return;
        }
        final HeadViewHolder headViewHolder = (HeadViewHolder) uVar;
        headViewHolder.checkbox.setChecked(i.a(this.f5553b).l());
        headViewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                i.a(NotificationSetAdapter.this.f5553b).d(z);
                a.a(22, 0, null);
                headViewHolder.stateText.setText(z ? R.string.dj : R.string.bi);
                if (!z) {
                    NotificationMonitorService.f5847a.clear();
                    f.a(NotificationSetAdapter.this.f5553b).d();
                }
            }
        });
        headViewHolder.stateText.setText(headViewHolder.checkbox.isChecked() ? R.string.dj : R.string.bi);
    }

    public int getItemCount() {
        if (this.f5552a == null) {
            return 1;
        }
        return this.f5552a.size() + 1;
    }

    /* access modifiers changed from: private */
    public void a() {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            int i2 = i;
            if (this.f5552a == null || i2 >= this.f5552a.size()) {
                String stringBuffer2 = stringBuffer.toString();
            } else {
                if (this.f5552a.get(i2).f5652c) {
                    stringBuffer.append(this.f5552a.get(i2).f5651b);
                    stringBuffer.append(",");
                }
                i = i2 + 1;
            }
        }
        String stringBuffer22 = stringBuffer.toString();
        if (stringBuffer22.length() == 0) {
            stringBuffer22 = ",";
        }
        i.a(this.f5553b).a(stringBuffer22);
    }

    class HeadViewHolder extends RecyclerView.u {
        @BindView(R.id.jx)
        SwitchCompat checkbox;
        @BindView(R.id.jv)
        LinearLayout content;
        @BindView(R.id.jw)
        TextView stateText;

        public HeadViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick({2131624327})
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.jv /*2131624327*/:
                    i.a(NotificationSetAdapter.this.f5553b).d(!i.a(NotificationSetAdapter.this.f5553b).l());
                    a.a(22, 0, null);
                    NotificationSetAdapter.this.notifyItemChanged(0);
                    if (!i.a(NotificationSetAdapter.this.f5553b).l()) {
                        NotificationMonitorService.f5847a.clear();
                        f.a(NotificationSetAdapter.this.f5553b).d();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    class ViewHolder extends RecyclerView.u {
        @BindView(R.id.c1)
        CheckBox checkbox;
        @BindView(R.id.jc)
        LinearLayout content;
        @BindView(R.id.bj)
        ImageView icon;
        @BindView(R.id.jy)
        TextView name;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick({2131624308})
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.jc /*2131624308*/:
                    int intValue = ((Integer) view.getTag()).intValue();
                    l lVar = NotificationSetAdapter.this.f5552a.get(intValue - 1);
                    lVar.f5652c = !lVar.f5652c;
                    NotificationSetAdapter.this.notifyItemChanged(intValue);
                    NotificationSetAdapter.this.a();
                    return;
                default:
                    return;
            }
        }
    }
}
