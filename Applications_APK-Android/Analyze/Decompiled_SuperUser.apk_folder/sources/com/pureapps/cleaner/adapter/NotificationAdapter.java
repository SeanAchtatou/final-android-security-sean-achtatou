package com.pureapps.cleaner.adapter;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.service.notification.StatusBarNotification;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.pureapps.cleaner.bean.NotificationInfo;
import com.pureapps.cleaner.service.NotificationMonitorService;
import com.pureapps.cleaner.view.ItemTouch.a;
import com.pureapps.cleaner.view.ItemTouch.b;
import explosionfield.ExplosionField;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.a<ViewHolder> implements a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<NotificationInfo> f5539a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Context f5540b;

    /* renamed from: c  reason: collision with root package name */
    private PackageManager f5541c;

    /* renamed from: d  reason: collision with root package name */
    private ExplosionField f5542d;

    /* renamed from: e  reason: collision with root package name */
    private RecyclerView f5543e = null;

    /* renamed from: f  reason: collision with root package name */
    private Handler f5544f = new Handler();

    /* renamed from: g  reason: collision with root package name */
    private Runnable f5545g = new Runnable() {
        public void run() {
            NotificationAdapter.this.b(0);
        }
    };

    public class ViewHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private ViewHolder f5548a;

        /* renamed from: b  reason: collision with root package name */
        private View f5549b;

        public ViewHolder_ViewBinding(final ViewHolder viewHolder, View view) {
            this.f5548a = viewHolder;
            viewHolder.icon = (ImageView) Utils.findRequiredViewAsType(view, R.id.bj, "field 'icon'", ImageView.class);
            viewHolder.title = (TextView) Utils.findRequiredViewAsType(view, R.id.bk, "field 'title'", TextView.class);
            viewHolder.content = (TextView) Utils.findRequiredViewAsType(view, R.id.jc, "field 'content'", TextView.class);
            viewHolder.time = (TextView) Utils.findRequiredViewAsType(view, R.id.f8339jp, "field 'time'", TextView.class);
            View findRequiredView = Utils.findRequiredView(view, R.id.f8338io, "field 'content_layout' and method 'onClick'");
            viewHolder.content_layout = (LinearLayout) Utils.castView(findRequiredView, R.id.f8338io, "field 'content_layout'", LinearLayout.class);
            this.f5549b = findRequiredView;
            findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
                public void doClick(View view) {
                    viewHolder.onClick(view);
                }
            });
        }

        public void unbind() {
            ViewHolder viewHolder = this.f5548a;
            if (viewHolder == null) {
                throw new IllegalStateException("Bindings already cleared.");
            }
            this.f5548a = null;
            viewHolder.icon = null;
            viewHolder.title = null;
            viewHolder.content = null;
            viewHolder.time = null;
            viewHolder.content_layout = null;
            this.f5549b.setOnClickListener(null);
            this.f5549b = null;
        }
    }

    public NotificationAdapter(ArrayList<NotificationInfo> arrayList, Activity activity, RecyclerView recyclerView) {
        this.f5539a = arrayList;
        this.f5540b = activity.getApplicationContext();
        this.f5541c = activity.getPackageManager();
        this.f5542d = ExplosionField.a((Activity) new WeakReference(activity).get());
        this.f5543e = recyclerView;
    }

    public void a(ArrayList<NotificationInfo> arrayList) {
        this.f5539a = arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: a */
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(this.f5540b).inflate((int) R.layout.c0, viewGroup, false));
    }

    /* renamed from: a */
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        NotificationInfo notificationInfo = this.f5539a.get(i);
        if (notificationInfo != null) {
            try {
                Drawable loadIcon = this.f5541c.getPackageInfo(notificationInfo.d(), 8192).applicationInfo.loadIcon(this.f5541c);
                if (loadIcon != null) {
                    viewHolder.icon.setImageDrawable(loadIcon);
                } else {
                    viewHolder.icon.setImageDrawable(this.f5541c.getDefaultActivityIcon());
                }
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
                viewHolder.icon.setImageDrawable(this.f5541c.getDefaultActivityIcon());
            }
            viewHolder.title.setText(notificationInfo.e());
            viewHolder.title.setSelected(true);
            viewHolder.content.setText(notificationInfo.b());
            viewHolder.content.setSelected(true);
            viewHolder.time.setText(notificationInfo.h());
            viewHolder.content_layout.setTag(Integer.valueOf(i));
            viewHolder.content.setVisibility((notificationInfo.b() == null || notificationInfo.b().length() == 0) ? 8 : 0);
        }
    }

    public int getItemCount() {
        if (this.f5539a == null) {
            return 0;
        }
        return this.f5539a.size();
    }

    public boolean a(int i, int i2) {
        return false;
    }

    public void a(int i) {
        View childAt;
        if (getItemCount() != 0) {
            if (i < this.f5543e.getChildCount() && (childAt = this.f5543e.getChildAt(i)) != null) {
                try {
                    this.f5542d.a(childAt);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (getItemCount() == 1) {
                this.f5544f.postDelayed(this.f5545g, 400);
            } else {
                b(i);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        if (this.f5539a.size() >= i + 1) {
            this.f5539a.remove(i);
            notifyItemRemoved(i);
            NotificationMonitorService.a(i);
            com.pureapps.cleaner.b.a.a(21, 0, null);
            this.f5540b.sendBroadcast(new Intent("delete.notification.action"));
        }
    }

    class ViewHolder extends RecyclerView.u implements b {
        @BindView(R.id.jc)
        TextView content;
        @BindView(R.id.f8338io)
        LinearLayout content_layout;
        @BindView(R.id.bj)
        ImageView icon;
        @BindView(R.id.f8339jp)
        TextView time;
        @BindView(R.id.bk)
        TextView title;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick({2131624283})
        public void onClick(View view) {
            StatusBarNotification f2;
            int intValue = ((Integer) view.getTag()).intValue();
            if (intValue < NotificationAdapter.this.f5539a.size() && (f2 = ((NotificationInfo) NotificationAdapter.this.f5539a.get(intValue)).f()) != null) {
                NotificationAdapter.this.f5539a.remove(intValue);
                NotificationMonitorService.a(intValue);
                NotificationAdapter.this.notifyDataSetChanged();
                com.pureapps.cleaner.b.a.a(21, 0, null);
                NotificationAdapter.this.f5540b.sendBroadcast(new Intent("delete.notification.action"));
                try {
                    if (Build.VERSION.SDK_INT >= 18 && f2.getNotification() != null && f2.getNotification().contentIntent != null) {
                        f2.getNotification().contentIntent.send();
                    }
                } catch (PendingIntent.CanceledException e2) {
                    e2.printStackTrace();
                }
            }
        }

        public void a() {
        }

        public void b() {
        }
    }
}
