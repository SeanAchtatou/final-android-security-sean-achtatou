package com.pureapps.cleaner;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.kingouser.com.R;

public class GuideDialog extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private ObjectAnimator f5370a = null;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, 16973840);
    }

    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.dimAmount = 0.0f;
        window.setAttributes(attributes);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        getDialog().requestWindowFeature(1);
        View inflate = layoutInflater.inflate((int) R.layout.by, viewGroup, false);
        ButterKnife.bind(this, inflate);
        return inflate;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ImageView imageView = (ImageView) getView().findViewById(R.id.jl);
        this.f5370a = ObjectAnimator.ofFloat(imageView, "translationX", imageView.getTranslationX(), ((float) getResources().getDisplayMetrics().widthPixels) - (getResources().getDisplayMetrics().density * 210.0f));
        this.f5370a.setDuration(1500L);
        this.f5370a.setInterpolator(new LinearInterpolator());
        this.f5370a.setRepeatCount(-1);
        this.f5370a.setRepeatMode(2);
        this.f5370a.start();
    }

    public void onDestroy() {
        super.onDestroy();
        this.f5370a.cancel();
    }

    @OnClick({2131624318})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.jm /*2131624318*/:
                dismiss();
                return;
            default:
                return;
        }
    }
}
