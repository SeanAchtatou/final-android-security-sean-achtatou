package com.pureapps.cleaner;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kingouser.com.R;

public class BoostResultActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private BoostResultActivity f5369a;

    public BoostResultActivity_ViewBinding(BoostResultActivity boostResultActivity, View view) {
        this.f5369a = boostResultActivity;
        boostResultActivity.mResultTitleText = (TextView) Utils.findRequiredViewAsType(view, R.id.cy, "field 'mResultTitleText'", TextView.class);
        boostResultActivity.mResultUnitText = (TextView) Utils.findRequiredViewAsType(view, R.id.cz, "field 'mResultUnitText'", TextView.class);
        boostResultActivity.mResultDesText = (TextView) Utils.findRequiredViewAsType(view, R.id.d0, "field 'mResultDesText'", TextView.class);
        boostResultActivity.mLayoutAdContainer = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.cx, "field 'mLayoutAdContainer'", LinearLayout.class);
    }

    public void unbind() {
        BoostResultActivity boostResultActivity = this.f5369a;
        if (boostResultActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f5369a = null;
        boostResultActivity.mResultTitleText = null;
        boostResultActivity.mResultUnitText = null;
        boostResultActivity.mResultDesText = null;
        boostResultActivity.mLayoutAdContainer = null;
    }
}
