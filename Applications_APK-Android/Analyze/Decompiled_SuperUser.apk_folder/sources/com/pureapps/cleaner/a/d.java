package com.pureapps.cleaner.a;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import com.kingouser.com.R;
import com.pureapps.cleaner.a.a;
import com.pureapps.cleaner.util.l;
import java.util.ArrayList;

/* compiled from: SnowAnimation */
public class d extends a {

    /* renamed from: a  reason: collision with root package name */
    private Context f5467a;

    /* renamed from: b  reason: collision with root package name */
    private ImageView f5468b;

    /* renamed from: c  reason: collision with root package name */
    private ViewGroup f5469c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public ArrayList<b> f5470d = new ArrayList<>();

    public d(Context context, ViewGroup viewGroup, ImageView imageView) {
        this.f5467a = context;
        this.f5469c = viewGroup;
        this.f5468b = imageView;
        if (this.f5468b != null) {
            this.f5468b.setImageResource(R.drawable.fh);
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.f5468b, "rotation", 0.0f, 360.0f);
            ofFloat.setDuration(2000L);
            ofFloat.setRepeatCount(-1);
            ofFloat.setInterpolator(new LinearInterpolator());
            ofFloat.start();
        }
    }

    public void a() {
        int y;
        b bVar = new b(this.f5467a);
        bVar.setImageResource(R.drawable.fh);
        this.f5470d.add(bVar);
        this.f5469c.addView(bVar, -2, -2);
        DisplayMetrics d2 = l.d(this.f5467a);
        int random = (int) (Math.random() * ((double) (d2.widthPixels - this.f5467a.getResources().getDrawable(R.drawable.fh).getIntrinsicWidth())));
        int random2 = (int) (((((double) d2.heightPixels) * Math.random()) / 8.0d) + ((double) (d2.heightPixels / 8)));
        float random3 = (float) (0.25d + (Math.random() * 0.75d));
        float random4 = (float) ((Math.random() * 0.5d) + 0.5d);
        bVar.a(new a.C0086a(random, random2, random3, 0.0f, random4));
        if (this.f5468b == null) {
            y = ((int) this.f5469c.getY()) + this.f5469c.getHeight();
        } else {
            y = (int) this.f5468b.getY();
        }
        bVar.b(new a.C0086a(random, y, random3, 0.0f, random4));
        bVar.a();
    }

    /* compiled from: SnowAnimation */
    private class b extends AppCompatImageView implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: b  reason: collision with root package name */
        private a.C0086a f5473b;

        /* renamed from: c  reason: collision with root package name */
        private a.C0086a f5474c;

        /* renamed from: d  reason: collision with root package name */
        private ValueAnimator f5475d;

        protected b(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void a(a.C0086a aVar) {
            this.f5473b = aVar;
        }

        /* access modifiers changed from: protected */
        public void b(a.C0086a aVar) {
            this.f5474c = aVar;
        }

        /* access modifiers changed from: protected */
        public void a() {
            if (this.f5473b != null && this.f5474c != null) {
                setX((float) this.f5473b.f5444a.x);
                setY((float) this.f5473b.f5444a.y);
                setAlpha(this.f5473b.f5445b);
                setRotation(this.f5473b.f5446c);
                setScaleX(this.f5473b.f5447d);
                setScaleY(this.f5473b.f5447d);
                this.f5475d = ValueAnimator.ofObject(new a(), this.f5473b, this.f5474c);
                this.f5475d.addUpdateListener(this);
                this.f5475d.setDuration(600L);
                this.f5475d.setInterpolator(new AccelerateInterpolator());
                this.f5475d.addListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animator) {
                        super.onAnimationEnd(animator);
                        d.this.f5470d.remove(b.this);
                        ((ViewGroup) b.this.getParent()).removeView(b.this);
                    }
                });
                this.f5475d.start();
            }
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            a.C0086a aVar = (a.C0086a) valueAnimator.getAnimatedValue();
            setX((float) aVar.f5444a.x);
            setY((float) aVar.f5444a.y);
            setAlpha(aVar.f5445b);
            setRotation(aVar.f5446c);
            setScaleX(aVar.f5447d);
            setScaleY(aVar.f5447d);
            invalidate();
        }
    }

    /* compiled from: SnowAnimation */
    public class a implements TypeEvaluator<a.C0086a> {
        public a() {
        }

        /* renamed from: a */
        public a.C0086a evaluate(float f2, a.C0086a aVar, a.C0086a aVar2) {
            return new a.C0086a((int) (((float) aVar.f5444a.x) + (((float) (aVar2.f5444a.x - aVar.f5444a.x)) * f2)), (int) (((float) aVar.f5444a.y) + (((float) (aVar2.f5444a.y - aVar.f5444a.y)) * f2)), aVar.f5445b + ((aVar2.f5445b - aVar.f5445b) * f2), aVar.f5446c + ((aVar2.f5446c - aVar.f5446c) * f2), aVar.f5447d + ((aVar2.f5447d - aVar.f5447d) * f2));
        }
    }
}
