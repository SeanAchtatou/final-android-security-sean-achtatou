package com.pureapps.cleaner.a;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import com.kingouser.com.R;
import com.pureapps.cleaner.a.a;
import com.pureapps.cleaner.util.l;

/* compiled from: RocketAnimation */
public class b extends a implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f5449a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ImageView f5450b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ImageView f5451c;

    /* renamed from: d  reason: collision with root package name */
    private ViewGroup f5452d;

    /* renamed from: e  reason: collision with root package name */
    private View f5453e;

    /* renamed from: f  reason: collision with root package name */
    private ValueAnimator f5454f;

    /* renamed from: g  reason: collision with root package name */
    private Point f5455g;

    /* renamed from: h  reason: collision with root package name */
    private DisplayMetrics f5456h = l.d(this.f5449a);

    public b(Context context, ViewGroup viewGroup, View view, ImageView imageView, ImageView imageView2) {
        this.f5449a = context;
        this.f5452d = viewGroup;
        this.f5453e = view;
        this.f5450b = imageView;
        this.f5451c = imageView2;
    }

    public void a() {
        this.f5449a.getResources().getDrawable(R.drawable.fg);
        int width = (this.f5456h.widthPixels - this.f5450b.getWidth()) / 2;
        int y = (int) this.f5453e.getY();
        this.f5455g = new Point(width, y);
        a.C0086a aVar = new a.C0086a(width, y, 1.0f, 0.0f, 1.0f);
        a.C0086a aVar2 = new a.C0086a(width, 0, 1.0f, 0.0f, 1.0f);
        this.f5450b.setX((float) aVar.f5444a.x);
        this.f5450b.setY((float) aVar.f5444a.y);
        this.f5450b.setAlpha(aVar.f5445b);
        this.f5450b.setScaleX(aVar.f5447d);
        this.f5450b.setScaleY(aVar.f5447d);
        this.f5451c.setY(this.f5452d.getY() + ((float) this.f5452d.getHeight()));
        this.f5450b.setVisibility(0);
        this.f5451c.setVisibility(0);
        this.f5454f = ValueAnimator.ofObject(new a(), aVar, aVar2);
        this.f5454f.addUpdateListener(this);
        this.f5454f.setDuration(1000L);
        this.f5454f.setInterpolator(new AccelerateInterpolator());
        this.f5454f.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                b.this.f5450b.setVisibility(4);
                b.this.f5451c.setVisibility(4);
            }
        });
        this.f5454f.start();
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        a.C0086a aVar = (a.C0086a) valueAnimator.getAnimatedValue();
        this.f5450b.setX((float) aVar.f5444a.x);
        this.f5450b.setY((float) aVar.f5444a.y);
        this.f5450b.setAlpha(aVar.f5445b);
        this.f5450b.setScaleX(aVar.f5447d);
        this.f5450b.setScaleY(aVar.f5447d);
        this.f5450b.invalidate();
        int y = (int) (this.f5452d.getY() + ((float) this.f5452d.getHeight()));
        int height = y - this.f5451c.getHeight();
        int height2 = this.f5450b.getHeight() - (y - this.f5455g.y);
        if (Math.abs(aVar.f5444a.y - this.f5455g.y) > height2) {
            int i = (((aVar.f5444a.y - this.f5455g.y) / 3) * 2) + y + height2;
            this.f5451c.setY(i < height ? (float) height : (float) i);
        }
    }

    /* compiled from: RocketAnimation */
    public class a implements TypeEvaluator<a.C0086a> {
        public a() {
        }

        /* renamed from: a */
        public a.C0086a evaluate(float f2, a.C0086a aVar, a.C0086a aVar2) {
            return new a.C0086a((int) (((float) aVar.f5444a.x) + (((float) (aVar2.f5444a.x - aVar.f5444a.x)) * f2)), (int) (((float) aVar.f5444a.y) + (((float) (aVar2.f5444a.y - aVar.f5444a.y)) * f2)), aVar.f5445b + ((aVar2.f5445b - aVar.f5445b) * f2), aVar.f5446c + ((aVar2.f5446c - aVar.f5446c) * f2), aVar.f5447d + ((aVar2.f5447d - aVar.f5447d) * f2));
        }
    }
}
