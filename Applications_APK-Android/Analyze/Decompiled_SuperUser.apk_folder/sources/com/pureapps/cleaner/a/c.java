package com.pureapps.cleaner.a;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.ContextThemeWrapper;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: Rotate3dAnimation */
public class c extends Animation {

    /* renamed from: a  reason: collision with root package name */
    ContextThemeWrapper f5459a;

    /* renamed from: b  reason: collision with root package name */
    float f5460b = 1.0f;

    /* renamed from: c  reason: collision with root package name */
    private final float f5461c;

    /* renamed from: d  reason: collision with root package name */
    private final float f5462d;

    /* renamed from: e  reason: collision with root package name */
    private final float f5463e;

    /* renamed from: f  reason: collision with root package name */
    private final float f5464f;

    /* renamed from: g  reason: collision with root package name */
    private final float f5465g;

    /* renamed from: h  reason: collision with root package name */
    private final boolean f5466h;
    private Camera i;

    public c(ContextThemeWrapper contextThemeWrapper, float f2, float f3, float f4, float f5, float f6, boolean z) {
        this.f5459a = contextThemeWrapper;
        this.f5461c = f2;
        this.f5462d = f3;
        this.f5463e = f4;
        this.f5464f = f5;
        this.f5465g = f6;
        this.f5466h = z;
        this.f5460b = contextThemeWrapper.getResources().getDisplayMetrics().density;
    }

    public void initialize(int i2, int i3, int i4, int i5) {
        super.initialize(i2, i3, i4, i5);
        this.i = new Camera();
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        float f3 = this.f5461c;
        float f4 = f3 + ((this.f5462d - f3) * f2);
        float f5 = this.f5463e;
        float f6 = this.f5464f;
        Camera camera = this.i;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.f5466h) {
            camera.translate(0.0f, 0.0f, this.f5465g * f2);
        } else {
            camera.translate(0.0f, 0.0f, this.f5465g * (1.0f - f2));
        }
        camera.rotateY(f4);
        camera.getMatrix(matrix);
        camera.restore();
        float[] fArr = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
        matrix.getValues(fArr);
        fArr[6] = fArr[6] / this.f5460b;
        matrix.setValues(fArr);
        matrix.preTranslate(-f5, -f6);
        matrix.postTranslate(f5, f6);
    }
}
