package com.pureapps.cleaner.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/* compiled from: AssetsDatabaseManager */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static String f5664a = "/data/data/%s/databases";

    /* renamed from: d  reason: collision with root package name */
    private static a f5665d = null;

    /* renamed from: b  reason: collision with root package name */
    private Map<String, SQLiteDatabase> f5666b = new HashMap();

    /* renamed from: c  reason: collision with root package name */
    private Context f5667c = null;

    private a(Context context) {
        this.f5667c = context;
    }

    public static a a(Context context) {
        if (f5665d == null) {
            f5665d = new a(context);
        }
        return f5665d;
    }

    public SQLiteDatabase a(String str) {
        if (this.f5666b.get(str) != null) {
            return this.f5666b.get(str);
        }
        if (this.f5667c == null) {
            return null;
        }
        Log.i("AssetsDatabaseManager", String.format("Create database %s.", str));
        String b2 = b(this.f5667c);
        String a2 = a(this.f5667c, str);
        File file = new File(a2);
        SharedPreferences sharedPreferences = this.f5667c.getSharedPreferences(a.class.toString(), 0);
        if (!sharedPreferences.getBoolean(str, false) || !file.exists()) {
            File file2 = new File(b2);
            if (!file2.exists() && !file2.mkdirs()) {
                Log.i("AssetsDatabaseManager", "Create \"" + b2 + "\" failed!");
                return null;
            } else if (!a(this.f5667c, str, a2)) {
                Log.i("AssetsDatabaseManager", String.format("Copy %s to %s failed!", str, a2));
                return null;
            } else {
                sharedPreferences.edit().putBoolean(str, true).commit();
            }
        }
        SQLiteDatabase openDatabase = SQLiteDatabase.openDatabase(a2, null, 16);
        if (openDatabase == null) {
            return openDatabase;
        }
        this.f5666b.put(str, openDatabase);
        return openDatabase;
    }

    public static String b(Context context) {
        return String.format(f5664a, context.getApplicationInfo().packageName);
    }

    public static String a(Context context, String str) {
        return b(context) + "/" + str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0047 A[SYNTHETIC, Splitter:B:10:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r6, java.lang.String r7, java.lang.String r8) {
        /*
            r0 = 0
            java.lang.String r1 = "AssetsDatabaseManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Copy "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r3 = " to "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r1, r2)
            r3 = 0
            android.content.res.AssetManager r1 = r6.getAssets()
            java.io.InputStream r1 = r1.open(r7)     // Catch:{ IOException -> 0x0055 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r2]     // Catch:{ IOException -> 0x0055 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0055 }
            r2.<init>(r8)     // Catch:{ IOException -> 0x0055 }
        L_0x0035:
            int r3 = r1.read(r4)     // Catch:{ IOException -> 0x0041 }
            r5 = -1
            if (r3 == r5) goto L_0x004b
            r5 = 0
            r2.write(r4, r5, r3)     // Catch:{ IOException -> 0x0041 }
            goto L_0x0035
        L_0x0041:
            r1 = move-exception
        L_0x0042:
            r1.printStackTrace()
            if (r2 == 0) goto L_0x004a
            r2.close()     // Catch:{ IOException -> 0x0050 }
        L_0x004a:
            return r0
        L_0x004b:
            r2.close()     // Catch:{ IOException -> 0x0041 }
            r0 = 1
            goto L_0x004a
        L_0x0050:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x0055:
            r1 = move-exception
            r2 = r3
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pureapps.cleaner.db.a.a(android.content.Context, java.lang.String, java.lang.String):boolean");
    }
}
