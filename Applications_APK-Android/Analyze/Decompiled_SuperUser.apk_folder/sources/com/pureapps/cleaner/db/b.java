package com.pureapps.cleaner.db;

import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.kingouser.com.R;
import com.kingouser.com.entity.UninstallAppInfo;
import com.pureapps.cleaner.bean.c;
import com.pureapps.cleaner.bean.d;
import com.pureapps.cleaner.bean.e;
import com.pureapps.cleaner.bean.f;
import com.pureapps.cleaner.bean.g;
import com.pureapps.cleaner.bean.h;
import com.pureapps.cleaner.bean.i;
import com.pureapps.cleaner.util.l;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: DbContent */
public abstract class b {
    public static final String AUTHORITY = "com.kingouser.com.database";
    public static final Uri CONTENT_URI = Uri.parse("content://com.kingouser.com.database");
    private static final int NOT_SAVED = -1;
    public static final String RECORD_ID = "_id";
    public long mId = -1;

    public abstract String[] getContentProjection();

    public abstract <T extends b> T restore(Cursor cursor);

    public abstract ContentValues toContentValues();

    public static <T extends b> T getContent(Cursor cursor, Class<T> cls) {
        try {
            b bVar = (b) cls.newInstance();
            bVar.mId = cursor.getLong(0);
            return bVar.restore(cursor);
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InstantiationException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    public Uri save(Context context) {
        if (isSaved()) {
            throw new UnsupportedOperationException();
        }
        Uri insert = context.getContentResolver().insert(CONTENT_URI, toContentValues());
        this.mId = ContentUris.parseId(insert);
        return insert;
    }

    public boolean isSaved() {
        return this.mId != -1;
    }

    public static void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    public static Cursor query(Context context, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return context.getContentResolver().query(uri, strArr, str, strArr2, str2);
    }

    public static int update(Context context, Uri uri, long j, ContentValues contentValues) {
        return context.getContentResolver().update(ContentUris.withAppendedId(uri, j), contentValues, null, null);
    }

    public static int update(Context context, Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return context.getContentResolver().update(uri, contentValues, str, strArr);
    }

    public static int delete(Context context, Uri uri, String str, String[] strArr) {
        return context.getContentResolver().delete(uri, str, strArr);
    }

    /* JADX INFO: finally extract failed */
    public static int count(Context context, Uri uri, String str, String[] strArr) {
        Cursor query = context.getContentResolver().query(uri, new String[]{"count(*)"}, str, strArr, null);
        try {
            if (query.moveToFirst()) {
                int i = query.getInt(0);
                closeCursor(query);
                return i;
            }
            closeCursor(query);
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            closeCursor(query);
            return 0;
        } catch (Throwable th) {
            closeCursor(query);
            throw th;
        }
    }

    /* compiled from: DbContent */
    public static class a extends b {

        /* renamed from: a  reason: collision with root package name */
        public static final Uri f5668a = Uri.parse("content://com.kingouser.com.database/JunkFiles");

        /* renamed from: b  reason: collision with root package name */
        public String f5669b;

        /* renamed from: c  reason: collision with root package name */
        public int f5670c;

        /* renamed from: d  reason: collision with root package name */
        public String f5671d;

        /* renamed from: e  reason: collision with root package name */
        public String f5672e;

        /* renamed from: f  reason: collision with root package name */
        public int f5673f;

        public ContentValues toContentValues() {
            ContentValues contentValues = new ContentValues();
            contentValues.put("path", this.f5669b);
            contentValues.put("junk_type", Integer.valueOf(this.f5670c));
            contentValues.put("title", this.f5671d);
            contentValues.put(UninstallAppInfo.COLUMN_PKG, this.f5672e);
            contentValues.put("path_type", Integer.valueOf(this.f5673f));
            return null;
        }

        /* renamed from: a */
        public a restore(Cursor cursor) {
            this.mId = cursor.getLong(cursor.getColumnIndex(b.RECORD_ID));
            this.f5669b = cursor.getString(cursor.getColumnIndex("path"));
            this.f5670c = cursor.getInt(cursor.getColumnIndex("junk_type"));
            this.f5671d = cursor.getString(cursor.getColumnIndex("title"));
            this.f5672e = cursor.getString(cursor.getColumnIndex(UninstallAppInfo.COLUMN_PKG));
            this.f5673f = cursor.getInt(cursor.getColumnIndex("path_type"));
            return this;
        }

        public String[] getContentProjection() {
            return new String[]{"path", "junk_type", "title", UninstallAppInfo.COLUMN_PKG, "path_type"};
        }

        public static void a(Context context, ArrayList<g> arrayList) {
            try {
                DbProvider.a(context, "JunkFiles");
                ArrayList arrayList2 = new ArrayList();
                Iterator<g> it = arrayList.iterator();
                while (it.hasNext()) {
                    g next = it.next();
                    if (next instanceof e) {
                        Iterator it2 = ((e) next).f5630f.iterator();
                        while (it2.hasNext()) {
                            i iVar = (i) it2.next();
                            arrayList2.add(ContentProviderOperation.newInsert(f5668a).withValue("path", iVar.l).withValue("junk_type", 0).withValue(UninstallAppInfo.COLUMN_PKG, ((f) iVar).f5624e).withValue("path_type", Integer.valueOf(iVar.m)).build());
                        }
                    } else if (next instanceof com.pureapps.cleaner.bean.b) {
                        Iterator it3 = ((com.pureapps.cleaner.bean.b) next).f5630f.iterator();
                        while (it3.hasNext()) {
                            i iVar2 = (i) it3.next();
                            arrayList2.add(ContentProviderOperation.newInsert(f5668a).withValue("path", iVar2.l).withValue("junk_type", 1).withValue("title", ((c) iVar2).f5614c).withValue("path_type", Integer.valueOf(iVar2.m)).build());
                        }
                    }
                }
                context.getContentResolver().applyBatch(b.AUTHORITY, arrayList2);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        public static ArrayList<a> a(Context context) {
            Cursor cursor;
            ArrayList<a> arrayList = new ArrayList<>();
            try {
                cursor = context.getContentResolver().query(f5668a, null, null, null, null);
                while (cursor.moveToNext()) {
                    try {
                        a aVar = new a();
                        aVar.restore(cursor);
                        arrayList.add(aVar);
                    } catch (Exception e2) {
                        e = e2;
                        try {
                            e.printStackTrace();
                            c.a(cursor);
                            return arrayList;
                        } catch (Throwable th) {
                            th = th;
                            c.a(cursor);
                            throw th;
                        }
                    }
                }
                c.a(cursor);
            } catch (Exception e3) {
                e = e3;
                cursor = null;
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
                c.a(cursor);
                throw th;
            }
            return arrayList;
        }

        public static void b(Context context) {
            try {
                context.getContentResolver().delete(f5668a, null, null);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static h getApkFiles(Context context) {
        PackageInfo packageArchiveInfo;
        h hVar = new h(3);
        hVar.f5633b = context.getString(R.string.ce);
        String[] strArr = {".apk"};
        Uri contentUri = MediaStore.Files.getContentUri("external");
        String[] strArr2 = {"_data", "title"};
        String str = "";
        for (int i = 0; i < strArr.length; i++) {
            if (i != 0) {
                str = str + " OR ";
            }
            str = str + "_data" + " LIKE '%" + strArr[i] + "'";
        }
        Cursor query = context.getContentResolver().query(contentUri, strArr2, str, null, "date_modified");
        if (query == null) {
            return hVar;
        }
        PackageManager packageManager = context.getPackageManager();
        if (query.moveToLast()) {
            do {
                File file = new File(query.getString(query.getColumnIndex("_data")));
                if (file.isFile() && (packageArchiveInfo = packageManager.getPackageArchiveInfo(file.getPath(), 0)) != null) {
                    d dVar = new d();
                    dVar.f5615a = packageArchiveInfo.packageName;
                    dVar.f5617c = file.getPath();
                    dVar.f5629e = file.length();
                    dVar.f5631g = true;
                    if (packageArchiveInfo.applicationInfo != null) {
                        Resources a2 = l.a(context, dVar.f5617c);
                        if (a2 != null) {
                            try {
                                dVar.f5628d = a2.getString(packageArchiveInfo.applicationInfo.labelRes);
                            } catch (Exception e2) {
                                e2.printStackTrace();
                                dVar.f5628d = packageManager.getApplicationLabel(packageArchiveInfo.applicationInfo).toString();
                            }
                            try {
                                dVar.f5616b = a2.getDrawable(packageArchiveInfo.applicationInfo.icon);
                            } catch (Exception e3) {
                                e3.printStackTrace();
                                dVar.f5616b = packageArchiveInfo.applicationInfo.loadIcon(packageManager);
                            }
                        } else {
                            dVar.f5628d = packageManager.getApplicationLabel(packageArchiveInfo.applicationInfo).toString();
                            dVar.f5616b = packageArchiveInfo.applicationInfo.loadIcon(packageManager);
                        }
                    }
                    hVar.f5637f++;
                    hVar.f5635d += dVar.f5629e;
                    hVar.f5634c = hVar.f5635d;
                    hVar.f5636e.add(dVar);
                }
            } while (query.moveToPrevious());
        }
        query.close();
        return hVar;
    }
}
