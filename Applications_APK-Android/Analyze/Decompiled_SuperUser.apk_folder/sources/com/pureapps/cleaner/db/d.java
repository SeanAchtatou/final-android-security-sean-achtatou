package com.pureapps.cleaner.db;

import android.content.Context;
import android.database.Cursor;

/* compiled from: PkgCacheContent */
public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    public long f5674a = -1;

    /* compiled from: PkgCacheContent */
    public static class a extends d {

        /* renamed from: b  reason: collision with root package name */
        public static final String[] f5675b = {b.RECORD_ID, "abbrev"};

        /* renamed from: c  reason: collision with root package name */
        public String f5676c;

        public a a(Cursor cursor) {
            this.f5674a = cursor.getLong(0);
            this.f5676c = cursor.getString(1);
            return this;
        }

        public static a a(Context context, String str) {
            Cursor cursor;
            Throwable th;
            a aVar = null;
            try {
                cursor = a.a(context).a("pkgcache.db").rawQuery("select * from query_abbrev where _id in(" + str + ")", null);
                try {
                    if (cursor.moveToNext()) {
                        a aVar2 = new a();
                        try {
                            aVar2.a(cursor);
                            c.a(cursor);
                            return aVar2;
                        } catch (Exception e2) {
                            Exception exc = e2;
                            aVar = aVar2;
                            e = exc;
                            try {
                                e.printStackTrace();
                                c.a(cursor);
                                return aVar;
                            } catch (Throwable th2) {
                                th = th2;
                                c.a(cursor);
                                throw th;
                            }
                        }
                    } else {
                        c.a(cursor);
                        return null;
                    }
                } catch (Exception e3) {
                    e = e3;
                    e.printStackTrace();
                    c.a(cursor);
                    return aVar;
                }
            } catch (Exception e4) {
                e = e4;
                cursor = null;
            } catch (Throwable th3) {
                cursor = null;
                th = th3;
                c.a(cursor);
                throw th;
            }
        }
    }
}
