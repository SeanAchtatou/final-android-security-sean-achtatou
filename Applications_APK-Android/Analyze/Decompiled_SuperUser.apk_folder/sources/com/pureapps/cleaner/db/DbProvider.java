package com.pureapps.cleaner.db;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;
import com.kingouser.com.entity.UninstallAppInfo;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.File;
import java.util.ArrayList;

public class DbProvider extends ContentProvider {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f5661a = {"apiCache", "ignoreList", "download", "analytics", "JunkFiles"};

    /* renamed from: b  reason: collision with root package name */
    private static final UriMatcher f5662b = new UriMatcher(-1);

    /* renamed from: c  reason: collision with root package name */
    private static SQLiteDatabase f5663c;

    static {
        f5662b.addURI(b.AUTHORITY, "apiCache", 0);
        f5662b.addURI(b.AUTHORITY, "apiCache/#", 1);
        f5662b.addURI(b.AUTHORITY, "ignoreList", FileUtils.FileMode.MODE_IRUSR);
        f5662b.addURI(b.AUTHORITY, "ignoreList/#", 257);
        f5662b.addURI(b.AUTHORITY, "download", FileUtils.FileMode.MODE_ISVTX);
        f5662b.addURI(b.AUTHORITY, "download/#", 513);
        f5662b.addURI(b.AUTHORITY, "analytics", 768);
        f5662b.addURI(b.AUTHORITY, "analytics/#", 769);
        f5662b.addURI(b.AUTHORITY, "JunkFiles", FileUtils.FileMode.MODE_ISGID);
        f5662b.addURI(b.AUTHORITY, "JunkFiles/#", 1025);
    }

    public static synchronized SQLiteDatabase a(Context context) {
        SQLiteDatabase sQLiteDatabase;
        synchronized (DbProvider.class) {
            if (f5663c != null) {
                sQLiteDatabase = f5663c;
            } else {
                f5663c = new DatabaseHelper(context).getWritableDatabase();
                sQLiteDatabase = f5663c;
            }
        }
        return sQLiteDatabase;
    }

    public boolean onCreate() {
        return false;
    }

    public void shutdown() {
        super.shutdown();
        onCreate();
    }

    /* access modifiers changed from: protected */
    public void a(Uri uri, ContentObserver contentObserver) {
        getContext().getContentResolver().notifyChange(uri, contentObserver);
    }

    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> arrayList) {
        a();
        SQLiteDatabase a2 = a(getContext());
        a2.beginTransaction();
        try {
            ContentProviderResult[] applyBatch = super.applyBatch(arrayList);
            a2.setTransactionSuccessful();
            a(b.CONTENT_URI, (ContentObserver) null);
            return applyBatch;
        } finally {
            a2.endTransaction();
        }
    }

    public String getType(Uri uri) {
        switch (f5662b.match(uri)) {
            case 0:
                return "vnd.android.cursor.dir/apiCache";
            case 1:
                return "vnd.android.cursor.item/apiCache";
            case FileUtils.FileMode.MODE_IRUSR:
                return "vnd.android.cursor.dir/ignoreList";
            case 257:
                return "vnd.android.cursor.item/ignoreList";
            case FileUtils.FileMode.MODE_ISVTX:
                return "vnd.android.cursor.dir/download";
            case 513:
                return "vnd.android.cursor.item/download";
            case 768:
                return "vnd.android.cursor.dir/analytics";
            case 769:
                return "vnd.android.cursor.item/analytics";
            case FileUtils.FileMode.MODE_ISGID:
                return "vnd.android.cursor.dir/JunkFiles";
            case 1025:
                return "vnd.android.cursor.item/JunkFiles";
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        Cursor query;
        a();
        int match = f5662b.match(uri);
        int i = match >> 8;
        Context context = getContext();
        SQLiteDatabase a2 = a(context);
        Uri uri2 = b.CONTENT_URI;
        switch (match) {
            case 0:
            case FileUtils.FileMode.MODE_IRUSR:
            case FileUtils.FileMode.MODE_ISVTX:
            case 768:
            case FileUtils.FileMode.MODE_ISGID:
                query = a2.query(f5661a[i], strArr, str, strArr2, null, null, str2);
                break;
            case 1:
            case 257:
            case 513:
            case 769:
            case 1025:
                String[] strArr3 = strArr;
                query = a2.query(f5661a[i], strArr3, a(ContentUris.parseId(uri), str), strArr2, null, null, str2);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (query != null && isTemporary()) {
            query.setNotificationUri(context.getContentResolver(), uri2);
        }
        return query;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        int update;
        a();
        int match = f5662b.match(uri);
        int i = match >> 8;
        SQLiteDatabase a2 = a(getContext());
        switch (match) {
            case 0:
            case FileUtils.FileMode.MODE_IRUSR:
            case FileUtils.FileMode.MODE_ISVTX:
            case 768:
            case FileUtils.FileMode.MODE_ISGID:
                update = a2.update(f5661a[i], contentValues, str, strArr);
                break;
            case 1:
            case 257:
            case 513:
            case 769:
            case 1025:
                update = a2.update(f5661a[i], contentValues, a(ContentUris.parseId(uri), str), strArr);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (update > 0) {
            a(uri, (ContentObserver) null);
        }
        return update;
    }

    public int delete(Uri uri, String str, String[] strArr) {
        int delete;
        a();
        int match = f5662b.match(uri);
        int i = match >> 8;
        SQLiteDatabase a2 = a(getContext());
        switch (match) {
            case 0:
            case FileUtils.FileMode.MODE_IRUSR:
            case FileUtils.FileMode.MODE_ISVTX:
            case 768:
            case FileUtils.FileMode.MODE_ISGID:
                delete = a2.delete(f5661a[i], str, strArr);
                break;
            case 1:
            case 257:
            case 513:
            case 769:
            case 1025:
                delete = a2.delete(f5661a[i], a(ContentUris.parseId(uri), str), strArr);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (delete > 0 && !a2.inTransaction()) {
            a(Uri.parse(b.CONTENT_URI + "/" + uri.getPathSegments().get(0)), (ContentObserver) null);
        }
        return delete;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        a();
        int match = f5662b.match(uri);
        int i = match >> 8;
        SQLiteDatabase a2 = a(getContext());
        switch (match) {
            case 0:
            case FileUtils.FileMode.MODE_IRUSR:
            case FileUtils.FileMode.MODE_ISVTX:
            case 768:
            case FileUtils.FileMode.MODE_ISGID:
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        Uri withAppendedId = ContentUris.withAppendedId(uri, a2.insert(f5661a[i], null, contentValues));
        a(uri, (ContentObserver) null);
        return withAppendedId;
    }

    private void a() {
        try {
            if (f5663c != null) {
                String path = f5663c.getPath();
                if (path == null || !new File(path).exists()) {
                    shutdown();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void a(Context context, String str) {
        SQLiteDatabase a2 = a(context);
        a2.execSQL("delete from " + str);
        a2.execSQL("update sqlite_sequence set seq=0 where name='" + str + "'");
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, "database.db", (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            e(sQLiteDatabase);
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }

        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            if (i2 == 1) {
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS apiCache");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ignoreList");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS download");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS JunkFiles");
                e(sQLiteDatabase);
            }
        }

        private void e(SQLiteDatabase sQLiteDatabase) {
            b(sQLiteDatabase);
            c(sQLiteDatabase);
            d(sQLiteDatabase);
            a(sQLiteDatabase);
        }

        /* access modifiers changed from: package-private */
        public void a(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS JunkFiles(_id INTEGER PRIMARY KEY autoincrement, path TEXT, junk_type INTEGER default 0, title TEXT, pkg TEXT, path_type INTEGER default 101)");
        }

        /* access modifiers changed from: package-private */
        public void b(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS apiCache(_id INTEGER PRIMARY KEY autoincrement, url TEXT, time INTEGER default 0, data BLOB)");
            a(sQLiteDatabase, "apiCache", "url");
        }

        /* access modifiers changed from: package-private */
        public void c(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS ignoreList(_id INTEGER PRIMARY KEY autoincrement, pkg TEXT)");
        }

        /* access modifiers changed from: package-private */
        public void d(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS download(_id INTEGER PRIMARY KEY autoincrement, sId TEXT, type INTEGER default 0, tn TEXT, tv TEXT, tvc INTEGER default 0, ts INTEGER default 0, du TEXT, dt INTEGER default 0, iu TEXT, state INTEGER default 0, pkg TEXT, md5 TEXT, fromId INTEGER default 0, openType INTEGER default 1)");
            a(sQLiteDatabase, "download", UninstallAppInfo.COLUMN_PKG);
        }

        private void a(SQLiteDatabase sQLiteDatabase, String str, String str2) {
            sQLiteDatabase.execSQL("create index " + str + '_' + str2 + " on " + str + " (" + str2 + ");");
        }
    }

    private String a(long j, String str) {
        StringBuilder sb = new StringBuilder((int) FileUtils.FileMode.MODE_IRUSR);
        sb.append("_id=");
        sb.append(j);
        if (!TextUtils.isEmpty(str)) {
            sb.append(" AND (");
            sb.append(str);
            sb.append(')');
        }
        return sb.toString();
    }
}
