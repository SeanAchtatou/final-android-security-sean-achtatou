package com.pureapps.cleaner;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.muzakki.ahmad.widget.CollapsingToolbarLayout;

public class MemoryBoostActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private MemoryBoostActivity f5413a;

    /* renamed from: b  reason: collision with root package name */
    private View f5414b;

    public MemoryBoostActivity_ViewBinding(final MemoryBoostActivity memoryBoostActivity, View view) {
        this.f5413a = memoryBoostActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.dd, "field 'mBtBoost' and method 'onClick'");
        memoryBoostActivity.mBtBoost = (Button) Utils.castView(findRequiredView, R.id.dd, "field 'mBtBoost'", Button.class);
        this.f5414b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                memoryBoostActivity.onClick(view);
            }
        });
        memoryBoostActivity.mListView = (ExpandableListView) Utils.findRequiredViewAsType(view, R.id.d8, "field 'mListView'", ExpandableListView.class);
        memoryBoostActivity.mLayoutAnimCooldown = (ViewGroup) Utils.findRequiredViewAsType(view, R.id.de, "field 'mLayoutAnimCooldown'", ViewGroup.class);
        memoryBoostActivity.mLayoutAnimBoost = (ViewGroup) Utils.findRequiredViewAsType(view, R.id.df, "field 'mLayoutAnimBoost'", ViewGroup.class);
        memoryBoostActivity.mIvAnimRocket = (ImageView) Utils.findRequiredViewAsType(view, R.id.dg, "field 'mIvAnimRocket'", ImageView.class);
        memoryBoostActivity.mIvAnimCloud = (ImageView) Utils.findRequiredViewAsType(view, R.id.dh, "field 'mIvAnimCloud'", ImageView.class);
        memoryBoostActivity.mSelectAppText = (TextView) Utils.findRequiredViewAsType(view, R.id.da, "field 'mSelectAppText'", TextView.class);
        memoryBoostActivity.mToolBarLayout = (CollapsingToolbarLayout) Utils.findRequiredViewAsType(view, R.id.d5, "field 'mToolBarLayout'", CollapsingToolbarLayout.class);
        memoryBoostActivity.mProgressLayout = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.db, "field 'mProgressLayout'", FrameLayout.class);
        memoryBoostActivity.mProgressImg = (ImageView) Utils.findRequiredViewAsType(view, R.id.dc, "field 'mProgressImg'", ImageView.class);
    }

    public void unbind() {
        MemoryBoostActivity memoryBoostActivity = this.f5413a;
        if (memoryBoostActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f5413a = null;
        memoryBoostActivity.mBtBoost = null;
        memoryBoostActivity.mListView = null;
        memoryBoostActivity.mLayoutAnimCooldown = null;
        memoryBoostActivity.mLayoutAnimBoost = null;
        memoryBoostActivity.mIvAnimRocket = null;
        memoryBoostActivity.mIvAnimCloud = null;
        memoryBoostActivity.mSelectAppText = null;
        memoryBoostActivity.mToolBarLayout = null;
        memoryBoostActivity.mProgressLayout = null;
        memoryBoostActivity.mProgressImg = null;
        this.f5414b.setOnClickListener(null);
        this.f5414b = null;
    }
}
