package com.pureapps.cleaner;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.kingouser.com.R;
import com.kingouser.com.application.App;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.muzakki.ahmad.widget.CollapsingToolbarLayout;
import com.pureapps.cleaner.a.d;
import com.pureapps.cleaner.adapter.MemoryBoostListAdapter;
import com.pureapps.cleaner.b.c;
import com.pureapps.cleaner.bean.j;
import com.pureapps.cleaner.manager.f;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.k;
import com.pureapps.cleaner.util.l;
import com.pureapps.cleaner.view.BBaseActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class MemoryBoostActivity extends BBaseActivity implements ExpandableListView.OnChildClickListener, ExpandableListView.OnGroupClickListener, c {
    private com.pureapps.cleaner.manager.b A = null;
    @BindView(R.id.dd)
    Button mBtBoost;
    @BindView(R.id.dh)
    ImageView mIvAnimCloud;
    @BindView(R.id.dg)
    ImageView mIvAnimRocket;
    @BindView(R.id.df)
    ViewGroup mLayoutAnimBoost;
    @BindView(R.id.de)
    ViewGroup mLayoutAnimCooldown;
    @BindView(R.id.d8)
    ExpandableListView mListView;
    @BindView(R.id.dc)
    ImageView mProgressImg;
    @BindView(R.id.db)
    FrameLayout mProgressLayout;
    @BindView(R.id.da)
    TextView mSelectAppText;
    @BindView(R.id.d5)
    CollapsingToolbarLayout mToolBarLayout;
    /* access modifiers changed from: private */
    public b n;
    /* access modifiers changed from: private */
    public MemoryBoostListAdapter o;
    private a p;
    /* access modifiers changed from: private */
    public d q;
    /* access modifiers changed from: private */
    public com.pureapps.cleaner.a.b r;
    private ObjectAnimator s;
    /* access modifiers changed from: private */
    public int u = -1;
    /* access modifiers changed from: private */
    public int v;
    private boolean w = false;
    private boolean x = false;
    private boolean y = false;
    private boolean z = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.a6);
        Toolbar toolbar = (Toolbar) findViewById(R.id.cu);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.d9);
        if (Build.VERSION.SDK_INT >= 19) {
            relativeLayout.setPadding(0, m(), 0, 0);
        }
        a(toolbar);
        f().a(true);
        this.n = new b();
        this.mToolBarLayout.setCollapsedTitleGravity(1);
        this.o = new MemoryBoostListAdapter(this);
        this.mListView.setAdapter(this.o);
        this.mListView.setOnChildClickListener(this);
        this.mListView.setOnGroupClickListener(this);
        this.A = new com.pureapps.cleaner.manager.b(this, false);
        this.s = ObjectAnimator.ofFloat(this.mProgressImg, "rotation", 0.0f, 360.0f);
        this.s.setDuration(1500L);
        this.s.setRepeatCount(-1);
        this.s.setInterpolator(new LinearInterpolator());
        n();
        j();
        q();
        if (getIntent().hasExtra("memory_notification_click") && getIntent().getBooleanExtra("memory_notification_click", false)) {
            i.a(this).n();
            this.y = true;
        }
        if (getIntent().hasExtra("notification_from_click")) {
            this.y = getIntent().getBooleanExtra("notification_from_click", false);
            if (this.y) {
                f.a(this).f();
            }
        }
        if (getIntent().hasExtra("notification_cpu_click")) {
            f.b(this);
            this.z = getIntent().getBooleanExtra("notification_cpu_click", false);
        }
        if (getIntent().hasExtra("notification_click_event_action")) {
            com.pureapps.cleaner.analytic.a.a(this).c(this.t, getIntent().getStringExtra("notification_click_event_action"));
        }
        com.pureapps.cleaner.b.a.a(this);
    }

    private void n() {
        this.mBtBoost.post(new Runnable() {
            public void run() {
                int unused = MemoryBoostActivity.this.u = MemoryBoostActivity.this.mBtBoost.getHeight();
                MemoryBoostActivity.this.o();
            }
        });
    }

    /* access modifiers changed from: private */
    public void o() {
        if (this.mListView != null && this.u != -1) {
            this.mListView.setPadding(0, 0, 0, (int) (((float) this.u) * 1.0f));
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.pureapps.cleaner.analytic.a.a(this).b(this.t, "MemoryBoost");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        k.a(this.p, true);
        this.A.d();
        com.pureapps.cleaner.b.a.b(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.f8343c, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 16908332) {
            finish();
            return true;
        } else if (itemId != R.id.mx) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            com.pureapps.cleaner.analytic.a.a(this).c(this.t, "BtnBoostAddIgnoreList");
            Intent intent = new Intent();
            intent.setClass(this, IgnoreListActivity.class);
            intent.setFlags(268435456);
            startActivity(intent);
            return true;
        }
    }

    @OnClick({2131624087})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dd /*2131624087*/:
                l();
                return;
            default:
                return;
        }
    }

    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        j a2 = this.o.getChild(i, i2);
        if (a2.f5638a != 0) {
            return false;
        }
        com.pureapps.cleaner.bean.k b2 = this.o.getGroup(i);
        if (a2.f5644g) {
            a2.f5644g = false;
            b2.f5649d--;
        } else {
            a2.f5644g = true;
            b2.f5649d++;
        }
        this.o.notifyDataSetChanged();
        j();
        return true;
    }

    public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long j) {
        return true;
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (i != 1) {
            super.onRequestPermissionsResult(i, strArr, iArr);
        } else if (iArr[0] == 0) {
            q();
        } else {
            App.a().a((int) R.string.c8);
        }
    }

    @SuppressLint({"StringFormatMatches"})
    public void j() {
        String substring;
        String str;
        long j;
        long j2 = 0;
        com.pureapps.cleaner.bean.k b2 = this.o.b();
        if (b2 != null && b2.f5648c.size() > 0) {
            Iterator<j> it = b2.f5648c.iterator();
            while (true) {
                j = j2;
                if (!it.hasNext()) {
                    break;
                }
                j2 = it.next().f5642e + j;
            }
            j2 = j;
        }
        String b3 = l.b(j2);
        if (j2 >= 1099511627776L) {
            substring = b3.substring(0, b3.length() - 2);
            str = "TB";
        } else if (j2 >= 1073741824) {
            substring = b3.substring(0, b3.length() - 2);
            str = "GB";
        } else if (j2 >= 1048576) {
            substring = b3.substring(0, b3.length() - 2);
            str = "MB";
        } else if (j2 >= 1024) {
            substring = b3.substring(0, b3.length() - 2);
            str = "KB";
        } else {
            substring = b3.substring(0, b3.length() - 1);
            str = "B";
        }
        this.mToolBarLayout.setSubtitle(l.a(substring));
        this.mToolBarLayout.setSubTitleUnit(str);
        this.mToolBarLayout.setMessageTitle("    " + getString(R.string.cy));
        int i = b2.f5649d;
        this.mSelectAppText.setText(String.format(getString(R.string.e1), Integer.valueOf(i)));
        a(p(), l.a(getApplicationContext(), p()));
    }

    private long p() {
        long j = 0;
        ArrayList<j> arrayList = this.o.b().f5648c;
        if (arrayList != null && arrayList.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= arrayList.size()) {
                    break;
                }
                j jVar = arrayList.get(i2);
                if (jVar.f5644g) {
                    j += jVar.f5642e;
                }
                i = i2 + 1;
            }
        }
        return j;
    }

    private void a(long j, String str) {
        if (j <= 0 || str.startsWith("0.00")) {
            this.mBtBoost.setEnabled(false);
            this.mBtBoost.setSelected(false);
            this.mBtBoost.setTextColor(getResources().getColor(R.color.a_));
            this.mBtBoost.setText(getResources().getString(R.string.ao));
            return;
        }
        this.mBtBoost.setSelected(true);
        this.mBtBoost.setEnabled(true);
        this.mBtBoost.setTextColor(getResources().getColor(R.color.a_));
        this.mBtBoost.setText(getResources().getString(R.string.ao) + " " + str);
    }

    public void k() {
        if (!this.A.c()) {
            this.mLayoutAnimCooldown.setVisibility(0);
            this.q = new d(this, this.mLayoutAnimCooldown, null);
            c(1);
            com.pureapps.cleaner.analytic.a.a(this).c(this.t, "BtnCoolDown");
        }
    }

    public void l() {
        if (!this.w) {
            this.mLayoutAnimBoost.setVisibility(0);
            this.r = new com.pureapps.cleaner.a.b(this, this.mLayoutAnimBoost, this.mBtBoost, this.mIvAnimRocket, this.mIvAnimCloud);
            c(0);
            com.pureapps.cleaner.analytic.a.a(this).c(this.t, "BtnMemoryBoost");
        }
    }

    private void c(int i) {
        if (!this.x) {
            this.x = true;
            if (i == 0) {
                com.pureapps.cleaner.manager.a.a().a(5);
            } else {
                com.pureapps.cleaner.manager.a.a().a(1);
            }
            this.mBtBoost.setVisibility(4);
            k.a(this.p, true);
            this.p = new a();
            this.p.executeOnExecutor(k.a().b(), Integer.valueOf(i));
        }
    }

    public void a(int i, long j, Object obj) {
        switch (i) {
            case 8:
                this.o.a((com.pureapps.cleaner.bean.k) obj);
                j();
                this.mListView.expandGroup(0);
                return;
            case 9:
                com.pureapps.cleaner.bean.k kVar = (com.pureapps.cleaner.bean.k) obj;
                this.o.a(kVar);
                Collections.sort(kVar.f5648c, new Comparator<j>() {
                    /* renamed from: a */
                    public int compare(j jVar, j jVar2) {
                        if (jVar.f5644g != jVar2.f5644g && !jVar.f5644g) {
                            return 1;
                        }
                        return -1;
                    }
                });
                this.v = l.e(this);
                this.o.a(this.v);
                this.o.notifyDataSetChanged();
                this.w = false;
                this.mBtBoost.setVisibility(0);
                this.o.a(false);
                int groupCount = this.o.getGroupCount();
                for (int i2 = 0; i2 < groupCount; i2++) {
                    this.mListView.expandGroup(i2);
                }
                this.mListView.expandGroup(1);
                this.s.cancel();
                this.mProgressLayout.setVisibility(8);
                if (this.y) {
                    l();
                }
                if (this.z) {
                    this.n.postDelayed(new Runnable() {
                        public void run() {
                            MemoryBoostActivity.this.k();
                        }
                    }, 1000);
                    return;
                }
                return;
            case 17:
                if (this.A != null) {
                    ArrayList<j> a2 = this.A.a();
                    ArrayList<com.pureapps.cleaner.bean.a> a3 = com.pureapps.cleaner.bean.a.a(this);
                    com.pureapps.cleaner.bean.k b2 = this.o.b();
                    int i3 = 0;
                    while (a2 != null && i3 < a2.size()) {
                        if (!b(a3, a2.get(i3).f5639b) && !a(b2.f5648c, a2.get(i3).f5639b)) {
                            b2.f5648c.add(a2.get(i3));
                        }
                        i3++;
                    }
                    Collections.sort(b2.f5648c, new Comparator<j>() {
                        /* renamed from: a */
                        public int compare(j jVar, j jVar2) {
                            if (jVar.f5644g != jVar2.f5644g && !jVar.f5644g) {
                                return 1;
                            }
                            return -1;
                        }
                    });
                    this.o.notifyDataSetChanged();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public boolean a(ArrayList<j> arrayList, String str) {
        int i = 0;
        while (arrayList != null && i < arrayList.size()) {
            if (arrayList.get(i).f5639b.equals(str)) {
                return true;
            }
            i++;
        }
        return false;
    }

    public boolean b(ArrayList<com.pureapps.cleaner.bean.a> arrayList, String str) {
        int i = 0;
        while (arrayList != null && i < arrayList.size()) {
            if (arrayList.get(i).f5608c.equals(str)) {
                return true;
            }
            i++;
        }
        return false;
    }

    class a extends AsyncTask<Integer, String, Long> {

        /* renamed from: b  reason: collision with root package name */
        private int f5405b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public int f5406c;

        /* renamed from: d  reason: collision with root package name */
        private int f5407d = 0;

        a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Long doInBackground(Integer... numArr) {
            long j;
            long j2;
            long j3 = 200;
            this.f5405b = Integer.valueOf(numArr[0].intValue()).intValue();
            ActivityManager activityManager = (ActivityManager) MemoryBoostActivity.this.getSystemService(ServiceManagerNative.ACTIVITY);
            com.pureapps.cleaner.bean.k b2 = MemoryBoostActivity.this.o.b();
            i.a(MemoryBoostActivity.this).u();
            if (b2 != null) {
                if (b2.f5649d == 0) {
                    j2 = 200;
                } else {
                    j2 = (long) (5000 / b2.f5649d);
                }
                if (j2 < 200) {
                    j3 = j2;
                }
                Iterator<j> it = b2.f5648c.iterator();
                j = 0;
                while (it.hasNext()) {
                    j next = it.next();
                    if (next.f5644g) {
                        activityManager.killBackgroundProcesses(next.f5639b);
                        j += next.f5642e;
                        publishProgress(next.f5639b);
                        this.f5407d++;
                        if (this.f5405b == 1) {
                            try {
                                Thread.sleep(j3);
                            } catch (Exception e2) {
                            }
                        }
                    }
                }
            } else {
                j = 0;
            }
            if (this.f5405b == 1) {
                this.f5406c = l.e(MemoryBoostActivity.this);
            }
            return Long.valueOf(j);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(String... strArr) {
            super.onProgressUpdate(strArr);
            if (this.f5405b == 1) {
                MemoryBoostActivity.this.q.a();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(final Long l) {
            int i;
            super.onPostExecute(l);
            if (this.f5405b == 1) {
                if (this.f5407d < 5) {
                    i = 5 - this.f5407d;
                    for (int i2 = 0; i2 < i; i2++) {
                        MemoryBoostActivity.this.n.postDelayed(new Runnable() {
                            public void run() {
                                MemoryBoostActivity.this.q.a();
                            }
                        }, (long) (i2 * 200));
                    }
                } else {
                    i = 0;
                }
                MemoryBoostActivity.this.n.postDelayed(new Runnable() {
                    public void run() {
                        MemoryBoostActivity.this.mLayoutAnimCooldown.setVisibility(8);
                        BoostResultActivity.a(MemoryBoostActivity.this, 2, String.valueOf(MemoryBoostActivity.this.v - a.this.f5406c));
                        MemoryBoostActivity.this.finish();
                    }
                }, (long) (((i + 1) * 200) + 600));
                return;
            }
            MemoryBoostActivity.this.r.a();
            MemoryBoostActivity.this.n.postDelayed(new Runnable() {
                public void run() {
                    MemoryBoostActivity.this.mLayoutAnimBoost.setVisibility(8);
                    BoostResultActivity.a(MemoryBoostActivity.this, 0, String.valueOf(l));
                    MemoryBoostActivity.this.finish();
                }
            }, 1200);
        }
    }

    private void q() {
        this.w = true;
        this.A.d();
        this.mProgressLayout.setVisibility(0);
        this.s.start();
        this.A.b();
    }

    class b extends Handler {
        b() {
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
        }
    }
}
