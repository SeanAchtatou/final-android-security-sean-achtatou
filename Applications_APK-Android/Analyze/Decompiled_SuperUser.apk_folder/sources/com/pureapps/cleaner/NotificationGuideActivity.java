package com.pureapps.cleaner;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.duapps.ad.AdError;
import com.kingouser.com.R;
import com.pureapps.cleaner.b.c;
import com.pureapps.cleaner.service.NotificationMonitorService;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.view.BBaseActivity;

public class NotificationGuideActivity extends BBaseActivity implements c {
    @BindView(R.id.dw)
    TextView btnUse;
    @BindView(R.id.bg)
    View image;
    @BindView(R.id.dm)
    View layout;
    @BindView(R.id.dt)
    View layout1;
    @BindView(R.id.dv)
    View layout2;
    @BindView(R.id.du)
    View layout3;
    /* access modifiers changed from: private */
    public ScaleAnimation n;
    /* access modifiers changed from: private */
    public TranslateAnimation o;
    /* access modifiers changed from: private */
    public ObjectAnimator p;
    /* access modifiers changed from: private */
    public ObjectAnimator q;
    /* access modifiers changed from: private */
    public ObjectAnimator r;
    /* access modifiers changed from: private */
    public ObjectAnimator s;
    @BindView(R.id.dr)
    View text;
    private a u;
    /* access modifiers changed from: private */
    public Handler v = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    NotificationGuideActivity.this.p.start();
                    NotificationGuideActivity.this.q.start();
                    NotificationGuideActivity.this.image.setVisibility(0);
                    NotificationGuideActivity.this.image.startAnimation(NotificationGuideActivity.this.n);
                    break;
                case 1:
                    NotificationGuideActivity.this.a(NotificationGuideActivity.this.layout3, NotificationGuideActivity.this.view1, 500);
                    break;
                case 2:
                    NotificationGuideActivity.this.a(NotificationGuideActivity.this.view1);
                    NotificationGuideActivity.this.a(NotificationGuideActivity.this.layout2, NotificationGuideActivity.this.view2, 450);
                    break;
                case 3:
                    NotificationGuideActivity.this.a(NotificationGuideActivity.this.layout1, NotificationGuideActivity.this.view3, (int) AdError.NETWORK_ERROR_CODE);
                    NotificationGuideActivity.this.a(NotificationGuideActivity.this.view2);
                    break;
                case 4:
                    NotificationGuideActivity.this.a(NotificationGuideActivity.this.view3);
                    break;
                case 5:
                    NotificationGuideActivity.this.view4.setVisibility(0);
                    NotificationGuideActivity.this.view4.startAnimation(NotificationGuideActivity.this.o);
                    break;
                case 6:
                    NotificationGuideActivity.this.a(NotificationGuideActivity.this.text);
                    break;
                case 7:
                    NotificationGuideActivity.this.r.start();
                    NotificationGuideActivity.this.s.start();
                    break;
                case 8:
                    NotificationGuideActivity.this.btnUse.setVisibility(0);
                    break;
            }
            super.handleMessage(message);
        }
    };
    @BindView(R.id.dn)
    View view1;
    @BindView(R.id.f9do)
    View view2;
    @BindView(R.id.dq)
    View view3;
    @BindView(R.id.dp)
    View view4;

    public static void a(Context context) {
        context.startActivity(new Intent(context, NotificationGuideActivity.class));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        getWindow().requestFeature(1);
        super.onCreate(bundle);
        com.pureapps.cleaner.b.a.a(this);
        setContentView((int) R.layout.a8);
        j();
        this.u = new a();
        this.u.start();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.pureapps.cleaner.b.a.b(this);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.pureapps.cleaner.analytic.a.a(this).b(this.t, "NotificationGuideView");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.NotificationManagerActivity.a(android.content.Context, boolean):void
     arg types: [com.pureapps.cleaner.NotificationGuideActivity, int]
     candidates:
      com.pureapps.cleaner.NotificationManagerActivity.a(com.pureapps.cleaner.NotificationManagerActivity, boolean):boolean
      android.support.v4.app.FragmentActivity.a(android.view.View, android.view.Menu):boolean
      com.pureapps.cleaner.NotificationManagerActivity.a(android.content.Context, boolean):void */
    @OnClick({2131624106})
    public void onClick(View view) {
        i.a(this).d(true);
        if (!NotificationMonitorService.b(this)) {
            com.pureapps.cleaner.manager.c.a();
            com.pureapps.cleaner.manager.c.a(this);
            NotificationMonitorService.a((Activity) this);
            return;
        }
        NotificationManagerActivity.a((Context) this, true);
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.NotificationManagerActivity.a(android.content.Context, boolean):void
     arg types: [com.pureapps.cleaner.NotificationGuideActivity, int]
     candidates:
      com.pureapps.cleaner.NotificationManagerActivity.a(com.pureapps.cleaner.NotificationManagerActivity, boolean):boolean
      android.support.v4.app.FragmentActivity.a(android.view.View, android.view.Menu):boolean
      com.pureapps.cleaner.NotificationManagerActivity.a(android.content.Context, boolean):void */
    public void a(int i, long j, Object obj) {
        switch (i) {
            case 19:
                if (i.a(this).l()) {
                    NotificationManagerActivity.a((Context) this, true);
                    finish();
                    return;
                }
                return;
            default:
                return;
        }
    }

    class a extends Thread implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        Message f5418a;

        a() {
        }

        public void run() {
            super.run();
            SystemClock.sleep(1000);
            this.f5418a = new Message();
            this.f5418a.what = 0;
            NotificationGuideActivity.this.v.sendMessage(this.f5418a);
            SystemClock.sleep(1000);
            this.f5418a = new Message();
            this.f5418a.what = 1;
            NotificationGuideActivity.this.v.sendMessage(this.f5418a);
            SystemClock.sleep(500);
            this.f5418a = new Message();
            this.f5418a.what = 2;
            NotificationGuideActivity.this.v.sendMessage(this.f5418a);
            SystemClock.sleep(300);
            this.f5418a = new Message();
            this.f5418a.what = 3;
            NotificationGuideActivity.this.v.sendMessage(this.f5418a);
            SystemClock.sleep(300);
            this.f5418a = new Message();
            this.f5418a.what = 4;
            NotificationGuideActivity.this.v.sendMessage(this.f5418a);
            SystemClock.sleep(700);
            this.f5418a = new Message();
            this.f5418a.what = 5;
            NotificationGuideActivity.this.v.sendMessage(this.f5418a);
            SystemClock.sleep(500);
            this.f5418a = new Message();
            this.f5418a.what = 6;
            NotificationGuideActivity.this.v.sendMessage(this.f5418a);
            SystemClock.sleep(500);
            this.f5418a = new Message();
            this.f5418a.what = 7;
            NotificationGuideActivity.this.v.sendMessage(this.f5418a);
            SystemClock.sleep(1000);
            this.f5418a = new Message();
            this.f5418a.what = 8;
            NotificationGuideActivity.this.v.sendMessage(this.f5418a);
        }
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        view.setVisibility(0);
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.u));
    }

    private void j() {
        this.n = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        this.n.setDuration(500);
        this.n.setInterpolator(new OvershootInterpolator());
        this.o = new TranslateAnimation((float) (-a(28.0f, getResources())), 0.0f, 0.0f, 0.0f);
        this.o.setDuration(700);
        this.p = ObjectAnimator.ofFloat(this.layout, "scaleX", 1.0f, 1.2f);
        this.p.setDuration(350L);
        this.q = ObjectAnimator.ofFloat(this.layout, "scaleY", 1.0f, 1.15f);
        this.q.setDuration(350L);
        this.r = ObjectAnimator.ofFloat(this.layout, "scaleX", 1.2f, 1.0f);
        this.r.setDuration(500L);
        this.s = ObjectAnimator.ofFloat(this.layout, "scaleY", 1.15f, 1.0f);
        this.s.setDuration(500L);
        k();
        this.btnUse.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void a(View view, View view5, int i) {
        view.getHeight();
        view.getWidth();
        view5.getHeight();
        view5.getWidth();
        int[] iArr = {9, 9};
        int[] iArr2 = {9, 9};
        view.getLocationOnScreen(iArr);
        this.layout.getLocationOnScreen(iArr2);
        ObjectAnimator.ofFloat(view, "translationY", view.getTranslationY(), view.getTranslationY() - ((float) (iArr[1] - iArr2[1]))).setDuration((long) i).start();
        ObjectAnimator.ofFloat(view, "scaleX", 1.0f, 0.0f).setDuration((long) i).start();
        ObjectAnimator.ofFloat(view, "scaleY", 1.0f, 0.0f).setDuration((long) i).start();
        ObjectAnimator.ofFloat(view, "alpha", 1.0f, 0.0f).setDuration((long) (i + 200)).start();
    }

    private void k() {
        this.view1.setVisibility(4);
        this.view2.setVisibility(4);
        this.view3.setVisibility(4);
        this.view4.setVisibility(4);
        this.text.setVisibility(4);
        this.image.setVisibility(4);
    }

    public static int a(float f2, Resources resources) {
        return (int) TypedValue.applyDimension(1, f2, resources.getDisplayMetrics());
    }
}
