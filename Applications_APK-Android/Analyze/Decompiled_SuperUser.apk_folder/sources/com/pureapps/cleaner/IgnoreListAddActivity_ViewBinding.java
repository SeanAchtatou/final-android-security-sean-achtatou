package com.pureapps.cleaner;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;

public class IgnoreListAddActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private IgnoreListAddActivity f5385a;

    /* renamed from: b  reason: collision with root package name */
    private View f5386b;

    public IgnoreListAddActivity_ViewBinding(final IgnoreListAddActivity ignoreListAddActivity, View view) {
        this.f5385a = ignoreListAddActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.fs, "field 'mBtnAdd' and method 'onClick'");
        ignoreListAddActivity.mBtnAdd = (Button) Utils.castView(findRequiredView, R.id.fs, "field 'mBtnAdd'", Button.class);
        this.f5386b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                ignoreListAddActivity.onClick(view);
            }
        });
        ignoreListAddActivity.mProgressBar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.fr, "field 'mProgressBar'", ProgressBar.class);
        ignoreListAddActivity.mListView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.d8, "field 'mListView'", RecyclerView.class);
    }

    public void unbind() {
        IgnoreListAddActivity ignoreListAddActivity = this.f5385a;
        if (ignoreListAddActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f5385a = null;
        ignoreListAddActivity.mBtnAdd = null;
        ignoreListAddActivity.mProgressBar = null;
        ignoreListAddActivity.mListView = null;
        this.f5386b.setOnClickListener(null);
        this.f5386b = null;
    }
}
