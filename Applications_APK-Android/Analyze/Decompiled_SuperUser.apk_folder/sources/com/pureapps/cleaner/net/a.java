package com.pureapps.cleaner.net;

import java.util.ArrayList;

/* compiled from: ConnectChangeListener */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f5821a;

    /* renamed from: b  reason: collision with root package name */
    private static ArrayList<C0091a> f5822b = new ArrayList<>();

    /* renamed from: com.pureapps.cleaner.net.a$a  reason: collision with other inner class name */
    /* compiled from: ConnectChangeListener */
    public interface C0091a {
        void a();
    }

    private a() {
    }

    public static a a() {
        if (f5821a == null) {
            f5821a = new a();
        }
        return f5821a;
    }

    public ArrayList<C0091a> b() {
        return f5822b;
    }
}
