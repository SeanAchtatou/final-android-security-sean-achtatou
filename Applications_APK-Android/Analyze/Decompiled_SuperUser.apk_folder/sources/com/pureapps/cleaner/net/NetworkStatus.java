package com.pureapps.cleaner.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import com.kingouser.com.application.App;
import java.util.Locale;

public class NetworkStatus {

    /* renamed from: d  reason: collision with root package name */
    private static String f5816d;

    /* renamed from: e  reason: collision with root package name */
    private static NetworkStatus f5817e;

    /* renamed from: a  reason: collision with root package name */
    private Context f5818a;

    /* renamed from: b  reason: collision with root package name */
    private ConnectivityManager f5819b = ((ConnectivityManager) this.f5818a.getSystemService("connectivity"));

    /* renamed from: c  reason: collision with root package name */
    private TelephonyManager f5820c = ((TelephonyManager) this.f5818a.getSystemService("phone"));

    public static NetworkStatus a() {
        if (f5817e == null) {
            f5817e = new NetworkStatus(App.a().getApplicationContext());
        }
        return f5817e;
    }

    public NetworkStatus(Context context) {
        this.f5818a = context.getApplicationContext();
    }

    public boolean b() {
        NetworkInfo networkInfo = this.f5819b.getNetworkInfo(1);
        if (networkInfo == null) {
            return false;
        }
        if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        return false;
    }

    public boolean a(Context context) {
        return ((WifiManager) context.getApplicationContext().getSystemService("wifi")).isWifiEnabled();
    }

    public void a(boolean z) {
        ((WifiManager) App.a().getApplicationContext().getSystemService("wifi")).setWifiEnabled(z);
    }

    public boolean c() {
        NetworkInfo activeNetworkInfo = this.f5819b.getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            return activeNetworkInfo.isConnected();
        }
        return false;
    }

    public static boolean b(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
                f5816d = "";
                return false;
            }
            if (activeNetworkInfo.getTypeName().equals("WIFI")) {
                f5816d = activeNetworkInfo.getTypeName().toLowerCase(Locale.ENGLISH);
            } else if (activeNetworkInfo.getExtraInfo() == null) {
                f5816d = "";
            } else {
                f5816d = activeNetworkInfo.getExtraInfo().toLowerCase(Locale.ENGLISH);
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }
}
