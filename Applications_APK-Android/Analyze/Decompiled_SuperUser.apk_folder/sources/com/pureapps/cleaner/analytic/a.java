package com.pureapps.cleaner.analytic;

import android.content.Context;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingoapp.uts.PlugPresenter;
import com.kingoapp.uts.UtsPresenter;
import com.kingoapp.uts.builder.UtsInfoBuilder;
import com.kingoapp.uts.model.PlugInfo;
import com.kingoapp.uts.util.DeviceIdGenerator;
import com.pureapps.cleaner.util.h;
import com.pureapps.cleaner.util.l;

/* compiled from: AnalyticsUtil */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f5572a;

    /* renamed from: b  reason: collision with root package name */
    private Context f5573b;

    private a(Context context) {
        this.f5573b = context.getApplicationContext();
    }

    public static a a(Context context) {
        if (f5572a == null) {
            f5572a = new a(context);
        }
        return f5572a;
    }

    public void a(String str, String str2) {
        PlugInfo plugInfo = new PlugInfo();
        plugInfo.setChannel(l.a(this.f5573b));
        if (str != null) {
            plugInfo.setFaildMessage(str);
        }
        plugInfo.setPlugId(str2);
        plugInfo.setpId(this.f5573b.getPackageName());
        plugInfo.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        plugInfo.setSystem(h.b(this.f5573b, this.f5573b.getPackageName()));
        PlugPresenter.newInstance().pushInfo(plugInfo);
    }

    public void a(String str) {
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("PlugStartServiceFaild");
        UtsInfoBuilder.setEvent(str);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
    }

    public void b(String str) {
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("PlugStartServiceFaild");
        UtsInfoBuilder.setEvent(str);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
    }

    public void a(FirebaseAnalytics firebaseAnalytics) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "superuser");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "AppOpen");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "AppOpen");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle);
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("AppOpen");
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
        GAnalytics.a(this.f5573b).b("superuser", "AppOpen");
    }

    public void a(FirebaseAnalytics firebaseAnalytics, String str) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "superuser");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, str);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, str);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle);
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu(str);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
        GAnalytics.a(this.f5573b).b("superuser", str);
    }

    public void b(FirebaseAnalytics firebaseAnalytics, String str) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ViewOpen");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, str);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "ViewOpen");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("ViewOpen");
        UtsInfoBuilder.setEvent(str);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
        GAnalytics.a(this.f5573b).a(str, String.valueOf(l.c(this.f5573b)));
    }

    public void c(FirebaseAnalytics firebaseAnalytics, String str) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ButtonClick");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, str);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "ButtonClick");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("ButtonClick");
        UtsInfoBuilder.setEvent(str);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
        GAnalytics.a(this.f5573b).a("superuser", "ButtonClick", str);
    }

    public void a(FirebaseAnalytics firebaseAnalytics, boolean z) {
        String str = z ? "SetNotificationToggleOn" : "SetNotificationToggleOff";
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "Setting");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, str);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Setting");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("Setting");
        UtsInfoBuilder.setEvent(str);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
        GAnalytics.a(this.f5573b).a("superuser", "Setting", str);
    }

    public void b(FirebaseAnalytics firebaseAnalytics) {
        String b2 = l.b(this.f5573b);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "Update");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, b2);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Update");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("Update");
        UtsInfoBuilder.setEvent(b2);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
        GAnalytics.a(this.f5573b).a("superuser", "Update", b2);
    }

    public void d(FirebaseAnalytics firebaseAnalytics, String str) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "Setting");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, str);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Setting");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("Setting");
        UtsInfoBuilder.setEvent(str);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
        GAnalytics.a(this.f5573b).a("superuser", "Setting", str);
    }

    public void e(FirebaseAnalytics firebaseAnalytics, String str) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "Setting");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, str);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Setting");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("Setting");
        UtsInfoBuilder.setEvent(str);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
        GAnalytics.a(this.f5573b).a("superuser", "Setting", str);
    }

    public void a(FirebaseAnalytics firebaseAnalytics, String str, String str2) {
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("ButtonClick");
        UtsInfoBuilder.setEvent(str);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
        GAnalytics.a(this.f5573b).a("superuser", str2, str);
    }

    public void b(FirebaseAnalytics firebaseAnalytics, boolean z) {
        String str;
        if (z) {
            str = "BaiduSwipe_On";
        } else {
            str = "BaiduSwipe_Off";
        }
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "Setting");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, str);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Setting");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        UtsInfoBuilder.setUserId(DeviceIdGenerator.readDeviceId(this.f5573b));
        UtsInfoBuilder.setPId(this.f5573b.getPackageName());
        UtsInfoBuilder.setMainMenu("superuser");
        UtsInfoBuilder.setChildMenu("Setting");
        UtsInfoBuilder.setEvent(str);
        UtsInfoBuilder.setVersionCode(String.valueOf(l.c(this.f5573b)));
        UtsInfoBuilder.setChannel(l.a(this.f5573b));
        UtsPresenter.newInstance().pushInfo(UtsInfoBuilder.builder());
        GAnalytics.a(this.f5573b).a("superuser", "Setting", str);
    }
}
