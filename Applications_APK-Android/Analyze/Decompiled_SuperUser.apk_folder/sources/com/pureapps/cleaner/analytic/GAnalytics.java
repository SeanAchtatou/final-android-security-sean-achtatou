package com.pureapps.cleaner.analytic;

import android.content.Context;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.kingouser.com.R;

public class GAnalytics {

    /* renamed from: a  reason: collision with root package name */
    private static Tracker f5569a;

    /* renamed from: b  reason: collision with root package name */
    private static GAnalytics f5570b;

    /* renamed from: c  reason: collision with root package name */
    private Context f5571c;

    public GAnalytics(Context context) {
        this.f5571c = context;
    }

    public static GAnalytics a(Context context) {
        if (f5570b == null) {
            f5570b = new GAnalytics(context);
        }
        return f5570b;
    }

    private synchronized Tracker a() {
        if (f5569a == null) {
            f5569a = GoogleAnalytics.getInstance(this.f5571c).newTracker((int) R.xml.f8281b);
            f5569a.enableAdvertisingIdCollection(true);
        }
        return f5569a;
    }

    public void a(String str, String str2) {
        Tracker a2 = a();
        a2.setScreenName(str);
        a2.setAppVersion(str2);
        a2.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void a(String str, String str2, String str3) {
        a().send(new HitBuilders.EventBuilder().setCategory(str).setAction(str3).setLabel(str2).build());
    }

    public void b(String str, String str2) {
        a().send(new HitBuilders.EventBuilder().setCategory(str).setAction(str2).build());
    }
}
