package com.pureapps.cleaner.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.pureapps.cleaner.service.CommonService;

public class CommonReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    public static String f5837a = "kingoroot.supersu.cleaner.ACTION_ALARM";

    public void onReceive(Context context, Intent intent) {
        try {
            String action = intent.getAction();
            Log.i("CommonReceiver", "action:" + action);
            if (f5837a.equals(action)) {
                CommonService.a(context, intent.getStringExtra("action"));
            } else if ("kingoroot.supersu.cleaner.ACTION_TEN_MINUTES_ALARM".equals(action)) {
                CommonService.a(context, action);
            } else if ("kingoroot.supersu.cleaner.ACTION_12HOUR_ALARM".equals(action)) {
                CommonService.a(context, action);
            } else if ("kingoroot.supersu.cleaner.ACTION_CPU_TEMP_CHECK".equals(action)) {
                CommonService.a(context, action);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
