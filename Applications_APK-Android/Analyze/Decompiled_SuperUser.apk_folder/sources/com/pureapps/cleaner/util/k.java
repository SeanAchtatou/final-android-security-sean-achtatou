package com.pureapps.cleaner.util;

import android.os.AsyncTask;
import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* compiled from: TaskManager */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private static k f5883a;

    /* renamed from: b  reason: collision with root package name */
    private ExecutorService f5884b = Executors.newCachedThreadPool();

    /* renamed from: c  reason: collision with root package name */
    private ExecutorService f5885c = Executors.newSingleThreadExecutor();

    /* renamed from: d  reason: collision with root package name */
    private Timer f5886d = new Timer();

    private k() {
    }

    public static k a() {
        if (f5883a == null) {
            f5883a = new k();
        }
        return f5883a;
    }

    public ExecutorService b() {
        return this.f5884b;
    }

    public void a(Runnable runnable) {
        this.f5884b.execute(runnable);
    }

    public static boolean a(AsyncTask<?, ?, ?> asyncTask) {
        if (asyncTask == null || asyncTask.getStatus() == AsyncTask.Status.FINISHED) {
            return false;
        }
        return true;
    }

    public static void a(AsyncTask<?, ?, ?> asyncTask, boolean z) {
        if (asyncTask != null && asyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            asyncTask.cancel(z);
        }
    }

    public static void a(Thread thread) {
        if (thread != null && thread != null) {
            try {
                if (!Thread.interrupted()) {
                    thread.interrupt();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
