package com.pureapps.cleaner.util;

import com.lody.virtual.helper.utils.FileUtils;
import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;

/* compiled from: MD5Util */
public class g {
    public static String a(String str) {
        if (str != null) {
            return a(str.getBytes());
        }
        return "";
    }

    public static String a(byte[] bArr) {
        try {
            byte[] digest = MessageDigest.getInstance("MD5").digest(bArr);
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                byte b3 = b2 & 255;
                if (b3 < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(b3));
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static String b(String str) {
        return a(new File(str));
    }

    public static String a(File file) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            MessageDigest instance = MessageDigest.getInstance("md5");
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                instance.update(bArr, 0, read);
            }
            for (byte b2 : instance.digest()) {
                String hexString = Integer.toHexString(b2 & 255);
                if (hexString.length() == 1) {
                    stringBuffer.append("0" + hexString);
                } else {
                    stringBuffer.append(hexString);
                }
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
