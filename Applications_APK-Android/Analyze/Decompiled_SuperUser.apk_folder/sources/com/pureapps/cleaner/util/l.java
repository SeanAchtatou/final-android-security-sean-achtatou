package com.pureapps.cleaner.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import com.kingouser.com.R;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.helper.utils.FileUtils;
import com.pureapps.cleaner.util.Const;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Pattern;
import org.json.JSONArray;

/* compiled from: Util */
public class l {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f5887a = {"/sys/devices/system/cpu/cpu0/cpufreq/cpu_temp", "/sys/devices/system/cpu/cpu0/cpufreq/FakeShmoo_cpu_temp", "/sys/class/thermal/thermal_zone0/temp", "/sys/class/i2c-adapter/i2c-4/4-004c/temperature", "/sys/devices/platform/tegra-i2c.3/i2c-4/4-004c/temperature", "/sys/devices/platform/omap/omap_temp_sensor.0/temperature", "/sys/devices/platform/tegra_tmon/temp1_input", "/sys/kernel/debug/tegra_thermal/temp_tj", "/sys/devices/platform/s5p-tmu/temperature", "/sys/class/thermal/thermal_zone1/temp", "/sys/class/hwmon/hwmon0/device/temp1_input", "/sys/devices/platform/s5p-tmu/curr_temp", "/sys/devices/virtual/thermal/thermal_zone0/temp", "/sys/devices/virtual/thermal/thermal_zone1/temp"};

    public static boolean a(long j) {
        long currentTimeMillis = (System.currentTimeMillis() / 86400000) * 86400000;
        return j >= currentTimeMillis && j < currentTimeMillis + 86400000;
    }

    public static String a(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), FileUtils.FileMode.MODE_IWUSR);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString("CHANNEL");
            }
            return "PCKingoRoot";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "PCKingoRoot";
        }
    }

    public static String b(long j) {
        if (j <= 0) {
            return "0B";
        }
        String[] strArr = {"B", "kB", "MB", "GB", "TB"};
        int log10 = (int) (Math.log10((double) j) / Math.log10(1024.0d));
        return new DecimalFormat("##.##").format(((double) j) / Math.pow(1024.0d, (double) log10)) + strArr[log10];
    }

    public static String a(Context context, long j) {
        String substring;
        String str;
        String b2 = b(j);
        if (j >= 1099511627776L) {
            substring = b2.substring(0, b2.length() - 2);
            str = "TB";
        } else if (j >= 1073741824) {
            substring = b2.substring(0, b2.length() - 2);
            str = "GB";
        } else if (j >= 1048576) {
            substring = b2.substring(0, b2.length() - 2);
            str = "MB";
        } else if (j >= 1024) {
            substring = b2.substring(0, b2.length() - 2);
            str = "KB";
        } else {
            substring = b2.substring(0, b2.length() - 1);
            str = "B";
        }
        char[] cArr = new char[substring.length()];
        for (int i = 0; i < substring.length(); i++) {
            char charAt = substring.charAt(i);
            if (charAt >= 1632 && charAt <= 1641) {
                charAt = (char) (charAt - 1584);
            } else if (charAt >= 1776 && charAt <= 1785) {
                charAt = (char) (charAt - 1728);
            }
            cArr[i] = charAt;
        }
        return new String(cArr) + str;
    }

    public static String a(String str) {
        char[] cArr = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt >= 1632 && charAt <= 1641) {
                charAt = (char) (charAt - 1584);
            } else if (charAt >= 1776 && charAt <= 1785) {
                charAt = (char) (charAt - 1728);
            }
            cArr[i] = charAt;
        }
        return new String(cArr);
    }

    public static Resources a(Context context, String str) {
        try {
            Class<?> cls = Class.forName("android.content.res.AssetManager");
            Object newInstance = cls.newInstance();
            cls.getMethod("addAssetPath", String.class).invoke(newInstance, str);
            Resources resources = context.getResources();
            return new Resources((AssetManager) newInstance, resources.getDisplayMetrics(), resources.getConfiguration());
        } catch (Throwable th) {
            return null;
        }
    }

    public static int a() {
        return Build.VERSION.SDK_INT;
    }

    public static String b(Context context) {
        try {
            return String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
        } catch (Exception e2) {
            e2.printStackTrace();
            return "1.0";
        }
    }

    public static int c(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static String b(Context context, long j) {
        if (j >= 86400000) {
            return context.getString(R.string.eg, Long.valueOf(j / 86400000));
        } else if (j >= 3600000) {
            return context.getString(R.string.eh, Long.valueOf(j / 3600000));
        } else {
            long j2 = j / 60000;
            if (j2 == 0) {
                j2 = 1;
            }
            return context.getString(R.string.ei, Long.valueOf(j2));
        }
    }

    public static DisplayMetrics d(Context context) {
        new DisplayMetrics();
        return context.getResources().getDisplayMetrics();
    }

    public static ArrayList<? extends Object> a(ArrayList<? extends Object> arrayList) {
        ArrayList<? extends Object> arrayList2 = new ArrayList<>();
        arrayList2.addAll(arrayList);
        return arrayList2;
    }

    public static ArrayList<String> a(JSONArray jSONArray) {
        ArrayList<String> arrayList = new ArrayList<>();
        if (jSONArray != null) {
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    arrayList.add(jSONArray.getString(i));
                    i++;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    public static int e(Context context) {
        int i;
        int i2;
        int i3;
        int parseInt;
        int i4 = 30;
        String[] strArr = f5887a;
        int length = strArr.length;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i5 < length) {
            String str = strArr[i5];
            if (new File(str).exists()) {
                String b2 = b(str);
                if (TextUtils.isEmpty(b2)) {
                    i = i6;
                    i2 = i7;
                    i3 = i4;
                } else {
                    if (b2.length() == 5) {
                        b2 = b2.substring(0, 2);
                    }
                    if (!c(b2) || (parseInt = Integer.parseInt(b2)) <= 0 || parseInt >= 100) {
                        i = i6;
                        i2 = i7;
                    } else {
                        i = i6 + parseInt;
                        i2 = i7 + 1;
                    }
                    i3 = i2 > 0 ? i / i2 : i4;
                }
            } else {
                i = i6;
                i2 = i7;
                i3 = i4;
            }
            i5++;
            i4 = i3;
            i7 = i2;
            i6 = i;
        }
        if (i.a(context).t() == 1) {
            return (int) ((((double) i4) * 1.8d) + 32.0d);
        }
        return i4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x003a A[SYNTHETIC, Splitter:B:24:0x003a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(java.lang.String r4) {
        /*
            r0 = 0
            boolean r1 = android.text.TextUtils.isEmpty(r4)
            if (r1 != 0) goto L_0x001f
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0025, all -> 0x0035 }
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ Exception -> 0x0025, all -> 0x0035 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0025, all -> 0x0035 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0025, all -> 0x0035 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0025, all -> 0x0035 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0025, all -> 0x0035 }
            java.lang.String r0 = r2.readLine()     // Catch:{ Exception -> 0x0045 }
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ IOException -> 0x0020 }
        L_0x001f:
            return r0
        L_0x0020:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001f
        L_0x0025:
            r1 = move-exception
            r2 = r0
        L_0x0027:
            r1.printStackTrace()     // Catch:{ all -> 0x0043 }
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x001f
        L_0x0030:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001f
        L_0x0035:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0038:
            if (r2 == 0) goto L_0x003d
            r2.close()     // Catch:{ IOException -> 0x003e }
        L_0x003d:
            throw r0
        L_0x003e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003d
        L_0x0043:
            r0 = move-exception
            goto L_0x0038
        L_0x0045:
            r1 = move-exception
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pureapps.cleaner.util.l.b(java.lang.String):java.lang.String");
    }

    public static boolean c(String str) {
        try {
            if (TextUtils.isEmpty(str) || !Pattern.compile("[0-9]*").matcher(str).matches()) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            Log.w("Util", "isNumeric", e2);
            return false;
        }
    }

    public static boolean b() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }

    public static long f(Context context) {
        if (a() >= 21) {
            if (Environment.isExternalStorageRemovable()) {
                return e(Environment.getExternalStorageDirectory().getAbsolutePath());
            }
        } else if (b()) {
            String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            if (!"/storage/emulated/0".equals(absolutePath)) {
                return e(absolutePath);
            }
        }
        return 0;
    }

    public static long g(Context context) {
        if (a() >= 21) {
            if (Environment.isExternalStorageRemovable()) {
                return d(Environment.getExternalStorageDirectory().getAbsolutePath());
            }
        } else if (b()) {
            String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            if (!"/storage/emulated/0".equals(absolutePath)) {
                return d(absolutePath);
            }
        }
        return 0;
    }

    public static long c() {
        return d(Environment.getDataDirectory().getPath());
    }

    public static long d() {
        return e(Environment.getDataDirectory().getPath());
    }

    public static long d(String str) {
        try {
            StatFs statFs = new StatFs(str);
            if (Build.VERSION.SDK_INT >= 18) {
                return statFs.getAvailableBytes();
            }
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static long e(String str) {
        try {
            StatFs statFs = new StatFs(str);
            if (Build.VERSION.SDK_INT >= 18) {
                return statFs.getTotalBytes();
            }
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static long h(Context context) {
        try {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            ((ActivityManager) context.getSystemService(ServiceManagerNative.ACTIVITY)).getMemoryInfo(memoryInfo);
            return memoryInfo.availMem;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static long i(Context context) {
        long j = 0;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("/proc/meminfo"), 8192);
            String readLine = bufferedReader.readLine();
            String[] split = readLine.split("\\s+");
            int length = split.length;
            for (int i = 0; i < length; i++) {
                Log.i(readLine, split[i] + "\t");
            }
            j = Long.valueOf(split[1]).longValue() * 1024;
            bufferedReader.close();
        } catch (IOException e2) {
        }
        System.out.println("总运存--->>>" + j);
        return j;
    }

    public static Const.CpuTempStyle a(Context context, int i) {
        if (i.a(context).t() == 0) {
            if (i < 40) {
                return Const.CpuTempStyle.FINE;
            }
            if (i < 60) {
                return Const.CpuTempStyle.HIGH;
            }
            return Const.CpuTempStyle.OVERHEAT;
        } else if (i < 104) {
            return Const.CpuTempStyle.FINE;
        } else {
            if (i < 140) {
                return Const.CpuTempStyle.HIGH;
            }
            return Const.CpuTempStyle.OVERHEAT;
        }
    }
}
