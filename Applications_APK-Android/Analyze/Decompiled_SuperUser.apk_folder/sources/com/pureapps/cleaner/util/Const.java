package com.pureapps.cleaner.util;

public class Const {

    /* renamed from: a  reason: collision with root package name */
    public static String[] f5856a = {"ERROR_CODE_INTERNAL_ERROR", "ERROR_CODE_INVALID_REQUEST", "ERROR_CODE_NETWORK_ERROR", "ERROR_CODE_NO_FILL"};

    public enum CpuTempStyle {
        FINE,
        HIGH,
        OVERHEAT
    }
}
