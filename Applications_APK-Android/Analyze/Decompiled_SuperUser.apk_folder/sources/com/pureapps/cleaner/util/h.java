package com.pureapps.cleaner.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import com.kingouser.com.application.App;
import com.lody.virtual.helper.utils.FileUtils;
import io.fabric.sdk.android.services.common.a;
import java.io.File;
import java.util.List;

/* compiled from: PackageUtil */
public class h {
    public static String a(Context context) {
        String str;
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent("android.settings.SETTINGS"), 65536);
        if (queryIntentActivities == null || queryIntentActivities.size() <= 0 || queryIntentActivities.get(0).activityInfo == null || queryIntentActivities.get(0).activityInfo.packageName.length() <= 0) {
            str = "";
        } else {
            str = queryIntentActivities.get(0).activityInfo.packageName;
        }
        if (str == null || str.length() == 0) {
            return "com.android.settings";
        }
        return str;
    }

    public static String b(Context context) {
        String str;
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent("android.intent.action.CALL", Uri.parse("tel:18410071869")), 65536);
        if (queryIntentActivities == null || queryIntentActivities.size() <= 0 || queryIntentActivities.get(0).activityInfo == null || queryIntentActivities.get(0).activityInfo.packageName.length() <= 0) {
            str = "";
        } else {
            str = queryIntentActivities.get(0).activityInfo.packageName;
        }
        if (str == null || str.length() == 0) {
            return a.ANDROID_CLIENT_TYPE;
        }
        return str;
    }

    public static String c(Context context) {
        String str;
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent("android.intent.action.SENDTO", Uri.parse("mailto:363541180@qq.com")), 65536);
        if (queryIntentActivities == null || queryIntentActivities.size() <= 0 || queryIntentActivities.get(0).activityInfo == null || queryIntentActivities.get(0).activityInfo.packageName.length() <= 0) {
            str = "";
        } else {
            str = queryIntentActivities.get(0).activityInfo.packageName;
        }
        if (str == null || str.length() == 0) {
            return a.ANDROID_CLIENT_TYPE;
        }
        return str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0084 A[EDGE_INSN: B:27:0x0084->B:22:0x0084 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String d(android.content.Context r7) {
        /*
            r6 = 0
            java.lang.String r1 = ""
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 19
            if (r0 < r2) goto L_0x0082
            java.lang.String r0 = "android.provider.Telephony$Sms"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x007e }
            java.lang.String r2 = "getDefaultSmsPackage"
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x007e }
            r4 = 0
            java.lang.Class<android.content.Context> r5 = android.content.Context.class
            r3[r4] = r5     // Catch:{ Exception -> 0x007e }
            java.lang.reflect.Method r0 = r0.getMethod(r2, r3)     // Catch:{ Exception -> 0x007e }
            r2 = 0
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x007e }
            r4 = 0
            r3[r4] = r7     // Catch:{ Exception -> 0x007e }
            java.lang.Object r0 = r0.invoke(r2, r3)     // Catch:{ Exception -> 0x007e }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x007e }
        L_0x002a:
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.SENDTO"
            java.lang.String r3 = "smsto"
            java.lang.String r4 = ""
            android.net.Uri r3 = android.net.Uri.fromParts(r3, r4, r6)
            r1.<init>(r2, r3)
            android.content.pm.PackageManager r2 = r7.getPackageManager()
            r3 = 65536(0x10000, float:9.18355E-41)
            java.util.List r1 = r2.queryIntentActivities(r1, r3)
            if (r1 == 0) goto L_0x0085
            int r3 = r1.size()
            if (r3 <= 0) goto L_0x0085
            java.util.Iterator r3 = r1.iterator()
            r1 = r0
        L_0x0050:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0084
            java.lang.Object r0 = r3.next()
            android.content.pm.ResolveInfo r0 = (android.content.pm.ResolveInfo) r0
            if (r0 == 0) goto L_0x0090
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            android.content.pm.ActivityInfo r6 = r0.activityInfo
            java.lang.String r6 = r6.packageName
            r2.getPreferredActivities(r5, r4, r6)
            int r4 = r4.size()
            if (r4 <= 0) goto L_0x0090
            if (r1 != 0) goto L_0x0090
            android.content.pm.ActivityInfo r0 = r0.activityInfo
            java.lang.String r1 = r0.packageName
            r0 = r1
        L_0x007c:
            r1 = r0
            goto L_0x0050
        L_0x007e:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0082:
            r0 = r1
            goto L_0x002a
        L_0x0084:
            r0 = r1
        L_0x0085:
            if (r0 == 0) goto L_0x008d
            int r1 = r0.length()
            if (r1 != 0) goto L_0x008f
        L_0x008d:
            java.lang.String r0 = "android"
        L_0x008f:
            return r0
        L_0x0090:
            r0 = r1
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pureapps.cleaner.util.h.d(android.content.Context):java.lang.String");
    }

    public static String a(PackageManager packageManager) {
        String str = "";
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
        if (resolveActivity == null) {
            return "null";
        }
        if (resolveActivity.activityInfo != null && !resolveActivity.activityInfo.packageName.equals(a.ANDROID_CLIENT_TYPE)) {
            str = resolveActivity.activityInfo.packageName;
        }
        if (str == null || str.length() == 0) {
            return a.ANDROID_CLIENT_TYPE;
        }
        return str;
    }

    public static String a() {
        String string = Settings.Secure.getString(App.a().getContentResolver(), "default_input_method");
        if (string != null) {
            try {
                return ComponentName.unflattenFromString(string).getPackageName();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return a.ANDROID_CLIENT_TYPE;
    }

    public static boolean a(Context context, String str) {
        PackageInfo packageInfo;
        try {
            if (!TextUtils.isEmpty(str) && (packageInfo = context.getPackageManager().getPackageInfo(str, 0)) != null && packageInfo.applicationInfo.enabled) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException | Exception e2) {
            return false;
        }
    }

    public static boolean b(Context context, String str) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 0);
            if ((packageInfo.applicationInfo.flags & 1) > 0 || (packageInfo.applicationInfo.flags & FileUtils.FileMode.MODE_IWUSR) > 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public static void c(Context context, String str) {
        try {
            File file = new File(str);
            if (Build.VERSION.SDK_INT > 23) {
                Uri a2 = FileProvider.a(context, "com.kingouser.com.myprovider", file);
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.addFlags(268435456);
                intent.addFlags(1);
                intent.setDataAndType(a2, "application/vnd.android.package-archive");
                context.startActivity(intent);
                return;
            }
            Intent intent2 = new Intent();
            intent2.setFlags(268435456);
            intent2.setAction("android.intent.action.VIEW");
            intent2.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            context.startActivity(intent2);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
