package com.pureapps.cleaner.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kingouser.com.entity.PlugEntity;
import io.fabric.sdk.android.services.b.b;
import java.util.HashMap;
import java.util.Map;

/* compiled from: SettingInfo */
public class i {

    /* renamed from: b  reason: collision with root package name */
    private static i f5872b;

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f5873a = this.f5874c.getSharedPreferences("com.best.clean.phone.boost.util.prefs", 0);

    /* renamed from: c  reason: collision with root package name */
    private Context f5874c;

    private i(Context context) {
        this.f5874c = context.getApplicationContext();
    }

    public static i a(Context context) {
        if (f5872b == null) {
            f5872b = new i(context);
        }
        return f5872b;
    }

    public HashMap<Integer, PlugEntity> a() {
        HashMap<Integer, PlugEntity> hashMap = (HashMap) new Gson().fromJson(b("plug_config", ""), new TypeToken<Map<Integer, PlugEntity>>() {
        }.getType());
        if (hashMap == null) {
            return new HashMap<>();
        }
        return hashMap;
    }

    public void a(PlugEntity plugEntity) {
        Gson gson = new Gson();
        HashMap<Integer, PlugEntity> a2 = a();
        a2.put(Integer.valueOf(plugEntity.operation), plugEntity);
        a("plug_config", gson.toJson(a2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.b(java.lang.String, int):int
      com.pureapps.cleaner.util.i.b(java.lang.String, java.lang.String):java.lang.String
      com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean
      com.pureapps.cleaner.util.i.b(java.lang.String, long):long */
    public long b() {
        return b("check_cloud_plug_time", 0L);
    }

    public void a(long j) {
        a("check_cloud_plug_time", j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.b(java.lang.String, int):int
      com.pureapps.cleaner.util.i.b(java.lang.String, long):long
      com.pureapps.cleaner.util.i.b(java.lang.String, java.lang.String):java.lang.String
      com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean */
    public boolean c() {
        return b("weather_push", true);
    }

    public void a(boolean z) {
        a("weather_push", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.b(java.lang.String, int):int
      com.pureapps.cleaner.util.i.b(java.lang.String, java.lang.String):java.lang.String
      com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean
      com.pureapps.cleaner.util.i.b(java.lang.String, long):long */
    public long d() {
        return b("show_swipe_time", 0L);
    }

    public void e() {
        a("show_swipe_time", System.currentTimeMillis());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.b(java.lang.String, int):int
      com.pureapps.cleaner.util.i.b(java.lang.String, long):long
      com.pureapps.cleaner.util.i.b(java.lang.String, java.lang.String):java.lang.String
      com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean */
    public boolean f() {
        return b("vip_saver", false);
    }

    public void b(boolean z) {
        a("vip_saver", z);
    }

    public int g() {
        return b("last_versioncode", 0);
    }

    public void a(int i) {
        a("last_versioncode", i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.b(java.lang.String, int):int
      com.pureapps.cleaner.util.i.b(java.lang.String, long):long
      com.pureapps.cleaner.util.i.b(java.lang.String, java.lang.String):java.lang.String
      com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean */
    public boolean h() {
        return b("swipe_enable", false);
    }

    public void c(boolean z) {
        a("swipe_enable", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.b(java.lang.String, int):int
      com.pureapps.cleaner.util.i.b(java.lang.String, long):long
      com.pureapps.cleaner.util.i.b(java.lang.String, java.lang.String):java.lang.String
      com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean */
    public boolean i() {
        return b("notification_delete_guide", true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.a(long, boolean):void
      com.pureapps.cleaner.util.i.a(java.lang.String, int):void
      com.pureapps.cleaner.util.i.a(java.lang.String, long):void
      com.pureapps.cleaner.util.i.a(java.lang.String, java.lang.String):void
      com.pureapps.cleaner.util.i.a(java.lang.String, boolean):void */
    public void j() {
        a("notification_delete_guide", false);
    }

    public int k() {
        return b("notification_theme", 1);
    }

    public void b(int i) {
        a("notification_theme", i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.b(java.lang.String, int):int
      com.pureapps.cleaner.util.i.b(java.lang.String, long):long
      com.pureapps.cleaner.util.i.b(java.lang.String, java.lang.String):java.lang.String
      com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean */
    public boolean l() {
        return b("quiet_notifications_switch", true);
    }

    public void d(boolean z) {
        a("quiet_notifications_switch", z);
    }

    public String m() {
        return b("quiet_notifications_back_list", "");
    }

    public void a(String str) {
        a("quiet_notifications_back_list", str);
    }

    public void n() {
        a("memory_notification_click_time", System.currentTimeMillis());
    }

    public String o() {
        return b("check_json", "");
    }

    public void b(String str) {
        a("check_json", str);
    }

    public int p() {
        return b("check_update_versioncode", 0);
    }

    public void c(int i) {
        a("check_update_versioncode", i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.b(java.lang.String, int):int
      com.pureapps.cleaner.util.i.b(java.lang.String, java.lang.String):java.lang.String
      com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean
      com.pureapps.cleaner.util.i.b(java.lang.String, long):long */
    public long q() {
        return b("junk_scan_time", 0L);
    }

    public void b(long j) {
        a("junk_scan_time", j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.b(java.lang.String, int):int
      com.pureapps.cleaner.util.i.b(java.lang.String, long):long
      com.pureapps.cleaner.util.i.b(java.lang.String, java.lang.String):java.lang.String
      com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean */
    public boolean r() {
        return b("notification_tool_eanble", true);
    }

    public void e(boolean z) {
        a("notification_tool_eanble", z);
    }

    public long s() {
        String b2 = b("boost_saved", "0_0");
        if (!TextUtils.isEmpty(b2)) {
            String[] split = b2.split(b.ROLL_OVER_FILE_NAME_SEPARATOR);
            if (split.length == 2) {
                long parseLong = Long.parseLong(split[0]);
                long parseLong2 = Long.parseLong(split[1]);
                if (System.currentTimeMillis() - parseLong < 86400000) {
                    return parseLong2;
                }
            }
        }
        return 0;
    }

    public void a(long j, boolean z) {
        if (z) {
            a("boost_saved", System.currentTimeMillis() + b.ROLL_OVER_FILE_NAME_SEPARATOR + (s() + j));
            return;
        }
        a("boost_saved", System.currentTimeMillis() + b.ROLL_OVER_FILE_NAME_SEPARATOR + j);
    }

    public int t() {
        return b("cpu_temp_type", 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.pureapps.cleaner.util.i.b(java.lang.String, int):int
      com.pureapps.cleaner.util.i.b(java.lang.String, long):long
      com.pureapps.cleaner.util.i.b(java.lang.String, java.lang.String):java.lang.String
      com.pureapps.cleaner.util.i.b(java.lang.String, boolean):boolean */
    public boolean u() {
        return b("grant_root_permission", true);
    }

    public void a(String str, int i) {
        SharedPreferences.Editor edit = this.f5873a.edit();
        edit.putInt(str, i);
        edit.commit();
    }

    public int b(String str, int i) {
        return this.f5873a.getInt(str, i);
    }

    public void a(String str, long j) {
        SharedPreferences.Editor edit = this.f5873a.edit();
        edit.putLong(str, j);
        edit.commit();
    }

    public long b(String str, long j) {
        return this.f5873a.getLong(str, j);
    }

    public void a(String str, String str2) {
        SharedPreferences.Editor edit = this.f5873a.edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public String b(String str, String str2) {
        return this.f5873a.getString(str, str2);
    }

    public void a(String str, boolean z) {
        SharedPreferences.Editor edit = this.f5873a.edit();
        edit.putBoolean(str, z);
        edit.commit();
    }

    public boolean b(String str, boolean z) {
        return this.f5873a.getBoolean(str, z);
    }
}
