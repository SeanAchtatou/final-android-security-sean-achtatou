package com.pureapps.cleaner.util;

import android.text.TextUtils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/* compiled from: EncryptUtil */
public class d {
    public static ArrayList<String> a(String[] strArr) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (String a2 : strArr) {
            arrayList.add(a(a2));
        }
        return arrayList;
    }

    public static String b(String[] strArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < strArr.length; i++) {
            String a2 = a(strArr[i]);
            if (i > 0) {
                stringBuffer.append(",");
            }
            stringBuffer.append(a2);
        }
        return stringBuffer.toString();
    }

    public static String c(String[] strArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < strArr.length; i++) {
            String a2 = a(strArr[i]);
            if (i > 0) {
                stringBuffer.append("+");
            }
            stringBuffer.append(a2);
        }
        return stringBuffer.toString();
    }

    public static String a(ArrayList<String> arrayList) {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= arrayList.size()) {
                return stringBuffer.toString();
            }
            if (i2 > 0) {
                stringBuffer.append(",");
            }
            stringBuffer.append(arrayList.get(i2));
            i = i2 + 1;
        }
    }

    public static String b(ArrayList<String> arrayList) {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= arrayList.size()) {
                return stringBuffer.toString();
            }
            if (i2 > 0) {
                stringBuffer.append("+");
            }
            stringBuffer.append(arrayList.get(i2));
            i = i2 + 1;
        }
    }

    public static String a(String str) {
        if (!TextUtils.isEmpty(str)) {
            return g.a(str.toLowerCase() + "booster");
        }
        return null;
    }

    public static long b(String str) {
        byte[] a2 = a((str + "booster").getBytes());
        if (a2 != null) {
            return b(a2);
        }
        return 0;
    }

    private static byte[] a(byte[] bArr) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            messageDigest = null;
        }
        if (messageDigest == null) {
            return null;
        }
        messageDigest.update(bArr);
        return messageDigest.digest();
    }

    private static long b(byte[] bArr) {
        long j = 0;
        if (bArr != null && bArr.length == 16) {
            int i = 0;
            while (i < 8) {
                long j2 = ((long) (bArr[i] & 255)) | (j << 8);
                i++;
                j = j2;
            }
        }
        return j;
    }
}
