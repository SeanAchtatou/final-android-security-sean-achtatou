package com.pureapps.cleaner.util;

import android.os.Build;
import android.text.TextUtils;
import java.lang.reflect.Method;

/* compiled from: Device */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public static Class<?> f5865a;

    /* renamed from: b  reason: collision with root package name */
    public static Method f5866b;

    /* renamed from: c  reason: collision with root package name */
    private static boolean f5867c;

    /* renamed from: d  reason: collision with root package name */
    private static boolean f5868d;

    /* renamed from: e  reason: collision with root package name */
    private static boolean f5869e;

    static {
        boolean z = true;
        f5865a = null;
        f5866b = null;
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            f5865a = cls;
            Method declaredMethod = cls.getDeclaredMethod("get", String.class, String.class);
            f5866b = declaredMethod;
            declaredMethod.setAccessible(true);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (TextUtils.isEmpty(a("ro.build.version.emui", null))) {
            f5867c = false;
        } else {
            f5867c = true;
        }
        if (TextUtils.isEmpty(a("ro.miui.ui.version.name", null))) {
            f5868d = false;
        } else {
            f5868d = true;
        }
        String a2 = a("ro.build.display.id", null);
        if (TextUtils.isEmpty(a2) || !a2.toLowerCase().contains("flyme")) {
            z = false;
        }
        f5869e = z;
    }

    public static String a(String str, String str2) {
        if (f5866b == null) {
            return str2;
        }
        try {
            String str3 = (String) f5866b.invoke(null, str, null);
            if (str3 != null) {
                return str3;
            }
            return str2;
        } catch (Exception e2) {
            return str2;
        }
    }

    public static final boolean a() {
        return f5867c;
    }

    public static final boolean b() {
        return f5869e;
    }

    public static int a(boolean z) {
        if (Build.VERSION.SDK_INT < 19 || Build.VERSION.SDK_INT >= 25) {
            return 2003;
        }
        if (z || a() || b() || a() || Build.VERSION.SDK_INT >= 19) {
            return 2005;
        }
        return 2003;
    }
}
