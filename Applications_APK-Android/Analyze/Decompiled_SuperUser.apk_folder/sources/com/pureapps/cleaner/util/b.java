package com.pureapps.cleaner.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

/* compiled from: BitmapUtil */
public class b {
    public static Bitmap a(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }
        Bitmap createBitmap = (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) ? Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888) : Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return createBitmap;
    }

    public static Bitmap b(Drawable drawable) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_4444 : Bitmap.Config.ARGB_4444);
            Canvas canvas = new Canvas(createBitmap);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            drawable.draw(canvas);
            return createBitmap;
        } catch (OutOfMemoryError e2) {
            Log.w("BitmapUtil", "drawableToBitmap", e2);
            return null;
        } catch (Exception e3) {
            Log.w("BitmapUtil", "drawableToBitmap", e3);
            return null;
        }
    }

    public static Bitmap a(Drawable drawable, int i, int i2) {
        if (i <= 0 || i2 <= 0) {
            try {
                i = drawable.getIntrinsicWidth();
                i2 = drawable.getIntrinsicHeight();
            } catch (OutOfMemoryError e2) {
                Log.w("BitmapUtil", "drawableToBitmap", e2);
                return null;
            } catch (Exception e3) {
                Log.w("BitmapUtil", "drawableToBitmap", e3);
                return null;
            }
        }
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        canvas.scale(((float) i) / ((float) drawable.getIntrinsicWidth()), ((float) i2) / ((float) drawable.getIntrinsicHeight()));
        drawable.draw(canvas);
        return createBitmap;
    }
}
