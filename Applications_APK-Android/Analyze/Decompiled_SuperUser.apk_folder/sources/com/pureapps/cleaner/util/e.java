package com.pureapps.cleaner.util;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import com.kingouser.com.util.ShellUtils;
import java.io.File;

/* compiled from: FileUtil */
public class e {
    public static String a(Context context) {
        File externalCacheDir = context.getExternalCacheDir();
        if (externalCacheDir == null) {
            externalCacheDir = new File(Environment.getExternalStorageDirectory().toString() + "/Android/data/" + context.getPackageName() + "/cache");
            externalCacheDir.mkdirs();
        }
        return externalCacheDir.getAbsolutePath();
    }

    public static boolean a(String str) {
        if (!TextUtils.isEmpty(str)) {
            return a(new File(str));
        }
        return false;
    }

    public static void b(String str) {
        j.a("rm -r " + str, true, true);
    }

    public static boolean a(File file) {
        if (file != null) {
            try {
                if (file.exists()) {
                    File file2 = new File(file.getAbsolutePath() + System.currentTimeMillis());
                    file.renameTo(file2);
                    return file2.delete();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }

    public static long b(File file) {
        long j = 0;
        if (file == null || !file.exists()) {
            System.out.println("文件或者文件夹不存在，请检查路径是否正确！");
            return 0;
        } else if (!file.isDirectory()) {
            return file.length();
        } else {
            File[] listFiles = file.listFiles();
            int length = listFiles.length;
            int i = 0;
            while (i < length) {
                long b2 = b(listFiles[i]) + j;
                i++;
                j = b2;
            }
            return j;
        }
    }

    public static long c(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return 0;
            }
            File file = new File(str);
            if (file == null || !file.exists()) {
                System.out.println("文件或者文件夹不存在，请检查路径是否正确！");
                return 0;
            } else if (!file.isDirectory()) {
                return file.length();
            } else {
                File[] listFiles = file.listFiles();
                int length = listFiles.length;
                int i = 0;
                long j = 0;
                while (i < length) {
                    long b2 = b(listFiles[i]) + j;
                    i++;
                    j = b2;
                }
                return j;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static long d(String str) {
        long j = 0;
        try {
            String str2 = j.a("ls -l " + str, true, false).f5881b;
            if (!TextUtils.isEmpty(str2)) {
                for (String split : str2.split(ShellUtils.COMMAND_LINE_END)) {
                    String[] split2 = split.split("\\s+");
                    if (split2 != null) {
                        try {
                            if (split2.length == 7) {
                                j += Long.parseLong(split2[3]);
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return j;
    }
}
