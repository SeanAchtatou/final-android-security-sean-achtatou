package com.pureapps.cleaner.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: AnimatorUtil */
public class a {
    public static void a(final View view, int i, int i2, final boolean z, long j) {
        ValueAnimator ofInt = ValueAnimator.ofInt(i, i2);
        final ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                layoutParams.height = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                view.setLayoutParams(layoutParams);
                view.requestLayout();
            }
        });
        ofInt.addListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animator) {
                if (z) {
                    view.setVisibility(0);
                }
                super.onAnimationStart(animator);
            }

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                if (!z) {
                    view.setVisibility(8);
                }
            }
        });
        ofInt.setDuration(j);
        ofInt.start();
    }

    public static void a(View view, boolean z, long j, int i) {
        if (z) {
            a(view, 0, i, z, j);
            return;
        }
        a(view, view.getLayoutParams().height, 0, z, j);
    }
}
