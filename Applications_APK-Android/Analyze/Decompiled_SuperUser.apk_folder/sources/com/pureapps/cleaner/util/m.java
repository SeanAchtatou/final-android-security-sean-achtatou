package com.pureapps.cleaner.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

/* compiled from: ViewUtil */
public final class m {

    /* renamed from: a  reason: collision with root package name */
    public static final boolean f5888a = (Build.VERSION.SDK_INT >= 12);

    public static Bitmap a(View view, int i, int i2, float f2, float f3, int i3, int i4) {
        if (i3 <= 0) {
            throw new IllegalArgumentException("downSampleFactor must be greater than 0.");
        } else if (i <= 0 || i2 <= 0) {
            return null;
        } else {
            Bitmap createBitmap = Bitmap.createBitmap((int) ((((float) i) - f2) / ((float) i3)), (int) ((((float) i2) - f3) / ((float) i3)), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            canvas.translate((-f2) / ((float) i3), (-f3) / ((float) i3));
            canvas.scale(1.0f / ((float) i3), 1.0f / ((float) i3));
            view.draw(canvas);
            if (i4 == 0) {
                return createBitmap;
            }
            Paint paint = new Paint();
            paint.setFlags(1);
            paint.setColor(i4);
            canvas.drawPaint(paint);
            return createBitmap;
        }
    }

    @TargetApi(12)
    public static void a(View view, float f2, float f3, int i, final Runnable runnable) {
        if (f5888a) {
            ViewPropertyAnimator duration = view.animate().alpha(f3).setDuration((long) i);
            if (runnable != null) {
                duration.setListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animator) {
                        runnable.run();
                    }
                });
                return;
            }
            return;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f3);
        alphaAnimation.setDuration((long) i);
        alphaAnimation.setFillAfter(true);
        if (runnable != null) {
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    new Handler(Looper.getMainLooper()).post(runnable);
                }

                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }
            });
        }
        view.startAnimation(alphaAnimation);
    }
}
