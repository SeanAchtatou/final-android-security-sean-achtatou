package com.pureapps.cleaner.util;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: ShellUtils */
public class j {
    public static a a(String str, boolean z, boolean z2) {
        return a(new String[]{str}, z, true, z2);
    }

    public static a a(final String[] strArr, final boolean z, final boolean z2, final boolean z3) {
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(1);
        try {
            a aVar = (a) newFixedThreadPool.submit(new Callable<a>() {
                /* JADX WARNING: Code restructure failed: missing block: B:100:0x0145, code lost:
                    r6.close();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:102:0x014a, code lost:
                    r3.close();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:104:0x014f, code lost:
                    r7.destroy();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:106:0x0153, code lost:
                    r1 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:107:0x0154, code lost:
                    r1.printStackTrace();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:112:0x0168, code lost:
                    r0 = th;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:113:0x0169, code lost:
                    r6 = null;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:121:0x017f, code lost:
                    r0 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:122:0x0180, code lost:
                    r5 = r4;
                    r1 = null;
                    r6 = null;
                    r8 = r7;
                    r7 = null;
                    r4 = r0;
                    r0 = null;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:123:0x0188, code lost:
                    r0 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:124:0x0189, code lost:
                    r5 = r4;
                    r1 = null;
                    r6 = null;
                    r2 = r8;
                    r8 = r7;
                    r4 = r0;
                    r0 = null;
                    r7 = null;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:125:0x0192, code lost:
                    r0 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:126:0x0193, code lost:
                    r5 = r4;
                    r1 = r2;
                    r6 = null;
                    r4 = r0;
                    r2 = r8;
                    r8 = r7;
                    r0 = null;
                    r7 = null;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:137:0x01d2, code lost:
                    r0 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:138:0x01d3, code lost:
                    r5 = r4;
                    r1 = null;
                    r6 = null;
                    r2 = r8;
                    r8 = r7;
                    r4 = r0;
                    r0 = null;
                    r7 = null;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:139:0x01dd, code lost:
                    r0 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:140:0x01de, code lost:
                    r5 = r4;
                    r1 = r2;
                    r6 = null;
                    r4 = r0;
                    r2 = r8;
                    r8 = r7;
                    r0 = null;
                    r7 = null;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:22:0x0064, code lost:
                    r0 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:23:0x0065, code lost:
                    r5 = r4;
                    r1 = null;
                    r6 = null;
                    r8 = r7;
                    r7 = null;
                    r4 = r0;
                    r0 = null;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
                    r5.close();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:87:0x0126, code lost:
                    r7.close();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:89:0x012b, code lost:
                    r6.close();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:91:0x0130, code lost:
                    r8.destroy();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:92:0x0135, code lost:
                    r4 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:93:0x0136, code lost:
                    r4.printStackTrace();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
                    r4.close();
                 */
                /* JADX WARNING: Failed to process nested try/catch */
                /* JADX WARNING: Removed duplicated region for block: B:100:0x0145 A[Catch:{ IOException -> 0x0153 }] */
                /* JADX WARNING: Removed duplicated region for block: B:102:0x014a A[Catch:{ IOException -> 0x0153 }] */
                /* JADX WARNING: Removed duplicated region for block: B:104:0x014f  */
                /* JADX WARNING: Removed duplicated region for block: B:108:0x0158  */
                /* JADX WARNING: Removed duplicated region for block: B:109:0x015e  */
                /* JADX WARNING: Removed duplicated region for block: B:112:0x0168 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:13:0x002e] */
                /* JADX WARNING: Removed duplicated region for block: B:27:0x0071 A[SYNTHETIC, Splitter:B:27:0x0071] */
                /* JADX WARNING: Removed duplicated region for block: B:30:0x0076 A[Catch:{ IOException -> 0x010e }] */
                /* JADX WARNING: Removed duplicated region for block: B:32:0x007b A[Catch:{ IOException -> 0x010e }] */
                /* JADX WARNING: Removed duplicated region for block: B:34:0x0080  */
                /* JADX WARNING: Removed duplicated region for block: B:37:0x0087  */
                /* JADX WARNING: Removed duplicated region for block: B:39:0x008a  */
                /* JADX WARNING: Removed duplicated region for block: B:84:0x0121 A[SYNTHETIC, Splitter:B:84:0x0121] */
                /* JADX WARNING: Removed duplicated region for block: B:87:0x0126 A[Catch:{ IOException -> 0x0135 }] */
                /* JADX WARNING: Removed duplicated region for block: B:89:0x012b A[Catch:{ IOException -> 0x0135 }] */
                /* JADX WARNING: Removed duplicated region for block: B:91:0x0130  */
                /* JADX WARNING: Removed duplicated region for block: B:97:0x0140 A[SYNTHETIC, Splitter:B:97:0x0140] */
                /* JADX WARNING: Unknown top exception splitter block from list: {B:24:0x006c=Splitter:B:24:0x006c, B:81:0x011c=Splitter:B:81:0x011c} */
                /* renamed from: a */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public com.pureapps.cleaner.util.j.a call() {
                    /*
                        r12 = this;
                        r0 = 0
                        r3 = 0
                        r2 = -1
                        java.lang.String[] r1 = r5
                        if (r1 == 0) goto L_0x000c
                        java.lang.String[] r1 = r5
                        int r1 = r1.length
                        if (r1 != 0) goto L_0x0012
                    L_0x000c:
                        com.pureapps.cleaner.util.j$a r0 = new com.pureapps.cleaner.util.j$a
                        r0.<init>(r2, r3, r3)
                    L_0x0011:
                        return r0
                    L_0x0012:
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder
                        r1.<init>()
                        java.lang.Runtime r4 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x01be, Exception -> 0x0114, all -> 0x013a }
                        boolean r1 = r6     // Catch:{ IOException -> 0x01be, Exception -> 0x0114, all -> 0x013a }
                        if (r1 == 0) goto L_0x003b
                        java.lang.String r1 = "su"
                    L_0x0021:
                        java.lang.Process r7 = r4.exec(r1)     // Catch:{ IOException -> 0x01be, Exception -> 0x0114, all -> 0x013a }
                        java.io.DataOutputStream r4 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x01c8, Exception -> 0x0176, all -> 0x0164 }
                        java.io.OutputStream r1 = r7.getOutputStream()     // Catch:{ IOException -> 0x01c8, Exception -> 0x0176, all -> 0x0164 }
                        r4.<init>(r1)     // Catch:{ IOException -> 0x01c8, Exception -> 0x0176, all -> 0x0164 }
                        java.lang.String[] r5 = r5     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        int r6 = r5.length     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        r1 = r0
                    L_0x0032:
                        if (r1 >= r6) goto L_0x0090
                        r8 = r5[r1]     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        if (r8 != 0) goto L_0x003e
                    L_0x0038:
                        int r1 = r1 + 1
                        goto L_0x0032
                    L_0x003b:
                        java.lang.String r1 = "sh"
                        goto L_0x0021
                    L_0x003e:
                        java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        r9.<init>()     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        java.lang.String r10 = "fkkkkkkpure:"
                        java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        java.lang.StringBuilder r9 = r9.append(r8)     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        java.lang.String r9 = r9.toString()     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        com.pureapps.cleaner.util.f.a(r9)     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        byte[] r8 = r8.getBytes()     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        r4.write(r8)     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        java.lang.String r8 = "\n"
                        r4.writeBytes(r8)     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        r4.flush()     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        goto L_0x0038
                    L_0x0064:
                        r0 = move-exception
                        r5 = r4
                        r1 = r3
                        r6 = r3
                        r8 = r7
                        r7 = r3
                        r4 = r0
                        r0 = r3
                    L_0x006c:
                        r4.printStackTrace()     // Catch:{ all -> 0x0170 }
                        if (r5 == 0) goto L_0x0074
                        r5.close()     // Catch:{ IOException -> 0x010e }
                    L_0x0074:
                        if (r7 == 0) goto L_0x0079
                        r7.close()     // Catch:{ IOException -> 0x010e }
                    L_0x0079:
                        if (r6 == 0) goto L_0x007e
                        r6.close()     // Catch:{ IOException -> 0x010e }
                    L_0x007e:
                        if (r8 == 0) goto L_0x0083
                        r8.destroy()
                    L_0x0083:
                        com.pureapps.cleaner.util.j$a r4 = new com.pureapps.cleaner.util.j$a
                        if (r1 != 0) goto L_0x0158
                        r1 = r3
                    L_0x0088:
                        if (r0 != 0) goto L_0x015e
                        r0 = r3
                    L_0x008b:
                        r4.<init>(r2, r1, r0)
                        r0 = r4
                        goto L_0x0011
                    L_0x0090:
                        java.lang.String r1 = "exit\n"
                        r4.writeBytes(r1)     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        r4.flush()     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        boolean r1 = r8     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                        if (r1 == 0) goto L_0x0205
                        int r8 = r7.waitFor()     // Catch:{ IOException -> 0x0064, Exception -> 0x017f, all -> 0x0168 }
                    L_0x00a0:
                        boolean r1 = r7     // Catch:{ IOException -> 0x01d2, Exception -> 0x0188, all -> 0x0168 }
                        if (r1 == 0) goto L_0x01fe
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01d2, Exception -> 0x0188, all -> 0x0168 }
                        r2.<init>()     // Catch:{ IOException -> 0x01d2, Exception -> 0x0188, all -> 0x0168 }
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01dd, Exception -> 0x0192, all -> 0x0168 }
                        r1.<init>()     // Catch:{ IOException -> 0x01dd, Exception -> 0x0192, all -> 0x0168 }
                        java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ IOException -> 0x01e8, Exception -> 0x019c, all -> 0x0168 }
                        java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x01e8, Exception -> 0x019c, all -> 0x0168 }
                        java.io.InputStream r9 = r7.getInputStream()     // Catch:{ IOException -> 0x01e8, Exception -> 0x019c, all -> 0x0168 }
                        r5.<init>(r9)     // Catch:{ IOException -> 0x01e8, Exception -> 0x019c, all -> 0x0168 }
                        r6.<init>(r5)     // Catch:{ IOException -> 0x01e8, Exception -> 0x019c, all -> 0x0168 }
                        java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x01f3, Exception -> 0x01a7, all -> 0x016b }
                        java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x01f3, Exception -> 0x01a7, all -> 0x016b }
                        java.io.InputStream r10 = r7.getErrorStream()     // Catch:{ IOException -> 0x01f3, Exception -> 0x01a7, all -> 0x016b }
                        r9.<init>(r10)     // Catch:{ IOException -> 0x01f3, Exception -> 0x01a7, all -> 0x016b }
                        r5.<init>(r9)     // Catch:{ IOException -> 0x01f3, Exception -> 0x01a7, all -> 0x016b }
                    L_0x00ca:
                        java.lang.String r9 = r6.readLine()     // Catch:{ IOException -> 0x00e4, Exception -> 0x01b2, all -> 0x016d }
                        if (r9 == 0) goto L_0x00da
                        r2.append(r9)     // Catch:{ IOException -> 0x00e4, Exception -> 0x01b2, all -> 0x016d }
                        r9 = 10
                        r2.append(r9)     // Catch:{ IOException -> 0x00e4, Exception -> 0x01b2, all -> 0x016d }
                        r8 = r0
                        goto L_0x00ca
                    L_0x00da:
                        java.lang.String r0 = r5.readLine()     // Catch:{ IOException -> 0x00e4, Exception -> 0x01b2, all -> 0x016d }
                        if (r0 == 0) goto L_0x00f0
                        r1.append(r0)     // Catch:{ IOException -> 0x00e4, Exception -> 0x01b2, all -> 0x016d }
                        goto L_0x00da
                    L_0x00e4:
                        r0 = move-exception
                        r11 = r0
                        r0 = r1
                        r1 = r2
                        r2 = r8
                        r8 = r7
                        r7 = r6
                        r6 = r5
                        r5 = r4
                        r4 = r11
                        goto L_0x006c
                    L_0x00f0:
                        r0 = r1
                        r1 = r2
                        r2 = r8
                    L_0x00f3:
                        if (r4 == 0) goto L_0x00f8
                        r4.close()     // Catch:{ IOException -> 0x0109 }
                    L_0x00f8:
                        if (r6 == 0) goto L_0x00fd
                        r6.close()     // Catch:{ IOException -> 0x0109 }
                    L_0x00fd:
                        if (r5 == 0) goto L_0x0102
                        r5.close()     // Catch:{ IOException -> 0x0109 }
                    L_0x0102:
                        if (r7 == 0) goto L_0x0083
                        r7.destroy()
                        goto L_0x0083
                    L_0x0109:
                        r4 = move-exception
                        r4.printStackTrace()
                        goto L_0x0102
                    L_0x010e:
                        r4 = move-exception
                        r4.printStackTrace()
                        goto L_0x007e
                    L_0x0114:
                        r0 = move-exception
                        r4 = r0
                        r5 = r3
                        r1 = r3
                        r6 = r3
                        r7 = r3
                        r8 = r3
                        r0 = r3
                    L_0x011c:
                        r4.printStackTrace()     // Catch:{ all -> 0x0170 }
                        if (r5 == 0) goto L_0x0124
                        r5.close()     // Catch:{ IOException -> 0x0135 }
                    L_0x0124:
                        if (r7 == 0) goto L_0x0129
                        r7.close()     // Catch:{ IOException -> 0x0135 }
                    L_0x0129:
                        if (r6 == 0) goto L_0x012e
                        r6.close()     // Catch:{ IOException -> 0x0135 }
                    L_0x012e:
                        if (r8 == 0) goto L_0x0083
                        r8.destroy()
                        goto L_0x0083
                    L_0x0135:
                        r4 = move-exception
                        r4.printStackTrace()
                        goto L_0x012e
                    L_0x013a:
                        r0 = move-exception
                        r4 = r3
                        r6 = r3
                        r7 = r3
                    L_0x013e:
                        if (r4 == 0) goto L_0x0143
                        r4.close()     // Catch:{ IOException -> 0x0153 }
                    L_0x0143:
                        if (r6 == 0) goto L_0x0148
                        r6.close()     // Catch:{ IOException -> 0x0153 }
                    L_0x0148:
                        if (r3 == 0) goto L_0x014d
                        r3.close()     // Catch:{ IOException -> 0x0153 }
                    L_0x014d:
                        if (r7 == 0) goto L_0x0152
                        r7.destroy()
                    L_0x0152:
                        throw r0
                    L_0x0153:
                        r1 = move-exception
                        r1.printStackTrace()
                        goto L_0x014d
                    L_0x0158:
                        java.lang.String r1 = r1.toString()
                        goto L_0x0088
                    L_0x015e:
                        java.lang.String r0 = r0.toString()
                        goto L_0x008b
                    L_0x0164:
                        r0 = move-exception
                        r4 = r3
                        r6 = r3
                        goto L_0x013e
                    L_0x0168:
                        r0 = move-exception
                        r6 = r3
                        goto L_0x013e
                    L_0x016b:
                        r0 = move-exception
                        goto L_0x013e
                    L_0x016d:
                        r0 = move-exception
                        r3 = r5
                        goto L_0x013e
                    L_0x0170:
                        r0 = move-exception
                        r4 = r5
                        r3 = r6
                        r6 = r7
                        r7 = r8
                        goto L_0x013e
                    L_0x0176:
                        r0 = move-exception
                        r4 = r0
                        r5 = r3
                        r1 = r3
                        r6 = r3
                        r8 = r7
                        r7 = r3
                        r0 = r3
                        goto L_0x011c
                    L_0x017f:
                        r0 = move-exception
                        r5 = r4
                        r1 = r3
                        r6 = r3
                        r8 = r7
                        r7 = r3
                        r4 = r0
                        r0 = r3
                        goto L_0x011c
                    L_0x0188:
                        r0 = move-exception
                        r5 = r4
                        r1 = r3
                        r6 = r3
                        r2 = r8
                        r8 = r7
                        r4 = r0
                        r0 = r3
                        r7 = r3
                        goto L_0x011c
                    L_0x0192:
                        r0 = move-exception
                        r5 = r4
                        r1 = r2
                        r6 = r3
                        r4 = r0
                        r2 = r8
                        r8 = r7
                        r0 = r3
                        r7 = r3
                        goto L_0x011c
                    L_0x019c:
                        r0 = move-exception
                        r5 = r4
                        r6 = r3
                        r4 = r0
                        r0 = r1
                        r1 = r2
                        r2 = r8
                        r8 = r7
                        r7 = r3
                        goto L_0x011c
                    L_0x01a7:
                        r0 = move-exception
                        r5 = r4
                        r4 = r0
                        r0 = r1
                        r1 = r2
                        r2 = r8
                        r8 = r7
                        r7 = r6
                        r6 = r3
                        goto L_0x011c
                    L_0x01b2:
                        r0 = move-exception
                        r11 = r0
                        r0 = r1
                        r1 = r2
                        r2 = r8
                        r8 = r7
                        r7 = r6
                        r6 = r5
                        r5 = r4
                        r4 = r11
                        goto L_0x011c
                    L_0x01be:
                        r0 = move-exception
                        r4 = r0
                        r5 = r3
                        r1 = r3
                        r6 = r3
                        r7 = r3
                        r8 = r3
                        r0 = r3
                        goto L_0x006c
                    L_0x01c8:
                        r0 = move-exception
                        r4 = r0
                        r5 = r3
                        r1 = r3
                        r6 = r3
                        r8 = r7
                        r7 = r3
                        r0 = r3
                        goto L_0x006c
                    L_0x01d2:
                        r0 = move-exception
                        r5 = r4
                        r1 = r3
                        r6 = r3
                        r2 = r8
                        r8 = r7
                        r4 = r0
                        r0 = r3
                        r7 = r3
                        goto L_0x006c
                    L_0x01dd:
                        r0 = move-exception
                        r5 = r4
                        r1 = r2
                        r6 = r3
                        r4 = r0
                        r2 = r8
                        r8 = r7
                        r0 = r3
                        r7 = r3
                        goto L_0x006c
                    L_0x01e8:
                        r0 = move-exception
                        r5 = r4
                        r6 = r3
                        r4 = r0
                        r0 = r1
                        r1 = r2
                        r2 = r8
                        r8 = r7
                        r7 = r3
                        goto L_0x006c
                    L_0x01f3:
                        r0 = move-exception
                        r5 = r4
                        r4 = r0
                        r0 = r1
                        r1 = r2
                        r2 = r8
                        r8 = r7
                        r7 = r6
                        r6 = r3
                        goto L_0x006c
                    L_0x01fe:
                        r0 = r3
                        r1 = r3
                        r5 = r3
                        r6 = r3
                        r2 = r8
                        goto L_0x00f3
                    L_0x0205:
                        r8 = r2
                        goto L_0x00a0
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.pureapps.cleaner.util.j.AnonymousClass1.call():com.pureapps.cleaner.util.j$a");
                }
            }).get(3000, TimeUnit.MILLISECONDS);
            System.out.println("任务成功返回:" + aVar);
            return aVar;
        } catch (TimeoutException e2) {
            System.out.println("处理超时啦....");
            e2.printStackTrace();
        } catch (Exception e3) {
            System.out.println("处理失败.");
            e3.printStackTrace();
        } finally {
            newFixedThreadPool.shutdown();
        }
        return new a(-1, null, "error");
    }

    /* compiled from: ShellUtils */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public int f5880a;

        /* renamed from: b  reason: collision with root package name */
        public String f5881b;

        /* renamed from: c  reason: collision with root package name */
        public String f5882c;

        public a(int i, String str, String str2) {
            this.f5880a = i;
            this.f5881b = str;
            this.f5882c = str2;
        }
    }
}
