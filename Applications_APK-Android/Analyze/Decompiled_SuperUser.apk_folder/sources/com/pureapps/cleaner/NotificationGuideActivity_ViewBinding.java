package com.pureapps.cleaner;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;

public class NotificationGuideActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private NotificationGuideActivity f5420a;

    /* renamed from: b  reason: collision with root package name */
    private View f5421b;

    public NotificationGuideActivity_ViewBinding(final NotificationGuideActivity notificationGuideActivity, View view) {
        this.f5420a = notificationGuideActivity;
        notificationGuideActivity.layout1 = Utils.findRequiredView(view, R.id.dt, "field 'layout1'");
        notificationGuideActivity.layout2 = Utils.findRequiredView(view, R.id.dv, "field 'layout2'");
        notificationGuideActivity.layout3 = Utils.findRequiredView(view, R.id.du, "field 'layout3'");
        notificationGuideActivity.layout = Utils.findRequiredView(view, R.id.dm, "field 'layout'");
        notificationGuideActivity.view1 = Utils.findRequiredView(view, R.id.dn, "field 'view1'");
        notificationGuideActivity.view2 = Utils.findRequiredView(view, R.id.f9do, "field 'view2'");
        notificationGuideActivity.view3 = Utils.findRequiredView(view, R.id.dq, "field 'view3'");
        notificationGuideActivity.view4 = Utils.findRequiredView(view, R.id.dp, "field 'view4'");
        notificationGuideActivity.text = Utils.findRequiredView(view, R.id.dr, "field 'text'");
        notificationGuideActivity.image = Utils.findRequiredView(view, R.id.bg, "field 'image'");
        View findRequiredView = Utils.findRequiredView(view, R.id.dw, "field 'btnUse' and method 'onClick'");
        notificationGuideActivity.btnUse = (TextView) Utils.castView(findRequiredView, R.id.dw, "field 'btnUse'", TextView.class);
        this.f5421b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                notificationGuideActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        NotificationGuideActivity notificationGuideActivity = this.f5420a;
        if (notificationGuideActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f5420a = null;
        notificationGuideActivity.layout1 = null;
        notificationGuideActivity.layout2 = null;
        notificationGuideActivity.layout3 = null;
        notificationGuideActivity.layout = null;
        notificationGuideActivity.view1 = null;
        notificationGuideActivity.view2 = null;
        notificationGuideActivity.view3 = null;
        notificationGuideActivity.view4 = null;
        notificationGuideActivity.text = null;
        notificationGuideActivity.image = null;
        notificationGuideActivity.btnUse = null;
        this.f5421b.setOnClickListener(null);
        this.f5421b = null;
    }
}
