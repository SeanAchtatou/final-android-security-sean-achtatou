package com.pureapps.cleaner;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;

public class GuideDialog_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private GuideDialog f5371a;

    /* renamed from: b  reason: collision with root package name */
    private View f5372b;

    public GuideDialog_ViewBinding(final GuideDialog guideDialog, View view) {
        this.f5371a = guideDialog;
        View findRequiredView = Utils.findRequiredView(view, R.id.jm, "method 'onClick'");
        this.f5372b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                guideDialog.onClick(view);
            }
        });
    }

    public void unbind() {
        if (this.f5371a == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f5371a = null;
        this.f5372b.setOnClickListener(null);
        this.f5372b = null;
    }
}
