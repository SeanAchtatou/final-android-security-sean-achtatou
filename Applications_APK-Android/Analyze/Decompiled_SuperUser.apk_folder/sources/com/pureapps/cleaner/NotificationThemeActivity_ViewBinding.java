package com.pureapps.cleaner;

import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;

public class NotificationThemeActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private NotificationThemeActivity f5434a;

    /* renamed from: b  reason: collision with root package name */
    private View f5435b;

    /* renamed from: c  reason: collision with root package name */
    private View f5436c;

    /* renamed from: d  reason: collision with root package name */
    private View f5437d;

    public NotificationThemeActivity_ViewBinding(final NotificationThemeActivity notificationThemeActivity, View view) {
        this.f5434a = notificationThemeActivity;
        notificationThemeActivity.mSwitch = (SwitchCompat) Utils.findRequiredViewAsType(view, R.id.e3, "field 'mSwitch'", SwitchCompat.class);
        notificationThemeActivity.mBlackSwitch = (CheckBox) Utils.findRequiredViewAsType(view, R.id.e6, "field 'mBlackSwitch'", CheckBox.class);
        notificationThemeActivity.mWhileSwitch = (CheckBox) Utils.findRequiredViewAsType(view, R.id.e8, "field 'mWhileSwitch'", CheckBox.class);
        notificationThemeActivity.mThemeLayout = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.e4, "field 'mThemeLayout'", LinearLayout.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.e2, "method 'onClick'");
        this.f5435b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                notificationThemeActivity.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.e5, "method 'onClick'");
        this.f5436c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                notificationThemeActivity.onClick(view);
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.e7, "method 'onClick'");
        this.f5437d = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                notificationThemeActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        NotificationThemeActivity notificationThemeActivity = this.f5434a;
        if (notificationThemeActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f5434a = null;
        notificationThemeActivity.mSwitch = null;
        notificationThemeActivity.mBlackSwitch = null;
        notificationThemeActivity.mWhileSwitch = null;
        notificationThemeActivity.mThemeLayout = null;
        this.f5435b.setOnClickListener(null);
        this.f5435b = null;
        this.f5436c.setOnClickListener(null);
        this.f5436c = null;
        this.f5437d.setOnClickListener(null);
        this.f5437d = null;
    }
}
