package com.pureapps.cleaner.bean;

import android.content.Context;
import com.pureapps.cleaner.util.l;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.json.JSONObject;

/* compiled from: OptimizerInfo */
public class m {
    private static m i;

    /* renamed from: a  reason: collision with root package name */
    public int f5653a;

    /* renamed from: b  reason: collision with root package name */
    public ArrayList<String> f5654b;

    /* renamed from: c  reason: collision with root package name */
    public ArrayList<String> f5655c;

    /* renamed from: d  reason: collision with root package name */
    public ArrayList<String> f5656d;

    /* renamed from: e  reason: collision with root package name */
    public ArrayList<String> f5657e;

    /* renamed from: f  reason: collision with root package name */
    public ArrayList<String> f5658f;

    /* renamed from: g  reason: collision with root package name */
    public ArrayList<String> f5659g;

    /* renamed from: h  reason: collision with root package name */
    public ArrayList<String> f5660h;

    private m() {
    }

    public static m a() {
        if (i == null) {
            i = new m();
        }
        return i;
    }

    public void a(Context context) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open("optimizer.dat")));
            String str = "";
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    str = str + readLine;
                } else {
                    JSONObject jSONObject = new JSONObject(str);
                    this.f5653a = jSONObject.getInt("version");
                    this.f5654b = l.a(jSONObject.getJSONArray("music"));
                    this.f5655c = l.a(jSONObject.getJSONArray("nc1"));
                    this.f5656d = l.a(jSONObject.getJSONArray("nc2"));
                    this.f5657e = l.a(jSONObject.getJSONArray("criticalPackage"));
                    this.f5658f = l.a(jSONObject.getJSONArray("criticalUid"));
                    this.f5659g = l.a(jSONObject.getJSONArray("group"));
                    this.f5660h = l.a(jSONObject.getJSONArray("nonCriticalPackage"));
                    return;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
