package com.pureapps.cleaner.bean;

import android.service.notification.StatusBarNotification;
import java.io.Serializable;

public class NotificationInfo implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private int f5598a;

    /* renamed from: b  reason: collision with root package name */
    private String f5599b;

    /* renamed from: c  reason: collision with root package name */
    private long f5600c;

    /* renamed from: d  reason: collision with root package name */
    private String f5601d;

    /* renamed from: e  reason: collision with root package name */
    private StatusBarNotification f5602e;

    /* renamed from: f  reason: collision with root package name */
    private String f5603f;

    /* renamed from: g  reason: collision with root package name */
    private String f5604g;

    /* renamed from: h  reason: collision with root package name */
    private String f5605h;
    private String i;

    public String a() {
        return this.f5605h;
    }

    public void a(String str) {
        this.f5605h = str;
    }

    public String b() {
        return this.i;
    }

    public void b(String str) {
        this.i = str;
    }

    public int c() {
        return this.f5598a;
    }

    public void a(int i2) {
        this.f5598a = i2;
    }

    public String d() {
        return this.f5599b;
    }

    public void c(String str) {
        this.f5599b = str;
    }

    public void a(long j) {
        this.f5600c = j;
    }

    public String e() {
        return this.f5601d;
    }

    public void d(String str) {
        this.f5601d = str;
    }

    public StatusBarNotification f() {
        return this.f5602e;
    }

    public void a(StatusBarNotification statusBarNotification) {
        this.f5602e = statusBarNotification;
    }

    public String g() {
        return this.f5603f;
    }

    public void e(String str) {
        this.f5603f = str;
    }

    public String h() {
        return this.f5604g;
    }

    public void f(String str) {
        this.f5604g = str;
    }
}
