package com.pureapps.cleaner.bean;

import android.graphics.drawable.Drawable;

/* compiled from: MemoryBoostAppInfo */
public class j {

    /* renamed from: a  reason: collision with root package name */
    public int f5638a;

    /* renamed from: b  reason: collision with root package name */
    public String f5639b;

    /* renamed from: c  reason: collision with root package name */
    public String f5640c;

    /* renamed from: d  reason: collision with root package name */
    public Drawable f5641d;

    /* renamed from: e  reason: collision with root package name */
    public long f5642e;

    /* renamed from: f  reason: collision with root package name */
    public long f5643f;

    /* renamed from: g  reason: collision with root package name */
    public boolean f5644g = true;

    /* renamed from: h  reason: collision with root package name */
    public int f5645h;
}
