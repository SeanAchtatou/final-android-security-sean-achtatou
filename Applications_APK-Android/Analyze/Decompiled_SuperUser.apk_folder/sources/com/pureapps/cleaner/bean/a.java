package com.pureapps.cleaner.bean;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.kingouser.com.entity.UninstallAppInfo;
import com.pureapps.cleaner.db.b;
import com.pureapps.cleaner.db.c;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: IgnoreAppInfo */
public class a extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f5606a = Uri.parse("content://com.kingouser.com.database/ignoreList");

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f5607b = {b.RECORD_ID, UninstallAppInfo.COLUMN_PKG};

    /* renamed from: c  reason: collision with root package name */
    public String f5608c;

    /* renamed from: d  reason: collision with root package name */
    public Drawable f5609d;

    /* renamed from: e  reason: collision with root package name */
    public String f5610e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f5611f;

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(UninstallAppInfo.COLUMN_PKG, this.f5608c);
        return contentValues;
    }

    /* renamed from: a */
    public a restore(Cursor cursor) {
        this.mId = cursor.getLong(0);
        this.f5608c = cursor.getString(1);
        return this;
    }

    public String[] getContentProjection() {
        return f5607b;
    }

    public static ArrayList<a> a(Context context) {
        Cursor cursor;
        ArrayList<a> arrayList = new ArrayList<>();
        try {
            cursor = context.getContentResolver().query(f5606a, null, null, null, null);
            while (cursor.moveToNext()) {
                try {
                    a aVar = new a();
                    aVar.restore(cursor);
                    arrayList.add(aVar);
                } catch (Exception e2) {
                    e = e2;
                    try {
                        e.printStackTrace();
                        c.a(cursor);
                        return arrayList;
                    } catch (Throwable th) {
                        th = th;
                        c.a(cursor);
                        throw th;
                    }
                }
            }
            c.a(cursor);
        } catch (Exception e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            c.a(cursor);
            throw th;
        }
        return arrayList;
    }

    public static void a(Context context, ArrayList<a> arrayList) {
        try {
            Iterator<a> it = arrayList.iterator();
            while (it.hasNext()) {
                context.getContentResolver().insert(f5606a, it.next().toContentValues());
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void b(Context context, ArrayList<a> arrayList) {
        try {
            Iterator<a> it = arrayList.iterator();
            while (it.hasNext()) {
                context.getContentResolver().delete(f5606a, "pkg = ? ", new String[]{it.next().f5608c});
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
