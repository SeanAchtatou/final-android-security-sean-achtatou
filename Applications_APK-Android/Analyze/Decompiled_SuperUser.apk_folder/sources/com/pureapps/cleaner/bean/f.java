package com.pureapps.cleaner.bean;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.pureapps.cleaner.db.a;
import com.pureapps.cleaner.db.b;
import com.pureapps.cleaner.db.c;
import com.pureapps.cleaner.db.d;
import com.pureapps.cleaner.util.l;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;

/* compiled from: JunkCachePathInfo */
public class f extends i {

    /* renamed from: a  reason: collision with root package name */
    public long f5620a;

    /* renamed from: b  reason: collision with root package name */
    public int f5621b;

    /* renamed from: c  reason: collision with root package name */
    public int f5622c;

    /* renamed from: d  reason: collision with root package name */
    public int f5623d;

    /* renamed from: e  reason: collision with root package name */
    public String f5624e;

    /* renamed from: f  reason: collision with root package name */
    public long f5625f;

    /* renamed from: g  reason: collision with root package name */
    public String f5626g;

    /* renamed from: h  reason: collision with root package name */
    public String f5627h;

    public static HashMap<Long, String> a(Context context) {
        Cursor cursor = null;
        HashMap<Long, String> hashMap = new HashMap<>();
        try {
            cursor = a.a(context).a("pkgcache.db").rawQuery("select dirmd5._id, dirmd5.md5 from dirmd5", null);
            while (cursor.moveToNext()) {
                long j = cursor.getLong(cursor.getColumnIndex(b.RECORD_ID));
                hashMap.put(Long.valueOf(j), cursor.getString(cursor.getColumnIndex("md5")));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            c.a(cursor);
        }
        return hashMap;
    }

    public static String a(String str, HashMap<Long, String> hashMap) {
        String str2;
        String[] strArr = new String[0];
        try {
            strArr = new String(str.getBytes(), "utf-8").split("//");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        String str3 = "";
        if (strArr.length < 2) {
            str2 = str;
        } else {
            String str4 = "//" + strArr[1];
            str2 = strArr[0];
            str3 = str4;
        }
        String[] split = str2.split("\\+");
        int length = split.length;
        if (length <= 0) {
            return str;
        }
        String str5 = "";
        for (int i = 0; i < length; i++) {
            if (i > 0) {
                str5 = str5 + "+";
            }
            if (l.c(split[i])) {
                str5 = str5 + hashMap.get(Long.valueOf(Long.parseLong(split[i])));
            } else {
                str5 = str5 + split[i];
            }
        }
        if (str3.length() > 0) {
            Log.e("junk", str5 + str3);
        }
        return str5 + str3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00fc, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00fd, code lost:
        r1 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0193, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0193 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:8:0x0063] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<com.pureapps.cleaner.bean.f> a(android.content.Context r13, java.util.HashMap<java.lang.Long, java.lang.String> r14, boolean r15) {
        /*
            r3 = 0
            r12 = 65535(0xffff, float:9.1834E-41)
            r2 = 0
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.util.Set r0 = r14.keySet()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.Iterator r6 = r0.iterator()
            r1 = r2
        L_0x0018:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0036
            java.lang.Object r0 = r6.next()
            java.lang.Long r0 = (java.lang.Long) r0
            long r8 = r0.longValue()
            if (r1 <= 0) goto L_0x002f
            java.lang.String r0 = ","
            r4.append(r0)
        L_0x002f:
            r4.append(r8)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0018
        L_0x0036:
            java.util.HashMap r6 = a(r13)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "select pathinfo2.pathid as pathid,pathinfo2.pathtype as pathtype,pathdata.dir as path,pathinfo2.cleantype as cleantype,pathinfo2.cleanlevel as cleanlevel,pathinfo2.cleanadv as cleanadv,pathinfo2.cleanpro as cleanpro,pathinfo2.langnamedesc as langnamedesc,pathinfo2.pkgid as pkgid from ("
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "select pathinfo.pathid as pathid,pathinfo.pathtype as pathtype,pathinfo.path as path,pathinfo.cleantype as cleantype,pathinfo.cleanlevel as cleanlevel,pathinfo.cleanadv as cleanadv,pathinfo.cleanpro as cleanpro,pathinfo.langnamedesc as langnamedesc,pkg.pkgid as pkgid  from pathinfo left join pkg on pathinfo.pkgid=pkg._id where pkg.pkgid in ("
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r4.toString()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ")"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ") as pathinfo2 left join pathdata on pathinfo2.path=pathdata._id"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.pureapps.cleaner.db.a r1 = com.pureapps.cleaner.db.a.a(r13)     // Catch:{ Exception -> 0x019b, all -> 0x0193 }
            java.lang.String r4 = "pkgcache.db"
            android.database.sqlite.SQLiteDatabase r1 = r1.a(r4)     // Catch:{ Exception -> 0x019b, all -> 0x0193 }
            r4 = 0
            android.database.Cursor r3 = r1.rawQuery(r0, r4)     // Catch:{ Exception -> 0x019b, all -> 0x0193 }
        L_0x0072:
            boolean r0 = r3.moveToNext()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            if (r0 == 0) goto L_0x0105
            com.pureapps.cleaner.bean.f r1 = new com.pureapps.cleaner.bean.f     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.<init>()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = "pathid"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            long r8 = r3.getLong(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.f5620a = r8     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = "pathtype"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r0 = r3.getInt(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.m = r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = "path"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = r3.getString(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.k = r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = "cleantype"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r0 = r3.getInt(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.f5621b = r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = "cleanlevel"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r0 = r3.getInt(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.f5622c = r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = "cleanadv"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r0 = r3.getInt(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.f5623d = r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = "langnamedesc"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = r3.getString(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.f5626g = r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = "pkgid"
            int r0 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            long r8 = r3.getLong(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.f5625f = r8     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            long r8 = r1.f5625f     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.Long r0 = java.lang.Long.valueOf(r8)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.Object r0 = r14.get(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.f5624e = r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r0 = r1.f5622c     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            if (r0 == r12) goto L_0x0072
            java.lang.String r0 = r1.k     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r0 = a(r0, r6)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.k = r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r5.add(r1)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            goto L_0x0072
        L_0x00fc:
            r0 = move-exception
            r1 = r3
        L_0x00fe:
            r0.printStackTrace()     // Catch:{ all -> 0x0198 }
            com.pureapps.cleaner.db.c.a(r1)
        L_0x0104:
            return r5
        L_0x0105:
            if (r15 == 0) goto L_0x018e
            com.pureapps.cleaner.manager.CacheManager r0 = new com.pureapps.cleaner.manager.CacheManager     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r0.<init>(r13)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            com.google.gson.Gson r1 = new com.google.gson.Gson     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r1.<init>()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r4 = "http://api.bos.kgmobi.com/kgservice/junk-library/getInfo"
            r7 = 0
            java.lang.String r4 = r0.a(r4, r7)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            if (r4 == 0) goto L_0x018e
            java.lang.String r4 = "http://api.bos.kgmobi.com/kgservice/junk-library/getInfo"
            r7 = 0
            java.lang.String r0 = r0.a(r4, r7)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            com.pureapps.cleaner.bean.f$1 r4 = new com.pureapps.cleaner.bean.f$1     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r4.<init>()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.reflect.Type r4 = r4.getType()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.Object r0 = r1.fromJson(r0, r4)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.util.Map r0 = (java.util.Map) r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
        L_0x0138:
            boolean r0 = r7.hasNext()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            if (r0 == 0) goto L_0x018e
            java.lang.Object r0 = r7.next()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.Object r0 = r0.getValue()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r4 = r2
        L_0x014b:
            if (r0 == 0) goto L_0x0138
            int r1 = r0.size()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            if (r4 >= r1) goto L_0x0138
            java.lang.Object r1 = r0.get(r4)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            com.pureapps.cleaner.bean.CloudCacheInfo r1 = (com.pureapps.cleaner.bean.CloudCacheInfo) r1     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r8 = r1.cleanLevel     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            if (r8 == r12) goto L_0x018a
            com.pureapps.cleaner.bean.f r8 = new com.pureapps.cleaner.bean.f     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r8.<init>()     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r9 = r1.pathid     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            long r10 = (long) r9     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r8.f5620a = r10     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r9 = r1.pathtype     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r8.m = r9     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r9 = r1.dir     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r8.k = r9     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r9 = r1.cleanType     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r8.f5621b = r9     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r9 = r1.cleanLevel     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r8.f5622c = r9     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            int r9 = r1.cleanAdv     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r8.f5623d = r9     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r1 = r1.pkgId     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r8.f5624e = r1     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r1 = r8.k     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            java.lang.String r1 = a(r1, r6)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r8.k = r1     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
            r5.add(r8)     // Catch:{ Exception -> 0x00fc, all -> 0x0193 }
        L_0x018a:
            int r1 = r4 + 1
            r4 = r1
            goto L_0x014b
        L_0x018e:
            com.pureapps.cleaner.db.c.a(r3)
            goto L_0x0104
        L_0x0193:
            r0 = move-exception
        L_0x0194:
            com.pureapps.cleaner.db.c.a(r3)
            throw r0
        L_0x0198:
            r0 = move-exception
            r3 = r1
            goto L_0x0194
        L_0x019b:
            r0 = move-exception
            r1 = r3
            goto L_0x00fe
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pureapps.cleaner.bean.f.a(android.content.Context, java.util.HashMap, boolean):java.util.ArrayList");
    }

    public void b(Context context) {
        String[] split;
        try {
            String[] split2 = this.f5626g.split("\\|");
            if (split2 != null) {
                int length = split2.length;
                Locale locale = context.getResources().getConfiguration().locale;
                String lowerCase = locale.getLanguage().toLowerCase();
                String lowerCase2 = locale.toString().toLowerCase();
                int i = 0;
                String str = null;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    split = split2[i].split(":");
                    if (lowerCase.equals(split[0]) || lowerCase2.equals(split[0])) {
                        str = split[1];
                    } else {
                        if (str == null) {
                            str = split[1];
                        } else if ("en".equals(split[0])) {
                            str = split[1];
                        }
                        i++;
                    }
                }
                str = split[1];
                this.f5627h = d.a.a(context, str).f5676c;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            c.a(null);
        }
    }
}
