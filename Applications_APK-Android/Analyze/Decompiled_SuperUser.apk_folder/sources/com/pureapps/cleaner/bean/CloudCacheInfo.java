package com.pureapps.cleaner.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class CloudCacheInfo implements Serializable {
    Map<String, String> brief;
    int cleanAdv;
    int cleanLevel;
    int cleanType;
    String createTime;
    String detial;
    String dir;
    String language;
    int pathid;
    int pathtype;
    String pkgId;

    public static Map<String, List<CloudCacheInfo>> getCloudCacheInfoMaps(List<String> list, String str) {
        JSONObject jSONObject = new JSONObject(str);
        HashMap hashMap = new HashMap();
        if (jSONObject.getInt("status") != 1) {
            return hashMap;
        }
        JSONArray jSONArray = jSONObject.getJSONArray("lib");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= jSONArray.length()) {
                return hashMap;
            }
            JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
            if (!jSONObject2.has("x")) {
                String str2 = list.get(i2);
                ArrayList arrayList = new ArrayList();
                JSONArray jSONArray2 = jSONObject2.getJSONArray("m");
                int i3 = 0;
                while (i3 < jSONArray2.length()) {
                    JSONObject jSONObject3 = jSONArray2.getJSONObject(i3);
                    CloudCacheInfo cloudCacheInfo = new CloudCacheInfo();
                    cloudCacheInfo.pathid = jSONObject3.getInt("a");
                    cloudCacheInfo.pathtype = jSONObject3.getInt("b");
                    cloudCacheInfo.cleanType = jSONObject3.getInt("c");
                    cloudCacheInfo.cleanLevel = jSONObject3.getInt("d");
                    cloudCacheInfo.cleanAdv = jSONObject3.getInt("e");
                    cloudCacheInfo.dir = jSONObject3.getString("g");
                    cloudCacheInfo.createTime = jSONObject3.getString("j");
                    try {
                        JSONArray jSONArray3 = jSONObject3.getJSONArray("k");
                        HashMap hashMap2 = new HashMap(jSONArray3.length());
                        for (int i4 = 0; i4 < jSONArray3.length(); i4++) {
                            JSONObject jSONObject4 = jSONArray3.getJSONObject(i4);
                            hashMap2.put(jSONObject4.getString("u"), jSONObject4.getString("k"));
                        }
                        cloudCacheInfo.brief = hashMap2;
                        cloudCacheInfo.pkgId = str2;
                        arrayList.add(cloudCacheInfo);
                        i3++;
                    } catch (Exception e2) {
                        throw e2;
                    }
                }
                hashMap.put(str2, arrayList);
            }
            i = i2 + 1;
        }
    }
}
