package com.pureapps.cleaner.bean;

import java.util.Locale;

/* compiled from: JunkAdPathInfo */
public class c extends i {

    /* renamed from: a  reason: collision with root package name */
    public String f5612a;

    /* renamed from: b  reason: collision with root package name */
    public String f5613b;

    /* renamed from: c  reason: collision with root package name */
    public String f5614c;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v16, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v18, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<com.pureapps.cleaner.bean.c> a(android.content.Context r6) {
        /*
            r1 = 0
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.lang.String r2 = a()
            java.lang.String r0 = b()
            boolean r4 = android.text.TextUtils.isEmpty(r2)
            if (r4 == 0) goto L_0x0090
            r0 = r1
        L_0x0015:
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 == 0) goto L_0x001d
            java.lang.String r0 = "en"
        L_0x001d:
            com.pureapps.cleaner.db.a r2 = com.pureapps.cleaner.db.a.a(r6)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r4 = "adcache.db"
            android.database.sqlite.SQLiteDatabase r2 = r2.a(r4)     // Catch:{ Exception -> 0x0088 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0088 }
            r4.<init>()     // Catch:{ Exception -> 0x0088 }
            java.lang.String r5 = "select advfolder.path , advfolder.describeinfo , advfolder.extra,describe.value  from advfolder left join (select id ,lang,value from advfolder_describeinfo where lang='"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0088 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r4 = "') as describe on advfolder._id=describe.id "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0088 }
            r4 = 0
            android.database.Cursor r1 = r2.rawQuery(r0, r4)     // Catch:{ Exception -> 0x0088 }
        L_0x0045:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0088 }
            if (r0 == 0) goto L_0x00cc
            com.pureapps.cleaner.bean.c r0 = new com.pureapps.cleaner.bean.c     // Catch:{ Exception -> 0x0088 }
            r0.<init>()     // Catch:{ Exception -> 0x0088 }
            r4 = 0
            r0.i = r4     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = "path"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0088 }
            r0.k = r2     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = "describeinfo"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0088 }
            r0.f5612a = r2     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = "extra"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0088 }
            r0.f5613b = r2     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = "value"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0088 }
            r0.f5614c = r2     // Catch:{ Exception -> 0x0088 }
            r3.add(r0)     // Catch:{ Exception -> 0x0088 }
            goto L_0x0045
        L_0x0088:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00d0 }
            com.pureapps.cleaner.db.c.a(r1)
        L_0x008f:
            return r3
        L_0x0090:
            java.lang.String r4 = "tw"
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 != 0) goto L_0x00a0
            java.lang.String r4 = "cn"
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 == 0) goto L_0x00b1
        L_0x00a0:
            java.lang.String r0 = r0.toLowerCase()
        L_0x00a4:
            java.lang.String r2 = r2.toLowerCase()
            boolean r4 = android.text.TextUtils.isEmpty(r0)
            if (r4 == 0) goto L_0x00b3
            r0 = r2
            goto L_0x0015
        L_0x00b1:
            r0 = r1
            goto L_0x00a4
        L_0x00b3:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = "_"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            goto L_0x0015
        L_0x00cc:
            com.pureapps.cleaner.db.c.a(r1)
            goto L_0x008f
        L_0x00d0:
            r0 = move-exception
            com.pureapps.cleaner.db.c.a(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pureapps.cleaner.bean.c.a(android.content.Context):java.util.ArrayList");
    }

    public static String a() {
        try {
            return Locale.getDefault().getLanguage();
        } catch (Exception e2) {
            return null;
        }
    }

    public static String b() {
        try {
            return Locale.getDefault().getCountry();
        } catch (Exception e2) {
            return null;
        }
    }
}
