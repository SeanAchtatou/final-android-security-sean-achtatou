package com.pureapps.cleaner.download;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.kingouser.com.entity.UninstallAppInfo;
import com.pureapps.cleaner.db.b;

public class DownloadTask implements Parcelable {
    public static final Parcelable.Creator<DownloadTask> CREATOR = new Parcelable.Creator<DownloadTask>() {
        /* renamed from: a */
        public DownloadTask createFromParcel(Parcel parcel) {
            return new DownloadTask(parcel);
        }

        /* renamed from: a */
        public DownloadTask[] newArray(int i) {
            return new DownloadTask[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f5677a = Uri.parse("content://com.kingouser.com.database/download");
    private static String r = "DownloadTask";
    private static final String[] s = {b.RECORD_ID, "sId", "type", "tn", "tv", "tvc", "ts", "du", "dt", "iu", "state", UninstallAppInfo.COLUMN_PKG, "md5", "fromId", "openType"};

    /* renamed from: b  reason: collision with root package name */
    public String f5678b;

    /* renamed from: c  reason: collision with root package name */
    public int f5679c;

    /* renamed from: d  reason: collision with root package name */
    public String f5680d;

    /* renamed from: e  reason: collision with root package name */
    public String f5681e;

    /* renamed from: f  reason: collision with root package name */
    public int f5682f;

    /* renamed from: g  reason: collision with root package name */
    public long f5683g;

    /* renamed from: h  reason: collision with root package name */
    public String f5684h;
    public long i;
    public String j;
    public int k;
    public String l;
    public String m;
    public long n;
    public boolean o;
    public long p;
    public int q;

    public DownloadTask() {
        this.k = 1;
        this.q = 1;
    }

    public int hashCode() {
        return ((this.f5679c + 527) * 31) + this.l.hashCode();
    }

    private DownloadTask(Parcel parcel) {
        boolean z = false;
        this.k = 1;
        this.q = 1;
        this.f5678b = parcel.readString();
        this.l = parcel.readString();
        this.m = parcel.readString();
        this.f5680d = parcel.readString();
        this.f5681e = parcel.readString();
        this.f5682f = parcel.readInt();
        this.f5683g = parcel.readLong();
        this.f5684h = parcel.readString();
        this.i = parcel.readLong();
        this.j = parcel.readString();
        this.n = parcel.readLong();
        this.k = parcel.readInt();
        this.o = parcel.readInt() != 0;
        this.f5679c = parcel.readInt();
        this.n = parcel.readLong();
        this.o = parcel.readInt() != 0 ? true : z;
        this.p = parcel.readLong();
        this.q = parcel.readInt();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3 = 1;
        parcel.writeString(this.f5678b);
        parcel.writeString(this.l);
        parcel.writeString(this.m);
        parcel.writeString(this.f5680d);
        parcel.writeString(this.f5681e);
        parcel.writeInt(this.f5682f);
        parcel.writeLong(this.f5683g);
        parcel.writeString(this.f5684h);
        parcel.writeLong(this.i);
        parcel.writeString(this.j);
        parcel.writeLong(this.n);
        parcel.writeInt(this.k);
        parcel.writeInt(this.o ? 1 : 0);
        parcel.writeInt(this.f5679c);
        parcel.writeLong(this.n);
        if (!this.o) {
            i3 = 0;
        }
        parcel.writeInt(i3);
        parcel.writeLong(this.p);
        parcel.writeInt(this.q);
    }

    public int describeContents() {
        return 0;
    }
}
