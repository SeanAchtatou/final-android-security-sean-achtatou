package com.pureapps.cleaner;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;
import com.muzakki.ahmad.widget.CollapsingToolbarLayout;
import com.pureapps.cleaner.view.AnimatedExpandableListView;

public class JunkClearActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private JunkClearActivity f5396a;

    /* renamed from: b  reason: collision with root package name */
    private View f5397b;

    public JunkClearActivity_ViewBinding(final JunkClearActivity junkClearActivity, View view) {
        this.f5396a = junkClearActivity;
        junkClearActivity.mToolBarLayout = (CollapsingToolbarLayout) Utils.findRequiredViewAsType(view, R.id.d5, "field 'mToolBarLayout'", CollapsingToolbarLayout.class);
        junkClearActivity.mListView = (AnimatedExpandableListView) Utils.findRequiredViewAsType(view, R.id.d8, "field 'mListView'", AnimatedExpandableListView.class);
        junkClearActivity.mTvSelectedInfo = (TextView) Utils.findRequiredViewAsType(view, R.id.d7, "field 'mTvSelectedInfo'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.d_, "field 'mBtClear' and method 'onClick'");
        junkClearActivity.mBtClear = (Button) Utils.castView(findRequiredView, R.id.d_, "field 'mBtClear'", Button.class);
        this.f5397b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                junkClearActivity.onClick(view);
            }
        });
        junkClearActivity.mRootView = (ViewGroup) Utils.findRequiredViewAsType(view, R.id.d3, "field 'mRootView'", ViewGroup.class);
        junkClearActivity.mProgressBar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.d6, "field 'mProgressBar'", ProgressBar.class);
    }

    public void unbind() {
        JunkClearActivity junkClearActivity = this.f5396a;
        if (junkClearActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f5396a = null;
        junkClearActivity.mToolBarLayout = null;
        junkClearActivity.mListView = null;
        junkClearActivity.mTvSelectedInfo = null;
        junkClearActivity.mBtClear = null;
        junkClearActivity.mRootView = null;
        junkClearActivity.mProgressBar = null;
        this.f5397b.setOnClickListener(null);
        this.f5397b = null;
    }
}
