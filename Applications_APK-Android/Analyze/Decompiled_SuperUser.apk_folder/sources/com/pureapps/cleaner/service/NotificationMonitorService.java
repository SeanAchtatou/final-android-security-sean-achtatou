package com.pureapps.cleaner.service;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.widget.RemoteViews;
import com.kingouser.com.R;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.server.notification.NotificationCompat;
import com.pureapps.cleaner.b.a;
import com.pureapps.cleaner.bean.NotificationInfo;
import com.pureapps.cleaner.manager.f;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.k;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

@SuppressLint({"OverrideAbstract"})
@TargetApi(18)
public class NotificationMonitorService extends NotificationListenerService {

    /* renamed from: a  reason: collision with root package name */
    public static ArrayList<NotificationInfo> f5847a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    public static boolean f5848b = false;

    /* renamed from: c  reason: collision with root package name */
    private IntentFilter f5849c = null;

    /* renamed from: d  reason: collision with root package name */
    private BroadcastReceiver f5850d = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("delete.notification.action")) {
                NotificationMonitorService.this.a();
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public Handler f5851e = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            a.a(20, 0, null);
            if (NotificationMonitorService.f5847a != null && NotificationMonitorService.f5847a.size() > 0) {
                f.a(NotificationMonitorService.this).c();
            } else if (NotificationMonitorService.f5847a == null || NotificationMonitorService.f5847a.size() == 0) {
                f.a(NotificationMonitorService.this).d();
            }
            try {
                System.gc();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    };

    public IBinder onBind(Intent intent) {
        f5848b = true;
        return super.onBind(intent);
    }

    public boolean onUnbind(Intent intent) {
        f5848b = false;
        return super.onUnbind(intent);
    }

    public static void a(Context context) {
        if (b(context) && !f5848b) {
            if (Build.VERSION.SDK_INT >= 24) {
                requestRebind(new ComponentName(context, NotificationMonitorService.class));
                return;
            }
            try {
                PackageManager packageManager = context.getPackageManager();
                packageManager.setComponentEnabledSetting(new ComponentName(context, NotificationMonitorService.class), 2, 1);
                packageManager.setComponentEnabledSetting(new ComponentName(context, NotificationMonitorService.class), 1, 1);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void a(Activity activity) {
        Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
        intent.addFlags(536936448);
        activity.startActivityForResult(intent, FileUtils.FileMode.MODE_ISGID);
    }

    public static boolean b(Context context) {
        String packageName = context.getPackageName();
        String string = Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
        if (TextUtils.isEmpty(string)) {
            return false;
        }
        for (String unflattenFromString : string.split(":")) {
            ComponentName unflattenFromString2 = ComponentName.unflattenFromString(unflattenFromString);
            if (unflattenFromString2 != null && TextUtils.equals(packageName, unflattenFromString2.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    public void onDestroy() {
        super.onDestroy();
        f5847a.clear();
        f.a(this).d();
        unregisterReceiver(this.f5850d);
    }

    public void onCreate() {
        super.onCreate();
        a.a(19, 0, null);
        this.f5849c = new IntentFilter();
        this.f5849c.addAction("delete.notification.action");
        registerReceiver(this.f5850d, this.f5849c);
    }

    public void a(String str, String str2, int i, String str3) {
        if (Build.VERSION.SDK_INT >= 21) {
            cancelNotification(str3);
        } else {
            cancelNotification(str, str2, i);
        }
    }

    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        if (!statusBarNotification.getPackageName().equals(getPackageName()) && !statusBarNotification.getPackageName().equals("com.pureapps.cleaner")) {
            a();
        }
    }

    public void onNotificationRemoved(StatusBarNotification statusBarNotification) {
    }

    public void a() {
        try {
            boolean l = i.a(this).l();
            if (Build.VERSION.SDK_INT >= 18 && b(getApplicationContext()) && l) {
                a(getActiveNotifications());
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static HashSet<String> a(String str) {
        HashSet<String> hashSet = new HashSet<>();
        for (String add : str.split(",")) {
            hashSet.add(add);
        }
        return hashSet;
    }

    public static void a(int i) {
        synchronized (f5847a) {
            try {
                f5847a.remove(i);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(NotificationInfo notificationInfo) {
        synchronized (f5847a) {
            try {
                f5847a.add(notificationInfo);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private void a(final StatusBarNotification[] statusBarNotificationArr) {
        k.a().a(new Runnable() {
            public void run() {
                NotificationInfo a2;
                if (NotificationMonitorService.f5847a == null) {
                    NotificationMonitorService.f5847a = new ArrayList<>();
                }
                HashSet<String> a3 = NotificationMonitorService.a(i.a(NotificationMonitorService.this).m());
                if (statusBarNotificationArr != null) {
                    for (StatusBarNotification statusBarNotification : statusBarNotificationArr) {
                        try {
                            if (!io.fabric.sdk.android.services.common.a.ANDROID_CLIENT_TYPE.equalsIgnoreCase(statusBarNotification.getPackageName()) && statusBarNotification.isClearable() && !statusBarNotification.getPackageName().equalsIgnoreCase(NotificationMonitorService.this.getPackageName()) && statusBarNotification.getPackageName() != null && a3.contains(statusBarNotification.getPackageName()) && (a2 = NotificationMonitorService.this.a(statusBarNotification)) != null) {
                                NotificationMonitorService.this.a(a2);
                                NotificationMonitorService.this.b();
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                }
                try {
                    if (NotificationMonitorService.f5847a != null) {
                        Iterator<NotificationInfo> it = NotificationMonitorService.f5847a.iterator();
                        while (it.hasNext()) {
                            NotificationInfo next = it.next();
                            if (Build.VERSION.SDK_INT < 21) {
                                NotificationMonitorService.this.a(next.d(), next.g(), next.c(), null);
                            } else {
                                NotificationMonitorService.this.a(null, null, 0, next.a());
                            }
                        }
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                NotificationMonitorService.this.f5851e.sendEmptyMessage(0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b() {
        int i = 0;
        while (f5847a != null && i < f5847a.size() - 1) {
            int i2 = i + 1;
            while (true) {
                int i3 = i2;
                if (i3 >= f5847a.size()) {
                    break;
                } else if (f5847a.get(i).c() == f5847a.get(i3).c()) {
                    a(i);
                    i = 0;
                    break;
                } else {
                    i2 = i3 + 1;
                }
            }
            i++;
        }
    }

    @SuppressLint({"NewApi"})
    public NotificationInfo a(StatusBarNotification statusBarNotification) {
        String str;
        String str2;
        String string = statusBarNotification.getNotification().extras.getString(NotificationCompat.EXTRA_TITLE);
        String string2 = statusBarNotification.getNotification().extras.getString(NotificationCompat.EXTRA_TEXT);
        if (string2 == null || string2.length() == 0) {
            string2 = statusBarNotification.getNotification().extras.getString(NotificationCompat.EXTRA_INFO_TEXT);
        }
        if (string == null || string.length() == 0) {
            str = (String) statusBarNotification.getNotification().tickerText;
        } else {
            str = string;
        }
        NotificationInfo notificationInfo = new NotificationInfo();
        if (Build.VERSION.SDK_INT >= 20) {
            notificationInfo.a(statusBarNotification.getKey());
        }
        List<String> a2 = a(statusBarNotification.getNotification());
        if (a2 != null && a2.size() >= 2) {
            if (a2.get(0) != null && a2.get(0).length() > 0) {
                str = a2.get(0);
            }
            if (a2.get(1) != null && a2.get(1).length() > 0) {
                str2 = a2.get(1);
                if (str2 == null || str2.length() == 0) {
                    str2 = getString(R.string.d9);
                }
                notificationInfo.a(statusBarNotification.getId());
                notificationInfo.c(statusBarNotification.getPackageName());
                notificationInfo.a(statusBarNotification.getPostTime());
                notificationInfo.b(str2);
                notificationInfo.d(str);
                notificationInfo.a(statusBarNotification);
                notificationInfo.e(statusBarNotification.getTag());
                notificationInfo.f(DateFormat.format("HH:mm", new Date(statusBarNotification.getPostTime())).toString());
                return notificationInfo;
            }
        }
        str2 = string2;
        str2 = getString(R.string.d9);
        notificationInfo.a(statusBarNotification.getId());
        notificationInfo.c(statusBarNotification.getPackageName());
        notificationInfo.a(statusBarNotification.getPostTime());
        notificationInfo.b(str2);
        notificationInfo.d(str);
        notificationInfo.a(statusBarNotification);
        notificationInfo.e(statusBarNotification.getTag());
        try {
            notificationInfo.f(DateFormat.format("HH:mm", new Date(statusBarNotification.getPostTime())).toString());
            return notificationInfo;
        } catch (Exception e2) {
            return notificationInfo;
        }
    }

    public static List<String> a(Notification notification) {
        ArrayList arrayList;
        RemoteViews remoteViews = notification.contentView;
        if (remoteViews == null) {
            return null;
        }
        ArrayList arrayList2 = new ArrayList();
        try {
            Field declaredField = remoteViews.getClass().getDeclaredField("mActions");
            declaredField.setAccessible(true);
            Iterator it = ((ArrayList) declaredField.get(remoteViews)).iterator();
            while (it.hasNext()) {
                Parcel obtain = Parcel.obtain();
                ((Parcelable) it.next()).writeToParcel(obtain, 0);
                obtain.setDataPosition(0);
                if (obtain.readInt() == 2) {
                    obtain.readInt();
                    String readString = obtain.readString();
                    if (readString != null) {
                        if (readString.equals("setText")) {
                            obtain.readInt();
                            arrayList2.add(((CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(obtain)).toString().trim());
                        } else if (readString.equals("setTime")) {
                            obtain.readInt();
                            arrayList2.add(new SimpleDateFormat("h:mm a").format(new Date(obtain.readLong())));
                        }
                        obtain.recycle();
                    }
                }
            }
            arrayList = arrayList2;
        } catch (Exception e2) {
            e2.printStackTrace();
            arrayList = null;
        }
        return arrayList;
    }
}
