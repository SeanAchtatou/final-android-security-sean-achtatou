package com.pureapps.cleaner.service;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.R;
import com.kingouser.com.application.App;
import com.kingouser.com.util.ResultUtils;
import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.pureapps.cleaner.analytic.a;
import com.pureapps.cleaner.b.c;
import com.pureapps.cleaner.manager.h;
import com.pureapps.cleaner.net.NetworkStatus;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.l;
import java.util.List;

public class BackService extends Service implements c {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f5838a = false;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public AlarmManager f5839b = null;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public PendingIntent f5840c = null;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public boolean f5841d = false;

    /* renamed from: e  reason: collision with root package name */
    private Handler f5842e = new Handler();

    /* renamed from: f  reason: collision with root package name */
    private IntentFilter f5843f;

    /* renamed from: g  reason: collision with root package name */
    private BroadcastReceiver f5844g = new BroadcastReceiver() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.pureapps.cleaner.service.BackService.a(com.pureapps.cleaner.service.BackService, android.content.Context, boolean):void
         arg types: [com.pureapps.cleaner.service.BackService, android.content.Context, int]
         candidates:
          com.pureapps.cleaner.service.BackService.a(int, long, java.lang.Object):void
          com.pureapps.cleaner.b.c.a(int, long, java.lang.Object):void
          com.pureapps.cleaner.service.BackService.a(com.pureapps.cleaner.service.BackService, android.content.Context, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.pureapps.cleaner.service.BackService.a(com.pureapps.cleaner.service.BackService, boolean):boolean
         arg types: [com.pureapps.cleaner.service.BackService, int]
         candidates:
          com.pureapps.cleaner.service.BackService.a(android.content.Context, boolean):void
          com.pureapps.cleaner.service.BackService.a(com.pureapps.cleaner.service.BackService, boolean):boolean */
        public void onReceive(Context context, Intent intent) {
            boolean z = false;
            h.a(context);
            f.b("mReceiver:", intent.getAction());
            String action = intent.getAction();
            if (action.equals("notification_calculator") && i.a(context).r()) {
                a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnNotificationCalculator");
                com.pureapps.cleaner.manager.f.a(BackService.this).h();
            } else if (action.equals("notification_wifi") && i.a(context).r()) {
                a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnNotificationWifi");
                com.pureapps.cleaner.manager.f.b(BackService.this);
                NetworkStatus a2 = NetworkStatus.a();
                if (!NetworkStatus.a().b()) {
                    z = true;
                }
                a2.a(z);
            } else if (action.equals("android.net.wifi.WIFI_STATE_CHANGED") && i.a(context).r()) {
                com.pureapps.cleaner.manager.f.a(context).g();
                BackService.this.a(context, false);
            } else if (action.equals("android.media.RINGER_MODE_CHANGED") && i.a(context).r()) {
                com.pureapps.cleaner.manager.f.a(context).g();
                BackService.this.a(context, false);
            } else if (action.equals("notification_ringer") && i.a(context).r()) {
                a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnNotificationMute");
                BackService.this.a();
            } else if (action.equals("notification_alarm") && i.a(context).r()) {
                com.pureapps.cleaner.manager.f.a(context).g();
            } else if (action.equals("notification_tool_close")) {
                if (!(BackService.this.f5839b == null || BackService.this.f5840c == null)) {
                    BackService.this.f5839b.cancel(BackService.this.f5840c);
                }
                com.pureapps.cleaner.manager.f.a(context).e();
            } else if (action.equals("notification_tool_open")) {
                if (BackService.this.f5839b != null && BackService.this.f5840c != null) {
                    BackService.this.f5839b.setRepeating(0, System.currentTimeMillis() + 120000, 120000, BackService.this.f5840c);
                }
            } else if (action.equals("android.app.action.NOTIFICATION_POLICY_ACCESS_GRANTED_CHANGED") && BackService.this.f5841d) {
                boolean unused = BackService.this.f5841d = false;
                NotificationManager notificationManager = (NotificationManager) BackService.this.getSystemService(ServiceManagerNative.NOTIFICATION);
                if (Build.VERSION.SDK_INT >= 23 && notificationManager.isNotificationPolicyAccessGranted()) {
                    BackService.this.a();
                    Intent intent2 = new Intent("android.intent.action.MAIN");
                    intent2.setFlags(268435456);
                    intent2.addCategory("android.intent.category.HOME");
                    context.startActivity(intent2);
                }
            } else if (action.equals("notification_update_action")) {
                a.a(context).c(FirebaseAnalytics.getInstance(context), "BtnNotificationUpdatePushClick");
                com.pureapps.cleaner.manager.f.b(context);
                com.kingouser.com.a.a(context).a();
            } else {
                if (action.equals("android.intent.action.SCREEN_ON")) {
                }
            }
        }
    };

    public static void a(Context context) {
        context.startService(new Intent(context, BackService.class));
    }

    public static boolean b(Context context) {
        List<ActivityManager.RunningServiceInfo> runningServices = ((ActivityManager) context.getSystemService(ServiceManagerNative.ACTIVITY)).getRunningServices(200);
        if (runningServices.size() <= 0) {
            return false;
        }
        for (ActivityManager.RunningServiceInfo runningServiceInfo : runningServices) {
            if (runningServiceInfo.service.getClassName().equals("com.pureapps.cleaner.service.BackService")) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void a(Context context, boolean z) {
        try {
            a.a(this).c(FirebaseAnalytics.getInstance(this), "BtBackServicesUpdateClick");
            com.kingouser.com.a.a(this).a(this, z);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        this.f5841d = false;
        NotificationManager notificationManager = (NotificationManager) getSystemService(ServiceManagerNative.NOTIFICATION);
        if (Build.VERSION.SDK_INT < 24) {
            AudioManager audioManager = (AudioManager) getSystemService("audio");
            try {
                if (audioManager.getRingerMode() == 0) {
                    audioManager.setRingerMode(2);
                } else {
                    audioManager.setRingerMode(0);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (!notificationManager.isNotificationPolicyAccessGranted()) {
            com.pureapps.cleaner.manager.f.b(this);
            Toast.makeText(this, (int) R.string.da, 0).show();
            startActivity(new Intent("android.settings.NOTIFICATION_POLICY_ACCESS_SETTINGS"));
            this.f5841d = true;
        } else if (notificationManager.getCurrentInterruptionFilter() == 1) {
            notificationManager.setInterruptionFilter(3);
        } else {
            notificationManager.setInterruptionFilter(1);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        com.pureapps.cleaner.b.a.a(this);
        this.f5839b = (AlarmManager) getSystemService("alarm");
        this.f5840c = PendingIntent.getBroadcast(this, 0, new Intent("notification_alarm"), 134217728);
        this.f5839b.setRepeating(0, System.currentTimeMillis() + 120000, 120000, this.f5840c);
        this.f5843f = new IntentFilter();
        this.f5843f.addAction("notification_tool_open");
        this.f5843f.addAction("notification_tool_close");
        this.f5843f.addAction("notification_alarm");
        this.f5843f.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        this.f5843f.addAction("notification_wifi");
        this.f5843f.addAction("android.media.RINGER_MODE_CHANGED");
        this.f5843f.addAction("notification_ringer");
        this.f5843f.addAction("notification_calculator");
        this.f5843f.addAction("notification_update_action");
        this.f5843f.addAction("kingoroot.supersu.cleaner.ACTION_2HOUR_ALARM_GIFT");
        this.f5843f.addAction("android.intent.action.SCREEN_ON");
        if (Build.VERSION.SDK_INT >= 23) {
            this.f5843f.addAction("android.app.action.NOTIFICATION_POLICY_ACCESS_GRANTED_CHANGED");
        }
        registerReceiver(this.f5844g, this.f5843f);
        CommonService.a(this, "start");
        com.pureapps.cleaner.manager.f.a(this).i();
        b();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    private void b() {
        if (ShellUtils.checkSuVerison() && l.c(this) > i.a(this).g() && ResultUtils.parseUpdate(i.a(App.a()).o()) != null) {
            this.f5842e.postDelayed(new Runnable() {
                public void run() {
                    com.pureapps.cleaner.manager.f.a(BackService.this).a();
                }
            }, 5000);
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return 1;
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.f5844g);
        this.f5839b.cancel(this.f5840c);
        com.pureapps.cleaner.b.a.b(this);
    }

    public void a(int i, long j, Object obj) {
        switch (i) {
            case 1:
                if (((Boolean) obj).booleanValue() && !ShellUtils.checkSuVerison() && j == 0) {
                    com.pureapps.cleaner.manager.f.a(this).b();
                    return;
                }
                return;
            case 3:
                if (!(this.f5839b == null || this.f5840c == null)) {
                    this.f5839b.cancel(this.f5840c);
                }
                com.pureapps.cleaner.manager.f.a(this).e();
                return;
            case 23:
                com.pureapps.cleaner.manager.f.a(this).g();
                return;
            default:
                return;
        }
    }
}
