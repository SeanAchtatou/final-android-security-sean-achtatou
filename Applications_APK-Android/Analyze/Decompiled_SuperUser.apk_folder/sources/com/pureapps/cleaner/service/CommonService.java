package com.pureapps.cleaner.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.pureapps.cleaner.manager.h;
import com.pureapps.cleaner.net.NetworkStatus;
import com.pureapps.cleaner.net.a;
import com.pureapps.cleaner.util.i;
import java.util.ArrayList;

public class CommonService extends IntentService {
    public static void a(Context context, String str) {
        a(context, str, (Bundle) null);
    }

    public static void a(Context context, String str, Bundle bundle) {
        context.startService(b(context, str, bundle));
    }

    public static Intent b(Context context, String str, Bundle bundle) {
        Intent intent = new Intent(context, CommonService.class);
        intent.putExtra("action", str);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        return intent;
    }

    public CommonService() {
        super("CommonService");
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        ArrayList<a.C0091a> b2;
        if (intent != null) {
            String stringExtra = intent.getStringExtra("action");
            Log.i("CommonService", "action=" + stringExtra);
            if ("bootComplete".equals(stringExtra)) {
                a(8640000, "kingoroot.supersu.cleaner.ACTION_TEN_MINUTES_ALARM", System.currentTimeMillis() + 8640000);
                a(8640000, "kingoroot.supersu.cleaner.ACTION_CPU_TEMP_CHECK", System.currentTimeMillis() + 8640000);
                a(43200000, "kingoroot.supersu.cleaner.ACTION_12HOUR_ALARM", 5000);
            } else if ("connectivity".equals(stringExtra)) {
                if (NetworkStatus.a().c() && (b2 = a.a().b()) != null) {
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 < b2.size()) {
                            b2.get(i2).a();
                            i = i2 + 1;
                        } else {
                            return;
                        }
                    }
                }
            } else if ("start".equals(stringExtra)) {
                a(8640000, "kingoroot.supersu.cleaner.ACTION_TEN_MINUTES_ALARM", System.currentTimeMillis() + 8640000);
                a(8640000, "kingoroot.supersu.cleaner.ACTION_CPU_TEMP_CHECK", System.currentTimeMillis() + 8640000);
                a(43200000, "kingoroot.supersu.cleaner.ACTION_12HOUR_ALARM", 5000);
            } else if ("kingoroot.supersu.cleaner.ACTION_TEN_MINUTES_ALARM".equals(stringExtra)) {
            } else {
                if ("kingoroot.supersu.cleaner.ACTION_CPU_TEMP_CHECK".equals(stringExtra)) {
                    h.a(getApplicationContext());
                } else if ("kingoroot.supersu.cleaner.ACTION_12HOUR_ALARM".equals(stringExtra)) {
                    a();
                } else if ("pkgAdded".equals(stringExtra)) {
                    intent.getExtras().getString("packageName");
                } else if ("pkgUpdate".equals(stringExtra)) {
                    intent.getExtras().getString("packageName");
                } else if ("pkgDel".equals(stringExtra)) {
                    intent.getExtras().getString("packageName");
                }
            }
        }
    }

    private void a() {
        if (System.currentTimeMillis() - i.a(this).b() > 43200000) {
            h.b(this);
        }
        com.pureapps.cleaner.analytic.a.a(this).c(FirebaseAnalytics.getInstance(this), "BtCommonServicesUpdateClick");
        com.kingouser.com.a.a(this).c(this);
    }

    private void a(long j, String str, long j2) {
        AlarmManager alarmManager = (AlarmManager) getSystemService("alarm");
        PendingIntent broadcast = PendingIntent.getBroadcast(this, (int) (System.currentTimeMillis() % 1000), new Intent(str), 134217728);
        try {
            alarmManager.cancel(broadcast);
            alarmManager.setRepeating(0, j2, j, broadcast);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
