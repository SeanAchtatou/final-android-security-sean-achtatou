package com.pureapps.cleaner;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SwitchCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.OnClick;
import com.kingouser.com.BaseActivity;
import com.kingouser.com.R;
import com.pureapps.cleaner.analytic.a;
import com.pureapps.cleaner.manager.f;
import com.pureapps.cleaner.service.BackService;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.view.NotificationMemoryView;

public class NotificationThemeActivity extends BaseActivity {
    @BindView(R.id.e6)
    CheckBox mBlackSwitch = null;
    @BindView(R.id.e3)
    SwitchCompat mSwitch = null;
    @BindView(R.id.e4)
    LinearLayout mThemeLayout = null;
    @BindView(R.id.e8)
    CheckBox mWhileSwitch = null;
    Handler n = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            f.a(NotificationThemeActivity.this).g();
        }
    };

    public static void a(Context context) {
        context.startActivity(new Intent(context, NotificationThemeActivity.class));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.aa);
        ActionBar f2 = f();
        if (f2 != null) {
            f2.d(true);
            f2.a(true);
        }
        j();
        this.mSwitch.setChecked(i.a(this).r());
        this.mThemeLayout.setAlpha(i.a(this).r() ? 1.0f : 0.5f);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.fk);
        int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.fk);
        NotificationMemoryView notificationMemoryView = new NotificationMemoryView(this);
        notificationMemoryView.measure(dimensionPixelSize, dimensionPixelSize2);
        notificationMemoryView.layout(0, 0, dimensionPixelSize, dimensionPixelSize2);
        notificationMemoryView.setDrawingCacheEnabled(true);
        notificationMemoryView.setPercent(60);
        ((ImageView) findViewById(R.id.e5).findViewById(R.id.kg)).setImageBitmap(notificationMemoryView.getDrawingCache());
        ((ImageView) findViewById(R.id.e7).findViewById(R.id.kg)).setImageBitmap(notificationMemoryView.getDrawingCache());
    }

    private void j() {
        switch (i.a(this).k()) {
            case 1:
                this.mBlackSwitch.setChecked(true);
                this.mWhileSwitch.setChecked(false);
                return;
            case 2:
                this.mBlackSwitch.setChecked(false);
                this.mWhileSwitch.setChecked(true);
                return;
            default:
                return;
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a.a(this).b(this.o, "NitificationThemeView");
    }

    @OnClick({2131624112, 2131624115, 2131624117})
    public void onClick(View view) {
        float f2;
        boolean z = true;
        switch (view.getId()) {
            case R.id.e2 /*2131624112*/:
                boolean r = i.a(this).r();
                this.mSwitch.setChecked(!r);
                i a2 = i.a(this);
                if (r) {
                    z = false;
                }
                a2.e(z);
                if (!i.a(this).r()) {
                    com.pureapps.cleaner.b.a.a(3, 0, null);
                } else if (!BackService.b(this)) {
                    BackService.a(this);
                } else {
                    this.n.sendEmptyMessageDelayed(0, 500);
                    com.pureapps.cleaner.b.a.a(2, 0, null);
                }
                a.a(this).a(this.o, i.a(this).r());
                LinearLayout linearLayout = this.mThemeLayout;
                if (i.a(this).r()) {
                    f2 = 1.0f;
                } else {
                    f2 = 0.5f;
                }
                linearLayout.setAlpha(f2);
                return;
            case R.id.e3 /*2131624113*/:
            case R.id.e4 /*2131624114*/:
            case R.id.e6 /*2131624116*/:
            default:
                return;
            case R.id.e5 /*2131624115*/:
                if (i.a(this).r()) {
                    i.a(this).b(1);
                    j();
                    com.pureapps.cleaner.b.a.a(23, 0, null);
                    return;
                }
                return;
            case R.id.e7 /*2131624117*/:
                if (i.a(this).r()) {
                    i.a(this).b(2);
                    j();
                    com.pureapps.cleaner.b.a.a(23, 0, null);
                    return;
                }
                return;
        }
    }
}
