package com.pureapps.cleaner;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.format.Formatter;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.kingouser.com.R;
import com.kingouser.com.application.App;
import com.muzakki.ahmad.widget.CollapsingToolbarLayout;
import com.pureapps.cleaner.adapter.JunkListAdapter;
import com.pureapps.cleaner.b.c;
import com.pureapps.cleaner.bean.g;
import com.pureapps.cleaner.bean.h;
import com.pureapps.cleaner.manager.e;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.l;
import com.pureapps.cleaner.view.AnimatedExpandableListView;
import com.pureapps.cleaner.view.BBaseActivity;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class JunkClearActivity extends BBaseActivity implements ExpandableListView.OnChildClickListener, ExpandableListView.OnGroupClickListener, c {
    @BindView(R.id.d_)
    Button mBtClear;
    @BindView(R.id.d8)
    AnimatedExpandableListView mListView;
    @BindView(R.id.d6)
    ProgressBar mProgressBar;
    @BindView(R.id.d3)
    ViewGroup mRootView;
    @BindView(R.id.d5)
    CollapsingToolbarLayout mToolBarLayout;
    @BindView(R.id.d7)
    TextView mTvSelectedInfo;
    e.C0090e n = new e.C0090e() {
        public void a() {
            if (!JunkClearActivity.this.isFinishing() && !JunkClearActivity.this.u) {
                JunkClearActivity.this.b(true);
            }
        }

        public void a(Integer num) {
            boolean z = true;
            if (!JunkClearActivity.this.isFinishing() && !JunkClearActivity.this.u) {
                if (num.intValue() > 0) {
                    JunkClearActivity.this.b(true);
                    JunkClearActivity.this.mBtClear.setVisibility(0);
                }
                JunkClearActivity junkClearActivity = JunkClearActivity.this;
                if (num.intValue() <= 0) {
                    z = false;
                }
                junkClearActivity.c(z);
            }
        }
    };
    private a o;
    /* access modifiers changed from: private */
    public JunkListAdapter p;
    /* access modifiers changed from: private */
    public boolean q = false;
    private boolean r = false;
    private boolean s = false;
    /* access modifiers changed from: private */
    public boolean u = false;
    /* access modifiers changed from: private */
    public int v = -1;
    private ProgressDialog w = null;
    private Handler x = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 24:
                    if (!JunkClearActivity.this.isFinishing() && !JunkClearActivity.this.u) {
                        Object[] objArr = (Object[]) message.obj;
                        String str = (String) objArr[0];
                        if (str != null && str.length() > 0) {
                            JunkClearActivity.this.a(message.arg1, str);
                        }
                        JunkClearActivity.this.p.a((h) objArr[1], (h) objArr[2], (h) objArr[3]);
                        JunkClearActivity.this.b(false);
                        return;
                    }
                    return;
                case 32:
                    if (!JunkClearActivity.this.isFinishing() && !JunkClearActivity.this.u) {
                        Object[] objArr2 = (Object[]) message.obj;
                        JunkClearActivity.this.p.a((h) objArr2[1], (h) objArr2[2], (h) objArr2[3]);
                        JunkClearActivity.this.b(false);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.u = false;
        setContentView((int) R.layout.a5);
        Toolbar toolbar = (Toolbar) findViewById(R.id.cu);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.d9);
        if (Build.VERSION.SDK_INT >= 19) {
            relativeLayout.setPadding(0, m(), 0, 0);
        }
        a(toolbar);
        f().a(true);
        this.o = new a();
        this.mToolBarLayout.setCollapsedTitleGravity(1);
        this.p = new JunkListAdapter(this, this.mListView);
        this.mListView.setAdapter(this.p);
        this.mListView.setOnChildClickListener(this);
        this.mListView.setOnGroupClickListener(this);
        b(true);
        o();
        com.pureapps.cleaner.b.a.a(this);
        if (l.a() >= 23 && getPackageManager().checkPermission("android.permission.CLEAR_APP_CACHE", getPackageName()) != 0) {
            ActivityCompat.a(this, new String[]{"android.permission.CLEAR_APP_CACHE"}, 2);
        }
        j();
        int groupCount = this.p.getGroupCount();
        for (int i = 0; i < groupCount; i++) {
            this.mListView.expandGroup(i);
        }
    }

    private void j() {
        this.mBtClear.post(new Runnable() {
            public void run() {
                int unused = JunkClearActivity.this.v = JunkClearActivity.this.mBtClear.getHeight();
                JunkClearActivity.this.k();
            }
        });
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.mListView != null && this.v != -1) {
            this.mListView.setPadding(0, 0, 0, (int) (((float) this.v) * 1.0f));
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.pureapps.cleaner.analytic.a.a(this).b(this.t, "JunkClean");
    }

    public void onBackPressed() {
        super.onBackPressed();
        this.u = true;
        l();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.pureapps.cleaner.b.a.b(this);
        l();
    }

    private void l() {
        if (this.p != null) {
            this.p.e();
        }
        this.p = null;
        if (this.mListView != null) {
            this.mListView.setOnChildClickListener(null);
            this.mListView.setOnGroupClickListener(null);
            this.mListView = null;
        }
        this.n = null;
        e.a().b();
        e.a().c();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.f8343c, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 16908332) {
            this.u = true;
            l();
            finish();
            return true;
        } else if (itemId != R.id.mx) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            IgnoreListActivity.a(this);
            return true;
        }
    }

    @OnClick({2131624083})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.d_ /*2131624083*/:
                if (!this.q) {
                    com.pureapps.cleaner.manager.a.a().a(6);
                    if (this.w != null && this.w.isShowing()) {
                        this.w.dismiss();
                    }
                    this.w = new ProgressDialog(this);
                    this.w.setCancelable(true);
                    this.w.setCanceledOnTouchOutside(false);
                    String string = getString(R.string.bh);
                    SpannableString spannableString = new SpannableString(string);
                    spannableString.setSpan(new ForegroundColorSpan(-16777216), 0, string.length(), 17);
                    spannableString.setSpan(new StyleSpan(1), 0, string.length(), 17);
                    this.w.setMessage(spannableString);
                    this.w.show();
                    this.o.postDelayed(new Runnable() {
                        public void run() {
                            JunkClearActivity.this.n();
                        }
                    }, 1500);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.adapter.JunkListAdapter.a(int, int):com.pureapps.cleaner.bean.g
     arg types: [int, int]
     candidates:
      com.pureapps.cleaner.view.AnimatedExpandableListView.a.a(int, int):void
      com.pureapps.cleaner.view.AnimatedExpandableListView.a.a(com.pureapps.cleaner.view.AnimatedExpandableListView$a, int):void
      com.pureapps.cleaner.view.AnimatedExpandableListView.a.a(com.pureapps.cleaner.view.AnimatedExpandableListView$a, com.pureapps.cleaner.view.AnimatedExpandableListView):void
      com.pureapps.cleaner.adapter.JunkListAdapter.a(int, int):com.pureapps.cleaner.bean.g */
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        h a2 = this.p.getGroup(i);
        g a3 = this.p.getChild(i, i2);
        if (!(a2 == null || a3 == null)) {
            if (a3.f5631g) {
                a3.f5631g = false;
                a2.f5637f--;
                a2.f5635d -= a3.f5629e;
            } else {
                a3.f5631g = true;
                a2.f5637f++;
                a2.f5635d += a3.f5629e;
            }
            this.p.notifyDataSetChanged();
            b(true);
        }
        return true;
    }

    public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long j) {
        return true;
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (i == 1) {
            if (iArr[0] == 0) {
                c(this.r);
            } else {
                App.a().a((int) R.string.c8);
            }
        } else if (i == 2) {
            if (iArr.length <= 0 || iArr[0] == 0) {
            }
        } else if (i != 3) {
            super.onRequestPermissionsResult(i, strArr, iArr);
        } else if (iArr[0] == 0) {
            o();
        } else {
            App.a().a((int) R.string.c8);
            finish();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, String str) {
        this.mProgressBar.setProgress(i);
        this.mTvSelectedInfo.setText(new File(str).getName());
        b(false);
    }

    public void b(boolean z) {
        long j;
        String substring;
        String str;
        long j2 = 0;
        if (this.p != null) {
            ArrayList<h> d2 = this.p.d();
            if (d2 == null || d2.size() <= 0) {
                j = 0;
            } else {
                Iterator<h> it = d2.iterator();
                long j3 = 0;
                j = 0;
                while (it.hasNext()) {
                    h next = it.next();
                    j += next.f5634c;
                    j3 = next.f5635d + j3;
                }
                j2 = j3;
            }
            String b2 = l.b(j);
            String formatFileSize = Formatter.formatFileSize(this, j2);
            if (j >= 1099511627776L) {
                substring = b2.substring(0, b2.length() - 2);
                str = "TB";
            } else if (j >= 1073741824) {
                substring = b2.substring(0, b2.length() - 2);
                str = "GB";
            } else if (j >= 1048576) {
                substring = b2.substring(0, b2.length() - 2);
                str = "MB";
            } else if (j >= 1024) {
                substring = b2.substring(0, b2.length() - 2);
                str = "KB";
            } else {
                substring = b2.substring(0, b2.length() - 1);
                str = "B";
            }
            this.mToolBarLayout.setSubtitle(l.a(substring));
            this.mToolBarLayout.setSubTitleUnit(str);
            this.mToolBarLayout.setMessageTitle("    " + getString(R.string.cc));
            if (z) {
                this.mTvSelectedInfo.setText(getString(R.string.e2, new Object[]{formatFileSize}));
            }
            if (z) {
                a(j, formatFileSize);
            }
        }
    }

    private void a(long j, String str) {
        f.a("refreshProgressCleanButton" + j);
        if (j <= 0 || str.startsWith("0.00")) {
            this.mBtClear.setEnabled(false);
            this.mBtClear.setSelected(false);
            this.mBtClear.setTextColor(getResources().getColor(R.color.a_));
            this.mBtClear.setText(getResources().getString(R.string.ar));
            return;
        }
        this.mBtClear.setSelected(true);
        this.mBtClear.setEnabled(true);
        this.mBtClear.setTextColor(getResources().getColor(R.color.a_));
        this.mBtClear.setText(getResources().getString(R.string.ar) + " " + str);
    }

    /* access modifiers changed from: private */
    public void n() {
        e.a().d();
        this.q = true;
        e.a().a(this, this.p);
        com.pureapps.cleaner.analytic.a.a(this).c(this.t, "BtnJunkClean");
    }

    private void o() {
        boolean z;
        PackageManager packageManager = getPackageManager();
        if (l.a() >= 23) {
            z = packageManager.checkPermission("android.permission.READ_EXTERNAL_STORAGE", getPackageName()) == 0;
            if (!z) {
                ActivityCompat.a(this, new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 3);
            }
        } else {
            z = true;
        }
        if (z) {
            e.a().a(this, this.p, this.n);
        }
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        boolean z2;
        String[] strArr;
        if (l.a() >= 23) {
            PackageManager packageManager = getPackageManager();
            boolean z3 = packageManager.checkPermission("android.permission.READ_EXTERNAL_STORAGE", getPackageName()) == 0;
            if (packageManager.checkPermission("android.permission.GET_PACKAGE_SIZE", getPackageName()) == 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (!z3 || !z2) {
                String[] strArr2 = {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.GET_PACKAGE_SIZE"};
                if (!z3 || z2) {
                    strArr = (z3 || !z2) ? strArr2 : new String[]{"android.permission.READ_EXTERNAL_STORAGE"};
                } else {
                    strArr = new String[]{"android.permission.GET_PACKAGE_SIZE"};
                }
                ActivityCompat.a(this, strArr, 1);
                return;
            }
        }
        e.a().a(getApplicationContext(), z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.util.i.a(long, boolean):void
     arg types: [long, int]
     candidates:
      com.pureapps.cleaner.util.i.a(java.lang.String, int):void
      com.pureapps.cleaner.util.i.a(java.lang.String, long):void
      com.pureapps.cleaner.util.i.a(java.lang.String, java.lang.String):void
      com.pureapps.cleaner.util.i.a(java.lang.String, boolean):void
      com.pureapps.cleaner.util.i.a(long, boolean):void */
    public void a(int i, final long j, Object obj) {
        switch (i) {
            case 16:
                int intValue = ((Integer) obj).intValue();
                if (this.w != null && this.w.isShowing()) {
                    this.w.dismiss();
                }
                this.q = false;
                i.a(getApplicationContext()).a(j, true);
                this.p.c();
                int lastVisiblePosition = this.mListView.getLastVisiblePosition() - this.mListView.getFirstVisiblePosition();
                if (intValue <= lastVisiblePosition) {
                    lastVisiblePosition = intValue;
                }
                this.o.postDelayed(new Runnable() {
                    public void run() {
                        BoostResultActivity.a(JunkClearActivity.this, 1, String.valueOf(j));
                        boolean unused = JunkClearActivity.this.q = false;
                        JunkClearActivity.this.finish();
                    }
                }, (long) ((lastVisiblePosition * 100) + 400));
                return;
            case 24:
                Message obtainMessage = this.x.obtainMessage();
                obtainMessage.what = i;
                obtainMessage.arg1 = (int) j;
                obtainMessage.obj = obj;
                this.x.sendMessage(obtainMessage);
                return;
            case 25:
                if (!isFinishing() && !this.u) {
                    this.mBtClear.setVisibility(0);
                    this.mProgressBar.setProgress(0);
                    b(true);
                    this.p.a(false);
                    return;
                }
                return;
            case 32:
                Message obtainMessage2 = this.x.obtainMessage();
                obtainMessage2.what = i;
                obtainMessage2.arg1 = (int) j;
                obtainMessage2.obj = obj;
                this.x.sendMessage(obtainMessage2);
                return;
            default:
                return;
        }
    }

    class a extends Handler {
        a() {
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
        }
    }
}
