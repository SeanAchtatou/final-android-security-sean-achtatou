package com.pureapps.cleaner;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import butterknife.BindView;
import butterknife.OnClick;
import com.kingouser.com.BaseActivity;
import com.kingouser.com.R;
import com.lody.virtual.helper.utils.FileUtils;
import com.pureapps.cleaner.adapter.IgnoreListAdapter;
import com.pureapps.cleaner.util.f;
import com.pureapps.cleaner.util.k;
import java.util.ArrayList;
import java.util.Iterator;
import me.everything.android.ui.overscroll.g;

public class IgnoreListActivity extends BaseActivity {
    @BindView(R.id.fq)
    Button mBtnRemove;
    @BindView(R.id.d8)
    RecyclerView mListView;
    private b n;
    /* access modifiers changed from: private */
    public IgnoreListAdapter p;
    private a q;
    private c r;

    public static void a(Context context) {
        context.startActivity(new Intent(context, IgnoreListActivity.class));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.ar);
        ActionBar f2 = f();
        if (f2 != null) {
            f2.d(true);
            f2.a(true);
        }
        this.n = new b();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.b(1);
        this.mListView.setLayoutManager(linearLayoutManager);
        this.mListView.setHasFixedSize(true);
        g.a(this.mListView, 0);
        this.p = new IgnoreListAdapter(this);
        this.mListView.setAdapter(this.p);
        m();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.pureapps.cleaner.analytic.a.a(this).b(this.o, "IgnoreList");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        k.a(this.q, true);
        k.a(this.r, true);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.f8344d, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 16908332) {
            com.pureapps.cleaner.analytic.a.a(this).c(this.o, "BtnIgnoreBackClick");
            finish();
            return true;
        } else if (itemId != R.id.mx) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            com.pureapps.cleaner.analytic.a.a(this).c(this.o, "BtnIgnoreGotoListAddClick");
            startActivityForResult(new Intent(this, IgnoreListAddActivity.class), 100);
            return true;
        }
    }

    @OnClick({2131624174})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fq /*2131624174*/:
                l();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 100 && i2 == -1) {
            m();
        }
        super.onActivityResult(i, i2, intent);
    }

    public void a(CheckBox checkBox, int i) {
        com.pureapps.cleaner.bean.a b2 = this.p.b(i);
        b2.f5611f = !b2.f5611f;
        checkBox.setChecked(b2.f5611f);
        j();
    }

    /* access modifiers changed from: private */
    public void j() {
        if (k() > 0) {
            this.mBtnRemove.setSelected(true);
            this.mBtnRemove.setEnabled(true);
            this.mBtnRemove.setTextColor(getResources().getColor(R.color.a_));
            return;
        }
        this.mBtnRemove.setEnabled(false);
        this.mBtnRemove.setSelected(false);
        this.mBtnRemove.setTextColor(getResources().getColor(R.color.a_));
    }

    private int k() {
        int i = 0;
        Iterator<com.pureapps.cleaner.bean.a> it = this.p.a().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            if (it.next().f5611f) {
                i = i2 + 1;
            } else {
                i = i2;
            }
        }
    }

    private void l() {
        com.pureapps.cleaner.analytic.a.a(this).c(this.o, "BtnIgnoreListRemoveClick");
        k.a(this.r, true);
        this.r = new c();
        this.r.executeOnExecutor(k.a().b(), new String[0]);
    }

    class c extends AsyncTask<String, Integer, ArrayList<String>> {
        c() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ArrayList<String> doInBackground(String... strArr) {
            ArrayList<com.pureapps.cleaner.bean.a> a2 = IgnoreListActivity.this.p.a();
            ArrayList arrayList = new ArrayList();
            ArrayList<String> arrayList2 = new ArrayList<>();
            int i = 0;
            for (int i2 = 0; i2 < a2.size(); i2++) {
                if (a2.get(i2).f5611f) {
                    arrayList.add(a2.get(i2));
                    publishProgress(Integer.valueOf(i2 - i));
                    i++;
                } else {
                    arrayList2.add(a2.get(i2).f5608c);
                }
            }
            com.pureapps.cleaner.bean.a.b(IgnoreListActivity.this, arrayList);
            return arrayList2;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(Integer... numArr) {
            super.onProgressUpdate(numArr);
            IgnoreListActivity.this.p.a(numArr[0].intValue());
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(ArrayList<String> arrayList) {
            super.onPostExecute(arrayList);
            IgnoreListActivity.this.j();
        }
    }

    private void m() {
        k.a(this.q, true);
        this.q = new a();
        this.q.executeOnExecutor(k.a().b(), new String[0]);
    }

    class a extends AsyncTask<String, String, ArrayList<com.pureapps.cleaner.bean.a>> {
        a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ArrayList<com.pureapps.cleaner.bean.a> doInBackground(String... strArr) {
            PackageManager packageManager = IgnoreListActivity.this.getPackageManager();
            ArrayList<com.pureapps.cleaner.bean.a> a2 = com.pureapps.cleaner.bean.a.a(IgnoreListActivity.this);
            ArrayList<com.pureapps.cleaner.bean.a> arrayList = new ArrayList<>();
            f.a("LoadListTask getAll:" + a2.size());
            Iterator<com.pureapps.cleaner.bean.a> it = a2.iterator();
            while (it.hasNext()) {
                com.pureapps.cleaner.bean.a next = it.next();
                try {
                    ApplicationInfo applicationInfo = packageManager.getApplicationInfo(next.f5608c, FileUtils.FileMode.MODE_IWUSR);
                    if (applicationInfo != null) {
                        next.f5610e = applicationInfo.loadLabel(packageManager).toString();
                        next.f5609d = applicationInfo.loadIcon(packageManager);
                        arrayList.add(next);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            return arrayList;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(ArrayList<com.pureapps.cleaner.bean.a> arrayList) {
            super.onPostExecute(arrayList);
            IgnoreListActivity.this.p.a(arrayList);
        }
    }

    class b extends Handler {
        b() {
        }

        public void handleMessage(Message message) {
            int i = message.what;
        }
    }
}
