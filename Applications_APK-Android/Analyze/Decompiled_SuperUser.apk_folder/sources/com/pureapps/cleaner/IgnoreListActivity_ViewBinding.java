package com.pureapps.cleaner;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;

public class IgnoreListActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private IgnoreListActivity f5378a;

    /* renamed from: b  reason: collision with root package name */
    private View f5379b;

    public IgnoreListActivity_ViewBinding(final IgnoreListActivity ignoreListActivity, View view) {
        this.f5378a = ignoreListActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.fq, "field 'mBtnRemove' and method 'onClick'");
        ignoreListActivity.mBtnRemove = (Button) Utils.castView(findRequiredView, R.id.fq, "field 'mBtnRemove'", Button.class);
        this.f5379b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                ignoreListActivity.onClick(view);
            }
        });
        ignoreListActivity.mListView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.d8, "field 'mListView'", RecyclerView.class);
    }

    public void unbind() {
        IgnoreListActivity ignoreListActivity = this.f5378a;
        if (ignoreListActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f5378a = null;
        ignoreListActivity.mBtnRemove = null;
        ignoreListActivity.mListView = null;
        this.f5379b.setOnClickListener(null);
        this.f5379b = null;
    }
}
