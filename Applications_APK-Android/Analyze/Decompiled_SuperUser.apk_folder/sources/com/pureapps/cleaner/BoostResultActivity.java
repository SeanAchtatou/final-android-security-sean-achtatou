package com.pureapps.cleaner;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.text.format.Formatter;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.BaseActivity;
import com.kingouser.com.R;
import com.pureapps.cleaner.analytic.a;
import com.pureapps.cleaner.manager.h;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.l;

public class BoostResultActivity extends BaseActivity {
    @BindView(R.id.cx)
    LinearLayout mLayoutAdContainer;
    @BindView(R.id.d0)
    TextView mResultDesText;
    @BindView(R.id.cy)
    TextView mResultTitleText;
    @BindView(R.id.cz)
    TextView mResultUnitText;
    private int n;
    private String p = "_memory";
    private final int q = 1;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, int i, String str) {
        boolean z;
        if (a(context)) {
            Intent intent = new Intent(context, BoostResultActivity.class);
            intent.putExtra("type", i);
            intent.putExtra(FirebaseAnalytics.Param.VALUE, str);
            intent.putExtra("CanShowSwipe", true);
            context.startActivity(intent);
            return;
        }
        if (h.a(h.f5804a)) {
            z = h.a(h.a(i, i.a(context).t(), str, false, true, i.a(context).f()));
        } else {
            z = false;
        }
        if (!z) {
            Intent intent2 = new Intent(context, BoostResultActivity.class);
            intent2.putExtra("type", i);
            intent2.putExtra("CanShowSwipe", false);
            intent2.putExtra(FirebaseAnalytics.Param.VALUE, str);
            context.startActivity(intent2);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String string;
        int i = 1;
        super.onCreate(bundle);
        setContentView((int) R.layout.a3);
        ActionBar f2 = f();
        if (f2 != null) {
            f2.d(true);
            f2.a(true);
        }
        this.n = getIntent().getIntExtra("type", 0);
        String stringExtra = getIntent().getStringExtra(FirebaseAnalytics.Param.VALUE);
        try {
            Long.parseLong(stringExtra);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        Typeface createFromAsset = Typeface.createFromAsset(getAssets(), "fonts/booster_number_font.otf");
        this.mResultTitleText.setTypeface(createFromAsset);
        this.mResultUnitText.setTypeface(createFromAsset);
        if (this.n == 2) {
            this.p = "_cool";
            f().a(getString(R.string.en));
            int intValue = Integer.valueOf(stringExtra).intValue();
            if (intValue >= 1) {
                i = intValue;
            }
            String string2 = getResources().getString(R.string.bj);
            if (i.a(this).t() == 0) {
                string = getString(R.string.ee);
            } else {
                string = getString(R.string.ef);
            }
            this.mResultTitleText.setText(String.valueOf(i));
            this.mResultUnitText.setText(string);
            this.mResultDesText.setText(string2);
        } else if (this.n == 1) {
            this.p = "_junk";
            f().a(getString(R.string.ev));
            a(Long.parseLong(stringExtra), getResources().getString(R.string.b8));
        } else if (this.n == 0) {
            this.p = "_memory";
            f().a(getString(R.string.er));
            a(Long.parseLong(stringExtra), getResources().getString(R.string.b5));
        } else if (this.n == 4) {
            this.p = "_notification";
            f().a(getString(R.string.d6));
            j();
        }
        k();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void j() {
        this.mResultTitleText.setVisibility(8);
        this.mResultDesText.setVisibility(8);
        this.mResultUnitText.setText(getString(R.string.d3));
    }

    private void a(long j, String str) {
        String substring;
        String str2;
        String formatFileSize = Formatter.formatFileSize(this, j);
        if (j > 1099511627776L) {
            substring = formatFileSize.substring(0, formatFileSize.length() - 2);
            str2 = "TB";
        } else if (j > 1073741824) {
            substring = formatFileSize.substring(0, formatFileSize.length() - 2);
            str2 = "GB";
        } else if (j > 1048576) {
            substring = formatFileSize.substring(0, formatFileSize.length() - 2);
            str2 = "MB";
        } else if (j > 1024) {
            substring = formatFileSize.substring(0, formatFileSize.length() - 2);
            str2 = "KB";
        } else {
            substring = formatFileSize.substring(0, formatFileSize.length() - 1);
            str2 = "B";
        }
        this.mResultTitleText.setText(l.a(substring));
        this.mResultUnitText.setText(str2);
        this.mResultDesText.setText(str);
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a.a(this).b(this.o, "Result" + this.p);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.pureapps.cleaner.manager.a.a().c();
    }

    private static boolean a(Context context) {
        if (!h.c(context) || i.a(context).h() || l.a(i.a(context).d())) {
            return false;
        }
        i.a(context).e();
        return true;
    }

    private void k() {
        this.mLayoutAdContainer.removeAllViews();
        if (getIntent().getBooleanExtra("CanShowSwipe", false)) {
            a.a(this).b(this.o, "SwipeGuideOpenView");
            this.mLayoutAdContainer.addView(com.pureapps.cleaner.manager.a.a().a(this));
            this.mLayoutAdContainer.findViewById(R.id.in).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT < 23 || Settings.canDrawOverlays(BoostResultActivity.this)) {
                        i.a(BoostResultActivity.this).c(true);
                        h.a(new Intent("com.plug.swipe.open"));
                        BoostResultActivity.this.finish();
                        return;
                    }
                    BoostResultActivity.this.startActivityForResult(new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.parse("package:" + BoostResultActivity.this.getPackageName())), 1);
                }
            });
        } else if (com.pureapps.cleaner.manager.a.a().b() != null && com.pureapps.cleaner.manager.a.a().b().getParent() == null) {
            this.mLayoutAdContainer.addView(com.pureapps.cleaner.manager.a.a().b());
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1 && Build.VERSION.SDK_INT >= 23) {
            if (Settings.canDrawOverlays(this)) {
                i.a(this).c(true);
                h.a(new Intent("com.plug.swipe.open"));
                sendBroadcast(new Intent("kingoroot.supersu.cleaner.ACTION_2HOUR_ALARM_GIFT"));
                finish();
                return;
            }
            i.a(this).c(false);
        }
    }
}
