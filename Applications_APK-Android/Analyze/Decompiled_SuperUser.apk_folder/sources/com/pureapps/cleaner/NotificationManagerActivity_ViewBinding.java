package com.pureapps.cleaner;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kingouser.com.R;

public class NotificationManagerActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private NotificationManagerActivity f5426a;

    /* renamed from: b  reason: collision with root package name */
    private View f5427b;

    public NotificationManagerActivity_ViewBinding(final NotificationManagerActivity notificationManagerActivity, View view) {
        this.f5426a = notificationManagerActivity;
        notificationManagerActivity.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.dy, "field 'mRecyclerView'", RecyclerView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.dz, "field 'mCleanAllBtn' and method 'onClick'");
        notificationManagerActivity.mCleanAllBtn = (Button) Utils.castView(findRequiredView, R.id.dz, "field 'mCleanAllBtn'", Button.class);
        this.f5427b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View view) {
                notificationManagerActivity.onClick(view);
            }
        });
        notificationManagerActivity.mNoNotificationText = (TextView) Utils.findRequiredViewAsType(view, R.id.e0, "field 'mNoNotificationText'", TextView.class);
        notificationManagerActivity.mDataLayout = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.dx, "field 'mDataLayout'", LinearLayout.class);
    }

    public void unbind() {
        NotificationManagerActivity notificationManagerActivity = this.f5426a;
        if (notificationManagerActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f5426a = null;
        notificationManagerActivity.mRecyclerView = null;
        notificationManagerActivity.mCleanAllBtn = null;
        notificationManagerActivity.mNoNotificationText = null;
        notificationManagerActivity.mDataLayout = null;
        this.f5427b.setOnClickListener(null);
        this.f5427b = null;
    }
}
