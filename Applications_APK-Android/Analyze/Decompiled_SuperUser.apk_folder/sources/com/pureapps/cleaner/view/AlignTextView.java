package com.pureapps.cleaner.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.text.Layout;
import android.text.TextPaint;
import android.util.AttributeSet;
import com.kingouser.com.util.ShellUtils;
import java.util.ArrayList;
import java.util.List;

public class AlignTextView extends AppCompatTextView {

    /* renamed from: a  reason: collision with root package name */
    private float f5891a;

    /* renamed from: b  reason: collision with root package name */
    private int f5892b;

    /* renamed from: c  reason: collision with root package name */
    private List<String> f5893c = new ArrayList();

    /* renamed from: d  reason: collision with root package name */
    private List<Integer> f5894d = new ArrayList();

    /* renamed from: e  reason: collision with root package name */
    private Align f5895e = Align.ALIGN_LEFT;

    public enum Align {
        ALIGN_LEFT,
        ALIGN_CENTER,
        ALIGN_RIGHT
    }

    public AlignTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f2;
        TextPaint paint = getPaint();
        paint.setColor(getCurrentTextColor());
        paint.drawableState = getDrawableState();
        this.f5892b = getMeasuredWidth();
        String charSequence = getText().toString();
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        Layout layout = getLayout();
        if (layout != null) {
            this.f5891a = fontMetrics.descent - fontMetrics.ascent;
            this.f5891a = (this.f5891a * layout.getSpacingMultiplier()) + layout.getSpacingAdd();
            float textSize = getTextSize();
            if ((getGravity() & CodedOutputStream.DEFAULT_BUFFER_SIZE) == 0) {
                f2 = textSize + ((this.f5891a - textSize) / 2.0f);
            } else {
                f2 = textSize;
            }
            int paddingLeft = getPaddingLeft();
            this.f5892b = (this.f5892b - paddingLeft) - getPaddingRight();
            this.f5893c.clear();
            this.f5894d.clear();
            for (String a2 : charSequence.split("\\n")) {
                a(paint, a2);
            }
            for (int i = 0; i < this.f5893c.size(); i++) {
                float f3 = (((float) i) * this.f5891a) + f2;
                String str = this.f5893c.get(i);
                float f4 = (float) paddingLeft;
                float measureText = ((float) this.f5892b) - paint.measureText(str);
                float length = measureText / ((float) (str.length() - 1));
                if (this.f5894d.contains(Integer.valueOf(i))) {
                    length = 0.0f;
                    if (this.f5895e == Align.ALIGN_CENTER) {
                        f4 += measureText / 2.0f;
                    } else if (this.f5895e == Align.ALIGN_RIGHT) {
                        f4 += measureText;
                    }
                }
                for (int i2 = 0; i2 < str.length(); i2++) {
                    canvas.drawText(str.substring(i2, i2 + 1), paint.measureText(str.substring(0, i2)) + (((float) i2) * length) + f4, f3, paint);
                }
            }
        }
    }

    public void setAlign(Align align) {
        this.f5895e = align;
        invalidate();
    }

    private void a(Paint paint, String str) {
        if (str.length() == 0) {
            this.f5893c.add(ShellUtils.COMMAND_LINE_END);
            return;
        }
        StringBuffer stringBuffer = new StringBuffer("");
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (paint.measureText(str.substring(i, i2 + 1)) > ((float) this.f5892b)) {
                this.f5893c.add(stringBuffer.toString());
                stringBuffer = new StringBuffer();
                i = i2;
            }
            stringBuffer.append(str.charAt(i2));
        }
        if (stringBuffer.length() > 0) {
            this.f5893c.add(stringBuffer.toString());
        }
        this.f5894d.add(Integer.valueOf(this.f5893c.size() - 1));
    }
}
