package com.pureapps.cleaner.view.ItemTouch;

import android.graphics.Canvas;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

public class NotificationItemTouchHelperCallback extends ItemTouchHelper.a {

    /* renamed from: a  reason: collision with root package name */
    private final a f5946a;

    public NotificationItemTouchHelperCallback(a aVar) {
        this.f5946a = aVar;
    }

    public boolean a() {
        return true;
    }

    public boolean b() {
        return true;
    }

    public int a(RecyclerView recyclerView, RecyclerView.u uVar) {
        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            return b(15, 0);
        }
        return b(0, 48);
    }

    public boolean b(RecyclerView recyclerView, RecyclerView.u uVar, RecyclerView.u uVar2) {
        if (uVar.getItemViewType() != uVar2.getItemViewType()) {
            return false;
        }
        this.f5946a.a(uVar.getAdapterPosition(), uVar2.getAdapterPosition());
        return true;
    }

    public void a(RecyclerView.u uVar, int i) {
        this.f5946a.a(uVar.getAdapterPosition());
    }

    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.u uVar, float f2, float f3, int i, boolean z) {
        if (i == 1) {
            uVar.itemView.setAlpha(1.0f - (Math.abs(f2) / ((float) uVar.itemView.getWidth())));
            uVar.itemView.setTranslationX(f2);
            return;
        }
        super.a(canvas, recyclerView, uVar, f2, f3, i, z);
    }

    public void b(RecyclerView.u uVar, int i) {
        if (i != 0 && (uVar instanceof b)) {
            ((b) uVar).a();
        }
        super.b(uVar, i);
    }

    public void d(RecyclerView recyclerView, RecyclerView.u uVar) {
        super.d(recyclerView, uVar);
        uVar.itemView.setAlpha(1.0f);
        if (uVar instanceof b) {
            ((b) uVar).b();
        }
    }
}
