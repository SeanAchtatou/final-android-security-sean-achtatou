package com.pureapps.cleaner.view;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import butterknife.ButterKnife;
import com.google.firebase.analytics.FirebaseAnalytics;
import io.fabric.sdk.android.services.common.a;

public class BBaseActivity extends AppCompatActivity {
    public FirebaseAnalytics t;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().setFlags(67108864, 67108864);
        }
        getWindow().findViewById(16908290).setBackgroundColor(-1);
        this.t = FirebaseAnalytics.getInstance(this);
    }

    public void setContentView(int i) {
        super.setContentView(i);
        ButterKnife.bind(this);
    }

    public int m() {
        int identifier = getResources().getIdentifier("status_bar_height", "dimen", a.ANDROID_CLIENT_TYPE);
        if (identifier > 0) {
            return getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            System.gc();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
