package com.pureapps.cleaner.view;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.util.LruCache;
import com.lody.virtual.helper.utils.FileUtils;

/* compiled from: TypefaceSpan */
public class a extends MetricAffectingSpan {

    /* renamed from: a  reason: collision with root package name */
    private static LruCache<String, Typeface> f5962a = new LruCache<>(12);

    /* renamed from: b  reason: collision with root package name */
    private Typeface f5963b;

    public a(Context context, String str) {
        this.f5963b = f5962a.get(str);
        if (this.f5963b == null) {
            this.f5963b = Typeface.createFromAsset(context.getApplicationContext().getAssets(), String.format("fonts/%s", str));
            f5962a.put(str, this.f5963b);
        }
    }

    public void updateMeasureState(TextPaint textPaint) {
        textPaint.setTypeface(this.f5963b);
        textPaint.setFlags(textPaint.getFlags() | FileUtils.FileMode.MODE_IWUSR);
    }

    public void updateDrawState(TextPaint textPaint) {
        textPaint.setTypeface(this.f5963b);
        textPaint.setFlags(textPaint.getFlags() | FileUtils.FileMode.MODE_IWUSR);
    }
}
