package com.pureapps.cleaner.view.etsyblur;

/* compiled from: BlurConfig */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public static final a f5977a = new j();

    /* renamed from: b  reason: collision with root package name */
    public static final d f5978b = new d(10, 4, 0, true, f5977a, false);

    /* renamed from: c  reason: collision with root package name */
    private final int f5979c;

    /* renamed from: d  reason: collision with root package name */
    private final int f5980d;

    /* renamed from: e  reason: collision with root package name */
    private final int f5981e;

    /* renamed from: f  reason: collision with root package name */
    private final boolean f5982f;

    /* renamed from: g  reason: collision with root package name */
    private final a f5983g;

    /* renamed from: h  reason: collision with root package name */
    private final boolean f5984h;

    private d(int i, int i2, int i3, boolean z, a aVar, boolean z2) {
        this.f5979c = i;
        this.f5980d = i2;
        this.f5981e = i3;
        this.f5982f = z;
        this.f5983g = aVar;
        this.f5984h = z2;
    }

    public int a() {
        return this.f5979c;
    }

    public int b() {
        return this.f5980d;
    }

    public int c() {
        return this.f5981e;
    }

    public boolean d() {
        return this.f5982f;
    }

    public a e() {
        return this.f5983g;
    }

    public boolean f() {
        return this.f5984h;
    }

    public static void a(int i) {
        if (i <= 0 || i > 25) {
            throw new IllegalArgumentException("radius must be greater than 0 and less than or equal to 25");
        }
    }

    public static void b(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("downScaleFactor must be greater than 0.");
        }
    }

    /* compiled from: BlurConfig */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private int f5985a = 10;

        /* renamed from: b  reason: collision with root package name */
        private int f5986b = 4;

        /* renamed from: c  reason: collision with root package name */
        private int f5987c = 0;

        /* renamed from: d  reason: collision with root package name */
        private boolean f5988d = true;

        /* renamed from: e  reason: collision with root package name */
        private a f5989e = d.f5977a;

        /* renamed from: f  reason: collision with root package name */
        private boolean f5990f = false;

        public a a(int i) {
            d.a(i);
            this.f5985a = i;
            return this;
        }

        public a b(int i) {
            d.b(i);
            this.f5986b = i;
            return this;
        }

        public a c(int i) {
            this.f5987c = i;
            return this;
        }

        public a a(boolean z) {
            this.f5988d = z;
            return this;
        }

        public a b(boolean z) {
            this.f5990f = z;
            return this;
        }

        public d a() {
            return new d(this.f5985a, this.f5986b, this.f5987c, this.f5988d, this.f5989e, this.f5990f);
        }
    }
}
