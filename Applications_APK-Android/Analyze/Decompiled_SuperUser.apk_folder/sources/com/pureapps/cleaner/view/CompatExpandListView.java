package com.pureapps.cleaner.view;

import android.content.Context;
import android.os.Build;
import android.support.v4.view.ag;
import android.support.v4.view.s;
import android.support.v4.view.u;
import android.support.v4.view.v;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ExpandableListView;

public class CompatExpandListView extends ExpandableListView implements u {

    /* renamed from: a  reason: collision with root package name */
    private v f5924a = new v(this);

    /* renamed from: b  reason: collision with root package name */
    private int f5925b;

    /* renamed from: c  reason: collision with root package name */
    private final int[] f5926c = new int[2];

    /* renamed from: d  reason: collision with root package name */
    private final int[] f5927d = new int[2];

    /* renamed from: e  reason: collision with root package name */
    private int f5928e;

    public CompatExpandListView(Context context) {
        super(context);
        if (Build.VERSION.SDK_INT >= 21) {
            setNestedScrollingEnabled(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.d(android.view.View, boolean):void
     arg types: [com.pureapps.cleaner.view.CompatExpandListView, int]
     candidates:
      android.support.v4.view.ag.d(android.view.View, float):void
      android.support.v4.view.ag.d(android.view.View, int):void
      android.support.v4.view.ag.d(android.view.View, boolean):void */
    public CompatExpandListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (Build.VERSION.SDK_INT >= 21) {
            setNestedScrollingEnabled(true);
        } else {
            ag.d((View) this, true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.d(android.view.View, boolean):void
     arg types: [com.pureapps.cleaner.view.CompatExpandListView, int]
     candidates:
      android.support.v4.view.ag.d(android.view.View, float):void
      android.support.v4.view.ag.d(android.view.View, int):void
      android.support.v4.view.ag.d(android.view.View, boolean):void */
    public CompatExpandListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (Build.VERSION.SDK_INT >= 21) {
            setNestedScrollingEnabled(true);
        } else {
            ag.d((View) this, true);
        }
    }

    public void setNestedScrollingEnabled(boolean z) {
        this.f5924a.a(z);
    }

    public boolean startNestedScroll(int i) {
        return this.f5924a.a(i);
    }

    public void stopNestedScroll() {
        this.f5924a.c();
    }

    public boolean hasNestedScrollingParent() {
        return this.f5924a.b();
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return this.f5924a.a(i, i2, i3, i4, iArr);
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return this.f5924a.a(i, i2, iArr, iArr2);
    }

    public boolean dispatchNestedFling(float f2, float f3, boolean z) {
        return this.f5924a.a(f2, f3, z);
    }

    public boolean dispatchNestedPreFling(float f2, float f3) {
        return this.f5924a.a(f2, f3);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a2 = s.a(motionEvent);
        if (a2 == 0) {
            this.f5928e = 0;
        }
        int y = (int) motionEvent.getY();
        motionEvent.offsetLocation(0.0f, (float) this.f5928e);
        switch (a2) {
            case 0:
                boolean onTouchEvent = super.onTouchEvent(motionEvent);
                this.f5925b = y;
                return onTouchEvent;
            case 1:
            case 3:
                boolean onTouchEvent2 = super.onTouchEvent(motionEvent);
                stopNestedScroll();
                return onTouchEvent2;
            case 2:
                int i = this.f5925b - y;
                int scrollY = getScrollY();
                startNestedScroll(2);
                if (dispatchNestedPreScroll(0, i, this.f5927d, this.f5926c)) {
                    i -= this.f5927d[1];
                    motionEvent.offsetLocation(0.0f, (float) (-this.f5926c[1]));
                    this.f5928e += this.f5926c[1];
                }
                boolean onTouchEvent3 = super.onTouchEvent(motionEvent);
                this.f5925b = y - this.f5926c[1];
                if (i < 0) {
                    int max = Math.max(0, scrollY + i);
                    int i2 = i - (max - scrollY);
                    if (dispatchNestedScroll(0, max - i2, 0, i2, this.f5926c)) {
                        motionEvent.offsetLocation(0.0f, (float) this.f5926c[1]);
                        this.f5928e += this.f5926c[1];
                        this.f5925b -= this.f5926c[1];
                    }
                }
                stopNestedScroll();
                return onTouchEvent3;
            default:
                return false;
        }
    }
}
