package com.pureapps.cleaner.view.jumpingbeans;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.os.Build;
import android.text.TextPaint;
import android.text.style.SuperscriptSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import java.lang.ref.WeakReference;

@SuppressLint({"ParcelCreator"})
final class JumpingBeansSpan extends SuperscriptSpan implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<TextView> f6000a;

    /* renamed from: b  reason: collision with root package name */
    private final int f6001b;

    /* renamed from: c  reason: collision with root package name */
    private final int f6002c;

    /* renamed from: d  reason: collision with root package name */
    private final float f6003d;

    /* renamed from: e  reason: collision with root package name */
    private int f6004e;

    /* renamed from: f  reason: collision with root package name */
    private ValueAnimator f6005f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f6006g;

    public JumpingBeansSpan(TextView textView, int i, int i2, int i3, float f2, boolean z) {
        this.f6000a = new WeakReference<>(textView);
        this.f6001b = i3 * i2;
        this.f6002c = i;
        this.f6003d = f2;
        this.f6006g = z;
    }

    public void updateMeasureState(TextPaint textPaint) {
        a(textPaint.ascent());
        textPaint.baselineShift = this.f6004e;
    }

    public void updateDrawState(TextPaint textPaint) {
        a(textPaint.ascent());
        textPaint.baselineShift = this.f6004e;
    }

    private void a(float f2) {
        int i = 0;
        if (this.f6005f == null) {
            this.f6004e = 0;
            this.f6005f = ValueAnimator.ofInt(0, (int) (f2 / 1.5f));
            this.f6005f.setDuration((long) this.f6002c).setStartDelay((long) this.f6001b);
            this.f6005f.setInterpolator(new a(this.f6003d));
            ValueAnimator valueAnimator = this.f6005f;
            if (this.f6006g) {
                i = -1;
            }
            valueAnimator.setRepeatCount(i);
            this.f6005f.setRepeatMode(1);
            this.f6005f.addUpdateListener(this);
        }
    }

    public void a() {
        if (this.f6005f != null) {
            this.f6005f.start();
        }
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        TextView textView = this.f6000a.get();
        if (textView != null) {
            a(valueAnimator, textView);
        } else {
            c();
        }
    }

    private void a(ValueAnimator valueAnimator, TextView textView) {
        if (a(textView)) {
            this.f6004e = ((Integer) valueAnimator.getAnimatedValue()).intValue();
            textView.invalidate();
        }
    }

    private static boolean a(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.isAttachedToWindow();
        }
        return view.getParent() != null;
    }

    private void c() {
        b();
        Log.w("JumpingBeans", "!!! Remember to call JumpingBeans.stopJumping() when appropriate !!!");
    }

    public void b() {
        if (this.f6005f != null) {
            this.f6005f.cancel();
            this.f6005f.removeAllListeners();
        }
        if (this.f6000a.get() != null) {
            this.f6000a.clear();
        }
    }

    private static class a implements TimeInterpolator {

        /* renamed from: a  reason: collision with root package name */
        private final float f6007a;

        public a(float f2) {
            this.f6007a = Math.abs(f2);
        }

        public float getInterpolation(float f2) {
            if (f2 > this.f6007a) {
                return 0.0f;
            }
            return (float) Math.sin(((double) (f2 / this.f6007a)) * 3.141592653589793d);
        }
    }
}
