package com.pureapps.cleaner.view.etsyblur;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import com.pureapps.cleaner.view.etsyblur.d;
import h.a.a;

public class BlurringView extends View {

    /* renamed from: b  reason: collision with root package name */
    private static final String f5964b = BlurringView.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    final ViewTreeObserver.OnPreDrawListener f5965a;

    /* renamed from: c  reason: collision with root package name */
    private d f5966c;

    /* renamed from: d  reason: collision with root package name */
    private f f5967d;

    /* renamed from: e  reason: collision with root package name */
    private View f5968e;

    /* renamed from: f  reason: collision with root package name */
    private int f5969f;

    /* renamed from: g  reason: collision with root package name */
    private int f5970g;

    /* renamed from: h  reason: collision with root package name */
    private Bitmap f5971h;
    private Canvas i;
    /* access modifiers changed from: private */
    public int j;
    private boolean k;
    /* access modifiers changed from: private */
    public boolean l;

    public BlurringView(Context context) {
        this(context, null);
    }

    public BlurringView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BlurringView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.l = false;
        this.f5965a = new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                if (BlurringView.this.j != 0 && !BlurringView.this.l) {
                    BlurringView.this.invalidate();
                    boolean unused = BlurringView.this.l = true;
                }
                return true;
            }
        };
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, a.C0102a.BlurringView);
        int i3 = obtainStyledAttributes.getInt(2, 0);
        int i4 = obtainStyledAttributes.getInt(0, 10);
        int i5 = obtainStyledAttributes.getInt(1, 4);
        boolean z = obtainStyledAttributes.getBoolean(3, true);
        boolean z2 = obtainStyledAttributes.getBoolean(4, false);
        obtainStyledAttributes.recycle();
        this.f5966c = new d.a().a(i4).b(i5).a(z).c(i3).b(z2).a();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f5966c == null) {
            throw new IllegalStateException("BlurConfig must be set before onAttachedToWindow() gets called.");
        } else if (isInEditMode()) {
            this.f5967d = new i();
        } else {
            this.f5967d = new c(getContext(), this.f5966c);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f5968e != null && this.f5968e.getViewTreeObserver().isAlive()) {
            this.f5968e.getViewTreeObserver().removeOnPreDrawListener(this.f5965a);
        }
        this.f5967d.a();
        if (this.f5971h != null && !this.f5971h.isRecycled()) {
            this.f5971h.recycle();
            this.f5971h = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Bitmap copy;
        boolean z = this.f5968e == getParent();
        if (z) {
            if (!this.k) {
                this.k = true;
            } else {
                return;
            }
        }
        if (this.f5968e != null && a()) {
            if (this.f5968e.getBackground() == null || !(this.f5968e.getBackground() instanceof ColorDrawable)) {
                this.f5971h.eraseColor(0);
            } else {
                this.f5971h.eraseColor(((ColorDrawable) this.f5968e.getBackground()).getColor());
            }
            this.i.save();
            this.i.translate((float) (-this.f5968e.getScrollX()), (float) (-this.f5968e.getScrollY()));
            this.f5968e.draw(this.i);
            this.i.restore();
            try {
                copy = this.f5967d.a(this.f5971h, true);
            } catch (OutOfMemoryError e2) {
                copy = this.f5971h.copy(Bitmap.Config.ARGB_8888, true);
            }
            if (copy != null) {
                canvas.save();
                canvas.translate(this.f5968e.getX() - getX(), this.f5968e.getY() - getY());
                canvas.scale((float) this.f5966c.b(), (float) this.f5966c.b());
                canvas.drawBitmap(copy, 0.0f, 0.0f, (Paint) null);
                canvas.restore();
            }
            if (this.f5966c.c() != 0) {
                canvas.drawColor(this.f5966c.c());
            }
        }
        if (z) {
            this.k = false;
        }
    }

    public void a(View view) {
        if (!(this.f5968e == null || this.f5968e == view || !this.f5968e.getViewTreeObserver().isAlive())) {
            this.f5968e.getViewTreeObserver().removeOnPreDrawListener(this.f5965a);
        }
        this.f5968e = view;
        if (this.f5968e.getViewTreeObserver().isAlive()) {
            this.f5968e.getViewTreeObserver().addOnPreDrawListener(this.f5965a);
        }
    }

    private boolean a() {
        int width = this.f5968e.getWidth();
        int height = this.f5968e.getHeight();
        if (!(width == this.f5969f && height == this.f5970g)) {
            this.f5969f = width;
            this.f5970g = height;
            int b2 = this.f5966c.b();
            int i2 = width / b2;
            int i3 = height / b2;
            if (!(this.f5971h != null && i2 == this.f5971h.getWidth() && i3 == this.f5971h.getHeight())) {
                if (i2 <= 0 || i3 <= 0) {
                    return false;
                }
                this.f5971h = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
                if (this.f5971h == null) {
                    return false;
                }
            }
            this.i = new Canvas(this.f5971h);
            this.i.scale(1.0f / ((float) b2), 1.0f / ((float) b2));
        }
        return true;
    }

    public void setnewState(int i2) {
        this.j = i2;
        if (this.l && i2 == 0 && getAlpha() == 0.0f) {
            this.l = false;
        }
    }
}
