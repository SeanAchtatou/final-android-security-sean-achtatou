package com.pureapps.cleaner.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

public class CustomLinearLayoutManager extends LinearLayoutManager {

    /* renamed from: a  reason: collision with root package name */
    private boolean f5929a = true;

    public CustomLinearLayoutManager(Context context) {
        super(context);
    }

    public void d(boolean z) {
        this.f5929a = z;
    }

    public boolean e() {
        return this.f5929a && super.e();
    }
}
