package com.pureapps.cleaner.view.etsyblur;

import android.support.v4.widget.DrawerLayout;
import android.view.View;

/* compiled from: BlurDrawerListener */
class e extends DrawerLayout.SimpleDrawerListener {

    /* renamed from: a  reason: collision with root package name */
    private final BlurringView f5991a;

    public e(BlurringView blurringView) {
        this.f5991a = blurringView;
        this.f5991a.setVisibility(4);
    }

    public void a(int i) {
        super.a(i);
        this.f5991a.setnewState(i);
    }

    public void a(View view, float f2) {
        if (this.f5991a.getVisibility() != 0) {
            this.f5991a.setVisibility(0);
        }
        this.f5991a.setAlpha(f2);
    }

    public void b(View view) {
        this.f5991a.setVisibility(4);
    }

    public void a(View view) {
        super.a(view);
    }
}
