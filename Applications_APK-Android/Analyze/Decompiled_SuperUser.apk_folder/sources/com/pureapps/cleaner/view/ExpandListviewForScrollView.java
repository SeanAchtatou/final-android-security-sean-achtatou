package com.pureapps.cleaner.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ExpandableListView;

public class ExpandListviewForScrollView extends ExpandableListView {
    public ExpandListviewForScrollView(Context context) {
        super(context);
    }

    public ExpandListviewForScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ExpandListviewForScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(536870911, Integer.MIN_VALUE));
    }
}
