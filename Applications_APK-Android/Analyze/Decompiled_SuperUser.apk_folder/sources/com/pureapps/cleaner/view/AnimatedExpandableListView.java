package com.pureapps.cleaner.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import java.util.ArrayList;
import java.util.List;

public class AnimatedExpandableListView extends CompatExpandListView {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5900a = a.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private a f5901b;

    public AnimatedExpandableListView(Context context) {
        super(context);
    }

    public AnimatedExpandableListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AnimatedExpandableListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setAdapter(ExpandableListAdapter expandableListAdapter) {
        super.setAdapter(expandableListAdapter);
        if (expandableListAdapter instanceof a) {
            this.f5901b = (a) expandableListAdapter;
            this.f5901b.a(this);
            return;
        }
        throw new ClassCastException(expandableListAdapter.toString() + " must implement AnimatedExpandableListAdapter");
    }

    @SuppressLint({"NewApi"})
    public boolean a(int i) {
        int firstVisiblePosition;
        if ((i == this.f5901b.getGroupCount() + -1) && Build.VERSION.SDK_INT >= 14) {
            return expandGroup(i, true);
        }
        int flatListPosition = getFlatListPosition(getPackedPositionForGroup(i));
        if (flatListPosition == -1 || (firstVisiblePosition = flatListPosition - getFirstVisiblePosition()) >= getChildCount() || getChildAt(firstVisiblePosition).getBottom() < getBottom()) {
            this.f5901b.a(i, 0);
            return expandGroup(i);
        }
        this.f5901b.c(i);
        return expandGroup(i);
    }

    public boolean b(int i) {
        int flatListPosition = getFlatListPosition(getPackedPositionForGroup(i));
        if (flatListPosition != -1) {
            int firstVisiblePosition = flatListPosition - getFirstVisiblePosition();
            if (firstVisiblePosition < 0 || firstVisiblePosition >= getChildCount()) {
                return collapseGroup(i);
            }
            if (getChildAt(firstVisiblePosition).getBottom() >= getBottom()) {
                return collapseGroup(i);
            }
        }
        long expandableListPosition = getExpandableListPosition(getFirstVisiblePosition());
        int packedPositionChild = getPackedPositionChild(expandableListPosition);
        int packedPositionGroup = getPackedPositionGroup(expandableListPosition);
        if (packedPositionChild == -1 || packedPositionGroup != i) {
            packedPositionChild = 0;
        }
        this.f5901b.c(i, packedPositionChild);
        this.f5901b.notifyDataSetChanged();
        return isGroupExpanded(i);
    }

    /* access modifiers changed from: private */
    public int getAnimationDuration() {
        return 300;
    }

    private static class c {

        /* renamed from: a  reason: collision with root package name */
        boolean f5920a;

        /* renamed from: b  reason: collision with root package name */
        boolean f5921b;

        /* renamed from: c  reason: collision with root package name */
        int f5922c;

        /* renamed from: d  reason: collision with root package name */
        int f5923d;

        private c() {
            this.f5920a = false;
            this.f5921b = false;
            this.f5923d = -1;
        }
    }

    public static abstract class a extends BaseExpandableListAdapter {

        /* renamed from: a  reason: collision with root package name */
        private SparseArray<c> f5906a = new SparseArray<>();

        /* renamed from: b  reason: collision with root package name */
        private AnimatedExpandableListView f5907b;

        public abstract View a(int i, int i2, boolean z, View view, ViewGroup viewGroup);

        public abstract int b(int i);

        /* access modifiers changed from: private */
        public void a(AnimatedExpandableListView animatedExpandableListView) {
            this.f5907b = animatedExpandableListView;
        }

        public int b(int i, int i2) {
            return 0;
        }

        public int f() {
            return 1;
        }

        private c a(int i) {
            c cVar = this.f5906a.get(i);
            if (cVar != null) {
                return cVar;
            }
            c cVar2 = new c();
            this.f5906a.put(i, cVar2);
            return cVar2;
        }

        public void c(int i) {
            a(i).f5923d = -1;
        }

        /* access modifiers changed from: private */
        public void a(int i, int i2) {
            c a2 = a(i);
            a2.f5920a = true;
            a2.f5922c = i2;
            a2.f5921b = true;
        }

        /* access modifiers changed from: private */
        public void c(int i, int i2) {
            c a2 = a(i);
            a2.f5920a = true;
            a2.f5922c = i2;
            a2.f5921b = false;
        }

        /* access modifiers changed from: private */
        public void d(int i) {
            a(i).f5920a = false;
        }

        public final int getChildType(int i, int i2) {
            if (a(i).f5920a) {
                return 0;
            }
            return b(i, i2) + 1;
        }

        public final int getChildTypeCount() {
            return f() + 1;
        }

        /* access modifiers changed from: protected */
        public ViewGroup.LayoutParams g() {
            return new AbsListView.LayoutParams(-1, -2, 0);
        }

        public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
            View view2;
            int i3;
            int intValue;
            int i4;
            c a2 = a(i);
            if (!a2.f5920a) {
                return a(i, i2, z, view, viewGroup);
            }
            if (!(view instanceof DummyView)) {
                view2 = new DummyView(viewGroup.getContext());
                view2.setLayoutParams(new AbsListView.LayoutParams(-1, 0));
            } else {
                view2 = view;
            }
            if (i2 < a2.f5922c) {
                view2.getLayoutParams().height = 0;
                return view2;
            }
            ExpandableListView expandableListView = (ExpandableListView) viewGroup;
            final DummyView dummyView = (DummyView) view2;
            dummyView.a();
            dummyView.a(expandableListView.getDivider(), viewGroup.getMeasuredWidth(), expandableListView.getDividerHeight());
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(viewGroup.getWidth(), 1073741824);
            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
            int i5 = 0;
            int height = viewGroup.getHeight();
            int b2 = b(i);
            int i6 = a2.f5922c;
            while (true) {
                if (i6 >= b2) {
                    i3 = i5;
                    break;
                }
                View a3 = a(i, i6, i6 == b2 + -1, null, viewGroup);
                AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams) a3.getLayoutParams();
                if (layoutParams == null) {
                    layoutParams = (AbsListView.LayoutParams) g();
                    a3.setLayoutParams(layoutParams);
                }
                int i7 = layoutParams.height;
                if (i7 > 0) {
                    i4 = View.MeasureSpec.makeMeasureSpec(i7, 1073741824);
                } else {
                    i4 = makeMeasureSpec2;
                }
                a3.measure(makeMeasureSpec, i4);
                i5 += a3.getMeasuredHeight();
                if (i5 >= height) {
                    dummyView.a(a3);
                    i3 = i5 + ((i5 / (i6 + 1)) * ((b2 - i6) - 1));
                    break;
                }
                dummyView.a(a3);
                i6++;
            }
            Object tag = dummyView.getTag();
            if (tag == null) {
                intValue = 0;
            } else {
                intValue = ((Integer) tag).intValue();
            }
            if (a2.f5921b && intValue != 1) {
                b bVar = new b(dummyView, 0, i3, a2);
                bVar.setDuration((long) this.f5907b.getAnimationDuration());
                final int i8 = i;
                bVar.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        a.this.d(i8);
                        a.this.notifyDataSetChanged();
                        dummyView.setTag(0);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
                dummyView.startAnimation(bVar);
                dummyView.setTag(1);
                return view2;
            } else if (a2.f5921b || intValue == 2) {
                return view2;
            } else {
                if (a2.f5923d == -1) {
                    a2.f5923d = i3;
                }
                b bVar2 = new b(dummyView, a2.f5923d, 0, a2);
                bVar2.setDuration((long) this.f5907b.getAnimationDuration());
                final int i9 = i;
                final ExpandableListView expandableListView2 = expandableListView;
                final c cVar = a2;
                bVar2.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        a.this.d(i9);
                        expandableListView2.collapseGroup(i9);
                        a.this.notifyDataSetChanged();
                        cVar.f5923d = -1;
                        dummyView.setTag(0);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
                dummyView.startAnimation(bVar2);
                dummyView.setTag(2);
                return view2;
            }
        }

        public final int getChildrenCount(int i) {
            c a2 = a(i);
            if (a2.f5920a) {
                return a2.f5922c + 1;
            }
            return b(i);
        }
    }

    private static class DummyView extends View {

        /* renamed from: a  reason: collision with root package name */
        private List<View> f5902a = new ArrayList();

        /* renamed from: b  reason: collision with root package name */
        private Drawable f5903b;

        /* renamed from: c  reason: collision with root package name */
        private int f5904c;

        /* renamed from: d  reason: collision with root package name */
        private int f5905d;

        public DummyView(Context context) {
            super(context);
        }

        public void a(Drawable drawable, int i, int i2) {
            if (drawable != null) {
                this.f5903b = drawable;
                this.f5904c = i;
                this.f5905d = i2;
                drawable.setBounds(0, 0, i, i2);
            }
        }

        public void a(View view) {
            view.layout(0, 0, getWidth(), view.getMeasuredHeight());
            this.f5902a.add(view);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            int size = this.f5902a.size();
            for (int i5 = 0; i5 < size; i5++) {
                View view = this.f5902a.get(i5);
                view.layout(i, i2, view.getMeasuredWidth() + i, view.getMeasuredHeight() + i2);
            }
        }

        public void a() {
            this.f5902a.clear();
        }

        public void dispatchDraw(Canvas canvas) {
            canvas.save();
            if (this.f5903b != null) {
                this.f5903b.setBounds(0, 0, this.f5904c, this.f5905d);
            }
            int size = this.f5902a.size();
            for (int i = 0; i < size; i++) {
                View view = this.f5902a.get(i);
                canvas.save();
                canvas.clipRect(0, 0, getWidth(), view.getMeasuredHeight());
                view.draw(canvas);
                canvas.restore();
                if (this.f5903b != null) {
                    this.f5903b.draw(canvas);
                    canvas.translate(0.0f, (float) this.f5905d);
                }
                canvas.translate(0.0f, (float) view.getMeasuredHeight());
            }
            canvas.restore();
        }
    }

    private static class b extends Animation {

        /* renamed from: a  reason: collision with root package name */
        private int f5916a;

        /* renamed from: b  reason: collision with root package name */
        private int f5917b;

        /* renamed from: c  reason: collision with root package name */
        private View f5918c;

        /* renamed from: d  reason: collision with root package name */
        private c f5919d;

        private b(View view, int i, int i2, c cVar) {
            this.f5916a = i;
            this.f5917b = i2 - i;
            this.f5918c = view;
            this.f5919d = cVar;
            this.f5918c.getLayoutParams().height = i;
            this.f5918c.requestLayout();
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f2, Transformation transformation) {
            super.applyTransformation(f2, transformation);
            if (f2 < 1.0f) {
                int i = this.f5916a + ((int) (((float) this.f5917b) * f2));
                this.f5918c.getLayoutParams().height = i;
                this.f5919d.f5923d = i;
                this.f5918c.requestLayout();
                return;
            }
            int i2 = this.f5916a + this.f5917b;
            this.f5918c.getLayoutParams().height = i2;
            this.f5919d.f5923d = i2;
            this.f5918c.requestLayout();
        }
    }
}
