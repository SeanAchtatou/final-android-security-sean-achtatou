package com.pureapps.cleaner.view.etsyblur;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.pureapps.cleaner.view.etsyblur.f;

/* compiled from: Blur */
public class c implements f {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5975a = c.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private f f5976b;

    public c(Context context, d dVar) {
        if (dVar.d()) {
            this.f5976b = new h(dVar);
        } else {
            this.f5976b = new i();
        }
        if (dVar.f()) {
            Log.d(f5975a, "Used Blur Method: " + this.f5976b.b());
        }
    }

    public Bitmap a(Bitmap bitmap, boolean z) {
        return this.f5976b.a(bitmap, z);
    }

    public void a(Bitmap bitmap, boolean z, f.a aVar) {
        this.f5976b.a(bitmap, z, aVar);
    }

    public void a() {
        this.f5976b.a();
    }

    public String b() {
        return this.f5976b.b();
    }
}
