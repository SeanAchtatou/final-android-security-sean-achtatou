package com.pureapps.cleaner.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import com.kingouser.com.R;

public class NotificationMemoryView extends View {

    /* renamed from: a  reason: collision with root package name */
    private int f5947a = Math.round(getResources().getDimension(R.dimen.fk) * 0.1f);

    /* renamed from: b  reason: collision with root package name */
    private int f5948b;

    /* renamed from: c  reason: collision with root package name */
    private int f5949c;

    /* renamed from: d  reason: collision with root package name */
    private float f5950d;

    /* renamed from: e  reason: collision with root package name */
    private RectF f5951e = new RectF();

    /* renamed from: f  reason: collision with root package name */
    private Paint f5952f;

    /* renamed from: g  reason: collision with root package name */
    private Paint f5953g;

    /* renamed from: h  reason: collision with root package name */
    private Paint f5954h;
    private float i = ((float) getResources().getDimensionPixelSize(R.dimen.f8));

    public NotificationMemoryView(Context context) {
        super(context, null);
        this.f5948b = context.getResources().getDimensionPixelSize(R.dimen.fk);
        this.f5949c = context.getResources().getDimensionPixelSize(R.dimen.fk);
        this.f5950d = 0.0f;
        this.f5951e = new RectF();
        this.f5952f = new Paint();
        this.f5952f.setStyle(Paint.Style.STROKE);
        this.f5952f.setStrokeWidth(this.i);
        this.f5952f.setColor(-9932684);
        this.f5952f.setAntiAlias(true);
        this.f5954h = new Paint();
        this.f5954h.setStyle(Paint.Style.STROKE);
        this.f5954h.setStrokeWidth(this.i);
        this.f5954h.setColor(-9225156);
        this.f5954h.setAntiAlias(true);
        this.f5953g = new Paint();
        this.f5953g.setStyle(Paint.Style.FILL);
        this.f5953g.setStrokeWidth(0.0f);
        this.f5953g.setColor(-9225156);
        this.f5953g.setAlpha(51);
        this.f5953g.setAntiAlias(true);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(this.f5951e, -90.0f, this.f5950d, true, this.f5953g);
        canvas.drawArc(this.f5951e, 0.0f, 360.0f, true, this.f5952f);
        canvas.drawArc(this.f5951e, -90.0f, this.f5950d, false, this.f5954h);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int defaultSize = getDefaultSize(this.f5948b, i2);
        int defaultSize2 = getDefaultSize(this.f5949c, i3);
        float f2 = this.i / 2.0f;
        this.f5951e.set(((float) this.f5947a) + f2, ((float) this.f5947a) + f2, (((float) defaultSize) - ((float) this.f5947a)) - f2, (((float) defaultSize2) - ((float) this.f5947a)) - f2);
        setMeasuredDimension(defaultSize, defaultSize2);
    }

    public final void setPercent(int i2) {
        this.f5950d = (((float) i2) / 100.0f) * 360.0f;
        if (i2 > 60) {
            this.f5954h.setColor(getResources().getColor(R.color.co));
            this.f5953g.setColor(getResources().getColor(R.color.co));
        } else if (i2 > 40) {
            this.f5954h.setColor(getResources().getColor(R.color.cn));
            this.f5953g.setColor(getResources().getColor(R.color.cn));
        } else {
            this.f5954h.setColor(getResources().getColor(R.color.am));
            this.f5953g.setColor(getResources().getColor(R.color.am));
        }
        this.f5953g.setAlpha(51);
        invalidate();
    }
}
