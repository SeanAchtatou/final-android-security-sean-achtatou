package com.pureapps.cleaner.view.etsyblur;

import android.graphics.Bitmap;

/* compiled from: BlurEngine */
public interface f {

    /* compiled from: BlurEngine */
    public interface a {
        void a(Bitmap bitmap);
    }

    Bitmap a(Bitmap bitmap, boolean z);

    void a();

    void a(Bitmap bitmap, boolean z, a aVar);

    String b();
}
