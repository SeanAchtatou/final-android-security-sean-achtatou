package com.pureapps.cleaner.view.etsyblur;

import android.support.v4.widget.DrawerLayout;
import android.view.View;
import com.kingouser.com.R;

/* compiled from: BlurSupport */
public class g {
    public static void a(final DrawerLayout drawerLayout) {
        View childAt = drawerLayout.getChildAt(0);
        final View findViewById = drawerLayout.findViewById(R.id.dj);
        if (childAt == null) {
            throw new IllegalStateException("There's no view to blur. DrawerLayout does not have the first child view.");
        } else if (findViewById == null) {
            throw new IllegalStateException("There's no blurringView. Include BlurringView with id set to 'blurring_view'");
        } else if (!(findViewById instanceof BlurringView)) {
            throw new IllegalStateException("blurring_view must be type BlurringView");
        } else {
            ((BlurringView) findViewById).a(childAt);
            drawerLayout.a(new e((BlurringView) findViewById));
            drawerLayout.post(new Runnable() {
                public void run() {
                    if (drawerLayout.g(8388611) || drawerLayout.g(8388613)) {
                        findViewById.setVisibility(0);
                    }
                }
            });
        }
    }
}
