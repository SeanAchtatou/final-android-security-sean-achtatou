package com.pureapps.cleaner.view.etsyblur;

import android.os.AsyncTask;
import java.util.LinkedList;
import java.util.List;

/* compiled from: BaseBlurEngine */
abstract class b implements f {

    /* renamed from: a  reason: collision with root package name */
    final List<AsyncTask> f5973a = new LinkedList();

    /* renamed from: b  reason: collision with root package name */
    final d f5974b;

    /* access modifiers changed from: package-private */
    public abstract long a(int i, int i2, int i3);

    /* access modifiers changed from: package-private */
    public abstract boolean b(int i, int i2, int i3);

    public b(d dVar) {
        this.f5974b = dVar;
    }

    public void a() {
        for (AsyncTask cancel : this.f5973a) {
            cancel.cancel(true);
        }
        this.f5973a.clear();
    }
}
