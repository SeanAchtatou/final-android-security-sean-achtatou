package com.pureapps.cleaner.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.FrameLayout;

public class FloatView extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private IntentFilter f5940a = null;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public a f5941b = null;

    /* renamed from: c  reason: collision with root package name */
    private BroadcastReceiver f5942c = new BroadcastReceiver() {

        /* renamed from: a  reason: collision with root package name */
        final String f5943a = "reason";

        /* renamed from: b  reason: collision with root package name */
        final String f5944b = "homekey";

        public void onReceive(Context context, Intent intent) {
            String stringExtra;
            if (intent.getAction().equals("android.intent.action.CLOSE_SYSTEM_DIALOGS") && (stringExtra = intent.getStringExtra("reason")) != null && stringExtra.equals("homekey") && FloatView.this.f5941b != null) {
                FloatView.this.f5941b.a();
            }
        }
    };

    public interface a {
        void a();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f5940a = new IntentFilter();
        this.f5940a.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        try {
            getContext().registerReceiver(this.f5942c, this.f5940a);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getContext().unregisterReceiver(this.f5942c);
    }

    public FloatView(Context context) {
        super(context);
    }

    public FloatView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setOnBackClickListener(a aVar) {
        this.f5941b = aVar;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case 4:
            case 82:
                if (this.f5941b != null) {
                    this.f5941b.a();
                    break;
                }
                break;
        }
        return super.dispatchKeyEvent(keyEvent);
    }
}
