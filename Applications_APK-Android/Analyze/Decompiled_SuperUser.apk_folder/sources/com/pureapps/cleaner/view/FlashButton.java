package com.pureapps.cleaner.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.LinearInterpolator;
import com.kingouser.com.R;
import com.pureapps.cleaner.util.b;

public class FlashButton extends AppCompatButton {

    /* renamed from: a  reason: collision with root package name */
    public boolean f5930a = false;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Bitmap f5931b;

    /* renamed from: c  reason: collision with root package name */
    private Bitmap f5932c;

    /* renamed from: d  reason: collision with root package name */
    private Paint f5933d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public PorterDuffXfermode f5934e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public float f5935f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public float f5936g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public boolean f5937h;
    private int i = -1;
    private ValueAnimator j = null;
    private boolean k = true;

    public FlashButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f5931b = b.a(context.getResources().getDrawable(R.drawable.i3));
        this.f5936g = (float) (-this.f5931b.getWidth());
        this.f5934e = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);
        this.f5933d = new Paint(1);
        this.f5933d.setColor(-1);
    }

    public void setAutoAnim(boolean z) {
        this.k = z;
    }

    public void a() {
        if (this.j != null) {
            this.j.cancel();
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.k) {
            setRepeatCount(-1);
            a(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        a();
    }

    public final void a(boolean z) {
        if (!this.f5930a || z) {
            this.f5930a = true;
            this.j = ValueAnimator.ofFloat(0.0f, 1.0f);
            this.j.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (valueAnimator.getAnimatedFraction() < 0.31034482f) {
                        float unused = FlashButton.this.f5936g = ((float) (-FlashButton.this.f5931b.getWidth())) + (FlashButton.this.f5935f * valueAnimator.getAnimatedFraction() * 3.2222223f);
                        FlashButton.this.invalidate();
                    }
                }
            });
            this.j.addListener(new AnimatorListenerAdapter() {
                public final void onAnimationRepeat(Animator animator) {
                    if (FlashButton.this.f5930a) {
                        PorterDuffXfermode unused = FlashButton.this.f5934e = new PorterDuffXfermode(FlashButton.this.f5937h ? PorterDuff.Mode.CLEAR : PorterDuff.Mode.SRC_IN);
                    } else {
                        animator.cancel();
                    }
                }
            });
            this.j.setRepeatCount(this.i);
            this.j.setRepeatMode(1);
            this.j.setDuration(1450L).setInterpolator(new LinearInterpolator());
            this.j.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f5932c != null) {
            int saveLayer = canvas.saveLayer(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.f5933d, 31);
            canvas.drawBitmap(this.f5931b, this.f5936g, 0.0f, this.f5933d);
            this.f5933d.setXfermode(this.f5934e);
            canvas.drawBitmap(this.f5932c, 0.0f, 0.0f, this.f5933d);
            this.f5933d.setXfermode(null);
            canvas.restoreToCount(saveLayer);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        if (i2 > 0 && i3 > 0) {
            float height = ((float) i3) / ((float) this.f5931b.getHeight());
            if (height <= 10.0f) {
                Bitmap bitmap = this.f5931b;
                int width = bitmap.getWidth();
                int height2 = bitmap.getHeight();
                Matrix matrix = new Matrix();
                matrix.postScale(((float) ((int) (height * ((float) this.f5931b.getWidth())))) / ((float) width), ((float) i3) / ((float) height2));
                this.f5931b = Bitmap.createBitmap(bitmap, 0, 0, width, height2, matrix, true);
                this.f5935f = (float) ((this.f5931b.getWidth() * 2) + i2);
                try {
                    this.f5932c = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
                } catch (OutOfMemoryError e2) {
                    this.f5932c = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
                }
                new Canvas(this.f5932c).drawRect(new RectF(0.0f, 0.0f, (float) i2, (float) i3), this.f5933d);
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f5937h = true;
                break;
            case 1:
            case 3:
                this.f5937h = false;
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setRepeatCount(int i2) {
        this.i = i2;
    }
}
