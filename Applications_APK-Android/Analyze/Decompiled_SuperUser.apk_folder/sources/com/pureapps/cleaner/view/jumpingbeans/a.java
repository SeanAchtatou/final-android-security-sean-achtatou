package com.pureapps.cleaner.view.jumpingbeans;

import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.widget.TextView;
import java.lang.ref.WeakReference;

/* compiled from: JumpingBeans */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final JumpingBeansSpan[] f6008a;

    /* renamed from: b  reason: collision with root package name */
    private final WeakReference<TextView> f6009b;

    private a(JumpingBeansSpan[] jumpingBeansSpanArr, TextView textView) {
        this.f6008a = jumpingBeansSpanArr;
        this.f6009b = new WeakReference<>(textView);
    }

    public void a() {
        int i = 0;
        while (this.f6008a != null && i < this.f6008a.length) {
            this.f6008a[i].a();
            i++;
        }
    }

    public static C0092a a(TextView textView) {
        return new C0092a(textView);
    }

    public void b() {
        for (JumpingBeansSpan jumpingBeansSpan : this.f6008a) {
            if (jumpingBeansSpan != null) {
                jumpingBeansSpan.b();
            }
        }
        b(this.f6009b.get());
    }

    private static void b(TextView textView) {
        if (textView != null) {
            CharSequence text = textView.getText();
            if (text instanceof Spanned) {
                textView.setText(a((Spanned) text));
            }
        }
    }

    private static CharSequence a(Spanned spanned) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(spanned.toString());
        for (Object obj : spanned.getSpans(0, spanned.length(), Object.class)) {
            if (!(obj instanceof JumpingBeansSpan)) {
                spannableStringBuilder.setSpan(obj, spanned.getSpanStart(obj), spanned.getSpanEnd(obj), spanned.getSpanFlags(obj));
            }
        }
        return spannableStringBuilder;
    }

    /* renamed from: com.pureapps.cleaner.view.jumpingbeans.a$a  reason: collision with other inner class name */
    /* compiled from: JumpingBeans */
    public static class C0092a {

        /* renamed from: a  reason: collision with root package name */
        private final TextView f6010a;

        /* renamed from: b  reason: collision with root package name */
        private int f6011b;

        /* renamed from: c  reason: collision with root package name */
        private int f6012c;

        /* renamed from: d  reason: collision with root package name */
        private float f6013d = 0.65f;

        /* renamed from: e  reason: collision with root package name */
        private int f6014e = 800;

        /* renamed from: f  reason: collision with root package name */
        private int f6015f = -1;

        /* renamed from: g  reason: collision with root package name */
        private CharSequence f6016g;

        /* renamed from: h  reason: collision with root package name */
        private boolean f6017h;
        private boolean i = false;

        C0092a(TextView textView) {
            this.f6010a = textView;
        }

        public C0092a a(boolean z) {
            this.i = z;
            return this;
        }

        public C0092a a(int i2, int i3) {
            CharSequence text = this.f6010a.getText();
            a(i2, i3, text);
            this.f6016g = text;
            this.f6017h = true;
            this.f6011b = i2;
            this.f6012c = i3;
            return this;
        }

        private static CharSequence a(int i2, int i3, CharSequence charSequence) {
            if (charSequence == null) {
                throw new NullPointerException("The textView text must not be null");
            } else if (i3 < i2) {
                throw new IllegalArgumentException("The start position must be smaller than the end position");
            } else if (i2 < 0) {
                throw new IndexOutOfBoundsException("The start position must be non-negative");
            } else if (i3 <= charSequence.length()) {
                return charSequence;
            } else {
                throw new IndexOutOfBoundsException("The end position must be smaller than the text length");
            }
        }

        public C0092a a(int i2) {
            if (i2 < 1) {
                throw new IllegalArgumentException("The loop duration must be bigger than zero");
            }
            this.f6014e = i2;
            return this;
        }

        public C0092a b(boolean z) {
            this.f6017h = z;
            return this;
        }

        public a a() {
            JumpingBeansSpan[] b2;
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.f6016g);
            if (this.f6017h) {
                b2 = a(spannableStringBuilder);
            } else {
                b2 = b(spannableStringBuilder);
            }
            this.f6010a.setText(spannableStringBuilder);
            return new a(b2, this.f6010a);
        }

        private JumpingBeansSpan[] a(SpannableStringBuilder spannableStringBuilder) {
            if (this.f6015f == -1) {
                this.f6015f = this.f6014e / ((this.f6012c - this.f6011b) * 3);
            }
            JumpingBeansSpan[] jumpingBeansSpanArr = new JumpingBeansSpan[(this.f6012c - this.f6011b)];
            int i2 = this.f6011b;
            while (true) {
                int i3 = i2;
                if (i3 >= this.f6012c) {
                    return jumpingBeansSpanArr;
                }
                JumpingBeansSpan jumpingBeansSpan = new JumpingBeansSpan(this.f6010a, this.f6014e, i3 - this.f6011b, this.f6015f, this.f6013d, this.i);
                spannableStringBuilder.setSpan(jumpingBeansSpan, i3, i3 + 1, 33);
                jumpingBeansSpanArr[i3 - this.f6011b] = jumpingBeansSpan;
                i2 = i3 + 1;
            }
        }

        private JumpingBeansSpan[] b(SpannableStringBuilder spannableStringBuilder) {
            JumpingBeansSpan[] jumpingBeansSpanArr = {new JumpingBeansSpan(this.f6010a, this.f6014e, 0, 0, this.f6013d, this.i)};
            spannableStringBuilder.setSpan(jumpingBeansSpanArr[0], this.f6011b, this.f6012c, 33);
            return jumpingBeansSpanArr;
        }
    }
}
