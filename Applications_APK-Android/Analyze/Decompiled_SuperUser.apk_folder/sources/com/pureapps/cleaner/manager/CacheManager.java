package com.pureapps.cleaner.manager;

import android.content.Context;
import com.pureapps.cleaner.b.b;
import com.pureapps.cleaner.net.NetworkStatus;
import com.pureapps.cleaner.util.e;
import com.pureapps.cleaner.util.g;
import com.pureapps.cleaner.util.l;
import java.io.File;
import java.io.IOException;

public class CacheManager {

    /* renamed from: a  reason: collision with root package name */
    private b f5685a = null;

    /* renamed from: b  reason: collision with root package name */
    private Context f5686b = null;

    public CacheManager(Context context) {
        this.f5686b = context;
        b();
    }

    private void b() {
        try {
            this.f5685a = b.a(new File(e.a(this.f5686b)), l.c(this.f5686b), 3, 10485760);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void a(String str, String str2, long j) {
        try {
            b.a b2 = this.f5685a.b(g.a(str));
            if (b2 != null) {
                b2.a(0, str2);
                b2.a(1, String.valueOf(j));
                b2.a(2, String.valueOf(System.currentTimeMillis()));
                b2.a();
                this.f5685a.a();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void a() {
        try {
            this.f5685a.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public String a(String str, boolean z) {
        try {
            b.c a2 = this.f5685a.a(g.a(str));
            if (a2 == null) {
                return null;
            }
            if (NetworkStatus.b(this.f5686b)) {
                if (System.currentTimeMillis() - Long.parseLong(a2.b(2)) > Long.parseLong(a2.b(1)) && z) {
                    return null;
                }
            }
            return a2.b(0);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
