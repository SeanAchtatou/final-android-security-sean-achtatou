package com.pureapps.cleaner.manager;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingoapp.uts.api.RxAndroidPlugins;
import com.kingouser.com.application.App;
import com.kingouser.com.entity.PlugEntity;
import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.remote.InstallResult;
import com.lody.virtual.server.pm.PackageCacheManager;
import com.pureapps.cleaner.util.e;
import com.pureapps.cleaner.util.i;
import io.reactivex.BackpressureStrategy;
import io.reactivex.d;
import io.reactivex.f;
import java.io.File;
import java.util.concurrent.Callable;

/* compiled from: PlugManager */
public class h {

    /* renamed from: a  reason: collision with root package name */
    public static String f5804a = "com.kg.apk";

    /* renamed from: b  reason: collision with root package name */
    public static final io.reactivex.h f5805b = RxAndroidPlugins.initMainThreadScheduler(new Callable() {
        /* renamed from: a */
        public io.reactivex.h call() {
            return a.f5815a;
        }
    });
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static boolean f5806c = false;

    /* compiled from: PlugManager */
    private static final class a {

        /* renamed from: a  reason: collision with root package name */
        static final io.reactivex.h f5815a = new d(new Handler(Looper.getMainLooper()));
    }

    public static boolean a(String str) {
        try {
            return VirtualCore.get().isAppInstalled(str);
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static void a(Context context) {
        if (c(context) && i.a(context).h()) {
            a(g());
        }
        if (e(context)) {
            a(h());
        }
    }

    public static boolean a(Intent intent) {
        try {
            VActivityManager.get().startService(null, intent, null, 0);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            com.pureapps.cleaner.analytic.a.a(App.a()).a(intent.getAction() + ":" + e2.getMessage());
            return false;
        }
    }

    private static void b(String str) {
        try {
            VirtualCore.get().uninstallPackage(str);
        } catch (Exception e2) {
            e2.printStackTrace();
            Log.e("PlugManager", "uninstall plug:" + str + " failed");
        }
    }

    private static boolean f() {
        return VirtualCore.get().isMainProcess();
    }

    public static void b(final Context context) {
        final String str = e.a(context) + File.separator + "result_plug.apk";
        final String str2 = e.a(context) + File.separator + "swipe_plug.apk";
        final String str3 = e.a(context) + File.separator + "float_plug.apk";
        e.a(context) + File.separator + "fix_plug.apk";
        i.a(context).a(System.currentTimeMillis());
        if (f()) {
            d.a(new f<String>() {
                /* JADX WARNING: Removed duplicated region for block: B:11:0x00e5  */
                /* JADX WARNING: Removed duplicated region for block: B:14:0x0117  */
                /* JADX WARNING: Removed duplicated region for block: B:22:0x0155  */
                /* JADX WARNING: Removed duplicated region for block: B:23:0x0161  */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void subscribe(io.reactivex.e<java.lang.String> r9) {
                    /*
                        r8 = this;
                        r6 = 3
                        r5 = 0
                        r4 = 1
                        r0 = 5000(0x1388, double:2.4703E-320)
                        java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x0146 }
                    L_0x0008:
                        boolean unused = com.pureapps.cleaner.manager.h.f5806c = r4
                        android.content.Context r0 = r6
                        java.lang.String r1 = "fix_plug.apk"
                        android.content.Context r2 = r6
                        java.lang.String r2 = com.pureapps.cleaner.util.e.a(r2)
                        java.lang.Boolean r0 = com.kingouser.com.util.FileUtils.CopyAssetsFile(r0, r1, r2)
                        boolean r0 = r0.booleanValue()
                        java.lang.String r1 = "PlugManager"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        r2.<init>()
                        java.lang.String r3 = "fix apk file copy result:"
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.StringBuilder r0 = r2.append(r0)
                        java.lang.String r0 = r0.toString()
                        com.pureapps.cleaner.util.f.b(r1, r0)
                        android.content.Context r0 = r6
                        java.lang.String r1 = "result_plug.apk"
                        android.content.Context r2 = r6
                        java.lang.String r2 = com.pureapps.cleaner.util.e.a(r2)
                        java.lang.Boolean r0 = com.kingouser.com.util.FileUtils.CopyAssetsFile(r0, r1, r2)
                        boolean r0 = r0.booleanValue()
                        java.lang.String r1 = "PlugManager"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        r2.<init>()
                        java.lang.String r3 = "复制文件结果:"
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.StringBuilder r0 = r2.append(r0)
                        java.lang.String r0 = r0.toString()
                        com.pureapps.cleaner.util.f.b(r1, r0)
                        java.lang.String r0 = com.kingouser.com.b.a.m
                        r1 = 2
                        java.lang.Object[] r1 = new java.lang.Object[r1]
                        java.lang.String r2 = "all"
                        r1[r5] = r2
                        android.content.Context r2 = r6
                        java.lang.String r2 = r2.getPackageName()
                        r1[r4] = r2
                        java.lang.String r0 = java.lang.String.format(r0, r1)
                        java.lang.String r1 = "PlugManager"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        r2.<init>()
                        java.lang.String r3 = "plug request url:"
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.StringBuilder r2 = r2.append(r0)
                        java.lang.String r2 = r2.toString()
                        com.pureapps.cleaner.util.f.b(r1, r2)
                        com.squareup.okhttp.r r2 = new com.squareup.okhttp.r
                        r2.<init>()
                        com.squareup.okhttp.s$a r1 = new com.squareup.okhttp.s$a
                        r1.<init>()
                        com.squareup.okhttp.s$a r0 = r1.a(r0)
                        com.squareup.okhttp.s r0 = r0.b()
                        r1 = 0
                        com.squareup.okhttp.e r0 = r2.a(r0)     // Catch:{ Exception -> 0x014c }
                        com.squareup.okhttp.u r0 = r0.a()     // Catch:{ Exception -> 0x014c }
                        com.squareup.okhttp.v r0 = r0.g()     // Catch:{ Exception -> 0x014c }
                        java.lang.String r0 = r0.e()     // Catch:{ Exception -> 0x014c }
                        java.lang.String r1 = "PlugManager"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016d }
                        r2.<init>()     // Catch:{ Exception -> 0x016d }
                        java.lang.String r3 = "plug request result:"
                        java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x016d }
                        java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x016d }
                        java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x016d }
                        com.pureapps.cleaner.util.f.b(r1, r2)     // Catch:{ Exception -> 0x016d }
                    L_0x00c7:
                        com.kingouser.com.entity.PlugEntity r1 = new com.kingouser.com.entity.PlugEntity
                        r1.<init>(r0, r4)
                        com.kingouser.com.entity.PlugEntity r2 = new com.kingouser.com.entity.PlugEntity
                        r2.<init>(r0, r6)
                        android.content.Context r0 = r6
                        android.content.pm.PackageManager r0 = r0.getPackageManager()
                        java.lang.String r3 = r0
                        android.content.pm.PackageInfo r0 = r0.getPackageArchiveInfo(r3, r4)
                        android.content.Context r3 = r6
                        boolean r0 = com.pureapps.cleaner.manager.h.b(r3, r0, r1)
                        if (r0 == 0) goto L_0x0155
                        java.lang.String r0 = "PlugManager"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "result plug will be install:"
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r4 = r0
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        com.pureapps.cleaner.util.f.b(r0, r3)
                        android.content.Context r0 = r6
                        com.pureapps.cleaner.util.i r0 = com.pureapps.cleaner.util.i.a(r0)
                        r0.a(r1)
                        java.lang.String r0 = r0
                        r9.a(r0)
                    L_0x010d:
                        android.content.Context r0 = r6
                        java.lang.String r1 = r2
                        boolean r0 = com.pureapps.cleaner.manager.h.b(r0, r2, r1, r6)
                        if (r0 == 0) goto L_0x0161
                        java.lang.String r0 = "PlugManager"
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder
                        r1.<init>()
                        java.lang.String r3 = "float plug will be install:"
                        java.lang.StringBuilder r1 = r1.append(r3)
                        java.lang.String r3 = r2
                        java.lang.StringBuilder r1 = r1.append(r3)
                        java.lang.String r1 = r1.toString()
                        com.pureapps.cleaner.util.f.b(r0, r1)
                        android.content.Context r0 = r6
                        com.pureapps.cleaner.util.i r0 = com.pureapps.cleaner.util.i.a(r0)
                        r0.a(r2)
                        java.lang.String r0 = r2
                        r9.a(r0)
                    L_0x013f:
                        boolean unused = com.pureapps.cleaner.manager.h.f5806c = r5
                        r9.a()
                        return
                    L_0x0146:
                        r0 = move-exception
                        r0.printStackTrace()
                        goto L_0x0008
                    L_0x014c:
                        r0 = move-exception
                        r7 = r0
                        r0 = r1
                        r1 = r7
                    L_0x0150:
                        r1.printStackTrace()
                        goto L_0x00c7
                    L_0x0155:
                        android.content.Context r0 = r6
                        com.pureapps.cleaner.analytic.a r0 = com.pureapps.cleaner.analytic.a.a(r0)
                        java.lang.String r1 = "down failed..."
                        r0.b(r1)
                        goto L_0x010d
                    L_0x0161:
                        android.content.Context r0 = r6
                        com.pureapps.cleaner.analytic.a r0 = com.pureapps.cleaner.analytic.a.a(r0)
                        java.lang.String r1 = "down failed..."
                        r0.b(r1)
                        goto L_0x013f
                    L_0x016d:
                        r1 = move-exception
                        goto L_0x0150
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.pureapps.cleaner.manager.h.AnonymousClass4.subscribe(io.reactivex.e):void");
                }
            }, BackpressureStrategy.ERROR).b(io.reactivex.c.a.a()).a(RxAndroidPlugins.onMainThreadScheduler(f5805b)).a(new io.reactivex.a.d<String>() {
                /* renamed from: a */
                public void accept(String str) {
                    InstallResult installPackage = VirtualCore.get().installPackage(str, 8);
                    if (str.equals(str)) {
                        h.a(h.c());
                    } else if (str.equals(str2)) {
                        h.a(h.g());
                    } else if (str.equals(str3)) {
                        boolean unused = h.f5806c = false;
                        h.a(h.h());
                    }
                    if (installPackage == null) {
                        com.pureapps.cleaner.analytic.a.a(context).a("InstallResult is null", "null");
                    } else if (!installPackage.isSuccess) {
                        com.pureapps.cleaner.util.f.b("PlugManager", "failed message:" + installPackage.error);
                        if (!h.a(installPackage.packageName)) {
                            com.pureapps.cleaner.analytic.a.a(context).a(installPackage.error, installPackage.packageName);
                        }
                    }
                    PackageInfo packageArchiveInfo = context.getPackageManager().getPackageArchiveInfo(str, 1);
                    if (packageArchiveInfo != null) {
                        Log.e("PlugManager", packageArchiveInfo.packageName + ":" + ((installPackage == null || !installPackage.isSuccess) ? "false" : "true version:" + packageArchiveInfo.versionCode));
                    }
                }
            }, new io.reactivex.a.d<Throwable>() {
                /* renamed from: a */
                public void accept(Throwable th) {
                    com.pureapps.cleaner.util.f.b("PlugManager", "出现异常!:" + th.getMessage());
                    boolean unused = h.f5806c = false;
                    com.pureapps.cleaner.analytic.a.a(context).b("RxjavaExcaption:" + th.getMessage());
                }
            });
        }
    }

    public static boolean a() {
        Intent intent = new Intent("com.kingoapp.plugin.ad.show.gift");
        intent.putExtra("Root", i());
        return a(intent);
    }

    public static boolean c(Context context) {
        PlugEntity plugEntity = i.a(context).a().get(2);
        if (plugEntity == null || !a(plugEntity.packagename)) {
            return false;
        }
        return true;
    }

    public static boolean d(Context context) {
        PlugEntity plugEntity = i.a(context).a().get(1);
        if (plugEntity != null && a(plugEntity.packagename)) {
            return true;
        }
        if (a(f5804a)) {
            return true;
        }
        return false;
    }

    public static boolean e(Context context) {
        PlugEntity plugEntity = i.a(context).a().get(3);
        if (plugEntity == null || !a(plugEntity.packagename)) {
            return false;
        }
        return true;
    }

    public static boolean b() {
        return f5806c;
    }

    /* access modifiers changed from: private */
    public static boolean b(Context context, PlugEntity plugEntity, String str, int i) {
        int i2 = 0;
        PlugEntity plugEntity2 = i.a(context).a().get(Integer.valueOf(i));
        if (i == 2 && (plugEntity2 == null || plugEntity2.packagename == null || plugEntity2.packagename.length() <= 0)) {
            i.a(context).c(false);
        }
        if (plugEntity == null || plugEntity.download.length() <= 0 || plugEntity.status != 0) {
            PackageInfo packageArchiveInfo = context.getPackageManager().getPackageArchiveInfo(str, 1);
            String str2 = plugEntity2 != null ? plugEntity2.packagename : packageArchiveInfo != null ? packageArchiveInfo.packageName : "";
            if (plugEntity2 != null) {
                if (PackageCacheManager.get(plugEntity2.packagename) != null) {
                    i2 = PackageCacheManager.get(plugEntity2.packagename).mVersionCode;
                } else {
                    i2 = plugEntity2.versioncode;
                }
            } else if (packageArchiveInfo != null) {
                i2 = packageArchiveInfo.versionCode;
            }
            if (plugEntity != null && plugEntity.packagename.length() > 0 && !plugEntity.packagename.equals(str2)) {
                e.a(str);
                b(str2);
                com.pureapps.cleaner.util.f.b("PlugManager", plugEntity.packagename + " find new package will down...");
                return cli.a.a.a(plugEntity.download, str);
            } else if (plugEntity != null && plugEntity.packagename.equals(str2) && i2 < plugEntity.versioncode) {
                e.a(str);
                com.pureapps.cleaner.util.f.b("PlugManager", plugEntity.packagename + " find new hight version will down...");
                f5806c = true;
                return cli.a.a.a(plugEntity.download, str);
            } else if (plugEntity == null || e.c(str) > 0) {
                return true;
            } else {
                com.pureapps.cleaner.util.f.b("PlugManager", plugEntity.packagename + " will down...");
                f5806c = true;
                return cli.a.a.a(plugEntity.download, str);
            }
        } else {
            if (plugEntity2 != null) {
                b(plugEntity2.packagename);
            }
            b(plugEntity.packagename);
            com.pureapps.cleaner.util.f.b("PlugManager", "plug will be unline...:" + plugEntity.packagename);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public static boolean b(Context context, PackageInfo packageInfo, PlugEntity plugEntity) {
        int i;
        PlugEntity plugEntity2 = i.a(context).a().get(1);
        if (plugEntity.packagename.length() <= 0 || plugEntity.status != 0) {
            String str = e.a(context) + File.separator + "result_plug.apk";
            String str2 = plugEntity2 != null ? plugEntity2.packagename : packageInfo != null ? packageInfo.packageName : f5804a;
            if (plugEntity2 != null) {
                if (PackageCacheManager.get(plugEntity2.packagename) != null) {
                    i = PackageCacheManager.get(plugEntity2.packagename).mVersionCode;
                } else {
                    i = plugEntity2.versioncode;
                }
            } else if (packageInfo != null) {
                i = packageInfo.versionCode;
            } else {
                i = 0;
            }
            if (plugEntity.packagename.length() > 0 && !plugEntity.packagename.equals(str2)) {
                b(str2);
                com.pureapps.cleaner.util.f.b("PlugManager", "result find new package will down...");
                e.a(str);
                return cli.a.a.a(plugEntity.download, str);
            } else if (packageInfo != null && packageInfo.versionCode > i) {
                com.pureapps.cleaner.util.f.b("PlugManager", "result local apk is hight not need down,will install...");
                return true;
            } else if (!plugEntity.packagename.equals(str2) || i >= plugEntity.versioncode) {
                return true;
            } else {
                com.pureapps.cleaner.util.f.b("PlugManager", "result find new hight version will down...");
                e.a(str);
                return cli.a.a.a(plugEntity.download, str);
            }
        } else {
            b(packageInfo.packageName);
            b(plugEntity.packagename);
            if (plugEntity2 != null) {
                b(plugEntity2.packagename);
            }
            com.pureapps.cleaner.util.f.b("PlugManager", "result will be unline...:" + plugEntity.packagename);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public static Intent g() {
        return new Intent("com.plug.swipe.start");
    }

    /* access modifiers changed from: private */
    public static Intent h() {
        Intent intent = new Intent("com.float.plug.start");
        intent.putExtra("Authorization", i.a(App.a()).f());
        intent.putExtra("Root", i());
        return intent;
    }

    private static boolean i() {
        try {
            return ShellUtils.checkSuVerison();
        } catch (Exception e2) {
            return false;
        }
    }

    public static Intent c() {
        return new Intent("com.kingoapp.plugin.ad.init.action");
    }

    public static Intent a(boolean z, int i) {
        Intent intent = new Intent("com.kingoapp.plugin.ad.load.action");
        intent.putExtra("isVip", z);
        intent.putExtra("type", i);
        return intent;
    }

    public static Intent a(int i, int i2, String str, boolean z, boolean z2, boolean z3) {
        Intent intent = new Intent("com.kingoapp.plugin.ad.show.action");
        intent.putExtra("type", i);
        intent.putExtra(FirebaseAnalytics.Param.VALUE, str);
        intent.putExtra("fromNotification", z);
        intent.putExtra("canbackhome", z2);
        intent.putExtra("isVip", z3);
        intent.putExtra("temptype", i2);
        return intent;
    }
}
