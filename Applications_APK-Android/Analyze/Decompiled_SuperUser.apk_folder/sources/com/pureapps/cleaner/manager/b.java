package com.pureapps.cleaner.manager;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.kingouser.com.R;
import com.kingouser.com.application.App;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.helper.utils.FileUtils;
import com.pureapps.cleaner.bean.j;
import com.pureapps.cleaner.bean.m;
import com.pureapps.cleaner.process.AndroidAppProcess;
import com.pureapps.cleaner.util.h;
import com.pureapps.cleaner.util.k;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: BoosterManager */
public class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f5703a;

    /* renamed from: b  reason: collision with root package name */
    private a f5704b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public boolean f5705c = false;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public ArrayList<j> f5706d = new ArrayList<>();

    public b(Context context, boolean z) {
        this.f5703a = context;
        this.f5705c = z;
    }

    public ArrayList<j> a() {
        return this.f5706d;
    }

    public void b() {
        k.a(this.f5704b, true);
        this.f5704b = new a();
        this.f5704b.executeOnExecutor(k.a().b(), new String[0]);
    }

    public boolean c() {
        return k.a(this.f5704b);
    }

    public void d() {
        k.a(this.f5704b, true);
    }

    public static boolean a(m mVar, String str) {
        if (mVar.f5655c.contains(str)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public long a(ActivityManager activityManager, int i) {
        try {
            return (long) (activityManager.getProcessMemoryInfo(new int[]{i})[0].getTotalPss() * FileUtils.FileMode.MODE_ISGID);
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    /* compiled from: BoosterManager */
    class a extends AsyncTask<String, com.pureapps.cleaner.bean.k, com.pureapps.cleaner.bean.k> {
        a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public com.pureapps.cleaner.bean.k doInBackground(String... strArr) {
            List runningAppProcesses;
            String str;
            String str2;
            Exception e2;
            PackageInfo packageInfo;
            ActivityManager activityManager = (ActivityManager) b.this.f5703a.getSystemService(ServiceManagerNative.ACTIVITY);
            PackageManager packageManager = b.this.f5703a.getApplicationContext().getPackageManager();
            if (Build.VERSION.SDK_INT > 23) {
                runningAppProcesses = com.pureapps.cleaner.process.a.a(activityManager);
            } else if (Build.VERSION.SDK_INT >= 21) {
                runningAppProcesses = com.pureapps.cleaner.process.a.a();
            } else {
                runningAppProcesses = activityManager.getRunningAppProcesses();
            }
            runningAppProcesses.addAll(activityManager.getRunningServices(Integer.MAX_VALUE));
            ArrayList<com.pureapps.cleaner.bean.a> a2 = com.pureapps.cleaner.bean.a.a(b.this.f5703a.getApplicationContext());
            m a3 = m.a();
            a3.a(b.this.f5703a.getApplicationContext());
            com.pureapps.cleaner.bean.k kVar = new com.pureapps.cleaner.bean.k();
            kVar.f5646a = 0;
            kVar.f5647b = b.this.f5703a.getString(R.string.ex);
            String str3 = "";
            String str4 = "";
            String a4 = h.a(packageManager);
            String a5 = h.a();
            String d2 = h.d(b.this.f5703a);
            String c2 = h.c(b.this.f5703a);
            String b2 = h.b(b.this.f5703a);
            String a6 = h.a(b.this.f5703a);
            int size = runningAppProcesses.size();
            int i = 0;
            while (i < size && !isCancelled()) {
                int i2 = 0;
                try {
                    ActivityManager.RunningAppProcessInfo runningAppProcessInfo = runningAppProcesses.get(i);
                    if (runningAppProcessInfo instanceof AndroidAppProcess) {
                        AndroidAppProcess androidAppProcess = (AndroidAppProcess) runningAppProcessInfo;
                        i2 = androidAppProcess.f5827d;
                        String str5 = androidAppProcess.f5826c;
                        str = androidAppProcess.a();
                        str2 = str5;
                    } else if (runningAppProcessInfo instanceof ActivityManager.RunningAppProcessInfo) {
                        ActivityManager.RunningAppProcessInfo runningAppProcessInfo2 = runningAppProcessInfo;
                        i2 = runningAppProcessInfo2.pid;
                        String str6 = runningAppProcessInfo2.processName;
                        str = runningAppProcessInfo2.pkgList[0];
                        str2 = str6;
                    } else if (runningAppProcessInfo instanceof ActivityManager.RunningServiceInfo) {
                        ActivityManager.RunningServiceInfo runningServiceInfo = (ActivityManager.RunningServiceInfo) runningAppProcessInfo;
                        i2 = runningServiceInfo.pid;
                        if (runningServiceInfo.service != null) {
                            String packageName = runningServiceInfo.service.getPackageName();
                            str = runningServiceInfo.service.getPackageName();
                            str2 = packageName;
                        } else {
                            String str7 = runningServiceInfo.process;
                            str = runningServiceInfo.process;
                            str2 = str7;
                        }
                    } else {
                        str = str4;
                        str2 = str3;
                    }
                    try {
                        if (!str2.contains("system") && !str2.contains("com.android") && !str.equals(io.fabric.sdk.android.services.common.a.ANDROID_CLIENT_TYPE)) {
                            if (!b.b(a3, str) && !a4.equals(str) && !a5.equals(str) && !d2.equals(str) && !c2.equals(str) && !b2.equals(str) && !a6.equals(str)) {
                                j jVar = new j();
                                try {
                                    packageInfo = packageManager.getPackageInfo(str, 0);
                                    if (!TextUtils.isEmpty(packageInfo.sharedUserId) && a3.f5658f.contains(packageInfo.sharedUserId)) {
                                    }
                                } catch (Exception e3) {
                                    e3.printStackTrace();
                                    packageInfo = null;
                                }
                                j a7 = b.this.b(kVar.f5648c, str);
                                if (a7 == null) {
                                    if (packageInfo != null) {
                                        jVar.f5639b = packageInfo.packageName;
                                        jVar.f5641d = packageInfo.applicationInfo.loadIcon(packageManager);
                                        jVar.f5640c = packageInfo.applicationInfo.loadLabel(packageManager).toString();
                                        jVar.f5642e = b.this.a(activityManager, i2);
                                        jVar.f5638a = 0;
                                        jVar.f5644g = b.a(a3, jVar.f5639b);
                                        if (jVar.f5642e > 0) {
                                            jVar.f5643f = com.pureapps.cleaner.process.b.a(i2);
                                            if (b.a(a2, str)) {
                                                b.this.f5706d.add(jVar);
                                            } else {
                                                kVar.f5648c.add(jVar);
                                                if (b.this.f5705c && jVar.f5644g) {
                                                    try {
                                                        activityManager.killBackgroundProcesses(jVar.f5639b);
                                                    } catch (Exception e4) {
                                                        e4.printStackTrace();
                                                    }
                                                }
                                                if (jVar.f5644g) {
                                                    kVar.f5649d++;
                                                }
                                                a7.f5642e = b.this.a(activityManager, i2) + a7.f5642e;
                                            }
                                        }
                                    } else {
                                        Log.d("test", "processName:" + str2);
                                    }
                                }
                                int i3 = (int) (((float) (i / size)) * 1.0f * 100.0f);
                                publishProgress(kVar);
                            }
                        }
                    } catch (Exception e5) {
                        e2 = e5;
                        e2.printStackTrace();
                        i++;
                        str3 = str2;
                        str4 = str;
                    }
                } catch (Exception e6) {
                    Exception exc = e6;
                    str = str4;
                    str2 = str3;
                    e2 = exc;
                    e2.printStackTrace();
                    i++;
                    str3 = str2;
                    str4 = str;
                }
                i++;
                str3 = str2;
                str4 = str;
            }
            return kVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(com.pureapps.cleaner.bean.k... kVarArr) {
            super.onProgressUpdate(kVarArr);
            com.pureapps.cleaner.b.a.a(8, 0, kVarArr[0]);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(com.pureapps.cleaner.bean.k kVar) {
            super.onPostExecute(kVar);
            com.pureapps.cleaner.b.a.a(9, 0, kVar);
        }
    }

    /* access modifiers changed from: private */
    public j b(ArrayList<j> arrayList, String str) {
        Iterator<j> it = arrayList.iterator();
        while (it.hasNext()) {
            j next = it.next();
            if (next.f5639b.equals(str)) {
                return next;
            }
        }
        return null;
    }

    public static boolean b(m mVar, String str) {
        if (!TextUtils.isEmpty(str) && !App.a().getPackageName().equals(str) && !mVar.f5659g.contains(str) && !mVar.f5654b.contains(str)) {
            return false;
        }
        return true;
    }

    public static boolean a(ArrayList<com.pureapps.cleaner.bean.a> arrayList, String str) {
        if (arrayList != null) {
            Iterator<com.pureapps.cleaner.bean.a> it = arrayList.iterator();
            while (it.hasNext()) {
                if (str.equals(it.next().f5608c)) {
                    return true;
                }
            }
        }
        return false;
    }
}
