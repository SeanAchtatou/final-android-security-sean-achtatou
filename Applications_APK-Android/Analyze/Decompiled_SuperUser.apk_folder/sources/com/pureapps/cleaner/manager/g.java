package com.pureapps.cleaner.manager;

import android.content.Context;
import android.os.SystemClock;
import com.pureapps.cleaner.util.k;
import com.squareup.okhttp.q;
import com.squareup.okhttp.r;
import com.squareup.okhttp.s;
import com.squareup.okhttp.t;
import com.squareup.okhttp.u;
import java.util.concurrent.TimeUnit;

/* compiled from: PayManager */
public class g {

    /* renamed from: a  reason: collision with root package name */
    public static g f5785a = null;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public r f5786b = null;

    /* renamed from: c  reason: collision with root package name */
    private final String f5787c = "RequestNeedPay";

    /* renamed from: d  reason: collision with root package name */
    private final String f5788d = "startVerifyPayResult";

    /* compiled from: PayManager */
    public interface a {
        void onBack(u uVar);
    }

    public static g a() {
        if (f5785a == null) {
            synchronized (g.class) {
                if (f5785a == null) {
                    f5785a = new g();
                }
            }
        }
        return f5785a;
    }

    public void a(final String str, final String str2, final a aVar) {
        k.a().a(new Runnable() {
            public void run() {
                try {
                    q a2 = q.a("application/json; charset=utf-8");
                    r rVar = new r();
                    rVar.a(10, TimeUnit.SECONDS);
                    u a3 = rVar.a(new s.a().a(str).a(t.a(a2, str2)).b()).a();
                    if (aVar != null) {
                        aVar.onBack(a3);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    if (aVar != null) {
                        aVar.onBack(null);
                    }
                }
            }
        });
    }

    public void a(Context context, final String str, final a aVar) {
        final String str2 = com.kingouser.com.b.a.j;
        k.a().a(new Runnable() {
            public void run() {
                try {
                    SystemClock.sleep(3000);
                    q a2 = q.a("application/json; charset=utf-8");
                    if (g.this.f5786b == null) {
                        r unused = g.this.f5786b = new r();
                        g.this.f5786b.a(5, TimeUnit.SECONDS);
                    }
                    g.this.f5786b.a("startVerifyPayResult");
                    u a3 = g.this.f5786b.a(new s.a().a(str2).a(t.a(a2, str)).a((Object) "startVerifyPayResult").b()).a();
                    if (aVar != null) {
                        aVar.onBack(a3);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    if (aVar != null) {
                        aVar.onBack(null);
                    }
                }
            }
        });
    }

    public void b(final String str, final String str2, final a aVar) {
        k.a().a(new Runnable() {
            public void run() {
                try {
                    SystemClock.sleep(3000);
                    q a2 = q.a("application/json; charset=utf-8");
                    if (g.this.f5786b == null) {
                        r unused = g.this.f5786b = new r();
                        g.this.f5786b.a(5, TimeUnit.SECONDS);
                    }
                    g.this.f5786b.a("startVerifyPayResult");
                    u a3 = g.this.f5786b.a(new s.a().a(str2).a(t.a(a2, str)).a((Object) "startVerifyPayResult").b()).a();
                    if (aVar != null) {
                        aVar.onBack(a3);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    if (aVar != null) {
                        aVar.onBack(null);
                    }
                }
            }
        });
    }

    public void a(final String str, final a aVar) {
        k.a().a(new Runnable() {
            public void run() {
                try {
                    if (g.this.f5786b != null) {
                        g.this.f5786b.a("RequestNeedPay");
                    } else {
                        r unused = g.this.f5786b = new r();
                        g.this.f5786b.a(10, TimeUnit.SECONDS);
                    }
                    u a2 = g.this.f5786b.a(new s.a().a(str).a((Object) "RequestNeedPay").b()).a();
                    if (aVar != null) {
                        aVar.onBack(a2);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    if (aVar != null) {
                        aVar.onBack(null);
                    }
                }
            }
        });
    }
}
