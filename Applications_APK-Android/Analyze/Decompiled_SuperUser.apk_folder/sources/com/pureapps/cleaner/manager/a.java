package com.pureapps.cleaner.manager;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.duapps.ad.AdError;
import com.duapps.ad.DuAdListener;
import com.duapps.ad.DuNativeAd;
import com.kingouser.com.R;
import com.kingouser.com.application.App;
import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.client.ipc.VActivityManager;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.view.FlashButton;

/* compiled from: AdManager */
public class a {

    /* renamed from: b  reason: collision with root package name */
    private static a f5687b = null;

    /* renamed from: a  reason: collision with root package name */
    private final String f5688a = "wyy";
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public DuNativeAd f5689c = null;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public View f5690d;

    /* renamed from: com.pureapps.cleaner.manager.a$a  reason: collision with other inner class name */
    /* compiled from: AdManager */
    public interface C0089a {
        void a();

        void a(DuNativeAd duNativeAd);

        void b();
    }

    public static a a() {
        if (f5687b == null) {
            synchronized (a.class) {
                if (f5687b == null) {
                    f5687b = new a();
                }
            }
        }
        return f5687b;
    }

    private int c(int i) {
        switch (i) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 12:
            case 13:
            case 14:
            default:
                return 140819;
            case 11:
                return 140820;
        }
    }

    public View b() {
        return this.f5690d;
    }

    public View a(Context context) {
        return LayoutInflater.from(context).inflate((int) R.layout.cu, (ViewGroup) null);
    }

    public void a(int i) {
        int i2 = 0;
        b(i);
        switch (i) {
            case 1:
                i2 = 2;
                break;
            case 6:
                i2 = 1;
                break;
            case 9:
                i2 = 4;
                break;
        }
        try {
            VActivityManager.get().startService(null, h.a(i.a(App.a()).f(), i2), null, 0);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a(int i, final ViewGroup viewGroup, final C0089a aVar) {
        if (!i.a(App.a()).f()) {
            final View inflate = LayoutInflater.from(App.a()).inflate((int) R.layout.be, (ViewGroup) null);
            DuNativeAd duNativeAd = new DuNativeAd(App.a(), c(i), 2);
            duNativeAd.setMobulaAdListener(new DuAdListener() {
                public void onError(DuNativeAd duNativeAd, AdError adError) {
                    if (aVar != null) {
                        aVar.a();
                    }
                }

                public void onAdLoaded(DuNativeAd duNativeAd) {
                    TextView textView = (TextView) inflate.findViewById(R.id.il);
                    textView.setSelected(true);
                    textView.setText(duNativeAd.getTitle());
                    ((RatingBar) inflate.findViewById(R.id.im)).setRating(duNativeAd.getRatings());
                    e.b(App.a()).a(duNativeAd.getIconUrl()).b(DiskCacheStrategy.ALL).a((ImageView) inflate.findViewById(R.id.ik));
                    ((TextView) inflate.findViewById(R.id.in)).setText(duNativeAd.getCallToAction());
                    duNativeAd.registerViewForInteraction((FrameLayout) inflate.findViewById(R.id.ii));
                    viewGroup.addView(inflate);
                    ImageView imageView = (ImageView) inflate.findViewById(R.id.ij);
                    imageView.getLayoutParams().height = App.a().getResources().getDisplayMetrics().heightPixels / 4;
                    String imageUrl = duNativeAd.getImageUrl();
                    if (!TextUtils.isEmpty(imageUrl)) {
                        e.b(App.a()).a(imageUrl).b(DiskCacheStrategy.ALL).a(imageView);
                    }
                    if (aVar != null) {
                        aVar.a(duNativeAd);
                    }
                }

                public void onClick(DuNativeAd duNativeAd) {
                    if (aVar != null) {
                        aVar.b();
                    }
                }
            });
            duNativeAd.load();
        }
    }

    public void b(int i, final ViewGroup viewGroup, final C0089a aVar) {
        if (!i.a(App.a()).f()) {
            viewGroup.setVisibility(4);
            final View inflate = LayoutInflater.from(App.a()).inflate((int) R.layout.bf, (ViewGroup) null);
            inflate.findViewById(R.id.ir).setVisibility(ShellUtils.checkSuVerison() ? 0 : 8);
            DuNativeAd duNativeAd = new DuNativeAd(App.a(), c(i), 2);
            duNativeAd.setMobulaAdListener(new DuAdListener() {
                public void onError(DuNativeAd duNativeAd, AdError adError) {
                    if (aVar != null) {
                        aVar.a();
                    }
                }

                public void onAdLoaded(DuNativeAd duNativeAd) {
                    viewGroup.setVisibility(0);
                    viewGroup.addView(inflate);
                    ((FrameLayout) inflate.findViewById(R.id.f8338io)).getLayoutParams().width = (int) (((float) App.a().getResources().getDisplayMetrics().widthPixels) * 0.92f);
                    View findViewById = inflate.findViewById(R.id.ii);
                    TextView textView = (TextView) inflate.findViewById(R.id.il);
                    textView.setSelected(true);
                    TextView textView2 = (TextView) inflate.findViewById(R.id.iq);
                    ImageView imageView = (ImageView) inflate.findViewById(R.id.ij);
                    FlashButton flashButton = (FlashButton) inflate.findViewById(R.id.in);
                    ((FrameLayout) inflate.findViewById(R.id.ip)).getLayoutParams().height = App.a().getResources().getDisplayMetrics().heightPixels / 4;
                    String imageUrl = duNativeAd.getImageUrl();
                    RatingBar ratingBar = (RatingBar) inflate.findViewById(R.id.im);
                    e.b(App.a()).a(duNativeAd.getIconUrl()).b(DiskCacheStrategy.ALL).a((ImageView) inflate.findViewById(R.id.ik));
                    if (TextUtils.isEmpty(imageUrl)) {
                        textView.setText(duNativeAd.getTitle());
                        ratingBar.setRating(duNativeAd.getRatings());
                        textView2.setText(duNativeAd.getShortDesc());
                        flashButton.setText(duNativeAd.getCallToAction());
                        duNativeAd.registerViewForInteraction(findViewById);
                        return;
                    }
                    textView.setText(duNativeAd.getTitle());
                    ratingBar.setRating(duNativeAd.getRatings());
                    textView2.setText(duNativeAd.getShortDesc());
                    flashButton.setText(duNativeAd.getCallToAction());
                    duNativeAd.registerViewForInteraction(findViewById);
                    e.b(App.a()).a(imageUrl).b(DiskCacheStrategy.ALL).a(imageView);
                    if (aVar != null) {
                        aVar.a(duNativeAd);
                    }
                }

                public void onClick(DuNativeAd duNativeAd) {
                    if (aVar != null) {
                        aVar.b();
                    }
                }
            });
            duNativeAd.load();
        }
    }

    public void b(int i) {
        this.f5690d = LayoutInflater.from(App.a()).inflate((int) R.layout.cj, (ViewGroup) null);
        final FrameLayout frameLayout = (FrameLayout) this.f5690d.findViewById(R.id.lq);
        final LinearLayout linearLayout = (LinearLayout) this.f5690d.findViewById(R.id.d9);
        final LinearLayout linearLayout2 = (LinearLayout) this.f5690d.findViewById(R.id.lr);
        frameLayout.setVisibility(8);
        linearLayout.setVisibility(8);
        linearLayout2.setVisibility(8);
        DuNativeAd duNativeAd = new DuNativeAd(App.a(), c(i), 2);
        duNativeAd.setMobulaAdListener(new DuAdListener() {
            public void onError(DuNativeAd duNativeAd, AdError adError) {
                Log.e("wyy", adError.getErrorCode() + ":" + adError.getErrorMessage());
            }

            public void onAdLoaded(DuNativeAd duNativeAd) {
                DuNativeAd unused = a.this.f5689c = duNativeAd;
                frameLayout.setVisibility(0);
                linearLayout.setVisibility(0);
                linearLayout2.setVisibility(0);
                FrameLayout frameLayout = (FrameLayout) a.this.f5690d.findViewById(R.id.ii);
                TextView textView = (TextView) a.this.f5690d.findViewById(R.id.il);
                RatingBar ratingBar = (RatingBar) a.this.f5690d.findViewById(R.id.im);
                TextView textView2 = (TextView) a.this.f5690d.findViewById(R.id.iq);
                ImageView imageView = (ImageView) a.this.f5690d.findViewById(R.id.ij);
                FlashButton flashButton = (FlashButton) a.this.f5690d.findViewById(R.id.in);
                String imageUrl = duNativeAd.getImageUrl();
                e.b(App.a()).a(duNativeAd.getIconUrl()).b(DiskCacheStrategy.ALL).a((ImageView) a.this.f5690d.findViewById(R.id.ik));
                if (TextUtils.isEmpty(imageUrl)) {
                    textView.setText(duNativeAd.getTitle());
                    ratingBar.setRating(duNativeAd.getRatings());
                    textView2.setText(duNativeAd.getShortDesc());
                    flashButton.setText(duNativeAd.getCallToAction());
                    duNativeAd.registerViewForInteraction(frameLayout);
                    imageView.setVisibility(8);
                    return;
                }
                imageView.setVisibility(0);
                textView.setText(duNativeAd.getTitle());
                ratingBar.setRating(duNativeAd.getRatings());
                e.b(App.a()).a(imageUrl).b(DiskCacheStrategy.ALL).a(imageView);
                textView2.setText(duNativeAd.getShortDesc());
                flashButton.setText(duNativeAd.getCallToAction());
                duNativeAd.registerViewForInteraction(frameLayout);
            }

            public void onClick(DuNativeAd duNativeAd) {
            }
        });
        if (!i.a(App.a()).f()) {
            duNativeAd.load();
        }
    }

    public void c() {
        if (this.f5689c != null) {
            this.f5689c = null;
        }
    }
}
