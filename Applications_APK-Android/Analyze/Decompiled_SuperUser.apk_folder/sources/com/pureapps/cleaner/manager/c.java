package com.pureapps.cleaner.manager;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import com.kingouser.com.R;
import com.pureapps.cleaner.view.FloatView;

/* compiled from: FloatManager */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static c f5708a = null;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static WindowManager f5709b = null;

    /* renamed from: c  reason: collision with root package name */
    private static WindowManager.LayoutParams f5710c = null;

    public static c a() {
        if (f5708a == null) {
            synchronized (c.class) {
                if (f5708a == null) {
                    f5708a = new c();
                }
            }
        }
        return f5708a;
    }

    public static void a(Context context) {
        f5709b = (WindowManager) context.getApplicationContext().getSystemService("window");
        f5710c = new WindowManager.LayoutParams();
        f5710c.type = com.pureapps.cleaner.util.c.a(false);
        f5710c.format = -2;
        f5710c.x = 0;
        f5710c.y = 0;
        f5710c.width = -1;
        f5710c.height = -1;
        f5710c.flags = 262176;
        final View inflate = LayoutInflater.from(context).inflate((int) R.layout.bz, (ViewGroup) null);
        inflate.setFocusableInTouchMode(true);
        final ImageView imageView = (ImageView) inflate.findViewById(R.id.jl);
        final View findViewById = inflate.findViewById(R.id.jo);
        findViewById.setAlpha(0.0f);
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(findViewById, "alpha", 0.2f, 0.6f);
        ofFloat.setRepeatMode(2);
        ofFloat.setRepeatCount(1);
        ofFloat.setDuration(100L);
        ofFloat.addListener(new Animator.AnimatorListener() {
            public void onAnimationStart(Animator animator) {
                findViewById.setVisibility(0);
            }

            public void onAnimationEnd(Animator animator) {
                findViewById.setAlpha(0.0f);
            }

            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }
        });
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(imageView, "translationY", imageView.getTranslationY() + (80.0f * context.getResources().getDisplayMetrics().density), imageView.getTranslationY());
        ofFloat2.setDuration(1200L);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(imageView, "scaleX", 1.0f, 0.6f);
        ofFloat3.setDuration(600L);
        ofFloat3.setRepeatCount(1);
        ofFloat3.setRepeatMode(2);
        ObjectAnimator ofFloat4 = ObjectAnimator.ofFloat(imageView, "scaleY", 1.0f, 0.6f);
        ofFloat4.setDuration(600L);
        ofFloat4.setRepeatCount(1);
        ofFloat4.setRepeatMode(2);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.play(ofFloat3).with(ofFloat4);
        final AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.play(animatorSet).after(ofFloat2);
        animatorSet2.start();
        ofFloat4.addListener(new Animator.AnimatorListener() {
            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                animatorSet2.start();
            }

            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
                ofFloat.start();
            }
        });
        inflate.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i != 4) {
                    return false;
                }
                try {
                    c.f5709b.removeViewImmediate(inflate);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                animatorSet2.cancel();
                imageView.clearAnimation();
                return false;
            }
        });
        inflate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    c.f5709b.removeViewImmediate(inflate);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                animatorSet2.cancel();
                imageView.clearAnimation();
            }
        });
        FloatView floatView = (FloatView) inflate.findViewById(R.id.jn);
        floatView.setFocusable(true);
        floatView.setOnBackClickListener(new FloatView.a() {
            public void a() {
                try {
                    c.f5709b.removeViewImmediate(inflate);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                animatorSet2.cancel();
                imageView.clearAnimation();
            }
        });
        try {
            f5709b.addView(inflate, f5710c);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
