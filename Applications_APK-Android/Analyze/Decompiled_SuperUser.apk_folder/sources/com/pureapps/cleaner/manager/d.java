package com.pureapps.cleaner.manager;

import android.os.Handler;
import android.os.Message;
import io.reactivex.disposables.c;
import io.reactivex.h;
import java.util.concurrent.TimeUnit;

/* compiled from: HandlerScheduler */
final class d extends h {

    /* renamed from: a  reason: collision with root package name */
    private final Handler f5723a;

    d(Handler handler) {
        this.f5723a = handler;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public io.reactivex.disposables.b scheduleDirect(Runnable runnable, long j, TimeUnit timeUnit) {
        if (runnable == null) {
            throw new NullPointerException("run == null");
        } else if (timeUnit == null) {
            throw new NullPointerException("unit == null");
        } else {
            b bVar = new b(this.f5723a, io.reactivex.b.a.a(runnable));
            this.f5723a.postDelayed(bVar, Math.max(0L, timeUnit.toMillis(j)));
            return bVar;
        }
    }

    public h.c createWorker() {
        return new a(this.f5723a);
    }

    /* compiled from: HandlerScheduler */
    private static final class b implements io.reactivex.disposables.b, Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final Handler f5726a;

        /* renamed from: b  reason: collision with root package name */
        private final Runnable f5727b;

        /* renamed from: c  reason: collision with root package name */
        private volatile boolean f5728c;

        b(Handler handler, Runnable runnable) {
            this.f5726a = handler;
            this.f5727b = runnable;
        }

        public void run() {
            try {
                this.f5727b.run();
            } catch (Throwable th) {
                IllegalStateException illegalStateException = new IllegalStateException("Fatal Exception thrown on Scheduler.", th);
                io.reactivex.b.a.a(illegalStateException);
                Thread currentThread = Thread.currentThread();
                currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, illegalStateException);
            }
        }

        public void dispose() {
            this.f5728c = true;
            this.f5726a.removeCallbacks(this);
        }
    }

    /* compiled from: HandlerScheduler */
    private static final class a extends h.c {

        /* renamed from: a  reason: collision with root package name */
        private final Handler f5724a;

        /* renamed from: b  reason: collision with root package name */
        private volatile boolean f5725b;

        a(Handler handler) {
            this.f5724a = handler;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [int, long]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        public io.reactivex.disposables.b schedule(Runnable runnable, long j, TimeUnit timeUnit) {
            if (runnable == null) {
                throw new NullPointerException("run == null");
            } else if (timeUnit == null) {
                throw new NullPointerException("unit == null");
            } else if (this.f5725b) {
                return c.a();
            } else {
                b bVar = new b(this.f5724a, io.reactivex.b.a.a(runnable));
                Message obtain = Message.obtain(this.f5724a, bVar);
                obtain.obj = this;
                this.f5724a.sendMessageDelayed(obtain, Math.max(0L, timeUnit.toMillis(j)));
                if (!this.f5725b) {
                    return bVar;
                }
                this.f5724a.removeCallbacks(bVar);
                return c.a();
            }
        }

        public void dispose() {
            this.f5725b = true;
            this.f5724a.removeCallbacksAndMessages(this);
        }
    }
}
