package com.pureapps.cleaner.manager;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.MainActivity;
import com.kingouser.com.R;
import com.kingouser.com.application.App;
import com.lody.virtual.helper.utils.FileUtils;
import com.pureapps.cleaner.MemoryBoostActivity;
import com.pureapps.cleaner.NotificationManagerActivity;
import com.pureapps.cleaner.bean.m;
import com.pureapps.cleaner.net.NetworkStatus;
import com.pureapps.cleaner.service.BackService;
import com.pureapps.cleaner.service.NotificationMonitorService;
import com.pureapps.cleaner.util.b;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.k;
import com.pureapps.cleaner.util.l;
import com.pureapps.cleaner.view.NotificationMemoryView;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: NotificationManager */
public class f {

    /* renamed from: b  reason: collision with root package name */
    private static f f5776b;

    /* renamed from: a  reason: collision with root package name */
    public NotificationManagerCompat f5777a;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Context f5778c;

    /* renamed from: d  reason: collision with root package name */
    private final long f5779d = 10800000;

    /* renamed from: e  reason: collision with root package name */
    private final int f5780e = 16;

    /* renamed from: f  reason: collision with root package name */
    private final int f5781f = 32;

    /* renamed from: g  reason: collision with root package name */
    private final int f5782g = 48;

    /* renamed from: h  reason: collision with root package name */
    private final int f5783h = 64;
    private final int i = 80;
    private final int j = 96;
    private final int k = 112;

    private f(Context context) {
        this.f5778c = context.getApplicationContext();
        this.f5777a = NotificationManagerCompat.a(this.f5778c);
    }

    public static f a(Context context) {
        if (f5776b == null) {
            f5776b = new f(context);
        }
        return f5776b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a() {
        if (this.f5778c == null) {
            this.f5778c = App.a();
        }
        if (this.f5778c != null) {
            Intent intent = new Intent(this.f5778c, MainActivity.class);
            intent.setFlags(603979776);
            intent.putExtra("notification_root_update_click", true);
            PendingIntent activity = PendingIntent.getActivity(this.f5778c, 120, intent, 134217728);
            RemoteViews remoteViews = new RemoteViews(this.f5778c.getPackageName(), (int) R.layout.cw);
            remoteViews.setTextViewText(R.id.f6, this.f5778c.getString(R.string.de));
            remoteViews.setTextViewText(R.id.m7, this.f5778c.getString(17039370));
            Notification b2 = new NotificationCompat.Builder(this.f5778c).a((int) R.drawable.ga).a(remoteViews).a(activity).c(this.f5778c.getString(R.string.de)).b();
            b2.flags = 16;
            if (Build.VERSION.SDK_INT >= 16) {
                b2.priority = 1;
            }
            try {
                this.f5777a.a(112, b2);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            com.pureapps.cleaner.analytic.a.a(this.f5778c).a(FirebaseAnalytics.getInstance(this.f5778c), "NotificationUpdateRootPush");
        }
    }

    public void b() {
        PendingIntent broadcast = PendingIntent.getBroadcast(this.f5778c, 110, new Intent("notification_update_action"), 134217728);
        Notification b2 = new NotificationCompat.Builder(this.f5778c).a((int) R.drawable.ga).a(broadcast).c(this.f5778c.getString(R.string.dh)).a(new RemoteViews(this.f5778c.getPackageName(), (int) R.layout.cw)).b();
        b2.flags = 16;
        if (Build.VERSION.SDK_INT >= 16) {
            b2.priority = 1;
        }
        this.f5777a.a(64, b2);
        com.pureapps.cleaner.analytic.a.a(this.f5778c).a(FirebaseAnalytics.getInstance(this.f5778c), "NotificationUpdatePush");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void c() {
        int i2 = 8;
        if (NotificationMonitorService.f5847a != null && NotificationMonitorService.f5847a.size() != 0) {
            Intent intent = new Intent(this.f5778c, NotificationManagerActivity.class);
            intent.putExtra("canbackhome", false);
            intent.setFlags(268435456);
            PendingIntent activity = PendingIntent.getActivity(this.f5778c, 10, intent, 134217728);
            RemoteViews remoteViews = new RemoteViews(this.f5778c.getPackageName(), (int) R.layout.c1);
            remoteViews.setViewVisibility(R.id.jr, NotificationMonitorService.f5847a.size() >= 2 ? 0 : 8);
            if (NotificationMonitorService.f5847a.size() >= 3) {
                i2 = 0;
            }
            remoteViews.setViewVisibility(R.id.js, i2);
            if (NotificationMonitorService.f5847a.size() >= 1) {
                a(remoteViews, NotificationMonitorService.f5847a.get(0).d(), R.id.jq);
            }
            if (NotificationMonitorService.f5847a.size() >= 2) {
                a(remoteViews, NotificationMonitorService.f5847a.get(1).d(), R.id.jr);
            }
            if (NotificationMonitorService.f5847a.size() >= 3) {
                a(remoteViews, NotificationMonitorService.f5847a.get(2).d(), R.id.js);
            }
            String valueOf = String.valueOf(NotificationMonitorService.f5847a.size());
            String string = this.f5778c.getString(R.string.e4, valueOf);
            SpannableString spannableString = new SpannableString(string);
            spannableString.setSpan(new ForegroundColorSpan(-16777216), 0, string.length(), 33);
            spannableString.setSpan(new ForegroundColorSpan(-65536), string.indexOf(valueOf), string.length(), 33);
            remoteViews.setTextViewText(R.id.bk, spannableString);
            Notification b2 = new NotificationCompat.Builder(this.f5778c).a((int) R.drawable.gb).b(remoteViews).a(remoteViews).a(activity).a(true).b();
            if (Build.VERSION.SDK_INT >= 16) {
                b2.priority = 1;
            }
            this.f5777a.a(96, b2);
        }
    }

    private void a(RemoteViews remoteViews, String str, int i2) {
        Bitmap a2;
        try {
            Drawable loadIcon = this.f5778c.getPackageManager().getPackageInfo(str, 8192).applicationInfo.loadIcon(this.f5778c.getPackageManager());
            if (loadIcon != null) {
                a2 = b.a(loadIcon, 50, 50);
            } else {
                a2 = b.a(this.f5778c.getPackageManager().getDefaultActivityIcon(), 50, 50);
            }
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            a2 = b.a(this.f5778c.getPackageManager().getDefaultActivityIcon(), 50, 50);
        }
        remoteViews.setImageViewBitmap(i2, a2);
    }

    public void d() {
        this.f5777a.a(96);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void a(RemoteViews remoteViews) {
        remoteViews.setTextViewText(R.id.f8334e, this.f5778c.getString(R.string.d7));
        remoteViews.setTextViewText(R.id.ki, this.f5778c.getString(R.string.d1));
        remoteViews.setTextViewText(R.id.km, this.f5778c.getString(R.string.d4));
        remoteViews.setTextViewText(R.id.ko, this.f5778c.getString(R.string.d2));
        if (NetworkStatus.a().a(this.f5778c)) {
            remoteViews.setImageViewResource(R.id.kt, R.drawable.gi);
        } else {
            remoteViews.setImageViewResource(R.id.kt, R.drawable.gh);
        }
        AudioManager audioManager = (AudioManager) this.f5778c.getSystemService("audio");
        if (audioManager == null || audioManager.getRingerMode() != 0) {
            remoteViews.setImageViewResource(R.id.kq, R.drawable.ge);
            remoteViews.setTextViewText(R.id.kr, this.f5778c.getString(R.string.dd));
        } else {
            remoteViews.setImageViewResource(R.id.kq, R.drawable.gd);
            remoteViews.setTextViewText(R.id.kr, this.f5778c.getString(R.string.dc));
        }
        int e2 = l.e(this.f5778c);
        if (i.a(this.f5778c).t() == 0) {
            remoteViews.setTextViewText(R.id.kl, e2 + this.f5778c.getString(R.string.ee));
        } else {
            remoteViews.setTextViewText(R.id.kl, e2 + this.f5778c.getString(R.string.ef));
            e2 = (int) (((float) (e2 - 32)) / 1.8f);
        }
        if (e2 <= 25) {
            remoteViews.setImageViewResource(R.id.kk, R.drawable.f6);
        } else if (e2 <= 25 || e2 >= 50) {
            remoteViews.setImageViewResource(R.id.kk, R.drawable.f8);
        } else {
            remoteViews.setImageViewResource(R.id.kk, R.drawable.f7);
        }
        Intent intent = new Intent(this.f5778c, MainActivity.class);
        intent.setFlags(603979776);
        intent.putExtra("notification_click_event_action", "BtnNotificationHome");
        remoteViews.setOnClickPendingIntent(R.id.ke, PendingIntent.getActivity(this.f5778c, 20, intent, 134217728));
        Intent intent2 = new Intent(this.f5778c, MemoryBoostActivity.class);
        intent2.putExtra("notification_cpu_click", true);
        intent2.putExtra("notification_click_event_action", "BtnNotificationCpu");
        remoteViews.setOnClickPendingIntent(R.id.kj, PendingIntent.getActivity(this.f5778c, 30, intent2, 134217728));
        Intent intent3 = new Intent(this.f5778c, MemoryBoostActivity.class);
        intent3.putExtra("notification_from_click", true);
        intent3.putExtra("notification_click_event_action", "BtnNotificationBoost");
        remoteViews.setOnClickPendingIntent(R.id.kf, PendingIntent.getActivity(this.f5778c, 40, intent3, 134217728));
        remoteViews.setOnClickPendingIntent(R.id.kn, PendingIntent.getBroadcast(this.f5778c, 50, new Intent("notification_calculator"), 134217728));
        remoteViews.setOnClickPendingIntent(R.id.ks, PendingIntent.getBroadcast(this.f5778c, 60, new Intent("notification_wifi"), 134217728));
        remoteViews.setOnClickPendingIntent(R.id.kp, PendingIntent.getBroadcast(this.f5778c, 70, new Intent("notification_ringer"), 134217728));
        long i2 = l.i(this.f5778c);
        float h2 = (float) (((i2 - l.h(this.f5778c)) * 100) / i2);
        remoteViews.setTextViewText(R.id.kh, ((int) h2) + "%");
        int dimensionPixelSize = this.f5778c.getResources().getDimensionPixelSize(R.dimen.fk);
        int dimensionPixelSize2 = this.f5778c.getResources().getDimensionPixelSize(R.dimen.fk);
        NotificationMemoryView notificationMemoryView = new NotificationMemoryView(this.f5778c);
        notificationMemoryView.measure(dimensionPixelSize, dimensionPixelSize2);
        notificationMemoryView.layout(0, 0, dimensionPixelSize, dimensionPixelSize2);
        notificationMemoryView.setDrawingCacheEnabled(true);
        notificationMemoryView.setPercent((int) h2);
        remoteViews.setImageViewBitmap(R.id.kg, notificationMemoryView.getDrawingCache());
    }

    public void e() {
        BackService.f5838a = false;
        this.f5777a.a(16);
    }

    public void f() {
        this.f5777a.a(32);
    }

    public void g() {
        RemoteViews remoteViews;
        Intent intent = new Intent(this.f5778c, MainActivity.class);
        intent.setFlags(268435456);
        if (i.a(this.f5778c).k() == 1) {
            remoteViews = new RemoteViews(this.f5778c.getPackageName(), (int) R.layout.ca);
        } else {
            remoteViews = new RemoteViews(this.f5778c.getPackageName(), (int) R.layout.cb);
        }
        PendingIntent activity = PendingIntent.getActivity(this.f5778c, 80, intent, 134217728);
        a(remoteViews);
        Notification b2 = new NotificationCompat.Builder(this.f5778c).a((int) R.drawable.gf).b(remoteViews).a(remoteViews).a(activity).a(true).b();
        if (Build.VERSION.SDK_INT >= 16) {
            try {
                b2.priority = Integer.MAX_VALUE;
            } catch (Exception e2) {
                b2.priority = 1;
            }
        }
        BackService.f5838a = true;
        this.f5777a.a(16, b2);
    }

    public void h() {
        ArrayList arrayList = new ArrayList();
        String k2 = k();
        if (!TextUtils.isEmpty(k2)) {
            arrayList.add(k2);
        }
        arrayList.add("com.android.calculator2");
        arrayList.add("com.android.calculator2.Calculator");
        arrayList.add("com.pantech.app.calculator");
        arrayList.add("com.pantech.app.calculator.SkyEngCalculator");
        arrayList.add("com.pantech.app.calculator");
        arrayList.add("com.pantech.app.calculator.SkyEngCalculator");
        arrayList.add("com.htc.calculator");
        arrayList.add("com.htc.calculator.Calculator");
        arrayList.add("com.sec.android.app.popupcalculator");
        arrayList.add("com.sec.android.app.popupcalculator.Calculator");
        arrayList.add("com.android.calculator3");
        arrayList.add("com.android.calculator3.Calculator");
        arrayList.add("com.sec.android.app.calculator");
        arrayList.add("com.sec.android.app.calculator.Calculator");
        arrayList.add("com.android.calculator2.CalculatorActivity");
        arrayList.add("com.meizu.flyme.calculator");
        arrayList.add("cn.nubia.calculator2.preset");
        arrayList.add("com.sec.android.app.popupcalculator");
        arrayList.add("com.htc.calculator.widget");
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            if (a(this.f5778c, (String) it.next())) {
                return;
            }
        }
        c(this.f5778c);
    }

    private String k() {
        List<PackageInfo> installedPackages = this.f5778c.getPackageManager().getInstalledPackages(0);
        if (installedPackages != null) {
            for (PackageInfo next : installedPackages) {
                if (next.packageName.contains("calculator")) {
                    return next.packageName;
                }
            }
        }
        return null;
    }

    private void c(Context context) {
        try {
            Intent intent = new Intent("android.intent.action.Calculator");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setFlags(268435456);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e2) {
            b(context);
            Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.ed), 0).show();
        }
    }

    private boolean a(Context context, String str) {
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(str);
        if (launchIntentForPackage == null) {
            return false;
        }
        launchIntentForPackage.setFlags(268435456);
        b(context);
        try {
            context.startActivity(launchIntentForPackage);
            return true;
        } catch (ActivityNotFoundException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static void b(Context context) {
        Method method;
        try {
            Object systemService = context.getSystemService("statusbar");
            if (Build.VERSION.SDK_INT <= 16) {
                method = systemService.getClass().getMethod("collapse", new Class[0]);
            } else {
                method = systemService.getClass().getMethod("collapsePanels", new Class[0]);
            }
            method.setAccessible(true);
            method.invoke(systemService, new Object[0]);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void i() {
        String m = i.a(this.f5778c).m();
        if (m == null || m.length() == 0) {
            a aVar = new a();
            com.pureapps.cleaner.util.f.b("LoadAppBlackList", "开始加载");
            aVar.executeOnExecutor(k.a().b(), new Void[0]);
        }
    }

    /* compiled from: NotificationManager */
    class a extends AsyncTask<Void, Void, ArrayList<String>> {
        a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ArrayList<String> doInBackground(Void... voidArr) {
            boolean z;
            ArrayList<String> arrayList = new ArrayList<>();
            PackageManager packageManager = f.this.f5778c.getPackageManager();
            List<PackageInfo> installedPackages = packageManager.getInstalledPackages(0);
            List j = f.this.j();
            for (int i = 0; i < installedPackages.size(); i++) {
                PackageInfo packageInfo = installedPackages.get(i);
                if ((packageInfo.applicationInfo.flags & 1) <= 0 || (packageInfo.applicationInfo.flags & FileUtils.FileMode.MODE_IWUSR) != 0) {
                    z = true;
                } else {
                    z = false;
                }
                String str = packageInfo.packageName;
                if (z) {
                    String charSequence = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
                    com.pureapps.cleaner.bean.l lVar = new com.pureapps.cleaner.bean.l();
                    lVar.f5650a = charSequence;
                    lVar.f5651b = str;
                    lVar.f5652c = true;
                    if (!j.contains(str)) {
                        arrayList.add(str);
                    }
                }
            }
            return arrayList;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(ArrayList<String> arrayList) {
            super.onPostExecute(arrayList);
            StringBuffer stringBuffer = new StringBuffer();
            int i = 0;
            while (arrayList != null && i < arrayList.size()) {
                stringBuffer.append(arrayList.get(i));
                stringBuffer.append(",");
                i++;
            }
            i.a(f.this.f5778c).a(stringBuffer.toString());
        }
    }

    public List j() {
        ArrayList arrayList = new ArrayList();
        m a2 = m.a();
        a2.a(this.f5778c);
        arrayList.addAll(a2.f5659g);
        arrayList.addAll(a2.f5655c);
        return arrayList;
    }
}
