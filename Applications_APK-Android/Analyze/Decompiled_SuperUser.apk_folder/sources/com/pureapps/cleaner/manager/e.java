package com.pureapps.cleaner.manager;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kingouser.com.R;
import com.kingouser.com.application.App;
import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.os.VUserHandle;
import com.pureapps.cleaner.adapter.JunkListAdapter;
import com.pureapps.cleaner.bean.CloudCache;
import com.pureapps.cleaner.bean.CloudCacheInfo;
import com.pureapps.cleaner.bean.f;
import com.pureapps.cleaner.bean.g;
import com.pureapps.cleaner.bean.h;
import com.pureapps.cleaner.db.DbProvider;
import com.pureapps.cleaner.db.b;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.util.j;
import com.pureapps.cleaner.util.k;
import com.pureapps.cleaner.util.l;
import com.squareup.okhttp.q;
import com.squareup.okhttp.r;
import com.squareup.okhttp.s;
import com.squareup.okhttp.t;
import com.squareup.okhttp.u;
import java.io.File;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* compiled from: JunkManager */
public class e {

    /* renamed from: b  reason: collision with root package name */
    private static e f5729b = null;

    /* renamed from: a  reason: collision with root package name */
    private final String f5730a = "JunkManager";

    /* renamed from: c  reason: collision with root package name */
    private d f5731c = null;

    /* renamed from: d  reason: collision with root package name */
    private a f5732d = null;

    /* renamed from: e  reason: collision with root package name */
    private b f5733e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public r f5734f = new r();
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public long f5735g = 0;

    /* renamed from: h  reason: collision with root package name */
    private final long f5736h = 600000;
    private c i = null;

    /* renamed from: com.pureapps.cleaner.manager.e$e  reason: collision with other inner class name */
    /* compiled from: JunkManager */
    public interface C0090e {
        void a();

        void a(Integer num);
    }

    public static e a() {
        if (f5729b == null) {
            synchronized (e.class) {
                if (f5729b == null) {
                    f5729b = new e();
                }
            }
        }
        return f5729b;
    }

    public void a(Context context) {
        k.a(this.f5733e, true);
        this.f5733e = new b(context);
        this.f5733e.executeOnExecutor(k.a().b(), new Void[0]);
    }

    /* compiled from: JunkManager */
    class b extends AsyncTask<Void, Void, Map<String, List<CloudCacheInfo>>> {

        /* renamed from: b  reason: collision with root package name */
        private CacheManager f5746b = null;

        /* renamed from: c  reason: collision with root package name */
        private SoftReference<Context> f5747c = null;

        public b(Context context) {
            this.f5747c = new SoftReference<>(context);
            this.f5746b = new CacheManager(context.getApplicationContext());
        }

        public List<String> a(Context context) {
            if (context == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            try {
                List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
                if (installedPackages == null) {
                    return null;
                }
                for (PackageInfo packageInfo : installedPackages) {
                    arrayList.add(packageInfo.packageName);
                }
                Collections.reverse(arrayList);
                return arrayList;
            } catch (Exception e2) {
                return null;
            }
        }

        private HashMap<Long, String> a(List<PackageInfo> list) {
            HashMap<Long, String> hashMap = new HashMap<>();
            for (PackageInfo next : list) {
                hashMap.put(Long.valueOf(com.pureapps.cleaner.util.d.b(next.packageName)), next.packageName);
            }
            return hashMap;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            e.this.f5734f.a(20, TimeUnit.SECONDS);
            e.this.f5734f.b(20, TimeUnit.SECONDS);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Map<String, List<CloudCacheInfo>> map) {
            super.onPostExecute(map);
            this.f5746b.a();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Map<String, List<CloudCacheInfo>> doInBackground(Void... voidArr) {
            Exception e2;
            Map<String, List<CloudCacheInfo>> map;
            Gson gson = new Gson();
            if (this.f5746b.a("http://api.bos.kgmobi.com/kgservice/junk-library/getInfo", true) != null) {
                return (Map) gson.fromJson(this.f5746b.a("http://api.bos.kgmobi.com/kgservice/junk-library/getInfo", true), new TypeToken<Map<String, List<CloudCacheInfo>>>() {
                }.getType());
            }
            if (System.currentTimeMillis() - e.this.f5735g < 600000) {
                return null;
            }
            Context context = this.f5747c.get();
            if (context == null) {
                return null;
            }
            List<String> a2 = a(context);
            ArrayList<f> a3 = f.a(context, a(context.getPackageManager().getInstalledPackages(0)), false);
            HashSet hashSet = new HashSet();
            for (int i = 0; i < a3.size(); i++) {
                hashSet.add(a3.get(i).f5624e);
            }
            a2.removeAll(hashSet);
            CloudCache cloudCache = new CloudCache();
            cloudCache.o = a2;
            s b2 = new s.a().a("http://api.bos.kgmobi.com/kgservice/junk-library/getInfo").a(t.a(q.a("text/json;charset=utf-8"), gson.toJson(cloudCache))).b();
            System.currentTimeMillis();
            try {
                u a4 = e.this.f5734f.a(b2).a();
                long unused = e.this.f5735g = System.currentTimeMillis();
                if (!a4.d()) {
                    return null;
                }
                map = CloudCacheInfo.getCloudCacheInfoMaps(a2, a4.g().e());
                try {
                    this.f5746b.a("http://api.bos.kgmobi.com/kgservice/junk-library/getInfo", gson.toJson(map), 172800000);
                    a4.g().close();
                    return map;
                } catch (Exception e3) {
                    e2 = e3;
                }
            } catch (Exception e4) {
                e2 = e4;
                map = null;
                e2.printStackTrace();
                return map;
            }
        }
    }

    public void a(Activity activity, JunkListAdapter junkListAdapter) {
        k.a(this.f5732d, true);
        this.f5732d = new a(activity, junkListAdapter);
        try {
            this.f5732d.executeOnExecutor(k.a().b(), new String[0]);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void b() {
        k.a(this.f5732d, true);
        this.f5732d = null;
    }

    /* compiled from: JunkManager */
    class a extends AsyncTask<String, String, Long> {

        /* renamed from: b  reason: collision with root package name */
        private int f5738b = 0;

        /* renamed from: c  reason: collision with root package name */
        private ArrayList<h> f5739c;

        /* renamed from: d  reason: collision with root package name */
        private Method f5740d;

        /* renamed from: e  reason: collision with root package name */
        private WeakReference<Activity> f5741e = null;

        /* renamed from: f  reason: collision with root package name */
        private JunkListAdapter f5742f = null;

        public a(Activity activity, JunkListAdapter junkListAdapter) {
            this.f5741e = new WeakReference<>(activity);
            this.f5742f = junkListAdapter;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Long doInBackground(String... strArr) {
            Activity activity = this.f5741e.get();
            if (activity != null) {
                try {
                    this.f5740d = activity.getPackageManager().getClass().getMethod("freeStorageAndNotify", Long.TYPE, IPackageDataObserver.class);
                } catch (NoSuchMethodException e2) {
                    e2.printStackTrace();
                }
            }
            long j = 0;
            if (this.f5742f != null) {
                this.f5739c = this.f5742f.d();
            }
            Iterator<h> it = this.f5739c.iterator();
            int i = 0;
            while (it.hasNext()) {
                i = it.next().f5637f + i;
            }
            if ((i == 0 ? 200 : (long) (5000 / i)) >= 200) {
            }
            int i2 = 0;
            while (this.f5739c != null && i2 < this.f5739c.size() && !isCancelled()) {
                ArrayList<? extends Object> a2 = l.a(this.f5739c.get(i2).f5636e);
                int size = a2.size();
                for (int i3 = 0; i3 < size; i3++) {
                    g gVar = (g) a2.get(i3);
                    if (gVar.f5631g) {
                        j += gVar.f5629e;
                        a(gVar, this.f5740d);
                        this.f5738b++;
                        publishProgress(gVar.f5628d);
                    }
                }
                i2++;
            }
            if (activity != null) {
                DbProvider.a(activity, "JunkFiles");
            }
            return Long.valueOf(j);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void onProgressUpdate(String... strArr) {
            super.onProgressUpdate(strArr);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Long l) {
            super.onPostExecute(l);
            i.a(App.a()).b(0L);
            if (l == null) {
                l = 0L;
            }
            com.pureapps.cleaner.b.a.a(16, l.longValue(), Integer.valueOf(this.f5738b));
        }

        private void a(g gVar, Method method) {
            Activity activity = this.f5741e.get();
            if (gVar instanceof com.pureapps.cleaner.bean.e) {
                com.pureapps.cleaner.bean.e eVar = (com.pureapps.cleaner.bean.e) gVar;
                if (!(activity == null || method == null || !activity.getString(R.string.cg).equals(eVar.f5618a))) {
                    final CountDownLatch countDownLatch = new CountDownLatch(1);
                    StatFs statFs = new StatFs(Environment.getDataDirectory().getAbsolutePath());
                    try {
                        this.f5740d.invoke(activity.getPackageManager(), Long.valueOf(((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())), new IPackageDataObserver.Stub() {
                            public void onRemoveCompleted(String str, boolean z) {
                                countDownLatch.countDown();
                            }
                        });
                        countDownLatch.await();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            } else if (gVar instanceof com.pureapps.cleaner.bean.d) {
                com.pureapps.cleaner.bean.d dVar = (com.pureapps.cleaner.bean.d) gVar;
                if (!com.pureapps.cleaner.util.e.a(dVar.f5617c)) {
                    com.pureapps.cleaner.util.e.b(dVar.f5617c);
                }
            }
            Iterator<? extends Object> it = l.a(gVar.f5630f).iterator();
            while (it.hasNext()) {
                com.pureapps.cleaner.bean.i iVar = (com.pureapps.cleaner.bean.i) it.next();
                if (iVar.m == -10000) {
                    com.pureapps.cleaner.util.e.b(iVar.l);
                } else if (iVar.m != -10001) {
                    com.pureapps.cleaner.util.e.a(iVar.l);
                }
            }
        }
    }

    public void a(Activity activity, JunkListAdapter junkListAdapter, C0090e eVar) {
        k.a(this.i, true);
        if (this.i == null) {
            this.i = new c(activity, junkListAdapter, eVar);
        }
        if (this.i.f5756h) {
            eVar.a(0);
            return;
        }
        this.i = new c(activity, junkListAdapter, eVar);
        this.i.executeOnExecutor(k.a().b(), new String[0]);
    }

    public void c() {
        k.a(this.i, true);
        this.i = null;
    }

    /* compiled from: JunkManager */
    class c extends AsyncTask<String, g, Integer> {

        /* renamed from: a  reason: collision with root package name */
        Method f5749a = null;

        /* renamed from: b  reason: collision with root package name */
        public h f5750b = null;

        /* renamed from: c  reason: collision with root package name */
        public h f5751c = null;

        /* renamed from: e  reason: collision with root package name */
        private WeakReference<Activity> f5753e = null;

        /* renamed from: f  reason: collision with root package name */
        private JunkListAdapter f5754f = null;

        /* renamed from: g  reason: collision with root package name */
        private C0090e f5755g = null;
        /* access modifiers changed from: private */

        /* renamed from: h  reason: collision with root package name */
        public boolean f5756h = false;

        public c(Activity activity, JunkListAdapter junkListAdapter, C0090e eVar) {
            this.f5753e = new WeakReference<>(activity);
            this.f5754f = junkListAdapter;
            this.f5755g = eVar;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            this.f5756h = true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer doInBackground(String... strArr) {
            ArrayList<b.a> arrayList;
            PackageManager packageManager;
            g gVar;
            g gVar2;
            this.f5756h = true;
            Activity activity = this.f5753e.get();
            if (activity == null || System.currentTimeMillis() - i.a(activity).q() <= 86400000) {
                ArrayList<b.a> arrayList2 = new ArrayList<>();
                if (activity != null) {
                    packageManager = activity.getPackageManager();
                    this.f5750b = this.f5754f.a();
                    this.f5751c = this.f5754f.b();
                    arrayList = b.a.a(activity);
                } else {
                    arrayList = arrayList2;
                    packageManager = null;
                }
                Iterator<b.a> it = arrayList.iterator();
                int i = 0;
                g gVar3 = null;
                while (it.hasNext()) {
                    b.a next = it.next();
                    int i2 = i + 1;
                    if (TextUtils.isEmpty(next.f5669b)) {
                        i = i2;
                    } else {
                        File file = new File(next.f5669b);
                        if (next.f5670c == 0) {
                            f fVar = new f();
                            fVar.l = next.f5669b;
                            fVar.f5624e = next.f5672e;
                            fVar.m = next.f5673f;
                            if (next.f5673f == -10001) {
                                fVar.j = a(packageManager, fVar.l);
                            } else if (next.f5673f == -10000) {
                                fVar.j = com.pureapps.cleaner.util.e.d(fVar.l);
                            } else {
                                if (file != null) {
                                    if (!file.exists()) {
                                        i = i2;
                                    } else {
                                        fVar.j = com.pureapps.cleaner.util.e.c(next.f5669b);
                                    }
                                }
                                i = i2;
                            }
                            if (activity != null) {
                                gVar = e.this.a(activity, packageManager, this.f5750b, this.f5751c, fVar);
                            } else {
                                gVar = gVar3;
                            }
                            if (gVar != null) {
                                publishProgress(gVar);
                            }
                        } else if (next.f5670c == 1) {
                            if (file != null) {
                                if (!file.exists()) {
                                    i = i2;
                                } else {
                                    com.pureapps.cleaner.bean.c cVar = new com.pureapps.cleaner.bean.c();
                                    cVar.l = next.f5669b;
                                    cVar.f5614c = next.f5671d;
                                    cVar.m = next.f5673f;
                                    cVar.j = com.pureapps.cleaner.util.e.c(next.f5669b);
                                    if (activity != null) {
                                        gVar2 = e.this.a(activity, packageManager, this.f5750b, this.f5751c, cVar);
                                    } else {
                                        gVar2 = gVar3;
                                    }
                                    if (gVar != null) {
                                        publishProgress(gVar);
                                    }
                                }
                            }
                            i = i2;
                        } else {
                            gVar = gVar3;
                        }
                        i = i2;
                        gVar3 = gVar;
                    }
                }
                return Integer.valueOf(i);
            }
            i.a(activity).b(System.currentTimeMillis());
            b.a.b(activity);
            return 0;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(g... gVarArr) {
            super.onProgressUpdate(gVarArr);
            this.f5754f.notifyDataSetChanged();
            if (this.f5755g != null) {
                this.f5755g.a();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Integer num) {
            super.onPostExecute(num);
            this.f5756h = false;
            if (num.intValue() > 0) {
                this.f5754f.a(false);
            }
            if (this.f5755g != null) {
                this.f5755g.a(num);
            }
        }

        private long a(PackageManager packageManager, String str) {
            Activity activity = this.f5753e.get();
            final long[] jArr = {0};
            try {
                if (this.f5749a == null) {
                    this.f5749a = packageManager.getClass().getMethod("getPackageSizeInfo", String.class, IPackageStatsObserver.class);
                }
                final CountDownLatch countDownLatch = new CountDownLatch(1);
                if (activity != null) {
                    this.f5749a.invoke(activity.getPackageManager(), str, new IPackageStatsObserver.Stub() {
                        public void onGetStatsCompleted(PackageStats packageStats, boolean z) {
                            if (z) {
                                jArr[0] = packageStats.cacheSize;
                            }
                            countDownLatch.countDown();
                        }
                    });
                    countDownLatch.await();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return jArr[0];
        }
    }

    public void a(Context context, boolean z) {
        if (this.f5731c == null) {
            this.f5731c = new d(context);
        }
        if (!this.f5731c.f5760a) {
            this.f5731c = new d(context);
            this.f5731c.executeOnExecutor(k.a().b(), Boolean.valueOf(z));
        }
    }

    public void d() {
        k.a(this.f5731c, true);
        this.f5731c = null;
    }

    /* compiled from: JunkManager */
    class d extends AsyncTask<Boolean, g, String> {

        /* renamed from: a  reason: collision with root package name */
        public boolean f5760a = false;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public int f5762c = 0;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public String f5763d = null;

        /* renamed from: e  reason: collision with root package name */
        private long f5764e;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public boolean f5765f;
        /* access modifiers changed from: private */

        /* renamed from: g  reason: collision with root package name */
        public Context f5766g = null;
        /* access modifiers changed from: private */

        /* renamed from: h  reason: collision with root package name */
        public Object[] f5767h = new Object[4];

        public d(Context context) {
            this.f5766g = context;
            this.f5760a = false;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            this.f5760a = true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String doInBackground(Boolean... boolArr) {
            this.f5760a = true;
            this.f5765f = boolArr[0].booleanValue();
            this.f5764e = System.currentTimeMillis();
            PackageManager packageManager = this.f5766g.getPackageManager();
            List<PackageInfo> installedPackages = packageManager.getInstalledPackages(FileUtils.FileMode.MODE_IWUSR);
            ArrayList<f> a2 = f.a(this.f5766g, a(installedPackages), true);
            ArrayList<com.pureapps.cleaner.bean.c> a3 = com.pureapps.cleaner.bean.c.a(this.f5766g);
            h hVar = new h(0);
            hVar.f5633b = this.f5766g.getString(R.string.cf);
            h hVar2 = new h(1);
            hVar2.f5633b = this.f5766g.getString(R.string.cd);
            h apkFiles = com.pureapps.cleaner.db.b.getApkFiles(this.f5766g);
            apkFiles.f5633b = this.f5766g.getString(R.string.ce);
            this.f5767h[1] = hVar;
            this.f5767h[2] = hVar2;
            this.f5767h[3] = apkFiles;
            com.pureapps.cleaner.b.a.a(24, (long) this.f5762c, this.f5767h);
            a(packageManager, hVar, installedPackages);
            a(hVar, hVar2, a2, a3);
            Log.d("JunkManager", "loadListFromSystem UseTime=" + (System.currentTimeMillis() - this.f5764e));
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(hVar.f5636e);
            arrayList.addAll(hVar2.f5636e);
            arrayList.addAll(apkFiles.f5636e);
            b.a.a(this.f5766g, arrayList);
            return null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(g... gVarArr) {
            super.onProgressUpdate(gVarArr);
            if (!this.f5765f && !isCancelled()) {
                this.f5767h[0] = this.f5763d;
                com.pureapps.cleaner.b.a.a(24, (long) this.f5762c, this.f5767h);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(String str) {
            super.onPostExecute(str);
            this.f5760a = false;
            com.pureapps.cleaner.b.a.a(25, 0, null);
        }

        private void a(PackageManager packageManager, h hVar, List<PackageInfo> list) {
            String str;
            String[] strArr = {"/data/local/tmp/*", "/data/tmp/*", "/data/system/usagestats/*", "/data/system/appusagestates/*", "/data/tombstones/*", "/data/anr/*", "/dev/log/main", Environment.getExternalStorageDirectory().toString() + File.separator + Environment.DIRECTORY_DCIM + File.separator + ".thumbnails/*"};
            final com.pureapps.cleaner.bean.e a2 = a(hVar);
            if (a2 == null) {
                a2 = new com.pureapps.cleaner.bean.e();
                a2.f5628d = this.f5766g.getString(R.string.cg);
                a2.f5618a = this.f5766g.getString(R.string.cg);
                hVar.f5636e.add(0, a2);
                if (a2.f5631g) {
                    hVar.f5637f++;
                }
            }
            boolean u = i.a(this.f5766g).u();
            final int length = strArr.length;
            final int size = list.size();
            for (int i = 0; i < length && !isCancelled(); i++) {
                try {
                    String str2 = strArr[i];
                    if (strArr[i].endsWith("*")) {
                        str = strArr[i].substring(0, strArr[i].lastIndexOf("/*"));
                    } else {
                        str = strArr[i];
                    }
                    if (str.startsWith(Environment.getExternalStorageDirectory().toString())) {
                        a(str, a2, hVar);
                    } else {
                        a(u, str, a2, hVar);
                    }
                    this.f5762c = (i * 20) / (length + size);
                    this.f5763d = strArr[i].substring(0, strArr[i].lastIndexOf("/"));
                    if (!this.f5765f) {
                        publishProgress(new g[0]);
                    } else {
                        com.pureapps.cleaner.b.a.a(32, 0, this.f5767h);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            try {
                final CountDownLatch countDownLatch = new CountDownLatch(size);
                Method method = packageManager.getClass().getMethod("getPackageSizeInfo", String.class, IPackageStatsObserver.class);
                for (final int i2 = 0; i2 < size && !isCancelled(); i2++) {
                    final List<PackageInfo> list2 = list;
                    final h hVar2 = hVar;
                    method.invoke(packageManager, list.get(i2).packageName, new IPackageStatsObserver.Stub() {
                        public void onGetStatsCompleted(PackageStats packageStats, boolean z) {
                            synchronized (list2) {
                                if (z) {
                                    if (packageStats.cacheSize > 0) {
                                        try {
                                            f fVar = new f();
                                            fVar.f5624e = d.this.f5766g.getString(R.string.cg);
                                            fVar.m = -10001;
                                            fVar.l = packageStats.packageName;
                                            fVar.j = packageStats.cacheSize;
                                            if (!e.this.a(a2, fVar)) {
                                                a2.f5630f.add(fVar);
                                                a2.f5629e += fVar.j;
                                                hVar2.f5634c += fVar.j;
                                                if (a2.f5631g) {
                                                    hVar2.f5635d += fVar.j;
                                                }
                                                int unused = d.this.f5762c = ((length + i2) * 20) / (length + size);
                                                String unused2 = d.this.f5763d = fVar.l;
                                                if (!d.this.f5765f) {
                                                    d.this.publishProgress(new g[0]);
                                                } else {
                                                    com.pureapps.cleaner.b.a.a(32, 0, d.this.f5767h);
                                                }
                                            } else {
                                                return;
                                            }
                                        } catch (Exception e2) {
                                            e2.printStackTrace();
                                        }
                                    }
                                }
                                synchronized (countDownLatch) {
                                    countDownLatch.countDown();
                                }
                            }
                        }
                    });
                }
                countDownLatch.await();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            publishProgress(a2);
        }

        private f a(boolean z, String str, com.pureapps.cleaner.bean.e eVar, h hVar) {
            String str2;
            f fVar;
            Exception e2;
            f fVar2 = null;
            int i = 0;
            if (z) {
                str2 = j.a("ls -l " + str, true, false).f5881b;
            } else {
                str2 = null;
            }
            if (!TextUtils.isEmpty(str2)) {
                String[] split = str2.split(ShellUtils.COMMAND_LINE_END);
                int length = split.length;
                while (i < length && !isCancelled()) {
                    String[] split2 = split[i].split("\\s+");
                    if (split2 != null) {
                        try {
                            if (split2.length == 7) {
                                fVar = new f();
                                try {
                                    fVar.f5624e = this.f5766g.getString(R.string.cg);
                                    fVar.m = VUserHandle.USER_NULL;
                                    fVar.l = str + File.separator + split2[6];
                                    fVar.j = Long.parseLong(split2[3]);
                                    if (!e.this.a(eVar, fVar)) {
                                        eVar.f5630f.add(fVar);
                                        eVar.f5629e += fVar.j;
                                        hVar.f5634c += fVar.j;
                                        if (eVar.f5631g) {
                                            hVar.f5635d += fVar.j;
                                        }
                                    }
                                } catch (Exception e3) {
                                    e2 = e3;
                                    e2.printStackTrace();
                                    i++;
                                    fVar2 = fVar;
                                }
                                i++;
                                fVar2 = fVar;
                            }
                        } catch (Exception e4) {
                            Exception exc = e4;
                            fVar = fVar2;
                            e2 = exc;
                            e2.printStackTrace();
                            i++;
                            fVar2 = fVar;
                        }
                    }
                    fVar = fVar2;
                    i++;
                    fVar2 = fVar;
                }
            }
            return fVar2;
        }

        private f a(String str, com.pureapps.cleaner.bean.e eVar, h hVar) {
            f fVar = null;
            File file = new File(str);
            if (file != null && file.exists()) {
                File[] listFiles = file.listFiles();
                int length = listFiles.length;
                if (listFiles != null && length > 0 && !isCancelled()) {
                    int length2 = listFiles.length;
                    int i = 0;
                    while (i < length2) {
                        File file2 = listFiles[i];
                        f fVar2 = new f();
                        fVar2.f5624e = this.f5766g.getString(R.string.cg);
                        fVar2.m = 101;
                        fVar2.l = file2.getAbsolutePath();
                        fVar2.j = com.pureapps.cleaner.util.e.b(file2);
                        if (!e.this.a(eVar, fVar2)) {
                            eVar.f5630f.add(fVar2);
                            eVar.f5629e += fVar2.j;
                            hVar.f5634c += fVar2.j;
                            if (eVar.f5631g) {
                                hVar.f5635d += fVar2.j;
                            }
                        }
                        i++;
                        fVar = fVar2;
                    }
                }
            }
            return fVar;
        }

        private com.pureapps.cleaner.bean.e a(h hVar) {
            String string = this.f5766g.getString(R.string.cg);
            Iterator<g> it = hVar.f5636e.iterator();
            while (it.hasNext()) {
                g next = it.next();
                if ((next instanceof com.pureapps.cleaner.bean.e) && string.equals(((com.pureapps.cleaner.bean.e) next).f5618a)) {
                    return (com.pureapps.cleaner.bean.e) next;
                }
            }
            return null;
        }

        private HashMap<Long, String> a(List<PackageInfo> list) {
            HashMap<Long, String> hashMap = new HashMap<>();
            for (PackageInfo next : list) {
                hashMap.put(Long.valueOf(com.pureapps.cleaner.util.d.b(next.packageName)), next.packageName);
            }
            return hashMap;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c3, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00c4, code lost:
            r1 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x00cc, code lost:
            r0 = th;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00cc A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0002] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void a(com.pureapps.cleaner.bean.h r12, com.pureapps.cleaner.bean.h r13, java.util.ArrayList<com.pureapps.cleaner.bean.f> r14, java.util.ArrayList<com.pureapps.cleaner.bean.c> r15) {
            /*
                r11 = this;
                r6 = 0
                r7 = 0
                android.content.Context r0 = r11.f5766g     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                android.content.pm.PackageManager r8 = r0.getPackageManager()     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                r0 = 1
                java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                r0 = 0
                java.lang.String r1 = "_data"
                r2[r0] = r1     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                java.lang.String r0 = "title!='.nomedia' and _data not like '%tencent/MicroMsg%'"
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                r1.<init>()     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                java.lang.String r1 = " AND format = 12289"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                r0 = 5
                java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                r1 = 0
                java.lang.String r4 = "_data"
                r0[r1] = r4     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                r1 = 1
                java.lang.String r4 = "format"
                r0[r1] = r4     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                r1 = 2
                java.lang.String r4 = "date_modified"
                r0[r1] = r4     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                r1 = 3
                java.lang.String r4 = "_size"
                r0[r1] = r4     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                r1 = 4
                java.lang.String r4 = "mime_type"
                r0[r1] = r4     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                java.lang.String r0 = "mime_type is null "
                r0 = 0
                java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                r5 = 0
                android.content.Context r0 = r11.f5766g     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                java.lang.String r1 = "external"
                android.net.Uri r1 = android.provider.MediaStore.Files.getContentUri(r1)     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                android.database.Cursor r6 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00c3, all -> 0x00cc }
                int r9 = r6.getCount()     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
            L_0x005b:
                boolean r0 = r6.moveToNext()     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                if (r0 == 0) goto L_0x00bf
                boolean r0 = r11.isCancelled()     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                if (r0 != 0) goto L_0x00bf
                java.io.File r10 = new java.io.File     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                java.lang.String r0 = "_data"
                int r0 = r6.getColumnIndex(r0)     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                r10.<init>(r0)     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                if (r10 == 0) goto L_0x00a8
                boolean r0 = r10.isDirectory()     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                if (r0 == 0) goto L_0x00a8
                com.pureapps.cleaner.bean.f r5 = r11.a(r14, r10)     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                if (r5 != 0) goto L_0x0088
                com.pureapps.cleaner.bean.c r5 = r11.b(r15, r10)     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
            L_0x0088:
                if (r5 == 0) goto L_0x00a8
                long r0 = r5.j     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                r2 = 0
                int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r0 <= 0) goto L_0x00a8
                com.pureapps.cleaner.manager.e r0 = com.pureapps.cleaner.manager.e.this     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                android.content.Context r1 = r11.f5766g     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                r2 = r8
                r3 = r12
                r4 = r13
                com.pureapps.cleaner.bean.g r0 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                if (r0 == 0) goto L_0x00a8
                r1 = 1
                com.pureapps.cleaner.bean.g[] r1 = new com.pureapps.cleaner.bean.g[r1]     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                r2 = 0
                r1[r2] = r0     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                r11.publishProgress(r1)     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
            L_0x00a8:
                int r0 = r7 + 1
                int r1 = r0 * 80
                int r1 = r1 / r9
                int r1 = r1 + 20
                r11.f5762c = r1     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                java.lang.String r1 = r10.getAbsolutePath()     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                r11.f5763d = r1     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                r1 = 0
                com.pureapps.cleaner.bean.g[] r1 = new com.pureapps.cleaner.bean.g[r1]     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                r11.publishProgress(r1)     // Catch:{ Exception -> 0x00d4, all -> 0x00cc }
                r7 = r0
                goto L_0x005b
            L_0x00bf:
                com.pureapps.cleaner.db.c.a(r6)
            L_0x00c2:
                return
            L_0x00c3:
                r0 = move-exception
                r1 = r6
            L_0x00c5:
                r0.printStackTrace()     // Catch:{ all -> 0x00d1 }
                com.pureapps.cleaner.db.c.a(r1)
                goto L_0x00c2
            L_0x00cc:
                r0 = move-exception
            L_0x00cd:
                com.pureapps.cleaner.db.c.a(r6)
                throw r0
            L_0x00d1:
                r0 = move-exception
                r6 = r1
                goto L_0x00cd
            L_0x00d4:
                r0 = move-exception
                r1 = r6
                goto L_0x00c5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.pureapps.cleaner.manager.e.d.a(com.pureapps.cleaner.bean.h, com.pureapps.cleaner.bean.h, java.util.ArrayList, java.util.ArrayList):void");
        }

        private f a(ArrayList<f> arrayList, File file) {
            f fVar = null;
            try {
                String lowerCase = file.getAbsolutePath().substring((Environment.getExternalStorageDirectory().toString() + "/").length()).toLowerCase();
                String[] split = lowerCase.split("/");
                if (split == null || split.length <= 5) {
                    ArrayList<String> a2 = com.pureapps.cleaner.util.d.a(split);
                    Iterator<f> it = arrayList.iterator();
                    while (it.hasNext()) {
                        f next = it.next();
                        fVar = a(next.k, next, lowerCase, a2, split);
                        if (fVar != null) {
                            break;
                        }
                    }
                    if (fVar != null) {
                        fVar.l = file.getAbsolutePath();
                        fVar.j = com.pureapps.cleaner.util.e.b(file);
                        fVar.b(this.f5766g);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return fVar;
        }

        private f a(String str, f fVar, String str2, ArrayList<String> arrayList, String[] strArr) {
            if (str.equals(str2) || str.equals(com.pureapps.cleaner.util.d.a(arrayList)) || str.equals(com.pureapps.cleaner.util.d.b(arrayList))) {
                return fVar;
            }
            if (arrayList.size() > 0) {
                String str3 = arrayList.get(0);
                if (1 < arrayList.size()) {
                    str3 = str3 + "+" + str2.substring((strArr[0] + File.separator).length());
                }
                if (str.equals(str3)) {
                    return fVar;
                }
            }
            if (arrayList.size() > 1) {
                String str4 = arrayList.get(0) + "+" + arrayList.get(1);
                if (2 < arrayList.size()) {
                    str4 = str4 + "+" + str2.substring((strArr[0] + File.separator + strArr[1] + File.separator).length());
                }
                if (str.equals(str4)) {
                    return fVar;
                }
            }
            return null;
        }

        private com.pureapps.cleaner.bean.c b(ArrayList<com.pureapps.cleaner.bean.c> arrayList, File file) {
            com.pureapps.cleaner.bean.c cVar;
            try {
                String substring = file.getAbsolutePath().substring((Environment.getExternalStorageDirectory().getAbsolutePath() + "/").length());
                String[] split = substring.split("/");
                if (split != null && split.length > 5) {
                    return null;
                }
                String b2 = com.pureapps.cleaner.util.d.b(split);
                String c2 = com.pureapps.cleaner.util.d.c(split);
                Iterator<com.pureapps.cleaner.bean.c> it = arrayList.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        cVar = null;
                        break;
                    }
                    cVar = it.next();
                    if (!cVar.k.equals(substring)) {
                        if (!cVar.k.equals(b2)) {
                            if (cVar.k.equals(c2)) {
                                break;
                            }
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (cVar == null) {
                    return cVar;
                }
                cVar.l = file.getAbsolutePath();
                cVar.j = com.pureapps.cleaner.util.e.b(file);
                return cVar;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }
    }

    /* access modifiers changed from: private */
    public g a(Context context, PackageManager packageManager, h hVar, h hVar2, com.pureapps.cleaner.bean.i iVar) {
        Exception exc;
        com.pureapps.cleaner.bean.e eVar;
        com.pureapps.cleaner.bean.e eVar2;
        boolean z;
        com.pureapps.cleaner.bean.b bVar;
        boolean z2;
        if (iVar == null) {
            return null;
        }
        try {
            if (iVar instanceof f) {
                f fVar = (f) iVar;
                Iterator<g> it = hVar.f5636e.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        z2 = false;
                        eVar2 = null;
                        break;
                    }
                    g next = it.next();
                    if (((com.pureapps.cleaner.bean.e) next).f5618a.equals(fVar.f5624e)) {
                        if (a(next, iVar)) {
                            return null;
                        }
                        next.f5630f.add(fVar);
                        next.f5629e += fVar.j;
                        hVar.f5634c += fVar.j;
                        if (next.f5631g) {
                            hVar.f5635d += fVar.j;
                        }
                        eVar2 = next;
                        z2 = true;
                    }
                }
                if (!z2) {
                    try {
                        com.pureapps.cleaner.bean.e eVar3 = new com.pureapps.cleaner.bean.e();
                        String string = context.getString(R.string.cg);
                        if (string.equals(fVar.f5624e)) {
                            eVar3.f5618a = fVar.f5624e;
                            eVar3.f5628d = string;
                            eVar3.f5630f.add(fVar);
                            eVar3.f5629e += fVar.j;
                            hVar.f5634c += fVar.j;
                            hVar.f5636e.add(eVar3);
                            if (eVar3.f5631g) {
                                hVar.f5637f++;
                                hVar.f5635d += fVar.j;
                                eVar = eVar3;
                                return eVar;
                            }
                        } else {
                            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(fVar.f5624e, FileUtils.FileMode.MODE_IWUSR);
                            if (applicationInfo == null) {
                                eVar = null;
                                return eVar;
                            }
                            eVar3.f5618a = fVar.f5624e;
                            eVar3.f5628d = applicationInfo.loadLabel(packageManager).toString();
                            eVar3.f5619b = applicationInfo.loadIcon(packageManager);
                            eVar3.f5630f.add(fVar);
                            eVar3.f5629e += fVar.j;
                            hVar.f5634c += fVar.j;
                            hVar.f5636e.add(eVar3);
                            if (eVar3.f5631g) {
                                hVar.f5637f++;
                                hVar.f5635d += fVar.j;
                            }
                        }
                        eVar = eVar3;
                    } catch (Exception e2) {
                        exc = e2;
                        eVar = eVar2;
                        exc.printStackTrace();
                        return eVar;
                    }
                    return eVar;
                }
            } else {
                if (iVar instanceof com.pureapps.cleaner.bean.c) {
                    com.pureapps.cleaner.bean.c cVar = (com.pureapps.cleaner.bean.c) iVar;
                    Iterator<g> it2 = hVar2.f5636e.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            z = false;
                            bVar = null;
                            break;
                        }
                        g next2 = it2.next();
                        if (((com.pureapps.cleaner.bean.b) next2).f5628d.equals(cVar.f5614c)) {
                            if (a(next2, iVar)) {
                                return null;
                            }
                            next2.f5630f.add(cVar);
                            next2.f5629e += cVar.j;
                            hVar2.f5634c += cVar.j;
                            if (next2.f5631g) {
                                hVar2.f5635d += cVar.j;
                            }
                            bVar = next2;
                            z = true;
                        }
                    }
                    if (!z) {
                        com.pureapps.cleaner.bean.b bVar2 = new com.pureapps.cleaner.bean.b();
                        bVar2.f5628d = cVar.f5614c;
                        bVar2.f5630f.add(cVar);
                        bVar2.f5629e += cVar.j;
                        hVar2.f5636e.add(bVar2);
                        hVar2.f5634c += cVar.j;
                        try {
                            if (bVar2.f5631g) {
                                hVar2.f5637f++;
                                hVar2.f5635d += cVar.j;
                            }
                            eVar = bVar2;
                        } catch (Exception e3) {
                            Exception exc2 = e3;
                            eVar = bVar2;
                            exc = exc2;
                            exc.printStackTrace();
                            return eVar;
                        }
                    }
                } else {
                    eVar = null;
                }
                return eVar;
            }
            eVar = eVar2;
        } catch (Exception e4) {
            exc = e4;
            eVar = null;
            exc.printStackTrace();
            return eVar;
        }
        return eVar;
    }

    /* access modifiers changed from: private */
    public boolean a(g gVar, com.pureapps.cleaner.bean.i iVar) {
        Iterator<com.pureapps.cleaner.bean.i> it = gVar.f5630f.iterator();
        while (it.hasNext()) {
            com.pureapps.cleaner.bean.i next = it.next();
            if (iVar.l != null && iVar.l.equals(next.l)) {
                return true;
            }
        }
        return false;
    }
}
