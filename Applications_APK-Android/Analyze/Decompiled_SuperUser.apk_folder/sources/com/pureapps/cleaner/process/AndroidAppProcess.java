package com.pureapps.cleaner.process;

import android.app.ActivityManager;
import android.os.Parcel;
import android.os.Parcelable;

public class AndroidAppProcess extends AndroidProcess {
    public static final Parcelable.Creator<AndroidAppProcess> CREATOR = new Parcelable.Creator<AndroidAppProcess>() {
        /* renamed from: a */
        public AndroidAppProcess createFromParcel(Parcel parcel) {
            return new AndroidAppProcess(parcel);
        }

        /* renamed from: a */
        public AndroidAppProcess[] newArray(int i) {
            return new AndroidAppProcess[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public boolean f5823a;

    /* renamed from: b  reason: collision with root package name */
    public int f5824b;

    /* renamed from: e  reason: collision with root package name */
    private Cgroup f5825e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AndroidAppProcess(int i) {
        super(i);
        boolean z = true;
        this.f5825e = super.b();
        ControlGroup a2 = this.f5825e.a("cpuacct");
        ControlGroup a3 = this.f5825e.a("cpu");
        if (a3 == null || a2 == null || !a2.f5831c.contains("pid_")) {
            throw new NotAndroidAppProcessException(i);
        }
        this.f5823a = a3.f5831c.contains("bg_non_interactive") ? false : z;
        try {
            this.f5824b = Integer.parseInt(a2.f5831c.split("/")[1].replace("uid_", ""));
        } catch (Exception e2) {
            this.f5824b = c().a();
        }
    }

    public AndroidAppProcess(ActivityManager.RunningServiceInfo runningServiceInfo) {
        super(runningServiceInfo);
    }

    public String a() {
        return this.f5826c.split(":")[0];
    }

    public Cgroup b() {
        return this.f5825e;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.f5825e, i);
        parcel.writeByte((byte) (this.f5823a ? 1 : 0));
    }

    protected AndroidAppProcess(Parcel parcel) {
        super(parcel);
        this.f5825e = (Cgroup) parcel.readParcelable(Cgroup.class.getClassLoader());
        this.f5823a = parcel.readByte() != 0;
    }

    public static final class NotAndroidAppProcessException extends Exception {
        public NotAndroidAppProcessException(int i) {
            super(String.format("The process %d does not belong to any application", Integer.valueOf(i)));
        }
    }
}
