package com.pureapps.cleaner.process;

import com.kingouser.com.util.ShellUtils;

public final class Status extends ProcFile {
    public static Status a(int i) {
        return new Status(String.format("/proc/%d/status", Integer.valueOf(i)));
    }

    private Status(String str) {
        super(str);
    }

    public String a(String str) {
        for (String str2 : this.f5832b.split(ShellUtils.COMMAND_LINE_END)) {
            if (str2.startsWith(str + ":")) {
                return str2.split(str + ":")[1].trim();
            }
        }
        return null;
    }

    public int a() {
        try {
            return Integer.parseInt(a("Uid").split("\\s+")[0]);
        } catch (Exception e2) {
            return -1;
        }
    }
}
