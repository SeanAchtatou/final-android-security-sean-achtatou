package com.pureapps.cleaner.process;

import android.os.Parcel;
import android.os.Parcelable;

public class ControlGroup implements Parcelable {
    public static final Parcelable.Creator<ControlGroup> CREATOR = new Parcelable.Creator<ControlGroup>() {
        /* renamed from: a */
        public ControlGroup createFromParcel(Parcel parcel) {
            return new ControlGroup(parcel);
        }

        /* renamed from: a */
        public ControlGroup[] newArray(int i) {
            return new ControlGroup[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public final int f5829a;

    /* renamed from: b  reason: collision with root package name */
    public final String f5830b;

    /* renamed from: c  reason: collision with root package name */
    public final String f5831c;

    protected ControlGroup(String str) {
        String[] split = str.split(":");
        this.f5829a = Integer.parseInt(split[0]);
        this.f5830b = split[1];
        this.f5831c = split[2];
    }

    protected ControlGroup(Parcel parcel) {
        this.f5829a = parcel.readInt();
        this.f5830b = parcel.readString();
        this.f5831c = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f5829a);
        parcel.writeString(this.f5830b);
        parcel.writeString(this.f5831c);
    }
}
