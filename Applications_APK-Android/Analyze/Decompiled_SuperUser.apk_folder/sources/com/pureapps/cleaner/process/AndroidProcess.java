package com.pureapps.cleaner.process;

import android.app.ActivityManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.io.IOException;

public class AndroidProcess implements Parcelable {
    public static final Parcelable.Creator<AndroidProcess> CREATOR = new Parcelable.Creator<AndroidProcess>() {
        /* renamed from: a */
        public AndroidProcess createFromParcel(Parcel parcel) {
            return new AndroidProcess(parcel);
        }

        /* renamed from: a */
        public AndroidProcess[] newArray(int i) {
            return new AndroidProcess[i];
        }
    };

    /* renamed from: c  reason: collision with root package name */
    public String f5826c = null;

    /* renamed from: d  reason: collision with root package name */
    public final int f5827d;

    public AndroidProcess(int i) {
        this.f5827d = i;
        this.f5826c = a(i);
    }

    public AndroidProcess(ActivityManager.RunningServiceInfo runningServiceInfo) {
        this.f5827d = runningServiceInfo.pid;
        this.f5826c = runningServiceInfo.process;
    }

    static String a(int i) {
        String str = null;
        try {
            str = ProcFile.b(String.format("/proc/%d/cmdline", Integer.valueOf(i))).trim();
        } catch (IOException e2) {
        }
        if (TextUtils.isEmpty(str) || "null".equals(str)) {
            return Stat.a(i).a();
        }
        return str;
    }

    public Cgroup b() {
        return Cgroup.a(this.f5827d);
    }

    public Status c() {
        return Status.a(this.f5827d);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f5826c);
        parcel.writeInt(this.f5827d);
    }

    protected AndroidProcess(Parcel parcel) {
        this.f5826c = parcel.readString();
        this.f5827d = parcel.readInt();
    }
}
