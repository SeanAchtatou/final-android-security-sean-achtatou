package com.pureapps.cleaner.process;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class ProcFile extends File implements Parcelable {
    public static final Parcelable.Creator<ProcFile> CREATOR = new Parcelable.Creator<ProcFile>() {
        /* renamed from: a */
        public ProcFile createFromParcel(Parcel parcel) {
            return new ProcFile(parcel);
        }

        /* renamed from: a */
        public ProcFile[] newArray(int i) {
            return new ProcFile[i];
        }
    };

    /* renamed from: b  reason: collision with root package name */
    public final String f5832b;

    protected static String b(String str) {
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(str));
        sb.append(bufferedReader.readLine());
        for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
            sb.append(10).append(readLine);
        }
        bufferedReader.close();
        return sb.toString();
    }

    protected ProcFile(String str) {
        super(str);
        this.f5832b = b(str);
    }

    protected ProcFile(Parcel parcel) {
        super(parcel.readString());
        this.f5832b = parcel.readString();
    }

    public long length() {
        return (long) this.f5832b.length();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getAbsolutePath());
        parcel.writeString(this.f5832b);
    }
}
