package com.pureapps.cleaner.process;

import android.os.Parcel;
import android.os.Parcelable;

public final class Statm extends ProcFile {
    public static final Parcelable.Creator<Statm> CREATOR = new Parcelable.Creator<Statm>() {
        /* renamed from: a */
        public Statm createFromParcel(Parcel parcel) {
            return new Statm(parcel);
        }

        /* renamed from: a */
        public Statm[] newArray(int i) {
            return new Statm[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public final String[] f5834a;

    private Statm(Parcel parcel) {
        super(parcel);
        this.f5834a = parcel.createStringArray();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeStringArray(this.f5834a);
    }
}
