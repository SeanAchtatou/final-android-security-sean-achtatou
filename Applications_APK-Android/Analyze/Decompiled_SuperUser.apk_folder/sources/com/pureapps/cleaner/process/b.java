package com.pureapps.cleaner.process;

import android.annotation.TargetApi;
import android.os.Build;
import android.system.Os;
import android.util.Log;
import java.lang.reflect.Method;
import java.util.HashMap;

/* compiled from: ProcessUtil */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static HashMap<String, Method> f5835a = new HashMap<>();

    /* renamed from: b  reason: collision with root package name */
    private static Object f5836b;

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0046 A[SYNTHETIC, Splitter:B:13:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0079 A[SYNTHETIC, Splitter:B:28:0x0079] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long a(int r8) {
        /*
            r0 = 0
            r4 = 0
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            r6.<init>()     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            java.lang.String r7 = "/proc/"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            java.lang.String r7 = "/stat"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0084, all -> 0x0075 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x003a }
            r2.<init>()     // Catch:{ Exception -> 0x003a }
        L_0x0030:
            java.lang.String r4 = r3.readLine()     // Catch:{ Exception -> 0x003a }
            if (r4 == 0) goto L_0x004a
            r2.append(r4)     // Catch:{ Exception -> 0x003a }
            goto L_0x0030
        L_0x003a:
            r2 = move-exception
        L_0x003b:
            java.lang.String r4 = "getUsageTime"
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x0082 }
            android.util.Log.e(r4, r5, r2)     // Catch:{ all -> 0x0082 }
            if (r3 == 0) goto L_0x0049
            r3.close()     // Catch:{ Exception -> 0x0070 }
        L_0x0049:
            return r0
        L_0x004a:
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x003a }
            java.lang.String r4 = " "
            java.lang.String[] r2 = r2.split(r4)     // Catch:{ Exception -> 0x003a }
            r4 = 21
            r2 = r2[r4]     // Catch:{ Exception -> 0x003a }
            long r4 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x003a }
            r6 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 * r6
            long r6 = a()     // Catch:{ Exception -> 0x003a }
            long r0 = r4 / r6
            if (r3 == 0) goto L_0x0049
            r3.close()     // Catch:{ Exception -> 0x006b }
            goto L_0x0049
        L_0x006b:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0049
        L_0x0070:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0049
        L_0x0075:
            r0 = move-exception
            r3 = r4
        L_0x0077:
            if (r3 == 0) goto L_0x007c
            r3.close()     // Catch:{ Exception -> 0x007d }
        L_0x007c:
            throw r0
        L_0x007d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x007c
        L_0x0082:
            r0 = move-exception
            goto L_0x0077
        L_0x0084:
            r2 = move-exception
            r3 = r4
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pureapps.cleaner.process.b.a(int):long");
    }

    @TargetApi(21)
    public static long a() {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                return Os.sysconf(6);
            }
            Method a2 = a("sysconf");
            if (a2 == null) {
                return -1;
            }
            return ((Long) a2.invoke(f5836b, 6)).longValue();
        } catch (Throwable th) {
            Log.e("LibCore", "", th);
            return -1;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1 = java.lang.Class.forName("libcore.io.Libcore").getDeclaredField("os");
        r1.setAccessible(true);
        com.pureapps.cleaner.process.b.f5836b = r1.get(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002a, code lost:
        if (com.pureapps.cleaner.process.b.f5836b == null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        r2 = com.pureapps.cleaner.process.b.f5836b.getClass().getMethods();
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        if (r1 >= r2.length) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0044, code lost:
        if (r5.equals(r2[r1].getName()) == false) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0046, code lost:
        r0 = r2[r1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0048, code lost:
        if (r0 == null) goto L_0x000f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004a, code lost:
        r0.setAccessible(true);
        com.pureapps.cleaner.process.b.f5835a.put(r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0054, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0061, code lost:
        r1 = r1 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.reflect.Method a(java.lang.String r5) {
        /*
            r1 = 0
            java.util.HashMap<java.lang.String, java.lang.reflect.Method> r2 = com.pureapps.cleaner.process.b.f5835a     // Catch:{ Exception -> 0x0064 }
            monitor-enter(r2)     // Catch:{ Exception -> 0x0064 }
            java.util.HashMap<java.lang.String, java.lang.reflect.Method> r0 = com.pureapps.cleaner.process.b.f5835a     // Catch:{ all -> 0x0059 }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x0059 }
            java.lang.reflect.Method r0 = (java.lang.reflect.Method) r0     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x0010
            monitor-exit(r2)     // Catch:{ all -> 0x0069 }
        L_0x000f:
            return r0
        L_0x0010:
            monitor-exit(r2)     // Catch:{ all -> 0x0069 }
            java.lang.String r1 = "libcore.io.Libcore"
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ Exception -> 0x0054 }
            java.lang.String r2 = "os"
            java.lang.reflect.Field r1 = r1.getDeclaredField(r2)     // Catch:{ Exception -> 0x0054 }
            r2 = 1
            r1.setAccessible(r2)     // Catch:{ Exception -> 0x0054 }
            r2 = 0
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x0054 }
            com.pureapps.cleaner.process.b.f5836b = r1     // Catch:{ Exception -> 0x0054 }
            java.lang.Object r1 = com.pureapps.cleaner.process.b.f5836b     // Catch:{ Exception -> 0x0054 }
            if (r1 == 0) goto L_0x0048
            java.lang.Object r1 = com.pureapps.cleaner.process.b.f5836b     // Catch:{ Exception -> 0x0054 }
            java.lang.Class r1 = r1.getClass()     // Catch:{ Exception -> 0x0054 }
            java.lang.reflect.Method[] r2 = r1.getMethods()     // Catch:{ Exception -> 0x0054 }
            r1 = 0
        L_0x0037:
            int r3 = r2.length     // Catch:{ Exception -> 0x0054 }
            if (r1 >= r3) goto L_0x0048
            r3 = r2[r1]     // Catch:{ Exception -> 0x0054 }
            java.lang.String r3 = r3.getName()     // Catch:{ Exception -> 0x0054 }
            boolean r3 = r5.equals(r3)     // Catch:{ Exception -> 0x0054 }
            if (r3 == 0) goto L_0x0061
            r0 = r2[r1]     // Catch:{ Exception -> 0x0054 }
        L_0x0048:
            if (r0 == 0) goto L_0x000f
            r1 = 1
            r0.setAccessible(r1)     // Catch:{ Exception -> 0x0054 }
            java.util.HashMap<java.lang.String, java.lang.reflect.Method> r1 = com.pureapps.cleaner.process.b.f5835a     // Catch:{ Exception -> 0x0054 }
            r1.put(r5, r0)     // Catch:{ Exception -> 0x0054 }
            goto L_0x000f
        L_0x0054:
            r1 = move-exception
        L_0x0055:
            r1.printStackTrace()
            goto L_0x000f
        L_0x0059:
            r0 = move-exception
        L_0x005a:
            monitor-exit(r2)     // Catch:{ all -> 0x0059 }
            throw r0     // Catch:{ Exception -> 0x005c }
        L_0x005c:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x0055
        L_0x0061:
            int r1 = r1 + 1
            goto L_0x0037
        L_0x0064:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
            goto L_0x0055
        L_0x0069:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pureapps.cleaner.process.b.a(java.lang.String):java.lang.reflect.Method");
    }
}
