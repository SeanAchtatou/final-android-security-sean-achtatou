package com.pureapps.cleaner.process;

import android.os.Parcel;
import android.os.Parcelable;
import com.kingouser.com.util.ShellUtils;
import java.util.ArrayList;
import java.util.Iterator;

public final class Cgroup extends ProcFile {
    public static final Parcelable.Creator<Cgroup> CREATOR = new Parcelable.Creator<Cgroup>() {
        /* renamed from: a */
        public Cgroup createFromParcel(Parcel parcel) {
            return new Cgroup(parcel);
        }

        /* renamed from: a */
        public Cgroup[] newArray(int i) {
            return new Cgroup[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public final ArrayList<ControlGroup> f5828a;

    public static Cgroup a(int i) {
        return new Cgroup(String.format("/proc/%d/cgroup", Integer.valueOf(i)));
    }

    private Cgroup(String str) {
        super(str);
        String[] split = this.f5832b.split(ShellUtils.COMMAND_LINE_END);
        this.f5828a = new ArrayList<>();
        for (String controlGroup : split) {
            try {
                this.f5828a.add(new ControlGroup(controlGroup));
            } catch (Exception e2) {
            }
        }
    }

    private Cgroup(Parcel parcel) {
        super(parcel);
        this.f5828a = parcel.createTypedArrayList(ControlGroup.CREATOR);
    }

    public ControlGroup a(String str) {
        Iterator<ControlGroup> it = this.f5828a.iterator();
        while (it.hasNext()) {
            ControlGroup next = it.next();
            String[] split = next.f5830b.split(",");
            int length = split.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    if (split[i].equals(str)) {
                        return next;
                    }
                    i++;
                }
            }
        }
        return null;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeTypedList(this.f5828a);
    }
}
