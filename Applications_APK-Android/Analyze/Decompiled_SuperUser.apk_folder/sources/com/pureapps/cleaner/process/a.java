package com.pureapps.cleaner.process;

import android.app.ActivityManager;
import com.pureapps.cleaner.process.AndroidAppProcess;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProcessManager */
public class a {
    public static List<AndroidAppProcess> a() {
        ArrayList arrayList = new ArrayList();
        for (File file : new File("/proc").listFiles()) {
            if (file.isDirectory()) {
                try {
                    try {
                        arrayList.add(new AndroidAppProcess(Integer.parseInt(file.getName())));
                    } catch (AndroidAppProcess.NotAndroidAppProcessException e2) {
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                } catch (NumberFormatException e4) {
                }
            }
        }
        return arrayList;
    }

    public static List<AndroidAppProcess> a(ActivityManager activityManager) {
        ArrayList arrayList = new ArrayList();
        List<ActivityManager.RunningServiceInfo> runningServices = activityManager.getRunningServices(500);
        int size = runningServices.size();
        for (int i = 0; i < size; i++) {
            try {
                arrayList.add(new AndroidAppProcess(runningServices.get(i)));
            } catch (AndroidAppProcess.NotAndroidAppProcessException e2) {
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return arrayList;
    }
}
