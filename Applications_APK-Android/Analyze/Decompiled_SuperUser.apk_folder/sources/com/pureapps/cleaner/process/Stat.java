package com.pureapps.cleaner.process;

import android.os.Parcel;
import android.os.Parcelable;

public final class Stat extends ProcFile {
    public static final Parcelable.Creator<Stat> CREATOR = new Parcelable.Creator<Stat>() {
        /* renamed from: a */
        public Stat createFromParcel(Parcel parcel) {
            return new Stat(parcel);
        }

        /* renamed from: a */
        public Stat[] newArray(int i) {
            return new Stat[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final String[] f5833a;

    public static Stat a(int i) {
        return new Stat(String.format("/proc/%d/stat", Integer.valueOf(i)));
    }

    private Stat(String str) {
        super(str);
        this.f5833a = this.f5832b.split("\\s+");
    }

    private Stat(Parcel parcel) {
        super(parcel);
        this.f5833a = parcel.createStringArray();
    }

    public String a() {
        return this.f5833a[1].replace("(", "").replace(")", "");
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeStringArray(this.f5833a);
    }
}
