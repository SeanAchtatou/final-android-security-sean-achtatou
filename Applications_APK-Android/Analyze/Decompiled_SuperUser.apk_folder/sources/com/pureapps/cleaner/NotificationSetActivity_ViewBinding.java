package com.pureapps.cleaner;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kingouser.com.R;

public class NotificationSetActivity_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private NotificationSetActivity f5432a;

    public NotificationSetActivity_ViewBinding(NotificationSetActivity notificationSetActivity, View view) {
        this.f5432a = notificationSetActivity;
        notificationSetActivity.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.dy, "field 'mRecyclerView'", RecyclerView.class);
        notificationSetActivity.floatView = (TextView) Utils.findRequiredViewAsType(view, R.id.e1, "field 'floatView'", TextView.class);
    }

    public void unbind() {
        NotificationSetActivity notificationSetActivity = this.f5432a;
        if (notificationSetActivity == null) {
            throw new IllegalStateException("Bindings already cleared.");
        }
        this.f5432a = null;
        notificationSetActivity.mRecyclerView = null;
        notificationSetActivity.floatView = null;
    }
}
