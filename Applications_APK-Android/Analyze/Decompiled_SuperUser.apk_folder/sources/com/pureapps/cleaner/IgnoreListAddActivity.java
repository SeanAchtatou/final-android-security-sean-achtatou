package com.pureapps.cleaner;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.OnClick;
import com.kingouser.com.BaseActivity;
import com.kingouser.com.R;
import com.lody.virtual.helper.utils.FileUtils;
import com.pureapps.cleaner.adapter.IgnoreListAdapter;
import com.pureapps.cleaner.util.k;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import me.everything.android.ui.overscroll.g;

public class IgnoreListAddActivity extends BaseActivity {
    @BindView(R.id.fs)
    Button mBtnAdd;
    @BindView(R.id.d8)
    RecyclerView mListView;
    @BindView(R.id.fr)
    ProgressBar mProgressBar;
    private c n;
    /* access modifiers changed from: private */
    public IgnoreListAdapter p;
    private b q;
    private a r;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.as);
        ActionBar f2 = f();
        if (f2 != null) {
            f2.d(true);
            f2.a(true);
        }
        this.n = new c();
        this.mProgressBar.setVisibility(0);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.b(1);
        this.mListView.setLayoutManager(linearLayoutManager);
        this.mListView.setHasFixedSize(true);
        g.a(this.mListView, 0);
        this.p = new IgnoreListAdapter(this);
        this.mListView.setAdapter(this.p);
        m();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.pureapps.cleaner.analytic.a.a(this).b(this.o, "AddIgnoreList");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        k.a(this.q, true);
        k.a(this.r, true);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }

    @OnClick({2131624176})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fs /*2131624176*/:
                l();
                return;
            default:
                return;
        }
    }

    public void a(CheckBox checkBox, int i) {
        com.pureapps.cleaner.bean.a b2 = this.p.b(i);
        b2.f5611f = !b2.f5611f;
        checkBox.setChecked(b2.f5611f);
        j();
    }

    private void j() {
        if (k() > 0) {
            this.mBtnAdd.setSelected(true);
            this.mBtnAdd.setEnabled(true);
            this.mBtnAdd.setTextColor(getResources().getColor(R.color.a_));
            return;
        }
        this.mBtnAdd.setEnabled(false);
        this.mBtnAdd.setSelected(false);
        this.mBtnAdd.setTextColor(getResources().getColor(R.color.a_));
    }

    private int k() {
        int i = 0;
        Iterator<com.pureapps.cleaner.bean.a> it = this.p.a().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            if (it.next().f5611f) {
                i = i2 + 1;
            } else {
                i = i2;
            }
        }
    }

    private void l() {
        com.pureapps.cleaner.analytic.a.a(this).c(this.o, "BtnIgnoreListAddClick");
        k.a(this.r, true);
        this.r = new a();
        this.r.executeOnExecutor(k.a().b(), new String[0]);
    }

    class a extends AsyncTask<String, String, ArrayList<String>> {
        a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ArrayList<String> doInBackground(String... strArr) {
            ArrayList<com.pureapps.cleaner.bean.a> a2 = IgnoreListAddActivity.this.p.a();
            ArrayList arrayList = new ArrayList();
            Iterator<com.pureapps.cleaner.bean.a> it = a2.iterator();
            while (it.hasNext()) {
                com.pureapps.cleaner.bean.a next = it.next();
                if (next.f5611f) {
                    arrayList.add(next);
                }
            }
            com.pureapps.cleaner.bean.a.a(IgnoreListAddActivity.this, arrayList);
            ArrayList<com.pureapps.cleaner.bean.a> a3 = com.pureapps.cleaner.bean.a.a(IgnoreListAddActivity.this);
            ArrayList<String> arrayList2 = new ArrayList<>();
            Iterator<com.pureapps.cleaner.bean.a> it2 = a3.iterator();
            while (it2.hasNext()) {
                arrayList2.add(it2.next().f5608c);
            }
            return arrayList2;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(ArrayList<String> arrayList) {
            super.onPostExecute(arrayList);
            IgnoreListAddActivity.this.setResult(-1);
            IgnoreListAddActivity.this.finish();
        }
    }

    private void m() {
        k.a(this.q, true);
        this.q = new b();
        this.q.executeOnExecutor(k.a().b(), new String[0]);
    }

    class b extends AsyncTask<String, String, ArrayList<com.pureapps.cleaner.bean.a>> {
        b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ArrayList<com.pureapps.cleaner.bean.a> doInBackground(String... strArr) {
            boolean z;
            PackageManager packageManager = IgnoreListAddActivity.this.getPackageManager();
            ArrayList<com.pureapps.cleaner.bean.a> a2 = com.pureapps.cleaner.bean.a.a(IgnoreListAddActivity.this);
            List<PackageInfo> installedPackages = packageManager.getInstalledPackages(FileUtils.FileMode.MODE_IWUSR);
            ArrayList<com.pureapps.cleaner.bean.a> arrayList = new ArrayList<>();
            for (PackageInfo next : installedPackages) {
                try {
                    Iterator<com.pureapps.cleaner.bean.a> it = a2.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (!next.packageName.equals(it.next().f5608c)) {
                                if (next.packageName.equals(IgnoreListAddActivity.this.getPackageName())) {
                                    z = true;
                                    break;
                                }
                            } else {
                                z = true;
                                break;
                            }
                        } else {
                            z = false;
                            break;
                        }
                    }
                    if (next.packageName.endsWith(IgnoreListAddActivity.this.getPackageName())) {
                        z = true;
                    }
                    if (!z) {
                        com.pureapps.cleaner.bean.a aVar = new com.pureapps.cleaner.bean.a();
                        aVar.f5608c = next.packageName;
                        aVar.f5610e = next.applicationInfo.loadLabel(packageManager).toString();
                        try {
                            aVar.f5609d = next.applicationInfo.loadIcon(packageManager);
                        } catch (OutOfMemoryError e2) {
                            aVar.f5609d = packageManager.getDefaultActivityIcon();
                        }
                        arrayList.add(aVar);
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
            return arrayList;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(ArrayList<com.pureapps.cleaner.bean.a> arrayList) {
            super.onPostExecute(arrayList);
            IgnoreListAddActivity.this.mProgressBar.setVisibility(8);
            IgnoreListAddActivity.this.p.a(arrayList);
        }
    }

    class c extends Handler {
        c() {
        }

        public void handleMessage(Message message) {
            int i = message.what;
        }
    }
}
