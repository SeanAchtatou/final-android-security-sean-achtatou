package com.pureapps.cleaner.b;

import android.util.Log;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: DataCenter */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static ConcurrentLinkedQueue<c> f5574a = new ConcurrentLinkedQueue<>();

    public static void a(c cVar) {
        Log.d("DataCenter", "RegisterObserver:" + cVar);
        if (f5574a == null || cVar == null) {
            Log.d("DataCenter", "RegisterObserver _notifyObservers == null");
            return;
        }
        synchronized (f5574a) {
            f5574a.add(cVar);
        }
    }

    public static void b(c cVar) {
        Log.d("DataCenter", "unRegisterObserver:" + cVar);
        if (f5574a == null || cVar == null) {
            Log.d("DataCenter", "unRegisterObserver _notifyObservers == null");
            return;
        }
        synchronized (f5574a) {
            f5574a.remove(cVar);
        }
    }

    public static void a(int i, long j, Object obj) {
        if (f5574a != null) {
            synchronized (f5574a) {
                if (!f5574a.isEmpty()) {
                    Iterator<c> it = f5574a.iterator();
                    while (it.hasNext()) {
                        it.next().a(i, j, obj);
                    }
                }
            }
        }
    }
}
