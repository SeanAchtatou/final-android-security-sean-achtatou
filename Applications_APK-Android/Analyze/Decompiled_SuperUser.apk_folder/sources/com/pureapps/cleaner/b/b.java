package com.pureapps.cleaner.b;

import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: DiskLruCache */
public final class b implements Closeable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Charset f5575a = Charset.forName("UTF-8");
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final File f5576b;

    /* renamed from: c  reason: collision with root package name */
    private final File f5577c;

    /* renamed from: d  reason: collision with root package name */
    private final File f5578d;

    /* renamed from: e  reason: collision with root package name */
    private final int f5579e;

    /* renamed from: f  reason: collision with root package name */
    private final long f5580f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public final int f5581g;

    /* renamed from: h  reason: collision with root package name */
    private long f5582h = 0;
    /* access modifiers changed from: private */
    public Writer i;
    private final LinkedHashMap<String, C0088b> j = new LinkedHashMap<>(0, 0.75f, true);
    /* access modifiers changed from: private */
    public int k;
    private long l = 0;
    private final ExecutorService m = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue());
    private final Callable<Void> n = new Callable<Void>() {
        /* renamed from: a */
        public Void call() {
            synchronized (b.this) {
                if (b.this.i != null) {
                    b.this.i();
                    if (b.this.g()) {
                        b.this.f();
                        int unused = b.this.k = 0;
                    }
                }
            }
            return null;
        }
    };

    private static <T> T[] a(T[] tArr, int i2, int i3) {
        int length = tArr.length;
        if (i2 > i3) {
            throw new IllegalArgumentException();
        } else if (i2 < 0 || i2 > length) {
            throw new ArrayIndexOutOfBoundsException();
        } else {
            int i4 = i3 - i2;
            int min = Math.min(i4, length - i2);
            T[] tArr2 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), i4);
            System.arraycopy(tArr, i2, tArr2, 0, min);
            return tArr2;
        }
    }

    public static String a(Reader reader) {
        try {
            StringWriter stringWriter = new StringWriter();
            char[] cArr = new char[FileUtils.FileMode.MODE_ISGID];
            while (true) {
                int read = reader.read(cArr);
                if (read == -1) {
                    return stringWriter.toString();
                }
                stringWriter.write(cArr, 0, read);
            }
        } finally {
            reader.close();
        }
    }

    public static String a(InputStream inputStream) {
        StringBuilder sb = new StringBuilder(80);
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                throw new EOFException();
            } else if (read == 10) {
                int length = sb.length();
                if (length > 0 && sb.charAt(length - 1) == 13) {
                    sb.setLength(length - 1);
                }
                return sb.toString();
            } else {
                sb.append((char) read);
            }
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    public static void a(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            throw new IllegalArgumentException("not a directory: " + file);
        }
        for (File file2 : listFiles) {
            if (file2.isDirectory()) {
                a(file2);
            }
            if (!file2.delete()) {
                throw new IOException("failed to delete file: " + file2);
            }
        }
    }

    private b(File file, int i2, int i3, long j2) {
        this.f5576b = file;
        this.f5579e = i2;
        this.f5577c = new File(file, "journal");
        this.f5578d = new File(file, "journal.tmp");
        this.f5581g = i3;
        this.f5580f = j2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    public static b a(File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 <= 0) {
            throw new IllegalArgumentException("valueCount <= 0");
        } else {
            b bVar = new b(file, i2, i3, j2);
            if (bVar.f5577c.exists()) {
                try {
                    bVar.d();
                    bVar.e();
                    bVar.i = new BufferedWriter(new FileWriter(bVar.f5577c, true), 8192);
                    return bVar;
                } catch (IOException e2) {
                    bVar.b();
                }
            }
            file.mkdirs();
            b bVar2 = new b(file, i2, i3, j2);
            bVar2.f();
            return bVar2;
        }
    }

    private void d() {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(this.f5577c), 8192);
        try {
            String a2 = a((InputStream) bufferedInputStream);
            String a3 = a((InputStream) bufferedInputStream);
            String a4 = a((InputStream) bufferedInputStream);
            String a5 = a((InputStream) bufferedInputStream);
            String a6 = a((InputStream) bufferedInputStream);
            if (!"libcore.io.DiskLruCache".equals(a2) || !"1".equals(a3) || !Integer.toString(this.f5579e).equals(a4) || !Integer.toString(this.f5581g).equals(a5) || !"".equals(a6)) {
                throw new IOException("unexpected journal header: [" + a2 + ", " + a3 + ", " + a5 + ", " + a6 + "]");
            }
            while (true) {
                try {
                    d(a((InputStream) bufferedInputStream));
                } catch (EOFException e2) {
                    return;
                }
            }
        } finally {
            a((Closeable) bufferedInputStream);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.b.b.b.a(com.pureapps.cleaner.b.b$b, boolean):boolean
     arg types: [com.pureapps.cleaner.b.b$b, int]
     candidates:
      com.pureapps.cleaner.b.b.b.a(com.pureapps.cleaner.b.b$b, long):long
      com.pureapps.cleaner.b.b.b.a(com.pureapps.cleaner.b.b$b, com.pureapps.cleaner.b.b$a):com.pureapps.cleaner.b.b$a
      com.pureapps.cleaner.b.b.b.a(com.pureapps.cleaner.b.b$b, java.lang.String[]):void
      com.pureapps.cleaner.b.b.b.a(com.pureapps.cleaner.b.b$b, boolean):boolean */
    private void d(String str) {
        C0088b bVar;
        String[] split = str.split(" ");
        if (split.length < 2) {
            throw new IOException("unexpected journal line: " + str);
        }
        String str2 = split[1];
        if (!split[0].equals("REMOVE") || split.length != 2) {
            C0088b bVar2 = this.j.get(str2);
            if (bVar2 == null) {
                C0088b bVar3 = new C0088b(str2);
                this.j.put(str2, bVar3);
                bVar = bVar3;
            } else {
                bVar = bVar2;
            }
            if (split[0].equals("CLEAN") && split.length == this.f5581g + 2) {
                boolean unused = bVar.f5591d = true;
                a unused2 = bVar.f5592e = (a) null;
                bVar.a((String[]) a(split, 2, split.length));
            } else if (split[0].equals("DIRTY") && split.length == 2) {
                a unused3 = bVar.f5592e = new a(bVar);
            } else if (!split[0].equals("READ") || split.length != 2) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            this.j.remove(str2);
        }
    }

    private void e() {
        b(this.f5578d);
        Iterator<C0088b> it = this.j.values().iterator();
        while (it.hasNext()) {
            C0088b next = it.next();
            if (next.f5592e == null) {
                for (int i2 = 0; i2 < this.f5581g; i2++) {
                    this.f5582h += next.f5590c[i2];
                }
            } else {
                a unused = next.f5592e = (a) null;
                for (int i3 = 0; i3 < this.f5581g; i3++) {
                    b(next.a(i3));
                    b(next.b(i3));
                }
                it.remove();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* access modifiers changed from: private */
    public synchronized void f() {
        if (this.i != null) {
            this.i.close();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(this.f5578d), 8192);
        bufferedWriter.write("libcore.io.DiskLruCache");
        bufferedWriter.write(ShellUtils.COMMAND_LINE_END);
        bufferedWriter.write("1");
        bufferedWriter.write(ShellUtils.COMMAND_LINE_END);
        bufferedWriter.write(Integer.toString(this.f5579e));
        bufferedWriter.write(ShellUtils.COMMAND_LINE_END);
        bufferedWriter.write(Integer.toString(this.f5581g));
        bufferedWriter.write(ShellUtils.COMMAND_LINE_END);
        bufferedWriter.write(ShellUtils.COMMAND_LINE_END);
        for (C0088b next : this.j.values()) {
            if (next.f5592e != null) {
                bufferedWriter.write("DIRTY " + next.f5589b + 10);
            } else {
                bufferedWriter.write("CLEAN " + next.f5589b + next.a() + 10);
            }
        }
        bufferedWriter.close();
        this.f5578d.renameTo(this.f5577c);
        this.i = new BufferedWriter(new FileWriter(this.f5577c, true), 8192);
    }

    private static void b(File file) {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    public synchronized c a(String str) {
        c cVar = null;
        synchronized (this) {
            h();
            e(str);
            C0088b bVar = this.j.get(str);
            if (bVar != null) {
                if (bVar.f5591d) {
                    InputStream[] inputStreamArr = new InputStream[this.f5581g];
                    int i2 = 0;
                    while (i2 < this.f5581g) {
                        try {
                            inputStreamArr[i2] = new FileInputStream(bVar.a(i2));
                            i2++;
                        } catch (FileNotFoundException e2) {
                        }
                    }
                    this.k++;
                    this.i.append((CharSequence) ("READ " + str + 10));
                    if (g()) {
                        this.m.submit(this.n);
                    }
                    cVar = new c(str, bVar.f5593f, inputStreamArr);
                }
            }
        }
        return cVar;
    }

    public a b(String str) {
        return a(str, -1);
    }

    private synchronized a a(String str, long j2) {
        C0088b bVar;
        a aVar;
        h();
        e(str);
        C0088b bVar2 = this.j.get(str);
        if (j2 == -1 || (bVar2 != null && bVar2.f5593f == j2)) {
            if (bVar2 == null) {
                C0088b bVar3 = new C0088b(str);
                this.j.put(str, bVar3);
                bVar = bVar3;
            } else if (bVar2.f5592e != null) {
                aVar = null;
            } else {
                bVar = bVar2;
            }
            aVar = new a(bVar);
            a unused = bVar.f5592e = aVar;
            this.i.write("DIRTY " + str + 10);
            this.i.flush();
        } else {
            aVar = null;
        }
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pureapps.cleaner.b.b.b.a(com.pureapps.cleaner.b.b$b, boolean):boolean
     arg types: [com.pureapps.cleaner.b.b$b, int]
     candidates:
      com.pureapps.cleaner.b.b.b.a(com.pureapps.cleaner.b.b$b, long):long
      com.pureapps.cleaner.b.b.b.a(com.pureapps.cleaner.b.b$b, com.pureapps.cleaner.b.b$a):com.pureapps.cleaner.b.b$a
      com.pureapps.cleaner.b.b.b.a(com.pureapps.cleaner.b.b$b, java.lang.String[]):void
      com.pureapps.cleaner.b.b.b.a(com.pureapps.cleaner.b.b$b, boolean):boolean */
    /* access modifiers changed from: private */
    public synchronized void a(a aVar, boolean z) {
        synchronized (this) {
            C0088b a2 = aVar.f5585b;
            if (a2.f5592e != aVar) {
                throw new IllegalStateException();
            }
            if (z) {
                if (!a2.f5591d) {
                    for (int i2 = 0; i2 < this.f5581g; i2++) {
                        if (!a2.b(i2).exists()) {
                            aVar.b();
                            throw new IllegalStateException("edit didn't create file " + i2);
                        }
                    }
                }
            }
            for (int i3 = 0; i3 < this.f5581g; i3++) {
                File b2 = a2.b(i3);
                if (!z) {
                    b(b2);
                } else if (b2.exists()) {
                    File a3 = a2.a(i3);
                    b2.renameTo(a3);
                    long j2 = a2.f5590c[i3];
                    long length = a3.length();
                    a2.f5590c[i3] = length;
                    this.f5582h = (this.f5582h - j2) + length;
                }
            }
            this.k++;
            a unused = a2.f5592e = (a) null;
            if (a2.f5591d || z) {
                boolean unused2 = a2.f5591d = true;
                this.i.write("CLEAN " + a2.f5589b + a2.a() + 10);
                if (z) {
                    long j3 = this.l;
                    this.l = 1 + j3;
                    long unused3 = a2.f5593f = j3;
                }
            } else {
                this.j.remove(a2.f5589b);
                this.i.write("REMOVE " + a2.f5589b + 10);
            }
            if (this.f5582h > this.f5580f || g()) {
                this.m.submit(this.n);
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean g() {
        return this.k >= 2000 && this.k >= this.j.size();
    }

    public synchronized boolean c(String str) {
        boolean z;
        synchronized (this) {
            h();
            e(str);
            C0088b bVar = this.j.get(str);
            if (bVar == null || bVar.f5592e != null) {
                z = false;
            } else {
                for (int i2 = 0; i2 < this.f5581g; i2++) {
                    File a2 = bVar.a(i2);
                    if (!a2.delete()) {
                        throw new IOException("failed to delete " + a2);
                    }
                    this.f5582h -= bVar.f5590c[i2];
                    bVar.f5590c[i2] = 0;
                }
                this.k++;
                this.i.append((CharSequence) ("REMOVE " + str + 10));
                this.j.remove(str);
                if (g()) {
                    this.m.submit(this.n);
                }
                z = true;
            }
        }
        return z;
    }

    private void h() {
        if (this.i == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    public synchronized void a() {
        h();
        i();
        this.i.flush();
    }

    public synchronized void close() {
        if (this.i != null) {
            Iterator it = new ArrayList(this.j.values()).iterator();
            while (it.hasNext()) {
                C0088b bVar = (C0088b) it.next();
                if (bVar.f5592e != null) {
                    bVar.f5592e.b();
                }
            }
            i();
            this.i.close();
            this.i = null;
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        while (this.f5582h > this.f5580f) {
            c((String) this.j.entrySet().iterator().next().getKey());
        }
    }

    public void b() {
        close();
        a(this.f5576b);
    }

    private void e(String str) {
        if (str.contains(" ") || str.contains(ShellUtils.COMMAND_LINE_END) || str.contains("\r")) {
            throw new IllegalArgumentException("keys must not contain spaces or newlines: \"" + str + "\"");
        }
    }

    /* access modifiers changed from: private */
    public static String c(InputStream inputStream) {
        return a((Reader) new InputStreamReader(inputStream, f5575a));
    }

    /* compiled from: DiskLruCache */
    public final class c implements Closeable {

        /* renamed from: b  reason: collision with root package name */
        private final String f5595b;

        /* renamed from: c  reason: collision with root package name */
        private final long f5596c;

        /* renamed from: d  reason: collision with root package name */
        private final InputStream[] f5597d;

        private c(String str, long j, InputStream[] inputStreamArr) {
            this.f5595b = str;
            this.f5596c = j;
            this.f5597d = inputStreamArr;
        }

        public InputStream a(int i) {
            return this.f5597d[i];
        }

        public String b(int i) {
            return b.c(a(i));
        }

        public void close() {
            for (InputStream inputStream : this.f5597d) {
                b.a((Closeable) inputStream);
            }
        }
    }

    /* compiled from: DiskLruCache */
    public final class a {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final C0088b f5585b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public boolean f5586c;

        private a(C0088b bVar) {
            this.f5585b = bVar;
        }

        public OutputStream a(int i) {
            C0087a aVar;
            synchronized (b.this) {
                if (this.f5585b.f5592e != this) {
                    throw new IllegalStateException();
                }
                aVar = new C0087a(new FileOutputStream(this.f5585b.b(i)));
            }
            return aVar;
        }

        public void a(int i, String str) {
            OutputStreamWriter outputStreamWriter;
            try {
                outputStreamWriter = new OutputStreamWriter(a(i), b.f5575a);
                try {
                    outputStreamWriter.write(str);
                    b.a(outputStreamWriter);
                } catch (Throwable th) {
                    th = th;
                    b.a(outputStreamWriter);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                outputStreamWriter = null;
                b.a(outputStreamWriter);
                throw th;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.pureapps.cleaner.b.b.a(com.pureapps.cleaner.b.b, com.pureapps.cleaner.b.b$a, boolean):void
         arg types: [com.pureapps.cleaner.b.b, com.pureapps.cleaner.b.b$a, int]
         candidates:
          com.pureapps.cleaner.b.b.a(java.lang.Object[], int, int):T[]
          com.pureapps.cleaner.b.b.a(com.pureapps.cleaner.b.b, com.pureapps.cleaner.b.b$a, boolean):void */
        public void a() {
            if (this.f5586c) {
                b.this.a(this, false);
                b.this.c(this.f5585b.f5589b);
                return;
            }
            b.this.a(this, true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.pureapps.cleaner.b.b.a(com.pureapps.cleaner.b.b, com.pureapps.cleaner.b.b$a, boolean):void
         arg types: [com.pureapps.cleaner.b.b, com.pureapps.cleaner.b.b$a, int]
         candidates:
          com.pureapps.cleaner.b.b.a(java.lang.Object[], int, int):T[]
          com.pureapps.cleaner.b.b.a(com.pureapps.cleaner.b.b, com.pureapps.cleaner.b.b$a, boolean):void */
        public void b() {
            b.this.a(this, false);
        }

        /* renamed from: com.pureapps.cleaner.b.b$a$a  reason: collision with other inner class name */
        /* compiled from: DiskLruCache */
        private class C0087a extends FilterOutputStream {
            private C0087a(OutputStream outputStream) {
                super(outputStream);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.pureapps.cleaner.b.b.a.a(com.pureapps.cleaner.b.b$a, boolean):boolean
             arg types: [com.pureapps.cleaner.b.b$a, int]
             candidates:
              com.pureapps.cleaner.b.b.a.a(int, java.lang.String):void
              com.pureapps.cleaner.b.b.a.a(com.pureapps.cleaner.b.b$a, boolean):boolean */
            public void write(int i) {
                try {
                    this.out.write(i);
                } catch (IOException e2) {
                    boolean unused = a.this.f5586c = true;
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.pureapps.cleaner.b.b.a.a(com.pureapps.cleaner.b.b$a, boolean):boolean
             arg types: [com.pureapps.cleaner.b.b$a, int]
             candidates:
              com.pureapps.cleaner.b.b.a.a(int, java.lang.String):void
              com.pureapps.cleaner.b.b.a.a(com.pureapps.cleaner.b.b$a, boolean):boolean */
            public void write(byte[] bArr, int i, int i2) {
                try {
                    this.out.write(bArr, i, i2);
                } catch (IOException e2) {
                    boolean unused = a.this.f5586c = true;
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.pureapps.cleaner.b.b.a.a(com.pureapps.cleaner.b.b$a, boolean):boolean
             arg types: [com.pureapps.cleaner.b.b$a, int]
             candidates:
              com.pureapps.cleaner.b.b.a.a(int, java.lang.String):void
              com.pureapps.cleaner.b.b.a.a(com.pureapps.cleaner.b.b$a, boolean):boolean */
            public void close() {
                try {
                    this.out.close();
                } catch (IOException e2) {
                    boolean unused = a.this.f5586c = true;
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.pureapps.cleaner.b.b.a.a(com.pureapps.cleaner.b.b$a, boolean):boolean
             arg types: [com.pureapps.cleaner.b.b$a, int]
             candidates:
              com.pureapps.cleaner.b.b.a.a(int, java.lang.String):void
              com.pureapps.cleaner.b.b.a.a(com.pureapps.cleaner.b.b$a, boolean):boolean */
            public void flush() {
                try {
                    this.out.flush();
                } catch (IOException e2) {
                    boolean unused = a.this.f5586c = true;
                }
            }
        }
    }

    /* renamed from: com.pureapps.cleaner.b.b$b  reason: collision with other inner class name */
    /* compiled from: DiskLruCache */
    private final class C0088b {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final String f5589b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public final long[] f5590c;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public boolean f5591d;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public a f5592e;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public long f5593f;

        private C0088b(String str) {
            this.f5589b = str;
            this.f5590c = new long[b.this.f5581g];
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            for (long append : this.f5590c) {
                sb.append(' ').append(append);
            }
            return sb.toString();
        }

        /* access modifiers changed from: private */
        public void a(String[] strArr) {
            if (strArr.length != b.this.f5581g) {
                throw b(strArr);
            }
            int i = 0;
            while (i < strArr.length) {
                try {
                    this.f5590c[i] = Long.parseLong(strArr[i]);
                    i++;
                } catch (NumberFormatException e2) {
                    throw b(strArr);
                }
            }
        }

        private IOException b(String[] strArr) {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        public File a(int i) {
            return new File(b.this.f5576b, this.f5589b + "." + i);
        }

        public File b(int i) {
            return new File(b.this.f5576b, this.f5589b + "." + i + ".tmp");
        }
    }
}
