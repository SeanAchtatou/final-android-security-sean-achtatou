package com.muzakki.ahmad.widget;

import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

/* compiled from: AnimationUtils */
class a {

    /* renamed from: a  reason: collision with root package name */
    static final Interpolator f4482a = new LinearInterpolator();

    /* renamed from: b  reason: collision with root package name */
    static final Interpolator f4483b = new FastOutSlowInInterpolator();

    /* renamed from: c  reason: collision with root package name */
    static final Interpolator f4484c = new FastOutLinearInInterpolator();

    /* renamed from: d  reason: collision with root package name */
    static final Interpolator f4485d = new LinearOutSlowInInterpolator();

    /* renamed from: e  reason: collision with root package name */
    static final Interpolator f4486e = new DecelerateInterpolator();

    static float a(float f2, float f3, float f4) {
        return ((f3 - f2) * f4) + f2;
    }

    static int a(int i, int i2, float f2) {
        return Math.round(((float) (i2 - i)) * f2) + i;
    }
}
