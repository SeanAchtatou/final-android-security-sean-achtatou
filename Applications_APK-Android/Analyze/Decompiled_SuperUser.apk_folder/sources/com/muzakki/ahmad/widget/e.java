package com.muzakki.ahmad.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.a.a;

/* compiled from: ThemeUtils */
class e {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f4495a = {a.C0027a.colorPrimary};

    static void a(Context context) {
        boolean z = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(f4495a);
        if (!obtainStyledAttributes.hasValue(0)) {
            z = true;
        }
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
        if (z) {
            throw new IllegalArgumentException("You need to use a Theme.AppCompat theme (or descendant) with the design library.");
        }
    }
}
