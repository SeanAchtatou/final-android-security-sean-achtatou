package com.muzakki.ahmad.widget;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: ViewGroupUtils */
class i {

    /* renamed from: a  reason: collision with root package name */
    private static final a f4511a;

    /* compiled from: ViewGroupUtils */
    private interface a {
        void a(ViewGroup viewGroup, View view, Rect rect);
    }

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f4511a = new c();
        } else {
            f4511a = new b();
        }
    }

    static void a(ViewGroup viewGroup, View view, Rect rect) {
        f4511a.a(viewGroup, view, rect);
    }

    static void b(ViewGroup viewGroup, View view, Rect rect) {
        rect.set(0, 0, view.getWidth(), view.getHeight());
        a(viewGroup, view, rect);
    }

    /* compiled from: ViewGroupUtils */
    private static class b implements a {
        b() {
        }

        public void a(ViewGroup viewGroup, View view, Rect rect) {
            viewGroup.offsetDescendantRectToMyCoords(view, rect);
            rect.offset(view.getScrollX(), view.getScrollY());
        }
    }

    /* compiled from: ViewGroupUtils */
    private static class c implements a {
        c() {
        }

        public void a(ViewGroup viewGroup, View view, Rect rect) {
            j.a(viewGroup, view, rect);
        }
    }
}
