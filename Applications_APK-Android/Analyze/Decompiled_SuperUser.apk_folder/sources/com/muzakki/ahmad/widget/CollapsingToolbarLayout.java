package com.muzakki.ahmad.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.c;
import android.support.v4.view.ag;
import android.support.v4.view.be;
import android.support.v4.view.y;
import android.support.v7.a.a;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import com.lody.virtual.os.VUserInfo;
import com.muzakki.ahmad.widget.d;
import com.muzakki.ahmad.widget.f;

public class CollapsingToolbarLayout extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    final b f4469a;

    /* renamed from: b  reason: collision with root package name */
    Drawable f4470b;

    /* renamed from: c  reason: collision with root package name */
    int f4471c;

    /* renamed from: d  reason: collision with root package name */
    be f4472d;

    /* renamed from: e  reason: collision with root package name */
    private final Rect f4473e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f4474f;

    /* renamed from: g  reason: collision with root package name */
    private int f4475g;

    /* renamed from: h  reason: collision with root package name */
    private Toolbar f4476h;
    private View i;
    private View j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private boolean p;
    private boolean q;
    private Drawable r;
    private int s;
    private boolean t;
    private f u;
    private long v;
    private int w;
    private AppBarLayout.OnOffsetChangedListener x;

    public CollapsingToolbarLayout(Context context) {
        this(context, null);
    }

    public CollapsingToolbarLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CollapsingToolbarLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f4473e = new Rect();
        this.f4474f = true;
        this.w = -1;
        e.a(context);
        this.f4469a = new b(this);
        this.f4469a.a(a.f4486e);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d.C0085d.CollapsingToolbarLayout, i2, d.c.Widget_Design_CollapsingToolbar);
        this.f4469a.a(obtainStyledAttributes.getInt(d.C0085d.CollapsingToolbarLayout_expandedTitleGravity, 16));
        this.f4469a.b(obtainStyledAttributes.getInt(d.C0085d.CollapsingToolbarLayout_collapsedTitleGravity, 8388627));
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(d.C0085d.CollapsingToolbarLayout_expandedTitleMargin, 0);
        this.o = dimensionPixelSize;
        this.n = dimensionPixelSize;
        this.m = dimensionPixelSize;
        this.l = dimensionPixelSize;
        if (obtainStyledAttributes.hasValue(d.C0085d.CollapsingToolbarLayout_expandedTitleMarginStart)) {
            this.l = obtainStyledAttributes.getDimensionPixelSize(d.C0085d.CollapsingToolbarLayout_expandedTitleMarginStart, 0);
        }
        if (obtainStyledAttributes.hasValue(d.C0085d.CollapsingToolbarLayout_expandedTitleMarginEnd)) {
            this.n = obtainStyledAttributes.getDimensionPixelSize(d.C0085d.CollapsingToolbarLayout_expandedTitleMarginEnd, 0);
        }
        if (obtainStyledAttributes.hasValue(d.C0085d.CollapsingToolbarLayout_expandedTitleMarginTop)) {
            this.m = obtainStyledAttributes.getDimensionPixelSize(d.C0085d.CollapsingToolbarLayout_expandedTitleMarginTop, 0);
        }
        if (obtainStyledAttributes.hasValue(d.C0085d.CollapsingToolbarLayout_expandedTitleMarginBottom)) {
            this.o = obtainStyledAttributes.getDimensionPixelSize(d.C0085d.CollapsingToolbarLayout_expandedTitleMarginBottom, 0);
        }
        this.p = obtainStyledAttributes.getBoolean(d.C0085d.CollapsingToolbarLayout_titleEnabled, true);
        setTitle(obtainStyledAttributes.getText(d.C0085d.CollapsingToolbarLayout_title));
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, d.C0085d.SubtitleCollapsingToolbar, i2, d.c.gy);
        if (obtainStyledAttributes2.hasValue(d.C0085d.SubtitleCollapsingToolbar_subtitle)) {
            setSubtitle(obtainStyledAttributes2.getText(d.C0085d.SubtitleCollapsingToolbar_subtitle).toString());
        }
        this.f4469a.e(d.c.f7);
        this.f4469a.f(d.c.f9);
        if (obtainStyledAttributes2.hasValue(d.C0085d.SubtitleCollapsingToolbar_collapsedSubtitleAppearance)) {
            this.f4469a.e(obtainStyledAttributes2.getResourceId(d.C0085d.SubtitleCollapsingToolbar_collapsedSubtitleAppearance, 0));
        }
        if (obtainStyledAttributes2.hasValue(d.C0085d.SubtitleCollapsingToolbar_expandedSubtitleAppearance)) {
            this.f4469a.f(obtainStyledAttributes2.getResourceId(d.C0085d.SubtitleCollapsingToolbar_expandedSubtitleAppearance, 0));
        }
        this.f4469a.d(d.c.TextAppearance_Design_CollapsingToolbar_Expanded);
        this.f4469a.c(a.j.TextAppearance_AppCompat_Widget_ActionBar_Title);
        if (obtainStyledAttributes.hasValue(d.C0085d.CollapsingToolbarLayout_expandedTitleTextAppearance)) {
            this.f4469a.d(obtainStyledAttributes.getResourceId(d.C0085d.CollapsingToolbarLayout_expandedTitleTextAppearance, 0));
        }
        if (obtainStyledAttributes.hasValue(d.C0085d.CollapsingToolbarLayout_collapsedTitleTextAppearance)) {
            this.f4469a.c(obtainStyledAttributes.getResourceId(d.C0085d.CollapsingToolbarLayout_collapsedTitleTextAppearance, 0));
        }
        this.w = obtainStyledAttributes.getDimensionPixelSize(d.C0085d.CollapsingToolbarLayout_scrimVisibleHeightTrigger, -1);
        this.v = (long) obtainStyledAttributes.getInt(d.C0085d.CollapsingToolbarLayout_scrimAnimationDuration, 600);
        setContentScrim(obtainStyledAttributes.getDrawable(d.C0085d.CollapsingToolbarLayout_contentScrim));
        setStatusBarScrim(obtainStyledAttributes.getDrawable(d.C0085d.CollapsingToolbarLayout_statusBarScrim));
        this.f4475g = obtainStyledAttributes.getResourceId(d.C0085d.CollapsingToolbarLayout_toolbarId, -1);
        obtainStyledAttributes.recycle();
        setWillNotDraw(false);
        ag.a(this, new y() {
            public be onApplyWindowInsets(View view, be beVar) {
                return CollapsingToolbarLayout.this.a(beVar);
            }
        });
    }

    private static int c(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof ViewGroup.MarginLayoutParams)) {
            return view.getHeight();
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        return marginLayoutParams.bottomMargin + view.getHeight() + marginLayoutParams.topMargin;
    }

    static k a(View view) {
        k kVar = (k) view.getTag(d.b.view_offset_helper);
        if (kVar != null) {
            return kVar;
        }
        k kVar2 = new k(view);
        view.setTag(d.b.view_offset_helper, kVar2);
        return kVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.a(android.view.View, boolean):void
     arg types: [com.muzakki.ahmad.widget.CollapsingToolbarLayout, boolean]
     candidates:
      android.support.v4.view.ag.a(int, int):int
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.a(android.view.View, float):void
      android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
      android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
      android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
      android.support.v4.view.ag.a(android.view.View, int):boolean
      android.support.v4.view.ag.a(android.view.View, boolean):void */
    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (parent instanceof AppBarLayout) {
            ag.a((View) this, ag.x((View) parent));
            if (this.x == null) {
                this.x = new a();
            }
            ((AppBarLayout) parent).addOnOffsetChangedListener(this.x);
            ag.w(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        ViewParent parent = getParent();
        if (this.x != null && (parent instanceof AppBarLayout)) {
            ((AppBarLayout) parent).removeOnOffsetChangedListener(this.x);
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: package-private */
    public be a(be beVar) {
        be beVar2 = null;
        if (ag.x(this)) {
            beVar2 = beVar;
        }
        if (!l.a(this.f4472d, beVar2)) {
            this.f4472d = beVar2;
            requestLayout();
        }
        return beVar.g();
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        c();
        if (this.f4476h == null && this.r != null && this.s > 0) {
            this.r.mutate().setAlpha(this.s);
            this.r.draw(canvas);
        }
        if (this.p && this.q) {
            this.f4469a.a(canvas);
        }
        if (this.f4470b != null && this.s > 0) {
            int b2 = this.f4472d != null ? this.f4472d.b() : 0;
            if (b2 > 0) {
                this.f4470b.setBounds(0, -this.f4471c, getWidth(), b2 - this.f4471c);
                this.f4470b.mutate().setAlpha(this.s);
                this.f4470b.draw(canvas);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        boolean drawChild = super.drawChild(canvas, view, j2);
        if (this.r == null || this.s <= 0 || !d(view)) {
            return drawChild;
        }
        this.r.mutate().setAlpha(this.s);
        this.r.draw(canvas);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (this.r != null) {
            this.r.setBounds(0, 0, i2, i3);
        }
    }

    private void c() {
        Toolbar toolbar;
        if (this.f4474f) {
            this.f4476h = null;
            this.i = null;
            if (this.f4475g != -1) {
                this.f4476h = (Toolbar) findViewById(this.f4475g);
                if (this.f4476h != null) {
                    this.i = e(this.f4476h);
                }
            }
            if (this.f4476h == null) {
                int childCount = getChildCount();
                int i2 = 0;
                while (true) {
                    if (i2 >= childCount) {
                        toolbar = null;
                        break;
                    }
                    View childAt = getChildAt(i2);
                    if (childAt instanceof Toolbar) {
                        toolbar = (Toolbar) childAt;
                        break;
                    }
                    i2++;
                }
                this.f4476h = toolbar;
            }
            d();
            this.f4474f = false;
        }
    }

    private boolean d(View view) {
        return this.k >= 0 && this.k == indexOfChild(view) + 1;
    }

    private View e(View view) {
        ViewParent parent = view.getParent();
        View view2 = view;
        while (parent != this && parent != null) {
            if (parent instanceof View) {
                view2 = (View) parent;
            }
            parent = parent.getParent();
        }
        return view2;
    }

    private void d() {
        if (!this.p && this.j != null) {
            ViewParent parent = this.j.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.j);
            }
        }
        if (this.p && this.f4476h != null) {
            if (this.j == null) {
                this.j = new View(getContext());
            }
            if (this.j.getParent() == null) {
                this.f4476h.addView(this.j, -1, -1);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        c();
        super.onMeasure(i2, i3);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int titleMarginEnd;
        int i6;
        boolean z2 = true;
        super.onLayout(z, i2, i3, i4, i5);
        if (this.f4472d != null) {
            int b2 = this.f4472d.b();
            int childCount = getChildCount();
            for (int i7 = 0; i7 < childCount; i7++) {
                View childAt = getChildAt(i7);
                if (!ag.x(childAt) && childAt.getTop() < b2) {
                    ag.e(childAt, b2);
                }
            }
        }
        if (this.p && this.j != null) {
            this.q = ag.H(this.j) && this.j.getVisibility() == 0;
            if (this.q) {
                if (ag.g(this) != 1) {
                    z2 = false;
                }
                int b3 = b(this.i != null ? this.i : this.f4476h);
                i.b(this, this.j, this.f4473e);
                int titleMarginTop = this.f4476h.getTitleMarginTop() + this.f4473e.top + b3;
                b bVar = this.f4469a;
                int i8 = (int) (18.0f * getResources().getDisplayMetrics().density);
                int i9 = this.f4473e.right;
                if (z2) {
                    titleMarginEnd = this.f4476h.getTitleMarginStart();
                } else {
                    titleMarginEnd = this.f4476h.getTitleMarginEnd();
                }
                bVar.b(i8, titleMarginTop, titleMarginEnd + i9, (b3 + this.f4473e.bottom) - this.f4476h.getTitleMarginBottom());
                int statusBarHeight = (this.f4473e.bottom / 2) - getStatusBarHeight();
                b bVar2 = this.f4469a;
                int i10 = z2 ? this.n : this.l;
                int i11 = i4 - i2;
                if (z2) {
                    i6 = this.l;
                } else {
                    i6 = this.n;
                }
                bVar2.a(i10, 0, i11 - i6, statusBarHeight);
                this.f4469a.g();
            }
        }
        int childCount2 = getChildCount();
        for (int i12 = 0; i12 < childCount2; i12++) {
            a(getChildAt(i12)).a();
        }
        if (this.f4476h != null) {
            if (this.p && TextUtils.isEmpty(this.f4469a.h())) {
                this.f4469a.a(this.f4476h.getTitle());
            }
            if (this.i == null || this.i == this) {
                setMinimumHeight(c(this.f4476h));
                this.k = indexOfChild(this.f4476h);
            } else {
                setMinimumHeight(c(this.i));
                this.k = indexOfChild(this.i);
            }
        } else {
            this.k = -1;
        }
        b();
    }

    public int getStatusBarHeight() {
        int identifier = getResources().getIdentifier("status_bar_height", "dimen", io.fabric.sdk.android.services.common.a.ANDROID_CLIENT_TYPE);
        if (identifier > 0) {
            return getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    public CharSequence getTitle() {
        if (this.p) {
            return this.f4469a.h();
        }
        return null;
    }

    public void setTitle(CharSequence charSequence) {
        this.f4469a.a(charSequence);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f4469a.b(charSequence);
    }

    public void setSubTitleUnit(CharSequence charSequence) {
        this.f4469a.c(charSequence);
    }

    public void setSubTitle2(CharSequence charSequence) {
        this.f4469a.e(charSequence);
    }

    public void setUnitTextSize(float f2) {
        this.f4469a.b(f2);
    }

    public void setSubTitle2Unit(CharSequence charSequence) {
        this.f4469a.d(charSequence);
    }

    public void setMessageTitle(CharSequence charSequence) {
        this.f4469a.f(charSequence);
    }

    public void setTitleEnabled(boolean z) {
        if (z != this.p) {
            this.p = z;
            d();
            requestLayout();
        }
    }

    public void setScrimsShown(boolean z) {
        a(z, ag.F(this) && !isInEditMode());
    }

    public void a(boolean z, boolean z2) {
        int i2 = VUserInfo.FLAG_MASK_USER_TYPE;
        if (this.t != z) {
            if (z2) {
                if (!z) {
                    i2 = 0;
                }
                a(i2);
            } else {
                if (!z) {
                    i2 = 0;
                }
                setScrimAlpha(i2);
            }
            this.t = z;
        }
    }

    private void a(int i2) {
        c();
        if (this.u == null) {
            this.u = l.a();
            this.u.a(this.v);
            this.u.a(i2 > this.s ? a.f4484c : a.f4485d);
            this.u.a(new f.a() {
                public void a(f fVar) {
                    CollapsingToolbarLayout.this.setScrimAlpha(fVar.c());
                }
            });
        } else if (this.u.b()) {
            this.u.d();
        }
        this.u.a(this.s, i2);
        this.u.a();
    }

    /* access modifiers changed from: package-private */
    public void setScrimAlpha(int i2) {
        if (i2 != this.s) {
            if (!(this.r == null || this.f4476h == null)) {
                ag.c(this.f4476h);
            }
            this.s = i2;
            ag.c(this);
        }
    }

    public void setContentScrimColor(int i2) {
        setContentScrim(new ColorDrawable(i2));
    }

    public void setContentScrimResource(int i2) {
        setContentScrim(c.a(getContext(), i2));
    }

    public Drawable getContentScrim() {
        return this.r;
    }

    public void setContentScrim(Drawable drawable) {
        Drawable drawable2 = null;
        if (this.r != drawable) {
            if (this.r != null) {
                this.r.setCallback(null);
            }
            if (drawable != null) {
                drawable2 = drawable.mutate();
            }
            this.r = drawable2;
            if (this.r != null) {
                this.r.setBounds(0, 0, getWidth(), getHeight());
                this.r.setCallback(this);
                this.r.setAlpha(this.s);
            }
            ag.c(this);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        boolean z = false;
        Drawable drawable = this.f4470b;
        if (drawable != null && drawable.isStateful()) {
            z = false | drawable.setState(drawableState);
        }
        Drawable drawable2 = this.r;
        if (drawable2 != null && drawable2.isStateful()) {
            z |= drawable2.setState(drawableState);
        }
        if (this.f4469a != null) {
            z |= this.f4469a.a(drawableState);
        }
        if (z) {
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.r || drawable == this.f4470b;
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
        boolean z = i2 == 0;
        if (!(this.f4470b == null || this.f4470b.isVisible() == z)) {
            this.f4470b.setVisible(z, false);
        }
        if (this.r != null && this.r.isVisible() != z) {
            this.r.setVisible(z, false);
        }
    }

    public void setStatusBarScrimColor(int i2) {
        setStatusBarScrim(new ColorDrawable(i2));
    }

    public void setStatusBarScrimResource(int i2) {
        setStatusBarScrim(c.a(getContext(), i2));
    }

    public Drawable getStatusBarScrim() {
        return this.f4470b;
    }

    public void setStatusBarScrim(Drawable drawable) {
        Drawable drawable2 = null;
        if (this.f4470b != drawable) {
            if (this.f4470b != null) {
                this.f4470b.setCallback(null);
            }
            if (drawable != null) {
                drawable2 = drawable.mutate();
            }
            this.f4470b = drawable2;
            if (this.f4470b != null) {
                if (this.f4470b.isStateful()) {
                    this.f4470b.setState(getDrawableState());
                }
                android.support.v4.b.a.a.b(this.f4470b, ag.g(this));
                this.f4470b.setVisible(getVisibility() == 0, false);
                this.f4470b.setCallback(this);
                this.f4470b.setAlpha(this.s);
            }
            ag.c(this);
        }
    }

    public void setCollapsedTitleTextAppearance(int i2) {
        this.f4469a.c(i2);
    }

    public void setCollapsedTitleTextColor(int i2) {
        setCollapsedTitleTextColor(ColorStateList.valueOf(i2));
    }

    public void setCollapsedTitleTextColor(ColorStateList colorStateList) {
        this.f4469a.b(colorStateList);
    }

    public int getCollapsedTitleGravity() {
        return this.f4469a.c();
    }

    public void setCollapsedTitleGravity(int i2) {
        this.f4469a.b(i2);
    }

    public void setExpandedTitleTextAppearance(int i2) {
        this.f4469a.d(i2);
    }

    public void setExpandedTitleColor(int i2) {
        setExpandedTitleTextColor(ColorStateList.valueOf(i2));
    }

    public void setExpandedTitleTextColor(ColorStateList colorStateList) {
        this.f4469a.a(colorStateList);
    }

    public int getExpandedTitleGravity() {
        return this.f4469a.b();
    }

    public void setExpandedTitleGravity(int i2) {
        this.f4469a.a(i2);
    }

    public Typeface getCollapsedTitleTypeface() {
        return this.f4469a.d();
    }

    public void setCollapsedTitleTypeface(Typeface typeface) {
        this.f4469a.a(typeface);
    }

    public Typeface getExpandedTitleTypeface() {
        return this.f4469a.e();
    }

    public void setExpandedTitleTypeface(Typeface typeface) {
        this.f4469a.b(typeface);
    }

    public int getExpandedTitleMarginStart() {
        return this.l;
    }

    public void setExpandedTitleMarginStart(int i2) {
        this.l = i2;
        requestLayout();
    }

    public int getExpandedTitleMarginTop() {
        return this.m;
    }

    public void setExpandedTitleMarginTop(int i2) {
        this.m = i2;
        requestLayout();
    }

    public int getExpandedTitleMarginEnd() {
        return this.n;
    }

    public void setExpandedTitleMarginEnd(int i2) {
        this.n = i2;
        requestLayout();
    }

    public int getExpandedTitleMarginBottom() {
        return this.o;
    }

    public void setExpandedTitleMarginBottom(int i2) {
        this.o = i2;
        requestLayout();
    }

    public int getScrimVisibleHeightTrigger() {
        if (this.w >= 0) {
            return this.w;
        }
        int b2 = this.f4472d != null ? this.f4472d.b() : 0;
        int q2 = ag.q(this);
        if (q2 > 0) {
            return Math.min(b2 + (q2 * 2), getHeight());
        }
        return getHeight() / 3;
    }

    public void setScrimVisibleHeightTrigger(int i2) {
        if (this.w != i2) {
            this.w = i2;
            b();
        }
    }

    public long getScrimAnimationDuration() {
        return this.v;
    }

    public void setScrimAnimationDuration(long j2) {
        this.v = j2;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }

    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public FrameLayout.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.r != null || this.f4470b != null) {
            setScrimsShown(getHeight() + this.f4471c < getScrimVisibleHeightTrigger());
        }
    }

    /* access modifiers changed from: package-private */
    public final int b(View view) {
        return ((getHeight() - a(view).b()) - view.getHeight()) - ((LayoutParams) view.getLayoutParams()).bottomMargin;
    }

    public static class LayoutParams extends FrameLayout.LayoutParams {

        /* renamed from: a  reason: collision with root package name */
        int f4479a = 0;

        /* renamed from: b  reason: collision with root package name */
        float f4480b = 0.5f;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d.C0085d.CollapsingToolbarLayout_Layout);
            this.f4479a = obtainStyledAttributes.getInt(d.C0085d.CollapsingToolbarLayout_Layout_layout_collapseMode, 0);
            a(obtainStyledAttributes.getFloat(d.C0085d.CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier, 0.5f));
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public void a(float f2) {
            this.f4480b = f2;
        }
    }

    private class a implements AppBarLayout.OnOffsetChangedListener {
        a() {
        }

        public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
            int i2;
            CollapsingToolbarLayout.this.f4471c = i;
            if (CollapsingToolbarLayout.this.f4472d != null) {
                i2 = CollapsingToolbarLayout.this.f4472d.b();
            } else {
                i2 = 0;
            }
            int childCount = CollapsingToolbarLayout.this.getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = CollapsingToolbarLayout.this.getChildAt(i3);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                k a2 = CollapsingToolbarLayout.a(childAt);
                switch (layoutParams.f4479a) {
                    case 1:
                        a2.a(c.a(-i, 0, CollapsingToolbarLayout.this.b(childAt)));
                        break;
                    case 2:
                        a2.a(Math.round(layoutParams.f4480b * ((float) (-i))));
                        break;
                }
            }
            CollapsingToolbarLayout.this.b();
            if (CollapsingToolbarLayout.this.f4470b != null && i2 > 0) {
                ag.c(CollapsingToolbarLayout.this);
            }
            CollapsingToolbarLayout.this.f4469a.a(((float) Math.abs(i)) / ((float) ((CollapsingToolbarLayout.this.getHeight() - ag.q(CollapsingToolbarLayout.this)) - i2)));
        }
    }
}
