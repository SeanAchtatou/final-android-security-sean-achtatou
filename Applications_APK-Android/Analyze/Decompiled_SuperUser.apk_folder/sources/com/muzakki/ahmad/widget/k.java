package com.muzakki.ahmad.widget;

import android.support.v4.view.ag;
import android.view.View;

/* compiled from: ViewOffsetHelper */
class k {

    /* renamed from: a  reason: collision with root package name */
    private final View f4514a;

    /* renamed from: b  reason: collision with root package name */
    private int f4515b;

    /* renamed from: c  reason: collision with root package name */
    private int f4516c;

    /* renamed from: d  reason: collision with root package name */
    private int f4517d;

    /* renamed from: e  reason: collision with root package name */
    private int f4518e;

    public k(View view) {
        this.f4514a = view;
    }

    public void a() {
        this.f4515b = this.f4514a.getTop();
        this.f4516c = this.f4514a.getLeft();
        c();
    }

    private void c() {
        ag.e(this.f4514a, this.f4517d - (this.f4514a.getTop() - this.f4515b));
        ag.f(this.f4514a, this.f4518e - (this.f4514a.getLeft() - this.f4516c));
    }

    public boolean a(int i) {
        if (this.f4517d == i) {
            return false;
        }
        this.f4517d = i;
        c();
        return true;
    }

    public int b() {
        return this.f4515b;
    }
}
