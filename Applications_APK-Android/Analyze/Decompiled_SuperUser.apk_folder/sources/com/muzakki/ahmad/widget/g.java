package com.muzakki.ahmad.widget;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import com.muzakki.ahmad.widget.f;
import java.util.ArrayList;

/* compiled from: ValueAnimatorCompatImplGingerbread */
class g extends f.c {

    /* renamed from: a  reason: collision with root package name */
    private static final Handler f4499a = new Handler(Looper.getMainLooper());

    /* renamed from: b  reason: collision with root package name */
    private final int[] f4500b = new int[2];

    /* renamed from: c  reason: collision with root package name */
    private final float[] f4501c = new float[2];

    /* renamed from: d  reason: collision with root package name */
    private long f4502d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f4503e;

    /* renamed from: f  reason: collision with root package name */
    private float f4504f;

    /* renamed from: g  reason: collision with root package name */
    private long f4505g = 200;

    /* renamed from: h  reason: collision with root package name */
    private Interpolator f4506h;
    private ArrayList<f.c.a> i;
    private ArrayList<f.c.b> j;
    private final Runnable k = new Runnable() {
        public void run() {
            g.this.g();
        }
    };

    g() {
    }

    public void a() {
        if (!this.f4503e) {
            if (this.f4506h == null) {
                this.f4506h = new AccelerateDecelerateInterpolator();
            }
            this.f4503e = true;
            this.f4504f = 0.0f;
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        this.f4502d = SystemClock.uptimeMillis();
        h();
        i();
        f4499a.postDelayed(this.k, 10);
    }

    public boolean b() {
        return this.f4503e;
    }

    public void a(Interpolator interpolator) {
        this.f4506h = interpolator;
    }

    public void a(f.c.b bVar) {
        if (this.j == null) {
            this.j = new ArrayList<>();
        }
        this.j.add(bVar);
    }

    public void a(int i2, int i3) {
        this.f4500b[0] = i2;
        this.f4500b[1] = i3;
    }

    public int c() {
        return a.a(this.f4500b[0], this.f4500b[1], f());
    }

    public void d() {
        this.f4503e = false;
        f4499a.removeCallbacks(this.k);
        j();
        k();
    }

    public float f() {
        return this.f4504f;
    }

    public void a(long j2) {
        this.f4505g = j2;
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        if (this.f4503e) {
            float a2 = c.a(((float) (SystemClock.uptimeMillis() - this.f4502d)) / ((float) this.f4505g), 0.0f, 1.0f);
            if (this.f4506h != null) {
                a2 = this.f4506h.getInterpolation(a2);
            }
            this.f4504f = a2;
            h();
            if (SystemClock.uptimeMillis() >= this.f4502d + this.f4505g) {
                this.f4503e = false;
                k();
            }
        }
        if (this.f4503e) {
            f4499a.postDelayed(this.k, 10);
        }
    }

    private void h() {
        if (this.j != null) {
            int size = this.j.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.j.get(i2).a();
            }
        }
    }

    private void i() {
        if (this.i != null) {
            int size = this.i.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.i.get(i2).a();
            }
        }
    }

    private void j() {
        if (this.i != null) {
            int size = this.i.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.i.get(i2).c();
            }
        }
    }

    private void k() {
        if (this.i != null) {
            int size = this.i.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.i.get(i2).b();
            }
        }
    }
}
