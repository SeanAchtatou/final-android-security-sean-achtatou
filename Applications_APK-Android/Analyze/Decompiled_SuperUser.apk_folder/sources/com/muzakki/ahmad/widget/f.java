package com.muzakki.ahmad.widget;

import android.view.animation.Interpolator;

/* compiled from: ValueAnimatorCompat */
class f {

    /* renamed from: a  reason: collision with root package name */
    private final c f4496a;

    /* compiled from: ValueAnimatorCompat */
    interface a {
        void a(f fVar);
    }

    /* compiled from: ValueAnimatorCompat */
    interface b {
        f a();
    }

    f(c cVar) {
        this.f4496a = cVar;
    }

    public void a() {
        this.f4496a.a();
    }

    public boolean b() {
        return this.f4496a.b();
    }

    public void a(Interpolator interpolator) {
        this.f4496a.a(interpolator);
    }

    public void a(final a aVar) {
        if (aVar != null) {
            this.f4496a.a(new c.b() {
                public void a() {
                    aVar.a(f.this);
                }
            });
        } else {
            this.f4496a.a((c.b) null);
        }
    }

    public void a(int i, int i2) {
        this.f4496a.a(i, i2);
    }

    public int c() {
        return this.f4496a.c();
    }

    public void d() {
        this.f4496a.d();
    }

    public void a(long j) {
        this.f4496a.a(j);
    }

    /* compiled from: ValueAnimatorCompat */
    static abstract class c {

        /* compiled from: ValueAnimatorCompat */
        interface a {
            void a();

            void b();

            void c();
        }

        /* compiled from: ValueAnimatorCompat */
        interface b {
            void a();
        }

        /* access modifiers changed from: package-private */
        public abstract void a();

        /* access modifiers changed from: package-private */
        public abstract void a(int i, int i2);

        /* access modifiers changed from: package-private */
        public abstract void a(long j);

        /* access modifiers changed from: package-private */
        public abstract void a(Interpolator interpolator);

        /* access modifiers changed from: package-private */
        public abstract void a(b bVar);

        /* access modifiers changed from: package-private */
        public abstract boolean b();

        /* access modifiers changed from: package-private */
        public abstract int c();

        /* access modifiers changed from: package-private */
        public abstract void d();

        c() {
        }
    }
}
