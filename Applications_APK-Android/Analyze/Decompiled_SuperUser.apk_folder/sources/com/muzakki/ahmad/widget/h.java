package com.muzakki.ahmad.widget;

import android.animation.ValueAnimator;
import android.view.animation.Interpolator;
import com.muzakki.ahmad.widget.f;

/* compiled from: ValueAnimatorCompatImplHoneycombMr1 */
class h extends f.c {

    /* renamed from: a  reason: collision with root package name */
    private final ValueAnimator f4508a = new ValueAnimator();

    h() {
    }

    public void a() {
        this.f4508a.start();
    }

    public boolean b() {
        return this.f4508a.isRunning();
    }

    public void a(Interpolator interpolator) {
        this.f4508a.setInterpolator(interpolator);
    }

    public void a(final f.c.b bVar) {
        this.f4508a.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                bVar.a();
            }
        });
    }

    public void a(int i, int i2) {
        this.f4508a.setIntValues(i, i2);
    }

    public int c() {
        return ((Integer) this.f4508a.getAnimatedValue()).intValue();
    }

    public void d() {
        this.f4508a.cancel();
    }

    public void a(long j) {
        this.f4508a.setDuration(j);
    }
}
