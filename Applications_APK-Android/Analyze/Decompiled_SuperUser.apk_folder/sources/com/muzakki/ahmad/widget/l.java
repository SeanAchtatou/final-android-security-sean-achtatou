package com.muzakki.ahmad.widget;

import android.os.Build;
import com.muzakki.ahmad.widget.f;

/* compiled from: ViewUtils */
class l {

    /* renamed from: a  reason: collision with root package name */
    static final f.b f4519a = new f.b() {
        public f a() {
            return new f(Build.VERSION.SDK_INT >= 12 ? new h() : new g());
        }
    };

    static f a() {
        return f4519a.a();
    }

    static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }
}
