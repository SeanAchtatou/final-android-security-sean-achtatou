package com.muzakki.ahmad.widget;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.c.f;
import android.support.v4.view.ag;
import android.support.v7.a.a;
import android.support.v7.widget.an;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Interpolator;
import com.muzakki.ahmad.widget.d;

/* compiled from: CollapsingTextHelper */
final class b {

    /* renamed from: a  reason: collision with root package name */
    private static final boolean f4487a = (Build.VERSION.SDK_INT < 18);

    /* renamed from: b  reason: collision with root package name */
    private static final Paint f4488b = null;
    private boolean A;
    private boolean B;
    private Bitmap C;
    private Paint D;
    private float E;
    private float F;
    private float G;
    private float H;
    private int[] I;
    private boolean J;
    private Interpolator K;
    private Interpolator L;
    private float M;
    private float N;
    private float O;
    private int P;
    private float Q;
    private float R;
    private float S;
    private int T;
    private CharSequence U = "Testing";
    private CharSequence V = "";
    private CharSequence W = "unit";
    private CharSequence X = "";
    private CharSequence Y = "message";
    private float Z;
    private float aa = 50.0f;
    private int ab;
    private int ac;
    private TextPaint ad;
    private TextPaint ae;
    private TextPaint af;
    private float ag;
    private float ah = 25.0f;
    private float ai;
    private float aj;
    private float ak;

    /* renamed from: c  reason: collision with root package name */
    private final View f4489c;

    /* renamed from: d  reason: collision with root package name */
    private final Rect f4490d;

    /* renamed from: e  reason: collision with root package name */
    private final Rect f4491e;

    /* renamed from: f  reason: collision with root package name */
    private final RectF f4492f;

    /* renamed from: g  reason: collision with root package name */
    private final TextPaint f4493g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f4494h;
    private float i;
    private int j = 16;
    private int k = 16;
    private float l = 15.0f;
    private float m = 15.0f;
    private ColorStateList n;
    private ColorStateList o;
    private float p;
    private float q;
    private float r;
    private float s;
    private float t;
    private float u;
    private Typeface v;
    private Typeface w;
    private Typeface x;
    private CharSequence y;
    private CharSequence z;

    static {
        if (f4488b != null) {
            f4488b.setAntiAlias(true);
            f4488b.setColor(-65281);
        }
    }

    public b(View view) {
        this.f4489c = view;
        this.f4493g = new TextPaint(129);
        this.ad = new TextPaint(129);
        this.ae = new TextPaint(129);
        this.af = new TextPaint(129);
        this.f4491e = new Rect();
        this.f4490d = new Rect();
        this.f4492f = new RectF();
    }

    private static boolean a(float f2, float f3) {
        return Math.abs(f2 - f3) < 0.001f;
    }

    private static int a(int i2, int i3, float f2) {
        float f3 = 1.0f - f2;
        return Color.argb((int) ((((float) Color.alpha(i2)) * f3) + (((float) Color.alpha(i3)) * f2)), (int) ((((float) Color.red(i2)) * f3) + (((float) Color.red(i3)) * f2)), (int) ((((float) Color.green(i2)) * f3) + (((float) Color.green(i3)) * f2)), (int) ((f3 * ((float) Color.blue(i2))) + (((float) Color.blue(i3)) * f2)));
    }

    private static float a(float f2, float f3, float f4, Interpolator interpolator) {
        if (interpolator != null) {
            f4 = interpolator.getInterpolation(f4);
        }
        return a.a(f2, f3, f4);
    }

    private static boolean a(Rect rect, int i2, int i3, int i4, int i5) {
        return rect.left == i2 && rect.top == i3 && rect.right == i4 && rect.bottom == i5;
    }

    /* access modifiers changed from: package-private */
    public void a(Interpolator interpolator) {
        this.L = interpolator;
        g();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4, int i5) {
        if (!a(this.f4490d, i2, i3, i4, i5)) {
            this.f4490d.set(i2, i3, i4, i5);
            this.J = true;
            a();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3, int i4, int i5) {
        if (!a(this.f4491e, i2, i3, i4, i5)) {
            this.f4491e.set(i2, i3, i4, i5);
            this.J = true;
            a();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f4494h = this.f4491e.width() > 0 && this.f4491e.height() > 0 && this.f4490d.width() > 0 && this.f4490d.height() > 0;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (this.j != i2) {
            this.j = i2;
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        if (this.k != i2) {
            this.k = i2;
            g();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.an.a(int, float):float
     arg types: [int, int]
     candidates:
      android.support.v7.widget.an.a(int, int):int
      android.support.v7.widget.an.a(int, boolean):boolean
      android.support.v7.widget.an.a(int, float):float */
    /* access modifiers changed from: package-private */
    public void c(int i2) {
        an a2 = an.a(this.f4489c.getContext(), i2, a.k.TextAppearance);
        if (a2.g(a.k.TextAppearance_android_textColor)) {
            this.o = a2.e(a.k.TextAppearance_android_textColor);
        }
        if (a2.g(a.k.TextAppearance_android_textSize)) {
            this.m = (float) a2.e(a.k.TextAppearance_android_textSize, (int) this.m);
        }
        this.P = a2.a(a.k.TextAppearance_android_shadowColor, 0);
        this.N = a2.a(a.k.TextAppearance_android_shadowDx, 0.0f);
        this.O = a2.a(a.k.TextAppearance_android_shadowDy, 0.0f);
        this.M = a2.a(a.k.TextAppearance_android_shadowRadius, 0.0f);
        a2.a();
        if (Build.VERSION.SDK_INT >= 16) {
            this.v = g(i2);
        }
        g();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.an.a(int, float):float
     arg types: [int, int]
     candidates:
      android.support.v7.widget.an.a(int, int):int
      android.support.v7.widget.an.a(int, boolean):boolean
      android.support.v7.widget.an.a(int, float):float */
    /* access modifiers changed from: package-private */
    public void d(int i2) {
        an a2 = an.a(this.f4489c.getContext(), i2, a.k.TextAppearance);
        if (a2.g(a.k.TextAppearance_android_textColor)) {
            this.n = a2.e(a.k.TextAppearance_android_textColor);
        }
        if (a2.g(a.k.TextAppearance_android_textSize)) {
            this.l = (float) a2.e(a.k.TextAppearance_android_textSize, (int) this.l);
        }
        this.T = a2.a(a.k.TextAppearance_android_shadowColor, 0);
        this.R = a2.a(a.k.TextAppearance_android_shadowDx, 0.0f);
        this.S = a2.a(a.k.TextAppearance_android_shadowDy, 0.0f);
        this.Q = a2.a(a.k.TextAppearance_android_shadowRadius, 0.0f);
        a2.a();
        if (Build.VERSION.SDK_INT >= 16) {
            this.w = g(i2);
        }
        g();
    }

    /* access modifiers changed from: package-private */
    public void e(int i2) {
        TypedArray obtainStyledAttributes = this.f4489c.getContext().obtainStyledAttributes(i2, d.C0085d.TextAppearance);
        if (obtainStyledAttributes.hasValue(d.C0085d.TextAppearance_android_textColor)) {
            this.ab = obtainStyledAttributes.getColor(d.C0085d.TextAppearance_android_textColor, this.ab);
        }
        if (obtainStyledAttributes.hasValue(d.C0085d.TextAppearance_android_textSize)) {
            this.ah = (float) obtainStyledAttributes.getDimensionPixelSize(d.C0085d.TextAppearance_android_textSize, (int) this.ah);
        }
    }

    /* access modifiers changed from: package-private */
    public void f(int i2) {
        an a2 = an.a(this.f4489c.getContext(), i2, a.k.TextAppearance);
        if (a2.g(a.k.TextAppearance_android_textColor)) {
            this.ac = a2.b(a.k.TextAppearance_android_textColor, this.ab);
        }
        if (a2.g(a.k.TextAppearance_android_textSize)) {
            this.aa = (float) a2.e(a.k.TextAppearance_android_textSize, (int) this.aa);
        }
    }

    private Typeface g(int i2) {
        TypedArray obtainStyledAttributes = this.f4489c.getContext().obtainStyledAttributes(i2, new int[]{16843692});
        try {
            String string = obtainStyledAttributes.getString(0);
            if (string != null) {
                return Typeface.create(string, 0);
            }
            obtainStyledAttributes.recycle();
            return null;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: package-private */
    public Typeface d() {
        return this.v != null ? this.v : Typeface.DEFAULT;
    }

    /* access modifiers changed from: package-private */
    public void a(Typeface typeface) {
        if (this.v != typeface) {
            this.v = typeface;
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public Typeface e() {
        return this.w != null ? this.w : Typeface.DEFAULT;
    }

    /* access modifiers changed from: package-private */
    public void b(Typeface typeface) {
        if (this.w != typeface) {
            this.w = typeface;
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int[] iArr) {
        this.I = iArr;
        if (!f()) {
            return false;
        }
        g();
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return (this.o != null && this.o.isStateful()) || (this.n != null && this.n.isStateful());
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        float a2 = c.a(f2, 0.0f, 1.0f);
        if (a2 != this.i) {
            this.i = a2;
            i();
        }
    }

    private void i() {
        c(this.i);
    }

    private void c(float f2) {
        d(f2);
        this.t = a(this.r, this.s, f2, this.K);
        this.u = a(this.p, this.q, f2, this.K);
        this.ak = a(this.aj, this.ai, f2, this.K);
        e(a(this.l, this.m, f2, this.L));
        f(a(this.aa, this.ah, f2, this.L));
        if (this.o != this.n) {
            this.f4493g.setColor(a(j(), k(), f2));
        } else {
            this.f4493g.setColor(k());
        }
        if (this.ab != this.ac) {
            this.ad.setColor(a(this.ac, this.ab, f2));
            this.ae.setColor(a(this.ac, this.ab, f2));
            this.af.setColor(a(this.ac, this.ab, f2));
        } else {
            this.ad.setColor(this.ab);
            this.ae.setColor(this.ab);
            this.af.setColor(this.ab);
        }
        this.f4493g.setShadowLayer(a(this.Q, this.M, f2, (Interpolator) null), a(this.R, this.N, f2, (Interpolator) null), a(this.S, this.O, f2, (Interpolator) null), a(this.T, this.P, f2));
        ag.c(this.f4489c);
    }

    private int j() {
        if (this.I != null) {
            return this.n.getColorForState(this.I, 0);
        }
        return this.n.getDefaultColor();
    }

    private int k() {
        if (this.I != null) {
            return this.o.getColorForState(this.I, 0);
        }
        return this.o.getDefaultColor();
    }

    private void l() {
        float f2 = this.H;
        g(this.m);
        h(this.ah);
        float descent = this.f4493g.descent() - this.f4493g.ascent();
        float f3 = descent / 2.0f;
        if (this.U != null) {
            float descent2 = this.ad.descent() - this.ad.ascent();
            float descent3 = (descent2 / 2.0f) - this.ad.descent();
            float height = (((float) this.f4491e.height()) - (descent2 + descent)) / 3.0f;
            this.q = (((float) this.f4491e.top) + height) - this.f4493g.ascent();
            this.ai = (descent + ((height * 2.0f) + ((float) this.f4491e.top))) - this.ad.ascent();
        } else {
            this.q = ((float) this.f4491e.centerY()) + f3;
        }
        this.s = (float) this.f4491e.left;
        g(this.l);
        h(this.aa);
        float descent4 = (this.f4493g.descent() - this.f4493g.ascent()) / 2.0f;
        float descent5 = (this.ad.descent() - this.ad.ascent()) / 2.0f;
        if (this.U != null) {
            this.p = ((float) this.f4490d.centerY()) + descent5;
            this.aj = (descent5 + this.p) - this.ad.ascent();
        } else {
            this.p = descent5 + ((float) this.f4490d.centerY());
        }
        this.r = (float) this.f4490d.left;
        n();
        e(f2);
    }

    private void d(float f2) {
        this.f4492f.left = a((float) this.f4490d.left, (float) this.f4491e.left, f2, this.K);
        this.f4492f.top = a(this.p, this.q, f2, this.K);
        this.f4492f.right = a((float) this.f4490d.right, (float) this.f4491e.right, f2, this.K);
        this.f4492f.bottom = a((float) this.f4490d.bottom, (float) this.f4491e.bottom, f2, this.K);
    }

    public void a(Canvas canvas) {
        float ascent;
        float f2;
        int save = canvas.save();
        if (this.z != null && this.f4494h) {
            float f3 = this.t;
            float f4 = this.u;
            float f5 = this.ak;
            boolean z2 = this.B && this.C != null;
            if (z2) {
                ascent = this.E * this.G;
                float f6 = this.F * this.G;
            } else {
                ascent = this.f4493g.ascent() * this.G;
                float descent = this.f4493g.descent() * this.G;
            }
            if (z2) {
                f2 = f4 + ascent;
            } else {
                f2 = f4;
            }
            int save2 = canvas.save();
            if (this.U != null) {
                this.ad.setTextSize(this.Z * this.ag);
                canvas.drawText(this.U, 0, this.U.length(), f3, f5, this.ad);
                float measureText = f3 + this.ad.measureText(this.U.toString());
                canvas.drawText(this.W, 0, this.W.length(), measureText, f5, this.af);
                float measureText2 = measureText + this.af.measureText(this.W.toString());
                if (this.V.length() > 0) {
                    canvas.drawText(this.V, 0, this.V.length(), measureText2, f5, this.ad);
                    float measureText3 = measureText2 + this.ad.measureText(this.V.toString());
                    canvas.drawText(this.X, 0, this.X.length(), measureText3, f5, this.af);
                    measureText2 = measureText3 + this.af.measureText(this.X.toString());
                }
                try {
                    canvas.drawText(TextUtils.ellipsize(this.Y, this.ae, (float) ((int) (((float) canvas.getClipBounds().width()) - measureText2)), TextUtils.TruncateAt.END), 0, this.Y.length(), measureText2, f5, this.ae);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                canvas.restoreToCount(save2);
            }
            if (this.G != 1.0f) {
                canvas.scale(this.G, this.G, f3, f2);
            }
            if (z2) {
                canvas.drawBitmap(this.C, f3, f2, this.D);
            } else {
                canvas.drawText(this.z, 0, this.z.length(), f3, f2, this.f4493g);
            }
        }
        canvas.restoreToCount(save);
    }

    private boolean g(CharSequence charSequence) {
        boolean z2 = true;
        if (ag.g(this.f4489c) != 1) {
            z2 = false;
        }
        return (z2 ? f.f596d : f.f595c).a(charSequence, 0, charSequence.length());
    }

    private void e(float f2) {
        g(f2);
        this.B = f4487a && this.G != 1.0f;
        if (this.B) {
            m();
        }
        ag.c(this.f4489c);
    }

    private void f(float f2) {
        h(f2);
        ag.c(this.f4489c);
    }

    private void g(float f2) {
        float f3;
        boolean z2;
        boolean z3;
        boolean z4 = true;
        if (this.y != null) {
            float width = (float) this.f4491e.width();
            float width2 = (float) this.f4490d.width();
            if (a(f2, this.m)) {
                f3 = this.m;
                this.G = 1.0f;
                if (this.x != this.v) {
                    this.x = this.v;
                    z2 = true;
                } else {
                    z2 = false;
                }
            } else {
                f3 = this.l;
                if (this.x != this.w) {
                    this.x = this.w;
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (a(f2, this.l)) {
                    this.G = 1.0f;
                } else {
                    this.G = f2 / this.l;
                }
                float f4 = this.m / this.l;
                width = width2 * f4 > width ? Math.min(width / f4, width2) : width2;
            }
            if (width > 0.0f) {
                if (this.H != f3 || this.J || z3) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                this.H = f3;
                this.J = false;
            }
            if (this.z == null || z3) {
                this.f4493g.setTextSize(this.H);
                this.f4493g.setTypeface(this.x);
                TextPaint textPaint = this.f4493g;
                if (this.G == 1.0f) {
                    z4 = false;
                }
                textPaint.setLinearText(z4);
                CharSequence ellipsize = TextUtils.ellipsize(this.y, this.f4493g, width, TextUtils.TruncateAt.END);
                if (!TextUtils.equals(ellipsize, this.z)) {
                    this.z = ellipsize;
                    this.A = g(this.z);
                }
            }
        }
    }

    private void h(float f2) {
        float f3;
        float f4;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = true;
        if (this.U != null) {
            float width = (float) this.f4491e.width();
            float width2 = (float) this.f4490d.width();
            if (a(f2, this.ah)) {
                float f5 = this.ah;
                this.Z = 1.0f;
                float f6 = f5;
                f4 = width;
                f3 = f6;
            } else {
                float f7 = this.aa;
                if (a(f2, this.aa)) {
                    this.Z = 1.0f;
                } else {
                    this.Z = f2 / this.aa;
                }
                float f8 = this.ah / this.aa;
                if (width2 * f8 > width) {
                    float f9 = f7;
                    f4 = Math.min(width / f8, width2);
                    f3 = f9;
                } else {
                    f3 = f7;
                    f4 = width2;
                }
            }
            if (f4 > 0.0f) {
                if (this.ag != f3 || this.J) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                this.ag = f3;
                this.J = false;
            } else {
                z2 = false;
            }
            if (z2) {
                this.ad.setTextSize(this.ag);
                this.x = Typeface.createFromAsset(this.f4489c.getContext().getAssets(), "fonts/booster_number_font.otf");
                this.ad.setTypeface(this.x);
                TextPaint textPaint = this.ad;
                if (this.Z != 1.0f) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                textPaint.setLinearText(z3);
                this.af.setTextSize(this.f4489c.getContext().getResources().getDimension(d.a.v));
                this.ae.setTextSize(this.f4489c.getContext().getResources().getDimension(d.a.u));
                this.af.setTypeface(this.x);
                TextPaint textPaint2 = this.ae;
                if (this.Z != 1.0f) {
                    z4 = true;
                } else {
                    z4 = false;
                }
                textPaint2.setLinearText(z4);
                TextPaint textPaint3 = this.af;
                if (this.Z == 1.0f) {
                    z5 = false;
                }
                textPaint3.setLinearText(z5);
            }
        }
    }

    private void m() {
        if (this.C == null && !this.f4490d.isEmpty() && !TextUtils.isEmpty(this.z)) {
            c(0.0f);
            this.E = this.f4493g.ascent();
            this.F = this.f4493g.descent();
            int round = Math.round(this.f4493g.measureText(this.z, 0, this.z.length()));
            int round2 = Math.round(this.F - this.E);
            if (round > 0 && round2 > 0) {
                this.C = Bitmap.createBitmap(round, round2, Bitmap.Config.ARGB_8888);
                new Canvas(this.C).drawText(this.z, 0, this.z.length(), 0.0f, ((float) round2) - this.f4493g.descent(), this.f4493g);
                if (this.D == null) {
                    this.D = new Paint(3);
                }
            }
        }
    }

    public void g() {
        if (this.f4489c.getHeight() > 0 && this.f4489c.getWidth() > 0) {
            l();
            i();
        }
    }

    /* access modifiers changed from: package-private */
    public CharSequence h() {
        return this.y;
    }

    /* access modifiers changed from: package-private */
    public void a(CharSequence charSequence) {
        if (charSequence == null || !charSequence.equals(this.y)) {
            this.y = charSequence;
            this.z = null;
            n();
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(CharSequence charSequence) {
        if (charSequence == null || !charSequence.equals(this.U)) {
            this.U = charSequence;
            n();
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public void c(CharSequence charSequence) {
        if (charSequence == null || !charSequence.equals(this.W)) {
            this.W = charSequence;
            n();
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public void d(CharSequence charSequence) {
        if (charSequence == null || !charSequence.equals(this.X)) {
            this.X = charSequence;
            n();
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(float f2) {
        this.af.setTextSize(f2);
        n();
        g();
    }

    /* access modifiers changed from: package-private */
    public void e(CharSequence charSequence) {
        if (charSequence == null || !charSequence.equals(this.V)) {
            this.V = charSequence;
            n();
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public void f(CharSequence charSequence) {
        if (charSequence == null || !charSequence.equals(this.Y)) {
            this.Y = charSequence;
            n();
            g();
        }
    }

    private void n() {
        if (this.C != null) {
            this.C.recycle();
            this.C = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        if (this.n != colorStateList) {
            this.n = colorStateList;
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(ColorStateList colorStateList) {
        if (this.o != colorStateList) {
            this.o = colorStateList;
            g();
        }
    }
}
