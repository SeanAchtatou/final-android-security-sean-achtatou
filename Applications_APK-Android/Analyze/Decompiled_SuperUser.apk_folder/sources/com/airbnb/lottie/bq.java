package com.airbnb.lottie;

import android.support.v4.util.h;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: PerformanceTracker */
public class bq {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2568a = false;

    /* renamed from: b  reason: collision with root package name */
    private final Set<a> f2569b = new android.support.v4.util.a();

    /* renamed from: c  reason: collision with root package name */
    private Map<String, bh> f2570c = new HashMap();

    /* renamed from: d  reason: collision with root package name */
    private final Comparator<h<String, Float>> f2571d = new Comparator<h<String, Float>>() {
        /* renamed from: a */
        public int compare(h<String, Float> hVar, h<String, Float> hVar2) {
            float floatValue = ((Float) hVar.f881b).floatValue();
            float floatValue2 = ((Float) hVar2.f881b).floatValue();
            if (floatValue2 > floatValue) {
                return 1;
            }
            if (floatValue > floatValue2) {
                return -1;
            }
            return 0;
        }
    };

    /* compiled from: PerformanceTracker */
    public interface a {
        void a(float f2);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f2568a = z;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, float f2) {
        if (this.f2568a) {
            bh bhVar = this.f2570c.get(str);
            if (bhVar == null) {
                bhVar = new bh();
                this.f2570c.put(str, bhVar);
            }
            bhVar.a(f2);
            if (str.equals("root")) {
                for (a a2 : this.f2569b) {
                    a2.a(f2);
                }
            }
        }
    }
}
