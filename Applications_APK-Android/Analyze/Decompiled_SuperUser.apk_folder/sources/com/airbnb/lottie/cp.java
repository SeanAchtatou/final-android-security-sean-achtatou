package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: TextLayer */
class cp extends q {

    /* renamed from: e  reason: collision with root package name */
    private final char[] f2646e = new char[1];

    /* renamed from: f  reason: collision with root package name */
    private final RectF f2647f = new RectF();

    /* renamed from: g  reason: collision with root package name */
    private final Matrix f2648g = new Matrix();

    /* renamed from: h  reason: collision with root package name */
    private final Paint f2649h = new Paint(1) {
        {
            setStyle(Paint.Style.FILL);
        }
    };
    private final Paint i = new Paint(1) {
        {
            setStyle(Paint.Style.STROKE);
        }
    };
    private final Map<al, List<z>> j = new HashMap();
    private final co k;
    private final be l;
    private final bd m;
    private bb<Integer> n;
    private bb<Integer> o;
    private bb<Float> p;
    private bb<Float> q;

    cp(be beVar, Layer layer) {
        super(beVar, layer);
        this.l = beVar;
        this.m = layer.a();
        this.k = layer.s().b();
        this.k.a(this);
        a(this.k);
        k t = layer.t();
        if (!(t == null || t.f2679a == null)) {
            this.n = t.f2679a.b();
            this.n.a(this);
            a(this.n);
        }
        if (!(t == null || t.f2680b == null)) {
            this.o = t.f2680b.b();
            this.o.a(this);
            a(this.o);
        }
        if (!(t == null || t.f2681c == null)) {
            this.p = t.f2681c.b();
            this.p.a(this);
            a(this.p);
        }
        if (t != null && t.f2682d != null) {
            this.q = t.f2682d.b();
            this.q.a(this);
            a(this.q);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Canvas canvas, Matrix matrix, int i2) {
        canvas.save();
        if (!this.l.l()) {
            canvas.setMatrix(matrix);
        }
        ac acVar = (ac) this.k.b();
        ai aiVar = this.m.k().get(acVar.f2426b);
        if (aiVar != null) {
            if (this.n != null) {
                this.f2649h.setColor(((Integer) this.n.b()).intValue());
            } else {
                this.f2649h.setColor(acVar.f2431g);
            }
            if (this.o != null) {
                this.i.setColor(((Integer) this.o.b()).intValue());
            } else {
                this.i.setColor(acVar.f2432h);
            }
            if (this.p != null) {
                this.i.setStrokeWidth(((Float) this.p.b()).floatValue());
            } else {
                this.i.setStrokeWidth(cs.a(matrix) * ((float) acVar.i) * this.m.n());
            }
            if (this.l.l()) {
                a(acVar, matrix, aiVar, canvas);
            } else {
                a(acVar, aiVar, matrix, canvas);
            }
            canvas.restore();
        }
    }

    private void a(ac acVar, Matrix matrix, ai aiVar, Canvas canvas) {
        float f2;
        float f3 = ((float) acVar.f2427c) / 100.0f;
        float a2 = cs.a(matrix);
        String str = acVar.f2425a;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < str.length()) {
                al a3 = this.m.j().a(al.a(str.charAt(i3), aiVar.a(), aiVar.c()));
                if (a3 != null) {
                    a(a3, matrix, f3, acVar, canvas);
                    float b2 = ((float) a3.b()) * f3 * this.m.n() * a2;
                    float f4 = ((float) acVar.f2429e) / 10.0f;
                    if (this.q != null) {
                        f2 = ((Float) this.q.b()).floatValue() + f4;
                    } else {
                        f2 = f4;
                    }
                    canvas.translate((f2 * a2) + b2, 0.0f);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void a(ac acVar, ai aiVar, Matrix matrix, Canvas canvas) {
        String str;
        float f2;
        float a2 = cs.a(matrix);
        Typeface a3 = this.l.a(aiVar.a(), aiVar.c());
        if (a3 != null) {
            String str2 = acVar.f2425a;
            cn k2 = this.l.k();
            if (k2 != null) {
                str = k2.b(str2);
            } else {
                str = str2;
            }
            this.f2649h.setTypeface(a3);
            this.f2649h.setTextSize(((float) acVar.f2427c) * this.m.n());
            this.i.setTypeface(this.f2649h.getTypeface());
            this.i.setTextSize(this.f2649h.getTextSize());
            for (int i2 = 0; i2 < str.length(); i2++) {
                char charAt = str.charAt(i2);
                a(charAt, acVar, canvas);
                this.f2646e[0] = charAt;
                float measureText = this.f2649h.measureText(this.f2646e, 0, 1);
                float f3 = ((float) acVar.f2429e) / 10.0f;
                if (this.q != null) {
                    f2 = ((Float) this.q.b()).floatValue() + f3;
                } else {
                    f2 = f3;
                }
                canvas.translate((f2 * a2) + measureText, 0.0f);
            }
        }
    }

    private void a(al alVar, Matrix matrix, float f2, ac acVar, Canvas canvas) {
        List<z> a2 = a(alVar);
        for (int i2 = 0; i2 < a2.size(); i2++) {
            Path d2 = a2.get(i2).d();
            d2.computeBounds(this.f2647f, false);
            this.f2648g.set(matrix);
            this.f2648g.preScale(f2, f2);
            d2.transform(this.f2648g);
            if (acVar.j) {
                a(d2, this.f2649h, canvas);
                a(d2, this.i, canvas);
            } else {
                a(d2, this.i, canvas);
                a(d2, this.f2649h, canvas);
            }
        }
    }

    private void a(Path path, Paint paint, Canvas canvas) {
        if (paint.getColor() != 0) {
            if (paint.getStyle() != Paint.Style.STROKE || paint.getStrokeWidth() != 0.0f) {
                canvas.drawPath(path, paint);
            }
        }
    }

    private void a(char c2, ac acVar, Canvas canvas) {
        this.f2646e[0] = c2;
        if (acVar.j) {
            a(this.f2646e, this.f2649h, canvas);
            a(this.f2646e, this.i, canvas);
            return;
        }
        a(this.f2646e, this.i, canvas);
        a(this.f2646e, this.f2649h, canvas);
    }

    private void a(char[] cArr, Paint paint, Canvas canvas) {
        canvas.drawText(cArr, 0, 1, 0.0f, 0.0f, paint);
    }

    private List<z> a(al alVar) {
        if (this.j.containsKey(alVar)) {
            return this.j.get(alVar);
        }
        List<ce> a2 = alVar.a();
        int size = a2.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i2 = 0; i2 < size; i2++) {
            arrayList.add(new z(this.l, this, a2.get(i2)));
        }
        this.j.put(alVar, arrayList);
        return arrayList;
    }
}
