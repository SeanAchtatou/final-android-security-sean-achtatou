package com.airbnb.lottie;

import java.util.List;

/* compiled from: GradientColorKeyframeAnimation */
class ao extends bb<an> {

    /* renamed from: b  reason: collision with root package name */
    private final an f2467b;

    ao(List<? extends ba<an>> list) {
        super(list);
        an anVar = (an) ((ba) list.get(0)).f2512a;
        int c2 = anVar == null ? 0 : anVar.c();
        this.f2467b = new an(new float[c2], new int[c2]);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public an a(ba<an> baVar, float f2) {
        this.f2467b.a((an) baVar.f2512a, (an) baVar.f2513b, f2);
        return this.f2467b;
    }
}
