package com.airbnb.lottie;

import com.airbnb.lottie.ShapeStroke;
import com.airbnb.lottie.b;
import com.airbnb.lottie.c;
import com.airbnb.lottie.d;
import com.airbnb.lottie.f;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: GradientStroke */
class ar implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2484a;

    /* renamed from: b  reason: collision with root package name */
    private final GradientType f2485b;

    /* renamed from: c  reason: collision with root package name */
    private final c f2486c;

    /* renamed from: d  reason: collision with root package name */
    private final d f2487d;

    /* renamed from: e  reason: collision with root package name */
    private final f f2488e;

    /* renamed from: f  reason: collision with root package name */
    private final f f2489f;

    /* renamed from: g  reason: collision with root package name */
    private final b f2490g;

    /* renamed from: h  reason: collision with root package name */
    private final ShapeStroke.LineCapType f2491h;
    private final ShapeStroke.LineJoinType i;
    private final List<b> j;
    private final b k;

    private ar(String str, GradientType gradientType, c cVar, d dVar, f fVar, f fVar2, b bVar, ShapeStroke.LineCapType lineCapType, ShapeStroke.LineJoinType lineJoinType, List<b> list, b bVar2) {
        this.f2484a = str;
        this.f2485b = gradientType;
        this.f2486c = cVar;
        this.f2487d = dVar;
        this.f2488e = fVar;
        this.f2489f = fVar2;
        this.f2490g = bVar;
        this.f2491h = lineCapType;
        this.i = lineJoinType;
        this.j = list;
        this.k = bVar2;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f2484a;
    }

    /* access modifiers changed from: package-private */
    public GradientType b() {
        return this.f2485b;
    }

    /* access modifiers changed from: package-private */
    public c c() {
        return this.f2486c;
    }

    /* access modifiers changed from: package-private */
    public d d() {
        return this.f2487d;
    }

    /* access modifiers changed from: package-private */
    public f e() {
        return this.f2488e;
    }

    /* access modifiers changed from: package-private */
    public f f() {
        return this.f2489f;
    }

    /* access modifiers changed from: package-private */
    public b g() {
        return this.f2490g;
    }

    /* access modifiers changed from: package-private */
    public ShapeStroke.LineCapType h() {
        return this.f2491h;
    }

    /* access modifiers changed from: package-private */
    public ShapeStroke.LineJoinType i() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public List<b> j() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public b k() {
        return this.k;
    }

    public y a(be beVar, q qVar) {
        return new as(beVar, qVar, this);
    }

    /* compiled from: GradientStroke */
    static class a {
        static ar a(JSONObject jSONObject, bd bdVar) {
            String optString = jSONObject.optString("nm");
            JSONObject optJSONObject = jSONObject.optJSONObject("g");
            if (optJSONObject != null && optJSONObject.has("k")) {
                optJSONObject = optJSONObject.optJSONObject("k");
            }
            c cVar = null;
            if (optJSONObject != null) {
                cVar = c.a.a(optJSONObject, bdVar);
            }
            JSONObject optJSONObject2 = jSONObject.optJSONObject("o");
            d dVar = null;
            if (optJSONObject2 != null) {
                dVar = d.a.a(optJSONObject2, bdVar);
            }
            GradientType gradientType = jSONObject.optInt("t", 1) == 1 ? GradientType.Linear : GradientType.Radial;
            JSONObject optJSONObject3 = jSONObject.optJSONObject("s");
            f fVar = null;
            if (optJSONObject3 != null) {
                fVar = f.a.a(optJSONObject3, bdVar);
            }
            JSONObject optJSONObject4 = jSONObject.optJSONObject("e");
            f fVar2 = null;
            if (optJSONObject4 != null) {
                fVar2 = f.a.a(optJSONObject4, bdVar);
            }
            b a2 = b.a.a(jSONObject.optJSONObject("w"), bdVar);
            ShapeStroke.LineCapType lineCapType = ShapeStroke.LineCapType.values()[jSONObject.optInt("lc") - 1];
            ShapeStroke.LineJoinType lineJoinType = ShapeStroke.LineJoinType.values()[jSONObject.optInt("lj") - 1];
            b bVar = null;
            ArrayList arrayList = new ArrayList();
            if (jSONObject.has("d")) {
                JSONArray optJSONArray = jSONObject.optJSONArray("d");
                b bVar2 = null;
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject5 = optJSONArray.optJSONObject(i);
                    String optString2 = optJSONObject5.optString("n");
                    if (optString2.equals("o")) {
                        bVar2 = b.a.a(optJSONObject5.optJSONObject("v"), bdVar);
                    } else if (optString2.equals("d") || optString2.equals("g")) {
                        arrayList.add(b.a.a(optJSONObject5.optJSONObject("v"), bdVar));
                    }
                }
                if (arrayList.size() == 1) {
                    arrayList.add(arrayList.get(0));
                }
                bVar = bVar2;
            }
            return new ar(optString, gradientType, cVar, dVar, fVar, fVar2, a2, lineCapType, lineJoinType, arrayList, bVar);
        }
    }
}
