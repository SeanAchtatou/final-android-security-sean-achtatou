package com.airbnb.lottie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: FontCharacter */
class al {

    /* renamed from: a  reason: collision with root package name */
    private final List<ce> f2459a;

    /* renamed from: b  reason: collision with root package name */
    private final char f2460b;

    /* renamed from: c  reason: collision with root package name */
    private final int f2461c;

    /* renamed from: d  reason: collision with root package name */
    private final double f2462d;

    /* renamed from: e  reason: collision with root package name */
    private final String f2463e;

    /* renamed from: f  reason: collision with root package name */
    private final String f2464f;

    static int a(char c2, String str, String str2) {
        return ((((c2 + 0) * 31) + str.hashCode()) * 31) + str2.hashCode();
    }

    al(List<ce> list, char c2, int i, double d2, String str, String str2) {
        this.f2459a = list;
        this.f2460b = c2;
        this.f2461c = i;
        this.f2462d = d2;
        this.f2463e = str;
        this.f2464f = str2;
    }

    /* access modifiers changed from: package-private */
    public List<ce> a() {
        return this.f2459a;
    }

    /* access modifiers changed from: package-private */
    public double b() {
        return this.f2462d;
    }

    /* compiled from: FontCharacter */
    static class a {
        static al a(JSONObject jSONObject, bd bdVar) {
            int i = 0;
            char charAt = jSONObject.optString("ch").charAt(0);
            int optInt = jSONObject.optInt("size");
            double optDouble = jSONObject.optDouble("w");
            String optString = jSONObject.optString("style");
            String optString2 = jSONObject.optString("fFamily");
            JSONArray optJSONArray = jSONObject.optJSONObject("data").optJSONArray("shapes");
            List emptyList = Collections.emptyList();
            if (optJSONArray != null) {
                ArrayList arrayList = new ArrayList(optJSONArray.length());
                while (true) {
                    int i2 = i;
                    if (i2 >= optJSONArray.length()) {
                        break;
                    }
                    arrayList.add((ce) ce.a(optJSONArray.optJSONObject(i2), bdVar));
                    i = i2 + 1;
                }
                emptyList = arrayList;
            }
            return new al(emptyList, charAt, optInt, optDouble, optString, optString2);
        }
    }

    public int hashCode() {
        return a(this.f2460b, this.f2464f, this.f2463e);
    }
}
