package com.airbnb.lottie;

import com.airbnb.lottie.p;
import java.util.Collections;

/* compiled from: StaticKeyframeAnimation */
class cl<T> extends bb<T> {

    /* renamed from: b  reason: collision with root package name */
    private final T f2641b;

    cl(T t) {
        super(Collections.emptyList());
        this.f2641b = t;
    }

    public void a(float f2) {
    }

    /* access modifiers changed from: package-private */
    public void a(p.a aVar) {
    }

    public T b() {
        return this.f2641b;
    }

    public T a(ba<T> baVar, float f2) {
        return this.f2641b;
    }
}
