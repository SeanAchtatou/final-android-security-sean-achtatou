package com.airbnb.lottie;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.airbnb.lottie.Layer;
import com.lody.virtual.os.VUserInfo;
import java.util.HashSet;
import java.util.Set;

/* compiled from: LottieDrawable */
public class be extends Drawable implements Drawable.Callback {

    /* renamed from: c  reason: collision with root package name */
    private static final String f2533c = be.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    aj f2534a;

    /* renamed from: b  reason: collision with root package name */
    cn f2535b;

    /* renamed from: d  reason: collision with root package name */
    private final Matrix f2536d = new Matrix();

    /* renamed from: e  reason: collision with root package name */
    private bd f2537e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final ValueAnimator f2538f = ValueAnimator.ofFloat(0.0f, 1.0f);

    /* renamed from: g  reason: collision with root package name */
    private float f2539g = 1.0f;

    /* renamed from: h  reason: collision with root package name */
    private float f2540h = 0.0f;
    private float i = 1.0f;
    private final Set<a> j = new HashSet();
    private av k;
    private String l;
    private au m;
    private ak n;
    private boolean o;
    private boolean p;
    /* access modifiers changed from: private */
    public boolean q;
    private boolean r;
    private w s;
    private int t = VUserInfo.FLAG_MASK_USER_TYPE;
    private boolean u;

    public be() {
        this.f2538f.setRepeatCount(0);
        this.f2538f.setInterpolator(new LinearInterpolator());
        this.f2538f.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                if (be.this.q) {
                    be.this.f2538f.cancel();
                    be.this.b(1.0f);
                    return;
                }
                be.this.b(((Float) valueAnimator.getAnimatedValue()).floatValue());
            }
        });
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.r;
    }

    public void a(boolean z) {
        if (Build.VERSION.SDK_INT < 19) {
            Log.w(f2533c, "Merge paths are not supported pre-Kit Kat.");
            return;
        }
        this.r = z;
        if (this.f2537e != null) {
            p();
        }
    }

    public void a(String str) {
        this.l = str;
    }

    public String b() {
        return this.l;
    }

    public void c() {
        if (this.k != null) {
            this.k.a();
        }
    }

    public boolean a(bd bdVar) {
        if (this.f2537e == bdVar) {
            return false;
        }
        r();
        this.f2537e = bdVar;
        a(this.f2539g);
        s();
        p();
        q();
        b(this.f2540h);
        if (this.o) {
            this.o = false;
            h();
        }
        if (this.p) {
            this.p = false;
            i();
        }
        bdVar.a(this.u);
        return true;
    }

    public void b(boolean z) {
        this.u = z;
        if (this.f2537e != null) {
            this.f2537e.a(z);
        }
    }

    public bq d() {
        if (this.f2537e != null) {
            return this.f2537e.a();
        }
        return null;
    }

    private void p() {
        this.s = new w(this, Layer.a.a(this.f2537e), this.f2537e.i(), this.f2537e);
    }

    private void q() {
        if (this.s != null) {
            for (a next : this.j) {
                this.s.a(next.f2542a, next.f2543b, next.f2544c);
            }
        }
    }

    private void r() {
        c();
        this.s = null;
        this.k = null;
        invalidateSelf();
    }

    public void invalidateSelf() {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    public void setAlpha(int i2) {
        this.t = i2;
    }

    public int getAlpha() {
        return this.t;
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public void a(ColorFilter colorFilter) {
        a(null, null, colorFilter);
    }

    private void a(String str, String str2, ColorFilter colorFilter) {
        a aVar = new a(str, str2, colorFilter);
        if (colorFilter != null || !this.j.contains(aVar)) {
            this.j.add(new a(str, str2, colorFilter));
        } else {
            this.j.remove(aVar);
        }
        if (this.s != null) {
            this.s.a(str, str2, colorFilter);
        }
    }

    public int getOpacity() {
        return -3;
    }

    public void draw(Canvas canvas) {
        boolean z = false;
        bc.a("Drawable#draw");
        if (this.s != null) {
            float f2 = this.i;
            float f3 = 1.0f;
            float a2 = a(canvas);
            if (this.s.g() || this.s.f()) {
                f3 = f2 / a2;
                f2 = Math.min(f2, a2);
                if (f3 > 1.001f) {
                    z = true;
                }
            }
            if (z) {
                canvas.save();
                float f4 = f3 * f3;
                canvas.scale(f4, f4, (float) ((int) ((((float) this.f2537e.b().width()) * f2) / 2.0f)), (float) ((int) ((((float) this.f2537e.b().height()) * f2) / 2.0f)));
            }
            this.f2536d.reset();
            this.f2536d.preScale(f2, f2);
            this.s.a(canvas, this.f2536d, this.t);
            if (z) {
                canvas.restore();
            }
            bc.b("Drawable#draw");
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.q = true;
    }

    public void c(boolean z) {
        this.f2538f.setRepeatCount(z ? -1 : 0);
    }

    public boolean f() {
        return this.f2538f.getRepeatCount() == -1;
    }

    public boolean g() {
        return this.f2538f.isRunning();
    }

    public void h() {
        d(((double) this.f2540h) > 0.0d && ((double) this.f2540h) < 1.0d);
    }

    private void d(boolean z) {
        if (this.s == null) {
            this.o = true;
            this.p = false;
            return;
        }
        long duration = z ? (long) (this.f2540h * ((float) this.f2538f.getDuration())) : 0;
        this.f2538f.start();
        if (z) {
            this.f2538f.setCurrentPlayTime(duration);
        }
    }

    public void i() {
        e(((double) this.f2540h) > 0.0d && ((double) this.f2540h) < 1.0d);
    }

    private void e(boolean z) {
        if (this.s == null) {
            this.o = false;
            this.p = true;
            return;
        }
        if (z) {
            this.f2538f.setCurrentPlayTime((long) (this.f2540h * ((float) this.f2538f.getDuration())));
        }
        this.f2538f.reverse();
    }

    public void a(float f2) {
        this.f2539g = f2;
        if (f2 < 0.0f) {
            this.f2538f.setFloatValues(1.0f, 0.0f);
        } else {
            this.f2538f.setFloatValues(0.0f, 1.0f);
        }
        if (this.f2537e != null) {
            this.f2538f.setDuration((long) (((float) this.f2537e.c()) / Math.abs(f2)));
        }
    }

    public void b(float f2) {
        this.f2540h = f2;
        if (this.s != null) {
            this.s.a(f2);
        }
    }

    public float j() {
        return this.f2540h;
    }

    public void c(float f2) {
        this.i = f2;
        s();
    }

    public void a(au auVar) {
        this.m = auVar;
        if (this.k != null) {
            this.k.a(auVar);
        }
    }

    public void a(aj ajVar) {
        this.f2534a = ajVar;
        if (this.n != null) {
            this.n.a(ajVar);
        }
    }

    public void a(cn cnVar) {
        this.f2535b = cnVar;
    }

    /* access modifiers changed from: package-private */
    public cn k() {
        return this.f2535b;
    }

    /* access modifiers changed from: package-private */
    public boolean l() {
        return this.f2535b == null && this.f2537e.j().b() > 0;
    }

    public float m() {
        return this.i;
    }

    public bd n() {
        return this.f2537e;
    }

    private void s() {
        if (this.f2537e != null) {
            float m2 = m();
            setBounds(0, 0, (int) (((float) this.f2537e.b().width()) * m2), (int) (m2 * ((float) this.f2537e.b().height())));
        }
    }

    public void o() {
        this.o = false;
        this.p = false;
        this.f2538f.cancel();
    }

    public int getIntrinsicWidth() {
        if (this.f2537e == null) {
            return -1;
        }
        return (int) (((float) this.f2537e.b().width()) * this.i);
    }

    public int getIntrinsicHeight() {
        if (this.f2537e == null) {
            return -1;
        }
        return (int) (((float) this.f2537e.b().height()) * this.i);
    }

    /* access modifiers changed from: package-private */
    public Bitmap b(String str) {
        av t2 = t();
        if (t2 != null) {
            return t2.a(str);
        }
        return null;
    }

    private av t() {
        if (getCallback() == null) {
            return null;
        }
        if (this.k != null && !this.k.a(v())) {
            this.k.a();
            this.k = null;
        }
        if (this.k == null) {
            this.k = new av(getCallback(), this.l, this.m, this.f2537e.l());
        }
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public Typeface a(String str, String str2) {
        ak u2 = u();
        if (u2 != null) {
            return u2.a(str, str2);
        }
        return null;
    }

    private ak u() {
        if (getCallback() == null) {
            return null;
        }
        if (this.n == null) {
            this.n = new ak(getCallback(), this.f2534a);
        }
        return this.n;
    }

    private Context v() {
        Drawable.Callback callback = getCallback();
        if (callback == null) {
            return null;
        }
        if (callback instanceof View) {
            return ((View) callback).getContext();
        }
        return null;
    }

    private float a(Canvas canvas) {
        return Math.min(((float) canvas.getWidth()) / ((float) this.f2537e.b().width()), ((float) canvas.getHeight()) / ((float) this.f2537e.b().height()));
    }

    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j2);
        }
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    /* compiled from: LottieDrawable */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        final String f2542a;

        /* renamed from: b  reason: collision with root package name */
        final String f2543b;

        /* renamed from: c  reason: collision with root package name */
        final ColorFilter f2544c;

        a(String str, String str2, ColorFilter colorFilter) {
            this.f2542a = str;
            this.f2543b = str2;
            this.f2544c = colorFilter;
        }

        public int hashCode() {
            int i = 17;
            if (this.f2542a != null) {
                i = this.f2542a.hashCode() * 527;
            }
            if (this.f2543b != null) {
                return i * 31 * this.f2543b.hashCode();
            }
            return i;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (hashCode() == aVar.hashCode() && this.f2544c == aVar.f2544c) {
                return true;
            }
            return false;
        }
    }
}
