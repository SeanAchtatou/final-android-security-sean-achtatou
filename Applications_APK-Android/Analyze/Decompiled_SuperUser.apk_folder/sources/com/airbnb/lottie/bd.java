package com.airbnb.lottie;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.support.v4.util.e;
import android.support.v4.util.j;
import android.util.Log;
import com.airbnb.lottie.Layer;
import com.airbnb.lottie.ai;
import com.airbnb.lottie.al;
import com.airbnb.lottie.bf;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: LottieComposition */
public class bd {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Map<String, List<Layer>> f2525a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final Map<String, bf> f2526b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final Map<String, ai> f2527c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final j<al> f2528d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final e<Layer> f2529e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final List<Layer> f2530f;

    /* renamed from: g  reason: collision with root package name */
    private final HashSet<String> f2531g;

    /* renamed from: h  reason: collision with root package name */
    private final bq f2532h;
    private final Rect i;
    private final long j;
    private final long k;
    private final float l;
    private final float m;
    private final int n;
    private final int o;
    private final int p;

    private bd(Rect rect, long j2, long j3, float f2, float f3, int i2, int i3, int i4) {
        this.f2525a = new HashMap();
        this.f2526b = new HashMap();
        this.f2527c = new HashMap();
        this.f2528d = new j<>();
        this.f2529e = new e<>();
        this.f2530f = new ArrayList();
        this.f2531g = new HashSet<>();
        this.f2532h = new bq();
        this.i = rect;
        this.j = j2;
        this.k = j3;
        this.l = f2;
        this.m = f3;
        this.n = i2;
        this.o = i3;
        this.p = i4;
        if (!cs.a(this, 4, 5, 0)) {
            a("Lottie only supports bodymovin >= 4.5.0");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        Log.w("LOTTIE", str);
        this.f2531g.add(str);
    }

    public void a(boolean z) {
        this.f2532h.a(z);
    }

    public bq a() {
        return this.f2532h;
    }

    /* access modifiers changed from: package-private */
    public Layer a(long j2) {
        return this.f2529e.a(j2);
    }

    public Rect b() {
        return this.i;
    }

    public long c() {
        return (long) ((((float) (this.k - this.j)) / this.l) * 1000.0f);
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public int e() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public long g() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public long h() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public List<Layer> i() {
        return this.f2530f;
    }

    /* access modifiers changed from: package-private */
    public List<Layer> b(String str) {
        return this.f2525a.get(str);
    }

    /* access modifiers changed from: package-private */
    public j<al> j() {
        return this.f2528d;
    }

    /* access modifiers changed from: package-private */
    public Map<String, ai> k() {
        return this.f2527c;
    }

    /* access modifiers changed from: package-private */
    public Map<String, bf> l() {
        return this.f2526b;
    }

    /* access modifiers changed from: package-private */
    public float m() {
        return (((float) c()) * this.l) / 1000.0f;
    }

    /* access modifiers changed from: package-private */
    public float n() {
        return this.m;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("LottieComposition:\n");
        for (Layer a2 : this.f2530f) {
            sb.append(a2.a("\t"));
        }
        return sb.toString();
    }

    /* compiled from: LottieComposition */
    public static class a {
        public static s a(Context context, String str, bm bmVar) {
            try {
                return a(context, context.getAssets().open(str), bmVar);
            } catch (IOException e2) {
                throw new IllegalStateException("Unable to find file " + str, e2);
            }
        }

        public static s a(Context context, InputStream inputStream, bm bmVar) {
            af afVar = new af(context.getResources(), bmVar);
            afVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new InputStream[]{inputStream});
            return afVar;
        }

        public static s a(Resources resources, JSONObject jSONObject, bm bmVar) {
            ay ayVar = new ay(resources, bmVar);
            ayVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new JSONObject[]{jSONObject});
            return ayVar;
        }

        static bd a(Resources resources, InputStream inputStream) {
            try {
                byte[] bArr = new byte[inputStream.available()];
                inputStream.read(bArr);
                return a(resources, new JSONObject(new String(bArr, "UTF-8")));
            } catch (IOException e2) {
                Log.e("LOTTIE", "Failed to load composition.", new IllegalStateException("Unable to find file.", e2));
            } catch (JSONException e3) {
                Log.e("LOTTIE", "Failed to load composition.", new IllegalStateException("Unable to load JSON.", e3));
            } finally {
                cs.a(inputStream);
            }
            return null;
        }

        static bd a(Resources resources, JSONObject jSONObject) {
            Rect rect;
            float f2 = resources.getDisplayMetrics().density;
            int optInt = jSONObject.optInt("w", -1);
            int optInt2 = jSONObject.optInt("h", -1);
            if (optInt == -1 || optInt2 == -1) {
                rect = null;
            } else {
                rect = new Rect(0, 0, (int) (((float) optInt) * f2), (int) (((float) optInt2) * f2));
            }
            String[] split = jSONObject.optString("v").split("[.]");
            bd bdVar = new bd(rect, jSONObject.optLong("ip", 0), jSONObject.optLong("op", 0), (float) jSONObject.optDouble("fr", 0.0d), f2, Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
            JSONArray optJSONArray = jSONObject.optJSONArray("assets");
            b(optJSONArray, bdVar);
            a(optJSONArray, bdVar);
            b(jSONObject.optJSONObject("fonts"), bdVar);
            c(jSONObject.optJSONArray("chars"), bdVar);
            a(jSONObject, bdVar);
            return bdVar;
        }

        private static void a(JSONObject jSONObject, bd bdVar) {
            int i = 0;
            JSONArray optJSONArray = jSONObject.optJSONArray("layers");
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i2 = 0; i2 < length; i2++) {
                    Layer a2 = Layer.a.a(optJSONArray.optJSONObject(i2), bdVar);
                    if (a2.k() == Layer.LayerType.Image) {
                        i++;
                    }
                    a(bdVar.f2530f, bdVar.f2529e, a2);
                }
                if (i > 4) {
                    bdVar.a("You have " + i + " images. Lottie should primarily be " + "used with shapes. If you are using Adobe Illustrator, convert the Illustrator layers" + " to shape layers.");
                }
            }
        }

        private static void a(JSONArray jSONArray, bd bdVar) {
            if (jSONArray != null) {
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject optJSONObject = jSONArray.optJSONObject(i);
                    JSONArray optJSONArray = optJSONObject.optJSONArray("layers");
                    if (optJSONArray != null) {
                        ArrayList arrayList = new ArrayList(optJSONArray.length());
                        e eVar = new e();
                        for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                            Layer a2 = Layer.a.a(optJSONArray.optJSONObject(i2), bdVar);
                            eVar.b(a2.e(), a2);
                            arrayList.add(a2);
                        }
                        bdVar.f2525a.put(optJSONObject.optString("id"), arrayList);
                    }
                }
            }
        }

        private static void b(JSONArray jSONArray, bd bdVar) {
            if (jSONArray != null) {
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject optJSONObject = jSONArray.optJSONObject(i);
                    if (optJSONObject.has("p")) {
                        bf a2 = bf.a.a(optJSONObject);
                        bdVar.f2526b.put(a2.a(), a2);
                    }
                }
            }
        }

        private static void b(JSONObject jSONObject, bd bdVar) {
            JSONArray optJSONArray;
            if (jSONObject != null && (optJSONArray = jSONObject.optJSONArray("list")) != null) {
                int length = optJSONArray.length();
                for (int i = 0; i < length; i++) {
                    ai a2 = ai.a.a(optJSONArray.optJSONObject(i));
                    bdVar.f2527c.put(a2.b(), a2);
                }
            }
        }

        private static void c(JSONArray jSONArray, bd bdVar) {
            if (jSONArray != null) {
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    al a2 = al.a.a(jSONArray.optJSONObject(i), bdVar);
                    bdVar.f2528d.b(a2.hashCode(), a2);
                }
            }
        }

        private static void a(List<Layer> list, e<Layer> eVar, Layer layer) {
            list.add(layer);
            eVar.b(layer.e(), layer);
        }
    }
}
