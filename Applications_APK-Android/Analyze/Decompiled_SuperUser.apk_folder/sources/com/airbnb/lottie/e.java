package com.airbnb.lottie;

import android.graphics.PointF;
import com.airbnb.lottie.b;
import com.airbnb.lottie.bo;
import com.airbnb.lottie.m;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: AnimatablePathValue */
class e implements m<PointF> {

    /* renamed from: a  reason: collision with root package name */
    private final List<bo> f2672a;

    /* renamed from: b  reason: collision with root package name */
    private PointF f2673b;

    static m<PointF> a(JSONObject jSONObject, bd bdVar) {
        if (jSONObject.has("k")) {
            return new e(jSONObject.opt("k"), bdVar);
        }
        return new i(b.a.a(jSONObject.optJSONObject("x"), bdVar), b.a.a(jSONObject.optJSONObject("y"), bdVar));
    }

    e() {
        this.f2672a = new ArrayList();
        this.f2673b = new PointF(0.0f, 0.0f);
    }

    e(Object obj, bd bdVar) {
        this.f2672a = new ArrayList();
        if (a(obj)) {
            JSONArray jSONArray = (JSONArray) obj;
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                this.f2672a.add(bo.a.a(jSONArray.optJSONObject(i), bdVar, a.f2674a));
            }
            ba.a(this.f2672a);
            return;
        }
        this.f2673b = az.a((JSONArray) obj, bdVar.n());
    }

    private boolean a(Object obj) {
        if (!(obj instanceof JSONArray)) {
            return false;
        }
        Object opt = ((JSONArray) obj).opt(0);
        return (opt instanceof JSONObject) && ((JSONObject) opt).has("t");
    }

    /* renamed from: a */
    public bb<PointF> b() {
        if (!c()) {
            return new cl(this.f2673b);
        }
        return new bp(this.f2672a);
    }

    public boolean c() {
        return !this.f2672a.isEmpty();
    }

    public String toString() {
        return "initialPoint=" + this.f2673b;
    }

    /* compiled from: AnimatablePathValue */
    private static class a implements m.a<PointF> {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final m.a<PointF> f2674a = new a();

        private a() {
        }

        /* renamed from: a */
        public PointF b(Object obj, float f2) {
            return az.a((JSONArray) obj, f2);
        }
    }
}
