package com.airbnb.lottie;

import android.annotation.TargetApi;
import android.graphics.Path;
import android.os.Build;
import java.util.ArrayList;
import java.util.List;

@TargetApi(19)
/* compiled from: MergePathsContent */
class bi implements at, bn {

    /* renamed from: a  reason: collision with root package name */
    private final Path f2554a = new Path();

    /* renamed from: b  reason: collision with root package name */
    private final Path f2555b = new Path();

    /* renamed from: c  reason: collision with root package name */
    private final Path f2556c = new Path();

    /* renamed from: d  reason: collision with root package name */
    private final String f2557d;

    /* renamed from: e  reason: collision with root package name */
    private final List<bn> f2558e = new ArrayList();

    /* renamed from: f  reason: collision with root package name */
    private final MergePaths f2559f;

    bi(MergePaths mergePaths) {
        if (Build.VERSION.SDK_INT < 19) {
            throw new IllegalStateException("Merge paths are not supported pre-KitKat.");
        }
        this.f2557d = mergePaths.a();
        this.f2559f = mergePaths;
    }

    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP:0: B:0:0x0000->B:3:0x000a, LOOP_START, MTH_ENTER_BLOCK] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.util.ListIterator<com.airbnb.lottie.y> r3) {
        /*
            r2 = this;
        L_0x0000:
            boolean r0 = r3.hasPrevious()
            if (r0 == 0) goto L_0x000c
            java.lang.Object r0 = r3.previous()
            if (r0 != r2) goto L_0x0000
        L_0x000c:
            boolean r0 = r3.hasPrevious()
            if (r0 == 0) goto L_0x0027
            java.lang.Object r0 = r3.previous()
            com.airbnb.lottie.y r0 = (com.airbnb.lottie.y) r0
            boolean r1 = r0 instanceof com.airbnb.lottie.bn
            if (r1 == 0) goto L_0x000c
            java.util.List<com.airbnb.lottie.bn> r1 = r2.f2558e
            com.airbnb.lottie.bn r0 = (com.airbnb.lottie.bn) r0
            r1.add(r0)
            r3.remove()
            goto L_0x000c
        L_0x0027:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.airbnb.lottie.bi.a(java.util.ListIterator):void");
    }

    public void a(List<y> list, List<y> list2) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f2558e.size()) {
                this.f2558e.get(i2).a(list, list2);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public Path d() {
        this.f2556c.reset();
        switch (this.f2559f.b()) {
            case Merge:
                a();
                break;
            case Add:
                a(Path.Op.UNION);
                break;
            case Subtract:
                a(Path.Op.REVERSE_DIFFERENCE);
                break;
            case Intersect:
                a(Path.Op.INTERSECT);
                break;
            case ExcludeIntersections:
                a(Path.Op.XOR);
                break;
        }
        return this.f2556c;
    }

    public String e() {
        return this.f2557d;
    }

    private void a() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f2558e.size()) {
                this.f2556c.addPath(this.f2558e.get(i2).d());
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    @TargetApi(19)
    private void a(Path.Op op) {
        this.f2555b.reset();
        this.f2554a.reset();
        int size = this.f2558e.size() - 1;
        while (true) {
            int i = size;
            if (i < 1) {
                break;
            }
            bn bnVar = this.f2558e.get(i);
            if (bnVar instanceof z) {
                List<bn> b2 = ((z) bnVar).b();
                for (int size2 = b2.size() - 1; size2 >= 0; size2--) {
                    Path d2 = b2.get(size2).d();
                    d2.transform(((z) bnVar).c());
                    this.f2555b.addPath(d2);
                }
            } else {
                this.f2555b.addPath(bnVar.d());
            }
            size = i - 1;
        }
        bn bnVar2 = this.f2558e.get(0);
        if (bnVar2 instanceof z) {
            List<bn> b3 = ((z) bnVar2).b();
            for (int i2 = 0; i2 < b3.size(); i2++) {
                Path d3 = b3.get(i2).d();
                d3.transform(((z) bnVar2).c());
                this.f2554a.addPath(d3);
            }
        } else {
            this.f2554a.set(bnVar2.d());
        }
        this.f2556c.op(this.f2554a, this.f2555b, op);
    }
}
