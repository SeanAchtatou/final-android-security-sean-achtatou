package com.airbnb.lottie;

import android.graphics.Color;
import android.util.Log;
import com.airbnb.lottie.m;
import com.airbnb.lottie.n;
import com.lody.virtual.os.VUserInfo;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: AnimatableGradientColorValue */
class c extends o<an, an> {
    private c(List<ba<an>> list, an anVar) {
        super(list, anVar);
    }

    /* renamed from: a */
    public bb<an> b() {
        if (!e()) {
            return new cl(this.f2697b);
        }
        return new ao(this.f2696a);
    }

    /* compiled from: AnimatableGradientColorValue */
    static final class a {
        static c a(JSONObject jSONObject, bd bdVar) {
            n.a a2 = n.a(jSONObject, 1.0f, bdVar, new b(jSONObject.optInt("p", jSONObject.optJSONArray("k").length() / 4))).a();
            return new c(a2.f2694a, (an) a2.f2695b);
        }
    }

    /* compiled from: AnimatableGradientColorValue */
    private static class b implements m.a<an> {

        /* renamed from: a  reason: collision with root package name */
        private final int f2608a;

        private b(int i) {
            this.f2608a = i;
        }

        /* renamed from: a */
        public an b(Object obj, float f2) {
            JSONArray jSONArray = (JSONArray) obj;
            float[] fArr = new float[this.f2608a];
            int[] iArr = new int[this.f2608a];
            an anVar = new an(fArr, iArr);
            if (jSONArray.length() != this.f2608a * 4) {
                Log.w("LOTTIE", "Unexpected gradient length: " + jSONArray.length() + ". Expected " + (this.f2608a * 4) + ". This may affect the appearance of the gradient. " + "Make sure to save your After Effects file before exporting an animation with " + "gradients.");
            }
            int i = 0;
            int i2 = 0;
            for (int i3 = 0; i3 < this.f2608a * 4; i3++) {
                int i4 = i3 / 4;
                double optDouble = jSONArray.optDouble(i3);
                switch (i3 % 4) {
                    case 0:
                        fArr[i4] = (float) optDouble;
                        break;
                    case 1:
                        i2 = (int) (optDouble * 255.0d);
                        break;
                    case 2:
                        i = (int) (optDouble * 255.0d);
                        break;
                    case 3:
                        iArr[i4] = Color.argb((int) VUserInfo.FLAG_MASK_USER_TYPE, i2, i, (int) (optDouble * 255.0d));
                        break;
                }
            }
            a(anVar, jSONArray);
            return anVar;
        }

        private void a(an anVar, JSONArray jSONArray) {
            int i = this.f2608a * 4;
            if (jSONArray.length() > i) {
                int length = (jSONArray.length() - i) / 2;
                double[] dArr = new double[length];
                double[] dArr2 = new double[length];
                int i2 = 0;
                for (int i3 = i; i3 < jSONArray.length(); i3++) {
                    if (i3 % 2 == 0) {
                        dArr[i2] = jSONArray.optDouble(i3);
                    } else {
                        dArr2[i2] = jSONArray.optDouble(i3);
                        i2++;
                    }
                }
                for (int i4 = 0; i4 < anVar.c(); i4++) {
                    int i5 = anVar.b()[i4];
                    anVar.b()[i4] = Color.argb(a((double) anVar.a()[i4], dArr, dArr2), Color.red(i5), Color.green(i5), Color.blue(i5));
                }
            }
        }

        private int a(double d2, double[] dArr, double[] dArr2) {
            int i = 1;
            while (true) {
                int i2 = i;
                if (i2 >= dArr.length) {
                    return (int) (dArr2[dArr2.length - 1] * 255.0d);
                }
                double d3 = dArr[i2 - 1];
                double d4 = dArr[i2];
                if (dArr[i2] >= d2) {
                    return (int) (bj.a(dArr2[i2 - 1], dArr2[i2], (d2 - d3) / (d4 - d3)) * 255.0d);
                }
                i = i2 + 1;
            }
        }
    }
}
