package com.airbnb.lottie;

import com.airbnb.lottie.b;
import org.json.JSONObject;

class ShapeTrimPath implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2414a;

    /* renamed from: b  reason: collision with root package name */
    private final Type f2415b;

    /* renamed from: c  reason: collision with root package name */
    private final b f2416c;

    /* renamed from: d  reason: collision with root package name */
    private final b f2417d;

    /* renamed from: e  reason: collision with root package name */
    private final b f2418e;

    enum Type {
        Simultaneously,
        Individually;

        static Type a(int i) {
            switch (i) {
                case 1:
                    return Simultaneously;
                case 2:
                    return Individually;
                default:
                    throw new IllegalArgumentException("Unknown trim path type " + i);
            }
        }
    }

    private ShapeTrimPath(String str, Type type, b bVar, b bVar2, b bVar3) {
        this.f2414a = str;
        this.f2415b = type;
        this.f2416c = bVar;
        this.f2417d = bVar2;
        this.f2418e = bVar3;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f2414a;
    }

    /* access modifiers changed from: package-private */
    public Type b() {
        return this.f2415b;
    }

    /* access modifiers changed from: package-private */
    public b c() {
        return this.f2417d;
    }

    /* access modifiers changed from: package-private */
    public b d() {
        return this.f2416c;
    }

    /* access modifiers changed from: package-private */
    public b e() {
        return this.f2418e;
    }

    public y a(be beVar, q qVar) {
        return new cr(qVar, this);
    }

    public String toString() {
        return "Trim Path: {start: " + this.f2416c + ", end: " + this.f2417d + ", offset: " + this.f2418e + "}";
    }

    static class a {
        static ShapeTrimPath a(JSONObject jSONObject, bd bdVar) {
            return new ShapeTrimPath(jSONObject.optString("nm"), Type.a(jSONObject.optInt("m", 1)), b.a.a(jSONObject.optJSONObject("s"), bdVar, false), b.a.a(jSONObject.optJSONObject("e"), bdVar, false), b.a.a(jSONObject.optJSONObject("o"), bdVar, false));
        }
    }
}
