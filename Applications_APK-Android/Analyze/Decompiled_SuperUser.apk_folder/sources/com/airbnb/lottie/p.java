package com.airbnb.lottie;

import java.util.ArrayList;
import java.util.List;

/* compiled from: BaseKeyframeAnimation */
abstract class p<K, A> {

    /* renamed from: a  reason: collision with root package name */
    final List<a> f2698a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private boolean f2699b = false;

    /* renamed from: c  reason: collision with root package name */
    private final List<? extends ba<K>> f2700c;

    /* renamed from: d  reason: collision with root package name */
    private float f2701d = 0.0f;

    /* renamed from: e  reason: collision with root package name */
    private ba<K> f2702e;

    /* compiled from: BaseKeyframeAnimation */
    interface a {
        void a();
    }

    /* access modifiers changed from: package-private */
    public abstract A a(ba<K> baVar, float f2);

    p(List<? extends ba<K>> list) {
        this.f2700c = list;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f2699b = true;
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.f2698a.add(aVar);
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        if (f2 < f()) {
            f2 = 0.0f;
        } else if (f2 > g()) {
            f2 = 1.0f;
        }
        if (f2 != this.f2701d) {
            this.f2701d = f2;
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f2698a.size()) {
                    this.f2698a.get(i2).a();
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private ba<K> d() {
        int i = 0;
        if (this.f2700c.isEmpty()) {
            throw new IllegalStateException("There are no keyframes");
        } else if (this.f2702e != null && this.f2702e.a(this.f2701d)) {
            return this.f2702e;
        } else {
            ba<K> baVar = (ba) this.f2700c.get(0);
            if (this.f2701d < baVar.a()) {
                this.f2702e = baVar;
                return baVar;
            }
            while (!baVar.a(this.f2701d) && i < this.f2700c.size()) {
                baVar = (ba) this.f2700c.get(i);
                i++;
            }
            this.f2702e = baVar;
            return baVar;
        }
    }

    private float e() {
        if (this.f2699b) {
            return 0.0f;
        }
        ba d2 = d();
        if (!d2.c()) {
            return d2.f2514c.getInterpolation((this.f2701d - d2.a()) / (d2.b() - d2.a()));
        }
        return 0.0f;
    }

    private float f() {
        if (this.f2700c.isEmpty()) {
            return 0.0f;
        }
        return ((ba) this.f2700c.get(0)).a();
    }

    private float g() {
        if (this.f2700c.isEmpty()) {
            return 1.0f;
        }
        return ((ba) this.f2700c.get(this.f2700c.size() - 1)).b();
    }

    public A b() {
        return a(d(), e());
    }

    /* access modifiers changed from: package-private */
    public float c() {
        return this.f2701d;
    }
}
