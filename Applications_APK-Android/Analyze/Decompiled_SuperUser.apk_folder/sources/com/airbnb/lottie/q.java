package com.airbnb.lottie;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.Log;
import com.airbnb.lottie.Layer;
import com.airbnb.lottie.p;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: BaseLayer */
abstract class q implements ad, p.a {

    /* renamed from: a  reason: collision with root package name */
    final Matrix f2703a = new Matrix();

    /* renamed from: b  reason: collision with root package name */
    final be f2704b;

    /* renamed from: c  reason: collision with root package name */
    final Layer f2705c;

    /* renamed from: d  reason: collision with root package name */
    final cq f2706d;

    /* renamed from: e  reason: collision with root package name */
    private final Path f2707e = new Path();

    /* renamed from: f  reason: collision with root package name */
    private final Matrix f2708f = new Matrix();

    /* renamed from: g  reason: collision with root package name */
    private final Paint f2709g = new Paint(1);

    /* renamed from: h  reason: collision with root package name */
    private final Paint f2710h = new Paint(1);
    private final Paint i = new Paint(1);
    private final Paint j = new Paint();
    private final RectF k = new RectF();
    private final RectF l = new RectF();
    private final RectF m = new RectF();
    private final RectF n = new RectF();
    private final String o;
    private bg p;
    private q q;
    private q r;
    private List<q> s;
    private final List<p<?, ?>> t = new ArrayList();
    private boolean u = true;

    /* access modifiers changed from: package-private */
    public abstract void b(Canvas canvas, Matrix matrix, int i2);

    static q a(Layer layer, be beVar, bd bdVar) {
        switch (layer.k()) {
            case Shape:
                return new cg(beVar, layer);
            case PreComp:
                return new w(beVar, layer, bdVar.b(layer.g()), bdVar);
            case Solid:
                return new cj(beVar, layer);
            case Image:
                return new aw(beVar, layer, bdVar.n());
            case Null:
                return new bl(beVar, layer);
            case Text:
                return new cp(beVar, layer);
            default:
                Log.w("LOTTIE", "Unknown layer type " + layer.k());
                return null;
        }
    }

    q(be beVar, Layer layer) {
        this.f2704b = beVar;
        this.f2705c = layer;
        this.o = layer.f() + "#draw";
        this.j.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        this.f2710h.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        if (layer.l() == Layer.MatteType.Invert) {
            this.i.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        } else {
            this.i.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        }
        this.f2706d = layer.o().h();
        this.f2706d.a((p.a) this);
        this.f2706d.a(this);
        if (layer.j() != null && !layer.j().isEmpty()) {
            this.p = new bg(layer.j());
            for (p next : this.p.b()) {
                a(next);
                next.a(this);
            }
            for (bb next2 : this.p.c()) {
                a(next2);
                next2.a(this);
            }
        }
        f();
    }

    public void a() {
        g();
    }

    /* access modifiers changed from: package-private */
    public Layer b() {
        return this.f2705c;
    }

    /* access modifiers changed from: package-private */
    public void a(q qVar) {
        this.q = qVar;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.q != null;
    }

    /* access modifiers changed from: package-private */
    public void b(q qVar) {
        this.r = qVar;
    }

    private void f() {
        if (!this.f2705c.d().isEmpty()) {
            final ah ahVar = new ah(this.f2705c.d());
            ahVar.a();
            ahVar.a(new p.a() {
                public void a() {
                    q.this.a(((Float) ahVar.b()).floatValue() == 1.0f);
                }
            });
            a(((Float) ahVar.b()).floatValue() == 1.0f);
            a(ahVar);
            return;
        }
        a(true);
    }

    private void g() {
        this.f2704b.invalidateSelf();
    }

    /* access modifiers changed from: package-private */
    public void a(p<?, ?> pVar) {
        if (!(pVar instanceof cl)) {
            this.t.add(pVar);
        }
    }

    public void a(RectF rectF, Matrix matrix) {
        this.f2703a.set(matrix);
        this.f2703a.preConcat(this.f2706d.d());
    }

    @SuppressLint({"WrongConstant"})
    public void a(Canvas canvas, Matrix matrix, int i2) {
        bc.a(this.o);
        if (!this.u) {
            bc.b(this.o);
            return;
        }
        h();
        bc.a("Layer#parentMatrix");
        this.f2708f.reset();
        this.f2708f.set(matrix);
        for (int size = this.s.size() - 1; size >= 0; size--) {
            this.f2708f.preConcat(this.s.get(size).f2706d.d());
        }
        bc.b("Layer#parentMatrix");
        int intValue = (int) (((((float) this.f2706d.a().b().intValue()) * (((float) i2) / 255.0f)) / 100.0f) * 255.0f);
        if (c() || d()) {
            bc.a("Layer#computeBounds");
            this.k.set(0.0f, 0.0f, 0.0f, 0.0f);
            a(this.k, this.f2708f);
            c(this.k, this.f2708f);
            this.f2708f.preConcat(this.f2706d.d());
            b(this.k, this.f2708f);
            this.k.set(0.0f, 0.0f, (float) canvas.getWidth(), (float) canvas.getHeight());
            bc.b("Layer#computeBounds");
            bc.a("Layer#saveLayer");
            canvas.saveLayer(this.k, this.f2709g, 31);
            bc.b("Layer#saveLayer");
            a(canvas);
            bc.a("Layer#drawLayer");
            b(canvas, this.f2708f, intValue);
            bc.b("Layer#drawLayer");
            if (d()) {
                a(canvas, this.f2708f);
            }
            if (c()) {
                bc.a("Layer#drawMatte");
                bc.a("Layer#saveLayer");
                canvas.saveLayer(this.k, this.i, 19);
                bc.b("Layer#saveLayer");
                a(canvas);
                this.q.a(canvas, matrix, intValue);
                bc.a("Layer#restoreLayer");
                canvas.restore();
                bc.b("Layer#restoreLayer");
                bc.b("Layer#drawMatte");
            }
            bc.a("Layer#restoreLayer");
            canvas.restore();
            bc.b("Layer#restoreLayer");
            b(bc.b(this.o));
            return;
        }
        this.f2708f.preConcat(this.f2706d.d());
        bc.a("Layer#drawLayer");
        b(canvas, this.f2708f, intValue);
        bc.b("Layer#drawLayer");
        b(bc.b(this.o));
    }

    private void b(float f2) {
        this.f2704b.n().a().a(this.f2705c.f(), f2);
    }

    private void a(Canvas canvas) {
        bc.a("Layer#clearLayer");
        canvas.drawRect(this.k.left - 1.0f, this.k.top - 1.0f, this.k.right + 1.0f, 1.0f + this.k.bottom, this.j);
        bc.b("Layer#clearLayer");
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(android.graphics.RectF r10, android.graphics.Matrix r11) {
        /*
            r9 = this;
            r3 = 0
            r1 = 0
            android.graphics.RectF r0 = r9.l
            r0.set(r1, r1, r1, r1)
            boolean r0 = r9.d()
            if (r0 != 0) goto L_0x000e
        L_0x000d:
            return
        L_0x000e:
            com.airbnb.lottie.bg r0 = r9.p
            java.util.List r0 = r0.a()
            int r4 = r0.size()
            r2 = r3
        L_0x0019:
            if (r2 >= r4) goto L_0x009c
            com.airbnb.lottie.bg r0 = r9.p
            java.util.List r0 = r0.a()
            java.lang.Object r0 = r0.get(r2)
            com.airbnb.lottie.Mask r0 = (com.airbnb.lottie.Mask) r0
            com.airbnb.lottie.bg r1 = r9.p
            java.util.List r1 = r1.b()
            java.lang.Object r1 = r1.get(r2)
            com.airbnb.lottie.p r1 = (com.airbnb.lottie.p) r1
            java.lang.Object r1 = r1.b()
            android.graphics.Path r1 = (android.graphics.Path) r1
            android.graphics.Path r5 = r9.f2707e
            r5.set(r1)
            android.graphics.Path r1 = r9.f2707e
            r1.transform(r11)
            int[] r1 = com.airbnb.lottie.q.AnonymousClass2.f2714b
            com.airbnb.lottie.Mask$MaskMode r0 = r0.a()
            int r0 = r0.ordinal()
            r0 = r1[r0]
            switch(r0) {
                case 1: goto L_0x000d;
                default: goto L_0x0052;
            }
        L_0x0052:
            android.graphics.Path r0 = r9.f2707e
            android.graphics.RectF r1 = r9.n
            r0.computeBounds(r1, r3)
            if (r2 != 0) goto L_0x0066
            android.graphics.RectF r0 = r9.l
            android.graphics.RectF r1 = r9.n
            r0.set(r1)
        L_0x0062:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0019
        L_0x0066:
            android.graphics.RectF r0 = r9.l
            android.graphics.RectF r1 = r9.l
            float r1 = r1.left
            android.graphics.RectF r5 = r9.n
            float r5 = r5.left
            float r1 = java.lang.Math.min(r1, r5)
            android.graphics.RectF r5 = r9.l
            float r5 = r5.top
            android.graphics.RectF r6 = r9.n
            float r6 = r6.top
            float r5 = java.lang.Math.min(r5, r6)
            android.graphics.RectF r6 = r9.l
            float r6 = r6.right
            android.graphics.RectF r7 = r9.n
            float r7 = r7.right
            float r6 = java.lang.Math.max(r6, r7)
            android.graphics.RectF r7 = r9.l
            float r7 = r7.bottom
            android.graphics.RectF r8 = r9.n
            float r8 = r8.bottom
            float r7 = java.lang.Math.max(r7, r8)
            r0.set(r1, r5, r6, r7)
            goto L_0x0062
        L_0x009c:
            float r0 = r10.left
            android.graphics.RectF r1 = r9.l
            float r1 = r1.left
            float r0 = java.lang.Math.max(r0, r1)
            float r1 = r10.top
            android.graphics.RectF r2 = r9.l
            float r2 = r2.top
            float r1 = java.lang.Math.max(r1, r2)
            float r2 = r10.right
            android.graphics.RectF r3 = r9.l
            float r3 = r3.right
            float r2 = java.lang.Math.min(r2, r3)
            float r3 = r10.bottom
            android.graphics.RectF r4 = r9.l
            float r4 = r4.bottom
            float r3 = java.lang.Math.min(r3, r4)
            r10.set(r0, r1, r2, r3)
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.airbnb.lottie.q.b(android.graphics.RectF, android.graphics.Matrix):void");
    }

    private void c(RectF rectF, Matrix matrix) {
        if (c() && this.f2705c.l() != Layer.MatteType.Invert) {
            this.q.a(this.m, matrix);
            rectF.set(Math.max(rectF.left, this.m.left), Math.max(rectF.top, this.m.top), Math.min(rectF.right, this.m.right), Math.min(rectF.bottom, this.m.bottom));
        }
    }

    @SuppressLint({"WrongConstant"})
    private void a(Canvas canvas, Matrix matrix) {
        bc.a("Layer#drawMask");
        bc.a("Layer#saveLayer");
        canvas.saveLayer(this.k, this.f2710h, 19);
        bc.b("Layer#saveLayer");
        a(canvas);
        int size = this.p.a().size();
        for (int i2 = 0; i2 < size; i2++) {
            this.f2707e.set((Path) this.p.b().get(i2).b());
            this.f2707e.transform(matrix);
            switch (this.p.a().get(i2).a()) {
                case MaskModeSubtract:
                    this.f2707e.setFillType(Path.FillType.INVERSE_WINDING);
                    break;
                default:
                    this.f2707e.setFillType(Path.FillType.WINDING);
                    break;
            }
            int alpha = this.f2709g.getAlpha();
            this.f2709g.setAlpha((int) (((float) ((Integer) this.p.c().get(i2).b()).intValue()) * 2.55f));
            canvas.drawPath(this.f2707e, this.f2709g);
            this.f2709g.setAlpha(alpha);
        }
        bc.a("Layer#restoreLayer");
        canvas.restore();
        bc.b("Layer#restoreLayer");
        bc.b("Layer#drawMask");
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.p != null && !this.p.b().isEmpty();
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (z != this.u) {
            this.u = z;
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        if (this.f2705c.b() != 0.0f) {
            f2 /= this.f2705c.b();
        }
        if (this.q != null) {
            this.q.a(f2);
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.t.size()) {
                this.t.get(i3).a(f2);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void h() {
        if (this.s == null) {
            if (this.r == null) {
                this.s = Collections.emptyList();
                return;
            }
            this.s = new ArrayList();
            for (q qVar = this.r; qVar != null; qVar = qVar.r) {
                this.s.add(qVar);
            }
        }
    }

    public String e() {
        return this.f2705c.f();
    }

    public void a(List<y> list, List<y> list2) {
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
    }
}
