package com.airbnb.lottie;

import android.graphics.PointF;
import com.airbnb.lottie.b;
import org.json.JSONObject;

class PolystarShape implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2384a;

    /* renamed from: b  reason: collision with root package name */
    private final Type f2385b;

    /* renamed from: c  reason: collision with root package name */
    private final b f2386c;

    /* renamed from: d  reason: collision with root package name */
    private final m<PointF> f2387d;

    /* renamed from: e  reason: collision with root package name */
    private final b f2388e;

    /* renamed from: f  reason: collision with root package name */
    private final b f2389f;

    /* renamed from: g  reason: collision with root package name */
    private final b f2390g;

    /* renamed from: h  reason: collision with root package name */
    private final b f2391h;
    private final b i;

    enum Type {
        Star(1),
        Polygon(2);
        

        /* renamed from: c  reason: collision with root package name */
        private final int f2395c;

        private Type(int i) {
            this.f2395c = i;
        }

        static Type a(int i) {
            for (Type type : values()) {
                if (type.f2395c == i) {
                    return type;
                }
            }
            return null;
        }
    }

    private PolystarShape(String str, Type type, b bVar, m<PointF> mVar, b bVar2, b bVar3, b bVar4, b bVar5, b bVar6) {
        this.f2384a = str;
        this.f2385b = type;
        this.f2386c = bVar;
        this.f2387d = mVar;
        this.f2388e = bVar2;
        this.f2389f = bVar3;
        this.f2390g = bVar4;
        this.f2391h = bVar5;
        this.i = bVar6;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f2384a;
    }

    /* access modifiers changed from: package-private */
    public Type b() {
        return this.f2385b;
    }

    /* access modifiers changed from: package-private */
    public b c() {
        return this.f2386c;
    }

    /* access modifiers changed from: package-private */
    public m<PointF> d() {
        return this.f2387d;
    }

    /* access modifiers changed from: package-private */
    public b e() {
        return this.f2388e;
    }

    /* access modifiers changed from: package-private */
    public b f() {
        return this.f2389f;
    }

    /* access modifiers changed from: package-private */
    public b g() {
        return this.f2390g;
    }

    /* access modifiers changed from: package-private */
    public b h() {
        return this.f2391h;
    }

    /* access modifiers changed from: package-private */
    public b i() {
        return this.i;
    }

    public y a(be beVar, q qVar) {
        return new bt(beVar, qVar, this);
    }

    static class a {
        static PolystarShape a(JSONObject jSONObject, bd bdVar) {
            b bVar;
            b bVar2;
            String optString = jSONObject.optString("nm");
            Type a2 = Type.a(jSONObject.optInt("sy"));
            b a3 = b.a.a(jSONObject.optJSONObject("pt"), bdVar, false);
            m<PointF> a4 = e.a(jSONObject.optJSONObject("p"), bdVar);
            b a5 = b.a.a(jSONObject.optJSONObject("r"), bdVar, false);
            b a6 = b.a.a(jSONObject.optJSONObject("or"), bdVar);
            b a7 = b.a.a(jSONObject.optJSONObject("os"), bdVar, false);
            if (a2 == Type.Star) {
                bVar2 = b.a.a(jSONObject.optJSONObject("ir"), bdVar);
                bVar = b.a.a(jSONObject.optJSONObject("is"), bdVar, false);
            } else {
                bVar = null;
                bVar2 = null;
            }
            return new PolystarShape(optString, a2, a3, a4, a5, bVar2, a6, bVar, a7);
        }
    }
}
