package com.airbnb.lottie;

import com.airbnb.lottie.a;
import com.airbnb.lottie.b;
import org.json.JSONObject;

/* compiled from: AnimatableTextProperties */
class k {

    /* renamed from: a  reason: collision with root package name */
    final a f2679a;

    /* renamed from: b  reason: collision with root package name */
    final a f2680b;

    /* renamed from: c  reason: collision with root package name */
    final b f2681c;

    /* renamed from: d  reason: collision with root package name */
    final b f2682d;

    k(a aVar, a aVar2, b bVar, b bVar2) {
        this.f2679a = aVar;
        this.f2680b = aVar2;
        this.f2681c = bVar;
        this.f2682d = bVar2;
    }

    /* compiled from: AnimatableTextProperties */
    static final class a {
        static k a(JSONObject jSONObject, bd bdVar) {
            a aVar;
            a aVar2;
            b bVar;
            b bVar2 = null;
            if (jSONObject == null || !jSONObject.has("a")) {
                return new k(null, null, null, null);
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("a");
            JSONObject optJSONObject2 = optJSONObject.optJSONObject("fc");
            if (optJSONObject2 != null) {
                aVar = a.C0033a.a(optJSONObject2, bdVar);
            } else {
                aVar = null;
            }
            JSONObject optJSONObject3 = optJSONObject.optJSONObject("sc");
            if (optJSONObject3 != null) {
                aVar2 = a.C0033a.a(optJSONObject3, bdVar);
            } else {
                aVar2 = null;
            }
            JSONObject optJSONObject4 = optJSONObject.optJSONObject("sw");
            if (optJSONObject4 != null) {
                bVar = b.a.a(optJSONObject4, bdVar);
            } else {
                bVar = null;
            }
            JSONObject optJSONObject5 = optJSONObject.optJSONObject("t");
            if (optJSONObject5 != null) {
                bVar2 = b.a.a(optJSONObject5, bdVar);
            }
            return new k(aVar, aVar2, bVar, bVar2);
        }
    }
}
