package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import com.airbnb.lottie.p;
import java.util.List;

/* compiled from: RepeaterContent */
public class by implements ad, at, bn, p.a {

    /* renamed from: a  reason: collision with root package name */
    private final Matrix f2600a = new Matrix();

    /* renamed from: b  reason: collision with root package name */
    private final Path f2601b = new Path();

    /* renamed from: c  reason: collision with root package name */
    private final be f2602c;

    /* renamed from: d  reason: collision with root package name */
    private final q f2603d;

    /* renamed from: e  reason: collision with root package name */
    private final String f2604e;

    /* renamed from: f  reason: collision with root package name */
    private final bb<Float> f2605f;

    /* renamed from: g  reason: collision with root package name */
    private final bb<Float> f2606g;

    /* renamed from: h  reason: collision with root package name */
    private final cq f2607h;
    private z i;

    by(be beVar, q qVar, bx bxVar) {
        this.f2602c = beVar;
        this.f2603d = qVar;
        this.f2604e = bxVar.a();
        this.f2605f = bxVar.b().b();
        qVar.a(this.f2605f);
        this.f2605f.a(this);
        this.f2606g = bxVar.c().b();
        qVar.a(this.f2606g);
        this.f2606g.a(this);
        this.f2607h = bxVar.d().h();
        this.f2607h.a(qVar);
        this.f2607h.a(this);
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0005 A[LOOP:0: B:2:0x0005->B:5:0x000f, LOOP_START] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.util.ListIterator<com.airbnb.lottie.y> r7) {
        /*
            r6 = this;
            com.airbnb.lottie.z r0 = r6.i
            if (r0 == 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            boolean r0 = r7.hasPrevious()
            if (r0 == 0) goto L_0x0011
            java.lang.Object r0 = r7.previous()
            if (r0 != r6) goto L_0x0005
        L_0x0011:
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
        L_0x0016:
            boolean r0 = r7.hasPrevious()
            if (r0 == 0) goto L_0x0027
            java.lang.Object r0 = r7.previous()
            r4.add(r0)
            r7.remove()
            goto L_0x0016
        L_0x0027:
            java.util.Collections.reverse(r4)
            com.airbnb.lottie.z r0 = new com.airbnb.lottie.z
            com.airbnb.lottie.be r1 = r6.f2602c
            com.airbnb.lottie.q r2 = r6.f2603d
            java.lang.String r3 = "Repeater"
            r5 = 0
            r0.<init>(r1, r2, r3, r4, r5)
            r6.i = r0
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.airbnb.lottie.by.a(java.util.ListIterator):void");
    }

    public String e() {
        return this.f2604e;
    }

    public void a(List<y> list, List<y> list2) {
        this.i.a(list, list2);
    }

    public Path d() {
        Path d2 = this.i.d();
        this.f2601b.reset();
        float floatValue = ((Float) this.f2605f.b()).floatValue();
        float floatValue2 = ((Float) this.f2606g.b()).floatValue();
        for (int i2 = ((int) floatValue) - 1; i2 >= 0; i2--) {
            this.f2600a.set(this.f2607h.a(((float) i2) + floatValue2));
            this.f2601b.addPath(d2, this.f2600a);
        }
        return this.f2601b;
    }

    public void a(Canvas canvas, Matrix matrix, int i2) {
        float floatValue = ((Float) this.f2605f.b()).floatValue();
        float floatValue2 = ((Float) this.f2606g.b()).floatValue();
        float floatValue3 = this.f2607h.b().b().floatValue() / 100.0f;
        float floatValue4 = this.f2607h.c().b().floatValue() / 100.0f;
        for (int i3 = ((int) floatValue) - 1; i3 >= 0; i3--) {
            this.f2600a.set(matrix);
            this.f2600a.preConcat(this.f2607h.a(((float) i3) + floatValue2));
            this.i.a(canvas, this.f2600a, (int) (((float) i2) * bj.a(floatValue3, floatValue4, ((float) i3) / floatValue)));
        }
    }

    public void a(RectF rectF, Matrix matrix) {
        this.i.a(rectF, matrix);
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
        this.i.a(str, str2, colorFilter);
    }

    public void a() {
        this.f2602c.invalidateSelf();
    }
}
