package com.airbnb.lottie;

import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import com.airbnb.lottie.ShapeTrimPath;
import com.airbnb.lottie.p;
import java.util.List;

/* compiled from: RectangleContent */
class bv implements bn, p.a {

    /* renamed from: a  reason: collision with root package name */
    private final Path f2584a = new Path();

    /* renamed from: b  reason: collision with root package name */
    private final RectF f2585b = new RectF();

    /* renamed from: c  reason: collision with root package name */
    private final String f2586c;

    /* renamed from: d  reason: collision with root package name */
    private final be f2587d;

    /* renamed from: e  reason: collision with root package name */
    private final p<?, PointF> f2588e;

    /* renamed from: f  reason: collision with root package name */
    private final p<?, PointF> f2589f;

    /* renamed from: g  reason: collision with root package name */
    private final p<?, Float> f2590g;

    /* renamed from: h  reason: collision with root package name */
    private cr f2591h;
    private boolean i;

    bv(be beVar, q qVar, bw bwVar) {
        this.f2586c = bwVar.a();
        this.f2587d = beVar;
        this.f2588e = bwVar.d().b();
        this.f2589f = bwVar.c().b();
        this.f2590g = bwVar.b().b();
        qVar.a(this.f2588e);
        qVar.a(this.f2589f);
        qVar.a(this.f2590g);
        this.f2588e.a(this);
        this.f2589f.a(this);
        this.f2590g.a(this);
    }

    public String e() {
        return this.f2586c;
    }

    public void a() {
        b();
    }

    private void b() {
        this.i = false;
        this.f2587d.invalidateSelf();
    }

    public void a(List<y> list, List<y> list2) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < list.size()) {
                y yVar = list.get(i3);
                if ((yVar instanceof cr) && ((cr) yVar).b() == ShapeTrimPath.Type.Simultaneously) {
                    this.f2591h = (cr) yVar;
                    this.f2591h.a(this);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public Path d() {
        if (this.i) {
            return this.f2584a;
        }
        this.f2584a.reset();
        PointF b2 = this.f2589f.b();
        float f2 = b2.x / 2.0f;
        float f3 = b2.y / 2.0f;
        float floatValue = this.f2590g == null ? 0.0f : this.f2590g.b().floatValue();
        float min = Math.min(f2, f3);
        if (floatValue <= min) {
            min = floatValue;
        }
        PointF b3 = this.f2588e.b();
        this.f2584a.moveTo(b3.x + f2, (b3.y - f3) + min);
        this.f2584a.lineTo(b3.x + f2, (b3.y + f3) - min);
        if (min > 0.0f) {
            this.f2585b.set((b3.x + f2) - (2.0f * min), (b3.y + f3) - (2.0f * min), b3.x + f2, b3.y + f3);
            this.f2584a.arcTo(this.f2585b, 0.0f, 90.0f, false);
        }
        this.f2584a.lineTo((b3.x - f2) + min, b3.y + f3);
        if (min > 0.0f) {
            this.f2585b.set(b3.x - f2, (b3.y + f3) - (2.0f * min), (b3.x - f2) + (2.0f * min), b3.y + f3);
            this.f2584a.arcTo(this.f2585b, 90.0f, 90.0f, false);
        }
        this.f2584a.lineTo(b3.x - f2, (b3.y - f3) + min);
        if (min > 0.0f) {
            this.f2585b.set(b3.x - f2, b3.y - f3, (b3.x - f2) + (2.0f * min), (b3.y - f3) + (2.0f * min));
            this.f2584a.arcTo(this.f2585b, 180.0f, 90.0f, false);
        }
        this.f2584a.lineTo((b3.x + f2) - min, b3.y - f3);
        if (min > 0.0f) {
            this.f2585b.set((b3.x + f2) - (2.0f * min), b3.y - f3, f2 + b3.x, (b3.y - f3) + (min * 2.0f));
            this.f2584a.arcTo(this.f2585b, 270.0f, 90.0f, false);
        }
        this.f2584a.close();
        cs.a(this.f2584a, this.f2591h);
        this.i = true;
        return this.f2584a;
    }
}
