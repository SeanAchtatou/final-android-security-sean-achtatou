package com.airbnb.lottie;

import android.util.Log;
import org.json.JSONObject;

public class MergePaths implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2376a;

    /* renamed from: b  reason: collision with root package name */
    private final MergePathsMode f2377b;

    enum MergePathsMode {
        Merge,
        Add,
        Subtract,
        Intersect,
        ExcludeIntersections;

        private static MergePathsMode b(int i) {
            switch (i) {
                case 1:
                    return Merge;
                case 2:
                    return Add;
                case 3:
                    return Subtract;
                case 4:
                    return Intersect;
                case 5:
                    return ExcludeIntersections;
                default:
                    return Merge;
            }
        }
    }

    private MergePaths(String str, MergePathsMode mergePathsMode) {
        this.f2376a = str;
        this.f2377b = mergePathsMode;
    }

    public String a() {
        return this.f2376a;
    }

    /* access modifiers changed from: package-private */
    public MergePathsMode b() {
        return this.f2377b;
    }

    public y a(be beVar, q qVar) {
        if (beVar.a()) {
            return new bi(this);
        }
        Log.w("LOTTIE", "Animation contains merge paths but they are disabled.");
        return null;
    }

    public String toString() {
        return "MergePaths{mode=" + this.f2377b + '}';
    }

    static class a {
        static MergePaths a(JSONObject jSONObject) {
            return new MergePaths(jSONObject.optString("nm"), MergePathsMode.a(jSONObject.optInt("mm", 1)));
        }
    }
}
