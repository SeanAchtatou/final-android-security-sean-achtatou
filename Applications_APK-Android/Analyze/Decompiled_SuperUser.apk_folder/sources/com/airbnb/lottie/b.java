package com.airbnb.lottie;

import com.airbnb.lottie.m;
import com.airbnb.lottie.n;
import java.util.List;
import org.json.JSONObject;

/* compiled from: AnimatableFloatValue */
class b extends o<Float, Float> {
    private b() {
        super(Float.valueOf(0.0f));
    }

    private b(List<ba<Float>> list, Float f2) {
        super(list, f2);
    }

    /* renamed from: a */
    public bb<Float> b() {
        if (!e()) {
            return new cl(this.f2697b);
        }
        return new ah(this.f2696a);
    }

    /* renamed from: c */
    public Float d() {
        return (Float) this.f2697b;
    }

    /* renamed from: com.airbnb.lottie.b$b  reason: collision with other inner class name */
    /* compiled from: AnimatableFloatValue */
    private static class C0034b implements m.a<Float> {

        /* renamed from: a  reason: collision with root package name */
        static final C0034b f2510a = new C0034b();

        private C0034b() {
        }

        /* renamed from: a */
        public Float b(Object obj, float f2) {
            return Float.valueOf(az.a(obj) * f2);
        }
    }

    /* compiled from: AnimatableFloatValue */
    static final class a {
        static b a() {
            return new b();
        }

        static b a(JSONObject jSONObject, bd bdVar) {
            return a(jSONObject, bdVar, true);
        }

        static b a(JSONObject jSONObject, bd bdVar, boolean z) {
            float n = z ? bdVar.n() : 1.0f;
            if (jSONObject != null && jSONObject.has("x")) {
                bdVar.a("Lottie doesn't support expressions.");
            }
            n.a a2 = n.a(jSONObject, n, bdVar, C0034b.f2510a).a();
            return new b(a2.f2694a, (Float) a2.f2695b);
        }
    }
}
