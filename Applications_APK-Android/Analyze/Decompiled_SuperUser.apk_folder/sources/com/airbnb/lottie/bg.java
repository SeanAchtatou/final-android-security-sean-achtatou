package com.airbnb.lottie;

import android.graphics.Path;
import java.util.ArrayList;
import java.util.List;

/* compiled from: MaskKeyframeAnimation */
class bg {

    /* renamed from: a  reason: collision with root package name */
    private final List<p<?, Path>> f2549a;

    /* renamed from: b  reason: collision with root package name */
    private final List<bb<Integer>> f2550b;

    /* renamed from: c  reason: collision with root package name */
    private final List<Mask> f2551c;

    bg(List<Mask> list) {
        this.f2551c = list;
        this.f2549a = new ArrayList(list.size());
        this.f2550b = new ArrayList(list.size());
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                this.f2549a.add(list.get(i2).b().b());
                this.f2550b.add(list.get(i2).c().b());
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public List<Mask> a() {
        return this.f2551c;
    }

    /* access modifiers changed from: package-private */
    public List<p<?, Path>> b() {
        return this.f2549a;
    }

    /* access modifiers changed from: package-private */
    public List<bb<Integer>> c() {
        return this.f2550b;
    }
}
