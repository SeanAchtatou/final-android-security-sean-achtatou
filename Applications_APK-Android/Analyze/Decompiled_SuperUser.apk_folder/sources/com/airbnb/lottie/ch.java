package com.airbnb.lottie;

import com.airbnb.lottie.h;
import org.json.JSONObject;

/* compiled from: ShapePath */
class ch implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2632a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2633b;

    /* renamed from: c  reason: collision with root package name */
    private final h f2634c;

    private ch(String str, int i, h hVar) {
        this.f2632a = str;
        this.f2633b = i;
        this.f2634c = hVar;
    }

    public String a() {
        return this.f2632a;
    }

    /* access modifiers changed from: package-private */
    public h b() {
        return this.f2634c;
    }

    public y a(be beVar, q qVar) {
        return new cb(beVar, qVar, this);
    }

    public String toString() {
        return "ShapePath{name=" + this.f2632a + ", index=" + this.f2633b + ", hasAnimation=" + this.f2634c.e() + '}';
    }

    /* compiled from: ShapePath */
    static class a {
        static ch a(JSONObject jSONObject, bd bdVar) {
            return new ch(jSONObject.optString("nm"), jSONObject.optInt("ind"), h.a.a(jSONObject.optJSONObject("ks"), bdVar));
        }
    }
}
