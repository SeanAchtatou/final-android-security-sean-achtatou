package com.airbnb.lottie;

import java.util.List;

/* compiled from: ScaleKeyframeAnimation */
class bz extends bb<ca> {
    bz(List<ba<ca>> list) {
        super(list);
    }

    /* renamed from: b */
    public ca a(ba<ca> baVar, float f2) {
        if (baVar.f2512a == null || baVar.f2513b == null) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        ca caVar = (ca) baVar.f2512a;
        ca caVar2 = (ca) baVar.f2513b;
        return new ca(bj.a(caVar.a(), caVar2.a(), f2), bj.a(caVar.b(), caVar2.b(), f2));
    }
}
