package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import com.airbnb.lottie.p;
import java.util.ArrayList;
import java.util.List;

/* compiled from: FillContent */
class ag implements ad, p.a {

    /* renamed from: a  reason: collision with root package name */
    private final Path f2442a = new Path();

    /* renamed from: b  reason: collision with root package name */
    private final Paint f2443b = new Paint(1);

    /* renamed from: c  reason: collision with root package name */
    private final String f2444c;

    /* renamed from: d  reason: collision with root package name */
    private final List<bn> f2445d = new ArrayList();

    /* renamed from: e  reason: collision with root package name */
    private final bb<Integer> f2446e;

    /* renamed from: f  reason: collision with root package name */
    private final bb<Integer> f2447f;

    /* renamed from: g  reason: collision with root package name */
    private final be f2448g;

    ag(be beVar, q qVar, cd cdVar) {
        this.f2444c = cdVar.a();
        this.f2448g = beVar;
        if (cdVar.b() == null || cdVar.c() == null) {
            this.f2446e = null;
            this.f2447f = null;
            return;
        }
        this.f2442a.setFillType(cdVar.d());
        this.f2446e = cdVar.b().b();
        this.f2446e.a(this);
        qVar.a(this.f2446e);
        this.f2447f = cdVar.c().b();
        this.f2447f.a(this);
        qVar.a(this.f2447f);
    }

    public void a() {
        this.f2448g.invalidateSelf();
    }

    public void a(List<y> list, List<y> list2) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list2.size()) {
                y yVar = list2.get(i2);
                if (yVar instanceof bn) {
                    this.f2445d.add((bn) yVar);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public String e() {
        return this.f2444c;
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
        this.f2443b.setColorFilter(colorFilter);
    }

    public void a(Canvas canvas, Matrix matrix, int i) {
        bc.a("FillContent#draw");
        this.f2443b.setColor(((Integer) this.f2446e.b()).intValue());
        Paint paint = this.f2443b;
        paint.setAlpha((int) (((((float) ((Integer) this.f2447f.b()).intValue()) * (((float) i) / 255.0f)) / 100.0f) * 255.0f));
        this.f2442a.reset();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f2445d.size()) {
                this.f2442a.addPath(this.f2445d.get(i3).d(), matrix);
                i2 = i3 + 1;
            } else {
                canvas.drawPath(this.f2442a, this.f2443b);
                bc.b("FillContent#draw");
                return;
            }
        }
    }

    public void a(RectF rectF, Matrix matrix) {
        this.f2442a.reset();
        for (int i = 0; i < this.f2445d.size(); i++) {
            this.f2442a.addPath(this.f2445d.get(i).d(), matrix);
        }
        this.f2442a.computeBounds(rectF, false);
        rectF.set(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f);
    }
}
