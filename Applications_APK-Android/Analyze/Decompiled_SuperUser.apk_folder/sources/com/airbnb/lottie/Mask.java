package com.airbnb.lottie;

import com.airbnb.lottie.d;
import com.airbnb.lottie.h;
import org.json.JSONObject;

class Mask {

    /* renamed from: a  reason: collision with root package name */
    private final MaskMode f2368a;

    /* renamed from: b  reason: collision with root package name */
    private final h f2369b;

    /* renamed from: c  reason: collision with root package name */
    private final d f2370c;

    enum MaskMode {
        MaskModeAdd,
        MaskModeSubtract,
        MaskModeIntersect,
        MaskModeUnknown
    }

    private Mask(MaskMode maskMode, h hVar, d dVar) {
        this.f2368a = maskMode;
        this.f2369b = hVar;
        this.f2370c = dVar;
    }

    static class a {
        static Mask a(JSONObject jSONObject, bd bdVar) {
            MaskMode maskMode;
            String optString = jSONObject.optString("mode");
            char c2 = 65535;
            switch (optString.hashCode()) {
                case 97:
                    if (optString.equals("a")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 105:
                    if (optString.equals("i")) {
                        c2 = 2;
                        break;
                    }
                    break;
                case 115:
                    if (optString.equals("s")) {
                        c2 = 1;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    maskMode = MaskMode.MaskModeAdd;
                    break;
                case 1:
                    maskMode = MaskMode.MaskModeSubtract;
                    break;
                case 2:
                    maskMode = MaskMode.MaskModeIntersect;
                    break;
                default:
                    maskMode = MaskMode.MaskModeUnknown;
                    break;
            }
            return new Mask(maskMode, h.a.a(jSONObject.optJSONObject("pt"), bdVar), d.a.a(jSONObject.optJSONObject("o"), bdVar));
        }
    }

    /* access modifiers changed from: package-private */
    public MaskMode a() {
        return this.f2368a;
    }

    /* access modifiers changed from: package-private */
    public h b() {
        return this.f2369b;
    }

    /* access modifiers changed from: package-private */
    public d c() {
        return this.f2370c;
    }
}
