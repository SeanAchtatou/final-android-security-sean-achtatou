package com.airbnb.lottie;

import android.graphics.Path;
import android.graphics.PointF;

/* compiled from: MiscUtils */
class bj {
    static PointF a(PointF pointF, PointF pointF2) {
        return new PointF(pointF.x + pointF2.x, pointF.y + pointF2.y);
    }

    static void a(cc ccVar, Path path) {
        path.reset();
        PointF a2 = ccVar.a();
        path.moveTo(a2.x, a2.y);
        PointF pointF = new PointF(a2.x, a2.y);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= ccVar.c().size()) {
                break;
            }
            ab abVar = ccVar.c().get(i2);
            PointF a3 = abVar.a();
            PointF b2 = abVar.b();
            PointF c2 = abVar.c();
            if (!a3.equals(pointF) || !b2.equals(c2)) {
                path.cubicTo(a3.x, a3.y, b2.x, b2.y, c2.x, c2.y);
            } else {
                path.lineTo(c2.x, c2.y);
            }
            pointF.set(c2.x, c2.y);
            i = i2 + 1;
        }
        if (ccVar.b()) {
            path.close();
        }
    }

    static float a(float f2, float f3, float f4) {
        return ((f3 - f2) * f4) + f2;
    }

    static double a(double d2, double d3, double d4) {
        return ((d3 - d2) * d4) + d2;
    }

    static int a(int i, int i2, float f2) {
        return (int) (((float) i) + (((float) (i2 - i)) * f2));
    }

    static int a(float f2, float f3) {
        return a((int) f2, (int) f3);
    }

    static int a(int i, int i2) {
        return i - (b(i, i2) * i2);
    }

    private static int b(int i, int i2) {
        int i3 = i / i2;
        if ((i ^ i2) >= 0 || i3 * i2 == i) {
            return i3;
        }
        return i3 - 1;
    }

    static float b(float f2, float f3, float f4) {
        return Math.max(f3, Math.min(f4, f2));
    }
}
