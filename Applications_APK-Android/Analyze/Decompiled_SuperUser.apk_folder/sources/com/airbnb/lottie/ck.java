package com.airbnb.lottie;

import android.graphics.PointF;
import com.airbnb.lottie.p;
import java.util.Collections;

/* compiled from: SplitDimensionPathKeyframeAnimation */
class ck extends bb<PointF> {

    /* renamed from: b  reason: collision with root package name */
    private final PointF f2638b = new PointF();

    /* renamed from: c  reason: collision with root package name */
    private final bb<Float> f2639c;

    /* renamed from: d  reason: collision with root package name */
    private final bb<Float> f2640d;

    ck(bb<Float> bbVar, bb<Float> bbVar2) {
        super(Collections.emptyList());
        this.f2639c = bbVar;
        this.f2640d = bbVar2;
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        this.f2639c.a(f2);
        this.f2640d.a(f2);
        this.f2638b.set(((Float) this.f2639c.b()).floatValue(), ((Float) this.f2640d.b()).floatValue());
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f2698a.size()) {
                ((p.a) this.f2698a.get(i2)).a();
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: d */
    public PointF b() {
        return a(null, 0.0f);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public PointF a(ba<PointF> baVar, float f2) {
        return this.f2638b;
    }
}
