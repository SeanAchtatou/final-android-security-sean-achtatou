package com.airbnb.lottie;

import android.graphics.PointF;
import com.airbnb.lottie.n;
import java.util.List;
import org.json.JSONObject;

/* compiled from: AnimatablePointValue */
class f extends o<PointF, PointF> {
    private f(List<ba<PointF>> list, PointF pointF) {
        super(list, pointF);
    }

    /* renamed from: a */
    public bb<PointF> b() {
        if (!e()) {
            return new cl(this.f2697b);
        }
        return new bs(this.f2696a);
    }

    /* compiled from: AnimatablePointValue */
    static final class a {
        static f a(JSONObject jSONObject, bd bdVar) {
            n.a a2 = n.a(jSONObject, bdVar.n(), bdVar, br.f2573a).a();
            return new f(a2.f2694a, (PointF) a2.f2695b);
        }
    }
}
