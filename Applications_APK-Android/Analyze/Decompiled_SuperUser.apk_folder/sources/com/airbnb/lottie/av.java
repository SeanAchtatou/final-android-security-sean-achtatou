package com.airbnb.lottie;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ImageAssetManager */
class av {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2499a;

    /* renamed from: b  reason: collision with root package name */
    private String f2500b;

    /* renamed from: c  reason: collision with root package name */
    private au f2501c;

    /* renamed from: d  reason: collision with root package name */
    private final Map<String, bf> f2502d;

    /* renamed from: e  reason: collision with root package name */
    private final Map<String, Bitmap> f2503e = new HashMap();

    av(Drawable.Callback callback, String str, au auVar, Map<String, bf> map) {
        this.f2500b = str;
        if (!TextUtils.isEmpty(str) && this.f2500b.charAt(this.f2500b.length() - 1) != '/') {
            this.f2500b += '/';
        }
        if (!(callback instanceof View)) {
            Log.w("LOTTIE", "LottieDrawable must be inside of a view for images to work.");
            this.f2502d = new HashMap();
            this.f2499a = null;
            return;
        }
        this.f2499a = ((View) callback).getContext();
        this.f2502d = map;
        a(auVar);
    }

    /* access modifiers changed from: package-private */
    public void a(au auVar) {
        this.f2501c = auVar;
    }

    /* access modifiers changed from: package-private */
    public Bitmap a(String str) {
        Bitmap bitmap = this.f2503e.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        bf bfVar = this.f2502d.get(str);
        if (bfVar == null) {
            return null;
        }
        if (this.f2501c != null) {
            Bitmap a2 = this.f2501c.a(bfVar);
            if (a2 == null) {
                return a2;
            }
            this.f2503e.put(str, a2);
            return a2;
        }
        try {
            if (TextUtils.isEmpty(this.f2500b)) {
                throw new IllegalStateException("You must set an images folder before loading an image. Set it with LottieComposition#setImagesFolder or LottieDrawable#setImagesFolder");
            }
            InputStream open = this.f2499a.getAssets().open(this.f2500b + bfVar.b());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = true;
            options.inDensity = 160;
            Bitmap decodeStream = BitmapFactory.decodeStream(open, null, options);
            this.f2503e.put(str, decodeStream);
            return decodeStream;
        } catch (IOException e2) {
            Log.w("LOTTIE", "Unable to open asset.", e2);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Iterator<Map.Entry<String, Bitmap>> it = this.f2503e.entrySet().iterator();
        while (it.hasNext()) {
            ((Bitmap) it.next().getValue()).recycle();
            it.remove();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(Context context) {
        return (context == null && this.f2499a == null) || (context != null && this.f2499a.equals(context));
    }
}
