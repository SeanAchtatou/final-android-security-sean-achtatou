package com.airbnb.lottie;

import android.graphics.Path;
import android.graphics.PointF;
import com.airbnb.lottie.PolystarShape;
import com.airbnb.lottie.ShapeTrimPath;
import com.airbnb.lottie.p;
import java.util.List;

/* compiled from: PolystarContent */
class bt implements bn, p.a {

    /* renamed from: a  reason: collision with root package name */
    private final Path f2575a = new Path();

    /* renamed from: b  reason: collision with root package name */
    private final String f2576b;

    /* renamed from: c  reason: collision with root package name */
    private final be f2577c;

    /* renamed from: d  reason: collision with root package name */
    private final PolystarShape.Type f2578d;

    /* renamed from: e  reason: collision with root package name */
    private final p<?, Float> f2579e;

    /* renamed from: f  reason: collision with root package name */
    private final p<?, PointF> f2580f;

    /* renamed from: g  reason: collision with root package name */
    private final p<?, Float> f2581g;

    /* renamed from: h  reason: collision with root package name */
    private final p<?, Float> f2582h;
    private final p<?, Float> i;
    private final p<?, Float> j;
    private final p<?, Float> k;
    private cr l;
    private boolean m;

    bt(be beVar, q qVar, PolystarShape polystarShape) {
        this.f2577c = beVar;
        this.f2576b = polystarShape.a();
        this.f2578d = polystarShape.b();
        this.f2579e = polystarShape.c().b();
        this.f2580f = polystarShape.d().b();
        this.f2581g = polystarShape.e().b();
        this.i = polystarShape.g().b();
        this.k = polystarShape.i().b();
        if (this.f2578d == PolystarShape.Type.Star) {
            this.f2582h = polystarShape.f().b();
            this.j = polystarShape.h().b();
        } else {
            this.f2582h = null;
            this.j = null;
        }
        qVar.a(this.f2579e);
        qVar.a(this.f2580f);
        qVar.a(this.f2581g);
        qVar.a(this.i);
        qVar.a(this.k);
        if (this.f2578d == PolystarShape.Type.Star) {
            qVar.a(this.f2582h);
            qVar.a(this.j);
        }
        this.f2579e.a(this);
        this.f2580f.a(this);
        this.f2581g.a(this);
        this.i.a(this);
        this.k.a(this);
        if (this.f2578d == PolystarShape.Type.Star) {
            this.i.a(this);
            this.k.a(this);
        }
    }

    public void a() {
        b();
    }

    private void b() {
        this.m = false;
        this.f2577c.invalidateSelf();
    }

    public void a(List<y> list, List<y> list2) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < list.size()) {
                y yVar = list.get(i3);
                if ((yVar instanceof cr) && ((cr) yVar).b() == ShapeTrimPath.Type.Simultaneously) {
                    this.l = (cr) yVar;
                    this.l.a(this);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public Path d() {
        if (this.m) {
            return this.f2575a;
        }
        this.f2575a.reset();
        switch (this.f2578d) {
            case Star:
                c();
                break;
            case Polygon:
                f();
                break;
        }
        this.f2575a.close();
        cs.a(this.f2575a, this.l);
        this.m = true;
        return this.f2575a;
    }

    public String e() {
        return this.f2576b;
    }

    private void c() {
        double floatValue;
        double d2;
        float f2;
        double d3;
        float f3;
        float f4;
        float f5;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        float f11;
        float f12;
        float f13;
        boolean z;
        float floatValue2 = this.f2579e.b().floatValue();
        if (this.f2581g == null) {
            floatValue = 0.0d;
        } else {
            floatValue = (double) this.f2581g.b().floatValue();
        }
        double radians = Math.toRadians(floatValue - 90.0d);
        float f14 = (float) (6.283185307179586d / ((double) floatValue2));
        float f15 = f14 / 2.0f;
        float f16 = floatValue2 - ((float) ((int) floatValue2));
        if (f16 != 0.0f) {
            d2 = radians + ((double) ((1.0f - f16) * f15));
        } else {
            d2 = radians;
        }
        float floatValue3 = this.i.b().floatValue();
        float floatValue4 = this.f2582h.b().floatValue();
        if (this.j != null) {
            f2 = this.j.b().floatValue() / 100.0f;
        } else {
            f2 = 0.0f;
        }
        float f17 = 0.0f;
        if (this.k != null) {
            f17 = this.k.b().floatValue() / 100.0f;
        }
        if (f16 != 0.0f) {
            float f18 = ((floatValue3 - floatValue4) * f16) + floatValue4;
            float cos = (float) (((double) f18) * Math.cos(d2));
            float sin = (float) (((double) f18) * Math.sin(d2));
            this.f2575a.moveTo(cos, sin);
            d3 = d2 + ((double) ((f14 * f16) / 2.0f));
            f3 = f18;
            f4 = sin;
            f5 = cos;
        } else {
            float cos2 = (float) (((double) floatValue3) * Math.cos(d2));
            float sin2 = (float) (((double) floatValue3) * Math.sin(d2));
            this.f2575a.moveTo(cos2, sin2);
            d3 = d2 + ((double) f15);
            f3 = 0.0f;
            f4 = sin2;
            f5 = cos2;
        }
        double ceil = Math.ceil((double) floatValue2) * 2.0d;
        int i2 = 0;
        boolean z2 = false;
        float f19 = f4;
        float f20 = f5;
        while (true) {
            double d4 = d3;
            if (((double) i2) < ceil) {
                if (z2) {
                    f6 = floatValue3;
                } else {
                    f6 = floatValue4;
                }
                if (f3 == 0.0f || ((double) i2) != ceil - 2.0d) {
                    f7 = f15;
                } else {
                    f7 = (f14 * f16) / 2.0f;
                }
                if (f3 != 0.0f && ((double) i2) == ceil - 1.0d) {
                    f6 = f3;
                }
                float cos3 = (float) (((double) f6) * Math.cos(d4));
                float sin3 = (float) (((double) f6) * Math.sin(d4));
                if (f2 == 0.0f && f17 == 0.0f) {
                    this.f2575a.lineTo(cos3, sin3);
                } else {
                    float atan2 = (float) (Math.atan2((double) f19, (double) f20) - 1.5707963267948966d);
                    float cos4 = (float) Math.cos((double) atan2);
                    float sin4 = (float) Math.sin((double) atan2);
                    float atan22 = (float) (Math.atan2((double) sin3, (double) cos3) - 1.5707963267948966d);
                    float cos5 = (float) Math.cos((double) atan22);
                    float sin5 = (float) Math.sin((double) atan22);
                    float f21 = z2 ? f2 : f17;
                    if (z2) {
                        f8 = f17;
                    } else {
                        f8 = f2;
                    }
                    if (z2) {
                        f9 = floatValue4;
                    } else {
                        f9 = floatValue3;
                    }
                    if (z2) {
                        f10 = floatValue3;
                    } else {
                        f10 = floatValue4;
                    }
                    float f22 = cos4 * f9 * f21 * 0.47829f;
                    float f23 = f9 * f21 * 0.47829f * sin4;
                    float f24 = f10 * f8 * 0.47829f * cos5;
                    float f25 = f10 * f8 * 0.47829f * sin5;
                    if (f16 != 0.0f) {
                        if (i2 == 0) {
                            f23 *= f16;
                            f11 = f25;
                            float f26 = f24;
                            f12 = f22 * f16;
                            f13 = f26;
                        } else if (((double) i2) == ceil - 1.0d) {
                            f11 = f25 * f16;
                            float f27 = f24 * f16;
                            f12 = f22;
                            f13 = f27;
                        }
                        this.f2575a.cubicTo(f20 - f12, f19 - f23, f13 + cos3, f11 + sin3, cos3, sin3);
                    }
                    f11 = f25;
                    float f28 = f24;
                    f12 = f22;
                    f13 = f28;
                    this.f2575a.cubicTo(f20 - f12, f19 - f23, f13 + cos3, f11 + sin3, cos3, sin3);
                }
                d3 = d4 + ((double) f7);
                if (!z2) {
                    z = true;
                } else {
                    z = false;
                }
                i2++;
                z2 = z;
                f19 = sin3;
                f20 = cos3;
            } else {
                PointF b2 = this.f2580f.b();
                this.f2575a.offset(b2.x, b2.y);
                this.f2575a.close();
                return;
            }
        }
    }

    private void f() {
        double floatValue;
        int floor = (int) Math.floor((double) this.f2579e.b().floatValue());
        if (this.f2581g == null) {
            floatValue = 0.0d;
        } else {
            floatValue = (double) this.f2581g.b().floatValue();
        }
        double radians = Math.toRadians(floatValue - 90.0d);
        float f2 = (float) (6.283185307179586d / ((double) floor));
        float floatValue2 = this.k.b().floatValue() / 100.0f;
        float floatValue3 = this.i.b().floatValue();
        float cos = (float) (((double) floatValue3) * Math.cos(radians));
        float sin = (float) (((double) floatValue3) * Math.sin(radians));
        this.f2575a.moveTo(cos, sin);
        double d2 = radians + ((double) f2);
        double ceil = Math.ceil((double) floor);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            float f3 = cos;
            double d3 = d2;
            float f4 = sin;
            if (((double) i3) < ceil) {
                cos = (float) (((double) floatValue3) * Math.cos(d3));
                sin = (float) (((double) floatValue3) * Math.sin(d3));
                if (floatValue2 != 0.0f) {
                    float atan2 = (float) (Math.atan2((double) f4, (double) f3) - 1.5707963267948966d);
                    float cos2 = (float) Math.cos((double) atan2);
                    float sin2 = (float) Math.sin((double) atan2);
                    float atan22 = (float) (Math.atan2((double) sin, (double) cos) - 1.5707963267948966d);
                    this.f2575a.cubicTo(f3 - (cos2 * ((floatValue3 * floatValue2) * 0.25f)), f4 - (((floatValue3 * floatValue2) * 0.25f) * sin2), cos + (((float) Math.cos((double) atan22)) * floatValue3 * floatValue2 * 0.25f), (((float) Math.sin((double) atan22)) * floatValue3 * floatValue2 * 0.25f) + sin, cos, sin);
                } else {
                    this.f2575a.lineTo(cos, sin);
                }
                d2 = d3 + ((double) f2);
                i2 = i3 + 1;
            } else {
                PointF b2 = this.f2580f.b();
                this.f2575a.offset(b2.x, b2.y);
                this.f2575a.close();
                return;
            }
        }
    }
}
