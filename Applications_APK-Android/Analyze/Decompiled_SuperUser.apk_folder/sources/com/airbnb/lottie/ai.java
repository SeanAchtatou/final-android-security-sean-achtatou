package com.airbnb.lottie;

import org.json.JSONObject;

/* compiled from: Font */
class ai {

    /* renamed from: a  reason: collision with root package name */
    private final String f2449a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2450b;

    /* renamed from: c  reason: collision with root package name */
    private final String f2451c;

    /* renamed from: d  reason: collision with root package name */
    private final float f2452d;

    ai(String str, String str2, String str3, float f2) {
        this.f2449a = str;
        this.f2450b = str2;
        this.f2451c = str3;
        this.f2452d = f2;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f2449a;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.f2450b;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.f2451c;
    }

    /* compiled from: Font */
    static class a {
        static ai a(JSONObject jSONObject) {
            return new ai(jSONObject.optString("fFamily"), jSONObject.optString("fName"), jSONObject.optString("fStyle"), (float) jSONObject.optDouble("ascent"));
        }
    }
}
