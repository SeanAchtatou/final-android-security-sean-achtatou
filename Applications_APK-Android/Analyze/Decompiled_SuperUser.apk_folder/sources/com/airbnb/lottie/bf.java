package com.airbnb.lottie;

import org.json.JSONObject;

/* compiled from: LottieImageAsset */
public class bf {

    /* renamed from: a  reason: collision with root package name */
    private final int f2545a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2546b;

    /* renamed from: c  reason: collision with root package name */
    private final String f2547c;

    /* renamed from: d  reason: collision with root package name */
    private final String f2548d;

    private bf(int i, int i2, String str, String str2) {
        this.f2545a = i;
        this.f2546b = i2;
        this.f2547c = str;
        this.f2548d = str2;
    }

    /* compiled from: LottieImageAsset */
    static class a {
        static bf a(JSONObject jSONObject) {
            return new bf(jSONObject.optInt("w"), jSONObject.optInt("h"), jSONObject.optString("id"), jSONObject.optString("p"));
        }
    }

    public String a() {
        return this.f2547c;
    }

    public String b() {
        return this.f2548d;
    }
}
