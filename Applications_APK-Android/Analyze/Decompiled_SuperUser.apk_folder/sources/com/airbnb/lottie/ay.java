package com.airbnb.lottie;

import android.content.res.Resources;
import com.airbnb.lottie.bd;
import org.json.JSONObject;

/* compiled from: JsonCompositionLoader */
final class ay extends x<JSONObject> {

    /* renamed from: a  reason: collision with root package name */
    private final Resources f2508a;

    /* renamed from: b  reason: collision with root package name */
    private final bm f2509b;

    ay(Resources resources, bm bmVar) {
        this.f2508a = resources;
        this.f2509b = bmVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public bd doInBackground(JSONObject... jSONObjectArr) {
        return bd.a.a(this.f2508a, jSONObjectArr[0]);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(bd bdVar) {
        this.f2509b.a(bdVar);
    }
}
