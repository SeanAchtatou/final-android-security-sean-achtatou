package com.airbnb.lottie;

import com.airbnb.lottie.n;
import java.util.List;
import org.json.JSONObject;

/* compiled from: AnimatableColorValue */
class a extends o<Integer, Integer> {
    private a(List<ba<Integer>> list, Integer num) {
        super(list, num);
    }

    /* renamed from: a */
    public bb<Integer> b() {
        if (!e()) {
            return new cl(this.f2697b);
        }
        return new v(this.f2696a);
    }

    public String toString() {
        return "AnimatableColorValue{initialValue=" + this.f2697b + '}';
    }

    /* renamed from: com.airbnb.lottie.a$a  reason: collision with other inner class name */
    /* compiled from: AnimatableColorValue */
    static final class C0033a {
        static a a(JSONObject jSONObject, bd bdVar) {
            n.a a2 = n.a(jSONObject, 1.0f, bdVar, u.f2728a).a();
            return new a(a2.f2694a, (Integer) a2.f2695b);
        }
    }
}
