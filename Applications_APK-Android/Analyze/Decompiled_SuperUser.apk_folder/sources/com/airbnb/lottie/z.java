package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import com.airbnb.lottie.p;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ContentGroup */
class z implements ad, bn, p.a {

    /* renamed from: a  reason: collision with root package name */
    private final Matrix f2734a;

    /* renamed from: b  reason: collision with root package name */
    private final Path f2735b;

    /* renamed from: c  reason: collision with root package name */
    private final RectF f2736c;

    /* renamed from: d  reason: collision with root package name */
    private final String f2737d;

    /* renamed from: e  reason: collision with root package name */
    private final List<y> f2738e;

    /* renamed from: f  reason: collision with root package name */
    private final be f2739f;

    /* renamed from: g  reason: collision with root package name */
    private List<bn> f2740g;

    /* renamed from: h  reason: collision with root package name */
    private cq f2741h;

    private static List<y> a(be beVar, q qVar, List<aa> list) {
        ArrayList arrayList = new ArrayList(list.size());
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return arrayList;
            }
            y a2 = list.get(i2).a(beVar, qVar);
            if (a2 != null) {
                arrayList.add(a2);
            }
            i = i2 + 1;
        }
    }

    static l a(List<aa> list) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return null;
            }
            aa aaVar = list.get(i2);
            if (aaVar instanceof l) {
                return (l) aaVar;
            }
            i = i2 + 1;
        }
    }

    z(be beVar, q qVar, ce ceVar) {
        this(beVar, qVar, ceVar.a(), a(beVar, qVar, ceVar.b()), a(ceVar.b()));
    }

    z(be beVar, q qVar, String str, List<y> list, l lVar) {
        this.f2734a = new Matrix();
        this.f2735b = new Path();
        this.f2736c = new RectF();
        this.f2737d = str;
        this.f2739f = beVar;
        this.f2738e = list;
        if (lVar != null) {
            this.f2741h = lVar.h();
            this.f2741h.a(qVar);
            this.f2741h.a(this);
        }
        ArrayList arrayList = new ArrayList();
        for (int size = list.size() - 1; size >= 0; size--) {
            y yVar = list.get(size);
            if (yVar instanceof at) {
                arrayList.add((at) yVar);
            }
        }
        for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
            ((at) arrayList.get(size2)).a(list.listIterator(list.size()));
        }
    }

    public void a() {
        this.f2739f.invalidateSelf();
    }

    public String e() {
        return this.f2737d;
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f2738e.size()) {
                y yVar = this.f2738e.get(i2);
                if (yVar instanceof ad) {
                    ad adVar = (ad) yVar;
                    if (str2 == null || str2.equals(yVar.e())) {
                        adVar.a(str, (String) null, colorFilter);
                    } else {
                        adVar.a(str, str2, colorFilter);
                    }
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void a(List<y> list, List<y> list2) {
        ArrayList arrayList = new ArrayList(list.size() + this.f2738e.size());
        arrayList.addAll(list);
        for (int size = this.f2738e.size() - 1; size >= 0; size--) {
            y yVar = this.f2738e.get(size);
            yVar.a(arrayList, this.f2738e.subList(0, size));
            arrayList.add(yVar);
        }
    }

    /* access modifiers changed from: package-private */
    public List<bn> b() {
        if (this.f2740g == null) {
            this.f2740g = new ArrayList();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f2738e.size()) {
                    break;
                }
                y yVar = this.f2738e.get(i2);
                if (yVar instanceof bn) {
                    this.f2740g.add((bn) yVar);
                }
                i = i2 + 1;
            }
        }
        return this.f2740g;
    }

    /* access modifiers changed from: package-private */
    public Matrix c() {
        if (this.f2741h != null) {
            return this.f2741h.d();
        }
        this.f2734a.reset();
        return this.f2734a;
    }

    public Path d() {
        this.f2734a.reset();
        if (this.f2741h != null) {
            this.f2734a.set(this.f2741h.d());
        }
        this.f2735b.reset();
        for (int size = this.f2738e.size() - 1; size >= 0; size--) {
            y yVar = this.f2738e.get(size);
            if (yVar instanceof bn) {
                this.f2735b.addPath(((bn) yVar).d(), this.f2734a);
            }
        }
        return this.f2735b;
    }

    public void a(Canvas canvas, Matrix matrix, int i) {
        this.f2734a.set(matrix);
        if (this.f2741h != null) {
            this.f2734a.preConcat(this.f2741h.d());
            i = (int) ((((((float) this.f2741h.a().b().intValue()) / 100.0f) * ((float) i)) / 255.0f) * 255.0f);
        }
        for (int size = this.f2738e.size() - 1; size >= 0; size--) {
            y yVar = this.f2738e.get(size);
            if (yVar instanceof ad) {
                ((ad) yVar).a(canvas, this.f2734a, i);
            }
        }
    }

    public void a(RectF rectF, Matrix matrix) {
        this.f2734a.set(matrix);
        if (this.f2741h != null) {
            this.f2734a.preConcat(this.f2741h.d());
        }
        this.f2736c.set(0.0f, 0.0f, 0.0f, 0.0f);
        for (int size = this.f2738e.size() - 1; size >= 0; size--) {
            y yVar = this.f2738e.get(size);
            if (yVar instanceof ad) {
                ((ad) yVar).a(this.f2736c, this.f2734a);
                if (rectF.isEmpty()) {
                    rectF.set(this.f2736c);
                } else {
                    rectF.set(Math.min(rectF.left, this.f2736c.left), Math.min(rectF.top, this.f2736c.top), Math.max(rectF.right, this.f2736c.right), Math.max(rectF.bottom, this.f2736c.bottom));
                }
            }
        }
    }
}
