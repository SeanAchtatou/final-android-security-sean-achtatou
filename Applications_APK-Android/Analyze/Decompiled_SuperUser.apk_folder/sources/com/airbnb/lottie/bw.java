package com.airbnb.lottie;

import android.graphics.PointF;
import com.airbnb.lottie.b;
import com.airbnb.lottie.f;
import org.json.JSONObject;

/* compiled from: RectangleShape */
class bw implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2592a;

    /* renamed from: b  reason: collision with root package name */
    private final m<PointF> f2593b;

    /* renamed from: c  reason: collision with root package name */
    private final f f2594c;

    /* renamed from: d  reason: collision with root package name */
    private final b f2595d;

    private bw(String str, m<PointF> mVar, f fVar, b bVar) {
        this.f2592a = str;
        this.f2593b = mVar;
        this.f2594c = fVar;
        this.f2595d = bVar;
    }

    /* compiled from: RectangleShape */
    static class a {
        static bw a(JSONObject jSONObject, bd bdVar) {
            return new bw(jSONObject.optString("nm"), e.a(jSONObject.optJSONObject("p"), bdVar), f.a.a(jSONObject.optJSONObject("s"), bdVar), b.a.a(jSONObject.optJSONObject("r"), bdVar));
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f2592a;
    }

    /* access modifiers changed from: package-private */
    public b b() {
        return this.f2595d;
    }

    /* access modifiers changed from: package-private */
    public f c() {
        return this.f2594c;
    }

    /* access modifiers changed from: package-private */
    public m<PointF> d() {
        return this.f2593b;
    }

    public y a(be beVar, q qVar) {
        return new bv(beVar, qVar, this);
    }

    public String toString() {
        return "RectangleShape{cornerRadius=" + this.f2595d.d() + ", position=" + this.f2593b + ", size=" + this.f2594c + '}';
    }
}
