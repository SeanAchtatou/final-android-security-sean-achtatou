package com.airbnb.lottie;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* compiled from: BaseAnimatableValue */
abstract class o<V, O> implements m<O> {

    /* renamed from: a  reason: collision with root package name */
    final List<ba<V>> f2696a;

    /* renamed from: b  reason: collision with root package name */
    final V f2697b;

    o(V v) {
        this(Collections.emptyList(), v);
    }

    o(List<ba<V>> list, V v) {
        this.f2696a = list;
        this.f2697b = v;
    }

    /* access modifiers changed from: package-private */
    public O a(Object obj) {
        return obj;
    }

    public boolean e() {
        return !this.f2696a.isEmpty();
    }

    public O d() {
        return a(this.f2697b);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("parseInitialValue=").append((Object) this.f2697b);
        if (!this.f2696a.isEmpty()) {
            sb.append(", values=").append(Arrays.toString(this.f2696a.toArray()));
        }
        return sb.toString();
    }
}
