package com.airbnb.lottie;

import android.graphics.Color;
import com.lody.virtual.os.VUserInfo;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: DocumentData */
class ac {

    /* renamed from: a  reason: collision with root package name */
    String f2425a;

    /* renamed from: b  reason: collision with root package name */
    String f2426b;

    /* renamed from: c  reason: collision with root package name */
    int f2427c;

    /* renamed from: d  reason: collision with root package name */
    int f2428d;

    /* renamed from: e  reason: collision with root package name */
    int f2429e;

    /* renamed from: f  reason: collision with root package name */
    double f2430f;

    /* renamed from: g  reason: collision with root package name */
    int f2431g;

    /* renamed from: h  reason: collision with root package name */
    int f2432h;
    int i;
    boolean j;

    ac(String str, String str2, int i2, int i3, int i4, double d2, int i5, int i6, int i7, boolean z) {
        this.f2425a = str;
        this.f2426b = str2;
        this.f2427c = i2;
        this.f2428d = i3;
        this.f2429e = i4;
        this.f2430f = d2;
        this.f2431g = i5;
        this.f2432h = i6;
        this.i = i7;
        this.j = z;
    }

    /* compiled from: DocumentData */
    static final class a {
        static ac a(JSONObject jSONObject) {
            String optString = jSONObject.optString("t");
            String optString2 = jSONObject.optString("f");
            int optInt = jSONObject.optInt("s");
            int optInt2 = jSONObject.optInt("j");
            int optInt3 = jSONObject.optInt("tr");
            double optDouble = jSONObject.optDouble("lh");
            JSONArray optJSONArray = jSONObject.optJSONArray("fc");
            int argb = Color.argb((int) VUserInfo.FLAG_MASK_USER_TYPE, (int) (optJSONArray.optDouble(0) * 255.0d), (int) (optJSONArray.optDouble(1) * 255.0d), (int) (optJSONArray.optDouble(2) * 255.0d));
            JSONArray optJSONArray2 = jSONObject.optJSONArray("sc");
            int i = 0;
            if (optJSONArray2 != null) {
                i = Color.argb((int) VUserInfo.FLAG_MASK_USER_TYPE, (int) (optJSONArray2.optDouble(0) * 255.0d), (int) (optJSONArray2.optDouble(1) * 255.0d), (int) (optJSONArray2.optDouble(2) * 255.0d));
            }
            return new ac(optString, optString2, optInt, optInt2, optInt3, optDouble, argb, i, jSONObject.optInt("sw"), jSONObject.optBoolean("of"));
        }
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.f2430f);
        return (((((((((((this.f2425a.hashCode() * 31) + this.f2426b.hashCode()) * 31) + this.f2427c) * 31) + this.f2428d) * 31) + this.f2429e) * 31) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)))) * 31) + this.f2431g;
    }
}
