package com.airbnb.lottie;

import android.graphics.Path;
import android.graphics.PointF;
import com.airbnb.lottie.ShapeTrimPath;
import com.airbnb.lottie.p;
import java.util.List;

/* compiled from: EllipseContent */
class ae implements bn, p.a {

    /* renamed from: a  reason: collision with root package name */
    private final Path f2433a = new Path();

    /* renamed from: b  reason: collision with root package name */
    private final String f2434b;

    /* renamed from: c  reason: collision with root package name */
    private final be f2435c;

    /* renamed from: d  reason: collision with root package name */
    private final p<?, PointF> f2436d;

    /* renamed from: e  reason: collision with root package name */
    private final p<?, PointF> f2437e;

    /* renamed from: f  reason: collision with root package name */
    private cr f2438f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f2439g;

    ae(be beVar, q qVar, t tVar) {
        this.f2434b = tVar.a();
        this.f2435c = beVar;
        this.f2436d = tVar.c().b();
        this.f2437e = tVar.b().b();
        qVar.a(this.f2436d);
        qVar.a(this.f2437e);
        this.f2436d.a(this);
        this.f2437e.a(this);
    }

    public void a() {
        b();
    }

    private void b() {
        this.f2439g = false;
        this.f2435c.invalidateSelf();
    }

    public void a(List<y> list, List<y> list2) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                y yVar = list.get(i2);
                if ((yVar instanceof cr) && ((cr) yVar).b() == ShapeTrimPath.Type.Simultaneously) {
                    this.f2438f = (cr) yVar;
                    this.f2438f.a(this);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public String e() {
        return this.f2434b;
    }

    public Path d() {
        if (this.f2439g) {
            return this.f2433a;
        }
        this.f2433a.reset();
        PointF b2 = this.f2436d.b();
        float f2 = b2.x / 2.0f;
        float f3 = b2.y / 2.0f;
        float f4 = f2 * 0.55228f;
        float f5 = f3 * 0.55228f;
        this.f2433a.reset();
        this.f2433a.moveTo(0.0f, -f3);
        this.f2433a.cubicTo(0.0f + f4, -f3, f2, 0.0f - f5, f2, 0.0f);
        this.f2433a.cubicTo(f2, 0.0f + f5, 0.0f + f4, f3, 0.0f, f3);
        this.f2433a.cubicTo(0.0f - f4, f3, -f2, 0.0f + f5, -f2, 0.0f);
        this.f2433a.cubicTo(-f2, 0.0f - f5, 0.0f - f4, -f3, 0.0f, -f3);
        PointF b3 = this.f2437e.b();
        this.f2433a.offset(b3.x, b3.y);
        this.f2433a.close();
        cs.a(this.f2433a, this.f2438f);
        this.f2439g = true;
        return this.f2433a;
    }
}
