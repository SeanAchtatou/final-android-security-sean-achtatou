package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.v4.util.e;
import java.util.List;

/* compiled from: GradientStrokeContent */
public class as extends r {

    /* renamed from: b  reason: collision with root package name */
    private final String f2492b;

    /* renamed from: c  reason: collision with root package name */
    private final e<LinearGradient> f2493c = new e<>();

    /* renamed from: d  reason: collision with root package name */
    private final e<RadialGradient> f2494d = new e<>();

    /* renamed from: e  reason: collision with root package name */
    private final RectF f2495e = new RectF();

    /* renamed from: f  reason: collision with root package name */
    private final GradientType f2496f;

    /* renamed from: g  reason: collision with root package name */
    private final int f2497g;

    /* renamed from: h  reason: collision with root package name */
    private final bb<an> f2498h;
    private final bb<PointF> i;
    private final bb<PointF> j;

    public /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public /* bridge */ /* synthetic */ void a(RectF rectF, Matrix matrix) {
        super.a(rectF, matrix);
    }

    public /* bridge */ /* synthetic */ void a(List list, List list2) {
        super.a(list, list2);
    }

    as(be beVar, q qVar, ar arVar) {
        super(beVar, qVar, arVar.h().a(), arVar.i().a(), arVar.d(), arVar.g(), arVar.j(), arVar.k());
        this.f2492b = arVar.a();
        this.f2496f = arVar.b();
        this.f2497g = (int) (beVar.n().c() / 32);
        this.f2498h = arVar.c().b();
        this.f2498h.a(this);
        qVar.a(this.f2498h);
        this.i = arVar.e().b();
        this.i.a(this);
        qVar.a(this.i);
        this.j = arVar.f().b();
        this.j.a(this);
        qVar.a(this.j);
    }

    public void a(Canvas canvas, Matrix matrix, int i2) {
        a(this.f2495e, matrix);
        if (this.f2496f == GradientType.Linear) {
            this.f2715a.setShader(b());
        } else {
            this.f2715a.setShader(c());
        }
        super.a(canvas, matrix, i2);
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
    }

    public String e() {
        return this.f2492b;
    }

    private LinearGradient b() {
        int d2 = d();
        LinearGradient a2 = this.f2493c.a((long) d2);
        if (a2 != null) {
            return a2;
        }
        PointF pointF = (PointF) this.i.b();
        PointF pointF2 = (PointF) this.j.b();
        an anVar = (an) this.f2498h.b();
        LinearGradient linearGradient = new LinearGradient((float) ((int) (this.f2495e.left + (this.f2495e.width() / 2.0f) + pointF.x)), (float) ((int) (pointF.y + this.f2495e.top + (this.f2495e.height() / 2.0f))), (float) ((int) (this.f2495e.left + (this.f2495e.width() / 2.0f) + pointF2.x)), (float) ((int) (this.f2495e.top + (this.f2495e.height() / 2.0f) + pointF2.y)), anVar.b(), anVar.a(), Shader.TileMode.CLAMP);
        this.f2493c.b((long) d2, linearGradient);
        return linearGradient;
    }

    private RadialGradient c() {
        int d2 = d();
        RadialGradient a2 = this.f2494d.a((long) d2);
        if (a2 != null) {
            return a2;
        }
        PointF pointF = (PointF) this.i.b();
        PointF pointF2 = (PointF) this.j.b();
        an anVar = (an) this.f2498h.b();
        int[] b2 = anVar.b();
        float[] a3 = anVar.a();
        int width = (int) (this.f2495e.left + (this.f2495e.width() / 2.0f) + pointF.x);
        int height = (int) (pointF.y + this.f2495e.top + (this.f2495e.height() / 2.0f));
        float f2 = pointF2.y;
        RadialGradient radialGradient = new RadialGradient((float) width, (float) height, (float) Math.hypot((double) (((int) ((this.f2495e.left + (this.f2495e.width() / 2.0f)) + pointF2.x)) - width), (double) (((int) (f2 + (this.f2495e.top + (this.f2495e.height() / 2.0f)))) - height)), b2, a3, Shader.TileMode.CLAMP);
        this.f2494d.b((long) d2, radialGradient);
        return radialGradient;
    }

    private int d() {
        int round = Math.round(this.i.c() * ((float) this.f2497g));
        int round2 = Math.round(this.j.c() * ((float) this.f2497g));
        int round3 = Math.round(this.f2498h.c() * ((float) this.f2497g));
        int i2 = 17;
        if (round != 0) {
            i2 = round * 527;
        }
        if (round2 != 0) {
            i2 = i2 * 31 * round2;
        }
        if (round3 != 0) {
            return i2 * 31 * round3;
        }
        return i2;
    }
}
