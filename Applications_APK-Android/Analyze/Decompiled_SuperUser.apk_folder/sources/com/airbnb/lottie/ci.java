package com.airbnb.lottie;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;

/* compiled from: SimpleColorFilter */
public class ci extends PorterDuffColorFilter {
    public ci(int i) {
        super(i, PorterDuff.Mode.SRC_ATOP);
    }
}
