package com.airbnb.lottie;

import android.graphics.Path;
import java.util.List;

/* compiled from: ShapeKeyframeAnimation */
class cf extends p<cc, Path> {

    /* renamed from: b  reason: collision with root package name */
    private final cc f2629b = new cc();

    /* renamed from: c  reason: collision with root package name */
    private final Path f2630c = new Path();

    cf(List<ba<cc>> list) {
        super(list);
    }

    /* renamed from: b */
    public Path a(ba<cc> baVar, float f2) {
        this.f2629b.a((cc) baVar.f2512a, (cc) baVar.f2513b, f2);
        bj.a(this.f2629b, this.f2630c);
        return this.f2630c;
    }
}
