package com.airbnb.lottie;

import android.graphics.PointF;
import com.airbnb.lottie.m;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: PointFFactory */
class br implements m.a<PointF> {

    /* renamed from: a  reason: collision with root package name */
    static final br f2573a = new br();

    private br() {
    }

    /* renamed from: a */
    public PointF b(Object obj, float f2) {
        if (obj instanceof JSONArray) {
            return az.a((JSONArray) obj, f2);
        }
        if (obj instanceof JSONObject) {
            return az.a((JSONObject) obj, f2);
        }
        throw new IllegalArgumentException("Unable to parse point from " + obj);
    }
}
