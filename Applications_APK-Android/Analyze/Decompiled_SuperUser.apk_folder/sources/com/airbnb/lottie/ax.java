package com.airbnb.lottie;

import java.util.List;

/* compiled from: IntegerKeyframeAnimation */
class ax extends bb<Integer> {
    ax(List<ba<Integer>> list) {
        super(list);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public Integer a(ba<Integer> baVar, float f2) {
        if (baVar.f2512a != null && baVar.f2513b != null) {
            return Integer.valueOf(bj.a(((Integer) baVar.f2512a).intValue(), ((Integer) baVar.f2513b).intValue(), f2));
        }
        throw new IllegalStateException("Missing values for keyframe.");
    }
}
