package com.airbnb.lottie;

import java.util.Map;

/* compiled from: TextDelegate */
public class cn {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, String> f2644a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f2645b;

    public String a(String str) {
        return str;
    }

    /* access modifiers changed from: package-private */
    public final String b(String str) {
        if (this.f2645b && this.f2644a.containsKey(str)) {
            return this.f2644a.get(str);
        }
        String a2 = a(str);
        if (!this.f2645b) {
            return a2;
        }
        this.f2644a.put(str, a2);
        return a2;
    }
}
