package com.airbnb.lottie;

import android.graphics.Paint;
import com.airbnb.lottie.a;
import com.airbnb.lottie.b;
import com.airbnb.lottie.d;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

class ShapeStroke implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2396a;

    /* renamed from: b  reason: collision with root package name */
    private final b f2397b;

    /* renamed from: c  reason: collision with root package name */
    private final List<b> f2398c;

    /* renamed from: d  reason: collision with root package name */
    private final a f2399d;

    /* renamed from: e  reason: collision with root package name */
    private final d f2400e;

    /* renamed from: f  reason: collision with root package name */
    private final b f2401f;

    /* renamed from: g  reason: collision with root package name */
    private final LineCapType f2402g;

    /* renamed from: h  reason: collision with root package name */
    private final LineJoinType f2403h;

    enum LineCapType {
        Butt,
        Round,
        Unknown;

        /* access modifiers changed from: package-private */
        public Paint.Cap a() {
            switch (this) {
                case Butt:
                    return Paint.Cap.BUTT;
                case Round:
                    return Paint.Cap.ROUND;
                default:
                    return Paint.Cap.SQUARE;
            }
        }
    }

    enum LineJoinType {
        Miter,
        Round,
        Bevel;

        /* access modifiers changed from: package-private */
        public Paint.Join a() {
            switch (this) {
                case Bevel:
                    return Paint.Join.BEVEL;
                case Miter:
                    return Paint.Join.MITER;
                case Round:
                    return Paint.Join.ROUND;
                default:
                    return null;
            }
        }
    }

    private ShapeStroke(String str, b bVar, List<b> list, a aVar, d dVar, b bVar2, LineCapType lineCapType, LineJoinType lineJoinType) {
        this.f2396a = str;
        this.f2397b = bVar;
        this.f2398c = list;
        this.f2399d = aVar;
        this.f2400e = dVar;
        this.f2401f = bVar2;
        this.f2402g = lineCapType;
        this.f2403h = lineJoinType;
    }

    public y a(be beVar, q qVar) {
        return new cm(beVar, qVar, this);
    }

    static class a {
        static ShapeStroke a(JSONObject jSONObject, bd bdVar) {
            String optString = jSONObject.optString("nm");
            ArrayList arrayList = new ArrayList();
            a a2 = a.C0033a.a(jSONObject.optJSONObject("c"), bdVar);
            b a3 = b.a.a(jSONObject.optJSONObject("w"), bdVar);
            d a4 = d.a.a(jSONObject.optJSONObject("o"), bdVar);
            LineCapType lineCapType = LineCapType.values()[jSONObject.optInt("lc") - 1];
            LineJoinType lineJoinType = LineJoinType.values()[jSONObject.optInt("lj") - 1];
            b bVar = null;
            if (jSONObject.has("d")) {
                JSONArray optJSONArray = jSONObject.optJSONArray("d");
                b bVar2 = null;
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                    String optString2 = optJSONObject.optString("n");
                    if (optString2.equals("o")) {
                        bVar2 = b.a.a(optJSONObject.optJSONObject("v"), bdVar);
                    } else if (optString2.equals("d") || optString2.equals("g")) {
                        arrayList.add(b.a.a(optJSONObject.optJSONObject("v"), bdVar));
                    }
                }
                if (arrayList.size() == 1) {
                    arrayList.add(arrayList.get(0));
                }
                bVar = bVar2;
            }
            return new ShapeStroke(optString, bVar, arrayList, a2, a4, a3, lineCapType, lineJoinType);
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f2396a;
    }

    /* access modifiers changed from: package-private */
    public a b() {
        return this.f2399d;
    }

    /* access modifiers changed from: package-private */
    public d c() {
        return this.f2400e;
    }

    /* access modifiers changed from: package-private */
    public b d() {
        return this.f2401f;
    }

    /* access modifiers changed from: package-private */
    public List<b> e() {
        return this.f2398c;
    }

    /* access modifiers changed from: package-private */
    public b f() {
        return this.f2397b;
    }

    /* access modifiers changed from: package-private */
    public LineCapType g() {
        return this.f2402g;
    }

    /* access modifiers changed from: package-private */
    public LineJoinType h() {
        return this.f2403h;
    }
}
