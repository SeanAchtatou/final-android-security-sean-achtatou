package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import com.airbnb.lottie.ShapeTrimPath;
import com.airbnb.lottie.p;
import java.util.ArrayList;
import java.util.List;

/* compiled from: BaseStrokeContent */
abstract class r implements ad, p.a {

    /* renamed from: a  reason: collision with root package name */
    final Paint f2715a = new Paint(1);

    /* renamed from: b  reason: collision with root package name */
    private final PathMeasure f2716b = new PathMeasure();

    /* renamed from: c  reason: collision with root package name */
    private final Path f2717c = new Path();

    /* renamed from: d  reason: collision with root package name */
    private final Path f2718d = new Path();

    /* renamed from: e  reason: collision with root package name */
    private final RectF f2719e = new RectF();

    /* renamed from: f  reason: collision with root package name */
    private final be f2720f;

    /* renamed from: g  reason: collision with root package name */
    private final List<a> f2721g = new ArrayList();

    /* renamed from: h  reason: collision with root package name */
    private final float[] f2722h;
    private final p<?, Float> i;
    private final p<?, Integer> j;
    private final List<p<?, Float>> k;
    private final p<?, Float> l;

    r(be beVar, q qVar, Paint.Cap cap, Paint.Join join, d dVar, b bVar, List<b> list, b bVar2) {
        this.f2720f = beVar;
        this.f2715a.setStyle(Paint.Style.STROKE);
        this.f2715a.setStrokeCap(cap);
        this.f2715a.setStrokeJoin(join);
        this.j = dVar.b();
        this.i = bVar.b();
        if (bVar2 == null) {
            this.l = null;
        } else {
            this.l = bVar2.b();
        }
        this.k = new ArrayList(list.size());
        this.f2722h = new float[list.size()];
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.k.add(list.get(i2).b());
        }
        qVar.a(this.j);
        qVar.a(this.i);
        for (int i3 = 0; i3 < this.k.size(); i3++) {
            qVar.a(this.k.get(i3));
        }
        if (this.l != null) {
            qVar.a(this.l);
        }
        this.j.a(this);
        this.i.a(this);
        for (int i4 = 0; i4 < list.size(); i4++) {
            this.k.get(i4).a(this);
        }
        if (this.l != null) {
            this.l.a(this);
        }
    }

    public void a() {
        this.f2720f.invalidateSelf();
    }

    public void a(List<y> list, List<y> list2) {
        a aVar;
        cr crVar;
        int size = list.size() - 1;
        cr crVar2 = null;
        while (size >= 0) {
            y yVar = list.get(size);
            if (!(yVar instanceof cr) || ((cr) yVar).b() != ShapeTrimPath.Type.Individually) {
                crVar = crVar2;
            } else {
                crVar = (cr) yVar;
            }
            size--;
            crVar2 = crVar;
        }
        if (crVar2 != null) {
            crVar2.a(this);
        }
        int size2 = list2.size() - 1;
        a aVar2 = null;
        while (size2 >= 0) {
            y yVar2 = list2.get(size2);
            if ((yVar2 instanceof cr) && ((cr) yVar2).b() == ShapeTrimPath.Type.Individually) {
                if (aVar2 != null) {
                    this.f2721g.add(aVar2);
                }
                a aVar3 = new a((cr) yVar2);
                ((cr) yVar2).a(this);
                aVar = aVar3;
            } else if (yVar2 instanceof bn) {
                if (aVar2 == null) {
                    aVar = new a(crVar2);
                } else {
                    aVar = aVar2;
                }
                aVar.f2723a.add((bn) yVar2);
            } else {
                aVar = aVar2;
            }
            size2--;
            aVar2 = aVar;
        }
        if (aVar2 != null) {
            this.f2721g.add(aVar2);
        }
    }

    public void a(Canvas canvas, Matrix matrix, int i2) {
        bc.a("StrokeContent#draw");
        this.f2715a.setAlpha((int) (((((float) this.j.b().intValue()) * (((float) i2) / 255.0f)) / 100.0f) * 255.0f));
        this.f2715a.setStrokeWidth(this.i.b().floatValue() * cs.a(matrix));
        if (this.f2715a.getStrokeWidth() <= 0.0f) {
            bc.b("StrokeContent#draw");
            return;
        }
        a(matrix);
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 < this.f2721g.size()) {
                a aVar = this.f2721g.get(i4);
                if (aVar.f2724b != null) {
                    a(canvas, aVar, matrix);
                } else {
                    bc.a("StrokeContent#buildPath");
                    this.f2717c.reset();
                    for (int size = aVar.f2723a.size() - 1; size >= 0; size--) {
                        this.f2717c.addPath(((bn) aVar.f2723a.get(size)).d(), matrix);
                    }
                    bc.b("StrokeContent#buildPath");
                    bc.a("StrokeContent#drawPath");
                    canvas.drawPath(this.f2717c, this.f2715a);
                    bc.b("StrokeContent#drawPath");
                }
                i3 = i4 + 1;
            } else {
                bc.b("StrokeContent#draw");
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void a(Canvas canvas, a aVar, Matrix matrix) {
        float f2;
        float f3;
        float f4;
        float f5;
        bc.a("StrokeContent#applyTrimPath");
        if (aVar.f2724b == null) {
            bc.b("StrokeContent#applyTrimPath");
            return;
        }
        this.f2717c.reset();
        for (int size = aVar.f2723a.size() - 1; size >= 0; size--) {
            this.f2717c.addPath(((bn) aVar.f2723a.get(size)).d(), matrix);
        }
        this.f2716b.setPath(this.f2717c, false);
        float length = this.f2716b.getLength();
        while (true) {
            f2 = length;
            if (!this.f2716b.nextContour()) {
                break;
            }
            length = this.f2716b.getLength() + f2;
        }
        float floatValue = (aVar.f2724b.f().b().floatValue() * f2) / 360.0f;
        float floatValue2 = ((aVar.f2724b.c().b().floatValue() * f2) / 100.0f) + floatValue;
        float floatValue3 = ((aVar.f2724b.d().b().floatValue() * f2) / 100.0f) + floatValue;
        int size2 = aVar.f2723a.size() - 1;
        float f6 = 0.0f;
        while (size2 >= 0) {
            this.f2718d.set(((bn) aVar.f2723a.get(size2)).d());
            this.f2718d.transform(matrix);
            this.f2716b.setPath(this.f2718d, false);
            float length2 = this.f2716b.getLength();
            if (floatValue3 > f2 && floatValue3 - f2 < f6 + length2 && f6 < floatValue3 - f2) {
                if (floatValue2 > f2) {
                    f5 = (floatValue2 - f2) / length2;
                } else {
                    f5 = 0.0f;
                }
                cs.a(this.f2718d, f5, Math.min((floatValue3 - f2) / length2, 1.0f), 0.0f);
                canvas.drawPath(this.f2718d, this.f2715a);
            } else if (f6 + length2 >= floatValue2 && f6 <= floatValue3) {
                if (f6 + length2 > floatValue3 || floatValue2 >= f6) {
                    if (floatValue2 < f6) {
                        f3 = 0.0f;
                    } else {
                        f3 = (floatValue2 - f6) / length2;
                    }
                    if (floatValue3 > f6 + length2) {
                        f4 = 1.0f;
                    } else {
                        f4 = (floatValue3 - f6) / length2;
                    }
                    cs.a(this.f2718d, f3, f4, 0.0f);
                    canvas.drawPath(this.f2718d, this.f2715a);
                } else {
                    canvas.drawPath(this.f2718d, this.f2715a);
                }
            }
            size2--;
            f6 += length2;
        }
        bc.b("StrokeContent#applyTrimPath");
    }

    public void a(RectF rectF, Matrix matrix) {
        bc.a("StrokeContent#getBounds");
        this.f2717c.reset();
        for (int i2 = 0; i2 < this.f2721g.size(); i2++) {
            a aVar = this.f2721g.get(i2);
            for (int i3 = 0; i3 < aVar.f2723a.size(); i3++) {
                this.f2717c.addPath(((bn) aVar.f2723a.get(i3)).d(), matrix);
            }
        }
        this.f2717c.computeBounds(this.f2719e, false);
        float floatValue = this.i.b().floatValue();
        this.f2719e.set(this.f2719e.left - (floatValue / 2.0f), this.f2719e.top - (floatValue / 2.0f), this.f2719e.right + (floatValue / 2.0f), (floatValue / 2.0f) + this.f2719e.bottom);
        rectF.set(this.f2719e);
        rectF.set(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f);
        bc.b("StrokeContent#getBounds");
    }

    private void a(Matrix matrix) {
        bc.a("StrokeContent#applyDashPattern");
        if (this.k.isEmpty()) {
            bc.b("StrokeContent#applyDashPattern");
            return;
        }
        float a2 = cs.a(matrix);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.k.size()) {
                break;
            }
            this.f2722h[i3] = ((Float) this.k.get(i3).b()).floatValue();
            if (i3 % 2 == 0) {
                if (this.f2722h[i3] < 1.0f) {
                    this.f2722h[i3] = 1.0f;
                }
            } else if (this.f2722h[i3] < 0.1f) {
                this.f2722h[i3] = 0.1f;
            }
            float[] fArr = this.f2722h;
            fArr[i3] = fArr[i3] * a2;
            i2 = i3 + 1;
        }
        this.f2715a.setPathEffect(new DashPathEffect(this.f2722h, this.l == null ? 0.0f : this.l.b().floatValue()));
        bc.b("StrokeContent#applyDashPattern");
    }

    /* compiled from: BaseStrokeContent */
    private static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final List<bn> f2723a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final cr f2724b;

        private a(cr crVar) {
            this.f2723a = new ArrayList();
            this.f2724b = crVar;
        }
    }
}
