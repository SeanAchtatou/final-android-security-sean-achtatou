package com.airbnb.lottie;

import android.graphics.Path;
import com.airbnb.lottie.c;
import com.airbnb.lottie.d;
import com.airbnb.lottie.f;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: GradientFill */
class ap implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final GradientType f2468a;

    /* renamed from: b  reason: collision with root package name */
    private final Path.FillType f2469b;

    /* renamed from: c  reason: collision with root package name */
    private final c f2470c;

    /* renamed from: d  reason: collision with root package name */
    private final d f2471d;

    /* renamed from: e  reason: collision with root package name */
    private final f f2472e;

    /* renamed from: f  reason: collision with root package name */
    private final f f2473f;

    /* renamed from: g  reason: collision with root package name */
    private final String f2474g;

    /* renamed from: h  reason: collision with root package name */
    private final b f2475h;
    private final b i;

    private ap(String str, GradientType gradientType, Path.FillType fillType, c cVar, d dVar, f fVar, f fVar2, b bVar, b bVar2) {
        this.f2468a = gradientType;
        this.f2469b = fillType;
        this.f2470c = cVar;
        this.f2471d = dVar;
        this.f2472e = fVar;
        this.f2473f = fVar2;
        this.f2474g = str;
        this.f2475h = bVar;
        this.i = bVar2;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f2474g;
    }

    /* access modifiers changed from: package-private */
    public GradientType b() {
        return this.f2468a;
    }

    /* access modifiers changed from: package-private */
    public Path.FillType c() {
        return this.f2469b;
    }

    /* access modifiers changed from: package-private */
    public c d() {
        return this.f2470c;
    }

    /* access modifiers changed from: package-private */
    public d e() {
        return this.f2471d;
    }

    /* access modifiers changed from: package-private */
    public f f() {
        return this.f2472e;
    }

    /* access modifiers changed from: package-private */
    public f g() {
        return this.f2473f;
    }

    public y a(be beVar, q qVar) {
        return new aq(beVar, qVar, this);
    }

    /* compiled from: GradientFill */
    static class a {
        static ap a(JSONObject jSONObject, bd bdVar) {
            c cVar;
            d dVar;
            f fVar;
            f fVar2;
            String optString = jSONObject.optString("nm");
            JSONObject optJSONObject = jSONObject.optJSONObject("g");
            if (optJSONObject != null && optJSONObject.has("k")) {
                int optInt = optJSONObject.optInt("p");
                optJSONObject = optJSONObject.optJSONObject("k");
                try {
                    optJSONObject.put("p", optInt);
                } catch (JSONException e2) {
                }
            }
            if (optJSONObject != null) {
                cVar = c.a.a(optJSONObject, bdVar);
            } else {
                cVar = null;
            }
            JSONObject optJSONObject2 = jSONObject.optJSONObject("o");
            if (optJSONObject2 != null) {
                dVar = d.a.a(optJSONObject2, bdVar);
            } else {
                dVar = null;
            }
            Path.FillType fillType = jSONObject.optInt("r", 1) == 1 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD;
            GradientType gradientType = jSONObject.optInt("t", 1) == 1 ? GradientType.Linear : GradientType.Radial;
            JSONObject optJSONObject3 = jSONObject.optJSONObject("s");
            if (optJSONObject3 != null) {
                fVar = f.a.a(optJSONObject3, bdVar);
            } else {
                fVar = null;
            }
            JSONObject optJSONObject4 = jSONObject.optJSONObject("e");
            if (optJSONObject4 != null) {
                fVar2 = f.a.a(optJSONObject4, bdVar);
            } else {
                fVar2 = null;
            }
            return new ap(optString, gradientType, fillType, cVar, dVar, fVar, fVar2, null, null);
        }
    }
}
