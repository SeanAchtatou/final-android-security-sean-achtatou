package com.airbnb.lottie;

import android.graphics.Path;
import com.airbnb.lottie.ShapeTrimPath;
import com.airbnb.lottie.p;
import java.util.List;

/* compiled from: ShapeContent */
class cb implements bn, p.a {

    /* renamed from: a  reason: collision with root package name */
    private final Path f2612a = new Path();

    /* renamed from: b  reason: collision with root package name */
    private final String f2613b;

    /* renamed from: c  reason: collision with root package name */
    private final be f2614c;

    /* renamed from: d  reason: collision with root package name */
    private final p<?, Path> f2615d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f2616e;

    /* renamed from: f  reason: collision with root package name */
    private cr f2617f;

    cb(be beVar, q qVar, ch chVar) {
        this.f2613b = chVar.a();
        this.f2614c = beVar;
        this.f2615d = chVar.b().b();
        qVar.a(this.f2615d);
        this.f2615d.a(this);
    }

    public void a() {
        b();
    }

    private void b() {
        this.f2616e = false;
        this.f2614c.invalidateSelf();
    }

    public void a(List<y> list, List<y> list2) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                y yVar = list.get(i2);
                if ((yVar instanceof cr) && ((cr) yVar).b() == ShapeTrimPath.Type.Simultaneously) {
                    this.f2617f = (cr) yVar;
                    this.f2617f.a(this);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public Path d() {
        if (this.f2616e) {
            return this.f2612a;
        }
        this.f2612a.reset();
        this.f2612a.set(this.f2615d.b());
        this.f2612a.setFillType(Path.FillType.EVEN_ODD);
        cs.a(this.f2612a, this.f2617f);
        this.f2616e = true;
        return this.f2612a;
    }

    public String e() {
        return this.f2613b;
    }
}
