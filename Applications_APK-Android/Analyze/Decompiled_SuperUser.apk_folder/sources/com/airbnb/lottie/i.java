package com.airbnb.lottie;

import android.graphics.PointF;

/* compiled from: AnimatableSplitDimensionPathValue */
class i implements m<PointF> {

    /* renamed from: a  reason: collision with root package name */
    private final b f2676a;

    /* renamed from: b  reason: collision with root package name */
    private final b f2677b;

    i(b bVar, b bVar2) {
        this.f2676a = bVar;
        this.f2677b = bVar2;
    }

    /* renamed from: a */
    public bb<PointF> b() {
        return new ck(this.f2676a.b(), this.f2677b.b());
    }
}
