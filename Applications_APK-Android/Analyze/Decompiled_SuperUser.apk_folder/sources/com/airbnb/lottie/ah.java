package com.airbnb.lottie;

import java.util.List;

/* compiled from: FloatKeyframeAnimation */
class ah extends bb<Float> {
    ah(List<ba<Float>> list) {
        super(list);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public Float a(ba<Float> baVar, float f2) {
        if (baVar.f2512a != null && baVar.f2513b != null) {
            return Float.valueOf(bj.a(((Float) baVar.f2512a).floatValue(), ((Float) baVar.f2513b).floatValue(), f2));
        }
        throw new IllegalStateException("Missing values for keyframe.");
    }
}
