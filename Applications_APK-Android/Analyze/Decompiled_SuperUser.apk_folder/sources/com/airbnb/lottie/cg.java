package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.RectF;
import java.util.Collections;

/* compiled from: ShapeLayer */
class cg extends q {

    /* renamed from: e  reason: collision with root package name */
    private final z f2631e;

    cg(be beVar, Layer layer) {
        super(beVar, layer);
        this.f2631e = new z(beVar, this, new ce(layer.f(), layer.n()));
        this.f2631e.a(Collections.emptyList(), Collections.emptyList());
    }

    /* access modifiers changed from: package-private */
    public void b(Canvas canvas, Matrix matrix, int i) {
        this.f2631e.a(canvas, matrix, i);
    }

    public void a(RectF rectF, Matrix matrix) {
        super.a(rectF, matrix);
        this.f2631e.a(rectF, this.f2703a);
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
        this.f2631e.a(str, str2, colorFilter);
    }
}
