package com.airbnb.lottie;

import android.graphics.Path;
import com.airbnb.lottie.a;
import com.airbnb.lottie.d;
import org.json.JSONObject;

/* compiled from: ShapeFill */
class cd implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f2622a;

    /* renamed from: b  reason: collision with root package name */
    private final Path.FillType f2623b;

    /* renamed from: c  reason: collision with root package name */
    private final String f2624c;

    /* renamed from: d  reason: collision with root package name */
    private final a f2625d;

    /* renamed from: e  reason: collision with root package name */
    private final d f2626e;

    private cd(String str, boolean z, Path.FillType fillType, a aVar, d dVar) {
        this.f2624c = str;
        this.f2622a = z;
        this.f2623b = fillType;
        this.f2625d = aVar;
        this.f2626e = dVar;
    }

    /* compiled from: ShapeFill */
    static class a {
        static cd a(JSONObject jSONObject, bd bdVar) {
            a aVar;
            d dVar;
            String optString = jSONObject.optString("nm");
            JSONObject optJSONObject = jSONObject.optJSONObject("c");
            if (optJSONObject != null) {
                aVar = a.C0033a.a(optJSONObject, bdVar);
            } else {
                aVar = null;
            }
            JSONObject optJSONObject2 = jSONObject.optJSONObject("o");
            if (optJSONObject2 != null) {
                dVar = d.a.a(optJSONObject2, bdVar);
            } else {
                dVar = null;
            }
            return new cd(optString, jSONObject.optBoolean("fillEnabled"), jSONObject.optInt("r", 1) == 1 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD, aVar, dVar);
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f2624c;
    }

    /* access modifiers changed from: package-private */
    public a b() {
        return this.f2625d;
    }

    /* access modifiers changed from: package-private */
    public d c() {
        return this.f2626e;
    }

    /* access modifiers changed from: package-private */
    public Path.FillType d() {
        return this.f2623b;
    }

    public y a(be beVar, q qVar) {
        return new ag(beVar, qVar, this);
    }

    public String toString() {
        String hexString;
        Object c2;
        StringBuilder append = new StringBuilder().append("ShapeFill{color=");
        if (this.f2625d == null) {
            hexString = "null";
        } else {
            hexString = Integer.toHexString(((Integer) this.f2625d.d()).intValue());
        }
        StringBuilder append2 = append.append(hexString).append(", fillEnabled=").append(this.f2622a).append(", opacity=");
        if (this.f2626e == null) {
            c2 = "null";
        } else {
            c2 = this.f2626e.d();
        }
        return append2.append(c2).append('}').toString();
    }
}
