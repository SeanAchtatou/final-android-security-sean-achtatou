package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.support.v4.util.e;
import java.util.ArrayList;
import java.util.List;

/* compiled from: CompositionLayer */
class w extends q {

    /* renamed from: e  reason: collision with root package name */
    private final bb<Float> f2729e;

    /* renamed from: f  reason: collision with root package name */
    private final List<q> f2730f = new ArrayList();

    /* renamed from: g  reason: collision with root package name */
    private final RectF f2731g = new RectF();

    /* renamed from: h  reason: collision with root package name */
    private final Rect f2732h = new Rect();
    private final RectF i = new RectF();
    private Boolean j;
    private Boolean k;

    w(be beVar, Layer layer, List<Layer> list, bd bdVar) {
        super(beVar, layer);
        q qVar;
        b u = layer.u();
        if (u != null) {
            this.f2729e = u.b();
            a(this.f2729e);
            this.f2729e.a(this);
        } else {
            this.f2729e = null;
        }
        e eVar = new e(bdVar.i().size());
        int size = list.size() - 1;
        q qVar2 = null;
        while (size >= 0) {
            q a2 = q.a(list.get(size), beVar, bdVar);
            if (a2 == null) {
                qVar = qVar2;
            } else {
                eVar.b(a2.b().e(), a2);
                if (qVar2 != null) {
                    qVar2.a(a2);
                    qVar = null;
                } else {
                    this.f2730f.add(0, a2);
                    switch (r0.l()) {
                        case Add:
                        case Invert:
                            qVar = a2;
                            continue;
                        default:
                            qVar = qVar2;
                            continue;
                    }
                }
            }
            size--;
            qVar2 = qVar;
        }
        for (int i2 = 0; i2 < eVar.b(); i2++) {
            q qVar3 = (q) eVar.a(eVar.b(i2));
            q qVar4 = (q) eVar.a(qVar3.b().m());
            if (qVar4 != null) {
                qVar3.b(qVar4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Canvas canvas, Matrix matrix, int i2) {
        bc.a("CompositionLayer#draw");
        canvas.getClipBounds(this.f2732h);
        this.i.set(0.0f, 0.0f, (float) this.f2705c.h(), (float) this.f2705c.i());
        matrix.mapRect(this.i);
        for (int size = this.f2730f.size() - 1; size >= 0; size--) {
            boolean z = true;
            if (!this.i.isEmpty()) {
                z = canvas.clipRect(this.i);
            }
            if (z) {
                this.f2730f.get(size).a(canvas, matrix, i2);
            }
        }
        if (!this.f2732h.isEmpty()) {
            canvas.clipRect(this.f2732h, Region.Op.REPLACE);
        }
        bc.b("CompositionLayer#draw");
    }

    public void a(RectF rectF, Matrix matrix) {
        super.a(rectF, matrix);
        this.f2731g.set(0.0f, 0.0f, 0.0f, 0.0f);
        for (int size = this.f2730f.size() - 1; size >= 0; size--) {
            this.f2730f.get(size).a(this.f2731g, this.f2703a);
            if (rectF.isEmpty()) {
                rectF.set(this.f2731g);
            } else {
                rectF.set(Math.min(rectF.left, this.f2731g.left), Math.min(rectF.top, this.f2731g.top), Math.max(rectF.right, this.f2731g.right), Math.max(rectF.bottom, this.f2731g.bottom));
            }
        }
    }

    public void a(float f2) {
        super.a(f2);
        if (this.f2729e != null) {
            f2 = ((float) ((long) (((Float) this.f2729e.b()).floatValue() * 1000.0f))) / ((float) this.f2704b.n().c());
        }
        if (this.f2705c.b() != 0.0f) {
            f2 /= this.f2705c.b();
        }
        float c2 = f2 - this.f2705c.c();
        for (int size = this.f2730f.size() - 1; size >= 0; size--) {
            this.f2730f.get(size).a(c2);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        if (this.k == null) {
            for (int size = this.f2730f.size() - 1; size >= 0; size--) {
                q qVar = this.f2730f.get(size);
                if (qVar instanceof cg) {
                    if (qVar.d()) {
                        this.k = true;
                        return true;
                    }
                } else if ((qVar instanceof w) && ((w) qVar).f()) {
                    this.k = true;
                    return true;
                }
            }
            this.k = false;
        }
        return this.k.booleanValue();
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        if (this.j == null) {
            if (c()) {
                this.j = true;
                return true;
            }
            for (int size = this.f2730f.size() - 1; size >= 0; size--) {
                if (this.f2730f.get(size).c()) {
                    this.j = true;
                    return true;
                }
            }
            this.j = false;
        }
        return this.j.booleanValue();
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f2730f.size()) {
                q qVar = this.f2730f.get(i3);
                String f2 = qVar.b().f();
                if (str == null) {
                    qVar.a((String) null, (String) null, colorFilter);
                } else if (f2.equals(str)) {
                    qVar.a(str, str2, colorFilter);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }
}
