package com.airbnb.lottie;

import java.util.List;

/* compiled from: ColorKeyframeAnimation */
class v extends bb<Integer> {
    v(List<ba<Integer>> list) {
        super(list);
    }

    /* renamed from: b */
    public Integer a(ba<Integer> baVar, float f2) {
        if (baVar.f2512a != null && baVar.f2513b != null) {
            return Integer.valueOf(am.a(f2, ((Integer) baVar.f2512a).intValue(), ((Integer) baVar.f2513b).intValue()));
        }
        throw new IllegalStateException("Missing values for keyframe.");
    }
}
