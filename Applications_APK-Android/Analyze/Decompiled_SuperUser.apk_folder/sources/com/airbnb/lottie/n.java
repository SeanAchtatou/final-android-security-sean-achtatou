package com.airbnb.lottie;

import com.airbnb.lottie.ba;
import com.airbnb.lottie.m;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: AnimatableValueParser */
class n<T> {

    /* renamed from: a  reason: collision with root package name */
    private final JSONObject f2690a;

    /* renamed from: b  reason: collision with root package name */
    private final float f2691b;

    /* renamed from: c  reason: collision with root package name */
    private final bd f2692c;

    /* renamed from: d  reason: collision with root package name */
    private final m.a<T> f2693d;

    private n(JSONObject jSONObject, float f2, bd bdVar, m.a<T> aVar) {
        this.f2690a = jSONObject;
        this.f2691b = f2;
        this.f2692c = bdVar;
        this.f2693d = aVar;
    }

    static <T> n<T> a(JSONObject jSONObject, float f2, bd bdVar, m.a<T> aVar) {
        return new n<>(jSONObject, f2, bdVar, aVar);
    }

    /* access modifiers changed from: package-private */
    public a<T> a() {
        List b2 = b();
        return new a<>(b2, a(b2));
    }

    private List<ba<T>> b() {
        if (this.f2690a == null) {
            return Collections.emptyList();
        }
        Object opt = this.f2690a.opt("k");
        if (a(opt)) {
            return ba.a.a((JSONArray) opt, this.f2692c, this.f2691b, this.f2693d);
        }
        return Collections.emptyList();
    }

    private T a(List<ba<T>> list) {
        if (this.f2690a == null) {
            return null;
        }
        if (!list.isEmpty()) {
            return list.get(0).f2512a;
        }
        return this.f2693d.b(this.f2690a.opt("k"), this.f2691b);
    }

    private static boolean a(Object obj) {
        if (!(obj instanceof JSONArray)) {
            return false;
        }
        Object opt = ((JSONArray) obj).opt(0);
        return (opt instanceof JSONObject) && ((JSONObject) opt).has("t");
    }

    /* compiled from: AnimatableValueParser */
    static class a<T> {

        /* renamed from: a  reason: collision with root package name */
        final List<ba<T>> f2694a;

        /* renamed from: b  reason: collision with root package name */
        final T f2695b;

        a(List<ba<T>> list, T t) {
            this.f2694a = list;
            this.f2695b = t;
        }
    }
}
