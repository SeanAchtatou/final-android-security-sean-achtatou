package com.airbnb.lottie;

import android.graphics.Matrix;
import android.graphics.PointF;
import com.airbnb.lottie.p;

/* compiled from: TransformKeyframeAnimation */
class cq {

    /* renamed from: a  reason: collision with root package name */
    private final Matrix f2652a = new Matrix();

    /* renamed from: b  reason: collision with root package name */
    private final bb<PointF> f2653b;

    /* renamed from: c  reason: collision with root package name */
    private final p<?, PointF> f2654c;

    /* renamed from: d  reason: collision with root package name */
    private final bb<ca> f2655d;

    /* renamed from: e  reason: collision with root package name */
    private final bb<Float> f2656e;

    /* renamed from: f  reason: collision with root package name */
    private final bb<Integer> f2657f;

    /* renamed from: g  reason: collision with root package name */
    private final p<?, Float> f2658g;

    /* renamed from: h  reason: collision with root package name */
    private final p<?, Float> f2659h;

    cq(l lVar) {
        this.f2653b = lVar.a().b();
        this.f2654c = lVar.b().b();
        this.f2655d = lVar.c().b();
        this.f2656e = lVar.d().b();
        this.f2657f = lVar.e().b();
        if (lVar.f() != null) {
            this.f2658g = lVar.f().b();
        } else {
            this.f2658g = null;
        }
        if (lVar.g() != null) {
            this.f2659h = lVar.g().b();
        } else {
            this.f2659h = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(q qVar) {
        qVar.a(this.f2653b);
        qVar.a(this.f2654c);
        qVar.a(this.f2655d);
        qVar.a(this.f2656e);
        qVar.a(this.f2657f);
        if (this.f2658g != null) {
            qVar.a(this.f2658g);
        }
        if (this.f2659h != null) {
            qVar.a(this.f2659h);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(p.a aVar) {
        this.f2653b.a(aVar);
        this.f2654c.a(aVar);
        this.f2655d.a(aVar);
        this.f2656e.a(aVar);
        this.f2657f.a(aVar);
        if (this.f2658g != null) {
            this.f2658g.a(aVar);
        }
        if (this.f2659h != null) {
            this.f2659h.a(aVar);
        }
    }

    /* access modifiers changed from: package-private */
    public p<?, Integer> a() {
        return this.f2657f;
    }

    public p<?, Float> b() {
        return this.f2658g;
    }

    public p<?, Float> c() {
        return this.f2659h;
    }

    /* access modifiers changed from: package-private */
    public Matrix d() {
        this.f2652a.reset();
        PointF b2 = this.f2654c.b();
        if (!(b2.x == 0.0f && b2.y == 0.0f)) {
            this.f2652a.preTranslate(b2.x, b2.y);
        }
        float floatValue = ((Float) this.f2656e.b()).floatValue();
        if (floatValue != 0.0f) {
            this.f2652a.preRotate(floatValue);
        }
        ca caVar = (ca) this.f2655d.b();
        if (!(caVar.a() == 1.0f && caVar.b() == 1.0f)) {
            this.f2652a.preScale(caVar.a(), caVar.b());
        }
        PointF pointF = (PointF) this.f2653b.b();
        if (!(pointF.x == 0.0f && pointF.y == 0.0f)) {
            this.f2652a.preTranslate(-pointF.x, -pointF.y);
        }
        return this.f2652a;
    }

    /* access modifiers changed from: package-private */
    public Matrix a(float f2) {
        PointF b2 = this.f2654c.b();
        PointF pointF = (PointF) this.f2653b.b();
        ca caVar = (ca) this.f2655d.b();
        float floatValue = ((Float) this.f2656e.b()).floatValue();
        this.f2652a.reset();
        this.f2652a.preTranslate(b2.x * f2, b2.y * f2);
        this.f2652a.preScale((float) Math.pow((double) caVar.a(), (double) f2), (float) Math.pow((double) caVar.b(), (double) f2));
        this.f2652a.preRotate(floatValue * f2, pointF.x, pointF.y);
        return this.f2652a;
    }
}
