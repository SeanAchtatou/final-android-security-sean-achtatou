package com.airbnb.lottie;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import java.io.Closeable;

/* compiled from: Utils */
final class cs {

    /* renamed from: a  reason: collision with root package name */
    private static final PathMeasure f2666a = new PathMeasure();

    /* renamed from: b  reason: collision with root package name */
    private static final Path f2667b = new Path();

    /* renamed from: c  reason: collision with root package name */
    private static final Path f2668c = new Path();

    /* renamed from: d  reason: collision with root package name */
    private static final float[] f2669d = new float[4];

    /* renamed from: e  reason: collision with root package name */
    private static final float f2670e = ((float) Math.sqrt(2.0d));

    static Path a(PointF pointF, PointF pointF2, PointF pointF3, PointF pointF4) {
        Path path = new Path();
        path.moveTo(pointF.x, pointF.y);
        if (pointF3 == null || pointF4 == null || (pointF3.length() == 0.0f && pointF4.length() == 0.0f)) {
            path.lineTo(pointF2.x, pointF2.y);
        } else {
            path.cubicTo(pointF.x + pointF3.x, pointF.y + pointF3.y, pointF2.x + pointF4.x, pointF2.y + pointF4.y, pointF2.x, pointF2.y);
        }
        return path;
    }

    static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    static float a(Matrix matrix) {
        f2669d[0] = 0.0f;
        f2669d[1] = 0.0f;
        f2669d[2] = f2670e;
        f2669d[3] = f2670e;
        matrix.mapPoints(f2669d);
        return ((float) Math.hypot((double) (f2669d[2] - f2669d[0]), (double) (f2669d[3] - f2669d[1]))) / 2.0f;
    }

    static void a(Path path, cr crVar) {
        if (crVar != null) {
            a(path, crVar.c().b().floatValue() / 100.0f, crVar.d().b().floatValue() / 100.0f, crVar.f().b().floatValue() / 360.0f);
        }
    }

    static void a(Path path, float f2, float f3, float f4) {
        bc.a("applyTrimPathIfNeeded");
        f2666a.setPath(path, false);
        float length = f2666a.getLength();
        if (f2 == 1.0f && f3 == 0.0f) {
            bc.b("applyTrimPathIfNeeded");
        } else if (length < 1.0f || ((double) Math.abs((f3 - f2) - 1.0f)) < 0.01d) {
            bc.b("applyTrimPathIfNeeded");
        } else {
            float f5 = length * f2;
            float f6 = length * f3;
            float min = Math.min(f5, f6);
            float max = Math.max(f5, f6);
            float f7 = f4 * length;
            float f8 = min + f7;
            float f9 = max + f7;
            if (f8 >= length && f9 >= length) {
                f8 = (float) bj.a(f8, length);
                f9 = (float) bj.a(f9, length);
            }
            if (f8 < 0.0f) {
                f8 = (float) bj.a(f8, length);
            }
            if (f9 < 0.0f) {
                f9 = (float) bj.a(f9, length);
            }
            if (f8 == f9) {
                path.reset();
                bc.b("applyTrimPathIfNeeded");
                return;
            }
            if (f8 >= f9) {
                f8 -= length;
            }
            f2667b.reset();
            f2666a.getSegment(f8, f9, f2667b, true);
            if (f9 > length) {
                f2668c.reset();
                f2666a.getSegment(0.0f, f9 % length, f2668c, true);
                f2667b.addPath(f2668c);
            } else if (f8 < 0.0f) {
                f2668c.reset();
                f2666a.getSegment(f8 + length, length, f2668c, true);
                f2667b.addPath(f2668c);
            }
            path.set(f2667b);
            bc.b("applyTrimPathIfNeeded");
        }
    }

    static boolean a(bd bdVar, int i, int i2, int i3) {
        if (bdVar.d() < i) {
            return false;
        }
        if (bdVar.d() > i) {
            return true;
        }
        if (bdVar.e() < i2) {
            return false;
        }
        if (bdVar.e() > i2 || bdVar.f() >= i3) {
            return true;
        }
        return false;
    }

    static int a(float f2, float f3, float f4, float f5) {
        int i = 17;
        if (f2 != 0.0f) {
            i = (int) (((float) 527) * f2);
        }
        if (f3 != 0.0f) {
            i = (int) (((float) (i * 31)) * f3);
        }
        if (f4 != 0.0f) {
            i = (int) (((float) (i * 31)) * f4);
        }
        if (f5 != 0.0f) {
            return (int) (((float) (i * 31)) * f5);
        }
        return i;
    }
}
