package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;

/* compiled from: StrokeContent */
class cm extends r {

    /* renamed from: b  reason: collision with root package name */
    private final String f2642b;

    /* renamed from: c  reason: collision with root package name */
    private final bb<Integer> f2643c;

    cm(be beVar, q qVar, ShapeStroke shapeStroke) {
        super(beVar, qVar, shapeStroke.g().a(), shapeStroke.h().a(), shapeStroke.c(), shapeStroke.d(), shapeStroke.e(), shapeStroke.f());
        this.f2642b = shapeStroke.a();
        this.f2643c = shapeStroke.b().b();
        this.f2643c.a(this);
        qVar.a(this.f2643c);
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
        this.f2715a.setColorFilter(colorFilter);
    }

    public void a(Canvas canvas, Matrix matrix, int i) {
        this.f2715a.setColor(((Integer) this.f2643c.b()).intValue());
        super.a(canvas, matrix, i);
    }

    public String e() {
        return this.f2642b;
    }
}
