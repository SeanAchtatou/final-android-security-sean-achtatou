package com.airbnb.lottie;

import android.graphics.PointF;
import java.util.List;

/* compiled from: PointKeyframeAnimation */
class bs extends bb<PointF> {

    /* renamed from: b  reason: collision with root package name */
    private final PointF f2574b = new PointF();

    bs(List<ba<PointF>> list) {
        super(list);
    }

    /* renamed from: b */
    public PointF a(ba<PointF> baVar, float f2) {
        if (baVar.f2512a == null || baVar.f2513b == null) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        PointF pointF = (PointF) baVar.f2512a;
        PointF pointF2 = (PointF) baVar.f2513b;
        this.f2574b.set(pointF.x + ((pointF2.x - pointF.x) * f2), ((pointF2.y - pointF.y) * f2) + pointF.y);
        return this.f2574b;
    }
}
