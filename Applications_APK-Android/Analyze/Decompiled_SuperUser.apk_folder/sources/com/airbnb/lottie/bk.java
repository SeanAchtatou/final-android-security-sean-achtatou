package com.airbnb.lottie;

import android.support.v4.util.h;

/* compiled from: MutablePair */
public class bk<T> {

    /* renamed from: a  reason: collision with root package name */
    T f2561a;

    /* renamed from: b  reason: collision with root package name */
    T f2562b;

    public void a(T t, T t2) {
        this.f2561a = t;
        this.f2562b = t2;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        if (!b(hVar.f880a, this.f2561a) || !b(hVar.f881b, this.f2562b)) {
            return false;
        }
        return true;
    }

    private static boolean b(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public int hashCode() {
        int i = 0;
        int hashCode = this.f2561a == null ? 0 : this.f2561a.hashCode();
        if (this.f2562b != null) {
            i = this.f2562b.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        return "Pair{" + String.valueOf(this.f2561a) + " " + String.valueOf(this.f2562b) + "}";
    }
}
