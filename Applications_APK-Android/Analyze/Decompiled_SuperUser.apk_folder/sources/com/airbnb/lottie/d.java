package com.airbnb.lottie;

import com.airbnb.lottie.m;
import com.airbnb.lottie.n;
import java.util.List;
import org.json.JSONObject;

/* compiled from: AnimatableIntegerValue */
class d extends o<Integer, Integer> {
    private d() {
        super(100);
    }

    d(List<ba<Integer>> list, Integer num) {
        super(list, num);
    }

    /* renamed from: a */
    public bb<Integer> b() {
        if (!e()) {
            return new cl(this.f2697b);
        }
        return new ax(this.f2696a);
    }

    /* renamed from: c */
    public Integer d() {
        return (Integer) this.f2697b;
    }

    /* compiled from: AnimatableIntegerValue */
    static final class a {
        static d a() {
            return new d();
        }

        static d a(JSONObject jSONObject, bd bdVar) {
            if (jSONObject != null && jSONObject.has("x")) {
                bdVar.a("Lottie doesn't support expressions.");
            }
            n.a a2 = n.a(jSONObject, 1.0f, bdVar, b.f2671a).a();
            return new d(a2.f2694a, (Integer) a2.f2695b);
        }
    }

    /* compiled from: AnimatableIntegerValue */
    private static class b implements m.a<Integer> {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final b f2671a = new b();

        private b() {
        }

        /* renamed from: a */
        public Integer b(Object obj, float f2) {
            return Integer.valueOf(Math.round(az.a(obj) * f2));
        }
    }
}
