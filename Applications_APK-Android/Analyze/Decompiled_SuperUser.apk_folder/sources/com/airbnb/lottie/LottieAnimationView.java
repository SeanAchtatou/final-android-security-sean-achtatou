package com.airbnb.lottie;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import com.airbnb.lottie.bd;
import com.airbnb.lottie.bu;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class LottieAnimationView extends AppCompatImageView {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2347a = LottieAnimationView.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final Map<String, bd> f2348b = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static final Map<String, WeakReference<bd>> f2349c = new HashMap();

    /* renamed from: d  reason: collision with root package name */
    private final bm f2350d = new bm() {
        public void a(bd bdVar) {
            if (bdVar != null) {
                LottieAnimationView.this.setComposition(bdVar);
            }
            s unused = LottieAnimationView.this.k = (s) null;
        }
    };

    /* renamed from: e  reason: collision with root package name */
    private final be f2351e = new be();

    /* renamed from: f  reason: collision with root package name */
    private CacheStrategy f2352f;

    /* renamed from: g  reason: collision with root package name */
    private String f2353g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f2354h = false;
    private boolean i = false;
    private boolean j = false;
    /* access modifiers changed from: private */
    public s k;
    private bd l;

    public enum CacheStrategy {
        None,
        Weak,
        Strong
    }

    public LottieAnimationView(Context context) {
        super(context);
        a((AttributeSet) null);
    }

    public LottieAnimationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet);
    }

    public LottieAnimationView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(attributeSet);
    }

    private void a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, bu.a.LottieAnimationView);
        this.f2352f = CacheStrategy.values()[obtainStyledAttributes.getInt(bu.a.LottieAnimationView_lottie_cacheStrategy, CacheStrategy.None.ordinal())];
        String string = obtainStyledAttributes.getString(bu.a.LottieAnimationView_lottie_fileName);
        if (!isInEditMode() && string != null) {
            setAnimation(string);
        }
        if (obtainStyledAttributes.getBoolean(bu.a.LottieAnimationView_lottie_autoPlay, false)) {
            this.f2351e.h();
            this.i = true;
        }
        this.f2351e.c(obtainStyledAttributes.getBoolean(bu.a.LottieAnimationView_lottie_loop, false));
        setImageAssetsFolder(obtainStyledAttributes.getString(bu.a.LottieAnimationView_lottie_imageAssetsFolder));
        setProgress(obtainStyledAttributes.getFloat(bu.a.LottieAnimationView_lottie_progress, 0.0f));
        a(obtainStyledAttributes.getBoolean(bu.a.LottieAnimationView_lottie_enableMergePathsForKitKatAndAbove, false));
        if (obtainStyledAttributes.hasValue(bu.a.LottieAnimationView_lottie_colorFilter)) {
            a(new ci(obtainStyledAttributes.getColor(bu.a.LottieAnimationView_lottie_colorFilter, 0)));
        }
        if (obtainStyledAttributes.hasValue(bu.a.LottieAnimationView_lottie_scale)) {
            this.f2351e.c(obtainStyledAttributes.getFloat(bu.a.LottieAnimationView_lottie_scale, 1.0f));
        }
        obtainStyledAttributes.recycle();
        if (Build.VERSION.SDK_INT >= 17 && Settings.Global.getFloat(getContext().getContentResolver(), "animator_duration_scale", 1.0f) == 0.0f) {
            this.f2351e.e();
        }
        h();
    }

    public void setImageResource(int i2) {
        super.setImageResource(i2);
        a();
    }

    public void setImageDrawable(Drawable drawable) {
        if (drawable != this.f2351e) {
            a();
        }
        super.setImageDrawable(drawable);
    }

    public void a(ColorFilter colorFilter) {
        this.f2351e.a(colorFilter);
    }

    public void invalidateDrawable(Drawable drawable) {
        if (getDrawable() == this.f2351e) {
            super.invalidateDrawable(this.f2351e);
        } else {
            super.invalidateDrawable(drawable);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f2363a = this.f2353g;
        savedState.f2364b = this.f2351e.j();
        savedState.f2365c = this.f2351e.g();
        savedState.f2366d = this.f2351e.f();
        savedState.f2367e = this.f2351e.b();
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.f2353g = savedState.f2363a;
        if (!TextUtils.isEmpty(this.f2353g)) {
            setAnimation(this.f2353g);
        }
        setProgress(savedState.f2364b);
        b(savedState.f2366d);
        if (savedState.f2365c) {
            c();
        }
        this.f2351e.a(savedState.f2367e);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.i && this.f2354h) {
            c();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (b()) {
            d();
            this.f2354h = true;
        }
        a();
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.f2351e != null) {
            this.f2351e.c();
        }
    }

    public void a(boolean z) {
        this.f2351e.a(z);
    }

    public void setAnimation(String str) {
        a(str, this.f2352f);
    }

    public void a(final String str, final CacheStrategy cacheStrategy) {
        this.f2353g = str;
        if (f2349c.containsKey(str)) {
            bd bdVar = (bd) f2349c.get(str).get();
            if (bdVar != null) {
                setComposition(bdVar);
                return;
            }
        } else if (f2348b.containsKey(str)) {
            setComposition(f2348b.get(str));
            return;
        }
        this.f2353g = str;
        this.f2351e.o();
        g();
        this.k = bd.a.a(getContext(), str, new bm() {
            public void a(bd bdVar) {
                if (cacheStrategy == CacheStrategy.Strong) {
                    LottieAnimationView.f2348b.put(str, bdVar);
                } else if (cacheStrategy == CacheStrategy.Weak) {
                    LottieAnimationView.f2349c.put(str, new WeakReference(bdVar));
                }
                LottieAnimationView.this.setComposition(bdVar);
            }
        });
    }

    public void setAnimation(JSONObject jSONObject) {
        g();
        this.k = bd.a.a(getResources(), jSONObject, this.f2350d);
    }

    private void g() {
        if (this.k != null) {
            this.k.a();
            this.k = null;
        }
    }

    public void setComposition(bd bdVar) {
        this.f2351e.setCallback(this);
        boolean a2 = this.f2351e.a(bdVar);
        h();
        if (a2) {
            setImageDrawable(null);
            setImageDrawable(this.f2351e);
            this.l = bdVar;
            requestLayout();
        }
    }

    public void b(boolean z) {
        this.f2351e.c(z);
    }

    public boolean b() {
        return this.f2351e.g();
    }

    public void c() {
        this.f2351e.h();
        h();
    }

    public void setSpeed(float f2) {
        this.f2351e.a(f2);
    }

    public void setImageAssetsFolder(String str) {
        this.f2351e.a(str);
    }

    public String getImageAssetsFolder() {
        return this.f2351e.b();
    }

    public void setImageAssetDelegate(au auVar) {
        this.f2351e.a(auVar);
    }

    public void setFontAssetDelegate(aj ajVar) {
        this.f2351e.a(ajVar);
    }

    public void setTextDelegate(cn cnVar) {
        this.f2351e.a(cnVar);
    }

    public void setScale(float f2) {
        this.f2351e.c(f2);
        if (getDrawable() == this.f2351e) {
            setImageDrawable(null);
            setImageDrawable(this.f2351e);
        }
    }

    public float getScale() {
        return this.f2351e.m();
    }

    public void d() {
        this.f2351e.o();
        h();
    }

    public void setProgress(float f2) {
        this.f2351e.b(f2);
    }

    public float getProgress() {
        return this.f2351e.j();
    }

    public long getDuration() {
        if (this.l != null) {
            return this.l.c();
        }
        return 0;
    }

    public void setPerformanceTrackingEnabled(boolean z) {
        this.f2351e.b(z);
    }

    public bq getPerformanceTracker() {
        return this.f2351e.d();
    }

    private void h() {
        int i2 = 1;
        if (this.j && this.f2351e.g()) {
            i2 = 2;
        }
        setLayerType(i2, null);
    }

    private static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        String f2363a;

        /* renamed from: b  reason: collision with root package name */
        float f2364b;

        /* renamed from: c  reason: collision with root package name */
        boolean f2365c;

        /* renamed from: d  reason: collision with root package name */
        boolean f2366d;

        /* renamed from: e  reason: collision with root package name */
        String f2367e;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        private SavedState(Parcel parcel) {
            super(parcel);
            boolean z;
            boolean z2 = true;
            this.f2363a = parcel.readString();
            this.f2364b = parcel.readFloat();
            if (parcel.readInt() == 1) {
                z = true;
            } else {
                z = false;
            }
            this.f2365c = z;
            this.f2366d = parcel.readInt() != 1 ? false : z2;
            this.f2367e = parcel.readString();
        }

        public void writeToParcel(Parcel parcel, int i) {
            int i2;
            int i3 = 1;
            super.writeToParcel(parcel, i);
            parcel.writeString(this.f2363a);
            parcel.writeFloat(this.f2364b);
            if (this.f2365c) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            parcel.writeInt(i2);
            if (!this.f2366d) {
                i3 = 0;
            }
            parcel.writeInt(i3);
            parcel.writeString(this.f2367e);
        }
    }
}
