package com.airbnb.lottie;

import android.util.Log;
import com.airbnb.lottie.MergePaths;
import com.airbnb.lottie.PolystarShape;
import com.airbnb.lottie.ShapeStroke;
import com.airbnb.lottie.ShapeTrimPath;
import com.airbnb.lottie.ap;
import com.airbnb.lottie.ar;
import com.airbnb.lottie.bw;
import com.airbnb.lottie.bx;
import com.airbnb.lottie.cd;
import com.airbnb.lottie.ch;
import com.airbnb.lottie.l;
import com.airbnb.lottie.t;
import com.kingouser.com.util.ShellUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ShapeGroup */
class ce implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2627a;

    /* renamed from: b  reason: collision with root package name */
    private final List<aa> f2628b;

    static aa a(JSONObject jSONObject, bd bdVar) {
        String optString = jSONObject.optString("ty");
        char c2 = 65535;
        switch (optString.hashCode()) {
            case 3239:
                if (optString.equals("el")) {
                    c2 = 7;
                    break;
                }
                break;
            case 3270:
                if (optString.equals("fl")) {
                    c2 = 3;
                    break;
                }
                break;
            case 3295:
                if (optString.equals("gf")) {
                    c2 = 4;
                    break;
                }
                break;
            case 3307:
                if (optString.equals("gr")) {
                    c2 = 0;
                    break;
                }
                break;
            case 3308:
                if (optString.equals("gs")) {
                    c2 = 2;
                    break;
                }
                break;
            case 3488:
                if (optString.equals("mm")) {
                    c2 = 11;
                    break;
                }
                break;
            case 3633:
                if (optString.equals("rc")) {
                    c2 = 8;
                    break;
                }
                break;
            case 3646:
                if (optString.equals("rp")) {
                    c2 = 12;
                    break;
                }
                break;
            case 3669:
                if (optString.equals(ShellUtils.COMMAND_SH)) {
                    c2 = 6;
                    break;
                }
                break;
            case 3679:
                if (optString.equals("sr")) {
                    c2 = 10;
                    break;
                }
                break;
            case 3681:
                if (optString.equals("st")) {
                    c2 = 1;
                    break;
                }
                break;
            case 3705:
                if (optString.equals("tm")) {
                    c2 = 9;
                    break;
                }
                break;
            case 3710:
                if (optString.equals("tr")) {
                    c2 = 5;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                return a.b(jSONObject, bdVar);
            case 1:
                return ShapeStroke.a.a(jSONObject, bdVar);
            case 2:
                return ar.a.a(jSONObject, bdVar);
            case 3:
                return cd.a.a(jSONObject, bdVar);
            case 4:
                return ap.a.a(jSONObject, bdVar);
            case 5:
                return l.a.a(jSONObject, bdVar);
            case 6:
                return ch.a.a(jSONObject, bdVar);
            case 7:
                return t.a.a(jSONObject, bdVar);
            case 8:
                return bw.a.a(jSONObject, bdVar);
            case 9:
                return ShapeTrimPath.a.a(jSONObject, bdVar);
            case 10:
                return PolystarShape.a.a(jSONObject, bdVar);
            case 11:
                return MergePaths.a.a(jSONObject);
            case 12:
                return bx.a.a(jSONObject, bdVar);
            default:
                Log.w("LOTTIE", "Unknown shape type " + optString);
                return null;
        }
    }

    ce(String str, List<aa> list) {
        this.f2627a = str;
        this.f2628b = list;
    }

    /* compiled from: ShapeGroup */
    static class a {
        /* access modifiers changed from: private */
        public static ce b(JSONObject jSONObject, bd bdVar) {
            JSONArray optJSONArray = jSONObject.optJSONArray("it");
            String optString = jSONObject.optString("nm");
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < optJSONArray.length(); i++) {
                aa a2 = ce.a(optJSONArray.optJSONObject(i), bdVar);
                if (a2 != null) {
                    arrayList.add(a2);
                }
            }
            return new ce(optString, arrayList);
        }
    }

    public String a() {
        return this.f2627a;
    }

    /* access modifiers changed from: package-private */
    public List<aa> b() {
        return this.f2628b;
    }

    public y a(be beVar, q qVar) {
        return new z(beVar, qVar, this);
    }

    public String toString() {
        return "ShapeGroup{name='" + this.f2627a + "' Shapes: " + Arrays.toString(this.f2628b.toArray()) + '}';
    }
}
