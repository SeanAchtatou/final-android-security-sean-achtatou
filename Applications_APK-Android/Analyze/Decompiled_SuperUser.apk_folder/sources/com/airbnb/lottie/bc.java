package com.airbnb.lottie;

import android.support.v4.os.j;

/* compiled from: L */
public class bc {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2520a = false;

    /* renamed from: b  reason: collision with root package name */
    private static String[] f2521b;

    /* renamed from: c  reason: collision with root package name */
    private static long[] f2522c;

    /* renamed from: d  reason: collision with root package name */
    private static int f2523d = 0;

    /* renamed from: e  reason: collision with root package name */
    private static int f2524e = 0;

    static void a(String str) {
        if (f2520a) {
            if (f2523d == 20) {
                f2524e++;
                return;
            }
            f2521b[f2523d] = str;
            f2522c[f2523d] = System.nanoTime();
            j.a(str);
            f2523d++;
        }
    }

    static float b(String str) {
        if (f2524e > 0) {
            f2524e--;
            return 0.0f;
        } else if (!f2520a) {
            return 0.0f;
        } else {
            f2523d--;
            if (f2523d == -1) {
                throw new IllegalStateException("Can't end trace section. There are none.");
            } else if (!str.equals(f2521b[f2523d])) {
                throw new IllegalStateException("Unbalanced trace call " + str + ". Expected " + f2521b[f2523d] + ".");
            } else {
                j.a();
                return ((float) (System.nanoTime() - f2522c[f2523d])) / 1000000.0f;
            }
        }
    }
}
