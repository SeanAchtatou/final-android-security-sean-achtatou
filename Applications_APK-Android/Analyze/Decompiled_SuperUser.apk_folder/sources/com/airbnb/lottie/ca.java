package com.airbnb.lottie;

import com.airbnb.lottie.m;
import org.json.JSONArray;

/* compiled from: ScaleXY */
class ca {

    /* renamed from: a  reason: collision with root package name */
    private final float f2609a;

    /* renamed from: b  reason: collision with root package name */
    private final float f2610b;

    ca(float f2, float f3) {
        this.f2609a = f2;
        this.f2610b = f3;
    }

    ca() {
        this(1.0f, 1.0f);
    }

    /* access modifiers changed from: package-private */
    public float a() {
        return this.f2609a;
    }

    /* access modifiers changed from: package-private */
    public float b() {
        return this.f2610b;
    }

    public String toString() {
        return a() + "x" + b();
    }

    /* compiled from: ScaleXY */
    static class a implements m.a<ca> {

        /* renamed from: a  reason: collision with root package name */
        static final a f2611a = new a();

        private a() {
        }

        /* renamed from: a */
        public ca b(Object obj, float f2) {
            JSONArray jSONArray = (JSONArray) obj;
            return new ca((((float) jSONArray.optDouble(0, 1.0d)) / 100.0f) * f2, (((float) jSONArray.optDouble(1, 1.0d)) / 100.0f) * f2);
        }
    }
}
