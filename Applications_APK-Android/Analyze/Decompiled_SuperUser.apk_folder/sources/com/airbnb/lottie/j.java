package com.airbnb.lottie;

import com.airbnb.lottie.ac;
import com.airbnb.lottie.m;
import com.airbnb.lottie.n;
import java.util.List;
import org.json.JSONObject;

/* compiled from: AnimatableTextFrame */
class j extends o<ac, ac> {
    j(List<ba<ac>> list, ac acVar) {
        super(list, acVar);
    }

    /* renamed from: a */
    public co b() {
        return new co(this.f2696a);
    }

    /* compiled from: AnimatableTextFrame */
    static final class a {
        static j a(JSONObject jSONObject, bd bdVar) {
            if (jSONObject != null && jSONObject.has("x")) {
                bdVar.a("Lottie doesn't support expressions.");
            }
            n.a a2 = n.a(jSONObject, 1.0f, bdVar, b.f2678a).a();
            return new j(a2.f2694a, (ac) a2.f2695b);
        }
    }

    /* compiled from: AnimatableTextFrame */
    private static class b implements m.a<ac> {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final b f2678a = new b();

        private b() {
        }

        /* renamed from: a */
        public ac b(Object obj, float f2) {
            return ac.a.a((JSONObject) obj);
        }
    }
}
