package com.airbnb.lottie;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import java.util.HashMap;
import java.util.Map;

/* compiled from: FontAssetManager */
class ak {

    /* renamed from: a  reason: collision with root package name */
    private final bk<String> f2453a = new bk<>();

    /* renamed from: b  reason: collision with root package name */
    private final Map<bk<String>, Typeface> f2454b = new HashMap();

    /* renamed from: c  reason: collision with root package name */
    private final Map<String, Typeface> f2455c = new HashMap();

    /* renamed from: d  reason: collision with root package name */
    private final AssetManager f2456d;

    /* renamed from: e  reason: collision with root package name */
    private aj f2457e;

    /* renamed from: f  reason: collision with root package name */
    private String f2458f = ".ttf";

    ak(Drawable.Callback callback, aj ajVar) {
        this.f2457e = ajVar;
        if (!(callback instanceof View)) {
            Log.w("LOTTIE", "LottieDrawable must be inside of a view for images to work.");
            this.f2456d = null;
            return;
        }
        this.f2456d = ((View) callback).getContext().getAssets();
    }

    /* access modifiers changed from: package-private */
    public void a(aj ajVar) {
        this.f2457e = ajVar;
    }

    /* access modifiers changed from: package-private */
    public Typeface a(String str, String str2) {
        this.f2453a.a(str, str2);
        Typeface typeface = this.f2454b.get(this.f2453a);
        if (typeface != null) {
            return typeface;
        }
        Typeface a2 = a(a(str), str2);
        this.f2454b.put(this.f2453a, a2);
        return a2;
    }

    private Typeface a(String str) {
        String b2;
        Typeface typeface = this.f2455c.get(str);
        if (typeface == null) {
            typeface = null;
            if (this.f2457e != null) {
                typeface = this.f2457e.a(str);
            }
            if (!(this.f2457e == null || typeface != null || (b2 = this.f2457e.b(str)) == null)) {
                typeface = Typeface.createFromAsset(this.f2456d, b2);
            }
            if (typeface == null) {
                typeface = Typeface.createFromAsset(this.f2456d, "fonts/" + str + this.f2458f);
            }
            this.f2455c.put(str, typeface);
        }
        return typeface;
    }

    private Typeface a(Typeface typeface, String str) {
        int i = 0;
        boolean contains = str.contains("Italic");
        boolean contains2 = str.contains("Bold");
        if (contains && contains2) {
            i = 3;
        } else if (contains) {
            i = 2;
        } else if (contains2) {
            i = 1;
        }
        return typeface.getStyle() == i ? typeface : Typeface.create(typeface, i);
    }
}
