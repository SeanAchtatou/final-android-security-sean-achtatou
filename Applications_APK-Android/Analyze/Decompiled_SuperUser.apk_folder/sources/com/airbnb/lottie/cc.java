package com.airbnb.lottie;

import android.graphics.PointF;
import com.airbnb.lottie.m;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ShapeData */
class cc {

    /* renamed from: a  reason: collision with root package name */
    private final List<ab> f2618a;

    /* renamed from: b  reason: collision with root package name */
    private PointF f2619b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f2620c;

    private cc(PointF pointF, boolean z, List<ab> list) {
        this.f2618a = new ArrayList();
        this.f2619b = pointF;
        this.f2620c = z;
        this.f2618a.addAll(list);
    }

    cc() {
        this.f2618a = new ArrayList();
    }

    private void a(float f2, float f3) {
        if (this.f2619b == null) {
            this.f2619b = new PointF();
        }
        this.f2619b.set(f2, f3);
    }

    /* access modifiers changed from: package-private */
    public PointF a() {
        return this.f2619b;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.f2620c;
    }

    /* access modifiers changed from: package-private */
    public List<ab> c() {
        return this.f2618a;
    }

    /* access modifiers changed from: package-private */
    public void a(cc ccVar, cc ccVar2, float f2) {
        if (this.f2619b == null) {
            this.f2619b = new PointF();
        }
        this.f2620c = ccVar.b() || ccVar2.b();
        if (this.f2618a.isEmpty() || this.f2618a.size() == ccVar.c().size() || this.f2618a.size() == ccVar2.c().size()) {
            if (this.f2618a.isEmpty()) {
                for (int size = ccVar.c().size() - 1; size >= 0; size--) {
                    this.f2618a.add(new ab());
                }
            }
            PointF a2 = ccVar.a();
            PointF a3 = ccVar2.a();
            a(bj.a(a2.x, a3.x, f2), bj.a(a2.y, a3.y, f2));
            for (int size2 = this.f2618a.size() - 1; size2 >= 0; size2--) {
                ab abVar = ccVar.c().get(size2);
                ab abVar2 = ccVar2.c().get(size2);
                PointF a4 = abVar.a();
                PointF b2 = abVar.b();
                PointF c2 = abVar.c();
                PointF a5 = abVar2.a();
                PointF b3 = abVar2.b();
                PointF c3 = abVar2.c();
                this.f2618a.get(size2).a(bj.a(a4.x, a5.x, f2), bj.a(a4.y, a5.y, f2));
                this.f2618a.get(size2).b(bj.a(b2.x, b3.x, f2), bj.a(b2.y, b3.y, f2));
                this.f2618a.get(size2).c(bj.a(c2.x, c3.x, f2), bj.a(c2.y, c3.y, f2));
            }
            return;
        }
        throw new IllegalStateException("Curves must have the same number of control points. This: " + c().size() + "\tShape 1: " + ccVar.c().size() + "\tShape 2: " + ccVar2.c().size());
    }

    public String toString() {
        return "ShapeData{numCurves=" + this.f2618a.size() + "closed=" + this.f2620c + '}';
    }

    /* compiled from: ShapeData */
    static class a implements m.a<cc> {

        /* renamed from: a  reason: collision with root package name */
        static final a f2621a = new a();

        private a() {
        }

        /* renamed from: a */
        public cc b(Object obj, float f2) {
            JSONObject jSONObject;
            JSONObject jSONObject2 = null;
            if (obj instanceof JSONArray) {
                Object opt = ((JSONArray) obj).opt(0);
                if (!(opt instanceof JSONObject) || !((JSONObject) opt).has("v")) {
                    jSONObject = null;
                } else {
                    jSONObject = (JSONObject) opt;
                }
                jSONObject2 = jSONObject;
            } else if ((obj instanceof JSONObject) && ((JSONObject) obj).has("v")) {
                jSONObject2 = (JSONObject) obj;
            }
            if (jSONObject2 == null) {
                return null;
            }
            JSONArray optJSONArray = jSONObject2.optJSONArray("v");
            JSONArray optJSONArray2 = jSONObject2.optJSONArray("i");
            JSONArray optJSONArray3 = jSONObject2.optJSONArray("o");
            boolean optBoolean = jSONObject2.optBoolean("c", false);
            if (optJSONArray == null || optJSONArray2 == null || optJSONArray3 == null || optJSONArray.length() != optJSONArray2.length() || optJSONArray.length() != optJSONArray3.length()) {
                throw new IllegalStateException("Unable to process points array or tangents. " + jSONObject2);
            } else if (optJSONArray.length() == 0) {
                return new cc(new PointF(), false, Collections.emptyList());
            } else {
                int length = optJSONArray.length();
                PointF a2 = a(0, optJSONArray);
                a2.x *= f2;
                a2.y *= f2;
                ArrayList arrayList = new ArrayList(length);
                for (int i = 1; i < length; i++) {
                    PointF a3 = a(i, optJSONArray);
                    PointF a4 = a(i - 1, optJSONArray);
                    PointF a5 = a(i - 1, optJSONArray3);
                    PointF a6 = a(i, optJSONArray2);
                    PointF a7 = bj.a(a4, a5);
                    PointF a8 = bj.a(a3, a6);
                    a7.x *= f2;
                    a7.y *= f2;
                    a8.x *= f2;
                    a8.y *= f2;
                    a3.x *= f2;
                    a3.y *= f2;
                    arrayList.add(new ab(a7, a8, a3));
                }
                if (optBoolean) {
                    PointF a9 = a(0, optJSONArray);
                    PointF a10 = a(length - 1, optJSONArray);
                    PointF a11 = a(length - 1, optJSONArray3);
                    PointF a12 = a(0, optJSONArray2);
                    PointF a13 = bj.a(a10, a11);
                    PointF a14 = bj.a(a9, a12);
                    if (f2 != 1.0f) {
                        a13.x *= f2;
                        a13.y *= f2;
                        a14.x *= f2;
                        a14.y *= f2;
                        a9.x *= f2;
                        a9.y *= f2;
                    }
                    arrayList.add(new ab(a13, a14, a9));
                }
                return new cc(a2, optBoolean, arrayList);
            }
        }

        private static PointF a(int i, JSONArray jSONArray) {
            if (i >= jSONArray.length()) {
                throw new IllegalArgumentException("Invalid index " + i + ". There are only " + jSONArray.length() + " points.");
            }
            JSONArray optJSONArray = jSONArray.optJSONArray(i);
            Object opt = optJSONArray.opt(0);
            Object opt2 = optJSONArray.opt(1);
            return new PointF(opt instanceof Double ? new Float(((Double) opt).doubleValue()).floatValue() : (float) ((Integer) opt).intValue(), opt2 instanceof Double ? new Float(((Double) opt2).doubleValue()).floatValue() : (float) ((Integer) opt2).intValue());
        }
    }
}
