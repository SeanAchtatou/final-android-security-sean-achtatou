package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.RectF;

/* compiled from: DrawingContent */
interface ad extends y {
    void a(Canvas canvas, Matrix matrix, int i);

    void a(RectF rectF, Matrix matrix);

    void a(String str, String str2, ColorFilter colorFilter);
}
