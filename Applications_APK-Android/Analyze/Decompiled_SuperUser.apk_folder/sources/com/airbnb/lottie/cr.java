package com.airbnb.lottie;

import com.airbnb.lottie.ShapeTrimPath;
import com.airbnb.lottie.p;
import java.util.ArrayList;
import java.util.List;

/* compiled from: TrimPathContent */
class cr implements p.a, y {

    /* renamed from: a  reason: collision with root package name */
    private String f2660a;

    /* renamed from: b  reason: collision with root package name */
    private final List<p.a> f2661b = new ArrayList();

    /* renamed from: c  reason: collision with root package name */
    private final ShapeTrimPath.Type f2662c;

    /* renamed from: d  reason: collision with root package name */
    private final p<?, Float> f2663d;

    /* renamed from: e  reason: collision with root package name */
    private final p<?, Float> f2664e;

    /* renamed from: f  reason: collision with root package name */
    private final p<?, Float> f2665f;

    cr(q qVar, ShapeTrimPath shapeTrimPath) {
        this.f2660a = shapeTrimPath.a();
        this.f2662c = shapeTrimPath.b();
        this.f2663d = shapeTrimPath.d().b();
        this.f2664e = shapeTrimPath.c().b();
        this.f2665f = shapeTrimPath.e().b();
        qVar.a(this.f2663d);
        qVar.a(this.f2664e);
        qVar.a(this.f2665f);
        this.f2663d.a(this);
        this.f2664e.a(this);
        this.f2665f.a(this);
    }

    public void a() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f2661b.size()) {
                this.f2661b.get(i2).a();
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void a(List<y> list, List<y> list2) {
    }

    public String e() {
        return this.f2660a;
    }

    /* access modifiers changed from: package-private */
    public void a(p.a aVar) {
        this.f2661b.add(aVar);
    }

    /* access modifiers changed from: package-private */
    public ShapeTrimPath.Type b() {
        return this.f2662c;
    }

    public p<?, Float> c() {
        return this.f2663d;
    }

    public p<?, Float> d() {
        return this.f2664e;
    }

    public p<?, Float> f() {
        return this.f2665f;
    }
}
