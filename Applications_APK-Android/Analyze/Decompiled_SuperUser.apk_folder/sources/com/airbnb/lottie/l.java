package com.airbnb.lottie;

import android.graphics.PointF;
import android.util.Log;
import com.airbnb.lottie.b;
import com.airbnb.lottie.d;
import com.airbnb.lottie.g;
import java.util.Collections;
import org.json.JSONObject;

/* compiled from: AnimatableTransform */
class l implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final e f2683a;

    /* renamed from: b  reason: collision with root package name */
    private final m<PointF> f2684b;

    /* renamed from: c  reason: collision with root package name */
    private final g f2685c;

    /* renamed from: d  reason: collision with root package name */
    private final b f2686d;

    /* renamed from: e  reason: collision with root package name */
    private final d f2687e;

    /* renamed from: f  reason: collision with root package name */
    private final b f2688f;

    /* renamed from: g  reason: collision with root package name */
    private final b f2689g;

    private l(e eVar, m<PointF> mVar, g gVar, b bVar, d dVar, b bVar2, b bVar3) {
        this.f2683a = eVar;
        this.f2684b = mVar;
        this.f2685c = gVar;
        this.f2686d = bVar;
        this.f2687e = dVar;
        this.f2688f = bVar2;
        this.f2689g = bVar3;
    }

    /* access modifiers changed from: package-private */
    public e a() {
        return this.f2683a;
    }

    /* access modifiers changed from: package-private */
    public m<PointF> b() {
        return this.f2684b;
    }

    /* access modifiers changed from: package-private */
    public g c() {
        return this.f2685c;
    }

    /* access modifiers changed from: package-private */
    public b d() {
        return this.f2686d;
    }

    /* access modifiers changed from: package-private */
    public d e() {
        return this.f2687e;
    }

    public b f() {
        return this.f2688f;
    }

    public b g() {
        return this.f2689g;
    }

    public cq h() {
        return new cq(this);
    }

    public y a(be beVar, q qVar) {
        return null;
    }

    /* compiled from: AnimatableTransform */
    static class a {
        static l a() {
            return new l(new e(), new e(), g.a.a(), b.a.a(), d.a.a(), b.a.a(), b.a.a());
        }

        static l a(JSONObject jSONObject, bd bdVar) {
            e eVar;
            m<PointF> mVar;
            g gVar;
            b bVar;
            d dVar;
            b bVar2;
            b bVar3;
            JSONObject optJSONObject = jSONObject.optJSONObject("a");
            if (optJSONObject != null) {
                eVar = new e(optJSONObject.opt("k"), bdVar);
            } else {
                Log.w("LOTTIE", "Layer has no transform property. You may be using an unsupported layer type such as a camera.");
                eVar = new e();
            }
            JSONObject optJSONObject2 = jSONObject.optJSONObject("p");
            if (optJSONObject2 != null) {
                mVar = e.a(optJSONObject2, bdVar);
            } else {
                a("position");
                mVar = null;
            }
            JSONObject optJSONObject3 = jSONObject.optJSONObject("s");
            if (optJSONObject3 != null) {
                gVar = g.a.a(optJSONObject3, bdVar);
            } else {
                gVar = new g(Collections.emptyList(), new ca());
            }
            JSONObject optJSONObject4 = jSONObject.optJSONObject("r");
            if (optJSONObject4 == null) {
                optJSONObject4 = jSONObject.optJSONObject("rz");
            }
            if (optJSONObject4 != null) {
                bVar = b.a.a(optJSONObject4, bdVar, false);
            } else {
                a("rotation");
                bVar = null;
            }
            JSONObject optJSONObject5 = jSONObject.optJSONObject("o");
            if (optJSONObject5 != null) {
                dVar = d.a.a(optJSONObject5, bdVar);
            } else {
                dVar = new d(Collections.emptyList(), 100);
            }
            JSONObject optJSONObject6 = jSONObject.optJSONObject("so");
            if (optJSONObject6 != null) {
                bVar2 = b.a.a(optJSONObject6, bdVar, false);
            } else {
                bVar2 = null;
            }
            JSONObject optJSONObject7 = jSONObject.optJSONObject("eo");
            if (optJSONObject7 != null) {
                bVar3 = b.a.a(optJSONObject7, bdVar, false);
            } else {
                bVar3 = null;
            }
            return new l(eVar, mVar, gVar, bVar, dVar, bVar2, bVar3);
        }

        private static void a(String str) {
            throw new IllegalArgumentException("Missing transform for " + str);
        }
    }
}
