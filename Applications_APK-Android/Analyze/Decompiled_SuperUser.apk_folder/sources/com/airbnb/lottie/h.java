package com.airbnb.lottie;

import android.graphics.Path;
import com.airbnb.lottie.cc;
import com.airbnb.lottie.n;
import java.util.List;
import org.json.JSONObject;

/* compiled from: AnimatableShapeValue */
class h extends o<cc, Path> {

    /* renamed from: c  reason: collision with root package name */
    private final Path f2675c;

    private h(List<ba<cc>> list, cc ccVar) {
        super(list, ccVar);
        this.f2675c = new Path();
    }

    public p<?, Path> b() {
        if (!e()) {
            return new cl(a((cc) this.f2697b));
        }
        return new cf(this.f2696a);
    }

    /* access modifiers changed from: package-private */
    public Path a(cc ccVar) {
        this.f2675c.reset();
        bj.a(ccVar, this.f2675c);
        return this.f2675c;
    }

    /* compiled from: AnimatableShapeValue */
    static final class a {
        static h a(JSONObject jSONObject, bd bdVar) {
            n.a a2 = n.a(jSONObject, bdVar.n(), bdVar, cc.a.f2621a).a();
            return new h(a2.f2694a, (cc) a2.f2695b);
        }
    }
}
