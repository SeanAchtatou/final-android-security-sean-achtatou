package com.airbnb.lottie;

import com.airbnb.lottie.b;
import com.airbnb.lottie.l;
import org.json.JSONObject;

/* compiled from: Repeater */
class bx implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2596a;

    /* renamed from: b  reason: collision with root package name */
    private final b f2597b;

    /* renamed from: c  reason: collision with root package name */
    private final b f2598c;

    /* renamed from: d  reason: collision with root package name */
    private final l f2599d;

    bx(String str, b bVar, b bVar2, l lVar) {
        this.f2596a = str;
        this.f2597b = bVar;
        this.f2598c = bVar2;
        this.f2599d = lVar;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f2596a;
    }

    /* access modifiers changed from: package-private */
    public b b() {
        return this.f2597b;
    }

    /* access modifiers changed from: package-private */
    public b c() {
        return this.f2598c;
    }

    /* access modifiers changed from: package-private */
    public l d() {
        return this.f2599d;
    }

    public y a(be beVar, q qVar) {
        return new by(beVar, qVar, this);
    }

    /* compiled from: Repeater */
    static final class a {
        static bx a(JSONObject jSONObject, bd bdVar) {
            return new bx(jSONObject.optString("nm"), b.a.a(jSONObject.optJSONObject("c"), bdVar, false), b.a.a(jSONObject.optJSONObject("o"), bdVar, false), l.a.a(jSONObject.optJSONObject("tr"), bdVar));
        }
    }
}
