package com.airbnb.lottie;

import com.airbnb.lottie.ca;
import com.airbnb.lottie.n;
import java.util.List;
import org.json.JSONObject;

/* compiled from: AnimatableScaleValue */
class g extends o<ca, ca> {
    private g() {
        super(new ca());
    }

    g(List<ba<ca>> list, ca caVar) {
        super(list, caVar);
    }

    /* renamed from: a */
    public bb<ca> b() {
        if (!e()) {
            return new cl(this.f2697b);
        }
        return new bz(this.f2696a);
    }

    /* compiled from: AnimatableScaleValue */
    static final class a {
        static g a(JSONObject jSONObject, bd bdVar) {
            n.a a2 = n.a(jSONObject, 1.0f, bdVar, ca.a.f2611a).a();
            return new g(a2.f2694a, (ca) a2.f2695b);
        }

        static g a() {
            return new g();
        }
    }
}
