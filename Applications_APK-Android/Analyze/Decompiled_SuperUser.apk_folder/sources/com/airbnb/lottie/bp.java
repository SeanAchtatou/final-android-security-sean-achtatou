package com.airbnb.lottie;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import java.util.List;

/* compiled from: PathKeyframeAnimation */
class bp extends bb<PointF> {

    /* renamed from: b  reason: collision with root package name */
    private final PointF f2564b = new PointF();

    /* renamed from: c  reason: collision with root package name */
    private final float[] f2565c = new float[2];

    /* renamed from: d  reason: collision with root package name */
    private bo f2566d;

    /* renamed from: e  reason: collision with root package name */
    private PathMeasure f2567e;

    bp(List<? extends ba<PointF>> list) {
        super(list);
    }

    /* renamed from: b */
    public PointF a(ba<PointF> baVar, float f2) {
        bo boVar = (bo) baVar;
        Path e2 = boVar.e();
        if (e2 == null) {
            return (PointF) baVar.f2512a;
        }
        if (this.f2566d != boVar) {
            this.f2567e = new PathMeasure(e2, false);
            this.f2566d = boVar;
        }
        this.f2567e.getPosTan(this.f2567e.getLength() * f2, this.f2565c, null);
        this.f2564b.set(this.f2565c[0], this.f2565c[1]);
        return this.f2564b;
    }
}
