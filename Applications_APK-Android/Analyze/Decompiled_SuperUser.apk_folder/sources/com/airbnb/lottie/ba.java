package com.airbnb.lottie;

import android.graphics.PointF;
import android.support.v4.util.j;
import android.support.v4.view.animation.b;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import com.airbnb.lottie.m;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: Keyframe */
class ba<T> {
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public static final Interpolator f2511f = new LinearInterpolator();

    /* renamed from: a  reason: collision with root package name */
    final T f2512a;

    /* renamed from: b  reason: collision with root package name */
    final T f2513b;

    /* renamed from: c  reason: collision with root package name */
    final Interpolator f2514c;

    /* renamed from: d  reason: collision with root package name */
    final float f2515d;

    /* renamed from: e  reason: collision with root package name */
    Float f2516e;

    /* renamed from: g  reason: collision with root package name */
    private final bd f2517g;

    /* renamed from: h  reason: collision with root package name */
    private float f2518h = Float.MIN_VALUE;
    private float i = Float.MIN_VALUE;

    static void a(List<? extends ba<?>> list) {
        int size = list.size();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= size - 1) {
                break;
            }
            ((ba) list.get(i3)).f2516e = Float.valueOf(((ba) list.get(i3 + 1)).f2515d);
            i2 = i3 + 1;
        }
        ba baVar = (ba) list.get(size - 1);
        if (baVar.f2512a == null) {
            list.remove(baVar);
        }
    }

    public ba(bd bdVar, T t, T t2, Interpolator interpolator, float f2, Float f3) {
        this.f2517g = bdVar;
        this.f2512a = t;
        this.f2513b = t2;
        this.f2514c = interpolator;
        this.f2515d = f2;
        this.f2516e = f3;
    }

    /* access modifiers changed from: package-private */
    public float a() {
        if (this.f2518h == Float.MIN_VALUE) {
            this.f2518h = (this.f2515d - ((float) this.f2517g.g())) / this.f2517g.m();
        }
        return this.f2518h;
    }

    /* access modifiers changed from: package-private */
    public float b() {
        if (this.i == Float.MIN_VALUE) {
            if (this.f2516e == null) {
                this.i = 1.0f;
            } else {
                this.i = a() + ((this.f2516e.floatValue() - this.f2515d) / this.f2517g.m());
            }
        }
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.f2514c == null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(float f2) {
        return f2 >= a() && f2 <= b();
    }

    public String toString() {
        return "Keyframe{startValue=" + ((Object) this.f2512a) + ", endValue=" + ((Object) this.f2513b) + ", startFrame=" + this.f2515d + ", endFrame=" + this.f2516e + ", interpolator=" + this.f2514c + '}';
    }

    /* compiled from: Keyframe */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        private static final j<WeakReference<Interpolator>> f2519a = new j<>();

        static <T> ba<T> a(JSONObject jSONObject, bd bdVar, float f2, m.a<T> aVar) {
            T b2;
            Interpolator interpolator;
            T t;
            T t2;
            T t3;
            PointF pointF;
            PointF pointF2;
            Interpolator d2;
            float f3 = 0.0f;
            if (jSONObject.has("t")) {
                f3 = (float) jSONObject.optDouble("t", 0.0d);
                Object opt = jSONObject.opt("s");
                if (opt != null) {
                    t2 = aVar.b(opt, f2);
                } else {
                    t2 = null;
                }
                Object opt2 = jSONObject.opt("e");
                if (opt2 != null) {
                    t3 = aVar.b(opt2, f2);
                } else {
                    t3 = null;
                }
                JSONObject optJSONObject = jSONObject.optJSONObject("o");
                JSONObject optJSONObject2 = jSONObject.optJSONObject("i");
                if (optJSONObject == null || optJSONObject2 == null) {
                    pointF = null;
                    pointF2 = null;
                } else {
                    PointF a2 = az.a(optJSONObject, f2);
                    PointF a3 = az.a(optJSONObject2, f2);
                    pointF2 = a2;
                    pointF = a3;
                }
                if (jSONObject.optInt("h", 0) == 1) {
                    d2 = ba.f2511f;
                    t3 = t2;
                } else if (pointF2 != null) {
                    pointF2.x = bj.b(pointF2.x, -f2, f2);
                    pointF2.y = bj.b(pointF2.y, -100.0f, 100.0f);
                    pointF.x = bj.b(pointF.x, -f2, f2);
                    pointF.y = bj.b(pointF.y, -100.0f, 100.0f);
                    int a4 = cs.a(pointF2.x, pointF2.y, pointF.x, pointF.y);
                    WeakReference a5 = f2519a.a(a4);
                    if (a5 == null || a5.get() == null) {
                        d2 = b.a(pointF2.x / f2, pointF2.y / f2, pointF.x / f2, pointF.y / f2);
                        f2519a.b(a4, new WeakReference(d2));
                    } else {
                        d2 = (Interpolator) f2519a.a(a4).get();
                    }
                } else {
                    d2 = ba.f2511f;
                }
                interpolator = d2;
                b2 = t3;
                t = t2;
            } else {
                b2 = aVar.b(jSONObject, f2);
                interpolator = null;
                t = b2;
            }
            return new ba<>(bdVar, t, b2, interpolator, f3, null);
        }

        static <T> List<ba<T>> a(JSONArray jSONArray, bd bdVar, float f2, m.a<T> aVar) {
            int length = jSONArray.length();
            if (length == 0) {
                return Collections.emptyList();
            }
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < length; i++) {
                arrayList.add(a(jSONArray.optJSONObject(i), bdVar, f2, aVar));
            }
            ba.a(arrayList);
            return arrayList;
        }
    }
}
