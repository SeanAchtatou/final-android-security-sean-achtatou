package com.airbnb.lottie;

import android.graphics.Color;
import com.airbnb.lottie.m;
import org.json.JSONArray;

/* compiled from: ColorFactory */
class u implements m.a<Integer> {

    /* renamed from: a  reason: collision with root package name */
    static final u f2728a = new u();

    u() {
    }

    /* renamed from: a */
    public Integer b(Object obj, float f2) {
        JSONArray jSONArray = (JSONArray) obj;
        if (jSONArray.length() != 4) {
            return -16777216;
        }
        boolean z = true;
        for (int i = 0; i < jSONArray.length(); i++) {
            if (jSONArray.optDouble(i) > 1.0d) {
                z = false;
            }
        }
        float f3 = z ? 255.0f : 1.0f;
        return Integer.valueOf(Color.argb((int) (jSONArray.optDouble(3) * ((double) f3)), (int) (jSONArray.optDouble(0) * ((double) f3)), (int) (jSONArray.optDouble(1) * ((double) f3)), (int) (jSONArray.optDouble(2) * ((double) f3))));
    }
}
