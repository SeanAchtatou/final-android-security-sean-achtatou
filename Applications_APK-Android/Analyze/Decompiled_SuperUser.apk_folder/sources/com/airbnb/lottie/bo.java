package com.airbnb.lottie;

import android.graphics.Path;
import android.graphics.PointF;
import android.view.animation.Interpolator;
import com.airbnb.lottie.ba;
import com.airbnb.lottie.m;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: PathKeyframe */
class bo extends ba<PointF> {
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public Path f2563f;

    private bo(bd bdVar, PointF pointF, PointF pointF2, Interpolator interpolator, float f2, Float f3) {
        super(bdVar, pointF, pointF2, interpolator, f2, f3);
    }

    /* compiled from: PathKeyframe */
    static class a {
        static bo a(JSONObject jSONObject, bd bdVar, m.a<PointF> aVar) {
            PointF pointF;
            PointF pointF2;
            ba a2 = ba.a.a(jSONObject, bdVar, bdVar.n(), aVar);
            JSONArray optJSONArray = jSONObject.optJSONArray("ti");
            JSONArray optJSONArray2 = jSONObject.optJSONArray("to");
            if (optJSONArray == null || optJSONArray2 == null) {
                pointF = null;
                pointF2 = null;
            } else {
                PointF a3 = az.a(optJSONArray2, bdVar.n());
                pointF = az.a(optJSONArray, bdVar.n());
                pointF2 = a3;
            }
            bo boVar = new bo(bdVar, (PointF) a2.f2512a, (PointF) a2.f2513b, a2.f2514c, a2.f2515d, a2.f2516e);
            boolean z = (a2.f2513b == null || a2.f2512a == null || !((PointF) a2.f2512a).equals(((PointF) a2.f2513b).x, ((PointF) a2.f2513b).y)) ? false : true;
            if (boVar.f2513b != null && !z) {
                Path unused = boVar.f2563f = cs.a((PointF) a2.f2512a, (PointF) a2.f2513b, pointF2, pointF);
            }
            return boVar;
        }
    }

    /* access modifiers changed from: package-private */
    public Path e() {
        return this.f2563f;
    }
}
