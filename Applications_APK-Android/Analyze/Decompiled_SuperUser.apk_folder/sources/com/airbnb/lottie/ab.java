package com.airbnb.lottie;

import android.graphics.PointF;

/* compiled from: CubicCurveData */
class ab {

    /* renamed from: a  reason: collision with root package name */
    private final PointF f2422a;

    /* renamed from: b  reason: collision with root package name */
    private final PointF f2423b;

    /* renamed from: c  reason: collision with root package name */
    private final PointF f2424c;

    ab() {
        this.f2422a = new PointF();
        this.f2423b = new PointF();
        this.f2424c = new PointF();
    }

    ab(PointF pointF, PointF pointF2, PointF pointF3) {
        this.f2422a = pointF;
        this.f2423b = pointF2;
        this.f2424c = pointF3;
    }

    /* access modifiers changed from: package-private */
    public void a(float f2, float f3) {
        this.f2422a.set(f2, f3);
    }

    /* access modifiers changed from: package-private */
    public PointF a() {
        return this.f2422a;
    }

    /* access modifiers changed from: package-private */
    public void b(float f2, float f3) {
        this.f2423b.set(f2, f3);
    }

    /* access modifiers changed from: package-private */
    public PointF b() {
        return this.f2423b;
    }

    /* access modifiers changed from: package-private */
    public void c(float f2, float f3) {
        this.f2424c.set(f2, f3);
    }

    /* access modifiers changed from: package-private */
    public PointF c() {
        return this.f2424c;
    }
}
