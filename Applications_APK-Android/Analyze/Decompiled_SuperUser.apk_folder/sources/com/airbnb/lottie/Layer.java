package com.airbnb.lottie;

import android.graphics.Color;
import android.graphics.Rect;
import com.airbnb.lottie.Mask;
import com.airbnb.lottie.b;
import com.airbnb.lottie.j;
import com.airbnb.lottie.k;
import com.airbnb.lottie.l;
import com.kingouser.com.util.ShellUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

class Layer {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2326a = Layer.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final List<aa> f2327b;

    /* renamed from: c  reason: collision with root package name */
    private final bd f2328c;

    /* renamed from: d  reason: collision with root package name */
    private final String f2329d;

    /* renamed from: e  reason: collision with root package name */
    private final long f2330e;

    /* renamed from: f  reason: collision with root package name */
    private final LayerType f2331f;

    /* renamed from: g  reason: collision with root package name */
    private final long f2332g;

    /* renamed from: h  reason: collision with root package name */
    private final String f2333h;
    private final List<Mask> i;
    private final l j;
    private final int k;
    private final int l;
    private final int m;
    private final float n;
    private final float o;
    private final int p;
    private final int q;
    private final j r;
    private final k s;
    private final b t;
    private final List<ba<Float>> u;
    private final MatteType v;

    enum LayerType {
        PreComp,
        Solid,
        Image,
        Null,
        Shape,
        Text,
        Unknown
    }

    enum MatteType {
        None,
        Add,
        Invert,
        Unknown
    }

    private Layer(List<aa> list, bd bdVar, String str, long j2, LayerType layerType, long j3, String str2, List<Mask> list2, l lVar, int i2, int i3, int i4, float f2, float f3, int i5, int i6, j jVar, k kVar, List<ba<Float>> list3, MatteType matteType, b bVar) {
        this.f2327b = list;
        this.f2328c = bdVar;
        this.f2329d = str;
        this.f2330e = j2;
        this.f2331f = layerType;
        this.f2332g = j3;
        this.f2333h = str2;
        this.i = list2;
        this.j = lVar;
        this.k = i2;
        this.l = i3;
        this.m = i4;
        this.n = f2;
        this.o = f3;
        this.p = i5;
        this.q = i6;
        this.r = jVar;
        this.s = kVar;
        this.u = list3;
        this.v = matteType;
        this.t = bVar;
    }

    /* access modifiers changed from: package-private */
    public bd a() {
        return this.f2328c;
    }

    /* access modifiers changed from: package-private */
    public float b() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public float c() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public List<ba<Float>> d() {
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public long e() {
        return this.f2330e;
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.f2329d;
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return this.f2333h;
    }

    /* access modifiers changed from: package-private */
    public int h() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public int i() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public List<Mask> j() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public LayerType k() {
        return this.f2331f;
    }

    /* access modifiers changed from: package-private */
    public MatteType l() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public long m() {
        return this.f2332g;
    }

    /* access modifiers changed from: package-private */
    public List<aa> n() {
        return this.f2327b;
    }

    /* access modifiers changed from: package-private */
    public l o() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public int p() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public int q() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public int r() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public j s() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public k t() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public b u() {
        return this.t;
    }

    public String toString() {
        return a("");
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str).append(f()).append(ShellUtils.COMMAND_LINE_END);
        Layer a2 = this.f2328c.a(m());
        if (a2 != null) {
            sb.append("\t\tParents: ").append(a2.f());
            Layer a3 = this.f2328c.a(a2.m());
            while (a3 != null) {
                sb.append("->").append(a3.f());
                a3 = this.f2328c.a(a3.m());
            }
            sb.append(str).append(ShellUtils.COMMAND_LINE_END);
        }
        if (!j().isEmpty()) {
            sb.append(str).append("\tMasks: ").append(j().size()).append(ShellUtils.COMMAND_LINE_END);
        }
        if (!(r() == 0 || q() == 0)) {
            sb.append(str).append("\tBackground: ").append(String.format(Locale.US, "%dx%d %X\n", Integer.valueOf(r()), Integer.valueOf(q()), Integer.valueOf(p())));
        }
        if (!this.f2327b.isEmpty()) {
            sb.append(str).append("\tShapes:\n");
            for (aa append : this.f2327b) {
                sb.append(str).append("\t\t").append(append).append(ShellUtils.COMMAND_LINE_END);
            }
        }
        return sb.toString();
    }

    static class a {
        static Layer a(bd bdVar) {
            Rect b2 = bdVar.b();
            return new Layer(Collections.emptyList(), bdVar, "root", -1, LayerType.PreComp, -1, null, Collections.emptyList(), l.a.a(), 0, 0, 0, 0.0f, 0.0f, b2.width(), b2.height(), null, null, Collections.emptyList(), MatteType.None, null);
        }

        static Layer a(JSONObject jSONObject, bd bdVar) {
            LayerType layerType;
            String optString = jSONObject.optString("nm");
            String optString2 = jSONObject.optString("refId");
            if (optString.endsWith(".ai") || jSONObject.optString("cl", "").equals("ai")) {
                bdVar.a("Convert your Illustrator layers to shape layers.");
            }
            long optLong = jSONObject.optLong("ind");
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int optInt = jSONObject.optInt("ty", -1);
            if (optInt < LayerType.Unknown.ordinal()) {
                layerType = LayerType.values()[optInt];
            } else {
                layerType = LayerType.Unknown;
            }
            if (layerType == LayerType.Text && !cs.a(bdVar, 4, 8, 0)) {
                layerType = LayerType.Unknown;
                bdVar.a("Text is only supported on bodymovin >= 4.8.0");
            }
            LayerType layerType2 = layerType;
            long optLong2 = jSONObject.optLong("parent", -1);
            if (layerType2 == LayerType.Solid) {
                i = (int) (((float) jSONObject.optInt("sw")) * bdVar.n());
                i2 = (int) (((float) jSONObject.optInt(ShellUtils.COMMAND_SH)) * bdVar.n());
                i3 = Color.parseColor(jSONObject.optString("sc"));
            }
            l a2 = l.a.a(jSONObject.optJSONObject("ks"), bdVar);
            MatteType matteType = MatteType.values()[jSONObject.optInt("tt")];
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            JSONArray optJSONArray = jSONObject.optJSONArray("masksProperties");
            if (optJSONArray != null) {
                for (int i6 = 0; i6 < optJSONArray.length(); i6++) {
                    arrayList.add(Mask.a.a(optJSONArray.optJSONObject(i6), bdVar));
                }
            }
            ArrayList arrayList3 = new ArrayList();
            JSONArray optJSONArray2 = jSONObject.optJSONArray("shapes");
            if (optJSONArray2 != null) {
                for (int i7 = 0; i7 < optJSONArray2.length(); i7++) {
                    aa a3 = ce.a(optJSONArray2.optJSONObject(i7), bdVar);
                    if (a3 != null) {
                        arrayList3.add(a3);
                    }
                }
            }
            j jVar = null;
            k kVar = null;
            JSONObject optJSONObject = jSONObject.optJSONObject("t");
            if (optJSONObject != null) {
                jVar = j.a.a(optJSONObject.optJSONObject("d"), bdVar);
                kVar = k.a.a(optJSONObject.optJSONArray("a").optJSONObject(0), bdVar);
            }
            if (jSONObject.has("ef")) {
                bdVar.a("Lottie doesn't support layer effects. If you are using them for  fills, strokes, trim paths etc. then try adding them directly as contents  in your shape.");
            }
            float optDouble = (float) jSONObject.optDouble("sr", 1.0d);
            float optDouble2 = ((float) jSONObject.optDouble("st")) / bdVar.m();
            if (layerType2 == LayerType.PreComp) {
                i4 = (int) (((float) jSONObject.optInt("w")) * bdVar.n());
                i5 = (int) (((float) jSONObject.optInt("h")) * bdVar.n());
            }
            float optLong3 = ((float) jSONObject.optLong("ip")) / optDouble;
            float optLong4 = ((float) jSONObject.optLong("op")) / optDouble;
            if (optLong3 > 0.0f) {
                ArrayList arrayList4 = arrayList2;
                arrayList4.add(new ba(bdVar, Float.valueOf(0.0f), Float.valueOf(0.0f), null, 0.0f, Float.valueOf(optLong3)));
            }
            if (optLong4 <= 0.0f) {
                optLong4 = (float) (bdVar.h() + 1);
            }
            ArrayList arrayList5 = arrayList2;
            arrayList5.add(new ba(bdVar, Float.valueOf(1.0f), Float.valueOf(1.0f), null, optLong3, Float.valueOf(optLong4)));
            ArrayList arrayList6 = arrayList2;
            arrayList6.add(new ba(bdVar, Float.valueOf(0.0f), Float.valueOf(0.0f), null, optLong4, Float.valueOf(Float.MAX_VALUE)));
            b bVar = null;
            if (jSONObject.has("tm")) {
                bVar = b.a.a(jSONObject.optJSONObject("tm"), bdVar, false);
            }
            return new Layer(arrayList3, bdVar, optString, optLong, layerType2, optLong2, optString2, arrayList, a2, i, i2, i3, optDouble, optDouble2, i4, i5, jVar, kVar, arrayList2, matteType, bVar);
        }
    }
}
