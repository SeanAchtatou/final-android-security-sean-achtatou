package com.airbnb.lottie;

/* compiled from: GradientColor */
class an {

    /* renamed from: a  reason: collision with root package name */
    private final float[] f2465a;

    /* renamed from: b  reason: collision with root package name */
    private final int[] f2466b;

    an(float[] fArr, int[] iArr) {
        this.f2465a = fArr;
        this.f2466b = iArr;
    }

    /* access modifiers changed from: package-private */
    public float[] a() {
        return this.f2465a;
    }

    /* access modifiers changed from: package-private */
    public int[] b() {
        return this.f2466b;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.f2466b.length;
    }

    /* access modifiers changed from: package-private */
    public void a(an anVar, an anVar2, float f2) {
        if (anVar.f2466b.length != anVar2.f2466b.length) {
            throw new IllegalArgumentException("Cannot interpolate between gradients. Lengths vary (" + anVar.f2466b.length + " vs " + anVar2.f2466b.length + ")");
        }
        for (int i = 0; i < anVar.f2466b.length; i++) {
            this.f2465a[i] = bj.a(anVar.f2465a[i], anVar2.f2465a[i], f2);
            this.f2466b[i] = am.a(f2, anVar.f2466b[i], anVar2.f2466b[i]);
        }
    }
}
