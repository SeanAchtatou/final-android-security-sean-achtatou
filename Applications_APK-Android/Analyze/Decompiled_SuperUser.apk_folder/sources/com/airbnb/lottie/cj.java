package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;

/* compiled from: SolidLayer */
class cj extends q {

    /* renamed from: e  reason: collision with root package name */
    private final RectF f2635e = new RectF();

    /* renamed from: f  reason: collision with root package name */
    private final Paint f2636f = new Paint();

    /* renamed from: g  reason: collision with root package name */
    private final Layer f2637g;

    cj(be beVar, Layer layer) {
        super(beVar, layer);
        this.f2637g = layer;
        this.f2636f.setAlpha(0);
        this.f2636f.setStyle(Paint.Style.FILL);
        this.f2636f.setColor(layer.p());
    }

    public void b(Canvas canvas, Matrix matrix, int i) {
        int alpha = Color.alpha(this.f2637g.p());
        if (alpha != 0) {
            int intValue = (int) (((((float) this.f2706d.a().b().intValue()) * (((float) alpha) / 255.0f)) / 100.0f) * 255.0f);
            this.f2636f.setAlpha(intValue);
            if (intValue > 0) {
                a(matrix);
                canvas.drawRect(this.f2635e, this.f2636f);
            }
        }
    }

    public void a(RectF rectF, Matrix matrix) {
        super.a(rectF, matrix);
        a(this.f2703a);
        rectF.set(this.f2635e);
    }

    private void a(Matrix matrix) {
        this.f2635e.set(0.0f, 0.0f, (float) this.f2637g.r(), (float) this.f2637g.q());
        matrix.mapRect(this.f2635e);
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
        this.f2636f.setColorFilter(colorFilter);
    }
}
