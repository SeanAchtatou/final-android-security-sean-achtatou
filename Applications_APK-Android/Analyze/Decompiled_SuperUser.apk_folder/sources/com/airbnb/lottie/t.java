package com.airbnb.lottie;

import android.graphics.PointF;
import com.airbnb.lottie.f;
import org.json.JSONObject;

/* compiled from: CircleShape */
class t implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2725a;

    /* renamed from: b  reason: collision with root package name */
    private final m<PointF> f2726b;

    /* renamed from: c  reason: collision with root package name */
    private final f f2727c;

    private t(String str, m<PointF> mVar, f fVar) {
        this.f2725a = str;
        this.f2726b = mVar;
        this.f2727c = fVar;
    }

    public y a(be beVar, q qVar) {
        return new ae(beVar, qVar, this);
    }

    /* compiled from: CircleShape */
    static class a {
        static t a(JSONObject jSONObject, bd bdVar) {
            return new t(jSONObject.optString("nm"), e.a(jSONObject.optJSONObject("p"), bdVar), f.a.a(jSONObject.optJSONObject("s"), bdVar));
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f2725a;
    }

    public m<PointF> b() {
        return this.f2726b;
    }

    public f c() {
        return this.f2727c;
    }
}
