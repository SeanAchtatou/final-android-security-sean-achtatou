package com.airbnb.lottie;

import android.content.res.Resources;
import com.airbnb.lottie.bd;
import java.io.InputStream;

/* compiled from: FileCompositionLoader */
final class af extends x<InputStream> {

    /* renamed from: a  reason: collision with root package name */
    private final Resources f2440a;

    /* renamed from: b  reason: collision with root package name */
    private final bm f2441b;

    af(Resources resources, bm bmVar) {
        this.f2440a = resources;
        this.f2441b = bmVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public bd doInBackground(InputStream... inputStreamArr) {
        return bd.a.a(this.f2440a, inputStreamArr[0]);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(bd bdVar) {
        this.f2441b.a(bdVar);
    }
}
