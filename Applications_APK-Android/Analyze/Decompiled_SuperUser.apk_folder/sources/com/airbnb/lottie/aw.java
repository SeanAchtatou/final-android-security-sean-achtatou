package com.airbnb.lottie;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

/* compiled from: ImageLayer */
class aw extends q {

    /* renamed from: e  reason: collision with root package name */
    private final Paint f2504e = new Paint(3);

    /* renamed from: f  reason: collision with root package name */
    private final Rect f2505f = new Rect();

    /* renamed from: g  reason: collision with root package name */
    private final Rect f2506g = new Rect();

    /* renamed from: h  reason: collision with root package name */
    private final float f2507h;

    aw(be beVar, Layer layer, float f2) {
        super(beVar, layer);
        this.f2507h = f2;
    }

    public void b(Canvas canvas, Matrix matrix, int i) {
        Bitmap f2 = f();
        if (f2 != null) {
            this.f2504e.setAlpha(i);
            canvas.save();
            canvas.concat(matrix);
            this.f2505f.set(0, 0, f2.getWidth(), f2.getHeight());
            this.f2506g.set(0, 0, (int) (((float) f2.getWidth()) * this.f2507h), (int) (((float) f2.getHeight()) * this.f2507h));
            canvas.drawBitmap(f2, this.f2505f, this.f2506g, this.f2504e);
            canvas.restore();
        }
    }

    public void a(RectF rectF, Matrix matrix) {
        super.a(rectF, matrix);
        Bitmap f2 = f();
        if (f2 != null) {
            rectF.set(rectF.left, rectF.top, Math.min(rectF.right, (float) f2.getWidth()), Math.min(rectF.bottom, (float) f2.getHeight()));
            this.f2703a.mapRect(rectF);
        }
    }

    private Bitmap f() {
        return this.f2704b.b(this.f2705c.g());
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
        this.f2504e.setColorFilter(colorFilter);
    }
}
