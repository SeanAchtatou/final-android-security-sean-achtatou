package com.airbnb.lottie;

import android.graphics.PointF;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: JsonUtils */
class az {
    static PointF a(JSONObject jSONObject, float f2) {
        return new PointF(a(jSONObject.opt("x")) * f2, a(jSONObject.opt("y")) * f2);
    }

    static PointF a(JSONArray jSONArray, float f2) {
        if (jSONArray.length() >= 2) {
            return new PointF(((float) jSONArray.optDouble(0, 1.0d)) * f2, ((float) jSONArray.optDouble(1, 1.0d)) * f2);
        }
        throw new IllegalArgumentException("Unable to parse point for " + jSONArray);
    }

    static float a(Object obj) {
        if (obj instanceof Float) {
            return ((Float) obj).floatValue();
        }
        if (obj instanceof Integer) {
            return (float) ((Integer) obj).intValue();
        }
        if (obj instanceof Double) {
            return (float) ((Double) obj).doubleValue();
        }
        if (obj instanceof JSONArray) {
            return (float) ((JSONArray) obj).optDouble(0);
        }
        return 0.0f;
    }
}
