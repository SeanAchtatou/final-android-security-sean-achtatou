package com.airbnb.lottie;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.v4.util.e;
import com.airbnb.lottie.p;
import java.util.ArrayList;
import java.util.List;

/* compiled from: GradientFillContent */
class aq implements ad, p.a {

    /* renamed from: a  reason: collision with root package name */
    private final String f2476a;

    /* renamed from: b  reason: collision with root package name */
    private final e<LinearGradient> f2477b = new e<>();

    /* renamed from: c  reason: collision with root package name */
    private final e<RadialGradient> f2478c = new e<>();

    /* renamed from: d  reason: collision with root package name */
    private final Matrix f2479d = new Matrix();

    /* renamed from: e  reason: collision with root package name */
    private final Path f2480e = new Path();

    /* renamed from: f  reason: collision with root package name */
    private final Paint f2481f = new Paint(1);

    /* renamed from: g  reason: collision with root package name */
    private final RectF f2482g = new RectF();

    /* renamed from: h  reason: collision with root package name */
    private final List<bn> f2483h = new ArrayList();
    private final GradientType i;
    private final bb<an> j;
    private final bb<Integer> k;
    private final bb<PointF> l;
    private final bb<PointF> m;
    private final be n;
    private final int o;

    aq(be beVar, q qVar, ap apVar) {
        this.f2476a = apVar.a();
        this.n = beVar;
        this.i = apVar.b();
        this.f2480e.setFillType(apVar.c());
        this.o = (int) (beVar.n().c() / 32);
        this.j = apVar.d().b();
        this.j.a(this);
        qVar.a(this.j);
        this.k = apVar.e().b();
        this.k.a(this);
        qVar.a(this.k);
        this.l = apVar.f().b();
        this.l.a(this);
        qVar.a(this.l);
        this.m = apVar.g().b();
        this.m.a(this);
        qVar.a(this.m);
    }

    public void a() {
        this.n.invalidateSelf();
    }

    public void a(List<y> list, List<y> list2) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < list2.size()) {
                y yVar = list2.get(i3);
                if (yVar instanceof bn) {
                    this.f2483h.add((bn) yVar);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void a(Canvas canvas, Matrix matrix, int i2) {
        Shader c2;
        bc.a("GradientFillContent#draw");
        this.f2480e.reset();
        for (int i3 = 0; i3 < this.f2483h.size(); i3++) {
            this.f2480e.addPath(this.f2483h.get(i3).d(), matrix);
        }
        this.f2480e.computeBounds(this.f2482g, false);
        if (this.i == GradientType.Linear) {
            c2 = b();
        } else {
            c2 = c();
        }
        this.f2479d.set(matrix);
        c2.setLocalMatrix(this.f2479d);
        this.f2481f.setShader(c2);
        this.f2481f.setAlpha((int) (((((float) ((Integer) this.k.b()).intValue()) * (((float) i2) / 255.0f)) / 100.0f) * 255.0f));
        canvas.drawPath(this.f2480e, this.f2481f);
        bc.b("GradientFillContent#draw");
    }

    public void a(RectF rectF, Matrix matrix) {
        this.f2480e.reset();
        for (int i2 = 0; i2 < this.f2483h.size(); i2++) {
            this.f2480e.addPath(this.f2483h.get(i2).d(), matrix);
        }
        this.f2480e.computeBounds(rectF, false);
        rectF.set(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f);
    }

    public void a(String str, String str2, ColorFilter colorFilter) {
    }

    public String e() {
        return this.f2476a;
    }

    private LinearGradient b() {
        int d2 = d();
        LinearGradient a2 = this.f2477b.a((long) d2);
        if (a2 != null) {
            return a2;
        }
        PointF pointF = (PointF) this.l.b();
        PointF pointF2 = (PointF) this.m.b();
        an anVar = (an) this.j.b();
        LinearGradient linearGradient = new LinearGradient(pointF.x, pointF.y, pointF2.x, pointF2.y, anVar.b(), anVar.a(), Shader.TileMode.CLAMP);
        this.f2477b.b((long) d2, linearGradient);
        return linearGradient;
    }

    private RadialGradient c() {
        int d2 = d();
        RadialGradient a2 = this.f2478c.a((long) d2);
        if (a2 != null) {
            return a2;
        }
        PointF pointF = (PointF) this.l.b();
        PointF pointF2 = (PointF) this.m.b();
        an anVar = (an) this.j.b();
        int[] b2 = anVar.b();
        float[] a3 = anVar.a();
        float f2 = pointF.x;
        float f3 = pointF.y;
        RadialGradient radialGradient = new RadialGradient(f2, f3, (float) Math.hypot((double) (pointF2.x - f2), (double) (pointF2.y - f3)), b2, a3, Shader.TileMode.CLAMP);
        this.f2478c.b((long) d2, radialGradient);
        return radialGradient;
    }

    private int d() {
        int round = Math.round(this.l.c() * ((float) this.o));
        int round2 = Math.round(this.m.c() * ((float) this.o));
        int round3 = Math.round(this.j.c() * ((float) this.o));
        int i2 = 17;
        if (round != 0) {
            i2 = round * 527;
        }
        if (round2 != 0) {
            i2 = i2 * 31 * round2;
        }
        if (round3 != 0) {
            return i2 * 31 * round3;
        }
        return i2;
    }
}
