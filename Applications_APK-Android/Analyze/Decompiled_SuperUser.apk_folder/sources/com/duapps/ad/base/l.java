package com.duapps.ad.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.duapps.ad.internal.b.e;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONStringer;

public class l {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3573a = l.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static final Object f3574b = new Object();

    /* renamed from: c  reason: collision with root package name */
    private static final byte[] f3575c = {-53, -49, 125, 31, 17, 26, 81, 36, -53, 17, 39, 43, -64, 79, 48, -9, 32, -60, -21, -92, -48, 58, -59, -73, -36, -121, -71, -92, -87, 87, -121, 19, -92, -96, 67, 53, 51, 99, 53, 59, 57, 33, 121, -22, 31, -80, 118, -69};

    private l() {
    }

    public static void a(Context context, List<String> list, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("_toolbox_prefs", 0).edit();
        try {
            JSONStringer array = new JSONStringer().array();
            for (String value : list) {
                array.value(value);
            }
            array.endArray();
            edit.putString("n_pid" + i, array.toString());
            e.a(edit);
        } catch (JSONException e2) {
        }
    }

    public static List<String> a(Context context, int i) {
        String string = context.getSharedPreferences("_toolbox_prefs", 0).getString("n_pid" + i, "");
        ArrayList arrayList = new ArrayList();
        if (!TextUtils.isEmpty(string)) {
            try {
                JSONArray jSONArray = new JSONArray(string);
                int length = jSONArray.length();
                for (int i2 = 0; i2 < length; i2++) {
                    String optString = jSONArray.optString(i2);
                    if (!TextUtils.isEmpty(optString)) {
                        arrayList.add(optString);
                    }
                }
            } catch (JSONException e2) {
            }
        }
        return arrayList;
    }

    public static void a(Context context, String str, long j) {
        c(context, "last_modified_" + str, j);
    }

    public static long a(Context context, String str) {
        return d(context, "last_modified_" + str, 0);
    }

    public static void a(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("_toolbox_prefs", 0).edit();
        synchronized (f3574b) {
            edit.putInt("fb_no_fill_c", 0);
            edit.putLong("fb_no_fill_t", 0);
            e.a(edit);
        }
    }

    public static void b(Context context) {
        c(context, "fb_success_t", System.currentTimeMillis());
    }

    public static void c(Context context) {
        c(context, "ls_priotity_client", System.currentTimeMillis());
    }

    public static long d(Context context) {
        return d(context, "ls_priotity_client", 0);
    }

    public static void a(Context context, long j) {
        c(context, "ls_priotity_server", j);
    }

    public static long b(Context context, int i) {
        return h(context, "priotity_server" + i);
    }

    public static void a(Context context, int i, long j) {
        b(context, "priotity_server" + i, j);
    }

    public static long e(Context context) {
        return d(context, "ls_priotity_server", 0);
    }

    private static void b(Context context, String str, long j) {
        c(context, str, j);
    }

    private static long h(Context context, String str) {
        return d(context, str, 0);
    }

    public static boolean a(String str, Context context) {
        long e2 = e(str, context);
        if (e2 == 0) {
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis() - e2;
        if (currentTimeMillis > 300000) {
            return true;
        }
        if (currentTimeMillis > 300000 || c(str, context) >= 60) {
            return false;
        }
        return true;
    }

    public static boolean f(Context context) {
        return a("load_frequently_times", context);
    }

    public static boolean g(Context context) {
        return a("fill_frequently_times", context);
    }

    public static void h(Context context) {
        b("fill_frequently_times", context);
    }

    public static void i(Context context) {
        b("load_frequently_times", context);
    }

    public static void b(String str, Context context) {
        int c2;
        if (System.currentTimeMillis() - e(str, context) >= 300000) {
            d(str, context);
            c2 = 1;
        } else {
            c2 = c(str, context) + 1;
        }
        b(context, str, c2);
    }

    private static int c(String str, Context context) {
        return context.getSharedPreferences("_toolbox_prefs", 0).getInt(str, 0);
    }

    private static void d(String str, Context context) {
        c(context, str + "_pull_time", System.currentTimeMillis());
    }

    private static long e(String str, Context context) {
        return d(context, str + "_pull_time", 0);
    }

    public static void c(Context context, int i) {
        b(context, "log_priotity", i);
    }

    public static int j(Context context) {
        return a(context, "log_priotity", 3);
    }

    public static void a(Context context, int i, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "priority_policy_" + i, str);
        }
    }

    public static String d(Context context, int i) {
        return b(context, "priority_policy_" + i, "");
    }

    public static void b(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "imid", str);
        }
    }

    public static String k(Context context) {
        try {
            String str = new String(b.a("8a1n9d0i3c1y0c2f", "8a1n9d0i3c1y0c2f", f3575c));
            a.c(f3573a, "getInID(): " + str);
            return b(context, "imid", str);
        } catch (Exception e2) {
            a.d(f3573a, "getInID Exception :" + e2.toString());
            return null;
        }
    }

    public static void e(Context context, int i) {
        context.getSharedPreferences("_toolbox_prefs", 0).edit().putInt("tcppTctp", i).apply();
    }

    public static int l(Context context) {
        return context.getSharedPreferences("_toolbox_prefs", 0).getInt("tcppTctp", 0);
    }

    public static void f(Context context, int i) {
        b(context, "fid_time", i);
    }

    public static int m(Context context) {
        return a(context, "fid_time", 0);
    }

    private static final int a(Context context, String str, int i) {
        return context.getSharedPreferences("_toolbox_prefs", 0).getInt(str, i);
    }

    private static final void b(Context context, String str, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("_toolbox_prefs", 0).edit();
        edit.putInt(str, i);
        e.a(edit);
    }

    public static long n(Context context) {
        return d(context, "tcpp_ct", 86400000);
    }

    public static void b(Context context, long j) {
        c(context, "tcpp_ct", j);
    }

    public static void g(Context context, int i) {
        b(context, "key_tcpp_pull_interval_time", i);
    }

    public static long o(Context context) {
        return ((long) a(context, "key_tcpp_pull_interval_time", 0)) * 60000;
    }

    public static long p(Context context) {
        return d(context, "ls_tcpp", 0);
    }

    public static void q(Context context) {
        c(context, "ls_tcpp", System.currentTimeMillis());
    }

    public static long r(Context context) {
        return ((long) a(context, "key_fb_ct", 60)) * 60000;
    }

    public static void h(Context context, int i) {
        b(context, "key_fb_ct", i);
    }

    public static void c(Context context, String str) {
        a(context, "key_priority_browsers", str);
    }

    public static String s(Context context) {
        return b(context, "key_priority_browsers", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, int):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, long):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean t(Context context) {
        return b(context, "itwd", true);
    }

    public static void a(Context context, boolean z) {
        a(context, "itwd", z);
    }

    public static long u(Context context) {
        return d(context, "c_filter_t", 0);
    }

    public static void c(Context context, long j) {
        c(context, "c_filter_t", j);
    }

    public static long v(Context context) {
        return d(context, "k_nu_interval", 14400000);
    }

    public static void d(Context context, long j) {
        c(context, "k_nu_interval", j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, int):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, long):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean w(Context context) {
        return b(context, "isNU", false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, int):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, long):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean x(Context context) {
        return b(context, "k_allow_charles", true);
    }

    public static void b(Context context, boolean z) {
        a(context, "k_allow_charles", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, int):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, long):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean y(Context context) {
        return b(context, "k_allow_tcpdump", true);
    }

    public static void c(Context context, boolean z) {
        a(context, "k_allow_tcpdump", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, int):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, long):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean z(Context context) {
        return b(context, "k_allow_simulator", true);
    }

    public static void d(Context context, boolean z) {
        a(context, "k_allow_simulator", z);
    }

    private static void c(Context context, String str, long j) {
        SharedPreferences.Editor edit = context.getSharedPreferences("_toolbox_prefs", 0).edit();
        edit.putLong(str, j);
        e.a(edit);
    }

    private static long d(Context context, String str, long j) {
        return context.getSharedPreferences("_toolbox_prefs", 0).getLong(str, j);
    }

    private static void a(Context context, String str, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("_toolbox_prefs", 0).edit();
        edit.putBoolean(str, z);
        e.a(edit);
    }

    private static boolean b(Context context, String str, boolean z) {
        return context.getSharedPreferences("_toolbox_prefs", 0).getBoolean(str, z);
    }

    public static void d(Context context, String str) {
        if (!g(context, "k_pk") && !TextUtils.isEmpty(str)) {
            a(context, "k_pk", str);
        }
    }

    public static String A(Context context) {
        return b(context, "k_pk", "");
    }

    public static void e(Context context, String str) {
        a(context, "k_location", str);
    }

    public static String B(Context context) {
        return b(context, "k_location", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, int):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, long):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean C(Context context) {
        return b(context, "k_isSus", true);
    }

    public static void e(Context context, boolean z) {
        a(context, "k_isSus", z);
    }

    public static void f(Context context, String str) {
        a(context, "k_exg", str);
    }

    public static String D(Context context) {
        return b(context, "k_exg", "");
    }

    public static boolean g(Context context, String str) {
        return context.getSharedPreferences("_toolbox_prefs", 0).contains(str);
    }

    private static void a(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences("_toolbox_prefs", 0).edit();
        edit.putString(str, str2);
        e.a(edit);
    }

    private static String b(Context context, String str, String str2) {
        return context.getSharedPreferences("_toolbox_prefs", 0).getString(str, str2);
    }

    public static int E(Context context) {
        return a(context, "key_ptas", 0);
    }

    public static void i(Context context, int i) {
        if (!context.getSharedPreferences("_toolbox_prefs", 0).contains("key_ptas")) {
            b(context, "key_ptas", i);
        }
    }

    public static void f(Context context, boolean z) {
        a(context, "k_anit", z);
    }

    public static long F(Context context) {
        long d2 = d(context, "tts_cache_time", 86400000);
        if (d2 < 0) {
            return 0;
        }
        return d2;
    }

    public static void g(Context context, boolean z) {
        a(context, "exe_iad", z);
    }

    public static void j(Context context, int i) {
        b(context, "mbj_sp_time", i);
    }

    public static int G(Context context) {
        return a(context, "mbj_sp_time", -1);
    }

    public static void h(Context context, boolean z) {
        a(context, "ptay", z);
    }

    public static void i(Context context, boolean z) {
        a(context, "s_i_d_t", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, int):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, long):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean H(Context context) {
        return b(context, "s_i_d_t", false);
    }

    public static void j(Context context, boolean z) {
        a(context, "s_i_d_p", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, int):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, long):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean I(Context context) {
        return b(context, "s_i_d_p", false);
    }

    public static void k(Context context, boolean z) {
        a(context, "s_i_i_t", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, int):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, long):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean J(Context context) {
        return b(context, "s_i_i_t", false);
    }

    public static void l(Context context, boolean z) {
        a(context, "s_i_i_p", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, int):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, long):void
      com.duapps.ad.base.l.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean K(Context context) {
        return b(context, "s_i_i_p", false);
    }
}
