package com.duapps.ad.base;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.SparseArray;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class o {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3586a = o.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static o f3587b;

    /* renamed from: g  reason: collision with root package name */
    private static volatile String f3588g = null;

    /* renamed from: c  reason: collision with root package name */
    private SparseArray<List<String>> f3589c = new SparseArray<>();

    /* renamed from: d  reason: collision with root package name */
    private SparseArray<String> f3590d = new SparseArray<>();

    /* renamed from: e  reason: collision with root package name */
    private SparseArray<String> f3591e = new SparseArray<>();

    /* renamed from: f  reason: collision with root package name */
    private Context f3592f;

    public static synchronized o a(Context context) {
        o oVar;
        synchronized (o.class) {
            if (f3587b == null) {
                f3587b = new o(context.getApplicationContext());
            }
            oVar = f3587b;
        }
        return oVar;
    }

    public static String b(Context context) {
        if (f3588g == null) {
            synchronized (o.class) {
                try {
                    String string = context.getPackageManager().getApplicationInfo(context.getPackageName(), FileUtils.FileMode.MODE_IWUSR).metaData.getString("app_license");
                    if (TextUtils.isEmpty(string)) {
                        a.d(f3586a, "license should not null");
                    }
                    f3588g = string;
                } catch (PackageManager.NameNotFoundException e2) {
                }
            }
        }
        return f3588g;
    }

    public boolean a(int i) {
        return this.f3589c.indexOfKey(i) >= 0;
    }

    public List<String> b(int i) {
        List<String> a2 = l.a(this.f3592f, i);
        if (a2 == null || a2.size() == 0) {
            return this.f3589c.get(i);
        }
        return a2;
    }

    public String c(int i) {
        return this.f3591e.get(i);
    }

    public synchronized HashSet<Integer> a() {
        HashSet<Integer> hashSet;
        hashSet = new HashSet<>();
        int size = this.f3589c.size();
        while (size > 0) {
            int i = size - 1;
            hashSet.add(Integer.valueOf(this.f3589c.keyAt(i)));
            size = i;
        }
        int size2 = this.f3590d.size();
        while (size2 > 0) {
            size2--;
            hashSet.add(Integer.valueOf(this.f3590d.keyAt(size)));
        }
        return hashSet;
    }

    public String b() {
        return l.k(this.f3592f);
    }

    private o(Context context) {
        this.f3592f = context;
    }

    public void a(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                throw new IllegalArgumentException("pidsJson cannot be null");
            }
            JSONObject jSONObject = new JSONObject(str);
            JSONArray optJSONArray = jSONObject.optJSONArray("native");
            JSONArray optJSONArray2 = jSONObject.optJSONArray("list");
            JSONArray optJSONArray3 = jSONObject.optJSONArray("lockscreen");
            JSONArray optJSONArray4 = jSONObject.optJSONArray("ducaller");
            JSONArray optJSONArray5 = jSONObject.optJSONArray("weather");
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                    int optInt = jSONObject2.optInt("pid");
                    ArrayList arrayList = new ArrayList();
                    JSONArray optJSONArray6 = jSONObject2.optJSONArray("fbids");
                    if (optJSONArray6 != null) {
                        int length2 = optJSONArray6.length();
                        for (int i2 = 0; i2 < length2; i2++) {
                            String optString = optJSONArray6.optString(i2);
                            if (!TextUtils.isEmpty(optString)) {
                                arrayList.add(optString);
                            }
                        }
                    }
                    this.f3589c.put(optInt, arrayList);
                    this.f3591e.put(optInt, jSONObject2.optString("amid"));
                }
            }
            if (optJSONArray5 != null) {
                int length3 = optJSONArray5.length();
                for (int i3 = 0; i3 < length3; i3++) {
                    JSONObject jSONObject3 = optJSONArray5.getJSONObject(i3);
                    int optInt2 = jSONObject3.optInt("pid");
                    ArrayList arrayList2 = new ArrayList();
                    JSONArray optJSONArray7 = jSONObject3.optJSONArray("fbids");
                    if (optJSONArray7 != null) {
                        int length4 = optJSONArray7.length();
                        for (int i4 = 0; i4 < length4; i4++) {
                            String optString2 = optJSONArray7.optString(i4);
                            if (!TextUtils.isEmpty(optString2)) {
                                arrayList2.add(optString2);
                            }
                        }
                    }
                    this.f3589c.put(optInt2, arrayList2);
                }
            }
            if (optJSONArray2 != null) {
                int length5 = optJSONArray2.length();
                for (int i5 = 0; i5 < length5; i5++) {
                    JSONObject jSONObject4 = optJSONArray2.getJSONObject(i5);
                    this.f3590d.put(jSONObject4.optInt("pid"), jSONObject4.optString("fbids"));
                }
            }
            if (optJSONArray3 != null) {
                int length6 = optJSONArray3.length();
                for (int i6 = 0; i6 < length6; i6++) {
                    JSONObject jSONObject5 = optJSONArray3.getJSONObject(i6);
                    int optInt3 = jSONObject5.optInt("pid");
                    ArrayList arrayList3 = new ArrayList();
                    JSONArray optJSONArray8 = jSONObject5.optJSONArray("fbids");
                    if (optJSONArray8 != null) {
                        int length7 = optJSONArray8.length();
                        for (int i7 = 0; i7 < length7; i7++) {
                            String optString3 = optJSONArray8.optString(i7);
                            if (!TextUtils.isEmpty(optString3)) {
                                arrayList3.add(optString3);
                            }
                        }
                    }
                    this.f3589c.put(optInt3, arrayList3);
                }
            }
            if (optJSONArray4 != null) {
                int length8 = optJSONArray4.length();
                for (int i8 = 0; i8 < length8; i8++) {
                    JSONObject jSONObject6 = optJSONArray4.getJSONObject(i8);
                    int optInt4 = jSONObject6.optInt("pid");
                    ArrayList arrayList4 = new ArrayList();
                    JSONArray optJSONArray9 = jSONObject6.optJSONArray("fbids");
                    if (optJSONArray9 != null) {
                        int length9 = optJSONArray9.length();
                        for (int i9 = 0; i9 < length9; i9++) {
                            String optString4 = optJSONArray9.optString(i9);
                            if (!TextUtils.isEmpty(optString4)) {
                                arrayList4.add(optString4);
                            }
                        }
                    }
                    this.f3589c.put(optInt4, arrayList4);
                }
            }
        } catch (JSONException e2) {
            a.d(f3586a, "JSON parse Exception :" + e2.getMessage());
        }
    }
}
