package com.duapps.ad.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.l;
import com.duapps.ad.AdError;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.entity.AdModel;
import com.duapps.ad.entity.a.a;
import com.duapps.ad.entity.a.b;
import com.duapps.ad.entity.c;
import com.duapps.ad.internal.b.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class p extends b<a> {
    /* access modifiers changed from: private */
    public static final String n = p.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    u<AdModel> f3593a = new u<AdModel>() {
        public void a() {
            a.a(p.n, "start load cache data--");
            p.this.f3686d = true;
            p.this.f3687e = true;
        }

        public void a(int i, AdModel adModel) {
            p.this.f3686d = false;
            if (i == 200 && adModel != null) {
                List a2 = j.a(p.this.f3690h, p.this.a(adModel.f3683h));
                if (a2.size() <= 0) {
                    com.duapps.ad.stats.b.c(p.this.f3690h, p.this.i);
                    return;
                }
                synchronized (p.this.p) {
                    p.this.p.addAll(a2);
                    a.a(p.n, "store data into cache list -- list.size = " + p.this.p.size());
                }
            }
        }

        public void a(int i, String str) {
            a.a(p.n, "fail to get cache -" + str);
            p.this.f3685c = true;
            p.this.f3686d = false;
            if (!p.this.k && p.this.m != null) {
                p.this.m.a(new AdError(i, str));
            }
        }
    };

    /* renamed from: b  reason: collision with root package name */
    BroadcastReceiver f3594b = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("action_notify_preparse_cache_result".equals(intent.getAction())) {
                long longExtra = intent.getLongExtra("ad_id", -1);
                int intExtra = intent.getIntExtra("parse_result_type", 0);
                synchronized (p.this.p) {
                    if (p.this.p != null && p.this.p.size() > 0) {
                        Iterator it = p.this.p.iterator();
                        while (it.hasNext()) {
                            if (((AdData) it.next()).f3665b == longExtra && intExtra != 1) {
                                it.remove();
                            }
                        }
                    }
                }
            }
        }
    };
    private int o;
    /* access modifiers changed from: private */
    public final List<AdData> p = Collections.synchronizedList(new ArrayList());

    public p(Context context, int i, long j, int i2) {
        super(context, i, j);
        this.o = i2;
        g();
    }

    public void a(boolean z) {
        super.a(z);
        if (e.a(this.f3690h)) {
            if (this.f3686d) {
                a.c(n, "isRefreshing ...");
                return;
            }
            int c2 = c();
            if (this.o - c2 <= 0) {
                a.c(n, "no need refresh");
            } else {
                t.a(this.f3690h).a(Integer.valueOf(this.i).intValue(), 1, this.f3593a, this.o - c2);
            }
        }
    }

    public int c() {
        int i;
        int i2 = 0;
        synchronized (this.p) {
            Iterator<AdData> it = this.p.iterator();
            while (it.hasNext()) {
                AdData next = it.next();
                if (next == null) {
                    it.remove();
                } else {
                    if (e.a(this.f3690h, next.f3667d) || !next.a()) {
                        it.remove();
                        i = i2;
                    } else {
                        i = i2 + 1;
                    }
                    i2 = i;
                }
            }
        }
        return i2;
    }

    public int b() {
        return this.o;
    }

    /* renamed from: a */
    public c d() {
        AdData adData;
        synchronized (this.p) {
            AdData adData2 = null;
            while (this.p.size() > 0 && ((adData2 = this.p.remove(0)) == null || !adData2.a())) {
            }
            adData = adData2;
            a.c(n, "OL poll title-> " + (adData != null ? adData.f3666c : "null"));
        }
        com.duapps.ad.stats.b.f(this.f3690h, adData == null ? "FAIL" : "OK", this.i);
        if (adData == null) {
            return null;
        }
        if (adData.f3664a == 2) {
            k.a(this.f3690h).a(adData);
        }
        return new c(this.f3690h, adData, this.m);
    }

    /* access modifiers changed from: private */
    public List<AdData> a(List<AdData> list) {
        ArrayList arrayList = new ArrayList();
        for (AdData next : list) {
            if (!e.a(this.f3690h, next.f3667d)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public void e() {
        synchronized (this.p) {
            this.p.clear();
        }
    }

    private void g() {
        try {
            l.a(this.f3690h).a(this.f3594b, new IntentFilter("action_notify_preparse_cache_result"));
        } catch (Exception e2) {
        }
    }
}
