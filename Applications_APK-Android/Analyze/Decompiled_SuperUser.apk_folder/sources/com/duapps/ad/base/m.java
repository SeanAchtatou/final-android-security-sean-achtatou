package com.duapps.ad.base;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.duapps.ad.stats.DuAdCacheProvider;

public class m {

    /* renamed from: a  reason: collision with root package name */
    private static m f3576a;

    /* renamed from: c  reason: collision with root package name */
    private static final String f3577c = m.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private Context f3578b;

    public static synchronized m a(Context context) {
        m mVar;
        synchronized (m.class) {
            if (f3576a == null) {
                f3576a = new m(context.getApplicationContext());
            }
            mVar = f3576a;
        }
        return mVar;
    }

    private m(Context context) {
        this.f3578b = context;
        a();
    }

    private void a() {
        try {
            this.f3578b.getContentResolver().delete(DuAdCacheProvider.a(this.f3578b, 3), "ts<?", new String[]{String.valueOf(System.currentTimeMillis() - 7200000)});
        } catch (Exception e2) {
            a.a(f3577c, "mDatabase initCacheDatabase() del exception: ", e2);
        }
    }

    static class a {

        /* renamed from: a  reason: collision with root package name */
        public String f3579a;

        /* renamed from: b  reason: collision with root package name */
        public String f3580b;

        /* renamed from: c  reason: collision with root package name */
        public long f3581c;

        a() {
        }
    }

    public a a(String str) {
        Cursor cursor;
        String[] strArr = {"data", "ts"};
        String[] strArr2 = {str};
        a aVar = new a();
        aVar.f3579a = str;
        try {
            cursor = this.f3578b.getContentResolver().query(DuAdCacheProvider.a(this.f3578b, 3), strArr, "key=?", strArr2, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        aVar.f3580b = cursor.getString(0);
                        aVar.f3581c = cursor.getLong(1);
                    }
                } catch (Exception e2) {
                    e = e2;
                    try {
                        a.a(f3577c, "getCacheEntry() exception: ", e);
                        if (cursor != null && !cursor.isClosed()) {
                            cursor.close();
                        }
                        return aVar;
                    } catch (Throwable th) {
                        th = th;
                        cursor.close();
                        throw th;
                    }
                }
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        } catch (Exception e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            throw th;
        }
        return aVar;
    }

    public void a(a aVar) {
        ContentValues contentValues = new ContentValues(4);
        contentValues.put("key", aVar.f3579a);
        contentValues.put("data", aVar.f3580b);
        contentValues.put("ts", Long.valueOf(aVar.f3581c));
        try {
            if (this.f3578b.getContentResolver().update(DuAdCacheProvider.a(this.f3578b, 3), contentValues, "key=?", new String[]{aVar.f3579a}) < 1) {
                this.f3578b.getContentResolver().insert(DuAdCacheProvider.a(this.f3578b, 3), contentValues);
            }
        } catch (Exception e2) {
            a.a(f3577c, "cacheDabase saveCacheEntry() exception: ", e2);
        } catch (Throwable th) {
            a.a(f3577c, "cacheDabase saveCacheEntry() exception: ", th);
        }
    }
}
