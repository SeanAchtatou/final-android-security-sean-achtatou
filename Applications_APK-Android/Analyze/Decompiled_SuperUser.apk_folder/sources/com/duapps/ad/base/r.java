package com.duapps.ad.base;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.entity.AdModel;
import com.duapps.ad.stats.DuAdCacheProvider;
import com.duapps.ad.stats.e;
import org.json.JSONObject;

public class r {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f3600a = r.class.getSimpleName();

    /* renamed from: c  reason: collision with root package name */
    private static r f3601c;

    /* renamed from: b  reason: collision with root package name */
    private Context f3602b;

    public static synchronized r a(Context context) {
        r rVar;
        synchronized (r.class) {
            if (f3601c == null) {
                f3601c = new r(context.getApplicationContext());
            }
            rVar = f3601c;
        }
        return rVar;
    }

    private r(Context context) {
        this.f3602b = context;
    }

    public void a(final AdModel adModel) {
        v.a().a(new Runnable() {
            public void run() {
                if (adModel == null || adModel.f3683h == null || adModel.f3683h.size() == 0) {
                    a.c(r.f3600a, "ads == null || ads.list == null || ads.list.size() == 0");
                    return;
                }
                r.this.a();
                try {
                    for (AdData next : adModel.f3683h) {
                        a.c(r.f3600a, "wall---before insert: ad.package: " + next.f3667d + ",ad.preParse:" + next.H);
                        if (next.H == 1) {
                            r.this.a(new e(next));
                        }
                    }
                } catch (Exception e2) {
                    if (a.a()) {
                        a.a(r.f3600a, "wall---batch update or insert triggerPreParse data error: ", e2);
                    }
                }
            }
        });
    }

    public void a(e eVar) {
        if (eVar != null && !TextUtils.isEmpty(eVar.a())) {
            try {
                ContentValues contentValues = new ContentValues(5);
                contentValues.put("ad_id", Long.valueOf(eVar.f().f3665b));
                contentValues.put("pkgName", eVar.a());
                contentValues.put("data", e.a(eVar).toString());
                contentValues.put("ctime", Long.valueOf(System.currentTimeMillis()));
                if (this.f3602b.getContentResolver().update(DuAdCacheProvider.a(this.f3602b, 5), contentValues, "pkgName=?", new String[]{eVar.a()}) == 0) {
                    this.f3602b.getContentResolver().insert(DuAdCacheProvider.a(this.f3602b, 5), contentValues);
                    a.c(f3600a, "update or insert triggerPreParse data success");
                }
            } catch (Exception e2) {
                a.a(f3600a, "update or insert triggerPreParse data error: ", e2);
            } catch (Throwable th) {
                a.a(f3600a, "update or insert triggerPreParse data error: ", th);
            }
        }
    }

    public e a(String str) {
        Cursor cursor;
        Cursor cursor2;
        e eVar;
        long currentTimeMillis = System.currentTimeMillis() - l.n(this.f3602b);
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            cursor = this.f3602b.getContentResolver().query(DuAdCacheProvider.a(this.f3602b, 5), new String[]{"pkgName", "data"}, "pkgName=? AND ctime>?", new String[]{str, Long.toString(currentTimeMillis)}, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        eVar = e.a(new JSONObject(cursor.getString(1)));
                        if (cursor != null && !cursor.isClosed()) {
                            cursor.close();
                        }
                        return eVar;
                    }
                } catch (Exception e2) {
                    cursor2 = cursor;
                    try {
                        if (a.a()) {
                            a.c(f3600a, "fetch triggerPreParse data error");
                        }
                        if (cursor2 == null || cursor2.isClosed()) {
                            eVar = null;
                        } else {
                            cursor2.close();
                            eVar = null;
                        }
                        return eVar;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        cursor = cursor2;
                        th = th2;
                        cursor.close();
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            eVar = null;
            cursor.close();
        } catch (Exception e3) {
            cursor2 = null;
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            cursor.close();
            throw th;
        }
        return eVar;
    }

    public void a() {
        try {
            this.f3602b.getContentResolver().delete(DuAdCacheProvider.a(this.f3602b, 5), "ctime<?", new String[]{Long.toString(System.currentTimeMillis() - l.n(this.f3602b))});
        } catch (Exception e2) {
            a.a(f3600a, "clearTriggerPreParseData error: ", e2);
        } catch (Throwable th) {
            a.a(f3600a, "clearTriggerPreParseData error: ", th);
        }
    }
}
