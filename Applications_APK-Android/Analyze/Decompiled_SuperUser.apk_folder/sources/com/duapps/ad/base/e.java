package com.duapps.ad.base;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class e {

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        private final String f3533a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f3534b;

        a(String str, boolean z) {
            this.f3533a = str;
            this.f3534b = z;
        }

        public String a() {
            return this.f3533a;
        }
    }

    /* JADX INFO: finally extract failed */
    public static a a(Context context) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("Cannot be called from the main thread");
        }
        b bVar = new b();
        Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
        intent.setPackage("com.google.android.gms");
        if (!context.bindService(intent, bVar, 1)) {
            return null;
        }
        try {
            c cVar = new c(bVar.a());
            a aVar = new a(cVar.a(), cVar.a(true));
            context.unbindService(bVar);
            return aVar;
        } catch (Exception e2) {
            context.unbindService(bVar);
            return null;
        } catch (Throwable th) {
            context.unbindService(bVar);
            throw th;
        }
    }

    private static class b implements ServiceConnection {

        /* renamed from: a  reason: collision with root package name */
        boolean f3535a = false;

        /* renamed from: b  reason: collision with root package name */
        private final BlockingQueue<IBinder> f3536b = new LinkedBlockingQueue();

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.f3536b.put(iBinder);
            } catch (InterruptedException e2) {
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
        }

        public IBinder a() {
            if (this.f3535a) {
                throw new IllegalStateException("Binder already consumed");
            }
            this.f3535a = true;
            return this.f3536b.take();
        }
    }

    private static final class c implements IInterface {

        /* renamed from: a  reason: collision with root package name */
        private IBinder f3537a;

        public c(IBinder iBinder) {
            this.f3537a = iBinder;
        }

        public IBinder asBinder() {
            return this.f3537a;
        }

        public String a() {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.f3537a.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readString();
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }

        public boolean a(boolean z) {
            boolean z2 = true;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                obtain.writeInt(z ? 1 : 0);
                this.f3537a.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z2 = false;
                }
                return z2;
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
    }
}
