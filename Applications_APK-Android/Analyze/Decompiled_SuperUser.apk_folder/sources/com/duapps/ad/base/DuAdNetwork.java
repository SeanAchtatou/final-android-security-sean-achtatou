package com.duapps.ad.base;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.duapps.ad.internal.a.d;
import com.duapps.ad.internal.b;
import com.duapps.ad.internal.b.e;
import com.duapps.ad.internal.c;
import com.duapps.ad.stats.ToolStatsCore;
import com.duapps.ad.stats.g;
import java.net.URI;
import java.util.List;

public class DuAdNetwork {

    /* renamed from: a  reason: collision with root package name */
    static final String f3520a = DuAdNetwork.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    public static boolean f3521b = false;

    /* renamed from: c  reason: collision with root package name */
    public static volatile boolean f3522c;

    /* renamed from: d  reason: collision with root package name */
    private static DuAdNetwork f3523d;

    /* renamed from: e  reason: collision with root package name */
    private static String f3524e;

    /* renamed from: f  reason: collision with root package name */
    private static a f3525f;

    /* renamed from: g  reason: collision with root package name */
    private com.duapps.ad.internal.a f3526g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public Context f3527h;
    private d i;
    private final b j = new b() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duapps.ad.internal.c.a(java.lang.String, boolean, boolean):void
         arg types: [java.lang.String, int, int]
         candidates:
          com.duapps.ad.internal.c.a(java.lang.String, com.duapps.ad.stats.e, boolean):void
          com.duapps.ad.internal.c.a(java.lang.String, boolean, boolean):void */
        public void a(String str, String str2) {
            boolean a2 = e.a(DuAdNetwork.this.f3527h, str);
            boolean t = l.t(DuAdNetwork.this.f3527h);
            com.duapps.ad.stats.b.a(DuAdNetwork.this.f3527h, str2, a2, str, t);
            if (t && !TextUtils.isEmpty(str) && !a2) {
                c.a(DuAdNetwork.this.f3527h).a(str, false, true);
            }
        }
    };

    public interface a {
        String a();
    }

    public static String a() {
        return f3524e;
    }

    public static String b() {
        if (f3525f != null) {
            return f3525f.a();
        }
        return null;
    }

    private DuAdNetwork(Context context) {
        this.f3527h = context;
        f3522c = true;
        a(context.getApplicationContext());
        ToolStatsCore.a(context);
        this.i = new d(context.getApplicationContext());
        this.i.a();
        this.f3526g = com.duapps.ad.internal.a.a(context);
        this.f3526g.a();
        this.f3526g.a(this.j);
    }

    public static void init(Context context, String str) {
        o.a(context).a(str);
        synchronized (DuAdNetwork.class) {
            if (f3523d == null) {
                f3523d = new DuAdNetwork(context.getApplicationContext());
            }
        }
    }

    private void a(Context context) {
        if (TextUtils.isEmpty(o.b(context))) {
            a.d(f3520a, "app_license should not null");
        }
    }

    public static void onPackageAddReceived(Context context, Intent intent) {
        int i2 = 0;
        if (context != null && intent != null) {
            String action = intent.getAction();
            if ("android.intent.action.PACKAGE_ADDED".equals(action)) {
                boolean booleanExtra = intent.getBooleanExtra("android.intent.extra.REPLACING", false);
                if (a.a()) {
                    a.c(f3520a, "ACTION_PACKAGE_ADDED, replaceing? " + booleanExtra);
                }
                String dataString = intent.getDataString();
                String schemeSpecificPart = dataString == null ? null : URI.create(dataString).getSchemeSpecificPart();
                if (booleanExtra) {
                    i2 = 1;
                }
                g.a(context, schemeSpecificPart, i2);
                if (!booleanExtra) {
                    a(context, schemeSpecificPart);
                }
            } else if (a.a()) {
                a.c(f3520a, "Not ACTION_PACKAGE_ADDED: " + action);
            }
        } else if (a.a()) {
            a.c(f3520a, "Params error.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.internal.c.a(java.lang.String, boolean, boolean):void
     arg types: [java.lang.String, int, int]
     candidates:
      com.duapps.ad.internal.c.a(java.lang.String, com.duapps.ad.stats.e, boolean):void
      com.duapps.ad.internal.c.a(java.lang.String, boolean, boolean):void */
    private static void a(Context context, String str) {
        List<com.duapps.ad.stats.e> c2;
        q a2 = q.a(context);
        com.duapps.ad.stats.e b2 = a2.b(str);
        if (b2 == null) {
            if (a.a()) {
                a.c(f3520a, "Non-click item, skip.");
            }
            c.a(context).a(str, false, false);
            return;
        }
        if (l.l(context) > 0 && ((c2 = a2.c(str)) == null || c2.size() == 0)) {
            c.a(context).a(str, false, false);
        }
        g.f(context, b2);
        b(context, str);
        a2.a(str);
    }

    private static void b(final Context context, final String str) {
        int G = l.G(context);
        if (G >= 0) {
            com.duapps.ad.internal.b.d.a(new Runnable() {
                public void run() {
                    try {
                        context.startActivity(context.getPackageManager().getLaunchIntentForPackage(str));
                    } catch (Exception e2) {
                    }
                }
            }, G);
        }
    }
}
