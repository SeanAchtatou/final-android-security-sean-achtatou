package com.duapps.ad.base;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.duapps.ad.AdError;
import com.duapps.ad.base.m;
import com.duapps.ad.base.s;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.entity.AdModel;
import com.duapps.ad.inmobi.IMDataModel;
import com.duapps.ad.internal.a.d;
import com.duapps.ad.internal.b.e;
import com.duapps.ad.stats.DuAdCacheProvider;
import com.lody.virtual.server.pm.installer.PackageHelper;
import io.fabric.sdk.android.services.b.b;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class t {

    /* renamed from: a  reason: collision with root package name */
    private static String f3612a = "http://api.mobula.sdk.duapps.com/adunion/slot/getDlAd?";
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static String f3613b = "http://api.mobula.sdk.duapps.com/adunion/rtb/getInmobiAd?";

    /* renamed from: c  reason: collision with root package name */
    private static String f3614c = "http://api.mobula.sdk.duapps.com/adunion/rtb/fetchAd?";

    /* renamed from: e  reason: collision with root package name */
    private static t f3615e;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public Context f3616d;

    public static synchronized t a(Context context) {
        t tVar;
        synchronized (t.class) {
            if (f3615e == null) {
                f3615e = new t(context.getApplicationContext());
            }
            tVar = f3615e;
        }
        return tVar;
    }

    private t(Context context) {
        this.f3616d = context;
        b(context);
    }

    private void b(Context context) {
        try {
            this.f3616d.getContentResolver().delete(DuAdCacheProvider.a(this.f3616d, 3), "ts<?", new String[]{String.valueOf(System.currentTimeMillis() - 7200000)});
        } catch (Exception e2) {
            a.a("ToolboxCacheManager", "mDatabase initCacheDatabase() del exception: ", e2);
        }
    }

    private void a(int i, String str, int i2, String str2, String str3, u<AdModel> uVar, String str4, int i3, String str5) {
        uVar.a();
        final String b2 = o.b(this.f3616d);
        final String str6 = str3 + b2 + b.ROLL_OVER_FILE_NAME_SEPARATOR + str + b.ROLL_OVER_FILE_NAME_SEPARATOR + i + b.ROLL_OVER_FILE_NAME_SEPARATOR + i2;
        if (!e.a(this.f3616d)) {
            uVar.a((int) AdError.NETWORK_ERROR_CODE, AdError.NETWORK_ERROR.getErrorMessage());
            return;
        }
        final int i4 = i3;
        final int i5 = i2;
        final int i6 = i;
        final String str7 = str;
        final String str8 = str4;
        final String str9 = str5;
        final String str10 = str2;
        final u<AdModel> uVar2 = uVar;
        v.a().a(new Runnable() {
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                try {
                    List<NameValuePair> a2 = g.a(t.this.f3616d, b2, true);
                    a2.add(new BasicNameValuePair("play", e.a(t.this.f3616d, "com.android.vending") ? "1" : "0"));
                    a2.add(new BasicNameValuePair("res", "1080*460,244*244,170*170,108*108"));
                    a2.add(new BasicNameValuePair("ps", String.valueOf(i4)));
                    a2.add(new BasicNameValuePair("pn", String.valueOf(i5)));
                    a2.add(new BasicNameValuePair("sid", String.valueOf(i6)));
                    a2.add(new BasicNameValuePair("sType", str7));
                    a2.add(new BasicNameValuePair("dllv", str8));
                    if (!TextUtils.isEmpty(str9)) {
                        a2.add(new BasicNameValuePair("adPkg", str9));
                    }
                    URL url = new URL(str10 + URLEncodedUtils.format(a2, "UTF-8"));
                    a.c("ToolboxCacheManager", "getWall sType :" + str8 + "," + str7 + ", Url ->" + url.toString());
                    s.a(url, new s.b() {
                        public void a(int i, s.a aVar) {
                            if (i == 200 && aVar != null) {
                                try {
                                    JSONObject jSONObject = aVar.f3609a;
                                    JSONObject jSONObject2 = jSONObject.getJSONObject("datas");
                                    a.a("ToolboxCacheManager", "getWall sType :" + str8 + "," + str7 + ", response ->" + jSONObject.toString());
                                    m a2 = m.a(t.this.f3616d);
                                    m.a a3 = a2.a(str6);
                                    a3.f3580b = aVar.f3609a.toString();
                                    a3.f3581c = System.currentTimeMillis();
                                    a3.f3579a = str6;
                                    a2.a(a3);
                                    int optInt = jSONObject2.optInt("sId");
                                    if (optInt > 0) {
                                        l.i(t.this.f3616d, optInt);
                                    }
                                    String optString = jSONObject2.optString("pe");
                                    if (!TextUtils.isEmpty(optString)) {
                                        try {
                                            d.a(t.this.f3616d, new JSONObject(e.e(optString)));
                                        } catch (Exception e2) {
                                            e2.printStackTrace();
                                        }
                                    }
                                    AdModel adModel = new AdModel(b2, optInt, str7, jSONObject2, a3.f3581c);
                                    l.d(t.this.f3616d, adModel.n);
                                    j.a(t.this.f3616d).a(adModel.f3683h);
                                    if ("normal".equals(str8)) {
                                        k.a(t.this.f3616d).a();
                                        ArrayList arrayList = new ArrayList();
                                        for (AdData next : adModel.f3683h) {
                                            if (next.f3664a == 1) {
                                                arrayList.add(next);
                                            }
                                        }
                                        if (arrayList.size() > 0) {
                                            k.a(t.this.f3616d).a(arrayList);
                                        }
                                    }
                                    r.a(t.this.f3616d).a(adModel);
                                    uVar2.a(i, adModel);
                                    l.a(t.this.f3616d, optInt, aVar.f3611c);
                                } catch (JSONException e3) {
                                    a.a("ToolboxCacheManager", "getWall sType :" + str7 + ",parse JsonException :", e3);
                                    uVar2.a((int) AdError.SERVER_ERROR_CODE, AdError.SERVER_ERROR.getErrorMessage());
                                    com.duapps.ad.stats.b.a(t.this.f3616d, str8, i6, (int) PackageHelper.INSTALL_PARSE_FAILED_BAD_MANIFEST, SystemClock.elapsedRealtime() - elapsedRealtime);
                                    return;
                                }
                            }
                            com.duapps.ad.stats.b.a(t.this.f3616d, str8, i6, i, SystemClock.elapsedRealtime() - elapsedRealtime);
                        }

                        public void a(int i, String str) {
                            a.c("ToolboxCacheManager", "getWall sType :" + str7 + ", parse failed: " + str);
                            uVar2.a((int) AdError.INTERNAL_ERROR_CODE, AdError.INTERNAL_ERROR.getErrorMessage());
                            long elapsedRealtime = SystemClock.elapsedRealtime() - elapsedRealtime;
                            if (i != 204) {
                                com.duapps.ad.stats.b.a("dl", i, t.this.f3616d, i6, elapsedRealtime, str8, b2, str);
                            }
                            com.duapps.ad.stats.b.a(t.this.f3616d, str8, i6, i, elapsedRealtime);
                        }
                    }, l.b(t.this.f3616d, i6));
                } catch (MalformedURLException e2) {
                    a.a("ToolboxCacheManager", "getWall sType :" + str7 + ", parse exception.", e2);
                    uVar2.a((int) AdError.UNKNOW_ERROR_CODE, AdError.UNKNOW_ERROR.getErrorMessage());
                    com.duapps.ad.stats.b.a(t.this.f3616d, str8, i6, (int) PackageHelper.INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION, SystemClock.elapsedRealtime() - elapsedRealtime);
                }
            }
        });
    }

    private void a(int i, String str, int i2, String str2, String str3, u<AdModel> uVar, int i3) {
        uVar.a();
        final String b2 = o.b(this.f3616d);
        final DisplayMetrics displayMetrics = this.f3616d.getResources().getDisplayMetrics();
        final String str4 = str3 + b2 + b.ROLL_OVER_FILE_NAME_SEPARATOR + str + b.ROLL_OVER_FILE_NAME_SEPARATOR + i + b.ROLL_OVER_FILE_NAME_SEPARATOR + i2;
        if (!e.a(this.f3616d)) {
            uVar.a((int) AdError.NETWORK_ERROR_CODE, AdError.NETWORK_ERROR.getErrorMessage());
            return;
        }
        final int i4 = i2;
        final int i5 = i;
        final String str5 = str;
        final int i6 = i3;
        final String str6 = str2;
        final u<AdModel> uVar2 = uVar;
        v.a().a(new Runnable() {
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                try {
                    List<NameValuePair> a2 = g.a(t.this.f3616d, b2, true);
                    a2.add(new BasicNameValuePair("play", e.a(t.this.f3616d, "com.android.vending") ? "1" : "0"));
                    a2.add(new BasicNameValuePair("res", String.valueOf(displayMetrics.heightPixels) + "*" + String.valueOf(displayMetrics.widthPixels)));
                    a2.add(new BasicNameValuePair("ps", String.valueOf(20)));
                    a2.add(new BasicNameValuePair("pn", String.valueOf(i4)));
                    a2.add(new BasicNameValuePair("sid", String.valueOf(i5)));
                    a2.add(new BasicNameValuePair("sType", str5));
                    a2.add(new BasicNameValuePair("aSize", String.valueOf(i6)));
                    a2.add(new BasicNameValuePair("ua", h.f3541b));
                    URL url = new URL(str6 + URLEncodedUtils.format(a2, "UTF-8"));
                    a.c("ToolboxCacheManager", "getOnlineWall sType :" + str5 + ", Url ->" + url.toString());
                    s.a(url, new s.b() {
                        public void a(int i, s.a aVar) {
                            if (i == 200 && aVar != null) {
                                try {
                                    JSONObject jSONObject = aVar.f3609a;
                                    JSONObject jSONObject2 = jSONObject.getJSONObject("datas");
                                    a.a("ToolboxCacheManager", "getOnlineWall sType :" + str5 + ", response ->" + jSONObject.toString());
                                    m a2 = m.a(t.this.f3616d);
                                    m.a a3 = a2.a(str4);
                                    a3.f3580b = aVar.f3609a.toString();
                                    a3.f3581c = System.currentTimeMillis();
                                    a3.f3579a = str4;
                                    a2.a(a3);
                                    AdModel adModel = new AdModel(b2, i5, str5, jSONObject2, a3.f3581c);
                                    l.d(t.this.f3616d, adModel.n);
                                    j.a(t.this.f3616d).a(adModel.f3683h);
                                    k.a(t.this.f3616d).a();
                                    ArrayList arrayList = new ArrayList();
                                    for (AdData next : adModel.f3683h) {
                                        if (next.f3664a == 1) {
                                            arrayList.add(next);
                                        }
                                    }
                                    if (arrayList.size() > 0) {
                                        k.a(t.this.f3616d).a(arrayList);
                                    }
                                    uVar2.a(i, adModel);
                                    l.a(t.this.f3616d, i5, aVar.f3611c);
                                } catch (JSONException e2) {
                                    a.a("ToolboxCacheManager", "getOnlineWall sType :" + str5 + ",parse JsonException :", e2);
                                    uVar2.a((int) AdError.SERVER_ERROR_CODE, AdError.SERVER_ERROR.getErrorMessage());
                                    com.duapps.ad.stats.b.c(t.this.f3616d, i5, (int) PackageHelper.INSTALL_PARSE_FAILED_BAD_MANIFEST, SystemClock.elapsedRealtime() - elapsedRealtime);
                                    return;
                                }
                            }
                            com.duapps.ad.stats.b.c(t.this.f3616d, i5, i, SystemClock.elapsedRealtime() - elapsedRealtime);
                        }

                        public void a(int i, String str) {
                            a.c("ToolboxCacheManager", "getOnlineWall sType :" + str5 + ", parse failed: " + str);
                            uVar2.a((int) AdError.INTERNAL_ERROR_CODE, AdError.INTERNAL_ERROR.getErrorMessage());
                            long elapsedRealtime = SystemClock.elapsedRealtime() - elapsedRealtime;
                            if (i != 204) {
                                com.duapps.ad.stats.b.a("ol", i, t.this.f3616d, i5, elapsedRealtime, str5, b2, str);
                            }
                            com.duapps.ad.stats.b.c(t.this.f3616d, i5, i, SystemClock.elapsedRealtime() - elapsedRealtime);
                        }
                    }, l.b(t.this.f3616d, i5));
                } catch (MalformedURLException e2) {
                    a.a("ToolboxCacheManager", "getWall sType :" + str5 + ", parse exception.", e2);
                    uVar2.a((int) AdError.UNKNOW_ERROR_CODE, AdError.UNKNOW_ERROR.getErrorMessage());
                    com.duapps.ad.stats.b.c(t.this.f3616d, i5, (int) PackageHelper.INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION, SystemClock.elapsedRealtime() - elapsedRealtime);
                }
            }
        });
    }

    public void a(int i, int i2, u<AdModel> uVar, String str) {
        a(i, "native", i2, f3612a, "native_", uVar, "normal", 9, str);
    }

    public void a(int i, int i2, u<AdModel> uVar) {
        a(i, "native", i2, f3612a, "native_", uVar, "normal", 20, null);
    }

    public void b(int i, int i2, u<AdModel> uVar) {
        a(i, "native", i2, f3612a, "native_", uVar, "high", 20, null);
    }

    public void a(int i, int i2, u<AdModel> uVar, int i3) {
        a(i, "online", i2, f3614c, "online_", uVar, i3);
    }

    public void a(int i, String str, String str2, u<IMDataModel> uVar) {
        uVar.a();
        if (!l.a(f3613b + i, this.f3616d)) {
            uVar.a((int) AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE, "This url is request too frequently.");
            a.c("ToolboxCacheManager", "This url is request too frequently.");
            return;
        }
        final String b2 = o.b(this.f3616d);
        if (!e.a(this.f3616d)) {
            uVar.a((int) AdError.NETWORK_ERROR_CODE, AdError.NETWORK_ERROR.getErrorMessage());
            return;
        }
        final int i2 = i;
        final String str3 = str;
        final String str4 = str2;
        final u<IMDataModel> uVar2 = uVar;
        v.a().a(new Runnable() {
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                try {
                    List<NameValuePair> a2 = g.a(t.this.f3616d, b2, true);
                    a2.add(new BasicNameValuePair("play", e.a(t.this.f3616d, "com.android.vending") ? "1" : "0"));
                    a2.add(new BasicNameValuePair("res", "1080*460,244*244,170*170,108*108"));
                    a2.add(new BasicNameValuePair("ps", String.valueOf(20)));
                    a2.add(new BasicNameValuePair("pn", String.valueOf(1)));
                    a2.add(new BasicNameValuePair("sid", String.valueOf(i2)));
                    a2.add(new BasicNameValuePair("sType", "native"));
                    a2.add(new BasicNameValuePair("or", String.valueOf(b.j(t.this.f3616d))));
                    a2.add(new BasicNameValuePair("siteId", str3));
                    a2.add(new BasicNameValuePair("aSize", str4));
                    String property = System.getProperty("http.agent");
                    if (TextUtils.isEmpty(property)) {
                        property = "dianxinosdxbs/3.2 (Linux; Android; Tapas OTA) Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18";
                    }
                    a2.add(new BasicNameValuePair("ua", property));
                    URL url = new URL(t.f3613b + URLEncodedUtils.format(a2, "UTF-8"));
                    a.c("ToolboxCacheManager", "getInmobiNativeAds sType :native, Url ->" + url.toString());
                    s.a(url, new s.b() {
                        public void a(int i, s.a aVar) {
                            if (i == 200 && aVar != null) {
                                try {
                                    JSONObject jSONObject = aVar.f3609a;
                                    JSONObject jSONObject2 = jSONObject.getJSONObject("datas");
                                    a.a("ToolboxCacheManager", "getInmobiNativeAds sType :native, response ->" + jSONObject.toString());
                                    uVar2.a(i, new IMDataModel(b2, i2, "native", jSONObject2, System.currentTimeMillis()));
                                } catch (JSONException e2) {
                                    a.a("ToolboxCacheManager", "getInmobiNativeAds sType :native,parse JsonException :", e2);
                                    uVar2.a((int) AdError.SERVER_ERROR_CODE, AdError.SERVER_ERROR.getErrorMessage());
                                    com.duapps.ad.stats.b.b(t.this.f3616d, i2, (int) PackageHelper.INSTALL_PARSE_FAILED_BAD_MANIFEST, SystemClock.elapsedRealtime() - elapsedRealtime);
                                    return;
                                }
                            }
                            com.duapps.ad.stats.b.b(t.this.f3616d, i2, i, SystemClock.elapsedRealtime() - elapsedRealtime);
                        }

                        public void a(int i, String str) {
                            a.c("ToolboxCacheManager", "getInmobiNativeAds sType :native, parse failed: " + str);
                            uVar2.a((int) AdError.INTERNAL_ERROR_CODE, AdError.INTERNAL_ERROR.getErrorMessage());
                            long elapsedRealtime = SystemClock.elapsedRealtime() - elapsedRealtime;
                            if (i != 204) {
                                com.duapps.ad.stats.b.a("im", i, t.this.f3616d, i2, elapsedRealtime, "native", b2, str);
                            }
                            com.duapps.ad.stats.b.b(t.this.f3616d, i2, i, SystemClock.elapsedRealtime() - elapsedRealtime);
                        }
                    }, l.b(t.this.f3616d, i2));
                    l.b(t.f3613b + i2, t.this.f3616d);
                } catch (MalformedURLException e2) {
                    a.a("ToolboxCacheManager", "getInmobiNativeAds sType :native, parse exception.", e2);
                    uVar2.a((int) AdError.UNKNOW_ERROR_CODE, AdError.UNKNOW_ERROR.getErrorMessage());
                    com.duapps.ad.stats.b.b(t.this.f3616d, i2, (int) PackageHelper.INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION, SystemClock.elapsedRealtime() - elapsedRealtime);
                }
            }
        });
    }
}
