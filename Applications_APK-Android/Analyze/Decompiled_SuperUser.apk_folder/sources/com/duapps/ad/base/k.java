package com.duapps.ad.base;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.l;
import android.text.TextUtils;
import com.duapps.ad.base.h;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.internal.b.e;
import com.duapps.ad.stats.g;
import java.util.Iterator;
import java.util.List;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3565a = k.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static k f3566b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Context f3567c;

    /* renamed from: d  reason: collision with root package name */
    private h f3568d;

    /* renamed from: e  reason: collision with root package name */
    private h.a f3569e = new a();

    /* renamed from: f  reason: collision with root package name */
    private Object f3570f = new Object();

    /* renamed from: g  reason: collision with root package name */
    private long f3571g;

    public static k a(Context context) {
        synchronized (k.class) {
            if (f3566b == null) {
                f3566b = new k(context.getApplicationContext());
            }
        }
        return f3566b;
    }

    private k(Context context) {
        this.f3567c = context;
        this.f3568d = h.a(this.f3567c);
    }

    public void a(List<AdData> list) {
        for (AdData a2 : list) {
            a(a2);
        }
    }

    public void a(AdData adData) {
        boolean z = true;
        synchronized (this.f3570f) {
            boolean a2 = e.a(this.f3567c, adData.f3667d);
            if (com.duapps.ad.stats.h.a(this.f3567c).c(adData.f3667d) != 1) {
                z = false;
            }
            if (adData != null && !TextUtils.isEmpty(adData.f3667d) && !a2 && AdData.b(adData) && !z) {
                this.f3568d.a(adData, adData.i, this.f3569e);
            }
        }
    }

    public void a() {
        synchronized (this.f3570f) {
            if (this.f3571g + 14400000 < System.currentTimeMillis()) {
                com.duapps.ad.stats.h.a(this.f3567c).a();
                com.duapps.ad.stats.h.a(this.f3567c).b(this.f3567c);
                this.f3571g = System.currentTimeMillis();
            }
        }
    }

    public boolean b(AdData adData) {
        if (adData == null || TextUtils.isEmpty(adData.f3667d)) {
            return false;
        }
        return this.f3568d.a(adData, adData.f3667d);
    }

    public static <T extends AdData> void a(Context context, List<T> list) {
        com.duapps.ad.stats.h a2 = com.duapps.ad.stats.h.a(context);
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            AdData adData = (AdData) it.next();
            if (AdData.b(adData) && a2.c(adData.f3667d) == 2) {
                it.remove();
            }
        }
    }

    public i a(String str) {
        return com.duapps.ad.stats.h.a(this.f3567c).d(str);
    }

    private class a implements h.a {
        private a() {
        }

        public void a(AdData adData, int i, int i2, long j) {
            g.b(k.this.f3567c, adData, i, i2, j);
        }

        public void a(AdData adData, i iVar) {
            com.duapps.ad.stats.h.a(k.this.f3567c).b(iVar);
        }

        public void b(AdData adData, i iVar) {
            if (adData != null && iVar != null) {
                Intent intent = new Intent("action_notify_preparse_cache_result");
                intent.putExtra("ad_id", adData.f3665b);
                intent.putExtra("ad_pkgname", adData.f3667d);
                intent.putExtra("parse_result_type", iVar.f3556c);
                l.a(k.this.f3567c).a(intent);
            }
        }
    }
}
