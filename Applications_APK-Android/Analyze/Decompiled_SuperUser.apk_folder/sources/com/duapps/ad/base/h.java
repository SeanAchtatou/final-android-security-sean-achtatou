package com.duapps.ad.base;

import android.content.Context;
import android.os.Process;
import android.text.TextUtils;
import com.duapps.ad.entity.AdData;
import java.util.HashSet;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class h {

    /* renamed from: a  reason: collision with root package name */
    static final String f3540a = h.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    public static final String f3541b;

    /* renamed from: c  reason: collision with root package name */
    private static h f3542c;

    /* renamed from: d  reason: collision with root package name */
    private static final ThreadFactory f3543d = new ThreadFactory() {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f3548a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "parse #" + this.f3548a.getAndIncrement());
        }
    };

    /* renamed from: e  reason: collision with root package name */
    private final PriorityBlockingQueue<Runnable> f3544e = new PriorityBlockingQueue<>(20);
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public HashSet<String> f3545f = new HashSet<>();

    /* renamed from: g  reason: collision with root package name */
    private Context f3546g;

    /* renamed from: h  reason: collision with root package name */
    private ThreadPoolExecutor f3547h;

    public interface a {
        void a(AdData adData, int i, int i2, long j);

        void a(AdData adData, i iVar);

        void b(AdData adData, i iVar);
    }

    static {
        String property = System.getProperty("http.agent");
        if (TextUtils.isEmpty(property)) {
            f3541b = "dianxinosdxbs/3.2 (Linux; Android; Tapas OTA) Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18";
        } else {
            f3541b = property;
        }
    }

    public static h a(Context context) {
        synchronized (h.class) {
            if (f3542c == null) {
                f3542c = new h(context.getApplicationContext());
            }
        }
        return f3542c;
    }

    private h(Context context) {
        this.f3546g = context;
        this.f3547h = new ThreadPoolExecutor(5, 5, 1, TimeUnit.MINUTES, this.f3544e, f3543d);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002a, code lost:
        r1 = new com.duapps.ad.base.h.b(r4, r5, r6, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        if (r4.f3544e.contains(r1) == false) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0037, code lost:
        com.duapps.ad.base.a.c(com.duapps.ad.base.h.f3540a, "Task already in Queue");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003f, code lost:
        r4.f3547h.execute(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.duapps.ad.entity.AdData r5, java.lang.String r6, com.duapps.ad.base.h.a r7) {
        /*
            r4 = this;
            r0 = 0
            if (r5 == 0) goto L_0x0009
            boolean r1 = android.text.TextUtils.isEmpty(r6)
            if (r1 == 0) goto L_0x000a
        L_0x0009:
            return r0
        L_0x000a:
            java.lang.String r1 = r5.i
            boolean r1 = com.duapps.ad.stats.d.b(r1)
            if (r1 != 0) goto L_0x0009
            java.util.HashSet<java.lang.String> r1 = r4.f3545f
            monitor-enter(r1)
            java.util.HashSet<java.lang.String> r2 = r4.f3545f     // Catch:{ all -> 0x0026 }
            boolean r2 = r2.contains(r6)     // Catch:{ all -> 0x0026 }
            if (r2 == 0) goto L_0x0029
            java.lang.String r2 = com.duapps.ad.base.h.f3540a     // Catch:{ all -> 0x0026 }
            java.lang.String r3 = "Task already Running."
            com.duapps.ad.base.a.c(r2, r3)     // Catch:{ all -> 0x0026 }
            monitor-exit(r1)     // Catch:{ all -> 0x0026 }
            goto L_0x0009
        L_0x0026:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0026 }
            throw r0
        L_0x0029:
            monitor-exit(r1)     // Catch:{ all -> 0x0026 }
            com.duapps.ad.base.h$b r1 = new com.duapps.ad.base.h$b
            r1.<init>(r5, r6, r7)
            java.util.concurrent.PriorityBlockingQueue<java.lang.Runnable> r2 = r4.f3544e
            boolean r2 = r2.contains(r1)
            if (r2 == 0) goto L_0x003f
            java.lang.String r1 = com.duapps.ad.base.h.f3540a
            java.lang.String r2 = "Task already in Queue"
            com.duapps.ad.base.a.c(r1, r2)
            goto L_0x0009
        L_0x003f:
            java.util.concurrent.ThreadPoolExecutor r0 = r4.f3547h
            r0.execute(r1)
            r0 = 1
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duapps.ad.base.h.a(com.duapps.ad.entity.AdData, java.lang.String, com.duapps.ad.base.h$a):boolean");
    }

    public boolean a(AdData adData, String str) {
        if (!this.f3545f.contains(str)) {
            return false;
        }
        a.c(f3540a, "task:" + str + " already Running.");
        return true;
    }

    private class b implements Comparable<b>, Runnable {

        /* renamed from: b  reason: collision with root package name */
        private AdData f3550b;

        /* renamed from: c  reason: collision with root package name */
        private long f3551c;

        /* renamed from: d  reason: collision with root package name */
        private String f3552d;

        /* renamed from: e  reason: collision with root package name */
        private a f3553e;

        public b(AdData adData, String str, a aVar) {
            this.f3550b = adData;
            this.f3552d = str;
            this.f3553e = aVar;
        }

        /* renamed from: a */
        public int compareTo(b bVar) {
            return bVar.f3550b.A - this.f3550b.A;
        }

        public void run() {
            Process.setThreadPriority(10);
            synchronized (h.this.f3545f) {
                h.this.f3545f.add(this.f3552d);
            }
            a(this.f3550b);
            synchronized (h.this.f3545f) {
                h.this.f3545f.remove(this.f3552d);
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            String str = this.f3552d;
            String str2 = ((b) obj).f3552d;
            if (str != null || str2 == null) {
                return str.equals(str2);
            }
            return false;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f0, code lost:
            if (com.duapps.ad.base.a.a() == false) goto L_0x010a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f2, code lost:
            com.duapps.ad.base.a.c(com.duapps.ad.base.h.f3540a, "DONE [TCTB] = " + r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x010a, code lost:
            a(r13, 2, r4, r5, android.os.SystemClock.elapsedRealtime() - r12.f3551c);
            r0.disconnect();
         */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x00bb A[Catch:{ all -> 0x0125 }] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00e8  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0121  */
        /* JADX WARNING: Removed duplicated region for block: B:58:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void a(com.duapps.ad.entity.AdData r13) {
            /*
                r12 = this;
                r5 = 0
                r8 = 0
                java.lang.String r4 = r13.i     // Catch:{ Exception -> 0x0127 }
                long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0127 }
                r12.f3551c = r0     // Catch:{ Exception -> 0x0127 }
            L_0x000a:
                if (r4 == 0) goto L_0x0089
                r0 = 10
                if (r5 >= r0) goto L_0x0089
                int r5 = r5 + 1
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0127 }
                r0.<init>(r4)     // Catch:{ Exception -> 0x0127 }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0127 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0127 }
                r1 = 0
                r0.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.String r1 = "User-Agent"
                java.lang.String r2 = com.duapps.ad.base.h.f3541b     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.String r1 = "Pragma"
                java.lang.String r2 = "no-cache"
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.String r1 = "Accept-Encoding"
                java.lang.String r2 = "gzip,deflate"
                r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                r1 = 30000(0x7530, float:4.2039E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                r2 = 302(0x12e, float:4.23E-43)
                if (r1 == r2) goto L_0x004f
                r2 = 301(0x12d, float:4.22E-43)
                if (r1 == r2) goto L_0x004f
                r2 = 307(0x133, float:4.3E-43)
                if (r1 == r2) goto L_0x004f
                r2 = 303(0x12f, float:4.25E-43)
                if (r1 != r2) goto L_0x00ec
            L_0x004f:
                java.lang.String r1 = "Location"
                java.lang.String r4 = r0.getHeaderField(r1)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                boolean r1 = com.duapps.ad.stats.d.b(r4)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                if (r1 == 0) goto L_0x008f
                boolean r1 = com.duapps.ad.base.a.a()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                if (r1 == 0) goto L_0x0079
                java.lang.String r1 = com.duapps.ad.base.h.f3540a     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                r2.<init>()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.String r3 = "DONE [TCTP] url = "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                com.duapps.ad.base.a.c(r1, r2)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
            L_0x0079:
                r3 = 1
                long r6 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                long r10 = r12.f3551c     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                long r6 = r6 - r10
                r1 = r12
                r2 = r13
                r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                r0.disconnect()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
            L_0x0089:
                if (r8 == 0) goto L_0x008e
                r8.disconnect()
            L_0x008e:
                return
            L_0x008f:
                boolean r1 = com.duapps.ad.base.a.a()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                if (r1 == 0) goto L_0x00ad
                java.lang.String r1 = com.duapps.ad.base.h.f3540a     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                r2.<init>()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.String r3 = "Middle LOC = "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                com.duapps.ad.base.a.c(r1, r2)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
            L_0x00ad:
                r0.disconnect()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                goto L_0x000a
            L_0x00b2:
                r1 = move-exception
                r8 = r0
                r0 = r1
            L_0x00b5:
                boolean r1 = com.duapps.ad.base.a.a()     // Catch:{ all -> 0x0125 }
                if (r1 == 0) goto L_0x00d7
                java.lang.String r1 = com.duapps.ad.base.h.f3540a     // Catch:{ all -> 0x0125 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
                r2.<init>()     // Catch:{ all -> 0x0125 }
                java.lang.String r3 = "DONE [TCTB] = EXCEPTION; "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0125 }
                java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0125 }
                java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0125 }
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0125 }
                com.duapps.ad.base.a.c(r1, r0)     // Catch:{ all -> 0x0125 }
            L_0x00d7:
                r3 = 3
                r4 = 0
                long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0125 }
                long r6 = r12.f3551c     // Catch:{ all -> 0x0125 }
                long r6 = r0 - r6
                r1 = r12
                r2 = r13
                r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0125 }
                if (r8 == 0) goto L_0x008e
                r8.disconnect()
                goto L_0x008e
            L_0x00ec:
                boolean r1 = com.duapps.ad.base.a.a()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                if (r1 == 0) goto L_0x010a
                java.lang.String r1 = com.duapps.ad.base.h.f3540a     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                r2.<init>()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.String r3 = "DONE [TCTB] = "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                com.duapps.ad.base.a.c(r1, r2)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
            L_0x010a:
                r3 = 2
                long r6 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                long r10 = r12.f3551c     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                long r6 = r6 - r10
                r1 = r12
                r2 = r13
                r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                r0.disconnect()     // Catch:{ Exception -> 0x00b2, all -> 0x011c }
                goto L_0x0089
            L_0x011c:
                r1 = move-exception
                r8 = r0
                r0 = r1
            L_0x011f:
                if (r8 == 0) goto L_0x0124
                r8.disconnect()
            L_0x0124:
                throw r0
            L_0x0125:
                r0 = move-exception
                goto L_0x011f
            L_0x0127:
                r0 = move-exception
                goto L_0x00b5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.duapps.ad.base.h.b.a(com.duapps.ad.entity.AdData):void");
        }

        private void a(AdData adData, int i, String str, int i2, long j) {
            i iVar = new i();
            iVar.f3554a = adData.i;
            iVar.f3557d = str;
            iVar.f3555b = adData.f3667d;
            iVar.f3556c = i;
            iVar.f3558e = System.currentTimeMillis();
            if (this.f3553e != null) {
                this.f3553e.a(adData, i, i2, j);
                this.f3553e.a(adData, iVar);
                this.f3553e.b(adData, iVar);
            }
        }
    }
}
