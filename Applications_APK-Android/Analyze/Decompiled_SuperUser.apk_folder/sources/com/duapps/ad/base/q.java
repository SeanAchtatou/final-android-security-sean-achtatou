package com.duapps.ad.base;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import com.duapps.ad.stats.DuAdCacheProvider;
import com.duapps.ad.stats.e;
import org.json.JSONObject;

public class q {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3597a = q.class.getSimpleName();

    /* renamed from: c  reason: collision with root package name */
    private static q f3598c;

    /* renamed from: b  reason: collision with root package name */
    private Context f3599b;

    public static synchronized q a(Context context) {
        q qVar;
        synchronized (q.class) {
            if (f3598c == null) {
                f3598c = new q(context.getApplicationContext());
            }
            qVar = f3598c;
        }
        return qVar;
    }

    private q(Context context) {
        this.f3599b = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void a(e eVar) {
        if (eVar != null && !TextUtils.isEmpty(eVar.a())) {
            try {
                ContentValues contentValues = new ContentValues(5);
                contentValues.put("pkgName", eVar.a());
                contentValues.put("ctime", Long.valueOf(System.currentTimeMillis()));
                contentValues.put("cdata", e.a(eVar).toString());
                if (this.f3599b.getContentResolver().update(DuAdCacheProvider.a(this.f3599b, 2), contentValues, "pkgName=?", new String[]{eVar.a()}) == 0) {
                    contentValues.put("status", (Integer) 0);
                    this.f3599b.getContentResolver().insert(DuAdCacheProvider.a(this.f3599b, 2), contentValues);
                }
            } catch (Exception e2) {
                a.a(f3597a, "updateOrInsertValidClickTime() exception: ", e2);
            } catch (Throwable th) {
                a.a(f3597a, "updateOrInsertValidClickTime() exception: ", th);
            }
        }
    }

    public void a(String str) {
        try {
            this.f3599b.getContentResolver().delete(DuAdCacheProvider.a(this.f3599b, 2), "pkgName=?", new String[]{str});
        } catch (Exception e2) {
            a.a(f3597a, "clearValidClickTimeRecord exception: ", e2);
        } catch (Throwable th) {
            a.a(f3597a, "clearValidClickTimeRecord exception: ", th);
        }
    }

    public e b(String str) {
        Cursor cursor;
        e eVar;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            cursor = this.f3599b.getContentResolver().query(DuAdCacheProvider.a(this.f3599b, 2), new String[]{"cdata", "ctime"}, "pkgName=? AND ctime>= ?", new String[]{str, String.valueOf(System.currentTimeMillis() - 86400000)}, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        eVar = e.a(new JSONObject(cursor.getString(0)));
                        if (cursor != null && !cursor.isClosed()) {
                            cursor.close();
                        }
                        return eVar;
                    }
                } catch (Exception e2) {
                    e = e2;
                    try {
                        a.a(f3597a, "getValidClickTimeRecord exception: ", e);
                        if (cursor == null || cursor.isClosed()) {
                            eVar = null;
                        } else {
                            cursor.close();
                            eVar = null;
                        }
                        return eVar;
                    } catch (Throwable th) {
                        th = th;
                        cursor.close();
                        throw th;
                    }
                }
            }
            eVar = null;
            cursor.close();
        } catch (Exception e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            throw th;
        }
        return eVar;
    }

    /* JADX WARN: Type inference failed for: r0v21, types: [java.util.List<com.duapps.ad.stats.e>] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.duapps.ad.stats.e> c(java.lang.String r11) {
        /*
            r10 = this;
            r9 = 1
            r6 = 0
            r8 = 2
            r7 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r11)
            if (r0 == 0) goto L_0x000c
            r0 = r6
        L_0x000b:
            return r0
        L_0x000c:
            java.lang.String[] r2 = new java.lang.String[r8]
            java.lang.String r0 = "cdata"
            r2[r7] = r0
            java.lang.String r0 = "ctime"
            r2[r9] = r0
            android.content.Context r0 = r10.f3599b
            int r0 = com.duapps.ad.base.l.l(r0)
            long r4 = java.lang.System.currentTimeMillis()
            int r0 = r0 * 60
            int r0 = r0 * 1000
            long r0 = (long) r0
            long r0 = r4 - r0
            java.lang.String r3 = "pkgName=? AND ctime>= ?"
            java.lang.String[] r4 = new java.lang.String[r8]
            r4[r7] = r11
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r4[r9] = r0
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            android.content.Context r0 = r10.f3599b     // Catch:{ Exception -> 0x0064 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0064 }
            android.content.Context r1 = r10.f3599b     // Catch:{ Exception -> 0x0064 }
            r5 = 2
            android.net.Uri r1 = com.duapps.ad.stats.DuAdCacheProvider.a(r1, r5)     // Catch:{ Exception -> 0x0064 }
            r5 = 0
            android.database.Cursor r6 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0064 }
            if (r6 == 0) goto L_0x0079
        L_0x004c:
            boolean r0 = r6.moveToNext()     // Catch:{ Exception -> 0x0064 }
            if (r0 == 0) goto L_0x0079
            r0 = 0
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x0064 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0064 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0064 }
            com.duapps.ad.stats.e r0 = com.duapps.ad.stats.e.a(r1)     // Catch:{ Exception -> 0x0064 }
            r7.add(r0)     // Catch:{ Exception -> 0x0064 }
            goto L_0x004c
        L_0x0064:
            r0 = move-exception
            java.lang.String r1 = com.duapps.ad.base.q.f3597a     // Catch:{ all -> 0x0085 }
            java.lang.String r2 = "getValidClickTimeRecord exception: "
            com.duapps.ad.base.a.a(r1, r2, r0)     // Catch:{ all -> 0x0085 }
            if (r6 == 0) goto L_0x0077
            boolean r0 = r6.isClosed()
            if (r0 != 0) goto L_0x0077
            r6.close()
        L_0x0077:
            r0 = r7
            goto L_0x000b
        L_0x0079:
            if (r6 == 0) goto L_0x0077
            boolean r0 = r6.isClosed()
            if (r0 != 0) goto L_0x0077
            r6.close()
            goto L_0x0077
        L_0x0085:
            r0 = move-exception
            if (r6 == 0) goto L_0x0091
            boolean r1 = r6.isClosed()
            if (r1 != 0) goto L_0x0091
            r6.close()
        L_0x0091:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duapps.ad.base.q.c(java.lang.String):java.util.List");
    }
}
