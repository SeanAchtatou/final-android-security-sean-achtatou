package com.duapps.ad.base;

import android.content.Context;
import android.os.Looper;
import android.text.TextUtils;
import com.duapps.ad.base.e;
import com.duapps.ad.internal.b.e;
import java.lang.reflect.Method;

public final class f {
    public static String a(Context context) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("Cannot get advertising info on main thread.");
        }
        String b2 = b(context);
        if (TextUtils.isEmpty(b2)) {
            b2 = c(context);
        }
        a.c("GMS", "getAdvertisingId = " + b2);
        return b2;
    }

    private static String b(Context context) {
        Method a2 = e.a("com.google.android.gms.ads.identifier.AdvertisingIdClient", "getAdvertisingIdInfo", new Class[]{Context.class});
        if (a2 == null) {
            a.c("GMS", "getAdvertisingIdInfo =  null");
            return null;
        }
        Object a3 = e.a((Object) null, a2, new Object[]{context});
        if (a3 == null) {
            a.c("GMS", "advertisingInfo =  null");
            return null;
        }
        Method a4 = e.a(a3.getClass(), "getId", new Class[0]);
        Method a5 = e.a(a3.getClass(), "isLimitAdTrackingEnabled", new Class[0]);
        if (a4 != null && a5 != null) {
            return (String) e.a(a3, a4, new Object[0]);
        }
        a.c("GMS", "getId =  null or isLimitAdTrackingEnabled = null");
        return null;
    }

    private static String c(Context context) {
        try {
            e.a a2 = e.a(context);
            if (a2 == null) {
                return null;
            }
            return a2.a();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
