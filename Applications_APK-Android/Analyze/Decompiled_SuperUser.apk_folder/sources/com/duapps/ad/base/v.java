package com.duapps.ad.base;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class v {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadFactory f3645a = new ThreadFactory() {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f3649a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "TooboxThread #" + this.f3649a.getAndIncrement());
        }
    };

    /* renamed from: b  reason: collision with root package name */
    private static final BlockingQueue<Runnable> f3646b = new LinkedBlockingQueue(200);

    /* renamed from: c  reason: collision with root package name */
    private static v f3647c;

    /* renamed from: d  reason: collision with root package name */
    private ThreadPoolExecutor f3648d = new ThreadPoolExecutor(5, 50, 1, TimeUnit.SECONDS, f3646b, f3645a);

    private v() {
    }

    public static synchronized v a() {
        v vVar;
        synchronized (v.class) {
            if (f3647c == null) {
                f3647c = new v();
            }
            vVar = f3647c;
        }
        return vVar;
    }

    public void a(Runnable runnable) {
        this.f3648d.execute(runnable);
    }

    public static void b(Runnable runnable) {
        a().a(runnable);
    }
}
