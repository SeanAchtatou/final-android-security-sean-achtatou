package com.duapps.ad.base;

import android.content.Context;
import android.text.TextUtils;
import com.duapps.ad.internal.b.e;
import com.kingouser.com.entity.UninstallAppInfo;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static final List<NameValuePair> f3538a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private static boolean f3539b = false;

    private g() {
    }

    public static void a(Context context) {
        synchronized (g.class) {
            if (!f3539b) {
                f3538a.add(new BasicNameValuePair("h", b.b(context)));
                f3538a.add(new BasicNameValuePair("w", b.c(context)));
                f3538a.add(new BasicNameValuePair("model", b.b()));
                f3538a.add(new BasicNameValuePair("vendor", b.a()));
                f3538a.add(new BasicNameValuePair("sdk", b.c()));
                f3538a.add(new BasicNameValuePair("dpi", b.g(context)));
                f3538a.add(new BasicNameValuePair("sv", "1.0.9.9"));
                f3538a.add(new BasicNameValuePair("svn", "HW-1.0.9.9"));
                f3538a.add(new BasicNameValuePair(UninstallAppInfo.COLUMN_PKG, b.a(context)));
                f3538a.add(new BasicNameValuePair("v", String.valueOf(b.e(context))));
                f3538a.add(new BasicNameValuePair("vn", b.d(context)));
                f3539b = true;
            }
        }
    }

    public static List<NameValuePair> a(Context context, String str, boolean z) {
        a(context);
        ArrayList arrayList = new ArrayList(f3538a);
        String f2 = b.f(context);
        if (!TextUtils.isEmpty(f2)) {
            arrayList.add(new BasicNameValuePair("op", f2));
        }
        String a2 = f.a(context);
        if (!TextUtils.isEmpty(a2)) {
            arrayList.add(new BasicNameValuePair("goid", "RSB_" + e.a(a2)));
        }
        arrayList.add(new BasicNameValuePair("locale", b.h(context)));
        arrayList.add(new BasicNameValuePair("ntt", b.i(context)));
        arrayList.add(new BasicNameValuePair("ls", str));
        String c2 = e.c(context);
        if (!TextUtils.isEmpty(c2)) {
            arrayList.add(new BasicNameValuePair("aid", c2));
        }
        if (z) {
            arrayList.add(new BasicNameValuePair("pk", l.A(context)));
        }
        String a3 = DuAdNetwork.a();
        if (!TextUtils.isEmpty(a3)) {
            arrayList.add(new BasicNameValuePair("lc", a3));
        }
        return arrayList;
    }
}
