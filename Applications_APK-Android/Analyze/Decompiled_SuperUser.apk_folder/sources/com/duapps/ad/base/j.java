package com.duapps.ad.base;

import android.content.Context;
import com.duapps.ad.base.h;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.internal.b.e;
import com.duapps.ad.stats.g;
import java.util.Iterator;
import java.util.List;

public class j {

    /* renamed from: a  reason: collision with root package name */
    static final String f3559a = j.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static j f3560b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Context f3561c;

    /* renamed from: d  reason: collision with root package name */
    private h f3562d;

    /* renamed from: e  reason: collision with root package name */
    private h.a f3563e = new a();

    public static j a(Context context) {
        synchronized (j.class) {
            if (f3560b == null) {
                f3560b = new j(context.getApplicationContext());
            }
        }
        return f3560b;
    }

    private j(Context context) {
        this.f3561c = context;
        this.f3562d = h.a(this.f3561c);
    }

    public boolean a(List<AdData> list) {
        for (AdData next : list) {
            if (!e.a(this.f3561c, next.f3667d) && AdData.a(this.f3561c, next)) {
                this.f3562d.a(next, next.i, this.f3563e);
            }
        }
        return true;
    }

    public static <T extends AdData> List<T> a(Context context, List<T> list) {
        com.duapps.ad.stats.h a2 = com.duapps.ad.stats.h.a(context);
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            AdData adData = (AdData) it.next();
            if (AdData.a(context, adData) && a2.b(adData.i) == 2) {
                it.remove();
            }
        }
        return list;
    }

    public i a(String str) {
        return com.duapps.ad.stats.h.a(this.f3561c).a(str);
    }

    private class a implements h.a {
        private a() {
        }

        public void a(AdData adData, int i, int i2, long j) {
            g.a(j.this.f3561c, adData, i, i2, j);
        }

        public void a(AdData adData, i iVar) {
            com.duapps.ad.stats.h.a(j.this.f3561c).a(iVar);
        }

        public void b(AdData adData, i iVar) {
        }
    }
}
