package com.duapps.ad.base;

import com.duapps.ad.internal.b.e;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.CharArrayBuffer;
import org.json.JSONObject;

public final class s {

    /* renamed from: a  reason: collision with root package name */
    private static final Header f3605a = new BasicHeader("Content-Encoding", "gzip");

    /* renamed from: b  reason: collision with root package name */
    private static final Header f3606b = new BasicHeader("Accept-Encoding", "gzip");

    /* renamed from: c  reason: collision with root package name */
    private static s f3607c = new s();

    /* renamed from: d  reason: collision with root package name */
    private static DefaultHttpClient f3608d;

    private s() {
    }

    public static void a(URL url, b bVar, long j) {
        try {
            a aVar = new a();
            ArrayList arrayList = new ArrayList();
            arrayList.add(a(j));
            int a2 = a(url, arrayList, aVar, 1);
            if (200 == a2 || 304 == a2) {
                bVar.a(a2, aVar);
            } else {
                bVar.a(a2, aVar.a());
            }
        } catch (Exception e2) {
            bVar.a(e2, (a) null);
            a.a("ToolboxRequestHelper", "failed to get project", e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.base.s.a(java.net.URL, java.util.List<org.apache.http.Header>, boolean):org.apache.http.HttpResponse
     arg types: [java.net.URL, java.util.List<org.apache.http.Header>, int]
     candidates:
      com.duapps.ad.base.s.a(java.net.URI, java.lang.String, java.util.List<org.apache.http.Header>):org.apache.http.HttpResponse
      com.duapps.ad.base.s.a(java.net.URL, com.duapps.ad.base.s$b, long):void
      com.duapps.ad.base.s.a(java.net.URL, java.util.List<org.apache.http.Header>, boolean):org.apache.http.HttpResponse */
    private static int a(URL url, List<Header> list, a aVar, int i) {
        if (i > 2) {
            throw new IOException("Too much recursion:2");
        }
        HttpResponse a2 = a(url, list, true);
        try {
            int statusCode = a2.getStatusLine().getStatusCode();
            if (statusCode == 200 || statusCode == 304) {
                if (statusCode == 200) {
                    JSONObject c2 = c(a2);
                    JSONObject jSONObject = c2.getJSONObject("responseHeader");
                    aVar.f3610b = jSONObject;
                    statusCode = jSONObject.getInt("status");
                    if (statusCode == 200) {
                        aVar.f3609a = c2.getJSONObject("response");
                        aVar.f3611c = b(a2);
                    } else {
                        a(a2);
                    }
                }
                a(a2);
            }
            return statusCode;
        } finally {
            a(a2);
        }
    }

    public static void a(HttpResponse httpResponse) {
        if (httpResponse != null && httpResponse.getEntity() != null) {
            try {
                httpResponse.getEntity().consumeContent();
            } catch (Exception e2) {
                a.a("ToolboxRequestHelper", "failed to cosume entity", e2);
            }
        }
    }

    private static long b(HttpResponse httpResponse) {
        Header firstHeader = httpResponse.getFirstHeader("Last-Modified");
        if (firstHeader != null) {
            return a(firstHeader.getValue()).getTime();
        }
        return 0;
    }

    private static Header a(long j) {
        return new BasicHeader("If-Modified-Since", a(new Date(j)));
    }

    private static JSONObject c(HttpResponse httpResponse) {
        InputStream inputStream;
        HttpEntity entity = httpResponse.getEntity();
        InputStream content = entity.getContent();
        int contentLength = (int) entity.getContentLength();
        if (contentLength < 0) {
            contentLength = 1024;
        }
        Header contentEncoding = entity.getContentEncoding();
        if (contentEncoding == null || contentEncoding.getValue().indexOf("gzip") == -1) {
            inputStream = content;
        } else {
            inputStream = new GZIPInputStream(content);
        }
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
        CharArrayBuffer charArrayBuffer = new CharArrayBuffer(contentLength);
        char[] cArr = new char[FileUtils.FileMode.MODE_ISGID];
        while (true) {
            int read = inputStreamReader.read(cArr);
            if (read != -1) {
                charArrayBuffer.append(cArr, 0, read);
            } else {
                e.a(inputStreamReader);
                return new JSONObject(charArrayBuffer.toString());
            }
        }
    }

    public static HttpResponse a(URI uri, String str, List<Header> list) {
        HttpPost httpPost = new HttpPost(uri.toString());
        if (list != null) {
            for (Header addHeader : list) {
                httpPost.addHeader(addHeader);
            }
        }
        httpPost.addHeader(f3605a);
        httpPost.addHeader(f3606b);
        httpPost.setEntity(new ByteArrayEntity(e.b(str)));
        if (a.a()) {
            a.c("ToolboxRequestHelper", "request uri:" + httpPost.getURI() + ",body:" + str + ",headers:" + Arrays.asList(httpPost.getAllHeaders()));
        }
        httpPost.getParams().setParameter("http.socket.timeout", 20000);
        try {
            return a().execute(httpPost);
        } catch (IOException e2) {
            httpPost.abort();
            throw e2;
        }
    }

    public static HttpResponse a(URL url, List<Header> list, boolean z) {
        HttpGet httpGet = new HttpGet(url.toString());
        if (list != null) {
            for (Header addHeader : list) {
                httpGet.addHeader(addHeader);
            }
        }
        if (z) {
            httpGet.addHeader(f3606b);
        }
        if (a.a()) {
            a.c("ToolboxRequestHelper", "request uri: " + httpGet.getURI() + ", headers: " + Arrays.asList(httpGet.getAllHeaders()));
        }
        httpGet.getParams().setParameter("http.socket.timeout", 20000);
        try {
            return a().execute(httpGet);
        } catch (IOException e2) {
            httpGet.abort();
            throw e2;
        }
    }

    public static synchronized DefaultHttpClient a() {
        DefaultHttpClient defaultHttpClient;
        synchronized (s.class) {
            if (f3608d != null) {
                defaultHttpClient = f3608d;
            } else {
                SchemeRegistry schemeRegistry = new SchemeRegistry();
                schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
                schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                f3608d = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
                HttpConnectionParams.setSoTimeout(f3608d.getParams(), 30000);
                HttpConnectionParams.setConnectionTimeout(f3608d.getParams(), 30000);
                f3608d.getParams().setIntParameter("http.protocol.max-redirects", 10);
                HttpClientParams.setCookiePolicy(f3608d.getParams(), "compatibility");
                HttpProtocolParams.setUserAgent(f3608d.getParams(), "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18");
                f3608d.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(3, true));
                defaultHttpClient = f3608d;
            }
        }
        return defaultHttpClient;
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public JSONObject f3609a;

        /* renamed from: b  reason: collision with root package name */
        public JSONObject f3610b;

        /* renamed from: c  reason: collision with root package name */
        public long f3611c;

        public String a() {
            return this.f3610b == null ? "NETWORK_FAIL" : this.f3610b.optString("msg");
        }
    }

    public static abstract class b implements c<a> {
        public void a(Exception exc, a aVar) {
            a(-1, e.a(exc));
        }
    }

    public static String a(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(date);
    }

    public static Date a(String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.parse(str);
    }
}
