package com.duapps.ad.base;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.duapps.ad.entity.a.a;
import com.duapps.ad.entity.a.b;
import com.duapps.ad.internal.a.c;
import com.duapps.ad.internal.b.e;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class d {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f3532a = {"admob", "download", "facebook", "inmobi", "online", "dlh"};

    public static long a(Context context, int i, int i2, List<String> list, ConcurrentHashMap<String, b<a>> concurrentHashMap) {
        long j = 0;
        if (context == null || list == null || list.size() == 0 || concurrentHashMap == null) {
            return 0;
        }
        a.c("ChannelFactory", "cacheSize==" + i2);
        c a2 = com.duapps.ad.internal.a.b.a(context).a(i, true);
        int i3 = 0;
        Iterator<String> it = list.iterator();
        while (true) {
            long j2 = j;
            if (!it.hasNext()) {
                return j2;
            }
            String next = it.next();
            long a3 = a2.a(next);
            int a4 = a(list, i2, next);
            a.c("ChannelFactory", "Create channel:" + next + ",wt:" + a3 + ",cacheSize: " + a4);
            b<a> a5 = a(next, context, i, a3, a4);
            if (a5 != null) {
                concurrentHashMap.put(next, a5);
                j = j2 + a3;
                a5.f3689g = j - a3;
                a.c("ChannelFactory", "channel:" + next + "startTime:" + a5.f3689g);
                a5.l = i3;
                i3++;
            } else {
                j = j2;
            }
        }
    }

    private static b<a> a(String str, Context context, int i, long j, int i2) {
        if ("facebook".equals(str)) {
            return new com.duapps.ad.entity.b(context, i, j, i2);
        }
        if ("download".equals(str)) {
            return new n(context, i, j);
        }
        if ("inmobi".equals(str)) {
            return new com.duapps.ad.inmobi.a(context, i, j, i2);
        }
        if ("dlh".equals(str)) {
            return new com.duapps.ad.b.a(context, i, j);
        }
        if ("online".equals(str)) {
            return new p(context, i, j, i2);
        }
        if ("admob".equals(str)) {
            String c2 = o.a(context).c(i);
            if (!TextUtils.isEmpty(c2)) {
                return new com.duapps.ad.a.a(context, i, j, i2, c2);
            }
            return null;
        }
        a.d("ChannelFactory", "Unsupport error channel:" + str);
        return null;
    }

    public static List<String> a(List<String> list, Context context, int i) {
        ArrayList arrayList = new ArrayList(f3532a.length);
        List asList = Arrays.asList(f3532a);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return arrayList;
            }
            String str = list.get(i3);
            if (asList.contains(str) && a(str, context, i)) {
                arrayList.add(str);
            }
            i2 = i3 + 1;
        }
    }

    public static int a(List<String> list, int i, String str) {
        if (list == null || list.size() == 0 || str == null) {
            return 1;
        }
        if (i < 1) {
            i = 1;
        }
        if (i > list.size() - 1 && i > 5) {
            i = 5;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(list);
        if (arrayList.contains("download")) {
            arrayList.remove("download");
        }
        if (arrayList.contains("dlh")) {
            arrayList.remove("dlh");
        }
        if (arrayList.size() <= 0 || i <= arrayList.size() || !((String) arrayList.get(0)).equals(str)) {
            return 1;
        }
        return i - (arrayList.size() - 1);
    }

    private static boolean a(String str, Context context, int i) {
        if ("admob".equals(str)) {
            if (TextUtils.isEmpty(o.a(context).c(i)) || Build.VERSION.SDK_INT <= 10 || !e.c()) {
                return false;
            }
            return true;
        } else if (!"facebook".equals(str)) {
            return true;
        } else {
            List<String> b2 = o.a(context).b(i);
            if (b2 == null || b2.size() <= 0 || !e.b()) {
                return false;
            }
            return true;
        }
    }
}
