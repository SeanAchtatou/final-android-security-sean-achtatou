package com.duapps.ad.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.l;
import com.duapps.ad.AdError;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.entity.AdModel;
import com.duapps.ad.entity.a.a;
import com.duapps.ad.entity.a.b;
import com.duapps.ad.entity.c;
import com.duapps.ad.internal.b.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class n extends b<a> {
    /* access modifiers changed from: private */
    public static final String n = n.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    u<AdModel> f3582a = new u<AdModel>() {
        public void a() {
            a.a(n.n, "start load cache data--");
            n.this.f3686d = true;
            n.this.f3687e = true;
        }

        public void a(int i, AdModel adModel) {
            n.this.f3686d = false;
            if (i == 200 && adModel != null) {
                List a2 = j.a(n.this.f3690h, n.this.a(adModel.f3683h));
                k.a(n.this.f3690h, a2);
                int i2 = 5;
                int size = a2.size();
                if (5 > size) {
                    i2 = size;
                }
                if (size <= 0) {
                    com.duapps.ad.stats.b.a(n.this.f3690h, n.this.i);
                    return;
                }
                synchronized (n.this.o) {
                    n.this.o.clear();
                    for (int i3 = 0; i3 < i2; i3++) {
                        n.this.o.add(a2.get(i3));
                    }
                    a.a(n.n, "store data into cache list -- list.size = " + n.this.o.size());
                }
            }
        }

        public void a(int i, String str) {
            a.a(n.n, "fail to get cache -" + str);
            n.this.f3685c = true;
            n.this.f3686d = false;
            if (n.this.l == 0 && !n.this.k && n.this.m != null) {
                n.this.m.a(new AdError(i, str));
            }
        }
    };

    /* renamed from: b  reason: collision with root package name */
    BroadcastReceiver f3583b = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("action_notify_preparse_cache_result".equals(intent.getAction())) {
                long longExtra = intent.getLongExtra("ad_id", -1);
                int intExtra = intent.getIntExtra("parse_result_type", 0);
                synchronized (n.this.o) {
                    if (n.this.o != null && n.this.o.size() > 0) {
                        Iterator it = n.this.o.iterator();
                        while (it.hasNext()) {
                            if (((AdData) it.next()).f3665b == longExtra && intExtra != 1) {
                                it.remove();
                            }
                        }
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public final List<AdData> o = Collections.synchronizedList(new LinkedList());

    public n(Context context, int i, long j) {
        super(context, i, j);
        g();
    }

    /* renamed from: a */
    public c d() {
        AdData adData;
        synchronized (this.o) {
            AdData adData2 = null;
            while (this.o.size() > 0 && ((adData2 = this.o.remove(0)) == null || !adData2.a())) {
            }
            adData = adData2;
            a.c(n, "DL poll title-> " + (adData != null ? adData.f3666c : "null") + ", pkg : " + (adData != null ? adData.f3667d : "null") + ", pp : " + (adData != null ? Integer.valueOf(adData.H) : "null"));
        }
        com.duapps.ad.stats.b.b(this.f3690h, adData == null ? "FAIL" : "OK", this.i);
        if (adData == null) {
            return null;
        }
        if (adData.f3664a == 2) {
            k.a(this.f3690h).a(adData);
        }
        return new c(this.f3690h, adData, this.m);
    }

    public int b() {
        return 1;
    }

    public void a(boolean z) {
        super.a(z);
        if (!e.a(this.f3690h)) {
            a.c(n, "network error && sid = " + this.i);
        } else if (c() > 0) {
            a.c(n, "no need refresh");
        } else if (this.f3686d) {
            a.c(n, "DL already refreshing && sid = " + this.i);
        } else {
            t.a(this.f3690h).a(Integer.valueOf(this.i).intValue(), 1, this.f3582a);
        }
    }

    public int c() {
        int i;
        synchronized (this.o) {
            Iterator<AdData> it = this.o.iterator();
            i = 0;
            while (it.hasNext()) {
                AdData next = it.next();
                if (next == null) {
                    it.remove();
                } else {
                    if (e.a(this.f3690h, next.f3667d) || !next.a()) {
                        it.remove();
                    } else {
                        i++;
                    }
                }
            }
        }
        return i;
    }

    /* access modifiers changed from: private */
    public List<AdData> a(List<AdData> list) {
        ArrayList arrayList = new ArrayList();
        for (AdData next : list) {
            if (!e.a(this.f3690h, next.f3667d)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public void e() {
        synchronized (this.o) {
            this.o.clear();
        }
    }

    private void g() {
        try {
            l.a(this.f3690h).a(this.f3583b, new IntentFilter("action_notify_preparse_cache_result"));
        } catch (Exception e2) {
        }
    }
}
