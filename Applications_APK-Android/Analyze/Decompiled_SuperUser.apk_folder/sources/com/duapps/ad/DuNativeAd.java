package com.duapps.ad;

import android.content.Context;
import android.util.Log;
import android.view.View;
import com.duapps.ad.base.l;
import com.duapps.ad.base.o;
import com.duapps.ad.entity.a.a;
import java.util.List;

public class DuNativeAd {

    /* renamed from: a  reason: collision with root package name */
    e f3489a;

    /* renamed from: b  reason: collision with root package name */
    private Context f3490b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public a f3491c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public DuAdListener f3492d;

    /* renamed from: e  reason: collision with root package name */
    private int f3493e;

    /* renamed from: f  reason: collision with root package name */
    private View f3494f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public d f3495g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f3496h;
    private b i;

    public DuNativeAd(Context context, int i2) {
        this(context, i2, 1, true);
    }

    public DuNativeAd(Context context, int i2, boolean z) {
        this(context, i2, 1, z);
    }

    public DuNativeAd(Context context, int i2, int i3) {
        this(context, i2, i3, true);
    }

    public DuNativeAd(Context context, int i2, int i3, boolean z) {
        this.i = new b() {
            public void a(a aVar) {
                a unused = DuNativeAd.this.f3491c = aVar;
                DuAdListener a2 = DuNativeAd.this.f3492d;
                if (a2 != null) {
                    a2.onAdLoaded(DuNativeAd.this);
                }
                if (DuNativeAd.this.f3495g != null) {
                    DuNativeAd.this.f3491c.a(DuNativeAd.this.f3495g);
                }
            }

            public void a(AdError adError) {
                DuAdListener a2 = DuNativeAd.this.f3492d;
                if (a2 != null) {
                    a2.onError(DuNativeAd.this, adError);
                }
            }

            public void a() {
                DuAdListener a2 = DuNativeAd.this.f3492d;
                if (a2 != null) {
                    a2.onClick(DuNativeAd.this);
                }
            }
        };
        this.f3496h = o.a(context).a(i2);
        this.f3490b = context;
        this.f3493e = i2;
        this.f3489a = (e) PullRequestController.getInstance(context.getApplicationContext()).getPullController(this.f3493e, i3, z);
        if (!this.f3496h) {
            Log.e("DAP", "DAP Pid:" + this.f3493e + "cannot found in native configuration json file");
        }
    }

    public void setFbids(List<String> list) {
        if (list == null || list.size() <= 0) {
            Log.e("DuNativeAdError", "NativeAds fbID couldn't be null");
            return;
        }
        com.duapps.ad.base.a.c("test", "change FBID :" + list.toString());
        this.f3489a.a(list);
    }

    public void setMobulaAdListener(DuAdListener duAdListener) {
        this.f3492d = duAdListener;
    }

    public void setProcessClickCallback(d dVar) {
        this.f3495g = dVar;
    }

    public void fill() {
        if (!this.f3496h) {
            Log.e("DAP", "DAP Pid:" + this.f3493e + "cannot found in native configuration json file");
        } else if (!l.g(this.f3490b)) {
            this.i.a(AdError.LOAD_TOO_FREQUENTLY);
        } else {
            this.f3489a.fill();
            l.h(this.f3490b);
        }
    }

    public boolean isHasCached() {
        return this.f3489a.a() > 0;
    }

    public DuNativeAd getCacheAd() {
        a b2 = this.f3489a.b();
        if (b2 == null) {
            return null;
        }
        this.f3491c = b2;
        if (this.f3495g == null) {
            return this;
        }
        this.f3491c.a(this.f3495g);
        return this;
    }

    public void registerViewForInteraction(View view) {
        if (isAdLoaded()) {
            if (this.f3494f != null) {
                unregisterView();
            }
            this.f3494f = view;
            this.f3491c.a(view);
        }
    }

    public void registerViewForInteraction(View view, List<View> list) {
        if (isAdLoaded()) {
            if (this.f3494f != null) {
                unregisterView();
            }
            this.f3494f = view;
            this.f3491c.a(view, list);
        }
    }

    public boolean isAdLoaded() {
        return this.f3491c != null;
    }

    public void unregisterView() {
        if (isAdLoaded()) {
            this.f3491c.b();
        }
    }

    public void load() {
        if (!this.f3496h) {
            Log.e("DAP", "DAP Pid:" + this.f3493e + "cannot found in native configuration json file");
        } else if (!l.f(this.f3490b)) {
            this.i.a(AdError.LOAD_TOO_FREQUENTLY);
        } else {
            this.f3489a.a((b) null);
            this.f3489a.a(this.i);
            this.f3489a.load();
            l.i(this.f3490b);
        }
    }

    public void destory() {
        if (isAdLoaded()) {
            this.f3491c.c();
        }
        this.f3489a.a((b) null);
        this.f3489a.destroy();
    }

    public void clearCache() {
        this.f3489a.clearCache();
    }

    public String getTitle() {
        if (isAdLoaded()) {
            return this.f3491c.h();
        }
        return null;
    }

    public String getShortDesc() {
        if (isAdLoaded()) {
            return this.f3491c.g();
        }
        return null;
    }

    public String getIconUrl() {
        if (isAdLoaded()) {
            return this.f3491c.e();
        }
        return null;
    }

    public String getImageUrl() {
        if (isAdLoaded()) {
            return this.f3491c.d();
        }
        return null;
    }

    public float getRatings() {
        if (isAdLoaded()) {
            return this.f3491c.i();
        }
        return 4.5f;
    }

    public String getCallToAction() {
        if (isAdLoaded()) {
            return this.f3491c.f();
        }
        return null;
    }

    public String getSource() {
        if (isAdLoaded()) {
            return this.f3491c.l();
        }
        return null;
    }

    public int getAdChannelType() {
        if (isAdLoaded()) {
            return this.f3491c.j();
        }
        return -1;
    }

    public a getRealSource() {
        if (isAdLoaded()) {
            return this.f3491c;
        }
        return null;
    }

    public float getInctRank() {
        if (isAdLoaded()) {
            return (float) this.f3491c.m();
        }
        return -1.0f;
    }
}
