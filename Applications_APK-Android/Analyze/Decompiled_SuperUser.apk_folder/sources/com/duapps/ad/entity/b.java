package com.duapps.ad.entity;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import com.duapps.ad.AdError;
import com.duapps.ad.base.l;
import com.duapps.ad.base.o;
import com.duapps.ad.entity.a.a;
import com.duapps.ad.internal.b.e;
import com.duapps.ad.stats.g;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class b extends com.duapps.ad.entity.a.b<a> implements Handler.Callback {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public List<String> f3693a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private int f3694b;
    private int n = 0;
    /* access modifiers changed from: private */
    public final List<d> o = new LinkedList();
    /* access modifiers changed from: private */
    public Handler p;
    /* access modifiers changed from: private */
    public Context q = null;

    private String f() {
        String str;
        synchronized (this.f3693a) {
            if (this.f3693a.size() <= 0) {
                str = null;
            } else if (this.n >= this.f3693a.size()) {
                str = this.f3693a.get(0);
            } else {
                str = this.f3693a.get(this.n);
                this.n = (this.n + 1) % this.f3693a.size();
            }
        }
        return str;
    }

    public b(Context context, int i, long j, int i2) {
        super(context, i, j);
        this.q = context.getApplicationContext();
        e.a(this.i);
        b(o.a(this.q).b(i));
        HandlerThread handlerThread = new HandlerThread("fbnative", 10);
        handlerThread.start();
        this.p = new Handler(handlerThread.getLooper(), this);
        if (this.f3693a.size() <= 0) {
            com.duapps.ad.base.a.c("FbCache", "Refresh request failed: no available Placement Id");
            Log.e("DuNativeAd", "Please setup fbids in DuAdNetwork init method");
        }
        this.f3694b = (i2 <= 0 || i2 > 5) ? 1 : i2;
        this.p.sendEmptyMessageDelayed(1, l.r(this.q));
    }

    /* renamed from: a */
    public a d() {
        d dVar;
        d dVar2 = null;
        synchronized (this.o) {
            while (true) {
                if (this.o.size() <= 0) {
                    dVar = dVar2;
                    break;
                }
                dVar2 = this.o.remove(0);
                if (dVar2 != null) {
                    if (dVar2.a()) {
                        dVar = dVar2;
                        break;
                    }
                    dVar2.c();
                }
            }
        }
        com.duapps.ad.stats.b.a(this.q, dVar == null ? "FAIL" : "OK", this.i);
        return dVar;
    }

    public int b() {
        return this.f3694b;
    }

    public int c() {
        int i;
        int i2 = 0;
        synchronized (this.o) {
            Iterator<d> it = this.o.iterator();
            while (it.hasNext()) {
                d next = it.next();
                if (next == null) {
                    it.remove();
                } else {
                    if (next.a()) {
                        i = i2 + 1;
                    } else {
                        it.remove();
                        next.c();
                        i = i2;
                    }
                    i2 = i;
                }
            }
        }
        return i2;
    }

    public void a(boolean z) {
        super.a(z);
        if (!e.a(this.q)) {
            com.duapps.ad.base.a.c("FbCache", "network error && sid = " + this.i);
            return;
        }
        com.duapps.ad.base.a.c("FbCache", "Refresh request...");
        if (this.f3694b <= 0) {
            this.f3685c = true;
            com.duapps.ad.base.a.c("FbCache", "Refresh request failed: no available Placement Id");
            return;
        }
        this.f3685c = false;
        this.p.obtainMessage(0).sendToTarget();
    }

    public boolean handleMessage(Message message) {
        int i;
        int i2;
        int i3 = message.what;
        if (i3 == 0) {
            this.p.removeMessages(0);
            if (this.f3686d) {
                com.duapps.ad.base.a.c("FbCache", "Refresh request failed: already refreshing");
                return true;
            }
            this.f3686d = true;
            this.f3687e = true;
            synchronized (this.o) {
                Iterator<d> it = this.o.iterator();
                i = 0;
                while (it.hasNext()) {
                    d next = it.next();
                    if (next == null) {
                        it.remove();
                    } else {
                        if (next.a()) {
                            i2 = i + 1;
                        } else {
                            it.remove();
                            next.c();
                            i2 = i;
                        }
                        i = i2;
                    }
                }
            }
            if (i < this.f3694b) {
                int i4 = this.f3694b - i;
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("FbCache", "Refresh request send: green = " + i + " ,need = " + i4);
                }
                this.p.obtainMessage(2, i4, 0).sendToTarget();
            } else {
                com.duapps.ad.base.a.c("FbCache", "Refresh request OK: green is full");
                this.f3686d = false;
            }
            return true;
        } else if (i3 != 2) {
            return false;
        } else {
            int i5 = message.arg1;
            if (i5 > 0) {
                a(message, i5);
            } else {
                this.f3686d = false;
                com.duapps.ad.base.a.c("FbCache", "Refresh result: DONE for geeen count");
            }
            return true;
        }
    }

    private void a(Message message, int i) {
        final String f2 = f();
        if (com.duapps.ad.base.a.a()) {
            synchronized (this.f3693a) {
                int size = this.f3693a.size();
                StringBuffer stringBuffer = new StringBuffer();
                for (int i2 = 0; i2 < size; i2++) {
                    stringBuffer.append(this.f3693a.get(i2) + ",");
                }
                com.duapps.ad.base.a.c("FbCache", stringBuffer.toString());
            }
        }
        com.duapps.ad.base.a.c("FbCache", "refresh FB -> id = " + f2);
        if (f2 == null) {
            com.duapps.ad.base.a.d("DuNativeAd", "No Available Placement ID");
            this.f3686d = false;
            this.f3687e = false;
            return;
        }
        final d dVar = new d(this.q, f2, this.i);
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final int i3 = i;
        dVar.a(new a() {
            /* JADX INFO: finally extract failed */
            public void a(d dVar, boolean z) {
                com.duapps.ad.base.a.c("FbCache", "onAdLoaded: id=" + f2);
                int m = l.m(b.this.q);
                ReentrantLock reentrantLock = new ReentrantLock();
                try {
                    reentrantLock.lock();
                    int currentTimeMillis = ((int) System.currentTimeMillis()) / AdError.NETWORK_ERROR_CODE;
                    if (currentTimeMillis > m) {
                        g.a(b.this.q, b.this.i, b.this.f3693a.toString());
                        l.f(b.this.q, currentTimeMillis + 86400);
                    }
                    reentrantLock.unlock();
                    l.a(b.this.q);
                    l.b(b.this.q);
                    synchronized (b.this.o) {
                        b.this.o.add(dVar);
                    }
                    a(200);
                } catch (Throwable th) {
                    reentrantLock.unlock();
                    throw th;
                }
            }

            private void a(int i) {
                com.duapps.ad.stats.b.a(b.this.q, b.this.i, i, SystemClock.elapsedRealtime() - elapsedRealtime);
                com.duapps.ad.base.a.c("FbCache", "Refresh result: id = " + dVar.o() + "; code = " + i);
                if (i3 > 0) {
                    b.this.p.obtainMessage(2, i3 - 1, 0).sendToTarget();
                    return;
                }
                b.this.f3686d = false;
                com.duapps.ad.base.a.c("FbCache", "Refresh result: DONE for geeen count");
            }
        });
        dVar.n();
    }

    public void e() {
        synchronized (this.o) {
            this.o.clear();
        }
    }

    public void a(List<String> list) {
        if (list != null && list.size() != 0) {
            l.a(this.q, list, this.i);
            b(list);
        }
    }

    private void b(List<String> list) {
        if (list != null && list.size() > 0) {
            synchronized (this.f3693a) {
                this.f3693a.clear();
                this.f3693a.addAll(list);
            }
        }
    }
}
