package com.duapps.ad.entity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.l;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.duapps.ad.b;
import com.duapps.ad.base.k;
import com.duapps.ad.base.s;
import com.duapps.ad.base.v;
import com.duapps.ad.d;
import com.duapps.ad.stats.e;
import com.duapps.ad.stats.g;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;
import org.apache.http.Header;

public class c implements com.duapps.ad.entity.a.a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f3700a = c.class.getSimpleName();
    /* access modifiers changed from: private */
    public static Handler n = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Context f3701b;

    /* renamed from: c  reason: collision with root package name */
    private List<View> f3702c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public View f3703d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public AdData f3704e;

    /* renamed from: f  reason: collision with root package name */
    private WeakHashMap<View, WeakReference<c>> f3705f = new WeakHashMap<>();

    /* renamed from: g  reason: collision with root package name */
    private a f3706g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public com.duapps.ad.stats.c f3707h;
    /* access modifiers changed from: private */
    public b i;
    /* access modifiers changed from: private */
    public View.OnTouchListener j;
    /* access modifiers changed from: private */
    public d k;
    /* access modifiers changed from: private */
    public boolean l;
    private boolean m;
    /* access modifiers changed from: private */
    public Runnable o = new Runnable() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duapps.ad.entity.c.a(com.duapps.ad.entity.c, boolean):boolean
         arg types: [com.duapps.ad.entity.c, int]
         candidates:
          com.duapps.ad.entity.c.a(com.duapps.ad.entity.c, com.duapps.ad.stats.c):com.duapps.ad.stats.c
          com.duapps.ad.entity.c.a(java.util.List<android.view.View>, android.view.View):void
          com.duapps.ad.entity.c.a(android.view.View, java.util.List<android.view.View>):void
          com.duapps.ad.entity.a.a.a(android.view.View, java.util.List<android.view.View>):void
          com.duapps.ad.entity.c.a(com.duapps.ad.entity.c, boolean):boolean */
        public void run() {
            boolean unused = c.this.l = false;
            c.this.t();
            c.this.r();
        }
    };
    private BroadcastReceiver p = new BroadcastReceiver() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duapps.ad.entity.c.a(com.duapps.ad.entity.c, boolean):boolean
         arg types: [com.duapps.ad.entity.c, int]
         candidates:
          com.duapps.ad.entity.c.a(com.duapps.ad.entity.c, com.duapps.ad.stats.c):com.duapps.ad.stats.c
          com.duapps.ad.entity.c.a(java.util.List<android.view.View>, android.view.View):void
          com.duapps.ad.entity.c.a(android.view.View, java.util.List<android.view.View>):void
          com.duapps.ad.entity.a.a.a(android.view.View, java.util.List<android.view.View>):void
          com.duapps.ad.entity.c.a(com.duapps.ad.entity.c, boolean):boolean */
        public void onReceive(Context context, Intent intent) {
            if ("action_notify_preparse_cache_result".equals(intent.getAction())) {
                long longExtra = intent.getLongExtra("ad_id", -1);
                if (c.this.f3704e != null && c.this.l && longExtra == c.this.f3704e.f3665b) {
                    c.this.r();
                    c.this.t();
                    c.n.removeCallbacks(c.this.o);
                    boolean unused = c.this.l = false;
                }
            }
        }
    };

    public c(Context context, AdData adData, b bVar) {
        this.f3704e = adData;
        this.f3701b = context;
        this.i = bVar;
        this.f3702c = Collections.synchronizedList(new ArrayList());
    }

    public void a(View view) {
        ArrayList arrayList = new ArrayList();
        a(arrayList, view);
        a(view, arrayList);
    }

    public void a(View view, List<View> list) {
        if (view == null) {
            com.duapps.ad.base.a.d(f3700a, "registerViewForInteraction() -> Must provide a view");
        } else if (list == null || list.size() == 0) {
            com.duapps.ad.base.a.d(f3700a, "registerViewForInteraction() -> Invalid set of clickable views");
        } else if (!p()) {
            com.duapps.ad.base.a.d(f3700a, "registerViewForInteraction() -> Ad not loaded");
        } else {
            if (this.f3703d != null) {
                com.duapps.ad.base.a.b(f3700a, "Native Ad was already registered with a View, Auto unregistering and proceeding");
                b();
            }
            if (this.f3705f.containsKey(view) && this.f3705f.get(view).get() != null) {
                ((c) this.f3705f.get(view).get()).b();
                com.duapps.ad.base.a.c("NativeAdDLWrapper", "has perform unregisterview");
            }
            this.f3706g = new a();
            this.f3703d = view;
            for (View b2 : list) {
                b(b2);
            }
            this.f3705f.put(view, new WeakReference(this));
            if (!this.m) {
                g.i(this.f3701b, new e(this.f3704e));
                String[] strArr = this.f3704e.F;
                if (strArr != null && strArr.length > 0) {
                    for (final String str : strArr) {
                        v.a().a(new Runnable() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.duapps.ad.base.s.a(java.net.URL, java.util.List<org.apache.http.Header>, boolean):org.apache.http.HttpResponse
                             arg types: [java.net.URL, ?[OBJECT, ARRAY], int]
                             candidates:
                              com.duapps.ad.base.s.a(java.net.URI, java.lang.String, java.util.List<org.apache.http.Header>):org.apache.http.HttpResponse
                              com.duapps.ad.base.s.a(java.net.URL, com.duapps.ad.base.s$b, long):void
                              com.duapps.ad.base.s.a(java.net.URL, java.util.List<org.apache.http.Header>, boolean):org.apache.http.HttpResponse */
                            public void run() {
                                try {
                                    int statusCode = s.a(new URL(str), (List<Header>) null, true).getStatusLine().getStatusCode();
                                    if (statusCode != 200) {
                                        com.duapps.ad.base.a.c(c.f3700a, "Impression to " + c.this.f3704e.o + " failed!");
                                    } else if (statusCode == 200) {
                                        com.duapps.ad.base.a.c(c.f3700a, "Impression to " + c.this.f3704e.o + " success!");
                                    }
                                    com.duapps.ad.stats.b.a(c.this.f3701b, c.this.f3704e, statusCode);
                                } catch (Exception e2) {
                                    com.duapps.ad.base.a.c(c.f3700a, "Impression to " + c.this.f3704e.o + " exception!");
                                }
                            }
                        });
                    }
                }
                this.m = true;
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean p() {
        return this.f3704e != null;
    }

    public void b() {
        if (this.f3703d != null) {
            if (!this.f3705f.containsKey(this.f3703d) || this.f3705f.get(this.f3703d).get() != this) {
                com.duapps.ad.base.a.b(f3700a, "unregisterView() -> View not regitered with this NativeAd");
                return;
            }
            this.f3705f.remove(this.f3703d);
            q();
            this.f3703d = null;
        }
    }

    private void q() {
        synchronized (this.f3702c) {
            Iterator<View> it = this.f3702c.iterator();
            while (it.hasNext()) {
                View next = it.next();
                if (next == null) {
                    it.remove();
                } else {
                    next.setOnClickListener(null);
                    next.setOnTouchListener(null);
                }
            }
            this.f3702c.clear();
        }
    }

    private void b(View view) {
        synchronized (this.f3702c) {
            this.f3702c.add(view);
        }
        view.setOnClickListener(this.f3706g);
        view.setOnTouchListener(this.f3706g);
    }

    private void a(List<View> list, View view) {
        list.add(view);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                a(list, viewGroup.getChildAt(i2));
            }
        }
    }

    /* access modifiers changed from: private */
    public void r() {
        com.duapps.ad.base.a.c(f3700a, "mClickHandler handleClick");
        if (this.f3707h != null && this.f3704e != null) {
            this.f3707h.a(new e(this.f3704e));
            if (this.f3704e.G != null && this.f3704e.G.length > 0 && com.duapps.ad.internal.b.e.a(this.f3701b)) {
                g.a(this.f3704e);
            }
        }
    }

    class a implements View.OnClickListener, View.OnTouchListener {

        /* renamed from: b  reason: collision with root package name */
        private int f3713b;

        /* renamed from: c  reason: collision with root package name */
        private int f3714c;

        /* renamed from: d  reason: collision with root package name */
        private int f3715d;

        /* renamed from: e  reason: collision with root package name */
        private int f3716e;

        /* renamed from: f  reason: collision with root package name */
        private float f3717f;

        /* renamed from: g  reason: collision with root package name */
        private float f3718g;

        /* renamed from: h  reason: collision with root package name */
        private int f3719h;
        private int i;
        private boolean j;

        a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duapps.ad.entity.c.a(com.duapps.ad.entity.c, boolean):boolean
         arg types: [com.duapps.ad.entity.c, int]
         candidates:
          com.duapps.ad.entity.c.a(com.duapps.ad.entity.c, com.duapps.ad.stats.c):com.duapps.ad.stats.c
          com.duapps.ad.entity.c.a(java.util.List<android.view.View>, android.view.View):void
          com.duapps.ad.entity.c.a(android.view.View, java.util.List<android.view.View>):void
          com.duapps.ad.entity.a.a.a(android.view.View, java.util.List<android.view.View>):void
          com.duapps.ad.entity.c.a(com.duapps.ad.entity.c, boolean):boolean */
        public void onClick(View view) {
            if (c.this.i != null) {
                c.this.i.a();
                com.duapps.ad.base.a.c("NativeAdDLWrapper", "dl has click.....");
            }
            if (this.j) {
                com.duapps.ad.base.a.c(c.f3700a, "No touch data recorded,please ensure touch events reach the ad View by returing false if you intercept the event.");
            }
            if (c.this.f3707h == null) {
                com.duapps.ad.stats.c unused = c.this.f3707h = new com.duapps.ad.stats.c(c.this.f3701b);
                c.this.f3707h.a(c.this.k);
            }
            if (!c.this.p() || c.this.f3707h.e()) {
                com.duapps.ad.base.a.c(c.f3700a, "mClickHandler isWorking");
                return;
            }
            com.duapps.ad.base.a.c(c.f3700a, "mClickHandler handleClick");
            if (!AdData.b(c.this.f3704e) || !k.a(c.this.f3701b).b(c.this.f3704e)) {
                c.this.r();
            } else if (!c.this.l) {
                boolean unused2 = c.this.l = true;
                c.this.s();
                c.n.postDelayed(c.this.o, 4000);
            }
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 0 && c.this.f3703d != null) {
                this.f3715d = c.this.f3703d.getWidth();
                this.f3716e = c.this.f3703d.getHeight();
                int[] iArr = new int[2];
                c.this.f3703d.getLocationInWindow(iArr);
                this.f3717f = (float) iArr[0];
                this.f3718g = (float) iArr[1];
                Rect rect = new Rect();
                c.this.f3703d.getGlobalVisibleRect(rect);
                this.i = rect.width();
                this.f3719h = rect.height();
                int[] iArr2 = new int[2];
                view.getLocationInWindow(iArr2);
                this.f3713b = (((int) motionEvent.getX()) + iArr2[0]) - iArr[0];
                this.f3714c = (iArr2[1] + ((int) motionEvent.getY())) - iArr[1];
                this.j = true;
            }
            if (c.this.j != null) {
                return c.this.j.onTouch(view, motionEvent);
            }
            return false;
        }
    }

    public String d() {
        if (!p()) {
            return null;
        }
        return this.f3704e.t;
    }

    public String e() {
        if (!p()) {
            return null;
        }
        return this.f3704e.f3671h;
    }

    public String f() {
        if (!p()) {
            return null;
        }
        return this.f3704e.B;
    }

    public String g() {
        if (!p()) {
            return null;
        }
        return this.f3704e.f3669f;
    }

    public String h() {
        if (!p()) {
            return null;
        }
        return this.f3704e.f3666c;
    }

    public float i() {
        if (!p()) {
            return 0.0f;
        }
        return this.f3704e.l;
    }

    public void c() {
        this.k = null;
    }

    public int j() {
        return 1;
    }

    public String l() {
        return "dl";
    }

    public void a(d dVar) {
        this.k = dVar;
    }

    /* renamed from: a */
    public AdData k() {
        return this.f3704e;
    }

    public int m() {
        if (p()) {
            return this.f3704e.I;
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public void s() {
        try {
            l.a(this.f3701b).a(this.p, new IntentFilter("action_notify_preparse_cache_result"));
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public void t() {
        try {
            l.a(this.f3701b).a(this.p);
        } catch (Exception e2) {
        }
    }
}
