package com.duapps.ad.entity.a;

import android.content.Context;
import com.duapps.ad.AdError;
import com.duapps.ad.internal.b.e;

public class b<T> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f3684a;

    /* renamed from: c  reason: collision with root package name */
    public volatile boolean f3685c;

    /* renamed from: d  reason: collision with root package name */
    public volatile boolean f3686d;

    /* renamed from: e  reason: collision with root package name */
    public volatile boolean f3687e;

    /* renamed from: f  reason: collision with root package name */
    public long f3688f;

    /* renamed from: g  reason: collision with root package name */
    public long f3689g;
    /* access modifiers changed from: protected */

    /* renamed from: h  reason: collision with root package name */
    public Context f3690h;
    /* access modifiers changed from: protected */
    public int i;
    protected d j;
    public volatile boolean k;
    public int l;
    /* access modifiers changed from: protected */
    public com.duapps.ad.b m = new com.duapps.ad.b() {
        public void a(a aVar) {
        }

        public void a(AdError adError) {
            if (b.this.j != null && b.this.f3684a) {
                b.this.j.a(b.this, adError);
            }
        }

        public void a() {
            if (b.this.j != null) {
                b.this.j.a(b.this);
            }
        }
    };

    public b(Context context, int i2, long j2) {
        this.f3688f = j2;
        this.f3690h = context;
        this.i = i2;
        e.a(i2);
    }

    public int b() {
        return 0;
    }

    public void a(boolean z) {
        this.f3684a = z;
    }

    public T d() {
        return null;
    }

    public int c() {
        return 0;
    }

    public void a(d dVar) {
        this.j = dVar;
    }

    public void e() {
    }
}
