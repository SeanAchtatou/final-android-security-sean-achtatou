package com.duapps.ad.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdModel implements Parcelable {
    public static final Parcelable.Creator<AdModel> CREATOR = new Parcelable.Creator<AdModel>() {
        /* renamed from: a */
        public AdModel createFromParcel(Parcel parcel) {
            return new AdModel(parcel);
        }

        /* renamed from: a */
        public AdModel[] newArray(int i) {
            return new AdModel[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public String f3676a;

    /* renamed from: b  reason: collision with root package name */
    public String f3677b;

    /* renamed from: c  reason: collision with root package name */
    public int f3678c;

    /* renamed from: d  reason: collision with root package name */
    public String f3679d;

    /* renamed from: e  reason: collision with root package name */
    public int f3680e;

    /* renamed from: f  reason: collision with root package name */
    public int f3681f;

    /* renamed from: g  reason: collision with root package name */
    public int f3682g;

    /* renamed from: h  reason: collision with root package name */
    public final List<AdData> f3683h;
    public long i;
    public String j;
    public String k;
    public String l;
    public String m;
    public String n;

    public AdModel() {
        this.f3683h = new ArrayList();
    }

    public AdModel(String str, int i2, String str2, JSONObject jSONObject, long j2) {
        this.f3683h = new ArrayList();
        this.f3676a = str;
        this.f3678c = i2;
        this.f3679d = str2;
        if (jSONObject != null && i2 == jSONObject.optInt("sId")) {
            this.n = jSONObject.optString("pk");
            this.f3680e = jSONObject.optInt("pn");
            this.f3681f = jSONObject.optInt("ps");
            this.f3682g = jSONObject.optInt("total");
            this.f3677b = jSONObject.optString("logId");
            this.j = jSONObject.optString("ext");
            this.k = jSONObject.optString("title");
            this.l = jSONObject.optString("shortdesc");
            this.m = jSONObject.optString("description");
            JSONArray optJSONArray = jSONObject.optJSONArray("list");
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i3 = 0; i3 < length; i3++) {
                    JSONObject optJSONObject = optJSONArray.optJSONObject(i3);
                    if (optJSONObject != null) {
                        this.f3683h.add(new AdData(str, i2, str2, this.f3677b, optJSONObject, j2));
                    }
                }
            }
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.f3676a);
        parcel.writeString(this.f3677b);
        parcel.writeInt(this.f3678c);
        parcel.writeString(this.f3679d);
        parcel.writeInt(this.f3680e);
        parcel.writeInt(this.f3681f);
        parcel.writeInt(this.f3682g);
        parcel.writeTypedList(this.f3683h);
        parcel.writeString(this.j);
        parcel.writeString(this.k);
        parcel.writeString(this.l);
        parcel.writeString(this.m);
        parcel.writeLong(this.i);
        parcel.writeString(this.n);
    }

    private AdModel(Parcel parcel) {
        this.f3683h = new ArrayList();
        this.f3676a = parcel.readString();
        this.f3677b = parcel.readString();
        this.f3678c = parcel.readInt();
        this.f3679d = parcel.readString();
        this.f3680e = parcel.readInt();
        this.f3681f = parcel.readInt();
        this.f3682g = parcel.readInt();
        parcel.readTypedList(this.f3683h, AdData.CREATOR);
        this.j = parcel.readString();
        this.k = parcel.readString();
        this.l = parcel.readString();
        this.m = parcel.readString();
        this.i = parcel.readLong();
        this.n = parcel.readString();
    }
}
