package com.duapps.ad.entity;

import android.content.Context;
import android.view.View;
import com.duapps.ad.base.l;
import com.duapps.ad.entity.a.a;
import com.duapps.ad.stats.g;
import com.facebook.ads.AdListener;
import com.facebook.ads.NativeAd;
import java.util.List;

public class d implements a, AdListener {

    /* renamed from: g  reason: collision with root package name */
    private static final a f3720g = new a() {
        public void a(d dVar, boolean z) {
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private NativeAd f3721a;

    /* renamed from: b  reason: collision with root package name */
    private a f3722b = f3720g;

    /* renamed from: c  reason: collision with root package name */
    private String f3723c;

    /* renamed from: d  reason: collision with root package name */
    private volatile boolean f3724d = false;

    /* renamed from: e  reason: collision with root package name */
    private Context f3725e;

    /* renamed from: f  reason: collision with root package name */
    private int f3726f;

    /* renamed from: h  reason: collision with root package name */
    private long f3727h = 0;

    public boolean a() {
        long currentTimeMillis = System.currentTimeMillis() - this.f3727h;
        com.duapps.ad.base.a.c("NativeAdFBWrapper", "isValid()...ttl : " + currentTimeMillis + ", FacebookCacheTime : " + l.r(this.f3725e));
        return currentTimeMillis < l.r(this.f3725e) && currentTimeMillis > 0;
    }

    public d(Context context, String str, int i) {
        this.f3725e = context;
        this.f3723c = str;
        this.f3726f = i;
        this.f3721a = new NativeAd(context, str);
        this.f3721a.setAdListener(this);
    }

    public void n() {
        if (this.f3721a.isAdLoaded()) {
            this.f3722b.a(this, true);
        } else if (!this.f3724d) {
            this.f3724d = true;
            this.f3721a.loadAd();
        }
    }

    public void a(a aVar) {
        if (aVar == null) {
            this.f3722b = f3720g;
        } else {
            this.f3722b = aVar;
        }
    }

    public void c() {
        this.f3722b = f3720g;
        this.f3721a.destroy();
    }

    public String o() {
        return this.f3723c;
    }

    public float i() {
        NativeAd.Rating adStarRating = this.f3721a.getAdStarRating();
        if (adStarRating != null) {
            return (float) adStarRating.getValue();
        }
        return 4.5f;
    }

    public String d() {
        return this.f3721a.getAdCoverImage().getUrl();
    }

    public String e() {
        return this.f3721a.getAdIcon().getUrl();
    }

    public String f() {
        return this.f3721a.getAdCallToAction();
    }

    public String g() {
        return this.f3721a.getAdBody();
    }

    public String h() {
        return this.f3721a.getAdTitle();
    }

    public void b() {
        this.f3721a.unregisterView();
    }

    public void a(View view) {
        try {
            this.f3721a.registerViewForInteraction(view);
        } catch (Exception e2) {
        }
        g.a(this.f3725e, this.f3726f);
    }

    public void a(View view, List<View> list) {
        try {
            this.f3721a.registerViewForInteraction(view, list);
        } catch (Exception e2) {
        }
        g.a(this.f3725e, this.f3726f);
    }

    public int j() {
        return 2;
    }

    public void a(com.duapps.ad.d dVar) {
    }

    public Object k() {
        return this.f3721a;
    }

    public String l() {
        return "fb";
    }

    public int m() {
        return -1;
    }
}
