package com.duapps.ad.entity;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.duapps.ad.base.o;
import com.duapps.ad.internal.b.e;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.entity.UninstallAppInfo;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdData implements Parcelable {
    public static final Parcelable.Creator<AdData> CREATOR = new Parcelable.Creator<AdData>() {
        /* renamed from: a */
        public AdData createFromParcel(Parcel parcel) {
            return new AdData(parcel);
        }

        /* renamed from: a */
        public AdData[] newArray(int i) {
            return new AdData[i];
        }
    };
    private static final HashSet<String> J = new HashSet<>();
    public int A;
    public String B;
    public long C;
    public long D;
    public String E;
    public String[] F;
    public String[] G;
    public int H;
    public int I;
    private a K;

    /* renamed from: a  reason: collision with root package name */
    public int f3664a;

    /* renamed from: b  reason: collision with root package name */
    public long f3665b;

    /* renamed from: c  reason: collision with root package name */
    public String f3666c;

    /* renamed from: d  reason: collision with root package name */
    public String f3667d;

    /* renamed from: e  reason: collision with root package name */
    public String f3668e;

    /* renamed from: f  reason: collision with root package name */
    public String f3669f;

    /* renamed from: g  reason: collision with root package name */
    public int f3670g;

    /* renamed from: h  reason: collision with root package name */
    public String f3671h;
    public String i;
    public boolean j;
    public boolean k;
    public float l;
    public int m;
    public int n;
    public String o;
    public long p;
    public int q;
    public float r;
    public int s;
    public String t;
    public String u;
    public String v;
    public String w;
    public String x;
    public int y;
    public String z;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public String f3672a;

        /* renamed from: b  reason: collision with root package name */
        public String f3673b;

        /* renamed from: c  reason: collision with root package name */
        public String f3674c;

        /* renamed from: d  reason: collision with root package name */
        public String f3675d;
    }

    static {
        J.add("sites");
        J.add("yeahmobi");
        J.add("matomy");
        J.add("kissmyads");
        J.add("applift");
        J.add("glispa");
        J.add("appflood");
        J.add("efun");
        J.add("motiveinteractive");
        J.add("apploop");
        J.add("performence");
        J.add("admobix");
    }

    protected AdData() {
        this.f3670g = -1;
        this.n = -1;
        this.E = "download";
    }

    public static AdData a(Context context, int i2, String str, String str2, String str3) {
        AdData adData = new AdData();
        adData.x = o.b(context);
        adData.y = -1001;
        adData.f3665b = (long) i2;
        adData.w = "directflow";
        adData.z = "directflow";
        adData.f3666c = str;
        adData.f3667d = str2;
        adData.i = str3;
        adData.n = 1;
        return adData;
    }

    public AdData(String str, int i2, String str2, String str3, JSONObject jSONObject, long j2) {
        this(str, i2, str2, str3, jSONObject);
        this.C = j2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.entity.AdData.a(org.json.JSONArray, boolean):java.lang.String
     arg types: [org.json.JSONArray, int]
     candidates:
      com.duapps.ad.entity.AdData.a(android.content.Context, com.duapps.ad.entity.AdData):boolean
      com.duapps.ad.entity.AdData.a(org.json.JSONArray, boolean):java.lang.String */
    public AdData(String str, int i2, String str2, String str3, JSONObject jSONObject) {
        this.f3670g = -1;
        this.n = -1;
        this.E = "download";
        this.x = str;
        this.y = i2;
        this.z = str2;
        this.w = str3;
        this.f3665b = jSONObject.optLong("id");
        this.f3666c = jSONObject.optString("title");
        this.o = jSONObject.optString(FirebaseAnalytics.Param.SOURCE);
        this.i = jSONObject.optString("adUrl");
        try {
            this.i = e.e(this.i);
        } catch (Exception e2) {
        }
        this.f3667d = jSONObject.optString(UninstallAppInfo.COLUMN_PKG);
        try {
            this.f3667d = e.e(this.f3667d);
        } catch (Exception e3) {
        }
        this.f3669f = jSONObject.optString("shortDesc");
        this.f3668e = jSONObject.optString("description");
        this.n = jSONObject.optInt("openType", -1);
        this.m = jSONObject.optInt("integral");
        this.l = (float) jSONObject.optDouble("pts", 4.5d);
        this.r = (float) jSONObject.optDouble("contentRating", 0.0d);
        this.s = jSONObject.optInt("label", 0);
        this.u = jSONObject.optString("cate");
        this.v = jSONObject.optString("exg");
        this.A = jSONObject.optInt("preClick");
        this.H = jSONObject.optInt("pp", 0);
        this.f3664a = jSONObject.optInt("tts", 0);
        JSONArray optJSONArray = jSONObject.optJSONArray("images");
        this.f3671h = a(optJSONArray, true);
        this.K = a(optJSONArray);
        this.t = a(jSONObject.optJSONArray("bigImages"), true);
        this.B = jSONObject.optString("buttonDes");
        this.D = jSONObject.optLong("cacheTime", 120);
        this.F = b(jSONObject.optJSONArray("impUrls"));
        this.G = b(jSONObject.optJSONArray("cUrls"));
        this.I = jSONObject.optInt("inctRank", -1);
    }

    private static String[] b(JSONArray jSONArray) {
        if (jSONArray != null) {
            int length = jSONArray.length();
            String[] strArr = new String[length];
            int i2 = 0;
            while (i2 < length) {
                try {
                    strArr[i2] = jSONArray.getString(i2);
                    strArr[i2] = e.e(strArr[i2]);
                    i2++;
                } catch (JSONException e2) {
                    e2.printStackTrace();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
            return strArr;
        }
        return null;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        byte b2 = 1;
        parcel.writeLong(this.f3665b);
        parcel.writeString(this.f3666c);
        parcel.writeString(this.f3667d);
        parcel.writeString(this.f3668e);
        parcel.writeString(this.f3669f);
        parcel.writeInt(this.f3670g);
        parcel.writeString(this.f3671h);
        parcel.writeString(this.i);
        parcel.writeByte(this.j ? (byte) 1 : 0);
        if (!this.k) {
            b2 = 0;
        }
        parcel.writeByte(b2);
        parcel.writeFloat(this.l);
        parcel.writeInt(this.m);
        parcel.writeInt(this.n);
        parcel.writeString(this.o);
        parcel.writeLong(this.p);
        parcel.writeInt(this.q);
        parcel.writeFloat(this.r);
        parcel.writeInt(this.s);
        parcel.writeString(this.u);
        parcel.writeString(this.v);
        parcel.writeInt(this.A);
        parcel.writeInt(this.H);
        parcel.writeString(this.t);
        parcel.writeString(this.w);
        parcel.writeString(this.x);
        parcel.writeInt(this.y);
        parcel.writeString(this.z);
        parcel.writeString(this.B);
        parcel.writeLong(this.D);
        parcel.writeLong(this.C);
        parcel.writeStringArray(this.F);
        parcel.writeStringArray(this.G);
        parcel.writeInt(this.H);
        parcel.writeInt(this.I);
        parcel.writeInt(this.f3664a);
    }

    private AdData(Parcel parcel) {
        boolean z2 = true;
        this.f3670g = -1;
        this.n = -1;
        this.E = "download";
        this.f3665b = parcel.readLong();
        this.f3666c = parcel.readString();
        this.f3667d = parcel.readString();
        this.f3668e = parcel.readString();
        this.f3669f = parcel.readString();
        this.f3670g = parcel.readInt();
        this.f3671h = parcel.readString();
        this.i = parcel.readString();
        this.j = parcel.readByte() != 0;
        this.k = parcel.readByte() == 0 ? false : z2;
        this.l = parcel.readFloat();
        this.m = parcel.readInt();
        this.n = parcel.readInt();
        this.o = parcel.readString();
        this.p = parcel.readLong();
        this.q = parcel.readInt();
        this.r = parcel.readFloat();
        this.s = parcel.readInt();
        this.u = parcel.readString();
        this.v = parcel.readString();
        this.A = parcel.readInt();
        this.H = parcel.readInt();
        this.t = parcel.readString();
        this.w = parcel.readString();
        this.x = parcel.readString();
        this.y = parcel.readInt();
        this.z = parcel.readString();
        this.B = parcel.readString();
        this.D = parcel.readLong();
        this.C = parcel.readLong();
        this.F = parcel.createStringArray();
        this.G = parcel.createStringArray();
        this.H = parcel.readInt();
        this.I = parcel.readInt();
        this.f3664a = parcel.readInt();
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((this.f3667d == null ? 0 : this.f3667d.hashCode()) + 31) * 31;
        if (this.i != null) {
            i2 = this.i.hashCode();
        }
        return hashCode + i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AdData adData = (AdData) obj;
        if (this.f3667d == null) {
            if (adData.f3667d != null) {
                return false;
            }
        } else if (!this.f3667d.equals(adData.f3667d)) {
            return false;
        }
        if (this.i == null) {
            if (adData.i != null) {
                return false;
            }
            return true;
        } else if (!this.i.equals(adData.i)) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean a(Context context, AdData adData) {
        if (adData.A > 0) {
            return true;
        }
        return false;
    }

    public boolean a() {
        return System.currentTimeMillis() - this.C <= (this.D * 60) * 1000;
    }

    public static AdData a(JSONObject jSONObject) {
        AdData adData = new AdData();
        adData.E = jSONObject.optString("channel");
        adData.f3665b = jSONObject.optLong("id");
        adData.f3666c = jSONObject.optString("name");
        adData.f3667d = jSONObject.optString(UninstallAppInfo.COLUMN_PKG);
        adData.f3668e = jSONObject.optString("desc");
        adData.f3669f = jSONObject.optString("sdesc");
        adData.f3670g = jSONObject.optInt("pos");
        adData.n = jSONObject.optInt("opentype");
        adData.o = jSONObject.optString("urlsource");
        adData.f3671h = jSONObject.optString("icon");
        adData.i = jSONObject.optString("playurl");
        adData.l = (float) jSONObject.optDouble("pts");
        adData.m = jSONObject.optInt("points");
        adData.p = jSONObject.optLong("down");
        adData.q = jSONObject.optInt("adtype");
        adData.r = (float) jSONObject.optDouble("rating");
        adData.w = jSONObject.optString("logId");
        adData.x = jSONObject.optString("license");
        adData.y = jSONObject.optInt("sid");
        adData.z = jSONObject.optString("sType", "native");
        adData.s = jSONObject.optInt("label");
        adData.A = jSONObject.optInt("ttc");
        adData.A = jSONObject.optInt("preClick");
        adData.H = jSONObject.optInt("pp", 0);
        adData.u = jSONObject.optString("cate");
        adData.v = jSONObject.optString("exg");
        adData.F = b(jSONObject.optJSONArray("impUrls"));
        adData.G = b(jSONObject.optJSONArray("cUrls"));
        adData.H = jSONObject.optInt("pp", 0);
        adData.I = jSONObject.optInt("inctRank", -1);
        return adData;
    }

    public static JSONObject a(AdData adData) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("channel", adData.E);
        jSONObject.put("id", adData.f3665b);
        jSONObject.put("name", adData.f3666c);
        jSONObject.put(UninstallAppInfo.COLUMN_PKG, adData.f3667d);
        jSONObject.put("desc", adData.f3668e);
        jSONObject.put("sdesc", adData.f3669f);
        jSONObject.put("pos", adData.f3670g);
        jSONObject.put("opentype", adData.n);
        jSONObject.put("urlsource", adData.o);
        jSONObject.put("icon", adData.f3671h);
        jSONObject.put("playurl", adData.i);
        jSONObject.put("pts", (double) adData.l);
        jSONObject.put("points", adData.m);
        jSONObject.put("down", adData.p);
        jSONObject.put("adtype", adData.q);
        jSONObject.put("rating", (double) adData.r);
        jSONObject.put("logId", adData.w);
        jSONObject.put("license", adData.x);
        jSONObject.put("sid", adData.y);
        jSONObject.put("sType", adData.z);
        jSONObject.put("label", adData.s);
        jSONObject.put("preClick", adData.A);
        jSONObject.put("pp", adData.H);
        jSONObject.put("cate", adData.u);
        jSONObject.put("exg", adData.v);
        jSONObject.put("impUrls", adData.F);
        jSONObject.put("cUrls", adData.G);
        jSONObject.put("pp", adData.H);
        jSONObject.put("ttc", adData.A);
        jSONObject.put("inctRank", adData.I);
        return jSONObject;
    }

    public static String a(JSONArray jSONArray, boolean z2) {
        if (jSONArray == null) {
            return "";
        }
        int i2 = 0;
        int length = jSONArray.length();
        while (i2 < length) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i2);
            if (optJSONObject == null) {
                i2++;
            } else if (!z2) {
                return optJSONObject.optString("url", "");
            } else {
                try {
                    return e.e(optJSONObject.optString("url", ""));
                } catch (Exception e2) {
                    return optJSONObject.optString("url", "");
                }
            }
        }
        return "";
    }

    public static a a(JSONArray jSONArray) {
        a aVar = null;
        if (jSONArray != null) {
            int i2 = 0;
            int length = jSONArray.length();
            while (true) {
                if (i2 >= length) {
                    break;
                }
                JSONObject optJSONObject = jSONArray.optJSONObject(i2);
                if (optJSONObject != null) {
                    aVar = new a();
                    aVar.f3672a = optJSONObject.optString("id", "");
                    try {
                        aVar.f3673b = e.e(optJSONObject.optString("url", ""));
                        com.duapps.ad.base.a.c("decode", "parseImages decode succ: " + aVar.f3673b);
                    } catch (Exception e2) {
                        aVar.f3673b = optJSONObject.optString("url", "");
                        com.duapps.ad.base.a.c("decode", "parseImages decode fail: " + aVar.f3673b);
                    }
                    aVar.f3674c = optJSONObject.optString("type", "");
                    aVar.f3675d = optJSONObject.optString("res", "");
                    break;
                }
                i2++;
            }
        }
        return aVar;
    }

    public static boolean b(AdData adData) {
        return adData != null && adData.f3664a > 0;
    }
}
