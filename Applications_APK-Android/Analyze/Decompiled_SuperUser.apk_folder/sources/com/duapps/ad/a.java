package com.duapps.ad;

import android.content.Context;
import android.widget.RelativeLayout;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.NativeAd;

public class a extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3501a;

    public a(Context context, DuNativeAd duNativeAd, boolean z) {
        this(context, duNativeAd.getRealSource(), z);
    }

    public a(Context context, com.duapps.ad.entity.a.a aVar, boolean z) {
        super(context);
        this.f3501a = z;
        a(aVar);
    }

    private void a(com.duapps.ad.entity.a.a aVar) {
        if (aVar == null) {
            setVisibility(8);
            return;
        }
        Object k = aVar.k();
        if (k != null) {
            switch (aVar.j()) {
                case 2:
                    addView(new AdChoicesView(getContext(), (NativeAd) k, this.f3501a));
                    return;
                default:
                    return;
            }
        }
    }
}
