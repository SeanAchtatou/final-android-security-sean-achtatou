package com.duapps.ad.inmobi;

import android.content.Context;

public class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f3769a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f3770b;

    /* renamed from: c  reason: collision with root package name */
    private IMData f3771c;

    public d(Context context, boolean z, IMData iMData) {
        this.f3769a = context.getApplicationContext();
        this.f3770b = z;
        this.f3771c = iMData;
    }

    public void run() {
        if (this.f3770b) {
            c.a(this.f3769a).b(this.f3771c);
        } else {
            c.a(this.f3769a).a(this.f3771c);
        }
    }
}
