package com.duapps.ad.inmobi;

import android.content.Context;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.duapps.ad.b;
import com.duapps.ad.base.v;
import com.duapps.ad.d;
import com.duapps.ad.stats.c;
import com.duapps.ad.stats.g;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

public class e implements com.duapps.ad.entity.a.a {

    /* renamed from: e  reason: collision with root package name */
    private static WeakHashMap<View, WeakReference<e>> f3772e = new WeakHashMap<>();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3773a;

    /* renamed from: b  reason: collision with root package name */
    private List<View> f3774b = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public View f3775c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public IMData f3776d;

    /* renamed from: f  reason: collision with root package name */
    private a f3777f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public c f3778g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public b f3779h;
    /* access modifiers changed from: private */
    public View.OnTouchListener i;
    /* access modifiers changed from: private */
    public d j;

    public e(Context context, IMData iMData, b bVar) {
        this.f3776d = iMData;
        this.f3773a = context;
        this.f3779h = bVar;
    }

    /* access modifiers changed from: private */
    public boolean a() {
        return this.f3776d != null;
    }

    private void n() {
        for (View next : this.f3774b) {
            next.setOnClickListener(null);
            next.setOnTouchListener(null);
        }
        this.f3774b.clear();
    }

    private void b(View view) {
        this.f3774b.add(view);
        view.setOnClickListener(this.f3777f);
        view.setOnTouchListener(this.f3777f);
    }

    private void a(List<View> list, View view) {
        list.add(view);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                a(list, viewGroup.getChildAt(i2));
            }
        }
    }

    class a implements View.OnClickListener, View.OnTouchListener {

        /* renamed from: b  reason: collision with root package name */
        private int f3781b;

        /* renamed from: c  reason: collision with root package name */
        private int f3782c;

        /* renamed from: d  reason: collision with root package name */
        private int f3783d;

        /* renamed from: e  reason: collision with root package name */
        private int f3784e;

        /* renamed from: f  reason: collision with root package name */
        private float f3785f;

        /* renamed from: g  reason: collision with root package name */
        private float f3786g;

        /* renamed from: h  reason: collision with root package name */
        private int f3787h;
        private int i;
        private boolean j;

        a() {
        }

        public void onClick(View view) {
            if (e.this.f3779h != null) {
                e.this.f3779h.a();
            }
            if (this.j) {
                com.duapps.ad.base.a.c("NativeAdIMWrapper", "No touch data recorded,please ensure touch events reach the ad View by returing false if you intercept the event.");
            }
            if (e.this.f3778g == null) {
                c unused = e.this.f3778g = new c(e.this.f3773a);
                e.this.f3778g.a(e.this.j);
            }
            if (e.this.a()) {
                e.this.f3778g.a(new com.duapps.ad.stats.e(e.this.f3776d));
                v.a().a(new d(e.this.f3773a, true, e.this.f3776d));
            }
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 0 && e.this.f3775c != null) {
                this.f3783d = e.this.f3775c.getWidth();
                this.f3784e = e.this.f3775c.getHeight();
                int[] iArr = new int[2];
                e.this.f3775c.getLocationInWindow(iArr);
                this.f3785f = (float) iArr[0];
                this.f3786g = (float) iArr[1];
                Rect rect = new Rect();
                e.this.f3775c.getGlobalVisibleRect(rect);
                this.i = rect.width();
                this.f3787h = rect.height();
                int[] iArr2 = new int[2];
                view.getLocationInWindow(iArr2);
                this.f3781b = (((int) motionEvent.getX()) + iArr2[0]) - iArr[0];
                this.f3782c = (iArr2[1] + ((int) motionEvent.getY())) - iArr[1];
                this.j = true;
            }
            if (e.this.i != null) {
                return e.this.i.onTouch(view, motionEvent);
            }
            return false;
        }
    }

    public void a(View view) {
        ArrayList arrayList = new ArrayList();
        a(arrayList, view);
        a(view, arrayList);
    }

    public void a(View view, List<View> list) {
        if (view == null) {
            com.duapps.ad.base.a.d("NativeAdIMWrapper", "registerViewForInteraction() -> Must provide a view");
        } else if (list == null || list.size() == 0) {
            com.duapps.ad.base.a.d("NativeAdIMWrapper", "registerViewForInteraction() -> Invalid set of clickable views");
        } else if (!a()) {
            com.duapps.ad.base.a.d("NativeAdIMWrapper", "registerViewForInteraction() -> Ad not loaded");
        } else {
            if (this.f3775c != null) {
                com.duapps.ad.base.a.b("NativeAdIMWrapper", "Native Ad was already registered with a View, Auto unregistering and proceeding");
                b();
            }
            if (f3772e.containsKey(view) && f3772e.get(view).get() != null) {
                ((e) f3772e.get(view).get()).b();
            }
            this.f3777f = new a();
            this.f3775c = view;
            for (View b2 : list) {
                b(b2);
            }
            f3772e.put(view, new WeakReference(this));
            g.i(this.f3773a, new com.duapps.ad.stats.e(this.f3776d));
            v.a().a(new d(this.f3773a, false, this.f3776d));
        }
    }

    public void b() {
        if (this.f3775c != null) {
            if (!f3772e.containsKey(this.f3775c) || f3772e.get(this.f3775c).get() != this) {
                com.duapps.ad.base.a.b("NativeAdIMWrapper", "unregisterView() -> View not regitered with this NativeAd");
                return;
            }
            f3772e.remove(this.f3775c);
            n();
        }
    }

    public void c() {
        this.j = null;
        n();
        if (this.f3775c != null) {
            f3772e.remove(this.f3775c);
            this.f3775c = null;
        }
    }

    public String d() {
        if (!a()) {
            return null;
        }
        return this.f3776d.q;
    }

    public String e() {
        if (!a()) {
            return null;
        }
        return this.f3776d.p;
    }

    public String f() {
        if (!a()) {
            return null;
        }
        return this.f3776d.s;
    }

    public String g() {
        if (!a()) {
            return null;
        }
        return this.f3776d.n;
    }

    public String h() {
        if (!a()) {
            return null;
        }
        return this.f3776d.m;
    }

    public float i() {
        if (!a()) {
            return 0.0f;
        }
        return this.f3776d.o;
    }

    public String l() {
        return "im";
    }

    public void a(d dVar) {
        this.j = dVar;
    }

    public Object k() {
        return this.f3776d;
    }

    public int j() {
        return 3;
    }

    public int m() {
        return -1;
    }
}
