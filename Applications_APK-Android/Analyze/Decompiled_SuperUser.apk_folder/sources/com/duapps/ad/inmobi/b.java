package com.duapps.ad.inmobi;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.duapps.ad.inmobi.IMData;
import com.duapps.ad.internal.b.e;
import com.kingouser.com.util.ShellUtils;

public class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public IMData f3750a;

    /* renamed from: b  reason: collision with root package name */
    public IMData.a f3751b;

    /* renamed from: c  reason: collision with root package name */
    public f f3752c;

    /* renamed from: d  reason: collision with root package name */
    private Context f3753d;

    /* renamed from: e  reason: collision with root package name */
    private a f3754e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public volatile boolean f3755f = false;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public String f3756g;

    /* renamed from: h  reason: collision with root package name */
    private Handler f3757h = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            if (message.what == 100) {
                b.this.c();
                com.duapps.ad.base.a.c("InMobiDataExecutor", "loading js:" + b.this.f3756g);
                if (b.this.f3752c != null) {
                    b.this.f3752c.f3788a.loadData(b.this.f3756g, "text/html", "UTF-8");
                }
            }
        }
    };

    public interface a {
        void a(b bVar);
    }

    public b(Context context, f fVar, IMData iMData, IMData.a aVar, a aVar2) {
        this.f3753d = context;
        this.f3752c = fVar;
        this.f3750a = iMData;
        this.f3751b = aVar;
        if (fVar != null) {
            fVar.f3790c = false;
        }
        this.f3754e = aVar2;
    }

    private void a(String str) {
        if (!e.a(this.f3753d)) {
            this.f3755f = false;
            if (this.f3752c != null) {
                this.f3752c.f3790c = false;
                return;
            }
            return;
        }
        this.f3755f = true;
        this.f3756g = str;
        this.f3757h.sendEmptyMessage(100);
    }

    private void a() {
        a(this.f3750a.a(this.f3751b));
    }

    private void b() {
        a(this.f3750a.b());
    }

    public void run() {
        boolean z = true;
        com.duapps.ad.base.a.c("InMobiDataExecutor", " started");
        if (!this.f3755f) {
            this.f3755f = true;
            if (this.f3751b != IMData.a.Impression || this.f3750a.w) {
                if (this.f3751b == IMData.a.Click && !this.f3750a.x) {
                    if (!this.f3750a.w) {
                        b();
                    }
                }
                z = false;
            }
            if (z) {
                a();
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.f3752c == null) {
            this.f3755f = false;
        } else {
            this.f3752c.f3788a.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView webView, String str) {
                    int i = 0;
                    boolean unused = b.this.f3755f = false;
                    if (b.this.f3752c != null) {
                        b.this.f3752c.f3790c = false;
                    }
                    b.this.d();
                    com.duapps.ad.base.a.c("InMobiDataExecutor", "page finished:" + b.this.f3750a.u);
                    if (b.this.f3751b != IMData.a.Impression) {
                        i = 1;
                    }
                    com.duapps.ad.base.a.c("InMobiDataExecutor", "AdOperationType==" + i);
                    com.duapps.ad.base.a.c("InMobiDataExecutor", " completed");
                }

                public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                    com.duapps.ad.base.a.c("InMobiDataExecutor", "page started:");
                }

                public void onLoadResource(WebView webView, String str) {
                    com.duapps.ad.base.a.c("InMobiDataExecutor", "resource load:");
                }

                public void onReceivedError(WebView webView, int i, String str, String str2) {
                    com.duapps.ad.base.a.c("InMobiDataExecutor", "received error:" + i + "\tdesc:" + str + ShellUtils.COMMAND_LINE_END + str2);
                    boolean unused = b.this.f3755f = false;
                    if (b.this.f3752c != null) {
                        b.this.f3752c.f3790c = false;
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.f3754e != null) {
            this.f3754e.a(this);
        }
    }
}
