package com.duapps.ad.inmobi;

import android.content.Context;
import android.text.TextUtils;
import com.duapps.ad.AdError;
import com.duapps.ad.base.o;
import com.duapps.ad.base.t;
import com.duapps.ad.base.u;
import com.duapps.ad.entity.a.b;
import com.duapps.ad.internal.b.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class a extends b<com.duapps.ad.entity.a.a> {
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final String f3747b = a.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    u<IMDataModel> f3748a = new u<IMDataModel>() {
        public void a() {
            com.duapps.ad.base.a.a(a.f3747b, "start load cache data--");
            a.this.f3686d = true;
            a.this.f3687e = true;
        }

        public void a(int i, IMDataModel iMDataModel) {
            a.this.f3686d = false;
            if (i == 200 && iMDataModel != null) {
                com.duapps.ad.base.a.c(a.f3747b, i + "");
                List a2 = a.this.a(iMDataModel.f3746f);
                if (a2.size() <= 0) {
                    com.duapps.ad.stats.b.b(a.this.f3690h, a.this.i);
                    return;
                }
                synchronized (a.this.o) {
                    a.this.o.addAll(a2);
                    com.duapps.ad.base.a.a(a.f3747b, "store data into cache list -- list.size = " + a.this.o.size());
                }
            }
        }

        public void a(int i, String str) {
            com.duapps.ad.base.a.a(a.f3747b, "fail to get cache -" + str);
            a.this.f3685c = true;
            a.this.f3686d = false;
            if (!a.this.k && a.this.m != null) {
                a.this.m.a(new AdError(i, str));
            }
        }
    };
    private int n;
    /* access modifiers changed from: private */
    public final List<IMData> o = Collections.synchronizedList(new LinkedList());

    public a(Context context, int i, long j, int i2) {
        super(context, i, j);
        this.n = i2;
    }

    public void a(boolean z) {
        super.a(z);
        if (e.a(this.f3690h)) {
            if (this.n == 0) {
                com.duapps.ad.base.a.c(f3747b, "cacheSize is zero");
                return;
            }
            String b2 = o.a(this.f3690h).b();
            com.duapps.ad.base.a.c(f3747b, "ImCache inId = " + b2);
            int c2 = this.n - c();
            if (c2 > 0 && !this.f3686d) {
                this.f3686d = true;
                this.f3687e = true;
                a(b2, c2);
            }
        }
    }

    public int b() {
        return this.n;
    }

    /* renamed from: a */
    public com.duapps.ad.entity.a.a d() {
        IMData iMData;
        synchronized (this.o) {
            IMData iMData2 = null;
            while (this.o.size() > 0 && ((iMData2 = this.o.remove(0)) == null || !iMData2.a())) {
            }
            iMData = iMData2;
        }
        com.duapps.ad.stats.b.e(this.f3690h, iMData == null ? "FAIL" : "OK", this.i);
        if (iMData == null) {
            return null;
        }
        return new e(this.f3690h, iMData, this.m);
    }

    public int c() {
        int i;
        int i2 = 0;
        synchronized (this.o) {
            Iterator<IMData> it = this.o.iterator();
            while (it.hasNext()) {
                IMData next = it.next();
                if (next == null) {
                    it.remove();
                } else {
                    if (!next.a() || !a(this.f3690h, next.l)) {
                        it.remove();
                        i = i2;
                    } else {
                        i = i2 + 1;
                    }
                    i2 = i;
                }
            }
        }
        return i2;
    }

    public void e() {
        synchronized (this.o) {
            this.o.clear();
        }
    }

    private void a(String str, int i) {
        t.a(this.f3690h).a(Integer.valueOf(this.i).intValue(), str, String.valueOf(i), this.f3748a);
    }

    /* access modifiers changed from: private */
    public List<IMData> a(List<IMData> list) {
        ArrayList arrayList = new ArrayList();
        for (IMData next : list) {
            if (a(this.f3690h, next.l)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private boolean a(Context context, String str) {
        if (!TextUtils.isEmpty(str) && e.a(context, str)) {
            return false;
        }
        return true;
    }
}
