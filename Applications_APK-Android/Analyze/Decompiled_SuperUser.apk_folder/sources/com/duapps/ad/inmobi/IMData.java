package com.duapps.ad.inmobi;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.duapps.ad.AdError;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

public class IMData implements Parcelable {
    public static final Parcelable.Creator<IMData> CREATOR = new Parcelable.Creator<IMData>() {
        /* renamed from: a */
        public IMData createFromParcel(Parcel parcel) {
            return new IMData(parcel);
        }

        /* renamed from: a */
        public IMData[] newArray(int i) {
            return new IMData[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public String f3728a;

    /* renamed from: b  reason: collision with root package name */
    public int f3729b;

    /* renamed from: c  reason: collision with root package name */
    public String f3730c;

    /* renamed from: d  reason: collision with root package name */
    public String f3731d;

    /* renamed from: e  reason: collision with root package name */
    public long f3732e;

    /* renamed from: f  reason: collision with root package name */
    public int f3733f;

    /* renamed from: g  reason: collision with root package name */
    public int f3734g;

    /* renamed from: h  reason: collision with root package name */
    public int f3735h;
    public int i;
    public int j;
    public String k;
    public String l;
    public String m;
    public String n;
    public float o;
    public String p;
    public String q;
    public String r;
    public String s;
    public String t;
    public String u;
    public long v;
    public boolean w;
    public boolean x;
    public String y;

    public IMData() {
        this.w = false;
        this.x = false;
        this.y = "inmobi";
    }

    public IMData(String str, int i2, String str2, String str3, int i3, JSONObject jSONObject, long j2) {
        this.w = false;
        this.x = false;
        this.y = "inmobi";
        this.f3728a = str;
        this.f3729b = i2;
        this.f3731d = str2;
        this.f3730c = str3;
        this.f3732e = jSONObject.optLong("id");
        this.k = jSONObject.optString(FirebaseAnalytics.Param.SOURCE);
        this.f3734g = jSONObject.optInt("openType", -1);
        this.f3733f = jSONObject.optInt("mType");
        this.i = jSONObject.optInt("label");
        this.f3735h = jSONObject.optInt("preClick");
        this.j = jSONObject.optInt("cacheTime");
        this.v = j2;
        JSONObject optJSONObject = jSONObject.optJSONArray("ads").optJSONObject(i3);
        if (optJSONObject != null) {
            String optString = optJSONObject.optString("pubContent");
            this.t = optJSONObject.optString("contextCode");
            this.u = optJSONObject.optString("namespace");
            String a2 = a(optString);
            if (a2 != null) {
                try {
                    JSONObject jSONObject2 = new JSONObject(a2);
                    com.duapps.ad.base.a.c("IMData", "imAd==" + jSONObject2.toString());
                    this.m = jSONObject2.optString("title");
                    this.n = jSONObject2.optString("description");
                    this.o = (float) jSONObject2.optDouble("rating", 4.5d);
                    this.s = jSONObject2.optString("cta");
                    this.p = jSONObject2.optJSONObject("icon").optString("url");
                    this.q = jSONObject2.optJSONObject("screenshots").optString("url");
                    this.r = jSONObject2.optString("landingURL");
                    this.l = jSONObject2.optString("package_name");
                } catch (JSONException e2) {
                    com.duapps.ad.base.a.d("IMData", "JSONException:" + e2.toString());
                }
            }
        }
    }

    public IMData(String str, String str2, long j2) {
        this.w = false;
        this.x = false;
        this.y = "inmobi";
        this.u = str;
        this.t = str2;
        this.v = j2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        byte b2;
        byte b3 = 1;
        parcel.writeString(this.f3728a);
        parcel.writeString(this.f3730c);
        parcel.writeInt(this.f3729b);
        parcel.writeString(this.f3731d);
        parcel.writeLong(this.f3732e);
        parcel.writeString(this.k);
        parcel.writeInt(this.f3734g);
        parcel.writeInt(this.f3733f);
        parcel.writeInt(this.i);
        parcel.writeInt(this.f3735h);
        parcel.writeInt(this.j);
        parcel.writeString(this.m);
        parcel.writeString(this.n);
        parcel.writeString(this.s);
        parcel.writeString(this.p);
        parcel.writeString(this.q);
        parcel.writeString(this.r);
        parcel.writeString(this.t);
        parcel.writeString(this.u);
        parcel.writeFloat(this.o);
        parcel.writeLong(this.v);
        if (this.w) {
            b2 = 1;
        } else {
            b2 = 0;
        }
        parcel.writeByte(b2);
        if (!this.x) {
            b3 = 0;
        }
        parcel.writeByte(b3);
        parcel.writeString(this.l);
    }

    private IMData(Parcel parcel) {
        boolean z;
        boolean z2 = true;
        this.w = false;
        this.x = false;
        this.y = "inmobi";
        this.f3728a = parcel.readString();
        this.f3730c = parcel.readString();
        this.f3729b = parcel.readInt();
        this.f3731d = parcel.readString();
        this.f3732e = parcel.readLong();
        this.k = parcel.readString();
        this.f3734g = parcel.readInt();
        this.f3733f = parcel.readInt();
        this.i = parcel.readInt();
        this.f3735h = parcel.readInt();
        this.j = parcel.readInt();
        this.m = parcel.readString();
        this.n = parcel.readString();
        this.s = parcel.readString();
        this.p = parcel.readString();
        this.q = parcel.readString();
        this.r = parcel.readString();
        this.t = parcel.readString();
        this.u = parcel.readString();
        this.o = parcel.readFloat();
        this.v = parcel.readLong();
        if (parcel.readByte() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.w = z;
        this.x = parcel.readByte() == 0 ? false : z2;
        this.l = parcel.readString();
    }

    public boolean a() {
        return System.currentTimeMillis() - this.v <= ((long) ((this.j * 60) * AdError.NETWORK_ERROR_CODE));
    }

    public static String a(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            byte[] bytes = str.getBytes("UTF-8");
            return new String(Base64.decode(bytes, 0, bytes.length, 0), "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public enum a {
        None(0),
        Impression(18),
        Click(8);
        

        /* renamed from: d  reason: collision with root package name */
        private int f3740d;

        private a(int i) {
            this.f3740d = i;
        }

        public int a() {
            return this.f3740d;
        }
    }

    public String a(a aVar) {
        StringBuilder sb = new StringBuilder();
        int a2 = aVar.a();
        sb.append(this.t);
        sb.append("<script>");
        sb.append(this.u);
        sb.append("recordEvent(" + a2 + ")");
        sb.append("</script>");
        return sb.toString();
    }

    public String b() {
        return a(a.Impression) + "<script>recordEvent(8)</script>";
    }

    public static IMData a(JSONObject jSONObject) {
        IMData iMData = new IMData();
        iMData.y = jSONObject.optString("channel");
        iMData.f3728a = jSONObject.optString("license");
        iMData.f3730c = jSONObject.optString("logId");
        iMData.f3729b = jSONObject.optInt("sid");
        iMData.f3731d = jSONObject.optString("sType", "native");
        iMData.f3732e = jSONObject.optLong("id");
        iMData.k = jSONObject.optString(FirebaseAnalytics.Param.SOURCE);
        iMData.i = jSONObject.optInt("label");
        iMData.f3735h = jSONObject.optInt("preClick");
        iMData.f3734g = jSONObject.optInt("opentype");
        iMData.j = jSONObject.optInt("cacheTime");
        iMData.f3733f = jSONObject.optInt("mType");
        iMData.m = jSONObject.optString("title");
        iMData.n = jSONObject.optString("description");
        iMData.s = jSONObject.optString("cta");
        iMData.p = jSONObject.optString("icon");
        iMData.q = jSONObject.optString("screenshots");
        iMData.r = jSONObject.optString("landingURL");
        iMData.o = (float) jSONObject.optLong("rating");
        iMData.l = jSONObject.optString("package_name");
        return iMData;
    }

    public static JSONObject a(IMData iMData) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("channel", iMData.y);
        jSONObject.put("license", iMData.f3728a);
        jSONObject.put("logId", iMData.f3730c);
        jSONObject.put("sid", iMData.f3729b);
        jSONObject.put("sType", iMData.f3731d);
        jSONObject.put("id", iMData.f3732e);
        jSONObject.put(FirebaseAnalytics.Param.SOURCE, iMData.k);
        jSONObject.put("label", iMData.i);
        jSONObject.put("preClick", iMData.f3735h);
        jSONObject.put("opentype", iMData.f3734g);
        jSONObject.put("cacheTime", iMData.j);
        jSONObject.put("mType", iMData.f3733f);
        jSONObject.put("title", iMData.m);
        jSONObject.put("description", iMData.n);
        jSONObject.put("cta", iMData.s);
        jSONObject.put("icon", iMData.p);
        jSONObject.put("screenshots", iMData.q);
        jSONObject.put("landingURL", iMData.r);
        jSONObject.put("rating", (double) iMData.o);
        jSONObject.put("package_name", iMData.l);
        return jSONObject;
    }
}
