package com.duapps.ad.inmobi;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.duapps.ad.base.a;
import com.duapps.ad.inmobi.IMData;
import com.duapps.ad.inmobi.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

public class c implements Handler.Callback, b.a {

    /* renamed from: f  reason: collision with root package name */
    private static c f3760f;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<f> f3761a;

    /* renamed from: b  reason: collision with root package name */
    private ArrayList<IMData> f3762b;

    /* renamed from: c  reason: collision with root package name */
    private ArrayList<b> f3763c;

    /* renamed from: d  reason: collision with root package name */
    private ExecutorService f3764d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public Context f3765e;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public volatile boolean f3766g = false;

    /* renamed from: h  reason: collision with root package name */
    private Handler f3767h;
    private Handler i = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            if (message.what == 100) {
                for (int i = 1; i <= 3; i++) {
                    f fVar = new f(c.this.f3765e);
                    fVar.f3789b = i - 1;
                    c.this.f3761a.add(fVar);
                }
                if (c.this.f3766g) {
                    c.this.a((f) c.this.f3761a.get(0));
                }
            }
        }
    };

    public static c a(Context context) {
        synchronized (c.class) {
            if (f3760f == null) {
                f3760f = new c(context.getApplicationContext());
            }
        }
        return f3760f;
    }

    private c(Context context) {
        this.f3765e = context;
        this.f3761a = new ArrayList<>();
        this.i.sendEmptyMessage(100);
        a();
        this.f3764d = Executors.newFixedThreadPool(3);
        this.f3763c = new ArrayList<>();
        HandlerThread handlerThread = new HandlerThread("inmobiNative", 10);
        handlerThread.start();
        this.f3767h = new Handler(handlerThread.getLooper(), this);
        this.f3767h.sendEmptyMessageDelayed(1, 10800000);
        c();
    }

    private void a() {
        ArrayList<IMData> a2;
        String string = this.f3765e.getSharedPreferences("im_cache_prefs", 0).getString("im_cache_prefs_array", null);
        this.f3762b = new ArrayList<>();
        if (string != null && (a2 = a(string)) != null) {
            a.c("InMobiDataQueue", "arrayList size:" + a2.size());
            Iterator<IMData> it = a2.iterator();
            while (it.hasNext()) {
                this.f3762b.add(it.next());
            }
        }
    }

    private void b() {
        SharedPreferences.Editor edit = this.f3765e.getSharedPreferences("im_cache_prefs", 0).edit();
        String a2 = a(this.f3762b);
        if (!TextUtils.isEmpty(a2)) {
            edit.putString("im_cache_prefs_array", a2);
            edit.apply();
        }
    }

    private void c() {
        long currentTimeMillis = System.currentTimeMillis();
        ArrayList<IMData> arrayList = new ArrayList<>();
        Iterator<IMData> it = this.f3762b.iterator();
        while (it.hasNext()) {
            IMData next = it.next();
            if (currentTimeMillis - next.v < 10800000) {
                arrayList.add(next);
            }
        }
        this.f3762b.clear();
        this.f3762b = null;
        this.f3762b = arrayList;
        b();
    }

    private boolean a(IMData iMData, IMData.a aVar) {
        boolean z = false;
        if (aVar == IMData.a.Impression && iMData.w) {
            return true;
        }
        if (aVar == IMData.a.Click && iMData.x) {
            return true;
        }
        Iterator<b> it = this.f3763c.iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            b next = it.next();
            if (!next.f3750a.u.equals(iMData.u) || next.f3751b != aVar) {
                z = z2;
            } else {
                z = true;
            }
        }
    }

    private void a(b bVar, boolean z) {
        int i2;
        IMData iMData;
        if (z) {
            int i3 = 0;
            Iterator<IMData> it = this.f3762b.iterator();
            while (true) {
                i2 = i3;
                if (!it.hasNext()) {
                    iMData = null;
                    break;
                }
                iMData = it.next();
                if (bVar.f3750a.u.equals(iMData.u)) {
                    break;
                }
                i3 = i2 + 1;
            }
            if (iMData != null) {
                if (bVar.f3751b == IMData.a.Impression) {
                    iMData.w = true;
                } else if (bVar.f3751b == IMData.a.Click) {
                    iMData.x = true;
                }
                this.f3762b.set(i2, iMData);
                b();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0026 A[EDGE_INSN: B:18:0x0026->B:8:0x0026 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0015  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(com.duapps.ad.inmobi.b r7, boolean r8) {
        /*
            r6 = this;
            java.util.ArrayList<com.duapps.ad.inmobi.b> r0 = r6.f3763c
            r0.remove(r7)
            r6.a(r7, r8)
            r1 = 0
            java.util.ArrayList<com.duapps.ad.inmobi.b> r0 = r6.f3763c
            java.util.Iterator r2 = r0.iterator()
        L_0x000f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0026
            java.lang.Object r0 = r2.next()
            com.duapps.ad.inmobi.b r0 = (com.duapps.ad.inmobi.b) r0
            com.duapps.ad.inmobi.f r3 = r0.f3752c
            if (r3 == 0) goto L_0x0025
            com.duapps.ad.inmobi.f r3 = r0.f3752c
            boolean r3 = r3.f3790c
            if (r3 != 0) goto L_0x000f
        L_0x0025:
            r1 = r0
        L_0x0026:
            if (r1 == 0) goto L_0x0042
            if (r1 == r7) goto L_0x0042
            long r2 = java.lang.System.currentTimeMillis()
            com.duapps.ad.inmobi.IMData r0 = r1.f3750a
            long r4 = r0.v
            long r2 = r2 - r4
            r4 = 10800000(0xa4cb80, double:5.335909E-317)
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x0043
            r0 = 1
        L_0x003b:
            if (r0 != 0) goto L_0x0045
            java.util.ArrayList<com.duapps.ad.inmobi.b> r0 = r6.f3763c
            r0.remove(r1)
        L_0x0042:
            return
        L_0x0043:
            r0 = 0
            goto L_0x003b
        L_0x0045:
            java.util.ArrayList<com.duapps.ad.inmobi.f> r0 = r6.f3761a
            com.duapps.ad.inmobi.f r2 = r7.f3752c
            int r2 = r2.f3789b
            java.lang.Object r0 = r0.get(r2)
            com.duapps.ad.inmobi.f r0 = (com.duapps.ad.inmobi.f) r0
            r1.f3752c = r0
            java.util.concurrent.ExecutorService r0 = r6.f3764d
            r0.execute(r1)
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duapps.ad.inmobi.c.b(com.duapps.ad.inmobi.b, boolean):void");
    }

    private synchronized void b(IMData iMData, IMData.a aVar) {
        boolean z;
        if (a(iMData, aVar)) {
            a.c("InMobiDataQueue", "returning:" + iMData.u);
        } else {
            f fVar = null;
            Iterator<f> it = this.f3761a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                f next = it.next();
                if (!next.f3790c) {
                    fVar = next;
                    break;
                }
            }
            b bVar = new b(this.f3765e, fVar, iMData, aVar, this);
            this.f3763c.add(bVar);
            if (fVar != null) {
                this.f3764d.execute(bVar);
                z = false;
            } else {
                z = true;
            }
            if (z && this.f3763c.size() == 1) {
                this.f3766g = true;
            }
        }
    }

    private synchronized void c(IMData iMData, IMData.a aVar) {
        IMData iMData2;
        if (iMData != null) {
            if (!TextUtils.isEmpty(iMData.u) && !TextUtils.isEmpty(iMData.t)) {
                Iterator<IMData> it = this.f3762b.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        iMData2 = null;
                        break;
                    }
                    iMData2 = it.next();
                    if (iMData2.u.equals(iMData.u)) {
                        break;
                    }
                }
                if (iMData2 == null) {
                    iMData2 = new IMData(iMData.u, iMData.t, iMData.v);
                    this.f3762b.add(iMData2);
                }
                b(iMData2, aVar);
            }
        }
    }

    public void a(IMData iMData) {
        if (iMData == null || TextUtils.isEmpty(iMData.u) || TextUtils.isEmpty(iMData.t)) {
            a.c("InMobiDataQueue", "IMData is null or namespace is null or contextCode is null !");
        } else {
            c(iMData, IMData.a.Impression);
        }
    }

    public void b(IMData iMData) {
        if (iMData == null || TextUtils.isEmpty(iMData.u) || TextUtils.isEmpty(iMData.t)) {
            a.c("InMobiDataQueue", "IMData is null or namespace is null or contextCode is null !");
        } else {
            c(iMData, IMData.a.Click);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.inmobi.c.b(com.duapps.ad.inmobi.b, boolean):void
     arg types: [com.duapps.ad.inmobi.b, int]
     candidates:
      com.duapps.ad.inmobi.c.b(com.duapps.ad.inmobi.IMData, com.duapps.ad.inmobi.IMData$a):void
      com.duapps.ad.inmobi.c.b(com.duapps.ad.inmobi.b, boolean):void */
    public void a(b bVar) {
        b(bVar, true);
    }

    public static String a(List<IMData> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        JSONStringer jSONStringer = new JSONStringer();
        try {
            jSONStringer.array();
            for (int i2 = 0; i2 < list.size(); i2++) {
                IMData iMData = list.get(i2);
                jSONStringer.object();
                jSONStringer.key("namespace").value(iMData.u);
                jSONStringer.key("contextCode").value(iMData.u);
                jSONStringer.key("ts").value(iMData.v);
                jSONStringer.endObject();
            }
            jSONStringer.endArray();
            return jSONStringer.toString();
        } catch (JSONException e2) {
            a.c("InMobiDataQueue", "toJson exectpion=" + e2.toString());
            return null;
        }
    }

    public static ArrayList<IMData> a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        ArrayList<IMData> arrayList = new ArrayList<>();
        try {
            JSONArray jSONArray = new JSONArray(str);
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i2);
                arrayList.add(new IMData(jSONObject.getString("namespace"), jSONObject.getString("contextCode"), jSONObject.getLong("ts")));
            }
            return arrayList;
        } catch (JSONException e2) {
            a.c("InMobiDataQueue", "fromJson exectpion=" + e2.toString());
            return null;
        }
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f3767h.removeMessages(1);
                c();
                this.f3767h.sendEmptyMessageDelayed(1, 10800000);
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: private */
    public void a(f fVar) {
        if (this.f3764d != null && this.f3763c.size() > 0) {
            b bVar = this.f3763c.get(0);
            fVar.f3790c = true;
            bVar.f3752c = fVar;
            this.f3764d.execute(bVar);
        }
    }
}
