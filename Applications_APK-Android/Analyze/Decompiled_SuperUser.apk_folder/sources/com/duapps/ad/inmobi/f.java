package com.duapps.ad.inmobi;

import android.content.Context;
import android.os.Build;
import android.webkit.WebView;

public class f {

    /* renamed from: a  reason: collision with root package name */
    public WebView f3788a;

    /* renamed from: b  reason: collision with root package name */
    public int f3789b;

    /* renamed from: c  reason: collision with root package name */
    public boolean f3790c = false;

    public f(Context context) {
        this.f3788a = new WebView(context);
        if (this.f3788a.getSettings() != null) {
            this.f3788a.getSettings().setJavaScriptEnabled(true);
            this.f3788a.getSettings().setCacheMode(2);
            this.f3788a.getSettings().setLoadsImagesAutomatically(true);
            this.f3788a.getSettings().setBlockNetworkImage(false);
            this.f3788a.getSettings().setAllowContentAccess(false);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            this.f3788a.removeJavascriptInterface("searchBoxJavaBridge_");
            this.f3788a.removeJavascriptInterface("accessibility");
            this.f3788a.removeJavascriptInterface("accessibilityTraversal");
        }
        this.f3788a.setVisibility(0);
    }
}
