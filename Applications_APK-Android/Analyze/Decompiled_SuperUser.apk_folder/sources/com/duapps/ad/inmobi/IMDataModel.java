package com.duapps.ad.inmobi;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class IMDataModel implements Parcelable {
    public static final Parcelable.Creator<IMDataModel> CREATOR = new Parcelable.Creator<IMDataModel>() {
        /* renamed from: a */
        public IMDataModel createFromParcel(Parcel parcel) {
            return new IMDataModel(parcel);
        }

        /* renamed from: a */
        public IMDataModel[] newArray(int i) {
            return new IMDataModel[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public String f3741a;

    /* renamed from: b  reason: collision with root package name */
    public int f3742b;

    /* renamed from: c  reason: collision with root package name */
    public String f3743c;

    /* renamed from: d  reason: collision with root package name */
    public String f3744d;

    /* renamed from: e  reason: collision with root package name */
    public long f3745e;

    /* renamed from: f  reason: collision with root package name */
    public final List<IMData> f3746f;

    public IMDataModel(String str, int i, String str2, JSONObject jSONObject, long j) {
        JSONObject optJSONObject;
        this.f3746f = new ArrayList();
        this.f3741a = str;
        this.f3742b = i;
        this.f3744d = str2;
        this.f3745e = j;
        if (jSONObject != null && i == jSONObject.optInt("sId")) {
            this.f3743c = jSONObject.optString("logId");
            JSONArray optJSONArray = jSONObject.optJSONArray("list");
            if (optJSONArray.length() != 0 && (optJSONObject = optJSONArray.optJSONObject(0)) != null) {
                JSONArray optJSONArray2 = optJSONObject.optJSONArray("ads");
                for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                    this.f3746f.add(new IMData(str, i, str2, this.f3743c, i2, optJSONObject, j));
                }
            }
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f3741a);
        parcel.writeString(this.f3743c);
        parcel.writeInt(this.f3742b);
        parcel.writeString(this.f3744d);
        parcel.writeLong(this.f3745e);
        parcel.writeTypedList(this.f3746f);
    }

    private IMDataModel(Parcel parcel) {
        this.f3746f = new ArrayList();
        this.f3741a = parcel.readString();
        this.f3743c = parcel.readString();
        this.f3742b = parcel.readInt();
        this.f3744d = parcel.readString();
        this.f3745e = parcel.readLong();
        parcel.readTypedList(this.f3746f, IMData.CREATOR);
    }
}
