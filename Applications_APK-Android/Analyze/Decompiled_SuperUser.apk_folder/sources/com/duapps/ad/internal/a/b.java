package com.duapps.ad.internal.a;

import android.content.Context;
import android.util.SparseArray;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f3817a;

    /* renamed from: b  reason: collision with root package name */
    private Context f3818b;

    /* renamed from: c  reason: collision with root package name */
    private SparseArray<c> f3819c = new SparseArray<>();

    private b(Context context) {
        this.f3818b = context;
    }

    public static b a(Context context) {
        if (f3817a == null) {
            synchronized (b.class) {
                if (f3817a == null) {
                    f3817a = new b(context);
                }
            }
        }
        return f3817a;
    }

    public c a(int i, boolean z) {
        c a2;
        synchronized (this.f3819c) {
            if (this.f3819c.indexOfKey(i) >= 0) {
                a2 = this.f3819c.get(i);
            } else {
                a2 = c.a(this.f3818b, i, z);
                synchronized (this.f3819c) {
                    this.f3819c.put(i, a2);
                }
            }
        }
        return a2;
    }
}
