package com.duapps.ad.internal.a;

import android.content.Context;
import com.duapps.ad.base.a;
import com.duapps.ad.base.l;
import com.duapps.ad.base.o;
import com.duapps.ad.base.t;
import com.duapps.ad.base.u;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.entity.AdModel;
import com.duapps.ad.stats.c;
import java.util.Iterator;
import java.util.List;

public class e {

    /* renamed from: b  reason: collision with root package name */
    private static e f3829b = null;

    /* renamed from: a  reason: collision with root package name */
    u<AdModel> f3830a = new u<AdModel>() {
        public void a() {
        }

        public void a(int i, AdModel adModel) {
            List<AdData> list;
            int size;
            if (adModel != null && (list = adModel.f3683h) != null && (size = list.size()) > 0) {
                for (int i2 = 0; i2 < size; i2++) {
                    AdData adData = list.get(i2);
                    String str = adData.f3667d;
                    if (str != null && str.equals(e.this.f3832d) && adData.H == 1) {
                        com.duapps.ad.stats.e eVar = new com.duapps.ad.stats.e(adData);
                        eVar.a(true);
                        e.this.f3833e.e(eVar, adData.i);
                    }
                }
            }
        }

        public void a(int i, String str) {
        }
    };

    /* renamed from: c  reason: collision with root package name */
    private Context f3831c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public String f3832d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public c f3833e;

    private e(Context context) {
        this.f3831c = context;
        this.f3833e = new c(context);
    }

    public static e a(Context context) {
        if (f3829b == null) {
            synchronized (e.class) {
                if (f3829b == null) {
                    f3829b = new e(context.getApplicationContext());
                }
            }
        }
        return f3829b;
    }

    public void a(String str) {
        this.f3832d = str;
        a.a("TimerPuller", "Pull TriggerPreParseAd ... ");
        long o = l.o(this.f3831c);
        if (o != 0) {
            long a2 = a(l.p(this.f3831c), o);
            if (a2 == -1) {
                l.q(this.f3831c);
            } else if (a2 == 0) {
                a();
            }
        }
    }

    private long a(long j, long j2) {
        long currentTimeMillis = System.currentTimeMillis() - j;
        if (currentTimeMillis < 0) {
            return -1;
        }
        if (currentTimeMillis <= j2) {
            return j2 - currentTimeMillis;
        }
        return 0;
    }

    private void a() {
        if (com.duapps.ad.internal.b.e.a(this.f3831c)) {
            a.a("TimerPuller", "PullTcppNativeWall ... ");
            l.q(this.f3831c);
            Iterator<Integer> it = o.a(this.f3831c).a().iterator();
            if (it.hasNext()) {
                t.a(this.f3831c).a(it.next().intValue(), 1, this.f3830a, this.f3832d);
            }
        }
    }

    public void a(boolean z, String str) {
        this.f3832d = str;
        int i = 0;
        if (z) {
            Iterator<Integer> it = o.a(this.f3831c).a().iterator();
            if (it.hasNext()) {
                i = it.next().intValue();
            }
        } else {
            i = l.E(this.f3831c);
        }
        if (i == 0) {
            i = -19999;
        }
        t.a(this.f3831c).a(i, 1, this.f3830a, this.f3832d);
    }
}
