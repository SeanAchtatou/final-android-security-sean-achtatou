package com.duapps.ad.internal.a;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Base64;
import com.duapps.ad.AdError;
import com.duapps.ad.base.b;
import com.duapps.ad.base.g;
import com.duapps.ad.base.l;
import com.duapps.ad.base.o;
import com.duapps.ad.base.s;
import com.duapps.ad.base.v;
import com.duapps.ad.internal.b.e;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.lody.virtual.server.pm.installer.PackageHelper;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class d extends Handler {
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final String f3823b = d.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static String f3824c = "http://api.mobula.sdk.duapps.com/adunion/slot/getSrcPrio?";

    /* renamed from: a  reason: collision with root package name */
    private Context f3825a;

    public d(Context context) {
        this.f3825a = context;
        d();
    }

    public void handleMessage(Message message) {
        int i = message.what;
        if (5 == i) {
            removeMessages(5);
            a aVar = new a(this.f3825a);
            if (Looper.getMainLooper() == Looper.myLooper()) {
                v.a().a(aVar);
            } else {
                post(aVar);
            }
            sendEmptyMessageDelayed(5, 21600000);
        } else if (7 == i) {
            removeMessages(7);
            long o = l.o(this.f3825a);
            if (o > 0) {
                e.a(this.f3825a).a((String) null);
                sendEmptyMessageDelayed(7, o);
            }
        }
        super.handleMessage(message);
    }

    private void d() {
        if (l.u(this.f3825a) == 0) {
            l.c(this.f3825a, SystemClock.elapsedRealtime());
        }
    }

    public void a() {
        long j = 0;
        if (e.a(this.f3825a)) {
            long currentTimeMillis = System.currentTimeMillis() - l.d(this.f3825a);
            if (currentTimeMillis < 0) {
                l.c(this.f3825a);
                return;
            }
            if (currentTimeMillis <= 21600000) {
                j = 21600000 - currentTimeMillis;
            }
            sendEmptyMessageDelayed(5, j);
            sendEmptyMessage(7);
        }
    }

    public static void a(Context context, JSONObject jSONObject) {
        boolean z;
        boolean z2;
        String str;
        boolean z3;
        boolean z4;
        long j;
        boolean z5;
        boolean z6 = true;
        l.j(context, jSONObject.has("start_pkg_time") ? jSONObject.optInt("start_pkg_time") * AdError.NETWORK_ERROR_CODE : -1);
        if (jSONObject.optInt("isPkgT") > 0) {
            z = true;
        } else {
            z = false;
        }
        l.f(context, z);
        l.c(context, jSONObject.getInt("logPriority"));
        l.g(context, jSONObject.has("dInstall") ? jSONObject.optInt("dInstall") > 0 : true);
        if (!jSONObject.has("isPT")) {
            z2 = true;
        } else if (jSONObject.optInt("isPT") > 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        l.h(context, z2);
        com.duapps.ad.base.a.c("TEST", "getSrc logPriority :" + jSONObject.getInt("logPriority"));
        String optString = jSONObject.optString("imId");
        try {
            com.duapps.ad.base.a.c("TEST", "from server Inmobi ID = " + optString);
            str = new String(b.a("8a1n9d0i3c1y0c2f", "8a1n9d0i3c1y0c2f", Base64.decode(optString.getBytes(), 0)));
            try {
                com.duapps.ad.base.a.c("TEST", "AES Inmobi ID = " + str);
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            str = optString;
        }
        l.b(context, str);
        com.duapps.ad.base.a.c("TEST", "getSrc Inmobi ID = " + str);
        long optLong = jSONObject.optLong("tcppCacheTime", 0);
        if (optLong > 0) {
            optLong = optLong * 60 * 1000;
        }
        l.b(context, optLong);
        if (jSONObject.has("tcppPullTime")) {
            int i = jSONObject.getInt("tcppPullTime");
            com.duapps.ad.base.a.c(f3823b, "tcppPullTime  = " + i);
            if (i == 0 || i >= 30) {
                l.g(context, i);
            }
        }
        int optInt = jSONObject.optInt("fbct", 0);
        com.duapps.ad.base.a.c("StrategyPuller", "fbct  = " + optInt);
        if (optInt > 0) {
            l.h(context, optInt);
        }
        com.duapps.ad.base.a.c("TEST", "getSrcPrio --> tcppCacheTime : " + optLong);
        boolean z7 = jSONObject.optInt("isAllowC", 1) > 0;
        if (jSONObject.optInt("isAllowT", 1) > 0) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (jSONObject.optInt("isAllowS", 1) > 0) {
            z4 = true;
        } else {
            z4 = false;
        }
        long optLong2 = jSONObject.optLong("nuInterval", 0);
        if (optLong2 <= 0) {
            j = 14400000;
        } else {
            j = optLong2 * 60 * 1000;
        }
        l.b(context, z7);
        l.c(context, z3);
        l.d(context, z4);
        l.d(context, j);
        if (jSONObject.optInt("itwd", 1) > 0) {
            z5 = true;
        } else {
            z5 = false;
        }
        l.a(context, z5);
        JSONArray optJSONArray = jSONObject.optJSONArray("priorityBrowsers");
        if (optJSONArray != null && optJSONArray.length() > 0) {
            com.duapps.ad.base.a.c("StrategyPuller", "browserArray : " + optJSONArray.toString());
            l.c(context, optJSONArray.toString());
        }
        l.d(context, jSONObject.optString("pk"));
        String optString2 = jSONObject.optString(FirebaseAnalytics.Param.LOCATION);
        com.duapps.ad.base.a.c("StrategyPuller", "location:" + optString2);
        l.e(context, optString2);
        if (jSONObject.optInt("isSus", 1) <= 0) {
            z6 = false;
        }
        com.duapps.ad.base.a.c("StrategyPuller", "isSus:" + z6);
        l.e(context, z6);
        String optString3 = jSONObject.optString("exg");
        com.duapps.ad.base.a.c("StrategyPuller", "exg:" + optString3);
        l.f(context, optString3);
        a(jSONObject.optString("tps", "1000"), context);
    }

    private static void a(String str, Context context) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4 = true;
        if (str.length() < 4) {
            str = "1000";
        }
        if (str.charAt(0) == '1') {
            z = true;
        } else {
            z = false;
        }
        l.i(context, z);
        if (str.charAt(1) == '1') {
            z2 = true;
        } else {
            z2 = false;
        }
        l.j(context, z2);
        if (str.charAt(2) == '1') {
            z3 = true;
        } else {
            z3 = false;
        }
        l.k(context, z3);
        if (str.charAt(3) != '1') {
            z4 = false;
        }
        l.l(context, z4);
    }

    private static class a implements Runnable {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public Context f3826a;

        public a(Context context) {
            this.f3826a = context;
        }

        /* access modifiers changed from: private */
        public void a(JSONObject jSONObject) {
            JSONObject jSONObject2;
            try {
                String f2 = e.f(jSONObject.getString("datas"));
                jSONObject2 = new JSONObject(f2);
                com.duapps.ad.base.a.c("decode", "strategy decode succ:--> " + f2);
            } catch (Exception e2) {
                jSONObject2 = jSONObject.getJSONObject("datas");
                com.duapps.ad.base.a.c("decode", "strategy decode fail" + jSONObject.toString());
            }
            d.a(this.f3826a, jSONObject2);
            JSONArray jSONArray = jSONObject2.getJSONArray("list");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject optJSONObject = jSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    l.a(this.f3826a, optJSONObject.optInt("sid"), optJSONObject.toString());
                }
            }
            int optInt = jSONObject2.optInt("tcppTctp");
            l.e(this.f3826a, optInt);
            com.duapps.ad.base.a.c("TEST", "tcppTctp = " + optInt);
            l.c(this.f3826a);
        }

        public void run() {
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            try {
                List<NameValuePair> a2 = g.a(this.f3826a, o.b(this.f3826a), true);
                HashSet<Integer> a3 = o.a(this.f3826a).a();
                StringBuilder sb = new StringBuilder();
                Iterator<Integer> it = a3.iterator();
                while (it.hasNext()) {
                    sb.append(it.next());
                    sb.append(",");
                }
                String sb2 = sb.toString();
                if (sb2.length() > 1) {
                    sb2 = sb2.substring(0, sb2.length() - 1);
                }
                a2.add(new BasicNameValuePair("sid", sb2));
                a2.add(new BasicNameValuePair("res", "1080*460,244*244,170*170,108*108"));
                URL url = new URL(d.f3824c + URLEncodedUtils.format(a2, "UTF-8"));
                com.duapps.ad.base.a.c("test", "get src priority url: " + url);
                s.a(url, new s.b() {
                    public void a(int i, s.a aVar) {
                        l.a(a.this.f3826a, aVar.f3611c);
                        JSONObject jSONObject = aVar.f3609a;
                        if (jSONObject == null) {
                            com.duapps.ad.base.a.c(d.f3823b, "getSrc code :" + i + " ,\n responseJson is null!");
                            return;
                        }
                        com.duapps.ad.base.a.c(d.f3823b, "getSrc code :" + i + " ,\n response: " + jSONObject.toString());
                        if (200 == i && jSONObject != null) {
                            try {
                                a.this.a(jSONObject);
                            } catch (JSONException e2) {
                                com.duapps.ad.stats.b.a(a.this.f3826a, (int) PackageHelper.INSTALL_PARSE_FAILED_BAD_MANIFEST, SystemClock.elapsedRealtime() - elapsedRealtime);
                                return;
                            }
                        } else if (304 == i) {
                            return;
                        }
                        com.duapps.ad.stats.b.a(a.this.f3826a, i, SystemClock.elapsedRealtime() - elapsedRealtime);
                    }

                    public void a(int i, String str) {
                        l.c(a.this.f3826a);
                        com.duapps.ad.stats.b.a(a.this.f3826a, i, SystemClock.elapsedRealtime() - elapsedRealtime);
                    }
                }, l.e(this.f3826a));
            } catch (MalformedURLException e2) {
                com.duapps.ad.stats.b.a(this.f3826a, (int) PackageHelper.INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION, SystemClock.elapsedRealtime() - elapsedRealtime);
            }
            e.a(this.f3826a).a((String) null);
        }
    }
}
