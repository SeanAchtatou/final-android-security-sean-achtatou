package com.duapps.ad.internal.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.duapps.ad.AdError;
import com.duapps.ad.base.l;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

public class a {

    /* renamed from: b  reason: collision with root package name */
    private static volatile a f3799b;

    /* renamed from: e  reason: collision with root package name */
    private static boolean f3800e;

    /* renamed from: a  reason: collision with root package name */
    private int f3801a;

    /* renamed from: c  reason: collision with root package name */
    private Context f3802c;

    /* renamed from: d  reason: collision with root package name */
    private String f3803d;

    private a(Context context) {
        this.f3802c = context.getApplicationContext();
    }

    public static a a(Context context) {
        if (f3799b == null) {
            synchronized (a.class) {
                if (f3799b == null) {
                    f3799b = new a(context);
                }
            }
        }
        return f3799b;
    }

    public boolean a(String str) {
        this.f3803d = str;
        return a();
    }

    public boolean a() {
        boolean z = false;
        boolean c2 = c();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        long u = l.u(this.f3802c);
        long v = l.v(this.f3802c);
        if (c2) {
            boolean z2 = elapsedRealtime - u >= v;
            com.duapps.ad.base.a.c("NetworkRequestFilterManager", "isOverTime（4H）:" + z2 + ", last check time:" + u);
            if (!z2) {
                this.f3801a = 0;
                com.duapps.ad.stats.b.a(this.f3802c, this.f3801a, this.f3803d);
                return z;
            }
        }
        z = d();
        if (!z) {
            com.duapps.ad.stats.b.a(this.f3802c, h(), this.f3803d);
        }
        return z;
    }

    private boolean j() {
        boolean a2 = com.duapps.ad.internal.b.c.a(this.f3802c, "android.permission.READ_PHONE_STATE");
        com.duapps.ad.base.a.c("NetworkRequestFilterManager", "checkReadPhoneStatePermission:" + a2);
        return a2;
    }

    public boolean b() {
        boolean a2 = com.duapps.ad.internal.b.c.a(this.f3802c, "android.permission.ACCESS_WIFI_STATE");
        com.duapps.ad.base.a.c("NetworkRequestFilterManager", "checkReadWifiStatePermission:" + a2);
        return a2;
    }

    public boolean c() {
        return l.w(this.f3802c);
    }

    public boolean d() {
        if (l.x(this.f3802c) && b()) {
            boolean e2 = e();
            com.duapps.ad.base.a.c("NetworkRequestFilterManager", "isCharlesRunning:" + e2);
            if (e2) {
                this.f3801a = 1;
                return false;
            }
        }
        if (l.y(this.f3802c)) {
            boolean f2 = f();
            com.duapps.ad.base.a.c("NetworkRequestFilterManager", "isTcpDumpRunning:" + f2);
            if (f2) {
                this.f3801a = 2;
                return false;
            }
        }
        if (l.z(this.f3802c)) {
            boolean g2 = g();
            com.duapps.ad.base.a.c("NetworkRequestFilterManager", "isSimulator:" + g2);
            if (g2) {
                this.f3801a = 5;
                return false;
            }
        }
        return true;
    }

    public boolean e() {
        if (!b(this.f3802c) || TextUtils.isEmpty(System.getProperty("http.proxyHost"))) {
            return false;
        }
        return true;
    }

    public boolean f() {
        return i();
    }

    public boolean g() {
        boolean z = false;
        b a2 = b.a(this.f3802c);
        boolean z2 = a2.k() || a2.h() || a2.b() || a2.d() || a2.c() || a2.j();
        if (!j()) {
            z = z2;
        } else if (z2 || a2.f() || a2.e() || a2.i() || a2.g()) {
            z = true;
        }
        if (z) {
            a2.a();
        }
        return z;
    }

    public int h() {
        return this.f3801a;
    }

    private static boolean b(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        return 1 == activeNetworkInfo.getType();
    }

    public static boolean i() {
        if (f3800e) {
            return f3800e;
        }
        f3800e = k() != null;
        return f3800e;
    }

    private static String k() {
        String[] split = System.getenv("PATH").split(":");
        int length = split.length;
        for (int i = 0; i < length; i++) {
            String str = split[i] + "/su";
            if (new File(str).exists()) {
                com.duapps.ad.base.a.c("NetworkRequestFilterManager", "path:" + str + " is exists");
                return str;
            }
        }
        return null;
    }

    static class c {

        /* renamed from: a  reason: collision with root package name */
        public String f3815a;

        /* renamed from: b  reason: collision with root package name */
        public String f3816b;

        public c(String str, String str2) {
            this.f3815a = str;
            this.f3816b = str2;
        }
    }

    /* renamed from: com.duapps.ad.internal.a.a$a  reason: collision with other inner class name */
    static class C0048a {
        public static boolean a() {
            BufferedReader bufferedReader;
            int i;
            boolean z;
            boolean z2 = false;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/net/tcp")), AdError.NETWORK_ERROR_CODE);
                try {
                    bufferedReader.readLine();
                    ArrayList arrayList = new ArrayList();
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        arrayList.add(C0049a.a(readLine.split("\\W+")));
                    }
                    bufferedReader.close();
                    Iterator it = arrayList.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            i = -1;
                            break;
                        }
                        C0049a aVar = (C0049a) it.next();
                        if (aVar.f3805b == 0) {
                            i = aVar.f3806c;
                            break;
                        }
                    }
                    if (i != -1) {
                        Iterator it2 = arrayList.iterator();
                        while (it2.hasNext()) {
                            C0049a aVar2 = (C0049a) it2.next();
                            if (aVar2.f3805b == 0 || aVar2.f3806c != i) {
                                z = z2;
                            } else {
                                z = true;
                            }
                            z2 = z;
                        }
                    }
                    bufferedReader.close();
                } catch (Exception e2) {
                    e = e2;
                    try {
                        e.printStackTrace();
                        bufferedReader.close();
                        return z2;
                    } catch (Throwable th) {
                        th = th;
                        bufferedReader.close();
                        throw th;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                bufferedReader = null;
                e.printStackTrace();
                bufferedReader.close();
                return z2;
            } catch (Throwable th2) {
                th = th2;
                bufferedReader = null;
                bufferedReader.close();
                throw th;
            }
            return z2;
        }

        /* renamed from: com.duapps.ad.internal.a.a$a$a  reason: collision with other inner class name */
        public static class C0049a {

            /* renamed from: a  reason: collision with root package name */
            public int f3804a;

            /* renamed from: b  reason: collision with root package name */
            public long f3805b;

            /* renamed from: c  reason: collision with root package name */
            public int f3806c;

            static C0049a a(String[] strArr) {
                return new C0049a(strArr[1], strArr[2], strArr[3], strArr[4], strArr[5], strArr[6], strArr[7], strArr[8], strArr[9], strArr[10], strArr[11], strArr[12], strArr[13], strArr[14]);
            }

            public C0049a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14) {
                this.f3804a = Integer.parseInt(str, 16);
                this.f3805b = Long.parseLong(str2, 16);
                this.f3806c = Integer.parseInt(str3, 16);
            }
        }
    }

    static class b {

        /* renamed from: b  reason: collision with root package name */
        private static b f3807b;

        /* renamed from: e  reason: collision with root package name */
        private static String[] f3808e = {"15555215554", "15555215556", "15555215558", "15555215560", "15555215562", "15555215564", "15555215566", "15555215568", "15555215570", "15555215572", "15555215574", "15555215576", "15555215578", "15555215580", "15555215582", "15555215584"};

        /* renamed from: f  reason: collision with root package name */
        private static String[] f3809f = {"000000000000000", "e21833235b6eef10", "012345678912345"};

        /* renamed from: g  reason: collision with root package name */
        private static String[] f3810g = {"310260000000000"};

        /* renamed from: h  reason: collision with root package name */
        private static String[] f3811h = {"/dev/socket/qemud", "/dev/qemu_pipe"};
        private static String[] i = {"/dev/socket/genyd", "/dev/socket/baseband_genyd"};
        private static String[] j = {"goldfish"};
        private static c[] k = {new c("init.svc.qemud", null), new c("init.svc.qemu-props", null), new c("qemu.hw.mainkeys", null), new c("qemu.sf.fake_camera", null), new c("qemu.sf.lcd_density", null), new c("ro.bootloader", "unknown"), new c("ro.bootmode", "unknown"), new c("ro.hardware", "goldfish"), new c("ro.kernel.android.qemud", null), new c("ro.kernel.qemu.gles", null), new c("ro.kernel.qemu", "1"), new c("ro.product.device", "generic"), new c("ro.product.model", "sdk"), new c("ro.product.name", "sdk"), new c("ro.serialno", null)};
        private static int l = 5;

        /* renamed from: a  reason: collision with root package name */
        private Context f3812a;

        /* renamed from: c  reason: collision with root package name */
        private int f3813c;

        /* renamed from: d  reason: collision with root package name */
        private String f3814d;

        private b(Context context) {
            this.f3812a = context;
        }

        public static b a(Context context) {
            if (f3807b == null) {
                synchronized (b.class) {
                    if (f3807b == null) {
                        f3807b = new b(context.getApplicationContext());
                    }
                }
            }
            return f3807b;
        }

        private void a(String str, int i2) {
            this.f3814d = str;
            this.f3813c = i2;
        }

        public void a() {
            com.duapps.ad.stats.b.b(this.f3812a, this.f3813c, this.f3814d);
        }

        public boolean b() {
            for (String str : f3811h) {
                if (new File(str).exists()) {
                    a("hasPipes " + str, 1);
                    return true;
                }
            }
            return false;
        }

        public boolean c() {
            for (String str : i) {
                if (new File(str).exists()) {
                    a("hasGenyFiles " + str, 3);
                    return true;
                }
            }
            return false;
        }

        public boolean d() {
            for (File file : new File[]{new File("/proc/tty/drivers"), new File("/proc/cpuinfo")}) {
                if (file.exists() && file.canRead()) {
                    byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
                    try {
                        FileInputStream fileInputStream = new FileInputStream(file);
                        fileInputStream.read(bArr);
                        fileInputStream.close();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    String str = new String(bArr);
                    for (String str2 : j) {
                        if (str.indexOf(str2) != -1) {
                            a("hasQEmuDrivers " + str2, 4);
                            return true;
                        }
                    }
                    continue;
                }
            }
            return false;
        }

        public boolean e() {
            String line1Number = ((TelephonyManager) this.f3812a.getSystemService("phone")).getLine1Number();
            for (String str : f3808e) {
                if (str.equalsIgnoreCase(line1Number)) {
                    a("hasKnownPhoneNumber " + str, 5);
                    return true;
                }
            }
            return false;
        }

        public boolean f() {
            String deviceId = ((TelephonyManager) this.f3812a.getSystemService("phone")).getDeviceId();
            for (String str : f3809f) {
                if (str.equalsIgnoreCase(deviceId)) {
                    a("hasKnownDeviceId " + str, 6);
                    return true;
                }
            }
            return false;
        }

        public boolean g() {
            String subscriberId = ((TelephonyManager) this.f3812a.getSystemService("phone")).getSubscriberId();
            for (String str : f3810g) {
                if (str.equalsIgnoreCase(subscriberId)) {
                    a("hasKnownImsi " + str, 7);
                    return true;
                }
            }
            return false;
        }

        public boolean h() {
            String str = Build.BOARD;
            String str2 = Build.BRAND;
            String str3 = Build.DEVICE;
            String str4 = Build.HARDWARE;
            String str5 = Build.MODEL;
            String str6 = Build.PRODUCT;
            StringBuilder sb = new StringBuilder("hasEmulatorBuild ");
            if (str.compareTo("unknown") == 0) {
                sb.append("BOARD:" + str);
                a(sb.toString(), 8);
                return true;
            } else if (str2.compareTo("generic") == 0) {
                sb.append("BOARD:" + str);
                a(sb.toString(), 8);
                return true;
            } else if (str3.compareTo("generic") == 0) {
                sb.append("DEVICE:" + str3);
                a(sb.toString(), 8);
                return true;
            } else if (str5.compareTo("sdk") == 0) {
                sb.append("MODEL:" + str5);
                a(sb.toString(), 8);
                return true;
            } else if (str6.compareTo("sdk") == 0) {
                sb.append("PRODUCT:" + str6);
                a(sb.toString(), 8);
                return true;
            } else if (str4.compareTo("goldfish") != 0) {
                return false;
            } else {
                sb.append("HARDWARE:" + str4);
                a(sb.toString(), 8);
                return true;
            }
        }

        public boolean i() {
            String networkOperatorName = ((TelephonyManager) this.f3812a.getSystemService("phone")).getNetworkOperatorName();
            boolean z = networkOperatorName == null || networkOperatorName.equalsIgnoreCase(io.fabric.sdk.android.services.common.a.ANDROID_CLIENT_TYPE);
            a("isOperatorNameAndroid " + networkOperatorName, 9);
            return z;
        }

        public boolean j() {
            try {
                boolean a2 = C0048a.a();
                a("hasAdbInEmulator " + a2, 10);
                return a2;
            } catch (Exception e2) {
                e2.printStackTrace();
                return false;
            }
        }

        public boolean k() {
            String a2 = a(this.f3812a, "ro.kernel.qemu");
            boolean z = !TextUtils.isEmpty(a2) && a2.equals("1");
            if (z) {
                a("hasQEmu " + a2, 11);
            }
            return z;
        }

        public static String a(Context context, String str) {
            try {
                Class<?> loadClass = context.getClassLoader().loadClass("android.os.SystemProperties");
                return (String) loadClass.getMethod("get", String.class).invoke(loadClass, new String(str));
            } catch (IllegalArgumentException e2) {
                throw e2;
            } catch (Exception e3) {
                throw null;
            }
        }
    }
}
