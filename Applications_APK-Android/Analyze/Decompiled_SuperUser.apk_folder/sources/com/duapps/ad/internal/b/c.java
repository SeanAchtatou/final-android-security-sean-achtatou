package com.duapps.ad.internal.b;

import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.support.v4.app.f;
import android.support.v4.content.m;
import android.support.v4.util.i;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final i<String, Integer> f3843a = new i<>(8);

    /* renamed from: b  reason: collision with root package name */
    private static volatile int f3844b = -1;

    private static boolean a(String str) {
        Integer num = f3843a.get(str);
        return num == null || Build.VERSION.SDK_INT >= num.intValue();
    }

    public static boolean a(Context context, String... strArr) {
        for (String str : strArr) {
            if (a(str) && !a(context, str)) {
                return false;
            }
        }
        return true;
    }

    private static boolean a(Context context, String str) {
        if (Build.VERSION.SDK_INT >= 23 && "Xiaomi".equalsIgnoreCase(Build.MANUFACTURER)) {
            return b(context, str);
        }
        try {
            if (m.a(context, str) == 0) {
                return true;
            }
            return false;
        } catch (RuntimeException e2) {
            return false;
        }
    }

    private static boolean b(Context context, String str) {
        String a2 = f.a(str);
        if (a2 == null) {
            return true;
        }
        if (f.a(context, a2, Process.myUid(), context.getPackageName()) == 0 && m.a(context, str) == 0) {
            return true;
        }
        return false;
    }
}
