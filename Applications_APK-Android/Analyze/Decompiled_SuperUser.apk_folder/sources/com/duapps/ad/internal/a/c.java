package com.duapps.ad.internal.a;

import android.content.Context;
import android.text.TextUtils;
import com.duapps.ad.base.l;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public int f3820a;

    /* renamed from: b  reason: collision with root package name */
    public List<String> f3821b = new ArrayList();

    /* renamed from: c  reason: collision with root package name */
    public Map<String, Long> f3822c = new HashMap();

    public long a(String str) {
        if (this.f3822c.containsKey(str)) {
            return this.f3822c.get(str).longValue();
        }
        return 2000;
    }

    public static c a(JSONObject jSONObject) {
        c cVar = new c();
        cVar.f3820a = jSONObject.optInt("sid");
        JSONArray optJSONArray = jSONObject.optJSONArray("priority");
        if (optJSONArray != null && optJSONArray.length() > 0) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                cVar.f3821b.add(optJSONArray.optString(i));
            }
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("wt");
        for (String next : cVar.f3821b) {
            cVar.f3822c.put(next, Long.valueOf(optJSONObject.optLong(next, 2000)));
        }
        return cVar;
    }

    public static c a(Context context, int i, boolean z) {
        String d2 = l.d(context, i);
        try {
            if (!TextUtils.isEmpty(d2)) {
                return a(new JSONObject(d2));
            }
        } catch (JSONException e2) {
        }
        return a(i, z);
    }

    public static c a(int i, boolean z) {
        c cVar = new c();
        cVar.f3820a = i;
        ArrayList arrayList = new ArrayList(2);
        cVar.f3821b = arrayList;
        arrayList.add("facebook");
        arrayList.add("download");
        if (z) {
            arrayList.add("admob");
            arrayList.add("dlh");
            arrayList.add("inmobi");
        }
        HashMap hashMap = new HashMap(2);
        cVar.f3822c = hashMap;
        hashMap.put("facebook", 2000L);
        hashMap.put("download", 2000L);
        if (z) {
            hashMap.put("dlh", 2000L);
            hashMap.put("inmobi", 2000L);
            hashMap.put("admob", 2000L);
        }
        return cVar;
    }
}
