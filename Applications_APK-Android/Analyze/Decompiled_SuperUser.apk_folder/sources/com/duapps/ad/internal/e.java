package com.duapps.ad.internal;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

class e extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3852a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(a aVar, Handler handler) {
        super(handler);
        this.f3852a = aVar;
    }

    public void onChange(boolean z, Uri uri) {
        super.onChange(z, uri);
        if (uri != null) {
            this.f3852a.a(uri);
        }
    }
}
