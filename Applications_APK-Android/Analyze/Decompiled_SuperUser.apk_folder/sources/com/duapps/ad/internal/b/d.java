package com.duapps.ad.internal.b;

import android.os.Handler;
import android.os.Looper;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static Handler f3845a;

    public static boolean a() {
        return Looper.getMainLooper().equals(Looper.myLooper());
    }

    static {
        f3845a = null;
        f3845a = new Handler(Looper.getMainLooper());
    }

    public static void a(Runnable runnable) {
        f3845a.post(runnable);
    }

    public static void a(Runnable runnable, int i) {
        f3845a.postDelayed(runnable, (long) i);
    }
}
