package com.duapps.ad.internal;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Uri f3791a = Uri.parse("content://downloads");

    /* renamed from: b  reason: collision with root package name */
    private static final String f3792b = a.class.getSimpleName();

    /* renamed from: d  reason: collision with root package name */
    private static a f3793d;

    /* renamed from: c  reason: collision with root package name */
    private Context f3794c;

    /* renamed from: e  reason: collision with root package name */
    private List f3795e = new ArrayList();

    /* renamed from: f  reason: collision with root package name */
    private List f3796f = new ArrayList();

    /* renamed from: g  reason: collision with root package name */
    private ArrayList f3797g = new ArrayList();

    /* renamed from: h  reason: collision with root package name */
    private final Handler f3798h = new Handler(new d(this));
    private final ContentObserver i = new e(this, this.f3798h);

    private a(Context context) {
        this.f3794c = context;
    }

    public static a a(Context context) {
        if (f3793d == null) {
            synchronized (a.class) {
                if (f3793d == null) {
                    f3793d = new a(context.getApplicationContext());
                }
            }
        }
        return f3793d;
    }

    /* access modifiers changed from: private */
    public void a(Uri uri) {
        List<String> pathSegments = uri.getPathSegments();
        if (pathSegments != null && pathSegments.size() > 1) {
            String str = pathSegments.get(1);
            synchronized (this.f3796f) {
                if (!this.f3796f.contains(str)) {
                    this.f3796f.add(str);
                    a(str);
                }
            }
        }
    }

    private static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e2) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r4 = r7.f3797g;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004b, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r5 = r7.f3797g.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0056, code lost:
        if (r5.hasNext() == false) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0058, code lost:
        ((com.duapps.ad.internal.b) r5.next()).a(r3, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        monitor-exit(r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r8) {
        /*
            r7 = this;
            r0 = 0
            android.database.Cursor r1 = r7.b(r8)     // Catch:{ Throwable -> 0x007d, all -> 0x0078 }
            if (r1 != 0) goto L_0x000b
            a(r1)
        L_0x000a:
            return
        L_0x000b:
            java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            r1.moveToNext()     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            r0 = 9
            java.lang.String r2 = r1.getString(r0)     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            r0 = 15
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            boolean r3 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            if (r3 != 0) goto L_0x0074
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            java.lang.String r3 = "packageName"
            java.lang.String r3 = r0.getQueryParameter(r3)     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            boolean r0 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            if (r0 != 0) goto L_0x0074
            java.util.List r4 = r7.f3795e     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            monitor-enter(r4)     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            java.util.List r0 = r7.f3795e     // Catch:{ all -> 0x006b }
            boolean r0 = r0.contains(r3)     // Catch:{ all -> 0x006b }
            if (r0 == 0) goto L_0x0043
            monitor-exit(r4)     // Catch:{ all -> 0x006b }
            a(r1)
            goto L_0x000a
        L_0x0043:
            java.util.List r0 = r7.f3795e     // Catch:{ all -> 0x006b }
            r0.add(r3)     // Catch:{ all -> 0x006b }
            monitor-exit(r4)     // Catch:{ all -> 0x006b }
            java.util.ArrayList r4 = r7.f3797g     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            monitor-enter(r4)     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
            java.util.ArrayList r0 = r7.f3797g     // Catch:{ all -> 0x0062 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ all -> 0x0062 }
        L_0x0052:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x0062 }
            if (r0 == 0) goto L_0x0073
            java.lang.Object r0 = r5.next()     // Catch:{ all -> 0x0062 }
            com.duapps.ad.internal.b r0 = (com.duapps.ad.internal.b) r0     // Catch:{ all -> 0x0062 }
            r0.a(r3, r2)     // Catch:{ all -> 0x0062 }
            goto L_0x0052
        L_0x0062:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0062 }
            throw r0     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
        L_0x0065:
            r0 = move-exception
            r0 = r1
        L_0x0067:
            a(r0)
            goto L_0x000a
        L_0x006b:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x006b }
            throw r0     // Catch:{ Throwable -> 0x0065, all -> 0x006e }
        L_0x006e:
            r0 = move-exception
        L_0x006f:
            a(r1)
            throw r0
        L_0x0073:
            monitor-exit(r4)     // Catch:{ all -> 0x0062 }
        L_0x0074:
            a(r1)
            goto L_0x000a
        L_0x0078:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x006f
        L_0x007d:
            r1 = move-exception
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duapps.ad.internal.a.a(java.lang.String):void");
    }

    private Cursor b(String str) {
        return this.f3794c.getContentResolver().query(Uri.parse("content://downloads/public_downloads/" + str), null, null, null, null);
    }

    public void a() {
        this.f3794c.getContentResolver().registerContentObserver(f3791a, true, this.i);
    }

    public void a(b bVar) {
        if (!this.f3797g.contains(bVar)) {
            synchronized (this.f3797g) {
                this.f3797g.add(bVar);
            }
        }
    }
}
