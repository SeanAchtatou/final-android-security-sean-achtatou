package com.duapps.ad.internal;

import android.content.Context;
import android.text.TextUtils;
import com.duapps.ad.base.DuAdNetwork;
import com.duapps.ad.base.l;
import com.duapps.ad.base.r;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.internal.a.a;
import com.duapps.ad.stats.e;
import com.duapps.ad.stats.g;
import java.util.ArrayList;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3847a = c.class.getSimpleName();

    /* renamed from: c  reason: collision with root package name */
    private static c f3848c;

    /* renamed from: b  reason: collision with root package name */
    private ArrayList<String> f3849b = new ArrayList<>();

    /* renamed from: d  reason: collision with root package name */
    private Context f3850d;

    private c(Context context) {
        this.f3850d = context;
    }

    public static c a(Context context) {
        if (f3848c == null) {
            synchronized (c.class) {
                if (f3848c == null) {
                    f3848c = new c(context.getApplicationContext());
                }
            }
        }
        return f3848c;
    }

    public void a(String str, boolean z, boolean z2) {
        e a2 = r.a(this.f3850d).a(str);
        if (a2 == null) {
            g.a(this.f3850d, new e(AdData.a(this.f3850d, -999, null, str, null)), z ? -2 : -1, 0);
            if (b(str)) {
                return;
            }
            if ((b() && z2) || (d() && !z2)) {
                com.duapps.ad.internal.a.e.a(this.f3850d).a(DuAdNetwork.f3522c, str);
                a(str);
            }
        } else if (b(str)) {
        } else {
            if ((a() && z2) || (c() && !z2)) {
                a(str, a2, z);
                a(str);
            }
        }
    }

    private void a(String str, e eVar, boolean z) {
        int i = 1;
        if (a.a(this.f3850d).a(str)) {
            eVar.a(true);
            if (!z) {
                i = -1;
            }
            eVar.a(i);
            new com.duapps.ad.stats.c(this.f3850d).e(eVar, eVar.j());
        }
    }

    private boolean a() {
        return l.H(this.f3850d);
    }

    private boolean b() {
        return l.I(this.f3850d);
    }

    private boolean c() {
        return l.J(this.f3850d);
    }

    private boolean d() {
        return l.K(this.f3850d);
    }

    private void a(String str) {
        if (!TextUtils.isEmpty(str) && !b(str)) {
            synchronized (this.f3849b) {
                this.f3849b.add(str);
            }
        }
    }

    private boolean b(String str) {
        boolean contains;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        synchronized (this.f3849b) {
            contains = this.f3849b.contains(str);
        }
        return contains;
    }
}
