package com.duapps.ad.stats;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.duapps.ad.base.f;
import com.duapps.ad.base.h;
import com.duapps.ad.base.i;
import com.duapps.ad.base.j;
import com.duapps.ad.base.k;
import com.duapps.ad.base.l;
import com.duapps.ad.base.v;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.internal.b.e;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;

public class c extends d {
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public String f3871d = "";
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public boolean f3872e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public boolean f3873f = false;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public String f3874g = "";
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public String f3875h = "";
    /* access modifiers changed from: private */
    public String i = "";
    private List<String> j = new ArrayList();
    /* access modifiers changed from: private */
    public Context k;
    private WebView l;
    private a m;
    /* access modifiers changed from: private */
    public int n;

    protected interface a {
    }

    class b implements a, RedirectHandler {

        /* renamed from: b  reason: collision with root package name */
        private e f3887b;

        /* renamed from: c  reason: collision with root package name */
        private volatile boolean f3888c = false;

        public b(e eVar) {
            this.f3887b = eVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duapps.ad.stats.c.a(com.duapps.ad.stats.e, java.lang.String):void
         arg types: [com.duapps.ad.stats.e, java.lang.String]
         candidates:
          com.duapps.ad.stats.c.a(com.duapps.ad.stats.c, java.lang.String):java.lang.String
          com.duapps.ad.stats.c.a(java.lang.String, com.duapps.ad.stats.e):java.lang.String
          com.duapps.ad.stats.c.a(java.lang.String, java.util.List<java.lang.String>):boolean
          com.duapps.ad.stats.c.a(java.lang.String, java.lang.String):java.lang.String
          com.duapps.ad.stats.c.a(com.duapps.ad.stats.e, boolean):void
          com.duapps.ad.stats.d.a(com.duapps.ad.stats.e, java.lang.String):android.net.Uri
          com.duapps.ad.stats.d.a(com.duapps.ad.stats.d, android.widget.Toast):android.widget.Toast
          com.duapps.ad.stats.c.a(com.duapps.ad.stats.e, java.lang.String):void */
        public boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
            long j = 2;
            if (this.f3888c) {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[Http]Action canceled.");
                }
                g.g(c.this.k, this.f3887b);
                c.this.f();
            } else {
                int p = this.f3887b.p();
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                com.duapps.ad.base.a.c("ToolClickHandler", "statusCode " + statusCode);
                if (statusCode == 303 || statusCode == 302 || statusCode == 301 || statusCode == 307) {
                    String value = httpResponse.getHeaders("Location")[0].getValue();
                    if (value == null) {
                        if (p != 0) {
                            Context a2 = c.this.k;
                            e eVar = this.f3887b;
                            if (p <= 0) {
                                j = 1;
                            }
                            g.a(a2, eVar, j, statusCode, c.this.n);
                        }
                        if (com.duapps.ad.base.a.a()) {
                            com.duapps.ad.base.a.c("ToolClickHandler", "[Http] null URL.");
                        }
                        if (!this.f3887b.n()) {
                            c.this.b();
                            c.this.i(this.f3887b, this.f3887b.j());
                        }
                        c.this.f();
                    } else if (d.b(value)) {
                        if (p != 0) {
                            Context a3 = c.this.k;
                            e eVar2 = this.f3887b;
                            if (p <= 0) {
                                j = 1;
                            }
                            g.a(a3, eVar2, j, statusCode, "tctp", c.this.n);
                        }
                        if (com.duapps.ad.base.a.a()) {
                            com.duapps.ad.base.a.c("ToolClickHandler", "[Http] Market URL: " + value);
                        }
                        c.this.a(this.f3887b, value);
                        this.f3887b.b(true);
                        if (!this.f3887b.n()) {
                            c.this.b();
                            if (!TextUtils.isEmpty(c.this.f3875h)) {
                                g.a(c.this.k, this.f3887b, c.this.f3874g, c.this.f3875h, c.this.i);
                                g.a(c.this.k, this.f3887b, c.this.f3873f ? "1" : "0", c.this.f3875h);
                            }
                            c.this.h(this.f3887b, value);
                        }
                        c.this.f();
                    } else {
                        c.this.c(this.f3887b, value);
                    }
                } else {
                    if (p != 0) {
                        Context a4 = c.this.k;
                        e eVar3 = this.f3887b;
                        if (p <= 0) {
                            j = 1;
                        }
                        g.a(a4, eVar3, j, statusCode, c.this.n);
                    }
                    if (com.duapps.ad.base.a.a()) {
                        com.duapps.ad.base.a.c("ToolClickHandler", "[Http] non-Market URL: " + this.f3887b.j());
                    }
                    if (!this.f3887b.n()) {
                        c.this.b();
                        if (!TextUtils.isEmpty(c.this.f3875h)) {
                            g.a(c.this.k, this.f3887b, c.this.f3874g, c.this.f3875h, c.this.i);
                            g.a(c.this.k, this.f3887b, c.this.f3873f ? "1" : "0", c.this.f3875h);
                        }
                        c.this.g(this.f3887b, this.f3887b.j());
                    }
                    c.this.f();
                }
            }
            return false;
        }

        public URI getLocationURI(HttpResponse httpResponse, HttpContext httpContext) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void i(e eVar, String str) {
        if (!this.f3901b) {
            AdData f2 = eVar.f();
            String str2 = f2 != null ? f2.f3667d : null;
            if (TextUtils.isEmpty(str2)) {
                com.duapps.ad.base.a.c("ToolClickHandler", "browserUrl：" + str + " no pkgname");
                if (!TextUtils.isEmpty(this.f3875h)) {
                    g.a(this.k, eVar, this.f3874g, this.f3875h, this.i);
                    g.a(this.k, eVar, this.f3873f ? "1" : "0", this.f3875h);
                }
                g(eVar, str);
                return;
            }
            String str3 = "https://play.google.com/store/apps/details?id=" + str2;
            com.duapps.ad.base.a.c("ToolClickHandler", eVar.f().f3666c + " start google play via mock url -->" + str3);
            if (!TextUtils.isEmpty(this.f3875h)) {
                g.a(this.k, eVar, this.f3874g, this.f3875h, this.i);
                g.a(this.k, eVar, this.f3873f ? "1" : "0", this.f3875h);
            }
            if (e.a(this.k, "com.android.vending")) {
                h(eVar, str3);
            } else {
                g(eVar, str);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar, String str) {
        if (eVar.d() > 0) {
            i iVar = new i();
            iVar.f3554a = eVar.j();
            iVar.f3557d = str;
            iVar.f3555b = eVar.a();
            iVar.f3556c = 1;
            iVar.f3558e = System.currentTimeMillis();
            h.a(this.k).a(iVar);
        }
    }

    /* renamed from: com.duapps.ad.stats.c$c  reason: collision with other inner class name */
    class C0050c extends WebViewClient implements a {

        /* renamed from: a  reason: collision with root package name */
        e f3889a;

        /* renamed from: b  reason: collision with root package name */
        WebView f3890b;

        /* renamed from: d  reason: collision with root package name */
        private Runnable f3892d = new Runnable() {
            public void run() {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] timeout TIMEOUT_FINISH.");
                }
                if (!C0050c.this.f3894f && !C0050c.this.f3896h) {
                    boolean unused = C0050c.this.f3894f = true;
                    if (C0050c.this.f3895g) {
                        if (com.duapps.ad.base.a.a()) {
                            com.duapps.ad.base.a.c("ToolClickHandler", "[WebView]FinishRunnable canceled.");
                        }
                        g.g(c.this.k, C0050c.this.f3889a);
                        c.this.f();
                        return;
                    }
                    if (C0050c.this.f3890b != null) {
                        C0050c.this.f3890b.stopLoading();
                    }
                    c.this.b();
                    c.this.i(C0050c.this.f3889a, C0050c.this.f3889a.j());
                    c.this.f();
                }
            }
        };

        /* renamed from: e  reason: collision with root package name */
        private Runnable f3893e = new Runnable() {
            public void run() {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] Timeout TIMEOUT_START.");
                }
                if (!C0050c.this.f3894f && !C0050c.this.f3896h) {
                    boolean unused = C0050c.this.f3894f = true;
                    if (C0050c.this.f3895g) {
                        if (com.duapps.ad.base.a.a()) {
                            com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] StartRunnable canceled.");
                        }
                        g.g(c.this.k, C0050c.this.f3889a);
                        c.this.f();
                        return;
                    }
                    if (C0050c.this.f3890b != null) {
                        C0050c.this.f3890b.stopLoading();
                    }
                    c.this.b();
                    com.duapps.ad.base.a.c("ToolClickHandler", "timeout_上报_exg:" + c.this.f3875h);
                    c.this.i(C0050c.this.f3889a, C0050c.this.f3889a.j());
                    c.this.f();
                }
            }
        };
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public volatile boolean f3894f = false;
        /* access modifiers changed from: private */

        /* renamed from: g  reason: collision with root package name */
        public volatile boolean f3895g = false;
        /* access modifiers changed from: private */

        /* renamed from: h  reason: collision with root package name */
        public volatile boolean f3896h = false;

        public C0050c(e eVar) {
            this.f3889a = eVar;
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            a("Error: " + i);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            if (com.duapps.ad.base.a.a()) {
                com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] onPageStarted.");
            }
            this.f3890b = webView;
            this.f3894f = false;
            this.f3896h = false;
            c.this.f3900a.removeCallbacks(this.f3893e);
            c.this.f3900a.removeCallbacks(this.f3892d);
            if (com.duapps.ad.base.a.a()) {
                com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] start TIMEOUT_START: " + str);
            }
            c.this.f3900a.postDelayed(this.f3893e, 4000);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duapps.ad.stats.c.a(com.duapps.ad.stats.e, java.lang.String):void
         arg types: [com.duapps.ad.stats.e, java.lang.String]
         candidates:
          com.duapps.ad.stats.c.a(com.duapps.ad.stats.c, java.lang.String):java.lang.String
          com.duapps.ad.stats.c.a(java.lang.String, com.duapps.ad.stats.e):java.lang.String
          com.duapps.ad.stats.c.a(java.lang.String, java.util.List<java.lang.String>):boolean
          com.duapps.ad.stats.c.a(java.lang.String, java.lang.String):java.lang.String
          com.duapps.ad.stats.c.a(com.duapps.ad.stats.e, boolean):void
          com.duapps.ad.stats.d.a(com.duapps.ad.stats.e, java.lang.String):android.net.Uri
          com.duapps.ad.stats.d.a(com.duapps.ad.stats.d, android.widget.Toast):android.widget.Toast
          com.duapps.ad.stats.c.a(com.duapps.ad.stats.e, java.lang.String):void */
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            com.duapps.ad.base.a.c("ToolClickHandler", "url:" + str);
            c.this.f3902c = str;
            if (com.duapps.ad.base.a.a()) {
                com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] shouldOverrideUrlLoading.");
            }
            String a2 = c.this.a(str, this.f3889a);
            com.duapps.ad.base.a.c("ToolClickHandler", "needUrl:" + a2);
            c.this.f3900a.removeCallbacks(this.f3893e);
            c.this.f3900a.removeCallbacks(this.f3892d);
            if (this.f3895g || this.f3896h || this.f3894f) {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[WebView]Action canceled.");
                }
                this.f3896h = true;
                c.this.f();
                return true;
            } else if (a2 == null) {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] null URL.");
                }
                c.this.b();
                c.this.i(this.f3889a, this.f3889a.j());
                webView.stopLoading();
                c.this.f();
                this.f3896h = true;
                return true;
            } else {
                com.duapps.ad.base.a.c("ToolClickHandler", "是否需要加载_isUrlModify:" + c.this.f3873f + ",isMatchUrl:" + c.this.f3872e);
                if (!c.this.f3873f || !c.this.f3872e) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "不加载url " + a2);
                } else {
                    com.duapps.ad.base.a.c("ToolClickHandler", "加载url " + a2);
                    webView.loadUrl(a2);
                }
                if (d.b(a2)) {
                    if (com.duapps.ad.base.a.a()) {
                        com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] Market URL: " + a2);
                    }
                    c.this.a(this.f3889a, a2);
                    this.f3889a.b(true);
                    c.this.b();
                    com.duapps.ad.base.a.c("ToolClickHandler", "上报_mExgName:" + c.this.f3875h);
                    if (!TextUtils.isEmpty(c.this.f3875h)) {
                        g.a(c.this.k, this.f3889a, c.this.f3874g, c.this.f3875h, c.this.i);
                        g.a(c.this.k, this.f3889a, c.this.f3873f ? "1" : "0", c.this.f3875h);
                    }
                    c.this.h(this.f3889a, a2);
                    webView.stopLoading();
                    c.this.f();
                    this.f3896h = true;
                    return true;
                }
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] Decode URL: " + a2);
                }
                if (!this.f3894f) {
                    if (com.duapps.ad.base.a.a()) {
                        com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] start TIMEOUT_START: " + a2);
                    }
                    c.this.f3900a.postDelayed(this.f3893e, 4000);
                }
                return false;
            }
        }

        private void a(String str) {
            if (com.duapps.ad.base.a.a()) {
                com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] handleError");
            }
            c.this.f3900a.removeCallbacks(this.f3893e);
            c.this.f3900a.removeCallbacks(this.f3892d);
            if (this.f3895g) {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[WebView]Action canceled.");
                }
                g.g(c.this.k, this.f3889a);
                c.this.f();
            } else if (!this.f3896h) {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] onReceivedError: " + str);
                }
                this.f3890b.stopLoading();
                this.f3896h = true;
                c.this.b();
                c.this.i(this.f3889a, this.f3889a.j());
                c.this.f();
            } else if (com.duapps.ad.base.a.a()) {
                com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] already consumed");
            }
        }

        public void onPageFinished(WebView webView, String str) {
            if (com.duapps.ad.base.a.a()) {
                com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] Page finished");
            }
            c.this.f3900a.removeCallbacks(this.f3893e);
            c.this.f3900a.removeCallbacks(this.f3892d);
            if (this.f3895g) {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[WebView]Action canceled.");
                }
                g.g(c.this.k, this.f3889a);
                c.this.f();
            } else if (this.f3896h) {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] already consumed");
                }
            } else if (!this.f3894f) {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] start TIMEOUT_FINISH: " + str);
                }
                c.this.f3900a.postDelayed(this.f3892d, 2000);
            }
        }
    }

    /* access modifiers changed from: private */
    public String a(String str, e eVar) {
        String e2;
        if (eVar == null) {
            return str;
        }
        try {
            String str2 = eVar.f().v;
            if (TextUtils.isEmpty(str2)) {
                String D = l.D(this.k);
                if (TextUtils.isEmpty(D)) {
                    return str;
                }
                e2 = e.f(D);
            } else {
                e2 = e.e(str2);
            }
            return a(str, e2);
        } catch (Exception e3) {
            com.duapps.ad.base.a.c("ToolClickHandler", "getExgUrl exception:" + e3.getMessage());
            return str;
        }
    }

    public String a(String str, String str2) {
        try {
            com.duapps.ad.base.a.c("ToolClickHandler", "decode:" + str2);
            JSONArray jSONArray = new JSONArray(str2);
            com.duapps.ad.base.a.c("ToolClickHandler", jSONArray.length() + "长度");
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= jSONArray.length()) {
                    return str;
                }
                com.duapps.ad.base.a.c("ToolClickHandler", "jsonArray.length():" + jSONArray.length() + ",i:" + i3);
                String d2 = d(str, (String) jSONArray.get(i3));
                if (this.f3872e) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "jsonArray_url:" + d2);
                    return d2;
                }
                i2 = i3 + 1;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            com.duapps.ad.base.a.d("ToolClickHandler", "解析失败:" + e2.getMessage());
            return str;
        }
    }

    @TargetApi(11)
    private String d(String str, String str2) {
        String str3;
        String str4;
        String str5;
        String str6;
        boolean z;
        try {
            if (TextUtils.isEmpty(str2) || !str2.contains("@")) {
                return str;
            }
            String[] split = str2.split("@");
            String str7 = split[0];
            this.f3872e = c(str7, str);
            com.duapps.ad.base.a.c("ToolClickHandler", "isMatchUrl:" + this.f3872e);
            if (this.f3872e) {
                if (this.j != null && this.j.size() > 0) {
                    this.j.clear();
                }
                this.f3875h = str7;
                com.duapps.ad.base.a.c("ToolClickHandler", "exg_hostname:" + this.f3875h);
                com.duapps.ad.base.a.c("ToolClickHandler", "split[0]:" + split[0]);
                com.duapps.ad.base.a.c("ToolClickHandler", "split[1]:" + split[1]);
                int i2 = 1;
                str3 = str;
                while (i2 < split.length) {
                    try {
                        String str8 = split[i2];
                        com.duapps.ad.base.a.c("ToolClickHandler", "str:" + str8 + ",i:" + i2 + ",split.length:" + split.length);
                        if (str8.contains("=")) {
                            String[] split2 = str8.split("=");
                            String str9 = split2[0];
                            if (split2.length > 1) {
                                str5 = split2[1];
                            } else {
                                str5 = "";
                            }
                            Uri parse = Uri.parse(str3);
                            String queryParameter = parse.getQueryParameter(str9);
                            for (String add : parse.getQueryParameterNames()) {
                                this.j.add(add);
                            }
                            com.duapps.ad.base.a.c("ToolClickHandler", "oldKeyValue:" + queryParameter);
                            String b2 = b(str9, str5);
                            com.duapps.ad.base.a.c("ToolClickHandler", "isHasKey:" + a(str9, this.j));
                            if (!TextUtils.isEmpty(queryParameter) || a(str9, this.j)) {
                                str6 = a(str3, str9, b2);
                            } else {
                                str6 = b(str3, str9, b2);
                            }
                            str4 = c(str6, str9, b2);
                            try {
                                if (str.equals(str4)) {
                                    z = false;
                                } else {
                                    z = true;
                                }
                                this.f3873f = z;
                                com.duapps.ad.base.a.c("ToolClickHandler", "isUrlModify:" + this.f3873f);
                            } catch (Exception e2) {
                                Exception exc = e2;
                                str3 = str4;
                                e = exc;
                                com.duapps.ad.base.a.c("ToolClickHandler", "getGaidUrl exception:" + e.getMessage());
                                return str3;
                            }
                        } else {
                            str4 = str3;
                        }
                        i2++;
                        str3 = str4;
                    } catch (Exception e3) {
                        e = e3;
                    }
                }
            } else {
                str3 = str;
            }
            return str3;
        } catch (Exception e4) {
            e = e4;
            str3 = str;
            com.duapps.ad.base.a.c("ToolClickHandler", "getGaidUrl exception:" + e.getMessage());
            return str3;
        }
    }

    private boolean a(String str, List<String> list) {
        if (TextUtils.isEmpty(str) || list.size() <= 0) {
            return false;
        }
        for (String next : list) {
            if (str.equals(next)) {
                com.duapps.ad.base.a.c("ToolClickHandler", "key:" + str + ",dataKey:" + next);
                return true;
            }
        }
        return false;
    }

    private String b(String str, String str2, String str3) {
        String str4 = str + "&" + str2 + "=" + str3;
        this.f3873f = true;
        com.duapps.ad.base.a.c("ToolClickHandler", "appendNewUrl:" + str4);
        return str4;
    }

    private String c(String str, String str2, String str3) {
        if (TextUtils.isEmpty(str3) || !"delete".equals(str3)) {
            return str;
        }
        String str4 = "&" + str2 + "=" + "delete";
        if (!str.contains(str4)) {
            return str;
        }
        String replace = str.replace(str4, "");
        this.f3873f = true;
        com.duapps.ad.base.a.c("ToolClickHandler", "@null:" + replace);
        return replace;
    }

    public String b(String str, String str2) {
        if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str)) {
            return "";
        }
        if (str2.equals("{gaid}")) {
            String str3 = this.f3871d;
            this.i = str;
            this.f3874g = "gaid";
            return str3;
        } else if (str2.equals("{aid}")) {
            String c2 = e.c(this.k);
            if (!TextUtils.isEmpty(this.i)) {
                return c2;
            }
            this.i = str;
            this.f3874g = "anid";
            return c2;
        } else if (str2.equals("{gaid_md5}")) {
            String h2 = e.h(this.f3871d);
            this.i = str + "_md5";
            this.f3874g = "gaid";
            return h2;
        } else if (str2.equals("{gaid_sha1}")) {
            String g2 = e.g(this.f3871d);
            this.f3874g = "gaid";
            this.i = str + "_sha1";
            return g2;
        } else if (str2.equals("{aid_md5}")) {
            String h3 = e.h(e.c(this.k));
            if (!TextUtils.isEmpty(this.i)) {
                return h3;
            }
            this.i = str + "_md5";
            this.f3874g = "anid";
            return h3;
        } else if (str2.equals("{aid_sha1}")) {
            String g3 = e.g(e.c(this.k));
            if (!TextUtils.isEmpty(this.i)) {
                return g3;
            }
            this.i = str + "_sha1";
            this.f3874g = "anid";
            return g3;
        } else if (str2.equals("{null}")) {
            return "delete";
        } else {
            return str2;
        }
    }

    public String a(String str, String str2, String str3) {
        int indexOf;
        if (!TextUtils.isEmpty(str) && ((!TextUtils.isEmpty(str3) || !TextUtils.isEmpty(str2)) && (indexOf = str.indexOf(str2 + "=")) != -1)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str.substring(0, indexOf)).append(str2 + "=").append(str3);
            int indexOf2 = str.indexOf("&", indexOf);
            if (indexOf2 != -1) {
                sb.append(str.substring(indexOf2));
            }
            str = sb.toString();
            this.f3873f = true;
        }
        com.duapps.ad.base.a.c("ToolClickHandler", "replaceGaidUrl:" + str);
        return str;
    }

    public static boolean c(String str, String str2) {
        return str2.matches(str);
    }

    public c(Context context) {
        super(context);
        this.k = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duapps.ad.stats.c.a(com.duapps.ad.stats.e, boolean):void
     arg types: [com.duapps.ad.stats.e, int]
     candidates:
      com.duapps.ad.stats.c.a(com.duapps.ad.stats.c, java.lang.String):java.lang.String
      com.duapps.ad.stats.c.a(java.lang.String, com.duapps.ad.stats.e):java.lang.String
      com.duapps.ad.stats.c.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.duapps.ad.stats.c.a(java.lang.String, java.lang.String):java.lang.String
      com.duapps.ad.stats.c.a(com.duapps.ad.stats.e, java.lang.String):void
      com.duapps.ad.stats.d.a(com.duapps.ad.stats.e, java.lang.String):android.net.Uri
      com.duapps.ad.stats.d.a(com.duapps.ad.stats.d, android.widget.Toast):android.widget.Toast
      com.duapps.ad.stats.c.a(com.duapps.ad.stats.e, boolean):void */
    public void a(e eVar) {
        if (!e()) {
            com.duapps.ad.base.a.c("ToolClickHandler", "exg点击了广告");
            this.f3873f = false;
            this.i = "";
            this.f3874g = "";
            this.f3875h = "";
            this.f3902c = "";
            a(true);
            v.a().a(new Runnable() {
                public void run() {
                    String unused = c.this.f3871d = f.a(c.this.k);
                }
            });
            a(eVar, true);
        }
    }

    public void a(e eVar, boolean z) {
        this.f3901b = false;
        if (e.a(this.k, eVar.a())) {
            b(eVar);
            f();
            return;
        }
        if (z) {
            g.a(this.k, eVar);
        }
        if (!e.a(this.k)) {
            c(eVar);
            f();
        } else if (eVar.h()) {
            f(eVar, eVar.j());
        } else if (eVar.i()) {
            eVar.b(false);
            if (com.duapps.ad.base.a.a()) {
                com.duapps.ad.base.a.c("ToolClickHandler", "Clicked URL: " + eVar.j());
            }
            d(eVar);
        } else if (com.duapps.ad.base.a.a()) {
            com.duapps.ad.base.a.c("ToolClickHandler", "Unknown Open type: " + eVar.c());
        }
    }

    private void d(e eVar) {
        boolean a2 = e.a(this.k, "com.android.vending");
        if (com.duapps.ad.base.a.a()) {
            com.duapps.ad.base.a.c("ToolClickHandler", "Click with Play installed? " + a2);
        }
        if (a2) {
            String j2 = eVar.j();
            if (b(j2)) {
                eVar.b(true);
                if (!TextUtils.isEmpty(this.f3875h)) {
                    g.a(this.k, eVar, this.f3874g, this.f3875h, this.i);
                    g.a(this.k, eVar, this.f3873f ? "1" : "0", this.f3875h);
                }
                h(eVar, j2);
                f();
            } else if (eVar.d() > 0) {
                i a3 = j.a(this.k).a(j2);
                eVar.a(a3);
                if (1 == a3.f3556c) {
                    eVar.b(true);
                    if (!TextUtils.isEmpty(this.f3875h)) {
                        g.a(this.k, eVar, this.f3874g, this.f3875h, this.i);
                        g.a(this.k, eVar, this.f3873f ? "1" : "0", this.f3875h);
                    }
                    h(eVar, a3.f3557d);
                    f();
                } else if (eVar.g() == 1 || eVar.g() == 2) {
                    i a4 = k.a(this.k).a(eVar.a());
                    if (a4.f3556c == 1) {
                        eVar.b(true);
                        if (!TextUtils.isEmpty(this.f3875h)) {
                            g.a(this.k, eVar, this.f3874g, this.f3875h, this.i);
                            g.a(this.k, eVar, this.f3873f ? "1" : "0", this.f3875h);
                        }
                        h(eVar, a4.f3557d);
                        f();
                    } else if (a4.f3556c == 2 || a4.f3556c == 3) {
                        String str = "https://play.google.com/store/apps/details?id=" + eVar.f().f3667d;
                        com.duapps.ad.base.a.c("ToolClickHandler", eVar.f().f3666c + " parse result is " + a4.f3556c + " and start google play via url -->" + str);
                        if (!TextUtils.isEmpty(this.f3875h)) {
                            g.a(this.k, eVar, this.f3874g, this.f3875h, this.i);
                            g.a(this.k, eVar, this.f3873f ? "1" : "0", this.f3875h);
                        }
                        h(eVar, str);
                        f();
                    } else {
                        b(eVar, j2);
                    }
                } else if (a3.f3556c == 2 || a3.f3556c == 3) {
                    String str2 = "https://play.google.com/store/apps/details?id=" + eVar.f().f3667d;
                    com.duapps.ad.base.a.c("ToolClickHandler", eVar.f().f3666c + " parse result is " + a3.f3556c + " and start google play via url -->" + str2);
                    if (!TextUtils.isEmpty(this.f3875h)) {
                        g.a(this.k, eVar, this.f3874g, this.f3875h, this.i);
                        g.a(this.k, eVar, this.f3873f ? "1" : "0", this.f3875h);
                    }
                    h(eVar, str2);
                    f();
                } else {
                    a();
                    b(eVar, j2);
                }
            } else {
                a();
                b(eVar, j2);
            }
        } else {
            if (!TextUtils.isEmpty(this.f3875h)) {
                g.a(this.k, eVar, this.f3874g, this.f3875h, this.i);
                g.a(this.k, eVar, this.f3873f ? "1" : "0", this.f3875h);
            }
            g(eVar, eVar.j());
            f();
        }
    }

    /* access modifiers changed from: protected */
    public void b(final e eVar, final String str) {
        if (e.a()) {
            if (com.duapps.ad.base.a.a()) {
                com.duapps.ad.base.a.c("ToolClickHandler", "Newer OS, use WebView redirect.");
            }
            try {
                d(eVar, str);
            } catch (Throwable th) {
                v.b(new Runnable() {
                    public void run() {
                        c.this.c(eVar, str);
                    }
                });
            }
        } else {
            if (com.duapps.ad.base.a.a()) {
                com.duapps.ad.base.a.c("ToolClickHandler", "Older OS, use Http redirect.");
            }
            v.b(new Runnable() {
                public void run() {
                    c.this.c(eVar, str);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void c(e eVar, String str) {
        DefaultHttpClient d2 = d();
        b bVar = new b(eVar);
        this.m = bVar;
        d2.setRedirectHandler(bVar);
        if (com.duapps.ad.base.a.a()) {
            com.duapps.ad.base.a.c("ToolClickHandler", "[Http] Decode URL: " + str);
        }
        try {
            HttpGet httpGet = new HttpGet(str);
            HttpConnectionParams.setConnectionTimeout(httpGet.getParams(), io.fabric.sdk.android.services.common.a.DEFAULT_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpGet.getParams(), 4000);
            d2.execute(httpGet).getEntity();
        } catch (Exception e2) {
            Exception exc = e2;
            com.duapps.ad.base.a.b("ToolClickHandler", "[Http] Others error: ", exc);
            if (eVar.p() != 0) {
                g.a(this.k, eVar, eVar.p() > 0 ? 2 : 1, 0, exc.getClass().getSimpleName(), this.n);
            }
            StringBuilder sb = new StringBuilder();
            sb.append(exc.getMessage());
            for (StackTraceElement append : exc.getStackTrace()) {
                sb.append(",").append(append);
            }
            g.a(this.k, eVar, sb.toString());
            if (!eVar.n()) {
                b();
                i(eVar, str);
            }
            f();
            if (this.n < 1 && eVar.n()) {
                if (com.duapps.ad.base.a.a()) {
                    com.duapps.ad.base.a.c("ToolClickHandler", "Retry click url: " + str);
                }
                this.n++;
                c(eVar, str);
            }
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    @TargetApi(11)
    public void d(e eVar, String str) {
        if (this.l == null) {
            this.l = new WebView(this.k);
            WebSettings settings = this.l.getSettings();
            settings.setAllowContentAccess(true);
            settings.setJavaScriptEnabled(true);
            settings.setUserAgentString(h.f3541b);
            if (Build.VERSION.SDK_INT >= 11) {
                this.l.removeJavascriptInterface("searchBoxJavaBridge_");
                this.l.removeJavascriptInterface("accessibility");
                this.l.removeJavascriptInterface("accessibilityTraversal");
            }
        }
        this.l.stopLoading();
        C0050c cVar = new C0050c(eVar);
        this.m = cVar;
        this.l.setWebViewClient(cVar);
        if (com.duapps.ad.base.a.a()) {
            com.duapps.ad.base.a.c("ToolClickHandler", "[WebView] Decode URL: " + str);
        }
        HashMap hashMap = new HashMap();
        hashMap.put("X-Requested-With", "");
        this.l.loadUrl(str, hashMap);
    }

    public void e(final e eVar, final String str) {
        g.h(this.k, eVar);
        this.n = 0;
        v.b(new Runnable() {
            public void run() {
                c.this.c(eVar, str);
            }
        });
    }
}
