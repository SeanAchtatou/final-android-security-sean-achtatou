package com.duapps.ad.stats;

import android.content.Context;
import com.duapps.ad.base.l;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.internal.b.e;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

public class b {
    public static void a(String str, int i, Context context, int i2, long j, String str2, String str3, String str4) {
        if (4 <= l.j(context)) {
            try {
                JSONStringer endObject = new JSONStringer().object().key("key").value("re").key("sid").value((long) i2).key("co").value((long) i).key("tsi").value(j).key("stack").value(str4).key("channel").value(str).endObject();
                JSONStringer jSONStringer = new JSONStringer();
                jSONStringer.array();
                jSONStringer.value(new JSONObject(endObject.toString()));
                jSONStringer.endArray();
                ToolStatsCore.getInstance(context).a(str3, str2, jSONStringer.toString());
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void a(Context context, int i, int i2, long j) {
        if (3 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value("fbe").key("sid").value((long) i).key("co").value((long) i2).key("ts").value(System.currentTimeMillis()).key("tsi").value(j).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void a(Context context, String str, int i) {
        if (2 <= l.j(context)) {
            a(context, "fbgr", str, i);
        }
    }

    private static void a(Context context, String str, String str2, int i) {
        try {
            ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value(str).key("sid").value((long) i).key("st").value(str2).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
        } catch (JSONException e2) {
        }
    }

    public static void a(Context context, String str, int i, int i2, long j) {
        String str2 = "dle";
        if ("high".equals(str)) {
            str2 = "dlhe";
        }
        a(context, i, i2, j, str2);
    }

    public static void b(Context context, int i, int i2, long j) {
        a(context, i, i2, j, "ime");
    }

    public static void c(Context context, int i, int i2, long j) {
        a(context, i, i2, j, "ole");
    }

    private static void a(Context context, int i, int i2, long j, String str) {
        if (3 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value(str).key("sid").value((long) i).key("co").value((long) i2).key("ts").value(System.currentTimeMillis()).key("tsi").value(j).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void a(Context context, AdData adData, int i) {
        a(context, adData, i, "irc");
    }

    private static void a(Context context, AdData adData, int i, String str) {
        if (3 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value(str).key("id").value(adData.f3665b).key("sid").value((long) adData.y).key("co").value((long) i).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void b(Context context, String str, int i) {
        if (2 <= l.j(context)) {
            b(context, "dlgr", str, i);
        }
    }

    private static void b(Context context, String str, String str2, int i) {
        c(context, "dlgr", str2, i);
    }

    public static void c(Context context, String str, int i) {
        if (2 <= l.j(context)) {
            c(context, "dlhgr", str, i);
        }
    }

    public static void d(Context context, String str, int i) {
        c(context, "amgr", str, i);
    }

    public static void e(Context context, String str, int i) {
        c(context, "imgr", str, i);
    }

    public static void f(Context context, String str, int i) {
        c(context, "olgr", str, i);
    }

    private static void c(Context context, String str, String str2, int i) {
        if (2 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value(str).key("sid").value((long) i).key("st").value(str2).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void a(Context context, int i) {
        if (2 <= l.j(context)) {
            c(context, "dln", Integer.toString(0), i);
        }
    }

    public static void b(Context context, int i) {
        c(context, "imn", Integer.toString(0), i);
    }

    public static void c(Context context, int i) {
        c(context, "oln", Integer.toString(0), i);
    }

    public static void d(Context context, int i) {
        if (2 <= l.j(context)) {
            c(context, "dlhn", Integer.toString(0), i);
        }
    }

    public static void a(Context context, int i, long j) {
        if (3 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value("srce").key("co").value((long) i).key("ts").value(System.currentTimeMillis()).key("tsi").value(j).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void a(Context context, int i, String str) {
        if (3 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value("tfe").key("ts").value(System.currentTimeMillis()).key("result").value((long) i).key("pkg_name").value(e.a(str)).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void b(Context context, int i, String str) {
        if (4 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value("efe").key("ts").value(System.currentTimeMillis()).key("reason").value(str).key("type").value((long) i).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void a(Context context, String str, boolean z, String str2, boolean z2) {
        long j = 1;
        if (3 <= l.j(context)) {
            try {
                ToolStatsCore instance = ToolStatsCore.getInstance(context);
                JSONStringer key = new JSONStringer().object().key("key").value("tdm").key("ts").value(System.currentTimeMillis()).key("pkg_name").value(e.a(str)).key("co").value(z ? -1 : 1).key("dl_pkg").value(e.a(str2)).key("sco");
                if (!z2) {
                    j = -1;
                }
                instance.reportEvent("behavior", key.value(j).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void a(Context context, e eVar, String str, String str2) {
        if (4 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value("gp_url_info").key("ts").value(System.currentTimeMillis()).key("pkg_name").value(e.a(eVar.a())).key("logId").value(eVar.m()).key("id").value(eVar.b()).key("sid").value((long) eVar.k()).key("result_url").value(str).key("redirect_url").value(str2).key("directgp").value(eVar.o()).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void a(Context context, e eVar, String str) {
        if (1 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value("load_url_info").key("ts").value(System.currentTimeMillis()).key("pkg_name").value(e.a(eVar.a())).key("logId").value(eVar.m()).key("id").value(eVar.b()).key("sid").value((long) eVar.k()).key("ldUrl").value(str).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }
}
