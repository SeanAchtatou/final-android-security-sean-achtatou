package com.duapps.ad.stats;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import android.util.Base64;
import com.duapps.ad.base.a;
import com.duapps.ad.base.g;
import com.duapps.ad.base.l;
import com.duapps.ad.base.o;
import com.duapps.ad.base.s;
import com.duapps.ad.internal.b.e;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.CharArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;

public final class ToolStatsCore implements Handler.Callback {

    /* renamed from: b  reason: collision with root package name */
    private static ToolStatsCore f3861b;

    /* renamed from: c  reason: collision with root package name */
    private static String f3862c = "http://api.mobula.sdk.duapps.com/adunion/slot/getSrcPrio?";

    /* renamed from: d  reason: collision with root package name */
    private static String f3863d = "http://rts.mobula.sdk.duapps.com/orts/rp?";

    /* renamed from: e  reason: collision with root package name */
    private static String f3864e = "http://rts.mobula.sdk.duapps.com/orts/rpb?";

    /* renamed from: a  reason: collision with root package name */
    int f3865a = 0;

    /* renamed from: f  reason: collision with root package name */
    private Context f3866f;

    /* renamed from: g  reason: collision with root package name */
    private Handler f3867g;

    public static void a(String str) {
        if ("prod".equals(str)) {
            f3863d = "http://rts.mobula.sdk.duapps.com/orts/rp?";
            f3864e = "http://rts.mobula.sdk.duapps.com/orts/rpb?";
            f3862c = "http://api.mobula.sdk.duapps.com/adunion/slot/getSrcPrio?";
        } else if ("dev".equals(str) || "test".equals(str)) {
            f3863d = "http://sandbox.duapps.com:8124/orts/rp?";
            f3864e = "http://sandbox.duapps.com:8124/orts/rpb?";
            f3862c = "http://sandbox.duapps.com:8124/adunion/slot/getSrcPrio?";
        }
    }

    public static synchronized void a(Context context) {
        synchronized (ToolStatsCore.class) {
            if (f3861b == null) {
                f3861b = new ToolStatsCore(context.getApplicationContext());
            }
        }
    }

    public static ToolStatsCore getInstance(Context context) {
        a(context);
        return f3861b;
    }

    private ToolStatsCore(Context context) {
        this.f3866f = context;
        HandlerThread handlerThread = new HandlerThread("stts", 10);
        handlerThread.start();
        this.f3867g = new Handler(handlerThread.getLooper(), this);
        this.f3867g.sendEmptyMessage(4);
    }

    public void reportEvent(String str, String str2, int i) {
        String b2 = o.b(this.f3866f);
        if (!TextUtils.isEmpty(b2) && !TextUtils.isEmpty(str2)) {
            if (a.a()) {
                a.c("ToolStatsCore", "rept = " + str2);
            }
            ContentValues contentValues = new ContentValues(3);
            contentValues.put("ts", Long.valueOf(System.currentTimeMillis()));
            contentValues.put(FirebaseAnalytics.Param.CONTENT, Base64.encode(str2.getBytes(), 3));
            contentValues.put("ls", b2);
            contentValues.put("stype", str);
            try {
                this.f3866f.getContentResolver().insert(DuAdCacheProvider.a(this.f3866f, 4), contentValues);
            } catch (Exception e2) {
                a.a("ToolStatsCore", "mDatabase reportEvent() exception: ", e2);
            } catch (Throwable th) {
                a.a("ToolStatsCore", "mDatabase reportEvent() exception: ", th);
            }
            if (i == 0) {
                this.f3867g.sendEmptyMessageDelayed(3, 5000);
            }
        } else if (a.a()) {
            a.c("ToolStatsCore", "Discard ls=" + b2 + ";rp=" + str2);
        }
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 3:
                this.f3867g.removeMessages(3);
                a(message);
                this.f3865a++;
                if (3 > this.f3865a) {
                    this.f3867g.sendEmptyMessage(3);
                    return true;
                }
                this.f3865a = 0;
                return true;
            case 4:
                this.f3867g.removeMessages(4);
                this.f3867g.sendEmptyMessage(3);
                this.f3867g.sendEmptyMessageDelayed(4, 3600000);
                return true;
            default:
                return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e0 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:83:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.os.Message r19) {
        /*
            r18 = this;
            long r2 = java.lang.System.currentTimeMillis()
            r12 = 0
            r11 = 0
            r10 = 0
            r9 = 0
            org.json.JSONStringer r13 = new org.json.JSONStringer
            r13.<init>()
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r4 = 604800000(0x240c8400, double:2.988109026E-315)
            long r16 = r2 - r4
            java.lang.String r5 = "ts > ?"
            r2 = 1
            java.lang.String[] r6 = new java.lang.String[r2]
            r2 = 0
            java.lang.String r3 = java.lang.String.valueOf(r16)
            r6[r2] = r3
            r2 = 5
            java.lang.String[] r4 = new java.lang.String[r2]
            r2 = 0
            java.lang.String r3 = "ts"
            r4[r2] = r3
            r2 = 1
            java.lang.String r3 = "content"
            r4[r2] = r3
            r2 = 2
            java.lang.String r3 = "ls"
            r4[r2] = r3
            r2 = 3
            java.lang.String r3 = "stype"
            r4[r2] = r3
            r2 = 4
            java.lang.String r3 = "_id"
            r4[r2] = r3
            java.lang.String r2 = "ts DESC"
            java.lang.String r2 = " LIMIT 100 OFFSET 0"
            r8 = 0
            r0 = r18
            android.content.Context r2 = r0.f3866f     // Catch:{ Exception -> 0x0188, all -> 0x01a4 }
            android.content.ContentResolver r2 = r2.getContentResolver()     // Catch:{ Exception -> 0x0188, all -> 0x01a4 }
            r0 = r18
            android.content.Context r3 = r0.f3866f     // Catch:{ Exception -> 0x0188, all -> 0x01a4 }
            r7 = 4
            android.net.Uri r3 = com.duapps.ad.stats.DuAdCacheProvider.a(r3, r7)     // Catch:{ Exception -> 0x0188, all -> 0x01a4 }
            java.lang.String r7 = "ts DESC LIMIT 100 OFFSET 0"
            android.database.Cursor r3 = r2.query(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0188, all -> 0x01a4 }
            if (r3 != 0) goto L_0x0069
            if (r3 == 0) goto L_0x0068
            boolean r2 = r3.isClosed()
            if (r2 != 0) goto L_0x0068
            r3.close()
        L_0x0068:
            return
        L_0x0069:
            int r2 = r3.getCount()     // Catch:{ Exception -> 0x01be }
            r4 = 1
            if (r2 >= r4) goto L_0x007f
            r3.close()     // Catch:{ Exception -> 0x01be }
            if (r3 == 0) goto L_0x0068
            boolean r2 = r3.isClosed()
            if (r2 != 0) goto L_0x0068
            r3.close()
            goto L_0x0068
        L_0x007f:
            r13.array()     // Catch:{ Exception -> 0x01be }
            r4 = r9
            r5 = r10
            r6 = r11
        L_0x0085:
            boolean r2 = r3.moveToNext()     // Catch:{ Exception -> 0x01c3 }
            if (r2 == 0) goto L_0x00ce
            r2 = 0
            long r8 = r3.getLong(r2)     // Catch:{ Exception -> 0x01c3 }
            r2 = 1
            byte[] r10 = r3.getBlob(r2)     // Catch:{ Exception -> 0x01c3 }
            r2 = 2
            java.lang.String r2 = r3.getString(r2)     // Catch:{ Exception -> 0x01c3 }
            r7 = 3
            java.lang.String r7 = r3.getString(r7)     // Catch:{ Exception -> 0x01c3 }
            if (r5 != 0) goto L_0x00a2
            r5 = r2
        L_0x00a2:
            if (r4 != 0) goto L_0x00a5
            r4 = r7
        L_0x00a5:
            boolean r2 = r5.equals(r2)     // Catch:{ Exception -> 0x01c3 }
            if (r2 == 0) goto L_0x0085
            boolean r2 = r4.equals(r7)     // Catch:{ Exception -> 0x01c3 }
            if (r2 == 0) goto L_0x0085
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x01c3 }
            r7 = 3
            byte[] r7 = android.util.Base64.decode(r10, r7)     // Catch:{ Exception -> 0x01c3 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x01c3 }
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Exception -> 0x01c3 }
            r7.<init>(r2)     // Catch:{ Exception -> 0x01c3 }
            r13.value(r7)     // Catch:{ Exception -> 0x01c3 }
            java.lang.StringBuilder r2 = r14.append(r8)     // Catch:{ Exception -> 0x01c3 }
            java.lang.String r7 = ","
            r2.append(r7)     // Catch:{ Exception -> 0x01c3 }
            r6 = 1
            goto L_0x0085
        L_0x00ce:
            r13.endArray()     // Catch:{ Exception -> 0x01c3 }
            if (r3 == 0) goto L_0x01c9
            boolean r2 = r3.isClosed()
            if (r2 != 0) goto L_0x01c9
            r3.close()
            r3 = r5
            r2 = r12
        L_0x00de:
            if (r6 == 0) goto L_0x0068
            if (r2 != 0) goto L_0x0068
            if (r3 == 0) goto L_0x0068
            if (r4 == 0) goto L_0x0068
            int r2 = r14.length()
            int r2 = r2 + -1
            r14.deleteCharAt(r2)
            java.lang.String r5 = r13.toString()
            r0 = r18
            android.content.Context r2 = r0.f3866f
            java.lang.String r6 = "seq"
            long r6 = com.duapps.ad.base.l.a(r2, r6)
            r2 = r18
            boolean r2 = r2.a(r3, r4, r5, r6)
            if (r2 == 0) goto L_0x0068
            java.lang.String r2 = "ts <= ? "
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]
            r4 = 0
            java.lang.String r5 = java.lang.String.valueOf(r16)
            r3[r4] = r5
            r0 = r18
            android.content.Context r4 = r0.f3866f     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            android.content.ContentResolver r4 = r4.getContentResolver()     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            r0 = r18
            android.content.Context r5 = r0.f3866f     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            r8 = 4
            android.net.Uri r5 = com.duapps.ad.stats.DuAdCacheProvider.a(r5, r8)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            r4.delete(r5, r2, r3)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            r2.<init>()     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.String r3 = "ts IN ("
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.String r3 = r14.toString()     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.String r3 = ")"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            r0 = r18
            android.content.Context r3 = r0.f3866f     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            android.content.ContentResolver r3 = r3.getContentResolver()     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            r0 = r18
            android.content.Context r4 = r0.f3866f     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            r5 = 4
            android.net.Uri r4 = com.duapps.ad.stats.DuAdCacheProvider.a(r4, r5)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            r5 = 0
            int r2 = r3.delete(r4, r2, r5)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.String r3 = "ToolStatsCore"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            r4.<init>()     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.String r5 = "del srecord success :"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            com.duapps.ad.base.a.c(r3, r2)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            r0 = r18
            android.content.Context r2 = r0.f3866f     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            java.lang.String r3 = "seq"
            r4 = 1
            long r4 = r4 + r6
            com.duapps.ad.base.l.a(r2, r3, r4)     // Catch:{ Exception -> 0x017e, Throwable -> 0x01b2 }
            goto L_0x0068
        L_0x017e:
            r2 = move-exception
            java.lang.String r3 = "ToolStatsCore"
            java.lang.String r4 = "mDatabase reportEvent() del exception: "
            com.duapps.ad.base.a.a(r3, r4, r2)
            goto L_0x0068
        L_0x0188:
            r2 = move-exception
            r3 = r8
            r4 = r9
            r5 = r10
            r6 = r11
        L_0x018d:
            r7 = 1
            java.lang.String r8 = "ToolStatsCore"
            java.lang.String r9 = "mDatabase reportEvent() exception: "
            com.duapps.ad.base.a.a(r8, r9, r2)     // Catch:{ all -> 0x01bc }
            if (r3 == 0) goto L_0x01c5
            boolean r2 = r3.isClosed()
            if (r2 != 0) goto L_0x01c5
            r3.close()
            r3 = r5
            r2 = r7
            goto L_0x00de
        L_0x01a4:
            r2 = move-exception
            r3 = r8
        L_0x01a6:
            if (r3 == 0) goto L_0x01b1
            boolean r4 = r3.isClosed()
            if (r4 != 0) goto L_0x01b1
            r3.close()
        L_0x01b1:
            throw r2
        L_0x01b2:
            r2 = move-exception
            java.lang.String r3 = "ToolStatsCore"
            java.lang.String r4 = "mDatabase reportEvent() del exception: "
            com.duapps.ad.base.a.a(r3, r4, r2)
            goto L_0x0068
        L_0x01bc:
            r2 = move-exception
            goto L_0x01a6
        L_0x01be:
            r2 = move-exception
            r4 = r9
            r5 = r10
            r6 = r11
            goto L_0x018d
        L_0x01c3:
            r2 = move-exception
            goto L_0x018d
        L_0x01c5:
            r3 = r5
            r2 = r7
            goto L_0x00de
        L_0x01c9:
            r3 = r5
            r2 = r12
            goto L_0x00de
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duapps.ad.stats.ToolStatsCore.a(android.os.Message):void");
    }

    public boolean a(String str, String str2, String str3) {
        HttpResponse httpResponse = null;
        long a2 = l.a(this.f3866f, "seq");
        List<NameValuePair> a3 = g.a(this.f3866f, str, false);
        String valueOf = String.valueOf(System.currentTimeMillis());
        a3.add(new BasicNameValuePair("mdu", "adsdk"));
        a3.add(new BasicNameValuePair("rv", "1.0"));
        a3.add(new BasicNameValuePair("ts", valueOf));
        a3.add(new BasicNameValuePair("seq", Long.toString(a2)));
        a3.add(new BasicNameValuePair("stype", str2));
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str3.getBytes());
            instance.update(valueOf.getBytes());
            instance.update(str.getBytes());
            a3.add(new BasicNameValuePair("s", e.a(instance.digest())));
            try {
                httpResponse = s.a(new URI(f3864e + URLEncodedUtils.format(a3, "UTF-8")), str3, (List<Header>) null);
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                a.c("ToolStatsCore", "status code = " + statusCode);
                if (statusCode == 200) {
                    l.a(this.f3866f, "seq", a2 + 1);
                    if (a.a()) {
                        a(httpResponse);
                    }
                    a.c("reportException", "report success!!!");
                    return true;
                }
                s.a(httpResponse);
                return false;
            } catch (Exception e2) {
                a.a("ToolStatsCore", "post failed.", e2);
                return false;
            } finally {
                s.a(httpResponse);
            }
        } catch (NoSuchAlgorithmException e3) {
            return false;
        }
    }

    private boolean a(String str, String str2, String str3, long j) {
        URI uri;
        HttpResponse httpResponse = null;
        List<NameValuePair> a2 = g.a(this.f3866f, str, false);
        String valueOf = String.valueOf(System.currentTimeMillis());
        a2.add(new BasicNameValuePair("mdu", "adsdk"));
        a2.add(new BasicNameValuePair("rv", "1.0"));
        a2.add(new BasicNameValuePair("ts", valueOf));
        a2.add(new BasicNameValuePair("seq", Long.toString(j)));
        a2.add(new BasicNameValuePair("stype", str2));
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str3.getBytes());
            instance.update(valueOf.getBytes());
            instance.update(str.getBytes());
            a2.add(new BasicNameValuePair("s", e.a(instance.digest())));
            String format = URLEncodedUtils.format(a2, "UTF-8");
            try {
                if (str2.equalsIgnoreCase("behavior")) {
                    uri = new URI(f3864e + format);
                } else {
                    uri = new URI(f3863d + format);
                }
                httpResponse = s.a(uri, str3, (List<Header>) null);
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                a.c("ToolStatsCore", "status code = " + statusCode);
                if (statusCode == 200) {
                    if (a.a()) {
                        a(httpResponse);
                    }
                    return true;
                }
                s.a(httpResponse);
                return false;
            } catch (Exception e2) {
                a.a("ToolStatsCore", "post failed.", e2);
                return false;
            } finally {
                s.a(httpResponse);
            }
        } catch (NoSuchAlgorithmException e3) {
            return false;
        }
    }

    private static void a(HttpResponse httpResponse) {
        InputStream inputStream;
        HttpEntity entity = httpResponse.getEntity();
        InputStream content = entity.getContent();
        Header contentEncoding = entity.getContentEncoding();
        if (contentEncoding == null || contentEncoding.getValue().indexOf("gzip") == -1) {
            inputStream = content;
        } else {
            inputStream = new GZIPInputStream(content);
        }
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
        CharArrayBuffer charArrayBuffer = new CharArrayBuffer((int) FileUtils.FileMode.MODE_ISGID);
        char[] cArr = new char[FileUtils.FileMode.MODE_ISGID];
        while (true) {
            int read = inputStreamReader.read(cArr);
            if (read == -1) {
                break;
            }
            charArrayBuffer.append(cArr, 0, read);
        }
        e.a(inputStream);
        String charArrayBuffer2 = charArrayBuffer.toString();
        a.c("ToolStatsCore", "result = " + charArrayBuffer2);
        try {
            JSONObject jSONObject = new JSONObject(charArrayBuffer2).getJSONObject("responseHeader");
            if (jSONObject.getInt("status") != 200) {
                throw new IOException(jSONObject.getString("msg"));
            }
        } catch (JSONException e2) {
        }
    }
}
