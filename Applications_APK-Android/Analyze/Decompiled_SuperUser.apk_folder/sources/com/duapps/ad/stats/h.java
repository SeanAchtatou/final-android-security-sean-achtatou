package com.duapps.ad.stats;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.duapps.ad.base.a;
import com.duapps.ad.base.i;
import com.duapps.ad.base.l;
import com.kingouser.com.entity.UninstallAppInfo;

public class h {

    /* renamed from: b  reason: collision with root package name */
    private static h f3916b;

    /* renamed from: a  reason: collision with root package name */
    private Context f3917a;

    public static synchronized h a(Context context) {
        h hVar;
        synchronized (h.class) {
            if (f3916b == null) {
                f3916b = new h(context.getApplicationContext());
            }
            hVar = f3916b;
        }
        return hVar;
    }

    private h(Context context) {
        this.f3917a = context;
    }

    public void a(i iVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("_url", iVar.f3554a);
        contentValues.put(UninstallAppInfo.COLUMN_PKG, iVar.f3555b);
        contentValues.put("p_url", iVar.f3557d);
        contentValues.put("type", Integer.valueOf(iVar.f3556c));
        contentValues.put("ts", Long.valueOf(iVar.f3558e));
        try {
            if (this.f3917a.getContentResolver().update(DuAdCacheProvider.a(this.f3917a, 1), contentValues, "_url = ?", new String[]{iVar.f3554a}) < 1) {
                this.f3917a.getContentResolver().insert(DuAdCacheProvider.a(this.f3917a, 1), contentValues);
            }
            b();
        } catch (Exception e2) {
            a.a("ToolboxCacheMgr", "saveParseResult() exception: ", e2);
        } catch (Throwable th) {
            a.a("ToolboxCacheMgr", "saveParseResult() exception: ", th);
        }
    }

    public i a(String str) {
        Cursor cursor;
        i iVar = new i();
        iVar.f3554a = str;
        iVar.f3556c = 0;
        try {
            cursor = this.f3917a.getContentResolver().query(DuAdCacheProvider.a(this.f3917a, 1), new String[]{"_url", UninstallAppInfo.COLUMN_PKG, "p_url", "type"}, "_url=? AND ts >?", new String[]{str, String.valueOf(System.currentTimeMillis() - 86400000)}, "ts DESC");
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        iVar.f3554a = cursor.getString(0);
                        iVar.f3555b = cursor.getString(1);
                        iVar.f3557d = cursor.getString(2);
                        iVar.f3556c = cursor.getInt(3);
                    }
                } catch (Exception e2) {
                    e = e2;
                    try {
                        a.a("ToolboxCacheMgr", "getParseResult() Exception: ", e);
                        if (cursor != null && !cursor.isClosed()) {
                            cursor.close();
                        }
                        return iVar;
                    } catch (Throwable th) {
                        th = th;
                        cursor.close();
                        throw th;
                    }
                }
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        } catch (Exception e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            throw th;
        }
        return iVar;
    }

    private void b() {
        try {
            this.f3917a.getContentResolver().delete(DuAdCacheProvider.a(this.f3917a, 1), "ts<?", new String[]{String.valueOf(System.currentTimeMillis() - 86400000)});
        } catch (Exception e2) {
            a.a("ToolboxCacheMgr", "dumpTimeOutDatas() exception: ", e2);
        } catch (Throwable th) {
            a.a("ToolboxCacheMgr", "dumpTimeOutDatas() exception: ", th);
        }
    }

    public int b(String str) {
        Cursor cursor;
        int i;
        try {
            cursor = this.f3917a.getContentResolver().query(DuAdCacheProvider.a(this.f3917a, 1), new String[]{"type"}, "_url=? AND ts >?", new String[]{str, String.valueOf(System.currentTimeMillis() - 86400000)}, "ts DESC");
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        i = cursor.getInt(0);
                        if (cursor == null && !cursor.isClosed()) {
                            cursor.close();
                            return i;
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    try {
                        a.a("ToolboxCacheMgr", "getParseResultType() exception: ", e);
                        if (cursor == null || cursor.isClosed()) {
                            return 0;
                        }
                        cursor.close();
                        return 0;
                    } catch (Throwable th) {
                        th = th;
                        cursor.close();
                        throw th;
                    }
                }
            }
            i = 0;
            return cursor == null ? i : i;
        } catch (Exception e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            throw th;
        }
    }

    public int c(String str) {
        Cursor cursor;
        int i;
        try {
            cursor = this.f3917a.getContentResolver().query(DuAdCacheProvider.a(this.f3917a, 7), new String[]{"type"}, "pkg=? AND ts >?", new String[]{str, String.valueOf(System.currentTimeMillis() - l.F(this.f3917a))}, "ts DESC");
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        i = cursor.getInt(0);
                        if (cursor == null && !cursor.isClosed()) {
                            cursor.close();
                            return i;
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    try {
                        a.a("ToolboxCacheMgr", "getParseResultType() exception: ", e);
                        if (cursor == null || cursor.isClosed()) {
                            return 0;
                        }
                        cursor.close();
                        return 0;
                    } catch (Throwable th) {
                        th = th;
                        cursor.close();
                        throw th;
                    }
                }
            }
            i = 0;
            return cursor == null ? i : i;
        } catch (Exception e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            throw th;
        }
    }

    public void a() {
        try {
            this.f3917a.getContentResolver().delete(DuAdCacheProvider.a(this.f3917a, 7), "ts<?", new String[]{String.valueOf(System.currentTimeMillis() - l.F(this.f3917a))});
        } catch (Exception e2) {
            a.a("ToolboxCacheMgr", "removePreparseCacheTimeOutDatas exception: ", e2);
        } catch (Throwable th) {
            a.a("ToolboxCacheMgr", "removePreparseCacheTimeOutDatas del exception: ", th);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:74:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(android.content.Context r10) {
        /*
            r9 = this;
            r6 = 0
            r7 = 0
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]
            java.lang.String r0 = "pkg"
            r2[r6] = r0
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            android.content.Context r0 = r9.f3917a     // Catch:{ Exception -> 0x0088, all -> 0x009d }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0088, all -> 0x009d }
            android.content.Context r1 = r9.f3917a     // Catch:{ Exception -> 0x0088, all -> 0x009d }
            r3 = 7
            android.net.Uri r1 = com.duapps.ad.stats.DuAdCacheProvider.a(r1, r3)     // Catch:{ Exception -> 0x0088, all -> 0x009d }
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0088, all -> 0x009d }
            if (r1 == 0) goto L_0x0038
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x013c }
            if (r0 == 0) goto L_0x0038
            r0 = 0
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x013c }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x013c }
            if (r2 != 0) goto L_0x0038
            r8.add(r0)     // Catch:{ Exception -> 0x013c }
        L_0x0038:
            if (r1 == 0) goto L_0x0043
            boolean r0 = r1.isClosed()
            if (r0 != 0) goto L_0x0043
            r1.close()
        L_0x0043:
            int r0 = r8.size()
            if (r0 <= 0) goto L_0x0126
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            android.content.pm.PackageManager r0 = r10.getPackageManager()
            if (r0 == 0) goto L_0x00ab
            r1 = 256(0x100, float:3.59E-43)
            java.util.List r2 = r0.getInstalledPackages(r1)
            java.util.Iterator r4 = r8.iterator()
        L_0x005e:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00ab
            java.lang.Object r0 = r4.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.Iterator r5 = r2.iterator()
        L_0x006e:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x005e
            java.lang.Object r1 = r5.next()
            android.content.pm.PackageInfo r1 = (android.content.pm.PackageInfo) r1
            if (r1 == 0) goto L_0x006e
            java.lang.String r1 = r1.packageName
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x006e
            r3.add(r0)
            goto L_0x006e
        L_0x0088:
            r0 = move-exception
            r1 = r7
        L_0x008a:
            java.lang.String r2 = "ToolboxCacheMgr"
            java.lang.String r3 = "getParseResult() Exception: "
            com.duapps.ad.base.a.a(r2, r3, r0)     // Catch:{ all -> 0x0139 }
            if (r1 == 0) goto L_0x0043
            boolean r0 = r1.isClosed()
            if (r0 != 0) goto L_0x0043
            r1.close()
            goto L_0x0043
        L_0x009d:
            r0 = move-exception
            r1 = r7
        L_0x009f:
            if (r1 == 0) goto L_0x00aa
            boolean r2 = r1.isClosed()
            if (r2 != 0) goto L_0x00aa
            r1.close()
        L_0x00aa:
            throw r0
        L_0x00ab:
            int r0 = r3.size()
            if (r0 <= 0) goto L_0x0126
            java.lang.String r0 = "pkg IN ("
            r1 = r6
            r2 = r0
        L_0x00b5:
            int r0 = r3.size()
            if (r1 >= r0) goto L_0x0102
            java.lang.Object r0 = r3.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            if (r1 != 0) goto L_0x00e4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = "'"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "'"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
        L_0x00e0:
            int r1 = r1 + 1
            r2 = r0
            goto L_0x00b5
        L_0x00e4:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = ",'"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "'"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x00e0
        L_0x0102:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = ")"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.content.Context r1 = r9.f3917a     // Catch:{ Exception -> 0x0127, Throwable -> 0x0130 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ Exception -> 0x0127, Throwable -> 0x0130 }
            android.content.Context r2 = r9.f3917a     // Catch:{ Exception -> 0x0127, Throwable -> 0x0130 }
            r3 = 7
            android.net.Uri r2 = com.duapps.ad.stats.DuAdCacheProvider.a(r2, r3)     // Catch:{ Exception -> 0x0127, Throwable -> 0x0130 }
            r3 = 0
            r1.delete(r2, r0, r3)     // Catch:{ Exception -> 0x0127, Throwable -> 0x0130 }
        L_0x0126:
            return
        L_0x0127:
            r0 = move-exception
            java.lang.String r1 = "ToolboxCacheMgr"
            java.lang.String r2 = "removePreparseCachePackageInstalledDatas exception: "
            com.duapps.ad.base.a.a(r1, r2, r0)
            goto L_0x0126
        L_0x0130:
            r0 = move-exception
            java.lang.String r1 = "ToolboxCacheMgr"
            java.lang.String r2 = "removePreparseCachePackageInstalledDatas del exception: "
            com.duapps.ad.base.a.a(r1, r2, r0)
            goto L_0x0126
        L_0x0139:
            r0 = move-exception
            goto L_0x009f
        L_0x013c:
            r0 = move-exception
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duapps.ad.stats.h.b(android.content.Context):void");
    }

    public i d(String str) {
        Cursor cursor;
        i iVar = new i();
        iVar.f3555b = str;
        iVar.f3556c = 0;
        try {
            cursor = this.f3917a.getContentResolver().query(DuAdCacheProvider.a(this.f3917a, 7), new String[]{"_url", UninstallAppInfo.COLUMN_PKG, "p_url", "type"}, "pkg=? AND ts >?", new String[]{str, String.valueOf(System.currentTimeMillis() - l.F(this.f3917a))}, "ts DESC");
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        iVar.f3554a = cursor.getString(0);
                        iVar.f3555b = cursor.getString(1);
                        iVar.f3557d = cursor.getString(2);
                        iVar.f3556c = cursor.getInt(3);
                    }
                } catch (Exception e2) {
                    e = e2;
                    try {
                        a.a("ToolboxCacheMgr", "getParseResult() Exception: ", e);
                        if (cursor != null && !cursor.isClosed()) {
                            cursor.close();
                        }
                        return iVar;
                    } catch (Throwable th) {
                        th = th;
                        cursor.close();
                        throw th;
                    }
                }
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        } catch (Exception e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            throw th;
        }
        return iVar;
    }

    public void b(i iVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("_url", iVar.f3554a);
        contentValues.put(UninstallAppInfo.COLUMN_PKG, iVar.f3555b);
        contentValues.put("p_url", iVar.f3557d);
        contentValues.put("type", Integer.valueOf(iVar.f3556c));
        contentValues.put("ts", Long.valueOf(iVar.f3558e));
        try {
            if (this.f3917a.getContentResolver().update(DuAdCacheProvider.a(this.f3917a, 7), contentValues, "_url = ?", new String[]{iVar.f3554a}) < 1) {
                this.f3917a.getContentResolver().insert(DuAdCacheProvider.a(this.f3917a, 7), contentValues);
            }
        } catch (Exception e2) {
            a.a("ToolboxCacheMgr", "saveParseResult() exception: ", e2);
        }
    }
}
