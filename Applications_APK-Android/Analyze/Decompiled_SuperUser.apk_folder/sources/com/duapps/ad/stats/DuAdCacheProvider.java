package com.duapps.ad.stats;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import com.duapps.ad.base.a;

public final class DuAdCacheProvider extends ContentProvider {

    /* renamed from: b  reason: collision with root package name */
    private static Uri f3853b;

    /* renamed from: c  reason: collision with root package name */
    private static Uri f3854c;

    /* renamed from: d  reason: collision with root package name */
    private static Uri f3855d;

    /* renamed from: e  reason: collision with root package name */
    private static Uri f3856e;

    /* renamed from: f  reason: collision with root package name */
    private static Uri f3857f;

    /* renamed from: g  reason: collision with root package name */
    private static Uri f3858g;

    /* renamed from: h  reason: collision with root package name */
    private static Uri f3859h;
    private static Uri i;
    private static final Object j = new Object();
    private static final Object l = new Object();
    private static final Object n = new Object();
    private static UriMatcher p;

    /* renamed from: a  reason: collision with root package name */
    private String f3860a = DuAdCacheProvider.class.getSimpleName();
    private i k;
    private j m;
    private f o;
    private Context q;

    public boolean onCreate() {
        this.q = getContext();
        a(this.q);
        return true;
    }

    private void a(Context context) {
        p = a(context.getPackageName() + ".DuAdCacheProvider");
    }

    private static UriMatcher a(String str) {
        f3853b = Uri.parse("content://" + str);
        f3854c = Uri.withAppendedPath(f3853b, "parse");
        f3855d = Uri.withAppendedPath(f3853b, "click");
        f3856e = Uri.withAppendedPath(f3853b, "cache");
        f3857f = Uri.withAppendedPath(f3853b, "record");
        f3858g = Uri.withAppendedPath(f3853b, "preparse");
        f3859h = Uri.withAppendedPath(f3853b, "searchRecord");
        i = Uri.withAppendedPath(f3853b, "preparsecache");
        UriMatcher uriMatcher = new UriMatcher(-1);
        uriMatcher.addURI(str, "parse", 1);
        uriMatcher.addURI(str, "click", 2);
        uriMatcher.addURI(str, "cache", 3);
        uriMatcher.addURI(str, "record", 4);
        uriMatcher.addURI(str, "preparse", 5);
        uriMatcher.addURI(str, "searchRecord", 6);
        uriMatcher.addURI(str, "preparsecache", 7);
        return uriMatcher;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        Cursor cursor = null;
        int a2 = a(uri);
        if (a2 > 0 && a2 <= 7) {
            synchronized (b(a2)) {
                cursor = b(getContext(), a2).query(a(a2), strArr, str, strArr2, null, null, str2);
            }
        }
        return cursor;
    }

    public String getType(Uri uri) {
        switch (p.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/parse";
            case 2:
                return "vnd.android.cursor.dir/click";
            case 3:
                return "vnd.android.cursor.dir/cache";
            case 4:
                return "vnd.android.cursor.dir/record";
            case 5:
                return "vnd.android.cursor.dir/preparse";
            case 6:
                return "vnd.android.cursor.dir/searchRecord";
            case 7:
                return "vnd.android.cursor.dir/preparseCache";
            default:
                return "vnd.android.cursor.dir/unkown";
        }
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        int a2 = a(uri);
        if (a2 <= 0 || a2 > 7) {
            return null;
        }
        synchronized (b(a2)) {
            b(getContext(), a2).insert(a(a2), null, contentValues);
        }
        return uri;
    }

    public int delete(Uri uri, String str, String[] strArr) {
        a.c(this.f3860a, new StringBuilder().append("del selcetion  = ").append(str).append(" , selectionArgs = ").append(str).toString() != null ? str.toString() : null);
        int a2 = a(uri);
        int i2 = -1;
        if (a2 > 0 && a2 <= 7) {
            synchronized (b(a2)) {
                i2 = b(getContext(), a2).delete(a(a2), str, strArr);
            }
        }
        return i2;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        int a2 = a(uri);
        int i2 = -1;
        if (a2 > 0 && a2 <= 7) {
            synchronized (b(a2)) {
                i2 = b(getContext(), a2).update(a(a2), contentValues, str, strArr);
            }
        }
        return i2;
    }

    private int a(Uri uri) {
        if (uri == null || Uri.EMPTY == uri) {
            return -1;
        }
        int match = p.match(uri);
        a.c(this.f3860a, "match code = " + match);
        return match;
    }

    private SQLiteDatabase b(Context context, int i2) {
        switch (i2) {
            case 1:
            case 7:
                if (this.m == null) {
                    this.m = new j(context);
                }
                return this.m.getWritableDatabase();
            case 2:
            case 3:
            case 5:
                if (this.k == null) {
                    this.k = new i(context);
                }
                return this.k.getWritableDatabase();
            case 4:
                if (this.o == null) {
                    this.o = new f(context);
                }
                return this.o.getWritableDatabase();
            case 6:
            default:
                return null;
        }
    }

    private String a(int i2) {
        switch (i2) {
            case 1:
                return "ad_parse";
            case 2:
                return "tbvc";
            case 3:
                return "cache";
            case 4:
                return "srecord";
            case 5:
                return "appcache";
            case 6:
            default:
                return null;
            case 7:
                return "preparse_cache";
        }
    }

    private Object b(int i2) {
        switch (i2) {
            case 1:
            case 7:
                return l;
            case 2:
            case 3:
            case 5:
                return j;
            case 4:
                return n;
            case 6:
            default:
                return null;
        }
    }

    public static Uri a(Context context, int i2) {
        Uri uri = Uri.EMPTY;
        if (f3853b == null) {
            f3853b = Uri.parse("content://" + (context.getPackageName() + ".DuAdCacheProvider"));
        }
        if (f3854c == null) {
            f3854c = Uri.withAppendedPath(f3853b, "parse");
        }
        if (f3855d == null) {
            f3855d = Uri.withAppendedPath(f3853b, "click");
        }
        if (f3856e == null) {
            f3856e = Uri.withAppendedPath(f3853b, "cache");
        }
        if (f3857f == null) {
            f3857f = Uri.withAppendedPath(f3853b, "record");
        }
        if (f3858g == null) {
            f3858g = Uri.withAppendedPath(f3853b, "preparse");
        }
        if (f3859h == null) {
            f3859h = Uri.withAppendedPath(f3853b, "searchRecord");
        }
        if (i == null) {
            i = Uri.withAppendedPath(f3853b, "preparsecache");
        }
        switch (i2) {
            case 1:
                return f3854c;
            case 2:
                return f3855d;
            case 3:
                return f3856e;
            case 4:
                return f3857f;
            case 5:
                return f3858g;
            case 6:
                return f3859h;
            case 7:
                return i;
            default:
                return uri;
        }
    }
}
