package com.duapps.ad.stats;

import com.duapps.ad.base.i;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.inmobi.IMData;
import org.json.JSONObject;

public class e extends a {

    /* renamed from: d  reason: collision with root package name */
    public AdData f3909d;

    /* renamed from: e  reason: collision with root package name */
    public IMData f3910e;

    /* renamed from: f  reason: collision with root package name */
    private String f3911f;

    /* renamed from: g  reason: collision with root package name */
    private int f3912g;

    /* renamed from: h  reason: collision with root package name */
    private long f3913h;
    private int i;
    private String j;
    private int k;
    private i l;
    private String m;
    private int n;
    private boolean o;
    private boolean p;
    private int q;
    private int r;

    public e(AdData adData) {
        super(adData.x, adData.y, adData.z);
        this.f3909d = adData;
        this.f3913h = adData.f3665b;
        this.f3912g = adData.m;
        this.f3911f = adData.f3667d;
        this.i = adData.n;
        this.j = adData.i;
        this.k = adData.A;
        this.m = adData.w;
        this.n = adData.H;
        this.r = adData.f3664a;
    }

    public e(IMData iMData) {
        super(iMData.f3728a, iMData.f3729b, iMData.f3731d);
        this.f3913h = iMData.f3732e;
        this.f3912g = 0;
        this.f3911f = null;
        this.i = iMData.f3734g;
        this.j = iMData.r;
        this.k = iMData.f3735h;
        this.m = iMData.f3730c;
    }

    public String a() {
        return this.f3911f;
    }

    public long b() {
        return this.f3913h;
    }

    public int c() {
        return this.i;
    }

    public int d() {
        return this.k;
    }

    public void a(i iVar) {
        this.l = iVar;
    }

    public i e() {
        return this.l;
    }

    public AdData f() {
        return this.f3909d;
    }

    public int g() {
        return this.r;
    }

    public boolean h() {
        return this.i == 0;
    }

    public boolean i() {
        return this.i == 1;
    }

    public String j() {
        return this.j;
    }

    public int k() {
        return this.f3869b;
    }

    public String l() {
        return this.f3870c;
    }

    public String m() {
        return this.m;
    }

    public boolean n() {
        return this.o;
    }

    public void a(boolean z) {
        this.o = z;
    }

    public boolean o() {
        return this.p;
    }

    public void b(boolean z) {
        this.p = z;
    }

    public int p() {
        return this.q;
    }

    public void a(int i2) {
        this.q = i2;
    }

    public static JSONObject a(e eVar) {
        JSONObject jSONObject = new JSONObject();
        if (eVar.f3909d != null) {
            jSONObject.put("data", AdData.a(eVar.f3909d));
        } else if (eVar.f3910e != null) {
            jSONObject.put("data", IMData.a(eVar.f3910e));
        }
        return jSONObject;
    }

    public static e a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        JSONObject jSONObject2 = jSONObject.getJSONObject("data");
        if ("download".equals(jSONObject2.optString("channel"))) {
            return new e(AdData.a(jSONObject2));
        }
        if ("inmobi".equals(jSONObject2.optString("channel"))) {
            return new e(IMData.a(jSONObject2));
        }
        return null;
    }
}
