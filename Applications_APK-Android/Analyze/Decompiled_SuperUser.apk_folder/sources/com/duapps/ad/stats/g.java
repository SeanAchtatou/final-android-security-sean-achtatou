package com.duapps.ad.stats;

import android.content.Context;
import android.text.TextUtils;
import com.duapps.ad.base.DuAdNetwork;
import com.duapps.ad.base.a;
import com.duapps.ad.base.i;
import com.duapps.ad.base.l;
import com.duapps.ad.base.q;
import com.duapps.ad.base.s;
import com.duapps.ad.base.v;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.internal.b.e;
import java.net.URL;
import java.util.List;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONStringer;

public class g {
    public static void a(Context context, e eVar) {
        a(context, "tctc", eVar);
    }

    public static void b(Context context, e eVar) {
        a(context, "tcta", eVar);
    }

    public static void c(Context context, e eVar) {
        a(context, "tct", eVar);
    }

    public static void d(Context context, e eVar) {
        q.a(context).a(eVar);
        a(context, "tctb", eVar);
    }

    public static void e(Context context, e eVar) {
        q.a(context).a(eVar);
        a(context, "tctp", eVar);
    }

    public static void f(Context context, e eVar) {
        a(context, "thi", eVar);
    }

    public static void g(Context context, e eVar) {
        a(context, "tccu", eVar);
    }

    public static void h(Context context, e eVar) {
        i e2;
        if (1 <= l.j(context)) {
            ToolStatsCore instance = ToolStatsCore.getInstance(context);
            try {
                JSONStringer value = new JSONStringer().object().key("key").value("tcpp").key("ts").value(System.currentTimeMillis());
                value.key("logid").value(eVar.m());
                value.key("id").value(eVar.b());
                if (eVar.d() > 0 && (e2 = eVar.e()) != null) {
                    value.key("preclick").value(a(e2.f3556c));
                    value.key("adpkg").value(eVar.a());
                }
                if (eVar.l().equals("online")) {
                    value.key("adpkg").value(eVar.a());
                }
                value.key("sid").value((long) eVar.k());
                value.endObject();
                JSONStringer value2 = new JSONStringer().object().key("key").value("jm").key("end").value(e.a(value.toString()));
                value2.endObject();
                instance.reportEvent(eVar.f3870c, value2.toString(), 0);
            } catch (JSONException e3) {
                if (a.a()) {
                    a.a("ToolStatsHelper", "create report content failed.", e3);
                }
            }
        }
    }

    private static void a(Context context, String str, e eVar) {
        i e2;
        if (1 <= l.j(context)) {
            ToolStatsCore instance = ToolStatsCore.getInstance(context);
            try {
                JSONStringer value = new JSONStringer().object().key("key").value(str).key("ts").value(System.currentTimeMillis());
                value.key("logid").value(eVar.m());
                value.key("id").value(eVar.b());
                if (eVar.d() > 0 && (e2 = eVar.e()) != null) {
                    value.key("preclick").value(a(e2.f3556c));
                    value.key("adpkg").value(eVar.a());
                }
                if (eVar.l().equals("online")) {
                    value.key("adpkg").value(eVar.a());
                }
                String b2 = DuAdNetwork.b();
                if ("thi".equals(str) && b2 != null) {
                    value.key("referrer").value(b2);
                }
                value.key("sid").value((long) eVar.k());
                if (str.equals("tctp")) {
                    value.key("directgp").value(eVar.o());
                }
                value.endObject();
                instance.reportEvent(eVar.f3870c, value.toString(), 0);
            } catch (JSONException e3) {
                if (a.a()) {
                    a.a("ToolStatsHelper", "create report content failed.", e3);
                }
            }
        }
    }

    private static String a(int i) {
        if (i == 0) {
            return "none";
        }
        if (1 == i) {
            return "tctp";
        }
        if (2 == i) {
            return "tctb";
        }
        if (3 == i) {
            return "err";
        }
        return "";
    }

    public static void a(Context context, e eVar, String str) {
        if (4 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("key").value("ex").key("id").value(eVar.b()).key("ts").value(System.currentTimeMillis()).key("exm").value(str).endObject().toString(), 1);
            } catch (JSONException e2) {
                if (a.a()) {
                    a.a("ToolStatsHelper", "create report content failed.", e2);
                }
            }
        }
    }

    public static void a(Context context, String str, int i) {
        if (1 <= l.j(context)) {
            ToolStatsCore instance = ToolStatsCore.getInstance(context);
            try {
                JSONStringer value = new JSONStringer().object().key("key").value("thista").key("adpkg").value(e.a(str)).key("ac").value((long) i).key("ts").value(System.currentTimeMillis());
                value.endObject();
                instance.reportEvent("native", value.toString(), 0);
            } catch (JSONException e2) {
                if (a.a()) {
                    a.a("ToolStatsHelper", "create report content failed.", e2);
                }
            }
        }
    }

    public static void a(Context context, e eVar, long j, int i, String str, int i2) {
        if (1 <= l.j(context)) {
            ToolStatsCore instance = ToolStatsCore.getInstance(context);
            try {
                JSONStringer value = new JSONStringer().object().key("key").value("thist").key("id").value(eVar.b()).key("adpkg").value(e.a(eVar.a())).key("ac").value(j).key("retry").value((long) i2).key("ts").value(System.currentTimeMillis());
                if (j > 0) {
                    value.key("logid").value(eVar.m());
                    if (i > 0) {
                        value.key("sc").value((long) i);
                    }
                    if (!TextUtils.isEmpty(str)) {
                        value.key("st").value(str);
                    }
                }
                value.endObject();
                JSONStringer value2 = new JSONStringer().object().key("key").value("jm").key("end").value(e.a(value.toString()));
                value2.endObject();
                instance.reportEvent("native", value2.toString(), 0);
            } catch (JSONException e2) {
                if (a.a()) {
                    a.a("ToolStatsHelper", "create report content failed.", e2);
                }
            }
        }
    }

    public static void a(Context context, e eVar, long j, int i, int i2) {
        a(context, eVar, j, i, null, i2);
    }

    public static void a(Context context, e eVar, long j, int i) {
        a(context, eVar, j, -1, i);
    }

    public static void i(Context context, e eVar) {
        if (1 <= l.j(context)) {
            ToolStatsCore instance = ToolStatsCore.getInstance(context);
            try {
                JSONStringer value = new JSONStringer().object().key("key").value("show").key("sid").value((long) eVar.f3869b).key("logid").value(eVar.m()).key("ts").value(System.currentTimeMillis());
                value.key("ids").array().value(eVar.b()).endArray();
                if (eVar.l().equals("online")) {
                    value.key("adpkg").value(eVar.a());
                }
                value.endObject();
                instance.reportEvent(eVar.f3870c, value.toString(), 1);
            } catch (JSONException e2) {
                if (a.a()) {
                    a.a("ToolStatsHelper", "create report content failed.", e2);
                }
            }
        }
    }

    public static void a(Context context, int i) {
        if (1 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("facebook", new JSONStringer().object().key("key").value("show").key("sid").value((long) i).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
            } catch (Exception e2) {
            }
        }
    }

    public static void a(Context context, AdData adData, int i, int i2, long j) {
        if (1 <= l.j(context)) {
            try {
                ToolStatsCore instance = ToolStatsCore.getInstance(context);
                JSONStringer value = new JSONStringer().object().key("key").value("jm").key("end").value(e.a(new JSONStringer().object().key("key").value("pclick").key("id").value(adData.f3665b).key("logid").value(adData.w).key("sid").value((long) adData.y).key("ptype").value((long) i).key("loop").value((long) i2).key("tsi").value(j).endObject().toString()));
                value.endObject();
                instance.reportEvent("native", value.toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void a(Context context, int i, String str) {
        if (context != null && str != null && 3 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("behavior", new JSONStringer().object().key("sid").value(String.valueOf(i)).key("key").value("fid").key("fid").value(e.a(str)).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
            } catch (JSONException e2) {
                if (a.a()) {
                    a.a("ToolStatsHelper", "create report content failed", e2);
                }
            }
        }
    }

    public static void b(Context context, int i) {
        b(context, "admob", i);
    }

    private static void b(Context context, String str, int i) {
        if (1 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent(str, new JSONStringer().object().key("key").value("show").key("sid").value((long) i).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
            } catch (Exception e2) {
            }
        }
    }

    public static void b(Context context, AdData adData, int i, int i2, long j) {
        if (1 <= l.j(context)) {
            try {
                ToolStatsCore.getInstance(context).reportEvent("native", new JSONStringer().object().key("key").value("tts").key("id").value(adData.f3665b).key("logid").value(adData.w).key("sid").value((long) adData.y).key("ptype").value((long) i).key("loop").value((long) i2).key("tsi").value(j).key("ts").value(System.currentTimeMillis()).key("tts_t").value((long) adData.f3664a).endObject().toString(), 1);
            } catch (JSONException e2) {
            }
        }
    }

    public static void a(final AdData adData) {
        String[] strArr = adData.G;
        for (final String str : strArr) {
            v.a().a(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.duapps.ad.base.s.a(java.net.URL, java.util.List<org.apache.http.Header>, boolean):org.apache.http.HttpResponse
                 arg types: [java.net.URL, ?[OBJECT, ARRAY], int]
                 candidates:
                  com.duapps.ad.base.s.a(java.net.URI, java.lang.String, java.util.List<org.apache.http.Header>):org.apache.http.HttpResponse
                  com.duapps.ad.base.s.a(java.net.URL, com.duapps.ad.base.s$b, long):void
                  com.duapps.ad.base.s.a(java.net.URL, java.util.List<org.apache.http.Header>, boolean):org.apache.http.HttpResponse */
                public void run() {
                    try {
                        int statusCode = s.a(new URL(str), (List<Header>) null, true).getStatusLine().getStatusCode();
                        if (statusCode != 200) {
                            a.c("ToolStatsHelper", "click to " + adData.o + " failed!");
                        } else if (statusCode == 200) {
                            a.c("ToolStatsHelper", "click to " + adData.o + " success!");
                        }
                    } catch (Exception e2) {
                        a.c("ToolStatsHelper", "click to " + adData.o + " exception!");
                    }
                }
            });
        }
    }

    public static void a(Context context, e eVar, String str, String str2, String str3) {
        if (1 <= l.j(context)) {
            if (eVar == null) {
                a.c("ToolStatsHelper", "上报exg，data是空的，不上报");
                return;
            }
            ToolStatsCore instance = ToolStatsCore.getInstance(context);
            try {
                JSONStringer endObject = new JSONStringer().object().key("key").value("exg_gaid").key("logid").value(eVar.m()).key("device_id_type").value(str).key("hostname").value(str2).key("device_id_param").value(str3).key("ts").value(System.currentTimeMillis()).key("id").value(eVar.b()).endObject();
                a.c("ToolStatsHelper", "exg_gaid:" + endObject.toString());
                instance.reportEvent(eVar.f3870c, endObject.toString(), 1);
            } catch (JSONException e2) {
                if (a.a()) {
                    a.a("ToolStatsHelper", "reportExgGaidAnid failed.", e2);
                }
            }
        }
    }

    public static void a(Context context, e eVar, String str, String str2) {
        if (1 <= l.j(context)) {
            if (eVar == null) {
                a.c("ToolStatsHelper", "上报exg，data是空的_modify，不上报");
                return;
            }
            ToolStatsCore instance = ToolStatsCore.getInstance(context);
            try {
                JSONStringer endObject = new JSONStringer().object().key("key").value("exg_gaid").key("logid").value(eVar.m()).key("isModify").value(str).key("hostname").value(str2).key("ts").value(System.currentTimeMillis()).key("id").value(eVar.b()).endObject();
                a.c("ToolStatsHelper", "exg_ismodify:" + endObject.toString());
                instance.reportEvent(eVar.f3870c, endObject.toString(), 1);
            } catch (JSONException e2) {
                if (a.a()) {
                    a.a("ToolStatsHelper", "reportExgIsModify failed.", e2);
                }
            }
        }
    }
}
