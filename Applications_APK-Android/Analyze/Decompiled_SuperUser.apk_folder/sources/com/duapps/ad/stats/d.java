package com.duapps.ad.stats;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;
import com.duapps.ad.base.a;
import com.duapps.ad.base.h;
import com.duapps.ad.internal.b.e;
import java.util.Iterator;
import java.util.List;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

public abstract class d implements Handler.Callback {

    /* renamed from: d  reason: collision with root package name */
    private static DefaultHttpClient f3899d;

    /* renamed from: a  reason: collision with root package name */
    protected Handler f3900a;

    /* renamed from: b  reason: collision with root package name */
    protected volatile boolean f3901b;

    /* renamed from: c  reason: collision with root package name */
    protected String f3902c = "";
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public Context f3903e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public Toast f3904f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f3905g;

    /* renamed from: h  reason: collision with root package name */
    private com.duapps.ad.d f3906h;

    public d(Context context) {
        this.f3903e = context;
        if (Looper.getMainLooper() == Looper.myLooper()) {
            this.f3900a = new Handler(this);
        }
    }

    public boolean handleMessage(Message message) {
        return false;
    }

    public void a(com.duapps.ad.d dVar) {
        this.f3906h = dVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        com.duapps.ad.d dVar = this.f3906h;
        if (dVar != null) {
            dVar.a();
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        com.duapps.ad.d dVar = this.f3906h;
        if (dVar != null) {
            dVar.b();
        }
    }

    /* access modifiers changed from: protected */
    public void a(final String str) {
        this.f3900a.post(new Runnable() {
            public void run() {
                if (d.this.f3904f == null) {
                    Toast unused = d.this.f3904f = Toast.makeText(d.this.f3903e, str, 0);
                }
                d.this.f3904f.setText(str);
                d.this.f3904f.show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void b(e eVar) {
        if (a.a()) {
            a.c("BaseClickHandler", "Goto installed App: " + eVar.a());
        }
        g.b(this.f3903e, eVar);
        e.b(this.f3903e, eVar.a());
    }

    /* access modifiers changed from: protected */
    public void c(e eVar) {
        a(false);
        if (a.a()) {
            a.c("BaseClickHandler", "No network.");
        }
        c();
    }

    public void c() {
        a("Network Error.");
        a.c("BaseClickHandler", "Please check you network and try again.");
    }

    /* access modifiers changed from: protected */
    public void f(e eVar, String str) {
        if (a.a()) {
            a.c("BaseClickHandler", "An apk link.");
        }
        g(eVar, str);
    }

    /* access modifiers changed from: protected */
    public void g(e eVar, String str) {
        String str2;
        if (this.f3901b) {
            a.c("BaseClickHandler", "Has already report");
            return;
        }
        this.f3901b = true;
        b.a(this.f3903e, eVar, this.f3902c);
        if (str == null) {
            if (a.a()) {
                a.c("BaseClickHandler", "startBrowser: url is null");
            }
            g.c(this.f3903e, eVar);
            a.c("BaseClickHandler", "Please check you network and try again.");
            return;
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        intent.addFlags(268435456);
        PackageManager packageManager = this.f3903e.getPackageManager();
        ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 65536);
        if (resolveActivity == null) {
            if (a.a()) {
                a.c("BaseClickHandler", "Goto browser failed.");
            }
            a("No browser or Google Play installed");
            a.c("BaseClickHandler", "No browser or Google Play installed");
            g.c(this.f3903e, eVar);
            return;
        }
        a.c("BaseClickHandler", "defaultInfo.activityInfo.packageName : " + resolveActivity.activityInfo.packageName);
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
        Iterator<String> it = e.b(this.f3903e).iterator();
        loop0:
        while (true) {
            if (!it.hasNext()) {
                str2 = null;
                break;
            }
            str2 = it.next();
            Iterator<ResolveInfo> it2 = queryIntentActivities.iterator();
            while (true) {
                if (it2.hasNext()) {
                    ResolveInfo next = it2.next();
                    a.c("BaseClickHandler", "for loop browser : " + str2 + ", actInfo.packageName : " + next.activityInfo.packageName);
                    if (str2.equals(next.activityInfo.packageName)) {
                        break loop0;
                    }
                }
            }
        }
        if (str2 == null) {
            str2 = queryIntentActivities.get(0).activityInfo.packageName;
        }
        intent.setPackage(str2);
        if (a.a()) {
            a.c("BaseClickHandler", "Goto browser");
        }
        this.f3903e.startActivity(intent);
        g.d(this.f3903e, eVar);
        f();
    }

    private Uri a(e eVar, String str) {
        boolean z;
        boolean z2 = true;
        String a2 = eVar.a();
        Uri parse = Uri.parse(str);
        String queryParameter = parse.getQueryParameter("id");
        int indexOf = str.indexOf("id=");
        if (TextUtils.isEmpty(queryParameter) && indexOf <= 0) {
            StringBuilder sb = new StringBuilder(str);
            if (parse.getQueryParameterNames().size() <= 0) {
                sb.append("?").append("id=").append(a2);
            } else {
                if (str.lastIndexOf("?") > 0) {
                    z = true;
                } else {
                    z = false;
                }
                if (z) {
                    sb.append("id=").append(a2);
                } else {
                    sb.append(str.lastIndexOf("&") > 0 ? "id=" : "&id=").append(a2);
                }
            }
            parse = Uri.parse(sb.toString());
            if (a.a()) {
                StringBuilder append = new StringBuilder().append("Url contains & :");
                if (indexOf <= 0) {
                    z2 = false;
                }
                a.c("BaseClickHandler", append.append(z2).append(", direct url:").append(sb.toString()).toString());
            }
        } else if (!queryParameter.equals(a2)) {
            parse = Uri.parse(str.replaceAll("(id=[^&]*)", "id=" + a2));
            if (a.a()) {
                StringBuilder append2 = new StringBuilder().append("Url contains & :");
                if (indexOf <= 0) {
                    z2 = false;
                }
                a.c("BaseClickHandler", append2.append(z2).append(", direct url:").append(parse.toString()).toString());
                return parse;
            }
        }
        return parse;
    }

    /* access modifiers changed from: protected */
    public void h(e eVar, String str) {
        if (this.f3901b) {
            a.c("BaseClickHandler", "Has already report");
            return;
        }
        this.f3901b = true;
        b.a(this.f3903e, eVar, this.f3902c);
        Uri parse = Uri.parse(str);
        try {
            parse = a(eVar, str);
        } catch (Exception e2) {
            StringBuilder sb = new StringBuilder();
            sb.append(e2.getMessage());
            for (StackTraceElement append : e2.getStackTrace()) {
                sb.append(",").append(append);
            }
            g.a(this.f3903e, eVar, sb.toString());
        }
        b.a(this.f3903e, eVar, str, parse.toString());
        Intent intent = new Intent("android.intent.action.VIEW", parse);
        intent.setFlags(268435456);
        intent.setPackage("com.android.vending");
        try {
            if (a.a()) {
                a.c("BaseClickHandler", "Goto Play");
            }
            this.f3903e.startActivity(intent);
            g.e(this.f3903e, eVar);
        } catch (Exception e3) {
            if (a.a()) {
                a.a("BaseClickHandler", "Goto Play failed:", e3);
            }
            this.f3901b = false;
            g(eVar, str);
        }
    }

    public static boolean b(String str) {
        if (str == null || str.trim().length() == 0) {
            return false;
        }
        if (str.startsWith("http://market.android.com") || str.startsWith("https://market.android.com") || str.startsWith("https://play.google.com") || str.startsWith("http://play.google.com") || str.startsWith("market://")) {
            return true;
        }
        return false;
    }

    static synchronized DefaultHttpClient d() {
        DefaultHttpClient defaultHttpClient;
        synchronized (d.class) {
            if (f3899d != null) {
                defaultHttpClient = f3899d;
            } else {
                SchemeRegistry schemeRegistry = new SchemeRegistry();
                schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
                schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                defaultHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
                HttpConnectionParams.setConnectionTimeout(defaultHttpClient.getParams(), io.fabric.sdk.android.services.common.a.DEFAULT_TIMEOUT);
                HttpConnectionParams.setSoTimeout(defaultHttpClient.getParams(), 4000);
                defaultHttpClient.getParams().setIntParameter("http.protocol.max-redirects", 10);
                HttpClientParams.setCookiePolicy(defaultHttpClient.getParams(), "compatibility");
                HttpProtocolParams.setUserAgent(defaultHttpClient.getParams(), h.f3541b);
                defaultHttpClient.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(3, true));
                f3899d = defaultHttpClient;
            }
        }
        return defaultHttpClient;
    }

    public synchronized boolean e() {
        return this.f3905g;
    }

    public synchronized void a(boolean z) {
        this.f3905g = z;
    }

    public synchronized void f() {
        this.f3905g = false;
    }
}
