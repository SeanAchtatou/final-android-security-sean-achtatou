package com.duapps.ad.a;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import com.duapps.ad.entity.a.b;
import com.duapps.ad.internal.b.e;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class a extends b<com.duapps.ad.entity.a.a> implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3502a = a.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final List<c> f3503b = Collections.synchronizedList(new LinkedList());
    private NativeAdOptions.Builder n;
    private int o;
    private Handler p;
    private long q;
    private String r;

    public a(Context context, int i, long j, int i2, String str) {
        super(context, i, j);
        this.o = i2;
        this.r = str;
        f();
        HandlerThread handlerThread = new HandlerThread("adnative", 10);
        handlerThread.start();
        this.p = new Handler(handlerThread.getLooper(), this);
    }

    private void f() {
        this.n = new NativeAdOptions.Builder();
        this.n.setReturnUrlsForImageAssets(true);
        this.n.setImageOrientation(2);
    }

    public void a(boolean z) {
        super.a(z);
        com.duapps.ad.base.a.c(f3502a, "refresh request....!");
        if (!e.a(this.f3690h)) {
            com.duapps.ad.base.a.c(f3502a, "No Network!");
            return;
        }
        this.f3687e = true;
        this.p.obtainMessage(0).sendToTarget();
    }

    /* renamed from: a */
    public com.duapps.ad.entity.a.a d() {
        c cVar;
        c cVar2 = null;
        synchronized (this.f3503b) {
            while (this.f3503b.size() > 0 && ((cVar2 = this.f3503b.remove(0)) == null || !cVar2.a())) {
            }
            cVar = cVar2;
        }
        com.duapps.ad.stats.b.d(this.f3690h, cVar == null ? "FAIL" : "OK", this.i);
        return cVar;
    }

    public int b() {
        return this.o;
    }

    public int c() {
        int i;
        synchronized (this.f3503b) {
            Iterator<c> it = this.f3503b.iterator();
            i = 0;
            while (it.hasNext()) {
                c next = it.next();
                if (next == null || !next.a()) {
                    it.remove();
                } else {
                    i++;
                }
            }
        }
        return i;
    }

    public boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            this.p.removeMessages(0);
            if (this.f3686d) {
                com.duapps.ad.base.a.c(f3502a, "Refresh request failed: already refreshing");
                return true;
            }
            this.f3686d = true;
            int c2 = this.o - c();
            if (c2 > 0) {
                this.p.obtainMessage(2, c2, 0).sendToTarget();
                return true;
            }
            com.duapps.ad.base.a.c(f3502a, "Refresh request OK: green is full");
            this.f3686d = false;
            return true;
        } else if (i != 2) {
            return false;
        } else {
            int i2 = message.arg1;
            if (i2 > 0) {
                a(i2);
                return true;
            }
            this.f3686d = false;
            com.duapps.ad.base.a.c(f3502a, "Refresh result: DONE for geeen count");
            return true;
        }
    }

    private void a(final int i) {
        final c cVar = new c(this.f3690h, this.i);
        cVar.a(new d() {
        });
        AdLoader.Builder builder = new AdLoader.Builder(this.f3690h, this.r);
        builder.forAppInstallAd(new NativeAppInstallAd.OnAppInstallAdLoadedListener() {
        });
        builder.forContentAd(new NativeContentAd.OnContentAdLoadedListener() {
        });
        AdLoader build = builder.withAdListener(cVar).withNativeAdOptions(this.n.build()).build();
        if (build != null) {
            this.f3686d = true;
            com.duapps.ad.base.a.c(f3502a, "AdmobCacheManager start refresh ad!");
            build.loadAd(new AdRequest.Builder().build());
            this.q = SystemClock.elapsedRealtime();
        }
    }
}
