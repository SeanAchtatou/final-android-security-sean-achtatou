package com.duapps.ad.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.duapps.ad.d;
import com.duapps.ad.entity.a.a;
import com.duapps.ad.stats.g;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAdView;
import java.util.List;

public class c extends AdListener implements a {

    /* renamed from: a  reason: collision with root package name */
    private b f3512a;

    /* renamed from: b  reason: collision with root package name */
    private Context f3513b;

    /* renamed from: c  reason: collision with root package name */
    private int f3514c;

    /* renamed from: d  reason: collision with root package name */
    private d f3515d;

    /* renamed from: e  reason: collision with root package name */
    private long f3516e;

    public c(Context context, int i) {
        this.f3513b = context.getApplicationContext();
        this.f3514c = i;
    }

    private boolean n() {
        return this.f3512a != null;
    }

    public boolean a() {
        long currentTimeMillis = System.currentTimeMillis() - this.f3516e;
        return currentTimeMillis > 0 && currentTimeMillis < 3600000;
    }

    public void a(View view) {
        b(view);
    }

    public void a(View view, List<View> list) {
        b(view);
    }

    public void b() {
    }

    public void c() {
    }

    public String d() {
        if (!n()) {
            return null;
        }
        return this.f3512a.f();
    }

    public String e() {
        if (!n()) {
            return null;
        }
        return this.f3512a.g();
    }

    public String f() {
        if (!n()) {
            return null;
        }
        return this.f3512a.e();
    }

    public String g() {
        if (!n()) {
            return null;
        }
        return this.f3512a.d();
    }

    public String h() {
        if (!n()) {
            return null;
        }
        return this.f3512a.c();
    }

    public float i() {
        if (!n()) {
            return 0.0f;
        }
        return this.f3512a.h();
    }

    public int j() {
        if (!n()) {
            return -1;
        }
        if (this.f3512a.a()) {
            return 4;
        }
        return 5;
    }

    public void a(d dVar) {
    }

    public Object k() {
        return this.f3512a;
    }

    private void b(View view) {
        if (view != null) {
            if (n() && !(this.f3512a.f3511b == null && this.f3512a.f3510a == null)) {
                a(view, this.f3512a);
            }
            g.b(this.f3513b, this.f3514c);
        }
    }

    private void a(View view, b bVar) {
        if (view instanceof NativeAppInstallAdView) {
            ((NativeAppInstallAdView) view).setNativeAd(bVar.f3511b);
        } else if (view instanceof NativeContentAdView) {
            ((NativeContentAdView) view).setNativeAd(bVar.f3510a);
        } else if (view instanceof ViewGroup) {
            a(((ViewGroup) view).getChildAt(0), bVar);
        }
    }

    public void a(d dVar) {
        this.f3515d = dVar;
    }

    public String l() {
        return "admob";
    }

    public int m() {
        return -1;
    }
}
