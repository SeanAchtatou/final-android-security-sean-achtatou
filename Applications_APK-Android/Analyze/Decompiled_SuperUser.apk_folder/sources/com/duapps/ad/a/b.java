package com.duapps.ad.a;

import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import java.util.List;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public NativeContentAd f3510a;

    /* renamed from: b  reason: collision with root package name */
    public NativeAppInstallAd f3511b;

    public boolean a() {
        return this.f3511b != null;
    }

    public boolean b() {
        return this.f3510a != null;
    }

    public String c() {
        if (a()) {
            return this.f3511b.getHeadline().toString();
        }
        if (b()) {
            return this.f3510a.getHeadline().toString();
        }
        return null;
    }

    public String d() {
        if (a()) {
            return this.f3511b.getBody().toString();
        }
        if (b()) {
            return this.f3510a.getBody().toString();
        }
        return null;
    }

    public String e() {
        if (a()) {
            return this.f3511b.getCallToAction().toString();
        }
        if (b()) {
            return this.f3510a.getCallToAction().toString();
        }
        return null;
    }

    public String f() {
        List images;
        if (!a() || (images = this.f3511b.getImages()) == null || images.size() <= 0) {
            return null;
        }
        return ((NativeAd.Image) images.get(0)).getUri().toString();
    }

    public String g() {
        List images;
        if (a()) {
            NativeAd.Image icon = this.f3511b.getIcon();
            if (icon != null) {
                return icon.getUri().toString();
            }
            return null;
        } else if (!b() || (images = this.f3510a.getImages()) == null || images.size() <= 0) {
            return null;
        } else {
            return ((NativeAd.Image) images.get(0)).getUri().toString();
        }
    }

    public float h() {
        Double starRating;
        if (!a() || (starRating = this.f3511b.getStarRating()) == null) {
            return 4.5f;
        }
        return (float) (starRating.doubleValue() + 0.0d);
    }
}
