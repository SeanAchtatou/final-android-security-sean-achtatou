package com.duapps.ad;

import android.content.Context;
import android.util.SparseArray;

public class PullRequestController {

    /* renamed from: c  reason: collision with root package name */
    private static PullRequestController f3498c;

    /* renamed from: a  reason: collision with root package name */
    private SparseArray<IDuAdController> f3499a = new SparseArray<>();

    /* renamed from: b  reason: collision with root package name */
    private Context f3500b;

    private PullRequestController(Context context) {
        this.f3500b = context;
    }

    public static PullRequestController getInstance(Context context) {
        synchronized (PullRequestController.class) {
            if (f3498c == null) {
                f3498c = new PullRequestController(context.getApplicationContext());
            }
        }
        return f3498c;
    }

    public IDuAdController getPullController(int i, int i2) {
        return getPullController(i, i2, true);
    }

    public IDuAdController getPullController(int i, int i2, boolean z) {
        if (!z) {
            return new e(this.f3500b, i, i2);
        }
        synchronized (this.f3499a) {
            if (this.f3499a.indexOfKey(i) >= 0) {
                IDuAdController iDuAdController = this.f3499a.get(i);
                return iDuAdController;
            }
            e eVar = new e(this.f3500b, i, i2);
            this.f3499a.put(i, eVar);
            return eVar;
        }
    }

    public void clearCache() {
        synchronized (this.f3499a) {
            int size = this.f3499a.size();
            while (size > 0) {
                int i = size - 1;
                IDuAdController valueAt = this.f3499a.valueAt(i);
                valueAt.clearCache();
                valueAt.destroy();
                size = i;
            }
            this.f3499a.clear();
        }
    }

    public void remove(int i) {
        synchronized (this.f3499a) {
            this.f3499a.remove(i);
        }
    }
}
