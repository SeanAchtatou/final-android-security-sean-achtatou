package com.duapps.ad.b;

import android.content.Context;
import com.duapps.ad.AdError;
import com.duapps.ad.base.j;
import com.duapps.ad.base.t;
import com.duapps.ad.base.u;
import com.duapps.ad.entity.AdData;
import com.duapps.ad.entity.AdModel;
import com.duapps.ad.entity.a.b;
import com.duapps.ad.entity.c;
import com.duapps.ad.internal.b.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class a extends b<com.duapps.ad.entity.a.a> {
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final String f3517b = a.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    u<AdModel> f3518a = new u<AdModel>() {
        public void a() {
            com.duapps.ad.base.a.a(a.f3517b, "start load cache data--");
            a.this.f3686d = true;
            a.this.f3687e = true;
        }

        public void a(int i, AdModel adModel) {
            int i2 = 0;
            if (i == 200 && adModel != null) {
                a.this.f3686d = false;
                List a2 = j.a(a.this.f3690h, a.this.a(adModel.f3683h));
                if (a2.size() <= 0) {
                    com.duapps.ad.stats.b.d(a.this.f3690h, a.this.i);
                    return;
                }
                synchronized (a.this.n) {
                    a.this.n.clear();
                    while (i2 < a2.size() && i2 < 5) {
                        a.this.n.add(a2.get(i2));
                        i2++;
                    }
                    com.duapps.ad.base.a.a(a.f3517b, "store data into cache list -- list.size = " + a.this.n.size());
                }
            }
        }

        public void a(int i, String str) {
            com.duapps.ad.base.a.a(a.f3517b, "fail to get cache -" + str);
            a.this.f3685c = true;
            a.this.f3686d = false;
            if (!a.this.k && a.this.m != null) {
                a.this.m.a(new AdError(i, str));
            }
        }
    };
    /* access modifiers changed from: private */
    public final List<AdData> n = Collections.synchronizedList(new LinkedList());

    public a(Context context, int i, long j) {
        super(context, i, j);
    }

    /* renamed from: a */
    public c d() {
        AdData adData;
        synchronized (this.n) {
            AdData adData2 = null;
            while (this.n.size() > 0 && ((adData2 = this.n.remove(0)) == null || !adData2.a())) {
            }
            adData = adData2;
            com.duapps.ad.base.a.c(f3517b, "DLH poll title-> " + (adData != null ? adData.f3666c : "null"));
        }
        com.duapps.ad.stats.b.c(this.f3690h, adData == null ? "FAIL" : "OK", this.i);
        if (adData == null) {
            return null;
        }
        return new c(this.f3690h, adData, this.m);
    }

    public int b() {
        return 1;
    }

    public void a(boolean z) {
        super.a(z);
        if (!e.a(this.f3690h)) {
            com.duapps.ad.base.a.c(f3517b, "no net");
        } else if (c() > 0) {
            com.duapps.ad.base.a.c(f3517b, "DLH validAdCount is" + c());
        } else if (this.f3686d) {
            com.duapps.ad.base.a.c(f3517b, "DLH is refreshing!");
        } else {
            t.a(this.f3690h).b(Integer.valueOf(this.i).intValue(), 1, this.f3518a);
        }
    }

    public int c() {
        int i;
        int i2 = 0;
        synchronized (this.n) {
            Iterator<AdData> it = this.n.iterator();
            while (it.hasNext()) {
                AdData next = it.next();
                if (next == null) {
                    it.remove();
                } else {
                    if (e.a(this.f3690h, next.f3667d) || !next.a()) {
                        it.remove();
                        i = i2;
                    } else {
                        i = i2 + 1;
                    }
                    i2 = i;
                }
            }
        }
        return i2;
    }

    /* access modifiers changed from: private */
    public List<AdData> a(List<AdData> list) {
        ArrayList arrayList = new ArrayList();
        for (AdData next : list) {
            if (!e.a(this.f3690h, next.f3667d)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public void e() {
        synchronized (this.n) {
            this.n.clear();
        }
    }
}
