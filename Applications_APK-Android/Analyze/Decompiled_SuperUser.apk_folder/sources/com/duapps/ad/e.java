package com.duapps.ad;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import com.duapps.ad.entity.a.a;
import com.duapps.ad.entity.a.b;
import com.duapps.ad.entity.a.d;
import com.duapps.ad.internal.a.c;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class e implements Handler.Callback, IDuAdController {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f3650a = e.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private Context f3651b;

    /* renamed from: c  reason: collision with root package name */
    private int f3652c;

    /* renamed from: d  reason: collision with root package name */
    private Handler f3653d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public b f3654e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f3655f;

    /* renamed from: g  reason: collision with root package name */
    private ConcurrentHashMap<String, b<a>> f3656g = new ConcurrentHashMap<>();

    /* renamed from: h  reason: collision with root package name */
    private List<String> f3657h = Collections.synchronizedList(new ArrayList());
    private List<String> i = Collections.synchronizedList(new ArrayList());
    private long j;
    private HandlerThread k;
    private volatile boolean l;
    /* access modifiers changed from: private */
    public volatile boolean m;
    private c n;
    private int o;
    private d p = new d() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duapps.ad.e.a(com.duapps.ad.e, boolean):boolean
         arg types: [com.duapps.ad.e, int]
         candidates:
          com.duapps.ad.e.a(int, com.duapps.ad.entity.a.b):void
          com.duapps.ad.e.a(java.lang.String, long):boolean
          com.duapps.ad.e.a(com.duapps.ad.e, boolean):boolean */
        public void a(final a aVar) {
            if (com.duapps.ad.internal.b.d.a()) {
                b a2 = e.this.f3654e;
                if (a2 != null) {
                    a2.a(aVar);
                }
                boolean unused = e.this.m = false;
                return;
            }
            com.duapps.ad.internal.b.d.a(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.duapps.ad.e.a(com.duapps.ad.e, boolean):boolean
                 arg types: [com.duapps.ad.e, int]
                 candidates:
                  com.duapps.ad.e.a(int, com.duapps.ad.entity.a.b):void
                  com.duapps.ad.e.a(java.lang.String, long):boolean
                  com.duapps.ad.e.a(com.duapps.ad.e, boolean):boolean */
                public void run() {
                    b a2 = e.this.f3654e;
                    if (a2 != null) {
                        a2.a(aVar);
                    }
                    boolean unused = e.this.m = false;
                }
            });
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duapps.ad.e.a(com.duapps.ad.e, boolean):boolean
         arg types: [com.duapps.ad.e, int]
         candidates:
          com.duapps.ad.e.a(int, com.duapps.ad.entity.a.b):void
          com.duapps.ad.e.a(java.lang.String, long):boolean
          com.duapps.ad.e.a(com.duapps.ad.e, boolean):boolean */
        public void a(b bVar, final AdError adError) {
            if (!com.duapps.ad.entity.a.c.f3692a.equals(bVar)) {
                int b2 = bVar.b() - bVar.c();
                if (b2 > 0) {
                    com.duapps.ad.base.a.c(e.f3650a, "channel-->" + bVar.toString() + " is filled error.");
                    e.this.a(b2, bVar);
                }
            } else if (com.duapps.ad.internal.b.d.a()) {
                b a2 = e.this.f3654e;
                if (a2 != null) {
                    a2.a(adError);
                }
                boolean unused = e.this.m = false;
            } else {
                com.duapps.ad.internal.b.d.a(new Runnable() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.duapps.ad.e.a(com.duapps.ad.e, boolean):boolean
                     arg types: [com.duapps.ad.e, int]
                     candidates:
                      com.duapps.ad.e.a(int, com.duapps.ad.entity.a.b):void
                      com.duapps.ad.e.a(java.lang.String, long):boolean
                      com.duapps.ad.e.a(com.duapps.ad.e, boolean):boolean */
                    public void run() {
                        b a2 = e.this.f3654e;
                        if (a2 != null) {
                            a2.a(adError);
                        }
                        boolean unused = e.this.m = false;
                    }
                });
            }
        }

        public void a(b bVar) {
            com.duapps.ad.internal.b.d.a(new Runnable() {
                public void run() {
                    b a2 = e.this.f3654e;
                    if (a2 != null) {
                        a2.a();
                    }
                }
            });
        }
    };

    public e(Context context, int i2, int i3) {
        this.f3651b = context;
        this.f3652c = i2;
        this.o = i3;
        a(i3);
    }

    public void a(int i2) {
        this.n = com.duapps.ad.internal.a.b.a(this.f3651b).a(this.f3652c, true);
        this.f3657h.addAll(com.duapps.ad.base.d.a(this.n.f3821b, this.f3651b, this.f3652c));
        this.j = com.duapps.ad.base.d.a(this.f3651b, this.f3652c, i2, this.f3657h, this.f3656g);
        d();
        this.k = new HandlerThread("adRequest", 10);
        this.k.start();
        this.f3653d = new Handler(this.k.getLooper(), this);
    }

    private void d() {
        synchronized (this.f3657h) {
            for (String next : this.f3657h) {
                if (c(next)) {
                    this.f3656g.get(next).a(this.p);
                }
            }
        }
    }

    public void fill() {
        b bVar;
        if (com.duapps.ad.internal.b.e.a(this.f3651b)) {
            this.f3655f = true;
            h();
            if (this.f3657h != null && this.f3657h.size() > 0) {
                String str = this.f3657h.get(0);
                b bVar2 = this.f3656g.get(str);
                if (bVar2 != null) {
                    bVar2.a(true);
                    a(str);
                }
                if (!str.equals("download") && this.f3657h.contains("download") && (bVar = this.f3656g.get("download")) != null) {
                    bVar.f3687e = true;
                    bVar.a(true);
                }
            }
        }
    }

    private void a(String str) {
        long a2 = this.n.a(str);
        Message obtainMessage = this.f3653d.obtainMessage();
        obtainMessage.what = 101;
        obtainMessage.obj = str;
        this.f3653d.sendMessageDelayed(obtainMessage, a2);
        com.duapps.ad.base.a.c(f3650a, "channelName:" + str + " send time out msg：" + a2);
    }

    public void a(b bVar) {
        this.f3654e = bVar;
    }

    /* access modifiers changed from: private */
    public void a(int i2, b bVar) {
        int i3;
        b bVar2;
        if (i2 > 0 && (i3 = bVar.l) != this.f3657h.size() - 1) {
            String str = this.f3657h.get(i3 + 1);
            if (!TextUtils.isEmpty(str) && (bVar2 = this.f3656g.get(str)) != null) {
                if (!bVar2.f3687e) {
                    bVar2.a(true);
                    a(str);
                    int b2 = i2 - bVar2.b();
                    com.duapps.ad.base.a.c(f3650a, "channel-->" + str + " start to fill." + "need count:" + b2);
                    if (b2 > 0) {
                        a(b2, bVar2);
                        return;
                    }
                    return;
                }
                a(i2, bVar2);
            }
        }
    }

    public int a() {
        int i2 = 0;
        Iterator<b<a>> it = this.f3656g.values().iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                return i3;
            }
            i2 = it.next().c() + i3;
        }
    }

    public a b() {
        a aVar;
        synchronized (this.f3657h) {
            Iterator<String> it = this.f3657h.iterator();
            while (true) {
                if (!it.hasNext()) {
                    aVar = null;
                    break;
                }
                b bVar = this.f3656g.get(it.next());
                if (bVar != null && bVar.c() > 0) {
                    aVar = (a) bVar.d();
                    break;
                }
            }
        }
        return aVar;
    }

    public void load() {
        if (!com.duapps.ad.internal.b.e.a(this.f3651b)) {
            this.p.a(com.duapps.ad.entity.a.c.f3692a, AdError.NETWORK_ERROR);
        } else if (this.m) {
            com.duapps.ad.base.a.c(f3650a, "Current task is already refreshing.");
        } else {
            this.l = false;
            this.m = true;
            if (!e()) {
                this.i.clear();
                g();
                this.f3653d.sendEmptyMessage(100);
            }
        }
    }

    private boolean e() {
        a aVar;
        if (!this.f3655f) {
            return false;
        }
        this.f3655f = false;
        synchronized (this.f3657h) {
            for (String next : this.f3657h) {
                if (c(next)) {
                    b bVar = this.f3656g.get(next);
                    if (bVar.c() > 0 && (aVar = (a) bVar.d()) != null) {
                        this.p.a(aVar);
                        com.duapps.ad.base.a.c(f3650a, "onAdLoaded in load method");
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 100:
                this.f3653d.removeMessages(100);
                try {
                    f();
                    return false;
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                    return false;
                }
            case 101:
                this.f3653d.removeMessages(101);
                String str = (String) message.obj;
                b bVar = this.f3656g.get(str);
                int b2 = bVar.b() - bVar.c();
                if (b2 <= 0) {
                    return false;
                }
                bVar.k = true;
                com.duapps.ad.base.a.c(f3650a, "channelName:" + str + " is time out");
                a(b2, bVar);
                return false;
            default:
                return false;
        }
    }

    private void f() {
        boolean z;
        a aVar;
        com.duapps.ad.base.a.c(f3650a, "scanResult");
        long elapsedRealtime = SystemClock.elapsedRealtime();
        for (boolean z2 = false; !z2 && !this.l; z2 = z) {
            synchronized (this.f3657h) {
                Iterator<String> it = this.f3657h.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        z = z2;
                        break;
                    }
                    String next = it.next();
                    if (this.l) {
                        this.p.a(com.duapps.ad.entity.a.c.f3692a, AdError.NO_FILL);
                        com.duapps.ad.base.a.c(f3650a, "Current action has been canceled~");
                        z = z2;
                        break;
                    }
                    this.f3657h.wait(10);
                    long elapsedRealtime2 = SystemClock.elapsedRealtime() - elapsedRealtime;
                    if (elapsedRealtime2 > this.j) {
                        this.p.a(com.duapps.ad.entity.a.c.f3692a, AdError.TIME_OUT_ERROR);
                        z = true;
                        break;
                    } else if (!this.i.contains(next) && (this.f3656g.containsKey(next) || this.f3656g.get(next) != null)) {
                        b bVar = this.f3656g.get(next);
                        if (bVar.f3685c) {
                            b(next);
                            if (!this.i.contains(next)) {
                                this.i.add(next);
                            }
                            if (this.i.size() == this.f3656g.keySet().size()) {
                                this.p.a(com.duapps.ad.entity.a.c.f3692a, AdError.NO_FILL);
                                z = true;
                                break;
                            }
                            com.duapps.ad.base.a.c(f3650a, "channel:" + next + " is error");
                        } else if (bVar.c() > 0) {
                            if (a(next, elapsedRealtime2) && (aVar = (a) bVar.d()) != null) {
                                this.p.a(aVar);
                                com.duapps.ad.base.a.c(f3650a, "onAdLoaded in load method");
                                z = true;
                                break;
                            }
                        } else if (!bVar.f3686d && !bVar.f3687e) {
                            bVar.a(false);
                            com.duapps.ad.base.a.c(f3650a, next + " is refreshing...");
                        }
                    }
                }
            }
        }
    }

    private boolean a(String str, long j2) {
        long j3 = this.f3656g.get(str).f3689g;
        com.duapps.ad.base.a.c(f3650a, "channel:" + str + "-->[" + j3 + "," + this.j + "]");
        return j2 > j3 && j2 < this.j;
    }

    private void b(String str) {
        int indexOf = this.f3657h.indexOf(str);
        int size = this.f3657h.size();
        if (indexOf != size - 1) {
            for (int i2 = size - 1; i2 > indexOf; i2--) {
                if (i2 - 1 >= 0) {
                    String str2 = this.f3657h.get(i2);
                    String str3 = this.f3657h.get(i2 - 1);
                    b bVar = this.f3656g.get(str2);
                    b bVar2 = this.f3656g.get(str3);
                    if (!(bVar == null || bVar2 == null)) {
                        bVar.f3689g = bVar2.f3689g;
                        com.duapps.ad.base.a.c(f3650a, "channel :" + str2 + ", used --> channel :" + str3);
                    }
                }
            }
        }
    }

    private void g() {
        long j2 = this.j;
        synchronized (this.f3657h) {
            for (int size = this.f3657h.size() - 1; size >= 0; size--) {
                String str = this.f3657h.get(size);
                b bVar = this.f3656g.get(str);
                bVar.f3685c = false;
                bVar.f3687e = false;
                if (size == 0) {
                    bVar.f3689g = 0;
                } else {
                    j2 -= bVar.f3688f;
                    bVar.f3689g = j2;
                }
                com.duapps.ad.base.a.c(f3650a, "channel:" + str + ",startTime:" + bVar.f3689g + ", wt:" + bVar.f3688f);
            }
        }
    }

    private void h() {
        synchronized (this.f3657h) {
            for (String next : this.f3657h) {
                if (c(next)) {
                    b bVar = this.f3656g.get(next);
                    bVar.k = false;
                    bVar.f3687e = false;
                }
            }
        }
    }

    public void clearCache() {
        synchronized (this.f3657h) {
            for (String next : this.f3657h) {
                if (c(next)) {
                    this.f3656g.get(next).e();
                }
            }
        }
    }

    private boolean c(String str) {
        return this.f3656g.containsKey(str) && this.f3656g.get(str) != null;
    }

    public void destroy() {
        this.m = false;
        this.l = true;
    }

    public void a(List<String> list) {
        b bVar;
        if (c("facebook") && (bVar = this.f3656g.get("facebook")) != null) {
            ((com.duapps.ad.entity.b) bVar).a(list);
        }
    }
}
