package com.duapps.ad;

import android.text.TextUtils;

public class AdError {
    public static final AdError INTERNAL_ERROR = new AdError(INTERNAL_ERROR_CODE, "Internal Error");
    public static final int INTERNAL_ERROR_CODE = 2001;
    public static final AdError LOAD_TOO_FREQUENTLY = new AdError(LOAD_TOO_FREQUENTLY_ERROR_CODE, "Ad was re-loaded too frequently");
    public static final int LOAD_TOO_FREQUENTLY_ERROR_CODE = 1002;
    public static final AdError NETWORK_ERROR = new AdError(NETWORK_ERROR_CODE, "Network Error");
    public static final int NETWORK_ERROR_CODE = 1000;
    public static final AdError NO_FILL = new AdError(NO_FILL_ERROR_CODE, "No Fill");
    public static final int NO_FILL_ERROR_CODE = 1001;
    public static final AdError SERVER_ERROR = new AdError(SERVER_ERROR_CODE, "Server Error");
    public static final int SERVER_ERROR_CODE = 2000;
    public static final int TIME_OUT_CODE = 3000;
    public static final AdError TIME_OUT_ERROR = new AdError(TIME_OUT_CODE, "Time Out");
    public static final AdError UNKNOW_ERROR = new AdError(UNKNOW_ERROR_CODE, "unknow error");
    public static final int UNKNOW_ERROR_CODE = 3001;
    private final int errorCode;
    private final String errorMessage;

    public AdError(int i, String str) {
        str = TextUtils.isEmpty(str) ? "unknown error" : str;
        this.errorCode = i;
        this.errorMessage = str;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public static AdError createAdError(int i, String str) {
        return new AdError(i, str);
    }
}
