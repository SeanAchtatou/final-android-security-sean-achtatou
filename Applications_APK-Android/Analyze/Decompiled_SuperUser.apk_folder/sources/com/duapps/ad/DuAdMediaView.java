package com.duapps.ad;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.duapps.ad.entity.a.a;
import com.facebook.ads.MediaView;
import com.facebook.ads.MediaViewListener;
import com.facebook.ads.NativeAd;

public class DuAdMediaView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3485a;

    /* renamed from: b  reason: collision with root package name */
    private MediaView f3486b;

    /* renamed from: c  reason: collision with root package name */
    private c f3487c;

    public DuAdMediaView(Context context) {
        super(context);
    }

    public DuAdMediaView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setNativeAd(a aVar) {
        a(aVar);
    }

    public void setNativeAd(DuNativeAd duNativeAd) {
        if (duNativeAd == null) {
            setVisibility(8);
        } else {
            a(duNativeAd.getRealSource());
        }
    }

    public void setListener(c cVar) {
        this.f3487c = cVar;
    }

    public void setAutoPlay(boolean z) {
        this.f3485a = z;
        if (this.f3486b != null) {
            this.f3486b.setAutoplay(z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.duapps.ad.DuAdMediaView.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.facebook.ads.MediaView, android.widget.RelativeLayout$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.duapps.ad.DuAdMediaView.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    private void a(a aVar) {
        if (aVar == null) {
            setVisibility(8);
            return;
        }
        Object k = aVar.k();
        if (k == null) {
            setVisibility(8);
        } else if (aVar.j() == 2) {
            NativeAd nativeAd = (NativeAd) k;
            if (this.f3486b == null) {
                this.f3486b = new MediaView(getContext());
                this.f3486b.setAutoplay(this.f3485a);
                this.f3486b.setFocusable(false);
                this.f3486b.setClickable(false);
                addView((View) this.f3486b, (ViewGroup.LayoutParams) new RelativeLayout.LayoutParams(-1, -1));
            }
            this.f3486b.setNativeAd(nativeAd);
            this.f3486b.setListener(new MediaViewListener() {
            });
        }
    }
}
