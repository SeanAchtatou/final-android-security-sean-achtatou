package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.IdManager;
import java.util.Map;
import java.util.UUID;

class SessionMetadataCollector {
    private final Context context;
    private final IdManager idManager;
    private final String versionCode;
    private final String versionName;

    public SessionMetadataCollector(Context context2, IdManager idManager2, String str, String str2) {
        this.context = context2;
        this.idManager = idManager2;
        this.versionCode = str;
        this.versionName = str2;
    }

    public SessionEventMetadata getMetadata() {
        Map<IdManager.DeviceIdentifierType, String> i = this.idManager.i();
        String m = CommonUtils.m(this.context);
        String d2 = this.idManager.d();
        String g2 = this.idManager.g();
        return new SessionEventMetadata(this.idManager.c(), UUID.randomUUID().toString(), this.idManager.b(), i.get(IdManager.DeviceIdentifierType.ANDROID_ID), i.get(IdManager.DeviceIdentifierType.ANDROID_ADVERTISING_ID), this.idManager.l(), i.get(IdManager.DeviceIdentifierType.FONT_TOKEN), m, d2, g2, this.versionCode, this.versionName);
    }
}
