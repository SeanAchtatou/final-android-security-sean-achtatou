package com.crashlytics.android.answers;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.b.f;
import io.fabric.sdk.android.services.common.a;
import io.fabric.sdk.android.services.common.l;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.c;
import java.io.File;
import java.util.List;

class SessionAnalyticsFilesSender extends a implements f {
    static final String FILE_CONTENT_TYPE = "application/vnd.crashlytics.android.events";
    static final String FILE_PARAM_NAME = "session_analytics_file_";
    private final String apiKey;

    public SessionAnalyticsFilesSender(io.fabric.sdk.android.f fVar, String str, String str2, c cVar, String str3) {
        super(fVar, str, str2, cVar, HttpMethod.POST);
        this.apiKey = str3;
    }

    public boolean send(List<File> list) {
        HttpRequest a2 = getHttpRequest().a(a.HEADER_CLIENT_TYPE, a.ANDROID_CLIENT_TYPE).a(a.HEADER_CLIENT_VERSION, this.kit.getVersion()).a(a.HEADER_API_KEY, this.apiKey);
        int i = 0;
        for (File next : list) {
            a2.a(FILE_PARAM_NAME + i, next.getName(), FILE_CONTENT_TYPE, next);
            i++;
        }
        Fabric.h().a(Answers.TAG, "Sending " + list.size() + " analytics files to " + getUrl());
        int b2 = a2.b();
        Fabric.h().a(Answers.TAG, "Response code for analytics file send is " + b2);
        if (l.a(b2) == 0) {
            return true;
        }
        return false;
    }
}
