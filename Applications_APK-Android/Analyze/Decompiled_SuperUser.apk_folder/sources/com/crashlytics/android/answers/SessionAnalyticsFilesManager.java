package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.services.b.b;
import io.fabric.sdk.android.services.b.c;
import io.fabric.sdk.android.services.common.g;
import java.util.UUID;

class SessionAnalyticsFilesManager extends b<SessionEvent> {
    private static final String SESSION_ANALYTICS_TO_SEND_FILE_EXTENSION = ".tap";
    private static final String SESSION_ANALYTICS_TO_SEND_FILE_PREFIX = "sa";
    private io.fabric.sdk.android.services.settings.b analyticsSettingsData;

    SessionAnalyticsFilesManager(Context context, SessionEventTransform sessionEventTransform, g gVar, c cVar) {
        super(context, sessionEventTransform, gVar, cVar, 100);
    }

    /* access modifiers changed from: protected */
    public String generateUniqueRollOverFileName() {
        return SESSION_ANALYTICS_TO_SEND_FILE_PREFIX + b.ROLL_OVER_FILE_NAME_SEPARATOR + UUID.randomUUID().toString() + b.ROLL_OVER_FILE_NAME_SEPARATOR + this.currentTimeProvider.a() + SESSION_ANALYTICS_TO_SEND_FILE_EXTENSION;
    }

    /* access modifiers changed from: protected */
    public int getMaxFilesToKeep() {
        return this.analyticsSettingsData == null ? super.getMaxFilesToKeep() : this.analyticsSettingsData.f7273e;
    }

    /* access modifiers changed from: protected */
    public int getMaxByteSizePerFile() {
        return this.analyticsSettingsData == null ? super.getMaxByteSizePerFile() : this.analyticsSettingsData.f7271c;
    }

    /* access modifiers changed from: package-private */
    public void setAnalyticsSettingsData(io.fabric.sdk.android.services.settings.b bVar) {
        this.analyticsSettingsData = bVar;
    }
}
