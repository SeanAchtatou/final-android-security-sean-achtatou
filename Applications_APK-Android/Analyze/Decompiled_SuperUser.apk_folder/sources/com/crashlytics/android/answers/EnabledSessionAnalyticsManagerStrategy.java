package com.crashlytics.android.answers;

import android.content.Context;
import com.crashlytics.android.answers.SessionEvent;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.b.f;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.d;
import io.fabric.sdk.android.services.network.c;
import io.fabric.sdk.android.services.settings.b;
import java.io.IOException;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

class EnabledSessionAnalyticsManagerStrategy implements SessionAnalyticsManagerStrategy {
    static final int UNDEFINED_ROLLOVER_INTERVAL_SECONDS = -1;
    d apiKey = new d();
    private final Context context;
    boolean customEventsEnabled = true;
    EventFilter eventFilter = new KeepAllEventFilter();
    private final ScheduledExecutorService executorService;
    private final SessionAnalyticsFilesManager filesManager;
    f filesSender;
    private final c httpRequestFactory;
    private final io.fabric.sdk.android.f kit;
    final SessionEventMetadata metadata;
    boolean predefinedEventsEnabled = true;
    private final AtomicReference<ScheduledFuture<?>> rolloverFutureRef = new AtomicReference<>();
    volatile int rolloverIntervalSeconds = -1;

    public EnabledSessionAnalyticsManagerStrategy(io.fabric.sdk.android.f fVar, Context context2, ScheduledExecutorService scheduledExecutorService, SessionAnalyticsFilesManager sessionAnalyticsFilesManager, c cVar, SessionEventMetadata sessionEventMetadata) {
        this.kit = fVar;
        this.context = context2;
        this.executorService = scheduledExecutorService;
        this.filesManager = sessionAnalyticsFilesManager;
        this.httpRequestFactory = cVar;
        this.metadata = sessionEventMetadata;
    }

    public void setAnalyticsSettingsData(b bVar, String str) {
        this.filesSender = AnswersRetryFilesSender.build(new SessionAnalyticsFilesSender(this.kit, str, bVar.f7269a, this.httpRequestFactory, this.apiKey.a(this.context)));
        this.filesManager.setAnalyticsSettingsData(bVar);
        this.customEventsEnabled = bVar.f7274f;
        Fabric.h().a(Answers.TAG, "Custom event tracking " + (this.customEventsEnabled ? "enabled" : "disabled"));
        this.predefinedEventsEnabled = bVar.f7275g;
        Fabric.h().a(Answers.TAG, "Predefined event tracking " + (this.predefinedEventsEnabled ? "enabled" : "disabled"));
        if (bVar.i > 1) {
            Fabric.h().a(Answers.TAG, "Event sampling enabled");
            this.eventFilter = new SamplingEventFilter(bVar.i);
        }
        this.rolloverIntervalSeconds = bVar.f7270b;
        scheduleTimeBasedFileRollOver(0, (long) this.rolloverIntervalSeconds);
    }

    public void processEvent(SessionEvent.Builder builder) {
        SessionEvent build = builder.build(this.metadata);
        if (!this.customEventsEnabled && SessionEvent.Type.CUSTOM.equals(build.type)) {
            Fabric.h().a(Answers.TAG, "Custom events tracking disabled - skipping event: " + build);
        } else if (!this.predefinedEventsEnabled && SessionEvent.Type.PREDEFINED.equals(build.type)) {
            Fabric.h().a(Answers.TAG, "Predefined events tracking disabled - skipping event: " + build);
        } else if (this.eventFilter.skipEvent(build)) {
            Fabric.h().a(Answers.TAG, "Skipping filtered event: " + build);
        } else {
            try {
                this.filesManager.writeEvent(build);
            } catch (IOException e2) {
                Fabric.h().e(Answers.TAG, "Failed to write event: " + build, e2);
            }
            scheduleTimeBasedRollOverIfNeeded();
        }
    }

    public void scheduleTimeBasedRollOverIfNeeded() {
        if (this.rolloverIntervalSeconds != -1) {
            scheduleTimeBasedFileRollOver((long) this.rolloverIntervalSeconds, (long) this.rolloverIntervalSeconds);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void sendEvents() {
        /*
            r9 = this;
            r1 = 0
            io.fabric.sdk.android.services.b.f r0 = r9.filesSender
            if (r0 != 0) goto L_0x000d
            android.content.Context r0 = r9.context
            java.lang.String r1 = "skipping files send because we don't yet know the target endpoint"
            io.fabric.sdk.android.services.common.CommonUtils.a(r0, r1)
        L_0x000c:
            return
        L_0x000d:
            android.content.Context r0 = r9.context
            java.lang.String r2 = "Sending all files"
            io.fabric.sdk.android.services.common.CommonUtils.a(r0, r2)
            com.crashlytics.android.answers.SessionAnalyticsFilesManager r0 = r9.filesManager
            java.util.List r0 = r0.getBatchOfFilesToSend()
            r2 = r0
            r0 = r1
        L_0x001c:
            int r1 = r2.size()     // Catch:{ Exception -> 0x0062 }
            if (r1 <= 0) goto L_0x0052
            android.content.Context r1 = r9.context     // Catch:{ Exception -> 0x0062 }
            java.util.Locale r3 = java.util.Locale.US     // Catch:{ Exception -> 0x0062 }
            java.lang.String r4 = "attempt to send batch of %d files"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0062 }
            r6 = 0
            int r7 = r2.size()     // Catch:{ Exception -> 0x0062 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0062 }
            r5[r6] = r7     // Catch:{ Exception -> 0x0062 }
            java.lang.String r3 = java.lang.String.format(r3, r4, r5)     // Catch:{ Exception -> 0x0062 }
            io.fabric.sdk.android.services.common.CommonUtils.a(r1, r3)     // Catch:{ Exception -> 0x0062 }
            io.fabric.sdk.android.services.b.f r1 = r9.filesSender     // Catch:{ Exception -> 0x0062 }
            boolean r3 = r1.send(r2)     // Catch:{ Exception -> 0x0062 }
            if (r3 == 0) goto L_0x0050
            int r1 = r2.size()     // Catch:{ Exception -> 0x0062 }
            int r1 = r1 + r0
            com.crashlytics.android.answers.SessionAnalyticsFilesManager r0 = r9.filesManager     // Catch:{ Exception -> 0x0084 }
            r0.deleteSentFiles(r2)     // Catch:{ Exception -> 0x0084 }
            r0 = r1
        L_0x0050:
            if (r3 != 0) goto L_0x005a
        L_0x0052:
            if (r0 != 0) goto L_0x000c
            com.crashlytics.android.answers.SessionAnalyticsFilesManager r0 = r9.filesManager
            r0.deleteOldestInRollOverIfOverMax()
            goto L_0x000c
        L_0x005a:
            com.crashlytics.android.answers.SessionAnalyticsFilesManager r1 = r9.filesManager     // Catch:{ Exception -> 0x0062 }
            java.util.List r1 = r1.getBatchOfFilesToSend()     // Catch:{ Exception -> 0x0062 }
            r2 = r1
            goto L_0x001c
        L_0x0062:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0066:
            android.content.Context r2 = r9.context
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to send batch of analytics files to server: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r0.getMessage()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            io.fabric.sdk.android.services.common.CommonUtils.a(r2, r3, r0)
            r0 = r1
            goto L_0x0052
        L_0x0084:
            r0 = move-exception
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crashlytics.android.answers.EnabledSessionAnalyticsManagerStrategy.sendEvents():void");
    }

    public void cancelTimeBasedFileRollOver() {
        if (this.rolloverFutureRef.get() != null) {
            CommonUtils.a(this.context, "Cancelling time-based rollover because no events are currently being generated.");
            this.rolloverFutureRef.get().cancel(false);
            this.rolloverFutureRef.set(null);
        }
    }

    public void deleteAllEvents() {
        this.filesManager.deleteAllEventsFiles();
    }

    public boolean rollFileOver() {
        try {
            return this.filesManager.rollFileOver();
        } catch (IOException e2) {
            CommonUtils.a(this.context, "Failed to roll file over.", e2);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void scheduleTimeBasedFileRollOver(long j, long j2) {
        if (this.rolloverFutureRef.get() == null) {
            i iVar = new i(this.context, this);
            CommonUtils.a(this.context, "Scheduling time based file roll over every " + j2 + " seconds");
            try {
                this.rolloverFutureRef.set(this.executorService.scheduleAtFixedRate(iVar, j, j2, TimeUnit.SECONDS));
            } catch (RejectedExecutionException e2) {
                CommonUtils.a(this.context, "Failed to schedule time based file roll over", e2);
            }
        }
    }
}
