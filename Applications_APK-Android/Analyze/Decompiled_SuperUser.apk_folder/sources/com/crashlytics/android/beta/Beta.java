package com.crashlytics.android.beta;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.f;
import io.fabric.sdk.android.services.a.b;
import io.fabric.sdk.android.services.c.d;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.common.h;
import io.fabric.sdk.android.services.common.m;
import io.fabric.sdk.android.services.settings.q;
import io.fabric.sdk.android.services.settings.s;
import java.util.HashMap;
import java.util.Map;

public class Beta extends f<Boolean> implements h {
    private static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
    private static final String CRASHLYTICS_BUILD_PROPERTIES = "crashlytics-build.properties";
    static final String NO_DEVICE_TOKEN = "";
    public static final String TAG = "Beta";
    private final b<String> deviceTokenCache = new b<>();
    private final DeviceTokenLoader deviceTokenLoader = new DeviceTokenLoader();
    private UpdatesController updatesController;

    public static Beta getInstance() {
        return (Beta) Fabric.a(Beta.class);
    }

    /* access modifiers changed from: protected */
    @TargetApi(14)
    public boolean onPreExecute() {
        this.updatesController = createUpdatesController(Build.VERSION.SDK_INT, (Application) getContext().getApplicationContext());
        return true;
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground() {
        Fabric.h().a(TAG, "Beta kit initializing...");
        Context context = getContext();
        IdManager idManager = getIdManager();
        if (TextUtils.isEmpty(getBetaDeviceToken(context, idManager.j()))) {
            Fabric.h().a(TAG, "A Beta device token was not found for this app");
            return false;
        }
        Fabric.h().a(TAG, "Beta device token is present, checking for app updates.");
        io.fabric.sdk.android.services.settings.f betaSettingsData = getBetaSettingsData();
        BuildProperties loadBuildProperties = loadBuildProperties(context);
        if (canCheckForUpdates(betaSettingsData, loadBuildProperties)) {
            this.updatesController.initialize(context, this, idManager, betaSettingsData, loadBuildProperties, new d(this), new m(), new io.fabric.sdk.android.services.network.b(Fabric.h()));
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    @TargetApi(14)
    public UpdatesController createUpdatesController(int i, Application application) {
        if (i >= 14) {
            return new ActivityLifecycleCheckForUpdatesController(getFabric().e(), getFabric().f());
        }
        return new ImmediateCheckForUpdatesController();
    }

    public Map<IdManager.DeviceIdentifierType, String> getDeviceIdentifiers() {
        String betaDeviceToken = getBetaDeviceToken(getContext(), getIdManager().j());
        HashMap hashMap = new HashMap();
        if (!TextUtils.isEmpty(betaDeviceToken)) {
            hashMap.put(IdManager.DeviceIdentifierType.FONT_TOKEN, betaDeviceToken);
        }
        return hashMap;
    }

    public String getIdentifier() {
        return "com.crashlytics.sdk.android:beta";
    }

    public String getVersion() {
        return "1.2.5.dev";
    }

    /* access modifiers changed from: package-private */
    public boolean canCheckForUpdates(io.fabric.sdk.android.services.settings.f fVar, BuildProperties buildProperties) {
        return (fVar == null || TextUtils.isEmpty(fVar.f7294a) || buildProperties == null) ? false : true;
    }

    private String getBetaDeviceToken(Context context, String str) {
        String str2;
        try {
            str2 = this.deviceTokenCache.a(context, this.deviceTokenLoader);
            if ("".equals(str2)) {
                str2 = null;
            }
        } catch (Exception e2) {
            Fabric.h().e(TAG, "Failed to load the Beta device token", e2);
            str2 = null;
        }
        Fabric.h().a(TAG, "Beta device token present: " + (!TextUtils.isEmpty(str2)));
        return str2;
    }

    private io.fabric.sdk.android.services.settings.f getBetaSettingsData() {
        s b2 = q.a().b();
        if (b2 != null) {
            return b2.f7336f;
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0088 A[SYNTHETIC, Splitter:B:27:0x0088] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.crashlytics.android.beta.BuildProperties loadBuildProperties(android.content.Context r8) {
        /*
            r7 = this;
            r1 = 0
            android.content.res.AssetManager r0 = r8.getAssets()     // Catch:{ Exception -> 0x0061, all -> 0x0084 }
            java.lang.String r2 = "crashlytics-build.properties"
            java.io.InputStream r2 = r0.open(r2)     // Catch:{ Exception -> 0x0061, all -> 0x0084 }
            if (r2 == 0) goto L_0x00a5
            com.crashlytics.android.beta.BuildProperties r1 = com.crashlytics.android.beta.BuildProperties.fromPropertiesStream(r2)     // Catch:{ Exception -> 0x009b }
            io.fabric.sdk.android.i r0 = io.fabric.sdk.android.Fabric.h()     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r3 = "Beta"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a0 }
            r4.<init>()     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r5 = r1.packageName     // Catch:{ Exception -> 0x00a0 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r5 = " build properties: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r5 = r1.versionName     // Catch:{ Exception -> 0x00a0 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r5 = " ("
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r5 = r1.versionCode     // Catch:{ Exception -> 0x00a0 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r5 = ") - "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r5 = r1.buildId     // Catch:{ Exception -> 0x00a0 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00a0 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00a0 }
            r0.a(r3, r4)     // Catch:{ Exception -> 0x00a0 }
            r0 = r1
        L_0x004e:
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0053:
            return r0
        L_0x0054:
            r1 = move-exception
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Beta"
            java.lang.String r4 = "Error closing Beta build properties asset"
            r2.e(r3, r4, r1)
            goto L_0x0053
        L_0x0061:
            r0 = move-exception
            r2 = r1
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0066:
            io.fabric.sdk.android.i r3 = io.fabric.sdk.android.Fabric.h()     // Catch:{ all -> 0x0099 }
            java.lang.String r4 = "Beta"
            java.lang.String r5 = "Error reading Beta build properties"
            r3.e(r4, r5, r1)     // Catch:{ all -> 0x0099 }
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ IOException -> 0x0077 }
            goto L_0x0053
        L_0x0077:
            r1 = move-exception
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Beta"
            java.lang.String r4 = "Error closing Beta build properties asset"
            r2.e(r3, r4, r1)
            goto L_0x0053
        L_0x0084:
            r0 = move-exception
            r2 = r1
        L_0x0086:
            if (r2 == 0) goto L_0x008b
            r2.close()     // Catch:{ IOException -> 0x008c }
        L_0x008b:
            throw r0
        L_0x008c:
            r1 = move-exception
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Beta"
            java.lang.String r4 = "Error closing Beta build properties asset"
            r2.e(r3, r4, r1)
            goto L_0x008b
        L_0x0099:
            r0 = move-exception
            goto L_0x0086
        L_0x009b:
            r0 = move-exception
            r6 = r0
            r0 = r1
            r1 = r6
            goto L_0x0066
        L_0x00a0:
            r0 = move-exception
            r6 = r0
            r0 = r1
            r1 = r6
            goto L_0x0066
        L_0x00a5:
            r0 = r1
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crashlytics.android.beta.Beta.loadBuildProperties(android.content.Context):com.crashlytics.android.beta.BuildProperties");
    }

    /* access modifiers changed from: package-private */
    public String getOverridenSpiEndpoint() {
        return CommonUtils.b(getContext(), CRASHLYTICS_API_ENDPOINT);
    }
}
