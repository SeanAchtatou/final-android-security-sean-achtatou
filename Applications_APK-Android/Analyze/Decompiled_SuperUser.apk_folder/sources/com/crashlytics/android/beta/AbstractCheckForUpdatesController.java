package com.crashlytics.android.beta;

import android.annotation.SuppressLint;
import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.common.d;
import io.fabric.sdk.android.services.common.g;
import io.fabric.sdk.android.services.network.c;
import io.fabric.sdk.android.services.settings.f;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class AbstractCheckForUpdatesController implements UpdatesController {
    static final long LAST_UPDATE_CHECK_DEFAULT = 0;
    static final String LAST_UPDATE_CHECK_KEY = "last_update_check";
    private static final long MILLIS_PER_SECOND = 1000;
    private Beta beta;
    private f betaSettings;
    private BuildProperties buildProps;
    private Context context;
    private g currentTimeProvider;
    private final AtomicBoolean externallyReady;
    private c httpRequestFactory;
    private IdManager idManager;
    private final AtomicBoolean initialized;
    private long lastCheckTimeMillis;
    private io.fabric.sdk.android.services.c.c preferenceStore;

    public AbstractCheckForUpdatesController() {
        this(false);
    }

    public AbstractCheckForUpdatesController(boolean z) {
        this.initialized = new AtomicBoolean();
        this.lastCheckTimeMillis = LAST_UPDATE_CHECK_DEFAULT;
        this.externallyReady = new AtomicBoolean(z);
    }

    public void initialize(Context context2, Beta beta2, IdManager idManager2, f fVar, BuildProperties buildProperties, io.fabric.sdk.android.services.c.c cVar, g gVar, c cVar2) {
        this.context = context2;
        this.beta = beta2;
        this.idManager = idManager2;
        this.betaSettings = fVar;
        this.buildProps = buildProperties;
        this.preferenceStore = cVar;
        this.currentTimeProvider = gVar;
        this.httpRequestFactory = cVar2;
        if (signalInitialized()) {
            checkForUpdates();
        }
    }

    /* access modifiers changed from: protected */
    public boolean signalExternallyReady() {
        this.externallyReady.set(true);
        return this.initialized.get();
    }

    /* access modifiers changed from: package-private */
    public boolean signalInitialized() {
        this.initialized.set(true);
        return this.externallyReady.get();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"CommitPrefEdits"})
    public void checkForUpdates() {
        synchronized (this.preferenceStore) {
            if (this.preferenceStore.a().contains(LAST_UPDATE_CHECK_KEY)) {
                this.preferenceStore.a(this.preferenceStore.b().remove(LAST_UPDATE_CHECK_KEY));
            }
        }
        long a2 = this.currentTimeProvider.a();
        long j = ((long) this.betaSettings.f7295b) * MILLIS_PER_SECOND;
        Fabric.h().a(Beta.TAG, "Check for updates delay: " + j);
        Fabric.h().a(Beta.TAG, "Check for updates last check time: " + getLastCheckTimeMillis());
        long lastCheckTimeMillis2 = j + getLastCheckTimeMillis();
        Fabric.h().a(Beta.TAG, "Check for updates current time: " + a2 + ", next check time: " + lastCheckTimeMillis2);
        if (a2 >= lastCheckTimeMillis2) {
            try {
                performUpdateCheck();
            } finally {
                setLastCheckTimeMillis(a2);
            }
        } else {
            Fabric.h().a(Beta.TAG, "Check for updates next check time was not passed");
        }
    }

    private void performUpdateCheck() {
        Fabric.h().a(Beta.TAG, "Performing update check");
        new CheckForUpdatesRequest(this.beta, this.beta.getOverridenSpiEndpoint(), this.betaSettings.f7294a, this.httpRequestFactory, new CheckForUpdatesResponseTransform()).invoke(new d().a(this.context), this.idManager.i().get(IdManager.DeviceIdentifierType.FONT_TOKEN), this.buildProps);
    }

    /* access modifiers changed from: package-private */
    public void setLastCheckTimeMillis(long j) {
        this.lastCheckTimeMillis = j;
    }

    /* access modifiers changed from: package-private */
    public long getLastCheckTimeMillis() {
        return this.lastCheckTimeMillis;
    }
}
