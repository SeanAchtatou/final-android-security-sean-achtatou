package com.crashlytics.android.beta;

import android.content.Context;
import io.fabric.sdk.android.services.c.c;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.common.g;
import io.fabric.sdk.android.services.settings.f;

interface UpdatesController {
    void initialize(Context context, Beta beta, IdManager idManager, f fVar, BuildProperties buildProperties, c cVar, g gVar, io.fabric.sdk.android.services.network.c cVar2);

    boolean isActivityLifecycleTriggered();
}
