package com.crashlytics.android.beta;

import android.content.Context;
import io.fabric.sdk.android.services.a.d;
import java.io.FileInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DeviceTokenLoader implements d<String> {
    private static final String BETA_APP_PACKAGE_NAME = "io.crash.air";
    private static final String DIRFACTOR_DEVICE_TOKEN_PREFIX = "assets/com.crashlytics.android.beta/dirfactor-device-token=";

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        io.fabric.sdk.android.Fabric.h().a(com.crashlytics.android.beta.Beta.TAG, "Beta by Crashlytics app is not installed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005e, code lost:
        if (r1 != null) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0064, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0065, code lost:
        io.fabric.sdk.android.Fabric.h().e(com.crashlytics.android.beta.Beta.TAG, "Failed to close the APK file", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0071, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0072, code lost:
        r8 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0086, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0087, code lost:
        io.fabric.sdk.android.Fabric.h().e(com.crashlytics.android.beta.Beta.TAG, "Failed to close the APK file", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0093, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0094, code lost:
        r8 = r2;
        r2 = r1;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a9, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00aa, code lost:
        io.fabric.sdk.android.Fabric.h().e(com.crashlytics.android.beta.Beta.TAG, "Failed to close the APK file", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b8, code lost:
        r2 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00bf, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c0, code lost:
        io.fabric.sdk.android.Fabric.h().e(com.crashlytics.android.beta.Beta.TAG, "Failed to close the APK file", r1);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052 A[ExcHandler: NameNotFoundException (e android.content.pm.PackageManager$NameNotFoundException), Splitter:B:1:0x0009] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0082 A[SYNTHETIC, Splitter:B:25:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a4 A[SYNTHETIC, Splitter:B:34:0x00a4] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00bb A[SYNTHETIC, Splitter:B:41:0x00bb] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:22:0x0075=Splitter:B:22:0x0075, B:31:0x0097=Splitter:B:31:0x0097} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String load(android.content.Context r10) {
        /*
            r9 = this;
            long r4 = java.lang.System.nanoTime()
            java.lang.String r0 = ""
            r1 = 0
            java.lang.String r2 = "io.crash.air"
            java.util.zip.ZipInputStream r1 = r9.getZipInputStreamOfApkFrom(r10, r2)     // Catch:{ NameNotFoundException -> 0x0052, FileNotFoundException -> 0x0071, IOException -> 0x0093, all -> 0x00b7 }
            java.lang.String r0 = r9.determineDeviceToken(r1)     // Catch:{ NameNotFoundException -> 0x0052, FileNotFoundException -> 0x00d6, IOException -> 0x00d1 }
            if (r1 == 0) goto L_0x0016
            r1.close()     // Catch:{ IOException -> 0x0045 }
        L_0x0016:
            long r2 = java.lang.System.nanoTime()
            long r2 = r2 - r4
            double r2 = (double) r2
            r4 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r2 = r2 / r4
            io.fabric.sdk.android.i r1 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r4 = "Beta"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Beta device token load took "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r3 = "ms"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.a(r4, r2)
            return r0
        L_0x0045:
            r1 = move-exception
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Beta"
            java.lang.String r6 = "Failed to close the APK file"
            r2.e(r3, r6, r1)
            goto L_0x0016
        L_0x0052:
            r2 = move-exception
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()     // Catch:{ all -> 0x00cc }
            java.lang.String r3 = "Beta"
            java.lang.String r6 = "Beta by Crashlytics app is not installed"
            r2.a(r3, r6)     // Catch:{ all -> 0x00cc }
            if (r1 == 0) goto L_0x0016
            r1.close()     // Catch:{ IOException -> 0x0064 }
            goto L_0x0016
        L_0x0064:
            r1 = move-exception
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Beta"
            java.lang.String r6 = "Failed to close the APK file"
            r2.e(r3, r6, r1)
            goto L_0x0016
        L_0x0071:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
        L_0x0075:
            io.fabric.sdk.android.i r3 = io.fabric.sdk.android.Fabric.h()     // Catch:{ all -> 0x00cf }
            java.lang.String r6 = "Beta"
            java.lang.String r7 = "Failed to find the APK file"
            r3.e(r6, r7, r1)     // Catch:{ all -> 0x00cf }
            if (r2 == 0) goto L_0x0016
            r2.close()     // Catch:{ IOException -> 0x0086 }
            goto L_0x0016
        L_0x0086:
            r1 = move-exception
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Beta"
            java.lang.String r6 = "Failed to close the APK file"
            r2.e(r3, r6, r1)
            goto L_0x0016
        L_0x0093:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
        L_0x0097:
            io.fabric.sdk.android.i r3 = io.fabric.sdk.android.Fabric.h()     // Catch:{ all -> 0x00cf }
            java.lang.String r6 = "Beta"
            java.lang.String r7 = "Failed to read the APK file"
            r3.e(r6, r7, r1)     // Catch:{ all -> 0x00cf }
            if (r2 == 0) goto L_0x0016
            r2.close()     // Catch:{ IOException -> 0x00a9 }
            goto L_0x0016
        L_0x00a9:
            r1 = move-exception
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Beta"
            java.lang.String r6 = "Failed to close the APK file"
            r2.e(r3, r6, r1)
            goto L_0x0016
        L_0x00b7:
            r0 = move-exception
            r2 = r1
        L_0x00b9:
            if (r2 == 0) goto L_0x00be
            r2.close()     // Catch:{ IOException -> 0x00bf }
        L_0x00be:
            throw r0
        L_0x00bf:
            r1 = move-exception
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Beta"
            java.lang.String r4 = "Failed to close the APK file"
            r2.e(r3, r4, r1)
            goto L_0x00be
        L_0x00cc:
            r0 = move-exception
            r2 = r1
            goto L_0x00b9
        L_0x00cf:
            r0 = move-exception
            goto L_0x00b9
        L_0x00d1:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0097
        L_0x00d6:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crashlytics.android.beta.DeviceTokenLoader.load(android.content.Context):java.lang.String");
    }

    /* access modifiers changed from: package-private */
    public ZipInputStream getZipInputStreamOfApkFrom(Context context, String str) {
        return new ZipInputStream(new FileInputStream(context.getPackageManager().getApplicationInfo(str, 0).sourceDir));
    }

    /* access modifiers changed from: package-private */
    public String determineDeviceToken(ZipInputStream zipInputStream) {
        ZipEntry nextEntry = zipInputStream.getNextEntry();
        if (nextEntry != null) {
            String name = nextEntry.getName();
            if (name.startsWith(DIRFACTOR_DEVICE_TOKEN_PREFIX)) {
                return name.substring(DIRFACTOR_DEVICE_TOKEN_PREFIX.length(), name.length() - 1);
            }
        }
        return "";
    }
}
