package com.crashlytics.android.beta;

import io.fabric.sdk.android.f;
import io.fabric.sdk.android.services.common.a;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.c;
import java.util.HashMap;
import java.util.Map;

class CheckForUpdatesRequest extends a {
    static final String BETA_SOURCE = "3";
    static final String BUILD_VERSION = "build_version";
    static final String DISPLAY_VERSION = "display_version";
    static final String HEADER_BETA_TOKEN = "X-CRASHLYTICS-BETA-TOKEN";
    static final String INSTANCE = "instance";
    static final String SDK_ANDROID_DIR_TOKEN_TYPE = "3";
    static final String SOURCE = "source";
    private final CheckForUpdatesResponseTransform responseTransform;

    static String createBetaTokenHeaderValueFor(String str) {
        return "3:" + str;
    }

    public CheckForUpdatesRequest(f fVar, String str, String str2, c cVar, CheckForUpdatesResponseTransform checkForUpdatesResponseTransform) {
        super(fVar, str, str2, cVar, HttpMethod.GET);
        this.responseTransform = checkForUpdatesResponseTransform;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0120  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.crashlytics.android.beta.CheckForUpdatesResponse invoke(java.lang.String r8, java.lang.String r9, com.crashlytics.android.beta.BuildProperties r10) {
        /*
            r7 = this;
            r0 = 0
            java.util.Map r1 = r7.getQueryParamsFor(r10)     // Catch:{ Exception -> 0x00d3, all -> 0x011b }
            io.fabric.sdk.android.services.network.HttpRequest r2 = r7.getHttpRequest(r1)     // Catch:{ Exception -> 0x00d3, all -> 0x011b }
            io.fabric.sdk.android.services.network.HttpRequest r2 = r7.applyHeadersTo(r2, r8, r9)     // Catch:{ Exception -> 0x0145 }
            io.fabric.sdk.android.i r3 = io.fabric.sdk.android.Fabric.h()     // Catch:{ Exception -> 0x0145 }
            java.lang.String r4 = "Beta"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0145 }
            r5.<init>()     // Catch:{ Exception -> 0x0145 }
            java.lang.String r6 = "Checking for updates from "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0145 }
            java.lang.String r6 = r7.getUrl()     // Catch:{ Exception -> 0x0145 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0145 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0145 }
            r3.a(r4, r5)     // Catch:{ Exception -> 0x0145 }
            io.fabric.sdk.android.i r3 = io.fabric.sdk.android.Fabric.h()     // Catch:{ Exception -> 0x0145 }
            java.lang.String r4 = "Beta"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0145 }
            r5.<init>()     // Catch:{ Exception -> 0x0145 }
            java.lang.String r6 = "Checking for updates query params are: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0145 }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ Exception -> 0x0145 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0145 }
            r3.a(r4, r1)     // Catch:{ Exception -> 0x0145 }
            boolean r1 = r2.c()     // Catch:{ Exception -> 0x0145 }
            if (r1 == 0) goto L_0x008e
            io.fabric.sdk.android.i r1 = io.fabric.sdk.android.Fabric.h()     // Catch:{ Exception -> 0x0145 }
            java.lang.String r3 = "Beta"
            java.lang.String r4 = "Checking for updates was successful"
            r1.a(r3, r4)     // Catch:{ Exception -> 0x0145 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0145 }
            java.lang.String r3 = r2.e()     // Catch:{ Exception -> 0x0145 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0145 }
            com.crashlytics.android.beta.CheckForUpdatesResponseTransform r3 = r7.responseTransform     // Catch:{ Exception -> 0x0145 }
            com.crashlytics.android.beta.CheckForUpdatesResponse r0 = r3.fromJson(r1)     // Catch:{ Exception -> 0x0145 }
            if (r2 == 0) goto L_0x008d
            java.lang.String r1 = "X-REQUEST-ID"
            java.lang.String r1 = r2.b(r1)
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Fabric"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Checking for updates request ID: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            r2.a(r3, r1)
        L_0x008d:
            return r0
        L_0x008e:
            io.fabric.sdk.android.i r1 = io.fabric.sdk.android.Fabric.h()     // Catch:{ Exception -> 0x0145 }
            java.lang.String r3 = "Beta"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0145 }
            r4.<init>()     // Catch:{ Exception -> 0x0145 }
            java.lang.String r5 = "Checking for updates failed. Response code: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0145 }
            int r5 = r2.b()     // Catch:{ Exception -> 0x0145 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0145 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0145 }
            r1.e(r3, r4)     // Catch:{ Exception -> 0x0145 }
            if (r2 == 0) goto L_0x008d
            java.lang.String r1 = "X-REQUEST-ID"
            java.lang.String r1 = r2.b(r1)
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Fabric"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Checking for updates request ID: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            r2.a(r3, r1)
            goto L_0x008d
        L_0x00d3:
            r1 = move-exception
            r2 = r0
        L_0x00d5:
            io.fabric.sdk.android.i r3 = io.fabric.sdk.android.Fabric.h()     // Catch:{ all -> 0x0143 }
            java.lang.String r4 = "Beta"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0143 }
            r5.<init>()     // Catch:{ all -> 0x0143 }
            java.lang.String r6 = "Error while checking for updates from "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0143 }
            java.lang.String r6 = r7.getUrl()     // Catch:{ all -> 0x0143 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0143 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0143 }
            r3.e(r4, r5, r1)     // Catch:{ all -> 0x0143 }
            if (r2 == 0) goto L_0x008d
            java.lang.String r1 = "X-REQUEST-ID"
            java.lang.String r1 = r2.b(r1)
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Fabric"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Checking for updates request ID: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            r2.a(r3, r1)
            goto L_0x008d
        L_0x011b:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x011e:
            if (r2 == 0) goto L_0x0142
            java.lang.String r1 = "X-REQUEST-ID"
            java.lang.String r1 = r2.b(r1)
            io.fabric.sdk.android.i r2 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Fabric"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Checking for updates request ID: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            r2.a(r3, r1)
        L_0x0142:
            throw r0
        L_0x0143:
            r0 = move-exception
            goto L_0x011e
        L_0x0145:
            r1 = move-exception
            goto L_0x00d5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crashlytics.android.beta.CheckForUpdatesRequest.invoke(java.lang.String, java.lang.String, com.crashlytics.android.beta.BuildProperties):com.crashlytics.android.beta.CheckForUpdatesResponse");
    }

    private HttpRequest applyHeadersTo(HttpRequest httpRequest, String str, String str2) {
        return httpRequest.a(a.HEADER_ACCEPT, a.ACCEPT_JSON_VALUE).a(a.HEADER_USER_AGENT, a.CRASHLYTICS_USER_AGENT + this.kit.getVersion()).a(a.HEADER_DEVELOPER_TOKEN, a.CLS_ANDROID_SDK_DEVELOPER_TOKEN).a(a.HEADER_CLIENT_TYPE, a.ANDROID_CLIENT_TYPE).a(a.HEADER_CLIENT_VERSION, this.kit.getVersion()).a(a.HEADER_API_KEY, str).a(HEADER_BETA_TOKEN, createBetaTokenHeaderValueFor(str2));
    }

    private Map<String, String> getQueryParamsFor(BuildProperties buildProperties) {
        HashMap hashMap = new HashMap();
        hashMap.put(BUILD_VERSION, buildProperties.versionCode);
        hashMap.put(DISPLAY_VERSION, buildProperties.versionName);
        hashMap.put(INSTANCE, buildProperties.buildId);
        hashMap.put("source", "3");
        return hashMap;
    }
}
