package com.crashlytics.android.core;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashPromptDialog;
import com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler;
import com.crashlytics.android.core.LogFileManager;
import com.crashlytics.android.core.ReportUploader;
import com.crashlytics.android.core.internal.models.SessionEventData;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.c.a;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeliveryMechanism;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.common.f;
import io.fabric.sdk.android.services.network.c;
import io.fabric.sdk.android.services.settings.o;
import io.fabric.sdk.android.services.settings.p;
import io.fabric.sdk.android.services.settings.q;
import io.fabric.sdk.android.services.settings.s;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Thread;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CrashlyticsController {
    private static final int ANALYZER_VERSION = 1;
    static final FilenameFilter ANY_SESSION_FILENAME_FILTER = new FilenameFilter() {
        public boolean accept(File file, String str) {
            return CrashlyticsController.SESSION_FILE_PATTERN.matcher(str).matches();
        }
    };
    private static final String COLLECT_CUSTOM_KEYS = "com.crashlytics.CollectCustomKeys";
    private static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
    private static final String EVENT_TYPE_CRASH = "crash";
    private static final String EVENT_TYPE_LOGGED = "error";
    static final String FATAL_SESSION_DIR = "fatal-sessions";
    private static final String GENERATOR_FORMAT = "Crashlytics Android SDK/%s";
    private static final String[] INITIAL_SESSION_PART_TAGS = {SESSION_USER_TAG, SESSION_APP_TAG, SESSION_OS_TAG, SESSION_DEVICE_TAG};
    static final String INVALID_CLS_CACHE_DIR = "invalidClsFiles";
    static final Comparator<File> LARGEST_FILE_NAME_FIRST = new Comparator<File>() {
        public int compare(File file, File file2) {
            return file2.getName().compareTo(file.getName());
        }
    };
    static final int MAX_INVALID_SESSIONS = 4;
    private static final int MAX_LOCAL_LOGGED_EXCEPTIONS = 64;
    static final int MAX_OPEN_SESSIONS = 8;
    static final int MAX_STACK_SIZE = 1024;
    static final String NONFATAL_SESSION_DIR = "nonfatal-sessions";
    static final int NUM_STACK_REPETITIONS_ALLOWED = 10;
    private static final Map<String, String> SEND_AT_CRASHTIME_HEADER = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
    static final String SESSION_APP_TAG = "SessionApp";
    static final String SESSION_BEGIN_TAG = "BeginSession";
    static final String SESSION_DEVICE_TAG = "SessionDevice";
    static final String SESSION_EVENT_MISSING_BINARY_IMGS_TAG = "SessionMissingBinaryImages";
    static final String SESSION_FATAL_TAG = "SessionCrash";
    static final FilenameFilter SESSION_FILE_FILTER = new FilenameFilter() {
        public boolean accept(File file, String str) {
            return str.length() == ClsFileOutputStream.SESSION_FILE_EXTENSION.length() + 35 && str.endsWith(ClsFileOutputStream.SESSION_FILE_EXTENSION);
        }
    };
    /* access modifiers changed from: private */
    public static final Pattern SESSION_FILE_PATTERN = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
    private static final int SESSION_ID_LENGTH = 35;
    static final String SESSION_NON_FATAL_TAG = "SessionEvent";
    static final String SESSION_OS_TAG = "SessionOS";
    static final String SESSION_USER_TAG = "SessionUser";
    private static final boolean SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT = false;
    static final Comparator<File> SMALLEST_FILE_NAME_FIRST = new Comparator<File>() {
        public int compare(File file, File file2) {
            return file.getName().compareTo(file2.getName());
        }
    };
    private final AppData appData;
    private final CrashlyticsBackgroundWorker backgroundWorker;
    private CrashlyticsUncaughtExceptionHandler crashHandler;
    /* access modifiers changed from: private */
    public final CrashlyticsCore crashlyticsCore;
    private final DevicePowerStateListener devicePowerStateListener;
    private final AtomicInteger eventCounter = new AtomicInteger(0);
    private final a fileStore;
    private final ReportUploader.HandlingExceptionCheck handlingExceptionCheck;
    private final c httpRequestFactory;
    private final IdManager idManager;
    private final LogFileDirectoryProvider logFileDirectoryProvider;
    /* access modifiers changed from: private */
    public final LogFileManager logFileManager;
    private final PreferenceManager preferenceManager;
    private final ReportUploader.ReportFilesProvider reportFilesProvider;
    private final StackTraceTrimmingStrategy stackTraceTrimmingStrategy;
    private final String unityVersion;

    static class FileNameContainsFilter implements FilenameFilter {
        private final String string;

        public FileNameContainsFilter(String str) {
            this.string = str;
        }

        public boolean accept(File file, String str) {
            return str.contains(this.string) && !str.endsWith(ClsFileOutputStream.IN_PROGRESS_SESSION_FILE_EXTENSION);
        }
    }

    static class SessionPartFileFilter implements FilenameFilter {
        private final String sessionId;

        public SessionPartFileFilter(String str) {
            this.sessionId = str;
        }

        public boolean accept(File file, String str) {
            if (!str.equals(this.sessionId + ClsFileOutputStream.SESSION_FILE_EXTENSION) && str.contains(this.sessionId) && !str.endsWith(ClsFileOutputStream.IN_PROGRESS_SESSION_FILE_EXTENSION)) {
                return true;
            }
            return false;
        }
    }

    private static class AnySessionPartFileFilter implements FilenameFilter {
        private AnySessionPartFileFilter() {
        }

        public boolean accept(File file, String str) {
            return !CrashlyticsController.SESSION_FILE_FILTER.accept(file, str) && CrashlyticsController.SESSION_FILE_PATTERN.matcher(str).matches();
        }
    }

    static class InvalidPartFileFilter implements FilenameFilter {
        InvalidPartFileFilter() {
        }

        public boolean accept(File file, String str) {
            return ClsFileOutputStream.TEMP_FILENAME_FILTER.accept(file, str) || str.contains(CrashlyticsController.SESSION_EVENT_MISSING_BINARY_IMGS_TAG);
        }
    }

    CrashlyticsController(CrashlyticsCore crashlyticsCore2, CrashlyticsBackgroundWorker crashlyticsBackgroundWorker, c cVar, IdManager idManager2, PreferenceManager preferenceManager2, a aVar, AppData appData2, UnityVersionProvider unityVersionProvider) {
        this.crashlyticsCore = crashlyticsCore2;
        this.backgroundWorker = crashlyticsBackgroundWorker;
        this.httpRequestFactory = cVar;
        this.idManager = idManager2;
        this.preferenceManager = preferenceManager2;
        this.fileStore = aVar;
        this.appData = appData2;
        this.unityVersion = unityVersionProvider.getUnityVersion();
        Context context = crashlyticsCore2.getContext();
        this.logFileDirectoryProvider = new LogFileDirectoryProvider(aVar);
        this.logFileManager = new LogFileManager(context, this.logFileDirectoryProvider);
        this.reportFilesProvider = new ReportUploaderFilesProvider();
        this.handlingExceptionCheck = new ReportUploaderHandlingExceptionCheck();
        this.devicePowerStateListener = new DevicePowerStateListener(context);
        this.stackTraceTrimmingStrategy = new MiddleOutFallbackStrategy(1024, new RemoveRepeatsStrategy(10));
    }

    /* access modifiers changed from: package-private */
    public void enableExceptionHandling(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        openSession();
        this.crashHandler = new CrashlyticsUncaughtExceptionHandler(new CrashlyticsUncaughtExceptionHandler.CrashListener() {
            public void onUncaughtException(Thread thread, Throwable th) {
                CrashlyticsController.this.handleUncaughtException(thread, th);
            }
        }, uncaughtExceptionHandler);
        Thread.setDefaultUncaughtExceptionHandler(this.crashHandler);
    }

    /* access modifiers changed from: package-private */
    public synchronized void handleUncaughtException(final Thread thread, final Throwable th) {
        Fabric.h().a(CrashlyticsCore.TAG, "Crashlytics is handling uncaught exception \"" + th + "\" from thread " + thread.getName());
        this.devicePowerStateListener.dispose();
        final Date date = new Date();
        this.backgroundWorker.submitAndWait(new Callable<Void>() {
            public Void call() {
                CrashlyticsController.this.crashlyticsCore.createCrashMarker();
                CrashlyticsController.this.writeFatal(date, thread, th);
                s b2 = q.a().b();
                p pVar = b2 != null ? b2.f7332b : null;
                CrashlyticsController.this.doCloseSessions(pVar);
                CrashlyticsController.this.doOpenSession();
                if (pVar != null) {
                    CrashlyticsController.this.trimSessionFiles(pVar.f7325g);
                }
                if (!CrashlyticsController.this.shouldPromptUserBeforeSendingCrashReports(b2)) {
                    CrashlyticsController.this.sendSessionReports(b2);
                }
                return null;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void submitAllReports(float f2, s sVar) {
        if (sVar == null) {
            Fabric.h().d(CrashlyticsCore.TAG, "Could not send reports. Settings are not available.");
            return;
        }
        new ReportUploader(this.appData.apiKey, getCreateReportSpiCall(sVar.f7331a.f7291d), this.reportFilesProvider, this.handlingExceptionCheck).uploadReports(f2, shouldPromptUserBeforeSendingCrashReports(sVar) ? new PrivacyDialogCheck(this.crashlyticsCore, this.preferenceManager, sVar.f7333c) : new ReportUploader.AlwaysSendCheck());
    }

    /* access modifiers changed from: package-private */
    public void writeToLog(final long j, final String str) {
        this.backgroundWorker.submit(new Callable<Void>() {
            public Void call() {
                if (CrashlyticsController.this.isHandlingException()) {
                    return null;
                }
                CrashlyticsController.this.logFileManager.writeToLog(j, str);
                return null;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void writeNonFatalException(final Thread thread, final Throwable th) {
        final Date date = new Date();
        this.backgroundWorker.submit(new Runnable() {
            public void run() {
                if (!CrashlyticsController.this.isHandlingException()) {
                    CrashlyticsController.this.doWriteNonFatal(date, thread, th);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void cacheUserData(final String str, final String str2, final String str3) {
        this.backgroundWorker.submit(new Callable<Void>() {
            public Void call() {
                new MetaDataStore(CrashlyticsController.this.getFilesDir()).writeUserData(CrashlyticsController.this.getCurrentSessionId(), new UserMetaData(str, str2, str3));
                return null;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void cacheKeyData(final Map<String, String> map) {
        this.backgroundWorker.submit(new Callable<Void>() {
            public Void call() {
                new MetaDataStore(CrashlyticsController.this.getFilesDir()).writeKeyData(CrashlyticsController.this.getCurrentSessionId(), map);
                return null;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void openSession() {
        this.backgroundWorker.submit(new Callable<Void>() {
            public Void call() {
                CrashlyticsController.this.doOpenSession();
                return null;
            }
        });
    }

    /* access modifiers changed from: private */
    public String getCurrentSessionId() {
        File[] listSortedSessionBeginFiles = listSortedSessionBeginFiles();
        if (listSortedSessionBeginFiles.length > 0) {
            return getSessionIdFromSessionFile(listSortedSessionBeginFiles[0]);
        }
        return null;
    }

    private String getPreviousSessionId() {
        File[] listSortedSessionBeginFiles = listSortedSessionBeginFiles();
        if (listSortedSessionBeginFiles.length > 1) {
            return getSessionIdFromSessionFile(listSortedSessionBeginFiles[1]);
        }
        return null;
    }

    static String getSessionIdFromSessionFile(File file) {
        return file.getName().substring(0, 35);
    }

    /* access modifiers changed from: package-private */
    public boolean hasOpenSession() {
        return listSessionBeginFiles().length > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean finalizeSessions(final p pVar) {
        return ((Boolean) this.backgroundWorker.submitAndWait(new Callable<Boolean>() {
            public Boolean call() {
                if (CrashlyticsController.this.isHandlingException()) {
                    Fabric.h().a(CrashlyticsCore.TAG, "Skipping session finalization because a crash has already occurred.");
                    return Boolean.FALSE;
                }
                Fabric.h().a(CrashlyticsCore.TAG, "Finalizing previously open sessions.");
                CrashlyticsController.this.doCloseSessions(pVar, true);
                Fabric.h().a(CrashlyticsCore.TAG, "Closed all previously open sessions");
                return Boolean.TRUE;
            }
        })).booleanValue();
    }

    /* access modifiers changed from: private */
    public void doOpenSession() {
        Date date = new Date();
        String clsuuid = new CLSUUID(this.idManager).toString();
        Fabric.h().a(CrashlyticsCore.TAG, "Opening a new session with ID " + clsuuid);
        writeBeginSession(clsuuid, date);
        writeSessionApp(clsuuid);
        writeSessionOS(clsuuid);
        writeSessionDevice(clsuuid);
        this.logFileManager.setCurrentSession(clsuuid);
    }

    /* access modifiers changed from: package-private */
    public void doCloseSessions(p pVar) {
        doCloseSessions(pVar, false);
    }

    /* access modifiers changed from: private */
    public void doCloseSessions(p pVar, boolean z) {
        int i = z ? 1 : 0;
        trimOpenSessions(i + 8);
        File[] listSortedSessionBeginFiles = listSortedSessionBeginFiles();
        if (listSortedSessionBeginFiles.length <= i) {
            Fabric.h().a(CrashlyticsCore.TAG, "No open sessions to be closed.");
            return;
        }
        writeSessionUser(getSessionIdFromSessionFile(listSortedSessionBeginFiles[i]));
        if (pVar == null) {
            Fabric.h().a(CrashlyticsCore.TAG, "Unable to close session. Settings are not loaded.");
        } else {
            closeOpenSessions(listSortedSessionBeginFiles, i, pVar.f7321c);
        }
    }

    private void closeOpenSessions(File[] fileArr, int i, int i2) {
        Fabric.h().a(CrashlyticsCore.TAG, "Closing open sessions.");
        while (i < fileArr.length) {
            File file = fileArr[i];
            String sessionIdFromSessionFile = getSessionIdFromSessionFile(file);
            Fabric.h().a(CrashlyticsCore.TAG, "Closing session: " + sessionIdFromSessionFile);
            writeSessionPartsToSessionFile(file, sessionIdFromSessionFile, i2);
            i++;
        }
    }

    private void closeWithoutRenamingOrLog(ClsFileOutputStream clsFileOutputStream) {
        if (clsFileOutputStream != null) {
            try {
                clsFileOutputStream.closeInProgressStream();
            } catch (IOException e2) {
                Fabric.h().e(CrashlyticsCore.TAG, "Error closing session file stream in the presence of an exception", e2);
            }
        }
    }

    private void deleteSessionPartFilesFor(String str) {
        for (File delete : listSessionPartFilesFor(str)) {
            delete.delete();
        }
    }

    private File[] listSessionPartFilesFor(String str) {
        return listFilesMatching(new SessionPartFileFilter(str));
    }

    /* access modifiers changed from: package-private */
    public File[] listCompleteSessionFiles() {
        LinkedList linkedList = new LinkedList();
        Collections.addAll(linkedList, listFilesMatching(getFatalSessionFilesDir(), SESSION_FILE_FILTER));
        Collections.addAll(linkedList, listFilesMatching(getNonFatalSessionFilesDir(), SESSION_FILE_FILTER));
        Collections.addAll(linkedList, listFilesMatching(getFilesDir(), SESSION_FILE_FILTER));
        return (File[]) linkedList.toArray(new File[linkedList.size()]);
    }

    /* access modifiers changed from: package-private */
    public File[] listSessionBeginFiles() {
        return listFilesMatching(new FileNameContainsFilter(SESSION_BEGIN_TAG));
    }

    private File[] listSortedSessionBeginFiles() {
        File[] listSessionBeginFiles = listSessionBeginFiles();
        Arrays.sort(listSessionBeginFiles, LARGEST_FILE_NAME_FIRST);
        return listSessionBeginFiles;
    }

    /* access modifiers changed from: private */
    public File[] listFilesMatching(FilenameFilter filenameFilter) {
        return listFilesMatching(getFilesDir(), filenameFilter);
    }

    private File[] listFilesMatching(File file, FilenameFilter filenameFilter) {
        return ensureFileArrayNotNull(file.listFiles(filenameFilter));
    }

    private File[] listFiles(File file) {
        return ensureFileArrayNotNull(file.listFiles());
    }

    private File[] ensureFileArrayNotNull(File[] fileArr) {
        return fileArr == null ? new File[0] : fileArr;
    }

    private void trimSessionEventFiles(String str, int i) {
        Utils.capFileCount(getFilesDir(), new FileNameContainsFilter(str + SESSION_NON_FATAL_TAG), i, SMALLEST_FILE_NAME_FIRST);
    }

    /* access modifiers changed from: package-private */
    public void trimSessionFiles(int i) {
        int capFileCount = i - Utils.capFileCount(getFatalSessionFilesDir(), i, SMALLEST_FILE_NAME_FIRST);
        Utils.capFileCount(getFilesDir(), SESSION_FILE_FILTER, capFileCount - Utils.capFileCount(getNonFatalSessionFilesDir(), capFileCount, SMALLEST_FILE_NAME_FIRST), SMALLEST_FILE_NAME_FIRST);
    }

    private void trimOpenSessions(int i) {
        HashSet hashSet = new HashSet();
        File[] listSortedSessionBeginFiles = listSortedSessionBeginFiles();
        int min = Math.min(i, listSortedSessionBeginFiles.length);
        for (int i2 = 0; i2 < min; i2++) {
            hashSet.add(getSessionIdFromSessionFile(listSortedSessionBeginFiles[i2]));
        }
        this.logFileManager.discardOldLogFiles(hashSet);
        retainSessions(listFilesMatching(new AnySessionPartFileFilter()), hashSet);
    }

    private void retainSessions(File[] fileArr, Set<String> set) {
        for (File file : fileArr) {
            String name = file.getName();
            Matcher matcher = SESSION_FILE_PATTERN.matcher(name);
            if (!matcher.matches()) {
                Fabric.h().a(CrashlyticsCore.TAG, "Deleting unknown file: " + name);
                file.delete();
                return;
            }
            if (!set.contains(matcher.group(1))) {
                Fabric.h().a(CrashlyticsCore.TAG, "Trimming session file: " + name);
                file.delete();
            }
        }
    }

    private File[] getTrimmedNonFatalFiles(String str, File[] fileArr, int i) {
        if (fileArr.length <= i) {
            return fileArr;
        }
        Fabric.h().a(CrashlyticsCore.TAG, String.format(Locale.US, "Trimming down to %d logged exceptions.", Integer.valueOf(i)));
        trimSessionEventFiles(str, i);
        return listFilesMatching(new FileNameContainsFilter(str + SESSION_NON_FATAL_TAG));
    }

    /* access modifiers changed from: package-private */
    public void cleanInvalidTempFiles() {
        this.backgroundWorker.submit(new Runnable() {
            public void run() {
                CrashlyticsController.this.doCleanInvalidTempFiles(CrashlyticsController.this.listFilesMatching(new InvalidPartFileFilter()));
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void doCleanInvalidTempFiles(File[] fileArr) {
        final HashSet hashSet = new HashSet();
        for (File file : fileArr) {
            Fabric.h().a(CrashlyticsCore.TAG, "Found invalid session part file: " + file);
            hashSet.add(getSessionIdFromSessionFile(file));
        }
        if (!hashSet.isEmpty()) {
            File invalidFilesDir = getInvalidFilesDir();
            if (!invalidFilesDir.exists()) {
                invalidFilesDir.mkdir();
            }
            for (File file2 : listFilesMatching(new FilenameFilter() {
                public boolean accept(File file, String str) {
                    if (str.length() < 35) {
                        return false;
                    }
                    return hashSet.contains(str.substring(0, 35));
                }
            })) {
                Fabric.h().a(CrashlyticsCore.TAG, "Moving session file: " + file2);
                if (!file2.renameTo(new File(invalidFilesDir, file2.getName()))) {
                    Fabric.h().a(CrashlyticsCore.TAG, "Could not move session file. Deleting " + file2);
                    file2.delete();
                }
            }
            trimInvalidSessionFiles();
        }
    }

    private void trimInvalidSessionFiles() {
        File invalidFilesDir = getInvalidFilesDir();
        if (invalidFilesDir.exists()) {
            File[] listFilesMatching = listFilesMatching(invalidFilesDir, new InvalidPartFileFilter());
            Arrays.sort(listFilesMatching, Collections.reverseOrder());
            HashSet hashSet = new HashSet();
            for (int i = 0; i < listFilesMatching.length && hashSet.size() < 4; i++) {
                hashSet.add(getSessionIdFromSessionFile(listFilesMatching[i]));
            }
            retainSessions(listFiles(invalidFilesDir), hashSet);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.core.ClsFileOutputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    /* access modifiers changed from: private */
    public void writeFatal(Date date, Thread thread, Throwable th) {
        ClsFileOutputStream clsFileOutputStream;
        CodedOutputStream codedOutputStream = null;
        try {
            String currentSessionId = getCurrentSessionId();
            if (currentSessionId == null) {
                Fabric.h().e(CrashlyticsCore.TAG, "Tried to write a fatal exception while no session was open.", null);
                CommonUtils.a((Flushable) null, "Failed to flush to session begin file.");
                CommonUtils.a((Closeable) null, "Failed to close fatal exception file output stream.");
                return;
            }
            recordFatalExceptionAnswersEvent(currentSessionId, th.getClass().getName());
            ClsFileOutputStream clsFileOutputStream2 = new ClsFileOutputStream(getFilesDir(), currentSessionId + SESSION_FATAL_TAG);
            try {
                codedOutputStream = CodedOutputStream.newInstance(clsFileOutputStream2);
                writeSessionEvent(codedOutputStream, date, thread, th, EVENT_TYPE_CRASH, true);
                CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
                CommonUtils.a((Closeable) clsFileOutputStream2, "Failed to close fatal exception file output stream.");
            } catch (Exception e2) {
                e = e2;
                clsFileOutputStream = clsFileOutputStream2;
                try {
                    Fabric.h().e(CrashlyticsCore.TAG, "An error occurred in the fatal exception logger", e);
                    CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
                    CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close fatal exception file output stream.");
                } catch (Throwable th2) {
                    th = th2;
                    CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
                    CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close fatal exception file output stream.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                clsFileOutputStream = clsFileOutputStream2;
                CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
                CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close fatal exception file output stream.");
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            clsFileOutputStream = null;
            Fabric.h().e(CrashlyticsCore.TAG, "An error occurred in the fatal exception logger", e);
            CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close fatal exception file output stream.");
        } catch (Throwable th4) {
            th = th4;
            clsFileOutputStream = null;
            CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close fatal exception file output stream.");
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public void writeExternalCrashEvent(final SessionEventData sessionEventData) {
        this.backgroundWorker.submit(new Callable<Void>() {
            public Void call() {
                if (CrashlyticsController.this.isHandlingException()) {
                    return null;
                }
                CrashlyticsController.this.doWriteExternalCrashEvent(sessionEventData);
                return null;
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.core.ClsFileOutputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    /* access modifiers changed from: private */
    public void doWriteExternalCrashEvent(SessionEventData sessionEventData) {
        ClsFileOutputStream clsFileOutputStream;
        boolean z = true;
        CodedOutputStream codedOutputStream = null;
        try {
            String previousSessionId = getPreviousSessionId();
            if (previousSessionId == null) {
                Fabric.h().e(CrashlyticsCore.TAG, "Tried to write a native crash while no session was open.", null);
                CommonUtils.a((Flushable) null, "Failed to flush to session begin file.");
                CommonUtils.a((Closeable) null, "Failed to close fatal exception file output stream.");
                return;
            }
            recordFatalExceptionAnswersEvent(previousSessionId, String.format(Locale.US, "<native-crash [%s (%s)]>", sessionEventData.signal.code, sessionEventData.signal.name));
            if (sessionEventData.binaryImages == null || sessionEventData.binaryImages.length <= 0) {
                z = false;
            }
            clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), previousSessionId + (z ? SESSION_FATAL_TAG : SESSION_EVENT_MISSING_BINARY_IMGS_TAG));
            try {
                codedOutputStream = CodedOutputStream.newInstance(clsFileOutputStream);
                NativeCrashWriter.writeNativeCrash(sessionEventData, new LogFileManager(this.crashlyticsCore.getContext(), this.logFileDirectoryProvider, previousSessionId), new MetaDataStore(getFilesDir()).readKeyData(previousSessionId), codedOutputStream);
                CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
                CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close fatal exception file output stream.");
            } catch (Exception e2) {
                e = e2;
                try {
                    Fabric.h().e(CrashlyticsCore.TAG, "An error occurred in the native crash logger", e);
                    CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
                    CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close fatal exception file output stream.");
                } catch (Throwable th) {
                    th = th;
                    CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
                    CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close fatal exception file output stream.");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            clsFileOutputStream = null;
            Fabric.h().e(CrashlyticsCore.TAG, "An error occurred in the native crash logger", e);
            CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close fatal exception file output stream.");
        } catch (Throwable th2) {
            th = th2;
            clsFileOutputStream = null;
            CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close fatal exception file output stream.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.core.ClsFileOutputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    /* access modifiers changed from: private */
    public void doWriteNonFatal(Date date, Thread thread, Throwable th) {
        ClsFileOutputStream clsFileOutputStream;
        CodedOutputStream codedOutputStream = null;
        String currentSessionId = getCurrentSessionId();
        if (currentSessionId == null) {
            Fabric.h().e(CrashlyticsCore.TAG, "Tried to write a non-fatal exception while no session was open.", null);
            return;
        }
        recordLoggedExceptionAnswersEvent(currentSessionId, th.getClass().getName());
        try {
            Fabric.h().a(CrashlyticsCore.TAG, "Crashlytics is logging non-fatal exception \"" + th + "\" from thread " + thread.getName());
            ClsFileOutputStream clsFileOutputStream2 = new ClsFileOutputStream(getFilesDir(), currentSessionId + SESSION_NON_FATAL_TAG + CommonUtils.a(this.eventCounter.getAndIncrement()));
            try {
                codedOutputStream = CodedOutputStream.newInstance(clsFileOutputStream2);
                writeSessionEvent(codedOutputStream, date, thread, th, EVENT_TYPE_LOGGED, false);
                CommonUtils.a(codedOutputStream, "Failed to flush to non-fatal file.");
                CommonUtils.a((Closeable) clsFileOutputStream2, "Failed to close non-fatal file output stream.");
            } catch (Exception e2) {
                e = e2;
                clsFileOutputStream = clsFileOutputStream2;
                try {
                    Fabric.h().e(CrashlyticsCore.TAG, "An error occurred in the non-fatal exception logger", e);
                    CommonUtils.a(codedOutputStream, "Failed to flush to non-fatal file.");
                    CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close non-fatal file output stream.");
                    trimSessionEventFiles(currentSessionId, 64);
                } catch (Throwable th2) {
                    th = th2;
                    CommonUtils.a(codedOutputStream, "Failed to flush to non-fatal file.");
                    CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close non-fatal file output stream.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                clsFileOutputStream = clsFileOutputStream2;
                CommonUtils.a(codedOutputStream, "Failed to flush to non-fatal file.");
                CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close non-fatal file output stream.");
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            clsFileOutputStream = null;
            Fabric.h().e(CrashlyticsCore.TAG, "An error occurred in the non-fatal exception logger", e);
            CommonUtils.a(codedOutputStream, "Failed to flush to non-fatal file.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close non-fatal file output stream.");
            trimSessionEventFiles(currentSessionId, 64);
        } catch (Throwable th4) {
            th = th4;
            clsFileOutputStream = null;
            CommonUtils.a(codedOutputStream, "Failed to flush to non-fatal file.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close non-fatal file output stream.");
            throw th;
        }
        try {
            trimSessionEventFiles(currentSessionId, 64);
        } catch (Exception e4) {
            Fabric.h().e(CrashlyticsCore.TAG, "An error occurred when trimming non-fatal files.", e4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.core.ClsFileOutputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    private void writeBeginSession(String str, Date date) {
        ClsFileOutputStream clsFileOutputStream;
        CodedOutputStream codedOutputStream = null;
        try {
            clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), str + SESSION_BEGIN_TAG);
            try {
                codedOutputStream = CodedOutputStream.newInstance(clsFileOutputStream);
                SessionProtobufHelper.writeBeginSession(codedOutputStream, str, String.format(Locale.US, GENERATOR_FORMAT, this.crashlyticsCore.getVersion()), date.getTime() / 1000);
                CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
                CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close begin session file.");
            } catch (Throwable th) {
                th = th;
                CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
                CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close begin session file.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            clsFileOutputStream = null;
            CommonUtils.a(codedOutputStream, "Failed to flush to session begin file.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close begin session file.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.core.ClsFileOutputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    private void writeSessionApp(String str) {
        ClsFileOutputStream clsFileOutputStream;
        CodedOutputStream codedOutputStream = null;
        try {
            ClsFileOutputStream clsFileOutputStream2 = new ClsFileOutputStream(getFilesDir(), str + SESSION_APP_TAG);
            try {
                CodedOutputStream newInstance = CodedOutputStream.newInstance(clsFileOutputStream2);
                try {
                    SessionProtobufHelper.writeSessionApp(newInstance, this.idManager.c(), this.appData.apiKey, this.appData.versionCode, this.appData.versionName, this.idManager.b(), DeliveryMechanism.a(this.appData.installerPackageName).a(), this.unityVersion);
                    CommonUtils.a(newInstance, "Failed to flush to session app file.");
                    CommonUtils.a((Closeable) clsFileOutputStream2, "Failed to close session app file.");
                } catch (Throwable th) {
                    clsFileOutputStream = clsFileOutputStream2;
                    CodedOutputStream codedOutputStream2 = newInstance;
                    th = th;
                    codedOutputStream = codedOutputStream2;
                    CommonUtils.a(codedOutputStream, "Failed to flush to session app file.");
                    CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close session app file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                clsFileOutputStream = clsFileOutputStream2;
            }
        } catch (Throwable th3) {
            th = th3;
            clsFileOutputStream = null;
            CommonUtils.a(codedOutputStream, "Failed to flush to session app file.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close session app file.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.core.ClsFileOutputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    private void writeSessionOS(String str) {
        ClsFileOutputStream clsFileOutputStream;
        CodedOutputStream codedOutputStream = null;
        try {
            clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), str + SESSION_OS_TAG);
            try {
                codedOutputStream = CodedOutputStream.newInstance(clsFileOutputStream);
                SessionProtobufHelper.writeSessionOS(codedOutputStream, CommonUtils.g(this.crashlyticsCore.getContext()));
                CommonUtils.a(codedOutputStream, "Failed to flush to session OS file.");
                CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close session OS file.");
            } catch (Throwable th) {
                th = th;
                CommonUtils.a(codedOutputStream, "Failed to flush to session OS file.");
                CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close session OS file.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            clsFileOutputStream = null;
            CommonUtils.a(codedOutputStream, "Failed to flush to session OS file.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close session OS file.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.core.ClsFileOutputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    private void writeSessionDevice(String str) {
        ClsFileOutputStream clsFileOutputStream = null;
        CodedOutputStream codedOutputStream = null;
        try {
            ClsFileOutputStream clsFileOutputStream2 = new ClsFileOutputStream(getFilesDir(), str + SESSION_DEVICE_TAG);
            try {
                codedOutputStream = CodedOutputStream.newInstance(clsFileOutputStream2);
                Context context = this.crashlyticsCore.getContext();
                StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
                SessionProtobufHelper.writeSessionDevice(codedOutputStream, this.idManager.h(), CommonUtils.a(), Build.MODEL, Runtime.getRuntime().availableProcessors(), CommonUtils.b(), ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize()), CommonUtils.f(context), this.idManager.i(), CommonUtils.h(context), Build.MANUFACTURER, Build.PRODUCT);
                CommonUtils.a(codedOutputStream, "Failed to flush session device info.");
                CommonUtils.a((Closeable) clsFileOutputStream2, "Failed to close session device file.");
            } catch (Throwable th) {
                th = th;
                clsFileOutputStream = clsFileOutputStream2;
                CommonUtils.a(codedOutputStream, "Failed to flush session device info.");
                CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close session device file.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            CommonUtils.a(codedOutputStream, "Failed to flush session device info.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close session device file.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.core.ClsFileOutputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    private void writeSessionUser(String str) {
        ClsFileOutputStream clsFileOutputStream;
        CodedOutputStream codedOutputStream = null;
        try {
            clsFileOutputStream = new ClsFileOutputStream(getFilesDir(), str + SESSION_USER_TAG);
            try {
                codedOutputStream = CodedOutputStream.newInstance(clsFileOutputStream);
                UserMetaData userMetaData = getUserMetaData(str);
                if (userMetaData.isEmpty()) {
                    CommonUtils.a(codedOutputStream, "Failed to flush session user file.");
                    CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close session user file.");
                    return;
                }
                SessionProtobufHelper.writeSessionUser(codedOutputStream, userMetaData.id, userMetaData.name, userMetaData.email);
                CommonUtils.a(codedOutputStream, "Failed to flush session user file.");
                CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close session user file.");
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            th = th2;
            clsFileOutputStream = null;
            CommonUtils.a(codedOutputStream, "Failed to flush session user file.");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close session user file.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String, java.lang.String):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String, int):long
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String, java.lang.Throwable):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.io.OutputStream, byte[]):void
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String, boolean):boolean */
    private void writeSessionEvent(CodedOutputStream codedOutputStream, Date date, Thread thread, Throwable th, String str, boolean z) {
        Thread[] threadArr;
        Map<String, String> treeMap;
        TrimmedThrowableData trimmedThrowableData = new TrimmedThrowableData(th, this.stackTraceTrimmingStrategy);
        Context context = this.crashlyticsCore.getContext();
        long time = date.getTime() / 1000;
        Float c2 = CommonUtils.c(context);
        int a2 = CommonUtils.a(context, this.devicePowerStateListener.isPowerConnected());
        boolean d2 = CommonUtils.d(context);
        int i = context.getResources().getConfiguration().orientation;
        long b2 = CommonUtils.b() - CommonUtils.b(context);
        long b3 = CommonUtils.b(Environment.getDataDirectory().getPath());
        ActivityManager.RunningAppProcessInfo a3 = CommonUtils.a(context.getPackageName(), context);
        LinkedList linkedList = new LinkedList();
        StackTraceElement[] stackTraceElementArr = trimmedThrowableData.stacktrace;
        String str2 = this.appData.buildId;
        String c3 = this.idManager.c();
        if (z) {
            Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
            threadArr = new Thread[allStackTraces.size()];
            int i2 = 0;
            Iterator<Map.Entry<Thread, StackTraceElement[]>> it = allStackTraces.entrySet().iterator();
            while (true) {
                int i3 = i2;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                threadArr[i3] = (Thread) next.getKey();
                linkedList.add(this.stackTraceTrimmingStrategy.getTrimmedStackTrace((StackTraceElement[]) next.getValue()));
                i2 = i3 + 1;
            }
        } else {
            threadArr = new Thread[0];
        }
        if (!CommonUtils.a(context, COLLECT_CUSTOM_KEYS, true)) {
            treeMap = new TreeMap<>();
        } else {
            Map<String, String> attributes = this.crashlyticsCore.getAttributes();
            treeMap = (attributes == null || attributes.size() <= 1) ? attributes : new TreeMap<>(attributes);
        }
        SessionProtobufHelper.writeSessionEvent(codedOutputStream, time, str, trimmedThrowableData, thread, stackTraceElementArr, threadArr, linkedList, treeMap, this.logFileManager, a3, i, c3, str2, c2, a2, d2, b2, b3);
    }

    private void writeSessionPartsToSessionFile(File file, String str, int i) {
        boolean z;
        Fabric.h().a(CrashlyticsCore.TAG, "Collecting session parts for ID " + str);
        File[] listFilesMatching = listFilesMatching(new FileNameContainsFilter(str + SESSION_FATAL_TAG));
        boolean z2 = listFilesMatching != null && listFilesMatching.length > 0;
        Fabric.h().a(CrashlyticsCore.TAG, String.format(Locale.US, "Session %s has fatal exception: %s", str, Boolean.valueOf(z2)));
        File[] listFilesMatching2 = listFilesMatching(new FileNameContainsFilter(str + SESSION_NON_FATAL_TAG));
        if (listFilesMatching2 == null || listFilesMatching2.length <= 0) {
            z = false;
        } else {
            z = true;
        }
        Fabric.h().a(CrashlyticsCore.TAG, String.format(Locale.US, "Session %s has non-fatal exceptions: %s", str, Boolean.valueOf(z)));
        if (z2 || z) {
            synthesizeSessionFile(file, str, getTrimmedNonFatalFiles(str, listFilesMatching2, i), z2 ? listFilesMatching[0] : null);
        } else {
            Fabric.h().a(CrashlyticsCore.TAG, "No events present for session ID " + str);
        }
        Fabric.h().a(CrashlyticsCore.TAG, "Removing session part files for ID " + str);
        deleteSessionPartFilesFor(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [com.crashlytics.android.core.ClsFileOutputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    private void synthesizeSessionFile(File file, String str, File[] fileArr, File file2) {
        ClsFileOutputStream clsFileOutputStream;
        boolean z = file2 != null;
        File fatalSessionFilesDir = z ? getFatalSessionFilesDir() : getNonFatalSessionFilesDir();
        if (!fatalSessionFilesDir.exists()) {
            fatalSessionFilesDir.mkdirs();
        }
        try {
            clsFileOutputStream = new ClsFileOutputStream(fatalSessionFilesDir, str);
            try {
                CodedOutputStream newInstance = CodedOutputStream.newInstance(clsFileOutputStream);
                Fabric.h().a(CrashlyticsCore.TAG, "Collecting SessionStart data for session ID " + str);
                writeToCosFromFile(newInstance, file);
                newInstance.writeUInt64(4, new Date().getTime() / 1000);
                newInstance.writeBool(5, z);
                newInstance.writeUInt32(11, 1);
                newInstance.writeEnum(12, 3);
                writeInitialPartsTo(newInstance, str);
                writeNonFatalEventsTo(newInstance, fileArr, str);
                if (z) {
                    writeToCosFromFile(newInstance, file2);
                }
                CommonUtils.a(newInstance, "Error flushing session file stream");
                CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close CLS file");
            } catch (Exception e2) {
                e = e2;
                try {
                    Fabric.h().e(CrashlyticsCore.TAG, "Failed to write session file for session ID: " + str, e);
                    CommonUtils.a((Flushable) null, "Error flushing session file stream");
                    closeWithoutRenamingOrLog(clsFileOutputStream);
                } catch (Throwable th) {
                    th = th;
                    CommonUtils.a((Flushable) null, "Error flushing session file stream");
                    CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close CLS file");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            clsFileOutputStream = null;
            Fabric.h().e(CrashlyticsCore.TAG, "Failed to write session file for session ID: " + str, e);
            CommonUtils.a((Flushable) null, "Error flushing session file stream");
            closeWithoutRenamingOrLog(clsFileOutputStream);
        } catch (Throwable th2) {
            th = th2;
            clsFileOutputStream = null;
            CommonUtils.a((Flushable) null, "Error flushing session file stream");
            CommonUtils.a((Closeable) clsFileOutputStream, "Failed to close CLS file");
            throw th;
        }
    }

    private static void writeNonFatalEventsTo(CodedOutputStream codedOutputStream, File[] fileArr, String str) {
        Arrays.sort(fileArr, CommonUtils.f7116a);
        for (File file : fileArr) {
            try {
                Fabric.h().a(CrashlyticsCore.TAG, String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", str, file.getName()));
                writeToCosFromFile(codedOutputStream, file);
            } catch (Exception e2) {
                Fabric.h().e(CrashlyticsCore.TAG, "Error writting non-fatal to session.", e2);
            }
        }
    }

    private void writeInitialPartsTo(CodedOutputStream codedOutputStream, String str) {
        for (String str2 : INITIAL_SESSION_PART_TAGS) {
            File[] listFilesMatching = listFilesMatching(new FileNameContainsFilter(str + str2));
            if (listFilesMatching.length == 0) {
                Fabric.h().e(CrashlyticsCore.TAG, "Can't find " + str2 + " data for session ID " + str, null);
            } else {
                Fabric.h().a(CrashlyticsCore.TAG, "Collecting " + str2 + " data for session ID " + str);
                writeToCosFromFile(codedOutputStream, listFilesMatching[0]);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.FileInputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    private static void writeToCosFromFile(CodedOutputStream codedOutputStream, File file) {
        FileInputStream fileInputStream;
        if (!file.exists()) {
            Fabric.h().e(CrashlyticsCore.TAG, "Tried to include a file that doesn't exist: " + file.getName(), null);
            return;
        }
        try {
            fileInputStream = new FileInputStream(file);
            try {
                copyToCodedOutputStream(fileInputStream, codedOutputStream, (int) file.length());
                CommonUtils.a((Closeable) fileInputStream, "Failed to close file input stream.");
            } catch (Throwable th) {
                th = th;
                CommonUtils.a((Closeable) fileInputStream, "Failed to close file input stream.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            CommonUtils.a((Closeable) fileInputStream, "Failed to close file input stream.");
            throw th;
        }
    }

    private static void copyToCodedOutputStream(InputStream inputStream, CodedOutputStream codedOutputStream, int i) {
        int read;
        byte[] bArr = new byte[i];
        int i2 = 0;
        while (i2 < bArr.length && (read = inputStream.read(bArr, i2, bArr.length - i2)) >= 0) {
            i2 += read;
        }
        codedOutputStream.writeRawBytes(bArr);
    }

    private UserMetaData getUserMetaData(String str) {
        if (isHandlingException()) {
            return new UserMetaData(this.crashlyticsCore.getUserIdentifier(), this.crashlyticsCore.getUserName(), this.crashlyticsCore.getUserEmail());
        }
        return new MetaDataStore(getFilesDir()).readUserData(str);
    }

    /* access modifiers changed from: package-private */
    public boolean isHandlingException() {
        return this.crashHandler != null && this.crashHandler.isHandlingException();
    }

    /* access modifiers changed from: package-private */
    public File getFilesDir() {
        return this.fileStore.a();
    }

    /* access modifiers changed from: package-private */
    public File getFatalSessionFilesDir() {
        return new File(getFilesDir(), FATAL_SESSION_DIR);
    }

    /* access modifiers changed from: package-private */
    public File getNonFatalSessionFilesDir() {
        return new File(getFilesDir(), NONFATAL_SESSION_DIR);
    }

    /* access modifiers changed from: package-private */
    public File getInvalidFilesDir() {
        return new File(getFilesDir(), INVALID_CLS_CACHE_DIR);
    }

    /* access modifiers changed from: private */
    public boolean shouldPromptUserBeforeSendingCrashReports(s sVar) {
        if (sVar != null && sVar.f7334d.f7304a && !this.preferenceManager.shouldAlwaysSendReports()) {
            return true;
        }
        return false;
    }

    private CreateReportSpiCall getCreateReportSpiCall(String str) {
        return new DefaultCreateReportSpiCall(this.crashlyticsCore, CommonUtils.b(this.crashlyticsCore.getContext(), CRASHLYTICS_API_ENDPOINT), str, this.httpRequestFactory);
    }

    /* access modifiers changed from: private */
    public void sendSessionReports(s sVar) {
        if (sVar == null) {
            Fabric.h().d(CrashlyticsCore.TAG, "Cannot send reports. Settings are unavailable.");
            return;
        }
        Context context = this.crashlyticsCore.getContext();
        ReportUploader reportUploader = new ReportUploader(this.appData.apiKey, getCreateReportSpiCall(sVar.f7331a.f7291d), this.reportFilesProvider, this.handlingExceptionCheck);
        for (File sessionReport : listCompleteSessionFiles()) {
            this.backgroundWorker.submit(new SendReportRunnable(context, new SessionReport(sessionReport, SEND_AT_CRASHTIME_HEADER), reportUploader));
        }
    }

    private static void recordLoggedExceptionAnswersEvent(String str, String str2) {
        Answers answers = (Answers) Fabric.a(Answers.class);
        if (answers == null) {
            Fabric.h().a(CrashlyticsCore.TAG, "Answers is not available");
        } else {
            answers.onException(new f.b(str, str2));
        }
    }

    private static void recordFatalExceptionAnswersEvent(String str, String str2) {
        Answers answers = (Answers) Fabric.a(Answers.class);
        if (answers == null) {
            Fabric.h().a(CrashlyticsCore.TAG, "Answers is not available");
        } else {
            answers.onException(new f.a(str, str2));
        }
    }

    private final class ReportUploaderHandlingExceptionCheck implements ReportUploader.HandlingExceptionCheck {
        private ReportUploaderHandlingExceptionCheck() {
        }

        public boolean isHandlingException() {
            return CrashlyticsController.this.isHandlingException();
        }
    }

    private final class ReportUploaderFilesProvider implements ReportUploader.ReportFilesProvider {
        private ReportUploaderFilesProvider() {
        }

        public File[] getCompleteSessionFiles() {
            return CrashlyticsController.this.listCompleteSessionFiles();
        }

        public File[] getInvalidSessionFiles() {
            return CrashlyticsController.this.getInvalidFilesDir().listFiles();
        }
    }

    private static final class PrivacyDialogCheck implements ReportUploader.SendCheck {
        private final io.fabric.sdk.android.f kit;
        /* access modifiers changed from: private */
        public final PreferenceManager preferenceManager;
        private final o promptData;

        public PrivacyDialogCheck(io.fabric.sdk.android.f fVar, PreferenceManager preferenceManager2, o oVar) {
            this.kit = fVar;
            this.preferenceManager = preferenceManager2;
            this.promptData = oVar;
        }

        public boolean canSendReports() {
            Activity b2 = this.kit.getFabric().b();
            if (b2 == null || b2.isFinishing()) {
                return true;
            }
            final CrashPromptDialog create = CrashPromptDialog.create(b2, this.promptData, new CrashPromptDialog.AlwaysSendCallback() {
                public void sendUserReportsWithoutPrompting(boolean z) {
                    PrivacyDialogCheck.this.preferenceManager.setShouldAlwaysSendReports(z);
                }
            });
            b2.runOnUiThread(new Runnable() {
                public void run() {
                    create.show();
                }
            });
            Fabric.h().a(CrashlyticsCore.TAG, "Waiting for user opt-in.");
            create.await();
            return create.getOptIn();
        }
    }

    private static final class SendReportRunnable implements Runnable {
        private final Context context;
        private final Report report;
        private final ReportUploader reportUploader;

        public SendReportRunnable(Context context2, Report report2, ReportUploader reportUploader2) {
            this.context = context2;
            this.report = report2;
            this.reportUploader = reportUploader2;
        }

        public void run() {
            if (CommonUtils.n(this.context)) {
                Fabric.h().a(CrashlyticsCore.TAG, "Attempting to send crash report at time of crash...");
                this.reportUploader.forceUpload(this.report);
            }
        }
    }

    private static final class LogFileDirectoryProvider implements LogFileManager.DirectoryProvider {
        private static final String LOG_FILES_DIR = "log-files";
        private final a rootFileStore;

        public LogFileDirectoryProvider(a aVar) {
            this.rootFileStore = aVar;
        }

        public File getLogFileDir() {
            File file = new File(this.rootFileStore.a(), LOG_FILES_DIR);
            if (!file.exists()) {
                file.mkdirs();
            }
            return file;
        }
    }
}
