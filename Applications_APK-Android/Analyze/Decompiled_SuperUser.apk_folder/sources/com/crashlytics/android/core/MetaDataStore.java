package com.crashlytics.android.core;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

class MetaDataStore {
    private static final String KEYDATA_SUFFIX = "keys";
    private static final String KEY_USER_EMAIL = "userEmail";
    private static final String KEY_USER_ID = "userId";
    private static final String KEY_USER_NAME = "userName";
    private static final String METADATA_EXT = ".meta";
    private static final String USERDATA_SUFFIX = "user";
    private static final Charset UTF_8 = Charset.forName("UTF-8");
    private final File filesDir;

    public MetaDataStore(File file) {
        this.filesDir = file;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.BufferedWriter, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    public void writeUserData(String str, UserMetaData userMetaData) {
        BufferedWriter bufferedWriter;
        File userDataFileForSession = getUserDataFileForSession(str);
        BufferedWriter bufferedWriter2 = null;
        try {
            String userDataToJson = userDataToJson(userMetaData);
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(userDataFileForSession), UTF_8));
            try {
                bufferedWriter.write(userDataToJson);
                bufferedWriter.flush();
                CommonUtils.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
            } catch (Exception e2) {
                e = e2;
                try {
                    Fabric.h().e(CrashlyticsCore.TAG, "Error serializing user metadata.", e);
                    CommonUtils.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
                } catch (Throwable th) {
                    th = th;
                    bufferedWriter2 = bufferedWriter;
                    CommonUtils.a((Closeable) bufferedWriter2, "Failed to close user metadata file.");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            bufferedWriter = null;
            Fabric.h().e(CrashlyticsCore.TAG, "Error serializing user metadata.", e);
            CommonUtils.a((Closeable) bufferedWriter, "Failed to close user metadata file.");
        } catch (Throwable th2) {
            th = th2;
            CommonUtils.a((Closeable) bufferedWriter2, "Failed to close user metadata file.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.FileInputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    public UserMetaData readUserData(String str) {
        FileInputStream fileInputStream;
        File userDataFileForSession = getUserDataFileForSession(str);
        if (!userDataFileForSession.exists()) {
            return UserMetaData.EMPTY;
        }
        try {
            fileInputStream = new FileInputStream(userDataFileForSession);
            try {
                UserMetaData jsonToUserData = jsonToUserData(CommonUtils.a((InputStream) fileInputStream));
                CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                return jsonToUserData;
            } catch (Exception e2) {
                e = e2;
                try {
                    Fabric.h().e(CrashlyticsCore.TAG, "Error deserializing user metadata.", e);
                    CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    return UserMetaData.EMPTY;
                } catch (Throwable th) {
                    th = th;
                    CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            Fabric.h().e(CrashlyticsCore.TAG, "Error deserializing user metadata.", e);
            CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            return UserMetaData.EMPTY;
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.BufferedWriter, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    public void writeKeyData(String str, Map<String, String> map) {
        BufferedWriter bufferedWriter;
        File keysFileForSession = getKeysFileForSession(str);
        BufferedWriter bufferedWriter2 = null;
        try {
            String keysDataToJson = keysDataToJson(map);
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(keysFileForSession), UTF_8));
            try {
                bufferedWriter.write(keysDataToJson);
                bufferedWriter.flush();
                CommonUtils.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
            } catch (Exception e2) {
                e = e2;
                try {
                    Fabric.h().e(CrashlyticsCore.TAG, "Error serializing key/value metadata.", e);
                    CommonUtils.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                } catch (Throwable th) {
                    th = th;
                    bufferedWriter2 = bufferedWriter;
                    CommonUtils.a((Closeable) bufferedWriter2, "Failed to close key/value metadata file.");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            bufferedWriter = null;
            Fabric.h().e(CrashlyticsCore.TAG, "Error serializing key/value metadata.", e);
            CommonUtils.a((Closeable) bufferedWriter, "Failed to close key/value metadata file.");
        } catch (Throwable th2) {
            th = th2;
            CommonUtils.a((Closeable) bufferedWriter2, "Failed to close key/value metadata file.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.FileInputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    public Map<String, String> readKeyData(String str) {
        FileInputStream fileInputStream;
        File keysFileForSession = getKeysFileForSession(str);
        if (!keysFileForSession.exists()) {
            return Collections.emptyMap();
        }
        try {
            fileInputStream = new FileInputStream(keysFileForSession);
            try {
                Map<String, String> jsonToKeysData = jsonToKeysData(CommonUtils.a((InputStream) fileInputStream));
                CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                return jsonToKeysData;
            } catch (Exception e2) {
                e = e2;
                try {
                    Fabric.h().e(CrashlyticsCore.TAG, "Error deserializing user metadata.", e);
                    CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    return Collections.emptyMap();
                } catch (Throwable th) {
                    th = th;
                    CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            Fabric.h().e(CrashlyticsCore.TAG, "Error deserializing user metadata.", e);
            CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            return Collections.emptyMap();
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            CommonUtils.a((Closeable) fileInputStream, "Failed to close user metadata file.");
            throw th;
        }
    }

    private File getUserDataFileForSession(String str) {
        return new File(this.filesDir, str + "user" + METADATA_EXT);
    }

    private File getKeysFileForSession(String str) {
        return new File(this.filesDir, str + KEYDATA_SUFFIX + METADATA_EXT);
    }

    private static UserMetaData jsonToUserData(String str) {
        JSONObject jSONObject = new JSONObject(str);
        return new UserMetaData(valueOrNull(jSONObject, "userId"), valueOrNull(jSONObject, KEY_USER_NAME), valueOrNull(jSONObject, KEY_USER_EMAIL));
    }

    private static String userDataToJson(final UserMetaData userMetaData) {
        return new JSONObject() {
            {
                put("userId", userMetaData.id);
                put(MetaDataStore.KEY_USER_NAME, userMetaData.name);
                put(MetaDataStore.KEY_USER_EMAIL, userMetaData.email);
            }
        }.toString();
    }

    private static Map<String, String> jsonToKeysData(String str) {
        JSONObject jSONObject = new JSONObject(str);
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, valueOrNull(jSONObject, next));
        }
        return hashMap;
    }

    private static String keysDataToJson(Map<String, String> map) {
        return new JSONObject(map).toString();
    }

    private static String valueOrNull(JSONObject jSONObject, String str) {
        if (!jSONObject.isNull(str)) {
            return jSONObject.optString(str, null);
        }
        return null;
    }
}
