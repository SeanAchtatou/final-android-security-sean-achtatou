package com.paypal.android.sdk;

import android.os.Build;
import com.google.android.gms.common.Scopes;
import com.kingouser.com.util.Settings;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public final class eg extends eh {

    /* renamed from: a  reason: collision with root package name */
    public el f4800a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f4801b;

    /* renamed from: c  reason: collision with root package name */
    public String f4802c;

    /* renamed from: d  reason: collision with root package name */
    public String f4803d;

    /* renamed from: e  reason: collision with root package name */
    public String f4804e;

    /* renamed from: f  reason: collision with root package name */
    public long f4805f;
    private String j;
    private final boolean k;
    private final String l;
    private String m;
    private String n;

    public eg(bu buVar, x xVar, String str, String str2, el elVar, String str3, String str4, boolean z, String str5, boolean z2, String str6) {
        this(buVar, xVar, str, str2, elVar, z, str5, z2, str6);
        this.m = str3;
        this.n = str4;
    }

    public eg(bu buVar, x xVar, String str, String str2, el elVar, boolean z, String str3, boolean z2, String str4) {
        super(db.LoginRequest, buVar, xVar, b(str, str2));
        this.f4800a = elVar;
        this.f4801b = z;
        this.j = str3;
        this.k = z2;
        this.l = str4;
    }

    public final String b() {
        HashMap hashMap = new HashMap();
        hashMap.put("response_type", this.j);
        if (this.j != null && this.j.equals("token")) {
            hashMap.put("scope_consent_context", "access_token");
            if (!cd.a((CharSequence) this.l)) {
                hashMap.put("scope", this.l);
            }
        }
        hashMap.put("risk_data", cd.a(r.a().c().toString()));
        if (this.m != null) {
            hashMap.put("grant_type", "urn:paypal:params:oauth2:grant_type:otp");
            hashMap.put("nonce", this.n);
            JSONObject jSONObject = new JSONObject();
            jSONObject.accumulate("token_identifier", "otp");
            jSONObject.accumulate("token_value", this.m);
            hashMap.put("2fa_token_claims", a(jSONObject));
        } else if (this.f4800a.a()) {
            hashMap.put("grant_type", "password");
            hashMap.put(Scopes.EMAIL, cd.a(this.f4800a.b()));
            hashMap.put("password", cd.a(this.f4800a.c()));
        } else {
            hashMap.put("grant_type", "password");
            this.f4800a.d().c();
            hashMap.put("phone", cd.a("+" + this.f4800a.d().c() + " " + this.f4800a.d().a()));
            hashMap.put(Settings.KEY_PIN, this.f4800a.e());
        }
        hashMap.put("device_name", cd.a(Build.DEVICE));
        hashMap.put("redirect_uri", cd.a("urn:ietf:wg:oauth:2.0:oob"));
        return cd.a(hashMap);
    }

    public final void c() {
        JSONObject m2 = m();
        try {
            m2.getString("scope");
            this.f4804e = m2.getString("scope");
            if (this.k) {
                this.f4802c = m2.getString("code");
                this.f4807g = m2.getString("nonce");
                return;
            }
            this.f4803d = m2.getString("access_token");
            this.f4805f = m2.getLong("expires_in");
        } catch (JSONException e2) {
            b(m2);
        }
    }

    public final void d() {
        b(m());
    }

    public final String e() {
        return "{ \"access_token\": \"mock_access_token\", \"code\": \"mock_code_EJhi9jOPswug9TDOv93qg4Y28xIlqPDpAoqd7biDLpeGCPvORHjP1Fh4CbFPgKMGCHejdDwe9w1uDWnjPCp1lkaFBjVmjvjpFtnr6z1YeBbmfZYqa9faQT_71dmgZhMIFVkbi4yO7hk0LBHXt_wtdsw\", \"scope\": \"https://api.paypal.com/v1/payments/.*\", \"nonce\": \"mock_nonce\", \"token_type\": \"Bearer\",\"expires_in\":28800,\"visitor_id\":\"zVxjDBTRRNfYXdOb19-lcTblxm-6bzXGvSlP76ZiHOudKaAvoxrW8Cg5pA6EjIPpiz4zlw\" }";
    }

    public final boolean t() {
        return this.m != null;
    }
}
