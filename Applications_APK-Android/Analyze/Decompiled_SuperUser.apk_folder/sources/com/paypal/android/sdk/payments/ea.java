package com.paypal.android.sdk.payments;

import android.content.Intent;
import android.util.Log;

abstract class ea {

    /* renamed from: a  reason: collision with root package name */
    Intent f5301a;

    /* renamed from: b  reason: collision with root package name */
    PayPalConfiguration f5302b;

    ea(Intent intent, PayPalConfiguration payPalConfiguration) {
        this.f5301a = intent;
        this.f5302b = payPalConfiguration;
        if (!this.f5301a.hasExtra("com.paypal.android.sdk.paypalConfiguration")) {
            Log.w(b(), "Please add PayPalService.EXTRA_PAYPAL_CONFIGURATION to this activity for restart resiliency.");
        }
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z, String str) {
        if (!z) {
            Log.e(b(), str + " is invalid.  Please see the docs.");
        }
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a();

    /* access modifiers changed from: package-private */
    public abstract String b();

    /* access modifiers changed from: protected */
    public final boolean c() {
        if (this.f5302b.o()) {
            return true;
        }
        Log.e(b(), "Service extra invalid.");
        return false;
    }
}
