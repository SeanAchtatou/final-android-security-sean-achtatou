package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.SpannableString;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.paypal.android.sdk.Cdo;
import com.paypal.android.sdk.cd;
import com.paypal.android.sdk.ce;
import com.paypal.android.sdk.cl;
import com.paypal.android.sdk.cy;
import com.paypal.android.sdk.dn;
import com.paypal.android.sdk.dp;
import com.paypal.android.sdk.ds;
import com.paypal.android.sdk.dz;
import com.paypal.android.sdk.en;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.ew;
import com.paypal.android.sdk.ex;
import com.paypal.android.sdk.ey;
import com.paypal.android.sdk.fc;
import com.paypal.android.sdk.ff;
import com.paypal.android.sdk.fg;
import com.paypal.android.sdk.fs;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

public final class PaymentConfirmActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f5122a = PaymentConfirmActivity.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private cm f5123b;

    /* renamed from: c  reason: collision with root package name */
    private dx f5124c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f5125d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f5126e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public boolean f5127f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public fc f5128g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public bt f5129h;
    /* access modifiers changed from: private */
    public dh i;
    private Parcelable j;
    /* access modifiers changed from: private */
    public PayPalService k;
    private final ServiceConnection l = new cb(this);
    private boolean m;

    private static en a(PayPalPayment payPalPayment) {
        return new en(new BigDecimal(cy.a(payPalPayment.c().doubleValue(), payPalPayment.f()).trim()), payPalPayment.f());
    }

    private void a(int i2) {
        setResult(i2, new Intent());
    }

    static void a(Activity activity, int i2, dh dhVar, Parcelable parcelable, PayPalConfiguration payPalConfiguration) {
        a(activity, 2, dhVar, null, payPalConfiguration, false);
    }

    static void a(Activity activity, int i2, dh dhVar, Parcelable parcelable, PayPalConfiguration payPalConfiguration, boolean z) {
        Intent intent = new Intent(activity, PaymentConfirmActivity.class);
        intent.putExtras(activity.getIntent());
        intent.putExtra("com.paypal.android.sdk.payments.PaymentConfirmActivity.EXTRA_PAYMENT_KIND", dhVar);
        intent.putExtra("com.paypal.android.sdk.payments.PaymentConfirmActivity.EXTRA_CREDIT_CARD", parcelable);
        intent.putExtra("com.paypal.android.sdk.payments.PaymentConfirmActivity.EXTRA_RESET_PP_REQUEST_ID", z);
        intent.putExtra("com.paypal.android.sdk.paypalConfiguration", payPalConfiguration);
        activity.startActivityForResult(intent, i2);
    }

    private void a(Bundle bundle) {
        String string = bundle.getString("authAccount");
        String string2 = bundle.getString("authtoken");
        String string3 = bundle.getString("scope");
        long j2 = bundle.getLong("valid_until");
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (obj == null) {
                String.format("%s:null", next);
            } else {
                String.format("%s:%s (%s)", next, obj.toString(), obj.getClass().getName());
            }
        }
        ds dsVar = new ds(string2, string3, j2, false);
        if (this.k == null) {
            this.f5123b = new cm(this, string, dsVar);
        } else {
            a(string, dsVar);
        }
    }

    static /* synthetic */ void a(PaymentConfirmActivity paymentConfirmActivity, dz dzVar) {
        paymentConfirmActivity.f5124c = new dx(dzVar, paymentConfirmActivity.f5129h.a().o());
        paymentConfirmActivity.getIntent().putExtra("com.paypal.android.sdk.payments.PaymentConfirmActivity.EXTRA_PAYMENT_INFO", paymentConfirmActivity.f5124c);
        paymentConfirmActivity.b();
        paymentConfirmActivity.j();
    }

    static /* synthetic */ void a(PaymentConfirmActivity paymentConfirmActivity, List list, int i2) {
        paymentConfirmActivity.f5129h.b().a(i2);
        paymentConfirmActivity.f5128g.a(paymentConfirmActivity, (ew) list.get(i2));
    }

    private void a(String str) {
        this.f5128g.a(str);
    }

    private void a(String str, ds dsVar) {
        this.k.c().f4666c = str;
        a(str);
        this.k.c().f4670g = dsVar;
        if (this.i != dh.PayPal) {
            this.f5128g.b(true);
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        new StringBuilder().append(f5122a).append(".doLogin");
        if (dl.a(this, this.k)) {
            Intent a2 = new cl().a(this.k.d().k(), z ? dn.PROMPT_LOGIN : dn.USER_REQUIRED, Cdo.token, this.k.b().d().e());
            a2.putExtra("scope", "https://uri.paypal.com/services/payments/basic");
            new StringBuilder("startActivityForResult(").append(a2).append(", 2").append(")");
            Log.w("paypal.sdk", "requesting " + a2.getStringExtra("response_type") + " with scope={" + a2.getStringExtra("scope") + "} from Authenticator.");
            startActivityForResult(a2, 2);
            return;
        }
        LoginActivity.a(this, 1, this.k.q(), false, z, "https://uri.paypal.com/services/payments/basic", this.k.d());
    }

    private static Map b(PayPalPayment payPalPayment) {
        if (payPalPayment != null) {
            HashMap hashMap = new HashMap();
            PayPalPaymentDetails i2 = payPalPayment.i();
            if (i2 != null) {
                if (i2.c() != null) {
                    hashMap.put(FirebaseAnalytics.Param.SHIPPING, cy.a(i2.c().doubleValue(), payPalPayment.f()));
                }
                if (i2.b() != null) {
                    hashMap.put("subtotal", cy.a(i2.b().doubleValue(), payPalPayment.f()));
                }
                if (i2.d() != null) {
                    hashMap.put(FirebaseAnalytics.Param.TAX, cy.a(i2.d().doubleValue(), payPalPayment.f()));
                }
            }
            if (!hashMap.isEmpty()) {
                return hashMap;
            }
        }
        return null;
    }

    private void b() {
        if (this.f5124c != null) {
            JSONObject jSONObject = null;
            if (this.f5124c.b() != null) {
                jSONObject = this.f5124c.b().a();
            }
            int h2 = this.f5124c.h();
            ArrayList a2 = ff.a(jSONObject, this.f5124c.a(), this.f5124c.i());
            if (this.f5129h.a().a() || a2 == null || a2.size() <= 0) {
                this.f5128g.f().setClickable(false);
                this.f5128g.f().setVisibility(8);
            } else {
                this.f5128g.f().setVisibility(0);
                this.f5128g.f().setClickable(true);
                this.f5128g.a(getApplicationContext(), (ff) a2.get(h2));
                fg fgVar = new fg(this, a2, h2);
                new ListView(this).setAdapter((ListAdapter) fgVar);
                this.f5128g.d(new cg(this, fgVar, a2));
            }
            int g2 = this.f5124c.g();
            ArrayList a3 = ew.a(this.f5124c.c(), this.f5124c.d());
            if (a3 == null || a3.size() <= 0) {
                this.f5128g.e().setClickable(false);
                this.f5128g.e().setVisibility(8);
            } else {
                this.f5128g.e().setVisibility(0);
                this.f5128g.e().setClickable(true);
                this.f5128g.a(getApplicationContext(), (ew) a3.get(g2));
                ex exVar = new ex(this, a3, g2);
                new ListView(this).setAdapter((ListAdapter) exVar);
                this.f5128g.c(new cd(this, exVar, a3));
            }
            this.f5128g.b(true);
        }
    }

    static /* synthetic */ void b(PaymentConfirmActivity paymentConfirmActivity) {
        new StringBuilder().append(f5122a).append(".postBindSetup()");
        if (paymentConfirmActivity.i.equals(dh.PayPal)) {
            paymentConfirmActivity.f5128g.a(cd.b(paymentConfirmActivity.k.d().a()));
        } else {
            paymentConfirmActivity.f5128g.a((SpannableString) null);
        }
        if (paymentConfirmActivity.f5123b != null) {
            paymentConfirmActivity.a(paymentConfirmActivity.f5123b.f5241a, paymentConfirmActivity.f5123b.f5242b);
            paymentConfirmActivity.f5123b = null;
        }
        if (paymentConfirmActivity.getIntent().getBooleanExtra("com.paypal.android.sdk.payments.PaymentConfirmActivity.EXTRA_RESET_PP_REQUEST_ID", false)) {
            paymentConfirmActivity.k.c().a();
        }
        boolean e2 = paymentConfirmActivity.e();
        if (!paymentConfirmActivity.f5125d) {
            paymentConfirmActivity.f5125d = true;
            paymentConfirmActivity.k.a(ey.ConfirmPaymentWindow);
        }
        paymentConfirmActivity.f();
        paymentConfirmActivity.k.b(new ci(paymentConfirmActivity));
        if (dh.PayPal == paymentConfirmActivity.i && !e2 && !paymentConfirmActivity.f5127f && paymentConfirmActivity.f5124c == null) {
            paymentConfirmActivity.i();
        }
    }

    static /* synthetic */ void b(PaymentConfirmActivity paymentConfirmActivity, List list, int i2) {
        paymentConfirmActivity.f5129h.b().b(i2);
        paymentConfirmActivity.f5128g.a(paymentConfirmActivity, (ff) list.get(i2));
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.k.c().f4670g != null && !this.k.c().f4670g.a()) {
            this.k.c().f4670g = null;
            this.k.c().f4666c = null;
        }
    }

    private void d() {
        this.m = bindService(cf.b(this), this.l, 1);
    }

    private boolean e() {
        if (!this.i.equals(dh.PayPal) || this.k.j() || this.f5126e) {
            return false;
        }
        this.f5126e = true;
        a(false);
        return true;
    }

    private void f() {
        String d2;
        int f2;
        int g2;
        Enum a2;
        PayPalPayment a3 = this.f5129h.a();
        this.f5128g.a(a3.d(), cy.a(Locale.getDefault(), ce.a().c().a(), a3.c().doubleValue(), a3.f(), true));
        if (this.i == dh.PayPal) {
            this.f5128g.a(true);
            a(this.k.r());
        } else if (this.i == dh.CreditCard || this.i == dh.CreditCardToken) {
            this.f5128g.a(false);
            if (this.i == dh.CreditCard) {
                d2 = dp.a(cf.a(this.j));
                f2 = cf.b(this.j, "expiryMonth");
                g2 = cf.b(this.j, "expiryYear");
                a2 = cf.b(this.j);
            } else {
                dp s = this.k.s();
                d2 = s.d();
                f2 = s.f();
                g2 = s.g();
                a2 = cf.a(s);
            }
            this.f5128g.a(d2, cf.a(this, a2), String.format(Locale.getDefault(), "%02d / %04d", Integer.valueOf(f2), Integer.valueOf(g2)));
        } else {
            Log.wtf(f5122a, "Unknown payment type: " + this.i.toString());
            cf.a(this, "The payment is not a valid type. Please try again.", 3);
        }
        cf.a(this.f5128g.d(), this.k.e());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void
     arg types: [com.paypal.android.sdk.payments.bi, int]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.payments.bi):com.paypal.android.sdk.payments.bi
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.bt):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, boolean):boolean
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.Boolean):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(java.lang.String, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void */
    /* access modifiers changed from: private */
    public void g() {
        boolean z;
        switch (by.f5217a[this.i.ordinal()]) {
            case 1:
                if (e()) {
                    z = false;
                    break;
                } else {
                    z = true;
                    break;
                }
            case 2:
            case 3:
                if (!this.k.i()) {
                    showDialog(2);
                    new StringBuilder("token is expired, get new one. AccessToken: ").append(this.k.c().f4665b);
                    this.k.a(h(), true);
                    z = false;
                    break;
                }
            default:
                z = true;
                break;
        }
        if (z) {
            showDialog(2);
            if (this.f5129h == null || this.f5129h.a() == null || (this.i == dh.PayPal && this.f5129h.b() == null)) {
                onBackPressed();
                return;
            }
            PayPalPayment a2 = this.f5129h.a();
            en a3 = a(a2);
            Map b2 = b(a2);
            String d2 = a2.d();
            boolean j2 = this.k.d().j();
            switch (by.f5217a[this.i.ordinal()]) {
                case 1:
                    dx b3 = this.f5129h.b();
                    this.k.a(j2, b3.e(), b3.f(), b3.k() ? b3.m() : null, b3.j() ? b3.l() : null, a2.g());
                    return;
                case 2:
                    dp s = this.k.s();
                    this.k.a(this.k.c().b(), s.e(), a3, b2, a2.j(), d2, j2, this.k.a(s.a()), a2.g(), a2.e().toString(), a2.k(), a2.l(), a2.m());
                    return;
                case 3:
                    this.k.a(this.k.c().b(), cf.b(this.j).name().toLowerCase(Locale.US), cf.a(this.j, "cardNumber"), cf.a(this.j, "cvv"), cf.b(this.j, "expiryMonth"), cf.b(this.j, "expiryYear"), a3, b2, a2.j(), d2, j2, a2.g(), a2.e().toString(), a2.k(), a2.l(), a2.m());
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public bi h() {
        return new bx(this);
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.k != null && this.k.c().f4670g != null) {
            showDialog(2);
            PayPalPayment a2 = this.f5129h.a();
            this.k.a(a(a2), b(a2), a2.j(), a2.d(), this.k.d().j(), a2.g(), a2.e().toString(), a2.n(), a2.k(), a2.l(), a2.m(), a2.a(), a2.h());
            this.f5127f = true;
            a(this.k.r());
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        try {
            dismissDialog(2);
        } catch (IllegalArgumentException e2) {
        }
    }

    public final void finish() {
        super.finish();
        new StringBuilder().append(f5122a).append(".finish");
    }

    /* access modifiers changed from: protected */
    public final void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        new StringBuilder().append(f5122a).append(".onActivityResult(requestCode:").append(i2).append(", resultCode:").append(i3).append(", data:").append(intent).append(")");
        switch (i2) {
            case 1:
                this.f5126e = false;
                if (i3 == -1) {
                    if (this.f5128g != null) {
                        this.f5128g.b(false);
                    }
                    if (this.k != null) {
                        i();
                        return;
                    }
                    return;
                }
                a(i3);
                finish();
                return;
            case 2:
                this.f5126e = false;
                if (i3 == -1) {
                    this.f5128g.b(true);
                    a(intent.getExtras());
                    if (this.k != null) {
                        i();
                        return;
                    }
                    return;
                }
                a(i3);
                finish();
                return;
            default:
                Log.e(f5122a, "unhandled requestCode " + i2);
                return;
        }
    }

    public final void onBackPressed() {
        this.k.a(ey.ConfirmPaymentCancel);
        c();
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new StringBuilder().append(f5122a).append(".onCreate");
        d();
        if (bundle == null) {
            if (!cf.a(this)) {
                finish();
            }
            this.f5125d = false;
        } else {
            this.f5125d = bundle.getBoolean("pageTrackingSent");
            this.f5126e = bundle.getBoolean("isLoginActivityInProgress");
            this.f5127f = bundle.getBoolean("isSFOPaymentRequestInProgress");
        }
        if (getIntent().getExtras() == null) {
            onBackPressed();
            return;
        }
        this.i = (dh) getIntent().getSerializableExtra("com.paypal.android.sdk.payments.PaymentConfirmActivity.EXTRA_PAYMENT_KIND");
        this.j = getIntent().getParcelableExtra("com.paypal.android.sdk.payments.PaymentConfirmActivity.EXTRA_CREDIT_CARD");
        this.f5129h = new bt(getIntent());
        setTheme(16973934);
        requestWindowFeature(8);
        this.f5128g = new fc(this, this.i == dh.PayPal);
        setContentView(this.f5128g.a());
        cf.a(this, this.f5128g.b(), fs.CONFIRM);
        this.f5128g.b(new bu(this));
        this.f5128g.a(new ca(this));
        if (dh.PayPal == this.i) {
            this.f5124c = (dx) getIntent().getParcelableExtra("com.paypal.android.sdk.payments.PaymentConfirmActivity.EXTRA_PAYMENT_INFO");
            b();
        }
    }

    /* access modifiers changed from: protected */
    public final Dialog onCreateDialog(int i2, Bundle bundle) {
        switch (i2) {
            case 1:
                return cf.a(this, fs.PAY_FAILED_ALERT_TITLE, bundle);
            case 2:
                return cf.a(this, fs.PROCESSING, fs.ONE_MOMENT);
            case 3:
                return cf.a(this, fs.INTERNAL_ERROR, bundle, i2);
            case 4:
                return cf.a(this, fs.SESSION_EXPIRED_TITLE, bundle, new cj(this));
            case 5:
                ev.a(fs.UNEXPECTED_PAYMENT_FLOW);
                if (bundle == null || !cd.b((CharSequence) bundle.getString("BUNDLE_ERROR_CODE"))) {
                    fs fsVar = fs.WE_ARE_SORRY;
                    fs fsVar2 = fs.UNEXPECTED_PAYMENT_FLOW;
                    fs fsVar3 = fs.TRY_AGAIN;
                    fs fsVar4 = fs.CANCEL;
                    bv bvVar = new bv(this);
                    return new AlertDialog.Builder(this).setIcon(17301543).setTitle(ev.a(fsVar)).setMessage(ev.a(fsVar2)).setPositiveButton(ev.a(fsVar3), bvVar).setNegativeButton(ev.a(fsVar4), new bw(this)).create();
                }
                String string = bundle.getString("BUNDLE_ERROR_CODE");
                fs fsVar5 = fs.WE_ARE_SORRY;
                String a2 = ev.a(string);
                fs fsVar6 = fs.TRY_AGAIN;
                fs fsVar7 = fs.CANCEL;
                ck ckVar = new ck(this);
                return new AlertDialog.Builder(this).setIcon(17301543).setTitle(ev.a(fsVar5)).setMessage(a2).setPositiveButton(ev.a(fsVar6), ckVar).setNegativeButton(ev.a(fsVar7), new cl(this)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        new StringBuilder().append(f5122a).append(".onDestroy");
        if (this.k != null) {
            this.k.m();
        }
        if (this.m) {
            unbindService(this.l);
            this.m = false;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void onRestart() {
        super.onRestart();
        d();
    }

    /* access modifiers changed from: protected */
    public final void onResume() {
        super.onResume();
        new StringBuilder().append(f5122a).append(".onResume");
        if (this.k != null) {
            f();
        }
    }

    /* access modifiers changed from: protected */
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("pageTrackingSent", this.f5125d);
        bundle.putBoolean("isLoginActivityInProgress", this.f5126e);
        bundle.putBoolean("isSFOPaymentRequestInProgress", this.f5127f);
    }

    public final void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        new StringBuilder().append(f5122a).append(".onWindowFocusChanged");
        this.f5128g.c();
    }
}
