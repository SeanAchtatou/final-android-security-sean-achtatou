package com.paypal.android.sdk;

import android.location.Location;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class q {
    public String A;
    public Boolean B;
    public String C;
    public String D;
    public Boolean E;
    public String F;
    public String G;
    public long H;
    public long I;
    public String J;
    public Boolean K;
    public Integer L;
    public int M = -1;
    public int N = -1;
    public String O;
    public int P = -1;
    public String Q;
    public Boolean R;
    public Boolean S;
    public String T;
    public long U;
    public long V;
    public String W;
    public String X;
    public String Y;
    public String Z;

    /* renamed from: a  reason: collision with root package name */
    public String f5340a;
    public String aa;
    public String ab;
    public String ac;
    public String ad;
    public String ae;
    public List af;
    public Map ag;
    private String ah = "full";

    /* renamed from: b  reason: collision with root package name */
    public String f5341b;

    /* renamed from: c  reason: collision with root package name */
    public String f5342c;

    /* renamed from: d  reason: collision with root package name */
    public int f5343d = -1;

    /* renamed from: e  reason: collision with root package name */
    public String f5344e;

    /* renamed from: f  reason: collision with root package name */
    public int f5345f = -1;

    /* renamed from: g  reason: collision with root package name */
    public String f5346g;

    /* renamed from: h  reason: collision with root package name */
    public String f5347h;
    public String i;
    public String j;
    public String k;
    public String l;
    public String m;
    public long n = -1;
    public String o;
    public List p;
    public List q;
    public String r;
    public String s;
    public String t;
    public Location u;
    public int v = -1;
    public String w;
    public String x = "Android";
    public String y;
    public String z;

    private static JSONObject a(Location location) {
        if (location == null) {
            return null;
        }
        try {
            return new JSONObject("{\"lat\":" + location.getLatitude() + ",\"lng\":" + location.getLongitude() + ",\"acc\":" + location.getAccuracy() + ",\"timestamp\":" + location.getTime() + "}");
        } catch (JSONException e2) {
            return null;
        }
    }

    private void a(JSONObject jSONObject) {
        if (this.ag != null) {
            for (Map.Entry entry : this.ag.entrySet()) {
                try {
                    jSONObject.put((String) entry.getKey(), entry.getValue());
                } catch (JSONException e2) {
                    aj.a((String) null, (String) null, e2);
                }
            }
        }
    }

    public final JSONObject a() {
        Integer num = null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("app_guid", this.f5340a);
            jSONObject.put("app_id", this.f5341b);
            jSONObject.put("app_version", this.f5342c);
            jSONObject.put("base_station_id", this.f5343d == -1 ? null : Integer.valueOf(this.f5343d));
            jSONObject.put("bssid", this.f5344e);
            jSONObject.put("bssid_array", this.af == null ? null : new JSONArray((Collection) this.af));
            jSONObject.put("cell_id", this.f5345f == -1 ? null : Integer.valueOf(this.f5345f));
            jSONObject.put("comp_version", this.f5346g);
            jSONObject.put("conf_url", this.f5347h);
            jSONObject.put("conf_version", this.i);
            jSONObject.put("conn_type", this.j);
            jSONObject.put("device_id", this.k);
            jSONObject.put("dc_id", this.ad);
            jSONObject.put("device_model", this.l);
            jSONObject.put("device_name", this.m);
            jSONObject.put("device_uptime", this.n == -1 ? null : Long.valueOf(this.n));
            jSONObject.put("ip_addrs", this.o);
            jSONObject.put("ip_addresses", this.p == null ? null : new JSONArray((Collection) this.p));
            jSONObject.put("known_apps", this.q == null ? null : new JSONArray((Collection) this.q));
            jSONObject.put("linker_id", this.r);
            jSONObject.put("locale_country", this.s);
            jSONObject.put("locale_lang", this.t);
            jSONObject.put(FirebaseAnalytics.Param.LOCATION, a(this.u));
            jSONObject.put("location_area_code", this.v == -1 ? null : Integer.valueOf(this.v));
            jSONObject.put("mac_addrs", this.w);
            jSONObject.put("os_type", this.x);
            jSONObject.put("os_version", this.y);
            jSONObject.put("payload_type", this.ah);
            jSONObject.put("phone_type", this.z);
            jSONObject.put("risk_comp_session_id", this.A);
            jSONObject.put("roaming", this.B);
            jSONObject.put("sim_operator_name", "".equals(this.C) ? null : this.C);
            jSONObject.put("sim_serial_number", this.D);
            jSONObject.put("sms_enabled", this.E);
            jSONObject.put("ssid", this.F);
            jSONObject.put("cdma_network_id", this.N == -1 ? null : Integer.valueOf(this.N));
            jSONObject.put("cdma_system_id", this.M == -1 ? null : Integer.valueOf(this.M));
            jSONObject.put("subscriber_id", this.G);
            jSONObject.put("timestamp", this.H);
            jSONObject.put("total_storage_space", this.I);
            jSONObject.put("tz_name", this.J);
            jSONObject.put("ds", this.K);
            jSONObject.put("tz", this.L);
            jSONObject.put("network_operator", this.O);
            if (this.P != -1) {
                num = Integer.valueOf(this.P);
            }
            jSONObject.put("source_app", num);
            jSONObject.put("source_app_version", this.Q);
            jSONObject.put("is_emulator", this.R);
            jSONObject.put("is_rooted", this.S);
            jSONObject.put("pairing_id", this.T);
            jSONObject.put("app_first_install_time", this.U);
            jSONObject.put("app_last_update_time", this.V);
            jSONObject.put("android_id", this.W);
            jSONObject.put("serial_number", this.Y);
            jSONObject.put("notif_token", this.X);
            jSONObject.put("bluetooth_mac_addrs", (Object) null);
            jSONObject.put("gsf_id", this.Z);
            jSONObject.put("VPN_setting", this.ab);
            jSONObject.put("proxy_setting", this.aa);
            jSONObject.put("c", this.ac);
            jSONObject.put("pm", this.ae);
            a(jSONObject);
        } catch (JSONException e2) {
        }
        return jSONObject;
    }

    public final JSONObject a(q qVar) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("is_emulator", qVar.R);
            jSONObject.put("is_rooted", qVar.S);
            jSONObject.put("app_guid", qVar.f5340a);
            jSONObject.put("risk_comp_session_id", qVar.A);
            jSONObject.put("timestamp", qVar.H);
            jSONObject.put("payload_type", "incremental");
            jSONObject.put("source_app", qVar.P);
            jSONObject.put("pairing_id", qVar.T);
            a(jSONObject);
            if (this.f5341b != null && !this.f5341b.equals(qVar.f5341b)) {
                jSONObject.put("app_id", qVar.f5341b);
            }
            if (this.f5342c != null && !this.f5342c.equals(qVar.f5342c)) {
                jSONObject.put("app_version", qVar.f5342c);
            }
            if (this.f5343d != qVar.f5343d) {
                jSONObject.put("base_station_id", qVar.f5343d);
            }
            if (this.f5344e != null && !this.f5344e.equals(qVar.f5344e)) {
                jSONObject.put("bssid", qVar.f5344e);
            }
            if (this.f5345f != qVar.f5345f) {
                jSONObject.put("cell_id", qVar.f5345f);
            }
            if (this.f5346g != null && !this.f5346g.equals(qVar.f5346g)) {
                jSONObject.put("comp_version", qVar.f5346g);
            }
            if (this.i != null && !this.i.equals(qVar.i)) {
                jSONObject.put("conf_version", qVar.i);
            }
            if (this.f5347h != null && !this.f5347h.equals(qVar.f5347h)) {
                jSONObject.put("conf_url", qVar.f5347h);
            }
            if (this.j != null && !this.j.equals(qVar.j)) {
                jSONObject.put("conn_type", qVar.j);
            }
            if (this.k != null && !this.k.equals(qVar.k)) {
                jSONObject.put("device_id", qVar.k);
            }
            if (this.l != null && !this.l.equals(qVar.l)) {
                jSONObject.put("device_model", qVar.l);
            }
            if (this.m != null && !this.m.equals(qVar.m)) {
                jSONObject.put("device_name", qVar.m);
            }
            if (this.n != qVar.n) {
                jSONObject.put("device_uptime", qVar.n);
            }
            if (this.o != null && !this.o.equals(qVar.o)) {
                jSONObject.put("ip_addrs", qVar.o);
            }
            if (!(this.p == null || qVar.p == null || this.p.toString().equals(qVar.p.toString()))) {
                jSONObject.put("ip_addresses", new JSONArray((Collection) qVar.p));
            }
            if (!(this.q == null || qVar.q == null || this.q.toString().equals(qVar.q.toString()))) {
                jSONObject.put("known_apps", new JSONArray((Collection) qVar.q));
            }
            if (this.r != null && !this.r.equals(qVar.r)) {
                jSONObject.put("linker_id", qVar.r);
            }
            if (this.s != null && !this.s.equals(qVar.s)) {
                jSONObject.put("locale_country", qVar.s);
            }
            if (this.t != null && !this.t.equals(qVar.t)) {
                jSONObject.put("locale_lang", qVar.t);
            }
            if (!(this.u == null || qVar.u == null || this.u.toString().equals(qVar.u.toString()))) {
                jSONObject.put(FirebaseAnalytics.Param.LOCATION, a(qVar.u));
            }
            if (this.v != qVar.v) {
                jSONObject.put("location_area_code", qVar.v);
            }
            if (this.w != null && !this.w.equals(qVar.w)) {
                jSONObject.put("mac_addrs", qVar.w);
            }
            if (this.x != null && !this.x.equals(qVar.x)) {
                jSONObject.put("os_type", qVar.x);
            }
            if (this.y != null && !this.y.equals(qVar.y)) {
                jSONObject.put("os_version", qVar.y);
            }
            if (this.z != null && !this.z.equals(qVar.z)) {
                jSONObject.put("phone_type", qVar.z);
            }
            if (this.B != null && this.B.equals(qVar.B)) {
                jSONObject.put("roaming", qVar.B);
            }
            if (this.C != null && !this.C.equals(qVar.C)) {
                jSONObject.put("sim_operator_name", qVar.C);
            }
            if (this.D != null && !this.D.equals(qVar.D)) {
                jSONObject.put("sim_serial_number", qVar.D);
            }
            if (this.E != null && this.E.equals(qVar.E)) {
                jSONObject.put("sms_enabled", qVar.E);
            }
            if (this.F != null && !this.F.equals(qVar.F)) {
                jSONObject.put("ssid", qVar.F);
            }
            if (this.N != qVar.N) {
                jSONObject.put("cdma_network_id", qVar.N);
            }
            if (this.M != qVar.M) {
                jSONObject.put("cdma_system_id", qVar.M);
            }
            if (this.G != null && !this.G.equals(qVar.G)) {
                jSONObject.put("subscriber_id", qVar.G);
            }
            if (this.I != qVar.I) {
                jSONObject.put("total_storage_space", qVar.I);
            }
            if (this.J != null && !this.J.equals(qVar.J)) {
                jSONObject.put("tz_name", qVar.J);
            }
            if (this.K != null && !this.K.equals(qVar.K)) {
                jSONObject.put("ds", qVar.K);
            }
            if (this.L != null && !this.L.equals(qVar.L)) {
                jSONObject.put("tz", qVar.L);
            }
            if (this.O != null && !this.O.equals(qVar.O)) {
                jSONObject.put("network_operator", qVar.O);
            }
            if (this.Q != null && !this.Q.equals(qVar.Q)) {
                jSONObject.put("source_app_version", qVar.Q);
            }
            if (this.U != qVar.U) {
                jSONObject.put("app_first_install_time", qVar.U);
            }
            if (this.V != qVar.V) {
                jSONObject.put("app_last_update_time", qVar.V);
            }
            if (this.W != null && !this.W.equals(qVar.W)) {
                jSONObject.put("android_id", qVar.W);
            }
            if (this.Y != null && !this.Y.equals(qVar.Y)) {
                jSONObject.put("serial_number", qVar.Y);
            }
            if (this.X != null && !this.X.equals(qVar.X)) {
                jSONObject.put("notif_token", qVar.X);
            }
            if (this.Z != null && !this.Z.equals(qVar.Z)) {
                jSONObject.put("gsf_id", qVar.Z);
            }
            if (this.ab != null && !this.ab.equals(qVar.ab)) {
                jSONObject.put("VPN_setting", qVar.ab);
            }
            if (this.aa != null && !this.aa.equals(qVar.aa)) {
                jSONObject.put("proxy_setting", qVar.aa);
            }
            if (this.ac != null && !this.ac.equals(qVar.ac)) {
                jSONObject.put("c", qVar.ac);
            }
            if (this.ae != null && !this.ae.equals(qVar.ae)) {
                jSONObject.put("pm", qVar.ae);
            }
            if (this.af != null && !this.af.equals(qVar.af)) {
                jSONObject.put("bssid_arr", qVar.af);
            }
        } catch (JSONException e2) {
        }
        return jSONObject;
    }
}
