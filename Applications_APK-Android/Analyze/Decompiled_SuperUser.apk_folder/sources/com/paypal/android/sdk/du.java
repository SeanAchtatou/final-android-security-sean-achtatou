package com.paypal.android.sdk;

import android.os.Parcel;
import android.os.Parcelable;

public final class du implements Parcelable {
    public static final Parcelable.Creator CREATOR = new co();

    /* renamed from: a  reason: collision with root package name */
    private String f4768a;

    /* renamed from: b  reason: collision with root package name */
    private String f4769b;

    static {
        du.class.getSimpleName();
    }

    public du(Parcel parcel) {
        this.f4768a = parcel.readString();
        this.f4769b = parcel.readString();
    }

    public du(String str, String str2) {
        this.f4768a = str;
        this.f4769b = str2;
    }

    public final String a() {
        return this.f4768a;
    }

    public final String b() {
        return this.f4769b;
    }

    public final int describeContents() {
        return 0;
    }

    public final String toString() {
        return du.class.getSimpleName() + "(authCode:" + this.f4768a + ", scope:" + this.f4769b + ")";
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f4768a);
        parcel.writeString(this.f4769b);
    }
}
