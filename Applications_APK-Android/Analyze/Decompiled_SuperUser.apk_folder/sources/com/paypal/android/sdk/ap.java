package com.paypal.android.sdk;

import android.os.Message;

public class ap implements bu {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4561a = ap.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final a f4562b;

    /* renamed from: c  reason: collision with root package name */
    private final ao f4563c;

    /* renamed from: d  reason: collision with root package name */
    private final at f4564d = new at();

    /* renamed from: e  reason: collision with root package name */
    private final aq f4565e = new aq(this);

    /* renamed from: f  reason: collision with root package name */
    private bp f4566f;

    public ap(a aVar, ao aoVar, x xVar) {
        this.f4562b = aVar;
        this.f4563c = aoVar;
    }

    public final String a(br brVar) {
        new StringBuilder("mEnvironment:").append(this.f4563c).append(" mEnvironment.getEndpoints():").append(this.f4563c.c());
        if (this.f4563c == null || this.f4563c.c() == null) {
            return null;
        }
        String str = (String) this.f4563c.c().get(brVar.a());
        new StringBuilder("returning:").append(str);
        return str;
    }

    public final void a() {
        this.f4566f.a();
    }

    public final void a(av avVar) {
        this.f4564d.a(avVar);
    }

    public final void a(bp bpVar) {
        if (this.f4566f != null) {
            throw new IllegalStateException();
        }
        this.f4566f = bpVar;
    }

    public final void a(bt btVar) {
        btVar.l();
        r.a().f();
        if (!btVar.a()) {
            Message message = new Message();
            message.what = 2;
            message.obj = btVar;
            this.f4565e.sendMessage(message);
        }
    }

    public final void b() {
        this.f4564d.a();
    }

    public final void b(bt btVar) {
        this.f4566f.a(btVar);
    }

    public final String c() {
        return this.f4563c.a();
    }

    public final a d() {
        return this.f4562b;
    }

    public final String e() {
        return this.f4563c.b();
    }
}
