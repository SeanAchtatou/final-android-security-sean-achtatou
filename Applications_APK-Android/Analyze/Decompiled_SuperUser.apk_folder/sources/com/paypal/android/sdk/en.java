package com.paypal.android.sdk;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.math.BigDecimal;
import java.util.Currency;

public class en implements Parcelable {
    public static final Parcelable.Creator CREATOR = new dd();

    /* renamed from: c  reason: collision with root package name */
    private static /* synthetic */ boolean f4826c = (!en.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    private BigDecimal f4827a;

    /* renamed from: b  reason: collision with root package name */
    private Currency f4828b;

    public en(Parcel parcel) {
        this.f4827a = new BigDecimal(parcel.readString());
        try {
            this.f4828b = Currency.getInstance(parcel.readString());
        } catch (IllegalArgumentException e2) {
            Log.e("MoneySpec", "Exception reading currency code from parcel", e2);
            throw new RuntimeException(e2);
        }
    }

    public en(BigDecimal bigDecimal, String str) {
        this.f4827a = bigDecimal;
        this.f4828b = Currency.getInstance(str);
    }

    public final BigDecimal a() {
        return this.f4827a;
    }

    public final Currency b() {
        return this.f4828b;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (f4826c || (obj instanceof en)) {
            en enVar = (en) obj;
            return enVar.f4827a == this.f4827a && enVar.f4828b.equals(this.f4828b);
        }
        throw new AssertionError();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f4827a.toString());
        parcel.writeString(this.f4828b.getCurrencyCode());
    }
}
