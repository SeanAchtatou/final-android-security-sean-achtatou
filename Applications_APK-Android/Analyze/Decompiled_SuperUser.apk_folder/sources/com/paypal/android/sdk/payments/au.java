package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;

final class au implements Parcelable.Creator {
    au() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new PayPalPayment(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new PayPalPayment[i];
    }
}
