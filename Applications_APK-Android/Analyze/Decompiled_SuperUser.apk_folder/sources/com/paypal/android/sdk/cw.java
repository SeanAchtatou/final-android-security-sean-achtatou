package com.paypal.android.sdk;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class cw {

    /* renamed from: a  reason: collision with root package name */
    private static Map f4689a;

    static {
        cw.class.getSimpleName();
    }

    private static String a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), (int) FileUtils.FileMode.MODE_IWUSR).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            return "unknown versionName";
        }
    }

    public static JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            for (String str : f4689a.keySet()) {
                jSONObject.put(str, f4689a.get(str));
            }
            return jSONObject;
        } catch (JSONException e2) {
            Log.e("paypal.sdk", "Error encoding JSON", e2);
            return null;
        }
    }

    public static void a(a aVar) {
        if (f4689a == null) {
            HashMap hashMap = new HashMap();
            f4689a = hashMap;
            hashMap.put("app_version", a(aVar.f()));
            f4689a.put("app_category", "1");
            if (aVar.b() == 1) {
                f4689a.put("client_platform", "AndroidGSM");
            } else if (aVar.b() == 2) {
                f4689a.put("client_platform", "AndroidCDMA");
            } else {
                f4689a.put("client_platform", "AndroidOther");
            }
            f4689a.put("device_app_id", aVar.c());
        }
    }
}
