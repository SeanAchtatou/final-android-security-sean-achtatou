package com.paypal.android.sdk;

import com.paypal.android.sdk.payments.PayPalService;
import java.util.HashMap;
import java.util.Map;

public final class ch {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4674a = PayPalService.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static Map f4675b = new HashMap();

    public static ds a(String str) {
        ds dsVar = (ds) f4675b.get(str);
        new StringBuilder("getLoginAccessToken(").append(str).append(") returns String:").append(dsVar);
        return dsVar;
    }

    public static void a(ds dsVar, String str) {
        f4675b.put(str, dsVar);
        new StringBuilder("setLoginAccessToken(").append(dsVar).append(",").append(str).append(")");
    }

    public static void b(String str) {
        f4675b.remove(str);
    }
}
