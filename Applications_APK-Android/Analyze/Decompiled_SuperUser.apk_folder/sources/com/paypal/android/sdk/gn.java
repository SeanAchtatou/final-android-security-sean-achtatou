package com.paypal.android.sdk;

import android.content.Context;
import android.content.pm.PackageManager;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class gn {

    /* renamed from: a  reason: collision with root package name */
    private static final List f5024a = Arrays.asList("android.permission.ACCESS_NETWORK_STATE", "android.permission.INTERNET");

    /* renamed from: b  reason: collision with root package name */
    private final Context f5025b;

    static {
        gn.class.getSimpleName();
    }

    public gn(Context context) {
        this.f5025b = context;
    }

    public final void a() {
        try {
            HashSet hashSet = new HashSet(f5024a);
            String[] strArr = this.f5025b.getPackageManager().getPackageInfo(this.f5025b.getPackageName(), (int) CodedOutputStream.DEFAULT_BUFFER_SIZE).requestedPermissions;
            if (strArr != null) {
                for (String remove : strArr) {
                    hashSet.remove(remove);
                }
            }
            if (!hashSet.isEmpty()) {
                throw new RuntimeException("Missing required permissions in manifest:" + hashSet);
            }
        } catch (PackageManager.NameNotFoundException e2) {
            throw new RuntimeException("Exception loading manifest" + e2.getMessage());
        }
    }
}
