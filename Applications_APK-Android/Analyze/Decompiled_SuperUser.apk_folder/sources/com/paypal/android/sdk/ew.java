package com.paypal.android.sdk;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ew implements fh {

    /* renamed from: a  reason: collision with root package name */
    private String f4850a = ev.a(fs.PREFERRED_PAYMENT_METHOD);

    /* renamed from: b  reason: collision with root package name */
    private fa f4851b;

    private ew(JSONObject jSONObject) {
        this.f4851b = new fa(jSONObject.optJSONArray("funding_sources"), jSONObject.optJSONObject("backup_funding_instrument"));
    }

    public static ArrayList a(JSONObject jSONObject, JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        if (jSONObject != null) {
            ew ewVar = new ew(jSONObject);
            if (ewVar.h()) {
                arrayList.add(ewVar);
            }
        }
        if (jSONArray != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                try {
                    ew ewVar2 = new ew(jSONArray.getJSONObject(i));
                    if (ewVar2.h()) {
                        arrayList.add(ewVar2);
                    }
                } catch (JSONException e2) {
                }
            }
        }
        return arrayList;
    }

    private boolean h() {
        return this.f4851b.e() > 0;
    }

    public final String a() {
        return this.f4851b.d();
    }

    public final String b() {
        return this.f4850a;
    }

    public final String c() {
        return this.f4851b.a();
    }

    public final String d() {
        return this.f4851b.c();
    }

    public final boolean e() {
        return this.f4851b.b();
    }

    public final fa f() {
        return this.f4851b;
    }

    public final boolean g() {
        return this.f4851b.e() == 1;
    }
}
