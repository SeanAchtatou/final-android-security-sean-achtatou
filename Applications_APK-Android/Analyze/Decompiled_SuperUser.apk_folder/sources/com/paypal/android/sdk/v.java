package com.paypal.android.sdk;

import android.os.Build;
import java.io.File;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    private static p f5361a = new p();

    public static boolean a() {
        return (Build.TAGS != null && Build.TAGS.contains("test-keys")) || b() || c();
    }

    private static boolean b() {
        try {
            return new File(p.a("suFileName")).exists();
        } catch (Exception e2) {
            return false;
        }
    }

    private static boolean c() {
        try {
            return new File(p.a("superUserApk")).exists();
        } catch (Exception e2) {
            return false;
        }
    }
}
