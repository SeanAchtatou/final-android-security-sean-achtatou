package com.paypal.android.sdk.payments;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

final class bq implements bi {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PaymentActivity f5210a;

    bq(PaymentActivity paymentActivity) {
        this.f5210a = paymentActivity;
    }

    public final void a() {
        Date time = Calendar.getInstance().getTime();
        if (this.f5210a.f5118c.compareTo(time) > 0) {
            long time2 = this.f5210a.f5118c.getTime() - time.getTime();
            String unused = PaymentActivity.f5116a;
            new StringBuilder("Delaying ").append(time2).append(" miliseconds so user doesn't see flicker.");
            Timer unused2 = this.f5210a.f5117b = new Timer();
            this.f5210a.f5117b.schedule(new br(this), time2);
            return;
        }
        this.f5210a.b();
    }

    public final void a(bj bjVar) {
        cf.a(this.f5210a, bjVar, 1, 2, 3);
    }
}
