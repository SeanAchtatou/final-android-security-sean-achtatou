package com.paypal.android.sdk;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import okhttp3.k;
import okhttp3.t;

public class ba {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4585a = ba.class.getSimpleName();

    public static t.a a(int i, boolean z, boolean z2, String str, String str2) {
        new StringBuilder("Creating okhttp client.  networkTimeout=").append(i).append(", isTrustAll=").append(z).append(", useSslPinning=").append(z2).append(", userAgent=").append(str).append(", baseUrl=").append(str2);
        t.a a2 = new t.a().a(Arrays.asList(k.f8081a));
        a2.a(Integer.valueOf(i).longValue(), TimeUnit.SECONDS);
        a2.b(Integer.valueOf(i).longValue(), TimeUnit.SECONDS);
        a2.a().add(new bd(str));
        if (!z) {
            if (z2) {
                try {
                    a(a2);
                } catch (IOException | KeyManagementException | KeyStoreException | NoSuchAlgorithmException | CertificateException e2) {
                    throw new RuntimeException(e2);
                }
            } else {
                TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                instance.init((KeyStore) null);
                a(a2, instance.getTrustManagers());
            }
        }
        return a2;
    }

    private static void a(t.a aVar) {
        KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
        instance.load(null, null);
        CertificateFactory instance2 = CertificateFactory.getInstance("X.509");
        InputStream a2 = bb.a();
        try {
            for (Certificate certificate : instance2.generateCertificates(bb.a())) {
                if (certificate instanceof X509Certificate) {
                    instance.setCertificateEntry(((X509Certificate) certificate).getSubjectDN().getName(), certificate);
                }
            }
            TrustManagerFactory instance3 = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance3.init(instance);
            a(aVar, instance3.getTrustManagers());
        } finally {
            try {
                a2.close();
            } catch (IOException | NullPointerException e2) {
            }
        }
    }

    private static void a(t.a aVar, TrustManager[] trustManagerArr) {
        if (trustManagerArr.length != 1 || !(trustManagerArr[0] instanceof X509TrustManager)) {
            throw new IllegalStateException("Unexpected number of trust managers:" + Arrays.toString(trustManagerArr));
        }
        X509TrustManager x509TrustManager = (X509TrustManager) trustManagerArr[0];
        aVar.a(new bc(x509TrustManager), x509TrustManager);
    }
}
