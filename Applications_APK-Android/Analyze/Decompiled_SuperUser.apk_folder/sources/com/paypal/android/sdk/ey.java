package com.paypal.android.sdk;

import com.google.firebase.analytics.FirebaseAnalytics;

public enum ey {
    PreConnect("preconnect", "", false, false),
    DeviceCheck("devicecheck", "", false, false),
    PaymentMethodWindow("selectpaymentmethod", "", false, false),
    PaymentMethodCancel("selectpaymentmethod", "cancel", false, true),
    SelectPayPalPayment("selectpaymentmethod", "paypal", false, true),
    SelectCreditCardPayment("selectpaymentmethod", "card", false, true),
    ConfirmPaymentWindow("confirmpayment", "", false, false),
    ConfirmPayment("confirmpayment", "confirm", false, false),
    ConfirmPaymentCancel("confirmpayment", "cancel", false, true),
    PaymentSuccessful("paymentsuccessful", "", false, false),
    LoginWindow(FirebaseAnalytics.Event.LOGIN, "password", true, false),
    LoginPassword(FirebaseAnalytics.Event.LOGIN, "password", true, true),
    LoginPIN(FirebaseAnalytics.Event.LOGIN, "PIN", true, true),
    SignUp(FirebaseAnalytics.Event.LOGIN, "password", true, true),
    LoginForgotPassword(FirebaseAnalytics.Event.LOGIN, "password", true, true),
    LoginCancel(FirebaseAnalytics.Event.LOGIN, "cancel", true, true),
    ConsentWindow("authorizationconsent", "", false, false),
    ConsentAgree("authorizationconsent", "agree", false, true),
    ConsentCancel("authorizationconsent", "cancel", false, true),
    ConsentMerchantUrl("authorizationconsent", "merchanturl", false, true),
    ConsentPayPalPrivacyUrl("authorizationconsent", "privacy", false, true),
    AuthorizationSuccessful("authorizationsuccessful", "", false, false),
    LegalTextWindow("legaltext", "", false, false);
    
    private boolean A;
    private String x;
    private String y;
    private boolean z;

    private ey(String str, String str2, boolean z2, boolean z3) {
        this.x = str;
        this.y = str2;
        this.z = z2;
        this.A = z3;
    }

    public final String a() {
        return this.x + "::" + this.y;
    }

    public final String a(String str, boolean z2) {
        return dq.f4746a + ":" + str + ":" + (this.z ? z2 ? "returnuser" : "newuser" : "");
    }

    public final boolean b() {
        return this.A;
    }
}
