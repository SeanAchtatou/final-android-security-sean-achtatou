package com.paypal.android.sdk.payments;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

final class aq implements bi {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PayPalFuturePaymentActivity f5179a;

    aq(PayPalFuturePaymentActivity payPalFuturePaymentActivity) {
        this.f5179a = payPalFuturePaymentActivity;
    }

    public final void a() {
        Date time = Calendar.getInstance().getTime();
        if (this.f5179a.f5071b.compareTo(time) > 0) {
            long time2 = this.f5179a.f5071b.getTime() - time.getTime();
            String unused = PayPalFuturePaymentActivity.f5070a;
            new StringBuilder("Delaying ").append(time2).append(" milliseconds so user doesn't see flicker.");
            Timer unused2 = this.f5179a.f5072c = new Timer();
            this.f5179a.f5072c.schedule(new ar(this), time2);
            return;
        }
        this.f5179a.b();
    }

    public final void a(bj bjVar) {
        cf.a(this.f5179a, bjVar, 1, 2, 3);
    }
}
