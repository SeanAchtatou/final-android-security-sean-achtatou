package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.fs;
import com.paypal.android.sdk.gj;
import com.paypal.android.sdk.gm;
import com.paypal.android.sdk.gn;
import com.paypal.android.sdk.go;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

public final class PaymentActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f5116a = PaymentActivity.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Timer f5117b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Date f5118c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public PayPalService f5119d;

    /* renamed from: e  reason: collision with root package name */
    private final ServiceConnection f5120e = new bo(this);

    /* renamed from: f  reason: collision with root package name */
    private boolean f5121f;

    /* access modifiers changed from: private */
    public void b() {
        PaymentMethodActivity.a(this, 1, this.f5119d.d());
    }

    /* access modifiers changed from: private */
    public bi c() {
        return new bq(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void
     arg types: [com.paypal.android.sdk.payments.bi, int]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.payments.bi):com.paypal.android.sdk.payments.bi
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.bt):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, boolean):boolean
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.Boolean):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(java.lang.String, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void */
    static /* synthetic */ void c(PaymentActivity paymentActivity) {
        if (paymentActivity.f5119d.d() == null) {
            Log.e(f5116a, "Service state invalid.  Did you start the PayPalService?");
            paymentActivity.setResult(2);
            paymentActivity.finish();
            return;
        }
        bs bsVar = new bs(paymentActivity.getIntent(), paymentActivity.f5119d.d());
        if (!bsVar.c()) {
            Log.e(f5116a, "Service extras invalid.  Please see the docs.");
            paymentActivity.setResult(2);
            paymentActivity.finish();
        } else if (!bsVar.a()) {
            Log.e(f5116a, "Extras invalid.  Please see the docs.");
            paymentActivity.setResult(2);
            paymentActivity.finish();
        } else {
            paymentActivity.f5119d.l();
            paymentActivity.f5119d.c().a();
            if (paymentActivity.f5119d.i()) {
                paymentActivity.b();
                return;
            }
            Calendar instance = Calendar.getInstance();
            instance.add(13, 1);
            paymentActivity.f5118c = instance.getTime();
            paymentActivity.f5119d.a(paymentActivity.c(), false);
        }
    }

    public final void finish() {
        super.finish();
        new StringBuilder().append(f5116a).append(".finish");
    }

    /* access modifiers changed from: protected */
    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        new StringBuilder().append(f5116a).append(".onActivityResult");
        if (i == 1) {
            switch (i2) {
                case -1:
                    if (intent == null) {
                        Log.e(f5116a, "result was OK, no intent data, oops");
                        break;
                    } else {
                        PaymentConfirmation paymentConfirmation = (PaymentConfirmation) intent.getParcelableExtra("com.paypal.android.sdk.paymentConfirmation");
                        if (paymentConfirmation == null) {
                            Log.e(f5116a, "result was OK, have data, but no payment state in bundle, oops");
                            break;
                        } else {
                            Intent intent2 = new Intent();
                            intent2.putExtra("com.paypal.android.sdk.paymentConfirmation", paymentConfirmation);
                            setResult(-1, intent2);
                            break;
                        }
                    }
                case 0:
                    break;
                default:
                    Log.wtf("paypal.sdk", "unexpected request code " + i + " call it a cancel");
                    break;
            }
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new StringBuilder().append(f5116a).append(".onCreate");
        new go(this).a();
        new gn(this).a();
        new gm(this).a(Arrays.asList(PaymentActivity.class.getName(), LoginActivity.class.getName(), PaymentMethodActivity.class.getName(), PaymentConfirmActivity.class.getName()));
        this.f5121f = bindService(cf.b(this), this.f5120e, 1);
        setTheme(16973934);
        requestWindowFeature(8);
        gj gjVar = new gj(this);
        setContentView(gjVar.f5005a);
        gjVar.f5006b.setText(ev.a(fs.CHECKING_DEVICE));
        cf.a(this, (TextView) null, fs.CHECKING_DEVICE);
    }

    /* access modifiers changed from: protected */
    public final Dialog onCreateDialog(int i, Bundle bundle) {
        switch (i) {
            case 2:
                return cf.a(this, new bn(this));
            case 3:
                return cf.a(this, fs.UNAUTHORIZED_MERCHANT_TITLE, bundle, i);
            default:
                return cf.a(this, fs.UNAUTHORIZED_DEVICE_TITLE, bundle, i);
        }
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        new StringBuilder().append(f5116a).append(".onDestroy");
        if (this.f5119d != null) {
            this.f5119d.o();
            this.f5119d.u();
        }
        if (this.f5121f) {
            unbindService(this.f5120e);
            this.f5121f = false;
        }
        super.onDestroy();
    }
}
