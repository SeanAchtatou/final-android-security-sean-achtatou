package com.paypal.android.sdk.payments;

import android.app.AlertDialog;
import android.view.View;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.fd;
import com.paypal.android.sdk.fs;
import java.util.List;

final class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ fd f5320a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ List f5321b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ LoginActivity f5322c;

    p(LoginActivity loginActivity, fd fdVar, List list) {
        this.f5322c = loginActivity;
        this.f5320a = fdVar;
        this.f5321b = list;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(ev.a(fs.TWO_FACTOR_AUTH_ENTER_MOBILE_NUMBER)).setAdapter(this.f5320a, new q(this));
        builder.create().show();
    }
}
