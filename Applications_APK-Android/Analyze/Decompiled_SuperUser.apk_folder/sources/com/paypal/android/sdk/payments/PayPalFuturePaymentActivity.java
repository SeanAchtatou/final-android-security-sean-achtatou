package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.fs;
import com.paypal.android.sdk.gj;
import com.paypal.android.sdk.gm;
import com.paypal.android.sdk.gn;
import com.paypal.android.sdk.go;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

public final class PayPalFuturePaymentActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f5070a = PayPalFuturePaymentActivity.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Date f5071b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Timer f5072c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public PayPalService f5073d;

    /* renamed from: e  reason: collision with root package name */
    private final ServiceConnection f5074e = new ao(this);

    /* renamed from: f  reason: collision with root package name */
    private boolean f5075f;

    /* access modifiers changed from: private */
    public void b() {
        FuturePaymentConsentActivity.a(this, 1, this.f5073d.d());
    }

    /* access modifiers changed from: private */
    public bi c() {
        return new aq(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void
     arg types: [com.paypal.android.sdk.payments.bi, int]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.payments.bi):com.paypal.android.sdk.payments.bi
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.bt):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, boolean):boolean
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.Boolean):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(java.lang.String, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void */
    static /* synthetic */ void c(PayPalFuturePaymentActivity payPalFuturePaymentActivity) {
        if (payPalFuturePaymentActivity.f5073d.d() == null) {
            Log.e(f5070a, "Service state invalid.  Did you start the PayPalService?");
            payPalFuturePaymentActivity.setResult(2);
            payPalFuturePaymentActivity.finish();
            return;
        }
        ba baVar = new ba(payPalFuturePaymentActivity.getIntent(), payPalFuturePaymentActivity.f5073d.d(), false);
        if (!baVar.c()) {
            Log.e(f5070a, "Service extras invalid.  Please see the docs.");
            payPalFuturePaymentActivity.setResult(2);
            payPalFuturePaymentActivity.finish();
        } else if (!baVar.a()) {
            Log.e(f5070a, "Extras invalid.  Please see the docs.");
            payPalFuturePaymentActivity.setResult(2);
            payPalFuturePaymentActivity.finish();
        } else if (payPalFuturePaymentActivity.f5073d.i()) {
            payPalFuturePaymentActivity.b();
        } else {
            Calendar instance = Calendar.getInstance();
            instance.add(13, 1);
            payPalFuturePaymentActivity.f5071b = instance.getTime();
            payPalFuturePaymentActivity.f5073d.a(payPalFuturePaymentActivity.c(), false);
        }
    }

    public final void finish() {
        super.finish();
        new StringBuilder().append(f5070a).append(".finish");
    }

    /* access modifiers changed from: protected */
    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        new StringBuilder().append(f5070a).append(".onActivityResult");
        if (i == 1) {
            switch (i2) {
                case -1:
                    if (intent == null) {
                        Log.e(f5070a, "result was OK, no intent data, oops");
                        break;
                    } else {
                        PayPalAuthorization payPalAuthorization = (PayPalAuthorization) intent.getParcelableExtra("com.paypal.android.sdk.authorization");
                        if (payPalAuthorization == null) {
                            Log.e(f5070a, "result was OK, have data, but no authorization state in bundle, oops");
                            break;
                        } else {
                            Intent intent2 = new Intent();
                            intent2.putExtra("com.paypal.android.sdk.authorization", payPalAuthorization);
                            setResult(-1, intent2);
                            break;
                        }
                    }
                case 0:
                    break;
                default:
                    Log.wtf(f5070a, "unexpected request code " + i + " call it a cancel");
                    break;
            }
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new StringBuilder().append(f5070a).append(".onCreate");
        new go(this).a();
        new gn(this).a();
        new gm(this).a(Arrays.asList(PayPalFuturePaymentActivity.class.getName(), LoginActivity.class.getName(), FuturePaymentInfoActivity.class.getName(), FuturePaymentConsentActivity.class.getName()));
        this.f5075f = bindService(cf.b(this), this.f5074e, 1);
        setTheme(16973934);
        requestWindowFeature(8);
        gj gjVar = new gj(this);
        setContentView(gjVar.f5005a);
        gjVar.f5006b.setText(ev.a(fs.CHECKING_DEVICE));
        cf.a(this, (TextView) null, fs.CHECKING_DEVICE);
    }

    /* access modifiers changed from: protected */
    public final Dialog onCreateDialog(int i, Bundle bundle) {
        switch (i) {
            case 2:
                return cf.a(this, new an(this));
            case 3:
                return cf.a(this, fs.UNAUTHORIZED_MERCHANT_TITLE, bundle, i);
            default:
                return cf.a(this, fs.UNAUTHORIZED_DEVICE_TITLE, bundle, i);
        }
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        new StringBuilder().append(f5070a).append(".onDestroy");
        if (this.f5073d != null) {
            this.f5073d.o();
            this.f5073d.u();
        }
        if (this.f5075f) {
            unbindService(this.f5074e);
            this.f5075f = false;
        }
        super.onDestroy();
    }
}
