package com.paypal.android.sdk.payments;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

final class ay implements bi {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PayPalProfileSharingActivity f5183a;

    ay(PayPalProfileSharingActivity payPalProfileSharingActivity) {
        this.f5183a = payPalProfileSharingActivity;
    }

    public final void a() {
        Date time = Calendar.getInstance().getTime();
        if (this.f5183a.f5103b.compareTo(time) > 0) {
            long time2 = this.f5183a.f5103b.getTime() - time.getTime();
            String unused = PayPalProfileSharingActivity.f5102a;
            new StringBuilder("Delaying ").append(time2).append(" miliseconds so user doesn't see flicker.");
            Timer unused2 = this.f5183a.f5104c = new Timer();
            this.f5183a.f5104c.schedule(new az(this), time2);
            return;
        }
        ProfileSharingConsentActivity.a(this.f5183a, 1, this.f5183a.f5105d.d());
    }

    public final void a(bj bjVar) {
        cf.a(this.f5183a, bjVar, 1, 2, 3);
    }
}
