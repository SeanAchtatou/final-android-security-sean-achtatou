package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;

final class da implements Parcelable.Creator {
    da() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new ProofOfPayment(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new ProofOfPayment[i];
    }
}
