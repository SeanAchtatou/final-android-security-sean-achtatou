package com.paypal.android.sdk.payments;

import com.paypal.android.sdk.ce;
import com.paypal.android.sdk.ev;

class de {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5255a = de.class.getSimpleName();

    de() {
    }

    static String a() {
        String a2 = ce.a().c().a();
        return String.format("https://www.paypal.com/signup/account?country.x=%s&locale.x=%s", a2, ev.c(a2));
    }
}
