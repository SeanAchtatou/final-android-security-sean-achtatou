package com.paypal.android.sdk;

import android.util.Log;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

public final class cy {

    /* renamed from: a  reason: collision with root package name */
    private static List f4690a = Arrays.asList("AUD", "BRL", "CAD", "CHF", "CZK", "DKK", "EUR", "GBP", "HKD", "HUF", "ILS", "JPY", "MXN", "MYR", "NOK", "NZD", "PHP", "PLN", "RUB", "SEK", "SGD", "THB", "TWD", "TRY", "USD");

    /* renamed from: b  reason: collision with root package name */
    private static String f4691b = "JPY, HUF, TWD";

    /* renamed from: c  reason: collision with root package name */
    private static final Locale f4692c = Locale.US;

    /* renamed from: d  reason: collision with root package name */
    private static final Locale f4693d = Locale.GERMANY;

    /* renamed from: e  reason: collision with root package name */
    private static List f4694e = null;

    /* renamed from: f  reason: collision with root package name */
    private static NumberFormat f4695f = null;

    public static String a(double d2, String str) {
        return a(d2, str, (DecimalFormat) NumberFormat.getInstance(f4692c));
    }

    private static String a(double d2, String str, DecimalFormat decimalFormat) {
        String str2 = "#######0";
        if (f4691b.indexOf(str.toUpperCase(Locale.US)) == -1) {
            str2 = "#####0.00";
        }
        decimalFormat.applyPattern(str2);
        return decimalFormat.format(d2);
    }

    public static String a(double d2, Currency currency) {
        DecimalFormat decimalFormat = a(currency).equals(",") ? (DecimalFormat) NumberFormat.getInstance(f4693d) : (DecimalFormat) NumberFormat.getInstance(f4692c);
        String str = "#######0";
        if (f4691b.indexOf(currency.getCurrencyCode().toUpperCase(Locale.US)) == -1) {
            str = "#####0.00";
        }
        decimalFormat.applyPattern(str);
        return decimalFormat.format(d2);
    }

    private static String a(Currency currency) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        decimalFormat.setCurrency(currency);
        return decimalFormat.format(1.56d).indexOf(".") > 0 ? "." : ",";
    }

    public static String a(Locale locale, String str, double d2, String str2, boolean z) {
        String str3;
        boolean z2 = false;
        String symbol = Currency.getInstance(str2).getSymbol();
        Currency instance = Currency.getInstance(str2);
        if (f4695f == null) {
            f4695f = NumberFormat.getCurrencyInstance(locale);
        }
        f4695f.setCurrency(instance);
        if (!(f4695f.format(1234.56d).indexOf("1") == 0)) {
            z2 = true;
        }
        StringBuilder append = new StringBuilder().append(z2 ? symbol + " " : "");
        if (str.equalsIgnoreCase("AU")) {
            str3 = "AUD";
        } else if (str.equalsIgnoreCase("GB")) {
            str3 = "GBP";
        } else if (str.equalsIgnoreCase("UK")) {
            str3 = "GBP";
        } else if (str.equalsIgnoreCase("CA")) {
            str3 = "CAD";
        } else if (str.equalsIgnoreCase("AT")) {
            str3 = "EUR";
        } else if (str.equalsIgnoreCase("CZ")) {
            str3 = "CZK";
        } else if (str.equalsIgnoreCase("DK")) {
            str3 = "DKK";
        } else if (str.equalsIgnoreCase("FR")) {
            str3 = "EUR";
        } else if (str.equalsIgnoreCase("DE")) {
            str3 = "EUR";
        } else if (str.equalsIgnoreCase("HU")) {
            str3 = "HUF";
        } else if (str.equalsIgnoreCase("IE")) {
            str3 = "EUR";
        } else if (str.equalsIgnoreCase("IT")) {
            str3 = "EUR";
        } else if (str.equalsIgnoreCase("NL")) {
            str3 = "EUR";
        } else if (str.equalsIgnoreCase("PL")) {
            str3 = "PLN";
        } else if (str.equalsIgnoreCase("PT")) {
            str3 = "EUR";
        } else if (str.equalsIgnoreCase("ES")) {
            str3 = "EUR";
        } else if (str.equalsIgnoreCase("SE")) {
            str3 = "SEK";
        } else {
            if (!str.equalsIgnoreCase("ZA")) {
                if (str.equalsIgnoreCase("NZ")) {
                    str3 = "NZD";
                } else if (str.equalsIgnoreCase("LT")) {
                    str3 = "EUR";
                } else if (str.equalsIgnoreCase("JP")) {
                    str3 = "JPY";
                } else if (str.equalsIgnoreCase("BR")) {
                    str3 = "BRL";
                } else if (str.equalsIgnoreCase("MY")) {
                    str3 = "MYR";
                } else if (str.equalsIgnoreCase("MX")) {
                    str3 = "MXN";
                } else if (str.equalsIgnoreCase("RU")) {
                    str3 = "RUB";
                }
            }
            str3 = "USD";
        }
        return append.append(a(d2, str2, a(Currency.getInstance(str3)).equals(",") ? (DecimalFormat) NumberFormat.getInstance(f4693d) : (DecimalFormat) NumberFormat.getInstance(f4692c))).append(!z2 ? " " + symbol : "").toString();
    }

    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        try {
            Currency instance = Currency.getInstance(str);
            if (f4694e == null) {
                f4694e = new ArrayList();
                for (String instance2 : f4690a) {
                    f4694e.add(Currency.getInstance(instance2));
                }
                Collections.sort(f4694e, new cz());
            }
            return f4694e.contains(instance);
        } catch (IllegalArgumentException e2) {
            return false;
        }
    }

    public static boolean a(BigDecimal bigDecimal, String str, boolean z) {
        if (bigDecimal == null) {
            Log.e("paypal.sdk", "The specified amount is null.");
            return false;
        } else if (!z || BigDecimal.ZERO.compareTo(bigDecimal) == -1) {
            if (a(str)) {
                if (Arrays.asList("HUF", "JPY", "TWD").contains(str) && bigDecimal.scale() > 0) {
                    Log.e("paypal.sdk", "The specified currency (" + str + ") does not support fractional amounts.");
                    return false;
                }
            }
            return true;
        } else {
            Log.e("paypal.sdk", "The specified amount must be greater than zero.");
            return false;
        }
    }
}
