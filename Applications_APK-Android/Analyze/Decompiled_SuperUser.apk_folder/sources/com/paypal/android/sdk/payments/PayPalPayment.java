package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import com.paypal.android.sdk.cd;
import com.paypal.android.sdk.cy;
import java.math.BigDecimal;

public final class PayPalPayment implements Parcelable {
    public static final Parcelable.Creator CREATOR = new au();

    /* renamed from: a  reason: collision with root package name */
    private static final String f5090a = PayPalPayment.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private BigDecimal f5091b;

    /* renamed from: c  reason: collision with root package name */
    private String f5092c;

    /* renamed from: d  reason: collision with root package name */
    private String f5093d;

    /* renamed from: e  reason: collision with root package name */
    private String f5094e;

    /* renamed from: f  reason: collision with root package name */
    private PayPalPaymentDetails f5095f;

    /* renamed from: g  reason: collision with root package name */
    private String f5096g;

    /* renamed from: h  reason: collision with root package name */
    private PayPalItem[] f5097h;
    private boolean i;
    private ShippingAddress j;
    private String k;
    private String l;
    private String m;
    private String n;

    private PayPalPayment(Parcel parcel) {
        this.f5092c = parcel.readString();
        try {
            this.f5091b = new BigDecimal(parcel.readString());
        } catch (NumberFormatException e2) {
        }
        this.f5093d = parcel.readString();
        this.f5096g = parcel.readString();
        this.f5094e = parcel.readString();
        this.f5095f = (PayPalPaymentDetails) parcel.readParcelable(PayPalPaymentDetails.class.getClassLoader());
        int readInt = parcel.readInt();
        if (readInt > 0) {
            this.f5097h = new PayPalItem[readInt];
            parcel.readTypedArray(this.f5097h, PayPalItem.CREATOR);
        }
        this.j = (ShippingAddress) parcel.readParcelable(ShippingAddress.class.getClassLoader());
        this.i = parcel.readInt() == 1;
        this.k = parcel.readString();
        this.l = parcel.readString();
        this.m = parcel.readString();
        this.n = parcel.readString();
    }

    /* synthetic */ PayPalPayment(Parcel parcel, byte b2) {
        this(parcel);
    }

    public PayPalPayment(BigDecimal bigDecimal, String str, String str2, String str3) {
        this.f5091b = bigDecimal;
        this.f5092c = str;
        this.f5093d = str2;
        this.f5096g = str3;
        this.f5095f = null;
        this.f5094e = null;
        toString();
    }

    private static void a(boolean z, String str) {
        if (!z) {
            Log.e("paypal.sdk", str + " is invalid.  Please see the docs.");
        }
    }

    private static boolean a(String str, String str2, int i2) {
        if (!cd.b((CharSequence) str) || str.length() <= i2) {
            return true;
        }
        Log.e("paypal.sdk", str2 + " is too long (max " + i2 + ")");
        return false;
    }

    public final boolean a() {
        return !this.i && this.j == null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.cy.a(java.math.BigDecimal, java.lang.String, boolean):boolean
     arg types: [java.math.BigDecimal, java.lang.String, int]
     candidates:
      com.paypal.android.sdk.cy.a(double, java.lang.String, java.text.DecimalFormat):java.lang.String
      com.paypal.android.sdk.cy.a(java.math.BigDecimal, java.lang.String, boolean):boolean */
    public final boolean b() {
        boolean z;
        boolean a2 = cy.a(this.f5092c);
        boolean a3 = cy.a(this.f5091b, this.f5092c, true);
        boolean z2 = !TextUtils.isEmpty(this.f5093d);
        boolean z3 = cd.b(this.f5096g) && (this.f5096g.equals("sale") || this.f5096g.equals("authorize") || this.f5096g.equals("order"));
        boolean a4 = this.f5095f == null ? true : this.f5095f.a();
        boolean c2 = cd.a(this.f5094e) ? true : cd.c(this.f5094e);
        if (this.f5097h != null && this.f5097h.length != 0) {
            PayPalItem[] payPalItemArr = this.f5097h;
            int length = payPalItemArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z = true;
                    break;
                } else if (!payPalItemArr[i2].a()) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
        } else {
            z = true;
        }
        boolean z4 = a(this.k, "invoiceNumber", FileUtils.FileMode.MODE_IRUSR);
        if (!a(this.l, "custom", FileUtils.FileMode.MODE_IRUSR)) {
            z4 = false;
        }
        if (!a(this.m, "softDescriptor", 22)) {
            z4 = false;
        }
        a(a2, "currencyCode");
        a(a3, "amount");
        a(z2, "shortDescription");
        a(z3, "paymentIntent");
        a(a4, "details");
        a(c2, "bnCode");
        a(z, "items");
        return a2 && a3 && z2 && a4 && z3 && c2 && z && z4;
    }

    /* access modifiers changed from: protected */
    public final BigDecimal c() {
        return this.f5091b;
    }

    /* access modifiers changed from: protected */
    public final String d() {
        return this.f5093d;
    }

    public final int describeContents() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final String e() {
        return this.f5096g;
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return this.f5092c;
    }

    /* access modifiers changed from: protected */
    public final String g() {
        return this.f5094e;
    }

    /* access modifiers changed from: protected */
    public final String h() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public final PayPalPaymentDetails i() {
        return this.f5095f;
    }

    /* access modifiers changed from: protected */
    public final PayPalItem[] j() {
        return this.f5097h;
    }

    /* access modifiers changed from: protected */
    public final String k() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public final String l() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public final String m() {
        return this.m;
    }

    public final boolean n() {
        return this.i;
    }

    public final ShippingAddress o() {
        return this.j;
    }

    public final String toString() {
        Object[] objArr = new Object[4];
        objArr[0] = this.f5093d;
        objArr[1] = this.f5091b != null ? this.f5091b.toString() : null;
        objArr[2] = this.f5092c;
        objArr[3] = this.f5096g;
        return String.format("PayPalPayment: {%s: $%s %s, %s}", objArr);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int i3 = 0;
        parcel.writeString(this.f5092c);
        parcel.writeString(this.f5091b.toString());
        parcel.writeString(this.f5093d);
        parcel.writeString(this.f5096g);
        parcel.writeString(this.f5094e);
        parcel.writeParcelable(this.f5095f, 0);
        if (this.f5097h != null) {
            parcel.writeInt(this.f5097h.length);
            parcel.writeTypedArray(this.f5097h, 0);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeParcelable(this.j, 0);
        if (this.i) {
            i3 = 1;
        }
        parcel.writeInt(i3);
        parcel.writeString(this.k);
        parcel.writeString(this.l);
        parcel.writeString(this.m);
        parcel.writeString(this.n);
    }
}
