package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;
import java.math.BigDecimal;

public final class PayPalPaymentDetails implements Parcelable {
    public static final Parcelable.Creator CREATOR = new av();

    /* renamed from: a  reason: collision with root package name */
    private static final String f5098a = PayPalPaymentDetails.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private BigDecimal f5099b;

    /* renamed from: c  reason: collision with root package name */
    private BigDecimal f5100c;

    /* renamed from: d  reason: collision with root package name */
    private BigDecimal f5101d;

    private PayPalPaymentDetails(Parcel parcel) {
        BigDecimal bigDecimal = null;
        try {
            String readString = parcel.readString();
            this.f5100c = readString == null ? null : new BigDecimal(readString);
            String readString2 = parcel.readString();
            this.f5099b = readString2 == null ? null : new BigDecimal(readString2);
            String readString3 = parcel.readString();
            this.f5101d = readString3 != null ? new BigDecimal(readString3) : bigDecimal;
        } catch (NumberFormatException e2) {
            throw new RuntimeException("error unparceling PayPalPaymentDetails");
        }
    }

    /* synthetic */ PayPalPaymentDetails(Parcel parcel, byte b2) {
        this(parcel);
    }

    public final boolean a() {
        return this.f5099b != null;
    }

    public final BigDecimal b() {
        return this.f5099b;
    }

    public final BigDecimal c() {
        return this.f5100c;
    }

    public final BigDecimal d() {
        return this.f5101d;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        String str = null;
        parcel.writeString(this.f5100c == null ? null : this.f5100c.toString());
        parcel.writeString(this.f5099b == null ? null : this.f5099b.toString());
        if (this.f5101d != null) {
            str = this.f5101d.toString();
        }
        parcel.writeString(str);
    }
}
