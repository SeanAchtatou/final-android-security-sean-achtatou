package com.paypal.android.sdk;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import org.json.JSONObject;

public class r implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    public static z f5348a = null;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final String f5349b = r.class.getSimpleName();
    private static p w = new p();
    private static final Object x = new Object();
    private static r y;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Context f5350c;

    /* renamed from: d  reason: collision with root package name */
    private String f5351d;

    /* renamed from: e  reason: collision with root package name */
    private long f5352e;

    /* renamed from: f  reason: collision with root package name */
    private long f5353f;

    /* renamed from: g  reason: collision with root package name */
    private int f5354g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public int f5355h;
    private long i;
    /* access modifiers changed from: private */
    public String j;
    private n k;
    private q l;
    private q m;
    private Map n;
    private Location o;
    private Timer p;
    /* access modifiers changed from: private */
    public Handler q;
    private ax r;
    private String s;
    private String t;
    private boolean u;
    private String v;

    private r() {
    }

    static /* synthetic */ int a(r rVar) {
        int i2 = rVar.f5355h;
        rVar.f5355h = i2 + 1;
        return i2;
    }

    private static long a(Context context) {
        if (context == null) {
            return 0;
        }
        try {
            if (Build.VERSION.SDK_INT > 8) {
                return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime;
            }
            String str = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir;
            if (str != null) {
                return new File(str).lastModified();
            }
            return 0;
        } catch (PackageManager.NameNotFoundException e2) {
            return 0;
        }
    }

    public static r a() {
        r rVar;
        synchronized (x) {
            if (y == null) {
                y = new r();
            }
            rVar = y;
        }
        return rVar;
    }

    private static String a(TelephonyManager telephonyManager) {
        try {
            return telephonyManager.getSimOperatorName();
        } catch (SecurityException e2) {
            aj.a(f5349b, "Known SecurityException on some devices", e2);
            return null;
        }
    }

    private String a(String str, Map map) {
        String str2;
        this.n = null;
        if (str != null && this.t != null && str.equals(this.t)) {
            return str;
        }
        if (str == null || str.trim().length() == 0) {
            str2 = i();
        } else {
            str2 = str.trim();
            aj.a(3, "PRD", "Using custom pairing id");
        }
        this.t = str2;
        e();
        j();
        return str2;
    }

    private static ArrayList a(WifiManager wifiManager) {
        String bssid;
        int i2;
        ArrayList arrayList = new ArrayList();
        List<ScanResult> scanResults = wifiManager.getScanResults();
        if (scanResults == null || scanResults.size() == 0 || (bssid = wifiManager.getConnectionInfo().getBSSID()) == null || bssid.equals("00:00:00:00:00:00")) {
            return null;
        }
        int i3 = 0;
        int i4 = -1;
        int i5 = Integer.MIN_VALUE;
        while (true) {
            int i6 = i3;
            if (i6 >= scanResults.size()) {
                break;
            }
            if (!bssid.equals(scanResults.get(i6).BSSID) && i5 < (i2 = scanResults.get(i6).level)) {
                i4 = i6;
                i5 = i2;
            }
            i3 = i6 + 1;
        }
        arrayList.add(bssid);
        if (i4 != -1) {
            arrayList.add(scanResults.get(i4).BSSID);
        }
        return arrayList;
    }

    private void a(n nVar) {
        this.k = nVar;
        aj.a(f5349b, "Configuration loaded");
        aj.a(f5349b, "URL:     " + this.k.a());
        aj.a(f5349b, "Version: " + this.k.b());
        k();
        this.p = new Timer();
        long c2 = this.k.c();
        long d2 = this.k.d();
        long e2 = this.k.e();
        aj.a(f5349b, "Sending logRiskMetadata every " + c2 + " seconds.");
        aj.a(f5349b, "sessionTimeout set to " + d2 + " seconds.");
        aj.a(f5349b, "compTimeout set to    " + e2 + " seconds.");
        this.f5352e = c2 * 1000;
        this.f5353f = e2 * 1000;
        u.a(d2 * 1000);
    }

    private void a(q qVar, q qVar2) {
        boolean z = true;
        if (qVar != null) {
            qVar.ag = this.n;
            JSONObject a2 = qVar2 != null ? qVar.a(qVar2) : qVar.a();
            HashMap hashMap = new HashMap();
            hashMap.put("appGuid", this.f5351d);
            hashMap.put("libraryVersion", d());
            hashMap.put("additionalData", a2.toString());
            aj.a(f5349b, "Dyson Risk Data " + a2.toString());
            if (this.k != null) {
                String g2 = this.k.g();
                boolean h2 = this.k.h();
                aj.a(f5349b, "new LogRiskMetadataRequest to: " + g2);
                aj.a(f5349b, "endpointIsStage: " + h2 + " (using SSL: " + (!h2) + ")");
                Handler handler = this.q;
                if (h2) {
                    z = false;
                }
                af.a().a(new ab(g2, hashMap, handler, z));
            }
        }
    }

    private static long b(Context context) {
        if (context == null) {
            return 0;
        }
        try {
            if (Build.VERSION.SDK_INT > 8) {
                return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).lastUpdateTime;
            }
            String str = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir;
            if (str != null) {
                return new File(str).lastModified();
            }
            return 0;
        } catch (PackageManager.NameNotFoundException e2) {
            return 0;
        }
    }

    public static String d() {
        return String.format(Locale.US, "Dyson/%S (%S %S)", "3.5.7.release", "Android", Build.VERSION.RELEASE);
    }

    private static String i() {
        return aj.b(Boolean.FALSE.booleanValue());
    }

    private String j() {
        StringBuilder sb = new StringBuilder("https://b.stats.paypal.com/counter.cgi?p=");
        if (this.r == null || this.r == ax.UNKNOWN) {
            return "Beacon not recognize host app";
        }
        int a2 = this.r.a();
        if (this.t == null) {
            return "Beacon pairing id empty";
        }
        sb.append(this.t).append("&i=");
        String b2 = aj.b();
        if (b2.equals("")) {
            try {
                sb.append(p.a("emptyIp")).append("&t=");
            } catch (IOException e2) {
                aj.a(f5349b, "error reading property file", e2);
            }
        } else {
            sb.append(b2).append("&t=");
        }
        sb.append(String.valueOf(System.currentTimeMillis() / 1000)).append("&a=").append(a2);
        aj.a(f5349b, "Beacon Request URL " + sb.toString());
        af.a().a(new w(sb.toString(), this.f5351d, this.s, aj.a(this.f5350c), this.q));
        return sb.toString();
    }

    private void k() {
        if (this.p != null) {
            this.p.cancel();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:321:0x0517, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:322:0x0518, code lost:
        com.paypal.android.sdk.aj.a(com.paypal.android.sdk.r.f5349b, "Exception Thrown in " + com.paypal.android.sdk.bl.PPRiskDataAppGuid, r4);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x01d0 A[Catch:{ Exception -> 0x06b7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x01e0 A[Catch:{ Exception -> 0x06d6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x01ed A[Catch:{ Exception -> 0x06ef }] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x01fb A[Catch:{ Exception -> 0x070e }] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0208 A[Catch:{ Exception -> 0x072a }] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0218 A[Catch:{ Exception -> 0x0743 }] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0224 A[Catch:{ Exception -> 0x075c }] */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x0230 A[Catch:{ Exception -> 0x0775 }] */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x023e A[Catch:{ Exception -> 0x078e }] */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x024d A[Catch:{ Exception -> 0x07a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x025b A[Catch:{ Exception -> 0x07c0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x026d A[Catch:{ Exception -> 0x07d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x027f A[Catch:{ Exception -> 0x07fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x0290 A[Catch:{ Exception -> 0x081c }] */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x029d A[Catch:{ Exception -> 0x083b }] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x02aa A[Catch:{ Exception -> 0x0854 }] */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x02b6 A[Catch:{ Exception -> 0x086d }] */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x02c4 A[Catch:{ Exception -> 0x0886 }] */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x02db A[Catch:{ Exception -> 0x089f }] */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x02e9 A[Catch:{ Exception -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x02f7 A[Catch:{ Exception -> 0x08bb }] */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x0303 A[Catch:{ Exception -> 0x08d4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x031f A[Catch:{ Exception -> 0x08f3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x032c A[Catch:{ Exception -> 0x090f }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b8 A[Catch:{ Exception -> 0x04a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:221:0x033c A[Catch:{ Exception -> 0x0928 }] */
    /* JADX WARNING: Removed duplicated region for block: B:225:0x034a A[Catch:{ Exception -> 0x0941 }] */
    /* JADX WARNING: Removed duplicated region for block: B:229:0x0367 A[Catch:{ Exception -> 0x095a }] */
    /* JADX WARNING: Removed duplicated region for block: B:233:0x037d A[Catch:{ Exception -> 0x0973 }] */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x0397 A[Catch:{ Exception -> 0x098c }] */
    /* JADX WARNING: Removed duplicated region for block: B:241:0x03a9 A[Catch:{ Exception -> 0x09a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:245:0x03bb A[Catch:{ Exception -> 0x09c1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x0417 A[Catch:{ Exception -> 0x09da }] */
    /* JADX WARNING: Removed duplicated region for block: B:269:0x0429 A[Catch:{ Exception -> 0x09f3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c3 A[Catch:{ Exception -> 0x04fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:273:0x043b A[Catch:{ Exception -> 0x0a0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:277:0x044d A[Catch:{ Exception -> 0x0a25 }] */
    /* JADX WARNING: Removed duplicated region for block: B:281:0x045b A[Catch:{ Exception -> 0x0a3e }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ce A[Catch:{ Exception -> 0x0517 }] */
    /* JADX WARNING: Removed duplicated region for block: B:318:0x04f4 A[Catch:{ Exception -> 0x04a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00dc A[Catch:{ Exception -> 0x0530 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0107 A[Catch:{ Exception -> 0x0549 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0115 A[Catch:{ Exception -> 0x0562 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0120 A[Catch:{ Exception -> 0x0587 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0136 A[Catch:{ Exception -> 0x05a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0144 A[Catch:{ Exception -> 0x05b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0152 A[Catch:{ Exception -> 0x05d2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x016a A[Catch:{ Exception -> 0x05eb }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0180 A[Catch:{ Exception -> 0x0604 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x018e A[Catch:{ Exception -> 0x061f }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x019c A[Catch:{ Exception -> 0x063e }] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01a9 A[Catch:{ Exception -> 0x065d }] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01b6 A[Catch:{ Exception -> 0x067c }] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01c3 A[Catch:{ Exception -> 0x069b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.paypal.android.sdk.q l() {
        /*
            r18 = this;
            r0 = r18
            android.content.Context r2 = r0.f5350c
            if (r2 != 0) goto L_0x0008
            r2 = 0
        L_0x0007:
            return r2
        L_0x0008:
            com.paypal.android.sdk.q r5 = new com.paypal.android.sdk.q
            r5.<init>()
            r0 = r18
            com.paypal.android.sdk.n r2 = r0.k     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.ai r11 = r2.i()     // Catch:{ Exception -> 0x04a9 }
            r0 = r18
            android.content.Context r2 = r0.f5350c     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r3 = "phone"
            java.lang.Object r2 = r2.getSystemService(r3)     // Catch:{ Exception -> 0x04a9 }
            android.telephony.TelephonyManager r2 = (android.telephony.TelephonyManager) r2     // Catch:{ Exception -> 0x04a9 }
            r0 = r18
            android.content.Context r3 = r0.f5350c     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = "wifi"
            java.lang.Object r3 = r3.getSystemService(r4)     // Catch:{ Exception -> 0x04a9 }
            android.net.wifi.WifiManager r3 = (android.net.wifi.WifiManager) r3     // Catch:{ Exception -> 0x04a9 }
            r0 = r18
            android.content.Context r4 = r0.f5350c     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "connectivity"
            java.lang.Object r4 = r4.getSystemService(r6)     // Catch:{ Exception -> 0x04a9 }
            android.net.ConnectivityManager r4 = (android.net.ConnectivityManager) r4     // Catch:{ Exception -> 0x04a9 }
            r7 = 0
            r6 = 0
            r0 = r18
            android.content.Context r8 = r0.f5350c     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r9 = "android.permission.ACCESS_WIFI_STATE"
            boolean r8 = com.paypal.android.sdk.aj.a(r8, r9)     // Catch:{ Exception -> 0x04a9 }
            if (r8 == 0) goto L_0x0497
            android.net.wifi.WifiInfo r8 = r3.getConnectionInfo()     // Catch:{ Exception -> 0x04a9 }
            r10 = r8
        L_0x004c:
            r0 = r18
            android.content.Context r8 = r0.f5350c     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r9 = "android.permission.ACCESS_NETWORK_STATE"
            boolean r8 = com.paypal.android.sdk.aj.a(r8, r9)     // Catch:{ Exception -> 0x04a9 }
            if (r8 == 0) goto L_0x049b
            android.net.NetworkInfo r4 = r4.getActiveNetworkInfo()     // Catch:{ Exception -> 0x04a9 }
            r9 = r4
        L_0x005d:
            r0 = r18
            android.content.Context r4 = r0.f5350c     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "android.permission.ACCESS_COARSE_LOCATION"
            boolean r4 = com.paypal.android.sdk.aj.a(r4, r8)     // Catch:{ Exception -> 0x04a9 }
            if (r4 != 0) goto L_0x0075
            r0 = r18
            android.content.Context r4 = r0.f5350c     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "android.permission.ACCESS_FINE_LOCATION"
            boolean r4 = com.paypal.android.sdk.aj.a(r4, r8)     // Catch:{ Exception -> 0x04a9 }
            if (r4 == 0) goto L_0x049f
        L_0x0075:
            r4 = 1
            r8 = r4
        L_0x0077:
            r0 = r18
            android.content.Context r4 = r0.f5350c     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r12 = "android.permission.READ_PHONE_STATE"
            boolean r12 = com.paypal.android.sdk.aj.a(r4, r12)     // Catch:{ Exception -> 0x04a9 }
            java.util.Date r13 = new java.util.Date     // Catch:{ Exception -> 0x04a9 }
            r13.<init>()     // Catch:{ Exception -> 0x04a9 }
            int r4 = r2.getPhoneType()     // Catch:{ Exception -> 0x04a9 }
            switch(r4) {
                case 0: goto L_0x04a3;
                case 1: goto L_0x04b2;
                case 2: goto L_0x04d3;
                default: goto L_0x008d;
            }     // Catch:{ Exception -> 0x04a9 }
        L_0x008d:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r14 = "unknown ("
            r4.<init>(r14)     // Catch:{ Exception -> 0x04a9 }
            int r14 = r2.getPhoneType()     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r14)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r14 = ")"
            java.lang.StringBuilder r4 = r4.append(r14)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            r5.z = r4     // Catch:{ Exception -> 0x04a9 }
        L_0x00a8:
            java.lang.String r4 = "3.5.7.release"
            r5.f5346g = r4     // Catch:{ Exception -> 0x04a9 }
            r0 = r18
            java.lang.String r4 = r0.j     // Catch:{ Exception -> 0x04a9 }
            r5.f5347h = r4     // Catch:{ Exception -> 0x04a9 }
            r0 = r18
            com.paypal.android.sdk.n r4 = r0.k     // Catch:{ Exception -> 0x04a9 }
            if (r4 != 0) goto L_0x04f4
            r4 = 0
        L_0x00b9:
            r5.i = r4     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataOsType     // Catch:{ Exception -> 0x04fe }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x04fe }
            if (r4 != 0) goto L_0x00c6
            r4 = 0
            r5.x = r4     // Catch:{ Exception -> 0x04fe }
        L_0x00c6:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataAppGuid     // Catch:{ Exception -> 0x0517 }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x0517 }
            if (r4 == 0) goto L_0x00d4
            r0 = r18
            java.lang.String r4 = r0.f5351d     // Catch:{ Exception -> 0x0517 }
            r5.f5340a = r4     // Catch:{ Exception -> 0x0517 }
        L_0x00d4:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataTimestamp     // Catch:{ Exception -> 0x0530 }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x0530 }
            if (r4 == 0) goto L_0x00e2
            long r14 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0530 }
            r5.H = r14     // Catch:{ Exception -> 0x0530 }
        L_0x00e2:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            r4.<init>()     // Catch:{ Exception -> 0x04a9 }
            r0 = r18
            java.lang.String r14 = r0.f5351d     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r14)     // Catch:{ Exception -> 0x04a9 }
            long r14 = r5.H     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r14)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = com.paypal.android.sdk.aj.a(r4)     // Catch:{ Exception -> 0x04a9 }
            r5.ad = r4     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataPairingId     // Catch:{ Exception -> 0x0549 }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x0549 }
            if (r4 == 0) goto L_0x010d
            r0 = r18
            java.lang.String r4 = r0.t     // Catch:{ Exception -> 0x0549 }
            r5.T = r4     // Catch:{ Exception -> 0x0549 }
        L_0x010d:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataPhoneType     // Catch:{ Exception -> 0x0562 }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x0562 }
            if (r4 != 0) goto L_0x0118
            r4 = 0
            r5.z = r4     // Catch:{ Exception -> 0x0562 }
        L_0x0118:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataSourceApp     // Catch:{ Exception -> 0x0587 }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x0587 }
            if (r4 == 0) goto L_0x012e
            r0 = r18
            com.paypal.android.sdk.ax r4 = r0.r     // Catch:{ Exception -> 0x0587 }
            if (r4 != 0) goto L_0x057b
            com.paypal.android.sdk.ax r4 = com.paypal.android.sdk.ax.UNKNOWN     // Catch:{ Exception -> 0x0587 }
            int r4 = r4.a()     // Catch:{ Exception -> 0x0587 }
            r5.P = r4     // Catch:{ Exception -> 0x0587 }
        L_0x012e:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataSourceAppVersion     // Catch:{ Exception -> 0x05a0 }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x05a0 }
            if (r4 == 0) goto L_0x013c
            r0 = r18
            java.lang.String r4 = r0.s     // Catch:{ Exception -> 0x05a0 }
            r5.Q = r4     // Catch:{ Exception -> 0x05a0 }
        L_0x013c:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataNotifToken     // Catch:{ Exception -> 0x05b9 }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x05b9 }
            if (r4 == 0) goto L_0x014a
            r0 = r18
            java.lang.String r4 = r0.v     // Catch:{ Exception -> 0x05b9 }
            r5.X = r4     // Catch:{ Exception -> 0x05b9 }
        L_0x014a:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataAndroidId     // Catch:{ Exception -> 0x05d2 }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x05d2 }
            if (r4 == 0) goto L_0x0162
            r0 = r18
            android.content.Context r4 = r0.f5350c     // Catch:{ Exception -> 0x05d2 }
            android.content.ContentResolver r4 = r4.getContentResolver()     // Catch:{ Exception -> 0x05d2 }
            java.lang.String r14 = "android_id"
            java.lang.String r4 = android.provider.Settings.Secure.getString(r4, r14)     // Catch:{ Exception -> 0x05d2 }
            r5.W = r4     // Catch:{ Exception -> 0x05d2 }
        L_0x0162:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataDeviceUptime     // Catch:{ Exception -> 0x05eb }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x05eb }
            if (r4 == 0) goto L_0x0170
            long r14 = android.os.SystemClock.uptimeMillis()     // Catch:{ Exception -> 0x05eb }
            r5.n = r14     // Catch:{ Exception -> 0x05eb }
        L_0x0170:
            r0 = r18
            android.content.Context r4 = r0.f5350c     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.m r14 = com.paypal.android.sdk.aj.a(r4)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataAppId     // Catch:{ Exception -> 0x0604 }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x0604 }
            if (r4 == 0) goto L_0x0186
            java.lang.String r4 = r14.a()     // Catch:{ Exception -> 0x0604 }
            r5.f5341b = r4     // Catch:{ Exception -> 0x0604 }
        L_0x0186:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataAppVersion     // Catch:{ Exception -> 0x061f }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x061f }
            if (r4 == 0) goto L_0x0194
            java.lang.String r4 = r14.b()     // Catch:{ Exception -> 0x061f }
            r5.f5342c = r4     // Catch:{ Exception -> 0x061f }
        L_0x0194:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataBaseStationId     // Catch:{ Exception -> 0x063e }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x063e }
            if (r4 == 0) goto L_0x01a1
            if (r6 != 0) goto L_0x0638
            r4 = -1
        L_0x019f:
            r5.f5343d = r4     // Catch:{ Exception -> 0x063e }
        L_0x01a1:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataCdmaNetworkId     // Catch:{ Exception -> 0x065d }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x065d }
            if (r4 == 0) goto L_0x01ae
            if (r6 != 0) goto L_0x0657
            r4 = -1
        L_0x01ac:
            r5.N = r4     // Catch:{ Exception -> 0x065d }
        L_0x01ae:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataCdmaSystemId     // Catch:{ Exception -> 0x067c }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x067c }
            if (r4 == 0) goto L_0x01bb
            if (r6 != 0) goto L_0x0676
            r4 = -1
        L_0x01b9:
            r5.M = r4     // Catch:{ Exception -> 0x067c }
        L_0x01bb:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataBssid     // Catch:{ Exception -> 0x069b }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x069b }
            if (r4 == 0) goto L_0x01c8
            if (r10 != 0) goto L_0x0695
            r4 = 0
        L_0x01c6:
            r5.f5344e = r4     // Catch:{ Exception -> 0x069b }
        L_0x01c8:
            com.paypal.android.sdk.bl r4 = com.paypal.android.sdk.bl.PPRiskDataBssidArray     // Catch:{ Exception -> 0x06b7 }
            boolean r4 = r11.a(r4)     // Catch:{ Exception -> 0x06b7 }
            if (r4 == 0) goto L_0x01d8
            if (r8 == 0) goto L_0x06b4
            java.util.ArrayList r3 = a(r3)     // Catch:{ Exception -> 0x06b7 }
        L_0x01d6:
            r5.af = r3     // Catch:{ Exception -> 0x06b7 }
        L_0x01d8:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataCellId     // Catch:{ Exception -> 0x06d6 }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x06d6 }
            if (r3 == 0) goto L_0x01e5
            if (r7 != 0) goto L_0x06d0
            r3 = -1
        L_0x01e3:
            r5.f5345f = r3     // Catch:{ Exception -> 0x06d6 }
        L_0x01e5:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataNetworkOperator     // Catch:{ Exception -> 0x06ef }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x06ef }
            if (r3 == 0) goto L_0x01f3
            java.lang.String r3 = r2.getNetworkOperator()     // Catch:{ Exception -> 0x06ef }
            r5.O = r3     // Catch:{ Exception -> 0x06ef }
        L_0x01f3:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataConnType     // Catch:{ Exception -> 0x070e }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x070e }
            if (r3 == 0) goto L_0x0200
            if (r9 != 0) goto L_0x0708
            r3 = 0
        L_0x01fe:
            r5.j = r3     // Catch:{ Exception -> 0x070e }
        L_0x0200:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataDeviceId     // Catch:{ Exception -> 0x072a }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x072a }
            if (r3 == 0) goto L_0x0210
            if (r12 == 0) goto L_0x0727
            java.lang.String r3 = r2.getDeviceId()     // Catch:{ Exception -> 0x072a }
        L_0x020e:
            r5.k = r3     // Catch:{ Exception -> 0x072a }
        L_0x0210:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataDeviceModel     // Catch:{ Exception -> 0x0743 }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x0743 }
            if (r3 == 0) goto L_0x021c
            java.lang.String r3 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0743 }
            r5.l = r3     // Catch:{ Exception -> 0x0743 }
        L_0x021c:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataDeviceName     // Catch:{ Exception -> 0x075c }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x075c }
            if (r3 == 0) goto L_0x0228
            java.lang.String r3 = android.os.Build.DEVICE     // Catch:{ Exception -> 0x075c }
            r5.m = r3     // Catch:{ Exception -> 0x075c }
        L_0x0228:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataIpAddrs     // Catch:{ Exception -> 0x0775 }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x0775 }
            if (r3 == 0) goto L_0x0236
            java.lang.String r3 = com.paypal.android.sdk.aj.b()     // Catch:{ Exception -> 0x0775 }
            r5.o = r3     // Catch:{ Exception -> 0x0775 }
        L_0x0236:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataIpAddresses     // Catch:{ Exception -> 0x078e }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x078e }
            if (r3 == 0) goto L_0x0245
            r3 = 1
            java.util.List r3 = com.paypal.android.sdk.aj.a(r3)     // Catch:{ Exception -> 0x078e }
            r5.p = r3     // Catch:{ Exception -> 0x078e }
        L_0x0245:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataLinkerId     // Catch:{ Exception -> 0x07a7 }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x07a7 }
            if (r3 == 0) goto L_0x0253
            java.lang.String r3 = com.paypal.android.sdk.aj.a()     // Catch:{ Exception -> 0x07a7 }
            r5.r = r3     // Catch:{ Exception -> 0x07a7 }
        L_0x0253:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataLocaleCountry     // Catch:{ Exception -> 0x07c0 }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x07c0 }
            if (r3 == 0) goto L_0x0265
            java.util.Locale r3 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x07c0 }
            java.lang.String r3 = r3.getCountry()     // Catch:{ Exception -> 0x07c0 }
            r5.s = r3     // Catch:{ Exception -> 0x07c0 }
        L_0x0265:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataLocaleLang     // Catch:{ Exception -> 0x07d9 }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x07d9 }
            if (r3 == 0) goto L_0x0277
            java.util.Locale r3 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x07d9 }
            java.lang.String r3 = r3.getLanguage()     // Catch:{ Exception -> 0x07d9 }
            r5.t = r3     // Catch:{ Exception -> 0x07d9 }
        L_0x0277:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataLocation     // Catch:{ Exception -> 0x07fd }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x07fd }
            if (r3 == 0) goto L_0x0288
            r0 = r18
            android.location.Location r3 = r0.o     // Catch:{ Exception -> 0x07fd }
            if (r3 != 0) goto L_0x07f2
            r3 = 0
        L_0x0286:
            r5.u = r3     // Catch:{ Exception -> 0x07fd }
        L_0x0288:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataLocationAreaCode     // Catch:{ Exception -> 0x081c }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x081c }
            if (r3 == 0) goto L_0x0295
            if (r7 != 0) goto L_0x0816
            r3 = -1
        L_0x0293:
            r5.v = r3     // Catch:{ Exception -> 0x081c }
        L_0x0295:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataMacAddrs     // Catch:{ Exception -> 0x083b }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x083b }
            if (r3 == 0) goto L_0x02a2
            if (r10 != 0) goto L_0x0835
            r3 = 0
        L_0x02a0:
            r5.w = r3     // Catch:{ Exception -> 0x083b }
        L_0x02a2:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataOsType     // Catch:{ Exception -> 0x0854 }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x0854 }
            if (r3 == 0) goto L_0x02ae
            java.lang.String r3 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x0854 }
            r5.y = r3     // Catch:{ Exception -> 0x0854 }
        L_0x02ae:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataRiskCompSessionId     // Catch:{ Exception -> 0x086d }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x086d }
            if (r3 == 0) goto L_0x02bc
            java.lang.String r3 = com.paypal.android.sdk.u.b()     // Catch:{ Exception -> 0x086d }
            r5.A = r3     // Catch:{ Exception -> 0x086d }
        L_0x02bc:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataRoaming     // Catch:{ Exception -> 0x0886 }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x0886 }
            if (r3 == 0) goto L_0x02d3
            android.telephony.ServiceState r3 = new android.telephony.ServiceState     // Catch:{ Exception -> 0x0886 }
            r3.<init>()     // Catch:{ Exception -> 0x0886 }
            boolean r3 = r3.getRoaming()     // Catch:{ Exception -> 0x0886 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ Exception -> 0x0886 }
            r5.B = r3     // Catch:{ Exception -> 0x0886 }
        L_0x02d3:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataSimOperatorName     // Catch:{ Exception -> 0x089f }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x089f }
            if (r3 == 0) goto L_0x02e1
            java.lang.String r3 = a(r2)     // Catch:{ Exception -> 0x089f }
            r5.C = r3     // Catch:{ Exception -> 0x089f }
        L_0x02e1:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataSerialNumber     // Catch:{ Exception -> 0x08bb }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x08bb }
            if (r3 == 0) goto L_0x02f1
            if (r12 == 0) goto L_0x08b8
            java.lang.String r3 = r2.getSimSerialNumber()     // Catch:{ Exception -> 0x08bb }
        L_0x02ef:
            r5.D = r3     // Catch:{ Exception -> 0x08bb }
        L_0x02f1:
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x08bb }
            r4 = 9
            if (r3 < r4) goto L_0x02fb
            java.lang.String r3 = android.os.Build.SERIAL     // Catch:{ Exception -> 0x08bb }
            r5.Y = r3     // Catch:{ Exception -> 0x08bb }
        L_0x02fb:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataSmsEnabled     // Catch:{ Exception -> 0x08d4 }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x08d4 }
            if (r3 == 0) goto L_0x0317
            r0 = r18
            android.content.Context r3 = r0.f5350c     // Catch:{ Exception -> 0x08d4 }
            android.content.pm.PackageManager r3 = r3.getPackageManager()     // Catch:{ Exception -> 0x08d4 }
            java.lang.String r4 = "android.hardware.telephony"
            boolean r3 = r3.hasSystemFeature(r4)     // Catch:{ Exception -> 0x08d4 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ Exception -> 0x08d4 }
            r5.E = r3     // Catch:{ Exception -> 0x08d4 }
        L_0x0317:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataSsid     // Catch:{ Exception -> 0x08f3 }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x08f3 }
            if (r3 == 0) goto L_0x0324
            if (r10 != 0) goto L_0x08ed
            r3 = 0
        L_0x0322:
            r5.F = r3     // Catch:{ Exception -> 0x08f3 }
        L_0x0324:
            com.paypal.android.sdk.bl r3 = com.paypal.android.sdk.bl.PPRiskDataSubscriberId     // Catch:{ Exception -> 0x090f }
            boolean r3 = r11.a(r3)     // Catch:{ Exception -> 0x090f }
            if (r3 == 0) goto L_0x0334
            if (r12 == 0) goto L_0x090c
            java.lang.String r2 = r2.getSubscriberId()     // Catch:{ Exception -> 0x090f }
        L_0x0332:
            r5.G = r2     // Catch:{ Exception -> 0x090f }
        L_0x0334:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataTotalStorageSpace     // Catch:{ Exception -> 0x0928 }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x0928 }
            if (r2 == 0) goto L_0x0342
            long r2 = com.paypal.android.sdk.aj.c()     // Catch:{ Exception -> 0x0928 }
            r5.I = r2     // Catch:{ Exception -> 0x0928 }
        L_0x0342:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataTzName     // Catch:{ Exception -> 0x0941 }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x0941 }
            if (r2 == 0) goto L_0x035f
            java.util.TimeZone r2 = java.util.TimeZone.getDefault()     // Catch:{ Exception -> 0x0941 }
            java.util.TimeZone r3 = java.util.TimeZone.getDefault()     // Catch:{ Exception -> 0x0941 }
            boolean r3 = r3.inDaylightTime(r13)     // Catch:{ Exception -> 0x0941 }
            r4 = 1
            java.util.Locale r6 = java.util.Locale.ENGLISH     // Catch:{ Exception -> 0x0941 }
            java.lang.String r2 = r2.getDisplayName(r3, r4, r6)     // Catch:{ Exception -> 0x0941 }
            r5.J = r2     // Catch:{ Exception -> 0x0941 }
        L_0x035f:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataIsDaylightSaving     // Catch:{ Exception -> 0x095a }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x095a }
            if (r2 == 0) goto L_0x0375
            java.util.TimeZone r2 = java.util.TimeZone.getDefault()     // Catch:{ Exception -> 0x095a }
            boolean r2 = r2.inDaylightTime(r13)     // Catch:{ Exception -> 0x095a }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x095a }
            r5.K = r2     // Catch:{ Exception -> 0x095a }
        L_0x0375:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataTimeZoneOffset     // Catch:{ Exception -> 0x0973 }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x0973 }
            if (r2 == 0) goto L_0x038f
            java.util.TimeZone r2 = java.util.TimeZone.getDefault()     // Catch:{ Exception -> 0x0973 }
            long r6 = r13.getTime()     // Catch:{ Exception -> 0x0973 }
            int r2 = r2.getOffset(r6)     // Catch:{ Exception -> 0x0973 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0973 }
            r5.L = r2     // Catch:{ Exception -> 0x0973 }
        L_0x038f:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataIsEmulator     // Catch:{ Exception -> 0x098c }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x098c }
            if (r2 == 0) goto L_0x03a1
            boolean r2 = com.paypal.android.sdk.cd.a()     // Catch:{ Exception -> 0x098c }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x098c }
            r5.R = r2     // Catch:{ Exception -> 0x098c }
        L_0x03a1:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataIsRooted     // Catch:{ Exception -> 0x09a5 }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x09a5 }
            if (r2 == 0) goto L_0x03b3
            boolean r2 = com.paypal.android.sdk.v.a()     // Catch:{ Exception -> 0x09a5 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x09a5 }
            r5.S = r2     // Catch:{ Exception -> 0x09a5 }
        L_0x03b3:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataKnownApps     // Catch:{ Exception -> 0x09c1 }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x09c1 }
            if (r2 == 0) goto L_0x040f
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x09c1 }
            r3.<init>()     // Catch:{ Exception -> 0x09c1 }
            r0 = r18
            com.paypal.android.sdk.n r2 = r0.k     // Catch:{ Exception -> 0x09c1 }
            if (r2 == 0) goto L_0x0406
            r0 = r18
            com.paypal.android.sdk.n r2 = r0.k     // Catch:{ Exception -> 0x09c1 }
            java.util.List r2 = r2.f()     // Catch:{ Exception -> 0x09c1 }
            java.util.Iterator r4 = r2.iterator()     // Catch:{ Exception -> 0x03fd }
        L_0x03d2:
            boolean r2 = r4.hasNext()     // Catch:{ Exception -> 0x03fd }
            if (r2 == 0) goto L_0x0406
            java.lang.Object r2 = r4.next()     // Catch:{ Exception -> 0x03fd }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x03fd }
            r0 = r18
            android.content.Context r6 = r0.f5350c     // Catch:{ Exception -> 0x03fd }
            android.content.pm.PackageManager r6 = r6.getPackageManager()     // Catch:{ Exception -> 0x03fd }
            android.content.Intent r7 = new android.content.Intent     // Catch:{ Exception -> 0x03fd }
            r7.<init>()     // Catch:{ Exception -> 0x03fd }
            android.content.ComponentName r8 = android.content.ComponentName.unflattenFromString(r2)     // Catch:{ Exception -> 0x03fd }
            android.content.Intent r7 = r7.setComponent(r8)     // Catch:{ Exception -> 0x03fd }
            boolean r6 = com.paypal.android.sdk.aj.a(r6, r7)     // Catch:{ Exception -> 0x03fd }
            if (r6 == 0) goto L_0x03d2
            r3.add(r2)     // Catch:{ Exception -> 0x03fd }
            goto L_0x03d2
        L_0x03fd:
            r2 = move-exception
            java.lang.String r2 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x09c1 }
            java.lang.String r4 = "knownApps error"
            r6 = 0
            com.paypal.android.sdk.aj.a(r2, r4, r6)     // Catch:{ Exception -> 0x09c1 }
        L_0x0406:
            int r2 = r3.size()     // Catch:{ Exception -> 0x09c1 }
            if (r2 != 0) goto L_0x09be
            r2 = 0
        L_0x040d:
            r5.q = r2     // Catch:{ Exception -> 0x09c1 }
        L_0x040f:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataAppFirstInstallTime     // Catch:{ Exception -> 0x09da }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x09da }
            if (r2 == 0) goto L_0x0421
            r0 = r18
            android.content.Context r2 = r0.f5350c     // Catch:{ Exception -> 0x09da }
            long r2 = a(r2)     // Catch:{ Exception -> 0x09da }
            r5.U = r2     // Catch:{ Exception -> 0x09da }
        L_0x0421:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataAppLastUpdateTime     // Catch:{ Exception -> 0x09f3 }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x09f3 }
            if (r2 == 0) goto L_0x0433
            r0 = r18
            android.content.Context r2 = r0.f5350c     // Catch:{ Exception -> 0x09f3 }
            long r2 = b(r2)     // Catch:{ Exception -> 0x09f3 }
            r5.V = r2     // Catch:{ Exception -> 0x09f3 }
        L_0x0433:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataGsfId     // Catch:{ Exception -> 0x0a0c }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x0a0c }
            if (r2 == 0) goto L_0x0445
            r0 = r18
            android.content.Context r2 = r0.f5350c     // Catch:{ Exception -> 0x0a0c }
            java.lang.String r2 = com.paypal.android.sdk.aj.b(r2)     // Catch:{ Exception -> 0x0a0c }
            r5.Z = r2     // Catch:{ Exception -> 0x0a0c }
        L_0x0445:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataVPNSetting     // Catch:{ Exception -> 0x0a25 }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x0a25 }
            if (r2 == 0) goto L_0x0453
            java.lang.String r2 = com.paypal.android.sdk.aj.e()     // Catch:{ Exception -> 0x0a25 }
            r5.ab = r2     // Catch:{ Exception -> 0x0a25 }
        L_0x0453:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataProxySetting     // Catch:{ Exception -> 0x0a3e }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x0a3e }
            if (r2 == 0) goto L_0x0461
            java.lang.String r2 = com.paypal.android.sdk.aj.d()     // Catch:{ Exception -> 0x0a3e }
            r5.aa = r2     // Catch:{ Exception -> 0x0a3e }
        L_0x0461:
            com.paypal.android.sdk.bl r2 = com.paypal.android.sdk.bl.PPRiskDataCounter     // Catch:{ Exception -> 0x0a57 }
            boolean r2 = r11.a(r2)     // Catch:{ Exception -> 0x0a57 }
            if (r2 == 0) goto L_0x0484
            int r2 = r5.P     // Catch:{ Exception -> 0x0a57 }
            com.paypal.android.sdk.ax r3 = com.paypal.android.sdk.ax.PAYPAL     // Catch:{ Exception -> 0x0a57 }
            int r3 = r3.a()     // Catch:{ Exception -> 0x0a57 }
            if (r2 != r3) goto L_0x0484
            r0 = r18
            android.content.Context r2 = r0.f5350c     // Catch:{ Exception -> 0x0a57 }
            com.paypal.android.sdk.aj.c(r2)     // Catch:{ Exception -> 0x0a57 }
            r0 = r18
            android.content.Context r2 = r0.f5350c     // Catch:{ Exception -> 0x0a57 }
            java.lang.String r2 = com.paypal.android.sdk.aj.d(r2)     // Catch:{ Exception -> 0x0a57 }
            r5.ac = r2     // Catch:{ Exception -> 0x0a57 }
        L_0x0484:
            r0 = r18
            java.util.Map r2 = r0.n     // Catch:{ Exception -> 0x04a9 }
            r5.ag = r2     // Catch:{ Exception -> 0x04a9 }
            r0 = r18
            java.lang.String r2 = r0.t     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r2 = com.paypal.android.sdk.aj.b(r2)     // Catch:{ Exception -> 0x04a9 }
            r5.ae = r2     // Catch:{ Exception -> 0x04a9 }
        L_0x0494:
            r2 = r5
            goto L_0x0007
        L_0x0497:
            r8 = 0
            r10 = r8
            goto L_0x004c
        L_0x049b:
            r4 = 0
            r9 = r4
            goto L_0x005d
        L_0x049f:
            r4 = 0
            r8 = r4
            goto L_0x0077
        L_0x04a3:
            java.lang.String r4 = "none"
            r5.z = r4     // Catch:{ Exception -> 0x04a9 }
            goto L_0x00a8
        L_0x04a9:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b
            java.lang.String r4 = "Exception Thrown in During Collection"
            com.paypal.android.sdk.aj.a(r3, r4, r2)
            goto L_0x0494
        L_0x04b2:
            java.lang.String r4 = "gsm"
            r5.z = r4     // Catch:{ Exception -> 0x04a9 }
            if (r8 == 0) goto L_0x04c7
            android.telephony.CellLocation r4 = r2.getCellLocation()     // Catch:{ SecurityException -> 0x04c9 }
            java.lang.Class<android.telephony.gsm.GsmCellLocation> r14 = android.telephony.gsm.GsmCellLocation.class
            java.lang.Object r4 = com.paypal.android.sdk.aj.a(r4, r14)     // Catch:{ SecurityException -> 0x04c9 }
            android.telephony.gsm.GsmCellLocation r4 = (android.telephony.gsm.GsmCellLocation) r4     // Catch:{ SecurityException -> 0x04c9 }
        L_0x04c4:
            r7 = r4
            goto L_0x00a8
        L_0x04c7:
            r4 = 0
            goto L_0x04c4
        L_0x04c9:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = "Known SecurityException on some devices: "
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x00a8
        L_0x04d3:
            java.lang.String r4 = "cdma"
            r5.z = r4     // Catch:{ Exception -> 0x04a9 }
            if (r8 == 0) goto L_0x04e8
            android.telephony.CellLocation r4 = r2.getCellLocation()     // Catch:{ SecurityException -> 0x04ea }
            java.lang.Class<android.telephony.cdma.CdmaCellLocation> r14 = android.telephony.cdma.CdmaCellLocation.class
            java.lang.Object r4 = com.paypal.android.sdk.aj.a(r4, r14)     // Catch:{ SecurityException -> 0x04ea }
            android.telephony.cdma.CdmaCellLocation r4 = (android.telephony.cdma.CdmaCellLocation) r4     // Catch:{ SecurityException -> 0x04ea }
        L_0x04e5:
            r6 = r4
            goto L_0x00a8
        L_0x04e8:
            r4 = 0
            goto L_0x04e5
        L_0x04ea:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = "Known SecurityException on some devices: "
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x00a8
        L_0x04f4:
            r0 = r18
            com.paypal.android.sdk.n r4 = r0.k     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.b()     // Catch:{ Exception -> 0x04a9 }
            goto L_0x00b9
        L_0x04fe:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataOsType     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x00c6
        L_0x0517:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataAppGuid     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x00d4
        L_0x0530:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataTimestamp     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x00e2
        L_0x0549:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataPairingId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x010d
        L_0x0562:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataPhoneType     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0118
        L_0x057b:
            r0 = r18
            com.paypal.android.sdk.ax r4 = r0.r     // Catch:{ Exception -> 0x0587 }
            int r4 = r4.a()     // Catch:{ Exception -> 0x0587 }
            r5.P = r4     // Catch:{ Exception -> 0x0587 }
            goto L_0x012e
        L_0x0587:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataSourceApp     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x012e
        L_0x05a0:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataSourceAppVersion     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x013c
        L_0x05b9:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataNotifToken     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x014a
        L_0x05d2:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataAndroidId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0162
        L_0x05eb:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataDeviceUptime     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0170
        L_0x0604:
            r4 = move-exception
            java.lang.String r15 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r17 = "Exception Thrown in "
            r16.<init>(r17)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r17 = com.paypal.android.sdk.bl.PPRiskDataAppId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r16 = r16.append(r17)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = r16.toString()     // Catch:{ Exception -> 0x04a9 }
            r0 = r16
            com.paypal.android.sdk.aj.a(r15, r0, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0186
        L_0x061f:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataAppVersion     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0194
        L_0x0638:
            int r4 = r6.getBaseStationId()     // Catch:{ Exception -> 0x063e }
            goto L_0x019f
        L_0x063e:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataBaseStationId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x01a1
        L_0x0657:
            int r4 = r6.getNetworkId()     // Catch:{ Exception -> 0x065d }
            goto L_0x01ac
        L_0x065d:
            r4 = move-exception
            java.lang.String r14 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r16 = "Exception Thrown in "
            r15.<init>(r16)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r16 = com.paypal.android.sdk.bl.PPRiskDataCdmaNetworkId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r14, r15, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x01ae
        L_0x0676:
            int r4 = r6.getSystemId()     // Catch:{ Exception -> 0x067c }
            goto L_0x01b9
        L_0x067c:
            r4 = move-exception
            java.lang.String r6 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = "Exception Thrown in "
            r14.<init>(r15)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r15 = com.paypal.android.sdk.bl.PPRiskDataCdmaSystemId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r6, r14, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x01bb
        L_0x0695:
            java.lang.String r4 = r10.getBSSID()     // Catch:{ Exception -> 0x069b }
            goto L_0x01c6
        L_0x069b:
            r4 = move-exception
            java.lang.String r6 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r15 = "Exception Thrown in "
            r14.<init>(r15)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r15 = com.paypal.android.sdk.bl.PPRiskDataBssid     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r6, r14, r4)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x01c8
        L_0x06b4:
            r3 = 0
            goto L_0x01d6
        L_0x06b7:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataBssidArray     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x01d8
        L_0x06d0:
            int r3 = r7.getCid()     // Catch:{ Exception -> 0x06d6 }
            goto L_0x01e3
        L_0x06d6:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataCellId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x01e5
        L_0x06ef:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataNetworkOperator     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x01f3
        L_0x0708:
            java.lang.String r3 = r9.getTypeName()     // Catch:{ Exception -> 0x070e }
            goto L_0x01fe
        L_0x070e:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataConnType     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0200
        L_0x0727:
            r3 = 0
            goto L_0x020e
        L_0x072a:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataDeviceId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0210
        L_0x0743:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataDeviceModel     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x021c
        L_0x075c:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataDeviceName     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0228
        L_0x0775:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataIpAddrs     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0236
        L_0x078e:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataIpAddresses     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0245
        L_0x07a7:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataLinkerId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0253
        L_0x07c0:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataLocaleCountry     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0265
        L_0x07d9:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataLocaleLang     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0277
        L_0x07f2:
            android.location.Location r3 = new android.location.Location     // Catch:{ Exception -> 0x07fd }
            r0 = r18
            android.location.Location r4 = r0.o     // Catch:{ Exception -> 0x07fd }
            r3.<init>(r4)     // Catch:{ Exception -> 0x07fd }
            goto L_0x0286
        L_0x07fd:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r8 = "Exception Thrown in "
            r6.<init>(r8)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r8 = com.paypal.android.sdk.bl.PPRiskDataLocation     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0288
        L_0x0816:
            int r3 = r7.getLac()     // Catch:{ Exception -> 0x081c }
            goto L_0x0293
        L_0x081c:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r7 = "Exception Thrown in "
            r6.<init>(r7)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r7 = com.paypal.android.sdk.bl.PPRiskDataLocationAreaCode     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0295
        L_0x0835:
            java.lang.String r3 = r10.getMacAddress()     // Catch:{ Exception -> 0x083b }
            goto L_0x02a0
        L_0x083b:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r7 = "Exception Thrown in "
            r6.<init>(r7)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r7 = com.paypal.android.sdk.bl.PPRiskDataMacAddrs     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x02a2
        L_0x0854:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r7 = "Exception Thrown in "
            r6.<init>(r7)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r7 = com.paypal.android.sdk.bl.PPRiskDataOsType     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x02ae
        L_0x086d:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r7 = "Exception Thrown in "
            r6.<init>(r7)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r7 = com.paypal.android.sdk.bl.PPRiskDataRiskCompSessionId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x02bc
        L_0x0886:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r7 = "Exception Thrown in "
            r6.<init>(r7)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r7 = com.paypal.android.sdk.bl.PPRiskDataRoaming     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x02d3
        L_0x089f:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r7 = "Exception Thrown in "
            r6.<init>(r7)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r7 = com.paypal.android.sdk.bl.PPRiskDataSimOperatorName     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x02e1
        L_0x08b8:
            r3 = 0
            goto L_0x02ef
        L_0x08bb:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r7 = "Exception Thrown in "
            r6.<init>(r7)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r7 = com.paypal.android.sdk.bl.PPRiskDataSerialNumber     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x02fb
        L_0x08d4:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r7 = "Exception Thrown in "
            r6.<init>(r7)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r7 = com.paypal.android.sdk.bl.PPRiskDataSmsEnabled     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0317
        L_0x08ed:
            java.lang.String r3 = r10.getSSID()     // Catch:{ Exception -> 0x08f3 }
            goto L_0x0322
        L_0x08f3:
            r3 = move-exception
            java.lang.String r4 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r7 = "Exception Thrown in "
            r6.<init>(r7)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r7 = com.paypal.android.sdk.bl.PPRiskDataSsid     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r4, r6, r3)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0324
        L_0x090c:
            r2 = 0
            goto L_0x0332
        L_0x090f:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataSubscriberId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0334
        L_0x0928:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataTotalStorageSpace     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0342
        L_0x0941:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataTzName     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x035f
        L_0x095a:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataIsDaylightSaving     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0375
        L_0x0973:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataTimeZoneOffset     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x038f
        L_0x098c:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataIsEmulator     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x03a1
        L_0x09a5:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataIsRooted     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x03b3
        L_0x09be:
            r2 = r3
            goto L_0x040d
        L_0x09c1:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataKnownApps     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x040f
        L_0x09da:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataAppFirstInstallTime     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0421
        L_0x09f3:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataAppLastUpdateTime     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0433
        L_0x0a0c:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataGsfId     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0445
        L_0x0a25:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataVPNSetting     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0453
        L_0x0a3e:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataProxySetting     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0461
        L_0x0a57:
            r2 = move-exception
            java.lang.String r3 = com.paypal.android.sdk.r.f5349b     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r6 = "Exception Thrown in "
            r4.<init>(r6)     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.bl r6 = com.paypal.android.sdk.bl.PPRiskDataCounter     // Catch:{ Exception -> 0x04a9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x04a9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x04a9 }
            com.paypal.android.sdk.aj.a(r3, r4, r2)     // Catch:{ Exception -> 0x04a9 }
            goto L_0x0484
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.sdk.r.l():com.paypal.android.sdk.q");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.aj.a(java.util.Map, java.lang.String, java.lang.Boolean):boolean
     arg types: [java.util.Map, java.lang.String, int]
     candidates:
      com.paypal.android.sdk.aj.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.paypal.android.sdk.aj.a(java.util.Map, java.lang.String, java.lang.String):java.lang.String
      com.paypal.android.sdk.aj.a(int, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.aj.a(java.lang.String, java.lang.String, java.lang.Throwable):void
      com.paypal.android.sdk.aj.a(java.util.Map, java.lang.String, java.lang.Boolean):boolean */
    public final String a(Context context, String str, ax axVar, String str2, Map map) {
        String a2 = aj.a(map, "RISK_MANAGER_CONF_URL", (String) null);
        String a3 = aj.a(map, "RISK_MANAGER_PAIRING_ID", (String) null);
        this.v = aj.a(map, "RISK_MANAGER_NOTIF_TOKEN", (String) null);
        f5348a = (z) aj.a(map, z.class, "RISK_MANAGER_NETWORK_ADAPTER", new ac());
        boolean a4 = aj.a(map, "RISK_MANAGER_IS_DISABLE_REMOTE_CONFIG", (Boolean) false);
        this.u = false;
        this.f5350c = context;
        this.f5351d = aj.c(context, str);
        if (axVar == null) {
            this.r = ax.UNKNOWN;
        } else {
            this.r = axVar;
        }
        this.s = str2;
        this.l = null;
        this.m = null;
        this.f5355h = 0;
        this.f5354g = 0;
        if (a3 == null || a3.trim().length() == 0) {
            this.t = i();
        } else {
            aj.a(3, "PRD", "Using custom pairing id");
            this.t = a3.trim();
        }
        try {
            this.j = a2 == null ? "https://www.paypalobjects.com/webstatic/risk/dyson_config_android_v3.json" : a2;
            f();
            if (this.q == null) {
                this.q = new t(this);
                LocationManager locationManager = (LocationManager) this.f5350c.getSystemService(FirebaseAnalytics.Param.LOCATION);
                if (locationManager != null) {
                    onLocationChanged(aj.a(locationManager));
                    if (locationManager.isProviderEnabled("network")) {
                        locationManager.requestLocationUpdates("network", 3600000, 10.0f, this);
                    }
                }
            }
            k();
        } catch (Exception e2) {
            aj.a(f5349b, (String) null, e2);
        }
        j();
        a(new n(this.f5350c, !a4));
        return this.t;
    }

    public final void a(Message message) {
        String str;
        try {
            switch (message.what) {
                case 0:
                    aj.a(f5349b, "Dyson Async URL: " + message.obj);
                    return;
                case 1:
                    aj.a(f5349b, "LogRiskMetadataRequest failed." + ((Exception) message.obj).getMessage());
                    return;
                case 2:
                    String str2 = (String) message.obj;
                    aj.a(f5349b, "LogRiskMetadataRequest Server returned: " + str2);
                    try {
                        str = Uri.parse("?" + str2).getQueryParameter("responseEnvelope.ack");
                    } catch (UnsupportedOperationException e2) {
                        str = null;
                    }
                    if ("Success".equals(str)) {
                        aj.a(f5349b, "LogRiskMetadataRequest Success");
                        return;
                    }
                    return;
                case 10:
                    aj.a(f5349b, "Load Configuration URL: " + message.obj);
                    return;
                case 11:
                    aj.a(f5349b, "LoadConfigurationRequest failed.");
                    return;
                case 12:
                    n nVar = (n) message.obj;
                    if (nVar != null) {
                        a(nVar);
                        return;
                    }
                    return;
                case 20:
                    aj.a(f5349b, "Beacon URL: " + message.obj);
                    return;
                case 21:
                    aj.a(f5349b, "BeaconRequest failed " + ((Exception) message.obj).getMessage());
                    return;
                case 22:
                    aj.a(f5349b, "Beacon returned: " + message.obj);
                    return;
                default:
                    return;
            }
        } catch (Exception e3) {
            aj.a(f5349b, (String) null, e3);
        }
        aj.a(f5349b, (String) null, e3);
    }

    public final void b() {
        new Timer().schedule(new s(this), 0);
    }

    public final JSONObject c() {
        u.a();
        this.l = l();
        if (this.l == null) {
            return null;
        }
        return this.l.a();
    }

    public final void e() {
        u.a();
        this.l = l();
        a(this.l, (q) null);
    }

    public final void f() {
        aj.a(f5349b, "Host activity detected");
        this.i = System.currentTimeMillis();
    }

    public final String g() {
        return a((String) null, (Map) null);
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
            this.o = new Location(location);
            aj.a(f5349b, "Location Update: " + location.toString());
        }
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i2, Bundle bundle) {
    }
}
