package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.text.TextUtils;
import com.paypal.android.sdk.bw;
import com.paypal.android.sdk.ce;
import com.paypal.android.sdk.cy;
import com.paypal.android.sdk.dp;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.ey;
import com.paypal.android.sdk.fs;
import com.paypal.android.sdk.gb;
import java.util.Locale;
import java.util.Timer;

public final class PaymentMethodActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f5134a = PaymentMethodActivity.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private Timer f5135b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f5136c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f5137d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public boolean f5138e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f5139f;

    /* renamed from: g  reason: collision with root package name */
    private gb f5140g;

    /* renamed from: h  reason: collision with root package name */
    private bt f5141h;
    /* access modifiers changed from: private */
    public PayPalService i;
    private final ServiceConnection j = new cv(this);
    private boolean k;

    static void a(Activity activity, int i2, PayPalConfiguration payPalConfiguration) {
        Intent intent = new Intent(activity, PaymentMethodActivity.class);
        intent.putExtras(activity.getIntent());
        intent.putExtra("com.paypal.android.sdk.paypalConfiguration", payPalConfiguration);
        activity.startActivityForResult(intent, 1);
    }

    static /* synthetic */ void a(PaymentMethodActivity paymentMethodActivity) {
        paymentMethodActivity.i.a(ey.SelectPayPalPayment);
        PaymentConfirmActivity.a(paymentMethodActivity, 2, dh.PayPal, null, paymentMethodActivity.i.d(), true);
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.f5138e && !this.f5137d) {
            this.f5140g.m.setImageBitmap(bw.c("iVBORw0KGgoAAAANSUhEUgAAADcAAAAsCAYAAADByiAeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAohJREFUeNrcWYGRgjAQJMwXQAl0IFbwWMFrBS8VvHYAFagVoBVIB2IFaAV8B08H/GUm/GA+CUouBL2ZTByEwCZ3m70LcRCsrusQutDBswranhBSOTYNgC1rM1ZA82yD+6nN2Urn21xNYAF0Jmf33Ro45DhDH9+1ObN3mMe84yVXTusd7ojjTds7iIzeoZtAU7mE1/E/5p536fj/yvbFb+VmDK2sn9c27b2RcKuVOs9vdJVnVN0QBsyHrjAQQ9RNMmhn5j5tC1g8zQ0A3AK4dbNqKbJ7nJjevIeYKN3HBpSOjy2h6DjznuxLQR4xpZvbYj4MX5+CO2S9aJvGCCEL+LnGEgBv2EEsWRXqIj4HJpeA3ML9FRq5IbhioEiHCsVz6V9s/H9WlwdiDHArSfycHpicpWSM0ia4UvJRRY+xlsiJcKwrnBPBtbSnLEt594b427O9cvisgL2cr6XobMobwbWdDXAitvvUTW8E5JTbAHcWXMOQUiHnHRcb4CrBXoYhBiYSjTooOH5GfSRB4I8BnI/xAV0eoTNxaOCUWfBjdh0DOFFsZAjgMkGtZnBCCTH3pIb2BewY2gDn8bkbU/q5xpgJ5t6pK7++BNcWHdUqmUV8GsQUj72irGD1KNvNHgQY8VJOIccGz+c8ybhxR9pyVOSCsW4+11S/asOZeMCIwWvdnyvuxygzJhgrh3pYiHiYqZ3P8XXI4t6SniTJ3WAWhjHBNUripKqNKNyQFoVXmB+DFXOqWDyw/tLEGItBCv6DpUkmTouSBlyJqOrHYlHjlocXA0Y9JGvAbSWpxrPajoaAy6mKVwBIDyHjG7ZkanyqKXxtu+IacEQ3bCmgZt8gixlhZdEBzK8AAwBIvuGtI5K/kgAAAABJRU5ErkJggg==", this));
            this.f5140g.m.setVisibility(0);
            this.f5140g.m.setContentDescription(ev.a(fs.SCAN_CARD_ICON_DESCRIPTION));
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        new StringBuilder().append(f5134a).append(".refreshPayment");
        if (dl.a(this, this.i)) {
            this.i.h();
        }
        PayPalPayment a2 = this.f5141h.a();
        String a3 = cy.a(Locale.getDefault(), ce.a().c().a(), a2.c().doubleValue(), a2.f(), true);
        this.f5140g.f4974c.f4891d.setText(a2.d());
        this.f5140g.f4974c.f4890c.setText(a3);
        if (!this.i.j() || !this.i.c().f4670g.a()) {
            this.f5140g.f4975d.setVisibility(8);
            this.f5140g.f4973b.setVisibility(8);
        } else {
            String r = this.i.r();
            if (!TextUtils.isEmpty(r)) {
                this.f5140g.f4975d.setText(r);
                this.f5140g.f4975d.setVisibility(0);
                this.f5140g.f4973b.setVisibility(0);
            } else {
                this.f5140g.f4975d.setVisibility(8);
                this.f5140g.f4973b.setVisibility(8);
            }
        }
        if (this.i.d().i()) {
            new cx(this, (byte) 0).execute(new Void[0]);
            dp s = this.i.s();
            if (s == null || !s.b()) {
                this.f5140g.f4978g.setVisibility(8);
                this.f5140g.f4976e.setText(ev.a(fs.PAY_WITH_CARD));
                this.f5140g.k.setVisibility(8);
            } else {
                this.f5137d = true;
                this.f5140g.m.setVisibility(8);
                this.f5140g.f4976e.setText(s.d());
                Enum a4 = cf.a(s);
                this.f5140g.f4978g.setImageBitmap(cf.a(this, a4));
                this.f5140g.f4978g.setContentDescription(a4.toString());
                this.f5140g.f4978g.setVisibility(0);
                this.f5140g.k.setText(ev.a(fs.CLEAR_CREDIT_CARD_INFO));
                this.f5140g.k.setVisibility(0);
                this.f5140g.f4979h.setVisibility(0);
                this.f5140g.k.setVisibility(0);
            }
            b();
        } else {
            this.f5140g.f4979h.setVisibility(8);
            this.f5140g.k.setVisibility(8);
        }
        cf.a(this.f5140g.i.f4944b, this.i.d().b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    static /* synthetic */ void c(PaymentMethodActivity paymentMethodActivity) {
        paymentMethodActivity.i.a(ey.SelectCreditCardPayment);
        dp s = paymentMethodActivity.i.s();
        if (s == null || !s.b()) {
            String a2 = paymentMethodActivity.i.d().a();
            Intent intent = new Intent(paymentMethodActivity, cf.a("io.card.payment.CardIOActivity"));
            intent.putExtra(cf.a("io.card.payment.CardIOActivity", "EXTRA_LANGUAGE_OR_LOCALE"), a2);
            intent.putExtra(cf.a("io.card.payment.CardIOActivity", "EXTRA_REQUIRE_EXPIRY"), true);
            intent.putExtra(cf.a("io.card.payment.CardIOActivity", "EXTRA_REQUIRE_CVV"), true);
            new StringBuilder("startActivityForResult(").append(intent).append(", 1").append(")");
            paymentMethodActivity.startActivityForResult(intent, 1);
            return;
        }
        PaymentConfirmActivity.a(paymentMethodActivity, 2, dh.CreditCardToken, null, paymentMethodActivity.i.d());
    }

    private void d() {
        this.k = bindService(cf.b(this), this.j, 1);
    }

    static /* synthetic */ void i(PaymentMethodActivity paymentMethodActivity) {
        if (!paymentMethodActivity.f5139f) {
            paymentMethodActivity.f5139f = true;
            paymentMethodActivity.i.a(ey.PaymentMethodWindow);
        }
        boolean z = !paymentMethodActivity.f5136c && (!paymentMethodActivity.i.d().i() || paymentMethodActivity.i.s() == null);
        new StringBuilder("autoAdvanceToPayPalConfirmIfLoggedIn: ").append(z);
        if (!dl.a(paymentMethodActivity, paymentMethodActivity.i) && ((!paymentMethodActivity.i.d().i() && !paymentMethodActivity.f5136c) || (z && paymentMethodActivity.i.j() && paymentMethodActivity.i.c().f4670g.a()))) {
            paymentMethodActivity.showDialog(3);
            paymentMethodActivity.f5136c = true;
            paymentMethodActivity.f5135b = new Timer();
            paymentMethodActivity.f5135b.schedule(new cs(paymentMethodActivity), 1000);
            paymentMethodActivity.f5136c = true;
        }
        paymentMethodActivity.c();
    }

    /* access modifiers changed from: protected */
    public final void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        new StringBuilder().append(f5134a).append(".onActivityResult (requestCode: ").append(i2).append(", resultCode: ").append(i3).append(")");
        switch (i2) {
            case 1:
                if (intent != null && intent.hasExtra(cf.a("io.card.payment.CardIOActivity", "EXTRA_SCAN_RESULT"))) {
                    PaymentConfirmActivity.a(this, 2, dh.CreditCard, intent.getParcelableExtra(cf.a("io.card.payment.CardIOActivity", "EXTRA_SCAN_RESULT")), (PayPalConfiguration) getIntent().getParcelableExtra("com.paypal.android.sdk.paypalConfiguration"), true);
                    return;
                }
                return;
            case 2:
                if (i3 == -1) {
                    Intent intent2 = new Intent();
                    intent2.putExtra("com.paypal.android.sdk.paymentConfirmation", (PaymentConfirmation) intent.getParcelableExtra("com.paypal.android.sdk.paymentConfirmation"));
                    setResult(i3, intent2);
                    finish();
                    return;
                } else if (i3 == 0) {
                    this.f5136c = true;
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public final void onBackPressed() {
        new StringBuilder().append(f5134a).append(".onBackPressed");
        if (this.i != null) {
            this.i.a(ey.PaymentMethodCancel);
        }
        if (this.f5135b != null) {
            this.f5135b.cancel();
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new StringBuilder().append(f5134a).append(".onCreate");
        setTheme(16973934);
        requestWindowFeature(8);
        d();
        this.f5140g = new gb(this);
        this.f5141h = new bt(getIntent());
        setContentView(this.f5140g.f4972a);
        cf.a(this, this.f5140g.l, fs.YOUR_ORDER);
        this.f5140g.f4977f.setText(ev.a(fs.PAY_WITH));
        this.f5140g.f4973b.setText(ev.a(fs.LOG_OUT_BUTTON));
        this.f5140g.j.setOnClickListener(new co(this));
        this.f5140g.f4973b.setOnClickListener(new cp(this));
        this.f5140g.f4979h.setOnClickListener(new cq(this));
        this.f5140g.k.setOnClickListener(new cr(this));
        if (bundle == null) {
            if (!cf.a(this)) {
                finish();
            }
            this.f5139f = false;
        } else {
            this.f5136c = bundle.getBoolean("PP_PreventAutoLogin");
            this.f5139f = bundle.getBoolean("PP_PageTrackingSent");
        }
        this.f5135b = null;
    }

    /* access modifiers changed from: protected */
    public final Dialog onCreateDialog(int i2, Bundle bundle) {
        switch (i2) {
            case 1:
                return cf.a(this, fs.LOG_OUT, fs.CONFIRM_LOG_OUT, new ct(this));
            case 2:
                return cf.a(this, fs.CLEAR_CC_ALERT_TITLE, fs.CONFIRM_CLEAR_CREDIT_CARD_INFO, new cu(this));
            case 3:
                return cf.a(this, fs.AUTHENTICATING, fs.ONE_MOMENT);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        new StringBuilder().append(f5134a).append(".onDestroy");
        if (this.k) {
            unbindService(this.j);
            this.k = false;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void onRestart() {
        super.onRestart();
        d();
    }

    /* access modifiers changed from: protected */
    public final void onResume() {
        super.onResume();
        new StringBuilder().append(f5134a).append(".onResume");
        if (this.i != null) {
            c();
        }
    }

    /* access modifiers changed from: protected */
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        new StringBuilder().append(f5134a).append(".onSaveInstanceState");
        bundle.putBoolean("PP_PreventAutoLogin", this.f5136c);
        bundle.putBoolean("PP_PageTrackingSent", this.f5139f);
    }

    public final void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        this.f5140g.f4974c.a();
    }
}
