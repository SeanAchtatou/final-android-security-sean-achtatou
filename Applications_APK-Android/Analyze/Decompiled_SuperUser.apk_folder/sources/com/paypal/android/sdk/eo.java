package com.paypal.android.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class eo extends dw {
    public eo(db dbVar, bu buVar, x xVar, String str) {
        super(dbVar, buVar, xVar, str);
    }

    /* access modifiers changed from: protected */
    public final void b(JSONObject jSONObject) {
        String str;
        String str2;
        String str3;
        String string = jSONObject.getString("name");
        String string2 = jSONObject.getString("message");
        try {
            JSONObject jSONObject2 = jSONObject.getJSONArray("details").getJSONObject(0);
            String string3 = jSONObject2.getString("field");
            String string4 = jSONObject2.getString("issue");
            if (string.equals("VALIDATION_ERROR") && string3.contains("amount.currency")) {
                string = "pp_service_error_bad_currency";
            }
            String str4 = string4;
            str = string3;
            str2 = string;
            str3 = str4;
        } catch (JSONException e2) {
            str = "";
            str2 = string;
            str3 = "";
        }
        a(str2, string2, "field:" + str + ", issue:" + str3);
    }
}
