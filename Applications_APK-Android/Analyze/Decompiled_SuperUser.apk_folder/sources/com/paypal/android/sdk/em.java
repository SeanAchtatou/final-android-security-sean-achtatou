package com.paypal.android.sdk;

import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class em extends eo {

    /* renamed from: a  reason: collision with root package name */
    public String f4818a;

    /* renamed from: b  reason: collision with root package name */
    private en f4819b;

    /* renamed from: c  reason: collision with root package name */
    private Map f4820c;

    /* renamed from: d  reason: collision with root package name */
    private ei[] f4821d;

    /* renamed from: e  reason: collision with root package name */
    private String f4822e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f4823f;

    /* renamed from: g  reason: collision with root package name */
    private String f4824g;

    /* renamed from: h  reason: collision with root package name */
    private String f4825h;
    private String i;
    private String j;
    private String k;

    public em(db dbVar, bu buVar, x xVar, String str, String str2, String str3, en enVar, Map map, ei[] eiVarArr, String str4, boolean z, String str5, String str6, String str7) {
        super(dbVar, buVar, xVar, str);
        this.f4818a = str3;
        this.f4819b = enVar;
        this.f4820c = map;
        this.f4821d = eiVarArr;
        this.f4822e = str4;
        this.f4823f = z;
        this.f4824g = str7;
        if (cd.a((CharSequence) this.f4824g)) {
            this.f4824g = "sale";
        }
        this.f4824g = this.f4824g.toLowerCase(Locale.US);
        a("PayPal-Request-Id", str2);
        if (cd.b((CharSequence) str5)) {
            a("PayPal-Partner-Attribution-Id", str5);
        }
        if (cd.b((CharSequence) str6)) {
            a("PayPal-Client-Metadata-Id", str6);
        }
    }

    private static String a(JSONArray jSONArray) {
        JSONArray jSONArray2;
        JSONObject jSONObject;
        JSONObject jSONObject2;
        if (jSONArray == null) {
            return null;
        }
        try {
            JSONObject jSONObject3 = jSONArray.getJSONObject(0);
            if (jSONObject3 == null || (jSONArray2 = jSONObject3.getJSONArray("related_resources")) == null || (jSONObject = jSONArray2.getJSONObject(0)) == null || (jSONObject2 = jSONObject.getJSONObject("authorization")) == null) {
                return null;
            }
            return jSONObject2.optString("id");
        } catch (JSONException e2) {
            return null;
        }
    }

    public final boolean A() {
        return this.f4823f;
    }

    /* access modifiers changed from: protected */
    public final en B() {
        return this.f4819b;
    }

    public final String C() {
        return this.f4825h;
    }

    public final String D() {
        return this.i;
    }

    public final String E() {
        return this.f4824g;
    }

    public final String F() {
        return this.j;
    }

    public final String G() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public abstract void a(JSONObject jSONObject);

    public final String b() {
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.accumulate("intent", this.f4824g);
        JSONObject jSONObject3 = new JSONObject();
        JSONArray y = y();
        if (y != null) {
            jSONObject3.accumulate("funding_instruments", y);
        }
        jSONObject3.accumulate("payment_method", z());
        jSONObject2.accumulate("payer", jSONObject3);
        en enVar = this.f4819b;
        JSONObject jSONObject4 = new JSONObject();
        jSONObject4.accumulate(FirebaseAnalytics.Param.CURRENCY, enVar.b().getCurrencyCode());
        jSONObject4.accumulate("total", enVar.a().toPlainString());
        if (this.f4820c != null && !this.f4820c.isEmpty()) {
            if (this.f4820c == null || this.f4820c.isEmpty()) {
                jSONObject = null;
            } else {
                jSONObject = new JSONObject();
                if (this.f4820c.containsKey(FirebaseAnalytics.Param.SHIPPING)) {
                    jSONObject.accumulate(FirebaseAnalytics.Param.SHIPPING, this.f4820c.get(FirebaseAnalytics.Param.SHIPPING));
                }
                if (this.f4820c.containsKey("subtotal")) {
                    jSONObject.accumulate("subtotal", this.f4820c.get("subtotal"));
                }
                if (this.f4820c.containsKey(FirebaseAnalytics.Param.TAX)) {
                    jSONObject.accumulate(FirebaseAnalytics.Param.TAX, this.f4820c.get(FirebaseAnalytics.Param.TAX));
                }
            }
            jSONObject4.accumulate("details", jSONObject);
        }
        JSONObject jSONObject5 = new JSONObject();
        jSONObject5.accumulate("amount", jSONObject4);
        jSONObject5.accumulate("description", this.f4822e);
        if (this.f4821d != null && this.f4821d.length > 0) {
            JSONObject jSONObject6 = new JSONObject();
            jSONObject6.accumulate("items", ei.a(this.f4821d));
            jSONObject5.accumulate(FirebaseAnalytics.Param.ITEM_LIST, jSONObject6);
        }
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(jSONObject5);
        a(jSONObject5);
        jSONObject2.accumulate("transactions", jSONArray);
        return jSONObject2.toString();
    }

    public final void c() {
        JSONObject m = m();
        try {
            this.f4825h = m.getString("state");
            this.i = m.optString("id");
            this.j = m.optString("create_time");
            this.k = a(m.getJSONArray("transactions"));
        } catch (JSONException e2) {
            d();
        }
    }

    public final void d() {
        b(m());
    }

    /* access modifiers changed from: protected */
    public JSONArray y() {
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract String z();
}
