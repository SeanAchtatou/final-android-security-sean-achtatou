package com.paypal.android.sdk;

public abstract class aw {

    /* renamed from: a  reason: collision with root package name */
    private String f4574a;

    /* renamed from: b  reason: collision with root package name */
    private String f4575b;

    private aw(String str) {
    }

    protected aw(String str, String str2) {
        this(str);
        this.f4574a = str2;
        this.f4575b = null;
    }

    protected aw(String str, String str2, String str3, String str4) {
        this(str);
        this.f4574a = str2;
        this.f4575b = str3;
    }

    public final String a() {
        return this.f4575b;
    }

    public final String b() {
        return this.f4574a;
    }

    public String toString() {
        return "ErrorBase[mErrorCode=" + this.f4574a + " mErrorMsgShort=" + this.f4575b + "]";
    }
}
