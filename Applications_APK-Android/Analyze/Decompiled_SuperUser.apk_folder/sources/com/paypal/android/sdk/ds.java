package com.paypal.android.sdk;

import android.os.Parcel;
import android.os.Parcelable;

public final class ds extends cp implements Parcelable {
    public static final Parcelable.Creator CREATOR = new cn();

    /* renamed from: c  reason: collision with root package name */
    private boolean f4758c;

    /* renamed from: d  reason: collision with root package name */
    private String f4759d;

    static {
        ds.class.getSimpleName();
    }

    public ds(Parcel parcel) {
        super(parcel);
        this.f4759d = parcel.readString();
        this.f4758c = parcel.readByte() != 0;
    }

    public ds(String str, String str2, long j, boolean z) {
        this.f4681a = str;
        this.f4682b = j;
        this.f4759d = str2;
        this.f4758c = z;
    }

    public final boolean a() {
        return this.f4758c;
    }

    public final int describeContents() {
        return 0;
    }

    public final String toString() {
        return ds.class.getSimpleName() + "(token:" + this.f4681a + ", mGoodUntil:" + this.f4682b + ", isCreatedInternally:" + this.f4758c + ")";
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f4681a);
        parcel.writeLong(this.f4682b);
        parcel.writeString(this.f4759d);
        parcel.writeByte((byte) (this.f4758c ? 1 : 0));
    }
}
