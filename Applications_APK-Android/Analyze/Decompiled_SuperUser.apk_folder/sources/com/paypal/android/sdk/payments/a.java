package com.paypal.android.sdk.payments;

import com.paypal.android.sdk.dz;
import com.paypal.android.sdk.ed;

class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5157a = a.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private bj f5158b;

    /* renamed from: c  reason: collision with root package name */
    private Object f5159c;

    /* renamed from: d  reason: collision with root package name */
    private bf f5160d;

    a() {
    }

    private void b(bf bfVar) {
        bfVar.a(this.f5159c);
        boolean z = !(this.f5159c instanceof dz) && !(this.f5159c instanceof ed);
        this.f5159c = null;
        if (z) {
            this.f5160d = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.f5159c == null) {
            this.f5159c = "success";
        }
        if (this.f5160d != null) {
            b(this.f5160d);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(bf bfVar) {
        if (this.f5159c != null) {
            b(bfVar);
        } else if (this.f5158b != null) {
            bfVar.a(this.f5158b);
            this.f5158b = null;
            this.f5160d = null;
        } else {
            this.f5160d = bfVar;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(bj bjVar) {
        if (this.f5160d != null) {
            this.f5160d.a(bjVar);
        } else {
            this.f5158b = bjVar;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Object obj) {
        this.f5159c = obj;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        this.f5160d = null;
    }
}
