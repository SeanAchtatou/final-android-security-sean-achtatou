package com.paypal.android.sdk.payments;

import android.content.Intent;
import com.paypal.android.sdk.ak;
import com.paypal.android.sdk.cd;
import com.paypal.android.sdk.df;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

final class ba extends ea {

    /* renamed from: c  reason: collision with root package name */
    private boolean f5189c;

    ba(Intent intent, PayPalConfiguration payPalConfiguration, boolean z) {
        super(intent, payPalConfiguration);
        this.f5189c = z;
    }

    private static boolean a(String str) {
        try {
            new URL(str);
            return true;
        } catch (MalformedURLException e2) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        boolean b2 = cd.b((CharSequence) this.f5302b.l());
        a(b2, "merchantName");
        boolean z = this.f5302b.m() != null && cd.a(PayPalFuturePaymentActivity.class.getSimpleName(), this.f5302b.m().toString(), "merchantPrivacyPolicyUrl") && a(this.f5302b.m().toString());
        a(z, "merchantPrivacyPolicyUrl");
        boolean z2 = this.f5302b.n() != null && cd.a(PayPalFuturePaymentActivity.class.getSimpleName(), this.f5302b.n().toString(), "merchantUserAgreementUrl") && a(this.f5302b.n().toString());
        a(z2, "merchantUserAgreementUrl");
        boolean z3 = !this.f5189c;
        if (this.f5189c) {
            PayPalOAuthScopes payPalOAuthScopes = (PayPalOAuthScopes) this.f5301a.getParcelableExtra("com.paypal.android.sdk.requested_scopes");
            if (payPalOAuthScopes != null) {
                if (payPalOAuthScopes.a() != null && payPalOAuthScopes.a().size() > 0) {
                    Iterator it = payPalOAuthScopes.a().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            z3 = true;
                            break;
                        }
                        String str = (String) it.next();
                        if (!ak.i.contains(str) && !df.i.contains(str)) {
                            z3 = false;
                            break;
                        }
                    }
                } else {
                    z3 = false;
                }
            } else {
                z3 = false;
            }
        }
        a(z3, "paypalScopes");
        return b2 && z && z2 && z3;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return PayPalFuturePaymentActivity.class.getSimpleName();
    }
}
