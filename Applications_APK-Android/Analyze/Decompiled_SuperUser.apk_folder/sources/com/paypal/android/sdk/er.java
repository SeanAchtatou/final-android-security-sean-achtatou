package com.paypal.android.sdk;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.PhoneNumberUtils;
import java.util.HashMap;
import java.util.Locale;

public class er implements Parcelable {
    public static final Parcelable.Creator CREATOR = new dj();

    /* renamed from: c  reason: collision with root package name */
    private static HashMap f4841c;

    /* renamed from: a  reason: collision with root package name */
    private ee f4842a;

    /* renamed from: b  reason: collision with root package name */
    private String f4843b;

    static {
        HashMap hashMap = new HashMap();
        f4841c = hashMap;
        hashMap.put("US", "1");
        f4841c.put("CA", "1");
        f4841c.put("GB", "44");
        f4841c.put("FR", "33");
        f4841c.put("IT", "39");
        f4841c.put("ES", "34");
        f4841c.put("AU", "61");
        f4841c.put("MY", "60");
        f4841c.put("SG", "65");
        f4841c.put("AR", "54");
        f4841c.put("UK", "44");
        f4841c.put("ZA", "27");
        f4841c.put("GR", "30");
        f4841c.put("NL", "31");
        f4841c.put("BE", "32");
        f4841c.put("SG", "65");
        f4841c.put("PT", "351");
        f4841c.put("LU", "352");
        f4841c.put("IE", "353");
        f4841c.put("IS", "354");
        f4841c.put("MT", "356");
        f4841c.put("CY", "357");
        f4841c.put("FI", "358");
        f4841c.put("HU", "36");
        f4841c.put("LT", "370");
        f4841c.put("LV", "371");
        f4841c.put("EE", "372");
        f4841c.put("SI", "386");
        f4841c.put("CH", "41");
        f4841c.put("CZ", "420");
        f4841c.put("SK", "421");
        f4841c.put("AT", "43");
        f4841c.put("DK", "45");
        f4841c.put("SE", "46");
        f4841c.put("NO", "47");
        f4841c.put("PL", "48");
        f4841c.put("DE", "49");
        f4841c.put("MX", "52");
        f4841c.put("BR", "55");
        f4841c.put("NZ", "64");
        f4841c.put("TH", "66");
        f4841c.put("JP", "81");
        f4841c.put("KR", "82");
        f4841c.put("HK", "852");
        f4841c.put("CN", "86");
        f4841c.put("TW", "886");
        f4841c.put("TR", "90");
        f4841c.put("IN", "91");
        f4841c.put("IL", "972");
        f4841c.put("MC", "377");
        f4841c.put("CR", "506");
        f4841c.put("CL", "56");
        f4841c.put("VE", "58");
        f4841c.put("EC", "593");
        f4841c.put("UY", "598");
    }

    public er(Parcel parcel) {
        this.f4842a = (ee) parcel.readParcelable(ee.class.getClassLoader());
        this.f4843b = parcel.readString();
    }

    public er(dh dhVar, ee eeVar, String str) {
        a(eeVar, dhVar.a(de.e(str)));
    }

    public er(dh dhVar, String str) {
        a(dhVar.d(), dhVar.a(de.e(str)));
    }

    public static er a(dh dhVar, String str) {
        String[] split = str.split("[|]");
        if (split.length == 2) {
            return new er(dhVar, new ee(split[0]), split[1]);
        }
        throw new ek("");
    }

    private void a(ee eeVar, String str) {
        this.f4842a = eeVar;
        this.f4843b = str;
    }

    public final String a() {
        return this.f4843b;
    }

    public final String a(dh dhVar) {
        return dhVar.b().equals(Locale.US) ? PhoneNumberUtils.formatNumber(this.f4843b) : this.f4843b;
    }

    public final String b() {
        return this.f4842a.a() + "|" + this.f4843b;
    }

    public final String c() {
        return (String) f4841c.get(this.f4842a.a());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f4842a, 0);
        parcel.writeString(this.f4843b);
    }
}
