package com.paypal.android.sdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class af {

    /* renamed from: a  reason: collision with root package name */
    private List f4539a;

    /* renamed from: b  reason: collision with root package name */
    private List f4540b;

    static {
        af.class.getSimpleName();
    }

    private af() {
        this.f4539a = Collections.synchronizedList(new ArrayList());
        this.f4540b = Collections.synchronizedList(new ArrayList());
    }

    /* synthetic */ af(byte b2) {
        this();
    }

    public static af a() {
        return ag.f4541a;
    }

    private void b() {
        if (!this.f4540b.isEmpty()) {
            synchronized (this) {
                if (!this.f4540b.isEmpty()) {
                    ae aeVar = (ae) this.f4540b.get(0);
                    this.f4540b.remove(0);
                    this.f4539a.add(aeVar);
                    new Thread(aeVar).start();
                }
            }
        }
    }

    public final void a(ae aeVar) {
        this.f4540b.add(aeVar);
        if (this.f4539a.size() < 3) {
            b();
        }
    }

    public final void b(ae aeVar) {
        this.f4539a.remove(aeVar);
        b();
    }
}
