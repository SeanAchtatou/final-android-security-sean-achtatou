package com.paypal.android.sdk;

import java.io.IOException;
import okhttp3.e;
import okhttp3.f;
import okhttp3.x;

final class bo implements f {

    /* renamed from: a  reason: collision with root package name */
    private final bt f4618a;

    private bo(bi biVar, bt btVar) {
        this.f4618a = btVar;
    }

    /* synthetic */ bo(bi biVar, bt btVar, byte b2) {
        this(biVar, btVar);
    }

    public final void a(e eVar, IOException iOException) {
        this.f4618a.b(iOException.getMessage());
        String unused = bi.f4595a;
        new StringBuilder().append(this.f4618a.n()).append(" failure: ").append(iOException.getMessage());
    }

    public final void a(e eVar, x xVar) {
        this.f4618a.b(xVar.g().d());
        String unused = bi.f4595a;
        new StringBuilder().append(this.f4618a.n()).append(" success");
    }
}
