package com.paypal.android.sdk.payments;

import android.text.Editable;
import android.text.TextWatcher;

final class ah implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ LoginActivity f5173a;

    ah(LoginActivity loginActivity) {
        this.f5173a = loginActivity;
    }

    public final void afterTextChanged(Editable editable) {
        this.f5173a.i();
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
