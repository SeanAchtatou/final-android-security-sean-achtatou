package com.paypal.android.sdk;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

public class aa extends ae {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4524a = aa.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private Context f4525b;

    /* renamed from: c  reason: collision with root package name */
    private String f4526c;

    /* renamed from: d  reason: collision with root package name */
    private Handler f4527d;

    public aa(Context context, String str, Handler handler) {
        this.f4525b = context;
        this.f4526c = str;
        this.f4527d = handler;
    }

    public void run() {
        aj.a(f4524a, "entering LoadConfigurationRequest.");
        if (this.f4527d != null) {
            try {
                this.f4527d.sendMessage(Message.obtain(this.f4527d, 10, this.f4526c));
                this.f4527d.sendMessage(Message.obtain(this.f4527d, 12, new n(this.f4525b, this.f4526c)));
            } catch (Exception e2) {
                aj.a(f4524a, "LoadConfigurationRequest loading remote config failed.", e2);
                this.f4527d.sendMessage(Message.obtain(this.f4527d, 11, e2));
            } finally {
                af.a().b(this);
            }
            aj.a(f4524a, "leaving LoadConfigurationRequest.");
        }
    }
}
