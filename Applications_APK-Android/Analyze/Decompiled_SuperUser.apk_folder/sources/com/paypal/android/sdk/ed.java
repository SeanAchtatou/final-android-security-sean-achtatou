package com.paypal.android.sdk;

import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class ed extends bs {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4792a = ed.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private Map f4793b;

    /* renamed from: c  reason: collision with root package name */
    private String f4794c;

    /* renamed from: d  reason: collision with root package name */
    private String f4795d;

    /* renamed from: e  reason: collision with root package name */
    private String f4796e;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ed(com.paypal.android.sdk.bu r7, com.paypal.android.sdk.x r8, java.lang.String r9, java.lang.String r10, java.lang.String r11) {
        /*
            r6 = this;
            com.paypal.android.sdk.cb r1 = new com.paypal.android.sdk.cb
            com.paypal.android.sdk.db r0 = com.paypal.android.sdk.db.GetAppInfoRequest
            r1.<init>(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "Bearer "
            r0.<init>(r2)
            boolean r2 = com.paypal.android.sdk.an.a(r9)
            if (r2 == 0) goto L_0x0018
            if (r10 != 0) goto L_0x0018
            java.lang.String r10 = "mock_euat"
        L_0x0018:
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r4 = r0.toString()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "/"
            r0.<init>(r2)
            java.lang.StringBuilder r0 = r0.append(r11)
            java.lang.String r5 = r0.toString()
            r0 = r6
            r2 = r7
            r3 = r8
            r0.<init>(r1, r2, r3, r4, r5)
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r6.f4793b = r0
            java.lang.String r0 = "Content-Type"
            java.lang.String r1 = "application/x-www-form-urlencoded"
            r6.a(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.sdk.ed.<init>(com.paypal.android.sdk.bu, com.paypal.android.sdk.x, java.lang.String, java.lang.String, java.lang.String):void");
    }

    private static void a(JSONArray jSONArray, Map map) {
        if (jSONArray != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                if (jSONObject != null) {
                    String optString = jSONObject.optString("name");
                    String optString2 = jSONObject.optString(FirebaseAnalytics.Param.VALUE);
                    if (cd.b((CharSequence) optString) && cd.b((CharSequence) optString2)) {
                        map.put(optString, optString2);
                    }
                }
            }
        }
    }

    public final String b() {
        return "";
    }

    public final void c() {
        JSONObject jSONObject;
        JSONObject m = m();
        JSONArray optJSONArray = m.optJSONArray("capabilities");
        if (optJSONArray != null) {
            int i = 0;
            while (true) {
                if (i >= optJSONArray.length()) {
                    jSONObject = null;
                    break;
                }
                JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                if (jSONObject2 != null && "PAYPAL_ACCESS".equals(jSONObject2.optString("name"))) {
                    jSONObject = jSONObject2;
                    break;
                }
                i++;
            }
            if (jSONObject != null) {
                a(jSONObject.optJSONArray("attributes"), this.f4793b);
                new StringBuilder("Attributes: ").append(this.f4793b.toString());
            }
        }
        JSONArray optJSONArray2 = m.optJSONArray("attributes");
        if (optJSONArray2 != null) {
            HashMap hashMap = new HashMap();
            a(optJSONArray2, hashMap);
            this.f4794c = (String) hashMap.get("privacy_policy_url");
            this.f4795d = (String) hashMap.get("user_agreement_url");
            this.f4796e = (String) hashMap.get("display_name");
        }
    }

    public final void d() {
    }

    public final String e() {
        return " {\n     \"attributes\": [\n         {\n             \"name\": \"display_name\",\n             \"value\": \"Example Merchant\"\n         },\n         {\n             \"name\": \"privacy_policy_url\",\n             \"value\": \"http://www.example.com/privacy-policy\"\n         },\n         {\n             \"name\": \"user_agreement_url\",\n             \"value\": \"http://www.example.com/user-agreement\"\n         }\n     ],\n     \"name\": \"LiveTestApp\",\n     \"capabilities\": [\n         {\n             \"scopes\": [],\n             \"name\": \"PAYPAL_ACCESS\",\n             \"attributes\": [\n                 {\n                     \"name\": \"openid_connect\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_date_of_birth\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_fullname\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_gender\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_zip\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_language\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_city\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_country\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_timezone\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_email\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_street_address1\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_street_address2\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_phone_number\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_locale\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_state\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_age_range\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_account_verified\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_account_creation_date\",\n                     \"value\": \"Y\"\n                 },\n                 {\n                     \"name\": \"oauth_account_type\",\n                     \"value\": \"Y\"\n                 }\n             ]\n         },\n         {\n             \"scopes\": [\n                 \"https://api.paypal.com/v1/payments/.*\",\n                 \"https://api.paypal.com/v1/vault/credit-card\",\n                 \"https://api.paypal.com/v1/vault/credit-card/.*\"\n             ],\n             \"name\": \"PAYMENT\",\n             \"features\": [\n                 {\n                     \"status\": \"ACTIVE\",\n                     \"name\": \"ACCEPT_CARD\"\n                 },\n                 {\n                     \"status\": \"ACTIVE\",\n                     \"name\": \"ACCEPT_PAYPAL\"\n                 }\n             ]\n         }\n     ]\n }    ";
    }

    public final Map t() {
        return this.f4793b;
    }

    public final String u() {
        return this.f4794c;
    }

    public final String v() {
        return this.f4795d;
    }

    public final String w() {
        return this.f4796e;
    }
}
