package com.paypal.android.sdk;

public final class cu implements av {

    /* renamed from: a  reason: collision with root package name */
    private ct f4686a;

    public cu(ct ctVar) {
        this.f4686a = ctVar;
    }

    public final void a(bt btVar) {
        if (btVar instanceof ep) {
            if (btVar.q()) {
                this.f4686a.a((ep) btVar);
            } else {
                this.f4686a.b((ep) btVar);
            }
        } else if (btVar instanceof eg) {
            if (btVar.q()) {
                this.f4686a.a((eg) btVar);
            } else {
                this.f4686a.b((eg) btVar);
            }
        } else if (btVar instanceof ea) {
            if (btVar.q()) {
                this.f4686a.a((ea) btVar);
            } else {
                this.f4686a.b((ea) btVar);
            }
        } else if (btVar instanceof eq) {
            if (btVar.q()) {
                this.f4686a.a((eq) btVar);
            } else {
                this.f4686a.b((eq) btVar);
            }
        } else if (btVar instanceof eb) {
            if (btVar.q()) {
                this.f4686a.a();
            } else {
                this.f4686a.a((eb) btVar);
            }
        } else if (btVar instanceof dy) {
            if (btVar.q()) {
                this.f4686a.a((dy) btVar);
            } else {
                this.f4686a.b((dy) btVar);
            }
        } else if (btVar instanceof dz) {
            if (btVar.q()) {
                this.f4686a.a((dz) btVar);
            } else {
                this.f4686a.b((dz) btVar);
            }
        } else if (btVar instanceof dt) {
            if (btVar.q()) {
                this.f4686a.a((dt) btVar);
            } else {
                this.f4686a.b((dt) btVar);
            }
        } else if (btVar instanceof ed) {
            if (btVar.q()) {
                this.f4686a.a((ed) btVar);
            } else {
                this.f4686a.b((ed) btVar);
            }
        } else if (!(btVar instanceof ef)) {
            throw new RuntimeException("not handled");
        } else if (btVar.q()) {
            this.f4686a.a((ef) btVar);
        } else {
            this.f4686a.b((ef) btVar);
        }
    }
}
