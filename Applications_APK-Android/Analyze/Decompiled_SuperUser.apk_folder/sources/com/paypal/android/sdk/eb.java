package com.paypal.android.sdk;

public final class eb extends eo {

    /* renamed from: a  reason: collision with root package name */
    private String f4791a;

    public eb(bu buVar, x xVar, String str, String str2) {
        super(db.DeleteCreditCardRequest, buVar, xVar, str);
        this.f4791a = str2;
    }

    public final String b() {
        return this.f4791a;
    }

    public final void c() {
    }

    public final void d() {
        b(m());
    }

    public final String e() {
        return "mockDeleteCreditCardResponse";
    }
}
