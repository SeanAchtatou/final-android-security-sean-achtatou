package com.paypal.android.sdk;

import android.os.Parcel;
import android.os.Parcelable;

public class ee implements Parcelable {
    public static final Parcelable.Creator CREATOR = new cx();

    /* renamed from: a  reason: collision with root package name */
    private final String f4797a;

    public ee(Parcel parcel) {
        this.f4797a = parcel.readString();
    }

    public ee(String str) {
        if (str.equals("OTHER") || str.length() == 2) {
            this.f4797a = str;
        } else {
            this.f4797a = "US";
        }
    }

    public final String a() {
        return this.f4797a;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f4797a.equals(((ee) obj).f4797a);
    }

    public int hashCode() {
        return this.f4797a.hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f4797a);
    }
}
