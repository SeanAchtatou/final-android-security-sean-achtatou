package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;

final class am implements Parcelable.Creator {
    am() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new PayPalConfiguration(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new PayPalConfiguration[i];
    }
}
