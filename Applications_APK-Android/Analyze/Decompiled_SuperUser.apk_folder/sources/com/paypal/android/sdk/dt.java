package com.paypal.android.sdk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class dt extends eo {

    /* renamed from: a  reason: collision with root package name */
    private final String f4760a;

    /* renamed from: b  reason: collision with root package name */
    private final String f4761b;

    /* renamed from: c  reason: collision with root package name */
    private final boolean f4762c;

    /* renamed from: d  reason: collision with root package name */
    private final JSONObject f4763d;

    /* renamed from: e  reason: collision with root package name */
    private final JSONObject f4764e;

    /* renamed from: f  reason: collision with root package name */
    private JSONObject f4765f;

    /* renamed from: g  reason: collision with root package name */
    private String f4766g;

    /* renamed from: h  reason: collision with root package name */
    private String f4767h;
    private String i;
    private String j;

    public dt(bu buVar, x xVar, String str, String str2, boolean z, String str3, String str4, String str5, String str6, JSONObject jSONObject, JSONObject jSONObject2) {
        super(db.ApproveAndExecuteSfoPaymentRequest, buVar, xVar, str);
        this.f4762c = z;
        this.f4760a = str5;
        this.f4761b = str6;
        this.f4763d = jSONObject;
        this.f4764e = jSONObject2;
        a("PayPal-Request-Id", str2);
        if (cd.b((CharSequence) str3)) {
            a("PayPal-Partner-Attribution-Id", str3);
        }
        if (cd.b((CharSequence) str4)) {
            a("PayPal-Client-Metadata-Id", str4);
        }
    }

    private static String a(JSONArray jSONArray) {
        JSONArray jSONArray2;
        JSONObject jSONObject;
        if (jSONArray == null) {
            return null;
        }
        try {
            JSONObject jSONObject2 = jSONArray.getJSONObject(0);
            if (jSONObject2 == null || (jSONArray2 = jSONObject2.getJSONArray("related_resources")) == null || (jSONObject = jSONArray2.getJSONObject(0)) == null) {
                return null;
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("authorization");
            if (optJSONObject != null) {
                return optJSONObject.optString("id");
            }
            JSONObject optJSONObject2 = jSONObject.optJSONObject("order");
            if (optJSONObject2 != null) {
                return optJSONObject2.optString("id");
            }
            return null;
        } catch (JSONException e2) {
            return null;
        }
    }

    public final String b() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.accumulate("payment_id", this.f4760a);
        jSONObject.accumulate("session_id", this.f4761b);
        if (this.f4764e != null) {
            jSONObject.accumulate("funding_option", this.f4764e);
        }
        if (this.f4763d != null) {
            jSONObject.accumulate("shipping_address", this.f4763d);
        }
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.accumulate("device_info", cd.a(da.a().toString()));
        jSONObject2.accumulate("app_info", cd.a(cw.a().toString()));
        jSONObject2.accumulate("risk_data", cd.a(r.a().c().toString()));
        jSONObject.accumulate("client_info", jSONObject2);
        return jSONObject.toString();
    }

    public final void c() {
        try {
            this.f4765f = m().getJSONObject("payment");
            this.f4766g = this.f4765f.optString("state");
            this.f4767h = this.f4765f.optString("create_time");
            this.i = this.f4765f.optString("intent");
            this.j = a(this.f4765f.getJSONArray("transactions"));
        } catch (JSONException e2) {
            d();
        }
    }

    public final void d() {
        b(m());
    }

    public final String e() {
        return "{     \"payment\": {         \"id\": \"PAY-6PU626847B294842SKPEWXHY\",         \"transactions\": [             {                 \"amount\": {                     \"total\": \"2.85\",                     \"details\": {                         \"subtotal\": \"2.85\"                     },                     \"currency\": \"USD\"                 },                 \"description\": \"Awesome Sauce\",                 \"related_resources\": [                     {                         \"sale\": {                             \"amount\": {                                 \"total\": \"2.85\",                                 \"currency\": \"USD\"                             },                             \"id\": \"5LR21373K59921925\",                             \"parent_payment\": \"PAY-6PU626847B294842SKPEWXHY\",                             \"update_time\": \"2014-07-18T18:47:06Z\",                             \"state\": \"completed\",                             \"create_time\": \"2014-07-18T18:46:55Z\",                             \"links\": [                                 {                                     \"method\": \"GET\",                                     \"rel\": \"self\",                                     \"href\": \"https://www.stage2std019.stage.\"                                 },                                 {                                     \"method\": \"POST\",                                     \"rel\": \"refund\",                                     \"href\": \"https://www.stage2std019.stage. \"                                 },                                 {                                     \"method\": \"GET\",                                     \"rel\": \"parent_payment\",                                     \"href\": \"https://www.stage2std019.stage.PEWXHY \"                                 }                             ]                         }                     }                 ]             }         ],         \"update_time\": \"2014-07-18T18:47:06Z\",         \"payer\": {             \"payer_info\": {                 \"shipping_address\": {                                      }             },             \"payment_method\": \"paypal\"         },         \"state\": \"approved\",         \"create_time\": \"2014-07-18T18:46:55Z\",         \"links\": [             {                 \"method\": \"GET\",                 \"rel\": \"self\",                 \"href\": \"https://www.stage2std019.stage.paypal.\"             }         ],         \"intent\": \"sale\"     } } ";
    }

    public final void l() {
        r.a().g();
    }

    public final String t() {
        return this.f4760a;
    }

    public final boolean u() {
        return this.f4762c;
    }

    public final String v() {
        return this.f4766g;
    }

    public final String w() {
        return this.f4767h;
    }

    public final String x() {
        return this.i;
    }

    public final String y() {
        return this.j;
    }
}
