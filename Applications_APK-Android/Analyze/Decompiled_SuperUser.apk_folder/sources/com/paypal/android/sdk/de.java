package com.paypal.android.sdk;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class de {

    /* renamed from: a  reason: collision with root package name */
    private static List f4709a = Arrays.asList("AU", "BR", "CA", "ES", "FR", "GB", "IT", "MY", "SG", "US");

    /* renamed from: b  reason: collision with root package name */
    private static Pattern f4710b = Pattern.compile("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,} *$");

    /* renamed from: c  reason: collision with root package name */
    private static Pattern f4711c = Pattern.compile("^[0-9]{4,8}$");

    /* renamed from: d  reason: collision with root package name */
    private static Pattern f4712d = Pattern.compile("^\\+?[0-9]{7,14}$");

    /* renamed from: e  reason: collision with root package name */
    private static Pattern f4713e = Pattern.compile("[ .\\-\\(\\)]*");

    /* renamed from: f  reason: collision with root package name */
    private static Pattern f4714f = Pattern.compile("^\\+?0+$");

    /* renamed from: g  reason: collision with root package name */
    private static /* synthetic */ boolean f4715g = (!de.class.desiredAssertionStatus());

    public static boolean a(String str) {
        if (f4715g || str != null) {
            return f4710b.matcher(str).matches();
        }
        throw new AssertionError();
    }

    public static boolean b(String str) {
        if (f4715g || str != null) {
            return f4711c.matcher(str).matches();
        }
        throw new AssertionError();
    }

    public static boolean c(String str) {
        return str.length() >= 8;
    }

    public static boolean d(String str) {
        if (f4715g || str != null) {
            String replaceAll = f4713e.matcher(str).replaceAll("");
            return f4712d.matcher(replaceAll).matches() && !f4714f.matcher(replaceAll).matches();
        }
        throw new AssertionError();
    }

    public static String e(String str) {
        return f4713e.matcher(str).replaceAll("");
    }

    public static boolean f(String str) {
        if (cd.a((CharSequence) str)) {
            return false;
        }
        return f4709a.contains(str.toUpperCase());
    }
}
