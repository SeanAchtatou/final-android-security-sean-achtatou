package com.paypal.android.sdk;

import java.util.ArrayList;
import java.util.List;

public class at {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4571a = at.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final List f4572b = new ArrayList();

    public final void a() {
        synchronized (this.f4572b) {
            for (au remove : this.f4572b) {
                this.f4572b.remove(remove);
            }
        }
    }

    public final void a(av avVar) {
        synchronized (this.f4572b) {
            for (au auVar : this.f4572b) {
                if (auVar.f4573a == avVar) {
                    new StringBuilder("Ignoring attempt to re-register listener ").append(avVar);
                    return;
                }
            }
            this.f4572b.add(new au(this, avVar));
        }
    }

    public final void a(bt btVar, long j) {
        new StringBuilder("dispatching ").append(btVar.n());
        if (btVar.o() < 0) {
            new StringBuilder("discarding ").append(btVar.n());
            return;
        }
        ArrayList<au> arrayList = new ArrayList<>();
        synchronized (this.f4572b) {
            for (au add : this.f4572b) {
                arrayList.add(0, add);
            }
        }
        for (au auVar : arrayList) {
            auVar.f4573a.a(btVar);
        }
    }
}
