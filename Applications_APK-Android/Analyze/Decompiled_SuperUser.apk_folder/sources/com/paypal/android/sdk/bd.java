package com.paypal.android.sdk;

import io.fabric.sdk.android.services.common.a;
import okhttp3.r;
import okhttp3.x;

public final class bd implements r {

    /* renamed from: a  reason: collision with root package name */
    private final String f4586a;

    public bd(String str) {
        this.f4586a = str == null ? null : str.replaceAll("[^\\x00-\\x7F]", "");
    }

    public final x a(r.a aVar) {
        return aVar.a(aVar.a().e().b(a.HEADER_USER_AGENT).b(a.HEADER_USER_AGENT, this.f4586a).b());
    }
}
