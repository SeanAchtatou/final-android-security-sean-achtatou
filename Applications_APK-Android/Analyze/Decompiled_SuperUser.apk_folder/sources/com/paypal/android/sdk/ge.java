package com.paypal.android.sdk;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public final class ge {

    /* renamed from: a  reason: collision with root package name */
    public LinearLayout f4985a;

    /* renamed from: b  reason: collision with root package name */
    public RelativeLayout f4986b;

    /* renamed from: c  reason: collision with root package name */
    public final Button f4987c;

    /* renamed from: d  reason: collision with root package name */
    private final TableLayout f4988d;

    /* renamed from: e  reason: collision with root package name */
    private ImageView f4989e;

    /* renamed from: f  reason: collision with root package name */
    private TextView f4990f;

    /* renamed from: g  reason: collision with root package name */
    private TextView f4991g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, float):void
     arg types: [android.widget.RelativeLayout, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, float):void
     arg types: [android.widget.Button, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void */
    public ge(Context context) {
        this.f4985a = new LinearLayout(context);
        this.f4985a.setLayoutParams(bw.a());
        this.f4985a.setOrientation(1);
        bw.a(this.f4985a);
        this.f4988d = new TableLayout(context);
        this.f4988d.setColumnShrinkable(0, false);
        this.f4988d.setColumnStretchable(0, false);
        this.f4988d.setColumnStretchable(1, false);
        this.f4988d.setColumnShrinkable(1, false);
        TableRow tableRow = new TableRow(context);
        this.f4988d.addView(tableRow);
        this.f4985a.addView(this.f4988d);
        this.f4986b = new RelativeLayout(context);
        tableRow.addView(this.f4986b);
        bw.a((View) this.f4986b, 19, 1.0f);
        bw.b(this.f4986b, null, null, "10dip", null);
        this.f4991g = new TextView(context);
        bw.d(this.f4991g, 0);
        this.f4991g.setId(2301);
        this.f4986b.addView(this.f4991g);
        bw.b(this.f4991g, "6dip", null, null, null);
        this.f4990f = new TextView(context);
        bw.b(this.f4990f, 0);
        this.f4990f.setId(2302);
        this.f4986b.addView(this.f4990f, bw.a(-2, -2, 3, 2301));
        bw.b(this.f4990f, "6dip", null, null, null);
        this.f4989e = bw.a(context, "iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAATZJREFUeNrsmMENgkAQRVnPUoAWQB0WoAVoAViA3tW7FEADUIDebUAaoAELwDv+TcaEkFUOsDhj5ieTTdgQ5jHLZ3aDQKVSqVSqAVTX9Q4RSUw8RKSIO+KGmI/5fNMzeZvsGdF88yVia4ypxgCY9Lx/ipi1rkUExb8CVIXFh4SvqMKRPQBBrDAcHFMJIHL2AG8XwrB2TJ0AcWEPQBB2ySwdUxtAlBIAQgxpy5WsKnKmkjVAAyJzuJMXezU+ykp/ZFuJsA0BgA17gAZE5ttevQF02GsOiIQ9AEHEGGJf9uodoMNe7UddsAcgiMyHvU4C4dIl9NcfsWgbFf0jE91KiG7mxLfTojc0oreUojf1oo9VvthlgeS3Y7QpfZu5J+LhsMu9mG7w14e7Q4LIPF5XqVQqlWi9BBgAacm2vqgEoPIAAAAASUVORK5CYII=", "go to selection");
        this.f4989e.setId(2307);
        this.f4989e.setColorFilter(bv.f4638g);
        RelativeLayout.LayoutParams a2 = bw.a(context, "20dip", "20dip", 15);
        a2.addRule(1, 2302);
        a2.addRule(1, 2301);
        this.f4986b.addView(this.f4989e, a2);
        this.f4987c = new Button(context);
        this.f4987c.setId(2305);
        bw.a(this.f4987c, 21);
        this.f4987c.setTextSize(18.0f);
        tableRow.addView(this.f4987c);
        bw.b(this.f4987c, null, null, "6dip", null);
        bw.a((View) this.f4987c, 21, 1.0f);
        bw.a(this.f4985a);
        this.f4985a.setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, int):void
     arg types: [android.widget.TextView, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void */
    public final void a(String str) {
        this.f4990f.setText(str);
        bw.a((View) this.f4990f, -2, -1);
        this.f4990f.setEllipsize(TextUtils.TruncateAt.START);
    }

    public final void a(boolean z) {
        this.f4986b.setClickable(z);
        this.f4989e.setVisibility(z ? 0 : 8);
    }

    public final void b(String str) {
        this.f4991g.setText(str);
    }
}
