package com.paypal.android.sdk;

public final class u {

    /* renamed from: a  reason: collision with root package name */
    private static String f5358a = null;

    /* renamed from: b  reason: collision with root package name */
    private static long f5359b = 0;

    /* renamed from: c  reason: collision with root package name */
    private static long f5360c = 0;

    public static synchronized void a() {
        synchronized (u.class) {
            f5358a = aj.b(Boolean.TRUE.booleanValue());
            f5359b = System.currentTimeMillis();
        }
    }

    public static synchronized void a(long j) {
        synchronized (u.class) {
            f5360c = j;
        }
    }

    public static synchronized String b() {
        String str;
        synchronized (u.class) {
            if (f5358a == null) {
                a();
            }
            str = f5358a;
        }
        return str;
    }
}
