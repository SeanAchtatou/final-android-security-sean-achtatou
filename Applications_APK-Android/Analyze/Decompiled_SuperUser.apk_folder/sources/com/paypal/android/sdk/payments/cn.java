package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;

final class cn implements Parcelable.Creator {
    cn() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new PaymentConfirmation(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new PaymentConfirmation[i];
    }
}
