package com.paypal.android.sdk;

import android.util.Base64;
import io.fabric.sdk.android.services.common.a;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class eh extends bs {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4806a = eh.class.getSimpleName();

    /* renamed from: g  reason: collision with root package name */
    public String f4807g;

    /* renamed from: h  reason: collision with root package name */
    public boolean f4808h;
    public Map i;

    public eh(db dbVar, bu buVar, x xVar, String str) {
        super(new cb(dbVar), buVar, xVar, str);
        a(a.HEADER_ACCEPT, "application/json; charset=utf-8");
        a("Accept-Language", "en_US");
        a("Content-Type", "application/x-www-form-urlencoded");
    }

    protected static String a(JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(jSONObject);
        return cd.a(jSONArray.toString());
    }

    protected static String b(String str, String str2) {
        return "Basic " + ((!an.a(str) || str2 != null) ? new String(Base64.encode(str2.getBytes(), 2)) + ":" : "mock:");
    }

    /* access modifiers changed from: protected */
    public final void b(JSONObject jSONObject) {
        String string = jSONObject.getString("error");
        String optString = jSONObject.optString("error_description");
        if (jSONObject.has("nonce")) {
            this.f4807g = jSONObject.getString("nonce");
        }
        if (jSONObject.has("2fa_enabled") && jSONObject.getBoolean("2fa_enabled")) {
            this.f4808h = true;
            this.i = new LinkedHashMap();
            if (jSONObject.has("2fa_token_identifier")) {
                JSONArray jSONArray = jSONObject.getJSONArray("2fa_token_identifier");
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    String string2 = jSONObject2.getString("type");
                    String string3 = jSONObject2.getString("token_identifier");
                    String string4 = jSONObject2.getString("token_identifier_display");
                    if ("sms_otp".equals(string2)) {
                        this.i.put(string3, string4);
                        new StringBuilder("adding token [").append(string3).append(",").append(string4).append("]");
                    } else {
                        new StringBuilder("skipping token [").append(string3).append(",").append(string4).append("], as the type is not supported:").append(string2);
                    }
                }
            }
        }
        a(string, optString, null);
    }
}
