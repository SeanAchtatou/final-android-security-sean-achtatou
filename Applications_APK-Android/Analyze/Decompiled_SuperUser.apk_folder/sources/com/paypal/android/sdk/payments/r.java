package com.paypal.android.sdk.payments;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

final class r implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LoginActivity f5324a;

    r(LoginActivity loginActivity) {
        this.f5324a = loginActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        new StringBuilder().append(LoginActivity.class.getSimpleName()).append(".onServiceConnected");
        PayPalService unused = this.f5324a.r = ((bh) iBinder).f5201a;
        if (this.f5324a.r.a(new s(this))) {
            this.f5324a.a();
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        PayPalService unused = this.f5324a.r = (PayPalService) null;
    }
}
