package com.paypal.android.sdk.payments;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

final class dv implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ dn f5287a;

    dv(dn dnVar) {
        this.f5287a = dnVar;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        new StringBuilder().append(dn.f5269d).append(".onServiceConnected");
        this.f5287a.f5270a = ((bh) iBinder).f5201a;
        if (this.f5287a.f5270a.a(new dw(this))) {
            dn.d(this.f5287a);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        this.f5287a.f5270a = null;
    }
}
