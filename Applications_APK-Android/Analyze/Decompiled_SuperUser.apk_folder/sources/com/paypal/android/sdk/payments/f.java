package com.paypal.android.sdk.payments;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.lody.virtual.helper.utils.FileUtils;

final class f {

    /* renamed from: a  reason: collision with root package name */
    final IntentFilter f5304a;

    /* renamed from: b  reason: collision with root package name */
    final BroadcastReceiver f5305b;

    /* renamed from: c  reason: collision with root package name */
    boolean f5306c;

    f(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {
        this.f5304a = intentFilter;
        this.f5305b = broadcastReceiver;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((int) FileUtils.FileMode.MODE_IWUSR);
        sb.append("Receiver{");
        sb.append(this.f5305b);
        sb.append(" filter=");
        sb.append(this.f5304a);
        sb.append("}");
        return sb.toString();
    }
}
