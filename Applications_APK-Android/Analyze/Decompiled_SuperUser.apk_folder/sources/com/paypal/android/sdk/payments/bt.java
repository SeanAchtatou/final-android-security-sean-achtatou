package com.paypal.android.sdk.payments;

import android.content.Intent;

final class bt {

    /* renamed from: a  reason: collision with root package name */
    private Intent f5212a;

    bt(Intent intent) {
        this.f5212a = intent;
    }

    /* access modifiers changed from: package-private */
    public final PayPalPayment a() {
        return (PayPalPayment) this.f5212a.getParcelableExtra("com.paypal.android.sdk.payment");
    }

    public final dx b() {
        return (dx) this.f5212a.getParcelableExtra("com.paypal.android.sdk.payments.PaymentConfirmActivity.EXTRA_PAYMENT_INFO");
    }
}
