package com.paypal.android.sdk.payments;

import com.paypal.android.sdk.as;
import com.paypal.android.sdk.cd;
import com.paypal.android.sdk.dq;
import com.paypal.android.sdk.eu;
import com.paypal.android.sdk.ey;
import java.util.Map;

final class di extends dj {
    public di(PayPalService payPalService) {
        super(new bm(payPalService));
    }

    /* access modifiers changed from: protected */
    public final String a() {
        return "msdk";
    }

    /* access modifiers changed from: protected */
    public final void a(String str, Map map) {
        if (!b().a().f4664a.b()) {
            b().a().f4664a = new eu();
        }
        map.put("v49", str);
        map.put("v51", b().c().d().d());
        map.put("v52", dq.f4746a + " " + dq.f4748c);
        map.put("v53", dq.f4749d);
        map.put("clid", b().b());
        map.put("apid", b().c().d().c() + "|" + str + "|" + b().f());
        b().a(new as(b().a().f4664a.c(), map));
    }

    /* access modifiers changed from: protected */
    public final void a(Map map, ey eyVar, String str, String str2) {
        map.put("g", b().e());
        map.put("vers", dq.f4746a + ":" + b().d() + ":");
        map.put("srce", "msdk");
        map.put("sv", "mobile");
        map.put("bchn", "msdk");
        map.put("adte", "FALSE");
        map.put("bzsr", "mobile");
        if (cd.b((CharSequence) str)) {
            map.put("calc", str);
        }
        if (cd.b((CharSequence) str2)) {
            map.put("prid", str2);
        }
        map.put("e", eyVar.b() ? "cl" : "im");
    }
}
