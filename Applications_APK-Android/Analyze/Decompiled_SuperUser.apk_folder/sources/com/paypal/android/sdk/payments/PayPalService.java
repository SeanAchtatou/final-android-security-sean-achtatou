package com.paypal.android.sdk.payments;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import com.paypal.android.sdk.a;
import com.paypal.android.sdk.an;
import com.paypal.android.sdk.ao;
import com.paypal.android.sdk.ap;
import com.paypal.android.sdk.as;
import com.paypal.android.sdk.bi;
import com.paypal.android.sdk.bp;
import com.paypal.android.sdk.br;
import com.paypal.android.sdk.bt;
import com.paypal.android.sdk.ca;
import com.paypal.android.sdk.cb;
import com.paypal.android.sdk.cd;
import com.paypal.android.sdk.ce;
import com.paypal.android.sdk.cf;
import com.paypal.android.sdk.cg;
import com.paypal.android.sdk.ch;
import com.paypal.android.sdk.ci;
import com.paypal.android.sdk.cs;
import com.paypal.android.sdk.cu;
import com.paypal.android.sdk.cw;
import com.paypal.android.sdk.da;
import com.paypal.android.sdk.di;
import com.paypal.android.sdk.dk;
import com.paypal.android.sdk.dm;
import com.paypal.android.sdk.dp;
import com.paypal.android.sdk.ds;
import com.paypal.android.sdk.dt;
import com.paypal.android.sdk.dy;
import com.paypal.android.sdk.dz;
import com.paypal.android.sdk.ea;
import com.paypal.android.sdk.eb;
import com.paypal.android.sdk.ec;
import com.paypal.android.sdk.ed;
import com.paypal.android.sdk.ef;
import com.paypal.android.sdk.eg;
import com.paypal.android.sdk.ei;
import com.paypal.android.sdk.el;
import com.paypal.android.sdk.en;
import com.paypal.android.sdk.ep;
import com.paypal.android.sdk.es;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.ey;
import com.paypal.android.sdk.go;
import com.paypal.android.sdk.x;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.json.JSONObject;

public final class PayPalService extends Service {

    /* renamed from: a  reason: collision with root package name */
    static final ExecutorService f5108a = ca.a();
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static final String f5109c = PayPalService.class.getSimpleName();
    private static Intent t;

    /* renamed from: b  reason: collision with root package name */
    dp f5110b;

    /* renamed from: d  reason: collision with root package name */
    private x f5111d;

    /* renamed from: e  reason: collision with root package name */
    private cf f5112e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public PayPalConfiguration f5113f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public boolean f5114g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public a f5115h = new a();
    /* access modifiers changed from: private */
    public a i = new a();
    private dj j = new di(this);
    private String k;
    private ap l;
    /* access modifiers changed from: private */
    public bi m;
    private String n;
    /* access modifiers changed from: private */
    public a o;
    /* access modifiers changed from: private */
    public ci p;
    private List q = new ArrayList();
    private boolean r = true;
    private boolean s = true;
    private final BroadcastReceiver u = new bd(this);
    private final IBinder v = new bh(this);

    private boolean A() {
        return (this.f5113f == null || this.f5112e == null) ? false : true;
    }

    private static cf B() {
        return new cf();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void
     arg types: [com.paypal.android.sdk.payments.be, int]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.payments.bi):com.paypal.android.sdk.payments.bi
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.bt):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, boolean):boolean
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.Boolean):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(java.lang.String, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void */
    private void C() {
        a((bi) new be(this), false);
    }

    public static void a(Context context) {
        Log.w("paypal.sdk", "clearing user data");
        f5108a.submit(new bb(context));
    }

    private void a(Intent intent) {
        String stringExtra;
        t = intent;
        new StringBuilder("init:").append(b(intent));
        if (this.f5113f == null) {
            this.f5113f = (PayPalConfiguration) intent.getParcelableExtra("com.paypal.android.sdk.paypalConfiguration");
            if (this.f5113f == null) {
                throw new RuntimeException("Missing EXTRA_PAYPAL_CONFIGURATION. To avoid this error, set EXTRA_PAYPAL_CONFIGURATION in both PayPalService, and the initializing activity.");
            }
        }
        if (!this.f5113f.o()) {
            throw new RuntimeException("Service extras invalid.  Please check the docs.");
        } else if (!this.f5113f.i() || cf.d()) {
            String b2 = this.f5113f.b();
            if (an.c(b2)) {
                stringExtra = "https://api-m.paypal.com/v1/";
            } else if (an.b(b2)) {
                stringExtra = "https://api-m.sandbox.paypal.com/v1/";
            } else if (an.a(b2)) {
                stringExtra = null;
            } else if (!z() || !intent.hasExtra("com.paypal.android.sdk.baseEnvironmentUrl")) {
                throw new RuntimeException("Invalid environment selected:" + b2);
            } else {
                stringExtra = intent.getStringExtra("com.paypal.android.sdk.baseEnvironmentUrl");
            }
            this.p = new ci(this.o, this.f5113f.b());
            ao b3 = b(b2, stringExtra);
            if (this.l == null) {
                int intExtra = (!z() || !intent.hasExtra("com.paypal.android.sdk.mockNetworkDelay")) ? 500 : intent.getIntExtra("com.paypal.android.sdk.mockNetworkDelay", 500);
                boolean booleanExtra = (!z() || !intent.hasExtra("com.paypal.android.sdk.mockEnable2fa")) ? false : intent.getBooleanExtra("com.paypal.android.sdk.mockEnable2fa", false);
                int intExtra2 = (!z() || !intent.hasExtra("com.paypal.android.sdk.mock2faPhoneNumberCount")) ? 1 : intent.getIntExtra("com.paypal.android.sdk.mock2faPhoneNumberCount", 1);
                this.r = true;
                if (z() && intent.hasExtra("com.paypal.android.sdk.enableAuthenticator")) {
                    this.r = intent.getBooleanExtra("com.paypal.android.sdk.enableAuthenticator", true);
                }
                if (z() && intent.hasExtra("com.paypal.android.sdk.enableAuthenticatorSecurity")) {
                    this.s = intent.getBooleanExtra("com.paypal.android.sdk.enableAuthenticatorSecurity", true);
                }
                boolean booleanExtra2 = (!z() || !intent.hasExtra("com.paypal.android.sdk.enableStageSsl")) ? true : intent.getBooleanExtra("com.paypal.android.sdk.enableStageSsl", true);
                this.l = new ap(this.o, b3, a());
                this.l.a(new cu(new bk(this, (byte) 0)));
                this.l.a(new bp(this.l, an.a(this.f5113f.b()) ? new es(this.l, intExtra, booleanExtra, intExtra2) : new bi(this.o, this.f5113f.b(), a(), this.l, 90, booleanExtra2, Collections.singletonList(new cg(a().c())))));
            }
            ev.b(this.f5113f.a());
            if (this.f5112e == null) {
                this.f5112e = B();
            }
            if (!this.f5113f.j()) {
                a(this.o.f());
            }
            this.k = intent.getComponent().getPackageName();
            a(ey.PreConnect);
            C();
        } else {
            throw new RuntimeException("Credit Cards cannot be accepted without card.io dependency. Please check the docs.");
        }
    }

    private void a(bt btVar) {
        this.l.b(btVar);
    }

    /* access modifiers changed from: private */
    public void a(ey eyVar, boolean z, String str, String str2, String str3) {
        this.j.a(eyVar, z, str, str2, str3);
    }

    static /* synthetic */ void a(PayPalService payPalService, bt btVar) {
        payPalService.f5112e.f4665b = null;
        new StringBuilder().append(btVar.n()).append(" request error");
        String b2 = btVar.p().b();
        Log.e("paypal.sdk", b2);
        payPalService.b(ey.DeviceCheck, b2, btVar.j());
        if (payPalService.m != null) {
            payPalService.m.a(payPalService.b(btVar));
            payPalService.m = null;
        }
        payPalService.f5114g = false;
    }

    private static boolean a(ds dsVar) {
        return dsVar != null && dsVar.b();
    }

    private ei[] a(PayPalItem[] payPalItemArr) {
        if (payPalItemArr == null) {
            return null;
        }
        ei[] eiVarArr = new ei[payPalItemArr.length];
        int length = payPalItemArr.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            PayPalItem payPalItem = payPalItemArr[i2];
            eiVarArr[i3] = new ei(payPalItem.b(), payPalItem.c(), payPalItem.d(), payPalItem.e(), payPalItem.f());
            i2++;
            i3++;
        }
        return eiVarArr;
    }

    private static ao b(String str, String str2) {
        ao aoVar = new ao(str, str2);
        if (str2 != null) {
            if (!str2.startsWith("https://")) {
                throw new RuntimeException(str2 + " does not start with 'https://', ignoring " + str);
            }
            if (!str2.endsWith("/")) {
                new StringBuilder().append(str2).append(" does not end with a slash, adding one.");
                str2 = str2 + "/";
            }
            for (br brVar : cb.d()) {
                aoVar.c().put(brVar.a(), str2 + brVar.c());
            }
        }
        return aoVar;
    }

    /* access modifiers changed from: private */
    public bj b(bt btVar) {
        return new bj(this, btVar.p().b(), btVar.r(), btVar.p().a());
    }

    private static String b(Intent intent) {
        if (intent == null) {
            return "Intent = null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Intent{");
        sb.append("action:" + intent.getAction());
        sb.append(", cmp:" + intent.getComponent() + ", ");
        if (intent.getExtras() == null) {
            sb.append("null extras");
        } else {
            sb.append("extras:");
            for (String next : intent.getExtras().keySet()) {
                sb.append("(" + next + ":" + intent.getExtras().get(next) + ")");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, boolean, java.lang.String, java.lang.String, java.lang.String):void
     arg types: [com.paypal.android.sdk.ey, int, java.lang.String, java.lang.String, ?[OBJECT, ARRAY]]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.el, boolean, java.lang.String, boolean, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, boolean, java.lang.String, java.lang.String, java.lang.String):void */
    private void b(ey eyVar, String str, String str2) {
        a(eyVar, false, str, str2, (String) null);
    }

    static /* synthetic */ void c(PayPalService payPalService, bt btVar) {
        String b2 = btVar.p().b();
        Log.e("paypal.sdk", b2);
        payPalService.b(ey.ConfirmPayment, b2, btVar.j());
        payPalService.i.a(payPalService.b(btVar));
    }

    private static boolean z() {
        return "partner".equals("general");
    }

    /* access modifiers changed from: package-private */
    public final x a() {
        if (this.f5111d == null) {
            this.f5111d = new cf();
        }
        return this.f5111d;
    }

    /* access modifiers changed from: package-private */
    public final String a(String str) {
        return this.o.c(str);
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        this.l.b(new ef(this.l, a(), this.l.c(), this.f5113f.k(), this.f5112e.i, (String) new ArrayList(this.f5112e.f4669f.f4683a.keySet()).get(i2)));
    }

    public final void a(as asVar) {
        a(new ec(this.l, a(), dm.a(asVar)));
    }

    /* access modifiers changed from: package-private */
    public final void a(el elVar, String str, boolean z, String str2, boolean z2, String str3) {
        this.l.b(new eg(this.l, a(), this.l.c(), this.f5113f.k(), elVar, str, this.f5112e.i, z, str2, z2, str3));
    }

    /* access modifiers changed from: package-private */
    public final void a(el elVar, boolean z, String str, boolean z2, String str2) {
        this.l.b(new eg(this.l, a(), this.l.c(), this.f5113f.k(), elVar, z, str, z2, str2));
    }

    /* access modifiers changed from: package-private */
    public final void a(en enVar, Map map, PayPalItem[] payPalItemArr, String str, boolean z, String str2, String str3, boolean z2, String str4, String str5, String str6, boolean z3, String str7) {
        this.l.b(new dz(this.l, a(), this.f5112e.f4670g.c(), this.f5112e.b(), null, enVar, map, a(payPalItemArr), str, z, str2, this.n, str3, z2).d(str4).e(str5).f(str6).a(z3).g(str7));
    }

    /* access modifiers changed from: package-private */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, boolean, java.lang.String, java.lang.String, java.lang.String):void
     arg types: [com.paypal.android.sdk.ey, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.el, boolean, java.lang.String, boolean, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, boolean, java.lang.String, java.lang.String, java.lang.String):void */
    public final void a(ey eyVar) {
        a(eyVar, false, (String) null, (String) null, (String) null);
    }

    /* access modifiers changed from: package-private */
    public final void a(ey eyVar, Boolean bool) {
        a(eyVar, bool.booleanValue(), (String) null, (String) null, (String) null);
    }

    /* access modifiers changed from: package-private */
    public final void a(ey eyVar, Boolean bool, String str) {
        a(eyVar, bool.booleanValue(), (String) null, str, (String) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, boolean, java.lang.String, java.lang.String, java.lang.String):void
     arg types: [com.paypal.android.sdk.ey, int, ?[OBJECT, ARRAY], java.lang.String, ?[OBJECT, ARRAY]]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.el, boolean, java.lang.String, boolean, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, boolean, java.lang.String, java.lang.String, java.lang.String):void */
    /* access modifiers changed from: package-private */
    public final void a(ey eyVar, String str) {
        a(eyVar, false, (String) null, str, (String) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, boolean, java.lang.String, java.lang.String, java.lang.String):void
     arg types: [com.paypal.android.sdk.ey, int, ?[OBJECT, ARRAY], java.lang.String, java.lang.String]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.el, boolean, java.lang.String, boolean, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, boolean, java.lang.String, java.lang.String, java.lang.String):void */
    /* access modifiers changed from: package-private */
    public final void a(ey eyVar, String str, String str2) {
        a(eyVar, false, (String) null, str, str2);
    }

    /* access modifiers changed from: package-private */
    public final void a(bf bfVar) {
        this.f5115h.a(bfVar);
    }

    /* access modifiers changed from: package-private */
    public final void a(bi biVar, boolean z) {
        if (z) {
            this.f5112e.f4665b = null;
        }
        this.m = biVar;
        if (!this.f5114g && !this.f5112e.c()) {
            this.f5114g = true;
            a(ey.DeviceCheck);
            this.l.b(new ep(this.f5113f.b(), this.l, a(), this.f5113f.k()));
        }
    }

    public final void a(String str, String str2) {
        a(new eb(this.l, a(), str, str2));
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2, en enVar, Map map, PayPalItem[] payPalItemArr, String str3, boolean z, String str4, String str5, String str6, String str7, String str8, String str9) {
        this.l.b(new ea(this.l, a(), this.f5112e.f4665b.c(), str, str2, str4, enVar, map, a(payPalItemArr), str3, z, str5, this.n, str6).d(str7).e(str8).f(str9));
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2, String str3, String str4, int i2, int i3, en enVar, Map map, PayPalItem[] payPalItemArr, String str5, boolean z, String str6, String str7, String str8, String str9, String str10) {
        this.l.b(new ea(this.l, a(), this.f5112e.f4665b.c(), str, str2, (!str3.equalsIgnoreCase("4111111111111111") || !an.b(this.f5113f.b())) ? str3 : "4444333322221111", str4, i2, i3, null, enVar, map, a(payPalItemArr), str5, z, str6, this.n, str7).d(str8).e(str9).f(str10));
    }

    /* access modifiers changed from: package-private */
    public final void a(List list) {
        this.l.b(new dy(this.l, a(), this.l.c(), this.f5113f.k(), this.f5112e.f4668e.a(), this.f5112e.i, list));
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z, String str, String str2, JSONObject jSONObject, JSONObject jSONObject2, String str3) {
        this.l.b(new dt(this.l, a(), this.f5112e.f4670g.c(), this.f5112e.b(), z, str3, this.n, str, str2, jSONObject, jSONObject2));
    }

    /* access modifiers changed from: protected */
    public final boolean a(bl blVar) {
        if (A()) {
            return true;
        }
        this.q.add(blVar);
        return false;
    }

    /* access modifiers changed from: protected */
    public final ap b() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public final void b(bf bfVar) {
        this.i.a(bfVar);
    }

    /* access modifiers changed from: protected */
    public final cf c() {
        return this.f5112e;
    }

    /* access modifiers changed from: package-private */
    public final PayPalConfiguration d() {
        return this.f5113f;
    }

    /* access modifiers changed from: protected */
    public final String e() {
        return this.f5113f.b();
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return this.f5113f.k();
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        t();
        h();
        this.p.b();
        u();
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        this.f5112e.f4670g = null;
        ch.b(this.f5113f.b());
        this.f5112e.f4667d = null;
        this.f5112e.f4666c = null;
    }

    /* access modifiers changed from: package-private */
    public final boolean i() {
        return this.f5112e.c();
    }

    /* access modifiers changed from: package-private */
    public final boolean j() {
        cf cfVar = this.f5112e;
        return cfVar.f4670g != null && cfVar.f4670g.b();
    }

    /* access modifiers changed from: package-private */
    public final boolean k() {
        return (this.f5112e.f4668e == null || this.f5112e.i == null) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        di a2 = this.p.a();
        if (a2 == null) {
            h();
            return;
        }
        ds dsVar = this.f5112e.f4670g;
        ds a3 = ch.a(this.f5113f.b());
        if (!a(dsVar) && a(a3)) {
            this.f5112e.f4670g = a3;
        }
        this.f5112e.f4666c = a2.d() ? a2.c().equals(dk.EMAIL) ? a2.b() : a2.a().a(ce.a()) : null;
    }

    /* access modifiers changed from: package-private */
    public final void m() {
        this.i.b();
    }

    /* access modifiers changed from: package-private */
    public final void n() {
        this.f5115h.b();
    }

    /* access modifiers changed from: package-private */
    public final void o() {
        this.m = null;
    }

    public final IBinder onBind(Intent intent) {
        new StringBuilder("onBind(").append(b(intent)).append(")");
        if (!A()) {
            if (t == null) {
                a(intent);
            } else {
                a(t);
            }
        }
        return this.v;
    }

    public final void onCreate() {
        Log.w("paypal.sdk", PayPalService.class.getSimpleName() + " created. API:" + Build.VERSION.SDK_INT + " " + a().b());
        new cf();
        this.o = new a(this, "AndroidBasePrefs", new cd());
        cw.a(this.o);
        da.a(this.o);
        this.n = cs.a(f5108a, this, this.o.e(), "2.15.3", null);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.paypal.android.sdk.clearAllUserData");
        c.a(this).a(this.u, intentFilter);
    }

    public final void onDestroy() {
        if (this.l != null) {
            this.l.a();
            this.l.b();
            this.l = null;
        }
        try {
            c.a(this).a(this.u);
        } catch (Throwable th) {
            new StringBuilder("ignoring:").append(th.getMessage());
        }
        new StringBuilder("service destroyed: ").append(this);
    }

    public final void onRebind(Intent intent) {
        super.onRebind(intent);
        new StringBuilder("onRebind(").append(b(intent)).append(")");
    }

    public final int onStartCommand(Intent intent, int i2, int i3) {
        new StringBuilder("onStartCommand(").append(b(intent)).append(", ").append(i2).append(", ").append(i3).append(")");
        if (!A()) {
            new go(this).a();
            if (intent == null || intent.getExtras() == null) {
                throw new RuntimeException("Service extras required. Please see the docs.");
            }
            a(intent);
        }
        if (this.q.size() <= 0) {
            return 3;
        }
        for (bl a2 : this.q) {
            a2.a();
        }
        this.q.clear();
        return 3;
    }

    public final boolean onUnbind(Intent intent) {
        new StringBuilder("onUnbind(").append(b(intent)).append(")");
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void p() {
        this.l.b(new ed(this.l, a(), this.l.c(), this.f5112e.f4665b.c(), this.f5113f.k()));
    }

    /* access modifiers changed from: package-private */
    public final di q() {
        return this.p.a();
    }

    /* access modifiers changed from: package-private */
    public final String r() {
        return this.f5112e.f4666c;
    }

    /* access modifiers changed from: package-private */
    public final dp s() {
        return this.p.a(this.f5113f.k());
    }

    /* access modifiers changed from: package-private */
    public final void t() {
        this.f5110b = s();
        this.p.c();
        if (this.f5110b != null && this.f5112e.f4665b != null) {
            a(this.f5112e.f4665b.c(), this.f5110b.e());
            this.f5110b = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void u() {
        if (this.f5113f != null && this.f5113f.o()) {
            this.f5112e = B();
            C();
        }
    }

    /* access modifiers changed from: protected */
    public final String v() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public final boolean w() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public final boolean x() {
        return this.s;
    }
}
