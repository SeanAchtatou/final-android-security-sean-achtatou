package com.paypal.android.sdk.payments;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.widget.EditText;

final class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EditText f5319a;

    n(LoginActivity loginActivity, EditText editText) {
        this.f5319a = editText;
    }

    public final void run() {
        this.f5319a.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), 0, 9999.0f, 0.0f, 0));
        this.f5319a.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), 1, 9999.0f, 0.0f, 0));
    }
}
