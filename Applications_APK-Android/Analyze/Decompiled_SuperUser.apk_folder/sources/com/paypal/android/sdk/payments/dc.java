package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;

final class dc implements Parcelable.Creator {
    dc() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new ShippingAddress(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new ShippingAddress[i];
    }
}
