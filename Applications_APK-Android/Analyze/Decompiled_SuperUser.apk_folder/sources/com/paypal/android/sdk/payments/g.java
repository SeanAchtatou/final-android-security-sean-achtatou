package com.paypal.android.sdk.payments;

import com.paypal.android.sdk.cd;
import com.paypal.android.sdk.ce;
import com.paypal.android.sdk.ev;

class g {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5307a = g.class.getSimpleName();

    g() {
    }

    static String a() {
        String a2 = ce.a().c().a();
        if (cd.a((CharSequence) a2) || a2.equalsIgnoreCase("US")) {
            return "https://www.paypal.com/webapps/accountrecovery/passwordrecovery";
        }
        return String.format("https://www.paypal.com/%s/cgi-bin/webscr?cmd=_account-recovery&from=%s&locale.x=%s", a2.toLowerCase(), "PayPalMPL", ev.c(a2));
    }
}
