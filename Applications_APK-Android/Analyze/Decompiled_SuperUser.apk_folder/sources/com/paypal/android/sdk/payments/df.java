package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.content.DialogInterface;

final class df implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Activity f5256a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ int f5257b;

    df(Activity activity, int i) {
        this.f5256a = activity;
        this.f5257b = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f5256a.removeDialog(this.f5257b);
        this.f5256a.finish();
    }
}
