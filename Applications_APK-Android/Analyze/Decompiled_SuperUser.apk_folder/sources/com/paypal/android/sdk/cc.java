package com.paypal.android.sdk;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

final class cc implements Executor {
    private cc() {
    }

    /* synthetic */ cc(byte b2) {
        this();
    }

    public final void execute(Runnable runnable) {
        new Handler(Looper.getMainLooper()).post(runnable);
    }
}
