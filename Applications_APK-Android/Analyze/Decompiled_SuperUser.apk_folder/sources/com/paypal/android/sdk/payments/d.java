package com.paypal.android.sdk.payments;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class d extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ c f5254a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(c cVar, Looper looper) {
        super(looper);
        this.f5254a = cVar;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                c.a(this.f5254a);
                return;
            default:
                super.handleMessage(message);
                return;
        }
    }
}
