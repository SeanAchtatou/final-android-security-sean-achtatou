package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.fs;
import com.paypal.android.sdk.gj;
import com.paypal.android.sdk.gm;
import com.paypal.android.sdk.gn;
import com.paypal.android.sdk.go;
import java.util.Arrays;
import java.util.Date;
import java.util.Timer;

public final class PayPalProfileSharingActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f5102a = PayPalProfileSharingActivity.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Date f5103b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Timer f5104c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public PayPalService f5105d;

    /* renamed from: e  reason: collision with root package name */
    private final ServiceConnection f5106e = new ax(this);

    /* renamed from: f  reason: collision with root package name */
    private boolean f5107f;

    static /* synthetic */ bi a(PayPalProfileSharingActivity payPalProfileSharingActivity) {
        return new ay(payPalProfileSharingActivity);
    }

    public final void finish() {
        super.finish();
        new StringBuilder().append(getClass().getSimpleName()).append(".finish");
    }

    /* access modifiers changed from: protected */
    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        new StringBuilder().append(getClass().getSimpleName()).append(".onActivityResult");
        if (i == 1) {
            switch (i2) {
                case -1:
                    if (intent == null) {
                        Log.e(f5102a, "result was OK, no intent data, oops");
                        break;
                    } else {
                        PayPalAuthorization payPalAuthorization = (PayPalAuthorization) intent.getParcelableExtra("com.paypal.android.sdk.authorization");
                        if (payPalAuthorization == null) {
                            Log.e(f5102a, "result was OK, have data, but no authorization state in bundle, oops");
                            break;
                        } else {
                            Intent intent2 = new Intent();
                            intent2.putExtra("com.paypal.android.sdk.authorization", payPalAuthorization);
                            setResult(-1, intent2);
                            break;
                        }
                    }
                case 0:
                    break;
                default:
                    Log.wtf(f5102a, "unexpected request code " + i + " call it a cancel");
                    break;
            }
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new StringBuilder().append(getClass().getSimpleName()).append(".onCreate");
        new go(this).a();
        new gn(this).a();
        new gm(this).a(Arrays.asList(PayPalProfileSharingActivity.class.getName(), LoginActivity.class.getName(), FuturePaymentInfoActivity.class.getName(), ProfileSharingConsentActivity.class.getName()));
        this.f5107f = bindService(cf.b(this), this.f5106e, 1);
        setTheme(16973934);
        requestWindowFeature(8);
        gj gjVar = new gj(this);
        setContentView(gjVar.f5005a);
        gjVar.f5006b.setText(ev.a(fs.CHECKING_DEVICE));
        cf.a(this, (TextView) null, fs.CHECKING_DEVICE);
    }

    /* access modifiers changed from: protected */
    public final Dialog onCreateDialog(int i, Bundle bundle) {
        switch (i) {
            case 2:
                return cf.a(this, new aw(this));
            case 3:
                return cf.a(this, fs.UNAUTHORIZED_MERCHANT_TITLE, bundle, i);
            default:
                return cf.a(this, fs.UNAUTHORIZED_DEVICE_TITLE, bundle, i);
        }
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        new StringBuilder().append(getClass().getSimpleName()).append(".onDestroy");
        if (this.f5105d != null) {
            this.f5105d.o();
            this.f5105d.u();
        }
        if (this.f5107f) {
            unbindService(this.f5106e);
            this.f5107f = false;
        }
        super.onDestroy();
    }
}
