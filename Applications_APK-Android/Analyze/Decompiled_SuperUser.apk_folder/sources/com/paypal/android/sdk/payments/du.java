package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;
import com.paypal.android.sdk.ed;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

final class du implements Parcelable {
    public static final Parcelable.Creator CREATOR = new cz();

    /* renamed from: a  reason: collision with root package name */
    private List f5283a;

    /* renamed from: b  reason: collision with root package name */
    private String f5284b;

    /* renamed from: c  reason: collision with root package name */
    private String f5285c;

    /* renamed from: d  reason: collision with root package name */
    private String f5286d;

    du() {
        this.f5283a = new ArrayList();
    }

    public du(Parcel parcel) {
        this();
        if (parcel != null) {
            this.f5284b = parcel.readString();
            this.f5285c = parcel.readString();
            this.f5286d = parcel.readString();
            this.f5283a = new ArrayList();
            parcel.readList(this.f5283a, String.class.getClassLoader());
        }
    }

    du(ed edVar) {
        this.f5283a = a(edVar.t());
        this.f5284b = edVar.u();
        this.f5285c = edVar.v();
        this.f5286d = edVar.w();
    }

    private static List a(Map map) {
        ArrayList arrayList = new ArrayList();
        for (String str : map.keySet()) {
            if (((String) map.get(str)).toUpperCase().equals("Y")) {
                arrayList.add(str.toLowerCase(Locale.US));
            }
        }
        return arrayList;
    }

    public final List a() {
        return this.f5283a;
    }

    public final String b() {
        return this.f5284b;
    }

    public final String c() {
        return this.f5285c;
    }

    public final String d() {
        return this.f5286d;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f5284b);
        parcel.writeString(this.f5285c);
        parcel.writeString(this.f5286d);
        parcel.writeList(this.f5283a);
    }
}
