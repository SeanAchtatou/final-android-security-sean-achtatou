package com.paypal.android.sdk.payments;

import android.content.Intent;
import android.os.Bundle;
import com.paypal.android.sdk.dz;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.fs;

final class ci implements bf {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PaymentConfirmActivity f5237a;

    ci(PaymentConfirmActivity paymentConfirmActivity) {
        this.f5237a = paymentConfirmActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void
     arg types: [com.paypal.android.sdk.payments.bi, int]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.payments.bi):com.paypal.android.sdk.payments.bi
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.bt):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, boolean):boolean
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.Boolean):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(java.lang.String, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PaymentConfirmActivity.a(com.paypal.android.sdk.payments.PaymentConfirmActivity, boolean):boolean
     arg types: [com.paypal.android.sdk.payments.PaymentConfirmActivity, int]
     candidates:
      com.paypal.android.sdk.payments.PaymentConfirmActivity.a(com.paypal.android.sdk.payments.PaymentConfirmActivity, com.paypal.android.sdk.payments.PayPalService):com.paypal.android.sdk.payments.PayPalService
      com.paypal.android.sdk.payments.PaymentConfirmActivity.a(com.paypal.android.sdk.payments.PaymentConfirmActivity, com.paypal.android.sdk.dz):void
      com.paypal.android.sdk.payments.PaymentConfirmActivity.a(java.lang.String, com.paypal.android.sdk.ds):void
      com.paypal.android.sdk.payments.PaymentConfirmActivity.a(com.paypal.android.sdk.payments.PaymentConfirmActivity, boolean):boolean */
    public final void a(bj bjVar) {
        this.f5237a.k.c().a();
        this.f5237a.j();
        if (!bjVar.a() || "UNAUTHORIZED_PAYMENT".equals(bjVar.f5203b)) {
            switch (this.f5237a.i) {
                case PayPal:
                    boolean unused = this.f5237a.f5127f = false;
                    Bundle bundle = new Bundle();
                    bundle.putString("BUNDLE_ERROR_CODE", bjVar.f5203b);
                    this.f5237a.showDialog(5, bundle);
                    return;
                case CreditCardToken:
                case CreditCard:
                    this.f5237a.f5128g.b(true);
                    cf.a(this.f5237a, ev.a(bjVar.f5203b), 1);
                    return;
                default:
                    return;
            }
        } else {
            switch (this.f5237a.i) {
                case PayPal:
                    cf.a(this.f5237a, ev.a(fs.SESSION_EXPIRED_MESSAGE), 4);
                    return;
                case CreditCardToken:
                case CreditCard:
                    this.f5237a.showDialog(2);
                    String unused2 = PaymentConfirmActivity.f5122a;
                    new StringBuilder("server thinks token is expired, get new one. AccessToken: ").append(this.f5237a.k.c().f4665b);
                    this.f5237a.k.a(this.f5237a.h(), true);
                    return;
                default:
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PaymentConfirmActivity.a(com.paypal.android.sdk.payments.PaymentConfirmActivity, boolean):boolean
     arg types: [com.paypal.android.sdk.payments.PaymentConfirmActivity, int]
     candidates:
      com.paypal.android.sdk.payments.PaymentConfirmActivity.a(com.paypal.android.sdk.payments.PaymentConfirmActivity, com.paypal.android.sdk.payments.PayPalService):com.paypal.android.sdk.payments.PayPalService
      com.paypal.android.sdk.payments.PaymentConfirmActivity.a(com.paypal.android.sdk.payments.PaymentConfirmActivity, com.paypal.android.sdk.dz):void
      com.paypal.android.sdk.payments.PaymentConfirmActivity.a(java.lang.String, com.paypal.android.sdk.ds):void
      com.paypal.android.sdk.payments.PaymentConfirmActivity.a(com.paypal.android.sdk.payments.PaymentConfirmActivity, boolean):boolean */
    public final void a(Object obj) {
        if (obj instanceof ProofOfPayment) {
            PaymentConfirmation paymentConfirmation = new PaymentConfirmation(this.f5237a.k.e(), this.f5237a.f5129h.a(), (ProofOfPayment) obj);
            Intent intent = this.f5237a.getIntent();
            intent.putExtra("com.paypal.android.sdk.paymentConfirmation", paymentConfirmation);
            this.f5237a.c();
            this.f5237a.setResult(-1, intent);
            this.f5237a.finish();
        } else if (obj instanceof dz) {
            boolean unused = this.f5237a.f5127f = false;
            PaymentConfirmActivity.a(this.f5237a, (dz) obj);
        }
    }
}
