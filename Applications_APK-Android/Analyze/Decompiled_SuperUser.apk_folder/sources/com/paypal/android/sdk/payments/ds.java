package com.paypal.android.sdk.payments;

import com.paypal.android.sdk.ed;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.fs;

final class ds implements bf {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ dn f5281a;

    ds(dn dnVar) {
        this.f5281a = dnVar;
    }

    public final void a(bj bjVar) {
        this.f5281a.dismissDialog(2);
        if (bjVar.f5203b.equals("invalid_nonce")) {
            this.f5281a.f5272c.f4942h.setEnabled(false);
            cf.a(this.f5281a, ev.a(fs.SESSION_EXPIRED_MESSAGE), 4);
            return;
        }
        this.f5281a.f5272c.f4942h.setEnabled(true);
        cf.a(this.f5281a, ev.a(bjVar.f5203b), 1);
    }

    public final void a(Object obj) {
        if (obj instanceof ed) {
            dn.a(this.f5281a, (ed) obj);
        } else {
            this.f5281a.i();
        }
    }
}
