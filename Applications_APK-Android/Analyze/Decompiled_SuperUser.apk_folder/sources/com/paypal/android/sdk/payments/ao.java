package com.paypal.android.sdk.payments;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

final class ao implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PayPalFuturePaymentActivity f5177a;

    ao(PayPalFuturePaymentActivity payPalFuturePaymentActivity) {
        this.f5177a = payPalFuturePaymentActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        new StringBuilder().append(PayPalFuturePaymentActivity.f5070a).append(".onServiceConnected");
        if (this.f5177a.isFinishing()) {
            new StringBuilder().append(PayPalFuturePaymentActivity.f5070a).append(".onServiceConnected exit - isFinishing");
            return;
        }
        PayPalService unused = this.f5177a.f5073d = ((bh) iBinder).f5201a;
        if (this.f5177a.f5073d.a(new ap(this))) {
            PayPalFuturePaymentActivity.c(this.f5177a);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        PayPalService unused = this.f5177a.f5073d = (PayPalService) null;
        String unused2 = PayPalFuturePaymentActivity.f5070a;
    }
}
