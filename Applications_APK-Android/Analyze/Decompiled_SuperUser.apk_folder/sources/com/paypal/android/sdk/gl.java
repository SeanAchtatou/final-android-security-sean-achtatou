package com.paypal.android.sdk;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public final class gl {

    /* renamed from: a  reason: collision with root package name */
    private LinearLayout f5015a;

    /* renamed from: b  reason: collision with root package name */
    private RelativeLayout f5016b;

    /* renamed from: c  reason: collision with root package name */
    private ImageView f5017c;

    /* renamed from: d  reason: collision with root package name */
    private ImageView f5018d;

    /* renamed from: e  reason: collision with root package name */
    private TextView f5019e;

    /* renamed from: f  reason: collision with root package name */
    private TextView f5020f;

    /* renamed from: g  reason: collision with root package name */
    private TextView f5021g;

    /* renamed from: h  reason: collision with root package name */
    private TextView f5022h;

    public gl(Context context) {
        this.f5015a = new LinearLayout(context);
        this.f5015a.setOrientation(1);
        this.f5016b = new RelativeLayout(context);
        this.f5015a.addView(this.f5016b);
        this.f5017c = new ImageView(context);
        this.f5017c.setId(2301);
        this.f5016b.addView(this.f5017c);
        bw.b(this.f5017c, "35dip", "35dip");
        bw.b(this.f5017c, null, "4dip", null, null);
        this.f5018d = bw.a(context, "iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAATZJREFUeNrsmMENgkAQRVnPUoAWQB0WoAVoAViA3tW7FEADUIDebUAaoAELwDv+TcaEkFUOsDhj5ieTTdgQ5jHLZ3aDQKVSqVSqAVTX9Q4RSUw8RKSIO+KGmI/5fNMzeZvsGdF88yVia4ypxgCY9Lx/ipi1rkUExb8CVIXFh4SvqMKRPQBBrDAcHFMJIHL2AG8XwrB2TJ0AcWEPQBB2ySwdUxtAlBIAQgxpy5WsKnKmkjVAAyJzuJMXezU+ykp/ZFuJsA0BgA17gAZE5ttevQF02GsOiIQ9AEHEGGJf9uodoMNe7UddsAcgiMyHvU4C4dIl9NcfsWgbFf0jE91KiG7mxLfTojc0oreUojf1oo9VvthlgeS3Y7QpfZu5J+LhsMu9mG7w14e7Q4LIPF5XqVQqlWi9BBgAacm2vqgEoPIAAAAASUVORK5CYII=", "go to selection");
        this.f5018d.setId(2304);
        this.f5018d.setColorFilter(bv.f4638g);
        RelativeLayout.LayoutParams a2 = bw.a(context, "20dip", "20dip", 11);
        a2.addRule(15);
        this.f5016b.addView(this.f5018d, a2);
        this.f5019e = new TextView(context);
        bw.d(this.f5019e, 83);
        this.f5019e.setId(2302);
        RelativeLayout.LayoutParams a3 = bw.a(-2, -2, 1, 2301);
        a3.addRule(0, 2304);
        this.f5016b.addView(this.f5019e, a3);
        bw.b(this.f5019e, "6dip", null, null, null);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        linearLayout.setId(2306);
        RelativeLayout.LayoutParams a4 = bw.a(-2, -2, 1, 2301);
        a4.addRule(3, 2302);
        a4.addRule(0, 2304);
        this.f5016b.addView(linearLayout, a4);
        this.f5020f = new TextView(context);
        bw.b(this.f5020f, 83);
        linearLayout.addView(this.f5020f);
        bw.b(this.f5020f, "6dip", null, null, null);
        this.f5021g = new TextView(context);
        this.f5021g.setId(2305);
        bw.d(this.f5021g, 83);
        linearLayout.addView(this.f5021g);
        bw.b(this.f5021g, "6dip", null, null, null);
        this.f5022h = new TextView(context);
        this.f5022h.setId(2307);
        bw.c(this.f5022h, 83);
        RelativeLayout.LayoutParams a5 = bw.a(-2, -2, 1, 2301);
        a5.addRule(3, 2306);
        a5.addRule(0, 2304);
        this.f5016b.addView(this.f5022h, a5);
        this.f5022h.setText(ev.a(fs.PAY_AFTER_DELIVERY));
        bw.b(this.f5022h, "6dip", null, null, null);
        this.f5022h.setVisibility(8);
        bw.a(this.f5015a);
        this.f5015a.setVisibility(8);
    }

    public final View a() {
        return this.f5015a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, int):void
     arg types: [android.widget.TextView, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void */
    public final void a(Context context, fh fhVar) {
        this.f5017c.setImageBitmap(bw.c(fhVar.a(), context));
        this.f5019e.setText(fhVar.b());
        bw.a((View) this.f5019e, -2, -1);
        this.f5020f.setText(fhVar.c());
        bw.a((View) this.f5020f, -2, -1);
        this.f5020f.setEllipsize(TextUtils.TruncateAt.END);
        this.f5021g.setText(fhVar.d());
        bw.a((View) this.f5021g, -2, -1);
        this.f5021g.setEllipsize(TextUtils.TruncateAt.END);
        if (fhVar.e()) {
            this.f5022h.setVisibility(0);
        } else {
            this.f5022h.setVisibility(8);
        }
    }

    public final void a(View.OnClickListener onClickListener) {
        this.f5015a.setOnClickListener(onClickListener);
    }
}
