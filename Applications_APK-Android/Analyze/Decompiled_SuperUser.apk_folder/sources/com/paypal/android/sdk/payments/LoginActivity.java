package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paypal.android.sdk.an;
import com.paypal.android.sdk.ce;
import com.paypal.android.sdk.de;
import com.paypal.android.sdk.di;
import com.paypal.android.sdk.ee;
import com.paypal.android.sdk.el;
import com.paypal.android.sdk.er;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.ey;
import com.paypal.android.sdk.fd;
import com.paypal.android.sdk.fs;
import com.paypal.android.sdk.fz;
import java.util.ArrayList;
import java.util.Locale;

public final class LoginActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private final String f5051a = LoginActivity.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private bg f5052b;

    /* renamed from: c  reason: collision with root package name */
    private String f5053c;

    /* renamed from: d  reason: collision with root package name */
    private String f5054d;

    /* renamed from: e  reason: collision with root package name */
    private String f5055e;

    /* renamed from: f  reason: collision with root package name */
    private String f5056f;

    /* renamed from: g  reason: collision with root package name */
    private String f5057g;

    /* renamed from: h  reason: collision with root package name */
    private String f5058h;
    private el i;
    private String j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public fz p;
    private boolean q;
    /* access modifiers changed from: private */
    public PayPalService r;
    private final ServiceConnection s = new r(this);

    private el a(bg bgVar) {
        g();
        if (bgVar != bg.PIN) {
            return bgVar == bg.EMAIL ? new el(this.f5053c, this.f5054d) : this.i;
        }
        ce a2 = ce.a();
        return new el(this.f5056f == null ? new er(a2, this.f5055e) : new er(a2, new ee(this.f5056f), this.f5055e), this.f5057g);
    }

    static void a(Activity activity, int i2, di diVar, boolean z, boolean z2, String str, PayPalConfiguration payPalConfiguration) {
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.putExtras(activity.getIntent());
        intent.putExtra("com.paypal.android.sdk.payments.persistedLogin", diVar);
        intent.putExtra("com.paypal.android.sdk.payments.useResponseTypeCode", z);
        intent.putExtra("com.paypal.android.sdk.payments.forceLogin", z2);
        intent.putExtra("com.paypal.android.sdk.payments.requestedScopes", str);
        intent.putExtra("com.paypal.android.sdk.paypalConfiguration", payPalConfiguration);
        activity.startActivityForResult(intent, 1);
    }

    static /* synthetic */ void a(LoginActivity loginActivity, View view) {
        el a2 = loginActivity.a(loginActivity.f5052b);
        if (loginActivity.f5052b == bg.PIN) {
            loginActivity.i = new el(a2.d(), (String) null);
            loginActivity.b(bg.PIN_LOGIN_IN_PROGRESS);
        } else {
            loginActivity.i = new el(a2.b(), (String) null);
            loginActivity.b(bg.EMAIL_LOGIN_IN_PROGRESS);
        }
        loginActivity.r.a(a2, loginActivity.l, loginActivity.b(), loginActivity.c(), loginActivity.j);
    }

    static /* synthetic */ void a(LoginActivity loginActivity, bj bjVar) {
        if (bjVar.b()) {
            loginActivity.d();
            return;
        }
        if (bjVar.a() && bjVar.f5203b.equals("invalid_user")) {
            loginActivity.o();
            cf.a(loginActivity, ev.a(fs.TWO_FACTOR_AUTH_INVALID_ONE_TIME_PASSWORD), 3);
        } else if (bjVar.c()) {
            loginActivity.o();
            cf.a(loginActivity, ev.a(bjVar.f5203b), 3);
        } else if ("invalid_nonce".equals(bjVar.f5203b)) {
            loginActivity.f5058h = null;
            loginActivity.o();
            cf.a(loginActivity, ev.a(fs.SESSION_EXPIRED_MESSAGE), 5);
        } else {
            loginActivity.f5058h = null;
            loginActivity.o();
            cf.a(loginActivity, ev.a(bjVar.f5203b), 4);
        }
    }

    static /* synthetic */ void a(LoginActivity loginActivity, String str) {
        loginActivity.f5054d = null;
        loginActivity.f5057g = null;
        loginActivity.o();
        cf.a(loginActivity, ev.a(str), 1);
    }

    private String b() {
        return c() ? "code" : "token";
    }

    static /* synthetic */ void b(LoginActivity loginActivity, View view) {
        loginActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(g.a())));
        loginActivity.r.a(ey.LoginForgotPassword, Boolean.valueOf(loginActivity.l));
    }

    static /* synthetic */ void b(LoginActivity loginActivity, String str) {
        loginActivity.o();
        if ("invalid_nonce".equals(str)) {
            cf.a(loginActivity, ev.a(fs.SESSION_EXPIRED_MESSAGE), 5);
        } else {
            cf.a(loginActivity, ev.a(str), 2);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public void b(bg bgVar) {
        new StringBuilder("changeLoginState:").append(bgVar);
        if (bgVar != null) {
            this.f5052b = bgVar;
        } else {
            new StringBuilder("null loginState, refreshing:").append(this.f5052b);
        }
        try {
            dismissDialog(20);
            dismissDialog(21);
        } catch (IllegalArgumentException e2) {
        }
        switch (ab.f5162a[this.f5052b.ordinal()]) {
            case 1:
                showDialog(20);
                j();
                m();
                this.p.f4959b.setEnabled(false);
                this.p.f4961d.setEnabled(false);
                this.p.f4965h.setEnabled(false);
                break;
            case 2:
                showDialog(20);
                j();
                n();
                this.p.f4965h.setEnabled(false);
                break;
            case 3:
                showDialog(21);
                l();
                k();
                this.p.o.f4987c.setText(ev.a(fs.TWO_FACTOR_AUTH_SEND_SMS_AGAIN));
                this.p.l.setEnabled(false);
                this.p.l.setVisibility(8);
                this.p.m.setEnabled(false);
                this.p.m.setVisibility(8);
                break;
            case 4:
                showDialog(21);
                l();
                k();
                this.p.o.f4987c.setText(ev.a(fs.TWO_FACTOR_AUTH_SEND_SMS_AGAIN));
                this.p.l.setEnabled(false);
                this.p.l.setVisibility(0);
                this.p.m.setEnabled(false);
                this.p.m.setVisibility(0);
                break;
            case 5:
                showDialog(20);
                l();
                k();
                this.p.o.f4987c.setText(ev.a(fs.TWO_FACTOR_AUTH_SEND_SMS_AGAIN));
                this.p.l.setEnabled(false);
                this.p.l.setVisibility(0);
                this.p.m.setEnabled(false);
                this.p.m.setVisibility(0);
                break;
            case 6:
                j();
                m();
                this.p.f4959b.setEnabled(true);
                this.p.f4961d.setEnabled(true);
                h();
                break;
            case 7:
                j();
                n();
                this.p.f4959b.setEnabled(true);
                this.p.f4961d.setEnabled(true);
                h();
                break;
            case 8:
                j();
                m();
                this.p.f4959b.setEnabled(false);
                this.p.f4961d.setEnabled(false);
                this.p.f4965h.setEnabled(false);
                break;
            case 9:
                j();
                n();
                this.p.f4965h.setEnabled(false);
                break;
            case 10:
                l();
                k();
                this.p.o.f4987c.setText(ev.a(fs.TWO_FACTOR_AUTH_SEND_SMS));
                this.p.l.setEnabled(false);
                this.p.l.setVisibility(8);
                this.p.m.setEnabled(false);
                this.p.m.setVisibility(8);
                break;
            case 11:
                l();
                k();
                this.p.o.f4987c.setText(ev.a(fs.TWO_FACTOR_AUTH_SEND_SMS_AGAIN));
                this.p.l.setEnabled(false);
                this.p.l.setVisibility(0);
                this.p.m.setEnabled(false);
                this.p.m.setVisibility(0);
                break;
            case 12:
                l();
                k();
                this.p.o.f4987c.setText(ev.a(fs.TWO_FACTOR_AUTH_SEND_SMS_AGAIN));
                this.p.l.setEnabled(true);
                this.p.l.setVisibility(0);
                EditText editText = this.p.l;
                editText.requestFocus();
                new Handler().postDelayed(new n(this, editText), 200);
                this.p.m.setVisibility(0);
                i();
                break;
            case 13:
                l();
                k();
                this.p.o.f4987c.setText(ev.a(fs.TWO_FACTOR_AUTH_SEND_SMS_AGAIN));
                this.p.l.setEnabled(false);
                this.p.l.setVisibility(0);
                this.p.m.setEnabled(false);
                this.p.m.setVisibility(0);
                break;
        }
        switch (ab.f5162a[this.f5052b.ordinal()]) {
            case 1:
            case 2:
                this.r.a(new aj(this));
                return;
            case 3:
            case 4:
                this.r.a(new l(this));
                return;
            case 5:
                this.r.a(new m(this));
                return;
            default:
                return;
        }
    }

    static /* synthetic */ void c(LoginActivity loginActivity, View view) {
        loginActivity.g();
        if (loginActivity.f5052b == bg.PIN) {
            loginActivity.b(bg.EMAIL);
        } else {
            loginActivity.b(bg.PIN);
        }
        loginActivity.f();
        loginActivity.p.a(loginActivity.f5052b == bg.EMAIL);
    }

    private boolean c() {
        return getIntent().getBooleanExtra("com.paypal.android.sdk.payments.useResponseTypeCode", false);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.r.c().f4669f.f4683a.isEmpty()) {
            o();
            cf.a(this, ev.a(fs.TWO_FACTOR_AUTH_NO_ACTIVE_TOKENS_ERROR), 10);
            return;
        }
        b(bg.TWO_FA_SEND_FIRST_SMS);
    }

    static /* synthetic */ void d(LoginActivity loginActivity, View view) {
        loginActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(de.a())));
        loginActivity.r.a(ey.SignUp, Boolean.valueOf(loginActivity.l));
    }

    /* access modifiers changed from: private */
    public void e() {
        setResult(-1);
        finish();
    }

    static /* synthetic */ void e(LoginActivity loginActivity, View view) {
        if (loginActivity.f5052b == bg.TWO_FA_ENTER_OTP) {
            loginActivity.b(bg.TWO_FA_SEND_ANOTHER_SMS_IN_PROGRESS);
        } else {
            loginActivity.b(bg.TWO_FA_SEND_FIRST_SMS_IN_PROGRESS);
        }
        loginActivity.p.l.setText("");
        loginActivity.r.a(loginActivity.o);
    }

    private void f() {
        cf.a(this.p.f4960c.f4944b, this.r.e());
        b((bg) null);
    }

    static /* synthetic */ void f(LoginActivity loginActivity, View view) {
        loginActivity.b(bg.TWO_FA_LOGIN_OTP_IN_PROGRESS);
        loginActivity.r.a(loginActivity.a(loginActivity.f5052b), loginActivity.p.l.getText().toString(), loginActivity.l, loginActivity.b(), loginActivity.c(), loginActivity.j);
    }

    private void g() {
        if (this.f5052b == bg.PIN) {
            this.f5055e = this.p.f4959b.getText().toString();
            this.f5057g = this.p.f4961d.getText().toString();
            return;
        }
        this.f5053c = this.p.f4959b.getText().toString();
        this.f5054d = this.p.f4961d.getText().toString();
    }

    static /* synthetic */ void g(LoginActivity loginActivity) {
        switch (ab.f5162a[loginActivity.f5052b.ordinal()]) {
            case 8:
                loginActivity.b(bg.EMAIL);
                return;
            case 9:
                loginActivity.b(bg.PIN);
                return;
            case 10:
            case 12:
            default:
                new StringBuilder().append(loginActivity.f5052b).append(" case not handled");
                return;
            case 11:
                loginActivity.b(bg.TWO_FA_ENTER_OTP);
                return;
            case 13:
                loginActivity.b(bg.TWO_FA_ENTER_OTP);
                return;
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        boolean z = true;
        String obj = this.p.f4959b.getText().toString();
        String obj2 = this.p.f4961d.getText().toString();
        if (this.f5052b == bg.PIN) {
            if (!de.d(obj) || !de.b(obj2)) {
                z = false;
            }
        } else if (!de.a(obj) || !de.c(obj2)) {
            z = false;
        }
        this.p.f4965h.setEnabled(z);
        this.p.f4965h.setFocusable(z);
    }

    static /* synthetic */ void h(LoginActivity loginActivity) {
        if (loginActivity.i.a()) {
            loginActivity.b(bg.EMAIL);
        } else {
            loginActivity.b(bg.PIN);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        this.p.m.setEnabled(6 == this.p.l.getText().toString().length());
    }

    private void j() {
        this.p.o.f4985a.setVisibility(8);
        this.p.k.setEnabled(false);
        this.p.k.setVisibility(8);
        this.p.o.f4987c.setVisibility(8);
        this.p.m.setEnabled(false);
        this.p.m.setVisibility(8);
        this.p.l.setEnabled(false);
        this.p.l.setVisibility(8);
    }

    private void k() {
        cf.a(this, (TextView) null, fs.TWO_FACTOR_AUTH_TITLE);
        this.p.k.setEnabled(true);
        this.p.k.setVisibility(0);
        new StringBuilder("phone numbers: ").append(this.r.c().f4669f.f4683a);
        ArrayList arrayList = new ArrayList(this.r.c().f4669f.f4683a.values());
        this.p.o.a((String) arrayList.get(this.o));
        this.p.o.f4985a.setVisibility(0);
        if (arrayList.size() > 1) {
            this.p.o.a(true);
            fd fdVar = new fd(this, arrayList, this.o);
            new ListView(this).setAdapter((ListAdapter) fdVar);
            this.p.o.f4986b.setOnClickListener(new p(this, fdVar, arrayList));
        } else {
            this.p.o.a(false);
        }
        this.p.o.f4987c.setVisibility(0);
    }

    private void l() {
        this.p.f4965h.setEnabled(false);
        this.p.f4965h.setVisibility(8);
        this.p.f4959b.setEnabled(false);
        this.p.f4959b.setVisibility(8);
        this.p.f4961d.setEnabled(false);
        this.p.f4961d.setVisibility(8);
        this.p.f4962e.setEnabled(false);
        this.p.f4962e.setVisibility(8);
    }

    private void m() {
        cf.a(this, (TextView) null, fs.LOG_IN_TO_PAYPAL);
        this.p.f4959b.setVisibility(0);
        this.p.f4959b.setText(this.f5053c);
        this.p.f4959b.setHint(ev.a(fs.EMAIL));
        this.p.f4959b.setInputType(33);
        this.p.f4961d.setVisibility(0);
        this.p.f4961d.setText(this.f5054d);
        this.p.f4961d.setHint(ev.a(fs.PASSWORD));
        this.p.f4961d.setInputType(129);
        if (this.p.f4959b.getText().length() > 0 && this.p.f4961d.getText().length() == 0) {
            this.p.f4961d.requestFocus();
        }
        this.p.f4959b.setContentDescription("Email");
        this.p.f4961d.setContentDescription("Password");
        this.p.f4965h.setVisibility(0);
        this.p.f4962e.setVisibility(0);
        this.p.f4963f.setVisibility(0);
        this.p.f4964g.setVisibility(0);
        this.p.j.setText(ev.a(fs.LOGIN_WITH_PHONE));
    }

    private void n() {
        cf.a(this, (TextView) null, fs.LOG_IN_TO_PAYPAL);
        this.p.f4959b.setVisibility(0);
        this.p.f4959b.setText(this.f5055e);
        this.p.f4959b.setHint(ev.a(fs.PHONE));
        this.p.f4959b.setInputType(3);
        this.p.f4961d.setVisibility(0);
        this.p.f4961d.setText(this.f5057g);
        this.p.f4961d.setHint(ev.a(fs.PIN));
        this.p.f4961d.setInputType(18);
        if (this.p.f4959b.getText().length() > 0 && this.p.f4961d.getText().length() == 0) {
            this.p.f4961d.requestFocus();
        }
        this.p.f4959b.setContentDescription("Phone");
        this.p.f4961d.setContentDescription("Pin");
        this.p.f4965h.setVisibility(0);
        this.p.f4962e.setVisibility(0);
        this.p.f4963f.setVisibility(0);
        this.p.f4964g.setVisibility(4);
        this.p.j.setText(ev.a(fs.LOGIN_WITH_EMAIL));
    }

    private void o() {
        switch (ab.f5162a[this.f5052b.ordinal()]) {
            case 1:
                b(bg.EMAIL_LOGIN_FAILED);
                return;
            case 2:
                b(bg.PIN_LOGIN_FAILED);
                return;
            case 3:
            case 4:
                b(bg.TWO_FA_SEND_SMS_FAILED);
                return;
            case 5:
                b(bg.TWO_FA_LOGIN_OTP_FAILED);
                return;
            default:
                new StringBuilder().append(this.f5052b).append(" case not handled");
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        PayPalConfiguration d2 = this.r.d();
        if (ev.f4847a) {
            this.p.f4961d.setGravity(5);
            this.p.f4959b.setGravity(5);
            this.p.l.setGravity(5);
        }
        if (!de.f(Locale.getDefault().getCountry().toLowerCase(Locale.US)) || !this.r.c().f4671h) {
            this.p.j.setVisibility(4);
        }
        if (this.m) {
            this.m = false;
            this.f5053c = d2.c();
            String d3 = d2.d();
            if (d3 != null) {
                this.f5055e = d3;
            }
            String e2 = d2.e();
            if (e2 != null) {
                this.f5056f = e2;
            }
            if (d2.f() && !an.c(d2.b())) {
                this.f5054d = d2.g();
                this.f5057g = d2.h();
            }
        }
        if (getIntent().getBooleanExtra("com.paypal.android.sdk.payments.forceLogin", false) && !this.n) {
            this.n = true;
            this.r.h();
        }
        if (!this.r.j()) {
            if (!this.k) {
                this.k = true;
                this.r.a(ey.LoginWindow, Boolean.valueOf(this.l));
            }
            if (this.f5052b == null) {
                di diVar = (di) getIntent().getParcelableExtra("com.paypal.android.sdk.payments.persistedLogin");
                if (diVar != null) {
                    this.l = true;
                    if (TextUtils.isEmpty(this.f5053c) && !TextUtils.isEmpty(diVar.b())) {
                        this.f5053c = diVar.b();
                    }
                    if (this.f5055e == null && diVar.a() != null) {
                        this.f5055e = diVar.a().a(ce.a());
                    }
                    switch (ab.f5163b[diVar.c().ordinal()]) {
                        case 1:
                            b(bg.EMAIL);
                            break;
                        case 2:
                            b(bg.PIN);
                            break;
                    }
                } else {
                    b(bg.EMAIL);
                }
            }
            f();
            return;
        }
        e();
    }

    public final void onBackPressed() {
        this.r.a(ey.LoginCancel, Boolean.valueOf(this.l));
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new StringBuilder().append(getClass().getSimpleName()).append(".onCreate");
        this.j = getIntent().getExtras().getString("com.paypal.android.sdk.payments.requestedScopes");
        this.q = bindService(cf.b(this), this.s, 1);
        setTheme(16973934);
        requestWindowFeature(8);
        this.p = new fz(this);
        setContentView(this.p.f4958a);
        this.p.f4963f.setText(ev.a(fs.SIGN_UP));
        this.p.f4964g.setText(ev.a(fs.FORGOT_PASSWORD));
        this.p.i.setText(ev.a(fs.LOG_IN));
        this.p.i.setHint(ev.a(fs.LOG_IN));
        this.p.k.setText(ev.a(fs.TWO_FACTOR_AUTH_SUBTITLE));
        this.p.l.setHint(ev.a(fs.TWO_FACTOR_AUTH_ENTER_SECURITY_CODE));
        this.p.n.setText(ev.a(fs.LOG_IN));
        this.p.o.b(ev.a(fs.TWO_FACTOR_AUTH_ENTER_MOBILE_NUMBER));
        k kVar = new k(this);
        this.p.f4959b.addTextChangedListener(kVar);
        this.p.f4961d.addTextChangedListener(kVar);
        this.p.f4965h.setOnClickListener(new z(this));
        this.p.f4964g.setOnClickListener(new ac(this));
        this.p.j.setOnClickListener(new ad(this));
        this.p.f4963f.setOnClickListener(new ae(this));
        this.p.o.f4987c.setOnClickListener(new af(this));
        this.p.l.addTextChangedListener(new ah(this));
        this.p.m.setOnClickListener(new ai(this));
        if (bundle == null) {
            this.k = false;
            this.m = true;
        } else {
            this.m = false;
            this.k = bundle.getBoolean("PP_PageTrackingSent");
            this.f5052b = (bg) bundle.getParcelable("PP_LoginType");
            this.f5053c = bundle.getString("PP_SavedEmail");
            this.f5055e = bundle.getString("PP_SavedPhone");
            this.f5056f = bundle.getString("PP_savedPhoneCountryCode");
            this.f5054d = bundle.getString("PP_SavedPassword");
            this.f5057g = bundle.getString("PP_SavedPIN");
            this.l = bundle.getBoolean("PP_IsReturningUser");
            this.n = bundle.getBoolean("PP_IsClearedLogin");
            this.j = bundle.getString("PP_RequestedScopes");
            this.f5058h = bundle.getString("PP_SavedOTP");
            this.i = (el) bundle.getParcelable("PP_OriginalLoginData");
            this.o = bundle.getInt("PP_TwoFaSelectedPhoneNumberIndex");
        }
        this.p.l.setText(this.f5058h);
    }

    /* access modifiers changed from: protected */
    public final Dialog onCreateDialog(int i2, Bundle bundle) {
        switch (i2) {
            case 1:
                return cf.a(this, fs.LOGIN_FAILED_ALERT_TITLE, bundle, new t(this));
            case 2:
                return cf.a(this, fs.WE_ARE_SORRY, bundle, new u(this));
            case 3:
                return cf.a(this, fs.LOGIN_FAILED_ALERT_TITLE, bundle, new v(this));
            case 4:
                return cf.a(this, fs.LOGIN_FAILED_ALERT_TITLE, bundle, new w(this));
            case 5:
                return cf.a(this, fs.SESSION_EXPIRED_TITLE, bundle, new x(this));
            case 10:
                return cf.a(this, fs.LOGIN_FAILED_ALERT_TITLE, bundle, new aa(this));
            case 20:
                return cf.a(this, fs.AUTHENTICATING, fs.ONE_MOMENT);
            case 21:
                return cf.a(this, fs.TWO_FACTOR_AUTH_SENDING_DIALOG, fs.ONE_MOMENT);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        new StringBuilder().append(getClass().getSimpleName()).append(".onDestroy");
        if (this.r != null) {
            this.r.n();
        }
        if (this.q) {
            unbindService(this.s);
            this.q = false;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void onResume() {
        super.onResume();
        new StringBuilder().append(getClass().getSimpleName()).append(".onResume");
        if (this.r != null) {
            f();
        }
    }

    /* access modifiers changed from: protected */
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        g();
        bundle.putParcelable("PP_LoginType", this.f5052b);
        bundle.putString("PP_SavedEmail", this.f5053c);
        bundle.putString("PP_SavedPhone", this.f5055e);
        bundle.putString("PP_savedPhoneCountryCode", this.f5056f);
        bundle.putString("PP_SavedPassword", this.f5054d);
        bundle.putString("PP_SavedPIN", this.f5057g);
        bundle.putBoolean("PP_IsReturningUser", this.l);
        bundle.putBoolean("PP_PageTrackingSent", this.k);
        bundle.putBoolean("PP_IsClearedLogin", this.n);
        bundle.putString("PP_RequestedScopes", this.j);
        bundle.putString("PP_SavedOTP", this.f5058h);
        bundle.putParcelable("PP_OriginalLoginData", this.i);
        bundle.putInt("PP_TwoFaSelectedPhoneNumberIndex", this.o);
    }
}
