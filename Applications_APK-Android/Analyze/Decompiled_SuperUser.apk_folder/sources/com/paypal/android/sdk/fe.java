package com.paypal.android.sdk;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public final class fe {

    /* renamed from: a  reason: collision with root package name */
    public TableLayout f4888a;

    /* renamed from: b  reason: collision with root package name */
    public TableLayout f4889b;

    /* renamed from: c  reason: collision with root package name */
    public TextView f4890c;

    /* renamed from: d  reason: collision with root package name */
    public TextView f4891d;

    /* renamed from: e  reason: collision with root package name */
    private int f4892e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f4893f = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, float):void
     arg types: [android.widget.TextView, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void */
    public fe(Context context, String str) {
        this.f4889b = new TableLayout(context);
        this.f4889b.setColumnShrinkable(0, false);
        this.f4889b.setColumnStretchable(0, false);
        this.f4889b.setColumnStretchable(1, false);
        this.f4889b.setColumnShrinkable(1, false);
        TableRow tableRow = new TableRow(context);
        this.f4889b.addView(tableRow);
        this.f4891d = new TextView(context);
        this.f4891d.setTextColor(bv.i);
        this.f4891d.setText("Item");
        this.f4891d.setSingleLine(true);
        this.f4891d.setGravity(83);
        this.f4891d.setTextSize(18.0f);
        this.f4891d.setTextColor(bv.i);
        this.f4891d.setTypeface(bv.q);
        tableRow.addView(this.f4891d);
        bw.a((View) this.f4891d, 16, 1.0f);
        this.f4892e = bw.a("10dip", context);
        bw.b(this.f4891d, null, null, "10dip", null);
        this.f4890c = new TextView(context);
        this.f4890c.setTextSize(18.0f);
        this.f4890c.setTypeface(bv.r);
        this.f4890c.setText(str);
        this.f4890c.setSingleLine(true);
        this.f4890c.setGravity(85);
        this.f4890c.setTextColor(bv.j);
        tableRow.addView(this.f4890c);
        bw.a((View) this.f4890c, 5, 1.0f);
        this.f4888a = this.f4889b;
    }

    public final void a() {
        TextView textView = this.f4890c;
        TextView textView2 = this.f4891d;
        float measureText = textView.getPaint().measureText(textView.getText().toString());
        int width = (this.f4889b.getWidth() - ((int) measureText)) - this.f4892e;
        CharSequence ellipsize = TextUtils.ellipsize(textView2.getText(), textView2.getPaint(), (float) width, TextUtils.TruncateAt.END);
        textView2.setWidth(width);
        textView.setWidth((int) measureText);
        textView2.setText(ellipsize);
    }
}
