package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;

final class av implements Parcelable.Creator {
    av() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new PayPalPaymentDetails(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new PayPalPaymentDetails[i];
    }
}
