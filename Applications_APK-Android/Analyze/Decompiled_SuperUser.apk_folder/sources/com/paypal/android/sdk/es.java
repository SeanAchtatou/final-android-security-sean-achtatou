package com.paypal.android.sdk;

import java.util.Locale;

public final class es extends bg {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f4844a;

    /* renamed from: b  reason: collision with root package name */
    private final int f4845b;

    public es(bu buVar, int i, boolean z, int i2) {
        super(i, buVar);
        this.f4844a = z;
        this.f4845b = i2;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        int i = this.f4845b;
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 != 0) {
                sb.append(",\n");
            }
            sb.append(String.format(Locale.US, "    {\n        \"type\":\"sms_otp\",\n        \"token_identifier\":\"mock_token_id_%s\",\n        \"token_identifier_display\":\"xxx-xxx-%s\"\n    }\n", Integer.valueOf(i2), new String(new char[4]).replace("\u0000", new StringBuilder().append(i2).toString()).substring(0, 4)));
        }
        return String.format(Locale.US, "{\n    \"nonce\":\"mock-login-nonce\",\n    \"error\":\"2fa_required\",\n    \"error_description\":\"Unable to authenticate the user. 2fa flow completion is necessary for successful login.\",\n    \"visitor_id\":\"mock-visitor_id\",\n    \"2fa_enabled\":\"true\",\n    \"2fa_token_identifier\":[\n%s    ]\n}", sb.toString());
    }

    /* access modifiers changed from: protected */
    public final int c() {
        return 401;
    }

    /* access modifiers changed from: protected */
    public final boolean c(bt btVar) {
        return this.f4844a && (btVar instanceof eg) && !((eg) btVar).t();
    }
}
