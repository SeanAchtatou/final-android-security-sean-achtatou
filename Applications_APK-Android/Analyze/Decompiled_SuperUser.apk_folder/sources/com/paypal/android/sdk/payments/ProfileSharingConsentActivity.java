package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.content.Intent;

public final class ProfileSharingConsentActivity extends dn {

    /* renamed from: d  reason: collision with root package name */
    private static final String f5142d = ProfileSharingConsentActivity.class.getSimpleName();

    /* access modifiers changed from: package-private */
    public static void a(Activity activity, int i, PayPalConfiguration payPalConfiguration) {
        Intent intent = new Intent(activity, ProfileSharingConsentActivity.class);
        intent.putExtras(activity.getIntent());
        intent.putExtra("com.paypal.android.sdk.paypalConfiguration", payPalConfiguration);
        activity.startActivityForResult(intent, 1);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f5271b = (PayPalOAuthScopes) getIntent().getParcelableExtra("com.paypal.android.sdk.requested_scopes");
    }

    public final /* bridge */ /* synthetic */ void finish() {
        super.finish();
    }

    public final /* bridge */ /* synthetic */ void onBackPressed() {
        super.onBackPressed();
    }
}
