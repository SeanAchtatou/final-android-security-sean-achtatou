package com.paypal.android.sdk.payments;

import android.text.style.URLSpan;
import android.view.View;

final class eb extends URLSpan {

    /* renamed from: a  reason: collision with root package name */
    private bc f5303a;

    eb(URLSpan uRLSpan, bc bcVar) {
        super(uRLSpan.getURL());
        this.f5303a = bcVar;
    }

    public final void onClick(View view) {
        this.f5303a.a();
        super.onClick(view);
    }
}
