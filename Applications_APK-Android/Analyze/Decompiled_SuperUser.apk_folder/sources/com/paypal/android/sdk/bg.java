package com.paypal.android.sdk;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public abstract class bg extends be {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f4589a = bg.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final bu f4590b;

    /* renamed from: c  reason: collision with root package name */
    private final ThreadPoolExecutor f4591c = ((ThreadPoolExecutor) Executors.newCachedThreadPool());
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final int f4592d;

    public bg(int i, bu buVar) {
        this.f4592d = i;
        this.f4590b = buVar;
    }

    public final void a() {
    }

    /* access modifiers changed from: protected */
    public abstract String b();

    public final boolean b(bt btVar) {
        this.f4591c.submit(new bh(this, btVar));
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract int c();

    /* access modifiers changed from: protected */
    public abstract boolean c(bt btVar);
}
