package com.paypal.android.sdk.payments;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

final class cv implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PaymentMethodActivity f5250a;

    cv(PaymentMethodActivity paymentMethodActivity) {
        this.f5250a = paymentMethodActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        new StringBuilder().append(PaymentMethodActivity.f5134a).append(".onServiceConnected");
        if (this.f5250a.isFinishing()) {
            new StringBuilder().append(PaymentMethodActivity.f5134a).append(".onServiceConnected exit - isFinishing");
            return;
        }
        PayPalService unused = this.f5250a.i = ((bh) iBinder).f5201a;
        if (this.f5250a.i.a(new cw(this))) {
            PaymentMethodActivity.i(this.f5250a);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        PayPalService unused = this.f5250a.i = (PayPalService) null;
    }
}
