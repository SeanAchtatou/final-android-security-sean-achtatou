package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.content.Intent;
import android.text.style.URLSpan;
import android.view.View;

final class b extends URLSpan {

    /* renamed from: a  reason: collision with root package name */
    private Activity f5185a;

    /* renamed from: b  reason: collision with root package name */
    private Class f5186b;

    /* renamed from: c  reason: collision with root package name */
    private bc f5187c;

    /* renamed from: d  reason: collision with root package name */
    private ag f5188d;

    b(URLSpan uRLSpan, Activity activity, Class cls, bc bcVar, ag agVar) {
        super(uRLSpan.getURL());
        this.f5185a = activity;
        this.f5186b = cls;
        this.f5187c = bcVar;
        this.f5188d = agVar;
    }

    public final void onClick(View view) {
        Intent intent = new Intent(this.f5185a, this.f5186b);
        intent.putExtra("com.paypal.details.scope", this.f5188d);
        this.f5187c.a();
        this.f5185a.startActivity(intent);
    }
}
