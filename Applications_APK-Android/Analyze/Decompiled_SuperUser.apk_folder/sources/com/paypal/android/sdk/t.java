package com.paypal.android.sdk;

import android.os.Handler;
import android.os.Message;
import java.lang.ref.WeakReference;

final class t extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference f5357a;

    public t(r rVar) {
        this.f5357a = new WeakReference(rVar);
    }

    public final void handleMessage(Message message) {
        r rVar = (r) this.f5357a.get();
        if (rVar != null) {
            rVar.a(message);
        }
    }
}
