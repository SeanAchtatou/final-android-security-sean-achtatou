package com.paypal.android.sdk;

import android.util.Log;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public abstract class bt {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4624a = bt.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static long f4625b = 1;

    /* renamed from: c  reason: collision with root package name */
    private final Map f4626c = new LinkedHashMap();

    /* renamed from: d  reason: collision with root package name */
    private final bu f4627d;

    /* renamed from: e  reason: collision with root package name */
    private final br f4628e;

    /* renamed from: f  reason: collision with root package name */
    private final String f4629f;

    /* renamed from: g  reason: collision with root package name */
    private final long f4630g;

    /* renamed from: h  reason: collision with root package name */
    private String f4631h;
    private String i;
    private aw j;
    private Integer k;
    private String l;

    public bt(br brVar, bu buVar, x xVar, String str) {
        long j2 = f4625b;
        f4625b = 1 + j2;
        this.f4630g = j2;
        this.f4628e = brVar;
        this.f4629f = str;
        this.f4627d = buVar;
    }

    public static void k() {
    }

    public String a(br brVar) {
        String a2 = this.f4627d.a(brVar);
        if (a2 != null) {
            return this.f4629f != null ? a2 + this.f4629f : a2;
        }
        throw new RuntimeException("API " + brVar.toString() + " has no record for server " + this.f4627d.c());
    }

    public final void a(aw awVar) {
        if (this.j != null) {
            IllegalStateException illegalStateException = new IllegalStateException("Multiple exceptions reported");
            Log.e(f4624a, "first mError=" + this.j);
            Log.e(f4624a, "second mError=" + awVar);
            Log.e(f4624a, "", illegalStateException);
            throw illegalStateException;
        }
        this.j = awVar;
    }

    public final void a(Integer num) {
        this.k = num;
    }

    public final void a(String str) {
        this.f4631h = str;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2) {
        this.f4626c.put(str, str2);
    }

    public final void a(String str, String str2, String str3) {
        a(new ay(str, str2, str3));
    }

    public boolean a() {
        return false;
    }

    public abstract String b();

    public final void b(String str) {
        this.i = str;
    }

    public abstract void c();

    public final void c(String str) {
        this.l = str;
    }

    public abstract void d();

    public abstract String e();

    public final String f() {
        return this.f4631h;
    }

    public final String g() {
        return this.i;
    }

    public final br h() {
        return this.f4628e;
    }

    public final Map i() {
        return this.f4626c;
    }

    public final String j() {
        return this.l;
    }

    public void l() {
    }

    /* access modifiers changed from: protected */
    public final JSONObject m() {
        String str = this.i;
        Object nextValue = new JSONTokener(str).nextValue();
        if (nextValue instanceof JSONObject) {
            return (JSONObject) nextValue;
        }
        throw new JSONException("could not parse:" + str + "\nnextValue:" + nextValue);
    }

    public final String n() {
        return getClass().getSimpleName() + " SN:" + this.f4630g;
    }

    public final long o() {
        return this.f4630g;
    }

    public final aw p() {
        return this.j;
    }

    public final boolean q() {
        return this.j == null;
    }

    public final Integer r() {
        return this.k;
    }

    public final bu s() {
        return this.f4627d;
    }
}
