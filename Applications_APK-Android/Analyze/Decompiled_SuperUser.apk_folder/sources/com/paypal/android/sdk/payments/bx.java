package com.paypal.android.sdk.payments;

import com.paypal.android.sdk.ev;

final class bx implements bi {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PaymentConfirmActivity f5216a;

    bx(PaymentConfirmActivity paymentConfirmActivity) {
        this.f5216a = paymentConfirmActivity;
    }

    public final void a() {
        String unused = PaymentConfirmActivity.f5122a;
        this.f5216a.g();
    }

    public final void a(bj bjVar) {
        this.f5216a.j();
        cf.a(this.f5216a, ev.a(bjVar.f5203b), 1);
        if (this.f5216a.i != dh.PayPal) {
            this.f5216a.f5128g.b(true);
        }
    }
}
