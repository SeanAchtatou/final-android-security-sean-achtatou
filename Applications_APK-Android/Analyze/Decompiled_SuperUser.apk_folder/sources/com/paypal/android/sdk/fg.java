package com.paypal.android.sdk;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Locale;

public final class fg extends ArrayAdapter {

    /* renamed from: a  reason: collision with root package name */
    private int f4902a;

    public fg(Context context, ArrayList arrayList, int i) {
        super(context, 0, arrayList);
        this.f4902a = i;
    }

    private static int a(Context context, RelativeLayout relativeLayout, String str, int i) {
        if (cd.a((CharSequence) str)) {
            return i;
        }
        TextView textView = new TextView(context);
        textView.setId(i + 1);
        RelativeLayout.LayoutParams a2 = bw.a(-2, -2, 1, 2301);
        a2.addRule(0, 2302);
        a2.addRule(3, i);
        textView.setText(str);
        bw.d(textView, 83);
        bw.b(textView, "6dip", null, "6dip", null);
        relativeLayout.addView(textView, a2);
        textView.setEllipsize(TextUtils.TruncateAt.END);
        return i + 1;
    }

    public final void a(int i) {
        this.f4902a = i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        String str;
        ff ffVar = (ff) getItem(i);
        Context context = viewGroup.getContext();
        LinearLayout linearLayout = new LinearLayout(viewGroup.getContext());
        RelativeLayout relativeLayout = new RelativeLayout(context);
        linearLayout.addView(relativeLayout);
        bw.a(relativeLayout, null, "6dip", null, "6dip");
        ImageView a2 = bw.a(viewGroup.getContext(), ffVar.a(), "");
        a2.setId(2301);
        RelativeLayout.LayoutParams a3 = bw.a(context, "40dip", "40dip", 9);
        a3.addRule(10);
        relativeLayout.addView(a2, a3);
        bw.a(a2, "6dip", null, null, null);
        ImageView a4 = bw.a(viewGroup.getContext(), "iVBORw0KGgoAAAANSUhEUgAAAGQAAABZCAYAAADIBoEnAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABb9JREFUeNrsnE1oXFUUx+8MgyD9YHDRLrow1S6LJgsV3JgsgnSXLtwJTTcqcWGL4La2SyHEjZKupkI2rpp9F2YVIQunJAGjNIkRgqRCLUwkUCLjOcz/kuc4mfdxP96d+86BwxtC5r157zfnf865H1NXYkFZXR5BMet2X4zh+LIAKRdEk7xFL3fp+BEd36Xjq/JkyoExSf4XeRd+SD5H/j75JYkQvzAW6PADeTPx5zPkX5G/Tn7VBhQBkk2iGMStU/7FKhQBMhzGOKJiMuVfrUERIOkwxjO+RUN5wwSKABkOo5nzrRrKNKBcESDlwUja5+QfsoTR+a7meWNDEFiHoY2BXCS/T+c9T8e1Wu2lY4mQcmBom4aEXUEDeU6AlAdD22vk35K/Q/52WrKvCQynMPptCb5PvjVIwmoVh9HMWdrasHXyefIn5BsEpSNAToC0PcPQ9jegrCJS9iqfQzBiO17S5RnEIfkR+XHly16C8SUdZku49CPkkJ/JtykynlW+DyEYM3S44/myOm/8BhD70hieVFQtj5c84MYQErXNQNKaw0aFYDQBo+npksuQp9/JNwnEkQyd/NcWPCXxA8jTen8FJUBOomPWUxJ/BIn6Y1CPIUD85Q3uKxYB5NQuvPJAEnnDtUTdI/8FIPZNThZ7hLjOGz8iX/xJ3i4iUZUBgn7DZd5YhkwxhLWiElUJIFhV6FKq5nW+IBCbNk8ca4S46jc4ed9FSbtHMLZsX6ARYXTw+qlJRzC+QMe9aZq8T7NaZDBYqtoOomMHMuUURowR0nIEgyPjOZJ3x+UN1COKDhdS5RVGNJLlSKq8w4gpQloxwIgCiAOpKg3GyEsWxqp2LUZHqTBiiJBWTDCMy97EN5THdb5XvZHPvUGT9w6ig2VqxtLpDgCDV4JslAXDWLKwsyip33raUg9FHzuC0URVNTYqHbhzycKeu/5kyt/Y78g/IH/P4e7UWzHCKBwhmBJNG03V05k8yd+2FS3oOXYt3T/DcDZQ6CWH5JgS5aX4vOeOx4DO87JNS7nF1rC6XoiwHwqM3JKVWCme1XgpPu+PuEb+lqmEYdLJRs9xHxH8zPZ8hrcIMVjXxPvueIvXWfKHvGmlyEPA9Rcs3DODeKh6M33t0Or4eo6HYbps/2OAuUTnmyDPK5c2Erle0nlsM6+VIVm2VopPA8oF1dtN1Mj4hWAQn1lo/O7h9VrWlYTBAcGy/RmL19RQzuWAcsewI9f7MQ5R3nZUoFbPAGPWwXUzQ0FHbvoZ7qLX2Auh1ygExMPyyySUiZToMK2o1lFRbanArW7Q+NmCcp38lUEb7C2UubqiOgqxosoExCOMZPU1jeqr/6coTMrcHURHsBVV1gh5U/V2+vi0T9DV809RXMAXw6TMTSbxrZCTeL/VUrryG6iwxjx8Fh4C/1T15iN+he4Xray4vF1VAY1RGSd1upHH5LfJL0PnVxx/lotI8g080KIwlgGjM2owhkbIkBL0huPqawlfgDMF88Yc8sZqqM2fNSB9nXNLuVmyadL8zUH6OIk/VSNohSao6GZ5N+kUvWR/HMi9zKuTKeSRhFEYSALMCjk3dbeRjMuykc4b1oAkwHxNh8t4ML6N88ai7jfUiJu1ZUAE5Tn5dSRkX9Gi92so9BtHAuT/YJYRLSsePv8i8sbT0AcNSwOSiJYp5BZXxhsueayKo2JDRWJOVy4it0w4kDA9NMK2OSrjVKUD0R0/JMxmecx5g8eptn2skowKSELCOFIeWCpx11HiPlGRmdfF1vQAbxrmFU7gS3i9oSI076vfkVduGnTjWqo6AsQelAcFkv1SzFJVKpBEsp/KCGUndqkqHUhOKLrEjVaqggCSEQpXVduxS1UwQFKgRF9VBQlkCJToq6pggQyA8g35T1WRKm3B/dYJQ+l2X/AQPsP5R4mJiWSJCRABIiZABIiYABEgYgJEgIgJEDEBIkDE0u1fAQYA3p2Buu6CTa4AAAAASUVORK5CYII=", "checked");
        a4.setId(2302);
        RelativeLayout.LayoutParams a5 = bw.a(context, "20dip", "20dip", 11);
        a5.addRule(10);
        relativeLayout.addView(a4, a5);
        a4.setColorFilter(bv.f4633b);
        bw.a(a4, null, null, "8dip", null);
        if (i == this.f4902a) {
            a4.setVisibility(0);
        } else {
            a4.setVisibility(4);
        }
        TextView textView = new TextView(context);
        textView.setId(2303);
        textView.setText(ffVar.f());
        bw.b(textView, 83);
        bw.b(textView, "6dip", null, "6dip", null);
        relativeLayout.addView(textView, bw.a(-2, -2, 1, 2301));
        int a6 = a(context, relativeLayout, ffVar.h(), a(context, relativeLayout, ffVar.g(), 2303));
        StringBuilder sb = new StringBuilder();
        sb.append(ffVar.i());
        sb.append(" ");
        sb.append(ffVar.j());
        if (cd.b((CharSequence) ffVar.k())) {
            sb.append("  ");
            sb.append(ffVar.k());
        }
        if (cd.b((CharSequence) ffVar.l())) {
            StringBuilder append = sb.append("  ");
            String l = ffVar.l();
            if (!cd.a((CharSequence) l)) {
                String lowerCase = Locale.getDefault().getCountry().toLowerCase(Locale.US);
                if (cd.a((CharSequence) lowerCase) || !lowerCase.equals(l.toLowerCase(Locale.US))) {
                    str = "[" + l + "]";
                    append.append(str);
                }
            }
            str = "";
            append.append(str);
        }
        a(context, relativeLayout, sb.toString(), a6);
        return linearLayout;
    }
}
