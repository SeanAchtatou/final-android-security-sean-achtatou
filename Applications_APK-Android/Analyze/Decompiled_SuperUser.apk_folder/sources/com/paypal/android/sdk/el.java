package com.paypal.android.sdk;

import android.os.Parcel;
import android.os.Parcelable;

public final class el implements Parcelable {
    public static final Parcelable.Creator CREATOR = new dc();

    /* renamed from: a  reason: collision with root package name */
    private String f4814a;

    /* renamed from: b  reason: collision with root package name */
    private String f4815b;

    /* renamed from: c  reason: collision with root package name */
    private er f4816c;

    /* renamed from: d  reason: collision with root package name */
    private String f4817d;

    public el(Parcel parcel) {
        this.f4814a = parcel.readString();
        this.f4815b = parcel.readString();
        this.f4816c = (er) parcel.readParcelable(er.class.getClassLoader());
        this.f4817d = parcel.readString();
    }

    public el(er erVar, String str) {
        this.f4816c = erVar;
        this.f4817d = str;
    }

    public el(String str, String str2) {
        this.f4814a = str;
        this.f4815b = str2;
    }

    public final boolean a() {
        return this.f4814a != null;
    }

    public final String b() {
        return this.f4814a;
    }

    public final String c() {
        return this.f4815b;
    }

    public final er d() {
        return this.f4816c;
    }

    public final int describeContents() {
        return 0;
    }

    public final String e() {
        return this.f4817d;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f4814a);
        parcel.writeString(this.f4815b);
        parcel.writeParcelable(this.f4816c, 0);
        parcel.writeString(this.f4817d);
    }
}
