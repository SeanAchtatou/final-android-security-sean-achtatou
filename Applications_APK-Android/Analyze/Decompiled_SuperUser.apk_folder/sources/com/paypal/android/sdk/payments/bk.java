package com.paypal.android.sdk.payments;

import android.util.Log;
import com.paypal.android.sdk.ce;
import com.paypal.android.sdk.ch;
import com.paypal.android.sdk.cr;
import com.paypal.android.sdk.ct;
import com.paypal.android.sdk.di;
import com.paypal.android.sdk.dk;
import com.paypal.android.sdk.dp;
import com.paypal.android.sdk.ds;
import com.paypal.android.sdk.dt;
import com.paypal.android.sdk.du;
import com.paypal.android.sdk.dy;
import com.paypal.android.sdk.dz;
import com.paypal.android.sdk.ea;
import com.paypal.android.sdk.eb;
import com.paypal.android.sdk.ed;
import com.paypal.android.sdk.ef;
import com.paypal.android.sdk.eg;
import com.paypal.android.sdk.el;
import com.paypal.android.sdk.ep;
import com.paypal.android.sdk.eq;
import com.paypal.android.sdk.ey;
import java.util.UUID;

final class bk implements ct {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PayPalService f5204a;

    private bk(PayPalService payPalService) {
        this.f5204a = payPalService;
    }

    /* synthetic */ bk(PayPalService payPalService, byte b2) {
        this(payPalService);
    }

    private static ds a(String str, String str2, long j) {
        return new ds(str, str2, System.currentTimeMillis() + (1000 * j), true);
    }

    public final void a() {
        String unused = PayPalService.f5109c;
    }

    public final void a(dt dtVar) {
        String unused = PayPalService.f5109c;
        this.f5204a.a(ey.PaymentSuccessful, dtVar.j(), dtVar.t());
        if (!dtVar.u() || (this.f5204a.c().f4670g != null && !this.f5204a.c().f4670g.a())) {
            this.f5204a.h();
        } else {
            ch.a(this.f5204a.c().f4670g, this.f5204a.e());
            if (this.f5204a.c().f4667d != null) {
                this.f5204a.p.a(this.f5204a.c().f4667d);
            }
        }
        this.f5204a.i.a(cf.a(dtVar));
        this.f5204a.i.a();
    }

    public final void a(dy dyVar) {
        String unused = PayPalService.f5109c;
        this.f5204a.a(ey.AuthorizationSuccessful, dyVar.j());
        this.f5204a.i.a(dyVar.f4770a);
        this.f5204a.i.a();
    }

    public final void a(dz dzVar) {
        String unused = PayPalService.f5109c;
        this.f5204a.i.a(dzVar);
        this.f5204a.i.a();
    }

    public final void a(ea eaVar) {
        String unused = PayPalService.f5109c;
        this.f5204a.a(ey.PaymentSuccessful, eaVar.j(), eaVar.D());
        this.f5204a.i.a(cf.a(eaVar));
        if (eaVar.t() == null || !eaVar.A()) {
            if (!eaVar.A()) {
                this.f5204a.t();
            }
            this.f5204a.i.a();
            return;
        }
        this.f5204a.b().b(new eq(this.f5204a.b(), this.f5204a.a(), this.f5204a.c().f4665b.c(), UUID.randomUUID().toString(), eaVar.u(), eaVar.t(), eaVar.v(), eaVar.w(), eaVar.x()));
    }

    public final void a(eb ebVar) {
        String b2 = ebVar.p().b();
        String unused = PayPalService.f5109c;
        new StringBuilder("DeleteCreditCardRequest Error:").append(b2);
        Log.e("paypal.sdk", b2);
    }

    public final void a(ed edVar) {
        String unused = PayPalService.f5109c;
        this.f5204a.i.a(edVar);
        this.f5204a.i.a();
    }

    public final void a(ef efVar) {
        String unused = PayPalService.f5109c;
        this.f5204a.c().i = efVar.f4807g;
        this.f5204a.f5115h.a();
    }

    public final void a(eg egVar) {
        long j = 840;
        String unused = PayPalService.f5109c;
        long j2 = egVar.f4805f;
        if (j2 <= 840) {
            j = j2;
        }
        if (egVar.f4802c == null) {
            this.f5204a.c().f4670g = a(egVar.f4803d, egVar.f4804e, j);
        } else {
            this.f5204a.c().i = egVar.f4807g;
            this.f5204a.c().f4668e = new du(egVar.f4802c, egVar.f4804e);
        }
        this.f5204a.c().f4669f = null;
        di diVar = new di();
        if (egVar.f4800a.a()) {
            diVar.a(egVar.f4800a.b());
            diVar.a(dk.EMAIL);
        } else {
            diVar.a(egVar.f4800a.d());
            diVar.a(dk.PHONE);
        }
        this.f5204a.c().f4667d = diVar;
        if (egVar.f4800a.a()) {
            this.f5204a.c().f4666c = egVar.f4800a.b();
            this.f5204a.a(ey.LoginPassword, Boolean.valueOf(egVar.f4801b), egVar.j());
        } else {
            this.f5204a.c().f4666c = egVar.f4800a.d().a(ce.a());
            this.f5204a.a(ey.LoginPIN, Boolean.valueOf(egVar.f4801b), egVar.j());
        }
        this.f5204a.f5115h.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, boolean):boolean
     arg types: [com.paypal.android.sdk.payments.PayPalService, int]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.payments.bi):com.paypal.android.sdk.payments.bi
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.bt):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.Boolean):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void
      com.paypal.android.sdk.payments.PayPalService.a(java.lang.String, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, boolean):boolean */
    public final void a(ep epVar) {
        String unused = PayPalService.f5109c;
        this.f5204a.a(ey.DeviceCheck, epVar.j());
        this.f5204a.c().f4665b = a(epVar.f4829a, epVar.f4830b, epVar.f4831c);
        this.f5204a.c().f4671h = epVar.f4832d;
        boolean unused2 = this.f5204a.f5114g = false;
        if (this.f5204a.m != null) {
            String unused3 = PayPalService.f5109c;
            this.f5204a.m.a();
            bi unused4 = this.f5204a.m = null;
        }
    }

    public final void a(eq eqVar) {
        String unused = PayPalService.f5109c;
        this.f5204a.p.a(new dp(this.f5204a.o, eqVar.t(), eqVar.f4833a, eqVar.v(), eqVar.u(), eqVar.w(), eqVar.x(), eqVar.y()), this.f5204a.f5113f.k());
        this.f5204a.i.a();
    }

    public final void b(dt dtVar) {
        String b2 = dtVar.p().b();
        String unused = PayPalService.f5109c;
        new StringBuilder("ApproveAndExecuteSfoPaymentRequest Error: ").append(b2);
        PayPalService.c(this.f5204a, dtVar);
    }

    public final void b(dy dyVar) {
        String b2 = dyVar.p().b();
        String unused = PayPalService.f5109c;
        new StringBuilder("ConsentRequest Error:").append(b2);
        Log.e("paypal.sdk", b2);
        this.f5204a.i.a(this.f5204a.b(dyVar));
    }

    public final void b(dz dzVar) {
        String b2 = dzVar.p().b();
        String unused = PayPalService.f5109c;
        new StringBuilder("CreateSfoPaymentRequest Error: ").append(b2);
        Log.e("paypal.sdk", b2);
        this.f5204a.i.a(this.f5204a.b(dzVar));
    }

    public final void b(ea eaVar) {
        String b2 = eaVar.p().b();
        String unused = PayPalService.f5109c;
        new StringBuilder("CreditCardPaymentRequest Error:").append(b2);
        PayPalService.c(this.f5204a, eaVar);
    }

    public final void b(ed edVar) {
        String b2 = edVar.p().b();
        String unused = PayPalService.f5109c;
        new StringBuilder("GetAppInfoRequest Error: ").append(b2);
        Log.e("paypal.sdk", b2);
        this.f5204a.i.a(this.f5204a.b(edVar));
    }

    public final void b(ef efVar) {
        String b2 = efVar.p().b();
        String unused = PayPalService.f5109c;
        new StringBuilder("LoginChallengeRequest Error:").append(b2);
        Log.e("paypal.sdk", b2);
        this.f5204a.f5115h.a(this.f5204a.b(efVar));
    }

    public final void b(eg egVar) {
        String b2 = egVar.p().b();
        String unused = PayPalService.f5109c;
        new StringBuilder("LoginRequest Error: ").append(b2);
        Log.e("paypal.sdk", b2);
        this.f5204a.h();
        el elVar = egVar.f4800a;
        boolean z = egVar.f4801b;
        if (elVar.a()) {
            this.f5204a.a(ey.LoginPassword, z, b2, egVar.j(), (String) null);
        } else {
            this.f5204a.a(ey.LoginPIN, z, b2, egVar.j(), (String) null);
        }
        this.f5204a.p.b();
        if (egVar.f4808h) {
            this.f5204a.c().i = egVar.f4807g;
            this.f5204a.c().f4669f = new cr(egVar.i);
        }
        this.f5204a.f5115h.a(this.f5204a.b(egVar));
    }

    public final void b(ep epVar) {
        PayPalService.a(this.f5204a, epVar);
    }

    public final void b(eq eqVar) {
        String b2 = eqVar.p().b();
        String unused = PayPalService.f5109c;
        new StringBuilder("TokenizeCreditCardRequest Error:").append(b2);
        Log.e("paypal.sdk", b2);
        this.f5204a.i.a();
    }
}
