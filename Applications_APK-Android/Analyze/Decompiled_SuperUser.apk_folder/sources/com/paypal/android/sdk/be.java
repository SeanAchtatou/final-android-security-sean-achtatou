package com.paypal.android.sdk;

import android.util.Log;
import org.json.JSONException;

public abstract class be implements bq {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4587a = be.class.getSimpleName();

    protected static void a(bt btVar) {
        try {
            new StringBuilder("parsing success response\n:").append(btVar.g());
            btVar.c();
        } catch (Exception e2) {
            Log.e("paypal.sdk", "Exception parsing server response", e2);
            btVar.a(new ay(bx.PARSE_RESPONSE_ERROR, e2));
        }
    }

    protected static void a(bt btVar, int i) {
        btVar.a(Integer.valueOf(i));
        try {
            new StringBuilder("parsing error response:\n").append(btVar.g());
            btVar.d();
        } catch (JSONException e2) {
            Log.e("paypal.sdk", "Exception parsing server response", e2);
            btVar.a(bx.INTERNAL_SERVER_ERROR.toString(), i + " http response received.  Response not parsable: " + e2.getMessage(), null);
        }
    }
}
