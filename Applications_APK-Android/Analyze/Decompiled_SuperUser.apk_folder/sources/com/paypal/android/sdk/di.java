package com.paypal.android.sdk;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public final class di implements Parcelable {
    public static final Parcelable.Creator CREATOR = new cj();

    /* renamed from: a  reason: collision with root package name */
    private er f4724a;

    /* renamed from: b  reason: collision with root package name */
    private String f4725b;

    /* renamed from: c  reason: collision with root package name */
    private dk f4726c;

    public di() {
    }

    public di(Parcel parcel) {
        this.f4724a = (er) parcel.readParcelable(er.class.getClassLoader());
        this.f4725b = parcel.readString();
        this.f4726c = (dk) parcel.readSerializable();
    }

    public di(String str, er erVar, dk dkVar) {
        this.f4725b = str;
        this.f4724a = erVar;
        this.f4726c = dkVar;
    }

    public final er a() {
        return this.f4724a;
    }

    public final void a(dk dkVar) {
        this.f4726c = dkVar;
    }

    public final void a(er erVar) {
        this.f4724a = erVar;
    }

    public final void a(String str) {
        this.f4725b = str;
    }

    public final String b() {
        return this.f4725b;
    }

    public final dk c() {
        return this.f4726c;
    }

    public final boolean d() {
        return !(this.f4726c == null || ((this.f4724a == null && this.f4726c.equals(dk.PHONE)) || (TextUtils.isEmpty(this.f4725b) && this.f4726c.equals(dk.EMAIL))));
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f4724a, 0);
        parcel.writeString(this.f4725b);
        parcel.writeSerializable(this.f4726c);
    }
}
