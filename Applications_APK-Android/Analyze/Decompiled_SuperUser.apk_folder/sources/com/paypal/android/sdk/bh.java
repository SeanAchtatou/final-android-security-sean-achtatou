package com.paypal.android.sdk;

import android.text.TextUtils;

final class bh implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bt f4593a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ bg f4594b;

    bh(bg bgVar, bt btVar) {
        this.f4594b = bgVar;
        this.f4593a = btVar;
    }

    public final void run() {
        String unused = bg.f4589a;
        new StringBuilder("Mock executing ").append(this.f4593a.n()).append(" request: ").append(this.f4593a.f());
        if (this.f4594b.c(this.f4593a)) {
            String b2 = this.f4594b.b();
            this.f4593a.h().a();
            new StringBuilder("mock failure:\n").append(b2);
            this.f4593a.b(b2);
            bg.a(this.f4593a, this.f4594b.c());
        } else {
            String e2 = this.f4593a.e();
            if (TextUtils.isEmpty(e2)) {
                throw new RuntimeException("Empty mock value for " + this.f4593a.h());
            }
            this.f4593a.h().a();
            new StringBuilder("mock response:").append(e2);
            this.f4593a.b(e2);
            bg.a(this.f4593a);
        }
        if (!this.f4593a.a()) {
            try {
                String unused2 = bg.f4589a;
                new StringBuilder("sleep for [").append(this.f4594b.f4592d).append(" ms].");
                Thread.sleep((long) this.f4594b.f4592d);
                String unused3 = bg.f4589a;
                new StringBuilder("end [").append(this.f4594b.f4592d).append(" ms] sleep");
            } catch (InterruptedException e3) {
                this.f4593a.h().a();
            }
            this.f4594b.f4590b.a(this.f4593a);
        }
    }
}
