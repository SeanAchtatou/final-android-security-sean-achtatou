package com.paypal.android.sdk;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class da {

    /* renamed from: a  reason: collision with root package name */
    private static final HashMap f4699a = new ej();

    /* renamed from: b  reason: collision with root package name */
    private static Map f4700b;

    static {
        da.class.getSimpleName();
    }

    public static JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            for (String str : f4700b.keySet()) {
                jSONObject.put(str, f4700b.get(str));
            }
            return jSONObject;
        } catch (JSONException e2) {
            Log.e("paypal.sdk", "Error encoding JSON", e2);
            return null;
        }
    }

    public static void a(a aVar) {
        if (f4700b == null) {
            HashMap hashMap = new HashMap();
            f4700b = hashMap;
            hashMap.put("device_identifier", cd.a(aVar.e()));
            f4700b.put("device_type", "Android");
            f4700b.put("device_name", cd.a(Build.DEVICE));
            f4700b.put("device_model", cd.a(Build.MODEL));
            Map map = f4700b;
            String str = (String) f4699a.get(Integer.valueOf(aVar.b()));
            if (TextUtils.isEmpty(str)) {
                str = "ANDROIDGSM_UNDEFINED";
            }
            map.put("device_key_type", str);
            f4700b.put("device_os", "Android");
            f4700b.put("device_os_version", cd.a(Build.VERSION.RELEASE));
            f4700b.put("is_device_simulator", Boolean.toString(Build.PRODUCT.equals("sdk") || Build.PRODUCT.equals("google_sdk") || Build.FINGERPRINT.contains("generic")));
        }
    }
}
