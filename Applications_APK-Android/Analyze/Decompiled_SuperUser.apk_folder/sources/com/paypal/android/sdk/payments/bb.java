package com.paypal.android.sdk.payments;

import android.content.Context;
import android.content.Intent;
import com.paypal.android.sdk.a;
import com.paypal.android.sdk.cd;
import com.paypal.android.sdk.ch;
import com.paypal.android.sdk.ci;
import com.paypal.android.sdk.cw;
import com.paypal.android.sdk.da;

final class bb implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Context f5190a;

    bb(Context context) {
        this.f5190a = context;
    }

    public final void run() {
        String unused = PayPalService.f5109c;
        Context context = this.f5190a;
        new cf();
        a aVar = new a(context, "AndroidBasePrefs", new cd());
        cw.a(aVar);
        da.a(aVar);
        for (String str : new bz(this)) {
            ci ciVar = new ci(aVar, str);
            ciVar.b();
            ciVar.c();
            ch.b(str);
        }
        c.a(this.f5190a).a(new Intent("com.paypal.android.sdk.clearAllUserData"));
        String unused2 = PayPalService.f5109c;
    }
}
