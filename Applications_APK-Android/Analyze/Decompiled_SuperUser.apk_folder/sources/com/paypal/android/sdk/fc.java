package com.paypal.android.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.SpannableString;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public final class fc {

    /* renamed from: a  reason: collision with root package name */
    private ViewGroup f4879a;

    /* renamed from: b  reason: collision with root package name */
    private ViewGroup f4880b;

    /* renamed from: c  reason: collision with root package name */
    private fe f4881c;

    /* renamed from: d  reason: collision with root package name */
    private LinearLayout f4882d = bw.a(this.f4880b);

    /* renamed from: e  reason: collision with root package name */
    private TextView f4883e;

    /* renamed from: f  reason: collision with root package name */
    private LinearLayout f4884f;

    /* renamed from: g  reason: collision with root package name */
    private TextView f4885g;

    /* renamed from: h  reason: collision with root package name */
    private TextView f4886h;
    private gf i;
    private fu j;
    private fe k;
    private gl l;
    private gl m;
    private gc n;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, int):void
     arg types: [android.widget.TextView, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.content.Context, boolean, int, android.widget.LinearLayout):android.widget.LinearLayout
     arg types: [android.content.Context, int, int, android.widget.LinearLayout]
     candidates:
      com.paypal.android.sdk.bw.a(int, int, int, int):android.widget.RelativeLayout$LayoutParams
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String, int):android.widget.RelativeLayout$LayoutParams
      com.paypal.android.sdk.bw.a(android.content.Context, boolean, int, android.widget.LinearLayout):android.widget.LinearLayout */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, int):void
     arg types: [android.widget.LinearLayout, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, float):void
     arg types: [android.widget.LinearLayout, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void */
    public fc(Context context, boolean z) {
        this.f4880b = bw.a(context);
        LinearLayout b2 = bw.b(this.f4882d);
        this.f4883e = new TextView(context);
        bw.a(this.f4883e, "0dip", "0dip", "0dip", "14dip");
        this.f4883e.setTextSize(24.0f);
        this.f4883e.setTextColor(bv.f4632a);
        b2.addView(this.f4883e);
        bw.a((View) this.f4883e, -2, -2);
        this.f4881c = new fe(context, "description");
        this.f4881c.f4891d.setTypeface(bv.r);
        b2.addView(this.f4881c.f4888a);
        bw.a((View) this.f4881c.f4888a);
        bw.a(b2);
        if (z) {
            this.n = new gc(context);
            b2.addView(this.n.a());
            bw.a(b2);
            this.l = new gl(context);
            b2.addView(this.l.a());
        } else {
            this.i = new gf(context);
            b2.addView(this.i.f4992a);
            bw.a((View) this.i.f4992a);
            bw.a(b2);
            this.k = new fe(context, "00 / 0000");
            b2.addView(this.k.f4888a);
            bw.a((View) this.k.f4888a);
        }
        this.m = new gl(context);
        this.m.a(context, new ff());
        b2.addView(this.m.a());
        this.f4885g = new TextView(context);
        this.f4885g.setId(43002);
        bw.b(this.f4885g);
        b2.addView(this.f4885g);
        bw.a((View) this.f4885g, -1, -2);
        bw.b(this.f4885g, null, "20dip", null, "10dip");
        this.f4885g.setVisibility(8);
        this.f4884f = bw.a(context, true, 43001, b2);
        this.f4886h = new TextView(context);
        bw.a(this.f4886h);
        this.f4886h.setText("init");
        this.f4884f.addView(this.f4886h);
        this.j = new fu(context);
        this.f4882d.addView(this.j.f4943a);
        bw.a((View) this.j.f4943a, -2, -2);
        bw.a((View) this.j.f4943a, 17, 1.0f);
        this.f4879a = this.f4880b;
    }

    public final View a() {
        return this.f4879a;
    }

    public final void a(Context context, ew ewVar) {
        if (this.l != null) {
            this.l.a(context, ewVar);
        }
    }

    public final void a(Context context, ff ffVar) {
        if (this.m != null) {
            this.m.a(context, ffVar);
        }
    }

    public final void a(SpannableString spannableString) {
        if (cd.b(spannableString)) {
            this.f4885g.setText(spannableString);
            this.f4885g.setVisibility(0);
            return;
        }
        this.f4885g.setVisibility(8);
    }

    public final void a(View.OnClickListener onClickListener) {
        if (this.n != null) {
            this.n.a(onClickListener);
        }
    }

    public final void a(String str) {
        this.n.a(str);
    }

    public final void a(String str, Bitmap bitmap, String str2) {
        this.i.f4993b.setText(str);
        this.i.f4994c.setImageBitmap(bitmap);
        this.k.f4890c.setText(str2);
    }

    public final void a(String str, String str2) {
        this.f4881c.f4891d.setText(str);
        this.f4881c.f4890c.setText(str2);
    }

    public final void a(boolean z) {
        if (z) {
            if (cd.c()) {
                this.f4886h.setText(ev.a(fs.AGREE_AND_PAY));
            } else {
                this.f4886h.setText(ev.a(fs.CONFIRM_SEND_PAYMENT));
            }
            this.n.b();
            return;
        }
        this.f4886h.setText(ev.a(fs.CONFIRM_CHARGE_CREDIT_CARD));
        this.i.f4992a.setVisibility(0);
        this.k.f4888a.setVisibility(0);
        this.k.f4891d.setText(ev.a(fs.EXPIRES_ON_DATE));
    }

    public final TextView b() {
        return this.f4883e;
    }

    public final void b(View.OnClickListener onClickListener) {
        this.f4884f.setOnClickListener(onClickListener);
    }

    public final void b(boolean z) {
        if (this.f4884f != null) {
            this.f4884f.setEnabled(z);
        }
    }

    public final void c() {
        this.f4881c.a();
    }

    public final void c(View.OnClickListener onClickListener) {
        if (this.l != null) {
            this.l.a(onClickListener);
        }
    }

    public final TextView d() {
        return this.j.f4944b;
    }

    public final void d(View.OnClickListener onClickListener) {
        if (this.m != null) {
            this.m.a(onClickListener);
        }
    }

    public final View e() {
        if (this.l != null) {
            return this.l.a();
        }
        return null;
    }

    public final View f() {
        if (this.m != null) {
            return this.m.a();
        }
        return null;
    }
}
