package com.paypal.android.sdk;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.List;

public final class dy extends eh {

    /* renamed from: a  reason: collision with root package name */
    public final String f4770a;

    /* renamed from: b  reason: collision with root package name */
    private List f4771b;

    /* renamed from: c  reason: collision with root package name */
    private final String f4772c;

    public dy(bu buVar, x xVar, String str, String str2, String str3, String str4, List list) {
        super(db.ConsentRequest, buVar, xVar, b(str, str2));
        this.f4770a = str3;
        this.f4772c = str4;
        this.f4771b = list;
    }

    public final String b() {
        HashMap hashMap = new HashMap();
        hashMap.put("code", this.f4770a);
        hashMap.put("nonce", this.f4772c);
        hashMap.put("scope", TextUtils.join(" ", this.f4771b));
        return cd.a(hashMap);
    }

    public final void c() {
    }

    public final void d() {
        b(m());
    }

    public final String e() {
        return "{\"code\":\"EOTHbvqh0vwM2ldM2QIXbjVw0hZNuZEJLqdWmfTBLLSvGfqgyy9GKvjGybIxyGMd7gHXCXVtymqFQHS-J-4-Ir6u2LUVVdyLKonwTtdFw9qhBaMb4NZuZHKS0bGxdZlRAB3_Fk8HX2r3z8j03xScx4M\",\"scope\":\"https://uri.paypal.com/services/payments/futurepayments\"}";
    }
}
