package com.paypal.android.sdk.payments;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import java.util.Calendar;
import java.util.Date;

final class ax implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PayPalProfileSharingActivity f5182a;

    ax(PayPalProfileSharingActivity payPalProfileSharingActivity) {
        this.f5182a = payPalProfileSharingActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void
     arg types: [com.paypal.android.sdk.payments.bi, int]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.payments.bi):com.paypal.android.sdk.payments.bi
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.bt):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, boolean):boolean
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.Boolean):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(java.lang.String, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void */
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        new StringBuilder().append(PayPalProfileSharingActivity.class.getSimpleName()).append(".onServiceConnected");
        if (this.f5182a.isFinishing()) {
            new StringBuilder().append(PayPalProfileSharingActivity.class.getSimpleName()).append(".onServiceConnected exit - isFinishing");
            return;
        }
        PayPalService unused = this.f5182a.f5105d = ((bh) iBinder).f5201a;
        if (this.f5182a.f5105d.d() == null) {
            Log.e(PayPalProfileSharingActivity.f5102a, "Service state invalid.  Did you start the PayPalService?");
            this.f5182a.setResult(2);
            this.f5182a.finish();
            return;
        }
        ba baVar = new ba(this.f5182a.getIntent(), this.f5182a.f5105d.d(), true);
        if (!baVar.c()) {
            Log.e(PayPalProfileSharingActivity.f5102a, "Service extras invalid.  Please see the docs.");
            this.f5182a.setResult(2);
            this.f5182a.finish();
        } else if (!baVar.a()) {
            Log.e(PayPalProfileSharingActivity.f5102a, "Extras invalid.  Please see the docs.");
            this.f5182a.setResult(2);
            this.f5182a.finish();
        } else if (this.f5182a.f5105d.i()) {
            ProfileSharingConsentActivity.a(this.f5182a, 1, this.f5182a.f5105d.d());
        } else {
            Calendar instance = Calendar.getInstance();
            instance.add(13, 1);
            Date unused2 = this.f5182a.f5103b = instance.getTime();
            this.f5182a.f5105d.a(PayPalProfileSharingActivity.a(this.f5182a), false);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        PayPalService unused = this.f5182a.f5105d = (PayPalService) null;
        String unused2 = PayPalProfileSharingActivity.f5102a;
    }
}
