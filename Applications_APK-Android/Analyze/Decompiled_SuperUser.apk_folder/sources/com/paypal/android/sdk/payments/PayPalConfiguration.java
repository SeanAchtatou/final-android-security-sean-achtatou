package com.paypal.android.sdk.payments;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import com.paypal.android.sdk.an;
import com.paypal.android.sdk.cd;

public final class PayPalConfiguration implements Parcelable {
    public static final Parcelable.Creator CREATOR = new am();

    /* renamed from: a  reason: collision with root package name */
    private static final String f5062a = PayPalConfiguration.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private String f5063b;

    /* renamed from: c  reason: collision with root package name */
    private String f5064c;

    /* renamed from: d  reason: collision with root package name */
    private String f5065d;

    /* renamed from: e  reason: collision with root package name */
    private String f5066e;

    /* renamed from: f  reason: collision with root package name */
    private String f5067f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f5068g;

    /* renamed from: h  reason: collision with root package name */
    private String f5069h;
    private String i;
    private boolean j;
    private String k;
    private String l;
    private Uri m;
    private Uri n;
    private boolean o;

    public PayPalConfiguration() {
        this.j = cf.d();
        this.o = true;
    }

    private PayPalConfiguration(Parcel parcel) {
        boolean z = true;
        this.j = cf.d();
        this.o = true;
        this.f5064c = parcel.readString();
        this.f5063b = parcel.readString();
        this.f5065d = parcel.readString();
        this.f5066e = parcel.readString();
        this.f5067f = parcel.readString();
        this.f5068g = parcel.readByte() == 1;
        this.f5069h = parcel.readString();
        this.i = parcel.readString();
        this.j = parcel.readByte() == 1;
        this.k = parcel.readString();
        this.l = parcel.readString();
        this.m = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.n = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.o = parcel.readByte() != 1 ? false : z;
    }

    /* synthetic */ PayPalConfiguration(Parcel parcel, byte b2) {
        this(parcel);
    }

    private static void a(boolean z, String str) {
        if (!z) {
            Log.e(f5062a, str + " is invalid.  Please see the docs.");
        }
    }

    public final PayPalConfiguration a(String str) {
        this.f5064c = str;
        return this;
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return this.f5063b;
    }

    public final PayPalConfiguration b(String str) {
        this.k = str;
        return this;
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        if (TextUtils.isEmpty(this.f5064c)) {
            this.f5064c = "live";
            Log.w("paypal.sdk", "defaulting to production environment");
        }
        return this.f5064c;
    }

    /* access modifiers changed from: package-private */
    public final String c() {
        return this.f5065d;
    }

    /* access modifiers changed from: package-private */
    public final String d() {
        return this.f5066e;
    }

    public final int describeContents() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final String e() {
        return this.f5067f;
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return this.f5068g;
    }

    /* access modifiers changed from: package-private */
    public final String g() {
        return this.f5069h;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public final boolean i() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final boolean j() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final String k() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public final String l() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public final Uri m() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final Uri n() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public final boolean o() {
        boolean z;
        boolean a2 = cd.a(f5062a, b(), "environment");
        a(a2, "environment");
        if (!a2) {
            z = false;
        } else if (an.a(b())) {
            z = true;
        } else {
            z = cd.a(f5062a, this.k, "clientId");
            a(z, "clientId");
        }
        return a2 && z;
    }

    public final String toString() {
        return String.format(PayPalConfiguration.class.getSimpleName() + ": {environment:%s, client_id:%s, languageOrLocale:%s}", this.f5064c, this.k, this.f5063b);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int i3 = 1;
        parcel.writeString(this.f5064c);
        parcel.writeString(this.f5063b);
        parcel.writeString(this.f5065d);
        parcel.writeString(this.f5066e);
        parcel.writeString(this.f5067f);
        parcel.writeByte((byte) (this.f5068g ? 1 : 0));
        parcel.writeString(this.f5069h);
        parcel.writeString(this.i);
        parcel.writeByte((byte) (this.j ? 1 : 0));
        parcel.writeString(this.k);
        parcel.writeString(this.l);
        parcel.writeParcelable(this.m, 0);
        parcel.writeParcelable(this.n, 0);
        if (!this.o) {
            i3 = 0;
        }
        parcel.writeByte((byte) i3);
    }
}
