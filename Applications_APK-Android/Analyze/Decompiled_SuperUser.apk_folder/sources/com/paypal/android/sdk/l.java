package com.paypal.android.sdk;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import java.util.List;

public class l extends k {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5038a = l.class.getSimpleName();

    protected static Intent a(String str, String str2) {
        Intent intent = new Intent(str);
        intent.setComponent(ComponentName.unflattenFromString(str2));
        intent.setPackage("com.paypal.android.p2pmobile");
        return intent;
    }

    public final boolean a(Context context, String str, String str2) {
        boolean z = false;
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(a(str, str2), 0);
        if (queryIntentActivities != null && queryIntentActivities.size() > 0) {
            z = true;
        }
        if (!z) {
            new StringBuilder("PayPal wallet app will not accept intent to: [action:").append(str).append(", class:").append(str2).append("]");
        }
        return z;
    }

    public final boolean a(Context context, boolean z) {
        return a(context, z, "com.paypal.android.p2pmobile", "O=Paypal", "O=Paypal", 34172764);
    }
}
