package com.paypal.android.sdk;

import io.fabric.sdk.android.services.b.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class fb {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4872a = fb.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static final Map f4873b = new HashMap();

    /* renamed from: c  reason: collision with root package name */
    private static final Set f4874c = new HashSet();

    /* renamed from: g  reason: collision with root package name */
    private static /* synthetic */ boolean f4875g = (!fb.class.desiredAssertionStatus());

    /* renamed from: d  reason: collision with root package name */
    private Map f4876d = new LinkedHashMap();

    /* renamed from: e  reason: collision with root package name */
    private fi f4877e;

    /* renamed from: f  reason: collision with root package name */
    private Class f4878f;

    static {
        f4873b.put("zh_CN", "zh-Hans");
        f4873b.put("zh_TW", "zh-Hant_TW");
        f4873b.put("zh_HK", "zh-Hant");
        f4873b.put("en_UK", "en_GB");
        f4873b.put("en_IE", "en_GB");
        f4873b.put("iw_IL", "he");
        f4873b.put("no", "nb");
        f4874c.add("he");
        f4874c.add("ar");
    }

    public fb(Class cls, List list) {
        this.f4878f = cls;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            fi fiVar = (fi) it.next();
            String a2 = fiVar.a();
            if (a2 == null) {
                throw new RuntimeException("Null localeName");
            } else if (this.f4876d.containsKey(a2)) {
                throw new RuntimeException("Locale " + a2 + " already added");
            } else {
                this.f4876d.put(a2, fiVar);
                b(a2);
            }
        }
        a((String) null);
    }

    private void b(String str) {
        fi fiVar = (fi) this.f4876d.get(str);
        ArrayList arrayList = new ArrayList();
        new StringBuilder("Checking locale ").append(str);
        for (Enum enumR : (Enum[]) this.f4878f.getEnumConstants()) {
            String str2 = "[" + str + "," + enumR + "]";
            if (fiVar.a(enumR, null) == null) {
                arrayList.add("Missing " + str2);
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    private fi c(String str) {
        fi fiVar = null;
        if (str == null || str.length() < 2) {
            return null;
        }
        if (f4873b.containsKey(str)) {
            String str2 = (String) f4873b.get(str);
            new StringBuilder("Overriding locale specifier ").append(str).append(" with ").append(str2);
            fiVar = (fi) this.f4876d.get(str2);
        }
        if (fiVar == null) {
            fiVar = (fi) this.f4876d.get(str.contains(b.ROLL_OVER_FILE_NAME_SEPARATOR) ? str : str + b.ROLL_OVER_FILE_NAME_SEPARATOR + Locale.getDefault().getCountry());
        }
        if (fiVar == null) {
            fiVar = (fi) this.f4876d.get(str);
        }
        if (fiVar != null) {
            return fiVar;
        }
        return (fi) this.f4876d.get(str.substring(0, 2));
    }

    public final String a() {
        return this.f4877e.a();
    }

    public final String a(Enum enumR) {
        fi fiVar = this.f4877e;
        String upperCase = Locale.getDefault().getCountry().toUpperCase(Locale.US);
        String a2 = fiVar.a(enumR, upperCase);
        if (a2 == null) {
            new StringBuilder("Missing localized string for [").append(this.f4877e.a()).append(",Key.").append(enumR.toString()).append("]");
            a2 = ((fi) this.f4876d.get("en")).a(enumR, upperCase);
        }
        if (a2 != null) {
            return a2;
        }
        new StringBuilder("Missing localized string for [en,Key.").append(enumR.toString()).append("], so defaulting to keyname");
        return enumR.toString();
    }

    public final String a(String str, Enum enumR) {
        String a2 = this.f4877e.a(str);
        if (a2 != null) {
            return a2;
        }
        return String.format(a(enumR), str);
    }

    public final void a(String str) {
        fi fiVar = null;
        new StringBuilder("setLanguage(").append(str).append(")");
        this.f4877e = null;
        if (str != null) {
            fiVar = c(str);
        }
        if (fiVar == null) {
            String locale = Locale.getDefault().toString();
            new StringBuilder().append(str).append(" not found.  Attempting to look for ").append(locale);
            fiVar = c(locale);
        }
        if (fiVar == null) {
            fiVar = (fi) this.f4876d.get("en");
        }
        if (f4875g || fiVar != null) {
            this.f4877e = fiVar;
            if (f4875g || this.f4877e != null) {
                new StringBuilder("setting locale to:").append(this.f4877e.a());
                return;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }
}
