package com.paypal.android.sdk;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import java.util.Collection;
import java.util.HashSet;

public final class gm {

    /* renamed from: a  reason: collision with root package name */
    private final Context f5023a;

    public gm(Context context) {
        this.f5023a = context;
    }

    public final void a(Collection collection) {
        try {
            HashSet hashSet = new HashSet(collection);
            ActivityInfo[] activityInfoArr = this.f5023a.getPackageManager().getPackageInfo(this.f5023a.getPackageName(), 1).activities;
            if (activityInfoArr != null) {
                for (ActivityInfo activityInfo : activityInfoArr) {
                    hashSet.remove(activityInfo.name);
                }
            }
            if (!hashSet.isEmpty()) {
                throw new RuntimeException("Missing required activities in manifest:" + hashSet);
            }
        } catch (PackageManager.NameNotFoundException e2) {
            throw new RuntimeException("Exception loading manifest" + e2.getMessage());
        }
    }
}
