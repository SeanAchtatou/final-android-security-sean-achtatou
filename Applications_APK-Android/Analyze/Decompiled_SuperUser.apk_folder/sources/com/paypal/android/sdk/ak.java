package com.paypal.android.sdk;

import com.google.android.gms.common.Scopes;
import java.util.Collection;

public enum ak {
    FUTURE_PAYMENTS("https://uri.paypal.com/services/payments/futurepayments", false),
    PROFILE(Scopes.PROFILE, true),
    PAYPAL_ATTRIBUTES("https://uri.paypal.com/services/paypalattributes", true),
    OPENID("openid", true),
    EMAIL(Scopes.EMAIL, true),
    ADDRESS("address", true),
    PHONE("phone", true);
    

    /* renamed from: h  reason: collision with root package name */
    public static final Collection f4557h = new al();
    public static final Collection i = new am();
    private String j;
    private boolean k;

    private ak(String str, boolean z) {
        this.j = str;
        this.k = z;
    }

    public final String a() {
        return this.j;
    }
}
