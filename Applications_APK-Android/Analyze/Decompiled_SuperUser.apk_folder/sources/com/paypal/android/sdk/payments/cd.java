package com.paypal.android.sdk.payments;

import android.app.AlertDialog;
import android.view.View;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.ex;
import com.paypal.android.sdk.fs;
import java.util.ArrayList;

final class cd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ex f5229a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ArrayList f5230b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ PaymentConfirmActivity f5231c;

    cd(PaymentConfirmActivity paymentConfirmActivity, ex exVar, ArrayList arrayList) {
        this.f5231c = paymentConfirmActivity;
        this.f5229a = exVar;
        this.f5230b = arrayList;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(ev.a(fs.PREFERRED_PAYMENT_METHOD)).setAdapter(this.f5229a, new ce(this));
        builder.create().show();
    }
}
