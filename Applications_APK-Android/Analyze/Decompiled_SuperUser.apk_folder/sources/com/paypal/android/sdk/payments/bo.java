package com.paypal.android.sdk.payments;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

final class bo implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PaymentActivity f5208a;

    bo(PaymentActivity paymentActivity) {
        this.f5208a = paymentActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        new StringBuilder().append(PaymentActivity.f5116a).append(".onServiceConnected");
        if (this.f5208a.isFinishing()) {
            new StringBuilder().append(PaymentActivity.f5116a).append(".onServiceConnected exit - isFinishing");
            return;
        }
        PayPalService unused = this.f5208a.f5119d = ((bh) iBinder).f5201a;
        if (this.f5208a.f5119d.a(new bp(this))) {
            PaymentActivity.c(this.f5208a);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        PayPalService unused = this.f5208a.f5119d = (PayPalService) null;
        String unused2 = PaymentActivity.f5116a;
    }
}
