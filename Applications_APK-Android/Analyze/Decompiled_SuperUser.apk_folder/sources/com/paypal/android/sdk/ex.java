package com.paypal.android.sdk;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;

public final class ex extends ArrayAdapter {

    /* renamed from: a  reason: collision with root package name */
    private int f4852a;

    public ex(Context context, ArrayList arrayList, int i) {
        super(context, 0, arrayList);
        this.f4852a = i;
    }

    private static void a(Context context, RelativeLayout relativeLayout, ew ewVar) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setId(2304);
        int i = 2304;
        linearLayout.setOrientation(0);
        RelativeLayout.LayoutParams a2 = bw.a(-2, -2, 1, 2301);
        a2.addRule(0, 2303);
        relativeLayout.addView(linearLayout, a2);
        TextView textView = new TextView(context);
        textView.setId(2302);
        bw.b(textView, 83);
        linearLayout.addView(textView);
        bw.b(textView, "6dip", null, null, null);
        TextView textView2 = new TextView(context);
        bw.d(textView2, 83);
        linearLayout.addView(textView2);
        bw.b(textView2, "6dip", null, "6dip", null);
        if (ewVar.e()) {
            TextView textView3 = new TextView(context);
            textView3.setId(2306);
            i = 2306;
            bw.c(textView3, 83);
            RelativeLayout.LayoutParams a3 = bw.a(-2, -2, 1, 2301);
            a3.addRule(0, 2303);
            a3.addRule(3, 2304);
            relativeLayout.addView(textView3, a3);
            bw.b(textView3, "6dip", null, null, null);
            textView3.setText(ev.a(fs.PAY_AFTER_DELIVERY));
        }
        int i2 = i;
        int i3 = 2500;
        Iterator it = ewVar.f().iterator();
        int i4 = 2400;
        while (true) {
            int i5 = i3;
            if (it.hasNext()) {
                ez ezVar = (ez) it.next();
                TextView textView4 = new TextView(context);
                textView4.setId(i4);
                textView4.setText(ezVar.a() + " " + ezVar.b());
                RelativeLayout.LayoutParams a4 = bw.a(-2, -2, 1, 2301);
                a4.addRule(0, i5);
                if (textView4.getId() == 2400) {
                    a4.addRule(3, i2);
                } else {
                    a4.addRule(3, textView4.getId() - 1);
                }
                relativeLayout.addView(textView4, a4);
                bw.e(textView4, 83);
                bw.b(textView4, "6dip", null, null, null);
                textView4.setEllipsize(TextUtils.TruncateAt.END);
                TextView textView5 = new TextView(context);
                textView5.setId(i5);
                textView5.setText(ezVar.c());
                RelativeLayout.LayoutParams a5 = bw.a(-2, -2, 0, 2303);
                a5.addRule(8, i4);
                relativeLayout.addView(textView5, a5);
                bw.d(textView5, 85);
                bw.b(textView5, "6dip", null, "6dip", null);
                i4++;
                i3 = i5 + 1;
            } else {
                textView.setText(ewVar.c());
                textView.setEllipsize(TextUtils.TruncateAt.END);
                textView2.setText(ewVar.d());
                textView2.setEllipsize(TextUtils.TruncateAt.END);
                return;
            }
        }
    }

    public final void a(int i) {
        this.f4852a = i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ew ewVar = (ew) getItem(i);
        LinearLayout linearLayout = new LinearLayout(viewGroup.getContext());
        RelativeLayout relativeLayout = new RelativeLayout(viewGroup.getContext());
        linearLayout.addView(relativeLayout);
        bw.a(relativeLayout, null, "6dip", null, "6dip");
        ImageView a2 = bw.a(viewGroup.getContext(), ewVar.a(), "");
        a2.setId(2301);
        RelativeLayout.LayoutParams a3 = bw.a(viewGroup.getContext(), "30dip", "30dip", 9);
        a3.addRule(10);
        relativeLayout.addView(a2, a3);
        bw.a(a2, "4dip", null, null, null);
        ImageView a4 = bw.a(viewGroup.getContext(), "iVBORw0KGgoAAAANSUhEUgAAAGQAAABZCAYAAADIBoEnAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABb9JREFUeNrsnE1oXFUUx+8MgyD9YHDRLrow1S6LJgsV3JgsgnSXLtwJTTcqcWGL4La2SyHEjZKupkI2rpp9F2YVIQunJAGjNIkRgqRCLUwkUCLjOcz/kuc4mfdxP96d+86BwxtC5r157zfnf865H1NXYkFZXR5BMet2X4zh+LIAKRdEk7xFL3fp+BEd36Xjq/JkyoExSf4XeRd+SD5H/j75JYkQvzAW6PADeTPx5zPkX5G/Tn7VBhQBkk2iGMStU/7FKhQBMhzGOKJiMuVfrUERIOkwxjO+RUN5wwSKABkOo5nzrRrKNKBcESDlwUja5+QfsoTR+a7meWNDEFiHoY2BXCS/T+c9T8e1Wu2lY4mQcmBom4aEXUEDeU6AlAdD22vk35K/Q/52WrKvCQynMPptCb5PvjVIwmoVh9HMWdrasHXyefIn5BsEpSNAToC0PcPQ9jegrCJS9iqfQzBiO17S5RnEIfkR+XHly16C8SUdZku49CPkkJ/JtykynlW+DyEYM3S44/myOm/8BhD70hieVFQtj5c84MYQErXNQNKaw0aFYDQBo+npksuQp9/JNwnEkQyd/NcWPCXxA8jTen8FJUBOomPWUxJ/BIn6Y1CPIUD85Q3uKxYB5NQuvPJAEnnDtUTdI/8FIPZNThZ7hLjOGz8iX/xJ3i4iUZUBgn7DZd5YhkwxhLWiElUJIFhV6FKq5nW+IBCbNk8ca4S46jc4ed9FSbtHMLZsX6ARYXTw+qlJRzC+QMe9aZq8T7NaZDBYqtoOomMHMuUURowR0nIEgyPjOZJ3x+UN1COKDhdS5RVGNJLlSKq8w4gpQloxwIgCiAOpKg3GyEsWxqp2LUZHqTBiiJBWTDCMy97EN5THdb5XvZHPvUGT9w6ig2VqxtLpDgCDV4JslAXDWLKwsyip33raUg9FHzuC0URVNTYqHbhzycKeu/5kyt/Y78g/IH/P4e7UWzHCKBwhmBJNG03V05k8yd+2FS3oOXYt3T/DcDZQ6CWH5JgS5aX4vOeOx4DO87JNS7nF1rC6XoiwHwqM3JKVWCme1XgpPu+PuEb+lqmEYdLJRs9xHxH8zPZ8hrcIMVjXxPvueIvXWfKHvGmlyEPA9Rcs3DODeKh6M33t0Or4eo6HYbps/2OAuUTnmyDPK5c2Erle0nlsM6+VIVm2VopPA8oF1dtN1Mj4hWAQn1lo/O7h9VrWlYTBAcGy/RmL19RQzuWAcsewI9f7MQ5R3nZUoFbPAGPWwXUzQ0FHbvoZ7qLX2Auh1ygExMPyyySUiZToMK2o1lFRbanArW7Q+NmCcp38lUEb7C2UubqiOgqxosoExCOMZPU1jeqr/6coTMrcHURHsBVV1gh5U/V2+vi0T9DV809RXMAXw6TMTSbxrZCTeL/VUrryG6iwxjx8Fh4C/1T15iN+he4Xray4vF1VAY1RGSd1upHH5LfJL0PnVxx/lotI8g080KIwlgGjM2owhkbIkBL0huPqawlfgDMF88Yc8sZqqM2fNSB9nXNLuVmyadL8zUH6OIk/VSNohSao6GZ5N+kUvWR/HMi9zKuTKeSRhFEYSALMCjk3dbeRjMuykc4b1oAkwHxNh8t4ML6N88ai7jfUiJu1ZUAE5Tn5dSRkX9Gi92so9BtHAuT/YJYRLSsePv8i8sbT0AcNSwOSiJYp5BZXxhsueayKo2JDRWJOVy4it0w4kDA9NMK2OSrjVKUD0R0/JMxmecx5g8eptn2skowKSELCOFIeWCpx11HiPlGRmdfF1vQAbxrmFU7gS3i9oSI076vfkVduGnTjWqo6AsQelAcFkv1SzFJVKpBEsp/KCGUndqkqHUhOKLrEjVaqggCSEQpXVduxS1UwQFKgRF9VBQlkCJToq6pggQyA8g35T1WRKm3B/dYJQ+l2X/AQPsP5R4mJiWSJCRABIiZABIiYABEgYgJEgIgJEDEBIkDE0u1fAQYA3p2Buu6CTa4AAAAASUVORK5CYII=", "checked");
        a4.setId(2303);
        RelativeLayout.LayoutParams a5 = bw.a(viewGroup.getContext(), "20dip", "20dip", 11);
        a5.addRule(10);
        relativeLayout.addView(a4, a5);
        a4.setColorFilter(bv.f4633b);
        bw.a(a4, null, null, "8dip", null);
        if (i != this.f4852a) {
            a4.setVisibility(4);
        }
        if (ewVar.g()) {
            Context context = viewGroup.getContext();
            TextView textView = new TextView(context);
            textView.setId(2302);
            int i2 = 2302;
            bw.b(textView, 83);
            RelativeLayout.LayoutParams a6 = bw.a(-2, -2, 1, 2301);
            a6.addRule(0, 2303);
            relativeLayout.addView(textView, a6);
            bw.b(textView, "6dip", null, null, null);
            if (ewVar.e()) {
                TextView textView2 = new TextView(context);
                textView2.setId(2306);
                i2 = 2306;
                bw.c(textView2, 83);
                RelativeLayout.LayoutParams a7 = bw.a(-2, -2, 1, 2301);
                a7.addRule(0, 2303);
                a7.addRule(3, 2302);
                relativeLayout.addView(textView2, a7);
                bw.b(textView2, "6dip", null, null, null);
                textView2.setText(ev.a(fs.PAY_AFTER_DELIVERY));
            }
            TextView textView3 = new TextView(context);
            textView3.setId(2305);
            bw.d(textView3, 83);
            RelativeLayout.LayoutParams a8 = bw.a(-2, -2, 1, 2301);
            a8.addRule(3, i2);
            relativeLayout.addView(textView3, a8);
            bw.b(textView3, "6dip", null, null, null);
            TextView textView4 = new TextView(context);
            RelativeLayout.LayoutParams a9 = bw.a(-2, -2, 0, 2303);
            a9.addRule(8, 2305);
            relativeLayout.addView(textView4, a9);
            bw.d(textView4, 85);
            bw.b(textView4, null, null, "6dip", null);
            textView.setText(ewVar.c());
            textView3.setText(ewVar.d());
            textView4.setText(ewVar.f().a(0).c());
        } else {
            a(viewGroup.getContext(), relativeLayout, ewVar);
        }
        return linearLayout;
    }
}
