package com.paypal.android.sdk.payments;

import com.paypal.android.sdk.ak;
import java.util.HashMap;

final class o extends HashMap {
    o() {
        put(y.openid_connect, ak.OPENID);
        put(y.oauth_fullname, ak.PROFILE);
        put(y.oauth_gender, ak.PROFILE);
        put(y.oauth_date_of_birth, ak.PROFILE);
        put(y.oauth_timezone, ak.PROFILE);
        put(y.oauth_locale, ak.PROFILE);
        put(y.oauth_language, ak.PROFILE);
        put(y.oauth_age_range, ak.PAYPAL_ATTRIBUTES);
        put(y.oauth_account_verified, ak.PAYPAL_ATTRIBUTES);
        put(y.oauth_account_type, ak.PAYPAL_ATTRIBUTES);
        put(y.oauth_account_creation_date, ak.PAYPAL_ATTRIBUTES);
        put(y.oauth_email, ak.EMAIL);
        put(y.oauth_street_address1, ak.ADDRESS);
        put(y.oauth_street_address2, ak.ADDRESS);
        put(y.oauth_city, ak.ADDRESS);
        put(y.oauth_state, ak.ADDRESS);
        put(y.oauth_country, ak.ADDRESS);
        put(y.oauth_zip, ak.ADDRESS);
        put(y.oauth_phone_number, ak.PHONE);
    }
}
