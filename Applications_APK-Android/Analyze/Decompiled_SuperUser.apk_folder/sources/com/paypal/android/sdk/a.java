package com.paypal.android.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.UUID;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4520a = a.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final Context f4521b;

    /* renamed from: c  reason: collision with root package name */
    private final String f4522c;

    /* renamed from: d  reason: collision with root package name */
    private final az f4523d;

    public a(Context context, String str, cd cdVar) {
        if (context == null) {
            throw new NullPointerException("context == null");
        } else if (str == null) {
            throw new NullPointerException("prefs == null");
        } else {
            this.f4521b = context;
            this.f4522c = str;
            this.f4523d = cdVar.a(this);
        }
    }

    public final String a(String str) {
        return this.f4523d.b(this.f4521b.getSharedPreferences(this.f4522c, 0).getString(str, null));
    }

    public final void a(String str, String str2) {
        SharedPreferences.Editor edit = this.f4521b.getSharedPreferences(this.f4522c, 0).edit();
        edit.putString(str, this.f4523d.a(str2));
        edit.commit();
    }

    public final boolean a() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.f4521b.getSystemService("connectivity");
        if (connectivityManager == null) {
            Log.w("paypal.sdk", "Unable to retrieve Context.CONNECTIVITY_SERVICE. Ignoring.");
            return true;
        } else if (connectivityManager.getAllNetworkInfo() == null) {
            Log.w("paypal.sdk", "ConnectivityManager.getAllNetworkInfo() returned null. Ignoring.");
            return true;
        } else {
            int i = 0;
            for (NetworkInfo isConnectedOrConnecting : connectivityManager.getAllNetworkInfo()) {
                if (isConnectedOrConnecting.isConnectedOrConnecting()) {
                    i++;
                }
            }
            return i > 0;
        }
    }

    public final int b() {
        return ((TelephonyManager) this.f4521b.getSystemService("phone")).getPhoneType();
    }

    public final String b(String str) {
        return this.f4523d.a(str);
    }

    public final String c() {
        try {
            PackageManager packageManager = this.f4521b.getPackageManager();
            return packageManager.getPackageInfo(this.f4521b.getPackageName(), 0).applicationInfo.loadLabel(packageManager).toString();
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    public final String c(String str) {
        return this.f4523d.b(str);
    }

    public final String d() {
        try {
            return ((TelephonyManager) this.f4521b.getSystemService("phone")).getSimOperatorName();
        } catch (SecurityException e2) {
            e2.toString();
            return null;
        }
    }

    public final String e() {
        String string = this.f4521b.getSharedPreferences(this.f4522c, 0).getString("InstallationGUID", null);
        if (string != null) {
            return string;
        }
        String uuid = UUID.randomUUID().toString();
        SharedPreferences.Editor edit = this.f4521b.getSharedPreferences(this.f4522c, 0).edit();
        edit.putString("InstallationGUID", uuid);
        edit.commit();
        return uuid;
    }

    public final Context f() {
        return this.f4521b;
    }
}
