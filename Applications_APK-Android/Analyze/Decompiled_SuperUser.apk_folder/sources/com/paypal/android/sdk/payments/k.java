package com.paypal.android.sdk.payments;

import android.text.Editable;
import android.text.TextWatcher;

final class k implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ LoginActivity f5316a;

    k(LoginActivity loginActivity) {
        this.f5316a = loginActivity;
    }

    public final void afterTextChanged(Editable editable) {
        this.f5316a.h();
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
