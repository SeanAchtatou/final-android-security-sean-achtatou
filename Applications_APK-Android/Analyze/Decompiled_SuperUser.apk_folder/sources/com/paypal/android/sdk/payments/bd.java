package com.paypal.android.sdk.payments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

final class bd extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PayPalService f5191a;

    bd(PayPalService payPalService) {
        this.f5191a = payPalService;
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("com.paypal.android.sdk.clearAllUserData")) {
            this.f5191a.g();
            Log.w("paypal.sdk", "active service user data cleared");
        }
    }
}
