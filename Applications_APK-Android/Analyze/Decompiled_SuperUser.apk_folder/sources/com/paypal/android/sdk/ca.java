package com.paypal.android.sdk;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class ca {

    /* renamed from: a  reason: collision with root package name */
    private static final int f4659a;

    /* renamed from: b  reason: collision with root package name */
    private static int f4660b;

    /* renamed from: c  reason: collision with root package name */
    private static int f4661c = ((f4659a << 1) + 1);

    static {
        new ca();
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        f4659a = availableProcessors;
        f4660b = availableProcessors + 1;
    }

    private ca() {
        new cc((byte) 0);
    }

    public static ThreadPoolExecutor a() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(f4660b, f4661c, 1, TimeUnit.SECONDS, new LinkedBlockingQueue());
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        return threadPoolExecutor;
    }
}
