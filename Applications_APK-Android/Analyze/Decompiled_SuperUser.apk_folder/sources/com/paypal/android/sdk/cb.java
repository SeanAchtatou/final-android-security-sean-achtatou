package com.paypal.android.sdk;

import java.util.ArrayList;
import java.util.List;

public class cb implements br {

    /* renamed from: a  reason: collision with root package name */
    final db f4662a;

    public cb(db dbVar) {
        this.f4662a = dbVar;
    }

    public static List d() {
        ArrayList arrayList = new ArrayList();
        for (db cbVar : db.values()) {
            arrayList.add(new cb(cbVar));
        }
        return arrayList;
    }

    public String a() {
        return this.f4662a.name();
    }

    public bz b() {
        return this.f4662a.a();
    }

    public String c() {
        return this.f4662a.b();
    }
}
