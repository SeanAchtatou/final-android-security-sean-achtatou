package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.paypal.android.sdk.ak;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class PayPalOAuthScopes implements Parcelable {
    public static final Parcelable.Creator CREATOR = new at();

    /* renamed from: a  reason: collision with root package name */
    public static final String f5082a = ak.FUTURE_PAYMENTS.a();

    /* renamed from: b  reason: collision with root package name */
    public static final String f5083b = ak.PROFILE.a();

    /* renamed from: c  reason: collision with root package name */
    public static final String f5084c = ak.PAYPAL_ATTRIBUTES.a();

    /* renamed from: d  reason: collision with root package name */
    public static final String f5085d = ak.EMAIL.a();

    /* renamed from: e  reason: collision with root package name */
    public static final String f5086e = ak.ADDRESS.a();

    /* renamed from: f  reason: collision with root package name */
    public static final String f5087f = ak.PHONE.a();

    /* renamed from: g  reason: collision with root package name */
    public static final String f5088g = ak.OPENID.a();

    /* renamed from: h  reason: collision with root package name */
    private final List f5089h;

    public PayPalOAuthScopes() {
        this.f5089h = new ArrayList();
    }

    private PayPalOAuthScopes(Parcel parcel) {
        this.f5089h = new ArrayList();
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            this.f5089h.add(parcel.readString());
        }
    }

    /* synthetic */ PayPalOAuthScopes(Parcel parcel, byte b2) {
        this(parcel);
    }

    public PayPalOAuthScopes(Set set) {
        this();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            this.f5089h.add((String) it.next());
        }
    }

    /* access modifiers changed from: package-private */
    public final List a() {
        return this.f5089h;
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        return TextUtils.join(" ", this.f5089h);
    }

    public final int describeContents() {
        return 0;
    }

    public final String toString() {
        return String.format(PayPalOAuthScopes.class.getSimpleName() + ": {%s}", this.f5089h);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f5089h.size());
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f5089h.size()) {
                parcel.writeString((String) this.f5089h.get(i3));
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }
}
