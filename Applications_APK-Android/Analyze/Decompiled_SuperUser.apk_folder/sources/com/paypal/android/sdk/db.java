package com.paypal.android.sdk;

public enum db {
    FptiRequest(bz.POST, null),
    PreAuthRequest(bz.POST, "oauth2/token"),
    LoginRequest(bz.POST, "oauth2/login"),
    LoginChallengeRequest(bz.POST, "oauth2/login/challenge"),
    ConsentRequest(bz.POST, "oauth2/consent"),
    CreditCardPaymentRequest(bz.POST, "payments/payment"),
    PayPalPaymentRequest(bz.POST, "payments/payment"),
    CreateSfoPaymentRequest(bz.POST, "orchestration/msdk-create-sfo-payment"),
    ApproveAndExecuteSfoPaymentRequest(bz.POST, "orchestration/msdk-approve-and-execute-sfo-payment"),
    TokenizeCreditCardRequest(bz.POST, "vault/credit-card"),
    DeleteCreditCardRequest(bz.DELETE, "vault/credit-card"),
    GetAppInfoRequest(bz.GET, "apis/applications");
    
    private bz m;
    private String n;

    private db(bz bzVar, String str) {
        this.m = bzVar;
        this.n = str;
    }

    /* access modifiers changed from: package-private */
    public final bz a() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        return this.n;
    }
}
