package com.paypal.android.sdk;

import java.util.Map;

public final class as {

    /* renamed from: a  reason: collision with root package name */
    public final String f4569a;

    /* renamed from: b  reason: collision with root package name */
    public final Map f4570b;

    public as(String str, Map map) {
        this.f4570b = map;
        this.f4569a = str;
    }
}
