package com.paypal.android.sdk;

import android.content.Context;
import android.util.Log;
import java.util.Collections;
import java.util.concurrent.ExecutorService;

public final class cs {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4684a = cs.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static by f4685b;

    public static final synchronized String a(ExecutorService executorService, Context context, String str, String str2, String str3) {
        String b2;
        synchronized (cs.class) {
            if (f4685b == null) {
                try {
                    f4685b = new by();
                    b2 = f4685b.a(context, str, str2, Collections.emptyMap());
                    executorService.submit(new dv());
                    new StringBuilder("Init risk component: ").append(f4685b.c()).append(" returning new clientMetadataId:").append(b2);
                } catch (Throwable th) {
                    Log.e("paypal.sdk", "An internal component failed to initialize: " + th.getMessage());
                    b2 = null;
                }
            } else {
                b2 = f4685b.b();
                new StringBuilder("risk component already initialized, returning new clientMetadataId:").append(b2);
            }
        }
        return b2;
    }
}
