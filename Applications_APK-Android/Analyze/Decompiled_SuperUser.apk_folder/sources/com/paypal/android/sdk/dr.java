package com.paypal.android.sdk;

public enum dr {
    AMEX,
    DINERSCLUB,
    DISCOVER,
    JCB,
    MASTERCARD,
    VISA,
    MAESTRO,
    UNKNOWN,
    INSUFFICIENT_DIGITS;

    public static dr a(String str) {
        if (str == null) {
            return UNKNOWN;
        }
        for (dr drVar : values()) {
            if (drVar != UNKNOWN && drVar != INSUFFICIENT_DIGITS && str.equalsIgnoreCase(drVar.toString())) {
                return drVar;
            }
        }
        return UNKNOWN;
    }
}
