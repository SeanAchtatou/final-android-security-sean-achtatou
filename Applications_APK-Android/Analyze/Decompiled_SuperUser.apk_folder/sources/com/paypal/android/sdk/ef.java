package com.paypal.android.sdk;

import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public final class ef extends eh {

    /* renamed from: a  reason: collision with root package name */
    private final String f4798a;

    /* renamed from: b  reason: collision with root package name */
    private final String f4799b;

    public ef(bu buVar, x xVar, String str, String str2, String str3, String str4) {
        super(db.LoginChallengeRequest, buVar, xVar, b(str, str2));
        this.f4798a = str3;
        this.f4799b = str4;
    }

    public final String b() {
        HashMap hashMap = new HashMap();
        hashMap.put("authn_schemes", "2fa_pre_login");
        hashMap.put("nonce", this.f4798a);
        JSONObject jSONObject = new JSONObject();
        jSONObject.accumulate("authn_scheme", "security_key_sms_token");
        jSONObject.accumulate("token_identifier", this.f4799b);
        hashMap.put("2fa_token_identifiers", a(jSONObject));
        return cd.a(hashMap);
    }

    public final void c() {
        JSONObject m = m();
        try {
            this.f4807g = m.getString("nonce");
        } catch (JSONException e2) {
            b(m);
        }
    }

    public final void d() {
        b(m());
    }

    public final String e() {
        return "{\"nonce\": \"mock-login-challenge-nonce\"}";
    }
}
