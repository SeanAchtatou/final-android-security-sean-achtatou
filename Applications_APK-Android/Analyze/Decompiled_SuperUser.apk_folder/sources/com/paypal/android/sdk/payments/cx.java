package com.paypal.android.sdk.payments;

import android.os.AsyncTask;

final class cx extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PaymentMethodActivity f5252a;

    private cx(PaymentMethodActivity paymentMethodActivity) {
        this.f5252a = paymentMethodActivity;
    }

    /* synthetic */ cx(PaymentMethodActivity paymentMethodActivity, byte b2) {
        this(paymentMethodActivity);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        long currentTimeMillis = System.currentTimeMillis();
        String unused = PaymentMethodActivity.f5134a;
        boolean unused2 = this.f5252a.f5138e = cf.e();
        String unused3 = PaymentMethodActivity.f5134a;
        new StringBuilder("cameraAvailable:").append(this.f5252a.f5138e).append(" time elapsed = ").append(Long.toString(System.currentTimeMillis() - currentTimeMillis));
        this.f5252a.runOnUiThread(new cy(this));
        return null;
    }
}
