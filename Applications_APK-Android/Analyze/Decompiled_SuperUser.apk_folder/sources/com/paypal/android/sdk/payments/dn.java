package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.BulletSpan;
import android.text.style.URLSpan;
import android.util.Log;
import com.paypal.android.sdk.Cdo;
import com.paypal.android.sdk.ak;
import com.paypal.android.sdk.cd;
import com.paypal.android.sdk.cl;
import com.paypal.android.sdk.df;
import com.paypal.android.sdk.ed;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.ey;
import com.paypal.android.sdk.fs;
import com.paypal.android.sdk.ft;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

abstract class dn extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public static final String f5269d = dn.class.getSimpleName();
    private static final Map l = new o();

    /* renamed from: a  reason: collision with root package name */
    protected PayPalService f5270a;

    /* renamed from: b  reason: collision with root package name */
    protected PayPalOAuthScopes f5271b;

    /* renamed from: c  reason: collision with root package name */
    protected ft f5272c;

    /* renamed from: e  reason: collision with root package name */
    private boolean f5273e;

    /* renamed from: f  reason: collision with root package name */
    private dk f5274f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f5275g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f5276h;
    private boolean i;
    private du j;
    private final ServiceConnection k = new dv(this);

    dn() {
    }

    private void a(int i2, PayPalAuthorization payPalAuthorization) {
        Intent intent = new Intent();
        intent.putExtra("com.paypal.android.sdk.authorization", payPalAuthorization);
        setResult(i2, intent);
    }

    private void a(int i2, String str, String str2, ag agVar) {
        SpannableString spannableString = new SpannableString(Html.fromHtml(str2 + str));
        if (agVar != null) {
            URLSpan[] uRLSpanArr = (URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class);
            if (uRLSpanArr.length > 0) {
                URLSpan uRLSpan = uRLSpanArr[0];
                spannableString.setSpan(new b(uRLSpan, this, FuturePaymentInfoActivity.class, new Cdo(this), agVar), spannableString.getSpanStart(uRLSpan), spannableString.getSpanEnd(uRLSpan), 33);
                spannableString.removeSpan(uRLSpan);
            }
        } else {
            a(spannableString);
        }
        spannableString.setSpan(new BulletSpan(15), 0, spannableString.length(), 0);
        this.f5272c.f4937c[i2].setVisibility(0);
        this.f5272c.f4937c[i2].setFocusable(true);
        this.f5272c.f4937c[i2].setNextFocusLeftId((i2 + 100) - 1);
        this.f5272c.f4937c[i2].setNextFocusRightId(i2 + 100 + 1);
        this.f5272c.f4937c[i2].setText(spannableString);
    }

    private void a(SpannableString spannableString) {
        for (URLSpan uRLSpan : (URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class)) {
            spannableString.setSpan(new eb(uRLSpan, new dp(this)), spannableString.getSpanStart(uRLSpan), spannableString.getSpanEnd(uRLSpan), 33);
            spannableString.removeSpan(uRLSpan);
        }
    }

    private void a(dk dkVar) {
        this.f5270a.c().i = dkVar.f5264a;
        this.f5270a.c().f4668e = dkVar.f5265b;
        this.f5270a.c().f4666c = dkVar.f5266c;
        j();
    }

    static /* synthetic */ void a(dn dnVar, ed edVar) {
        dnVar.j = new du(edVar);
        dnVar.getIntent().putExtra("com.paypal.android.sdk.payments.ppAppInfo", dnVar.j);
        dnVar.f();
        dnVar.k();
    }

    private void b(SpannableString spannableString) {
        for (URLSpan uRLSpan : (URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class)) {
            spannableString.setSpan(new eb(uRLSpan, new dq(this)), spannableString.getSpanStart(uRLSpan), spannableString.getSpanEnd(uRLSpan), 33);
            spannableString.removeSpan(uRLSpan);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void
     arg types: [com.paypal.android.sdk.payments.dr, int]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.payments.bi):com.paypal.android.sdk.payments.bi
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.bt):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, boolean):boolean
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.Boolean):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(java.lang.String, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void */
    private void c() {
        if (this.f5270a != null) {
            showDialog(2);
            if (!this.f5270a.i()) {
                new StringBuilder("token is expired, get new one. AccessToken: ").append(this.f5270a.c().f4665b);
                this.f5270a.a((bi) new dr(this), true);
                return;
            }
            this.f5270a.p();
        }
    }

    private void d() {
        this.f5273e = bindService(cf.b(this), this.k, 1);
    }

    static /* synthetic */ void d(dn dnVar) {
        boolean z;
        new StringBuilder().append(f5269d).append(".postBindSetup()");
        new StringBuilder().append(f5269d).append(".startLoginIfNeeded (access token: ").append(dnVar.f5270a.c().f4670g).append(")");
        if (dnVar.f5270a.j() || dnVar.f5276h) {
            z = false;
        } else {
            new StringBuilder().append(f5269d).append(" -- doing the login...");
            dnVar.f5276h = true;
            dnVar.e();
            z = true;
        }
        ft ftVar = dnVar.f5272c;
        if (dnVar.i) {
            dnVar.i = false;
            dnVar.j();
        }
        if (!dnVar.f5275g) {
            dnVar.f5275g = true;
            dnVar.f5270a.a(ey.ConsentWindow);
        }
        cf.a(ftVar.f4940f.f4944b, dnVar.f5270a.e());
        dnVar.f5270a.b(new ds(dnVar));
        dnVar.f();
        if (!z && dnVar.j == null) {
            dnVar.c();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        new StringBuilder().append(f5269d).append(".doLogin");
        if (dl.a(this, this.f5270a)) {
            Intent a2 = new cl().a(this.f5270a.d().k(), com.paypal.android.sdk.dn.PROMPT_LOGIN, Cdo.code, this.f5270a.b().d().e());
            new StringBuilder("startActivityForResult(").append(a2).append(", 2").append(")");
            Log.w("paypal.sdk", "requesting code with scope=null from Authenticator.");
            startActivityForResult(a2, 2);
            return;
        }
        LoginActivity.a(this, 1, null, true, false, this.f5271b.b(), this.f5270a.d());
    }

    static /* synthetic */ void e(dn dnVar) {
        dnVar.f5270a.a(ey.ConsentCancel);
        dnVar.finish();
    }

    private void f() {
        int i2;
        if (this.f5271b != null && this.j != null && this.f5270a != null) {
            String l2 = this.f5270a.d().l();
            if (this.j.d() != null) {
                l2 = this.j.d();
            }
            String uri = this.f5270a.d().m().toString();
            if (this.j.b() != null) {
                uri = this.j.b();
            }
            String uri2 = this.f5270a.d().n().toString();
            if (this.j.c() != null) {
                uri2 = this.j.c();
            }
            String format = String.format(ev.a(fs.CONSENT_AGREEMENT_INTRO), "<b>" + l2 + "</b>");
            String str = ev.f4847a ? "‏" : "";
            this.f5272c.f4937c[0].setText(Html.fromHtml(str + format));
            if (ev.f4847a) {
                this.f5272c.f4937c[0].setGravity(5);
            }
            this.f5272c.f4937c[0].setVisibility(0);
            List a2 = this.f5271b.a();
            if (a2.contains(ak.FUTURE_PAYMENTS.a()) || a2.contains(df.PAYMENT_CODE.a()) || a2.contains(df.MIS_CUSTOMER.a())) {
                a(1, String.format(ev.a(fs.CONSENT_AGREEMENT_FUTURE_PAYMENTS), "future-payment-consent", "<b>" + l2 + "</b>", "<b>" + l2 + "</b>"), str, ag.FUTURE_PAYMENTS);
                i2 = 2;
            } else {
                i2 = 1;
            }
            if (a2.contains(df.GET_FUNDING_OPTIONS.a())) {
                a(i2, ev.a(fs.CONSENT_AGREEMENT_FUNDING_OPTIONS), str, null);
                i2++;
            }
            if (a2.contains(df.FINANCIAL_INSTRUMENTS.a())) {
                a(i2, ev.a(fs.CONSENT_AGREEMENT_FINANCIAL_INSTRUMENTS), str, ag.FINANCIAL_INSTRUMENTS);
                i2++;
            }
            if (a2.contains(df.SEND_MONEY.a())) {
                a(i2, String.format(ev.a(fs.CONSENT_AGREEMENT_SEND_MONEY), "", l2), str, ag.SEND_MONEY);
                i2++;
            }
            if (a2.contains(df.REQUEST_MONEY.a())) {
                a(i2, String.format(ev.a(fs.CONSENT_AGREEMENT_REQUEST_MONEY), "", l2), str, ag.REQUEST_MONEY);
                i2++;
            }
            if (a2.contains(df.LOYALTY.a())) {
                a(i2, ev.a(fs.CONSENT_AGREEMENT_LOYALTY_CARD), str, null);
                i2++;
            }
            if (a2.contains(df.EXPRESS_CHECKOUT.a())) {
                a(i2, ev.a(fs.CONSENT_AGREEMENT_EXPRESS_CHECKOUT), str, null);
                i2++;
            }
            if (!Collections.disjoint(a2, ak.f4557h)) {
                if (g().size() > 0) {
                    a(i2, String.format(ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTES), h()), str, null);
                    i2++;
                }
            }
            a(i2, String.format(ev.a(fs.CONSENT_AGREEMENT_MERCHANT_PRIVACY_POLICY), "<b>" + l2 + "</b>", uri, uri2), str, null);
            this.f5272c.f4937c[i2].setNextFocusRightId(2);
            int i3 = i2 + 1;
            String a3 = ev.a(fs.PRIVACY);
            Object[] objArr = new Object[1];
            String lowerCase = Locale.getDefault().getCountry().toLowerCase(Locale.US);
            if (cd.a((CharSequence) lowerCase)) {
                lowerCase = "us";
            }
            objArr[0] = String.format("https://www.paypal.com/%s/cgi-bin/webscr?cmd=p/gen/ua/policy_privacy-outside", lowerCase);
            SpannableString spannableString = new SpannableString(Html.fromHtml(str + String.format(a3, objArr)));
            b(spannableString);
            this.f5272c.f4938d.setText(spannableString);
            this.f5272c.f4938d.setMovementMethod(LinkMovementMethod.getInstance());
            this.f5272c.f4938d.setNextFocusLeftId((i3 + 100) - 1);
            this.f5272c.f4938d.setNextFocusRightId(1);
            SpannableString b2 = cd.b(this.f5270a.d().a());
            if (b2 != null) {
                this.f5272c.f4939e.setText(b2);
                this.f5272c.f4939e.setVisibility(0);
            }
            this.f5272c.i.setText(ev.a(fs.CONSENT_AGREEMENT_AGREE));
            this.f5272c.f4941g.setOnClickListener(new dy(this));
            this.f5272c.f4942h.setOnClickListener(new dz(this));
            this.f5272c.f4942h.setEnabled(true);
            if (this.f5274f != null) {
                a(this.f5274f);
                this.f5274f = null;
            }
        }
    }

    static /* synthetic */ void f(dn dnVar) {
        dnVar.f5270a.a(ey.ConsentAgree);
        if (!dnVar.f5270a.k()) {
            new StringBuilder("code/nonce invalid.  code:").append(dnVar.f5270a.c().f4668e).append(", nonce:").append(dnVar.f5270a.c().i);
            cf.a(dnVar, ev.a(fs.SESSION_EXPIRED_MESSAGE), 4);
            return;
        }
        dnVar.showDialog(2);
        dnVar.f5270a.a(dnVar.f5271b.a());
    }

    private Set g() {
        List a2 = this.f5271b.a();
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        y[] values = y.values();
        int length = values.length;
        for (int i2 = 0; i2 < length; i2++) {
            y yVar = values[i2];
            if (this.j.a().contains(yVar.name()) && a2.contains(((ak) l.get(yVar)).a())) {
                String a3 = yVar == y.openid_connect ? null : yVar == y.oauth_account_creation_date ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_ACCOUNT_CREATION_DATE) : yVar == y.oauth_account_verified ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_ACCOUNT_STATUS) : yVar == y.oauth_account_type ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_ACCOUNT_TYPE) : (yVar == y.oauth_street_address1 || yVar == y.oauth_street_address2 || yVar == y.oauth_city || yVar == y.oauth_state || yVar == y.oauth_country || yVar == y.oauth_zip) ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_ADDRESS) : yVar == y.oauth_age_range ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_AGE_RANGE) : yVar == y.oauth_date_of_birth ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_DATE_OF_BIRTH) : yVar == y.oauth_email ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_EMAIL_ADDRESS) : yVar == y.oauth_fullname ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_FULL_NAME) : yVar == y.oauth_gender ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_GENDER) : yVar == y.oauth_language ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_LANGUAGE) : yVar == y.oauth_locale ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_LOCALE) : yVar == y.oauth_phone_number ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_PHONE) : yVar == y.oauth_timezone ? ev.a(fs.CONSENT_AGREEMENT_ATTRIBUTE_TIME_ZONE) : yVar.name();
                if (a3 != null) {
                    linkedHashSet.add(a3);
                }
            }
        }
        return linkedHashSet;
    }

    private String h() {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (String str : g()) {
            if (!z) {
                sb.append(", ");
            } else {
                z = false;
            }
            sb.append(str);
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public void i() {
        a(-1, new PayPalAuthorization(this.f5270a.e(), this.f5270a.c().f4668e.a(), this.f5270a.c().f4666c));
        finish();
    }

    private void j() {
        String b2 = this.f5270a.c().f4668e.b();
        if (b2 == null || !Arrays.asList(b2.split(" ")).containsAll(this.f5271b.a())) {
            c();
        } else {
            i();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        try {
            dismissDialog(2);
        } catch (IllegalArgumentException e2) {
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public void finish() {
        super.finish();
        new StringBuilder().append(f5269d).append(".finish");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        new StringBuilder().append(f5269d).append(".onActivityResult(").append(i2).append(",").append(i3).append(",").append(intent).append(")");
        switch (i2) {
            case 1:
                if (i3 != -1) {
                    a(i3, (PayPalAuthorization) null);
                    finish();
                    return;
                } else if (this.f5270a == null) {
                    this.i = true;
                    return;
                } else {
                    j();
                    return;
                }
            case 2:
                if (i3 == -1) {
                    dk a2 = dm.a(intent.getExtras());
                    if (this.f5270a == null) {
                        this.f5274f = a2;
                        return;
                    } else {
                        a(a2);
                        return;
                    }
                } else {
                    a(i3, (PayPalAuthorization) null);
                    finish();
                    return;
                }
            default:
                Log.e(f5269d, "unhandled requestCode " + i2);
                return;
        }
    }

    public void onBackPressed() {
        this.f5270a.a(ey.ConsentCancel);
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new StringBuilder().append(f5269d).append(".onCreate");
        if (bundle == null) {
            if (!cf.a(this)) {
                finish();
            }
            this.f5275g = false;
        } else {
            this.f5275g = bundle.getBoolean("pageTrackingSent");
            this.f5276h = bundle.getBoolean("isLoginActivityStarted");
        }
        a();
        this.j = (du) getIntent().getParcelableExtra("com.paypal.android.sdk.payments.ppAppInfo");
        d();
        setTheme(16973934);
        requestWindowFeature(8);
        this.f5272c = new ft(this);
        setContentView(this.f5272c.f4935a);
        cf.a(this, this.f5272c.f4936b, (fs) null);
        this.f5272c.f4941g.setText(ev.a(fs.CANCEL));
        this.f5272c.f4941g.setVisibility(0);
        f();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2, Bundle bundle) {
        switch (i2) {
            case 1:
                return cf.a(this, fs.CONSENT_FAILED_ALERT_TITLE, bundle);
            case 2:
                return cf.a(this, fs.PROCESSING, fs.ONE_MOMENT);
            case 3:
                return cf.a(this, fs.INTERNAL_ERROR, bundle, i2);
            case 4:
                return cf.a(this, fs.SESSION_EXPIRED_TITLE, bundle, new dt(this));
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        new StringBuilder().append(f5269d).append(".onDestroy");
        if (this.f5270a != null) {
            this.f5270a.m();
        }
        if (this.f5273e) {
            unbindService(this.k);
            this.f5273e = false;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        d();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new StringBuilder().append(f5269d).append(".onResume");
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("pageTrackingSent", this.f5275g);
        bundle.putBoolean("isLoginActivityStarted", this.f5276h);
    }
}
