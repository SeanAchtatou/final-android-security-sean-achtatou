package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.content.DialogInterface;

final class dg implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Activity f5258a;

    dg(Activity activity) {
        this.f5258a = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f5258a.finish();
    }
}
