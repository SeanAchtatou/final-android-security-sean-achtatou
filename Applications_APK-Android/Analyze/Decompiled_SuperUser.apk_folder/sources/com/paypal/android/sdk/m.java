package com.paypal.android.sdk;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private String f5039a;

    /* renamed from: b  reason: collision with root package name */
    private String f5040b;

    public final String a() {
        return this.f5039a;
    }

    public final void a(String str) {
        this.f5039a = str;
    }

    public final String b() {
        return this.f5040b;
    }

    public final void b(String str) {
        this.f5040b = str;
    }
}
