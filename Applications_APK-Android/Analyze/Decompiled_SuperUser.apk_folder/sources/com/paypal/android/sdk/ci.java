package com.paypal.android.sdk;

public final class ci {

    /* renamed from: c  reason: collision with root package name */
    private static /* synthetic */ boolean f4676c = (!ci.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    private a f4677a;

    /* renamed from: b  reason: collision with root package name */
    private String f4678b;

    public ci(a aVar, String str) {
        if (f4676c || aVar != null) {
            this.f4677a = aVar;
            this.f4678b = "com.paypal.android.sdk.encr." + str + ".";
            return;
        }
        throw new AssertionError();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:14:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.paypal.android.sdk.di a() {
        /*
            r6 = this;
            r1 = 0
            com.paypal.android.sdk.a r0 = r6.f4677a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r6.f4678b
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "loginPhoneNumber"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.String r0 = r0.a(r2)
            if (r0 == 0) goto L_0x0071
            com.paypal.android.sdk.ce r2 = com.paypal.android.sdk.ce.a()     // Catch:{ ek -> 0x0070 }
            com.paypal.android.sdk.er r0 = com.paypal.android.sdk.er.a(r2, r0)     // Catch:{ ek -> 0x0070 }
        L_0x0026:
            com.paypal.android.sdk.a r2 = r6.f4677a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r6.f4678b
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "loginEmail"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = r2.a(r3)
            com.paypal.android.sdk.a r2 = r6.f4677a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r6.f4678b
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "loginTypePrevious"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            java.lang.String r2 = r2.a(r3)
            boolean r3 = android.text.TextUtils.isEmpty(r2)
            if (r3 == 0) goto L_0x0073
            r2 = r1
        L_0x0063:
            com.paypal.android.sdk.di r3 = new com.paypal.android.sdk.di
            r3.<init>(r4, r0, r2)
            boolean r0 = r3.d()
            if (r0 == 0) goto L_0x006f
            r1 = r3
        L_0x006f:
            return r1
        L_0x0070:
            r0 = move-exception
        L_0x0071:
            r0 = r1
            goto L_0x0026
        L_0x0073:
            com.paypal.android.sdk.dk r2 = com.paypal.android.sdk.dk.valueOf(r2)
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.sdk.ci.a():com.paypal.android.sdk.di");
    }

    public final dp a(String str) {
        String a2 = this.f4677a.a(this.f4678b + "tokenizedRedactedCardNumber");
        String a3 = this.f4677a.a(this.f4678b + "token");
        String a4 = this.f4677a.a(this.f4678b + "tokenPayerID");
        String a5 = this.f4677a.a(this.f4678b + "tokenValidUntil");
        String a6 = this.f4677a.a(this.f4678b + "tokenizedCardType");
        String a7 = this.f4677a.a(this.f4678b + "tokenizedCardExpiryMonth");
        int parseInt = a7 != null ? Integer.parseInt(a7) : 0;
        String a8 = this.f4677a.a(this.f4678b + "tokenizedCardExpiryYear");
        int parseInt2 = a8 != null ? Integer.parseInt(a8) : 0;
        String c2 = this.f4677a.c(this.f4677a.a(this.f4678b + "tokenClientId"));
        if (cd.a((CharSequence) c2) || !c2.equals(str)) {
            return null;
        }
        dp dpVar = new dp(a3, a4, a5, a2, a6, parseInt, parseInt2);
        if (!dpVar.b()) {
            return null;
        }
        return dpVar;
    }

    public final void a(di diVar) {
        String str = null;
        this.f4677a.a(this.f4678b + "loginPhoneNumber", diVar.a() != null ? diVar.a().b() : null);
        this.f4677a.a(this.f4678b + "loginEmail", diVar.b());
        if (diVar.c() != null) {
            str = diVar.c().toString();
        }
        this.f4677a.a(this.f4678b + "loginTypePrevious", str);
    }

    public final void a(dp dpVar, String str) {
        String str2 = null;
        this.f4677a.a(this.f4678b + "token", dpVar.e());
        this.f4677a.a(this.f4678b + "tokenPayerID", dpVar.a());
        this.f4677a.a(this.f4678b + "tokenValidUntil", dpVar.c() != null ? new et().format(dpVar.c()) : null);
        this.f4677a.a(this.f4678b + "tokenizedRedactedCardNumber", dpVar.d());
        if (dpVar.h() != null) {
            str2 = dpVar.h().toString();
        }
        this.f4677a.a(this.f4678b + "tokenizedCardType", str2);
        this.f4677a.a(this.f4678b + "tokenizedCardExpiryMonth", String.valueOf(dpVar.f()));
        this.f4677a.a(this.f4678b + "tokenizedCardExpiryYear", String.valueOf(dpVar.g()));
        this.f4677a.a(this.f4678b + "tokenClientId", this.f4677a.b(str));
    }

    public final void b() {
        a(new di());
    }

    public final void c() {
        a(new dp(), null);
    }
}
