package com.paypal.android.sdk.payments;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

final class cb implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PaymentConfirmActivity f5227a;

    cb(PaymentConfirmActivity paymentConfirmActivity) {
        this.f5227a = paymentConfirmActivity;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        new StringBuilder().append(PaymentConfirmActivity.f5122a).append(".onServiceConnected");
        PayPalService unused = this.f5227a.k = ((bh) iBinder).f5201a;
        if (this.f5227a.k.a(new cc(this))) {
            PaymentConfirmActivity.b(this.f5227a);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        PayPalService unused = this.f5227a.k = (PayPalService) null;
    }
}
