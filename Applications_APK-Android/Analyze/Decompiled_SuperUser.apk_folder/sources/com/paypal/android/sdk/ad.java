package com.paypal.android.sdk;

import android.net.Uri;
import java.util.Map;

public class ad implements y {

    /* renamed from: e  reason: collision with root package name */
    private static int f4533e = 60000;

    /* renamed from: f  reason: collision with root package name */
    private static int f4534f = 60000;

    /* renamed from: a  reason: collision with root package name */
    private final ah f4535a = new ah(cd.b());

    /* renamed from: b  reason: collision with root package name */
    private byte[] f4536b;

    /* renamed from: c  reason: collision with root package name */
    private Uri f4537c;

    /* renamed from: d  reason: collision with root package name */
    private Map f4538d;

    static {
        ad.class.getSimpleName();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(byte[] r10) {
        /*
            r9 = this;
            r2 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ all -> 0x00bc }
            android.net.Uri r1 = r9.f4537c     // Catch:{ all -> 0x00bc }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00bc }
            r0.<init>(r1)     // Catch:{ all -> 0x00bc }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ all -> 0x00bc }
            javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0     // Catch:{ all -> 0x00bc }
            int r1 = com.paypal.android.sdk.ad.f4534f     // Catch:{ all -> 0x0058 }
            r0.setReadTimeout(r1)     // Catch:{ all -> 0x0058 }
            int r1 = com.paypal.android.sdk.ad.f4533e     // Catch:{ all -> 0x0058 }
            r0.setConnectTimeout(r1)     // Catch:{ all -> 0x0058 }
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)     // Catch:{ all -> 0x0058 }
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ all -> 0x0058 }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ all -> 0x0058 }
            com.paypal.android.sdk.ah r1 = r9.f4535a     // Catch:{ all -> 0x0058 }
            r0.setSSLSocketFactory(r1)     // Catch:{ all -> 0x0058 }
            java.util.Map r1 = r9.f4538d     // Catch:{ all -> 0x0058 }
            java.util.Set r1 = r1.entrySet()     // Catch:{ all -> 0x0058 }
            java.util.Iterator r3 = r1.iterator()     // Catch:{ all -> 0x0058 }
        L_0x0038:
            boolean r1 = r3.hasNext()     // Catch:{ all -> 0x0058 }
            if (r1 == 0) goto L_0x0068
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x0058 }
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1     // Catch:{ all -> 0x0058 }
            java.lang.Object r4 = r1.getKey()     // Catch:{ all -> 0x0058 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0058 }
            java.lang.Object r1 = r1.getValue()     // Catch:{ all -> 0x0058 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0058 }
            r0.setRequestProperty(r4, r1)     // Catch:{ all -> 0x0058 }
            goto L_0x0038
        L_0x0058:
            r1 = move-exception
            r3 = r0
            r0 = r1
            r1 = r2
        L_0x005c:
            com.paypal.android.sdk.cd.a(r2)
            com.paypal.android.sdk.cd.a(r1)
            if (r3 == 0) goto L_0x0067
            r3.disconnect()
        L_0x0067:
            throw r0
        L_0x0068:
            int r1 = r10.length     // Catch:{ all -> 0x0058 }
            r0.setFixedLengthStreamingMode(r1)     // Catch:{ all -> 0x0058 }
            java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ all -> 0x0058 }
            r3.write(r10)     // Catch:{ all -> 0x00c0 }
            r3.flush()     // Catch:{ all -> 0x00c0 }
            int r4 = r0.getResponseCode()     // Catch:{ all -> 0x00c0 }
            r1 = 200(0xc8, float:2.8E-43)
            if (r4 != r1) goto L_0x00b5
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x00c0 }
            java.io.InputStream r5 = r0.getInputStream()     // Catch:{ all -> 0x00c0 }
            r1.<init>(r5)     // Catch:{ all -> 0x00c0 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ all -> 0x009c }
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x009c }
            r5.<init>()     // Catch:{ all -> 0x009c }
        L_0x0090:
            int r6 = r1.read(r2)     // Catch:{ all -> 0x009c }
            r7 = -1
            if (r6 == r7) goto L_0x00a3
            r7 = 0
            r5.write(r2, r7, r6)     // Catch:{ all -> 0x009c }
            goto L_0x0090
        L_0x009c:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r3
            r3 = r0
            r0 = r8
            goto L_0x005c
        L_0x00a3:
            byte[] r2 = r5.toByteArray()     // Catch:{ all -> 0x009c }
            r9.f4536b = r2     // Catch:{ all -> 0x009c }
        L_0x00a9:
            com.paypal.android.sdk.cd.a(r1)
            com.paypal.android.sdk.cd.a(r3)
            if (r0 == 0) goto L_0x00b4
            r0.disconnect()
        L_0x00b4:
            return r4
        L_0x00b5:
            r1 = 0
            byte[] r1 = new byte[r1]     // Catch:{ all -> 0x00c0 }
            r9.f4536b = r1     // Catch:{ all -> 0x00c0 }
            r1 = r2
            goto L_0x00a9
        L_0x00bc:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x005c
        L_0x00c0:
            r1 = move-exception
            r8 = r1
            r1 = r3
            r3 = r0
            r0 = r8
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.sdk.ad.a(byte[]):int");
    }

    public final void a(Uri uri) {
        this.f4537c = uri;
    }

    public final void a(Map map) {
        this.f4538d = map;
    }

    public final byte[] a() {
        return this.f4536b;
    }
}
