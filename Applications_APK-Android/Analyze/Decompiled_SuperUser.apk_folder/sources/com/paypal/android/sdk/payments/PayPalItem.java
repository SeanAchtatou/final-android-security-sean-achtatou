package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.paypal.android.sdk.cd;
import com.paypal.android.sdk.cy;
import java.math.BigDecimal;

public final class PayPalItem implements Parcelable {
    public static final Parcelable.Creator CREATOR = new as();

    /* renamed from: a  reason: collision with root package name */
    private static final String f5076a = PayPalItem.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final String f5077b;

    /* renamed from: c  reason: collision with root package name */
    private final Integer f5078c;

    /* renamed from: d  reason: collision with root package name */
    private final BigDecimal f5079d;

    /* renamed from: e  reason: collision with root package name */
    private final String f5080e;

    /* renamed from: f  reason: collision with root package name */
    private final String f5081f;

    private PayPalItem(Parcel parcel) {
        this.f5077b = parcel.readString();
        this.f5078c = Integer.valueOf(parcel.readInt());
        try {
            this.f5079d = new BigDecimal(parcel.readString());
            this.f5080e = parcel.readString();
            this.f5081f = parcel.readString();
        } catch (NumberFormatException e2) {
            Log.e(f5076a, "bad price", e2);
            throw new RuntimeException(e2);
        }
    }

    /* synthetic */ PayPalItem(Parcel parcel, byte b2) {
        this(parcel);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.cy.a(java.math.BigDecimal, java.lang.String, boolean):boolean
     arg types: [java.math.BigDecimal, java.lang.String, int]
     candidates:
      com.paypal.android.sdk.cy.a(double, java.lang.String, java.text.DecimalFormat):java.lang.String
      com.paypal.android.sdk.cy.a(java.math.BigDecimal, java.lang.String, boolean):boolean */
    public final boolean a() {
        if (this.f5078c.intValue() <= 0) {
            Log.e("paypal.sdk", "item.quantity must be a positive integer.");
            return false;
        } else if (!cy.a(this.f5080e)) {
            Log.e("paypal.sdk", "item.currency field is required, and must be a supported currency.");
            return false;
        } else if (cd.a((CharSequence) this.f5077b)) {
            Log.e("paypal.sdk", "item.name field is required.");
            return false;
        } else if (cy.a(this.f5079d, this.f5080e, false)) {
            return true;
        } else {
            Log.e("paypal.sdk", "item.price field is required.");
            return false;
        }
    }

    public final String b() {
        return this.f5077b;
    }

    public final Integer c() {
        return this.f5078c;
    }

    public final BigDecimal d() {
        return this.f5079d;
    }

    public final int describeContents() {
        return 0;
    }

    public final String e() {
        return this.f5080e;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof PayPalItem)) {
            return false;
        }
        PayPalItem payPalItem = (PayPalItem) obj;
        String b2 = b();
        String b3 = payPalItem.b();
        if (b2 != null ? !b2.equals(b3) : b3 != null) {
            return false;
        }
        Integer c2 = c();
        Integer c3 = payPalItem.c();
        if (c2 != null ? !c2.equals(c3) : c3 != null) {
            return false;
        }
        BigDecimal d2 = d();
        BigDecimal d3 = payPalItem.d();
        if (d2 != null ? !d2.equals(d3) : d3 != null) {
            return false;
        }
        String e2 = e();
        String e3 = payPalItem.e();
        if (e2 != null ? !e2.equals(e3) : e3 != null) {
            return false;
        }
        String f2 = f();
        String f3 = payPalItem.f();
        if (f2 == null) {
            if (f3 == null) {
                return true;
            }
        } else if (f2.equals(f3)) {
            return true;
        }
        return false;
    }

    public final String f() {
        return this.f5081f;
    }

    public final int hashCode() {
        int i = 43;
        String b2 = b();
        int hashCode = b2 == null ? 43 : b2.hashCode();
        Integer c2 = c();
        int i2 = (hashCode + 59) * 59;
        int hashCode2 = c2 == null ? 43 : c2.hashCode();
        BigDecimal d2 = d();
        int i3 = (hashCode2 + i2) * 59;
        int hashCode3 = d2 == null ? 43 : d2.hashCode();
        String e2 = e();
        int i4 = (hashCode3 + i3) * 59;
        int hashCode4 = e2 == null ? 43 : e2.hashCode();
        String f2 = f();
        int i5 = (hashCode4 + i4) * 59;
        if (f2 != null) {
            i = f2.hashCode();
        }
        return i5 + i;
    }

    public final String toString() {
        return "PayPalItem(name=" + b() + ", quantity=" + c() + ", price=" + d() + ", currency=" + e() + ", sku=" + f() + ")";
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f5077b);
        parcel.writeInt(this.f5078c.intValue());
        parcel.writeString(this.f5079d.toString());
        parcel.writeString(this.f5080e);
        parcel.writeString(this.f5081f);
    }
}
