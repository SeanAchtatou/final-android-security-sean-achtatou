package com.paypal.android.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class bw {

    /* renamed from: a  reason: collision with root package name */
    private static final Map f4640a;

    /* renamed from: b  reason: collision with root package name */
    private static Pattern f4641b = Pattern.compile("^\\s*(\\d+(\\.\\d+)*)\\s*([a-zA-Z]+)\\s*$");

    /* renamed from: c  reason: collision with root package name */
    private static HashMap f4642c = new HashMap();

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("px", 0);
        hashMap.put("dip", 1);
        hashMap.put("dp", 1);
        hashMap.put("sp", 2);
        hashMap.put("pt", 3);
        hashMap.put("in", 4);
        hashMap.put("mm", 5);
        f4640a = Collections.unmodifiableMap(hashMap);
    }

    public static int a(String str, Context context) {
        if (str == null) {
            return 0;
        }
        return (int) b(str, context);
    }

    public static Bitmap a(String str, Context context, int i) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (context != null) {
            options.inTargetDensity = context.getResources().getDisplayMetrics().densityDpi;
        } else {
            options.inTargetDensity = 160;
        }
        options.inDensity = 240;
        options.inScaled = false;
        byte[] decode = Base64.decode(str, 0);
        return BitmapFactory.decodeByteArray(decode, 0, decode.length, options);
    }

    public static View a(LinearLayout linearLayout) {
        View view = new View(linearLayout.getContext());
        linearLayout.addView(view);
        view.setBackground(new ColorDrawable(bv.f4637f));
        a(view, -1, "1dip");
        b(view, null, "12dip", null, "12dip");
        return view;
    }

    public static ViewGroup.LayoutParams a() {
        return new ViewGroup.LayoutParams(-1, -2);
    }

    public static ViewGroup a(Context context) {
        ScrollView scrollView = new ScrollView(context);
        scrollView.setBackgroundColor(bv.f4635d);
        return scrollView;
    }

    public static ImageView a(Context context, String str, String str2) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setImageBitmap(c(str, context));
        imageView.setAdjustViewBounds(true);
        imageView.setContentDescription(str2);
        return imageView;
    }

    public static LinearLayout a(Context context, boolean z, int i, LinearLayout linearLayout) {
        LinearLayout linearLayout2 = new LinearLayout(context);
        if (i != 0) {
            linearLayout2.setId(i);
        }
        linearLayout.addView(linearLayout2);
        linearLayout2.setGravity(17);
        linearLayout2.setOrientation(0);
        a(linearLayout2, z, context);
        a(linearLayout2, -1, "58dip");
        b(linearLayout2, null, null, null, "4dip");
        return linearLayout2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, int):void
     arg types: [android.widget.LinearLayout, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void */
    public static LinearLayout a(ViewGroup viewGroup) {
        LinearLayout linearLayout = new LinearLayout(viewGroup.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundColor(bv.f4635d);
        viewGroup.addView(linearLayout);
        a((View) linearLayout, -1, -2);
        return linearLayout;
    }

    public static RelativeLayout.LayoutParams a(int i, int i2, int i3, int i4) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(i3, i4);
        return layoutParams;
    }

    public static RelativeLayout.LayoutParams a(Context context, String str, String str2, int i) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a(str, context), a(str2, context));
        layoutParams.addRule(i);
        return layoutParams;
    }

    public static void a(View view) {
        b(view, "4dip", null, "4dip", null);
    }

    public static void a(View view, int i, float f2) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof LinearLayout.LayoutParams) {
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) layoutParams;
            layoutParams2.gravity = i;
            layoutParams2.weight = f2;
        }
    }

    public static void a(View view, int i, int i2) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
    }

    public static void a(View view, int i, String str) {
        a(view, i, a(str, view.getContext()));
    }

    public static void a(View view, String str, int i) {
        a(view, a(str, view.getContext()), -2);
    }

    public static void a(View view, String str, String str2) {
        b(view, "4dip", str, "4dip", str2);
    }

    public static void a(View view, String str, String str2, String str3, String str4) {
        Context context = view.getContext();
        view.setPadding(a(str, context), a(str2, context), a(str3, context), a(str4, context));
    }

    public static void a(View view, boolean z, Context context) {
        a(view, -1, -2);
        a(view, "10dip", "0dip", "10dip", "0dip");
        view.setBackground(z ? bv.a(context) : bv.b(context));
        view.setFocusable(true);
        view.setMinimumHeight(a("54dip", context));
        if (view instanceof TextView) {
            a((TextView) view);
        }
        if (!(view instanceof Button)) {
            view.setClickable(true);
        }
    }

    public static void a(Button button) {
        a(button, 17);
    }

    public static void a(TextView textView) {
        textView.setGravity(17);
        textView.setTextColor(-1);
        textView.setTextSize(20.0f);
        textView.setTypeface(bv.m);
    }

    public static void a(TextView textView, int i) {
        a(textView, "2dip", "2dip", "2dip", "2dip");
        textView.setTypeface(bv.n);
        textView.setTextColor(bv.t);
        textView.setBackground(bv.c(textView.getContext()));
        textView.setAutoLinkMask(15);
        textView.setTextSize(14.0f);
        textView.setTextColor(bv.t);
        textView.setGravity(i);
    }

    public static float b(String str, Context context) {
        if (str == null) {
            return 0.0f;
        }
        String lowerCase = str.toLowerCase();
        if (f4642c.containsKey(lowerCase)) {
            return ((Float) f4642c.get(lowerCase)).floatValue();
        }
        Matcher matcher = f4641b.matcher(lowerCase);
        if (!matcher.matches()) {
            throw new NumberFormatException();
        }
        float parseFloat = Float.parseFloat(matcher.group(1));
        Integer num = (Integer) f4640a.get(matcher.group(3).toLowerCase());
        if (num == null) {
            num = 1;
        }
        float applyDimension = TypedValue.applyDimension(num.intValue(), parseFloat, context.getResources().getDisplayMetrics());
        f4642c.put(lowerCase, Float.valueOf(applyDimension));
        return applyDimension;
    }

    public static LinearLayout b(ViewGroup viewGroup) {
        LinearLayout linearLayout = new LinearLayout(viewGroup.getContext());
        linearLayout.setOrientation(1);
        a(linearLayout, "10dip", "14dip", "10dip", "14dip");
        viewGroup.addView(linearLayout, a());
        return linearLayout;
    }

    public static void b(View view, String str, String str2) {
        Context context = view.getContext();
        a(view, a(str, context), a(str2, context));
    }

    public static void b(View view, String str, String str2, String str3, String str4) {
        Context context = view.getContext();
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            ((ViewGroup.MarginLayoutParams) layoutParams).setMargins(a(str, context), a(str2, context), a(str3, context), a(str4, context));
        }
    }

    public static void b(TextView textView) {
        textView.setTextColor(bv.k);
        textView.setLinkTextColor(bv.t);
        textView.setTypeface(bv.s);
        textView.setTextSize(13.0f);
        textView.setSingleLine(false);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static void b(TextView textView, int i) {
        textView.setTextSize(18.0f);
        textView.setTypeface(bv.o);
        textView.setSingleLine(true);
        textView.setGravity(i);
        textView.setTextColor(bv.f4638g);
    }

    public static Bitmap c(String str, Context context) {
        return a(str, context, 240);
    }

    public static void c(TextView textView, int i) {
        textView.setTextSize(16.0f);
        textView.setTypeface(bv.p);
        textView.setSingleLine(true);
        textView.setGravity(83);
        textView.setTextColor(bv.f4638g);
    }

    public static void d(TextView textView, int i) {
        textView.setTextSize(14.0f);
        textView.setTypeface(bv.q);
        textView.setSingleLine(true);
        textView.setGravity(i);
        textView.setTextColor(bv.f4638g);
    }

    public static void e(TextView textView, int i) {
        textView.setTextSize(13.0f);
        textView.setTypeface(bv.q);
        textView.setSingleLine(true);
        textView.setGravity(83);
        textView.setTextColor(bv.f4638g);
    }
}
