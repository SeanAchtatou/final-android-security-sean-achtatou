package com.paypal.android.sdk;

import io.fabric.sdk.android.services.common.a;
import java.util.GregorianCalendar;
import java.util.Map;
import org.json.JSONObject;

public class ar extends bs {

    /* renamed from: a  reason: collision with root package name */
    private final as f4568a;

    static {
        ar.class.getSimpleName();
    }

    public ar(br brVar, bu buVar, x xVar, as asVar) {
        super(brVar, buVar, xVar, null);
        this.f4568a = asVar;
        a(a.HEADER_ACCEPT, "application/json; charset=utf-8");
        a("Accept-Language", "en_US");
        a("Content-Type", a.ACCEPT_JSON_VALUE);
    }

    private static JSONObject a(Map map) {
        JSONObject jSONObject = new JSONObject();
        for (String str : map.keySet()) {
            jSONObject.accumulate(str, map.get(str));
        }
        return jSONObject;
    }

    public final String a(br brVar) {
        return "https://api.paypal.com/v1/tracking/events";
    }

    public final boolean a() {
        return true;
    }

    public final String b() {
        String a2 = cd.a(s().d().e());
        String str = this.f4568a.f4569a;
        JSONObject jSONObject = new JSONObject();
        jSONObject.accumulate("tracking_visitor_id", a2);
        jSONObject.accumulate("tracking_visit_id", str);
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.accumulate("actor", jSONObject);
        jSONObject2.accumulate("channel", "mobile");
        long currentTimeMillis = System.currentTimeMillis();
        jSONObject2.accumulate("tracking_event", Long.toString(currentTimeMillis));
        this.f4568a.f4570b.put("t", Long.toString(currentTimeMillis - ((long) new GregorianCalendar().getTimeZone().getRawOffset())));
        this.f4568a.f4570b.put("dsid", a2);
        this.f4568a.f4570b.put("vid", str);
        jSONObject2.accumulate("event_params", a(this.f4568a.f4570b));
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.accumulate("events", jSONObject2);
        return jSONObject3.toString();
    }

    public final void c() {
    }

    public final void d() {
    }

    public final String e() {
        return "mockResponse";
    }
}
