package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;

public final class PayPalAuthorization implements Parcelable {
    public static final Parcelable.Creator CREATOR = new al();

    /* renamed from: a  reason: collision with root package name */
    private final String f5059a;

    /* renamed from: b  reason: collision with root package name */
    private final String f5060b;

    /* renamed from: c  reason: collision with root package name */
    private final String f5061c;

    static {
        PayPalAuthorization.class.getSimpleName();
    }

    private PayPalAuthorization(Parcel parcel) {
        this.f5059a = parcel.readString();
        this.f5060b = parcel.readString();
        this.f5061c = parcel.readString();
    }

    /* synthetic */ PayPalAuthorization(Parcel parcel, byte b2) {
        this(parcel);
    }

    PayPalAuthorization(String str, String str2, String str3) {
        this.f5059a = str;
        this.f5060b = str2;
        if ("partner".equals("general")) {
            this.f5061c = str3;
        } else {
            this.f5061c = null;
        }
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f5059a);
        parcel.writeString(this.f5060b);
        parcel.writeString(this.f5061c);
    }
}
