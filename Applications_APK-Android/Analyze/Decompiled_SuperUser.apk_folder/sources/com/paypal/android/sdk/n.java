package com.paypal.android.sdk;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class n {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5041a = n.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private Context f5042b;

    /* renamed from: c  reason: collision with root package name */
    private String f5043c;

    /* renamed from: d  reason: collision with root package name */
    private JSONObject f5044d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f5045e;

    public n(Context context, String str) {
        this(context, str, (byte) 0);
    }

    private n(Context context, String str, byte b2) {
        aj.a(f5041a, "entering Configuration url loading");
        this.f5042b = context;
        this.f5043c = str;
        String o = o();
        if ("".equals(o)) {
            throw new IOException("No valid config found for " + str);
        }
        aj.a(f5041a, "entering saveConfigDataToDisk");
        try {
            File file = new File(this.f5042b.getFilesDir(), "CONFIG_DATA");
            File file2 = new File(this.f5042b.getFilesDir(), "CONFIG_TIME");
            cd.a(file, o);
            cd.a(file2, String.valueOf(System.currentTimeMillis()));
            aj.a(f5041a, "leaving saveConfigDataToDisk successfully");
        } catch (IOException e2) {
            new StringBuilder("Failed to write config data: ").append(e2.toString());
        }
        this.f5044d = new JSONObject(o);
        aj.a(f5041a, "leaving Configuration url loading");
    }

    public n(Context context, boolean z) {
        this.f5042b = context;
        this.f5043c = null;
        this.f5045e = z;
        aj.a(3, "PRD", "confIsUpdatable=" + Boolean.toString(this.f5045e));
        this.f5044d = j();
        aj.a(f5041a, "Configuation initialize, dumping config");
        aj.a(f5041a, this.f5044d);
    }

    private JSONObject a(String str) {
        try {
            aj.a(f5041a, "entering getIncrementalConfig");
            JSONObject jSONObject = new JSONObject(aj.b(this.f5042b, str));
            aj.a(f5041a, "leaving getIncrementalConfig");
            return jSONObject;
        } catch (Exception e2) {
            aj.a(6, "PRD", "Error while loading prdc Config " + str, e2);
            return null;
        }
    }

    private static JSONObject a(JSONObject jSONObject, JSONObject jSONObject2) {
        try {
            aj.a(f5041a, "entering mergeConfig");
            Iterator<String> keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                aj.a(f5041a, "overridding " + next);
                jSONObject.put(next, jSONObject2.get(next));
            }
            aj.a(f5041a, "leaving mergeConfig");
            return jSONObject;
        } catch (Exception e2) {
            aj.a(6, "PRD", "Error encountered while applying prdc Config", e2);
            return null;
        }
    }

    private JSONObject j() {
        try {
            JSONObject l = l();
            if (l != null) {
                if (aj.b(l.optString("conf_version", ""), "3.0")) {
                    if (this.f5045e) {
                        if (System.currentTimeMillis() > Long.parseLong(r()) + (l.optLong("conf_refresh_time_interval", 0) * 1000)) {
                            n();
                        }
                    }
                    aj.a(f5041a, "Using cached config");
                    return l;
                }
                q();
            }
            JSONObject k = k();
            if (k == null) {
                Log.e(f5041a, "default Configuration loading failed,Using hardcoded config");
                return m();
            }
            String a2 = aj.a(this.f5042b, "prdc", (String) null);
            if (a2 == null) {
                if (this.f5045e) {
                    n();
                }
                aj.a(3, "PRD", "prdc field not configured, using default config");
                return k;
            }
            aj.a(3, "PRD", "prdc field is configured, loading path:" + a2);
            JSONObject a3 = a(a2);
            if (a3 == null) {
                aj.a(6, "PRD", "prdc Configuration loading failed, using default config");
                return k;
            }
            JSONObject a4 = a(k, a3);
            if (a4 == null) {
                aj.a(6, "PRD", "applying prdc Configuration failed, using default config");
                return k;
            }
            aj.a(3, "PRD", "prdc configuration loaded successfully");
            return a4;
        } catch (Exception e2) {
            aj.a(6, "PRD", "Severe Error while loading config, using hard code version", e2);
            return m();
        }
    }

    private static JSONObject k() {
        aj.a(f5041a, "entering getDefaultConfigurations");
        try {
            String str = new String(Base64.decode("eyAiY29uZl92ZXJzaW9uIjogIjMuMCIsImFzeW5jX3VwZGF0ZV90aW1lX2ludGVydmFsIjogMzYwMCwgImZvcmNlZF9mdWxsX3VwZGF0ZV90aW1lX2ludGVydmFsIjogMTgwMCwgImNvbmZfcmVmcmVzaF90aW1lX2ludGVydmFsIjogODY0MDAsICJhbmRyb2lkX2FwcHNfdG9fY2hlY2siOiBbICJjb20uZWJheS5jbGFzc2lmaWVkcy9jb20uZWJheS5hcHAuTWFpblRhYkFjdGl2aXR5IiwgImNvbS5lYmF5Lm1vYmlsZS9jb20uZWJheS5tb2JpbGUuYWN0aXZpdGllcy5lQmF5IiwgImNvbS5lYmF5LnJlZGxhc2VyL2NvbS5lYmF5LnJlZGxhc2VyLlNjYW5uZWRJdGVtc0FjdGl2aXR5IiwgImNvbS5taWxvLmFuZHJvaWQvY29tLm1pbG8uYW5kcm9pZC5hY3Rpdml0eS5Ib21lQWN0aXZpdHkiLCAiY29tLnBheXBhbC5hbmRyb2lkLnAycG1vYmlsZS9jb20ucGF5cGFsLmFuZHJvaWQucDJwbW9iaWxlLmFjdGl2aXR5LlNlbmRNb25leUFjdGl2aXR5IiwgImNvbS5yZW50L2NvbS5yZW50LmFjdGl2aXRpZXMuc2Vzc2lvbi5BY3Rpdml0eUhvbWUiLCAiY29tLnN0dWJodWIvY29tLnN0dWJodWIuQWJvdXQiLCAiY29tLnVsb2NhdGUvY29tLnVsb2NhdGUuYWN0aXZpdGllcy5TZXR0aW5ncyIsICJjb20ubm9zaHVmb3UuYW5kcm9pZC5zdS9jb20ubm9zaHVmb3UuYW5kcm9pZC5zdS5TdSIsICJzdGVyaWNzb24uYnVzeWJveC9zdGVyaWNzb24uYnVzeWJveC5BY3Rpdml0eS5NYWluQWN0aXZpdHkiLCAib3JnLnByb3h5ZHJvaWQvb3JnLnByb3h5ZHJvaWQuUHJveHlEcm9pZCIsICJjb20uYWVkLmRyb2lkdnBuL2NvbS5hZWQuZHJvaWR2cG4uTWFpbkdVSSIsICJuZXQub3BlbnZwbi5vcGVudnBuL25ldC5vcGVudnBuLm9wZW52cG4uT3BlblZQTkNsaWVudCIsICJjb20ucGhvbmVhcHBzOTkuYWFiaXByb3h5L2NvbS5waG9uZWFwcHM5OS5hYWJpcHJveHkuT3Jib3QiLCAiY29tLmV2YW5oZS5wcm94eW1hbmFnZXIucHJvL2NvbS5ldmFuaGUucHJveHltYW5hZ2VyLk1haW5BY3Rpdml0eSIsICJjb20uZXZhbmhlLnByb3h5bWFuYWdlci9jb20uZXZhbmhlLnByb3h5bWFuYWdlci5NYWluQWN0aXZpdHkiLCAiY29tLmFuZHJvbW8uZGV2MzA5MzYuYXBwNzYxOTgvY29tLmFuZHJvbW8uZGV2MzA5MzYuYXBwNzYxOTguQW5kcm9tb0Rhc2hib2FyZEFjdGl2aXR5IiwgImNvbS5tZ3JhbmphLmF1dG9wcm94eV9saXRlL2NvbS5tZ3JhbmphLmF1dG9wcm94eV9saXRlLlByb3h5TGlzdEFjdGl2aXR5IiwgImNvbS52cG5vbmVjbGljay5hbmRyb2lkL2NvbS52cG5vbmVjbGljay5hbmRyb2lkLk1haW5BY3Rpdml0eSIsICJuZXQuaGlkZW1hbi9uZXQuaGlkZW1hbi5TdGFydGVyQWN0aXZpdHkiLCAiY29tLmRvZW50ZXIuYW5kcm9pZC52cG4uZml2ZXZwbi9jb20uZG9lbnRlci5hbmRyb2lkLnZwbi5maXZldnBuLkZpdmVWcG4iLCAiY29tLnRpZ2VydnBucy5hbmRyb2lkL2NvbS50aWdlcnZwbnMuYW5kcm9pZC5NYWluQWN0aXZpdHkiLCAiY29tLnBhbmRhcG93LnZwbi9jb20ucGFuZGFwb3cudnBuLlBhbmRhUG93IiwgImNvbS5leHByZXNzdnBuLnZwbi9jb20uZXhwcmVzc3Zwbi52cG4uTWFpbkFjdGl2aXR5IiwgImNvbS5sb25kb250cnVzdG1lZGlhLnZwbi9jb20ubG9uZG9udHJ1c3RtZWRpYS52cG4uVnBuU2VydmljZUFjdGl2aXR5IiwgImZyLm1lbGVjb20uVlBOUFBUUC52MTAxL2ZyLm1lbGVjb20uVlBOUFBUUC52MTAxLlNwbGFzaFNjcmVlbiIsICJjb20uY2hlY2twb2ludC5WUE4vY29tLmNoZWNrcG9pbnQuVlBOLk1haW5IYW5kbGVyIiwgImNvbS50dW5uZWxiZWFyLmFuZHJvaWQvY29tLnR1bm5lbGJlYXIuYW5kcm9pZC5UYmVhck1haW5BY3Rpdml0eSIsICJkZS5ibGlua3Qub3BlbnZwbi9kZS5ibGlua3Qub3BlbnZwbi5NYWluQWN0aXZpdHkiLCAib3JnLmFqZWplLmZha2Vsb2NhdGlvbi9vcmcuYWplamUuZmFrZWxvY2F0aW9uLkZha2UiLCAiY29tLmxleGEuZmFrZWdwcy9jb20ubGV4YS5mYWtlZ3BzLlBpY2tQb2ludCIsICJjb20uZm9yZ290dGVucHJvamVjdHMubW9ja2xvY2F0aW9ucy9jb20uZm9yZ290dGVucHJvamVjdHMubW9ja2xvY2F0aW9ucy5NYWluIiwgImtyLndvb3QwcGlhLmdwcy9rci53b290MHBpYS5ncHMuQ2F0Y2hNZUlmVUNhbiIsICJjb20ubXkuZmFrZS5sb2NhdGlvbi9jb20ubXkuZmFrZS5sb2NhdGlvbi5jb20ubXkuZmFrZS5sb2NhdGlvbiIsICJqcC5uZXRhcnQuYXJzdGFsa2luZy9qcC5uZXRhcnQuYXJzdGFsa2luZy5NeVByZWZlcmVuY2VBY3Rpdml0eSIsICJsb2NhdGlvblBsYXkuR1BTQ2hlYXRGcmVlL2xvY2F0aW9uUGxheS5HUFNDaGVhdEZyZWUuQWN0aXZpdHlTbWFydExvY2F0aW9uIiwgIm9yZy5nb29kZXYubGF0aXR1ZGUvb3JnLmdvb2Rldi5sYXRpdHVkZS5MYXRpdHVkZUFjdGl2aXR5IiwgImNvbS5zY2hlZmZzYmxlbmQuZGV2aWNlc3Bvb2YvY29tLnNjaGVmZnNibGVuZC5kZXZpY2VzcG9vZi5EZXZpY2VTcG9vZkFjdGl2aXR5IiwgImNvbS5wcm94eUJyb3dzZXIvY29tLnByb3h5QnJvd3Nlci5OZXdQcm94eUJyb3dzZXJBY3Rpdml0eSIsICJjb20uaWNlY29sZGFwcHMucHJveHlzZXJ2ZXJwcm8vY29tLmljZWNvbGRhcHBzLnByb3h5c2VydmVycHJvLnZpZXdTdGFydCIsICJob3RzcG90c2hpZWxkLmFuZHJvaWQudnBuL2NvbS5hbmNob3JmcmVlLnVpLkhvdFNwb3RTaGllbGQiLCAiY29tLmRvZW50ZXIub25ldnBuL2NvbS5kb2VudGVyLm9uZXZwbi5WcG5TZXR0aW5ncyIsICJjb20ueWVzdnBuLmVuL2NvbS55ZXN2cG4uTWFpblRhYiIsICJjb20ub2ZmaWNld3l6ZS5wbHV0b3Zwbi9jb20ub2ZmaWNld3l6ZS5wbHV0b3Zwbi5Ib21lQWN0aXZpdHkiLCAib3JnLmFqZWplLmxvY2F0aW9uc3Bvb2ZlcnByby9vcmcuYWplamUubG9jYXRpb25zcG9vZmVycHJvLkZha2UiLCAibG9jYXRpb25QbGF5LkdQU0NoZWF0L2xvY2F0aW9uUGxheS5HUFNDaGVhdC5BY3Rpdml0eVNtYXJ0TG9jYXRpb24iIF0sICJsb2NhdGlvbl9taW5fYWNjdXJhY3kiOiA1MDAsICJsb2NhdGlvbl9tYXhfY2FjaGVfYWdlIjogMTgwMCwgInNlbmRfb25fYXBwX3N0YXJ0IjogMSwgImVuZHBvaW50X3VybCI6ICJodHRwczovL3N2Y3MucGF5cGFsLmNvbS9BY2Nlc3NDb250cm9sL0xvZ1Jpc2tNZXRhZGF0YSIsICJpbnRlcm5hbF9jYWNoZV9tYXhfYWdlIjogMzAsICJjb21wX3RpbWVvdXQiOiA2MDAgfQ==", 0), "UTF-8");
            aj.a(f5041a, "leaving getDefaultConfigurations, Default Conf load succeed");
            return new JSONObject(str);
        } catch (Exception e2) {
            aj.a(6, "PRD", "Read default config file exception.", e2);
            aj.a(f5041a, "leaving getDefaultConfigurations,returning null");
            return null;
        }
    }

    private JSONObject l() {
        aj.a(f5041a, "entering getCachedConfiguration");
        try {
            String p = p();
            if (!"".equals(p)) {
                aj.a(f5041a, "leaving getCachedConfiguration,cached config load succeed");
                return new JSONObject(p);
            }
        } catch (Exception e2) {
            aj.a(f5041a, "JSON Exception in creating config file", e2);
        }
        aj.a(f5041a, "leaving getCachedConfiguration,cached config load failed");
        return null;
    }

    private static JSONObject m() {
        aj.a(f5041a, "entering getHardcodedConfig");
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("conf_version", "3.0");
            jSONObject.put("async_update_time_interval", 3600);
            jSONObject.put("forced_full_update_time_interval", 1800);
            jSONObject.put("conf_refresh_time_interval", 86400);
            jSONObject.put("location_min_accuracy", 500);
            jSONObject.put("location_max_cache_age", 1800);
            jSONObject.put("endpoint_url", "https://svcs.paypal.com/AccessControl/LogRiskMetadata");
        } catch (JSONException e2) {
        }
        aj.a(f5041a, "leaving getHardcodedConfig");
        return jSONObject;
    }

    private static void n() {
        aj.a(f5041a, "Loading web config");
        r.a().b();
    }

    private String o() {
        BufferedReader bufferedReader;
        InputStream inputStream = null;
        aj.a(f5041a, "entering getRemoteConfig");
        StringBuilder sb = new StringBuilder();
        try {
            InputStream openStream = new URL(this.f5043c).openStream();
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(openStream, "UTF-8"));
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine != null) {
                            sb.append(readLine);
                        } else {
                            cd.a(openStream);
                            cd.a(bufferedReader);
                            aj.a(f5041a, "leaving getRemoteConfig successfully");
                            return sb.toString();
                        }
                    } catch (Throwable th) {
                        th = th;
                        inputStream = openStream;
                        cd.a(inputStream);
                        cd.a(bufferedReader);
                        throw th;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedReader = null;
                inputStream = openStream;
                cd.a(inputStream);
                cd.a(bufferedReader);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            bufferedReader = null;
        }
    }

    private String p() {
        try {
            return cd.a(new File(this.f5042b.getFilesDir(), "CONFIG_DATA"));
        } catch (IOException e2) {
            aj.a(f5041a, "Load cached config failed", e2);
            return "";
        }
    }

    private boolean q() {
        try {
            aj.a(f5041a, "entering deleteCachedConfigDataFromDisk");
            File file = new File(this.f5042b.getFilesDir(), "CONFIG_DATA");
            File file2 = new File(this.f5042b.getFilesDir(), "CONFIG_TIME");
            if (file.exists()) {
                aj.a(f5041a, "cached Config Data found, deleting");
                file.delete();
            }
            if (file2.exists()) {
                aj.a(f5041a, "cached Config Time found, deleting");
                file.delete();
            }
            aj.a(f5041a, "leaving deleteCachedConfigDataFromDisk");
            return true;
        } catch (Exception e2) {
            aj.a(f5041a, "error encountered while deleteCachedConfigData", e2);
            return false;
        }
    }

    private String r() {
        try {
            return cd.a(new File(this.f5042b.getFilesDir(), "CONFIG_TIME"));
        } catch (IOException e2) {
            return "";
        }
    }

    public final String a() {
        return this.f5043c;
    }

    public final String b() {
        return this.f5044d.optString("conf_version", "");
    }

    public final long c() {
        return this.f5044d.optLong("async_update_time_interval", 0);
    }

    public final long d() {
        return this.f5044d.optLong("forced_full_update_time_interval", 0);
    }

    public final long e() {
        return this.f5044d.optLong("comp_timeout", 0);
    }

    public final List f() {
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = this.f5044d.optJSONArray("android_apps_to_check");
        int i = 0;
        while (optJSONArray != null && i < optJSONArray.length()) {
            arrayList.add(optJSONArray.getString(i));
            i++;
        }
        return arrayList;
    }

    public final String g() {
        return this.f5044d.optString("endpoint_url", null);
    }

    public final boolean h() {
        return this.f5044d.optBoolean("endpoint_is_stage", false);
    }

    public final ai i() {
        try {
            String optString = this.f5044d.optString("CDS", "");
            if (optString == null || "".equals(optString)) {
                aj.a(3, "PRD", "No CDS is configured, enabling all variables");
                return ai.f4543a;
            }
            aj.a(3, "PRD", "CDS field was found");
            return new ai(optString.trim());
        } catch (Exception e2) {
            aj.a(6, "PRD", "Failed to decode CDS", e2);
            return ai.f4543a;
        }
    }
}
