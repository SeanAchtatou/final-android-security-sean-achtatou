package com.paypal.android.sdk;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class fa implements Iterable {

    /* renamed from: a  reason: collision with root package name */
    private List f4870a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private int f4871b;

    static {
        fa.class.getSimpleName();
    }

    public fa(JSONArray jSONArray, JSONObject jSONObject) {
        ez ezVar;
        ez ezVar2;
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                ezVar2 = ez.a(jSONArray.getJSONObject(i));
            } catch (JSONException e2) {
                Log.w("paypal.sdk", "Error extracting funding source: " + e2.getMessage());
                ezVar2 = null;
            }
            if (ezVar2 != null) {
                this.f4870a.add(ezVar2);
            }
        }
        if (jSONObject != null) {
            try {
                ezVar = ez.a(jSONObject);
            } catch (JSONException e3) {
                Log.w("paypal.sdk", "Error parsing backup funding instrument: " + e3.getMessage());
                ezVar = null;
            }
            if (ezVar != null) {
                this.f4870a.add(ezVar);
            }
        }
        this.f4871b = f();
    }

    private int f() {
        int i = 0;
        Double valueOf = Double.valueOf(0.0d);
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i3 >= this.f4870a.size()) {
                return i2;
            }
            if (((ez) this.f4870a.get(i3)).d().doubleValue() > valueOf.doubleValue()) {
                valueOf = ((ez) this.f4870a.get(i3)).d();
                i2 = i3;
            }
            i = i3 + 1;
        }
    }

    public final ez a(int i) {
        this.f4870a.size();
        return (ez) this.f4870a.get(0);
    }

    public final String a() {
        return ((ez) this.f4870a.get(this.f4871b)).a();
    }

    public final boolean b() {
        String f2 = ((ez) this.f4870a.get(this.f4871b)).f();
        if (cd.b((CharSequence) f2)) {
            return f2.toUpperCase().equals("DELAYED_TRANSFER");
        }
        return false;
    }

    public final String c() {
        return this.f4870a.size() == 1 ? ((ez) this.f4870a.get(0)).b() : ev.a(fs.AND_OTHER_FUNDING_SOURCES);
    }

    public final String d() {
        return ((ez) this.f4870a.get(this.f4871b)).e();
    }

    public final int e() {
        return this.f4870a.size();
    }

    public final Iterator iterator() {
        return this.f4870a.iterator();
    }
}
