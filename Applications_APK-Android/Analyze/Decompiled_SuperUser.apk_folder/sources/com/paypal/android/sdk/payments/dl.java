package com.paypal.android.sdk.payments;

import android.content.Context;
import com.paypal.android.sdk.an;
import com.paypal.android.sdk.cl;

class dl {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5267a = dl.class.getSimpleName();

    dl() {
    }

    static boolean a(Context context, PayPalService payPalService) {
        boolean z = false;
        if (an.e(payPalService.e())) {
            new StringBuilder("Is mock or sandbox:").append(payPalService.e());
        } else if (payPalService.w()) {
            cl clVar = new cl();
            boolean x = payPalService.x();
            boolean a2 = clVar.a(context, "com.paypal.android.p2pmobile.Sdk", "com.paypal.android.lib.authenticator.activity.SdkActivity");
            if (clVar.a(context, x) && a2) {
                z = true;
            }
        }
        new StringBuilder("returning isValid:").append(z);
        return z;
    }
}
