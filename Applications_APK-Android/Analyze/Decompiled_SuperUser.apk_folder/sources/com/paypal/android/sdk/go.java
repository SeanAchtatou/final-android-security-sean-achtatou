package com.paypal.android.sdk;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import com.lody.virtual.helper.utils.FileUtils;
import com.paypal.android.sdk.payments.PayPalService;

public class go {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5026a = go.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final Context f5027b;

    public go(Context context) {
        this.f5027b = context;
    }

    private boolean b() {
        try {
            PackageManager packageManager = this.f5027b.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(this.f5027b.getPackageName(), 4);
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.f5027b.getPackageName(), FileUtils.FileMode.MODE_IWUSR);
            if (packageInfo.services == null) {
                return false;
            }
            for (ServiceInfo serviceInfo : packageInfo.services) {
                if (serviceInfo.name.equals(PayPalService.class.getName())) {
                    new StringBuilder("context.getPackageName()=").append(this.f5027b.getPackageName());
                    new StringBuilder("serviceInfo.exported=").append(serviceInfo.exported);
                    new StringBuilder("serviceInfo.processName=").append(serviceInfo.processName);
                    new StringBuilder("applicationInfo.processName=").append(applicationInfo.processName);
                    if (!serviceInfo.exported && applicationInfo.processName.equals(serviceInfo.processName)) {
                        new StringBuilder("Found ").append(PayPalService.class.getName()).append(" in manifest.");
                        return true;
                    }
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            throw new RuntimeException("Exception loading manifest" + e2.getMessage());
        }
    }

    public final void a() {
        if (!b()) {
            throw new RuntimeException("Please declare the following in your manifest, and ensure it runs in the same process as your application:<service android:name=\"com.paypal.android.sdk.payments.PayPalService\" android:exported=\"false\" />");
        }
    }
}
