package com.paypal.android.sdk;

import android.text.TextUtils;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import okhttp3.q;
import okhttp3.r;
import okhttp3.v;
import okhttp3.x;

public class cg implements r {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4672a = cg.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final String f4673b;

    public cg(String str) {
        this.f4673b = str;
    }

    private static String a(String str, String str2) {
        Mac instance = Mac.getInstance("HmacSHA1");
        instance.init(new SecretKeySpec(str.getBytes(), "HmacSHA1"));
        instance.update(str2.getBytes());
        byte[] doFinal = instance.doFinal();
        StringBuilder sb = new StringBuilder();
        int length = doFinal.length;
        for (int i = 0; i < length; i++) {
            sb.append(String.format("%02x", Byte.valueOf(doFinal[i])));
        }
        return sb.toString();
    }

    public x a(r.a aVar) {
        v a2 = aVar.a();
        String format = String.format("Trace: [%s] %s, %s", this.f4673b, "\"%08.8x: Operation = %80s Duration: %8.2f Iterations: %+4d\"", "memorySize * 8 + offset");
        try {
            q c2 = a2.c();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < c2.a(); i++) {
                arrayList.add(c2.a(i) + ": " + c2.b(i));
            }
            Collections.sort(arrayList);
            return aVar.a(a2.e().b("PayPal-Item-Id").b("PayPal-Item-Id", a(format, TextUtils.join(";", arrayList.toArray()) + a2.d())).b());
        } catch (InvalidKeyException | NoSuchAlgorithmException e2) {
            return aVar.a(a2);
        }
    }
}
