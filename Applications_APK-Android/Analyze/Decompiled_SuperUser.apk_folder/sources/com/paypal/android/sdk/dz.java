package com.paypal.android.sdk;

import com.google.android.gms.common.Scopes;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class dz extends eo {

    /* renamed from: a  reason: collision with root package name */
    private String f4773a;

    /* renamed from: b  reason: collision with root package name */
    private String f4774b;

    /* renamed from: c  reason: collision with root package name */
    private JSONArray f4775c;

    /* renamed from: d  reason: collision with root package name */
    private JSONObject f4776d;

    /* renamed from: e  reason: collision with root package name */
    private JSONArray f4777e;

    /* renamed from: f  reason: collision with root package name */
    private en f4778f;

    /* renamed from: g  reason: collision with root package name */
    private Map f4779g;

    /* renamed from: h  reason: collision with root package name */
    private ei[] f4780h;
    private String i;
    private boolean j;
    private String k;
    private boolean l;
    private String m;
    private String n;
    private String o;
    private String p;

    public dz(bu buVar, x xVar, String str, String str2, String str3, en enVar, Map map, ei[] eiVarArr, String str4, boolean z, String str5, String str6, String str7, boolean z2) {
        super(db.CreateSfoPaymentRequest, buVar, xVar, str);
        this.f4778f = enVar;
        this.f4779g = map;
        this.f4780h = eiVarArr;
        this.i = str4;
        this.l = z2;
        this.k = str7;
        if (cd.a((CharSequence) this.k)) {
            this.k = "sale";
        }
        this.k = this.k.toLowerCase(Locale.US);
        a("PayPal-Request-Id", str2);
        if (cd.b((CharSequence) str5)) {
            a("PayPal-Partner-Attribution-Id", str5);
        }
        if (cd.b((CharSequence) str6)) {
            a("PayPal-Client-Metadata-Id", str6);
        }
    }

    public final dz a(boolean z) {
        this.j = z;
        return this;
    }

    public final String b() {
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.accumulate("intent", this.k);
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.accumulate("payment_method", "paypal");
        jSONObject2.accumulate("payer", jSONObject3);
        JSONObject jSONObject4 = new JSONObject();
        jSONObject4.accumulate("cancel_url", "http://cancelurl");
        jSONObject4.accumulate("return_url", "http://returnurl");
        jSONObject2.accumulate("redirect_urls", jSONObject4);
        en enVar = this.f4778f;
        JSONObject jSONObject5 = new JSONObject();
        jSONObject5.accumulate(FirebaseAnalytics.Param.CURRENCY, enVar.b().getCurrencyCode());
        jSONObject5.accumulate("total", enVar.a().toPlainString());
        if (this.f4779g != null && !this.f4779g.isEmpty()) {
            if (this.f4779g == null || this.f4779g.isEmpty()) {
                jSONObject = null;
            } else {
                jSONObject = new JSONObject();
                if (this.f4779g.containsKey(FirebaseAnalytics.Param.SHIPPING)) {
                    jSONObject.accumulate(FirebaseAnalytics.Param.SHIPPING, this.f4779g.get(FirebaseAnalytics.Param.SHIPPING));
                }
                if (this.f4779g.containsKey("subtotal")) {
                    jSONObject.accumulate("subtotal", this.f4779g.get("subtotal"));
                }
                if (this.f4779g.containsKey(FirebaseAnalytics.Param.TAX)) {
                    jSONObject.accumulate(FirebaseAnalytics.Param.TAX, this.f4779g.get(FirebaseAnalytics.Param.TAX));
                }
            }
            jSONObject5.accumulate("details", jSONObject);
        }
        JSONObject jSONObject6 = new JSONObject();
        jSONObject6.accumulate("amount", jSONObject5);
        jSONObject6.accumulate("description", this.i);
        if (this.f4780h != null && this.f4780h.length > 0) {
            JSONObject jSONObject7 = new JSONObject();
            jSONObject7.accumulate("items", ei.a(this.f4780h));
            jSONObject6.accumulate(FirebaseAnalytics.Param.ITEM_LIST, jSONObject7);
        }
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(jSONObject6);
        jSONObject2.accumulate("transactions", jSONArray);
        if (cd.b((CharSequence) this.m)) {
            jSONObject6.accumulate("invoice_number", this.m);
        }
        if (cd.b((CharSequence) this.n)) {
            jSONObject6.accumulate("custom", this.n);
        }
        if (cd.b((CharSequence) this.o)) {
            jSONObject6.accumulate("soft_descriptor", this.o);
        }
        if (cd.b((CharSequence) this.p)) {
            JSONObject jSONObject8 = new JSONObject();
            jSONObject8.accumulate(Scopes.EMAIL, this.p);
            jSONObject6.accumulate("payee", jSONObject8);
        }
        JSONObject jSONObject9 = new JSONObject();
        jSONObject9.accumulate("device_info", cd.a(da.a().toString()));
        jSONObject9.accumulate("app_info", cd.a(cw.a().toString()));
        jSONObject9.accumulate("risk_data", cd.a(r.a().c().toString()));
        JSONObject jSONObject10 = new JSONObject();
        jSONObject10.accumulate("payment", jSONObject2);
        jSONObject10.accumulate("client_info", jSONObject9);
        if (this.l) {
            jSONObject10.accumulate("retrieve_shipping_addresses", true);
        }
        jSONObject10.accumulate("no_shipping", Boolean.valueOf(this.j));
        return jSONObject10.toString();
    }

    public final void c() {
        JSONObject m2 = m();
        this.f4773a = m2.optString("payment_id");
        this.f4774b = m2.getString("session_id");
        this.f4775c = m2.optJSONArray("addresses");
        JSONObject optJSONObject = m2.optJSONObject("funding_options");
        if (optJSONObject != null) {
            this.f4776d = optJSONObject.optJSONObject("default_option");
            this.f4777e = optJSONObject.optJSONArray("alternate_options");
        }
    }

    public final dz d(String str) {
        this.m = str;
        return this;
    }

    public final void d() {
        b(m());
    }

    public final dz e(String str) {
        this.n = str;
        return this;
    }

    public final String e() {
        return "{    \"session_id\":\"7N0112287V303050T\",    \"payment_id\":\"PAY-18X32451H0459092JKO7KFUI\",    \"addresses\": [          {             \"city\": \"Columbia\",              \"line2\": \"6073 2nd Street\",              \"line1\": \"Suite 222\",              \"recipient_name\": \"Beverly Jello\",             \"state\": \"MD\",              \"postal_code\": \"21045\",             \"default_address\": false,              \"country_code\": \"US\",              \"type\": \"HOME_OR_WORK\",              \"id\": \"366853\"          },          {             \"city\": \"Austin\",              \"line2\": \"Apt. 222\",              \"line1\": \"52 North Main St. \",              \"recipient_name\": \"Michael Chassen\",             \"state\": \"TX\",              \"postal_code\": \"78729\",             \"default_address\": true,              \"country_code\": \"US\",              \"type\": \"HOME_OR_WORK\",              \"id\": \"366852\"          },          {             \"city\": \"Austin\",              \"line1\": \"202 South State St. \",              \"recipient_name\": \"Sam Stone\",             \"state\": \"TX\",              \"postal_code\": \"78729\",             \"default_address\": true,              \"country_code\": \"US\",              \"type\": \"HOME_OR_WORK\",              \"id\": \"366852\"          }     ],     \"funding_options\":{       \"default_option\":{          \"id\":\"1\",          \"backup_funding_instrument\":{             \"payment_card\":{                \"number\":\"8029\",                \"type\":\"VISA\"             }          },          \"funding_sources\":[             {                \"amount\":{                   \"value\":\"216.85\",                   \"currency\":\"USD\"                },                \"funding_instrument_type\":\"BANK_ACCOUNT\",                \"funding_mode\":\"INSTANT_TRANSFER\",                \"bank_account\":{                   \"bank_name\":\"SunTrust\",                   \"account_number\":\"7416\",                   \"account_number_type\":\"BBAN\",                   \"country_code\":\"US\",                   \"account_type\":\"CHECKING\"                }             },             {                \"amount\":{                   \"value\":\"6.00\",                   \"currency\":\"USD\"                },                \"funding_instrument_type\":\"CREDIT\",                \"funding_mode\":\"INSTANT_TRANSFER\",                \"credit\":{                   \"type\":\"BILL_ME_LATER\",                   \"id\":\"mock-id\"                }             }          ]       },       \"alternate_options\":[          {             \"id\":\"2\",             \"funding_sources\":[                {                   \"amount\":{                      \"value\":\"216.85\",                      \"currency\":\"USD\"                   },                   \"funding_instrument_type\":\"PAYMENT_CARD\",                   \"payment_card\":{                      \"number\":\"8029\",                      \"type\":\"VISA\"                   },                   \"funding_mode\":\"INSTANT_TRANSFER\"                },                {                   \"amount\":{                      \"value\":\"6.00\",                      \"currency\":\"USD\"                   },                   \"funding_instrument_type\":\"BALANCE\",                   \"funding_mode\":\"INSTANT_TRANSFER\"                }             ]          },          {             \"id\":\"3\",             \"funding_sources\":[                {                   \"amount\":{                      \"value\":\"216.85\",                      \"currency\":\"USD\"                   },                   \"funding_instrument_type\":\"PAYMENT_CARD\",                   \"payment_card\":{                      \"number\":\"8011\",                      \"type\":\"VISA\"                   },                   \"funding_mode\":\"INSTANT_TRANSFER\"                },                {                   \"amount\":{                      \"value\":\"6.00\",                      \"currency\":\"USD\"                   },                   \"funding_instrument_type\":\"BALANCE\",                   \"funding_mode\":\"INSTANT_TRANSFER\"                }             ]          }       ]    } }";
    }

    public final dz f(String str) {
        this.o = str;
        return this;
    }

    public final dz g(String str) {
        this.p = str;
        return this;
    }

    public final String t() {
        return this.f4773a;
    }

    public final String u() {
        return this.f4774b;
    }

    public final JSONArray v() {
        return this.f4775c;
    }

    public final JSONObject w() {
        return this.f4776d;
    }

    public final JSONArray x() {
        return this.f4777e;
    }
}
