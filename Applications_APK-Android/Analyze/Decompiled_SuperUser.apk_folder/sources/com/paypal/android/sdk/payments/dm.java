package com.paypal.android.sdk.payments;

import android.os.Bundle;
import com.paypal.android.sdk.du;

class dm {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5268a = dm.class.getSimpleName();

    dm() {
    }

    public static dk a(Bundle bundle) {
        String string = bundle.getString("authAccount");
        String string2 = bundle.getString("code");
        String string3 = bundle.getString("nonce");
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (obj == null) {
                String.format("%s:null", next);
            } else {
                String.format("%s:%s (%s)", next, obj.toString(), obj.getClass().getName());
            }
        }
        return new dk(string3, new du(string2, null), string);
    }
}
