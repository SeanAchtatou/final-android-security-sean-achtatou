package com.paypal.android.sdk;

import android.os.Handler;
import android.os.Message;

public class w extends ae {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5362a = w.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private Handler f5363b;

    /* renamed from: c  reason: collision with root package name */
    private String f5364c;

    /* renamed from: d  reason: collision with root package name */
    private String f5365d;

    /* renamed from: e  reason: collision with root package name */
    private String f5366e;

    /* renamed from: f  reason: collision with root package name */
    private m f5367f;

    public w(String str, String str2, String str3, m mVar, Handler handler) {
        this.f5363b = handler;
        this.f5364c = str;
        this.f5365d = str2;
        this.f5366e = str3;
        this.f5367f = mVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r11) {
        /*
            r10 = this;
            r2 = 0
            java.lang.String r4 = ""
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x00b6, all -> 0x00cf }
            r1.<init>(r11)     // Catch:{ Exception -> 0x00b6, all -> 0x00cf }
            java.net.URLConnection r0 = r1.openConnection()     // Catch:{ Exception -> 0x00b6, all -> 0x00cf }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00b6, all -> 0x00cf }
            r3 = 60000(0xea60, float:8.4078E-41)
            r0.setReadTimeout(r3)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r3 = 60000(0xea60, float:8.4078E-41)
            r0.setConnectTimeout(r3)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r3 = "GET"
            r0.setRequestMethod(r3)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r3 = "User-Agent"
            java.lang.String r5 = "%s/%s/%s/%s/Android"
            r6 = 4
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r7 = 0
            com.paypal.android.sdk.m r8 = r10.f5367f     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r8 = r8.a()     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r6[r7] = r8     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r7 = 1
            com.paypal.android.sdk.m r8 = r10.f5367f     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r8 = r8.b()     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r6[r7] = r8     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r7 = 2
            java.lang.String r8 = r10.f5366e     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r6[r7] = r8     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r7 = 3
            java.lang.String r8 = r10.f5365d     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r6[r7] = r8     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r5 = java.lang.String.format(r5, r6)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r0.setRequestProperty(r3, r5)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r3 = "Accept-Language"
            java.lang.String r5 = "en-us"
            r0.setRequestProperty(r3, r5)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            int r3 = r0.getResponseCode()     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r5 = com.paypal.android.sdk.w.f5362a     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r7 = "\nSending 'GET' request to URL : "
            r6.<init>(r7)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            com.paypal.android.sdk.aj.a(r5, r1)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r1 = com.paypal.android.sdk.w.f5362a     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r6 = "Response Code : "
            r5.<init>(r6)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            com.paypal.android.sdk.aj.a(r1, r3)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            java.io.InputStream r5 = r0.getInputStream()     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r1.<init>(r5)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r3.<init>(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x00da }
            r1 = r4
        L_0x008b:
            java.lang.String r2 = r3.readLine()     // Catch:{ Exception -> 0x00ea, all -> 0x00df }
            if (r2 == 0) goto L_0x00a3
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ea, all -> 0x00df }
            r4.<init>()     // Catch:{ Exception -> 0x00ea, all -> 0x00df }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x00ea, all -> 0x00df }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x00ea, all -> 0x00df }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x00ea, all -> 0x00df }
            goto L_0x008b
        L_0x00a3:
            java.lang.String r2 = com.paypal.android.sdk.w.f5362a     // Catch:{ Exception -> 0x00ea, all -> 0x00df }
            java.lang.String r4 = r1.toString()     // Catch:{ Exception -> 0x00ea, all -> 0x00df }
            com.paypal.android.sdk.aj.a(r2, r4)     // Catch:{ Exception -> 0x00ea, all -> 0x00df }
            com.paypal.android.sdk.cd.a(r3)
            if (r0 == 0) goto L_0x00f0
            r0.disconnect()
            r0 = r1
        L_0x00b5:
            return r0
        L_0x00b6:
            r0 = move-exception
            r1 = r0
            r3 = r2
            r0 = r4
        L_0x00ba:
            android.os.Handler r4 = r10.f5363b     // Catch:{ all -> 0x00e3 }
            android.os.Handler r5 = r10.f5363b     // Catch:{ all -> 0x00e3 }
            r6 = 1
            android.os.Message r1 = android.os.Message.obtain(r5, r6, r1)     // Catch:{ all -> 0x00e3 }
            r4.sendMessage(r1)     // Catch:{ all -> 0x00e3 }
            com.paypal.android.sdk.cd.a(r3)
            if (r2 == 0) goto L_0x00b5
            r2.disconnect()
            goto L_0x00b5
        L_0x00cf:
            r0 = move-exception
            r3 = r2
        L_0x00d1:
            com.paypal.android.sdk.cd.a(r3)
            if (r2 == 0) goto L_0x00d9
            r2.disconnect()
        L_0x00d9:
            throw r0
        L_0x00da:
            r1 = move-exception
            r3 = r2
            r2 = r0
            r0 = r1
            goto L_0x00d1
        L_0x00df:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x00d1
        L_0x00e3:
            r0 = move-exception
            goto L_0x00d1
        L_0x00e5:
            r1 = move-exception
            r3 = r2
            r2 = r0
            r0 = r4
            goto L_0x00ba
        L_0x00ea:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r1
            r1 = r9
            goto L_0x00ba
        L_0x00f0:
            r0 = r1
            goto L_0x00b5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.sdk.w.a(java.lang.String):java.lang.String");
    }

    public void run() {
        if (this.f5363b != null) {
            try {
                this.f5363b.sendMessage(Message.obtain(this.f5363b, 20, this.f5364c));
                String a2 = a(this.f5364c);
                aj.a(f5362a, String.format("%s/%s/%s/%s/Android", this.f5367f.a(), this.f5367f.b(), this.f5366e, this.f5365d));
                this.f5363b.sendMessage(Message.obtain(this.f5363b, 22, a2.toString()));
            } catch (Exception e2) {
                this.f5363b.sendMessage(Message.obtain(this.f5363b, 21, e2));
            } finally {
                af.a().b(this);
            }
        }
    }
}
