package com.paypal.android.sdk;

import java.util.UUID;

public final class cf {

    /* renamed from: a  reason: collision with root package name */
    public eu f4664a = new eu();

    /* renamed from: b  reason: collision with root package name */
    public ds f4665b;

    /* renamed from: c  reason: collision with root package name */
    public String f4666c;

    /* renamed from: d  reason: collision with root package name */
    public di f4667d;

    /* renamed from: e  reason: collision with root package name */
    public du f4668e;

    /* renamed from: f  reason: collision with root package name */
    public cr f4669f;

    /* renamed from: g  reason: collision with root package name */
    public ds f4670g;

    /* renamed from: h  reason: collision with root package name */
    public boolean f4671h;
    public String i;
    private String j;

    public cf() {
        new dx();
        a();
    }

    public final void a() {
        this.j = UUID.randomUUID().toString();
    }

    public final String b() {
        return this.j;
    }

    public final boolean c() {
        return this.f4665b != null && this.f4665b.b();
    }

    public final String toString() {
        return "BackendState(accessTokenState:" + this.f4665b + " loginAccessToken:" + this.f4670g + ")";
    }
}
