package com.paypal.android.sdk;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class dm {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4730a = dm.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static final Map f4731b;

    /* renamed from: c  reason: collision with root package name */
    private static final Set f4732c;

    static {
        HashMap hashMap = new HashMap();
        f4731b = hashMap;
        hashMap.put("c14", "erpg");
        f4731b.put("c25", "page");
        f4731b.put("c26", "link");
        f4731b.put("c27", "pgln");
        f4731b.put("c29", "eccd");
        f4731b.put("c35", "lgin");
        f4731b.put("vers", "vers");
        f4731b.put("c50", "rsta");
        f4731b.put("gn", "pgrp");
        f4731b.put("v49", "mapv");
        f4731b.put("v51", "mcar");
        f4731b.put("v52", "mosv");
        f4731b.put("v53", "mdvs");
        f4731b.put("clid", "clid");
        f4731b.put("apid", "apid");
        f4731b.put("calc", "calc");
        f4731b.put("e", "e");
        f4731b.put("t", "t");
        f4731b.put("g", "g");
        f4731b.put("srce", "srce");
        f4731b.put("vid", "vid");
        f4731b.put("bchn", "bchn");
        f4731b.put("adte", "adte");
        f4731b.put("sv", "sv");
        f4731b.put("dsid", "dsid");
        f4731b.put("bzsr", "bzsr");
        f4731b.put("prid", "prid");
        HashSet hashSet = new HashSet();
        f4732c = hashSet;
        hashSet.add("v25");
        f4732c.add("v31");
        f4732c.add("c37");
    }

    public static as a(as asVar) {
        Map map = asVar.f4570b;
        HashMap hashMap = new HashMap();
        for (String str : map.keySet()) {
            if (!cd.a((CharSequence) str)) {
                if (f4732c.contains(str)) {
                    new StringBuilder("SC key ").append(str).append(" not used in FPTI, skipping");
                } else if (!f4731b.containsKey(str)) {
                    new StringBuilder("No mapping for SC key ").append(str).append(", skipping");
                } else {
                    String str2 = (String) f4731b.get(str);
                    if (str2 != null) {
                        hashMap.put(str2, map.get(str));
                    }
                }
            }
        }
        return new as(asVar.f4569a, hashMap);
    }
}
