package com.paypal.android.sdk.payments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import java.util.ArrayList;
import java.util.HashMap;

class c {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5218a = c.class.getSimpleName();

    /* renamed from: g  reason: collision with root package name */
    private static final Object f5219g = new Object();

    /* renamed from: h  reason: collision with root package name */
    private static c f5220h;

    /* renamed from: b  reason: collision with root package name */
    private final Context f5221b;

    /* renamed from: c  reason: collision with root package name */
    private final HashMap f5222c = new HashMap();

    /* renamed from: d  reason: collision with root package name */
    private final HashMap f5223d = new HashMap();

    /* renamed from: e  reason: collision with root package name */
    private final ArrayList f5224e = new ArrayList();

    /* renamed from: f  reason: collision with root package name */
    private final Handler f5225f;

    private c(Context context) {
        this.f5221b = context;
        this.f5225f = new d(this, context.getMainLooper());
    }

    public static c a(Context context) {
        c cVar;
        synchronized (f5219g) {
            if (f5220h == null) {
                f5220h = new c(context.getApplicationContext());
            }
            cVar = f5220h;
        }
        return cVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        if (r3 >= r4.length) goto L_0x0001;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        r5 = r4[r3];
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0028, code lost:
        if (r1 >= r5.f5300b.size()) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002a, code lost:
        ((com.paypal.android.sdk.payments.f) r5.f5300b.get(r1)).f5305b.onReceive(r8.f5221b, r5.f5299a);
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0042, code lost:
        r3 = r3 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(com.paypal.android.sdk.payments.c r8) {
        /*
            r2 = 0
        L_0x0001:
            java.util.HashMap r1 = r8.f5222c
            monitor-enter(r1)
            java.util.ArrayList r0 = r8.f5224e     // Catch:{ all -> 0x003f }
            int r0 = r0.size()     // Catch:{ all -> 0x003f }
            if (r0 > 0) goto L_0x000e
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            return
        L_0x000e:
            com.paypal.android.sdk.payments.e[] r4 = new com.paypal.android.sdk.payments.e[r0]     // Catch:{ all -> 0x003f }
            java.util.ArrayList r0 = r8.f5224e     // Catch:{ all -> 0x003f }
            r0.toArray(r4)     // Catch:{ all -> 0x003f }
            java.util.ArrayList r0 = r8.f5224e     // Catch:{ all -> 0x003f }
            r0.clear()     // Catch:{ all -> 0x003f }
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            r3 = r2
        L_0x001c:
            int r0 = r4.length
            if (r3 >= r0) goto L_0x0001
            r5 = r4[r3]
            r1 = r2
        L_0x0022:
            java.util.ArrayList r0 = r5.f5300b
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x0042
            java.util.ArrayList r0 = r5.f5300b
            java.lang.Object r0 = r0.get(r1)
            com.paypal.android.sdk.payments.f r0 = (com.paypal.android.sdk.payments.f) r0
            android.content.BroadcastReceiver r0 = r0.f5305b
            android.content.Context r6 = r8.f5221b
            android.content.Intent r7 = r5.f5299a
            r0.onReceive(r6, r7)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0022
        L_0x003f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003f }
            throw r0
        L_0x0042:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.sdk.payments.c.a(com.paypal.android.sdk.payments.c):void");
    }

    public final void a(BroadcastReceiver broadcastReceiver) {
        int i;
        synchronized (this.f5222c) {
            ArrayList arrayList = (ArrayList) this.f5222c.remove(broadcastReceiver);
            if (arrayList != null) {
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    IntentFilter intentFilter = (IntentFilter) arrayList.get(i2);
                    for (int i3 = 0; i3 < intentFilter.countActions(); i3++) {
                        String action = intentFilter.getAction(i3);
                        ArrayList arrayList2 = (ArrayList) this.f5223d.get(action);
                        if (arrayList2 != null) {
                            int i4 = 0;
                            while (i4 < arrayList2.size()) {
                                if (((f) arrayList2.get(i4)).f5305b == broadcastReceiver) {
                                    arrayList2.remove(i4);
                                    i = i4 - 1;
                                } else {
                                    i = i4;
                                }
                                i4 = i + 1;
                            }
                            if (arrayList2.size() <= 0) {
                                this.f5223d.remove(action);
                            }
                        }
                    }
                }
            }
        }
    }

    public final void a(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        synchronized (this.f5222c) {
            f fVar = new f(intentFilter, broadcastReceiver);
            ArrayList arrayList = (ArrayList) this.f5222c.get(broadcastReceiver);
            if (arrayList == null) {
                arrayList = new ArrayList(1);
                this.f5222c.put(broadcastReceiver, arrayList);
            }
            arrayList.add(intentFilter);
            for (int i = 0; i < intentFilter.countActions(); i++) {
                String action = intentFilter.getAction(i);
                ArrayList arrayList2 = (ArrayList) this.f5223d.get(action);
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList(1);
                    this.f5223d.put(action, arrayList2);
                }
                arrayList2.add(fVar);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0115, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.content.Intent r16) {
        /*
            r15 = this;
            java.util.HashMap r13 = r15.f5222c
            monitor-enter(r13)
            java.lang.String r2 = r16.getAction()     // Catch:{ all -> 0x00bd }
            android.content.Context r1 = r15.f5221b     // Catch:{ all -> 0x00bd }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ all -> 0x00bd }
            r0 = r16
            java.lang.String r3 = r0.resolveTypeIfNeeded(r1)     // Catch:{ all -> 0x00bd }
            android.net.Uri r5 = r16.getData()     // Catch:{ all -> 0x00bd }
            java.lang.String r4 = r16.getScheme()     // Catch:{ all -> 0x00bd }
            java.util.Set r6 = r16.getCategories()     // Catch:{ all -> 0x00bd }
            int r1 = r16.getFlags()     // Catch:{ all -> 0x00bd }
            r1 = r1 & 8
            if (r1 == 0) goto L_0x0092
            r1 = 1
            r12 = r1
        L_0x0029:
            if (r12 == 0) goto L_0x004b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            java.lang.String r7 = "Resolving type "
            r1.<init>(r7)     // Catch:{ all -> 0x00bd }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x00bd }
            java.lang.String r7 = " scheme "
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x00bd }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x00bd }
            java.lang.String r7 = " of intent "
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x00bd }
            r0 = r16
            r1.append(r0)     // Catch:{ all -> 0x00bd }
        L_0x004b:
            java.util.HashMap r1 = r15.f5223d     // Catch:{ all -> 0x00bd }
            java.lang.String r7 = r16.getAction()     // Catch:{ all -> 0x00bd }
            java.lang.Object r1 = r1.get(r7)     // Catch:{ all -> 0x00bd }
            r0 = r1
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x00bd }
            r8 = r0
            if (r8 == 0) goto L_0x0114
            if (r12 == 0) goto L_0x0067
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            java.lang.String r7 = "Action list: "
            r1.<init>(r7)     // Catch:{ all -> 0x00bd }
            r1.append(r8)     // Catch:{ all -> 0x00bd }
        L_0x0067:
            r10 = 0
            r1 = 0
            r11 = r1
        L_0x006a:
            int r1 = r8.size()     // Catch:{ all -> 0x00bd }
            if (r11 >= r1) goto L_0x00df
            java.lang.Object r1 = r8.get(r11)     // Catch:{ all -> 0x00bd }
            r0 = r1
            com.paypal.android.sdk.payments.f r0 = (com.paypal.android.sdk.payments.f) r0     // Catch:{ all -> 0x00bd }
            r9 = r0
            if (r12 == 0) goto L_0x0086
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            java.lang.String r7 = "Matching against filter "
            r1.<init>(r7)     // Catch:{ all -> 0x00bd }
            android.content.IntentFilter r7 = r9.f5304a     // Catch:{ all -> 0x00bd }
            r1.append(r7)     // Catch:{ all -> 0x00bd }
        L_0x0086:
            boolean r1 = r9.f5306c     // Catch:{ all -> 0x00bd }
            if (r1 == 0) goto L_0x0095
            if (r12 == 0) goto L_0x00d1
            r1 = r10
        L_0x008d:
            int r7 = r11 + 1
            r11 = r7
            r10 = r1
            goto L_0x006a
        L_0x0092:
            r1 = 0
            r12 = r1
            goto L_0x0029
        L_0x0095:
            android.content.IntentFilter r1 = r9.f5304a     // Catch:{ all -> 0x00bd }
            java.lang.String r7 = "ClonedLocalBroadcastManager"
            int r1 = r1.match(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00bd }
            if (r1 < 0) goto L_0x00c0
            if (r12 == 0) goto L_0x00af
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            java.lang.String r14 = "  Filter matched!  match=0x"
            r7.<init>(r14)     // Catch:{ all -> 0x00bd }
            java.lang.String r1 = java.lang.Integer.toHexString(r1)     // Catch:{ all -> 0x00bd }
            r7.append(r1)     // Catch:{ all -> 0x00bd }
        L_0x00af:
            if (r10 != 0) goto L_0x0117
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x00bd }
            r1.<init>()     // Catch:{ all -> 0x00bd }
        L_0x00b6:
            r1.add(r9)     // Catch:{ all -> 0x00bd }
            r7 = 1
            r9.f5306c = r7     // Catch:{ all -> 0x00bd }
            goto L_0x008d
        L_0x00bd:
            r1 = move-exception
            monitor-exit(r13)     // Catch:{ all -> 0x00bd }
            throw r1
        L_0x00c0:
            if (r12 == 0) goto L_0x00d1
            switch(r1) {
                case -4: goto L_0x00d6;
                case -3: goto L_0x00d3;
                case -2: goto L_0x00d9;
                case -1: goto L_0x00dc;
                default: goto L_0x00c5;
            }
        L_0x00c5:
            java.lang.String r1 = "unknown reason"
        L_0x00c7:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            java.lang.String r9 = "  Filter did not match: "
            r7.<init>(r9)     // Catch:{ all -> 0x00bd }
            r7.append(r1)     // Catch:{ all -> 0x00bd }
        L_0x00d1:
            r1 = r10
            goto L_0x008d
        L_0x00d3:
            java.lang.String r1 = "action"
            goto L_0x00c7
        L_0x00d6:
            java.lang.String r1 = "category"
            goto L_0x00c7
        L_0x00d9:
            java.lang.String r1 = "data"
            goto L_0x00c7
        L_0x00dc:
            java.lang.String r1 = "type"
            goto L_0x00c7
        L_0x00df:
            if (r10 == 0) goto L_0x0114
            r1 = 0
            r2 = r1
        L_0x00e3:
            int r1 = r10.size()     // Catch:{ all -> 0x00bd }
            if (r2 >= r1) goto L_0x00f6
            java.lang.Object r1 = r10.get(r2)     // Catch:{ all -> 0x00bd }
            com.paypal.android.sdk.payments.f r1 = (com.paypal.android.sdk.payments.f) r1     // Catch:{ all -> 0x00bd }
            r3 = 0
            r1.f5306c = r3     // Catch:{ all -> 0x00bd }
            int r1 = r2 + 1
            r2 = r1
            goto L_0x00e3
        L_0x00f6:
            java.util.ArrayList r1 = r15.f5224e     // Catch:{ all -> 0x00bd }
            com.paypal.android.sdk.payments.e r2 = new com.paypal.android.sdk.payments.e     // Catch:{ all -> 0x00bd }
            r0 = r16
            r2.<init>(r0, r10)     // Catch:{ all -> 0x00bd }
            r1.add(r2)     // Catch:{ all -> 0x00bd }
            android.os.Handler r1 = r15.f5225f     // Catch:{ all -> 0x00bd }
            r2 = 1
            boolean r1 = r1.hasMessages(r2)     // Catch:{ all -> 0x00bd }
            if (r1 != 0) goto L_0x0111
            android.os.Handler r1 = r15.f5225f     // Catch:{ all -> 0x00bd }
            r2 = 1
            r1.sendEmptyMessage(r2)     // Catch:{ all -> 0x00bd }
        L_0x0111:
            r1 = 1
            monitor-exit(r13)     // Catch:{ all -> 0x00bd }
        L_0x0113:
            return r1
        L_0x0114:
            monitor-exit(r13)     // Catch:{ all -> 0x00bd }
            r1 = 0
            goto L_0x0113
        L_0x0117:
            r1 = r10
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.sdk.payments.c.a(android.content.Intent):boolean");
    }
}
