package com.paypal.android.sdk;

public class ai {

    /* renamed from: a  reason: collision with root package name */
    public static final ai f4543a = new ai();

    /* renamed from: b  reason: collision with root package name */
    private static final String f4544b = ai.class.getSimpleName();

    /* renamed from: c  reason: collision with root package name */
    private byte[] f4545c;

    private ai() {
        this.f4545c = null;
        this.f4545c = null;
    }

    public ai(String str) {
        this.f4545c = null;
        try {
            this.f4545c = new byte[((str.length() + 1) / 2)];
            int i = 0;
            int length = str.length() - 1;
            while (length >= 0) {
                int i2 = length - 1;
                this.f4545c[i] = (byte) Integer.parseInt(str.substring(i2 < 0 ? 0 : i2, length + 1), 16);
                length -= 2;
                i++;
            }
        } catch (Exception e2) {
            aj.a(f4544b, "PPRiskDataBitSet initialize failed", e2);
            this.f4545c = null;
        }
    }

    public final boolean a(bl blVar) {
        int a2 = blVar.a() / 8;
        if (this.f4545c == null) {
            return true;
        }
        if (a2 >= this.f4545c.length) {
            return false;
        }
        byte b2 = this.f4545c[a2];
        byte a3 = (byte) (1 << (blVar.a() % 8));
        return (b2 & a3) == a3;
    }
}
