package com.paypal.android.sdk;

import android.util.Base64;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public final class cv implements az {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4687a = cv.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final String f4688b;

    public cv(String str) {
        this.f4688b = str;
    }

    private static String a(Exception exc) {
        Log.e(f4687a, exc.getMessage());
        return null;
    }

    public final String a(String str) {
        if (str == null) {
            return null;
        }
        try {
            SecretKey generateSecret = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(this.f4688b.getBytes("UTF8")));
            byte[] bytes = str.getBytes("UTF8");
            Cipher instance = Cipher.getInstance("DES");
            instance.init(1, generateSecret);
            return Base64.encodeToString(instance.doFinal(bytes), 0);
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e2) {
            return a(e2);
        }
    }

    public final String b(String str) {
        if (str == null) {
            return null;
        }
        try {
            SecretKey generateSecret = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(this.f4688b.getBytes("UTF8")));
            byte[] decode = Base64.decode(str, 0);
            Cipher instance = Cipher.getInstance("DES");
            instance.init(2, generateSecret);
            return new String(instance.doFinal(decode));
        } catch (UnsupportedEncodingException | IllegalArgumentException | InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e2) {
            return a(e2);
        }
    }
}
