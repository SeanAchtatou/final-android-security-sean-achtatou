package com.paypal.android.sdk;

import java.util.Collection;

public enum df {
    GET_FUNDING_OPTIONS("https://uri.paypal.com/services/payments/funding-options"),
    PAYMENT_CODE("https://uri.paypal.com/services/pos/payments"),
    MIS_CUSTOMER("https://uri.paypal.com/services/mis/customer"),
    FINANCIAL_INSTRUMENTS("https://uri.paypal.com/services/wallet/financial-instruments/view"),
    SEND_MONEY("https://uri.paypal.com/services/wallet/sendmoney"),
    REQUEST_MONEY("https://uri.paypal.com/services/wallet/money-request/requests"),
    LOYALTY("https://uri.paypal.com/services/loyalty/memberships/update"),
    EXPRESS_CHECKOUT("https://uri.paypal.com/services/expresscheckout");
    
    public static final Collection i = new dg();
    private String j;

    private df(String str) {
        this.j = str;
    }

    public final String a() {
        return this.j;
    }
}
