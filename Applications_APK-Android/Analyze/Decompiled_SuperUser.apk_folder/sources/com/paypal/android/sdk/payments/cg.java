package com.paypal.android.sdk.payments;

import android.app.AlertDialog;
import android.view.View;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.fg;
import com.paypal.android.sdk.fs;
import java.util.ArrayList;

final class cg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ fg f5233a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ArrayList f5234b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ PaymentConfirmActivity f5235c;

    cg(PaymentConfirmActivity paymentConfirmActivity, fg fgVar, ArrayList arrayList) {
        this.f5235c = paymentConfirmActivity;
        this.f5233a = fgVar;
        this.f5234b = arrayList;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(ev.a(fs.SHIPPING_ADDRESS)).setAdapter(this.f5233a, new ch(this));
        builder.create().show();
    }
}
