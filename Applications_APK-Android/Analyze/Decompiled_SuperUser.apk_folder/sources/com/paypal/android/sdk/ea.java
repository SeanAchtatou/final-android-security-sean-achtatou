package com.paypal.android.sdk;

import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class ea extends em {

    /* renamed from: b  reason: collision with root package name */
    private String f4784b;

    /* renamed from: c  reason: collision with root package name */
    private String f4785c;

    /* renamed from: d  reason: collision with root package name */
    private String f4786d;

    /* renamed from: e  reason: collision with root package name */
    private int f4787e;

    /* renamed from: f  reason: collision with root package name */
    private int f4788f;

    /* renamed from: g  reason: collision with root package name */
    private String f4789g;

    /* renamed from: h  reason: collision with root package name */
    private String f4790h;
    private String i;
    private String j;

    public ea(bu buVar, x xVar, String str, String str2, String str3, String str4, en enVar, Map map, ei[] eiVarArr, String str5, boolean z, String str6, String str7, String str8) {
        super(db.CreditCardPaymentRequest, buVar, xVar, str, str2, str4, enVar, map, eiVarArr, str5, z, str6, str7, str8);
        this.f4789g = str3;
    }

    public ea(bu buVar, x xVar, String str, String str2, String str3, String str4, String str5, int i2, int i3, String str6, en enVar, Map map, ei[] eiVarArr, String str7, boolean z, String str8, String str9, String str10) {
        super(db.CreditCardPaymentRequest, buVar, xVar, str, str2, null, enVar, map, eiVarArr, str7, z, str8, str9, str10);
        this.f4784b = str3;
        this.f4785c = str4;
        this.f4786d = str5;
        this.f4787e = i2;
        this.f4788f = i3;
    }

    /* access modifiers changed from: package-private */
    public final void a(JSONObject jSONObject) {
        if (cd.b((CharSequence) this.f4790h)) {
            jSONObject.accumulate("invoice_number", this.f4790h);
        }
        if (cd.b((CharSequence) this.i)) {
            jSONObject.accumulate("custom", this.i);
        }
        if (cd.b((CharSequence) this.j)) {
            jSONObject.accumulate("soft_descriptor", this.j);
        }
    }

    public final ea d(String str) {
        this.f4790h = str;
        return this;
    }

    public final ea e(String str) {
        this.i = str;
        return this;
    }

    public final String e() {
        String a2 = cy.a(B().a().doubleValue(), B().b());
        return "{\"id\":\"PAY-6RV70583SB702805EKEYSZ6Y\",\"intent\":\"sale\",\"create_time\": \"2014-02-12T22:29:49Z\",\"update_time\": \"2014-02-12T22:29:50Z\",\"payer\":{\"funding_instruments\":[{\"credit_card\":{\"expire_month\":\"" + this.f4787e + "\",\"expire_year\":\"" + this.f4788f + "\",\"number\":\"" + (this.f4784b != null ? this.f4785c.substring(this.f4785c.length() - 4) : "xxxxxxxxxx1111") + "\",\"type\":\"VISA\"}}],\"payment_method\":\"credit_card\"},\"state\":\"approved\",\"transactions\":[{\"amount\":{\"currency\":\"USD\",\"total\":\"" + a2 + "\"},\"description\":\"I am a sanity check payment.\",\"item_list\":{},\"payee\":{\"merchant_id\":\"PKKPTJKL75YDS\"},\"related_resources\":[{\"sale\":{\"amount\":{\"currency\":\"USD\",\"total\":\"" + a2 + "\"},\"id\":\"0EW02334X44816642\",\"parent_payment\":\"PAY-123456789012345689\",\"state\":\"completed\"}}]}]}";
    }

    public final ea f(String str) {
        this.j = str;
        return this;
    }

    public final String t() {
        return this.f4785c;
    }

    public final String u() {
        return this.f4784b;
    }

    public final String v() {
        return this.f4786d;
    }

    public final int w() {
        return this.f4787e;
    }

    public final int x() {
        return this.f4788f;
    }

    /* access modifiers changed from: protected */
    public final JSONArray y() {
        JSONArray jSONArray = new JSONArray();
        if (this.f4784b != null) {
            JSONObject jSONObject = new JSONObject();
            jSONObject.accumulate("cvv2", this.f4786d);
            jSONObject.accumulate("expire_month", Integer.valueOf(this.f4787e));
            jSONObject.accumulate("expire_year", Integer.valueOf(this.f4788f));
            jSONObject.accumulate("number", this.f4785c);
            jSONObject.accumulate("type", this.f4784b);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.accumulate("credit_card", jSONObject);
            jSONArray.put(jSONObject2);
        } else {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.accumulate("payer_id", this.f4818a);
            jSONObject3.accumulate("credit_card_id", this.f4789g);
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.accumulate("credit_card_token", jSONObject3);
            jSONArray.put(jSONObject4);
        }
        return jSONArray;
    }

    /* access modifiers changed from: protected */
    public final String z() {
        return "credit_card";
    }
}
