package com.paypal.android.sdk;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public final class fu {

    /* renamed from: a  reason: collision with root package name */
    public LinearLayout f4943a = this.f4945c;

    /* renamed from: b  reason: collision with root package name */
    public TextView f4944b;

    /* renamed from: c  reason: collision with root package name */
    private LinearLayout f4945c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, int):void
     arg types: [android.widget.TextView, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.bw.a(android.view.View, int, float):void
     arg types: [android.widget.TextView, int, int]
     candidates:
      com.paypal.android.sdk.bw.a(java.lang.String, android.content.Context, int):android.graphics.Bitmap
      com.paypal.android.sdk.bw.a(android.content.Context, java.lang.String, java.lang.String):android.widget.ImageView
      com.paypal.android.sdk.bw.a(android.view.View, int, int):void
      com.paypal.android.sdk.bw.a(android.view.View, int, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, int):void
      com.paypal.android.sdk.bw.a(android.view.View, java.lang.String, java.lang.String):void
      com.paypal.android.sdk.bw.a(android.view.View, boolean, android.content.Context):void
      com.paypal.android.sdk.bw.a(android.view.View, int, float):void */
    public fu(Context context) {
        this.f4945c = new LinearLayout(context);
        this.f4945c.setOrientation(0);
        this.f4944b = new TextView(context);
        this.f4944b.setText("server");
        this.f4944b.setTextColor(-1);
        this.f4944b.setBackgroundColor(bv.f4636e);
        this.f4944b.setGravity(17);
        this.f4945c.addView(this.f4944b);
        bw.a(this.f4944b, "8dip", "8dip", "8dip", "8dip");
        bw.a((View) this.f4944b, -2, -2);
        bw.b(this.f4944b, null, "15dip", null, "15dip");
        bw.a((View) this.f4944b, 1, 1.0f);
    }
}
