package com.paypal.android.sdk;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Random;

public final class dx extends cp implements Parcelable {
    public static final Parcelable.Creator CREATOR = new cq();

    public dx() {
        this.f4682b = System.currentTimeMillis() + 1800000;
        Random random = new Random(this.f4682b);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            sb.append((char) ((Math.abs(random.nextInt()) % 10) + 48));
        }
        this.f4681a = sb.toString();
    }

    public dx(Parcel parcel) {
        super(parcel);
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f4681a);
        parcel.writeLong(this.f4682b);
    }
}
