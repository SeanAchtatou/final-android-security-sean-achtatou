package com.paypal.android.sdk;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RectShape;

public final class bv {
    private static int A = y;
    private static int B = f4632a;
    private static int C = Color.parseColor("#c5ddeb");
    private static int D = Color.parseColor("#717074");
    private static int E = Color.parseColor("#aa717074");
    private static int F = Color.parseColor("#5a5a5d");
    private static int G = Color.parseColor("#f5f5f5");

    /* renamed from: a  reason: collision with root package name */
    public static final int f4632a = Color.parseColor("#003087");

    /* renamed from: b  reason: collision with root package name */
    public static final int f4633b = Color.parseColor("#009CDE");

    /* renamed from: c  reason: collision with root package name */
    public static final Drawable f4634c = new ColorDrawable(Color.parseColor("#717074"));

    /* renamed from: d  reason: collision with root package name */
    public static final int f4635d = Color.parseColor("#f5f5f5");

    /* renamed from: e  reason: collision with root package name */
    public static final int f4636e = Color.parseColor("#c4dceb");

    /* renamed from: f  reason: collision with root package name */
    public static final int f4637f = Color.parseColor("#e5e5e5");

    /* renamed from: g  reason: collision with root package name */
    public static final int f4638g = Color.parseColor("#515151");

    /* renamed from: h  reason: collision with root package name */
    public static final int f4639h = Color.parseColor("#797979");
    public static final int i = f4638g;
    public static final int j = f4638g;
    public static final int k = f4638g;
    public static final int l = f4639h;
    public static final Typeface m = Typeface.create("sans-serif-light", 0);
    public static final Typeface n = Typeface.create("sans-serif-light", 0);
    public static final Typeface o = Typeface.create("sans-serif-bold", 0);
    public static final Typeface p = Typeface.create("sans-serif", 2);
    public static final Typeface q = Typeface.create("sans-serif-light", 0);
    public static final Typeface r = Typeface.create("sans-serif", 0);
    public static final Typeface s = Typeface.create("sans-serif-light", 0);
    public static final ColorStateList t = new ColorStateList(new int[][]{u, v}, new int[]{B, z});
    private static int[] u = {16842919, 16842910};
    private static int[] v = {16842910};
    private static int[] w = {-16842910};
    private static int[] x = {16842908};
    private static int y = Color.parseColor("#aa009CDE");
    private static int z = f4633b;

    static {
        Color.parseColor("#aa003087");
        Color.parseColor("#333333");
        Color.parseColor("#b32317");
        Typeface.create("sans-serif-light", 0);
    }

    private static Drawable a(int i2, float f2) {
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RectShape());
        shapeDrawable.getPaint().setStrokeWidth(2.0f * f2);
        shapeDrawable.getPaint().setStyle(Paint.Style.STROKE);
        shapeDrawable.getPaint().setColor(f4635d);
        return new LayerDrawable(new Drawable[]{new ColorDrawable(i2), shapeDrawable});
    }

    private static Drawable a(int i2, int i3, float f2) {
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RectShape());
        shapeDrawable.getPaint().setStrokeWidth(2.0f * f2);
        shapeDrawable.getPaint().setStyle(Paint.Style.STROKE);
        shapeDrawable.getPaint().setColor(f4635d);
        ShapeDrawable shapeDrawable2 = new ShapeDrawable(new RectShape());
        shapeDrawable2.getPaint().setStrokeWidth(f2);
        shapeDrawable2.getPaint().setStyle(Paint.Style.STROKE);
        shapeDrawable2.getPaint().setColor(i3);
        return new LayerDrawable(new Drawable[]{new ColorDrawable(i2), shapeDrawable, shapeDrawable2});
    }

    public static Drawable a(Context context) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(u, new ColorDrawable(B));
        stateListDrawable.addState(w, new ColorDrawable(C));
        stateListDrawable.addState(x, a(z, A, d(context)));
        stateListDrawable.addState(v, a(z, d(context)));
        return stateListDrawable;
    }

    public static Drawable b(Context context) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(u, new ColorDrawable(F));
        stateListDrawable.addState(w, new ColorDrawable(G));
        stateListDrawable.addState(x, a(D, E, d(context)));
        stateListDrawable.addState(v, a(D, d(context)));
        return stateListDrawable;
    }

    protected static Drawable c(Context context) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(x, a(0, A, d(context)));
        stateListDrawable.addState(v, new ColorDrawable(0));
        return stateListDrawable;
    }

    private static float d(Context context) {
        return context.getResources().getDisplayMetrics().density * (bw.b("4dip", context) / 2.0f);
    }
}
