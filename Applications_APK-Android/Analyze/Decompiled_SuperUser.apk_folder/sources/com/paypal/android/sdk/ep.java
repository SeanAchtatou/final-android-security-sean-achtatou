package com.paypal.android.sdk;

import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ep extends eh {

    /* renamed from: a  reason: collision with root package name */
    public String f4829a;

    /* renamed from: b  reason: collision with root package name */
    public String f4830b;

    /* renamed from: c  reason: collision with root package name */
    public long f4831c;

    /* renamed from: d  reason: collision with root package name */
    public boolean f4832d;

    public ep(String str, bu buVar, x xVar, String str2) {
        super(db.PreAuthRequest, buVar, xVar, b(str, str2));
    }

    public final String b() {
        HashMap hashMap = new HashMap();
        hashMap.put("response_type", "token");
        hashMap.put("grant_type", "client_credentials");
        hashMap.put("return_authn_schemes", "true");
        hashMap.put("device_info", cd.a(da.a().toString()));
        hashMap.put("app_info", cd.a(cw.a().toString()));
        hashMap.put("risk_data", cd.a(r.a().c().toString()));
        return cd.a(hashMap);
    }

    public final void c() {
        JSONObject m = m();
        try {
            this.f4829a = m.getString("access_token");
            this.f4830b = m.getString("scope");
            this.f4831c = m.getLong("expires_in");
            JSONArray jSONArray = m.getJSONArray("supported_authn_schemes");
            for (int i = 0; i < jSONArray.length(); i++) {
                if (jSONArray.get(i).equals("phone_pin")) {
                    this.f4832d = true;
                }
            }
        } catch (JSONException e2) {
            b(m);
        }
    }

    public final void d() {
        b(m());
    }

    public final String e() {
        return "{\"scope\":\"https://api.paypal.com/v1/payments/.* https://api.paypal.com/v1/vault/credit-card https://api.paypal.com/v1/vault/credit-card/.* openid\",\"access_token\":\"iPVzWUwTo1fEG6n.2sTZCahv8wa2b8uGpBs1KZ-6XxA\",\"token_type\":\"Bearer\",\"expires_in\":900,\"supported_authn_schemes\": [ \"email_password\", \"phone_pin\" ]}";
    }
}
