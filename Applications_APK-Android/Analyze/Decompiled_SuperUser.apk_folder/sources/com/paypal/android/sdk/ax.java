package com.paypal.android.sdk;

public enum ax {
    UNKNOWN(0),
    PAYPAL(10),
    EBAY(11),
    MSDK(12);
    

    /* renamed from: e  reason: collision with root package name */
    private int f4581e;

    private ax(int i) {
        this.f4581e = i;
    }

    public final int a() {
        return this.f4581e;
    }
}
