package com.paypal.android.sdk;

import android.content.Intent;
import android.os.Bundle;

public class cl extends l {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4680a = cl.class.getSimpleName();

    public final Intent a(String str, dn dnVar, Cdo doVar, String str2) {
        Intent a2 = a("com.paypal.android.p2pmobile.Sdk", "com.paypal.android.lib.authenticator.activity.SdkActivity");
        Bundle bundle = new Bundle();
        bundle.putString("target_client_id", str);
        if (dnVar != null) {
            bundle.putString("token_request_type", dnVar.toString());
        }
        if (doVar != null) {
            bundle.putString("response_type", doVar.toString());
        }
        bundle.putString("app_guid", str2);
        new StringBuilder("launching authenticator with bundle:").append(bundle);
        a2.putExtras(bundle);
        return a2;
    }
}
