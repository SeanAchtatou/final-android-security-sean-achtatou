package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;

final class at implements Parcelable.Creator {
    at() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new PayPalOAuthScopes(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new PayPalOAuthScopes[i];
    }
}
