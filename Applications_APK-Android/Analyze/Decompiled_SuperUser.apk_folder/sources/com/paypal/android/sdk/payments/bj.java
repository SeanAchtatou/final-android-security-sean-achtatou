package com.paypal.android.sdk.payments;

import com.paypal.android.sdk.bx;

final class bj {

    /* renamed from: a  reason: collision with root package name */
    Integer f5202a;

    /* renamed from: b  reason: collision with root package name */
    String f5203b;

    bj(PayPalService payPalService, String str, Integer num, String str2) {
        this.f5203b = str;
        this.f5202a = num;
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.f5202a != null && this.f5202a.equals(401);
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return a() && this.f5203b.equals("2fa_required");
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        return this.f5203b.equals(bx.SERVER_COMMUNICATION_ERROR.toString());
    }
}
