package com.paypal.android.sdk;

import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public final class ab extends ae {

    /* renamed from: a  reason: collision with root package name */
    private String f4528a;

    /* renamed from: b  reason: collision with root package name */
    private HashMap f4529b;

    /* renamed from: c  reason: collision with root package name */
    private Map f4530c = new HashMap();

    /* renamed from: d  reason: collision with root package name */
    private Handler f4531d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f4532e;

    static {
        r.class.getSimpleName();
    }

    public ab(String str, HashMap hashMap, Handler handler, boolean z) {
        this.f4528a = str;
        this.f4529b = hashMap;
        this.f4531d = handler;
        this.f4532e = z;
    }

    public final void run() {
        boolean z;
        boolean z2 = true;
        if (this.f4531d != null) {
            try {
                this.f4531d.sendMessage(Message.obtain(this.f4531d, 0, this.f4528a));
                if (!this.f4532e) {
                    this.f4530c.put("CLIENT-AUTH", "No cert");
                }
                this.f4530c.put("X-PAYPAL-RESPONSE-DATA-FORMAT", "NV");
                this.f4530c.put("X-PAYPAL-REQUEST-DATA-FORMAT", "NV");
                this.f4530c.put("X-PAYPAL-SERVICE-VERSION", "1.0.0");
                if (this.f4532e) {
                    y a2 = r.f5348a.a();
                    a2.a(Uri.parse(this.f4528a));
                    a2.a(this.f4530c);
                    HashMap hashMap = this.f4529b;
                    StringBuilder sb = new StringBuilder();
                    for (Map.Entry entry : hashMap.entrySet()) {
                        if (z2) {
                            z = false;
                        } else {
                            sb.append("&");
                            z = z2;
                        }
                        sb.append(URLEncoder.encode((String) entry.getKey(), "UTF-8"));
                        sb.append("=");
                        sb.append(URLEncoder.encode((String) entry.getValue(), "UTF-8"));
                        z2 = z;
                    }
                    int a3 = a2.a(sb.toString().getBytes("UTF-8"));
                    if (a3 != 200) {
                        throw new Exception("Network Connection Error with wrong http code: " + a3);
                    }
                    this.f4531d.sendMessage(Message.obtain(this.f4531d, 2, new String(a2.a(), "UTF-8")));
                } else {
                    this.f4531d.sendMessage(Message.obtain(this.f4531d, 2, "not supported"));
                }
            } catch (Exception e2) {
                this.f4531d.sendMessage(Message.obtain(this.f4531d, 1, e2));
            } finally {
                af.a().b(this);
            }
        }
    }
}
