package com.paypal.android.sdk.payments;

import com.paypal.android.sdk.du;

final class dk {

    /* renamed from: a  reason: collision with root package name */
    public final String f5264a;

    /* renamed from: b  reason: collision with root package name */
    public final du f5265b;

    /* renamed from: c  reason: collision with root package name */
    public final String f5266c;

    public dk(String str, du duVar, String str2) {
        this.f5264a = str;
        this.f5265b = duVar;
        this.f5266c = str2;
    }
}
