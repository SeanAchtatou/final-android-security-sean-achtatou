package com.paypal.android.sdk;

import android.text.TextUtils;

public abstract class bs extends bt {
    static {
        bs.class.getSimpleName();
    }

    public bs(br brVar, bu buVar, x xVar, String str) {
        this(brVar, buVar, xVar, str, null);
    }

    public bs(br brVar, bu buVar, x xVar, String str, String str2) {
        super(brVar, buVar, xVar, str2);
        if (!TextUtils.isEmpty(str)) {
            a("Authorization", str);
        }
    }
}
