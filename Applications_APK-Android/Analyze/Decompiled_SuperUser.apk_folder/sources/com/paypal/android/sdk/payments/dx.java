package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;
import com.paypal.android.sdk.dz;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class dx implements Parcelable {
    public static final Parcelable.Creator CREATOR = new db();

    /* renamed from: a  reason: collision with root package name */
    private JSONArray f5289a;

    /* renamed from: b  reason: collision with root package name */
    private ShippingAddress f5290b;

    /* renamed from: c  reason: collision with root package name */
    private int f5291c = -1;

    /* renamed from: d  reason: collision with root package name */
    private int f5292d;

    /* renamed from: e  reason: collision with root package name */
    private JSONObject f5293e;

    /* renamed from: f  reason: collision with root package name */
    private JSONArray f5294f;

    /* renamed from: g  reason: collision with root package name */
    private int f5295g = -1;

    /* renamed from: h  reason: collision with root package name */
    private String f5296h;
    private String i;

    public dx(Parcel parcel) {
        if (parcel != null) {
            try {
                String readString = parcel.readString();
                if (readString != null) {
                    this.f5289a = new JSONArray(readString);
                } else {
                    this.f5289a = null;
                }
            } catch (JSONException e2) {
                this.f5289a = null;
            }
            this.f5290b = (ShippingAddress) parcel.readParcelable(ShippingAddress.class.getClassLoader());
            try {
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    this.f5293e = new JSONObject(readString2);
                } else {
                    this.f5293e = null;
                }
            } catch (JSONException e3) {
                this.f5293e = null;
            }
            try {
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    this.f5294f = new JSONArray(readString3);
                } else {
                    this.f5294f = null;
                }
            } catch (JSONException e4) {
                this.f5294f = null;
            }
            this.f5296h = parcel.readString();
            this.i = parcel.readString();
            this.f5295g = parcel.readInt();
            this.f5291c = parcel.readInt();
            this.f5292d = parcel.readInt();
        }
    }

    public dx(dz dzVar, ShippingAddress shippingAddress) {
        this.f5289a = dzVar.v();
        this.f5293e = dzVar.w();
        this.f5294f = dzVar.x();
        this.f5296h = dzVar.t();
        this.i = dzVar.u();
        this.f5290b = shippingAddress;
        if (this.f5290b != null) {
            this.f5291c = 0;
            this.f5292d = a(this.f5290b, this.f5289a);
            return;
        }
        this.f5291c = a(this.f5289a);
        this.f5292d = -1;
    }

    private static int a(ShippingAddress shippingAddress, JSONArray jSONArray) {
        if (!(shippingAddress == null || jSONArray == null)) {
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                if (shippingAddress.a(jSONArray.optJSONObject(i2))) {
                    return i2;
                }
            }
        }
        return -1;
    }

    private static int a(JSONArray jSONArray) {
        if (jSONArray == null) {
            return -1;
        }
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            if (jSONArray.optJSONObject(i2).optBoolean("default_address", false)) {
                return i2;
            }
        }
        return 0;
    }

    public final JSONArray a() {
        return this.f5289a;
    }

    public final void a(int i2) {
        this.f5295g = i2;
    }

    public final ShippingAddress b() {
        return this.f5290b;
    }

    public final void b(int i2) {
        this.f5291c = i2;
    }

    public final JSONObject c() {
        return this.f5293e;
    }

    public final JSONArray d() {
        return this.f5294f;
    }

    public final int describeContents() {
        return 0;
    }

    public final String e() {
        return this.f5296h;
    }

    public final String f() {
        return this.i;
    }

    public final int g() {
        if (this.f5295g < 0) {
            return 0;
        }
        return this.f5295g;
    }

    public final int h() {
        if (this.f5291c < 0) {
            return 0;
        }
        return this.f5291c;
    }

    public final int i() {
        return this.f5292d;
    }

    public final boolean j() {
        return this.f5295g != -1;
    }

    public final boolean k() {
        return this.f5291c != -1;
    }

    public final JSONObject l() {
        if (this.f5295g <= 0) {
            return null;
        }
        return this.f5294f.optJSONObject(this.f5295g - 1);
    }

    public final JSONObject m() {
        if (this.f5291c < 0) {
            return null;
        }
        if (this.f5290b == null) {
            return this.f5289a.optJSONObject(this.f5291c);
        }
        if (this.f5291c == 0) {
            return this.f5292d < 0 ? this.f5290b.a() : this.f5289a.optJSONObject(this.f5292d);
        }
        int i2 = this.f5291c - 1;
        if (this.f5292d >= 0 && i2 >= this.f5292d) {
            i2++;
        }
        return this.f5289a.optJSONObject(i2);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        String str = null;
        parcel.writeString(this.f5289a != null ? this.f5289a.toString() : null);
        parcel.writeParcelable(this.f5290b, 0);
        parcel.writeString(this.f5293e != null ? this.f5293e.toString() : null);
        if (this.f5294f != null) {
            str = this.f5294f.toString();
        }
        parcel.writeString(str);
        parcel.writeString(this.f5296h);
        parcel.writeString(this.i);
        parcel.writeInt(this.f5295g);
        parcel.writeInt(this.f5291c);
        parcel.writeInt(this.f5292d);
    }
}
