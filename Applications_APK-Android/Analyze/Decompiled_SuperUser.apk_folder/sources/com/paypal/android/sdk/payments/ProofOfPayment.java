package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.paypal.android.sdk.cd;
import org.json.JSONException;
import org.json.JSONObject;

public final class ProofOfPayment implements Parcelable {
    public static final Parcelable.Creator CREATOR = new da();

    /* renamed from: a  reason: collision with root package name */
    private static final String f5143a = ProofOfPayment.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private String f5144b;

    /* renamed from: c  reason: collision with root package name */
    private String f5145c;

    /* renamed from: d  reason: collision with root package name */
    private String f5146d;

    /* renamed from: e  reason: collision with root package name */
    private String f5147e;

    /* renamed from: f  reason: collision with root package name */
    private String f5148f;

    private ProofOfPayment(Parcel parcel) {
        this(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
    }

    /* synthetic */ ProofOfPayment(Parcel parcel, byte b2) {
        this(parcel);
    }

    ProofOfPayment(String str, String str2, String str3, String str4, String str5) {
        this.f5144b = str;
        this.f5145c = str2;
        this.f5146d = str3;
        this.f5147e = str4;
        this.f5148f = str5;
        new StringBuilder("ProofOfPayment created: ").append(toString());
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("create_time", this.f5146d);
            jSONObject.put("id", this.f5145c);
            jSONObject.put("intent", this.f5147e);
            jSONObject.put("state", this.f5144b);
            if (!cd.b((CharSequence) this.f5148f) || !cd.b((CharSequence) this.f5147e)) {
                return jSONObject;
            }
            if (this.f5147e.equals("authorize")) {
                jSONObject.put("authorization_id", this.f5148f);
                return jSONObject;
            } else if (!this.f5147e.equals("order")) {
                return jSONObject;
            } else {
                jSONObject.put("order_id", this.f5148f);
                return jSONObject;
            }
        } catch (JSONException e2) {
            Log.e(f5143a, "error encoding JSON", e2);
            return null;
        }
    }

    public final int describeContents() {
        return 0;
    }

    public final String toString() {
        return "{" + this.f5147e + ": " + (cd.b(this.f5148f) ? this.f5148f : "no transactionId") + "}";
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f5144b);
        parcel.writeString(this.f5145c);
        parcel.writeString(this.f5146d);
        parcel.writeString(this.f5147e);
        parcel.writeString(this.f5148f);
    }
}
