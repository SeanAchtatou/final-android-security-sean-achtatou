package com.paypal.android.sdk;

import java.util.HashMap;
import java.util.Map;

public final class ao {

    /* renamed from: a  reason: collision with root package name */
    private String f4558a;

    /* renamed from: b  reason: collision with root package name */
    private String f4559b;

    /* renamed from: c  reason: collision with root package name */
    private Map f4560c = new HashMap();

    public ao(String str, String str2) {
        this.f4558a = str;
        this.f4559b = str2;
    }

    public final String a() {
        return this.f4558a;
    }

    public final String b() {
        return this.f4559b;
    }

    public final Map c() {
        return this.f4560c;
    }

    public final String toString() {
        return getClass().getSimpleName() + "(" + this.f4558a + ",mEndpoints=" + this.f4560c + ")";
    }
}
