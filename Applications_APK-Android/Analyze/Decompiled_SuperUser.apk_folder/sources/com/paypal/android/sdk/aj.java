package com.paypal.android.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.util.Base64;
import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;

public final class aj {

    /* renamed from: a  reason: collision with root package name */
    private static final boolean f4546a = Boolean.valueOf(System.getProperty("dyson.debug.mode", Boolean.FALSE.toString())).booleanValue();

    /* renamed from: b  reason: collision with root package name */
    private static final boolean f4547b = Boolean.valueOf(System.getProperty("prd.debug.mode", Boolean.FALSE.toString())).booleanValue();

    /* renamed from: c  reason: collision with root package name */
    private static final String f4548c = (r.class.getSimpleName() + "." + aj.class.getSimpleName());

    /* renamed from: d  reason: collision with root package name */
    private static final Uri f4549d;

    static {
        Uri uri;
        try {
            uri = Uri.parse("content://com.google.android.gsf.gservices");
        } catch (Exception e2) {
            uri = null;
        }
        f4549d = uri;
    }

    private aj() {
    }

    public static Location a(LocationManager locationManager) {
        Location location = null;
        try {
            List<String> providers = locationManager.getProviders(true);
            int size = providers.size() - 1;
            while (size >= 0) {
                try {
                    Location lastKnownLocation = locationManager.getLastKnownLocation(providers.get(size));
                    if (lastKnownLocation != null) {
                        return lastKnownLocation;
                    }
                    size--;
                    location = lastKnownLocation;
                } catch (RuntimeException e2) {
                    return location;
                }
            }
            return location;
        } catch (RuntimeException e3) {
            return null;
        }
    }

    public static m a(Context context) {
        m mVar = new m();
        mVar.a(context.getPackageName());
        try {
            mVar.b(context.getPackageManager().getPackageInfo(mVar.a(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e2) {
        }
        return mVar;
    }

    public static Object a(Object obj, Class cls) {
        if (obj == null || !cls.isAssignableFrom(obj.getClass())) {
            return null;
        }
        return cls.cast(obj);
    }

    public static Object a(Map map, Class cls, String str, Object obj) {
        Object obj2;
        if (map == null || (obj2 = map.get(str)) == null) {
            return obj;
        }
        if (cls.isAssignableFrom(obj2.getClass())) {
            return cls.cast(obj2);
        }
        a(6, "PRD", "cannot parse data for " + str, new Exception("cannot parse data for " + str));
        return obj;
    }

    public static String a() {
        try {
            o oVar = new o();
            oVar.a(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/com.ebay.lid/");
            String b2 = oVar.b("fb.bin");
            if (!"".equals(b2.trim())) {
                return b2;
            }
            String b3 = b(Boolean.TRUE.booleanValue());
            oVar.a("fb.bin", b3.getBytes("UTF-8"));
            return b3;
        } catch (Exception e2) {
            return "";
        }
    }

    public static String a(Context context, String str, String str2) {
        try {
            new StringBuilder("entering getMetadata loading name=").append(str);
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), FileUtils.FileMode.MODE_IWUSR);
            if (applicationInfo.metaData != null) {
                new StringBuilder("leaving getMetadata successfully loading name=").append(str);
                return applicationInfo.metaData.getString(str);
            }
        } catch (PackageManager.NameNotFoundException e2) {
            new StringBuilder("load metadata in manifest failed, name=").append(str);
        }
        new StringBuilder("leaving getMetadata with default value,name=").append(str);
        return null;
    }

    public static String a(String str) {
        MessageDigest instance = MessageDigest.getInstance("SHA-256");
        instance.update(str.getBytes());
        byte[] digest = instance.digest();
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b2 : digest) {
            stringBuffer.append(Integer.toString((b2 & 255) + 256, 16).substring(1));
        }
        return stringBuffer.toString().substring(0, 32);
    }

    public static String a(Map map, String str, String str2) {
        return (String) a(map, String.class, str, (Object) null);
    }

    public static List a(boolean z) {
        ArrayList arrayList = new ArrayList();
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    InetAddress nextElement = inetAddresses.nextElement();
                    if (!nextElement.isLoopbackAddress()) {
                        String hostAddress = nextElement.getHostAddress();
                        if (!(nextElement instanceof Inet6Address)) {
                            arrayList.add(hostAddress);
                        } else if (z) {
                            arrayList.add(hostAddress);
                        }
                    }
                }
            }
        } catch (Exception e2) {
        }
        return arrayList;
    }

    public static void a(int i, String str, String str2) {
        if (f4547b) {
            Log.println(i, str, str2);
        }
    }

    public static void a(int i, String str, String str2, Throwable th) {
        if (f4547b) {
            Log.println(6, str, str2 + 10 + Log.getStackTraceString(th));
        }
    }

    public static void a(String str, String str2) {
    }

    public static void a(String str, String str2, Throwable th) {
    }

    public static void a(String str, JSONObject jSONObject) {
        if (f4546a && jSONObject != null) {
            jSONObject.toString();
        }
    }

    public static boolean a(Context context, String str) {
        try {
            return context.getApplicationContext().checkCallingOrSelfPermission(str) == 0;
        } catch (Exception e2) {
            return false;
        }
    }

    public static boolean a(PackageManager packageManager, Intent intent) {
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
        return queryIntentActivities != null && queryIntentActivities.size() > 0;
    }

    public static boolean a(Map map, String str, Boolean bool) {
        return ((Boolean) a(map, Boolean.class, str, bool)).booleanValue();
    }

    public static String b() {
        List a2 = a(false);
        return a2.isEmpty() ? "" : (String) a2.get(0);
    }

    public static String b(Context context) {
        Cursor query;
        String str = null;
        if (!(f4549d == null || !a(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") || (query = context.getContentResolver().query(f4549d, null, null, new String[]{"android_id"}, null)) == null)) {
            try {
                if (query.moveToFirst() && query.getColumnCount() >= 2) {
                    str = Long.toHexString(Long.parseLong(query.getString(1)));
                    query.close();
                }
            } catch (NumberFormatException e2) {
            } finally {
                query.close();
            }
        }
        return str;
    }

    public static String b(Context context, String str) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(str)));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                sb.append(readLine);
            } else {
                bufferedReader.close();
                return new String(Base64.decode(sb.toString(), 0), "UTF-8");
            }
        }
    }

    public static String b(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList = new ArrayList();
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if ((charAt >= '0' && charAt <= '9') || ((charAt >= 'A' && charAt <= 'F') || (charAt >= 'a' && charAt <= 'f'))) {
                int parseInt = Integer.parseInt(new StringBuilder().append(str.charAt(i2)).toString(), 16);
                i += parseInt;
                arrayList.add(Integer.valueOf(parseInt));
            }
        }
        int i3 = i + 1;
        int size = arrayList.size() % 4;
        for (int i4 = 0; i4 < arrayList.size() - size; i4 += 4) {
            sb.append(Integer.toHexString((((Integer) arrayList.get((((Integer) arrayList.get(i4 + 3)).intValue() % 4) + i4)).intValue() + i3) % 16));
        }
        if (sb.toString().length() == 0) {
            return null;
        }
        return sb.toString().length() >= 4 ? sb.toString().substring(0, 4) : sb.toString();
    }

    public static String b(boolean z) {
        return z ? UUID.randomUUID().toString() : UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static boolean b(String str, String str2) {
        String[] split = str.split("\\.");
        String[] split2 = str2.split("\\.");
        new StringBuilder("Cached version is ").append(str);
        new StringBuilder("default version is ").append(str2);
        int i = 0;
        while (i < split.length && i < split2.length && split[i].equals(split2[i])) {
            i++;
        }
        return ((i >= split.length || i >= split2.length) ? Integer.valueOf(Integer.signum(split.length - split2.length)) : Integer.valueOf(Integer.signum(Integer.valueOf(split[i]).compareTo(Integer.valueOf(split2[i]))))).intValue() >= 0;
    }

    public static long c() {
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (IllegalArgumentException e2) {
            e2.getLocalizedMessage();
            return 0;
        }
    }

    public static String c(Context context, String str) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("RiskManagerAG", 0);
        String string = sharedPreferences.getString("RiskManagerAG", "");
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (str != null && !str.equals(string)) {
            edit.putString("RiskManagerAG", str);
            edit.commit();
            return str;
        } else if (!string.equals("")) {
            return string;
        } else {
            String b2 = b(Boolean.TRUE.booleanValue());
            edit.putString("RiskManagerAG", b2);
            edit.commit();
            return b2;
        }
    }

    public static void c(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("RiskManagerCT", 0);
        int i = sharedPreferences.getInt("RiskManagerCT", 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt("RiskManagerCT", (i <= 0 || i >= Integer.MAX_VALUE) ? 1 : i + 1);
        edit.commit();
    }

    public static String d() {
        String property;
        String property2;
        if (Build.VERSION.SDK_INT < 14 || (property = System.getProperty("http.proxyHost")) == null || (property2 = System.getProperty("http.proxyPort")) == null) {
            return null;
        }
        return "host=" + property + ",port=" + property2;
    }

    public static String d(Context context) {
        return new StringBuilder().append(context.getSharedPreferences("RiskManagerCT", 0).getInt("RiskManagerCT", 0)).toString();
    }

    public static String e() {
        try {
            Iterator it = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
            while (it.hasNext()) {
                NetworkInterface networkInterface = (NetworkInterface) it.next();
                if (networkInterface.isUp() && networkInterface.getInterfaceAddresses().size() != 0) {
                    String name = networkInterface.getName();
                    if (name.startsWith("ppp") || name.startsWith("tun") || name.startsWith("tap")) {
                        return name;
                    }
                }
            }
        } catch (Exception e2) {
        }
        return null;
    }
}
