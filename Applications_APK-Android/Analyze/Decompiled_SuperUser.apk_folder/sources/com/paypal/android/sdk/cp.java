package com.paypal.android.sdk;

import android.os.Parcel;

public abstract class cp {

    /* renamed from: a  reason: collision with root package name */
    protected String f4681a;

    /* renamed from: b  reason: collision with root package name */
    protected long f4682b;

    public cp() {
    }

    public cp(Parcel parcel) {
        this.f4681a = parcel.readString();
        this.f4682b = parcel.readLong();
    }

    public final boolean b() {
        return this.f4682b > System.currentTimeMillis();
    }

    public final String c() {
        return this.f4681a;
    }
}
