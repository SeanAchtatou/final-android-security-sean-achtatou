package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.content.Intent;
import java.util.Arrays;
import java.util.HashSet;

public final class FuturePaymentConsentActivity extends dn {

    /* renamed from: d  reason: collision with root package name */
    private static final String f5049d = FuturePaymentConsentActivity.class.getSimpleName();

    static void a(Activity activity, int i, PayPalConfiguration payPalConfiguration) {
        Intent intent = new Intent(activity, FuturePaymentConsentActivity.class);
        intent.putExtras(activity.getIntent());
        intent.putExtra("com.paypal.android.sdk.paypalConfiguration", payPalConfiguration);
        activity.startActivityForResult(intent, 1);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f5271b = new PayPalOAuthScopes(new HashSet(Arrays.asList(PayPalOAuthScopes.f5082a)));
    }

    public final /* bridge */ /* synthetic */ void finish() {
        super.finish();
    }

    public final /* bridge */ /* synthetic */ void onBackPressed() {
        super.onBackPressed();
    }
}
