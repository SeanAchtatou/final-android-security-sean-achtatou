package com.paypal.android.sdk.payments;

import com.duapps.ad.AdError;
import com.paypal.android.sdk.ap;
import com.paypal.android.sdk.as;
import com.paypal.android.sdk.cf;
import java.util.GregorianCalendar;

final class bm {

    /* renamed from: a  reason: collision with root package name */
    private final PayPalService f5205a;

    /* renamed from: b  reason: collision with root package name */
    private final String f5206b = Integer.toString((new GregorianCalendar().getTimeZone().getRawOffset() / AdError.NETWORK_ERROR_CODE) / 60);

    public bm(PayPalService payPalService) {
        this.f5205a = payPalService;
    }

    public final cf a() {
        return this.f5205a.c();
    }

    public final void a(as asVar) {
        this.f5205a.a(asVar);
    }

    public final String b() {
        return this.f5205a.f();
    }

    public final ap c() {
        return this.f5205a.b();
    }

    public final String d() {
        return this.f5205a.e();
    }

    public final String e() {
        return this.f5206b;
    }

    public final String f() {
        return this.f5205a.v();
    }
}
