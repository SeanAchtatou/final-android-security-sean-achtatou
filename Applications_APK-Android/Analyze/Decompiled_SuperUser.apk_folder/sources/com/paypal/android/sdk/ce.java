package com.paypal.android.sdk;

import java.util.Locale;

public final class ce implements dh {

    /* renamed from: a  reason: collision with root package name */
    private static volatile ce f4663a;

    private ce() {
    }

    public static ce a() {
        if (f4663a == null) {
            synchronized (ce.class) {
                if (f4663a == null) {
                    f4663a = new ce();
                }
            }
        }
        return f4663a;
    }

    public final String a(String str) {
        return str;
    }

    public final Locale b() {
        return Locale.getDefault();
    }

    public final ee c() {
        return new ee(Locale.getDefault().getCountry());
    }

    public final ee d() {
        return c();
    }
}
