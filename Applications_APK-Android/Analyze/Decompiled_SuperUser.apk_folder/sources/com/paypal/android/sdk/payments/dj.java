package com.paypal.android.sdk.payments;

import android.text.TextUtils;
import com.paypal.android.sdk.ce;
import com.paypal.android.sdk.dq;
import com.paypal.android.sdk.ey;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

abstract class dj {

    /* renamed from: a  reason: collision with root package name */
    private final bm f5263a;

    public dj(bm bmVar) {
        this.f5263a = bmVar;
    }

    private String a(ey eyVar, boolean z) {
        String str = dq.f4747b + ":" + a() + ":" + eyVar.a();
        return z ? str + "|error" : str;
    }

    /* access modifiers changed from: protected */
    public abstract String a();

    public void a(ey eyVar, boolean z, String str, String str2, String str3) {
        ce.a();
        String locale = Locale.getDefault().toString();
        HashMap hashMap = new HashMap();
        boolean z2 = !TextUtils.isEmpty(str);
        hashMap.put("gn", a(eyVar, z2));
        hashMap.put("v31", a(eyVar, z2));
        String str4 = a(eyVar, z2) + ":" + eyVar.a(this.f5263a.d(), z);
        hashMap.put("c25", z2 ? str4 + "|error" : str4);
        hashMap.put("v25", "D=c25");
        hashMap.put("c37", dq.f4746a + "::");
        hashMap.put("c50", locale);
        hashMap.put("c35", "out");
        a(hashMap, eyVar, str2, str3);
        if (str != null) {
            hashMap.put("c29", str);
        }
        a("2.15.3", hashMap);
    }

    /* access modifiers changed from: package-private */
    public abstract void a(String str, Map map);

    /* access modifiers changed from: protected */
    public void a(Map map, ey eyVar, String str, String str2) {
    }

    /* access modifiers changed from: protected */
    public bm b() {
        return this.f5263a;
    }
}
