package com.paypal.android.sdk;

import com.google.firebase.analytics.FirebaseAnalytics;
import java.math.BigDecimal;
import org.json.JSONArray;
import org.json.JSONObject;

public class ei {

    /* renamed from: a  reason: collision with root package name */
    private String f4809a;

    /* renamed from: b  reason: collision with root package name */
    private Integer f4810b;

    /* renamed from: c  reason: collision with root package name */
    private BigDecimal f4811c;

    /* renamed from: d  reason: collision with root package name */
    private String f4812d;

    /* renamed from: e  reason: collision with root package name */
    private String f4813e;

    static {
        ei.class.getSimpleName();
    }

    public ei(String str, Integer num, BigDecimal bigDecimal, String str2, String str3) {
        this.f4809a = str;
        this.f4810b = num;
        this.f4811c = bigDecimal;
        this.f4812d = str2;
        this.f4813e = str3;
    }

    public static JSONArray a(ei[] eiVarArr) {
        if (eiVarArr == null) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        for (ei eiVar : eiVarArr) {
            JSONObject jSONObject = new JSONObject();
            jSONObject.accumulate(FirebaseAnalytics.Param.QUANTITY, Integer.toString(eiVar.f4810b.intValue()));
            jSONObject.accumulate("name", eiVar.f4809a);
            jSONObject.accumulate(FirebaseAnalytics.Param.PRICE, eiVar.f4811c.toString());
            jSONObject.accumulate(FirebaseAnalytics.Param.CURRENCY, eiVar.f4812d);
            jSONObject.accumulate("sku", eiVar.f4813e);
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }
}
