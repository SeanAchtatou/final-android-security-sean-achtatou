package com.paypal.android.sdk;

import android.util.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLException;
import okhttp3.f;
import okhttp3.q;
import okhttp3.s;
import okhttp3.t;
import okhttp3.v;
import okhttp3.w;
import okhttp3.x;

public class bi extends be {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f4595a = bi.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static final s f4596b = s.a("charset=utf-8");

    /* renamed from: c  reason: collision with root package name */
    private final a f4597c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final String f4598d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final bu f4599e;

    /* renamed from: f  reason: collision with root package name */
    private final t f4600f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public final t f4601g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public final x f4602h;
    private final ScheduledExecutorService i;
    /* access modifiers changed from: private */
    public final ConcurrentLinkedQueue j;

    public bi(a aVar, String str, x xVar, bu buVar, int i2, boolean z, List list) {
        boolean z2 = true;
        this.f4597c = aVar;
        this.f4598d = str;
        this.f4602h = xVar;
        this.f4599e = buVar;
        boolean d2 = an.d(str);
        boolean z3 = d2 && !z;
        z2 = d2 ? false : z2;
        t.a a2 = ba.a(90, z3, z2, xVar.b(), this.f4599e.e());
        a2.a().addAll(list);
        a2.a().add(new bf());
        this.f4600f = a2.b();
        t.a a3 = ba.a(90, z3, z2, xVar.b(), this.f4599e.e());
        a3.a().add(new bf());
        this.f4601g = a3.b();
        this.i = Executors.newSingleThreadScheduledExecutor();
        this.j = new ConcurrentLinkedQueue();
    }

    private static String a(String str, String str2) {
        if (str2 == null) {
            return str;
        }
        if (!str.endsWith("/")) {
            str = str + "/";
        }
        return str + str2;
    }

    static /* synthetic */ void a(bi biVar, bt btVar, x xVar, IOException iOException) {
        new StringBuilder().append(btVar.n()).append(" failure.");
        if (xVar != null) {
            Log.e("paypal.sdk", "request failure with http statusCode:" + xVar.b() + ",exception:" + xVar.d());
            a(btVar, xVar.b());
            if (btVar.q()) {
                btVar.a(bx.INTERNAL_SERVER_ERROR.toString(), xVar.b() + " http response received.  Response not parsable.", null);
            }
        } else if (iOException == null) {
            throw new RuntimeException("Both Response or Exception cannot be null");
        } else if (!(iOException instanceof SSLException) || !"Connection closed by peer".equals(iOException.getMessage())) {
            btVar.a(new ay(bx.SERVER_COMMUNICATION_ERROR, iOException));
        } else {
            btVar.a(new ay(bx.DEVICE_OS_TOO_OLD, iOException));
        }
        Log.e("paypal.sdk", "request failed with server response:" + btVar.g());
        biVar.f4599e.a(btVar);
    }

    /* access modifiers changed from: private */
    public void a(bt btVar, String str, t tVar, f fVar) {
        switch (bm.f4615a[btVar.h().b().ordinal()]) {
            case 1:
                tVar.a(new v.a().a(a(str, btVar.f())).a(c(btVar)).b()).a(fVar);
                return;
            case 2:
                tVar.a(new v.a().a(str).a(w.a(f4596b, btVar.f())).a(c(btVar)).b()).a(fVar);
                return;
            case 3:
                tVar.a(new v.a().a(a(str, btVar.f())).a(c(btVar)).a().b()).a(fVar);
                return;
            default:
                throw new RuntimeException(btVar.h().b() + " not supported.");
        }
    }

    private static q c(bt btVar) {
        q.a aVar = new q.a();
        for (Map.Entry entry : btVar.i().entrySet()) {
            aVar.a((String) entry.getKey(), (String) entry.getValue());
        }
        return aVar.a();
    }

    public final void a() {
        this.f4600f.s().b();
        this.f4601g.s().b();
    }

    public final boolean b(bt btVar) {
        if (!this.f4597c.a()) {
            btVar.a(new ay(bx.SERVER_COMMUNICATION_ERROR.toString()));
            return false;
        }
        bt.k();
        String a2 = btVar.a(btVar.h());
        try {
            if (btVar.a()) {
                new StringBuilder().append(btVar.n()).append(" endpoint: ").append(a2);
                new StringBuilder().append(btVar.n()).append(" request: ").append(btVar.f());
                this.j.offer(new bj(this, btVar, a2));
                int nextInt = new Random().nextInt(190) + 10;
                new StringBuilder("Delaying tracking execution for ").append(nextInt).append(" seconds");
                this.i.schedule(new bk(this), (long) nextInt, TimeUnit.SECONDS);
            } else {
                new StringBuilder().append(btVar.n()).append(" endpoint: ").append(a2);
                new StringBuilder().append(btVar.n()).append(" request: ").append(btVar.f());
                a(btVar, a2, this.f4600f, new bn(this, btVar, (byte) 0));
            }
            return true;
        } catch (UnsupportedEncodingException e2) {
            Log.e(f4595a, "encoding failure", e2);
            btVar.a(new ay(bx.INTERNAL_ERROR, e2));
            return false;
        } catch (IOException e3) {
            Log.e(f4595a, "communication failure", e3);
            btVar.a(new ay(bx.SERVER_COMMUNICATION_ERROR, e3));
            return false;
        }
    }
}
