package com.paypal.android.sdk;

public class ay extends aw {
    public ay(bx bxVar, Exception exc) {
        this(bxVar.toString(), exc);
    }

    public ay(String str) {
        super("RequestError", str);
    }

    public ay(String str, String str2, String str3) {
        super("RequestError", str, str2, str3);
    }

    public ay(String str, Throwable th) {
        super(th.getClass().toString(), str, th.toString(), th.getMessage());
    }
}
