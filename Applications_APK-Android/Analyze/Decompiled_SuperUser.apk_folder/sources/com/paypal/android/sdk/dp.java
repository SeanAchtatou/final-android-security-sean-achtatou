package com.paypal.android.sdk;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.Date;

public final class dp extends ck implements Parcelable {
    public static final Parcelable.Creator CREATOR = new cm();

    /* renamed from: b  reason: collision with root package name */
    private String f4740b;

    /* renamed from: c  reason: collision with root package name */
    private Date f4741c;

    /* renamed from: d  reason: collision with root package name */
    private String f4742d;

    /* renamed from: e  reason: collision with root package name */
    private dr f4743e;

    /* renamed from: f  reason: collision with root package name */
    private int f4744f;

    /* renamed from: g  reason: collision with root package name */
    private int f4745g;

    public dp() {
    }

    private dp(Parcel parcel) {
        this.f4679a = parcel.readString();
        this.f4740b = parcel.readString();
        this.f4742d = parcel.readString();
        this.f4741c = (Date) parcel.readSerializable();
        this.f4743e = (dr) parcel.readSerializable();
        this.f4744f = parcel.readInt();
        this.f4745g = parcel.readInt();
    }

    /* synthetic */ dp(Parcel parcel, byte b2) {
        this(parcel);
    }

    public dp(a aVar, String str, String str2, Date date, String str3, String str4, int i, int i2) {
        this.f4679a = aVar.b(str2);
        this.f4740b = str;
        this.f4741c = date;
        b(str3);
        c(str4);
        this.f4744f = i;
        this.f4745g = i2;
    }

    public dp(String str, String str2, String str3, String str4, String str5, int i, int i2) {
        this.f4679a = str2;
        this.f4740b = str;
        this.f4741c = et.a(str3);
        b(str4);
        c(str5);
        this.f4744f = i;
        this.f4745g = i2;
    }

    public static String a(String str) {
        if (str == null) {
            return null;
        }
        return "x-" + str.substring(str.length() - 4);
    }

    private void b(String str) {
        if (str != null) {
            this.f4742d = str.substring(str.length() - 4);
        } else {
            this.f4742d = null;
        }
    }

    private void c(String str) {
        this.f4743e = dr.a(str);
    }

    public final boolean b() {
        return !TextUtils.isEmpty(this.f4740b) && !TextUtils.isEmpty(this.f4742d) && !TextUtils.isEmpty(this.f4679a) && this.f4741c != null && !this.f4741c.before(new Date()) && this.f4743e != null && this.f4743e != dr.UNKNOWN && this.f4744f > 0 && this.f4744f <= 12 && this.f4745g >= 0 && this.f4745g <= 9999;
    }

    public final Date c() {
        return this.f4741c;
    }

    public final String d() {
        return a(this.f4742d);
    }

    public final int describeContents() {
        return 0;
    }

    public final String e() {
        return this.f4740b;
    }

    public final int f() {
        return this.f4744f;
    }

    public final int g() {
        return this.f4745g;
    }

    public final dr h() {
        return this.f4743e;
    }

    public final String toString() {
        return "TokenizedCreditCard(token=" + this.f4740b + ",lastFourDigits=" + this.f4742d + ",payerId=" + this.f4679a + ",tokenValidUntil=" + this.f4741c + ",cardType=" + this.f4743e + ",expiryMonth/year=" + this.f4744f + "/" + this.f4745g + ")";
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f4679a);
        parcel.writeString(this.f4740b);
        parcel.writeString(this.f4742d);
        parcel.writeSerializable(this.f4741c);
        parcel.writeSerializable(this.f4743e);
        parcel.writeInt(this.f4744f);
        parcel.writeInt(this.f4745g);
    }
}
