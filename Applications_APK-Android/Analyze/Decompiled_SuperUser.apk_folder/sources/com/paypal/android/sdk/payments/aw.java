package com.paypal.android.sdk.payments;

import android.content.DialogInterface;

final class aw implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PayPalProfileSharingActivity f5181a;

    aw(PayPalProfileSharingActivity payPalProfileSharingActivity) {
        this.f5181a = payPalProfileSharingActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void
     arg types: [com.paypal.android.sdk.payments.bi, int]
     candidates:
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.payments.bi):com.paypal.android.sdk.payments.bi
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, com.paypal.android.sdk.bt):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.PayPalService, boolean):boolean
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.Boolean):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.ey, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(java.lang.String, java.lang.String):void
      com.paypal.android.sdk.payments.PayPalService.a(com.paypal.android.sdk.payments.bi, boolean):void */
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f5181a.f5105d.a(PayPalProfileSharingActivity.a(this.f5181a), true);
    }
}
