package com.paypal.android.sdk;

import android.os.Handler;
import android.os.Message;
import java.lang.ref.WeakReference;

final class aq extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference f4567a;

    public aq(ap apVar) {
        this.f4567a = new WeakReference(apVar);
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 2:
                ap apVar = (ap) this.f4567a.get();
                if (apVar != null) {
                    apVar.f4564d.a((bt) message.obj, 0);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
