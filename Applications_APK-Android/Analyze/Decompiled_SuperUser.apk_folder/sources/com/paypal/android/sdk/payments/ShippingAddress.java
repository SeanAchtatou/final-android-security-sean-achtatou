package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.paypal.android.sdk.cd;
import org.json.JSONException;
import org.json.JSONObject;

public final class ShippingAddress implements Parcelable {
    public static final Parcelable.Creator CREATOR = new dc();

    /* renamed from: a  reason: collision with root package name */
    private static final String f5149a = ShippingAddress.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private String f5150b;

    /* renamed from: c  reason: collision with root package name */
    private String f5151c;

    /* renamed from: d  reason: collision with root package name */
    private String f5152d;

    /* renamed from: e  reason: collision with root package name */
    private String f5153e;

    /* renamed from: f  reason: collision with root package name */
    private String f5154f;

    /* renamed from: g  reason: collision with root package name */
    private String f5155g;

    /* renamed from: h  reason: collision with root package name */
    private String f5156h;

    public ShippingAddress() {
    }

    private ShippingAddress(Parcel parcel) {
        this.f5150b = parcel.readString();
        this.f5151c = parcel.readString();
        this.f5152d = parcel.readString();
        this.f5153e = parcel.readString();
        this.f5154f = parcel.readString();
        this.f5155g = parcel.readString();
        this.f5156h = parcel.readString();
    }

    /* synthetic */ ShippingAddress(Parcel parcel, byte b2) {
        this(parcel);
    }

    private static boolean a(String str) {
        return cd.b((CharSequence) str);
    }

    private static boolean a(String str, String str2) {
        if (cd.a((CharSequence) str)) {
            return cd.a((CharSequence) str2);
        }
        if (cd.a((CharSequence) str2)) {
            return false;
        }
        return str.trim().equals(str2.trim());
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.accumulate("recipient_name", this.f5150b);
            jSONObject.accumulate("line1", this.f5151c);
            jSONObject.accumulate("city", this.f5153e);
            jSONObject.accumulate("country_code", this.f5156h);
            if (a(this.f5152d)) {
                jSONObject.accumulate("line2", this.f5152d);
            }
            if (a(this.f5154f)) {
                jSONObject.accumulate("state", this.f5154f);
            }
            if (a(this.f5155g)) {
                jSONObject.accumulate("postal_code", this.f5155g);
            }
        } catch (JSONException e2) {
            Log.e(f5149a, e2.getMessage());
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(JSONObject jSONObject) {
        return a(jSONObject.optString("recipient_name"), this.f5150b) && a(jSONObject.optString("line1"), this.f5151c) && a(jSONObject.optString("line2"), this.f5152d) && a(jSONObject.optString("city"), this.f5153e) && a(jSONObject.optString("state"), this.f5154f) && a(jSONObject.optString("country_code"), this.f5156h) && a(jSONObject.optString("postal_code"), this.f5155g);
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f5150b);
        parcel.writeString(this.f5151c);
        parcel.writeString(this.f5152d);
        parcel.writeString(this.f5153e);
        parcel.writeString(this.f5154f);
        parcel.writeString(this.f5155g);
        parcel.writeString(this.f5156h);
    }
}
