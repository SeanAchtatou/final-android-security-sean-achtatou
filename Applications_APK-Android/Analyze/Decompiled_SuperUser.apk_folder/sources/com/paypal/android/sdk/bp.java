package com.paypal.android.sdk;

import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONException;

public class bp extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4619a = bp.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final bu f4620b;

    /* renamed from: c  reason: collision with root package name */
    private final List f4621c = Collections.synchronizedList(new LinkedList());

    /* renamed from: d  reason: collision with root package name */
    private boolean f4622d;

    /* renamed from: e  reason: collision with root package name */
    private final bq f4623e;

    public bp(bu buVar, bq bqVar) {
        this.f4620b = buVar;
        this.f4623e = bqVar;
        start();
    }

    public final void a() {
        if (!this.f4622d) {
            this.f4623e.a();
            this.f4622d = true;
            synchronized (this.f4621c) {
                this.f4621c.notifyAll();
            }
            interrupt();
            while (isAlive()) {
                try {
                    Thread.sleep(10);
                    new StringBuilder("Waiting for ").append(getClass().getSimpleName()).append(" to die");
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    public final void a(bt btVar) {
        synchronized (this.f4621c) {
            this.f4621c.add(btVar);
            new StringBuilder("Queued ").append(btVar.n());
            this.f4621c.notifyAll();
        }
    }

    public void run() {
        bt btVar;
        new StringBuilder("Starting ").append(getClass().getSimpleName());
        while (!this.f4622d) {
            synchronized (this.f4621c) {
                if (this.f4621c.isEmpty()) {
                    try {
                        this.f4621c.wait();
                        btVar = null;
                    } catch (InterruptedException e2) {
                        btVar = null;
                    }
                } else {
                    btVar = (bt) this.f4621c.remove(0);
                }
            }
            if (btVar != null) {
                try {
                    btVar.a(btVar.b());
                } catch (JSONException e3) {
                    Log.e("paypal.sdk", "Exception computing request", e3);
                    btVar.a(new ay(bx.PARSE_RESPONSE_ERROR.toString(), "JSON Exception in computeRequest", e3.getMessage()));
                } catch (UnsupportedEncodingException e4) {
                    Log.e("paypal.sdk", "Exception computing request", e4);
                    btVar.a(new ay(bx.PARSE_RESPONSE_ERROR.toString(), "Unsupported encoding", e4.getMessage()));
                }
                if (!this.f4623e.b(btVar)) {
                    this.f4620b.a(btVar);
                }
            }
        }
        new StringBuilder().append(getClass().getSimpleName()).append(" exiting");
    }
}
