package com.paypal.android.sdk.payments;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public final class PaymentConfirmation implements Parcelable {
    public static final Parcelable.Creator CREATOR = new cn();

    /* renamed from: a  reason: collision with root package name */
    private static final String f5130a = PaymentConfirmation.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private String f5131b;

    /* renamed from: c  reason: collision with root package name */
    private PayPalPayment f5132c;

    /* renamed from: d  reason: collision with root package name */
    private ProofOfPayment f5133d;

    private PaymentConfirmation(Parcel parcel) {
        this.f5131b = parcel.readString();
        this.f5132c = (PayPalPayment) parcel.readParcelable(PayPalPayment.class.getClassLoader());
        this.f5133d = (ProofOfPayment) parcel.readParcelable(ProofOfPayment.class.getClassLoader());
    }

    /* synthetic */ PaymentConfirmation(Parcel parcel, byte b2) {
        this(parcel);
    }

    PaymentConfirmation(String str, PayPalPayment payPalPayment, ProofOfPayment proofOfPayment) {
        this.f5131b = str;
        this.f5132c = payPalPayment;
        this.f5133d = proofOfPayment;
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("environment", this.f5131b);
            jSONObject2.put("paypal_sdk_version", "2.15.3");
            jSONObject2.put("platform", "Android");
            jSONObject2.put("product_name", "PayPal-Android-SDK");
            jSONObject.put("client", jSONObject2);
            jSONObject.put("response", this.f5133d.a());
            jSONObject.put("response_type", "payment");
            return jSONObject;
        } catch (JSONException e2) {
            Log.e(f5130a, "Error encoding JSON", e2);
            return null;
        }
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f5131b);
        parcel.writeParcelable(this.f5132c, 0);
        parcel.writeParcelable(this.f5133d, 0);
    }
}
