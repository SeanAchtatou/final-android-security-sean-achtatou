package com.paypal.android.sdk;

import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.util.Locale;
import okhttp3.e;
import okhttp3.f;
import okhttp3.x;

final class bn implements f {

    /* renamed from: a  reason: collision with root package name */
    private final bt f4616a;

    /* renamed from: b  reason: collision with root package name */
    private /* synthetic */ bi f4617b;

    private bn(bi biVar, bt btVar) {
        this.f4617b = biVar;
        this.f4616a = btVar;
    }

    /* synthetic */ bn(bi biVar, bt btVar, byte b2) {
        this(biVar, btVar);
    }

    private String a(String str) {
        Locale locale = Locale.US;
        String str2 = this.f4616a.n() + " PayPal Debug-ID: %s [%s, %s]";
        StringBuilder append = new StringBuilder().append(this.f4617b.f4602h.a()).append(";");
        x unused = this.f4617b.f4602h;
        return String.format(locale, str2, str, this.f4617b.f4598d, append.append("release").toString());
    }

    public final void a(e eVar, IOException iOException) {
        try {
            this.f4616a.b(iOException.getMessage());
            String a2 = eVar.a().a("PayPal-Debug-Id");
            if (!TextUtils.isEmpty(a2)) {
                Log.w("paypal.sdk", a(a2));
            }
            bi.a(this.f4617b, this.f4616a, (x) null, iOException);
        } catch (Throwable th) {
            Log.e("paypal.sdk", "exception in response handler", th);
            throw th;
        }
    }

    public final void a(e eVar, x xVar) {
        try {
            String a2 = xVar.a("paypal-debug-id");
            this.f4616a.b(xVar.g().d());
            if (!xVar.c()) {
                if (!TextUtils.isEmpty(a2)) {
                    Log.w("paypal.sdk", a(a2));
                }
                bi.a(this.f4617b, this.f4616a, xVar, (IOException) null);
                return;
            }
            this.f4616a.c(a2);
            String unused = bi.f4595a;
            new StringBuilder().append(this.f4616a.n()).append(" success. response: ").append(this.f4616a.g());
            if (!TextUtils.isEmpty(a2)) {
                Log.w("paypal.sdk", a(a2));
            }
            if (this.f4616a.q()) {
                bi.a(this.f4616a);
            }
            this.f4617b.f4599e.a(this.f4616a);
        } catch (Throwable th) {
            Log.e("paypal.sdk", "exception in response handler", th);
            throw th;
        }
    }
}
