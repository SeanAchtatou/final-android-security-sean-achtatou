package com.paypal.android.sdk;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public final class eq extends eo {

    /* renamed from: a  reason: collision with root package name */
    public final String f4833a;

    /* renamed from: b  reason: collision with root package name */
    private final String f4834b;

    /* renamed from: c  reason: collision with root package name */
    private final String f4835c;

    /* renamed from: d  reason: collision with root package name */
    private final String f4836d;

    /* renamed from: e  reason: collision with root package name */
    private final int f4837e;

    /* renamed from: f  reason: collision with root package name */
    private final int f4838f;

    /* renamed from: g  reason: collision with root package name */
    private String f4839g;

    /* renamed from: h  reason: collision with root package name */
    private String f4840h;
    private Date i;

    public eq(bu buVar, x xVar, String str, String str2, String str3, String str4, String str5, int i2, int i3) {
        super(db.TokenizeCreditCardRequest, buVar, xVar, str);
        this.f4833a = str2;
        this.f4834b = str3;
        if (str4 == null) {
            throw new RuntimeException("cardNumber should not be null.  If it is, then you're probably trying to tokenize a card that's already been tokenized.");
        }
        this.f4835c = str4;
        this.f4836d = str5;
        this.f4837e = i2;
        this.f4838f = i3;
    }

    public final String b() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.accumulate("payer_id", this.f4833a);
        jSONObject.accumulate("cvv2", this.f4836d);
        jSONObject.accumulate("expire_month", Integer.valueOf(this.f4837e));
        jSONObject.accumulate("expire_year", Integer.valueOf(this.f4838f));
        jSONObject.accumulate("number", this.f4835c);
        jSONObject.accumulate("type", this.f4834b);
        return jSONObject.toString();
    }

    public final void c() {
        JSONObject m = m();
        try {
            this.f4839g = m.getString("id");
            String string = m.getString("number");
            if (this.f4840h == null || !this.f4840h.endsWith(string.substring(string.length() - 4))) {
                this.f4840h = string;
            }
            this.i = et.a(m.getString("valid_until"));
        } catch (JSONException e2) {
            d();
        }
    }

    public final void d() {
        b(m());
    }

    public final String e() {
        return "{\"id\":\"CARD-50Y58962PH1899901KFFBSDA\",\"valid_until\":\"2016-03-19T00:00:00.000Z\",\"state\":\"ok\",\"type\":\"visa\",\"number\":\"xxxxxxxxxxxx" + this.f4835c.substring(this.f4835c.length() - 4) + "\",\"expire_month\":\"" + this.f4837e + "\",\"expire_year\":\"" + this.f4838f + "\",\"links\":[{\"href\":\"https://api.sandbox.paypal.com/v1/vault/credit-card/CARD-50Y58962PH1899901KFFBSDA\",\"rel\":\"self\",\"method\":\"GET\"}]}";
    }

    public final String t() {
        return this.f4839g;
    }

    public final String u() {
        return this.f4840h;
    }

    public final Date v() {
        return this.i;
    }

    public final String w() {
        return this.f4834b;
    }

    public final int x() {
        return this.f4837e;
    }

    public final int y() {
        return this.f4838f;
    }
}
