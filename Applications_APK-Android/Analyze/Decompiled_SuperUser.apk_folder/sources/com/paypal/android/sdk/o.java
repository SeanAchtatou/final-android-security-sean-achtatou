package com.paypal.android.sdk;

import android.os.Environment;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    private boolean f5046a = false;

    /* renamed from: b  reason: collision with root package name */
    private boolean f5047b = false;

    /* renamed from: c  reason: collision with root package name */
    private File f5048c;

    public o() {
        String externalStorageState = Environment.getExternalStorageState();
        char c2 = 65535;
        switch (externalStorageState.hashCode()) {
            case 1242932856:
                if (externalStorageState.equals("mounted")) {
                    c2 = 0;
                    break;
                }
                break;
            case 1299749220:
                if (externalStorageState.equals("mounted_ro")) {
                    c2 = 1;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                this.f5047b = true;
                this.f5046a = true;
                break;
            case 1:
                this.f5046a = true;
                this.f5047b = false;
                break;
            default:
                this.f5047b = false;
                this.f5046a = false;
                break;
        }
        this.f5048c = Environment.getExternalStorageDirectory();
    }

    public final void a(String str) {
        this.f5048c = new File(str);
    }

    public final void a(String str, byte[] bArr) {
        FileOutputStream fileOutputStream;
        Throwable th;
        if (this.f5046a && this.f5047b) {
            FileOutputStream fileOutputStream2 = null;
            try {
                if (this.f5048c.mkdirs() || this.f5048c.isDirectory()) {
                    fileOutputStream = new FileOutputStream(new File(this.f5048c, str));
                    try {
                        fileOutputStream.write(bArr);
                        fileOutputStream2 = fileOutputStream;
                    } catch (Throwable th2) {
                        th = th2;
                        cd.a(fileOutputStream);
                        throw th;
                    }
                }
                cd.a(fileOutputStream2);
            } catch (Throwable th3) {
                Throwable th4 = th3;
                fileOutputStream = null;
                th = th4;
                cd.a(fileOutputStream);
                throw th;
            }
        }
    }

    public final String b(String str) {
        FileInputStream fileInputStream;
        byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
        if (this.f5047b) {
            try {
                fileInputStream = new FileInputStream(new File(this.f5048c, str));
                try {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    for (int read = fileInputStream.read(bArr, 0, FileUtils.FileMode.MODE_ISGID); read != -1; read = fileInputStream.read(bArr, 0, FileUtils.FileMode.MODE_ISGID)) {
                        byteArrayOutputStream.write(bArr, 0, read);
                    }
                    bArr = byteArrayOutputStream.toByteArray();
                    cd.a(fileInputStream);
                } catch (IOException e2) {
                    cd.a(fileInputStream);
                    return "";
                } catch (Throwable th) {
                    th = th;
                    cd.a(fileInputStream);
                    throw th;
                }
            } catch (IOException e3) {
                fileInputStream = null;
                cd.a(fileInputStream);
                return "";
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = null;
                cd.a(fileInputStream);
                throw th;
            }
        }
        return new String(bArr, "UTF-8");
    }
}
