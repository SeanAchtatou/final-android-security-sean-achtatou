package com.paypal.android.sdk.payments;

import android.content.Intent;

final class bs extends ea {
    bs(Intent intent, PayPalConfiguration payPalConfiguration) {
        super(intent, payPalConfiguration);
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        bt btVar = new bt(this.f5301a);
        boolean z = btVar.a() != null && btVar.a().b();
        a(z, "PaymentActivity.EXTRA_PAYMENT");
        return z;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return PaymentActivity.class.getSimpleName();
    }
}
