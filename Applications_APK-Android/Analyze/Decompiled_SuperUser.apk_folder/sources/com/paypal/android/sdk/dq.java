package com.paypal.android.sdk;

import android.os.Build;

public final class dq {

    /* renamed from: a  reason: collision with root package name */
    public static String f4746a = "Android";

    /* renamed from: b  reason: collision with root package name */
    public static String f4747b = "mobile";

    /* renamed from: c  reason: collision with root package name */
    public static String f4748c = Build.VERSION.RELEASE;

    /* renamed from: d  reason: collision with root package name */
    public static String f4749d = Build.MODEL;
}
