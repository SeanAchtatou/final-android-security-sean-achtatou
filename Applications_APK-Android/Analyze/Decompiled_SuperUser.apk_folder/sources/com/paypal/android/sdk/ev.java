package com.paypal.android.sdk;

import io.fabric.sdk.android.services.b.b;
import java.util.Set;

public final class ev {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f4847a;

    /* renamed from: b  reason: collision with root package name */
    private static final fb f4848b = new fb(fs.class, gd.f4984a);

    /* renamed from: c  reason: collision with root package name */
    private static Set f4849c = new fr();

    public static String a(fs fsVar) {
        return f4848b.a(fsVar);
    }

    public static String a(String str) {
        return str.equals(bx.DEVICE_OS_TOO_OLD.toString()) ? f4848b.a(fs.ANDROID_OS_TOO_OLD) : str.equals(bx.SERVER_COMMUNICATION_ERROR.toString()) ? f4848b.a(fs.SERVER_PROBLEM) : str.equals(bx.INTERNAL_SERVER_ERROR.toString()) ? f4848b.a("INTERNAL_SERVICE_ERROR", fs.SYSTEM_ERROR_WITH_CODE) : str.equals(bx.PARSE_RESPONSE_ERROR.toString()) ? f4848b.a(fs.PP_SERVICE_ERROR_JSON_PARSE_ERROR) : f4848b.a(str, fs.SYSTEM_ERROR_WITH_CODE);
    }

    public static void b(String str) {
        f4848b.a(str);
        f4847a = cd.b(str) && f4849c.contains(str);
    }

    public static String c(String str) {
        String a2 = f4848b.a();
        return !a2.contains(b.ROLL_OVER_FILE_NAME_SEPARATOR) ? a2 + b.ROLL_OVER_FILE_NAME_SEPARATOR + str : a2;
    }
}
