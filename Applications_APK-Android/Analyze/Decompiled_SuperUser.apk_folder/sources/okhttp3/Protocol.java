package okhttp3;

import java.io.IOException;

public enum Protocol {
    HTTP_1_0("http/1.0"),
    HTTP_1_1("http/1.1"),
    SPDY_3("spdy/3.1"),
    HTTP_2("h2");
    

    /* renamed from: e  reason: collision with root package name */
    private final String f7709e;

    private Protocol(String str) {
        this.f7709e = str;
    }

    public static Protocol a(String str) {
        if (str.equals(HTTP_1_0.f7709e)) {
            return HTTP_1_0;
        }
        if (str.equals(HTTP_1_1.f7709e)) {
            return HTTP_1_1;
        }
        if (str.equals(HTTP_2.f7709e)) {
            return HTTP_2;
        }
        if (str.equals(SPDY_3.f7709e)) {
            return SPDY_3;
        }
        throw new IOException("Unexpected protocol: " + str);
    }

    public String toString() {
        return this.f7709e;
    }
}
