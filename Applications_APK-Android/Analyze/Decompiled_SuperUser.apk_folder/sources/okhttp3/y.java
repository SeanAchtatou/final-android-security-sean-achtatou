package okhttp3;

import java.io.Closeable;
import java.nio.charset.Charset;
import okhttp3.internal.c;
import okio.e;

/* compiled from: ResponseBody */
public abstract class y implements Closeable {
    public abstract s a();

    public abstract long b();

    public abstract e c();

    public final String d() {
        e c2 = c();
        try {
            return c2.a(c.a(c2, e()));
        } finally {
            c.a(c2);
        }
    }

    private Charset e() {
        s a2 = a();
        return a2 != null ? a2.a(c.f7823e) : c.f7823e;
    }

    public void close() {
        c.a(c());
    }

    public static y a(s sVar, byte[] bArr) {
        return a(sVar, (long) bArr.length, new okio.c().c(bArr));
    }

    public static y a(final s sVar, final long j, final e eVar) {
        if (eVar != null) {
            return new y() {
                public s a() {
                    return sVar;
                }

                public long b() {
                    return j;
                }

                public e c() {
                    return eVar;
                }
            };
        }
        throw new NullPointerException("source == null");
    }
}
