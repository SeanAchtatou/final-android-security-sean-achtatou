package okhttp3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLSocket;
import okhttp3.internal.c;

/* compiled from: ConnectionSpec */
public final class k {

    /* renamed from: a  reason: collision with root package name */
    public static final k f8081a = new a(true).a(f8084h).a(TlsVersion.TLS_1_3, TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0).a(true).a();

    /* renamed from: b  reason: collision with root package name */
    public static final k f8082b = new a(f8081a).a(TlsVersion.TLS_1_0).a(true).a();

    /* renamed from: c  reason: collision with root package name */
    public static final k f8083c = new a(false).a();

    /* renamed from: h  reason: collision with root package name */
    private static final h[] f8084h = {h.aW, h.ba, h.aX, h.bb, h.bh, h.bg, h.ax, h.aH, h.ay, h.aI, h.af, h.ag, h.D, h.H, h.f7758h};

    /* renamed from: d  reason: collision with root package name */
    final boolean f8085d;

    /* renamed from: e  reason: collision with root package name */
    final boolean f8086e;

    /* renamed from: f  reason: collision with root package name */
    final String[] f8087f;

    /* renamed from: g  reason: collision with root package name */
    final String[] f8088g;

    k(a aVar) {
        this.f8085d = aVar.f8089a;
        this.f8087f = aVar.f8090b;
        this.f8088g = aVar.f8091c;
        this.f8086e = aVar.f8092d;
    }

    public boolean a() {
        return this.f8085d;
    }

    public List<h> b() {
        if (this.f8087f == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.f8087f.length);
        for (String a2 : this.f8087f) {
            arrayList.add(h.a(a2));
        }
        return Collections.unmodifiableList(arrayList);
    }

    public List<TlsVersion> c() {
        if (this.f8088g == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.f8088g.length);
        for (String a2 : this.f8088g) {
            arrayList.add(TlsVersion.a(a2));
        }
        return Collections.unmodifiableList(arrayList);
    }

    public boolean d() {
        return this.f8086e;
    }

    /* access modifiers changed from: package-private */
    public void a(SSLSocket sSLSocket, boolean z) {
        k b2 = b(sSLSocket, z);
        if (b2.f8088g != null) {
            sSLSocket.setEnabledProtocols(b2.f8088g);
        }
        if (b2.f8087f != null) {
            sSLSocket.setEnabledCipherSuites(b2.f8087f);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.c.a(java.lang.Object[], java.lang.Object):int
     arg types: [java.lang.String[], java.lang.String]
     candidates:
      okhttp3.internal.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      okhttp3.internal.c.a(okhttp3.HttpUrl, boolean):java.lang.String
      okhttp3.internal.c.a(okio.e, java.nio.charset.Charset):java.nio.charset.Charset
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      okhttp3.internal.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      okhttp3.internal.c.a(java.lang.Object, java.lang.Object):boolean
      okhttp3.internal.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object):int */
    private k b(SSLSocket sSLSocket, boolean z) {
        String[] enabledCipherSuites;
        String[] enabledProtocols;
        if (this.f8087f != null) {
            enabledCipherSuites = (String[]) c.a(String.class, this.f8087f, sSLSocket.getEnabledCipherSuites());
        } else {
            enabledCipherSuites = sSLSocket.getEnabledCipherSuites();
        }
        if (this.f8088g != null) {
            enabledProtocols = (String[]) c.a(String.class, this.f8088g, sSLSocket.getEnabledProtocols());
        } else {
            enabledProtocols = sSLSocket.getEnabledProtocols();
        }
        if (z && c.a((Object[]) sSLSocket.getSupportedCipherSuites(), (Object) "TLS_FALLBACK_SCSV") != -1) {
            enabledCipherSuites = c.a(enabledCipherSuites, "TLS_FALLBACK_SCSV");
        }
        return new a(this).a(enabledCipherSuites).b(enabledProtocols).a();
    }

    public boolean a(SSLSocket sSLSocket) {
        if (!this.f8085d) {
            return false;
        }
        if (this.f8088g != null && !a(this.f8088g, sSLSocket.getEnabledProtocols())) {
            return false;
        }
        if (this.f8087f == null || a(this.f8087f, sSLSocket.getEnabledCipherSuites())) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.c.a(java.lang.Object[], java.lang.Object):int
     arg types: [java.lang.String[], java.lang.String]
     candidates:
      okhttp3.internal.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      okhttp3.internal.c.a(okhttp3.HttpUrl, boolean):java.lang.String
      okhttp3.internal.c.a(okio.e, java.nio.charset.Charset):java.nio.charset.Charset
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      okhttp3.internal.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      okhttp3.internal.c.a(java.lang.Object, java.lang.Object):boolean
      okhttp3.internal.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object):int */
    private static boolean a(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length == 0 || strArr2.length == 0) {
            return false;
        }
        for (String str : strArr) {
            if (c.a((Object[]) strArr2, (Object) str) != -1) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof k)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        k kVar = (k) obj;
        if (this.f8085d != kVar.f8085d) {
            return false;
        }
        if (!this.f8085d || (Arrays.equals(this.f8087f, kVar.f8087f) && Arrays.equals(this.f8088g, kVar.f8088g) && this.f8086e == kVar.f8086e)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (!this.f8085d) {
            return 17;
        }
        return (this.f8086e ? 0 : 1) + ((((Arrays.hashCode(this.f8087f) + 527) * 31) + Arrays.hashCode(this.f8088g)) * 31);
    }

    public String toString() {
        if (!this.f8085d) {
            return "ConnectionSpec()";
        }
        return "ConnectionSpec(cipherSuites=" + (this.f8087f != null ? b().toString() : "[all enabled]") + ", tlsVersions=" + (this.f8088g != null ? c().toString() : "[all enabled]") + ", supportsTlsExtensions=" + this.f8086e + ")";
    }

    /* compiled from: ConnectionSpec */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        boolean f8089a;

        /* renamed from: b  reason: collision with root package name */
        String[] f8090b;

        /* renamed from: c  reason: collision with root package name */
        String[] f8091c;

        /* renamed from: d  reason: collision with root package name */
        boolean f8092d;

        a(boolean z) {
            this.f8089a = z;
        }

        public a(k kVar) {
            this.f8089a = kVar.f8085d;
            this.f8090b = kVar.f8087f;
            this.f8091c = kVar.f8088g;
            this.f8092d = kVar.f8086e;
        }

        public a a(h... hVarArr) {
            if (!this.f8089a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            }
            String[] strArr = new String[hVarArr.length];
            for (int i = 0; i < hVarArr.length; i++) {
                strArr[i] = hVarArr[i].bi;
            }
            return a(strArr);
        }

        public a a(String... strArr) {
            if (!this.f8089a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length == 0) {
                throw new IllegalArgumentException("At least one cipher suite is required");
            } else {
                this.f8090b = (String[]) strArr.clone();
                return this;
            }
        }

        public a a(TlsVersion... tlsVersionArr) {
            if (!this.f8089a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            }
            String[] strArr = new String[tlsVersionArr.length];
            for (int i = 0; i < tlsVersionArr.length; i++) {
                strArr[i] = tlsVersionArr[i].f7716f;
            }
            return b(strArr);
        }

        public a b(String... strArr) {
            if (!this.f8089a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length == 0) {
                throw new IllegalArgumentException("At least one TLS version is required");
            } else {
                this.f8091c = (String[]) strArr.clone();
                return this;
            }
        }

        public a a(boolean z) {
            if (!this.f8089a) {
                throw new IllegalStateException("no TLS extensions for cleartext connections");
            }
            this.f8092d = z;
            return this;
        }

        public k a() {
            return new k(this);
        }
    }
}
