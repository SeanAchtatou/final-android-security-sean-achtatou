package okhttp3;

import java.io.Closeable;
import java.io.Flushable;
import okhttp3.internal.a.d;
import okhttp3.internal.a.e;

/* compiled from: Cache */
public final class c implements Closeable, Flushable {

    /* renamed from: a  reason: collision with root package name */
    final e f7726a;

    /* renamed from: b  reason: collision with root package name */
    final d f7727b;

    public void flush() {
        this.f7727b.flush();
    }

    public void close() {
        this.f7727b.close();
    }
}
