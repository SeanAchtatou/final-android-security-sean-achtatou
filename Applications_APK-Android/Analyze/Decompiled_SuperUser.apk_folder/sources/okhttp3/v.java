package okhttp3;

import okhttp3.internal.b.f;
import okhttp3.internal.c;
import okhttp3.q;

/* compiled from: Request */
public final class v {

    /* renamed from: a  reason: collision with root package name */
    final HttpUrl f8145a;

    /* renamed from: b  reason: collision with root package name */
    final String f8146b;

    /* renamed from: c  reason: collision with root package name */
    final q f8147c;

    /* renamed from: d  reason: collision with root package name */
    final w f8148d;

    /* renamed from: e  reason: collision with root package name */
    final Object f8149e;

    /* renamed from: f  reason: collision with root package name */
    private volatile d f8150f;

    v(a aVar) {
        Object obj;
        this.f8145a = aVar.f8151a;
        this.f8146b = aVar.f8152b;
        this.f8147c = aVar.f8153c.a();
        this.f8148d = aVar.f8154d;
        if (aVar.f8155e != null) {
            obj = aVar.f8155e;
        } else {
            obj = this;
        }
        this.f8149e = obj;
    }

    public HttpUrl a() {
        return this.f8145a;
    }

    public String b() {
        return this.f8146b;
    }

    public q c() {
        return this.f8147c;
    }

    public String a(String str) {
        return this.f8147c.a(str);
    }

    public w d() {
        return this.f8148d;
    }

    public a e() {
        return new a(this);
    }

    public d f() {
        d dVar = this.f8150f;
        if (dVar != null) {
            return dVar;
        }
        d a2 = d.a(this.f8147c);
        this.f8150f = a2;
        return a2;
    }

    public boolean g() {
        return this.f8145a.c();
    }

    public String toString() {
        return "Request{method=" + this.f8146b + ", url=" + this.f8145a + ", tag=" + (this.f8149e != this ? this.f8149e : null) + '}';
    }

    /* compiled from: Request */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        HttpUrl f8151a;

        /* renamed from: b  reason: collision with root package name */
        String f8152b;

        /* renamed from: c  reason: collision with root package name */
        q.a f8153c;

        /* renamed from: d  reason: collision with root package name */
        w f8154d;

        /* renamed from: e  reason: collision with root package name */
        Object f8155e;

        public a() {
            this.f8152b = "GET";
            this.f8153c = new q.a();
        }

        a(v vVar) {
            this.f8151a = vVar.f8145a;
            this.f8152b = vVar.f8146b;
            this.f8154d = vVar.f8148d;
            this.f8155e = vVar.f8149e;
            this.f8153c = vVar.f8147c.b();
        }

        public a a(HttpUrl httpUrl) {
            if (httpUrl == null) {
                throw new NullPointerException("url == null");
            }
            this.f8151a = httpUrl;
            return this;
        }

        public a a(String str) {
            if (str == null) {
                throw new NullPointerException("url == null");
            }
            if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                str = "http:" + str.substring(3);
            } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                str = "https:" + str.substring(4);
            }
            HttpUrl e2 = HttpUrl.e(str);
            if (e2 != null) {
                return a(e2);
            }
            throw new IllegalArgumentException("unexpected url: " + str);
        }

        public a a(String str, String str2) {
            this.f8153c.c(str, str2);
            return this;
        }

        public a b(String str, String str2) {
            this.f8153c.a(str, str2);
            return this;
        }

        public a b(String str) {
            this.f8153c.b(str);
            return this;
        }

        public a a(q qVar) {
            this.f8153c = qVar.b();
            return this;
        }

        public a a(w wVar) {
            return a("POST", wVar);
        }

        public a b(w wVar) {
            return a("DELETE", wVar);
        }

        public a a() {
            return b(c.f7822d);
        }

        public a a(String str, w wVar) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (wVar != null && !f.c(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (wVar != null || !f.b(str)) {
                this.f8152b = str;
                this.f8154d = wVar;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        public v b() {
            if (this.f8151a != null) {
                return new v(this);
            }
            throw new IllegalStateException("url == null");
        }
    }
}
