package okhttp3;

import java.net.InetSocketAddress;
import java.net.Proxy;

/* compiled from: Route */
public final class z {

    /* renamed from: a  reason: collision with root package name */
    final a f8179a;

    /* renamed from: b  reason: collision with root package name */
    final Proxy f8180b;

    /* renamed from: c  reason: collision with root package name */
    final InetSocketAddress f8181c;

    public z(a aVar, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (aVar == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress == null) {
            throw new NullPointerException("inetSocketAddress == null");
        } else {
            this.f8179a = aVar;
            this.f8180b = proxy;
            this.f8181c = inetSocketAddress;
        }
    }

    public a a() {
        return this.f8179a;
    }

    public Proxy b() {
        return this.f8180b;
    }

    public InetSocketAddress c() {
        return this.f8181c;
    }

    public boolean d() {
        return this.f8179a.i != null && this.f8180b.type() == Proxy.Type.HTTP;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof z)) {
            return false;
        }
        z zVar = (z) obj;
        if (!this.f8179a.equals(zVar.f8179a) || !this.f8180b.equals(zVar.f8180b) || !this.f8181c.equals(zVar.f8181c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((((this.f8179a.hashCode() + 527) * 31) + this.f8180b.hashCode()) * 31) + this.f8181c.hashCode();
    }

    public String toString() {
        return "Route{" + this.f8181c + "}";
    }
}
