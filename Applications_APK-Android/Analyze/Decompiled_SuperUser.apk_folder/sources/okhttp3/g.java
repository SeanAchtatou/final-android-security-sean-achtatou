package okhttp3;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;
import okhttp3.internal.c;
import okio.ByteString;

/* compiled from: CertificatePinner */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    public static final g f7743a = new a().a();

    /* renamed from: b  reason: collision with root package name */
    private final Set<b> f7744b;

    /* renamed from: c  reason: collision with root package name */
    private final okhttp3.internal.f.b f7745c;

    g(Set<b> set, okhttp3.internal.f.b bVar) {
        this.f7744b = set;
        this.f7745c = bVar;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof g) && c.a(this.f7745c, ((g) obj).f7745c) && this.f7744b.equals(((g) obj).f7744b);
    }

    public int hashCode() {
        return ((this.f7745c != null ? this.f7745c.hashCode() : 0) * 31) + this.f7744b.hashCode();
    }

    public void a(String str, List<Certificate> list) {
        List<b> a2 = a(str);
        if (!a2.isEmpty()) {
            if (this.f7745c != null) {
                list = this.f7745c.a(list, str);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i);
                int size2 = a2.size();
                int i2 = 0;
                ByteString byteString = null;
                ByteString byteString2 = null;
                while (i2 < size2) {
                    b bVar = a2.get(i2);
                    if (bVar.f7749c.equals("sha256/")) {
                        if (byteString == null) {
                            byteString = b(x509Certificate);
                        }
                        if (bVar.f7750d.equals(byteString)) {
                            return;
                        }
                    } else if (bVar.f7749c.equals("sha1/")) {
                        if (byteString2 == null) {
                            byteString2 = a(x509Certificate);
                        }
                        if (bVar.f7750d.equals(byteString2)) {
                            return;
                        }
                    } else {
                        throw new AssertionError();
                    }
                    i2++;
                    byteString2 = byteString2;
                    byteString = byteString;
                }
            }
            StringBuilder append = new StringBuilder().append("Certificate pinning failure!").append("\n  Peer certificate chain:");
            int size3 = list.size();
            for (int i3 = 0; i3 < size3; i3++) {
                X509Certificate x509Certificate2 = (X509Certificate) list.get(i3);
                append.append("\n    ").append(a((Certificate) x509Certificate2)).append(": ").append(x509Certificate2.getSubjectDN().getName());
            }
            append.append("\n  Pinned certificates for ").append(str).append(":");
            int size4 = a2.size();
            for (int i4 = 0; i4 < size4; i4++) {
                append.append("\n    ").append(a2.get(i4));
            }
            throw new SSLPeerUnverifiedException(append.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public List<b> a(String str) {
        List<b> emptyList = Collections.emptyList();
        for (b next : this.f7744b) {
            if (next.a(str)) {
                if (emptyList.isEmpty()) {
                    emptyList = new ArrayList<>();
                }
                emptyList.add(next);
            }
        }
        return emptyList;
    }

    /* access modifiers changed from: package-private */
    public g a(okhttp3.internal.f.b bVar) {
        return c.a(this.f7745c, bVar) ? this : new g(this.f7744b, bVar);
    }

    public static String a(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha256/" + b((X509Certificate) certificate).b();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    static ByteString a(X509Certificate x509Certificate) {
        return ByteString.a(x509Certificate.getPublicKey().getEncoded()).c();
    }

    static ByteString b(X509Certificate x509Certificate) {
        return ByteString.a(x509Certificate.getPublicKey().getEncoded()).d();
    }

    /* compiled from: CertificatePinner */
    static final class b {

        /* renamed from: a  reason: collision with root package name */
        final String f7747a;

        /* renamed from: b  reason: collision with root package name */
        final String f7748b;

        /* renamed from: c  reason: collision with root package name */
        final String f7749c;

        /* renamed from: d  reason: collision with root package name */
        final ByteString f7750d;

        /* access modifiers changed from: package-private */
        public boolean a(String str) {
            if (!this.f7747a.startsWith("*.")) {
                return str.equals(this.f7748b);
            }
            return str.regionMatches(false, str.indexOf(46) + 1, this.f7748b, 0, this.f7748b.length());
        }

        public boolean equals(Object obj) {
            return (obj instanceof b) && this.f7747a.equals(((b) obj).f7747a) && this.f7749c.equals(((b) obj).f7749c) && this.f7750d.equals(((b) obj).f7750d);
        }

        public int hashCode() {
            return ((((this.f7747a.hashCode() + 527) * 31) + this.f7749c.hashCode()) * 31) + this.f7750d.hashCode();
        }

        public String toString() {
            return this.f7749c + this.f7750d.b();
        }
    }

    /* compiled from: CertificatePinner */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        private final List<b> f7746a = new ArrayList();

        public g a() {
            return new g(new LinkedHashSet(this.f7746a), null);
        }
    }
}
