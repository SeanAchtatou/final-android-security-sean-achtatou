package okhttp3;

import java.util.Collections;
import java.util.List;

/* compiled from: CookieJar */
public interface m {

    /* renamed from: a  reason: collision with root package name */
    public static final m f8101a = new m() {
        public void a(HttpUrl httpUrl, List<l> list) {
        }

        public List<l> a(HttpUrl httpUrl) {
            return Collections.emptyList();
        }
    };

    List<l> a(HttpUrl httpUrl);

    void a(HttpUrl httpUrl, List<l> list);
}
