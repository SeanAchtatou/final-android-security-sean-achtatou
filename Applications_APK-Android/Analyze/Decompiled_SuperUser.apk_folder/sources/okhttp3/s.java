package okhttp3;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: MediaType */
public final class s {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f8116a = Pattern.compile("([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)/([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)");

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f8117b = Pattern.compile(";\\s*(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)=(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)|\"([^\"]*)\"))?");

    /* renamed from: c  reason: collision with root package name */
    private final String f8118c;

    /* renamed from: d  reason: collision with root package name */
    private final String f8119d;

    /* renamed from: e  reason: collision with root package name */
    private final String f8120e;

    /* renamed from: f  reason: collision with root package name */
    private final String f8121f;

    private s(String str, String str2, String str3, String str4) {
        this.f8118c = str;
        this.f8119d = str2;
        this.f8120e = str3;
        this.f8121f = str4;
    }

    public static s a(String str) {
        Matcher matcher = f8116a.matcher(str);
        if (!matcher.lookingAt()) {
            return null;
        }
        String lowerCase = matcher.group(1).toLowerCase(Locale.US);
        String lowerCase2 = matcher.group(2).toLowerCase(Locale.US);
        Matcher matcher2 = f8117b.matcher(str);
        String str2 = null;
        for (int end = matcher.end(); end < str.length(); end = matcher2.end()) {
            matcher2.region(end, str.length());
            if (!matcher2.lookingAt()) {
                return null;
            }
            String group = matcher2.group(1);
            if (group != null && group.equalsIgnoreCase("charset")) {
                String group2 = matcher2.group(2);
                if (group2 == null) {
                    group2 = matcher2.group(3);
                } else if (group2.startsWith("'") && group2.endsWith("'") && group2.length() > 2) {
                    group2 = group2.substring(1, group2.length() - 1);
                }
                if (str2 == null || group2.equalsIgnoreCase(str2)) {
                    str2 = group2;
                } else {
                    throw new IllegalArgumentException("Multiple different charsets: " + str);
                }
            }
        }
        return new s(str, lowerCase, lowerCase2, str2);
    }

    public Charset a() {
        if (this.f8121f != null) {
            return Charset.forName(this.f8121f);
        }
        return null;
    }

    public Charset a(Charset charset) {
        return this.f8121f != null ? Charset.forName(this.f8121f) : charset;
    }

    public String toString() {
        return this.f8118c;
    }

    public boolean equals(Object obj) {
        return (obj instanceof s) && ((s) obj).f8118c.equals(this.f8118c);
    }

    public int hashCode() {
        return this.f8118c.hashCode();
    }
}
