package okhttp3;

import java.nio.charset.Charset;
import okhttp3.internal.c;
import okio.d;

/* compiled from: RequestBody */
public abstract class w {
    public abstract s a();

    public abstract void a(d dVar);

    public long b() {
        return -1;
    }

    public static w a(s sVar, String str) {
        Charset charset = c.f7823e;
        if (sVar != null && (charset = sVar.a()) == null) {
            charset = c.f7823e;
            sVar = s.a(sVar + "; charset=utf-8");
        }
        return a(sVar, str.getBytes(charset));
    }

    public static w a(s sVar, byte[] bArr) {
        return a(sVar, bArr, 0, bArr.length);
    }

    public static w a(final s sVar, final byte[] bArr, final int i, final int i2) {
        if (bArr == null) {
            throw new NullPointerException("content == null");
        }
        c.a((long) bArr.length, (long) i, (long) i2);
        return new w() {
            public s a() {
                return sVar;
            }

            public long b() {
                return (long) i2;
            }

            public void a(d dVar) {
                dVar.c(bArr, i, i2);
            }
        };
    }
}
