package okhttp3;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.c;
import okhttp3.u;

/* compiled from: Dispatcher */
public final class n {

    /* renamed from: a  reason: collision with root package name */
    private int f8102a = 64;

    /* renamed from: b  reason: collision with root package name */
    private int f8103b = 5;

    /* renamed from: c  reason: collision with root package name */
    private Runnable f8104c;

    /* renamed from: d  reason: collision with root package name */
    private ExecutorService f8105d;

    /* renamed from: e  reason: collision with root package name */
    private final Deque<u.a> f8106e = new ArrayDeque();

    /* renamed from: f  reason: collision with root package name */
    private final Deque<u.a> f8107f = new ArrayDeque();

    /* renamed from: g  reason: collision with root package name */
    private final Deque<u> f8108g = new ArrayDeque();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object):int
      okhttp3.internal.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      okhttp3.internal.c.a(okhttp3.HttpUrl, boolean):java.lang.String
      okhttp3.internal.c.a(okio.e, java.nio.charset.Charset):java.nio.charset.Charset
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      okhttp3.internal.c.a(java.lang.Object, java.lang.Object):boolean
      okhttp3.internal.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      okhttp3.internal.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    public synchronized ExecutorService a() {
        if (this.f8105d == null) {
            this.f8105d = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), c.a("OkHttp Dispatcher", false));
        }
        return this.f8105d;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(u.a aVar) {
        if (this.f8107f.size() >= this.f8102a || c(aVar) >= this.f8103b) {
            this.f8106e.add(aVar);
        } else {
            this.f8107f.add(aVar);
            a().execute(aVar);
        }
    }

    public synchronized void b() {
        for (u.a b2 : this.f8106e) {
            b2.b().b();
        }
        for (u.a b3 : this.f8107f) {
            b3.b().b();
        }
        for (u b4 : this.f8108g) {
            b4.b();
        }
    }

    private void d() {
        if (this.f8107f.size() < this.f8102a && !this.f8106e.isEmpty()) {
            Iterator<u.a> it = this.f8106e.iterator();
            while (it.hasNext()) {
                u.a next = it.next();
                if (c(next) < this.f8103b) {
                    it.remove();
                    this.f8107f.add(next);
                    a().execute(next);
                }
                if (this.f8107f.size() >= this.f8102a) {
                    return;
                }
            }
        }
    }

    private int c(u.a aVar) {
        int i = 0;
        Iterator<u.a> it = this.f8107f.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            if (it.next().a().equals(aVar.a())) {
                i = i2 + 1;
            } else {
                i = i2;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(u.a aVar) {
        a(this.f8107f, aVar, true);
    }

    private <T> void a(Deque<T> deque, T t, boolean z) {
        int c2;
        Runnable runnable;
        synchronized (this) {
            if (!deque.remove(t)) {
                throw new AssertionError("Call wasn't in-flight!");
            }
            if (z) {
                d();
            }
            c2 = c();
            runnable = this.f8104c;
        }
        if (c2 == 0 && runnable != null) {
            runnable.run();
        }
    }

    public synchronized int c() {
        return this.f8107f.size() + this.f8108g.size();
    }
}
