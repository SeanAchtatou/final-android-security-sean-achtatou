package okhttp3;

import java.util.concurrent.TimeUnit;
import okhttp3.internal.b.e;

/* compiled from: CacheControl */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    public static final d f7728a = new a().a().c();

    /* renamed from: b  reason: collision with root package name */
    public static final d f7729b = new a().b().a(Integer.MAX_VALUE, TimeUnit.SECONDS).c();

    /* renamed from: c  reason: collision with root package name */
    String f7730c;

    /* renamed from: d  reason: collision with root package name */
    private final boolean f7731d;

    /* renamed from: e  reason: collision with root package name */
    private final boolean f7732e;

    /* renamed from: f  reason: collision with root package name */
    private final int f7733f;

    /* renamed from: g  reason: collision with root package name */
    private final int f7734g;

    /* renamed from: h  reason: collision with root package name */
    private final boolean f7735h;
    private final boolean i;
    private final boolean j;
    private final int k;
    private final int l;
    private final boolean m;
    private final boolean n;

    private d(boolean z, boolean z2, int i2, int i3, boolean z3, boolean z4, boolean z5, int i4, int i5, boolean z6, boolean z7, String str) {
        this.f7731d = z;
        this.f7732e = z2;
        this.f7733f = i2;
        this.f7734g = i3;
        this.f7735h = z3;
        this.i = z4;
        this.j = z5;
        this.k = i4;
        this.l = i5;
        this.m = z6;
        this.n = z7;
        this.f7730c = str;
    }

    d(a aVar) {
        this.f7731d = aVar.f7736a;
        this.f7732e = aVar.f7737b;
        this.f7733f = aVar.f7738c;
        this.f7734g = -1;
        this.f7735h = false;
        this.i = false;
        this.j = false;
        this.k = aVar.f7739d;
        this.l = aVar.f7740e;
        this.m = aVar.f7741f;
        this.n = aVar.f7742g;
    }

    public boolean a() {
        return this.f7731d;
    }

    public boolean b() {
        return this.f7732e;
    }

    public int c() {
        return this.f7733f;
    }

    public boolean d() {
        return this.f7735h;
    }

    public boolean e() {
        return this.i;
    }

    public boolean f() {
        return this.j;
    }

    public int g() {
        return this.k;
    }

    public int h() {
        return this.l;
    }

    public boolean i() {
        return this.m;
    }

    public static d a(q qVar) {
        boolean z;
        String str;
        String str2;
        boolean z2 = false;
        boolean z3 = false;
        int i2 = -1;
        int i3 = -1;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        int i4 = -1;
        int i5 = -1;
        boolean z7 = false;
        boolean z8 = false;
        boolean z9 = true;
        int a2 = qVar.a();
        int i6 = 0;
        String str3 = null;
        while (true) {
            z = z2;
            if (i6 >= a2) {
                break;
            }
            String a3 = qVar.a(i6);
            String b2 = qVar.b(i6);
            if (a3.equalsIgnoreCase("Cache-Control")) {
                if (str3 != null) {
                    z9 = false;
                } else {
                    str3 = b2;
                }
            } else if (a3.equalsIgnoreCase("Pragma")) {
                z9 = false;
            } else {
                z2 = z;
                i6++;
            }
            z2 = z;
            int i7 = 0;
            while (i7 < b2.length()) {
                int a4 = e.a(b2, i7, "=,;");
                String trim = b2.substring(i7, a4).trim();
                if (a4 == b2.length() || b2.charAt(a4) == ',' || b2.charAt(a4) == ';') {
                    i7 = a4 + 1;
                    str2 = null;
                } else {
                    int a5 = e.a(b2, a4 + 1);
                    if (a5 >= b2.length() || b2.charAt(a5) != '\"') {
                        int a6 = e.a(b2, a5, ",;");
                        String trim2 = b2.substring(a5, a6).trim();
                        i7 = a6;
                        str2 = trim2;
                    } else {
                        int i8 = a5 + 1;
                        int a7 = e.a(b2, i8, "\"");
                        String substring = b2.substring(i8, a7);
                        i7 = a7 + 1;
                        str2 = substring;
                    }
                }
                if ("no-cache".equalsIgnoreCase(trim)) {
                    z2 = true;
                } else if ("no-store".equalsIgnoreCase(trim)) {
                    z3 = true;
                } else if ("max-age".equalsIgnoreCase(trim)) {
                    i2 = e.b(str2, -1);
                } else if ("s-maxage".equalsIgnoreCase(trim)) {
                    i3 = e.b(str2, -1);
                } else if ("private".equalsIgnoreCase(trim)) {
                    z4 = true;
                } else if ("public".equalsIgnoreCase(trim)) {
                    z5 = true;
                } else if ("must-revalidate".equalsIgnoreCase(trim)) {
                    z6 = true;
                } else if ("max-stale".equalsIgnoreCase(trim)) {
                    i4 = e.b(str2, Integer.MAX_VALUE);
                } else if ("min-fresh".equalsIgnoreCase(trim)) {
                    i5 = e.b(str2, -1);
                } else if ("only-if-cached".equalsIgnoreCase(trim)) {
                    z7 = true;
                } else if ("no-transform".equalsIgnoreCase(trim)) {
                    z8 = true;
                }
            }
            i6++;
        }
        if (!z9) {
            str = null;
        } else {
            str = str3;
        }
        return new d(z, z3, i2, i3, z4, z5, z6, i4, i5, z7, z8, str);
    }

    public String toString() {
        String str = this.f7730c;
        if (str != null) {
            return str;
        }
        String j2 = j();
        this.f7730c = j2;
        return j2;
    }

    private String j() {
        StringBuilder sb = new StringBuilder();
        if (this.f7731d) {
            sb.append("no-cache, ");
        }
        if (this.f7732e) {
            sb.append("no-store, ");
        }
        if (this.f7733f != -1) {
            sb.append("max-age=").append(this.f7733f).append(", ");
        }
        if (this.f7734g != -1) {
            sb.append("s-maxage=").append(this.f7734g).append(", ");
        }
        if (this.f7735h) {
            sb.append("private, ");
        }
        if (this.i) {
            sb.append("public, ");
        }
        if (this.j) {
            sb.append("must-revalidate, ");
        }
        if (this.k != -1) {
            sb.append("max-stale=").append(this.k).append(", ");
        }
        if (this.l != -1) {
            sb.append("min-fresh=").append(this.l).append(", ");
        }
        if (this.m) {
            sb.append("only-if-cached, ");
        }
        if (this.n) {
            sb.append("no-transform, ");
        }
        if (sb.length() == 0) {
            return "";
        }
        sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }

    /* compiled from: CacheControl */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        boolean f7736a;

        /* renamed from: b  reason: collision with root package name */
        boolean f7737b;

        /* renamed from: c  reason: collision with root package name */
        int f7738c = -1;

        /* renamed from: d  reason: collision with root package name */
        int f7739d = -1;

        /* renamed from: e  reason: collision with root package name */
        int f7740e = -1;

        /* renamed from: f  reason: collision with root package name */
        boolean f7741f;

        /* renamed from: g  reason: collision with root package name */
        boolean f7742g;

        public a a() {
            this.f7736a = true;
            return this;
        }

        public a a(int i, TimeUnit timeUnit) {
            if (i < 0) {
                throw new IllegalArgumentException("maxStale < 0: " + i);
            }
            long seconds = timeUnit.toSeconds((long) i);
            this.f7739d = seconds > 2147483647L ? Integer.MAX_VALUE : (int) seconds;
            return this;
        }

        public a b() {
            this.f7741f = true;
            return this;
        }

        public d c() {
            return new d(this);
        }
    }
}
