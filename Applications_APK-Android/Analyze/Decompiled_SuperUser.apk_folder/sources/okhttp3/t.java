package okhttp3;

import java.net.Proxy;
import java.net.ProxySelector;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import okhttp3.internal.a.e;
import okhttp3.internal.c;
import okhttp3.internal.connection.d;
import okhttp3.internal.connection.f;
import okhttp3.internal.f.b;
import okhttp3.q;
import okhttp3.x;

/* compiled from: OkHttpClient */
public class t implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    static final List<Protocol> f8122a = c.a(Protocol.HTTP_2, Protocol.HTTP_1_1);

    /* renamed from: b  reason: collision with root package name */
    static final List<k> f8123b = c.a(k.f8081a, k.f8082b, k.f8083c);
    final int A;
    final int B;

    /* renamed from: c  reason: collision with root package name */
    final n f8124c;

    /* renamed from: d  reason: collision with root package name */
    final Proxy f8125d;

    /* renamed from: e  reason: collision with root package name */
    final List<Protocol> f8126e;

    /* renamed from: f  reason: collision with root package name */
    final List<k> f8127f;

    /* renamed from: g  reason: collision with root package name */
    final List<r> f8128g;

    /* renamed from: h  reason: collision with root package name */
    final List<r> f8129h;
    final ProxySelector i;
    final m j;
    final c k;
    final e l;
    final SocketFactory m;
    final SSLSocketFactory n;
    final b o;
    final HostnameVerifier p;
    final g q;
    final b r;
    final b s;
    final j t;
    final o u;
    final boolean v;
    final boolean w;
    final boolean x;
    final int y;
    final int z;

    static {
        okhttp3.internal.a.f7759a = new okhttp3.internal.a() {
            public void a(q.a aVar, String str) {
                aVar.a(str);
            }

            public void a(q.a aVar, String str, String str2) {
                aVar.b(str, str2);
            }

            public boolean a(j jVar, okhttp3.internal.connection.c cVar) {
                return jVar.b(cVar);
            }

            public okhttp3.internal.connection.c a(j jVar, a aVar, f fVar) {
                return jVar.a(aVar, fVar);
            }

            public Socket b(j jVar, a aVar, f fVar) {
                return jVar.b(aVar, fVar);
            }

            public void b(j jVar, okhttp3.internal.connection.c cVar) {
                jVar.a(cVar);
            }

            public d a(j jVar) {
                return jVar.f8074a;
            }

            public int a(x.a aVar) {
                return aVar.f8170c;
            }

            public void a(k kVar, SSLSocket sSLSocket, boolean z) {
                kVar.a(sSLSocket, z);
            }
        };
    }

    public t() {
        this(new a());
    }

    t(a aVar) {
        boolean z2;
        this.f8124c = aVar.f8130a;
        this.f8125d = aVar.f8131b;
        this.f8126e = aVar.f8132c;
        this.f8127f = aVar.f8133d;
        this.f8128g = c.a(aVar.f8134e);
        this.f8129h = c.a(aVar.f8135f);
        this.i = aVar.f8136g;
        this.j = aVar.f8137h;
        this.k = aVar.i;
        this.l = aVar.j;
        this.m = aVar.k;
        boolean z3 = false;
        for (k next : this.f8127f) {
            if (z3 || next.a()) {
                z2 = true;
            } else {
                z2 = false;
            }
            z3 = z2;
        }
        if (aVar.l != null || !z3) {
            this.n = aVar.l;
            this.o = aVar.m;
        } else {
            X509TrustManager x2 = x();
            this.n = a(x2);
            this.o = b.a(x2);
        }
        this.p = aVar.n;
        this.q = aVar.o.a(this.o);
        this.r = aVar.p;
        this.s = aVar.q;
        this.t = aVar.r;
        this.u = aVar.s;
        this.v = aVar.t;
        this.w = aVar.u;
        this.x = aVar.v;
        this.y = aVar.w;
        this.z = aVar.x;
        this.A = aVar.y;
        this.B = aVar.z;
    }

    private X509TrustManager x() {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance.init((KeyStore) null);
            TrustManager[] trustManagers = instance.getTrustManagers();
            if (trustManagers.length == 1 && (trustManagers[0] instanceof X509TrustManager)) {
                return (X509TrustManager) trustManagers[0];
            }
            throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
        } catch (GeneralSecurityException e2) {
            throw new AssertionError();
        }
    }

    private SSLSocketFactory a(X509TrustManager x509TrustManager) {
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init(null, new TrustManager[]{x509TrustManager}, null);
            return instance.getSocketFactory();
        } catch (GeneralSecurityException e2) {
            throw new AssertionError();
        }
    }

    public int a() {
        return this.y;
    }

    public int b() {
        return this.z;
    }

    public int c() {
        return this.A;
    }

    public Proxy d() {
        return this.f8125d;
    }

    public ProxySelector e() {
        return this.i;
    }

    public m f() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public e g() {
        return this.k != null ? this.k.f7726a : this.l;
    }

    public o h() {
        return this.u;
    }

    public SocketFactory i() {
        return this.m;
    }

    public SSLSocketFactory j() {
        return this.n;
    }

    public HostnameVerifier k() {
        return this.p;
    }

    public g l() {
        return this.q;
    }

    public b m() {
        return this.s;
    }

    public b n() {
        return this.r;
    }

    public j o() {
        return this.t;
    }

    public boolean p() {
        return this.v;
    }

    public boolean q() {
        return this.w;
    }

    public boolean r() {
        return this.x;
    }

    public n s() {
        return this.f8124c;
    }

    public List<Protocol> t() {
        return this.f8126e;
    }

    public List<k> u() {
        return this.f8127f;
    }

    public List<r> v() {
        return this.f8128g;
    }

    public List<r> w() {
        return this.f8129h;
    }

    public e a(v vVar) {
        return new u(this, vVar, false);
    }

    /* compiled from: OkHttpClient */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        n f8130a = new n();

        /* renamed from: b  reason: collision with root package name */
        Proxy f8131b;

        /* renamed from: c  reason: collision with root package name */
        List<Protocol> f8132c = t.f8122a;

        /* renamed from: d  reason: collision with root package name */
        List<k> f8133d = t.f8123b;

        /* renamed from: e  reason: collision with root package name */
        final List<r> f8134e = new ArrayList();

        /* renamed from: f  reason: collision with root package name */
        final List<r> f8135f = new ArrayList();

        /* renamed from: g  reason: collision with root package name */
        ProxySelector f8136g = ProxySelector.getDefault();

        /* renamed from: h  reason: collision with root package name */
        m f8137h = m.f8101a;
        c i;
        e j;
        SocketFactory k = SocketFactory.getDefault();
        SSLSocketFactory l;
        b m;
        HostnameVerifier n = okhttp3.internal.f.d.f7919a;
        g o = g.f7743a;
        b p = b.f7725a;
        b q = b.f7725a;
        j r = new j();
        o s = o.f8109a;
        boolean t = true;
        boolean u = true;
        boolean v = true;
        int w = io.fabric.sdk.android.services.common.a.DEFAULT_TIMEOUT;
        int x = io.fabric.sdk.android.services.common.a.DEFAULT_TIMEOUT;
        int y = io.fabric.sdk.android.services.common.a.DEFAULT_TIMEOUT;
        int z = 0;

        public a a(long j2, TimeUnit timeUnit) {
            this.w = a("timeout", j2, timeUnit);
            return this;
        }

        public a b(long j2, TimeUnit timeUnit) {
            this.x = a("timeout", j2, timeUnit);
            return this;
        }

        private static int a(String str, long j2, TimeUnit timeUnit) {
            if (j2 < 0) {
                throw new IllegalArgumentException(str + " < 0");
            } else if (timeUnit == null) {
                throw new NullPointerException("unit == null");
            } else {
                long millis = timeUnit.toMillis(j2);
                if (millis > 2147483647L) {
                    throw new IllegalArgumentException(str + " too large.");
                } else if (millis != 0 || j2 <= 0) {
                    return (int) millis;
                } else {
                    throw new IllegalArgumentException(str + " too small.");
                }
            }
        }

        public a a(SSLSocketFactory sSLSocketFactory, X509TrustManager x509TrustManager) {
            if (sSLSocketFactory == null) {
                throw new NullPointerException("sslSocketFactory == null");
            } else if (x509TrustManager == null) {
                throw new NullPointerException("trustManager == null");
            } else {
                this.l = sSLSocketFactory;
                this.m = b.a(x509TrustManager);
                return this;
            }
        }

        public a a(List<k> list) {
            this.f8133d = c.a(list);
            return this;
        }

        public List<r> a() {
            return this.f8134e;
        }

        public t b() {
            return new t(this);
        }
    }
}
