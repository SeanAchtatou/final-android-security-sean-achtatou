package okhttp3;

import java.io.IOException;
import java.util.ArrayList;
import okhttp3.internal.b;
import okhttp3.internal.b.g;
import okhttp3.internal.b.j;
import okhttp3.internal.e.e;

/* compiled from: RealCall */
final class u implements e {

    /* renamed from: a  reason: collision with root package name */
    final t f8138a;

    /* renamed from: b  reason: collision with root package name */
    final j f8139b;

    /* renamed from: c  reason: collision with root package name */
    final v f8140c;

    /* renamed from: d  reason: collision with root package name */
    final boolean f8141d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f8142e;

    u(t tVar, v vVar, boolean z) {
        this.f8138a = tVar;
        this.f8140c = vVar;
        this.f8141d = z;
        this.f8139b = new j(tVar, z);
    }

    public v a() {
        return this.f8140c;
    }

    private void h() {
        this.f8139b.a(e.b().a("response.body().close()"));
    }

    public void a(f fVar) {
        synchronized (this) {
            if (this.f8142e) {
                throw new IllegalStateException("Already Executed");
            }
            this.f8142e = true;
        }
        h();
        this.f8138a.s().a(new a(fVar));
    }

    public void b() {
        this.f8139b.a();
    }

    public boolean c() {
        return this.f8139b.b();
    }

    /* renamed from: d */
    public u clone() {
        return new u(this.f8138a, this.f8140c, this.f8141d);
    }

    /* compiled from: RealCall */
    final class a extends b {

        /* renamed from: c  reason: collision with root package name */
        private final f f8144c;

        a(f fVar) {
            super("OkHttp %s", u.this.f());
            this.f8144c = fVar;
        }

        /* access modifiers changed from: package-private */
        public String a() {
            return u.this.f8140c.a().f();
        }

        /* access modifiers changed from: package-private */
        public u b() {
            return u.this;
        }

        /* access modifiers changed from: protected */
        public void c() {
            boolean z = true;
            try {
                x g2 = u.this.g();
                if (u.this.f8139b.b()) {
                    try {
                        this.f8144c.a(u.this, new IOException("Canceled"));
                    } catch (IOException e2) {
                        e = e2;
                    }
                } else {
                    this.f8144c.a(u.this, g2);
                }
                u.this.f8138a.s().b(this);
                return;
            } catch (IOException e3) {
                e = e3;
                z = false;
            }
            if (z) {
                try {
                    e.b().a(4, "Callback failure for " + u.this.e(), e);
                } catch (Throwable th) {
                    u.this.f8138a.s().b(this);
                    throw th;
                }
            } else {
                this.f8144c.a(u.this, e);
            }
            u.this.f8138a.s().b(this);
        }
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return (c() ? "canceled " : "") + (this.f8141d ? "web socket" : "call") + " to " + f();
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.f8140c.a().m();
    }

    /* access modifiers changed from: package-private */
    public x g() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.f8138a.v());
        arrayList.add(this.f8139b);
        arrayList.add(new okhttp3.internal.b.a(this.f8138a.f()));
        arrayList.add(new okhttp3.internal.a.a(this.f8138a.g()));
        arrayList.add(new okhttp3.internal.connection.a(this.f8138a));
        if (!this.f8141d) {
            arrayList.addAll(this.f8138a.w());
        }
        arrayList.add(new okhttp3.internal.b.b(this.f8141d));
        return new g(arrayList, null, null, null, 0, this.f8140c).a(this.f8140c);
    }
}
