package okhttp3.internal;

/* compiled from: NamedRunnable */
public abstract class b implements Runnable {

    /* renamed from: b  reason: collision with root package name */
    protected final String f7795b;

    /* access modifiers changed from: protected */
    public abstract void c();

    public b(String str, Object... objArr) {
        this.f7795b = c.a(str, objArr);
    }

    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.f7795b);
        try {
            c();
        } finally {
            Thread.currentThread().setName(name);
        }
    }
}
