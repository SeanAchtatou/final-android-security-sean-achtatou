package okhttp3.internal.http2;

import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import okio.e;
import okio.p;
import okio.q;
import okio.r;

/* compiled from: Http2Stream */
public final class g {
    static final /* synthetic */ boolean i = (!g.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    long f8031a = 0;

    /* renamed from: b  reason: collision with root package name */
    long f8032b;

    /* renamed from: c  reason: collision with root package name */
    final int f8033c;

    /* renamed from: d  reason: collision with root package name */
    final e f8034d;

    /* renamed from: e  reason: collision with root package name */
    final a f8035e;

    /* renamed from: f  reason: collision with root package name */
    final c f8036f = new c();

    /* renamed from: g  reason: collision with root package name */
    final c f8037g = new c();

    /* renamed from: h  reason: collision with root package name */
    ErrorCode f8038h = null;
    private final List<a> j;
    private List<a> k;
    private boolean l;
    private final b m;

    g(int i2, e eVar, boolean z, boolean z2, List<a> list) {
        if (eVar == null) {
            throw new NullPointerException("connection == null");
        } else if (list == null) {
            throw new NullPointerException("requestHeaders == null");
        } else {
            this.f8033c = i2;
            this.f8034d = eVar;
            this.f8032b = (long) eVar.m.d();
            this.m = new b((long) eVar.l.d());
            this.f8035e = new a();
            this.m.f8046b = z2;
            this.f8035e.f8041b = z;
            this.j = list;
        }
    }

    public int a() {
        return this.f8033c;
    }

    public synchronized boolean b() {
        boolean z = false;
        synchronized (this) {
            if (this.f8038h == null) {
                if ((!this.m.f8046b && !this.m.f8045a) || ((!this.f8035e.f8041b && !this.f8035e.f8040a) || !this.l)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public boolean c() {
        boolean z;
        if ((this.f8033c & 1) == 1) {
            z = true;
        } else {
            z = false;
        }
        return this.f8034d.f7972b == z;
    }

    public synchronized List<a> d() {
        List<a> list;
        if (!c()) {
            throw new IllegalStateException("servers cannot read response headers");
        }
        this.f8036f.c();
        while (this.k == null && this.f8038h == null) {
            try {
                l();
            } catch (Throwable th) {
                this.f8036f.b();
                throw th;
            }
        }
        this.f8036f.b();
        list = this.k;
        if (list != null) {
            this.k = null;
        } else {
            throw new StreamResetException(this.f8038h);
        }
        return list;
    }

    public r e() {
        return this.f8036f;
    }

    public r f() {
        return this.f8037g;
    }

    public q g() {
        return this.m;
    }

    public p h() {
        synchronized (this) {
            if (!this.l && !c()) {
                throw new IllegalStateException("reply before requesting the sink");
            }
        }
        return this.f8035e;
    }

    public void a(ErrorCode errorCode) {
        if (d(errorCode)) {
            this.f8034d.b(this.f8033c, errorCode);
        }
    }

    public void b(ErrorCode errorCode) {
        if (d(errorCode)) {
            this.f8034d.a(this.f8033c, errorCode);
        }
    }

    private boolean d(ErrorCode errorCode) {
        if (i || !Thread.holdsLock(this)) {
            synchronized (this) {
                if (this.f8038h != null) {
                    return false;
                }
                if (this.m.f8046b && this.f8035e.f8041b) {
                    return false;
                }
                this.f8038h = errorCode;
                notifyAll();
                this.f8034d.b(this.f8033c);
                return true;
            }
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void a(List<a> list) {
        boolean z = true;
        if (i || !Thread.holdsLock(this)) {
            synchronized (this) {
                this.l = true;
                if (this.k == null) {
                    this.k = list;
                    z = b();
                    notifyAll();
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(this.k);
                    arrayList.add(null);
                    arrayList.addAll(list);
                    this.k = arrayList;
                }
            }
            if (!z) {
                this.f8034d.b(this.f8033c);
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar, int i2) {
        if (i || !Thread.holdsLock(this)) {
            this.m.a(eVar, (long) i2);
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void i() {
        boolean b2;
        if (i || !Thread.holdsLock(this)) {
            synchronized (this) {
                this.m.f8046b = true;
                b2 = b();
                notifyAll();
            }
            if (!b2) {
                this.f8034d.b(this.f8033c);
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public synchronized void c(ErrorCode errorCode) {
        if (this.f8038h == null) {
            this.f8038h = errorCode;
            notifyAll();
        }
    }

    /* compiled from: Http2Stream */
    private final class b implements q {

        /* renamed from: c  reason: collision with root package name */
        static final /* synthetic */ boolean f8044c = (!g.class.desiredAssertionStatus());

        /* renamed from: a  reason: collision with root package name */
        boolean f8045a;

        /* renamed from: b  reason: collision with root package name */
        boolean f8046b;

        /* renamed from: e  reason: collision with root package name */
        private final okio.c f8048e = new okio.c();

        /* renamed from: f  reason: collision with root package name */
        private final okio.c f8049f = new okio.c();

        /* renamed from: g  reason: collision with root package name */
        private final long f8050g;

        b(long j) {
            this.f8050g = j;
        }

        public long a(okio.c cVar, long j) {
            long a2;
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            }
            synchronized (g.this) {
                b();
                c();
                if (this.f8049f.b() == 0) {
                    a2 = -1;
                } else {
                    a2 = this.f8049f.a(cVar, Math.min(j, this.f8049f.b()));
                    g.this.f8031a += a2;
                    if (g.this.f8031a >= ((long) (g.this.f8034d.l.d() / 2))) {
                        g.this.f8034d.a(g.this.f8033c, g.this.f8031a);
                        g.this.f8031a = 0;
                    }
                    synchronized (g.this.f8034d) {
                        g.this.f8034d.j += a2;
                        if (g.this.f8034d.j >= ((long) (g.this.f8034d.l.d() / 2))) {
                            g.this.f8034d.a(0, g.this.f8034d.j);
                            g.this.f8034d.j = 0;
                        }
                    }
                }
            }
            return a2;
        }

        private void b() {
            g.this.f8036f.c();
            while (this.f8049f.b() == 0 && !this.f8046b && !this.f8045a && g.this.f8038h == null) {
                try {
                    g.this.l();
                } finally {
                    g.this.f8036f.b();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(e eVar, long j) {
            boolean z;
            boolean z2;
            boolean z3;
            if (f8044c || !Thread.holdsLock(g.this)) {
                while (j > 0) {
                    synchronized (g.this) {
                        z = this.f8046b;
                        z2 = this.f8049f.b() + j > this.f8050g;
                    }
                    if (z2) {
                        eVar.g(j);
                        g.this.b(ErrorCode.FLOW_CONTROL_ERROR);
                        return;
                    } else if (z) {
                        eVar.g(j);
                        return;
                    } else {
                        long a2 = eVar.a(this.f8048e, j);
                        if (a2 == -1) {
                            throw new EOFException();
                        }
                        j -= a2;
                        synchronized (g.this) {
                            if (this.f8049f.b() == 0) {
                                z3 = true;
                            } else {
                                z3 = false;
                            }
                            this.f8049f.a(this.f8048e);
                            if (z3) {
                                g.this.notifyAll();
                            }
                        }
                    }
                }
                return;
            }
            throw new AssertionError();
        }

        public r a() {
            return g.this.f8036f;
        }

        public void close() {
            synchronized (g.this) {
                this.f8045a = true;
                this.f8049f.r();
                g.this.notifyAll();
            }
            g.this.j();
        }

        private void c() {
            if (this.f8045a) {
                throw new IOException("stream closed");
            } else if (g.this.f8038h != null) {
                throw new StreamResetException(g.this.f8038h);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        boolean z;
        boolean b2;
        if (i || !Thread.holdsLock(this)) {
            synchronized (this) {
                z = !this.m.f8046b && this.m.f8045a && (this.f8035e.f8041b || this.f8035e.f8040a);
                b2 = b();
            }
            if (z) {
                a(ErrorCode.CANCEL);
            } else if (!b2) {
                this.f8034d.b(this.f8033c);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* compiled from: Http2Stream */
    final class a implements p {

        /* renamed from: c  reason: collision with root package name */
        static final /* synthetic */ boolean f8039c = (!g.class.desiredAssertionStatus());

        /* renamed from: a  reason: collision with root package name */
        boolean f8040a;

        /* renamed from: b  reason: collision with root package name */
        boolean f8041b;

        /* renamed from: e  reason: collision with root package name */
        private final okio.c f8043e = new okio.c();

        a() {
        }

        public void a_(okio.c cVar, long j) {
            if (f8039c || !Thread.holdsLock(g.this)) {
                this.f8043e.a_(cVar, j);
                while (this.f8043e.b() >= 16384) {
                    a(false);
                }
                return;
            }
            throw new AssertionError();
        }

        private void a(boolean z) {
            long min;
            synchronized (g.this) {
                g.this.f8037g.c();
                while (g.this.f8032b <= 0 && !this.f8041b && !this.f8040a && g.this.f8038h == null) {
                    try {
                        g.this.l();
                    } catch (Throwable th) {
                        g.this.f8037g.b();
                        throw th;
                    }
                }
                g.this.f8037g.b();
                g.this.k();
                min = Math.min(g.this.f8032b, this.f8043e.b());
                g.this.f8032b -= min;
            }
            g.this.f8037g.c();
            try {
                g.this.f8034d.a(g.this.f8033c, z && min == this.f8043e.b(), this.f8043e, min);
            } finally {
                g.this.f8037g.b();
            }
        }

        public void flush() {
            if (f8039c || !Thread.holdsLock(g.this)) {
                synchronized (g.this) {
                    g.this.k();
                }
                while (this.f8043e.b() > 0) {
                    a(false);
                    g.this.f8034d.b();
                }
                return;
            }
            throw new AssertionError();
        }

        public r a() {
            return g.this.f8037g;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: okhttp3.internal.http2.e.a(int, boolean, okio.c, long):void
         arg types: [int, int, ?[OBJECT, ARRAY], int]
         candidates:
          okhttp3.internal.http2.e.a(int, okio.e, int, boolean):void
          okhttp3.internal.http2.e.a(boolean, int, int, okhttp3.internal.http2.j):void
          okhttp3.internal.http2.e.a(int, boolean, okio.c, long):void */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
            if (r6.f8042d.f8035e.f8041b != false) goto L_0x004e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
            if (r6.f8043e.b() <= 0) goto L_0x0042;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
            if (r6.f8043e.b() <= 0) goto L_0x004e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x003b, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0042, code lost:
            r6.f8042d.f8034d.a(r6.f8042d.f8033c, true, (okio.c) null, 0L);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x004e, code lost:
            r1 = r6.f8042d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0050, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            r6.f8040a = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0054, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0055, code lost:
            r6.f8042d.f8034d.b();
            r6.f8042d.j();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() {
            /*
                r6 = this;
                r4 = 0
                r2 = 1
                boolean r0 = okhttp3.internal.http2.g.a.f8039c
                if (r0 != 0) goto L_0x0015
                okhttp3.internal.http2.g r0 = okhttp3.internal.http2.g.this
                boolean r0 = java.lang.Thread.holdsLock(r0)
                if (r0 == 0) goto L_0x0015
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x0015:
                okhttp3.internal.http2.g r1 = okhttp3.internal.http2.g.this
                monitor-enter(r1)
                boolean r0 = r6.f8040a     // Catch:{ all -> 0x003f }
                if (r0 == 0) goto L_0x001e
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
            L_0x001d:
                return
            L_0x001e:
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
                okhttp3.internal.http2.g r0 = okhttp3.internal.http2.g.this
                okhttp3.internal.http2.g$a r0 = r0.f8035e
                boolean r0 = r0.f8041b
                if (r0 != 0) goto L_0x004e
                okio.c r0 = r6.f8043e
                long r0 = r0.b()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0042
            L_0x0031:
                okio.c r0 = r6.f8043e
                long r0 = r0.b()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x004e
                r6.a(r2)
                goto L_0x0031
            L_0x003f:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
                throw r0
            L_0x0042:
                okhttp3.internal.http2.g r0 = okhttp3.internal.http2.g.this
                okhttp3.internal.http2.e r0 = r0.f8034d
                okhttp3.internal.http2.g r1 = okhttp3.internal.http2.g.this
                int r1 = r1.f8033c
                r3 = 0
                r0.a(r1, r2, r3, r4)
            L_0x004e:
                okhttp3.internal.http2.g r1 = okhttp3.internal.http2.g.this
                monitor-enter(r1)
                r0 = 1
                r6.f8040a = r0     // Catch:{ all -> 0x0062 }
                monitor-exit(r1)     // Catch:{ all -> 0x0062 }
                okhttp3.internal.http2.g r0 = okhttp3.internal.http2.g.this
                okhttp3.internal.http2.e r0 = r0.f8034d
                r0.b()
                okhttp3.internal.http2.g r0 = okhttp3.internal.http2.g.this
                r0.j()
                goto L_0x001d
            L_0x0062:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0062 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.http2.g.a.close():void");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.f8032b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        if (this.f8035e.f8040a) {
            throw new IOException("stream closed");
        } else if (this.f8035e.f8041b) {
            throw new IOException("stream finished");
        } else if (this.f8038h != null) {
            throw new StreamResetException(this.f8038h);
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        try {
            wait();
        } catch (InterruptedException e2) {
            throw new InterruptedIOException();
        }
    }

    /* compiled from: Http2Stream */
    class c extends okio.a {
        c() {
        }

        /* access modifiers changed from: protected */
        public void a() {
            g.this.b(ErrorCode.CANCEL);
        }

        /* access modifiers changed from: protected */
        public IOException a(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        public void b() {
            if (b_()) {
                throw a((IOException) null);
            }
        }
    }
}
