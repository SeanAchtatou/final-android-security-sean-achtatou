package okhttp3.internal.http2;

import java.util.List;
import okio.e;

/* compiled from: PushObserver */
public interface k {

    /* renamed from: a  reason: collision with root package name */
    public static final k f8069a = new k() {
        public boolean a(int i, List<a> list) {
            return true;
        }

        public boolean a(int i, List<a> list, boolean z) {
            return true;
        }

        public boolean a(int i, e eVar, int i2, boolean z) {
            eVar.g((long) i2);
            return true;
        }

        public void a(int i, ErrorCode errorCode) {
        }
    };

    void a(int i, ErrorCode errorCode);

    boolean a(int i, List<a> list);

    boolean a(int i, List<a> list, boolean z);

    boolean a(int i, e eVar, int i2, boolean z);
}
