package okhttp3.internal.http2;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.entity.UidPolicy;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okio.ByteString;
import okio.c;
import okio.e;
import okio.k;
import okio.q;

/* compiled from: Hpack */
final class b {

    /* renamed from: a  reason: collision with root package name */
    static final a[] f7940a = {new a(a.f7937f, ""), new a(a.f7934c, "GET"), new a(a.f7934c, "POST"), new a(a.f7935d, "/"), new a(a.f7935d, "/index.html"), new a(a.f7936e, "http"), new a(a.f7936e, "https"), new a(a.f7933b, "200"), new a(a.f7933b, "204"), new a(a.f7933b, "206"), new a(a.f7933b, "304"), new a(a.f7933b, "400"), new a(a.f7933b, "404"), new a(a.f7933b, "500"), new a("accept-charset", ""), new a("accept-encoding", "gzip, deflate"), new a("accept-language", ""), new a("accept-ranges", ""), new a("accept", ""), new a("access-control-allow-origin", ""), new a("age", ""), new a((String) UidPolicy.ALLOW, ""), new a("authorization", ""), new a("cache-control", ""), new a("content-disposition", ""), new a("content-encoding", ""), new a("content-language", ""), new a("content-length", ""), new a("content-location", ""), new a("content-range", ""), new a("content-type", ""), new a("cookie", ""), new a("date", ""), new a("etag", ""), new a("expect", ""), new a("expires", ""), new a("from", ""), new a("host", ""), new a("if-match", ""), new a("if-modified-since", ""), new a("if-none-match", ""), new a("if-range", ""), new a("if-unmodified-since", ""), new a("last-modified", ""), new a("link", ""), new a((String) FirebaseAnalytics.Param.LOCATION, ""), new a("max-forwards", ""), new a("proxy-authenticate", ""), new a("proxy-authorization", ""), new a("range", ""), new a("referer", ""), new a("refresh", ""), new a("retry-after", ""), new a("server", ""), new a("set-cookie", ""), new a("strict-transport-security", ""), new a("transfer-encoding", ""), new a("user-agent", ""), new a("vary", ""), new a("via", ""), new a("www-authenticate", "")};

    /* renamed from: b  reason: collision with root package name */
    static final Map<ByteString, Integer> f7941b = a();

    /* compiled from: Hpack */
    static final class a {

        /* renamed from: a  reason: collision with root package name */
        a[] f7942a;

        /* renamed from: b  reason: collision with root package name */
        int f7943b;

        /* renamed from: c  reason: collision with root package name */
        int f7944c;

        /* renamed from: d  reason: collision with root package name */
        int f7945d;

        /* renamed from: e  reason: collision with root package name */
        private final List<a> f7946e;

        /* renamed from: f  reason: collision with root package name */
        private final e f7947f;

        /* renamed from: g  reason: collision with root package name */
        private final int f7948g;

        /* renamed from: h  reason: collision with root package name */
        private int f7949h;

        a(int i, q qVar) {
            this(i, i, qVar);
        }

        a(int i, int i2, q qVar) {
            this.f7946e = new ArrayList();
            this.f7942a = new a[8];
            this.f7943b = this.f7942a.length - 1;
            this.f7944c = 0;
            this.f7945d = 0;
            this.f7948g = i;
            this.f7949h = i2;
            this.f7947f = k.a(qVar);
        }

        private void d() {
            if (this.f7949h >= this.f7945d) {
                return;
            }
            if (this.f7949h == 0) {
                e();
            } else {
                a(this.f7945d - this.f7949h);
            }
        }

        private void e() {
            Arrays.fill(this.f7942a, (Object) null);
            this.f7943b = this.f7942a.length - 1;
            this.f7944c = 0;
            this.f7945d = 0;
        }

        private int a(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.f7942a.length;
                while (true) {
                    length--;
                    if (length < this.f7943b || i <= 0) {
                        System.arraycopy(this.f7942a, this.f7943b + 1, this.f7942a, this.f7943b + 1 + i2, this.f7944c);
                        this.f7943b += i2;
                    } else {
                        i -= this.f7942a[length].i;
                        this.f7945d -= this.f7942a[length].i;
                        this.f7944c--;
                        i2++;
                    }
                }
                System.arraycopy(this.f7942a, this.f7943b + 1, this.f7942a, this.f7943b + 1 + i2, this.f7944c);
                this.f7943b += i2;
            }
            return i2;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            while (!this.f7947f.f()) {
                byte h2 = this.f7947f.h() & 255;
                if (h2 == 128) {
                    throw new IOException("index == 0");
                } else if ((h2 & 128) == 128) {
                    b(a(h2, 127) - 1);
                } else if (h2 == 64) {
                    g();
                } else if ((h2 & 64) == 64) {
                    e(a(h2, 63) - 1);
                } else if ((h2 & 32) == 32) {
                    this.f7949h = a(h2, 31);
                    if (this.f7949h < 0 || this.f7949h > this.f7948g) {
                        throw new IOException("Invalid dynamic table size update " + this.f7949h);
                    }
                    d();
                } else if (h2 == 16 || h2 == 0) {
                    f();
                } else {
                    d(a(h2, 15) - 1);
                }
            }
        }

        public List<a> b() {
            ArrayList arrayList = new ArrayList(this.f7946e);
            this.f7946e.clear();
            return arrayList;
        }

        private void b(int i) {
            if (g(i)) {
                this.f7946e.add(b.f7940a[i]);
                return;
            }
            int c2 = c(i - b.f7940a.length);
            if (c2 < 0 || c2 > this.f7942a.length - 1) {
                throw new IOException("Header index too large " + (i + 1));
            }
            this.f7946e.add(this.f7942a[c2]);
        }

        private int c(int i) {
            return this.f7943b + 1 + i;
        }

        private void d(int i) {
            this.f7946e.add(new a(f(i), c()));
        }

        private void f() {
            this.f7946e.add(new a(b.a(c()), c()));
        }

        private void e(int i) {
            a(-1, new a(f(i), c()));
        }

        private void g() {
            a(-1, new a(b.a(c()), c()));
        }

        private ByteString f(int i) {
            if (g(i)) {
                return b.f7940a[i].f7938g;
            }
            return this.f7942a[c(i - b.f7940a.length)].f7938g;
        }

        private boolean g(int i) {
            return i >= 0 && i <= b.f7940a.length + -1;
        }

        private void a(int i, a aVar) {
            this.f7946e.add(aVar);
            int i2 = aVar.i;
            if (i != -1) {
                i2 -= this.f7942a[c(i)].i;
            }
            if (i2 > this.f7949h) {
                e();
                return;
            }
            int a2 = a((this.f7945d + i2) - this.f7949h);
            if (i == -1) {
                if (this.f7944c + 1 > this.f7942a.length) {
                    a[] aVarArr = new a[(this.f7942a.length * 2)];
                    System.arraycopy(this.f7942a, 0, aVarArr, this.f7942a.length, this.f7942a.length);
                    this.f7943b = this.f7942a.length - 1;
                    this.f7942a = aVarArr;
                }
                int i3 = this.f7943b;
                this.f7943b = i3 - 1;
                this.f7942a[i3] = aVar;
                this.f7944c++;
            } else {
                this.f7942a[a2 + c(i) + i] = aVar;
            }
            this.f7945d = i2 + this.f7945d;
        }

        private int h() {
            return this.f7947f.h() & 255;
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2) {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int h2 = h();
                if ((h2 & FileUtils.FileMode.MODE_IWUSR) == 0) {
                    return (h2 << i4) + i2;
                }
                i2 += (h2 & 127) << i4;
                i4 += 7;
            }
        }

        /* access modifiers changed from: package-private */
        public ByteString c() {
            int h2 = h();
            boolean z = (h2 & FileUtils.FileMode.MODE_IWUSR) == 128;
            int a2 = a(h2, 127);
            if (z) {
                return ByteString.a(i.a().a(this.f7947f.f((long) a2)));
            }
            return this.f7947f.c((long) a2);
        }
    }

    private static Map<ByteString, Integer> a() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(f7940a.length);
        for (int i = 0; i < f7940a.length; i++) {
            if (!linkedHashMap.containsKey(f7940a[i].f7938g)) {
                linkedHashMap.put(f7940a[i].f7938g, Integer.valueOf(i));
            }
        }
        return Collections.unmodifiableMap(linkedHashMap);
    }

    /* renamed from: okhttp3.internal.http2.b$b  reason: collision with other inner class name */
    /* compiled from: Hpack */
    static final class C0114b {

        /* renamed from: a  reason: collision with root package name */
        int f7950a;

        /* renamed from: b  reason: collision with root package name */
        int f7951b;

        /* renamed from: c  reason: collision with root package name */
        a[] f7952c;

        /* renamed from: d  reason: collision with root package name */
        int f7953d;

        /* renamed from: e  reason: collision with root package name */
        int f7954e;

        /* renamed from: f  reason: collision with root package name */
        int f7955f;

        /* renamed from: g  reason: collision with root package name */
        private final c f7956g;

        /* renamed from: h  reason: collision with root package name */
        private final boolean f7957h;
        private int i;
        private boolean j;

        C0114b(c cVar) {
            this(CodedOutputStream.DEFAULT_BUFFER_SIZE, true, cVar);
        }

        C0114b(int i2, boolean z, c cVar) {
            this.i = Integer.MAX_VALUE;
            this.f7952c = new a[8];
            this.f7953d = this.f7952c.length - 1;
            this.f7954e = 0;
            this.f7955f = 0;
            this.f7950a = i2;
            this.f7951b = i2;
            this.f7957h = z;
            this.f7956g = cVar;
        }

        private void a() {
            Arrays.fill(this.f7952c, (Object) null);
            this.f7953d = this.f7952c.length - 1;
            this.f7954e = 0;
            this.f7955f = 0;
        }

        private int b(int i2) {
            int i3 = 0;
            if (i2 > 0) {
                int length = this.f7952c.length;
                while (true) {
                    length--;
                    if (length < this.f7953d || i2 <= 0) {
                        System.arraycopy(this.f7952c, this.f7953d + 1, this.f7952c, this.f7953d + 1 + i3, this.f7954e);
                        Arrays.fill(this.f7952c, this.f7953d + 1, this.f7953d + 1 + i3, (Object) null);
                        this.f7953d += i3;
                    } else {
                        i2 -= this.f7952c[length].i;
                        this.f7955f -= this.f7952c[length].i;
                        this.f7954e--;
                        i3++;
                    }
                }
                System.arraycopy(this.f7952c, this.f7953d + 1, this.f7952c, this.f7953d + 1 + i3, this.f7954e);
                Arrays.fill(this.f7952c, this.f7953d + 1, this.f7953d + 1 + i3, (Object) null);
                this.f7953d += i3;
            }
            return i3;
        }

        private void a(a aVar) {
            int i2 = aVar.i;
            if (i2 > this.f7951b) {
                a();
                return;
            }
            b((this.f7955f + i2) - this.f7951b);
            if (this.f7954e + 1 > this.f7952c.length) {
                a[] aVarArr = new a[(this.f7952c.length * 2)];
                System.arraycopy(this.f7952c, 0, aVarArr, this.f7952c.length, this.f7952c.length);
                this.f7953d = this.f7952c.length - 1;
                this.f7952c = aVarArr;
            }
            int i3 = this.f7953d;
            this.f7953d = i3 - 1;
            this.f7952c[i3] = aVar;
            this.f7954e++;
            this.f7955f = i2 + this.f7955f;
        }

        /* access modifiers changed from: package-private */
        public void a(List<a> list) {
            int i2;
            int i3;
            if (this.j) {
                if (this.i < this.f7951b) {
                    a(this.i, 31, 32);
                }
                this.j = false;
                this.i = Integer.MAX_VALUE;
                a(this.f7951b, 31, 32);
            }
            int size = list.size();
            for (int i4 = 0; i4 < size; i4++) {
                a aVar = list.get(i4);
                ByteString f2 = aVar.f7938g.f();
                ByteString byteString = aVar.f7939h;
                Integer num = b.f7941b.get(f2);
                if (num != null) {
                    i2 = num.intValue() + 1;
                    if (i2 > 1 && i2 < 8) {
                        if (okhttp3.internal.c.a(b.f7940a[i2 - 1].f7939h, byteString)) {
                            i3 = i2;
                        } else if (okhttp3.internal.c.a(b.f7940a[i2].f7939h, byteString)) {
                            i3 = i2 + 1;
                        }
                    }
                    i3 = -1;
                } else {
                    i2 = -1;
                    i3 = -1;
                }
                if (i3 == -1) {
                    int i5 = this.f7953d + 1;
                    int length = this.f7952c.length;
                    while (true) {
                        if (i5 >= length) {
                            break;
                        }
                        if (okhttp3.internal.c.a(this.f7952c[i5].f7938g, f2)) {
                            if (okhttp3.internal.c.a(this.f7952c[i5].f7939h, byteString)) {
                                i3 = (i5 - this.f7953d) + b.f7940a.length;
                                break;
                            } else if (i2 == -1) {
                                i2 = (i5 - this.f7953d) + b.f7940a.length;
                            }
                        }
                        i5++;
                    }
                }
                if (i3 != -1) {
                    a(i3, 127, FileUtils.FileMode.MODE_IWUSR);
                } else if (i2 == -1) {
                    this.f7956g.i(64);
                    a(f2);
                    a(byteString);
                    a(aVar);
                } else if (!f2.a(a.f7932a) || a.f7937f.equals(f2)) {
                    a(i2, 63, 64);
                    a(byteString);
                    a(aVar);
                } else {
                    a(i2, 15, 0);
                    a(byteString);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3, int i4) {
            if (i2 < i3) {
                this.f7956g.i(i4 | i2);
                return;
            }
            this.f7956g.i(i4 | i3);
            int i5 = i2 - i3;
            while (i5 >= 128) {
                this.f7956g.i((i5 & 127) | FileUtils.FileMode.MODE_IWUSR);
                i5 >>>= 7;
            }
            this.f7956g.i(i5);
        }

        /* access modifiers changed from: package-private */
        public void a(ByteString byteString) {
            if (!this.f7957h || i.a().a(byteString) >= byteString.g()) {
                a(byteString.g(), 127, 0);
                this.f7956g.b(byteString);
                return;
            }
            c cVar = new c();
            i.a().a(byteString, cVar);
            ByteString n = cVar.n();
            a(n.g(), 127, FileUtils.FileMode.MODE_IWUSR);
            this.f7956g.b(n);
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            this.f7950a = i2;
            int min = Math.min(i2, 16384);
            if (this.f7951b != min) {
                if (min < this.f7951b) {
                    this.i = Math.min(this.i, min);
                }
                this.j = true;
                this.f7951b = min;
                b();
            }
        }

        private void b() {
            if (this.f7951b >= this.f7955f) {
                return;
            }
            if (this.f7951b == 0) {
                a();
            } else {
                b(this.f7955f - this.f7951b);
            }
        }
    }

    static ByteString a(ByteString byteString) {
        int i = 0;
        int g2 = byteString.g();
        while (i < g2) {
            byte a2 = byteString.a(i);
            if (a2 < 65 || a2 > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + byteString.a());
            }
        }
        return byteString;
    }
}
