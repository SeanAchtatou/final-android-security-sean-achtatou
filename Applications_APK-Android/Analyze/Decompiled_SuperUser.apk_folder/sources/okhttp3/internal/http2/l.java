package okhttp3.internal.http2;

import java.util.Arrays;

/* compiled from: Settings */
public final class l {

    /* renamed from: a  reason: collision with root package name */
    private int f8070a;

    /* renamed from: b  reason: collision with root package name */
    private final int[] f8071b = new int[10];

    /* access modifiers changed from: package-private */
    public void a() {
        this.f8070a = 0;
        Arrays.fill(this.f8071b, 0);
    }

    /* access modifiers changed from: package-private */
    public l a(int i, int i2) {
        if (i < this.f8071b.length) {
            this.f8070a = (1 << i) | this.f8070a;
            this.f8071b[i] = i2;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i) {
        if (((1 << i) & this.f8070a) != 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int b(int i) {
        return this.f8071b[i];
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return Integer.bitCount(this.f8070a);
    }

    /* access modifiers changed from: package-private */
    public int c() {
        if ((2 & this.f8070a) != 0) {
            return this.f8071b[1];
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public int c(int i) {
        return (16 & this.f8070a) != 0 ? this.f8071b[4] : i;
    }

    /* access modifiers changed from: package-private */
    public int d(int i) {
        return (32 & this.f8070a) != 0 ? this.f8071b[5] : i;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        if ((128 & this.f8070a) != 0) {
            return this.f8071b[7];
        }
        return 65535;
    }

    /* access modifiers changed from: package-private */
    public void a(l lVar) {
        for (int i = 0; i < 10; i++) {
            if (lVar.a(i)) {
                a(i, lVar.b(i));
            }
        }
    }
}
