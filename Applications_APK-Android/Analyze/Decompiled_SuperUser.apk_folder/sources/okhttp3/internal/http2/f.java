package okhttp3.internal.http2;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.internal.c;
import okhttp3.internal.http2.b;
import okio.ByteString;
import okio.e;
import okio.q;
import okio.r;

/* compiled from: Http2Reader */
final class f implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    static final Logger f8020a = Logger.getLogger(c.class.getName());

    /* renamed from: b  reason: collision with root package name */
    final b.a f8021b = new b.a(CodedOutputStream.DEFAULT_BUFFER_SIZE, this.f8023d);

    /* renamed from: c  reason: collision with root package name */
    private final e f8022c;

    /* renamed from: d  reason: collision with root package name */
    private final a f8023d = new a(this.f8022c);

    /* renamed from: e  reason: collision with root package name */
    private final boolean f8024e;

    /* compiled from: Http2Reader */
    interface b {
        void a();

        void a(int i, int i2, int i3, boolean z);

        void a(int i, int i2, List<a> list);

        void a(int i, long j);

        void a(int i, ErrorCode errorCode);

        void a(int i, ErrorCode errorCode, ByteString byteString);

        void a(boolean z, int i, int i2);

        void a(boolean z, int i, int i2, List<a> list);

        void a(boolean z, int i, e eVar, int i2);

        void a(boolean z, l lVar);
    }

    public f(e eVar, boolean z) {
        this.f8022c = eVar;
        this.f8024e = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.http2.f.a(boolean, okhttp3.internal.http2.f$b):boolean
     arg types: [int, okhttp3.internal.http2.f$b]
     candidates:
      okhttp3.internal.http2.f.a(okhttp3.internal.http2.f$b, int):void
      okhttp3.internal.http2.f.a(boolean, okhttp3.internal.http2.f$b):boolean */
    public void a(b bVar) {
        if (!this.f8024e) {
            ByteString c2 = this.f8022c.c((long) c.f7958a.g());
            if (f8020a.isLoggable(Level.FINE)) {
                f8020a.fine(c.a("<< CONNECTION %s", c2.e()));
            }
            if (!c.f7958a.equals(c2)) {
                throw c.b("Expected a connection header but was %s", c2.a());
            }
        } else if (!a(true, bVar)) {
            throw c.b("Required SETTINGS preface not received", new Object[0]);
        }
    }

    public boolean a(boolean z, b bVar) {
        try {
            this.f8022c.a(9);
            int a2 = a(this.f8022c);
            if (a2 < 0 || a2 > 16384) {
                throw c.b("FRAME_SIZE_ERROR: %s", Integer.valueOf(a2));
            }
            byte h2 = (byte) (this.f8022c.h() & 255);
            if (!z || h2 == 4) {
                byte h3 = (byte) (this.f8022c.h() & 255);
                int j = this.f8022c.j() & Integer.MAX_VALUE;
                if (f8020a.isLoggable(Level.FINE)) {
                    f8020a.fine(c.a(true, j, a2, h2, h3));
                }
                switch (h2) {
                    case 0:
                        b(bVar, a2, h3, j);
                        return true;
                    case 1:
                        a(bVar, a2, h3, j);
                        return true;
                    case 2:
                        c(bVar, a2, h3, j);
                        return true;
                    case 3:
                        d(bVar, a2, h3, j);
                        return true;
                    case 4:
                        e(bVar, a2, h3, j);
                        return true;
                    case 5:
                        f(bVar, a2, h3, j);
                        return true;
                    case 6:
                        g(bVar, a2, h3, j);
                        return true;
                    case 7:
                        h(bVar, a2, h3, j);
                        return true;
                    case 8:
                        i(bVar, a2, h3, j);
                        return true;
                    default:
                        this.f8022c.g((long) a2);
                        return true;
                }
            } else {
                throw c.b("Expected a SETTINGS frame but was %s", Byte.valueOf(h2));
            }
        } catch (IOException e2) {
            return false;
        }
    }

    private void a(b bVar, int i, byte b2, int i2) {
        short s = 0;
        if (i2 == 0) {
            throw c.b("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        }
        boolean z = (b2 & 1) != 0;
        if ((b2 & 8) != 0) {
            s = (short) (this.f8022c.h() & 255);
        }
        if ((b2 & 32) != 0) {
            a(bVar, i2);
            i -= 5;
        }
        bVar.a(z, i2, -1, a(a(i, b2, s), s, b2, i2));
    }

    private List<a> a(int i, short s, byte b2, int i2) {
        a aVar = this.f8023d;
        this.f8023d.f8028d = i;
        aVar.f8025a = i;
        this.f8023d.f8029e = s;
        this.f8023d.f8026b = b2;
        this.f8023d.f8027c = i2;
        this.f8021b.a();
        return this.f8021b.b();
    }

    private void b(b bVar, int i, byte b2, int i2) {
        boolean z;
        boolean z2 = true;
        short s = 0;
        if ((b2 & 1) != 0) {
            z = true;
        } else {
            z = false;
        }
        if ((b2 & 32) == 0) {
            z2 = false;
        }
        if (z2) {
            throw c.b("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
        }
        if ((b2 & 8) != 0) {
            s = (short) (this.f8022c.h() & 255);
        }
        bVar.a(z, i2, this.f8022c, a(i, b2, s));
        this.f8022c.g((long) s);
    }

    private void c(b bVar, int i, byte b2, int i2) {
        if (i != 5) {
            throw c.b("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
        } else if (i2 == 0) {
            throw c.b("TYPE_PRIORITY streamId == 0", new Object[0]);
        } else {
            a(bVar, i2);
        }
    }

    private void a(b bVar, int i) {
        int j = this.f8022c.j();
        bVar.a(i, j & Integer.MAX_VALUE, (this.f8022c.h() & 255) + 1, (Integer.MIN_VALUE & j) != 0);
    }

    private void d(b bVar, int i, byte b2, int i2) {
        if (i != 4) {
            throw c.b("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
        } else if (i2 == 0) {
            throw c.b("TYPE_RST_STREAM streamId == 0", new Object[0]);
        } else {
            int j = this.f8022c.j();
            ErrorCode a2 = ErrorCode.a(j);
            if (a2 == null) {
                throw c.b("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(j));
            } else {
                bVar.a(i2, a2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.http2.f.b.a(boolean, okhttp3.internal.http2.l):void
     arg types: [int, okhttp3.internal.http2.l]
     candidates:
      okhttp3.internal.http2.f.b.a(int, long):void
      okhttp3.internal.http2.f.b.a(int, okhttp3.internal.http2.ErrorCode):void
      okhttp3.internal.http2.f.b.a(boolean, okhttp3.internal.http2.l):void */
    private void e(b bVar, int i, byte b2, int i2) {
        if (i2 != 0) {
            throw c.b("TYPE_SETTINGS streamId != 0", new Object[0]);
        } else if ((b2 & 1) != 0) {
            if (i != 0) {
                throw c.b("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
            }
            bVar.a();
        } else if (i % 6 != 0) {
            throw c.b("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
        } else {
            l lVar = new l();
            for (int i3 = 0; i3 < i; i3 += 6) {
                short i4 = this.f8022c.i();
                int j = this.f8022c.j();
                switch (i4) {
                    case 2:
                        if (!(j == 0 || j == 1)) {
                            throw c.b("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                        }
                    case 3:
                        i4 = 4;
                        break;
                    case 4:
                        i4 = 7;
                        if (j >= 0) {
                            break;
                        } else {
                            throw c.b("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                        }
                    case 5:
                        if (j >= 16384 && j <= 16777215) {
                            break;
                        } else {
                            throw c.b("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(j));
                        }
                }
                lVar.a(i4, j);
            }
            bVar.a(false, lVar);
        }
    }

    private void f(b bVar, int i, byte b2, int i2) {
        short s = 0;
        if (i2 == 0) {
            throw c.b("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        }
        if ((b2 & 8) != 0) {
            s = (short) (this.f8022c.h() & 255);
        }
        bVar.a(i2, this.f8022c.j() & Integer.MAX_VALUE, a(a(i - 4, b2, s), s, b2, i2));
    }

    private void g(b bVar, int i, byte b2, int i2) {
        boolean z = true;
        if (i != 8) {
            throw c.b("TYPE_PING length != 8: %s", Integer.valueOf(i));
        } else if (i2 != 0) {
            throw c.b("TYPE_PING streamId != 0", new Object[0]);
        } else {
            int j = this.f8022c.j();
            int j2 = this.f8022c.j();
            if ((b2 & 1) == 0) {
                z = false;
            }
            bVar.a(z, j, j2);
        }
    }

    private void h(b bVar, int i, byte b2, int i2) {
        if (i < 8) {
            throw c.b("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
        } else if (i2 != 0) {
            throw c.b("TYPE_GOAWAY streamId != 0", new Object[0]);
        } else {
            int j = this.f8022c.j();
            int j2 = this.f8022c.j();
            int i3 = i - 8;
            ErrorCode a2 = ErrorCode.a(j2);
            if (a2 == null) {
                throw c.b("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(j2));
            }
            ByteString byteString = ByteString.f8183b;
            if (i3 > 0) {
                byteString = this.f8022c.c((long) i3);
            }
            bVar.a(j, a2, byteString);
        }
    }

    private void i(b bVar, int i, byte b2, int i2) {
        if (i != 4) {
            throw c.b("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
        }
        long j = ((long) this.f8022c.j()) & 2147483647L;
        if (j == 0) {
            throw c.b("windowSizeIncrement was 0", Long.valueOf(j));
        } else {
            bVar.a(i2, j);
        }
    }

    public void close() {
        this.f8022c.close();
    }

    /* compiled from: Http2Reader */
    static final class a implements q {

        /* renamed from: a  reason: collision with root package name */
        int f8025a;

        /* renamed from: b  reason: collision with root package name */
        byte f8026b;

        /* renamed from: c  reason: collision with root package name */
        int f8027c;

        /* renamed from: d  reason: collision with root package name */
        int f8028d;

        /* renamed from: e  reason: collision with root package name */
        short f8029e;

        /* renamed from: f  reason: collision with root package name */
        private final e f8030f;

        public a(e eVar) {
            this.f8030f = eVar;
        }

        public long a(okio.c cVar, long j) {
            while (this.f8028d == 0) {
                this.f8030f.g((long) this.f8029e);
                this.f8029e = 0;
                if ((this.f8026b & 4) != 0) {
                    return -1;
                }
                b();
            }
            long a2 = this.f8030f.a(cVar, Math.min(j, (long) this.f8028d));
            if (a2 == -1) {
                return -1;
            }
            this.f8028d = (int) (((long) this.f8028d) - a2);
            return a2;
        }

        public r a() {
            return this.f8030f.a();
        }

        public void close() {
        }

        private void b() {
            int i = this.f8027c;
            int a2 = f.a(this.f8030f);
            this.f8028d = a2;
            this.f8025a = a2;
            byte h2 = (byte) (this.f8030f.h() & 255);
            this.f8026b = (byte) (this.f8030f.h() & 255);
            if (f.f8020a.isLoggable(Level.FINE)) {
                f.f8020a.fine(c.a(true, this.f8027c, this.f8025a, h2, this.f8026b));
            }
            this.f8027c = this.f8030f.j() & Integer.MAX_VALUE;
            if (h2 != 9) {
                throw c.b("%s != TYPE_CONTINUATION", Byte.valueOf(h2));
            } else if (this.f8027c != i) {
                throw c.b("TYPE_CONTINUATION streamId changed", new Object[0]);
            }
        }
    }

    static int a(e eVar) {
        return ((eVar.h() & 255) << 16) | ((eVar.h() & 255) << 8) | (eVar.h() & 255);
    }

    static int a(int i, byte b2, short s) {
        if ((b2 & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        throw c.b("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
    }
}
