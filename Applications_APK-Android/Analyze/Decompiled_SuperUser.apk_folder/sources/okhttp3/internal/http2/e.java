package okhttp3.internal.http2;

import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.http2.f;
import okio.ByteString;
import okio.d;

/* compiled from: Http2Connection */
public final class e implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    static final ExecutorService f7971a = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), okhttp3.internal.c.a("OkHttp Http2Connection", true));
    static final /* synthetic */ boolean s;

    /* renamed from: b  reason: collision with root package name */
    final boolean f7972b;

    /* renamed from: c  reason: collision with root package name */
    final b f7973c;

    /* renamed from: d  reason: collision with root package name */
    final Map<Integer, g> f7974d = new LinkedHashMap();

    /* renamed from: e  reason: collision with root package name */
    final String f7975e;

    /* renamed from: f  reason: collision with root package name */
    int f7976f;

    /* renamed from: g  reason: collision with root package name */
    int f7977g;

    /* renamed from: h  reason: collision with root package name */
    boolean f7978h;
    final k i;
    long j = 0;
    long k;
    l l = new l();
    final l m = new l();
    boolean n = false;
    final Socket o;
    final h p;
    final c q;
    final Set<Integer> r = new LinkedHashSet();
    private final ExecutorService t;
    private Map<Integer, j> u;
    private int v;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object):int
      okhttp3.internal.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      okhttp3.internal.c.a(okhttp3.HttpUrl, boolean):java.lang.String
      okhttp3.internal.c.a(okio.e, java.nio.charset.Charset):java.nio.charset.Charset
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      okhttp3.internal.c.a(java.lang.Object, java.lang.Object):boolean
      okhttp3.internal.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      okhttp3.internal.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    static {
        boolean z;
        if (!e.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        s = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object):int
      okhttp3.internal.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      okhttp3.internal.c.a(okhttp3.HttpUrl, boolean):java.lang.String
      okhttp3.internal.c.a(okio.e, java.nio.charset.Charset):java.nio.charset.Charset
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      okhttp3.internal.c.a(java.lang.Object, java.lang.Object):boolean
      okhttp3.internal.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      okhttp3.internal.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    e(a aVar) {
        int i2 = 2;
        this.i = aVar.f8010f;
        this.f7972b = aVar.f8011g;
        this.f7973c = aVar.f8009e;
        this.f7977g = aVar.f8011g ? 1 : 2;
        if (aVar.f8011g) {
            this.f7977g += 2;
        }
        this.v = aVar.f8011g ? 1 : i2;
        if (aVar.f8011g) {
            this.l.a(7, 16777216);
        }
        this.f7975e = aVar.f8006b;
        this.t = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), okhttp3.internal.c.a(okhttp3.internal.c.a("OkHttp %s Push Observer", this.f7975e), true));
        this.m.a(7, 65535);
        this.m.a(5, 16384);
        this.k = (long) this.m.d();
        this.o = aVar.f8005a;
        this.p = new h(aVar.f8008d, this.f7972b);
        this.q = new c(new f(aVar.f8007c, this.f7972b));
    }

    /* access modifiers changed from: package-private */
    public synchronized g a(int i2) {
        return this.f7974d.get(Integer.valueOf(i2));
    }

    /* access modifiers changed from: package-private */
    public synchronized g b(int i2) {
        g remove;
        remove = this.f7974d.remove(Integer.valueOf(i2));
        notifyAll();
        return remove;
    }

    public synchronized int a() {
        return this.m.c(Integer.MAX_VALUE);
    }

    public g a(List<a> list, boolean z) {
        return b(0, list, z);
    }

    private g b(int i2, List<a> list, boolean z) {
        int i3;
        g gVar;
        boolean z2 = false;
        boolean z3 = !z;
        synchronized (this.p) {
            synchronized (this) {
                if (this.f7978h) {
                    throw new ConnectionShutdownException();
                }
                i3 = this.f7977g;
                this.f7977g += 2;
                gVar = new g(i3, this, z3, false, list);
                if (!z || this.k == 0 || gVar.f8032b == 0) {
                    z2 = true;
                }
                if (gVar.b()) {
                    this.f7974d.put(Integer.valueOf(i3), gVar);
                }
            }
            if (i2 == 0) {
                this.p.a(z3, i3, i2, list);
            } else if (this.f7972b) {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            } else {
                this.p.a(i2, i3, list);
            }
        }
        if (z2) {
            this.p.b();
        }
        return gVar;
    }

    public void a(int i2, boolean z, okio.c cVar, long j2) {
        int min;
        boolean z2;
        if (j2 == 0) {
            this.p.a(z, i2, cVar, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (this.k <= 0) {
                    try {
                        if (!this.f7974d.containsKey(Integer.valueOf(i2))) {
                            throw new IOException("stream closed");
                        }
                        wait();
                    } catch (InterruptedException e2) {
                        throw new InterruptedIOException();
                    }
                }
                min = Math.min((int) Math.min(j2, this.k), this.p.c());
                this.k -= (long) min;
            }
            j2 -= (long) min;
            h hVar = this.p;
            if (!z || j2 != 0) {
                z2 = false;
            } else {
                z2 = true;
            }
            hVar.a(z2, i2, cVar, min);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.k += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, ErrorCode errorCode) {
        final int i3 = i2;
        final ErrorCode errorCode2 = errorCode;
        f7971a.execute(new okhttp3.internal.b("OkHttp %s stream %d", new Object[]{this.f7975e, Integer.valueOf(i2)}) {
            public void c() {
                try {
                    e.this.b(i3, errorCode2);
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, ErrorCode errorCode) {
        this.p.a(i2, errorCode);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, long j2) {
        final int i3 = i2;
        final long j3 = j2;
        f7971a.execute(new okhttp3.internal.b("OkHttp Window Update %s stream %d", new Object[]{this.f7975e, Integer.valueOf(i2)}) {
            public void c() {
                try {
                    e.this.p.a(i3, j3);
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z, int i2, int i3, j jVar) {
        final boolean z2 = z;
        final int i4 = i2;
        final int i5 = i3;
        final j jVar2 = jVar;
        f7971a.execute(new okhttp3.internal.b("OkHttp %s ping %08x%08x", new Object[]{this.f7975e, Integer.valueOf(i2), Integer.valueOf(i3)}) {
            public void c() {
                try {
                    e.this.b(z2, i4, i5, jVar2);
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z, int i2, int i3, j jVar) {
        synchronized (this.p) {
            if (jVar != null) {
                jVar.a();
            }
            this.p.a(z, i2, i3);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized j c(int i2) {
        return this.u != null ? this.u.remove(Integer.valueOf(i2)) : null;
    }

    public void b() {
        this.p.b();
    }

    public void a(ErrorCode errorCode) {
        synchronized (this.p) {
            synchronized (this) {
                if (!this.f7978h) {
                    this.f7978h = true;
                    int i2 = this.f7976f;
                    this.p.a(i2, errorCode, okhttp3.internal.c.f7819a);
                }
            }
        }
    }

    public void close() {
        a(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
    }

    /* access modifiers changed from: package-private */
    public void a(ErrorCode errorCode, ErrorCode errorCode2) {
        IOException iOException;
        g[] gVarArr;
        j[] jVarArr;
        if (s || !Thread.holdsLock(this)) {
            try {
                a(errorCode);
                iOException = null;
            } catch (IOException e2) {
                iOException = e2;
            }
            synchronized (this) {
                if (!this.f7974d.isEmpty()) {
                    this.f7974d.clear();
                    gVarArr = (g[]) this.f7974d.values().toArray(new g[this.f7974d.size()]);
                } else {
                    gVarArr = null;
                }
                if (this.u != null) {
                    this.u = null;
                    jVarArr = (j[]) this.u.values().toArray(new j[this.u.size()]);
                } else {
                    jVarArr = null;
                }
            }
            if (gVarArr != null) {
                IOException iOException2 = iOException;
                for (g a2 : gVarArr) {
                    try {
                        a2.a(errorCode2);
                    } catch (IOException e3) {
                        if (iOException2 != null) {
                            iOException2 = e3;
                        }
                    }
                }
                iOException = iOException2;
            }
            if (jVarArr != null) {
                for (j c2 : jVarArr) {
                    c2.c();
                }
            }
            try {
                this.p.close();
                e = iOException;
            } catch (IOException e4) {
                e = e4;
                if (iOException != null) {
                    e = iOException;
                }
            }
            try {
                this.o.close();
            } catch (IOException e5) {
                e = e5;
            }
            if (e != null) {
                throw e;
            }
            return;
        }
        throw new AssertionError();
    }

    public void c() {
        a(true);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (z) {
            this.p.a();
            this.p.b(this.l);
            int d2 = this.l.d();
            if (d2 != 65535) {
                this.p.a(0, (long) (d2 - 65535));
            }
        }
        new Thread(this.q).start();
    }

    public synchronized boolean d() {
        return this.f7978h;
    }

    /* compiled from: Http2Connection */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        Socket f8005a;

        /* renamed from: b  reason: collision with root package name */
        String f8006b;

        /* renamed from: c  reason: collision with root package name */
        okio.e f8007c;

        /* renamed from: d  reason: collision with root package name */
        d f8008d;

        /* renamed from: e  reason: collision with root package name */
        b f8009e = b.f8012f;

        /* renamed from: f  reason: collision with root package name */
        k f8010f = k.f8069a;

        /* renamed from: g  reason: collision with root package name */
        boolean f8011g;

        public a(boolean z) {
            this.f8011g = z;
        }

        public a a(Socket socket, String str, okio.e eVar, d dVar) {
            this.f8005a = socket;
            this.f8006b = str;
            this.f8007c = eVar;
            this.f8008d = dVar;
            return this;
        }

        public a a(b bVar) {
            this.f8009e = bVar;
            return this;
        }

        public e a() {
            return new e(this);
        }
    }

    /* compiled from: Http2Connection */
    class c extends okhttp3.internal.b implements f.b {

        /* renamed from: a  reason: collision with root package name */
        final f f8013a;

        c(f fVar) {
            super("OkHttp %s", e.this.f7975e);
            this.f8013a = fVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: okhttp3.internal.http2.f.a(boolean, okhttp3.internal.http2.f$b):boolean
         arg types: [int, okhttp3.internal.http2.e$c]
         candidates:
          okhttp3.internal.http2.f.a(okhttp3.internal.http2.f$b, int):void
          okhttp3.internal.http2.f.a(boolean, okhttp3.internal.http2.f$b):boolean */
        /* access modifiers changed from: protected */
        public void c() {
            ErrorCode errorCode;
            Throwable th;
            ErrorCode errorCode2 = ErrorCode.INTERNAL_ERROR;
            ErrorCode errorCode3 = ErrorCode.INTERNAL_ERROR;
            try {
                this.f8013a.a(this);
                do {
                } while (this.f8013a.a(false, (f.b) this));
                try {
                    e.this.a(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
                } catch (IOException e2) {
                }
                okhttp3.internal.c.a(this.f8013a);
            } catch (IOException e3) {
                errorCode = ErrorCode.PROTOCOL_ERROR;
                try {
                    e.this.a(errorCode, ErrorCode.PROTOCOL_ERROR);
                } catch (IOException e4) {
                }
                okhttp3.internal.c.a(this.f8013a);
            } catch (Throwable th2) {
                th = th2;
                e.this.a(errorCode, errorCode3);
                okhttp3.internal.c.a(this.f8013a);
                throw th;
            }
        }

        public void a(boolean z, int i, okio.e eVar, int i2) {
            if (e.this.d(i)) {
                e.this.a(i, eVar, i2, z);
                return;
            }
            g a2 = e.this.a(i);
            if (a2 == null) {
                e.this.a(i, ErrorCode.PROTOCOL_ERROR);
                eVar.g((long) i2);
                return;
            }
            a2.a(eVar, i2);
            if (z) {
                a2.i();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0072, code lost:
            r0.a(r12);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0075, code lost:
            if (r9 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0077, code lost:
            r0.i();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(boolean r9, int r10, int r11, java.util.List<okhttp3.internal.http2.a> r12) {
            /*
                r8 = this;
                okhttp3.internal.http2.e r0 = okhttp3.internal.http2.e.this
                boolean r0 = r0.d(r10)
                if (r0 == 0) goto L_0x000e
                okhttp3.internal.http2.e r0 = okhttp3.internal.http2.e.this
                r0.a(r10, r12, r9)
            L_0x000d:
                return
            L_0x000e:
                okhttp3.internal.http2.e r6 = okhttp3.internal.http2.e.this
                monitor-enter(r6)
                okhttp3.internal.http2.e r0 = okhttp3.internal.http2.e.this     // Catch:{ all -> 0x0019 }
                boolean r0 = r0.f7978h     // Catch:{ all -> 0x0019 }
                if (r0 == 0) goto L_0x001c
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                goto L_0x000d
            L_0x0019:
                r0 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                throw r0
            L_0x001c:
                okhttp3.internal.http2.e r0 = okhttp3.internal.http2.e.this     // Catch:{ all -> 0x0019 }
                okhttp3.internal.http2.g r0 = r0.a(r10)     // Catch:{ all -> 0x0019 }
                if (r0 != 0) goto L_0x0071
                okhttp3.internal.http2.e r0 = okhttp3.internal.http2.e.this     // Catch:{ all -> 0x0019 }
                int r0 = r0.f7976f     // Catch:{ all -> 0x0019 }
                if (r10 > r0) goto L_0x002c
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                goto L_0x000d
            L_0x002c:
                int r0 = r10 % 2
                okhttp3.internal.http2.e r1 = okhttp3.internal.http2.e.this     // Catch:{ all -> 0x0019 }
                int r1 = r1.f7977g     // Catch:{ all -> 0x0019 }
                int r1 = r1 % 2
                if (r0 != r1) goto L_0x0038
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                goto L_0x000d
            L_0x0038:
                okhttp3.internal.http2.g r0 = new okhttp3.internal.http2.g     // Catch:{ all -> 0x0019 }
                okhttp3.internal.http2.e r2 = okhttp3.internal.http2.e.this     // Catch:{ all -> 0x0019 }
                r3 = 0
                r1 = r10
                r4 = r9
                r5 = r12
                r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0019 }
                okhttp3.internal.http2.e r1 = okhttp3.internal.http2.e.this     // Catch:{ all -> 0x0019 }
                r1.f7976f = r10     // Catch:{ all -> 0x0019 }
                okhttp3.internal.http2.e r1 = okhttp3.internal.http2.e.this     // Catch:{ all -> 0x0019 }
                java.util.Map<java.lang.Integer, okhttp3.internal.http2.g> r1 = r1.f7974d     // Catch:{ all -> 0x0019 }
                java.lang.Integer r2 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x0019 }
                r1.put(r2, r0)     // Catch:{ all -> 0x0019 }
                java.util.concurrent.ExecutorService r1 = okhttp3.internal.http2.e.f7971a     // Catch:{ all -> 0x0019 }
                okhttp3.internal.http2.e$c$1 r2 = new okhttp3.internal.http2.e$c$1     // Catch:{ all -> 0x0019 }
                java.lang.String r3 = "OkHttp %s stream %d"
                r4 = 2
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0019 }
                r5 = 0
                okhttp3.internal.http2.e r7 = okhttp3.internal.http2.e.this     // Catch:{ all -> 0x0019 }
                java.lang.String r7 = r7.f7975e     // Catch:{ all -> 0x0019 }
                r4[r5] = r7     // Catch:{ all -> 0x0019 }
                r5 = 1
                java.lang.Integer r7 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x0019 }
                r4[r5] = r7     // Catch:{ all -> 0x0019 }
                r2.<init>(r3, r4, r0)     // Catch:{ all -> 0x0019 }
                r1.execute(r2)     // Catch:{ all -> 0x0019 }
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                goto L_0x000d
            L_0x0071:
                monitor-exit(r6)     // Catch:{ all -> 0x0019 }
                r0.a(r12)
                if (r9 == 0) goto L_0x000d
                r0.i()
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.http2.e.c.a(boolean, int, int, java.util.List):void");
        }

        public void a(int i, ErrorCode errorCode) {
            if (e.this.d(i)) {
                e.this.c(i, errorCode);
                return;
            }
            g b2 = e.this.b(i);
            if (b2 != null) {
                b2.c(errorCode);
            }
        }

        public void a(boolean z, l lVar) {
            g[] gVarArr;
            long j;
            synchronized (e.this) {
                int d2 = e.this.m.d();
                if (z) {
                    e.this.m.a();
                }
                e.this.m.a(lVar);
                a(lVar);
                int d3 = e.this.m.d();
                if (d3 == -1 || d3 == d2) {
                    gVarArr = null;
                    j = 0;
                } else {
                    long j2 = (long) (d3 - d2);
                    if (!e.this.n) {
                        e.this.a(j2);
                        e.this.n = true;
                    }
                    if (!e.this.f7974d.isEmpty()) {
                        j = j2;
                        gVarArr = (g[]) e.this.f7974d.values().toArray(new g[e.this.f7974d.size()]);
                    } else {
                        j = j2;
                        gVarArr = null;
                    }
                }
                e.f7971a.execute(new okhttp3.internal.b("OkHttp %s settings", e.this.f7975e) {
                    public void c() {
                        e.this.f7973c.a(e.this);
                    }
                });
            }
            if (gVarArr != null && j != 0) {
                for (g gVar : gVarArr) {
                    synchronized (gVar) {
                        gVar.a(j);
                    }
                }
            }
        }

        private void a(final l lVar) {
            e.f7971a.execute(new okhttp3.internal.b("OkHttp %s ACK Settings", new Object[]{e.this.f7975e}) {
                public void c() {
                    try {
                        e.this.p.a(lVar);
                    } catch (IOException e2) {
                    }
                }
            });
        }

        public void a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: okhttp3.internal.http2.e.a(boolean, int, int, okhttp3.internal.http2.j):void
         arg types: [int, int, int, ?[OBJECT, ARRAY]]
         candidates:
          okhttp3.internal.http2.e.a(int, okio.e, int, boolean):void
          okhttp3.internal.http2.e.a(int, boolean, okio.c, long):void
          okhttp3.internal.http2.e.a(boolean, int, int, okhttp3.internal.http2.j):void */
        public void a(boolean z, int i, int i2) {
            if (z) {
                j c2 = e.this.c(i);
                if (c2 != null) {
                    c2.b();
                    return;
                }
                return;
            }
            e.this.a(true, i, i2, (j) null);
        }

        public void a(int i, ErrorCode errorCode, ByteString byteString) {
            g[] gVarArr;
            if (byteString.g() > 0) {
            }
            synchronized (e.this) {
                gVarArr = (g[]) e.this.f7974d.values().toArray(new g[e.this.f7974d.size()]);
                e.this.f7978h = true;
            }
            for (g gVar : gVarArr) {
                if (gVar.a() > i && gVar.c()) {
                    gVar.c(ErrorCode.REFUSED_STREAM);
                    e.this.b(gVar.a());
                }
            }
        }

        public void a(int i, long j) {
            if (i == 0) {
                synchronized (e.this) {
                    e.this.k += j;
                    e.this.notifyAll();
                }
                return;
            }
            g a2 = e.this.a(i);
            if (a2 != null) {
                synchronized (a2) {
                    a2.a(j);
                }
            }
        }

        public void a(int i, int i2, int i3, boolean z) {
        }

        public void a(int i, int i2, List<a> list) {
            e.this.a(i2, list);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean d(int i2) {
        return i2 != 0 && (i2 & 1) == 0;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, List<a> list) {
        synchronized (this) {
            if (this.r.contains(Integer.valueOf(i2))) {
                a(i2, ErrorCode.PROTOCOL_ERROR);
                return;
            }
            this.r.add(Integer.valueOf(i2));
            final int i3 = i2;
            final List<a> list2 = list;
            this.t.execute(new okhttp3.internal.b("OkHttp %s Push Request[%s]", new Object[]{this.f7975e, Integer.valueOf(i2)}) {
                public void c() {
                    if (e.this.i.a(i3, list2)) {
                        try {
                            e.this.p.a(i3, ErrorCode.CANCEL);
                            synchronized (e.this) {
                                e.this.r.remove(Integer.valueOf(i3));
                            }
                        } catch (IOException e2) {
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, List<a> list, boolean z) {
        final int i3 = i2;
        final List<a> list2 = list;
        final boolean z2 = z;
        this.t.execute(new okhttp3.internal.b("OkHttp %s Push Headers[%s]", new Object[]{this.f7975e, Integer.valueOf(i2)}) {
            public void c() {
                boolean a2 = e.this.i.a(i3, list2, z2);
                if (a2) {
                    try {
                        e.this.p.a(i3, ErrorCode.CANCEL);
                    } catch (IOException e2) {
                        return;
                    }
                }
                if (a2 || z2) {
                    synchronized (e.this) {
                        e.this.r.remove(Integer.valueOf(i3));
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, okio.e eVar, int i3, boolean z) {
        final okio.c cVar = new okio.c();
        eVar.a((long) i3);
        eVar.a(cVar, (long) i3);
        if (cVar.b() != ((long) i3)) {
            throw new IOException(cVar.b() + " != " + i3);
        }
        final int i4 = i2;
        final int i5 = i3;
        final boolean z2 = z;
        this.t.execute(new okhttp3.internal.b("OkHttp %s Push Data[%s]", new Object[]{this.f7975e, Integer.valueOf(i2)}) {
            public void c() {
                try {
                    boolean a2 = e.this.i.a(i4, cVar, i5, z2);
                    if (a2) {
                        e.this.p.a(i4, ErrorCode.CANCEL);
                    }
                    if (a2 || z2) {
                        synchronized (e.this) {
                            e.this.r.remove(Integer.valueOf(i4));
                        }
                    }
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void c(int i2, ErrorCode errorCode) {
        final int i3 = i2;
        final ErrorCode errorCode2 = errorCode;
        this.t.execute(new okhttp3.internal.b("OkHttp %s Push Reset[%s]", new Object[]{this.f7975e, Integer.valueOf(i2)}) {
            public void c() {
                e.this.i.a(i3, errorCode2);
                synchronized (e.this) {
                    e.this.r.remove(Integer.valueOf(i3));
                }
            }
        });
    }

    /* compiled from: Http2Connection */
    public static abstract class b {

        /* renamed from: f  reason: collision with root package name */
        public static final b f8012f = new b() {
            public void a(g gVar) {
                gVar.a(ErrorCode.REFUSED_STREAM);
            }
        };

        public abstract void a(g gVar);

        public void a(e eVar) {
        }
    }
}
