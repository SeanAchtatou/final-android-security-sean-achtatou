package okhttp3.internal.http2;

import com.lody.virtual.helper.utils.FileUtils;
import java.io.IOException;
import okio.ByteString;

/* compiled from: Http2 */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    static final ByteString f7958a = ByteString.a("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");

    /* renamed from: b  reason: collision with root package name */
    static final String[] f7959b = new String[64];

    /* renamed from: c  reason: collision with root package name */
    static final String[] f7960c = new String[FileUtils.FileMode.MODE_IRUSR];

    /* renamed from: d  reason: collision with root package name */
    private static final String[] f7961d = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    static {
        for (int i = 0; i < f7960c.length; i++) {
            f7960c[i] = okhttp3.internal.c.a("%8s", Integer.toBinaryString(i)).replace(' ', '0');
        }
        f7959b[0] = "";
        f7959b[1] = "END_STREAM";
        int[] iArr = {1};
        f7959b[8] = "PADDED";
        for (int i2 : iArr) {
            f7959b[i2 | 8] = f7959b[i2] + "|PADDED";
        }
        f7959b[4] = "END_HEADERS";
        f7959b[32] = "PRIORITY";
        f7959b[36] = "END_HEADERS|PRIORITY";
        for (int i3 : new int[]{4, 32, 36}) {
            for (int i4 : iArr) {
                f7959b[i4 | i3] = f7959b[i4] + '|' + f7959b[i3];
                f7959b[i4 | i3 | 8] = f7959b[i4] + '|' + f7959b[i3] + "|PADDED";
            }
        }
        for (int i5 = 0; i5 < f7959b.length; i5++) {
            if (f7959b[i5] == null) {
                f7959b[i5] = f7960c[i5];
            }
        }
    }

    private c() {
    }

    static IllegalArgumentException a(String str, Object... objArr) {
        throw new IllegalArgumentException(okhttp3.internal.c.a(str, objArr));
    }

    static IOException b(String str, Object... objArr) {
        throw new IOException(okhttp3.internal.c.a(str, objArr));
    }

    static String a(boolean z, int i, int i2, byte b2, byte b3) {
        String a2 = b2 < f7961d.length ? f7961d[b2] : okhttp3.internal.c.a("0x%02x", Byte.valueOf(b2));
        String a3 = a(b2, b3);
        Object[] objArr = new Object[5];
        objArr[0] = z ? "<<" : ">>";
        objArr[1] = Integer.valueOf(i);
        objArr[2] = Integer.valueOf(i2);
        objArr[3] = a2;
        objArr[4] = a3;
        return okhttp3.internal.c.a("%s 0x%08x %5d %-13s %s", objArr);
    }

    static String a(byte b2, byte b3) {
        if (b3 == 0) {
            return "";
        }
        switch (b2) {
            case 2:
            case 3:
            case 7:
            case 8:
                return f7960c[b3];
            case 4:
            case 6:
                return b3 == 1 ? "ACK" : f7960c[b3];
            case 5:
            default:
                String str = b3 < f7959b.length ? f7959b[b3] : f7960c[b3];
                if (b2 == 5 && (b3 & 4) != 0) {
                    return str.replace("HEADERS", "PUSH_PROMISE");
                }
                if (b2 != 0 || (b3 & 32) == 0) {
                    return str;
                }
                return str.replace("PRIORITY", "COMPRESSED");
        }
    }
}
