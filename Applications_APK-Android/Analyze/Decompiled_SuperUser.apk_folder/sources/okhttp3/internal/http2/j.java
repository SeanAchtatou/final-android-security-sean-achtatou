package okhttp3.internal.http2;

import java.util.concurrent.CountDownLatch;

/* compiled from: Ping */
final class j {

    /* renamed from: a  reason: collision with root package name */
    private final CountDownLatch f8066a = new CountDownLatch(1);

    /* renamed from: b  reason: collision with root package name */
    private long f8067b = -1;

    /* renamed from: c  reason: collision with root package name */
    private long f8068c = -1;

    j() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.f8067b != -1) {
            throw new IllegalStateException();
        }
        this.f8067b = System.nanoTime();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.f8068c != -1 || this.f8067b == -1) {
            throw new IllegalStateException();
        }
        this.f8068c = System.nanoTime();
        this.f8066a.countDown();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f8068c != -1 || this.f8067b == -1) {
            throw new IllegalStateException();
        }
        this.f8068c = this.f8067b - 1;
        this.f8066a.countDown();
    }
}
