package okhttp3.internal.http2;

import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.Protocol;
import okhttp3.internal.b.c;
import okhttp3.internal.b.h;
import okhttp3.internal.b.i;
import okhttp3.internal.b.k;
import okhttp3.internal.connection.f;
import okhttp3.q;
import okhttp3.t;
import okhttp3.v;
import okhttp3.x;
import okhttp3.y;
import okio.ByteString;
import okio.g;
import okio.p;

/* compiled from: Http2Codec */
public final class d implements c {

    /* renamed from: b  reason: collision with root package name */
    private static final ByteString f7962b = ByteString.a("connection");

    /* renamed from: c  reason: collision with root package name */
    private static final ByteString f7963c = ByteString.a("host");

    /* renamed from: d  reason: collision with root package name */
    private static final ByteString f7964d = ByteString.a("keep-alive");

    /* renamed from: e  reason: collision with root package name */
    private static final ByteString f7965e = ByteString.a("proxy-connection");

    /* renamed from: f  reason: collision with root package name */
    private static final ByteString f7966f = ByteString.a("transfer-encoding");

    /* renamed from: g  reason: collision with root package name */
    private static final ByteString f7967g = ByteString.a("te");

    /* renamed from: h  reason: collision with root package name */
    private static final ByteString f7968h = ByteString.a("encoding");
    private static final ByteString i = ByteString.a("upgrade");
    private static final List<ByteString> j = okhttp3.internal.c.a(f7962b, f7963c, f7964d, f7965e, f7967g, f7966f, f7968h, i, a.f7934c, a.f7935d, a.f7936e, a.f7937f);
    private static final List<ByteString> k = okhttp3.internal.c.a(f7962b, f7963c, f7964d, f7965e, f7967g, f7966f, f7968h, i);

    /* renamed from: a  reason: collision with root package name */
    final f f7969a;
    private final t l;
    private final e m;
    private g n;

    public d(t tVar, f fVar, e eVar) {
        this.l = tVar;
        this.f7969a = fVar;
        this.m = eVar;
    }

    public p a(v vVar, long j2) {
        return this.n.h();
    }

    public void a(v vVar) {
        if (this.n == null) {
            this.n = this.m.a(b(vVar), vVar.d() != null);
            this.n.e().a((long) this.l.b(), TimeUnit.MILLISECONDS);
            this.n.f().a((long) this.l.c(), TimeUnit.MILLISECONDS);
        }
    }

    public void a() {
        this.m.b();
    }

    public void b() {
        this.n.h().close();
    }

    public x.a a(boolean z) {
        x.a a2 = a(this.n.d());
        if (!z || okhttp3.internal.a.f7759a.a(a2) != 100) {
            return a2;
        }
        return null;
    }

    public static List<a> b(v vVar) {
        q c2 = vVar.c();
        ArrayList arrayList = new ArrayList(c2.a() + 4);
        arrayList.add(new a(a.f7934c, vVar.b()));
        arrayList.add(new a(a.f7935d, i.a(vVar.a())));
        String a2 = vVar.a("Host");
        if (a2 != null) {
            arrayList.add(new a(a.f7937f, a2));
        }
        arrayList.add(new a(a.f7936e, vVar.a().b()));
        int a3 = c2.a();
        for (int i2 = 0; i2 < a3; i2++) {
            ByteString a4 = ByteString.a(c2.a(i2).toLowerCase(Locale.US));
            if (!j.contains(a4)) {
                arrayList.add(new a(a4, c2.b(i2)));
            }
        }
        return arrayList;
    }

    public static x.a a(List<a> list) {
        q.a aVar;
        k kVar;
        q.a aVar2 = new q.a();
        int size = list.size();
        int i2 = 0;
        k kVar2 = null;
        while (i2 < size) {
            a aVar3 = list.get(i2);
            if (aVar3 == null) {
                if (kVar2 != null && kVar2.f7817b == 100) {
                    aVar = new q.a();
                    kVar = null;
                }
                aVar = aVar2;
                kVar = kVar2;
            } else {
                ByteString byteString = aVar3.f7938g;
                String a2 = aVar3.f7939h.a();
                if (byteString.equals(a.f7933b)) {
                    q.a aVar4 = aVar2;
                    kVar = k.a("HTTP/1.1 " + a2);
                    aVar = aVar4;
                } else {
                    if (!k.contains(byteString)) {
                        okhttp3.internal.a.f7759a.a(aVar2, byteString.a(), a2);
                    }
                    aVar = aVar2;
                    kVar = kVar2;
                }
            }
            i2++;
            kVar2 = kVar;
            aVar2 = aVar;
        }
        if (kVar2 != null) {
            return new x.a().a(Protocol.HTTP_2).a(kVar2.f7817b).a(kVar2.f7818c).a(aVar2.a());
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    public y a(x xVar) {
        return new h(xVar.f(), okio.k.a(new a(this.n.g())));
    }

    public void c() {
        if (this.n != null) {
            this.n.b(ErrorCode.CANCEL);
        }
    }

    /* compiled from: Http2Codec */
    class a extends g {
        public a(okio.q qVar) {
            super(qVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: okhttp3.internal.connection.f.a(boolean, okhttp3.internal.b.c):void
         arg types: [int, okhttp3.internal.http2.d]
         candidates:
          okhttp3.internal.connection.f.a(okhttp3.t, boolean):okhttp3.internal.b.c
          okhttp3.internal.connection.f.a(boolean, okhttp3.internal.b.c):void */
        public void close() {
            d.this.f7969a.a(false, (c) d.this);
            super.close();
        }
    }
}
