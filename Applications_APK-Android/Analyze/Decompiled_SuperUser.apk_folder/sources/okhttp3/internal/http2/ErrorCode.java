package okhttp3.internal.http2;

public enum ErrorCode {
    NO_ERROR(0),
    PROTOCOL_ERROR(1),
    INTERNAL_ERROR(2),
    FLOW_CONTROL_ERROR(3),
    REFUSED_STREAM(7),
    CANCEL(8);
    

    /* renamed from: g  reason: collision with root package name */
    public final int f7930g;

    private ErrorCode(int i) {
        this.f7930g = i;
    }

    public static ErrorCode a(int i) {
        for (ErrorCode errorCode : values()) {
            if (errorCode.f7930g == i) {
                return errorCode;
            }
        }
        return null;
    }
}
