package okhttp3.internal.http2;

import okhttp3.internal.c;
import okio.ByteString;

/* compiled from: Header */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static final ByteString f7932a = ByteString.a(":");

    /* renamed from: b  reason: collision with root package name */
    public static final ByteString f7933b = ByteString.a(":status");

    /* renamed from: c  reason: collision with root package name */
    public static final ByteString f7934c = ByteString.a(":method");

    /* renamed from: d  reason: collision with root package name */
    public static final ByteString f7935d = ByteString.a(":path");

    /* renamed from: e  reason: collision with root package name */
    public static final ByteString f7936e = ByteString.a(":scheme");

    /* renamed from: f  reason: collision with root package name */
    public static final ByteString f7937f = ByteString.a(":authority");

    /* renamed from: g  reason: collision with root package name */
    public final ByteString f7938g;

    /* renamed from: h  reason: collision with root package name */
    public final ByteString f7939h;
    final int i;

    public a(String str, String str2) {
        this(ByteString.a(str), ByteString.a(str2));
    }

    public a(ByteString byteString, String str) {
        this(byteString, ByteString.a(str));
    }

    public a(ByteString byteString, ByteString byteString2) {
        this.f7938g = byteString;
        this.f7939h = byteString2;
        this.i = byteString.g() + 32 + byteString2.g();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        if (!this.f7938g.equals(aVar.f7938g) || !this.f7939h.equals(aVar.f7939h)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.f7938g.hashCode() + 527) * 31) + this.f7939h.hashCode();
    }

    public String toString() {
        return c.a("%s: %s", this.f7938g.a(), this.f7939h.a());
    }
}
