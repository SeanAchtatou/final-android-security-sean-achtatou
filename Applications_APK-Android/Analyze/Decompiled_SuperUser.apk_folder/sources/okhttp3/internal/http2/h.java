package okhttp3.internal.http2;

import com.lody.virtual.os.VUserInfo;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.internal.http2.b;
import okio.c;
import okio.d;

/* compiled from: Http2Writer */
final class h implements Closeable {

    /* renamed from: b  reason: collision with root package name */
    private static final Logger f8052b = Logger.getLogger(c.class.getName());

    /* renamed from: a  reason: collision with root package name */
    final b.C0114b f8053a = new b.C0114b(this.f8056e);

    /* renamed from: c  reason: collision with root package name */
    private final d f8054c;

    /* renamed from: d  reason: collision with root package name */
    private final boolean f8055d;

    /* renamed from: e  reason: collision with root package name */
    private final c f8056e = new c();

    /* renamed from: f  reason: collision with root package name */
    private int f8057f = 16384;

    /* renamed from: g  reason: collision with root package name */
    private boolean f8058g;

    public h(d dVar, boolean z) {
        this.f8054c = dVar;
        this.f8055d = z;
    }

    public synchronized void a() {
        if (this.f8058g) {
            throw new IOException("closed");
        } else if (this.f8055d) {
            if (f8052b.isLoggable(Level.FINE)) {
                f8052b.fine(okhttp3.internal.c.a(">> CONNECTION %s", c.f7958a.e()));
            }
            this.f8054c.c(c.f7958a.h());
            this.f8054c.flush();
        }
    }

    public synchronized void a(l lVar) {
        if (this.f8058g) {
            throw new IOException("closed");
        }
        this.f8057f = lVar.d(this.f8057f);
        if (lVar.c() != -1) {
            this.f8053a.a(lVar.c());
        }
        a(0, 0, (byte) 4, (byte) 1);
        this.f8054c.flush();
    }

    public synchronized void a(int i, int i2, List<a> list) {
        if (this.f8058g) {
            throw new IOException("closed");
        }
        this.f8053a.a(list);
        long b2 = this.f8056e.b();
        int min = (int) Math.min((long) (this.f8057f - 4), b2);
        a(i, min + 4, (byte) 5, b2 == ((long) min) ? (byte) 4 : 0);
        this.f8054c.g(Integer.MAX_VALUE & i2);
        this.f8054c.a_(this.f8056e, (long) min);
        if (b2 > ((long) min)) {
            b(i, b2 - ((long) min));
        }
    }

    public synchronized void b() {
        if (this.f8058g) {
            throw new IOException("closed");
        }
        this.f8054c.flush();
    }

    public synchronized void a(boolean z, int i, int i2, List<a> list) {
        if (this.f8058g) {
            throw new IOException("closed");
        }
        a(z, i, list);
    }

    public synchronized void a(int i, ErrorCode errorCode) {
        if (this.f8058g) {
            throw new IOException("closed");
        } else if (errorCode.f7930g == -1) {
            throw new IllegalArgumentException();
        } else {
            a(i, 4, (byte) 3, (byte) 0);
            this.f8054c.g(errorCode.f7930g);
            this.f8054c.flush();
        }
    }

    public int c() {
        return this.f8057f;
    }

    public synchronized void a(boolean z, int i, c cVar, int i2) {
        if (this.f8058g) {
            throw new IOException("closed");
        }
        byte b2 = 0;
        if (z) {
            b2 = (byte) 1;
        }
        a(i, b2, cVar, i2);
    }

    /* access modifiers changed from: package-private */
    public void a(int i, byte b2, c cVar, int i2) {
        a(i, i2, (byte) 0, b2);
        if (i2 > 0) {
            this.f8054c.a_(cVar, (long) i2);
        }
    }

    public synchronized void b(l lVar) {
        int i;
        synchronized (this) {
            if (this.f8058g) {
                throw new IOException("closed");
            }
            a(0, lVar.b() * 6, (byte) 4, (byte) 0);
            for (int i2 = 0; i2 < 10; i2++) {
                if (lVar.a(i2)) {
                    if (i2 == 4) {
                        i = 3;
                    } else if (i2 == 7) {
                        i = 4;
                    } else {
                        i = i2;
                    }
                    this.f8054c.h(i);
                    this.f8054c.g(lVar.b(i2));
                }
            }
            this.f8054c.flush();
        }
    }

    public synchronized void a(boolean z, int i, int i2) {
        byte b2 = 0;
        synchronized (this) {
            if (this.f8058g) {
                throw new IOException("closed");
            }
            if (z) {
                b2 = 1;
            }
            a(0, 8, (byte) 6, b2);
            this.f8054c.g(i);
            this.f8054c.g(i2);
            this.f8054c.flush();
        }
    }

    public synchronized void a(int i, ErrorCode errorCode, byte[] bArr) {
        if (this.f8058g) {
            throw new IOException("closed");
        } else if (errorCode.f7930g == -1) {
            throw c.a("errorCode.httpCode == -1", new Object[0]);
        } else {
            a(0, bArr.length + 8, (byte) 7, (byte) 0);
            this.f8054c.g(i);
            this.f8054c.g(errorCode.f7930g);
            if (bArr.length > 0) {
                this.f8054c.c(bArr);
            }
            this.f8054c.flush();
        }
    }

    public synchronized void a(int i, long j) {
        if (this.f8058g) {
            throw new IOException("closed");
        } else if (j == 0 || j > 2147483647L) {
            throw c.a("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
        } else {
            a(i, 4, (byte) 8, (byte) 0);
            this.f8054c.g((int) j);
            this.f8054c.flush();
        }
    }

    public void a(int i, int i2, byte b2, byte b3) {
        if (f8052b.isLoggable(Level.FINE)) {
            f8052b.fine(c.a(false, i, i2, b2, b3));
        }
        if (i2 > this.f8057f) {
            throw c.a("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(this.f8057f), Integer.valueOf(i2));
        } else if ((Integer.MIN_VALUE & i) != 0) {
            throw c.a("reserved bit set: %s", Integer.valueOf(i));
        } else {
            a(this.f8054c, i2);
            this.f8054c.i(b2 & 255);
            this.f8054c.i(b3 & 255);
            this.f8054c.g(Integer.MAX_VALUE & i);
        }
    }

    public synchronized void close() {
        this.f8058g = true;
        this.f8054c.close();
    }

    private static void a(d dVar, int i) {
        dVar.i((i >>> 16) & VUserInfo.FLAG_MASK_USER_TYPE);
        dVar.i((i >>> 8) & VUserInfo.FLAG_MASK_USER_TYPE);
        dVar.i(i & VUserInfo.FLAG_MASK_USER_TYPE);
    }

    private void b(int i, long j) {
        while (j > 0) {
            int min = (int) Math.min((long) this.f8057f, j);
            j -= (long) min;
            a(i, min, (byte) 9, j == 0 ? (byte) 4 : 0);
            this.f8054c.a_(this.f8056e, (long) min);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z, int i, List<a> list) {
        if (this.f8058g) {
            throw new IOException("closed");
        }
        this.f8053a.a(list);
        long b2 = this.f8056e.b();
        int min = (int) Math.min((long) this.f8057f, b2);
        byte b3 = b2 == ((long) min) ? (byte) 4 : 0;
        if (z) {
            b3 = (byte) (b3 | 1);
        }
        a(i, min, (byte) 1, b3);
        this.f8054c.a_(this.f8056e, (long) min);
        if (b2 > ((long) min)) {
            b(i, b2 - ((long) min));
        }
    }
}
