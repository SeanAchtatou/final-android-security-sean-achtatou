package okhttp3.internal;

import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.lang.reflect.Array;
import java.net.IDN;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import okhttp3.HttpUrl;
import okhttp3.s;
import okhttp3.w;
import okhttp3.y;
import okio.ByteString;
import okio.e;
import okio.q;

/* compiled from: Util */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public static final byte[] f7819a = new byte[0];

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f7820b = new String[0];

    /* renamed from: c  reason: collision with root package name */
    public static final y f7821c = y.a(null, f7819a);

    /* renamed from: d  reason: collision with root package name */
    public static final w f7822d = w.a((s) null, f7819a);

    /* renamed from: e  reason: collision with root package name */
    public static final Charset f7823e = Charset.forName("UTF-8");

    /* renamed from: f  reason: collision with root package name */
    public static final TimeZone f7824f = TimeZone.getTimeZone("GMT");

    /* renamed from: g  reason: collision with root package name */
    private static final ByteString f7825g = ByteString.b("efbbbf");

    /* renamed from: h  reason: collision with root package name */
    private static final ByteString f7826h = ByteString.b("feff");
    private static final ByteString i = ByteString.b("fffe");
    private static final ByteString j = ByteString.b("0000ffff");
    private static final ByteString k = ByteString.b("ffff0000");
    private static final Charset l = Charset.forName("UTF-16BE");
    private static final Charset m = Charset.forName("UTF-16LE");
    private static final Charset n = Charset.forName("UTF-32BE");
    private static final Charset o = Charset.forName("UTF-32LE");
    private static final Pattern p = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");

    public static void a(long j2, long j3, long j4) {
        if ((j3 | j4) < 0 || j3 > j2 || j2 - j3 < j4) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    public static void a(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (AssertionError e2) {
                if (!a(e2)) {
                    throw e2;
                }
            } catch (RuntimeException e3) {
                throw e3;
            } catch (Exception e4) {
            }
        }
    }

    public static boolean a(q qVar, int i2, TimeUnit timeUnit) {
        try {
            return b(qVar, i2, timeUnit);
        } catch (IOException e2) {
            return false;
        }
    }

    public static boolean b(q qVar, int i2, TimeUnit timeUnit) {
        long nanoTime = System.nanoTime();
        long d2 = qVar.a().d_() ? qVar.a().d() - nanoTime : Long.MAX_VALUE;
        qVar.a().a(Math.min(d2, timeUnit.toNanos((long) i2)) + nanoTime);
        try {
            okio.c cVar = new okio.c();
            while (qVar.a(cVar, 8192) != -1) {
                cVar.r();
            }
            if (d2 == Long.MAX_VALUE) {
                qVar.a().f_();
            } else {
                qVar.a().a(d2 + nanoTime);
            }
            return true;
        } catch (InterruptedIOException e2) {
            if (d2 == Long.MAX_VALUE) {
                qVar.a().f_();
            } else {
                qVar.a().a(d2 + nanoTime);
            }
            return false;
        } catch (Throwable th) {
            if (d2 == Long.MAX_VALUE) {
                qVar.a().f_();
            } else {
                qVar.a().a(d2 + nanoTime);
            }
            throw th;
        }
    }

    public static <T> List<T> a(List list) {
        return Collections.unmodifiableList(new ArrayList(list));
    }

    public static <T> List<T> a(Object... objArr) {
        return Collections.unmodifiableList(Arrays.asList((Object[]) objArr.clone()));
    }

    public static ThreadFactory a(final String str, final boolean z) {
        return new ThreadFactory() {
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, str);
                thread.setDaemon(z);
                return thread;
            }
        };
    }

    public static <T> T[] a(Class cls, Object[] objArr, Object[] objArr2) {
        List a2 = a(objArr, objArr2);
        return a2.toArray((Object[]) Array.newInstance(cls, a2.size()));
    }

    private static <T> List<T> a(Object[] objArr, Object[] objArr2) {
        ArrayList arrayList = new ArrayList();
        for (Object obj : objArr) {
            int length = objArr2.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                }
                Object obj2 = objArr2[i2];
                if (obj.equals(obj2)) {
                    arrayList.add(obj2);
                    break;
                }
                i2++;
            }
        }
        return arrayList;
    }

    public static String a(HttpUrl httpUrl, boolean z) {
        String f2;
        if (httpUrl.f().contains(":")) {
            f2 = "[" + httpUrl.f() + "]";
        } else {
            f2 = httpUrl.f();
        }
        if (z || httpUrl.g() != HttpUrl.a(httpUrl.b())) {
            return f2 + ":" + httpUrl.g();
        }
        return f2;
    }

    public static boolean a(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }

    public static <T> int a(Object[] objArr, Object obj) {
        int length = objArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (a(objArr[i2], obj)) {
                return i2;
            }
        }
        return -1;
    }

    public static String[] a(String[] strArr, String str) {
        String[] strArr2 = new String[(strArr.length + 1)];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
        strArr2[strArr2.length - 1] = str;
        return strArr2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0003  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.lang.String r2, int r3, int r4) {
        /*
            r0 = r3
        L_0x0001:
            if (r0 >= r4) goto L_0x000b
            char r1 = r2.charAt(r0)
            switch(r1) {
                case 9: goto L_0x000c;
                case 10: goto L_0x000c;
                case 12: goto L_0x000c;
                case 13: goto L_0x000c;
                case 32: goto L_0x000c;
                default: goto L_0x000a;
            }
        L_0x000a:
            r4 = r0
        L_0x000b:
            return r4
        L_0x000c:
            int r0 = r0 + 1
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.c.a(java.lang.String, int, int):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0004  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(java.lang.String r2, int r3, int r4) {
        /*
            int r0 = r4 + -1
        L_0x0002:
            if (r0 < r3) goto L_0x000d
            char r1 = r2.charAt(r0)
            switch(r1) {
                case 9: goto L_0x000e;
                case 10: goto L_0x000e;
                case 12: goto L_0x000e;
                case 13: goto L_0x000e;
                case 32: goto L_0x000e;
                default: goto L_0x000b;
            }
        L_0x000b:
            int r3 = r0 + 1
        L_0x000d:
            return r3
        L_0x000e:
            int r0 = r0 + -1
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.c.b(java.lang.String, int, int):int");
    }

    public static String c(String str, int i2, int i3) {
        int a2 = a(str, i2, i3);
        return str.substring(a2, b(str, a2, i3));
    }

    public static int a(String str, int i2, int i3, String str2) {
        for (int i4 = i2; i4 < i3; i4++) {
            if (str2.indexOf(str.charAt(i4)) != -1) {
                return i4;
            }
        }
        return i3;
    }

    public static int a(String str, int i2, int i3, char c2) {
        for (int i4 = i2; i4 < i3; i4++) {
            if (str.charAt(i4) == c2) {
                return i4;
            }
        }
        return i3;
    }

    public static String a(String str) {
        try {
            String lowerCase = IDN.toASCII(str).toLowerCase(Locale.US);
            if (!lowerCase.isEmpty() && !d(lowerCase)) {
                return lowerCase;
            }
            return null;
        } catch (IllegalArgumentException e2) {
            return null;
        }
    }

    private static boolean d(String str) {
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (charAt <= 31 || charAt >= 127) {
                return true;
            }
            if (" #%/:?@[\\]".indexOf(charAt) != -1) {
                return true;
            }
        }
        return false;
    }

    public static int b(String str) {
        int i2 = 0;
        int length = str.length();
        while (i2 < length) {
            char charAt = str.charAt(i2);
            if (charAt <= 31 || charAt >= 127) {
                return i2;
            }
            i2++;
        }
        return -1;
    }

    public static boolean c(String str) {
        return p.matcher(str).matches();
    }

    public static String a(String str, Object... objArr) {
        return String.format(Locale.US, str, objArr);
    }

    public static Charset a(e eVar, Charset charset) {
        if (eVar.a(0, f7825g)) {
            eVar.g((long) f7825g.g());
            return f7823e;
        } else if (eVar.a(0, f7826h)) {
            eVar.g((long) f7826h.g());
            return l;
        } else if (eVar.a(0, i)) {
            eVar.g((long) i.g());
            return m;
        } else if (eVar.a(0, j)) {
            eVar.g((long) j.g());
            return n;
        } else if (!eVar.a(0, k)) {
            return charset;
        } else {
            eVar.g((long) k.g());
            return o;
        }
    }
}
