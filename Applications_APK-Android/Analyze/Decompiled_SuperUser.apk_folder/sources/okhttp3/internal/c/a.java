package okhttp3.internal.c;

import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okhttp3.HttpUrl;
import okhttp3.internal.b.h;
import okhttp3.internal.b.i;
import okhttp3.q;
import okhttp3.t;
import okhttp3.v;
import okhttp3.x;
import okhttp3.y;
import okio.k;
import okio.p;
import okio.q;
import okio.r;

/* compiled from: Http1Codec */
public final class a implements okhttp3.internal.b.c {

    /* renamed from: a  reason: collision with root package name */
    final t f7829a;

    /* renamed from: b  reason: collision with root package name */
    final okhttp3.internal.connection.f f7830b;

    /* renamed from: c  reason: collision with root package name */
    final okio.e f7831c;

    /* renamed from: d  reason: collision with root package name */
    final okio.d f7832d;

    /* renamed from: e  reason: collision with root package name */
    int f7833e = 0;

    public a(t tVar, okhttp3.internal.connection.f fVar, okio.e eVar, okio.d dVar) {
        this.f7829a = tVar;
        this.f7830b = fVar;
        this.f7831c = eVar;
        this.f7832d = dVar;
    }

    public p a(v vVar, long j) {
        if ("chunked".equalsIgnoreCase(vVar.a("Transfer-Encoding"))) {
            return e();
        }
        if (j != -1) {
            return a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    public void c() {
        okhttp3.internal.connection.c b2 = this.f7830b.b();
        if (b2 != null) {
            b2.b();
        }
    }

    public void a(v vVar) {
        a(vVar.c(), i.a(vVar, this.f7830b.b().a().b().type()));
    }

    public y a(x xVar) {
        return new h(xVar.f(), k.a(b(xVar)));
    }

    private q b(x xVar) {
        if (!okhttp3.internal.b.e.b(xVar)) {
            return b(0);
        }
        if ("chunked".equalsIgnoreCase(xVar.a("Transfer-Encoding"))) {
            return a(xVar.a().a());
        }
        long a2 = okhttp3.internal.b.e.a(xVar);
        if (a2 != -1) {
            return b(a2);
        }
        return f();
    }

    public void a() {
        this.f7832d.flush();
    }

    public void b() {
        this.f7832d.flush();
    }

    public void a(okhttp3.q qVar, String str) {
        if (this.f7833e != 0) {
            throw new IllegalStateException("state: " + this.f7833e);
        }
        this.f7832d.b(str).b("\r\n");
        int a2 = qVar.a();
        for (int i = 0; i < a2; i++) {
            this.f7832d.b(qVar.a(i)).b(": ").b(qVar.b(i)).b("\r\n");
        }
        this.f7832d.b("\r\n");
        this.f7833e = 1;
    }

    public x.a a(boolean z) {
        if (this.f7833e == 1 || this.f7833e == 3) {
            try {
                okhttp3.internal.b.k a2 = okhttp3.internal.b.k.a(this.f7831c.p());
                x.a a3 = new x.a().a(a2.f7816a).a(a2.f7817b).a(a2.f7818c).a(d());
                if (z && a2.f7817b == 100) {
                    return null;
                }
                this.f7833e = 4;
                return a3;
            } catch (EOFException e2) {
                IOException iOException = new IOException("unexpected end of stream on " + this.f7830b);
                iOException.initCause(e2);
                throw iOException;
            }
        } else {
            throw new IllegalStateException("state: " + this.f7833e);
        }
    }

    public okhttp3.q d() {
        q.a aVar = new q.a();
        while (true) {
            String p = this.f7831c.p();
            if (p.length() == 0) {
                return aVar.a();
            }
            okhttp3.internal.a.f7759a.a(aVar, p);
        }
    }

    public p e() {
        if (this.f7833e != 1) {
            throw new IllegalStateException("state: " + this.f7833e);
        }
        this.f7833e = 2;
        return new b();
    }

    public p a(long j) {
        if (this.f7833e != 1) {
            throw new IllegalStateException("state: " + this.f7833e);
        }
        this.f7833e = 2;
        return new d(j);
    }

    public okio.q b(long j) {
        if (this.f7833e != 4) {
            throw new IllegalStateException("state: " + this.f7833e);
        }
        this.f7833e = 5;
        return new e(j);
    }

    public okio.q a(HttpUrl httpUrl) {
        if (this.f7833e != 4) {
            throw new IllegalStateException("state: " + this.f7833e);
        }
        this.f7833e = 5;
        return new c(httpUrl);
    }

    public okio.q f() {
        if (this.f7833e != 4) {
            throw new IllegalStateException("state: " + this.f7833e);
        } else if (this.f7830b == null) {
            throw new IllegalStateException("streamAllocation == null");
        } else {
            this.f7833e = 5;
            this.f7830b.d();
            return new f();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(okio.h hVar) {
        r a2 = hVar.a();
        hVar.a(r.f8239b);
        a2.f_();
        a2.e_();
    }

    /* compiled from: Http1Codec */
    private final class d implements p {

        /* renamed from: b  reason: collision with root package name */
        private final okio.h f7845b = new okio.h(a.this.f7832d.a());

        /* renamed from: c  reason: collision with root package name */
        private boolean f7846c;

        /* renamed from: d  reason: collision with root package name */
        private long f7847d;

        d(long j) {
            this.f7847d = j;
        }

        public r a() {
            return this.f7845b;
        }

        public void a_(okio.c cVar, long j) {
            if (this.f7846c) {
                throw new IllegalStateException("closed");
            }
            okhttp3.internal.c.a(cVar.b(), 0, j);
            if (j > this.f7847d) {
                throw new ProtocolException("expected " + this.f7847d + " bytes but received " + j);
            }
            a.this.f7832d.a_(cVar, j);
            this.f7847d -= j;
        }

        public void flush() {
            if (!this.f7846c) {
                a.this.f7832d.flush();
            }
        }

        public void close() {
            if (!this.f7846c) {
                this.f7846c = true;
                if (this.f7847d > 0) {
                    throw new ProtocolException("unexpected end of stream");
                }
                a.this.a(this.f7845b);
                a.this.f7833e = 3;
            }
        }
    }

    /* compiled from: Http1Codec */
    private final class b implements p {

        /* renamed from: b  reason: collision with root package name */
        private final okio.h f7838b = new okio.h(a.this.f7832d.a());

        /* renamed from: c  reason: collision with root package name */
        private boolean f7839c;

        b() {
        }

        public r a() {
            return this.f7838b;
        }

        public void a_(okio.c cVar, long j) {
            if (this.f7839c) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                a.this.f7832d.j(j);
                a.this.f7832d.b("\r\n");
                a.this.f7832d.a_(cVar, j);
                a.this.f7832d.b("\r\n");
            }
        }

        public synchronized void flush() {
            if (!this.f7839c) {
                a.this.f7832d.flush();
            }
        }

        public synchronized void close() {
            if (!this.f7839c) {
                this.f7839c = true;
                a.this.f7832d.b("0\r\n\r\n");
                a.this.a(this.f7838b);
                a.this.f7833e = 3;
            }
        }
    }

    /* renamed from: okhttp3.internal.c.a$a  reason: collision with other inner class name */
    /* compiled from: Http1Codec */
    private abstract class C0112a implements okio.q {

        /* renamed from: a  reason: collision with root package name */
        protected final okio.h f7834a;

        /* renamed from: b  reason: collision with root package name */
        protected boolean f7835b;

        private C0112a() {
            this.f7834a = new okio.h(a.this.f7831c.a());
        }

        public r a() {
            return this.f7834a;
        }

        /* access modifiers changed from: protected */
        public final void a(boolean z) {
            if (a.this.f7833e != 6) {
                if (a.this.f7833e != 5) {
                    throw new IllegalStateException("state: " + a.this.f7833e);
                }
                a.this.a(this.f7834a);
                a.this.f7833e = 6;
                if (a.this.f7830b != null) {
                    a.this.f7830b.a(!z, a.this);
                }
            }
        }
    }

    /* compiled from: Http1Codec */
    private class e extends C0112a {

        /* renamed from: e  reason: collision with root package name */
        private long f7849e;

        public e(long j) {
            super();
            this.f7849e = j;
            if (this.f7849e == 0) {
                a(true);
            }
        }

        public long a(okio.c cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f7835b) {
                throw new IllegalStateException("closed");
            } else if (this.f7849e == 0) {
                return -1;
            } else {
                long a2 = a.this.f7831c.a(cVar, Math.min(this.f7849e, j));
                if (a2 == -1) {
                    a(false);
                    throw new ProtocolException("unexpected end of stream");
                }
                this.f7849e -= a2;
                if (this.f7849e == 0) {
                    a(true);
                }
                return a2;
            }
        }

        public void close() {
            if (!this.f7835b) {
                if (this.f7849e != 0 && !okhttp3.internal.c.a(this, 100, TimeUnit.MILLISECONDS)) {
                    a(false);
                }
                this.f7835b = true;
            }
        }
    }

    /* compiled from: Http1Codec */
    private class c extends C0112a {

        /* renamed from: e  reason: collision with root package name */
        private final HttpUrl f7841e;

        /* renamed from: f  reason: collision with root package name */
        private long f7842f = -1;

        /* renamed from: g  reason: collision with root package name */
        private boolean f7843g = true;

        c(HttpUrl httpUrl) {
            super();
            this.f7841e = httpUrl;
        }

        public long a(okio.c cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f7835b) {
                throw new IllegalStateException("closed");
            } else if (!this.f7843g) {
                return -1;
            } else {
                if (this.f7842f == 0 || this.f7842f == -1) {
                    b();
                    if (!this.f7843g) {
                        return -1;
                    }
                }
                long a2 = a.this.f7831c.a(cVar, Math.min(j, this.f7842f));
                if (a2 == -1) {
                    a(false);
                    throw new ProtocolException("unexpected end of stream");
                }
                this.f7842f -= a2;
                return a2;
            }
        }

        private void b() {
            if (this.f7842f != -1) {
                a.this.f7831c.p();
            }
            try {
                this.f7842f = a.this.f7831c.m();
                String trim = a.this.f7831c.p().trim();
                if (this.f7842f < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.f7842f + trim + "\"");
                } else if (this.f7842f == 0) {
                    this.f7843g = false;
                    okhttp3.internal.b.e.a(a.this.f7829a.f(), this.f7841e, a.this.d());
                    a(true);
                }
            } catch (NumberFormatException e2) {
                throw new ProtocolException(e2.getMessage());
            }
        }

        public void close() {
            if (!this.f7835b) {
                if (this.f7843g && !okhttp3.internal.c.a(this, 100, TimeUnit.MILLISECONDS)) {
                    a(false);
                }
                this.f7835b = true;
            }
        }
    }

    /* compiled from: Http1Codec */
    private class f extends C0112a {

        /* renamed from: e  reason: collision with root package name */
        private boolean f7851e;

        f() {
            super();
        }

        public long a(okio.c cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f7835b) {
                throw new IllegalStateException("closed");
            } else if (this.f7851e) {
                return -1;
            } else {
                long a2 = a.this.f7831c.a(cVar, j);
                if (a2 != -1) {
                    return a2;
                }
                this.f7851e = true;
                a(true);
                return -1;
            }
        }

        public void close() {
            if (!this.f7835b) {
                if (!this.f7851e) {
                    a(false);
                }
                this.f7835b = true;
            }
        }
    }
}
