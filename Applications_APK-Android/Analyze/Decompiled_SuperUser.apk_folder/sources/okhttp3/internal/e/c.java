package okhttp3.internal.e;

import com.google.android.gms.analytics.ecommerce.ProductAction;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import javax.net.ssl.SSLSocket;
import okhttp3.Protocol;

/* compiled from: JdkWithJettyBootPlatform */
class c extends e {

    /* renamed from: a  reason: collision with root package name */
    private final Method f7898a;

    /* renamed from: b  reason: collision with root package name */
    private final Method f7899b;

    /* renamed from: c  reason: collision with root package name */
    private final Method f7900c;

    /* renamed from: d  reason: collision with root package name */
    private final Class<?> f7901d;

    /* renamed from: e  reason: collision with root package name */
    private final Class<?> f7902e;

    public c(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
        this.f7898a = method;
        this.f7899b = method2;
        this.f7900c = method3;
        this.f7901d = cls;
        this.f7902e = cls2;
    }

    public void a(SSLSocket sSLSocket, String str, List<Protocol> list) {
        List<String> a2 = a(list);
        try {
            Object newProxyInstance = Proxy.newProxyInstance(e.class.getClassLoader(), new Class[]{this.f7901d, this.f7902e}, new a(a2));
            this.f7898a.invoke(null, sSLSocket, newProxyInstance);
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new AssertionError(e2);
        }
    }

    public void b(SSLSocket sSLSocket) {
        try {
            this.f7900c.invoke(null, sSLSocket);
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new AssertionError();
        }
    }

    public String a(SSLSocket sSLSocket) {
        try {
            a aVar = (a) Proxy.getInvocationHandler(this.f7899b.invoke(null, sSLSocket));
            if (aVar.f7903a || aVar.f7904b != null) {
                return aVar.f7903a ? null : aVar.f7904b;
            }
            e.b().a(4, "ALPN callback dropped: HTTP/2 is disabled. Is alpn-boot on the boot class path?", (Throwable) null);
            return null;
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new AssertionError();
        }
    }

    public static e a() {
        try {
            Class<?> cls = Class.forName("org.eclipse.jetty.alpn.ALPN");
            Class<?> cls2 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$Provider");
            Class<?> cls3 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ClientProvider");
            Class<?> cls4 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ServerProvider");
            return new c(cls.getMethod("put", SSLSocket.class, cls2), cls.getMethod("get", SSLSocket.class), cls.getMethod(ProductAction.ACTION_REMOVE, SSLSocket.class), cls3, cls4);
        } catch (ClassNotFoundException | NoSuchMethodException e2) {
            return null;
        }
    }

    /* compiled from: JdkWithJettyBootPlatform */
    private static class a implements InvocationHandler {

        /* renamed from: a  reason: collision with root package name */
        boolean f7903a;

        /* renamed from: b  reason: collision with root package name */
        String f7904b;

        /* renamed from: c  reason: collision with root package name */
        private final List<String> f7905c;

        public a(List<String> list) {
            this.f7905c = list;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            String name = method.getName();
            Class<?> returnType = method.getReturnType();
            if (objArr == null) {
                objArr = okhttp3.internal.c.f7820b;
            }
            if (name.equals("supports") && Boolean.TYPE == returnType) {
                return true;
            }
            if (name.equals("unsupported") && Void.TYPE == returnType) {
                this.f7903a = true;
                return null;
            } else if (name.equals("protocols") && objArr.length == 0) {
                return this.f7905c;
            } else {
                if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                    List list = (List) objArr[0];
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (this.f7905c.contains(list.get(i))) {
                            String str = (String) list.get(i);
                            this.f7904b = str;
                            return str;
                        }
                    }
                    String str2 = this.f7905c.get(0);
                    this.f7904b = str2;
                    return str2;
                } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                    return method.invoke(this, objArr);
                } else {
                    this.f7904b = (String) objArr[0];
                    return null;
                }
            }
        }
    }
}
