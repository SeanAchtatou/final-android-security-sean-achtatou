package okhttp3.internal.e;

import android.util.Log;
import com.kingouser.com.util.rsa.RSAUtils;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import okhttp3.Protocol;
import okhttp3.internal.c;

/* compiled from: AndroidPlatform */
class a extends e {

    /* renamed from: a  reason: collision with root package name */
    private final Class<?> f7885a;

    /* renamed from: b  reason: collision with root package name */
    private final d<Socket> f7886b;

    /* renamed from: c  reason: collision with root package name */
    private final d<Socket> f7887c;

    /* renamed from: d  reason: collision with root package name */
    private final d<Socket> f7888d;

    /* renamed from: e  reason: collision with root package name */
    private final d<Socket> f7889e;

    /* renamed from: f  reason: collision with root package name */
    private final b f7890f = b.a();

    public a(Class<?> cls, d<Socket> dVar, d<Socket> dVar2, d<Socket> dVar3, d<Socket> dVar4) {
        this.f7885a = cls;
        this.f7886b = dVar;
        this.f7887c = dVar2;
        this.f7888d = dVar3;
        this.f7889e = dVar4;
    }

    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) {
        try {
            socket.connect(inetSocketAddress, i);
        } catch (AssertionError e2) {
            if (c.a(e2)) {
                throw new IOException(e2);
            }
            throw e2;
        } catch (SecurityException e3) {
            IOException iOException = new IOException("Exception in connect");
            iOException.initCause(e3);
            throw iOException;
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<Protocol> list) {
        if (str != null) {
            this.f7886b.b(sSLSocket, true);
            this.f7887c.b(sSLSocket, str);
        }
        if (this.f7889e != null && this.f7889e.a(sSLSocket)) {
            this.f7889e.d(sSLSocket, b(list));
        }
    }

    public String a(SSLSocket sSLSocket) {
        if (this.f7888d == null || !this.f7888d.a(sSLSocket)) {
            return null;
        }
        byte[] bArr = (byte[]) this.f7888d.d(sSLSocket, new Object[0]);
        return bArr != null ? new String(bArr, c.f7823e) : null;
    }

    public void a(int i, String str, Throwable th) {
        int min;
        int i2 = i == 5 ? 5 : 3;
        if (th != null) {
            str = str + 10 + Log.getStackTraceString(th);
        }
        int i3 = 0;
        int length = str.length();
        while (i3 < length) {
            int indexOf = str.indexOf(10, i3);
            if (indexOf == -1) {
                indexOf = length;
            }
            while (true) {
                min = Math.min(indexOf, i3 + 4000);
                Log.println(i2, "OkHttp", str.substring(i3, min));
                if (min >= indexOf) {
                    break;
                }
                i3 = min;
            }
            i3 = min + 1;
        }
    }

    public Object a(String str) {
        return this.f7890f.a(str);
    }

    public void a(String str, Object obj) {
        if (!this.f7890f.a(obj)) {
            a(5, str, (Throwable) null);
        }
    }

    public boolean b(String str) {
        try {
            Class<?> cls = Class.forName("android.security.NetworkSecurityPolicy");
            Object invoke = cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", String.class).invoke(invoke, str)).booleanValue();
        } catch (ClassNotFoundException | NoSuchMethodException e2) {
            return super.b(str);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e3) {
            throw new AssertionError();
        }
    }

    public okhttp3.internal.f.b a(X509TrustManager x509TrustManager) {
        try {
            Class<?> cls = Class.forName("android.net.http.X509TrustManagerExtensions");
            return new C0113a(cls.getConstructor(X509TrustManager.class).newInstance(x509TrustManager), cls.getMethod("checkServerTrusted", X509Certificate[].class, String.class, String.class));
        } catch (Exception e2) {
            return super.a(x509TrustManager);
        }
    }

    public static e a() {
        Class<?> cls;
        d dVar;
        d dVar2;
        d dVar3;
        try {
            cls = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
        } catch (ClassNotFoundException e2) {
            cls = Class.forName("org.apache.harmony.xnet.provider.jsse.SSLParametersImpl");
        }
        try {
            d dVar4 = new d(null, "setUseSessionTickets", Boolean.TYPE);
            d dVar5 = new d(null, "setHostname", String.class);
            try {
                Class.forName("android.net.Network");
                dVar = new d(byte[].class, "getAlpnSelectedProtocol", new Class[0]);
                try {
                    dVar2 = new d(null, "setAlpnProtocols", byte[].class);
                    dVar3 = dVar;
                } catch (ClassNotFoundException e3) {
                    dVar2 = null;
                    dVar3 = dVar;
                    return new a(cls, dVar4, dVar5, dVar3, dVar2);
                }
            } catch (ClassNotFoundException e4) {
                dVar = null;
                dVar2 = null;
                dVar3 = dVar;
                return new a(cls, dVar4, dVar5, dVar3, dVar2);
            }
            return new a(cls, dVar4, dVar5, dVar3, dVar2);
        } catch (ClassNotFoundException e5) {
            return null;
        }
    }

    /* renamed from: okhttp3.internal.e.a$a  reason: collision with other inner class name */
    /* compiled from: AndroidPlatform */
    static final class C0113a extends okhttp3.internal.f.b {

        /* renamed from: a  reason: collision with root package name */
        private final Object f7891a;

        /* renamed from: b  reason: collision with root package name */
        private final Method f7892b;

        C0113a(Object obj, Method method) {
            this.f7891a = obj;
            this.f7892b = method;
        }

        public List<Certificate> a(List<Certificate> list, String str) {
            try {
                return (List) this.f7892b.invoke(this.f7891a, (X509Certificate[]) list.toArray(new X509Certificate[list.size()]), RSAUtils.KEY_ALGORITHM, str);
            } catch (InvocationTargetException e2) {
                SSLPeerUnverifiedException sSLPeerUnverifiedException = new SSLPeerUnverifiedException(e2.getMessage());
                sSLPeerUnverifiedException.initCause(e2);
                throw sSLPeerUnverifiedException;
            } catch (IllegalAccessException e3) {
                throw new AssertionError(e3);
            }
        }

        public boolean equals(Object obj) {
            return obj instanceof C0113a;
        }

        public int hashCode() {
            return 0;
        }
    }

    /* compiled from: AndroidPlatform */
    static final class b {

        /* renamed from: a  reason: collision with root package name */
        private final Method f7893a;

        /* renamed from: b  reason: collision with root package name */
        private final Method f7894b;

        /* renamed from: c  reason: collision with root package name */
        private final Method f7895c;

        b(Method method, Method method2, Method method3) {
            this.f7893a = method;
            this.f7894b = method2;
            this.f7895c = method3;
        }

        /* access modifiers changed from: package-private */
        public Object a(String str) {
            if (this.f7893a != null) {
                try {
                    Object invoke = this.f7893a.invoke(null, new Object[0]);
                    this.f7894b.invoke(invoke, str);
                    return invoke;
                } catch (Exception e2) {
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Object obj) {
            if (obj == null) {
                return false;
            }
            try {
                this.f7895c.invoke(obj, new Object[0]);
                return true;
            } catch (Exception e2) {
                return false;
            }
        }

        static b a() {
            Method method;
            Method method2;
            Method method3 = null;
            try {
                Class<?> cls = Class.forName("dalvik.system.CloseGuard");
                method2 = cls.getMethod("get", new Class[0]);
                method = cls.getMethod("open", String.class);
                method3 = cls.getMethod("warnIfOpen", new Class[0]);
            } catch (Exception e2) {
                method = null;
                method2 = null;
            }
            return new b(method2, method, method3);
        }
    }
}
