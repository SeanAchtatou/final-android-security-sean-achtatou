package okhttp3.internal.b;

import java.net.ProtocolException;
import okhttp3.Protocol;

/* compiled from: StatusLine */
public final class k {

    /* renamed from: a  reason: collision with root package name */
    public final Protocol f7816a;

    /* renamed from: b  reason: collision with root package name */
    public final int f7817b;

    /* renamed from: c  reason: collision with root package name */
    public final String f7818c;

    public k(Protocol protocol, int i, String str) {
        this.f7816a = protocol;
        this.f7817b = i;
        this.f7818c = str;
    }

    public static k a(String str) {
        Protocol protocol;
        String str2;
        int i = 9;
        if (str.startsWith("HTTP/1.")) {
            if (str.length() < 9 || str.charAt(8) != ' ') {
                throw new ProtocolException("Unexpected status line: " + str);
            }
            int charAt = str.charAt(7) - '0';
            if (charAt == 0) {
                protocol = Protocol.HTTP_1_0;
            } else if (charAt == 1) {
                protocol = Protocol.HTTP_1_1;
            } else {
                throw new ProtocolException("Unexpected status line: " + str);
            }
        } else if (str.startsWith("ICY ")) {
            protocol = Protocol.HTTP_1_0;
            i = 4;
        } else {
            throw new ProtocolException("Unexpected status line: " + str);
        }
        if (str.length() < i + 3) {
            throw new ProtocolException("Unexpected status line: " + str);
        }
        try {
            int parseInt = Integer.parseInt(str.substring(i, i + 3));
            if (str.length() <= i + 3) {
                str2 = "";
            } else if (str.charAt(i + 3) != ' ') {
                throw new ProtocolException("Unexpected status line: " + str);
            } else {
                str2 = str.substring(i + 4);
            }
            return new k(protocol, parseInt, str2);
        } catch (NumberFormatException e2) {
            throw new ProtocolException("Unexpected status line: " + str);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f7816a == Protocol.HTTP_1_0 ? "HTTP/1.0" : "HTTP/1.1");
        sb.append(' ').append(this.f7817b);
        if (this.f7818c != null) {
            sb.append(' ').append(this.f7818c);
        }
        return sb.toString();
    }
}
