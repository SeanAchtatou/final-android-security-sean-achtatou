package okhttp3.internal.b;

import java.net.ProtocolException;
import okhttp3.internal.c;
import okhttp3.internal.connection.f;
import okhttp3.r;
import okhttp3.v;
import okhttp3.x;
import okio.d;
import okio.k;

/* compiled from: CallServerInterceptor */
public final class b implements r {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f7797a;

    public b(boolean z) {
        this.f7797a = z;
    }

    public x a(r.a aVar) {
        x a2;
        c c2 = ((g) aVar).c();
        f b2 = ((g) aVar).b();
        v a3 = aVar.a();
        long currentTimeMillis = System.currentTimeMillis();
        c2.a(a3);
        x.a aVar2 = null;
        if (f.c(a3.b()) && a3.d() != null) {
            if ("100-continue".equalsIgnoreCase(a3.a("Expect"))) {
                c2.a();
                aVar2 = c2.a(true);
            }
            if (aVar2 == null) {
                d a4 = k.a(c2.a(a3, a3.d().b()));
                a3.d().a(a4);
                a4.close();
            }
        }
        c2.b();
        if (aVar2 == null) {
            aVar2 = c2.a(false);
        }
        x a5 = aVar2.a(a3).a(b2.b().d()).a(currentTimeMillis).b(System.currentTimeMillis()).a();
        int b3 = a5.b();
        if (!this.f7797a || b3 != 101) {
            a2 = a5.h().a(c2.a(a5)).a();
        } else {
            a2 = a5.h().a(c.f7821c).a();
        }
        if ("close".equalsIgnoreCase(a2.a().a("Connection")) || "close".equalsIgnoreCase(a2.a("Connection"))) {
            b2.d();
        }
        if ((b3 != 204 && b3 != 205) || a2.g().b() <= 0) {
            return a2;
        }
        throw new ProtocolException("HTTP " + b3 + " had non-zero Content-Length: " + a2.g().b());
    }
}
