package okhttp3.internal.b;

import java.net.Proxy;
import okhttp3.HttpUrl;
import okhttp3.v;

/* compiled from: RequestLine */
public final class i {
    public static String a(v vVar, Proxy.Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(vVar.b());
        sb.append(' ');
        if (b(vVar, type)) {
            sb.append(vVar.a());
        } else {
            sb.append(a(vVar.a()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }

    private static boolean b(v vVar, Proxy.Type type) {
        return !vVar.g() && type == Proxy.Type.HTTP;
    }

    public static String a(HttpUrl httpUrl) {
        String h2 = httpUrl.h();
        String j = httpUrl.j();
        return j != null ? h2 + '?' + j : h2;
    }
}
