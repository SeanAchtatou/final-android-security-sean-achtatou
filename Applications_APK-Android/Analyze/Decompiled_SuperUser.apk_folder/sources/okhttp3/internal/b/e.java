package okhttp3.internal.b;

import java.util.List;
import java.util.regex.Pattern;
import okhttp3.HttpUrl;
import okhttp3.l;
import okhttp3.m;
import okhttp3.q;
import okhttp3.x;

/* compiled from: HttpHeaders */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f7801a = Pattern.compile(" +([^ \"=]*)=(:?\"([^\"]*)\"|([^ \"=]*)) *(:?,|$)");

    public static long a(x xVar) {
        return a(xVar.f());
    }

    public static long a(q qVar) {
        return a(qVar.a("Content-Length"));
    }

    private static long a(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e2) {
            return -1;
        }
    }

    public static void a(m mVar, HttpUrl httpUrl, q qVar) {
        if (mVar != m.f8101a) {
            List<l> a2 = l.a(httpUrl, qVar);
            if (!a2.isEmpty()) {
                mVar.a(httpUrl, a2);
            }
        }
    }

    public static boolean b(x xVar) {
        if (xVar.a().b().equals("HEAD")) {
            return false;
        }
        int b2 = xVar.b();
        if ((b2 < 100 || b2 >= 200) && b2 != 204 && b2 != 304) {
            return true;
        }
        if (a(xVar) != -1 || "chunked".equalsIgnoreCase(xVar.a("Transfer-Encoding"))) {
            return true;
        }
        return false;
    }

    public static int a(String str, int i, String str2) {
        while (i < str.length() && str2.indexOf(str.charAt(i)) == -1) {
            i++;
        }
        return i;
    }

    public static int a(String str, int i) {
        while (i < str.length() && ((r0 = str.charAt(i)) == ' ' || r0 == 9)) {
            i++;
        }
        return i;
    }

    public static int b(String str, int i) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong > 2147483647L) {
                return Integer.MAX_VALUE;
            }
            if (parseLong < 0) {
                return 0;
            }
            return (int) parseLong;
        } catch (NumberFormatException e2) {
            return i;
        }
    }
}
