package okhttp3.internal.b;

import java.util.List;
import okhttp3.HttpUrl;
import okhttp3.i;
import okhttp3.internal.connection.f;
import okhttp3.r;
import okhttp3.v;
import okhttp3.x;

/* compiled from: RealInterceptorChain */
public final class g implements r.a {

    /* renamed from: a  reason: collision with root package name */
    private final List<r> f7802a;

    /* renamed from: b  reason: collision with root package name */
    private final f f7803b;

    /* renamed from: c  reason: collision with root package name */
    private final c f7804c;

    /* renamed from: d  reason: collision with root package name */
    private final i f7805d;

    /* renamed from: e  reason: collision with root package name */
    private final int f7806e;

    /* renamed from: f  reason: collision with root package name */
    private final v f7807f;

    /* renamed from: g  reason: collision with root package name */
    private int f7808g;

    public g(List<r> list, f fVar, c cVar, i iVar, int i, v vVar) {
        this.f7802a = list;
        this.f7805d = iVar;
        this.f7803b = fVar;
        this.f7804c = cVar;
        this.f7806e = i;
        this.f7807f = vVar;
    }

    public f b() {
        return this.f7803b;
    }

    public c c() {
        return this.f7804c;
    }

    public v a() {
        return this.f7807f;
    }

    public x a(v vVar) {
        return a(vVar, this.f7803b, this.f7804c, this.f7805d);
    }

    public x a(v vVar, f fVar, c cVar, i iVar) {
        if (this.f7806e >= this.f7802a.size()) {
            throw new AssertionError();
        }
        this.f7808g++;
        if (this.f7804c != null && !a(vVar.a())) {
            throw new IllegalStateException("network interceptor " + this.f7802a.get(this.f7806e - 1) + " must retain the same host and port");
        } else if (this.f7804c == null || this.f7808g <= 1) {
            g gVar = new g(this.f7802a, fVar, cVar, iVar, this.f7806e + 1, vVar);
            r rVar = this.f7802a.get(this.f7806e);
            x a2 = rVar.a(gVar);
            if (cVar != null && this.f7806e + 1 < this.f7802a.size() && gVar.f7808g != 1) {
                throw new IllegalStateException("network interceptor " + rVar + " must call proceed() exactly once");
            } else if (a2 != null) {
                return a2;
            } else {
                throw new NullPointerException("interceptor " + rVar + " returned null");
            }
        } else {
            throw new IllegalStateException("network interceptor " + this.f7802a.get(this.f7806e - 1) + " must call proceed() exactly once");
        }
    }

    private boolean a(HttpUrl httpUrl) {
        return httpUrl.f().equals(this.f7805d.a().a().a().f()) && httpUrl.g() == this.f7805d.a().a().a().g();
    }
}
