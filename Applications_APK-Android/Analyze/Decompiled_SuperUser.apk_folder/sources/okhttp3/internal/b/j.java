package okhttp3.internal.b;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.HttpRetryException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.HttpUrl;
import okhttp3.a;
import okhttp3.g;
import okhttp3.internal.c;
import okhttp3.internal.connection.RouteException;
import okhttp3.internal.connection.f;
import okhttp3.internal.http2.ConnectionShutdownException;
import okhttp3.r;
import okhttp3.t;
import okhttp3.v;
import okhttp3.w;
import okhttp3.x;
import okhttp3.y;
import okhttp3.z;

/* compiled from: RetryAndFollowUpInterceptor */
public final class j implements r {

    /* renamed from: a  reason: collision with root package name */
    private final t f7811a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f7812b;

    /* renamed from: c  reason: collision with root package name */
    private f f7813c;

    /* renamed from: d  reason: collision with root package name */
    private Object f7814d;

    /* renamed from: e  reason: collision with root package name */
    private volatile boolean f7815e;

    public j(t tVar, boolean z) {
        this.f7811a = tVar;
        this.f7812b = z;
    }

    public void a() {
        this.f7815e = true;
        f fVar = this.f7813c;
        if (fVar != null) {
            fVar.e();
        }
    }

    public boolean b() {
        return this.f7815e;
    }

    public void a(Object obj) {
        this.f7814d = obj;
    }

    public x a(r.a aVar) {
        boolean z;
        v a2 = aVar.a();
        this.f7813c = new f(this.f7811a.o(), a(a2.a()), this.f7814d);
        x xVar = null;
        int i = 0;
        v vVar = a2;
        while (!this.f7815e) {
            try {
                x a3 = ((g) aVar).a(vVar, this.f7813c, null, null);
                if (xVar != null) {
                    a3 = a3.h().c(xVar.h().a((y) null).a()).a();
                }
                vVar = a(a3);
                if (vVar == null) {
                    if (!this.f7812b) {
                        this.f7813c.c();
                    }
                    return a3;
                }
                c.a(a3.g());
                int i2 = i + 1;
                if (i2 > 20) {
                    this.f7813c.c();
                    throw new ProtocolException("Too many follow-up requests: " + i2);
                } else if (vVar.d() instanceof l) {
                    this.f7813c.c();
                    throw new HttpRetryException("Cannot retry streamed HTTP body", a3.b());
                } else {
                    if (!a(a3, vVar.a())) {
                        this.f7813c.c();
                        this.f7813c = new f(this.f7811a.o(), a(vVar.a()), this.f7814d);
                    } else if (this.f7813c.a() != null) {
                        throw new IllegalStateException("Closing the body of " + a3 + " didn't close its backing stream. Bad interceptor?");
                    }
                    i = i2;
                    xVar = a3;
                }
            } catch (RouteException e2) {
                if (!a(e2.a(), false, vVar)) {
                    throw e2.a();
                }
            } catch (IOException e3) {
                if (!(e3 instanceof ConnectionShutdownException)) {
                    z = true;
                } else {
                    z = false;
                }
                if (!a(e3, z, vVar)) {
                    throw e3;
                }
            } catch (Throwable th) {
                this.f7813c.a((IOException) null);
                this.f7813c.c();
                throw th;
            }
        }
        this.f7813c.c();
        throw new IOException("Canceled");
    }

    private a a(HttpUrl httpUrl) {
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        g gVar = null;
        if (httpUrl.c()) {
            sSLSocketFactory = this.f7811a.j();
            hostnameVerifier = this.f7811a.k();
            gVar = this.f7811a.l();
        } else {
            hostnameVerifier = null;
            sSLSocketFactory = null;
        }
        return new a(httpUrl.f(), httpUrl.g(), this.f7811a.h(), this.f7811a.i(), sSLSocketFactory, hostnameVerifier, gVar, this.f7811a.n(), this.f7811a.d(), this.f7811a.t(), this.f7811a.u(), this.f7811a.e());
    }

    private boolean a(IOException iOException, boolean z, v vVar) {
        this.f7813c.a(iOException);
        if (!this.f7811a.r()) {
            return false;
        }
        if ((!z || !(vVar.d() instanceof l)) && a(iOException, z) && this.f7813c.f()) {
            return true;
        }
        return false;
    }

    private boolean a(IOException iOException, boolean z) {
        boolean z2 = true;
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (iOException instanceof InterruptedIOException) {
            if (!(iOException instanceof SocketTimeoutException) || z) {
                z2 = false;
            }
            return z2;
        } else if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException)) {
            return true;
        } else {
            return false;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private v a(x xVar) {
        String a2;
        HttpUrl c2;
        Proxy d2;
        w wVar = null;
        if (xVar == null) {
            throw new IllegalStateException();
        }
        okhttp3.internal.connection.c b2 = this.f7813c.b();
        z a3 = b2 != null ? b2.a() : null;
        int b3 = xVar.b();
        String b4 = xVar.a().b();
        switch (b3) {
            case 300:
            case 301:
            case 302:
            case 303:
                break;
            case 307:
            case 308:
                if (!b4.equals("GET") && !b4.equals("HEAD")) {
                    return null;
                }
            case 401:
                return this.f7811a.m().a(a3, xVar);
            case 407:
                if (a3 != null) {
                    d2 = a3.b();
                } else {
                    d2 = this.f7811a.d();
                }
                if (d2.type() == Proxy.Type.HTTP) {
                    return this.f7811a.n().a(a3, xVar);
                }
                throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
            case 408:
                if (!(xVar.a().d() instanceof l)) {
                    return xVar.a();
                }
                return null;
            default:
                return null;
        }
        if (!this.f7811a.q() || (a2 = xVar.a("Location")) == null || (c2 = xVar.a().a().c(a2)) == null) {
            return null;
        }
        if (!c2.b().equals(xVar.a().a().b()) && !this.f7811a.p()) {
            return null;
        }
        v.a e2 = xVar.a().e();
        if (f.c(b4)) {
            boolean d3 = f.d(b4);
            if (f.e(b4)) {
                e2.a("GET", (w) null);
            } else {
                if (d3) {
                    wVar = xVar.a().d();
                }
                e2.a(b4, wVar);
            }
            if (!d3) {
                e2.b("Transfer-Encoding");
                e2.b("Content-Length");
                e2.b("Content-Type");
            }
        }
        if (!a(xVar, c2)) {
            e2.b("Authorization");
        }
        return e2.a(c2).b();
    }

    private boolean a(x xVar, HttpUrl httpUrl) {
        HttpUrl a2 = xVar.a().a();
        return a2.f().equals(httpUrl.f()) && a2.g() == httpUrl.g() && a2.b().equals(httpUrl.b());
    }
}
