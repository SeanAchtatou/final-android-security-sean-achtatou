package okhttp3.internal.b;

import okhttp3.q;
import okhttp3.s;
import okhttp3.y;
import okio.e;

/* compiled from: RealResponseBody */
public final class h extends y {

    /* renamed from: a  reason: collision with root package name */
    private final q f7809a;

    /* renamed from: b  reason: collision with root package name */
    private final e f7810b;

    public h(q qVar, e eVar) {
        this.f7809a = qVar;
        this.f7810b = eVar;
    }

    public s a() {
        String a2 = this.f7809a.a("Content-Type");
        if (a2 != null) {
            return s.a(a2);
        }
        return null;
    }

    public long b() {
        return e.a(this.f7809a);
    }

    public e c() {
        return this.f7810b;
    }
}
