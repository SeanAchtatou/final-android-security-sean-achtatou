package okhttp3.internal.b;

import java.util.List;
import okhttp3.internal.c;
import okhttp3.internal.d;
import okhttp3.l;
import okhttp3.m;
import okhttp3.q;
import okhttp3.r;
import okhttp3.s;
import okhttp3.v;
import okhttp3.w;
import okhttp3.x;
import okio.i;
import okio.k;

/* compiled from: BridgeInterceptor */
public final class a implements r {

    /* renamed from: a  reason: collision with root package name */
    private final m f7796a;

    public a(m mVar) {
        this.f7796a = mVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.c.a(okhttp3.HttpUrl, boolean):java.lang.String
     arg types: [okhttp3.HttpUrl, int]
     candidates:
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object):int
      okhttp3.internal.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      okhttp3.internal.c.a(okio.e, java.nio.charset.Charset):java.nio.charset.Charset
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      okhttp3.internal.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      okhttp3.internal.c.a(java.lang.Object, java.lang.Object):boolean
      okhttp3.internal.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      okhttp3.internal.c.a(okhttp3.HttpUrl, boolean):java.lang.String */
    public x a(r.a aVar) {
        boolean z = false;
        v a2 = aVar.a();
        v.a e2 = a2.e();
        w d2 = a2.d();
        if (d2 != null) {
            s a3 = d2.a();
            if (a3 != null) {
                e2.a("Content-Type", a3.toString());
            }
            long b2 = d2.b();
            if (b2 != -1) {
                e2.a("Content-Length", Long.toString(b2));
                e2.b("Transfer-Encoding");
            } else {
                e2.a("Transfer-Encoding", "chunked");
                e2.b("Content-Length");
            }
        }
        if (a2.a("Host") == null) {
            e2.a("Host", c.a(a2.a(), false));
        }
        if (a2.a("Connection") == null) {
            e2.a("Connection", "Keep-Alive");
        }
        if (a2.a("Accept-Encoding") == null && a2.a("Range") == null) {
            z = true;
            e2.a("Accept-Encoding", "gzip");
        }
        List<l> a4 = this.f7796a.a(a2.a());
        if (!a4.isEmpty()) {
            e2.a("Cookie", a(a4));
        }
        if (a2.a(io.fabric.sdk.android.services.common.a.HEADER_USER_AGENT) == null) {
            e2.a(io.fabric.sdk.android.services.common.a.HEADER_USER_AGENT, d.a());
        }
        x a5 = aVar.a(e2.b());
        e.a(this.f7796a, a2.a(), a5.f());
        x.a a6 = a5.h().a(a2);
        if (z && "gzip".equalsIgnoreCase(a5.a("Content-Encoding")) && e.b(a5)) {
            i iVar = new i(a5.g().c());
            q a7 = a5.f().b().b("Content-Encoding").b("Content-Length").a();
            a6.a(a7);
            a6.a(new h(a7, k.a(iVar)));
        }
        return a6.a();
    }

    private String a(List<l> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            l lVar = list.get(i);
            sb.append(lVar.a()).append('=').append(lVar.b());
        }
        return sb.toString();
    }
}
