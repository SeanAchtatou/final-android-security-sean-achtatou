package okhttp3.internal.connection;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import okhttp3.internal.b.c;
import okhttp3.internal.http2.ConnectionShutdownException;
import okhttp3.internal.http2.ErrorCode;
import okhttp3.internal.http2.StreamResetException;
import okhttp3.j;
import okhttp3.t;
import okhttp3.z;

/* compiled from: StreamAllocation */
public final class f {

    /* renamed from: b  reason: collision with root package name */
    static final /* synthetic */ boolean f7875b = (!f.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public final okhttp3.a f7876a;

    /* renamed from: c  reason: collision with root package name */
    private z f7877c;

    /* renamed from: d  reason: collision with root package name */
    private final j f7878d;

    /* renamed from: e  reason: collision with root package name */
    private final Object f7879e;

    /* renamed from: f  reason: collision with root package name */
    private final e f7880f;

    /* renamed from: g  reason: collision with root package name */
    private int f7881g;

    /* renamed from: h  reason: collision with root package name */
    private c f7882h;
    private boolean i;
    private boolean j;
    private c k;

    public f(j jVar, okhttp3.a aVar, Object obj) {
        this.f7878d = jVar;
        this.f7876a = aVar;
        this.f7880f = new e(aVar, g());
        this.f7879e = obj;
    }

    public c a(t tVar, boolean z) {
        try {
            c a2 = a(tVar.a(), tVar.b(), tVar.c(), tVar.r(), z).a(tVar, this);
            synchronized (this.f7878d) {
                this.k = a2;
            }
            return a2;
        } catch (IOException e2) {
            throw new RouteException(e2);
        }
    }

    private c a(int i2, int i3, int i4, boolean z, boolean z2) {
        c a2;
        while (true) {
            a2 = a(i2, i3, i4, z);
            synchronized (this.f7878d) {
                if (a2.f7860b != 0) {
                    if (a2.a(z2)) {
                        break;
                    }
                    d();
                } else {
                    break;
                }
            }
        }
        return a2;
    }

    private c a(int i2, int i3, int i4, boolean z) {
        c cVar;
        c cVar2;
        Socket socket;
        synchronized (this.f7878d) {
            if (this.i) {
                throw new IllegalStateException("released");
            } else if (this.k != null) {
                throw new IllegalStateException("codec != null");
            } else if (this.j) {
                throw new IOException("Canceled");
            } else {
                cVar = this.f7882h;
                if (cVar == null || cVar.f7859a) {
                    okhttp3.internal.a.f7759a.a(this.f7878d, this.f7876a, this);
                    if (this.f7882h != null) {
                        cVar = this.f7882h;
                    } else {
                        z zVar = this.f7877c;
                        if (zVar == null) {
                            zVar = this.f7880f.b();
                        }
                        synchronized (this.f7878d) {
                            this.f7877c = zVar;
                            this.f7881g = 0;
                            cVar2 = new c(this.f7878d, zVar);
                            a(cVar2);
                            if (this.j) {
                                throw new IOException("Canceled");
                            }
                        }
                        cVar2.a(i2, i3, i4, z);
                        g().b(cVar2.a());
                        synchronized (this.f7878d) {
                            okhttp3.internal.a.f7759a.b(this.f7878d, cVar2);
                            if (cVar2.e()) {
                                Socket b2 = okhttp3.internal.a.f7759a.b(this.f7878d, this.f7876a, this);
                                cVar = this.f7882h;
                                socket = b2;
                            } else {
                                cVar = cVar2;
                                socket = null;
                            }
                        }
                        okhttp3.internal.c.a(socket);
                    }
                }
            }
        }
        return cVar;
    }

    public void a(boolean z, c cVar) {
        Socket a2;
        synchronized (this.f7878d) {
            if (cVar != null) {
                if (cVar == this.k) {
                    if (!z) {
                        this.f7882h.f7860b++;
                    }
                    a2 = a(z, false, true);
                }
            }
            throw new IllegalStateException("expected " + this.k + " but was " + cVar);
        }
        okhttp3.internal.c.a(a2);
    }

    public c a() {
        c cVar;
        synchronized (this.f7878d) {
            cVar = this.k;
        }
        return cVar;
    }

    private d g() {
        return okhttp3.internal.a.f7759a.a(this.f7878d);
    }

    public synchronized c b() {
        return this.f7882h;
    }

    public void c() {
        Socket a2;
        synchronized (this.f7878d) {
            a2 = a(false, true, false);
        }
        okhttp3.internal.c.a(a2);
    }

    public void d() {
        Socket a2;
        synchronized (this.f7878d) {
            a2 = a(true, false, false);
        }
        okhttp3.internal.c.a(a2);
    }

    private Socket a(boolean z, boolean z2, boolean z3) {
        Socket socket;
        if (f7875b || Thread.holdsLock(this.f7878d)) {
            if (z3) {
                this.k = null;
            }
            if (z2) {
                this.i = true;
            }
            if (this.f7882h == null) {
                return null;
            }
            if (z) {
                this.f7882h.f7859a = true;
            }
            if (this.k != null) {
                return null;
            }
            if (!this.i && !this.f7882h.f7859a) {
                return null;
            }
            c(this.f7882h);
            if (this.f7882h.f7862d.isEmpty()) {
                this.f7882h.f7863e = System.nanoTime();
                if (okhttp3.internal.a.f7759a.a(this.f7878d, this.f7882h)) {
                    socket = this.f7882h.c();
                    this.f7882h = null;
                    return socket;
                }
            }
            socket = null;
            this.f7882h = null;
            return socket;
        }
        throw new AssertionError();
    }

    public void e() {
        c cVar;
        c cVar2;
        synchronized (this.f7878d) {
            this.j = true;
            cVar = this.k;
            cVar2 = this.f7882h;
        }
        if (cVar != null) {
            cVar.c();
        } else if (cVar2 != null) {
            cVar2.b();
        }
    }

    public void a(IOException iOException) {
        Socket a2;
        boolean z = false;
        synchronized (this.f7878d) {
            if (iOException instanceof StreamResetException) {
                StreamResetException streamResetException = (StreamResetException) iOException;
                if (streamResetException.f7931a == ErrorCode.REFUSED_STREAM) {
                    this.f7881g++;
                }
                if (streamResetException.f7931a != ErrorCode.REFUSED_STREAM || this.f7881g > 1) {
                    this.f7877c = null;
                }
                a2 = a(z, false, true);
            } else {
                if (this.f7882h != null && (!this.f7882h.e() || (iOException instanceof ConnectionShutdownException))) {
                    if (this.f7882h.f7860b == 0) {
                        if (!(this.f7877c == null || iOException == null)) {
                            this.f7880f.a(this.f7877c, iOException);
                        }
                        this.f7877c = null;
                    }
                }
                a2 = a(z, false, true);
            }
            z = true;
            a2 = a(z, false, true);
        }
        okhttp3.internal.c.a(a2);
    }

    public void a(c cVar) {
        if (!f7875b && !Thread.holdsLock(this.f7878d)) {
            throw new AssertionError();
        } else if (this.f7882h != null) {
            throw new IllegalStateException();
        } else {
            this.f7882h = cVar;
            cVar.f7862d.add(new a(this, this.f7879e));
        }
    }

    private void c(c cVar) {
        int size = cVar.f7862d.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (cVar.f7862d.get(i2).get() == this) {
                cVar.f7862d.remove(i2);
                return;
            }
        }
        throw new IllegalStateException();
    }

    public Socket b(c cVar) {
        if (!f7875b && !Thread.holdsLock(this.f7878d)) {
            throw new AssertionError();
        } else if (this.k == null && this.f7882h.f7862d.size() == 1) {
            Socket a2 = a(true, false, false);
            this.f7882h = cVar;
            cVar.f7862d.add(this.f7882h.f7862d.get(0));
            return a2;
        } else {
            throw new IllegalStateException();
        }
    }

    public boolean f() {
        return this.f7877c != null || this.f7880f.a();
    }

    public String toString() {
        c b2 = b();
        return b2 != null ? b2.toString() : this.f7876a.toString();
    }

    /* compiled from: StreamAllocation */
    public static final class a extends WeakReference<f> {

        /* renamed from: a  reason: collision with root package name */
        public final Object f7883a;

        a(f fVar, Object obj) {
            super(fVar);
            this.f7883a = obj;
        }
    }
}
