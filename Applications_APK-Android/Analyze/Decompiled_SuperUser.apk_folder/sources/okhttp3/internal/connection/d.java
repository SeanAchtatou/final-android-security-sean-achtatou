package okhttp3.internal.connection;

import java.util.LinkedHashSet;
import java.util.Set;
import okhttp3.z;

/* compiled from: RouteDatabase */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final Set<z> f7866a = new LinkedHashSet();

    public synchronized void a(z zVar) {
        this.f7866a.add(zVar);
    }

    public synchronized void b(z zVar) {
        this.f7866a.remove(zVar);
    }

    public synchronized boolean c(z zVar) {
        return this.f7866a.contains(zVar);
    }
}
