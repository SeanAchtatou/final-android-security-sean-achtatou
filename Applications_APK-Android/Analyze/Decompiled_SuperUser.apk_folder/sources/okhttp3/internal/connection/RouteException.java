package okhttp3.internal.connection;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class RouteException extends RuntimeException {

    /* renamed from: a  reason: collision with root package name */
    private static final Method f7852a;

    /* renamed from: b  reason: collision with root package name */
    private IOException f7853b;

    static {
        Method method;
        try {
            method = Throwable.class.getDeclaredMethod("addSuppressed", Throwable.class);
        } catch (Exception e2) {
            method = null;
        }
        f7852a = method;
    }

    public RouteException(IOException iOException) {
        super(iOException);
        this.f7853b = iOException;
    }

    public IOException a() {
        return this.f7853b;
    }

    public void a(IOException iOException) {
        a(iOException, this.f7853b);
        this.f7853b = iOException;
    }

    private void a(IOException iOException, IOException iOException2) {
        if (f7852a != null) {
            try {
                f7852a.invoke(iOException, iOException2);
            } catch (IllegalAccessException | InvocationTargetException e2) {
            }
        }
    }
}
