package okhttp3.internal.connection;

import okhttp3.internal.b.g;
import okhttp3.r;
import okhttp3.t;
import okhttp3.v;
import okhttp3.x;

/* compiled from: ConnectInterceptor */
public final class a implements r {

    /* renamed from: a  reason: collision with root package name */
    public final t f7854a;

    public a(t tVar) {
        this.f7854a = tVar;
    }

    public x a(r.a aVar) {
        g gVar = (g) aVar;
        v a2 = gVar.a();
        f b2 = gVar.b();
        return gVar.a(a2, b2, b2.a(this.f7854a, !a2.b().equals("GET")), b2.b());
    }
}
