package okhttp3.internal.connection;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import okhttp3.HttpUrl;
import okhttp3.a;
import okhttp3.internal.c;
import okhttp3.z;

/* compiled from: RouteSelector */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final a f7867a;

    /* renamed from: b  reason: collision with root package name */
    private final d f7868b;

    /* renamed from: c  reason: collision with root package name */
    private Proxy f7869c;

    /* renamed from: d  reason: collision with root package name */
    private InetSocketAddress f7870d;

    /* renamed from: e  reason: collision with root package name */
    private List<Proxy> f7871e = Collections.emptyList();

    /* renamed from: f  reason: collision with root package name */
    private int f7872f;

    /* renamed from: g  reason: collision with root package name */
    private List<InetSocketAddress> f7873g = Collections.emptyList();

    /* renamed from: h  reason: collision with root package name */
    private int f7874h;
    private final List<z> i = new ArrayList();

    public e(a aVar, d dVar) {
        this.f7867a = aVar;
        this.f7868b = dVar;
        a(aVar.a(), aVar.h());
    }

    public boolean a() {
        return e() || c() || g();
    }

    public z b() {
        if (!e()) {
            if (c()) {
                this.f7869c = d();
            } else if (g()) {
                return h();
            } else {
                throw new NoSuchElementException();
            }
        }
        this.f7870d = f();
        z zVar = new z(this.f7867a, this.f7869c, this.f7870d);
        if (!this.f7868b.c(zVar)) {
            return zVar;
        }
        this.i.add(zVar);
        return b();
    }

    public void a(z zVar, IOException iOException) {
        if (!(zVar.b().type() == Proxy.Type.DIRECT || this.f7867a.g() == null)) {
            this.f7867a.g().connectFailed(this.f7867a.a().a(), zVar.b().address(), iOException);
        }
        this.f7868b.a(zVar);
    }

    private void a(HttpUrl httpUrl, Proxy proxy) {
        List<Proxy> a2;
        if (proxy != null) {
            this.f7871e = Collections.singletonList(proxy);
        } else {
            List<Proxy> select = this.f7867a.g().select(httpUrl.a());
            if (select == null || select.isEmpty()) {
                a2 = c.a(Proxy.NO_PROXY);
            } else {
                a2 = c.a(select);
            }
            this.f7871e = a2;
        }
        this.f7872f = 0;
    }

    private boolean c() {
        return this.f7872f < this.f7871e.size();
    }

    private Proxy d() {
        if (!c()) {
            throw new SocketException("No route to " + this.f7867a.a().f() + "; exhausted proxy configurations: " + this.f7871e);
        }
        List<Proxy> list = this.f7871e;
        int i2 = this.f7872f;
        this.f7872f = i2 + 1;
        Proxy proxy = list.get(i2);
        a(proxy);
        return proxy;
    }

    private void a(Proxy proxy) {
        int i2;
        String str;
        this.f7873g = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            String f2 = this.f7867a.a().f();
            i2 = this.f7867a.a().g();
            str = f2;
        } else {
            SocketAddress address = proxy.address();
            if (!(address instanceof InetSocketAddress)) {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
            InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
            String a2 = a(inetSocketAddress);
            i2 = inetSocketAddress.getPort();
            str = a2;
        }
        if (i2 < 1 || i2 > 65535) {
            throw new SocketException("No route to " + str + ":" + i2 + "; port is out of range");
        }
        if (proxy.type() == Proxy.Type.SOCKS) {
            this.f7873g.add(InetSocketAddress.createUnresolved(str, i2));
        } else {
            List<InetAddress> a3 = this.f7867a.b().a(str);
            int size = a3.size();
            for (int i3 = 0; i3 < size; i3++) {
                this.f7873g.add(new InetSocketAddress(a3.get(i3), i2));
            }
        }
        this.f7874h = 0;
    }

    static String a(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        if (address == null) {
            return inetSocketAddress.getHostName();
        }
        return address.getHostAddress();
    }

    private boolean e() {
        return this.f7874h < this.f7873g.size();
    }

    private InetSocketAddress f() {
        if (!e()) {
            throw new SocketException("No route to " + this.f7867a.a().f() + "; exhausted inet socket addresses: " + this.f7873g);
        }
        List<InetSocketAddress> list = this.f7873g;
        int i2 = this.f7874h;
        this.f7874h = i2 + 1;
        return list.get(i2);
    }

    private boolean g() {
        return !this.i.isEmpty();
    }

    private z h() {
        return this.i.remove(0);
    }
}
