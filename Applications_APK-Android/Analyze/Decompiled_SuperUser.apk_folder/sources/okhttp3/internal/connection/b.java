package okhttp3.internal.connection;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;
import okhttp3.internal.a;
import okhttp3.k;

/* compiled from: ConnectionSpecSelector */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final List<k> f7855a;

    /* renamed from: b  reason: collision with root package name */
    private int f7856b = 0;

    /* renamed from: c  reason: collision with root package name */
    private boolean f7857c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f7858d;

    public b(List<k> list) {
        this.f7855a = list;
    }

    public k a(SSLSocket sSLSocket) {
        k kVar;
        int i = this.f7856b;
        int size = this.f7855a.size();
        int i2 = i;
        while (true) {
            if (i2 >= size) {
                kVar = null;
                break;
            }
            kVar = this.f7855a.get(i2);
            if (kVar.a(sSLSocket)) {
                this.f7856b = i2 + 1;
                break;
            }
            i2++;
        }
        if (kVar == null) {
            throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.f7858d + ", modes=" + this.f7855a + ", supported protocols=" + Arrays.toString(sSLSocket.getEnabledProtocols()));
        }
        this.f7857c = b(sSLSocket);
        a.f7759a.a(kVar, sSLSocket, this.f7858d);
        return kVar;
    }

    public boolean a(IOException iOException) {
        this.f7858d = true;
        if (!this.f7857c || (iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        if (((iOException instanceof SSLHandshakeException) && (iOException.getCause() instanceof CertificateException)) || (iOException instanceof SSLPeerUnverifiedException)) {
            return false;
        }
        if ((iOException instanceof SSLHandshakeException) || (iOException instanceof SSLProtocolException)) {
            return true;
        }
        return false;
    }

    private boolean b(SSLSocket sSLSocket) {
        int i = this.f7856b;
        while (true) {
            int i2 = i;
            if (i2 >= this.f7855a.size()) {
                return false;
            }
            if (this.f7855a.get(i2).a(sSLSocket)) {
                return true;
            }
            i = i2 + 1;
        }
    }
}
