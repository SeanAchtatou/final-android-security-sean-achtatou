package okhttp3.internal.connection;

import io.fabric.sdk.android.services.common.a;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.HttpUrl;
import okhttp3.Protocol;
import okhttp3.i;
import okhttp3.internal.http2.ErrorCode;
import okhttp3.internal.http2.e;
import okhttp3.internal.http2.g;
import okhttp3.j;
import okhttp3.p;
import okhttp3.t;
import okhttp3.v;
import okhttp3.z;
import okio.d;
import okio.k;

/* compiled from: RealConnection */
public final class c extends e.b implements i {

    /* renamed from: a  reason: collision with root package name */
    public boolean f7859a;

    /* renamed from: b  reason: collision with root package name */
    public int f7860b;

    /* renamed from: c  reason: collision with root package name */
    public int f7861c = 1;

    /* renamed from: d  reason: collision with root package name */
    public final List<Reference<f>> f7862d = new ArrayList();

    /* renamed from: e  reason: collision with root package name */
    public long f7863e = Long.MAX_VALUE;

    /* renamed from: g  reason: collision with root package name */
    private final j f7864g;

    /* renamed from: h  reason: collision with root package name */
    private final z f7865h;
    private Socket i;
    private Socket j;
    private p k;
    private Protocol l;
    private e m;
    private okio.e n;
    private d o;

    public c(j jVar, z zVar) {
        this.f7864g = jVar;
        this.f7865h = zVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0099 A[SYNTHETIC, Splitter:B:27:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0082 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r6, int r7, int r8, boolean r9) {
        /*
            r5 = this;
            r2 = 0
            okhttp3.Protocol r0 = r5.l
            if (r0 == 0) goto L_0x000d
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "already connected"
            r0.<init>(r1)
            throw r0
        L_0x000d:
            okhttp3.z r0 = r5.f7865h
            okhttp3.a r0 = r0.a()
            java.util.List r0 = r0.f()
            okhttp3.internal.connection.b r3 = new okhttp3.internal.connection.b
            r3.<init>(r0)
            okhttp3.z r1 = r5.f7865h
            okhttp3.a r1 = r1.a()
            javax.net.ssl.SSLSocketFactory r1 = r1.i()
            if (r1 != 0) goto L_0x0079
            okhttp3.k r1 = okhttp3.k.f8083c
            boolean r0 = r0.contains(r1)
            if (r0 != 0) goto L_0x003d
            okhttp3.internal.connection.RouteException r0 = new okhttp3.internal.connection.RouteException
            java.net.UnknownServiceException r1 = new java.net.UnknownServiceException
            java.lang.String r2 = "CLEARTEXT communication not enabled for client"
            r1.<init>(r2)
            r0.<init>(r1)
            throw r0
        L_0x003d:
            okhttp3.z r0 = r5.f7865h
            okhttp3.a r0 = r0.a()
            okhttp3.HttpUrl r0 = r0.a()
            java.lang.String r0 = r0.f()
            okhttp3.internal.e.e r1 = okhttp3.internal.e.e.b()
            boolean r1 = r1.b(r0)
            if (r1 != 0) goto L_0x0079
            okhttp3.internal.connection.RouteException r1 = new okhttp3.internal.connection.RouteException
            java.net.UnknownServiceException r2 = new java.net.UnknownServiceException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "CLEARTEXT communication to "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = " not permitted by network security policy"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r1.<init>(r2)
            throw r1
        L_0x0079:
            r1 = r2
        L_0x007a:
            okhttp3.z r0 = r5.f7865h     // Catch:{ IOException -> 0x009d }
            boolean r0 = r0.d()     // Catch:{ IOException -> 0x009d }
            if (r0 == 0) goto L_0x0099
            r5.a(r6, r7, r8)     // Catch:{ IOException -> 0x009d }
        L_0x0085:
            r5.a(r3)     // Catch:{ IOException -> 0x009d }
            okhttp3.internal.http2.e r0 = r5.m
            if (r0 == 0) goto L_0x0098
            okhttp3.j r1 = r5.f7864g
            monitor-enter(r1)
            okhttp3.internal.http2.e r0 = r5.m     // Catch:{ all -> 0x00ca }
            int r0 = r0.a()     // Catch:{ all -> 0x00ca }
            r5.f7861c = r0     // Catch:{ all -> 0x00ca }
            monitor-exit(r1)     // Catch:{ all -> 0x00ca }
        L_0x0098:
            return
        L_0x0099:
            r5.a(r6, r7)     // Catch:{ IOException -> 0x009d }
            goto L_0x0085
        L_0x009d:
            r0 = move-exception
            java.net.Socket r4 = r5.j
            okhttp3.internal.c.a(r4)
            java.net.Socket r4 = r5.i
            okhttp3.internal.c.a(r4)
            r5.j = r2
            r5.i = r2
            r5.n = r2
            r5.o = r2
            r5.k = r2
            r5.l = r2
            r5.m = r2
            if (r1 != 0) goto L_0x00c6
            okhttp3.internal.connection.RouteException r1 = new okhttp3.internal.connection.RouteException
            r1.<init>(r0)
        L_0x00bd:
            if (r9 == 0) goto L_0x00c5
            boolean r0 = r3.a(r0)
            if (r0 != 0) goto L_0x007a
        L_0x00c5:
            throw r1
        L_0x00c6:
            r1.a(r0)
            goto L_0x00bd
        L_0x00ca:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00ca }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.c.a(int, int, int, boolean):void");
    }

    private void a(int i2, int i3, int i4) {
        v f2 = f();
        HttpUrl a2 = f2.a();
        int i5 = 0;
        while (true) {
            i5++;
            if (i5 > 21) {
                throw new ProtocolException("Too many tunnel connections attempted: " + 21);
            }
            a(i2, i3);
            f2 = a(i3, i4, f2, a2);
            if (f2 != null) {
                okhttp3.internal.c.a(this.i);
                this.i = null;
                this.o = null;
                this.n = null;
            } else {
                return;
            }
        }
    }

    private void a(int i2, int i3) {
        Proxy b2 = this.f7865h.b();
        this.i = (b2.type() == Proxy.Type.DIRECT || b2.type() == Proxy.Type.HTTP) ? this.f7865h.a().c().createSocket() : new Socket(b2);
        this.i.setSoTimeout(i3);
        try {
            okhttp3.internal.e.e.b().a(this.i, this.f7865h.c(), i2);
            this.n = k.a(k.b(this.i));
            this.o = k.a(k.a(this.i));
        } catch (ConnectException e2) {
            ConnectException connectException = new ConnectException("Failed to connect to " + this.f7865h.c());
            connectException.initCause(e2);
            throw connectException;
        }
    }

    private void a(b bVar) {
        if (this.f7865h.a().i() == null) {
            this.l = Protocol.HTTP_1_1;
            this.j = this.i;
            return;
        }
        b(bVar);
        if (this.l == Protocol.HTTP_2) {
            this.j.setSoTimeout(0);
            this.m = new e.a(true).a(this.j, this.f7865h.a().a().f(), this.n, this.o).a(this).a();
            this.m.c();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v20, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v21, resolved type: java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(okhttp3.internal.connection.b r10) {
        /*
            r9 = this;
            r1 = 0
            okhttp3.z r0 = r9.f7865h
            okhttp3.a r2 = r0.a()
            javax.net.ssl.SSLSocketFactory r0 = r2.i()
            java.net.Socket r3 = r9.i     // Catch:{ AssertionError -> 0x0132 }
            okhttp3.HttpUrl r4 = r2.a()     // Catch:{ AssertionError -> 0x0132 }
            java.lang.String r4 = r4.f()     // Catch:{ AssertionError -> 0x0132 }
            okhttp3.HttpUrl r5 = r2.a()     // Catch:{ AssertionError -> 0x0132 }
            int r5 = r5.g()     // Catch:{ AssertionError -> 0x0132 }
            r6 = 1
            java.net.Socket r0 = r0.createSocket(r3, r4, r5, r6)     // Catch:{ AssertionError -> 0x0132 }
            javax.net.ssl.SSLSocket r0 = (javax.net.ssl.SSLSocket) r0     // Catch:{ AssertionError -> 0x0132 }
            okhttp3.k r3 = r10.a(r0)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            boolean r4 = r3.d()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            if (r4 == 0) goto L_0x0041
            okhttp3.internal.e.e r4 = okhttp3.internal.e.e.b()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            okhttp3.HttpUrl r5 = r2.a()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r5 = r5.f()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.util.List r6 = r2.e()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            r4.a(r0, r5, r6)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
        L_0x0041:
            r0.startHandshake()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            javax.net.ssl.SSLSession r4 = r0.getSession()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            okhttp3.p r4 = okhttp3.p.a(r4)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            javax.net.ssl.HostnameVerifier r5 = r2.j()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            okhttp3.HttpUrl r6 = r2.a()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r6 = r6.f()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            javax.net.ssl.SSLSession r7 = r0.getSession()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            boolean r5 = r5.verify(r6, r7)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            if (r5 != 0) goto L_0x00da
            java.util.List r1 = r4.b()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            r3 = 0
            java.lang.Object r1 = r1.get(r3)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.security.cert.X509Certificate r1 = (java.security.cert.X509Certificate) r1     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            javax.net.ssl.SSLPeerUnverifiedException r3 = new javax.net.ssl.SSLPeerUnverifiedException     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            r4.<init>()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r5 = "Hostname "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            okhttp3.HttpUrl r2 = r2.a()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r2 = r2.f()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r4 = " not verified:\n    certificate: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r4 = okhttp3.g.a(r1)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r4 = "\n    DN: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.security.Principal r4 = r1.getSubjectDN()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r4 = r4.getName()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r4 = "\n    subjectAltNames: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.util.List r1 = okhttp3.internal.f.d.a(r1)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r1 = r1.toString()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            r3.<init>(r1)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            throw r3     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
        L_0x00bc:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00c0:
            boolean r2 = okhttp3.internal.c.a(r0)     // Catch:{ all -> 0x00cc }
            if (r2 == 0) goto L_0x012c
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x00cc }
            r2.<init>(r0)     // Catch:{ all -> 0x00cc }
            throw r2     // Catch:{ all -> 0x00cc }
        L_0x00cc:
            r0 = move-exception
        L_0x00cd:
            if (r1 == 0) goto L_0x00d6
            okhttp3.internal.e.e r2 = okhttp3.internal.e.e.b()
            r2.b(r1)
        L_0x00d6:
            okhttp3.internal.c.a(r1)
            throw r0
        L_0x00da:
            okhttp3.g r5 = r2.k()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            okhttp3.HttpUrl r2 = r2.a()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r2 = r2.f()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.util.List r6 = r4.b()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            r5.a(r2, r6)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            boolean r2 = r3.d()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            if (r2 == 0) goto L_0x00fb
            okhttp3.internal.e.e r1 = okhttp3.internal.e.e.b()     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.lang.String r1 = r1.a(r0)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
        L_0x00fb:
            r9.j = r0     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.net.Socket r2 = r9.j     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            okio.q r2 = okio.k.b(r2)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            okio.e r2 = okio.k.a(r2)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            r9.n = r2     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            java.net.Socket r2 = r9.j     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            okio.p r2 = okio.k.a(r2)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            okio.d r2 = okio.k.a(r2)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            r9.o = r2     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            r9.k = r4     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            if (r1 == 0) goto L_0x0129
            okhttp3.Protocol r1 = okhttp3.Protocol.a(r1)     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
        L_0x011d:
            r9.l = r1     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            if (r0 == 0) goto L_0x0128
            okhttp3.internal.e.e r1 = okhttp3.internal.e.e.b()
            r1.b(r0)
        L_0x0128:
            return
        L_0x0129:
            okhttp3.Protocol r1 = okhttp3.Protocol.HTTP_1_1     // Catch:{ AssertionError -> 0x00bc, all -> 0x012d }
            goto L_0x011d
        L_0x012c:
            throw r0     // Catch:{ all -> 0x00cc }
        L_0x012d:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00cd
        L_0x0132:
            r0 = move-exception
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.c.b(okhttp3.internal.connection.b):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0079 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0096 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0061  */
    private okhttp3.v a(int r9, int r10, okhttp3.v r11, okhttp3.HttpUrl r12) {
        /*
            r8 = this;
            r2 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "CONNECT "
            java.lang.StringBuilder r0 = r0.append(r1)
            r1 = 1
            java.lang.String r1 = okhttp3.internal.c.a(r12, r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " HTTP/1.1"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r3 = r0.toString()
        L_0x001f:
            okhttp3.internal.c.a r4 = new okhttp3.internal.c.a
            okio.e r0 = r8.n
            okio.d r1 = r8.o
            r4.<init>(r2, r2, r0, r1)
            okio.e r0 = r8.n
            okio.r r0 = r0.a()
            long r6 = (long) r9
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.MILLISECONDS
            r0.a(r6, r1)
            okio.d r0 = r8.o
            okio.r r0 = r0.a()
            long r6 = (long) r10
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.MILLISECONDS
            r0.a(r6, r1)
            okhttp3.q r0 = r11.c()
            r4.a(r0, r3)
            r4.b()
            r0 = 0
            okhttp3.x$a r0 = r4.a(r0)
            okhttp3.x$a r0 = r0.a(r11)
            okhttp3.x r5 = r0.a()
            long r0 = okhttp3.internal.b.e.a(r5)
            r6 = -1
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 != 0) goto L_0x0063
            r0 = 0
        L_0x0063:
            okio.q r0 = r4.b(r0)
            r1 = 2147483647(0x7fffffff, float:NaN)
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS
            okhttp3.internal.c.b(r0, r1, r4)
            r0.close()
            int r0 = r5.b()
            switch(r0) {
                case 200: goto L_0x0096;
                case 407: goto L_0x00b8;
                default: goto L_0x0079;
            }
        L_0x0079:
            java.io.IOException r0 = new java.io.IOException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unexpected response code for CONNECT: "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r5.b()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0096:
            okio.e r0 = r8.n
            okio.c r0 = r0.c()
            boolean r0 = r0.f()
            if (r0 == 0) goto L_0x00ae
            okio.d r0 = r8.o
            okio.c r0 = r0.c()
            boolean r0 = r0.f()
            if (r0 != 0) goto L_0x00b6
        L_0x00ae:
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "TLS tunnel buffered too many bytes!"
            r0.<init>(r1)
            throw r0
        L_0x00b6:
            r11 = r2
        L_0x00b7:
            return r11
        L_0x00b8:
            okhttp3.z r0 = r8.f7865h
            okhttp3.a r0 = r0.a()
            okhttp3.b r0 = r0.d()
            okhttp3.z r1 = r8.f7865h
            okhttp3.v r11 = r0.a(r1, r5)
            if (r11 != 0) goto L_0x00d2
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "Failed to authenticate with proxy"
            r0.<init>(r1)
            throw r0
        L_0x00d2:
            java.lang.String r0 = "close"
            java.lang.String r1 = "Connection"
            java.lang.String r1 = r5.a(r1)
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x001f
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.connection.c.a(int, int, okhttp3.v, okhttp3.HttpUrl):okhttp3.v");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.c.a(okhttp3.HttpUrl, boolean):java.lang.String
     arg types: [okhttp3.HttpUrl, int]
     candidates:
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object):int
      okhttp3.internal.c.a(java.lang.String, java.lang.Object[]):java.lang.String
      okhttp3.internal.c.a(okio.e, java.nio.charset.Charset):java.nio.charset.Charset
      okhttp3.internal.c.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      okhttp3.internal.c.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
      okhttp3.internal.c.a(java.lang.Object, java.lang.Object):boolean
      okhttp3.internal.c.a(java.lang.String[], java.lang.String):java.lang.String[]
      okhttp3.internal.c.a(okhttp3.HttpUrl, boolean):java.lang.String */
    private v f() {
        return new v.a().a(this.f7865h.a().a()).a("Host", okhttp3.internal.c.a(this.f7865h.a().a(), true)).a("Proxy-Connection", "Keep-Alive").a(a.HEADER_USER_AGENT, okhttp3.internal.d.a()).b();
    }

    public boolean a(okhttp3.a aVar) {
        return this.f7862d.size() < this.f7861c && aVar.equals(a().a()) && !this.f7859a;
    }

    public okhttp3.internal.b.c a(t tVar, f fVar) {
        if (this.m != null) {
            return new okhttp3.internal.http2.d(tVar, fVar, this.m);
        }
        this.j.setSoTimeout(tVar.b());
        this.n.a().a((long) tVar.b(), TimeUnit.MILLISECONDS);
        this.o.a().a((long) tVar.c(), TimeUnit.MILLISECONDS);
        return new okhttp3.internal.c.a(tVar, fVar, this.n, this.o);
    }

    public z a() {
        return this.f7865h;
    }

    public void b() {
        okhttp3.internal.c.a(this.i);
    }

    public Socket c() {
        return this.j;
    }

    public boolean a(boolean z) {
        int soTimeout;
        if (this.j.isClosed() || this.j.isInputShutdown() || this.j.isOutputShutdown()) {
            return false;
        }
        if (this.m != null) {
            if (this.m.d()) {
                return false;
            }
            return true;
        } else if (!z) {
            return true;
        } else {
            try {
                soTimeout = this.j.getSoTimeout();
                this.j.setSoTimeout(1);
                if (this.n.f()) {
                    this.j.setSoTimeout(soTimeout);
                    return false;
                }
                this.j.setSoTimeout(soTimeout);
                return true;
            } catch (SocketTimeoutException e2) {
                return true;
            } catch (IOException e3) {
                return false;
            } catch (Throwable th) {
                this.j.setSoTimeout(soTimeout);
                throw th;
            }
        }
    }

    public void a(g gVar) {
        gVar.a(ErrorCode.REFUSED_STREAM);
    }

    public void a(e eVar) {
        synchronized (this.f7864g) {
            this.f7861c = eVar.a();
        }
    }

    public p d() {
        return this.k;
    }

    public boolean e() {
        return this.m != null;
    }

    public String toString() {
        return "Connection{" + this.f7865h.a().a().f() + ":" + this.f7865h.a().a().g() + ", proxy=" + this.f7865h.b() + " hostAddress=" + this.f7865h.c() + " cipherSuite=" + (this.k != null ? this.k.a() : "none") + " protocol=" + this.l + '}';
    }
}
