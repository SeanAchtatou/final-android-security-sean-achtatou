package okhttp3.internal.a;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.b.d;
import okhttp3.internal.b.e;
import okhttp3.q;
import okhttp3.v;
import okhttp3.x;

/* compiled from: CacheStrategy */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public final v f7766a;

    /* renamed from: b  reason: collision with root package name */
    public final x f7767b;

    c(v vVar, x xVar) {
        this.f7766a = vVar;
        this.f7767b = xVar;
    }

    public static boolean a(x xVar, v vVar) {
        switch (xVar.b()) {
            case 200:
            case 203:
            case 204:
            case 300:
            case 301:
            case 308:
            case 404:
            case 405:
            case 410:
            case 414:
            case 501:
                break;
            default:
                return false;
            case 302:
            case 307:
                if (xVar.a("Expires") == null && xVar.i().c() == -1 && !xVar.i().e() && !xVar.i().d()) {
                    return false;
                }
        }
        return !xVar.i().b() && !vVar.f().b();
    }

    /* compiled from: CacheStrategy */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        final long f7768a;

        /* renamed from: b  reason: collision with root package name */
        final v f7769b;

        /* renamed from: c  reason: collision with root package name */
        final x f7770c;

        /* renamed from: d  reason: collision with root package name */
        private Date f7771d;

        /* renamed from: e  reason: collision with root package name */
        private String f7772e;

        /* renamed from: f  reason: collision with root package name */
        private Date f7773f;

        /* renamed from: g  reason: collision with root package name */
        private String f7774g;

        /* renamed from: h  reason: collision with root package name */
        private Date f7775h;
        private long i;
        private long j;
        private String k;
        private int l = -1;

        public a(long j2, v vVar, x xVar) {
            this.f7768a = j2;
            this.f7769b = vVar;
            this.f7770c = xVar;
            if (xVar != null) {
                this.i = xVar.j();
                this.j = xVar.k();
                q f2 = xVar.f();
                int a2 = f2.a();
                for (int i2 = 0; i2 < a2; i2++) {
                    String a3 = f2.a(i2);
                    String b2 = f2.b(i2);
                    if ("Date".equalsIgnoreCase(a3)) {
                        this.f7771d = d.a(b2);
                        this.f7772e = b2;
                    } else if ("Expires".equalsIgnoreCase(a3)) {
                        this.f7775h = d.a(b2);
                    } else if ("Last-Modified".equalsIgnoreCase(a3)) {
                        this.f7773f = d.a(b2);
                        this.f7774g = b2;
                    } else if ("ETag".equalsIgnoreCase(a3)) {
                        this.k = b2;
                    } else if ("Age".equalsIgnoreCase(a3)) {
                        this.l = e.b(b2, -1);
                    }
                }
            }
        }

        public c a() {
            c b2 = b();
            if (b2.f7766a == null || !this.f7769b.f().i()) {
                return b2;
            }
            return new c(null, null);
        }

        private c b() {
            long j2;
            String str;
            String str2;
            long j3 = 0;
            if (this.f7770c == null) {
                return new c(this.f7769b, null);
            }
            if (this.f7769b.g() && this.f7770c.e() == null) {
                return new c(this.f7769b, null);
            }
            if (!c.a(this.f7770c, this.f7769b)) {
                return new c(this.f7769b, null);
            }
            okhttp3.d f2 = this.f7769b.f();
            if (f2.a() || a(this.f7769b)) {
                return new c(this.f7769b, null);
            }
            long d2 = d();
            long c2 = c();
            if (f2.c() != -1) {
                c2 = Math.min(c2, TimeUnit.SECONDS.toMillis((long) f2.c()));
            }
            if (f2.h() != -1) {
                j2 = TimeUnit.SECONDS.toMillis((long) f2.h());
            } else {
                j2 = 0;
            }
            okhttp3.d i2 = this.f7770c.i();
            if (!i2.f() && f2.g() != -1) {
                j3 = TimeUnit.SECONDS.toMillis((long) f2.g());
            }
            if (i2.a() || d2 + j2 >= j3 + c2) {
                if (this.k != null) {
                    str = "If-None-Match";
                    str2 = this.k;
                } else if (this.f7773f != null) {
                    str = "If-Modified-Since";
                    str2 = this.f7774g;
                } else if (this.f7771d == null) {
                    return new c(this.f7769b, null);
                } else {
                    str = "If-Modified-Since";
                    str2 = this.f7772e;
                }
                q.a b2 = this.f7769b.c().b();
                okhttp3.internal.a.f7759a.a(b2, str, str2);
                return new c(this.f7769b.e().a(b2.a()).b(), this.f7770c);
            }
            x.a h2 = this.f7770c.h();
            if (j2 + d2 >= c2) {
                h2.a("Warning", "110 HttpURLConnection \"Response is stale\"");
            }
            if (d2 > 86400000 && e()) {
                h2.a("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
            }
            return new c(null, h2.a());
        }

        private long c() {
            okhttp3.d i2 = this.f7770c.i();
            if (i2.c() != -1) {
                return TimeUnit.SECONDS.toMillis((long) i2.c());
            }
            if (this.f7775h != null) {
                long time = this.f7775h.getTime() - (this.f7771d != null ? this.f7771d.getTime() : this.j);
                if (time <= 0) {
                    time = 0;
                }
                return time;
            } else if (this.f7773f == null || this.f7770c.a().a().k() != null) {
                return 0;
            } else {
                long time2 = (this.f7771d != null ? this.f7771d.getTime() : this.i) - this.f7773f.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [int, long]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        private long d() {
            long j2 = 0;
            if (this.f7771d != null) {
                j2 = Math.max(0L, this.j - this.f7771d.getTime());
            }
            if (this.l != -1) {
                j2 = Math.max(j2, TimeUnit.SECONDS.toMillis((long) this.l));
            }
            return j2 + (this.j - this.i) + (this.f7768a - this.j);
        }

        private boolean e() {
            return this.f7770c.i().c() == -1 && this.f7775h == null;
        }

        private static boolean a(v vVar) {
            return (vVar.a("If-Modified-Since") == null && vVar.a("If-None-Match") == null) ? false : true;
        }
    }
}
