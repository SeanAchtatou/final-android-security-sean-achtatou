package okhttp3.internal.a;

import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;

/* compiled from: DiskLruCache */
public final class d implements Closeable, Flushable {

    /* renamed from: a  reason: collision with root package name */
    static final Pattern f7776a = Pattern.compile("[a-z0-9_-]{1,120}");
    static final /* synthetic */ boolean j = (!d.class.desiredAssertionStatus());

    /* renamed from: b  reason: collision with root package name */
    final okhttp3.internal.d.a f7777b;

    /* renamed from: c  reason: collision with root package name */
    final int f7778c;

    /* renamed from: d  reason: collision with root package name */
    okio.d f7779d;

    /* renamed from: e  reason: collision with root package name */
    final LinkedHashMap<String, b> f7780e;

    /* renamed from: f  reason: collision with root package name */
    int f7781f;

    /* renamed from: g  reason: collision with root package name */
    boolean f7782g;

    /* renamed from: h  reason: collision with root package name */
    boolean f7783h;
    boolean i;
    private long k;
    private long l;
    private long m;
    private final Executor n;
    private final Runnable o;

    /* access modifiers changed from: package-private */
    public synchronized void a(a aVar, boolean z) {
        synchronized (this) {
            b bVar = aVar.f7784a;
            if (bVar.f7793f != aVar) {
                throw new IllegalStateException();
            }
            if (z) {
                if (!bVar.f7792e) {
                    int i2 = 0;
                    while (true) {
                        if (i2 < this.f7778c) {
                            if (!aVar.f7785b[i2]) {
                                aVar.b();
                                throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                            } else if (!this.f7777b.b(bVar.f7791d[i2])) {
                                aVar.b();
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
            }
            for (int i3 = 0; i3 < this.f7778c; i3++) {
                File file = bVar.f7791d[i3];
                if (!z) {
                    this.f7777b.a(file);
                } else if (this.f7777b.b(file)) {
                    File file2 = bVar.f7790c[i3];
                    this.f7777b.a(file, file2);
                    long j2 = bVar.f7789b[i3];
                    long c2 = this.f7777b.c(file2);
                    bVar.f7789b[i3] = c2;
                    this.l = (this.l - j2) + c2;
                }
            }
            this.f7781f++;
            bVar.f7793f = null;
            if (bVar.f7792e || z) {
                bVar.f7792e = true;
                this.f7779d.b("CLEAN").i(32);
                this.f7779d.b(bVar.f7788a);
                bVar.a(this.f7779d);
                this.f7779d.i(10);
                if (z) {
                    long j3 = this.m;
                    this.m = 1 + j3;
                    bVar.f7794g = j3;
                }
            } else {
                this.f7780e.remove(bVar.f7788a);
                this.f7779d.b("REMOVE").i(32);
                this.f7779d.b(bVar.f7788a);
                this.f7779d.i(10);
            }
            this.f7779d.flush();
            if (this.l > this.k || a()) {
                this.n.execute(this.o);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.f7781f >= 2000 && this.f7781f >= this.f7780e.size();
    }

    /* access modifiers changed from: package-private */
    public boolean a(b bVar) {
        if (bVar.f7793f != null) {
            bVar.f7793f.a();
        }
        for (int i2 = 0; i2 < this.f7778c; i2++) {
            this.f7777b.a(bVar.f7790c[i2]);
            this.l -= bVar.f7789b[i2];
            bVar.f7789b[i2] = 0;
        }
        this.f7781f++;
        this.f7779d.b("REMOVE").i(32).b(bVar.f7788a).i(10);
        this.f7780e.remove(bVar.f7788a);
        if (!a()) {
            return true;
        }
        this.n.execute(this.o);
        return true;
    }

    public synchronized boolean b() {
        return this.f7783h;
    }

    private synchronized void d() {
        if (b()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    public synchronized void flush() {
        if (this.f7782g) {
            d();
            c();
            this.f7779d.flush();
        }
    }

    public synchronized void close() {
        if (!this.f7782g || this.f7783h) {
            this.f7783h = true;
        } else {
            for (b bVar : (b[]) this.f7780e.values().toArray(new b[this.f7780e.size()])) {
                if (bVar.f7793f != null) {
                    bVar.f7793f.b();
                }
            }
            c();
            this.f7779d.close();
            this.f7779d = null;
            this.f7783h = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        while (this.l > this.k) {
            a(this.f7780e.values().iterator().next());
        }
        this.i = false;
    }

    /* compiled from: DiskLruCache */
    public final class a {

        /* renamed from: a  reason: collision with root package name */
        final b f7784a;

        /* renamed from: b  reason: collision with root package name */
        final boolean[] f7785b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ d f7786c;

        /* renamed from: d  reason: collision with root package name */
        private boolean f7787d;

        /* access modifiers changed from: package-private */
        public void a() {
            if (this.f7784a.f7793f == this) {
                for (int i = 0; i < this.f7786c.f7778c; i++) {
                    try {
                        this.f7786c.f7777b.a(this.f7784a.f7791d[i]);
                    } catch (IOException e2) {
                    }
                }
                this.f7784a.f7793f = null;
            }
        }

        public void b() {
            synchronized (this.f7786c) {
                if (this.f7787d) {
                    throw new IllegalStateException();
                }
                if (this.f7784a.f7793f == this) {
                    this.f7786c.a(this, false);
                }
                this.f7787d = true;
            }
        }
    }

    /* compiled from: DiskLruCache */
    private final class b {

        /* renamed from: a  reason: collision with root package name */
        final String f7788a;

        /* renamed from: b  reason: collision with root package name */
        final long[] f7789b;

        /* renamed from: c  reason: collision with root package name */
        final File[] f7790c;

        /* renamed from: d  reason: collision with root package name */
        final File[] f7791d;

        /* renamed from: e  reason: collision with root package name */
        boolean f7792e;

        /* renamed from: f  reason: collision with root package name */
        a f7793f;

        /* renamed from: g  reason: collision with root package name */
        long f7794g;

        /* access modifiers changed from: package-private */
        public void a(okio.d dVar) {
            for (long k : this.f7789b) {
                dVar.i(32).k(k);
            }
        }
    }
}
