package okhttp3.internal.a;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Protocol;
import okhttp3.internal.a.c;
import okhttp3.internal.b.e;
import okhttp3.internal.b.f;
import okhttp3.internal.b.h;
import okhttp3.q;
import okhttp3.r;
import okhttp3.v;
import okhttp3.x;
import okhttp3.y;
import okio.d;
import okio.k;
import okio.p;
import okio.q;

/* compiled from: CacheInterceptor */
public final class a implements r {

    /* renamed from: a  reason: collision with root package name */
    final e f7760a;

    public a(e eVar) {
        this.f7760a = eVar;
    }

    public x a(r.a aVar) {
        x xVar = null;
        x a2 = this.f7760a != null ? this.f7760a.a(aVar.a()) : xVar;
        c a3 = new c.a(System.currentTimeMillis(), aVar.a(), a2).a();
        v vVar = a3.f7766a;
        x xVar2 = a3.f7767b;
        if (this.f7760a != null) {
            this.f7760a.a(a3);
        }
        if (a2 != null && xVar2 == null) {
            okhttp3.internal.c.a(a2.g());
        }
        if (vVar == null && xVar2 == null) {
            return new x.a().a(aVar.a()).a(Protocol.HTTP_1_1).a(504).a("Unsatisfiable Request (only-if-cached)").a(okhttp3.internal.c.f7821c).a(-1L).b(System.currentTimeMillis()).a();
        }
        if (vVar == null) {
            return xVar2.h().b(a(xVar2)).a();
        }
        try {
            xVar = aVar.a(vVar);
            if (xVar2 != null) {
                if (xVar.b() == 304) {
                    x a4 = xVar2.h().a(a(xVar2.f(), xVar.f())).a(xVar.j()).b(xVar.k()).b(a(xVar2)).a(a(xVar)).a();
                    xVar.g().close();
                    this.f7760a.a();
                    this.f7760a.a(xVar2, a4);
                    return a4;
                }
                okhttp3.internal.c.a(xVar2.g());
            }
            x a5 = xVar.h().b(a(xVar2)).a(a(xVar)).a();
            if (e.b(a5)) {
                return a(a(a5, xVar.a(), this.f7760a), a5);
            }
            return a5;
        } finally {
            if (xVar == null && a2 != null) {
                okhttp3.internal.c.a(a2.g());
            }
        }
    }

    private static x a(x xVar) {
        if (xVar == null || xVar.g() == null) {
            return xVar;
        }
        return xVar.h().a((y) null).a();
    }

    private b a(x xVar, v vVar, e eVar) {
        if (eVar == null) {
            return null;
        }
        if (c.a(xVar, vVar)) {
            return eVar.a(xVar);
        }
        if (!f.a(vVar.b())) {
            return null;
        }
        try {
            eVar.b(vVar);
            return null;
        } catch (IOException e2) {
            return null;
        }
    }

    private x a(final b bVar, x xVar) {
        p a2;
        if (bVar == null || (a2 = bVar.a()) == null) {
            return xVar;
        }
        final okio.e c2 = xVar.g().c();
        final d a3 = k.a(a2);
        return xVar.h().a(new h(xVar.f(), k.a(new q() {

            /* renamed from: a  reason: collision with root package name */
            boolean f7761a;

            public long a(okio.c cVar, long j) {
                try {
                    long a2 = c2.a(cVar, j);
                    if (a2 == -1) {
                        if (!this.f7761a) {
                            this.f7761a = true;
                            a3.close();
                        }
                        return -1;
                    }
                    cVar.a(a3.c(), cVar.b() - a2, a2);
                    a3.u();
                    return a2;
                } catch (IOException e2) {
                    if (!this.f7761a) {
                        this.f7761a = true;
                        bVar.b();
                    }
                    throw e2;
                }
            }

            public okio.r a() {
                return c2.a();
            }

            public void close() {
                if (!this.f7761a && !okhttp3.internal.c.a(this, 100, TimeUnit.MILLISECONDS)) {
                    this.f7761a = true;
                    bVar.b();
                }
                c2.close();
            }
        }))).a();
    }

    private static okhttp3.q a(okhttp3.q qVar, okhttp3.q qVar2) {
        q.a aVar = new q.a();
        int a2 = qVar.a();
        for (int i = 0; i < a2; i++) {
            String a3 = qVar.a(i);
            String b2 = qVar.b(i);
            if ((!"Warning".equalsIgnoreCase(a3) || !b2.startsWith("1")) && (!a(a3) || qVar2.a(a3) == null)) {
                okhttp3.internal.a.f7759a.a(aVar, a3, b2);
            }
        }
        int a4 = qVar2.a();
        for (int i2 = 0; i2 < a4; i2++) {
            String a5 = qVar2.a(i2);
            if (!"Content-Length".equalsIgnoreCase(a5) && a(a5)) {
                okhttp3.internal.a.f7759a.a(aVar, a5, qVar2.b(i2));
            }
        }
        return aVar.a();
    }

    static boolean a(String str) {
        if ("Connection".equalsIgnoreCase(str) || "Keep-Alive".equalsIgnoreCase(str) || "Proxy-Authenticate".equalsIgnoreCase(str) || "Proxy-Authorization".equalsIgnoreCase(str) || "TE".equalsIgnoreCase(str) || "Trailers".equalsIgnoreCase(str) || "Transfer-Encoding".equalsIgnoreCase(str) || "Upgrade".equalsIgnoreCase(str)) {
            return false;
        }
        return true;
    }
}
