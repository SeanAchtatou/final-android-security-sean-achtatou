package okhttp3.internal.f;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.x500.X500Principal;

/* compiled from: TrustRootIndex */
public abstract class e {
    public abstract X509Certificate a(X509Certificate x509Certificate);

    public static e a(X509TrustManager x509TrustManager) {
        try {
            Method declaredMethod = x509TrustManager.getClass().getDeclaredMethod("findTrustAnchorByIssuerAndSignature", X509Certificate.class);
            declaredMethod.setAccessible(true);
            return new a(x509TrustManager, declaredMethod);
        } catch (NoSuchMethodException e2) {
            return a(x509TrustManager.getAcceptedIssuers());
        }
    }

    public static e a(X509Certificate... x509CertificateArr) {
        return new b(x509CertificateArr);
    }

    /* compiled from: TrustRootIndex */
    static final class a extends e {

        /* renamed from: a  reason: collision with root package name */
        private final X509TrustManager f7920a;

        /* renamed from: b  reason: collision with root package name */
        private final Method f7921b;

        a(X509TrustManager x509TrustManager, Method method) {
            this.f7921b = method;
            this.f7920a = x509TrustManager;
        }

        public X509Certificate a(X509Certificate x509Certificate) {
            try {
                TrustAnchor trustAnchor = (TrustAnchor) this.f7921b.invoke(this.f7920a, x509Certificate);
                if (trustAnchor != null) {
                    return trustAnchor.getTrustedCert();
                }
                return null;
            } catch (IllegalAccessException e2) {
                throw new AssertionError();
            } catch (InvocationTargetException e3) {
                return null;
            }
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (!this.f7920a.equals(aVar.f7920a) || !this.f7921b.equals(aVar.f7921b)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.f7920a.hashCode() + (this.f7921b.hashCode() * 31);
        }
    }

    /* compiled from: TrustRootIndex */
    static final class b extends e {

        /* renamed from: a  reason: collision with root package name */
        private final Map<X500Principal, Set<X509Certificate>> f7922a = new LinkedHashMap();

        public b(X509Certificate... x509CertificateArr) {
            for (X509Certificate x509Certificate : x509CertificateArr) {
                X500Principal subjectX500Principal = x509Certificate.getSubjectX500Principal();
                Set set = this.f7922a.get(subjectX500Principal);
                if (set == null) {
                    set = new LinkedHashSet(1);
                    this.f7922a.put(subjectX500Principal, set);
                }
                set.add(x509Certificate);
            }
        }

        public X509Certificate a(X509Certificate x509Certificate) {
            Set<X509Certificate> set = this.f7922a.get(x509Certificate.getIssuerX500Principal());
            if (set == null) {
                return null;
            }
            for (X509Certificate x509Certificate2 : set) {
                try {
                    x509Certificate.verify(x509Certificate2.getPublicKey());
                    return x509Certificate2;
                } catch (Exception e2) {
                }
            }
            return null;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof b) || !((b) obj).f7922a.equals(this.f7922a)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.f7922a.hashCode();
        }
    }
}
