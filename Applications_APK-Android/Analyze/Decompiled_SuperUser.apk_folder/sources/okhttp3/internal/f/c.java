package okhttp3.internal.f;

import javax.security.auth.x500.X500Principal;

/* compiled from: DistinguishedNameParser */
final class c {

    /* renamed from: a  reason: collision with root package name */
    private final String f7912a;

    /* renamed from: b  reason: collision with root package name */
    private final int f7913b = this.f7912a.length();

    /* renamed from: c  reason: collision with root package name */
    private int f7914c;

    /* renamed from: d  reason: collision with root package name */
    private int f7915d;

    /* renamed from: e  reason: collision with root package name */
    private int f7916e;

    /* renamed from: f  reason: collision with root package name */
    private int f7917f;

    /* renamed from: g  reason: collision with root package name */
    private char[] f7918g;

    public c(X500Principal x500Principal) {
        this.f7912a = x500Principal.getName("RFC2253");
    }

    private String a() {
        while (this.f7914c < this.f7913b && this.f7918g[this.f7914c] == ' ') {
            this.f7914c++;
        }
        if (this.f7914c == this.f7913b) {
            return null;
        }
        this.f7915d = this.f7914c;
        this.f7914c++;
        while (this.f7914c < this.f7913b && this.f7918g[this.f7914c] != '=' && this.f7918g[this.f7914c] != ' ') {
            this.f7914c++;
        }
        if (this.f7914c >= this.f7913b) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f7912a);
        }
        this.f7916e = this.f7914c;
        if (this.f7918g[this.f7914c] == ' ') {
            while (this.f7914c < this.f7913b && this.f7918g[this.f7914c] != '=' && this.f7918g[this.f7914c] == ' ') {
                this.f7914c++;
            }
            if (this.f7918g[this.f7914c] != '=' || this.f7914c == this.f7913b) {
                throw new IllegalStateException("Unexpected end of DN: " + this.f7912a);
            }
        }
        this.f7914c++;
        while (this.f7914c < this.f7913b && this.f7918g[this.f7914c] == ' ') {
            this.f7914c++;
        }
        if (this.f7916e - this.f7915d > 4 && this.f7918g[this.f7915d + 3] == '.' && ((this.f7918g[this.f7915d] == 'O' || this.f7918g[this.f7915d] == 'o') && ((this.f7918g[this.f7915d + 1] == 'I' || this.f7918g[this.f7915d + 1] == 'i') && (this.f7918g[this.f7915d + 2] == 'D' || this.f7918g[this.f7915d + 2] == 'd')))) {
            this.f7915d += 4;
        }
        return new String(this.f7918g, this.f7915d, this.f7916e - this.f7915d);
    }

    private String b() {
        this.f7914c++;
        this.f7915d = this.f7914c;
        this.f7916e = this.f7915d;
        while (this.f7914c != this.f7913b) {
            if (this.f7918g[this.f7914c] == '\"') {
                this.f7914c++;
                while (this.f7914c < this.f7913b && this.f7918g[this.f7914c] == ' ') {
                    this.f7914c++;
                }
                return new String(this.f7918g, this.f7915d, this.f7916e - this.f7915d);
            }
            if (this.f7918g[this.f7914c] == '\\') {
                this.f7918g[this.f7916e] = e();
            } else {
                this.f7918g[this.f7916e] = this.f7918g[this.f7914c];
            }
            this.f7914c++;
            this.f7916e++;
        }
        throw new IllegalStateException("Unexpected end of DN: " + this.f7912a);
    }

    private String c() {
        if (this.f7914c + 4 >= this.f7913b) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f7912a);
        }
        this.f7915d = this.f7914c;
        this.f7914c++;
        while (true) {
            if (this.f7914c == this.f7913b || this.f7918g[this.f7914c] == '+' || this.f7918g[this.f7914c] == ',' || this.f7918g[this.f7914c] == ';') {
                this.f7916e = this.f7914c;
            } else if (this.f7918g[this.f7914c] == ' ') {
                this.f7916e = this.f7914c;
                this.f7914c++;
                while (this.f7914c < this.f7913b && this.f7918g[this.f7914c] == ' ') {
                    this.f7914c++;
                }
            } else {
                if (this.f7918g[this.f7914c] >= 'A' && this.f7918g[this.f7914c] <= 'F') {
                    char[] cArr = this.f7918g;
                    int i = this.f7914c;
                    cArr[i] = (char) (cArr[i] + ' ');
                }
                this.f7914c++;
            }
        }
        this.f7916e = this.f7914c;
        int i2 = this.f7916e - this.f7915d;
        if (i2 < 5 || (i2 & 1) == 0) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f7912a);
        }
        byte[] bArr = new byte[(i2 / 2)];
        int i3 = this.f7915d + 1;
        for (int i4 = 0; i4 < bArr.length; i4++) {
            bArr[i4] = (byte) a(i3);
            i3 += 2;
        }
        return new String(this.f7918g, this.f7915d, i2);
    }

    private String d() {
        this.f7915d = this.f7914c;
        this.f7916e = this.f7914c;
        while (this.f7914c < this.f7913b) {
            switch (this.f7918g[this.f7914c]) {
                case ' ':
                    this.f7917f = this.f7916e;
                    this.f7914c++;
                    char[] cArr = this.f7918g;
                    int i = this.f7916e;
                    this.f7916e = i + 1;
                    cArr[i] = ' ';
                    while (this.f7914c < this.f7913b && this.f7918g[this.f7914c] == ' ') {
                        char[] cArr2 = this.f7918g;
                        int i2 = this.f7916e;
                        this.f7916e = i2 + 1;
                        cArr2[i2] = ' ';
                        this.f7914c++;
                    }
                    if (this.f7914c != this.f7913b && this.f7918g[this.f7914c] != ',' && this.f7918g[this.f7914c] != '+' && this.f7918g[this.f7914c] != ';') {
                        break;
                    } else {
                        return new String(this.f7918g, this.f7915d, this.f7917f - this.f7915d);
                    }
                    break;
                case '+':
                case ',':
                case ';':
                    return new String(this.f7918g, this.f7915d, this.f7916e - this.f7915d);
                case '\\':
                    char[] cArr3 = this.f7918g;
                    int i3 = this.f7916e;
                    this.f7916e = i3 + 1;
                    cArr3[i3] = e();
                    this.f7914c++;
                    break;
                default:
                    char[] cArr4 = this.f7918g;
                    int i4 = this.f7916e;
                    this.f7916e = i4 + 1;
                    cArr4[i4] = this.f7918g[this.f7914c];
                    this.f7914c++;
                    break;
            }
        }
        return new String(this.f7918g, this.f7915d, this.f7916e - this.f7915d);
    }

    private char e() {
        this.f7914c++;
        if (this.f7914c == this.f7913b) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f7912a);
        }
        switch (this.f7918g[this.f7914c]) {
            case ' ':
            case '\"':
            case '#':
            case '%':
            case '*':
            case '+':
            case ',':
            case ';':
            case '<':
            case '=':
            case '>':
            case '\\':
            case '_':
                return this.f7918g[this.f7914c];
            default:
                return f();
        }
    }

    private char f() {
        int i;
        int i2;
        int a2 = a(this.f7914c);
        this.f7914c++;
        if (a2 < 128) {
            return (char) a2;
        }
        if (a2 < 192 || a2 > 247) {
            return '?';
        }
        if (a2 <= 223) {
            i = 1;
            i2 = a2 & 31;
        } else if (a2 <= 239) {
            i = 2;
            i2 = a2 & 15;
        } else {
            i = 3;
            i2 = a2 & 7;
        }
        int i3 = i2;
        for (int i4 = 0; i4 < i; i4++) {
            this.f7914c++;
            if (this.f7914c == this.f7913b || this.f7918g[this.f7914c] != '\\') {
                return '?';
            }
            this.f7914c++;
            int a3 = a(this.f7914c);
            this.f7914c++;
            if ((a3 & 192) != 128) {
                return '?';
            }
            i3 = (i3 << 6) + (a3 & 63);
        }
        return (char) i3;
    }

    private int a(int i) {
        int i2;
        int i3;
        if (i + 1 >= this.f7913b) {
            throw new IllegalStateException("Malformed DN: " + this.f7912a);
        }
        char c2 = this.f7918g[i];
        if (c2 >= '0' && c2 <= '9') {
            i2 = c2 - '0';
        } else if (c2 >= 'a' && c2 <= 'f') {
            i2 = c2 - 'W';
        } else if (c2 < 'A' || c2 > 'F') {
            throw new IllegalStateException("Malformed DN: " + this.f7912a);
        } else {
            i2 = c2 - '7';
        }
        char c3 = this.f7918g[i + 1];
        if (c3 >= '0' && c3 <= '9') {
            i3 = c3 - '0';
        } else if (c3 >= 'a' && c3 <= 'f') {
            i3 = c3 - 'W';
        } else if (c3 < 'A' || c3 > 'F') {
            throw new IllegalStateException("Malformed DN: " + this.f7912a);
        } else {
            i3 = c3 - '7';
        }
        return (i2 << 4) + i3;
    }

    public String a(String str) {
        this.f7914c = 0;
        this.f7915d = 0;
        this.f7916e = 0;
        this.f7917f = 0;
        this.f7918g = this.f7912a.toCharArray();
        String a2 = a();
        if (a2 == null) {
            return null;
        }
        do {
            String str2 = "";
            if (this.f7914c == this.f7913b) {
                return null;
            }
            switch (this.f7918g[this.f7914c]) {
                case '\"':
                    str2 = b();
                    break;
                case '#':
                    str2 = c();
                    break;
                case '+':
                case ',':
                case ';':
                    break;
                default:
                    str2 = d();
                    break;
            }
            if (str.equalsIgnoreCase(a2)) {
                return str2;
            }
            if (this.f7914c >= this.f7913b) {
                return null;
            }
            if (this.f7918g[this.f7914c] == ',' || this.f7918g[this.f7914c] == ';' || this.f7918g[this.f7914c] == '+') {
                this.f7914c++;
                a2 = a();
            } else {
                throw new IllegalStateException("Malformed DN: " + this.f7912a);
            }
        } while (a2 != null);
        throw new IllegalStateException("Malformed DN: " + this.f7912a);
    }
}
