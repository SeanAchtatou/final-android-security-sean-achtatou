package okhttp3;

import java.io.Closeable;
import okhttp3.q;

/* compiled from: Response */
public final class x implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    final v f8160a;

    /* renamed from: b  reason: collision with root package name */
    final Protocol f8161b;

    /* renamed from: c  reason: collision with root package name */
    final int f8162c;

    /* renamed from: d  reason: collision with root package name */
    final String f8163d;

    /* renamed from: e  reason: collision with root package name */
    final p f8164e;

    /* renamed from: f  reason: collision with root package name */
    final q f8165f;

    /* renamed from: g  reason: collision with root package name */
    final y f8166g;

    /* renamed from: h  reason: collision with root package name */
    final x f8167h;
    final x i;
    final x j;
    final long k;
    final long l;
    private volatile d m;

    x(a aVar) {
        this.f8160a = aVar.f8168a;
        this.f8161b = aVar.f8169b;
        this.f8162c = aVar.f8170c;
        this.f8163d = aVar.f8171d;
        this.f8164e = aVar.f8172e;
        this.f8165f = aVar.f8173f.a();
        this.f8166g = aVar.f8174g;
        this.f8167h = aVar.f8175h;
        this.i = aVar.i;
        this.j = aVar.j;
        this.k = aVar.k;
        this.l = aVar.l;
    }

    public v a() {
        return this.f8160a;
    }

    public int b() {
        return this.f8162c;
    }

    public boolean c() {
        return this.f8162c >= 200 && this.f8162c < 300;
    }

    public String d() {
        return this.f8163d;
    }

    public p e() {
        return this.f8164e;
    }

    public String a(String str) {
        return a(str, null);
    }

    public String a(String str, String str2) {
        String a2 = this.f8165f.a(str);
        return a2 != null ? a2 : str2;
    }

    public q f() {
        return this.f8165f;
    }

    public y g() {
        return this.f8166g;
    }

    public a h() {
        return new a(this);
    }

    public d i() {
        d dVar = this.m;
        if (dVar != null) {
            return dVar;
        }
        d a2 = d.a(this.f8165f);
        this.m = a2;
        return a2;
    }

    public long j() {
        return this.k;
    }

    public long k() {
        return this.l;
    }

    public void close() {
        this.f8166g.close();
    }

    public String toString() {
        return "Response{protocol=" + this.f8161b + ", code=" + this.f8162c + ", message=" + this.f8163d + ", url=" + this.f8160a.a() + '}';
    }

    /* compiled from: Response */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        v f8168a;

        /* renamed from: b  reason: collision with root package name */
        Protocol f8169b;

        /* renamed from: c  reason: collision with root package name */
        int f8170c;

        /* renamed from: d  reason: collision with root package name */
        String f8171d;

        /* renamed from: e  reason: collision with root package name */
        p f8172e;

        /* renamed from: f  reason: collision with root package name */
        q.a f8173f;

        /* renamed from: g  reason: collision with root package name */
        y f8174g;

        /* renamed from: h  reason: collision with root package name */
        x f8175h;
        x i;
        x j;
        long k;
        long l;

        public a() {
            this.f8170c = -1;
            this.f8173f = new q.a();
        }

        a(x xVar) {
            this.f8170c = -1;
            this.f8168a = xVar.f8160a;
            this.f8169b = xVar.f8161b;
            this.f8170c = xVar.f8162c;
            this.f8171d = xVar.f8163d;
            this.f8172e = xVar.f8164e;
            this.f8173f = xVar.f8165f.b();
            this.f8174g = xVar.f8166g;
            this.f8175h = xVar.f8167h;
            this.i = xVar.i;
            this.j = xVar.j;
            this.k = xVar.k;
            this.l = xVar.l;
        }

        public a a(v vVar) {
            this.f8168a = vVar;
            return this;
        }

        public a a(Protocol protocol) {
            this.f8169b = protocol;
            return this;
        }

        public a a(int i2) {
            this.f8170c = i2;
            return this;
        }

        public a a(String str) {
            this.f8171d = str;
            return this;
        }

        public a a(p pVar) {
            this.f8172e = pVar;
            return this;
        }

        public a a(String str, String str2) {
            this.f8173f.a(str, str2);
            return this;
        }

        public a a(q qVar) {
            this.f8173f = qVar.b();
            return this;
        }

        public a a(y yVar) {
            this.f8174g = yVar;
            return this;
        }

        public a a(x xVar) {
            if (xVar != null) {
                a("networkResponse", xVar);
            }
            this.f8175h = xVar;
            return this;
        }

        public a b(x xVar) {
            if (xVar != null) {
                a("cacheResponse", xVar);
            }
            this.i = xVar;
            return this;
        }

        private void a(String str, x xVar) {
            if (xVar.f8166g != null) {
                throw new IllegalArgumentException(str + ".body != null");
            } else if (xVar.f8167h != null) {
                throw new IllegalArgumentException(str + ".networkResponse != null");
            } else if (xVar.i != null) {
                throw new IllegalArgumentException(str + ".cacheResponse != null");
            } else if (xVar.j != null) {
                throw new IllegalArgumentException(str + ".priorResponse != null");
            }
        }

        public a c(x xVar) {
            if (xVar != null) {
                d(xVar);
            }
            this.j = xVar;
            return this;
        }

        private void d(x xVar) {
            if (xVar.f8166g != null) {
                throw new IllegalArgumentException("priorResponse.body != null");
            }
        }

        public a a(long j2) {
            this.k = j2;
            return this;
        }

        public a b(long j2) {
            this.l = j2;
            return this;
        }

        public x a() {
            if (this.f8168a == null) {
                throw new IllegalStateException("request == null");
            } else if (this.f8169b == null) {
                throw new IllegalStateException("protocol == null");
            } else if (this.f8170c >= 0) {
                return new x(this);
            } else {
                throw new IllegalStateException("code < 0: " + this.f8170c);
            }
        }
    }
}
