package okhttp3;

import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import okhttp3.internal.c;

/* compiled from: Handshake */
public final class p {

    /* renamed from: a  reason: collision with root package name */
    private final TlsVersion f8110a;

    /* renamed from: b  reason: collision with root package name */
    private final h f8111b;

    /* renamed from: c  reason: collision with root package name */
    private final List<Certificate> f8112c;

    /* renamed from: d  reason: collision with root package name */
    private final List<Certificate> f8113d;

    private p(TlsVersion tlsVersion, h hVar, List<Certificate> list, List<Certificate> list2) {
        this.f8110a = tlsVersion;
        this.f8111b = hVar;
        this.f8112c = list;
        this.f8113d = list2;
    }

    public static p a(SSLSession sSLSession) {
        Certificate[] certificateArr;
        List emptyList;
        List emptyList2;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        }
        h a2 = h.a(cipherSuite);
        String protocol = sSLSession.getProtocol();
        if (protocol == null) {
            throw new IllegalStateException("tlsVersion == null");
        }
        TlsVersion a3 = TlsVersion.a(protocol);
        try {
            certificateArr = sSLSession.getPeerCertificates();
        } catch (SSLPeerUnverifiedException e2) {
            certificateArr = null;
        }
        if (certificateArr != null) {
            emptyList = c.a(certificateArr);
        } else {
            emptyList = Collections.emptyList();
        }
        Certificate[] localCertificates = sSLSession.getLocalCertificates();
        if (localCertificates != null) {
            emptyList2 = c.a(localCertificates);
        } else {
            emptyList2 = Collections.emptyList();
        }
        return new p(a3, a2, emptyList, emptyList2);
    }

    public h a() {
        return this.f8111b;
    }

    public List<Certificate> b() {
        return this.f8112c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof p)) {
            return false;
        }
        p pVar = (p) obj;
        if (!c.a(this.f8111b, pVar.f8111b) || !this.f8111b.equals(pVar.f8111b) || !this.f8112c.equals(pVar.f8112c) || !this.f8113d.equals(pVar.f8113d)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((((this.f8110a != null ? this.f8110a.hashCode() : 0) + 527) * 31) + this.f8111b.hashCode()) * 31) + this.f8112c.hashCode()) * 31) + this.f8113d.hashCode();
    }
}
