package okhttp3;

import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.c;
import okhttp3.internal.connection.d;
import okhttp3.internal.connection.f;
import okhttp3.internal.e.e;

/* compiled from: ConnectionPool */
public final class j {

    /* renamed from: c  reason: collision with root package name */
    static final /* synthetic */ boolean f8072c = (!j.class.desiredAssertionStatus());

    /* renamed from: d  reason: collision with root package name */
    private static final Executor f8073d = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), c.a("OkHttp ConnectionPool", true));

    /* renamed from: a  reason: collision with root package name */
    final d f8074a;

    /* renamed from: b  reason: collision with root package name */
    boolean f8075b;

    /* renamed from: e  reason: collision with root package name */
    private final int f8076e;

    /* renamed from: f  reason: collision with root package name */
    private final long f8077f;

    /* renamed from: g  reason: collision with root package name */
    private final Runnable f8078g;

    /* renamed from: h  reason: collision with root package name */
    private final Deque<okhttp3.internal.connection.c> f8079h;

    public j() {
        this(5, 5, TimeUnit.MINUTES);
    }

    public j(int i, long j, TimeUnit timeUnit) {
        this.f8078g = new Runnable() {
            public void run() {
                while (true) {
                    long a2 = j.this.a(System.nanoTime());
                    if (a2 != -1) {
                        if (a2 > 0) {
                            long j = a2 / 1000000;
                            long j2 = a2 - (j * 1000000);
                            synchronized (j.this) {
                                try {
                                    j.this.wait(j, (int) j2);
                                } catch (InterruptedException e2) {
                                }
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        };
        this.f8079h = new ArrayDeque();
        this.f8074a = new d();
        this.f8076e = i;
        this.f8077f = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    /* access modifiers changed from: package-private */
    public okhttp3.internal.connection.c a(a aVar, f fVar) {
        if (f8072c || Thread.holdsLock(this)) {
            for (okhttp3.internal.connection.c next : this.f8079h) {
                if (next.a(aVar)) {
                    fVar.a(next);
                    return next;
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public Socket b(a aVar, f fVar) {
        if (f8072c || Thread.holdsLock(this)) {
            for (okhttp3.internal.connection.c next : this.f8079h) {
                if (next.a(aVar) && next.e() && next != fVar.b()) {
                    return fVar.b(next);
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void a(okhttp3.internal.connection.c cVar) {
        if (f8072c || Thread.holdsLock(this)) {
            if (!this.f8075b) {
                this.f8075b = true;
                f8073d.execute(this.f8078g);
            }
            this.f8079h.add(cVar);
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public boolean b(okhttp3.internal.connection.c cVar) {
        if (!f8072c && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (cVar.f7859a || this.f8076e == 0) {
            this.f8079h.remove(cVar);
            return true;
        } else {
            notifyAll();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public long a(long j) {
        okhttp3.internal.connection.c cVar;
        long j2;
        okhttp3.internal.connection.c cVar2 = null;
        long j3 = Long.MIN_VALUE;
        synchronized (this) {
            int i = 0;
            int i2 = 0;
            for (okhttp3.internal.connection.c next : this.f8079h) {
                if (a(next, j) > 0) {
                    i2++;
                } else {
                    int i3 = i + 1;
                    long j4 = j - next.f7863e;
                    if (j4 > j3) {
                        long j5 = j4;
                        cVar = next;
                        j2 = j5;
                    } else {
                        cVar = cVar2;
                        j2 = j3;
                    }
                    j3 = j2;
                    cVar2 = cVar;
                    i = i3;
                }
            }
            if (j3 >= this.f8077f || i > this.f8076e) {
                this.f8079h.remove(cVar2);
                c.a(cVar2.c());
                return 0;
            } else if (i > 0) {
                long j6 = this.f8077f - j3;
                return j6;
            } else if (i2 > 0) {
                long j7 = this.f8077f;
                return j7;
            } else {
                this.f8075b = false;
                return -1;
            }
        }
    }

    private int a(okhttp3.internal.connection.c cVar, long j) {
        List<Reference<f>> list = cVar.f7862d;
        int i = 0;
        while (i < list.size()) {
            Reference reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                e.b().a("A connection to " + cVar.a().a().a() + " was leaked. Did you forget to close a response body?", ((f.a) reference).f7883a);
                list.remove(i);
                cVar.f7859a = true;
                if (list.isEmpty()) {
                    cVar.f7863e = j - this.f8077f;
                    return 0;
                }
            }
        }
        return list.size();
    }
}
