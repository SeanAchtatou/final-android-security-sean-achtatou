package okhttp3;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import okhttp3.internal.c;

public final class HttpUrl {

    /* renamed from: d  reason: collision with root package name */
    private static final char[] f7682d = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: a  reason: collision with root package name */
    final String f7683a;

    /* renamed from: b  reason: collision with root package name */
    final String f7684b;

    /* renamed from: c  reason: collision with root package name */
    final int f7685c;

    /* renamed from: e  reason: collision with root package name */
    private final String f7686e;

    /* renamed from: f  reason: collision with root package name */
    private final String f7687f;

    /* renamed from: g  reason: collision with root package name */
    private final List<String> f7688g;

    /* renamed from: h  reason: collision with root package name */
    private final List<String> f7689h;
    private final String i;
    private final String j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.HttpUrl.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      okhttp3.HttpUrl.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String>
      okhttp3.HttpUrl.a(java.lang.StringBuilder, java.util.List<java.lang.String>):void
      okhttp3.HttpUrl.a(java.lang.String, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.HttpUrl.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String>
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      okhttp3.HttpUrl.a(java.lang.String, boolean):java.lang.String
      okhttp3.HttpUrl.a(java.lang.StringBuilder, java.util.List<java.lang.String>):void
      okhttp3.HttpUrl.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String> */
    HttpUrl(Builder builder) {
        String str = null;
        this.f7683a = builder.f7690a;
        this.f7686e = a(builder.f7691b, false);
        this.f7687f = a(builder.f7692c, false);
        this.f7684b = builder.f7693d;
        this.f7685c = builder.a();
        this.f7688g = a(builder.f7695f, false);
        this.f7689h = builder.f7696g != null ? a(builder.f7696g, true) : null;
        this.i = builder.f7697h != null ? a(builder.f7697h, false) : str;
        this.j = builder.toString();
    }

    public URI a() {
        String builder = n().b().toString();
        try {
            return new URI(builder);
        } catch (URISyntaxException e2) {
            try {
                return URI.create(builder.replaceAll("[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]", ""));
            } catch (Exception e3) {
                throw new RuntimeException(e2);
            }
        }
    }

    public String b() {
        return this.f7683a;
    }

    public boolean c() {
        return this.f7683a.equals("https");
    }

    public String d() {
        if (this.f7686e.isEmpty()) {
            return "";
        }
        int length = this.f7683a.length() + 3;
        return this.j.substring(length, c.a(this.j, length, this.j.length(), ":@"));
    }

    public String e() {
        if (this.f7687f.isEmpty()) {
            return "";
        }
        int indexOf = this.j.indexOf(64);
        return this.j.substring(this.j.indexOf(58, this.f7683a.length() + 3) + 1, indexOf);
    }

    public String f() {
        return this.f7684b;
    }

    public int g() {
        return this.f7685c;
    }

    public static int a(String str) {
        if (str.equals("http")) {
            return 80;
        }
        if (str.equals("https")) {
            return 443;
        }
        return -1;
    }

    public String h() {
        int indexOf = this.j.indexOf(47, this.f7683a.length() + 3);
        return this.j.substring(indexOf, c.a(this.j, indexOf, this.j.length(), "?#"));
    }

    static void a(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            sb.append('/');
            sb.append(list.get(i2));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.c.a(java.lang.String, int, int, char):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      okhttp3.internal.c.a(java.lang.String, int, int, java.lang.String):int
      okhttp3.internal.c.a(java.lang.String, int, int, char):int */
    public List<String> i() {
        int indexOf = this.j.indexOf(47, this.f7683a.length() + 3);
        int a2 = c.a(this.j, indexOf, this.j.length(), "?#");
        ArrayList arrayList = new ArrayList();
        while (indexOf < a2) {
            int i2 = indexOf + 1;
            indexOf = c.a(this.j, i2, a2, '/');
            arrayList.add(this.j.substring(i2, indexOf));
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.c.a(java.lang.String, int, int, char):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      okhttp3.internal.c.a(java.lang.String, int, int, java.lang.String):int
      okhttp3.internal.c.a(java.lang.String, int, int, char):int */
    public String j() {
        if (this.f7689h == null) {
            return null;
        }
        int indexOf = this.j.indexOf(63) + 1;
        return this.j.substring(indexOf, c.a(this.j, indexOf + 1, this.j.length(), '#'));
    }

    static void b(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2 += 2) {
            String str = list.get(i2);
            String str2 = list.get(i2 + 1);
            if (i2 > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }

    static List<String> b(String str) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 <= str.length()) {
            int indexOf = str.indexOf(38, i2);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i2);
            if (indexOf2 == -1 || indexOf2 > indexOf) {
                arrayList.add(str.substring(i2, indexOf));
                arrayList.add(null);
            } else {
                arrayList.add(str.substring(i2, indexOf2));
                arrayList.add(str.substring(indexOf2 + 1, indexOf));
            }
            i2 = indexOf + 1;
        }
        return arrayList;
    }

    public String k() {
        if (this.f7689h == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        b(sb, this.f7689h);
        return sb.toString();
    }

    public String l() {
        if (this.i == null) {
            return null;
        }
        return this.j.substring(this.j.indexOf(35) + 1);
    }

    public String m() {
        return d("/...").b("").c("").c().toString();
    }

    public HttpUrl c(String str) {
        Builder d2 = d(str);
        if (d2 != null) {
            return d2.c();
        }
        return null;
    }

    public Builder n() {
        Builder builder = new Builder();
        builder.f7690a = this.f7683a;
        builder.f7691b = d();
        builder.f7692c = e();
        builder.f7693d = this.f7684b;
        builder.f7694e = this.f7685c != a(this.f7683a) ? this.f7685c : -1;
        builder.f7695f.clear();
        builder.f7695f.addAll(i());
        builder.e(j());
        builder.f7697h = l();
        return builder;
    }

    public Builder d(String str) {
        Builder builder = new Builder();
        if (builder.a(this, str) == Builder.ParseResult.SUCCESS) {
            return builder;
        }
        return null;
    }

    public static HttpUrl e(String str) {
        Builder builder = new Builder();
        if (builder.a(null, str) == Builder.ParseResult.SUCCESS) {
            return builder.c();
        }
        return null;
    }

    public boolean equals(Object obj) {
        return (obj instanceof HttpUrl) && ((HttpUrl) obj).j.equals(this.j);
    }

    public int hashCode() {
        return this.j.hashCode();
    }

    public String toString() {
        return this.j;
    }

    public static final class Builder {

        /* renamed from: a  reason: collision with root package name */
        String f7690a;

        /* renamed from: b  reason: collision with root package name */
        String f7691b = "";

        /* renamed from: c  reason: collision with root package name */
        String f7692c = "";

        /* renamed from: d  reason: collision with root package name */
        String f7693d;

        /* renamed from: e  reason: collision with root package name */
        int f7694e = -1;

        /* renamed from: f  reason: collision with root package name */
        final List<String> f7695f = new ArrayList();

        /* renamed from: g  reason: collision with root package name */
        List<String> f7696g;

        /* renamed from: h  reason: collision with root package name */
        String f7697h;

        enum ParseResult {
            SUCCESS,
            MISSING_SCHEME,
            UNSUPPORTED_SCHEME,
            INVALID_PORT,
            INVALID_HOST
        }

        public Builder() {
            this.f7695f.add("");
        }

        public Builder a(String str) {
            if (str == null) {
                throw new NullPointerException("scheme == null");
            }
            if (str.equalsIgnoreCase("http")) {
                this.f7690a = "http";
            } else if (str.equalsIgnoreCase("https")) {
                this.f7690a = "https";
            } else {
                throw new IllegalArgumentException("unexpected scheme: " + str);
            }
            return this;
        }

        public Builder b(String str) {
            if (str == null) {
                throw new NullPointerException("username == null");
            }
            this.f7691b = HttpUrl.a(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
            return this;
        }

        public Builder c(String str) {
            if (str == null) {
                throw new NullPointerException("password == null");
            }
            this.f7692c = HttpUrl.a(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
            return this;
        }

        public Builder d(String str) {
            if (str == null) {
                throw new NullPointerException("host == null");
            }
            String e2 = e(str, 0, str.length());
            if (e2 == null) {
                throw new IllegalArgumentException("unexpected host: " + str);
            }
            this.f7693d = e2;
            return this;
        }

        public Builder a(int i) {
            if (i <= 0 || i > 65535) {
                throw new IllegalArgumentException("unexpected port: " + i);
            }
            this.f7694e = i;
            return this;
        }

        /* access modifiers changed from: package-private */
        public int a() {
            return this.f7694e != -1 ? this.f7694e : HttpUrl.a(this.f7690a);
        }

        public Builder e(String str) {
            this.f7696g = str != null ? HttpUrl.b(HttpUrl.a(str, " \"'<>#", true, false, true, true)) : null;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder b() {
            int size = this.f7695f.size();
            for (int i = 0; i < size; i++) {
                this.f7695f.set(i, HttpUrl.a(this.f7695f.get(i), "[]", true, true, false, true));
            }
            if (this.f7696g != null) {
                int size2 = this.f7696g.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    String str = this.f7696g.get(i2);
                    if (str != null) {
                        this.f7696g.set(i2, HttpUrl.a(str, "\\^`{|}", true, true, true, true));
                    }
                }
            }
            if (this.f7697h != null) {
                this.f7697h = HttpUrl.a(this.f7697h, " \"#<>\\^`{|}", true, true, false, false);
            }
            return this;
        }

        public HttpUrl c() {
            if (this.f7690a == null) {
                throw new IllegalStateException("scheme == null");
            } else if (this.f7693d != null) {
                return new HttpUrl(this);
            } else {
                throw new IllegalStateException("host == null");
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f7690a);
            sb.append("://");
            if (!this.f7691b.isEmpty() || !this.f7692c.isEmpty()) {
                sb.append(this.f7691b);
                if (!this.f7692c.isEmpty()) {
                    sb.append(':');
                    sb.append(this.f7692c);
                }
                sb.append('@');
            }
            if (this.f7693d.indexOf(58) != -1) {
                sb.append('[');
                sb.append(this.f7693d);
                sb.append(']');
            } else {
                sb.append(this.f7693d);
            }
            int a2 = a();
            if (a2 != HttpUrl.a(this.f7690a)) {
                sb.append(':');
                sb.append(a2);
            }
            HttpUrl.a(sb, this.f7695f);
            if (this.f7696g != null) {
                sb.append('?');
                HttpUrl.b(sb, this.f7696g);
            }
            if (this.f7697h != null) {
                sb.append('#');
                sb.append(this.f7697h);
            }
            return sb.toString();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: okhttp3.internal.c.a(java.lang.String, int, int, char):int
         arg types: [java.lang.String, int, int, int]
         candidates:
          okhttp3.internal.c.a(java.lang.String, int, int, java.lang.String):int
          okhttp3.internal.c.a(java.lang.String, int, int, char):int */
        /* access modifiers changed from: package-private */
        public ParseResult a(HttpUrl httpUrl, String str) {
            int i;
            int a2 = c.a(str, 0, str.length());
            int b2 = c.b(str, a2, str.length());
            if (b(str, a2, b2) != -1) {
                if (str.regionMatches(true, a2, "https:", 0, 6)) {
                    this.f7690a = "https";
                    a2 += "https:".length();
                } else if (!str.regionMatches(true, a2, "http:", 0, 5)) {
                    return ParseResult.UNSUPPORTED_SCHEME;
                } else {
                    this.f7690a = "http";
                    a2 += "http:".length();
                }
            } else if (httpUrl == null) {
                return ParseResult.MISSING_SCHEME;
            } else {
                this.f7690a = httpUrl.f7683a;
            }
            boolean z = false;
            boolean z2 = false;
            int c2 = c(str, a2, b2);
            if (c2 >= 2 || httpUrl == null || !httpUrl.f7683a.equals(this.f7690a)) {
                int i2 = a2 + c2;
                while (true) {
                    boolean z3 = z2;
                    boolean z4 = z;
                    int i3 = i2;
                    int a3 = c.a(str, i3, b2, "@/\\?#");
                    switch (a3 != b2 ? str.charAt(a3) : 65535) {
                        case 65535:
                        case '#':
                        case '/':
                        case '?':
                        case '\\':
                            int d2 = d(str, i3, a3);
                            if (d2 + 1 < a3) {
                                this.f7693d = e(str, i3, d2);
                                this.f7694e = g(str, d2 + 1, a3);
                                if (this.f7694e == -1) {
                                    return ParseResult.INVALID_PORT;
                                }
                            } else {
                                this.f7693d = e(str, i3, d2);
                                this.f7694e = HttpUrl.a(this.f7690a);
                            }
                            if (this.f7693d != null) {
                                a2 = a3;
                                break;
                            } else {
                                return ParseResult.INVALID_HOST;
                            }
                        case '@':
                            if (!z3) {
                                int a4 = c.a(str, i3, a3, ':');
                                String a5 = HttpUrl.a(str, i3, a4, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                                if (z4) {
                                    a5 = this.f7691b + "%40" + a5;
                                }
                                this.f7691b = a5;
                                if (a4 != a3) {
                                    z3 = true;
                                    this.f7692c = HttpUrl.a(str, a4 + 1, a3, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                                }
                                z4 = true;
                            } else {
                                this.f7692c += "%40" + HttpUrl.a(str, i3, a3, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                            }
                            i2 = a3 + 1;
                            z2 = z3;
                            break;
                        default:
                            z2 = z3;
                            i2 = i3;
                            break;
                    }
                    z = z4;
                }
            } else {
                this.f7691b = httpUrl.d();
                this.f7692c = httpUrl.e();
                this.f7693d = httpUrl.f7684b;
                this.f7694e = httpUrl.f7685c;
                this.f7695f.clear();
                this.f7695f.addAll(httpUrl.i());
                if (a2 == b2 || str.charAt(a2) == '#') {
                    e(httpUrl.j());
                }
            }
            int a6 = c.a(str, a2, b2, "?#");
            a(str, a2, a6);
            if (a6 >= b2 || str.charAt(a6) != '?') {
                i = a6;
            } else {
                i = c.a(str, a6, b2, '#');
                this.f7696g = HttpUrl.b(HttpUrl.a(str, a6 + 1, i, " \"'<>#", true, false, true, true));
            }
            if (i < b2 && str.charAt(i) == '#') {
                this.f7697h = HttpUrl.a(str, i + 1, b2, "", true, false, false, false);
            }
            return ParseResult.SUCCESS;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: okhttp3.HttpUrl.Builder.a(java.lang.String, int, int, boolean, boolean):void
         arg types: [java.lang.String, int, int, boolean, int]
         candidates:
          okhttp3.HttpUrl.Builder.a(java.lang.String, int, int, byte[], int):boolean
          okhttp3.HttpUrl.Builder.a(java.lang.String, int, int, boolean, boolean):void */
        private void a(String str, int i, int i2) {
            if (i != i2) {
                char charAt = str.charAt(i);
                if (charAt == '/' || charAt == '\\') {
                    this.f7695f.clear();
                    this.f7695f.add("");
                    i++;
                } else {
                    this.f7695f.set(this.f7695f.size() - 1, "");
                }
                int i3 = i;
                while (i3 < i2) {
                    int a2 = c.a(str, i3, i2, "/\\");
                    boolean z = a2 < i2;
                    a(str, i3, a2, z, true);
                    if (z) {
                        a2++;
                    }
                    i3 = a2;
                }
            }
        }

        private void a(String str, int i, int i2, boolean z, boolean z2) {
            String a2 = HttpUrl.a(str, i, i2, " \"<>^`{}|/\\?#", z2, false, false, true);
            if (!f(a2)) {
                if (g(a2)) {
                    d();
                    return;
                }
                if (this.f7695f.get(this.f7695f.size() - 1).isEmpty()) {
                    this.f7695f.set(this.f7695f.size() - 1, a2);
                } else {
                    this.f7695f.add(a2);
                }
                if (z) {
                    this.f7695f.add("");
                }
            }
        }

        private boolean f(String str) {
            return str.equals(".") || str.equalsIgnoreCase("%2e");
        }

        private boolean g(String str) {
            return str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e") || str.equalsIgnoreCase("%2e%2e");
        }

        private void d() {
            if (!this.f7695f.remove(this.f7695f.size() - 1).isEmpty() || this.f7695f.isEmpty()) {
                this.f7695f.add("");
            } else {
                this.f7695f.set(this.f7695f.size() - 1, "");
            }
        }

        private static int b(String str, int i, int i2) {
            if (i2 - i < 2) {
                return -1;
            }
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')) {
                return -1;
            }
            int i3 = i + 1;
            while (i3 < i2) {
                char charAt2 = str.charAt(i3);
                if ((charAt2 >= 'a' && charAt2 <= 'z') || ((charAt2 >= 'A' && charAt2 <= 'Z') || ((charAt2 >= '0' && charAt2 <= '9') || charAt2 == '+' || charAt2 == '-' || charAt2 == '.'))) {
                    i3++;
                } else if (charAt2 == ':') {
                    return i3;
                } else {
                    return -1;
                }
            }
            return -1;
        }

        private static int c(String str, int i, int i2) {
            int i3 = 0;
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != '\\' && charAt != '/') {
                    break;
                }
                i3++;
                i++;
            }
            return i3;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        private static int d(String str, int i, int i2) {
            int i3 = i;
            while (i3 < i2) {
                switch (str.charAt(i3)) {
                    case ':':
                        return i3;
                    case '[':
                        break;
                    default:
                        i3++;
                }
                do {
                    i3++;
                    if (i3 < i2) {
                    }
                    i3++;
                } while (str.charAt(i3) != ']');
                i3++;
            }
            return i2;
        }

        private static String e(String str, int i, int i2) {
            InetAddress f2;
            String a2 = HttpUrl.a(str, i, i2, false);
            if (!a2.contains(":")) {
                return c.a(a2);
            }
            if (!a2.startsWith("[") || !a2.endsWith("]")) {
                f2 = f(a2, 0, a2.length());
            } else {
                f2 = f(a2, 1, a2.length() - 1);
            }
            if (f2 == null) {
                return null;
            }
            byte[] address = f2.getAddress();
            if (address.length == 16) {
                return a(address);
            }
            throw new AssertionError();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
            return null;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static java.net.InetAddress f(java.lang.String r12, int r13, int r14) {
            /*
                r11 = 1
                r7 = -1
                r3 = 0
                r5 = 0
                r0 = 16
                byte[] r8 = new byte[r0]
                r0 = r13
                r4 = r7
                r1 = r7
                r2 = r5
            L_0x000c:
                if (r0 >= r14) goto L_0x002b
                int r6 = r8.length
                if (r2 != r6) goto L_0x0013
                r0 = r3
            L_0x0012:
                return r0
            L_0x0013:
                int r6 = r0 + 2
                if (r6 > r14) goto L_0x0032
                java.lang.String r6 = "::"
                r9 = 2
                boolean r6 = r12.regionMatches(r0, r6, r5, r9)
                if (r6 == 0) goto L_0x0032
                if (r1 == r7) goto L_0x0024
                r0 = r3
                goto L_0x0012
            L_0x0024:
                int r0 = r0 + 2
                int r1 = r2 + 2
                if (r0 != r14) goto L_0x00a1
                r2 = r1
            L_0x002b:
                int r0 = r8.length
                if (r2 == r0) goto L_0x0094
                if (r1 != r7) goto L_0x0085
                r0 = r3
                goto L_0x0012
            L_0x0032:
                if (r2 == 0) goto L_0x003e
                java.lang.String r6 = ":"
                boolean r6 = r12.regionMatches(r0, r6, r5, r11)
                if (r6 == 0) goto L_0x0055
                int r0 = r0 + 1
            L_0x003e:
                r4 = r5
                r6 = r0
            L_0x0040:
                if (r6 >= r14) goto L_0x004c
                char r9 = r12.charAt(r6)
                int r9 = okhttp3.HttpUrl.a(r9)
                if (r9 != r7) goto L_0x006c
            L_0x004c:
                int r9 = r6 - r0
                if (r9 == 0) goto L_0x0053
                r10 = 4
                if (r9 <= r10) goto L_0x0072
            L_0x0053:
                r0 = r3
                goto L_0x0012
            L_0x0055:
                java.lang.String r6 = "."
                boolean r0 = r12.regionMatches(r0, r6, r5, r11)
                if (r0 == 0) goto L_0x006a
                int r0 = r2 + -2
                boolean r0 = a(r12, r4, r14, r8, r0)
                if (r0 != 0) goto L_0x0067
                r0 = r3
                goto L_0x0012
            L_0x0067:
                int r2 = r2 + 2
                goto L_0x002b
            L_0x006a:
                r0 = r3
                goto L_0x0012
            L_0x006c:
                int r4 = r4 << 4
                int r4 = r4 + r9
                int r6 = r6 + 1
                goto L_0x0040
            L_0x0072:
                int r9 = r2 + 1
                int r10 = r4 >>> 8
                r10 = r10 & 255(0xff, float:3.57E-43)
                byte r10 = (byte) r10
                r8[r2] = r10
                int r2 = r9 + 1
                r4 = r4 & 255(0xff, float:3.57E-43)
                byte r4 = (byte) r4
                r8[r9] = r4
                r4 = r0
                r0 = r6
                goto L_0x000c
            L_0x0085:
                int r0 = r8.length
                int r3 = r2 - r1
                int r0 = r0 - r3
                int r3 = r2 - r1
                java.lang.System.arraycopy(r8, r1, r8, r0, r3)
                int r0 = r8.length
                int r0 = r0 - r2
                int r0 = r0 + r1
                java.util.Arrays.fill(r8, r1, r0, r5)
            L_0x0094:
                java.net.InetAddress r0 = java.net.InetAddress.getByAddress(r8)     // Catch:{ UnknownHostException -> 0x009a }
                goto L_0x0012
            L_0x009a:
                r0 = move-exception
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x00a1:
                r2 = r1
                goto L_0x003e
            */
            throw new UnsupportedOperationException("Method not decompiled: okhttp3.HttpUrl.Builder.f(java.lang.String, int, int):java.net.InetAddress");
        }

        private static boolean a(String str, int i, int i2, byte[] bArr, int i3) {
            int i4 = i;
            int i5 = i3;
            while (i4 < i2) {
                if (i5 == bArr.length) {
                    return false;
                }
                if (i5 != i3) {
                    if (str.charAt(i4) != '.') {
                        return false;
                    }
                    i4++;
                }
                int i6 = 0;
                int i7 = i4;
                while (i7 < i2) {
                    char charAt = str.charAt(i7);
                    if (charAt < '0' || charAt > '9') {
                        break;
                    } else if (i6 == 0 && i4 != i7) {
                        return false;
                    } else {
                        i6 = ((i6 * 10) + charAt) - 48;
                        if (i6 > 255) {
                            return false;
                        }
                        i7++;
                    }
                }
                if (i7 - i4 == 0) {
                    return false;
                }
                bArr[i5] = (byte) i6;
                i5++;
                i4 = i7;
            }
            if (i5 != i3 + 4) {
                return false;
            }
            return true;
        }

        private static String a(byte[] bArr) {
            int i = 0;
            int i2 = 0;
            int i3 = -1;
            int i4 = 0;
            while (i4 < bArr.length) {
                int i5 = i4;
                while (i5 < 16 && bArr[i5] == 0 && bArr[i5 + 1] == 0) {
                    i5 += 2;
                }
                int i6 = i5 - i4;
                if (i6 > i2) {
                    i2 = i6;
                    i3 = i4;
                }
                i4 = i5 + 2;
            }
            okio.c cVar = new okio.c();
            while (i < bArr.length) {
                if (i == i3) {
                    cVar.i(58);
                    i += i2;
                    if (i == 16) {
                        cVar.i(58);
                    }
                } else {
                    if (i > 0) {
                        cVar.i(58);
                    }
                    cVar.j((long) (((bArr[i] & 255) << 8) | (bArr[i + 1] & 255)));
                    i += 2;
                }
            }
            return cVar.o();
        }

        private static int g(String str, int i, int i2) {
            try {
                int parseInt = Integer.parseInt(HttpUrl.a(str, i, i2, "", false, false, false, true));
                if (parseInt <= 0 || parseInt > 65535) {
                    return -1;
                }
                return parseInt;
            } catch (NumberFormatException e2) {
                return -1;
            }
        }
    }

    static String a(String str, boolean z) {
        return a(str, 0, str.length(), z);
    }

    private List<String> a(List<String> list, boolean z) {
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i2 = 0; i2 < size; i2++) {
            String str = list.get(i2);
            arrayList.add(str != null ? a(str, z) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    static String a(String str, int i2, int i3, boolean z) {
        for (int i4 = i2; i4 < i3; i4++) {
            char charAt = str.charAt(i4);
            if (charAt == '%' || (charAt == '+' && z)) {
                okio.c cVar = new okio.c();
                cVar.a(str, i2, i4);
                a(cVar, str, i4, i3, z);
                return cVar.o();
            }
        }
        return str.substring(i2, i3);
    }

    static void a(okio.c cVar, String str, int i2, int i3, boolean z) {
        int i4 = i2;
        while (i4 < i3) {
            int codePointAt = str.codePointAt(i4);
            if (codePointAt != 37 || i4 + 2 >= i3) {
                if (codePointAt == 43 && z) {
                    cVar.i(32);
                }
                cVar.a(codePointAt);
            } else {
                int a2 = a(str.charAt(i4 + 1));
                int a3 = a(str.charAt(i4 + 2));
                if (!(a2 == -1 || a3 == -1)) {
                    cVar.i((a2 << 4) + a3);
                    i4 += 2;
                }
                cVar.a(codePointAt);
            }
            i4 += Character.charCount(codePointAt);
        }
    }

    static boolean a(String str, int i2, int i3) {
        return i2 + 2 < i3 && str.charAt(i2) == '%' && a(str.charAt(i2 + 1)) != -1 && a(str.charAt(i2 + 2)) != -1;
    }

    static int a(char c2) {
        if (c2 >= '0' && c2 <= '9') {
            return c2 - '0';
        }
        if (c2 >= 'a' && c2 <= 'f') {
            return (c2 - 'a') + 10;
        }
        if (c2 < 'A' || c2 > 'F') {
            return -1;
        }
        return (c2 - 'A') + 10;
    }

    static String a(String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        int i4 = i2;
        while (i4 < i3) {
            int codePointAt = str.codePointAt(i4);
            if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || ((codePointAt == 37 && (!z || (z2 && !a(str, i4, i3)))) || (codePointAt == 43 && z3)))) {
                okio.c cVar = new okio.c();
                cVar.a(str, i2, i4);
                a(cVar, str, i4, i3, str2, z, z2, z3, z4);
                return cVar.o();
            }
            i4 += Character.charCount(codePointAt);
        }
        return str.substring(i2, i3);
    }

    static void a(okio.c cVar, String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        okio.c cVar2 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt == 43 && z3) {
                    cVar.b(z ? "+" : "%2B");
                } else if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || (codePointAt == 37 && (!z || (z2 && !a(str, i2, i3)))))) {
                    if (cVar2 == null) {
                        cVar2 = new okio.c();
                    }
                    cVar2.a(codePointAt);
                    while (!cVar2.f()) {
                        byte h2 = cVar2.h() & 255;
                        cVar.i(37);
                        cVar.i((int) f7682d[(h2 >> 4) & 15]);
                        cVar.i((int) f7682d[h2 & 15]);
                    }
                } else {
                    cVar.a(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    static String a(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        return a(str, 0, str.length(), str2, z, z2, z3, z4);
    }
}
