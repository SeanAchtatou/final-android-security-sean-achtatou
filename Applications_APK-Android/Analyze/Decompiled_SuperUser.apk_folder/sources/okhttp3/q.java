package okhttp3;

import com.kingouser.com.util.ShellUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import okhttp3.internal.c;

/* compiled from: Headers */
public final class q {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f8114a;

    q(a aVar) {
        this.f8114a = (String[]) aVar.f8115a.toArray(new String[aVar.f8115a.size()]);
    }

    public String a(String str) {
        return a(this.f8114a, str);
    }

    public int a() {
        return this.f8114a.length / 2;
    }

    public String a(int i) {
        return this.f8114a[i * 2];
    }

    public String b(int i) {
        return this.f8114a[(i * 2) + 1];
    }

    public List<String> b(String str) {
        int a2 = a();
        ArrayList arrayList = null;
        for (int i = 0; i < a2; i++) {
            if (str.equalsIgnoreCase(a(i))) {
                if (arrayList == null) {
                    arrayList = new ArrayList(2);
                }
                arrayList.add(b(i));
            }
        }
        if (arrayList != null) {
            return Collections.unmodifiableList(arrayList);
        }
        return Collections.emptyList();
    }

    public a b() {
        a aVar = new a();
        Collections.addAll(aVar.f8115a, this.f8114a);
        return aVar;
    }

    public boolean equals(Object obj) {
        return (obj instanceof q) && Arrays.equals(((q) obj).f8114a, this.f8114a);
    }

    public int hashCode() {
        return Arrays.hashCode(this.f8114a);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int a2 = a();
        for (int i = 0; i < a2; i++) {
            sb.append(a(i)).append(": ").append(b(i)).append(ShellUtils.COMMAND_LINE_END);
        }
        return sb.toString();
    }

    public Map<String, List<String>> c() {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        int a2 = a();
        for (int i = 0; i < a2; i++) {
            String lowerCase = a(i).toLowerCase(Locale.US);
            Object obj = (List) treeMap.get(lowerCase);
            if (obj == null) {
                obj = new ArrayList(2);
                treeMap.put(lowerCase, obj);
            }
            obj.add(b(i));
        }
        return treeMap;
    }

    private static String a(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }

    /* compiled from: Headers */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        final List<String> f8115a = new ArrayList(20);

        /* access modifiers changed from: package-private */
        public a a(String str) {
            int indexOf = str.indexOf(":", 1);
            if (indexOf != -1) {
                return b(str.substring(0, indexOf), str.substring(indexOf + 1));
            }
            if (str.startsWith(":")) {
                return b("", str.substring(1));
            }
            return b("", str);
        }

        public a a(String str, String str2) {
            d(str, str2);
            return b(str, str2);
        }

        /* access modifiers changed from: package-private */
        public a b(String str, String str2) {
            this.f8115a.add(str);
            this.f8115a.add(str2.trim());
            return this;
        }

        public a b(String str) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f8115a.size()) {
                    return this;
                }
                if (str.equalsIgnoreCase(this.f8115a.get(i2))) {
                    this.f8115a.remove(i2);
                    this.f8115a.remove(i2);
                    i2 -= 2;
                }
                i = i2 + 2;
            }
        }

        public a c(String str, String str2) {
            d(str, str2);
            b(str);
            b(str, str2);
            return this;
        }

        private void d(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str.isEmpty()) {
                throw new IllegalArgumentException("name is empty");
            } else {
                int length = str.length();
                for (int i = 0; i < length; i++) {
                    char charAt = str.charAt(i);
                    if (charAt <= ' ' || charAt >= 127) {
                        throw new IllegalArgumentException(c.a("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i), str));
                    }
                }
                if (str2 == null) {
                    throw new NullPointerException("value == null");
                }
                int length2 = str2.length();
                int i2 = 0;
                while (i2 < length2) {
                    char charAt2 = str2.charAt(i2);
                    if ((charAt2 > 31 || charAt2 == 9) && charAt2 < 127) {
                        i2++;
                    } else {
                        throw new IllegalArgumentException(c.a("Unexpected char %#04x at %d in %s value: %s", Integer.valueOf(charAt2), Integer.valueOf(i2), str, str2));
                    }
                }
            }
        }

        public q a() {
            return new q(this);
        }
    }
}
