package okhttp3;

import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.HttpUrl;
import okhttp3.internal.c;

/* compiled from: Address */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    final HttpUrl f7717a;

    /* renamed from: b  reason: collision with root package name */
    final o f7718b;

    /* renamed from: c  reason: collision with root package name */
    final SocketFactory f7719c;

    /* renamed from: d  reason: collision with root package name */
    final b f7720d;

    /* renamed from: e  reason: collision with root package name */
    final List<Protocol> f7721e;

    /* renamed from: f  reason: collision with root package name */
    final List<k> f7722f;

    /* renamed from: g  reason: collision with root package name */
    final ProxySelector f7723g;

    /* renamed from: h  reason: collision with root package name */
    final Proxy f7724h;
    final SSLSocketFactory i;
    final HostnameVerifier j;
    final g k;

    public a(String str, int i2, o oVar, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, g gVar, b bVar, Proxy proxy, List<Protocol> list, List<k> list2, ProxySelector proxySelector) {
        this.f7717a = new HttpUrl.Builder().a(sSLSocketFactory != null ? "https" : "http").d(str).a(i2).c();
        if (oVar == null) {
            throw new NullPointerException("dns == null");
        }
        this.f7718b = oVar;
        if (socketFactory == null) {
            throw new NullPointerException("socketFactory == null");
        }
        this.f7719c = socketFactory;
        if (bVar == null) {
            throw new NullPointerException("proxyAuthenticator == null");
        }
        this.f7720d = bVar;
        if (list == null) {
            throw new NullPointerException("protocols == null");
        }
        this.f7721e = c.a(list);
        if (list2 == null) {
            throw new NullPointerException("connectionSpecs == null");
        }
        this.f7722f = c.a(list2);
        if (proxySelector == null) {
            throw new NullPointerException("proxySelector == null");
        }
        this.f7723g = proxySelector;
        this.f7724h = proxy;
        this.i = sSLSocketFactory;
        this.j = hostnameVerifier;
        this.k = gVar;
    }

    public HttpUrl a() {
        return this.f7717a;
    }

    public o b() {
        return this.f7718b;
    }

    public SocketFactory c() {
        return this.f7719c;
    }

    public b d() {
        return this.f7720d;
    }

    public List<Protocol> e() {
        return this.f7721e;
    }

    public List<k> f() {
        return this.f7722f;
    }

    public ProxySelector g() {
        return this.f7723g;
    }

    public Proxy h() {
        return this.f7724h;
    }

    public SSLSocketFactory i() {
        return this.i;
    }

    public HostnameVerifier j() {
        return this.j;
    }

    public g k() {
        return this.k;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        if (!this.f7717a.equals(aVar.f7717a) || !this.f7718b.equals(aVar.f7718b) || !this.f7720d.equals(aVar.f7720d) || !this.f7721e.equals(aVar.f7721e) || !this.f7722f.equals(aVar.f7722f) || !this.f7723g.equals(aVar.f7723g) || !c.a(this.f7724h, aVar.f7724h) || !c.a(this.i, aVar.i) || !c.a(this.j, aVar.j) || !c.a(this.k, aVar.k)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int hashCode = (((((((((((this.f7717a.hashCode() + 527) * 31) + this.f7718b.hashCode()) * 31) + this.f7720d.hashCode()) * 31) + this.f7721e.hashCode()) * 31) + this.f7722f.hashCode()) * 31) + this.f7723g.hashCode()) * 31;
        if (this.f7724h != null) {
            i2 = this.f7724h.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i2 + hashCode) * 31;
        if (this.i != null) {
            i3 = this.i.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i3 + i6) * 31;
        if (this.j != null) {
            i4 = this.j.hashCode();
        } else {
            i4 = 0;
        }
        int i8 = (i4 + i7) * 31;
        if (this.k != null) {
            i5 = this.k.hashCode();
        }
        return i8 + i5;
    }

    public String toString() {
        StringBuilder append = new StringBuilder().append("Address{").append(this.f7717a.f()).append(":").append(this.f7717a.g());
        if (this.f7724h != null) {
            append.append(", proxy=").append(this.f7724h);
        } else {
            append.append(", proxySelector=").append(this.f7723g);
        }
        append.append("}");
        return append.toString();
    }
}
