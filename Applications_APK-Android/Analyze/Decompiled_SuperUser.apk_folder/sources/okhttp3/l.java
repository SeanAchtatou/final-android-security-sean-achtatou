package okhttp3;

import com.duapps.ad.AdError;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.internal.b.d;
import okhttp3.internal.c;

/* compiled from: Cookie */
public final class l {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f8093a = Pattern.compile("(\\d{2,4})[^\\d]*");

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f8094b = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");

    /* renamed from: c  reason: collision with root package name */
    private static final Pattern f8095c = Pattern.compile("(\\d{1,2})[^\\d]*");

    /* renamed from: d  reason: collision with root package name */
    private static final Pattern f8096d = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");

    /* renamed from: e  reason: collision with root package name */
    private final String f8097e;

    /* renamed from: f  reason: collision with root package name */
    private final String f8098f;

    /* renamed from: g  reason: collision with root package name */
    private final long f8099g;

    /* renamed from: h  reason: collision with root package name */
    private final String f8100h;
    private final String i;
    private final boolean j;
    private final boolean k;
    private final boolean l;
    private final boolean m;

    private l(String str, String str2, long j2, String str3, String str4, boolean z, boolean z2, boolean z3, boolean z4) {
        this.f8097e = str;
        this.f8098f = str2;
        this.f8099g = j2;
        this.f8100h = str3;
        this.i = str4;
        this.j = z;
        this.k = z2;
        this.m = z3;
        this.l = z4;
    }

    public String a() {
        return this.f8097e;
    }

    public String b() {
        return this.f8098f;
    }

    private static boolean b(HttpUrl httpUrl, String str) {
        String f2 = httpUrl.f();
        if (f2.equals(str)) {
            return true;
        }
        if (!f2.endsWith(str) || f2.charAt((f2.length() - str.length()) - 1) != '.' || c.c(f2)) {
            return false;
        }
        return true;
    }

    public static l a(HttpUrl httpUrl, String str) {
        return a(System.currentTimeMillis(), httpUrl, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: okhttp3.internal.c.a(java.lang.String, int, int, char):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      okhttp3.internal.c.a(java.lang.String, int, int, java.lang.String):int
      okhttp3.internal.c.a(java.lang.String, int, int, char):int */
    static l a(long j2, HttpUrl httpUrl, String str) {
        long j3;
        String str2;
        String str3;
        int length = str.length();
        int a2 = c.a(str, 0, length, ';');
        int a3 = c.a(str, 0, a2, '=');
        if (a3 == a2) {
            return null;
        }
        String c2 = c.c(str, 0, a3);
        if (c2.isEmpty() || c.b(c2) != -1) {
            return null;
        }
        String c3 = c.c(str, a3 + 1, a2);
        if (c.b(c3) != -1) {
            return null;
        }
        long j4 = 253402300799999L;
        long j5 = -1;
        String str4 = null;
        String str5 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = true;
        boolean z4 = false;
        int i2 = a2 + 1;
        while (i2 < length) {
            int a4 = c.a(str, i2, length, ';');
            int a5 = c.a(str, i2, a4, '=');
            String c4 = c.c(str, i2, a5);
            String c5 = a5 < a4 ? c.c(str, a5 + 1, a4) : "";
            if (c4.equalsIgnoreCase("expires")) {
                try {
                    j4 = a(c5, 0, c5.length());
                    z4 = true;
                    str3 = str4;
                } catch (IllegalArgumentException e2) {
                    str3 = str4;
                }
            } else if (c4.equalsIgnoreCase("max-age")) {
                try {
                    j5 = a(c5);
                    z4 = true;
                    str3 = str4;
                } catch (NumberFormatException e3) {
                    str3 = str4;
                }
            } else if (c4.equalsIgnoreCase("domain")) {
                try {
                    str3 = b(c5);
                    z3 = false;
                } catch (IllegalArgumentException e4) {
                    str3 = str4;
                }
            } else if (c4.equalsIgnoreCase("path")) {
                str5 = c5;
                str3 = str4;
            } else if (c4.equalsIgnoreCase("secure")) {
                z = true;
                str3 = str4;
            } else if (c4.equalsIgnoreCase("httponly")) {
                z2 = true;
                str3 = str4;
            } else {
                str3 = str4;
            }
            String str6 = str3;
            i2 = a4 + 1;
            j4 = j4;
            str4 = str6;
        }
        if (j5 == Long.MIN_VALUE) {
            j3 = Long.MIN_VALUE;
        } else if (j5 != -1) {
            j3 = (j5 <= 9223372036854775L ? j5 * 1000 : Long.MAX_VALUE) + j2;
            if (j3 < j2 || j3 > 253402300799999L) {
                j3 = 253402300799999L;
            }
        } else {
            j3 = j4;
        }
        if (str4 == null) {
            str4 = httpUrl.f();
        } else if (!b(httpUrl, str4)) {
            return null;
        }
        if (str5 == null || !str5.startsWith("/")) {
            String h2 = httpUrl.h();
            int lastIndexOf = h2.lastIndexOf(47);
            str2 = lastIndexOf != 0 ? h2.substring(0, lastIndexOf) : "/";
        } else {
            str2 = str5;
        }
        return new l(c2, c3, j3, str4, str2, z, z2, z3, z4);
    }

    private static long a(String str, int i2, int i3) {
        int a2 = a(str, i2, i3, false);
        int i4 = -1;
        int i5 = -1;
        int i6 = -1;
        int i7 = -1;
        int i8 = -1;
        int i9 = -1;
        Matcher matcher = f8096d.matcher(str);
        while (a2 < i3) {
            int a3 = a(str, a2 + 1, i3, true);
            matcher.region(a2, a3);
            if (i4 == -1 && matcher.usePattern(f8096d).matches()) {
                i4 = Integer.parseInt(matcher.group(1));
                i5 = Integer.parseInt(matcher.group(2));
                i6 = Integer.parseInt(matcher.group(3));
            } else if (i7 == -1 && matcher.usePattern(f8095c).matches()) {
                i7 = Integer.parseInt(matcher.group(1));
            } else if (i8 == -1 && matcher.usePattern(f8094b).matches()) {
                i8 = f8094b.pattern().indexOf(matcher.group(1).toLowerCase(Locale.US)) / 4;
            } else if (i9 == -1 && matcher.usePattern(f8093a).matches()) {
                i9 = Integer.parseInt(matcher.group(1));
            }
            a2 = a(str, a3 + 1, i3, false);
        }
        if (i9 >= 70 && i9 <= 99) {
            i9 += 1900;
        }
        if (i9 >= 0 && i9 <= 69) {
            i9 += AdError.SERVER_ERROR_CODE;
        }
        if (i9 < 1601) {
            throw new IllegalArgumentException();
        } else if (i8 == -1) {
            throw new IllegalArgumentException();
        } else if (i7 < 1 || i7 > 31) {
            throw new IllegalArgumentException();
        } else if (i4 < 0 || i4 > 23) {
            throw new IllegalArgumentException();
        } else if (i5 < 0 || i5 > 59) {
            throw new IllegalArgumentException();
        } else if (i6 < 0 || i6 > 59) {
            throw new IllegalArgumentException();
        } else {
            GregorianCalendar gregorianCalendar = new GregorianCalendar(c.f7824f);
            gregorianCalendar.setLenient(false);
            gregorianCalendar.set(1, i9);
            gregorianCalendar.set(2, i8 - 1);
            gregorianCalendar.set(5, i7);
            gregorianCalendar.set(11, i4);
            gregorianCalendar.set(12, i5);
            gregorianCalendar.set(13, i6);
            gregorianCalendar.set(14, 0);
            return gregorianCalendar.getTimeInMillis();
        }
    }

    private static int a(String str, int i2, int i3, boolean z) {
        boolean z2;
        for (int i4 = i2; i4 < i3; i4++) {
            char charAt = str.charAt(i4);
            boolean z3 = (charAt < ' ' && charAt != 9) || charAt >= 127 || (charAt >= '0' && charAt <= '9') || ((charAt >= 'a' && charAt <= 'z') || ((charAt >= 'A' && charAt <= 'Z') || charAt == ':'));
            if (!z) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (z3 == z2) {
                return i4;
            }
        }
        return i3;
    }

    private static long a(String str) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong <= 0) {
                return Long.MIN_VALUE;
            }
            return parseLong;
        } catch (NumberFormatException e2) {
            if (!str.matches("-?\\d+")) {
                throw e2;
            } else if (!str.startsWith("-")) {
                return Long.MAX_VALUE;
            } else {
                return Long.MIN_VALUE;
            }
        }
    }

    private static String b(String str) {
        if (str.endsWith(".")) {
            throw new IllegalArgumentException();
        }
        if (str.startsWith(".")) {
            str = str.substring(1);
        }
        String a2 = c.a(str);
        if (a2 != null) {
            return a2;
        }
        throw new IllegalArgumentException();
    }

    public static List<l> a(HttpUrl httpUrl, q qVar) {
        ArrayList arrayList;
        List<String> b2 = qVar.b("Set-Cookie");
        ArrayList arrayList2 = null;
        int size = b2.size();
        for (int i2 = 0; i2 < size; i2++) {
            l a2 = a(httpUrl, b2.get(i2));
            if (a2 != null) {
                if (arrayList2 == null) {
                    arrayList = new ArrayList();
                } else {
                    arrayList = arrayList2;
                }
                arrayList.add(a2);
                arrayList2 = arrayList;
            }
        }
        if (arrayList2 != null) {
            return Collections.unmodifiableList(arrayList2);
        }
        return Collections.emptyList();
    }

    public String toString() {
        return a(false);
    }

    /* access modifiers changed from: package-private */
    public String a(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f8097e);
        sb.append('=');
        sb.append(this.f8098f);
        if (this.l) {
            if (this.f8099g == Long.MIN_VALUE) {
                sb.append("; max-age=0");
            } else {
                sb.append("; expires=").append(d.a(new Date(this.f8099g)));
            }
        }
        if (!this.m) {
            sb.append("; domain=");
            if (z) {
                sb.append(".");
            }
            sb.append(this.f8100h);
        }
        sb.append("; path=").append(this.i);
        if (this.j) {
            sb.append("; secure");
        }
        if (this.k) {
            sb.append("; httponly");
        }
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        if (lVar.f8097e.equals(this.f8097e) && lVar.f8098f.equals(this.f8098f) && lVar.f8100h.equals(this.f8100h) && lVar.i.equals(this.i) && lVar.f8099g == this.f8099g && lVar.j == this.j && lVar.k == this.k && lVar.l == this.l && lVar.m == this.m) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int hashCode = (((((((((this.f8097e.hashCode() + 527) * 31) + this.f8098f.hashCode()) * 31) + this.f8100h.hashCode()) * 31) + this.i.hashCode()) * 31) + ((int) (this.f8099g ^ (this.f8099g >>> 32)))) * 31;
        if (this.j) {
            i2 = 0;
        } else {
            i2 = 1;
        }
        int i6 = (i2 + hashCode) * 31;
        if (this.k) {
            i3 = 0;
        } else {
            i3 = 1;
        }
        int i7 = (i3 + i6) * 31;
        if (this.l) {
            i4 = 0;
        } else {
            i4 = 1;
        }
        int i8 = (i4 + i7) * 31;
        if (!this.m) {
            i5 = 1;
        }
        return i8 + i5;
    }
}
