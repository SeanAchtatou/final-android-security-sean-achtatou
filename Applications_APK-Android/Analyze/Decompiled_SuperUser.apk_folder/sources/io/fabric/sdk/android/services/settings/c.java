package io.fabric.sdk.android.services.settings;

/* compiled from: AppIconSettingsData */
class c {

    /* renamed from: a  reason: collision with root package name */
    public final String f7277a;

    /* renamed from: b  reason: collision with root package name */
    public final int f7278b;

    /* renamed from: c  reason: collision with root package name */
    public final int f7279c;

    public c(String str, int i, int i2) {
        this.f7277a = str;
        this.f7278b = i;
        this.f7279c = i2;
    }
}
