package io.fabric.sdk.android.services.common;

import android.os.SystemClock;
import android.util.Log;

/* compiled from: TimingMetric */
public class n {

    /* renamed from: a  reason: collision with root package name */
    private final String f7182a;

    /* renamed from: b  reason: collision with root package name */
    private final String f7183b;

    /* renamed from: c  reason: collision with root package name */
    private final boolean f7184c;

    /* renamed from: d  reason: collision with root package name */
    private long f7185d;

    /* renamed from: e  reason: collision with root package name */
    private long f7186e;

    public n(String str, String str2) {
        this.f7182a = str;
        this.f7183b = str2;
        this.f7184c = !Log.isLoggable(str2, 2);
    }

    public synchronized void a() {
        if (!this.f7184c) {
            this.f7185d = SystemClock.elapsedRealtime();
            this.f7186e = 0;
        }
    }

    public synchronized void b() {
        if (!this.f7184c) {
            if (this.f7186e == 0) {
                this.f7186e = SystemClock.elapsedRealtime() - this.f7185d;
                c();
            }
        }
    }

    private void c() {
        Log.v(this.f7183b, this.f7182a + ": " + this.f7186e + "ms");
    }
}
