package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;

/* compiled from: Dependency */
public interface a<T> {
    void addDependency(Object obj);

    boolean areDependenciesMet();

    Collection<T> getDependencies();
}
