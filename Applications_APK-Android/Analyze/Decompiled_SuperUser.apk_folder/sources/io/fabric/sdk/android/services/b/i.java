package io.fabric.sdk.android.services.b;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;

/* compiled from: TimeBasedFileRollOverRunnable */
public class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Context f7099a;

    /* renamed from: b  reason: collision with root package name */
    private final e f7100b;

    public i(Context context, e eVar) {
        this.f7099a = context;
        this.f7100b = eVar;
    }

    public void run() {
        try {
            CommonUtils.a(this.f7099a, "Performing time based file roll over.");
            if (!this.f7100b.rollFileOver()) {
                this.f7100b.cancelTimeBasedFileRollOver();
            }
        } catch (Exception e2) {
            CommonUtils.a(this.f7099a, "Failed to roll over file", e2);
        }
    }
}
