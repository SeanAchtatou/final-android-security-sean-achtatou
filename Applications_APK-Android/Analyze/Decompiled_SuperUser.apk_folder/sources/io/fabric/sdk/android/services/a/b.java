package io.fabric.sdk.android.services.a;

import android.content.Context;

/* compiled from: MemoryValueCache */
public class b<T> extends a<T> {

    /* renamed from: a  reason: collision with root package name */
    private T f7089a;

    public b() {
        this(null);
    }

    public b(c<T> cVar) {
        super(cVar);
    }

    /* access modifiers changed from: protected */
    public T a(Context context) {
        return this.f7089a;
    }

    /* access modifiers changed from: protected */
    public void a(Context context, String str) {
        this.f7089a = str;
    }
}
