package io.fabric.sdk.android.services.settings;

/* compiled from: AnalyticsSettingsData */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public final String f7269a;

    /* renamed from: b  reason: collision with root package name */
    public final int f7270b;

    /* renamed from: c  reason: collision with root package name */
    public final int f7271c;

    /* renamed from: d  reason: collision with root package name */
    public final int f7272d;

    /* renamed from: e  reason: collision with root package name */
    public final int f7273e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f7274f;

    /* renamed from: g  reason: collision with root package name */
    public final boolean f7275g;

    /* renamed from: h  reason: collision with root package name */
    public final boolean f7276h;
    public final int i;

    public b(String str, int i2, int i3, int i4, int i5, boolean z, boolean z2, int i6, boolean z3) {
        this.f7269a = str;
        this.f7270b = i2;
        this.f7271c = i3;
        this.f7272d = i4;
        this.f7273e = i5;
        this.f7274f = z;
        this.f7275g = z2;
        this.i = i6;
        this.f7276h = z3;
    }
}
