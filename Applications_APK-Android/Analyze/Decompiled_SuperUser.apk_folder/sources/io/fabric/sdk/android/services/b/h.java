package io.fabric.sdk.android.services.b;

import android.content.Context;
import com.lody.virtual.helper.utils.FileUtils;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.k;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: QueueFileEventStorage */
public class h implements c {

    /* renamed from: a  reason: collision with root package name */
    private final Context f7093a;

    /* renamed from: b  reason: collision with root package name */
    private final File f7094b;

    /* renamed from: c  reason: collision with root package name */
    private final String f7095c;

    /* renamed from: d  reason: collision with root package name */
    private final File f7096d;

    /* renamed from: e  reason: collision with root package name */
    private k f7097e = new k(this.f7096d);

    /* renamed from: f  reason: collision with root package name */
    private File f7098f;

    public h(Context context, File file, String str, String str2) {
        this.f7093a = context;
        this.f7094b = file;
        this.f7095c = str2;
        this.f7096d = new File(this.f7094b, str);
        e();
    }

    private void e() {
        this.f7098f = new File(this.f7094b, this.f7095c);
        if (!this.f7098f.exists()) {
            this.f7098f.mkdirs();
        }
    }

    public void a(byte[] bArr) {
        this.f7097e.a(bArr);
    }

    public int a() {
        return this.f7097e.a();
    }

    public void a(String str) {
        this.f7097e.close();
        a(this.f7096d, new File(this.f7098f, str));
        this.f7097e = new k(this.f7096d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.FileInputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.OutputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    private void a(File file, File file2) {
        FileInputStream fileInputStream;
        OutputStream outputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                outputStream = a(file2);
                CommonUtils.a(fileInputStream, outputStream, new byte[FileUtils.FileMode.MODE_ISGID]);
                CommonUtils.a((Closeable) fileInputStream, "Failed to close file input stream");
                CommonUtils.a((Closeable) outputStream, "Failed to close output stream");
                file.delete();
            } catch (Throwable th) {
                th = th;
                CommonUtils.a((Closeable) fileInputStream, "Failed to close file input stream");
                CommonUtils.a((Closeable) outputStream, "Failed to close output stream");
                file.delete();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            CommonUtils.a((Closeable) fileInputStream, "Failed to close file input stream");
            CommonUtils.a((Closeable) outputStream, "Failed to close output stream");
            file.delete();
            throw th;
        }
    }

    public OutputStream a(File file) {
        return new FileOutputStream(file);
    }

    public List<File> a(int i) {
        ArrayList arrayList = new ArrayList();
        for (File add : this.f7098f.listFiles()) {
            arrayList.add(add);
            if (arrayList.size() >= i) {
                break;
            }
        }
        return arrayList;
    }

    public void a(List<File> list) {
        for (File next : list) {
            CommonUtils.a(this.f7093a, String.format("deleting sent analytics file %s", next.getName()));
            next.delete();
        }
    }

    public List<File> c() {
        return Arrays.asList(this.f7098f.listFiles());
    }

    public void d() {
        try {
            this.f7097e.close();
        } catch (IOException e2) {
        }
        this.f7096d.delete();
    }

    public boolean b() {
        return this.f7097e.b();
    }

    public boolean a(int i, int i2) {
        return this.f7097e.a(i, i2);
    }
}
