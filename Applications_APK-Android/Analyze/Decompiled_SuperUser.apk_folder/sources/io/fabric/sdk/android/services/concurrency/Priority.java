package io.fabric.sdk.android.services.concurrency;

public enum Priority {
    LOW,
    NORMAL,
    HIGH,
    IMMEDIATE;

    static <Y> int a(f fVar, Y y) {
        Priority priority;
        if (y instanceof f) {
            priority = ((f) y).getPriority();
        } else {
            priority = NORMAL;
        }
        return priority.ordinal() - fVar.getPriority().ordinal();
    }
}
