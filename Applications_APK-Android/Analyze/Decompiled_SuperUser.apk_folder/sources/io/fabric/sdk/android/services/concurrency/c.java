package io.fabric.sdk.android.services.concurrency;

import io.fabric.sdk.android.services.concurrency.AsyncTask;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

/* compiled from: PriorityAsyncTask */
public abstract class c<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> implements a<j>, f, j {

    /* renamed from: a  reason: collision with root package name */
    private final h f7223a = new h();

    public final void a(ExecutorService executorService, Object... objArr) {
        super.a(new a(executorService, this), objArr);
    }

    public int compareTo(Object obj) {
        return Priority.a(this, obj);
    }

    /* renamed from: a */
    public void addDependency(j jVar) {
        if (b() != AsyncTask.Status.PENDING) {
            throw new IllegalStateException("Must not add Dependency after task is running");
        }
        ((a) ((f) e())).addDependency(jVar);
    }

    public Collection<j> getDependencies() {
        return ((a) ((f) e())).getDependencies();
    }

    public boolean areDependenciesMet() {
        return ((a) ((f) e())).areDependenciesMet();
    }

    public Priority getPriority() {
        return ((f) e()).getPriority();
    }

    public void setFinished(boolean z) {
        ((j) ((f) e())).setFinished(z);
    }

    public boolean isFinished() {
        return ((j) ((f) e())).isFinished();
    }

    public void setError(Throwable th) {
        ((j) ((f) e())).setError(th);
    }

    public <T extends a<j> & f & j> T e() {
        return this.f7223a;
    }

    /* compiled from: PriorityAsyncTask */
    private static class a<Result> implements Executor {

        /* renamed from: a  reason: collision with root package name */
        private final Executor f7224a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final c f7225b;

        public a(Executor executor, c cVar) {
            this.f7224a = executor;
            this.f7225b = cVar;
        }

        public void execute(Runnable runnable) {
            this.f7224a.execute(new e<Result>(runnable, null) {
                public <T extends a<j> & f & j> T a() {
                    return a.this.f7225b;
                }
            });
        }
    }
}
