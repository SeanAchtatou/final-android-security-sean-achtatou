package io.fabric.sdk.android.services.concurrency.a;

/* compiled from: RetryState */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private final int f7220a;

    /* renamed from: b  reason: collision with root package name */
    private final a f7221b;

    /* renamed from: c  reason: collision with root package name */
    private final d f7222c;

    public e(a aVar, d dVar) {
        this(0, aVar, dVar);
    }

    public e(int i, a aVar, d dVar) {
        this.f7220a = i;
        this.f7221b = aVar;
        this.f7222c = dVar;
    }

    public long a() {
        return this.f7221b.getDelayMillis(this.f7220a);
    }

    public e b() {
        return new e(this.f7220a + 1, this.f7221b, this.f7222c);
    }

    public e c() {
        return new e(this.f7221b, this.f7222c);
    }
}
