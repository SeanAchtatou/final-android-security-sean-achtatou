package io.fabric.sdk.android;

/* compiled from: InitializationCallback */
public interface d<T> {

    /* renamed from: d  reason: collision with root package name */
    public static final d f7075d = new a();

    void a(Exception exc);

    void a(Object obj);

    /* compiled from: InitializationCallback */
    public static class a implements d<Object> {
        private a() {
        }

        public void a(Object obj) {
        }

        public void a(Exception exc) {
        }
    }
}
