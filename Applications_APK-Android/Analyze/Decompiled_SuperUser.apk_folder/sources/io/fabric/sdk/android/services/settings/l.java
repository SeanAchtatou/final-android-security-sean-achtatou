package io.fabric.sdk.android.services.settings;

import com.google.firebase.analytics.FirebaseAnalytics;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.f;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.a;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.c;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: DefaultSettingsSpiCall */
class l extends a implements w {
    public l(f fVar, String str, String str2, c cVar) {
        this(fVar, str, str2, cVar, HttpMethod.GET);
    }

    l(f fVar, String str, String str2, c cVar, HttpMethod httpMethod) {
        super(fVar, str, str2, cVar, httpMethod);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject a(io.fabric.sdk.android.services.settings.v r8) {
        /*
            r7 = this;
            r0 = 0
            java.util.Map r1 = r7.b(r8)     // Catch:{ HttpRequestException -> 0x0072, all -> 0x00a4 }
            io.fabric.sdk.android.services.network.HttpRequest r2 = r7.getHttpRequest(r1)     // Catch:{ HttpRequestException -> 0x0072, all -> 0x00a4 }
            io.fabric.sdk.android.services.network.HttpRequest r2 = r7.a(r2, r8)     // Catch:{ HttpRequestException -> 0x00ce }
            io.fabric.sdk.android.i r3 = io.fabric.sdk.android.Fabric.h()     // Catch:{ HttpRequestException -> 0x00ce }
            java.lang.String r4 = "Fabric"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ HttpRequestException -> 0x00ce }
            r5.<init>()     // Catch:{ HttpRequestException -> 0x00ce }
            java.lang.String r6 = "Requesting settings from "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ HttpRequestException -> 0x00ce }
            java.lang.String r6 = r7.getUrl()     // Catch:{ HttpRequestException -> 0x00ce }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ HttpRequestException -> 0x00ce }
            java.lang.String r5 = r5.toString()     // Catch:{ HttpRequestException -> 0x00ce }
            r3.a(r4, r5)     // Catch:{ HttpRequestException -> 0x00ce }
            io.fabric.sdk.android.i r3 = io.fabric.sdk.android.Fabric.h()     // Catch:{ HttpRequestException -> 0x00ce }
            java.lang.String r4 = "Fabric"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ HttpRequestException -> 0x00ce }
            r5.<init>()     // Catch:{ HttpRequestException -> 0x00ce }
            java.lang.String r6 = "Settings query params were: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ HttpRequestException -> 0x00ce }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ HttpRequestException -> 0x00ce }
            java.lang.String r1 = r1.toString()     // Catch:{ HttpRequestException -> 0x00ce }
            r3.a(r4, r1)     // Catch:{ HttpRequestException -> 0x00ce }
            org.json.JSONObject r0 = r7.a(r2)     // Catch:{ HttpRequestException -> 0x00ce }
            if (r2 == 0) goto L_0x0071
            io.fabric.sdk.android.i r1 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Fabric"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Settings request ID: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "X-REQUEST-ID"
            java.lang.String r2 = r2.b(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r1.a(r3, r2)
        L_0x0071:
            return r0
        L_0x0072:
            r1 = move-exception
            r2 = r0
        L_0x0074:
            io.fabric.sdk.android.i r3 = io.fabric.sdk.android.Fabric.h()     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "Fabric"
            java.lang.String r5 = "Settings request failed."
            r3.e(r4, r5, r1)     // Catch:{ all -> 0x00cc }
            if (r2 == 0) goto L_0x0071
            io.fabric.sdk.android.i r1 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Fabric"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Settings request ID: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "X-REQUEST-ID"
            java.lang.String r2 = r2.b(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r1.a(r3, r2)
            goto L_0x0071
        L_0x00a4:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x00a7:
            if (r2 == 0) goto L_0x00cb
            io.fabric.sdk.android.i r1 = io.fabric.sdk.android.Fabric.h()
            java.lang.String r3 = "Fabric"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Settings request ID: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "X-REQUEST-ID"
            java.lang.String r2 = r2.b(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r1.a(r3, r2)
        L_0x00cb:
            throw r0
        L_0x00cc:
            r0 = move-exception
            goto L_0x00a7
        L_0x00ce:
            r1 = move-exception
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: io.fabric.sdk.android.services.settings.l.a(io.fabric.sdk.android.services.settings.v):org.json.JSONObject");
    }

    /* access modifiers changed from: package-private */
    public JSONObject a(HttpRequest httpRequest) {
        int b2 = httpRequest.b();
        Fabric.h().a("Fabric", "Settings result was: " + b2);
        if (a(b2)) {
            return a(httpRequest.e());
        }
        Fabric.h().e("Fabric", "Failed to retrieve settings from " + getUrl());
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i) {
        return i == 200 || i == 201 || i == 202 || i == 203;
    }

    private JSONObject a(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception e2) {
            Fabric.h().a("Fabric", "Failed to parse settings JSON from " + getUrl(), e2);
            Fabric.h().a("Fabric", "Settings response " + str);
            return null;
        }
    }

    private Map<String, String> b(v vVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("build_version", vVar.j);
        hashMap.put("display_version", vVar.i);
        hashMap.put(FirebaseAnalytics.Param.SOURCE, Integer.toString(vVar.k));
        if (vVar.l != null) {
            hashMap.put("icon_hash", vVar.l);
        }
        String str = vVar.f7347h;
        if (!CommonUtils.c(str)) {
            hashMap.put("instance", str);
        }
        return hashMap;
    }

    private HttpRequest a(HttpRequest httpRequest, v vVar) {
        a(httpRequest, a.HEADER_API_KEY, vVar.f7340a);
        a(httpRequest, a.HEADER_CLIENT_TYPE, a.ANDROID_CLIENT_TYPE);
        a(httpRequest, a.HEADER_CLIENT_VERSION, this.kit.getVersion());
        a(httpRequest, a.HEADER_ACCEPT, a.ACCEPT_JSON_VALUE);
        a(httpRequest, "X-CRASHLYTICS-DEVICE-MODEL", vVar.f7341b);
        a(httpRequest, "X-CRASHLYTICS-OS-BUILD-VERSION", vVar.f7342c);
        a(httpRequest, "X-CRASHLYTICS-OS-DISPLAY-VERSION", vVar.f7343d);
        a(httpRequest, "X-CRASHLYTICS-ADVERTISING-TOKEN", vVar.f7344e);
        a(httpRequest, "X-CRASHLYTICS-INSTALLATION-ID", vVar.f7345f);
        a(httpRequest, "X-CRASHLYTICS-ANDROID-ID", vVar.f7346g);
        return httpRequest;
    }

    private void a(HttpRequest httpRequest, String str, String str2) {
        if (str2 != null) {
            httpRequest.a(str, str2);
        }
    }
}
