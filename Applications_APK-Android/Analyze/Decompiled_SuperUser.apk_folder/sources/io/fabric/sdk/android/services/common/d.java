package io.fabric.sdk.android.services.common;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.lody.virtual.helper.utils.FileUtils;
import io.fabric.sdk.android.Fabric;

/* compiled from: ApiKey */
public class d {
    public String a(Context context) {
        String b2 = b(context);
        if (TextUtils.isEmpty(b2)) {
            b2 = c(context);
        }
        if (TextUtils.isEmpty(b2)) {
            d(context);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public String b(Context context) {
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), FileUtils.FileMode.MODE_IWUSR).metaData;
            if (bundle == null) {
                return null;
            }
            String string = bundle.getString("io.fabric.ApiKey");
            if (string != null) {
                return string;
            }
            Fabric.h().a("Fabric", "Falling back to Crashlytics key lookup from Manifest");
            return bundle.getString("com.crashlytics.ApiKey");
        } catch (Exception e2) {
            Fabric.h().a("Fabric", "Caught non-fatal exception while retrieving apiKey: " + e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String c(Context context) {
        int a2 = CommonUtils.a(context, "io.fabric.ApiKey", "string");
        if (a2 == 0) {
            Fabric.h().a("Fabric", "Falling back to Crashlytics key lookup from Strings");
            a2 = CommonUtils.a(context, "com.crashlytics.ApiKey", "string");
        }
        if (a2 != 0) {
            return context.getResources().getString(a2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void d(Context context) {
        if (Fabric.i() || CommonUtils.i(context)) {
            throw new IllegalArgumentException(a());
        }
        Fabric.h().e("Fabric", a());
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "Fabric could not be initialized, API key missing from AndroidManifest.xml. Add the following tag to your Application element \n\t<meta-data android:name=\"io.fabric.ApiKey\" android:value=\"YOUR_API_KEY\"/>";
    }
}
