package io.fabric.sdk.android.services.settings;

/* compiled from: SettingsRequest */
public class v {

    /* renamed from: a  reason: collision with root package name */
    public final String f7340a;

    /* renamed from: b  reason: collision with root package name */
    public final String f7341b;

    /* renamed from: c  reason: collision with root package name */
    public final String f7342c;

    /* renamed from: d  reason: collision with root package name */
    public final String f7343d;

    /* renamed from: e  reason: collision with root package name */
    public final String f7344e;

    /* renamed from: f  reason: collision with root package name */
    public final String f7345f;

    /* renamed from: g  reason: collision with root package name */
    public final String f7346g;

    /* renamed from: h  reason: collision with root package name */
    public final String f7347h;
    public final String i;
    public final String j;
    public final int k;
    public final String l;

    public v(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, int i2, String str11) {
        this.f7340a = str;
        this.f7341b = str2;
        this.f7342c = str3;
        this.f7343d = str4;
        this.f7344e = str5;
        this.f7345f = str6;
        this.f7346g = str7;
        this.f7347h = str8;
        this.i = str9;
        this.j = str10;
        this.k = i2;
        this.l = str11;
    }
}
