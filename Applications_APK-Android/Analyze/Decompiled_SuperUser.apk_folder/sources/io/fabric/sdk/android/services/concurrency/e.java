package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* compiled from: PriorityFutureTask */
public class e<V> extends FutureTask<V> implements a<j>, f, j {

    /* renamed from: b  reason: collision with root package name */
    final Object f7227b;

    public e(Callable<V> callable) {
        super(callable);
        this.f7227b = a(callable);
    }

    public e(Runnable runnable, V v) {
        super(runnable, v);
        this.f7227b = a(runnable);
    }

    public int compareTo(Object obj) {
        return ((f) a()).compareTo(obj);
    }

    /* renamed from: a */
    public void addDependency(j jVar) {
        ((a) ((f) a())).addDependency(jVar);
    }

    public Collection<j> getDependencies() {
        return ((a) ((f) a())).getDependencies();
    }

    public boolean areDependenciesMet() {
        return ((a) ((f) a())).areDependenciesMet();
    }

    public Priority getPriority() {
        return ((f) a()).getPriority();
    }

    public void setFinished(boolean z) {
        ((j) ((f) a())).setFinished(z);
    }

    public boolean isFinished() {
        return ((j) ((f) a())).isFinished();
    }

    public void setError(Throwable th) {
        ((j) ((f) a())).setError(th);
    }

    public <T extends a<j> & f & j> T a() {
        return (a) this.f7227b;
    }

    /* access modifiers changed from: protected */
    public <T extends a<j> & f & j> T a(Object obj) {
        if (h.isProperDelegate(obj)) {
            return (a) obj;
        }
        return new h();
    }
}
