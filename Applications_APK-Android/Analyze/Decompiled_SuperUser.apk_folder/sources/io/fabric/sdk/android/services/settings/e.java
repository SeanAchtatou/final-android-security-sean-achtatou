package io.fabric.sdk.android.services.settings;

/* compiled from: AppSettingsData */
public class e {

    /* renamed from: a  reason: collision with root package name */
    public final String f7288a;

    /* renamed from: b  reason: collision with root package name */
    public final String f7289b;

    /* renamed from: c  reason: collision with root package name */
    public final String f7290c;

    /* renamed from: d  reason: collision with root package name */
    public final String f7291d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f7292e;

    /* renamed from: f  reason: collision with root package name */
    public final c f7293f;

    public e(String str, String str2, String str3, String str4, boolean z, c cVar) {
        this.f7288a = str;
        this.f7289b = str2;
        this.f7290c = str3;
        this.f7291d = str4;
        this.f7292e = z;
        this.f7293f = cVar;
    }
}
