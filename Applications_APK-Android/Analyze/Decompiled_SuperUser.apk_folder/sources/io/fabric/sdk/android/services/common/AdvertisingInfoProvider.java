package io.fabric.sdk.android.services.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.c.c;
import io.fabric.sdk.android.services.c.d;

class AdvertisingInfoProvider {

    /* renamed from: a  reason: collision with root package name */
    private final Context f7107a;

    /* renamed from: b  reason: collision with root package name */
    private final c f7108b;

    public AdvertisingInfoProvider(Context context) {
        this.f7107a = context.getApplicationContext();
        this.f7108b = new d(context, "TwitterAdvertisingInfoPreferences");
    }

    public b a() {
        b b2 = b();
        if (c(b2)) {
            Fabric.h().a("Fabric", "Using AdvertisingInfo from Preference Store");
            a(b2);
            return b2;
        }
        b e2 = e();
        b(e2);
        return e2;
    }

    private void a(final b bVar) {
        new Thread(new e() {
            public void onRun() {
                b a2 = AdvertisingInfoProvider.this.e();
                if (!bVar.equals(a2)) {
                    Fabric.h().a("Fabric", "Asychronously getting Advertising Info and storing it to preferences");
                    AdvertisingInfoProvider.this.b(a2);
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    @SuppressLint({"CommitPrefEdits"})
    public void b(b bVar) {
        if (c(bVar)) {
            this.f7108b.a(this.f7108b.b().putString("advertising_id", bVar.f7151a).putBoolean("limit_ad_tracking_enabled", bVar.f7152b));
        } else {
            this.f7108b.a(this.f7108b.b().remove("advertising_id").remove("limit_ad_tracking_enabled"));
        }
    }

    /* access modifiers changed from: protected */
    public b b() {
        return new b(this.f7108b.a().getString("advertising_id", ""), this.f7108b.a().getBoolean("limit_ad_tracking_enabled", false));
    }

    public c c() {
        return new AdvertisingInfoReflectionStrategy(this.f7107a);
    }

    public c d() {
        return new AdvertisingInfoServiceStrategy(this.f7107a);
    }

    private boolean c(b bVar) {
        return bVar != null && !TextUtils.isEmpty(bVar.f7151a);
    }

    /* access modifiers changed from: private */
    public b e() {
        b a2 = c().a();
        if (!c(a2)) {
            a2 = d().a();
            if (!c(a2)) {
                Fabric.h().a("Fabric", "AdvertisingInfo not present");
            } else {
                Fabric.h().a("Fabric", "Using AdvertisingInfo from Service Provider");
            }
        } else {
            Fabric.h().a("Fabric", "Using AdvertisingInfo from Reflection Provider");
        }
        return a2;
    }
}
