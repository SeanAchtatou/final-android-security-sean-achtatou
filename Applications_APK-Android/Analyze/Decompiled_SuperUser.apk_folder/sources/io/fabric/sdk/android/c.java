package io.fabric.sdk.android;

import android.os.SystemClock;
import android.text.TextUtils;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* compiled from: FabricKitsFinder */
class c implements Callable<Map<String, h>> {

    /* renamed from: a  reason: collision with root package name */
    final String f7074a;

    c(String str) {
        this.f7074a = str;
    }

    /* renamed from: a */
    public Map<String, h> call() {
        HashMap hashMap = new HashMap();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        hashMap.putAll(c());
        hashMap.putAll(d());
        Fabric.h().b("Fabric", "finish scanning in " + (SystemClock.elapsedRealtime() - elapsedRealtime));
        return hashMap;
    }

    private Map<String, h> c() {
        HashMap hashMap = new HashMap();
        try {
            Class.forName("com.google.android.gms.ads.AdView");
            h hVar = new h("com.google.firebase.firebase-ads", "0.0.0", "binary");
            hashMap.put(hVar.a(), hVar);
            Fabric.h().b("Fabric", "Found kit: com.google.firebase.firebase-ads");
        } catch (Exception e2) {
        }
        return hashMap;
    }

    private Map<String, h> d() {
        h a2;
        HashMap hashMap = new HashMap();
        ZipFile b2 = b();
        Enumeration<? extends ZipEntry> entries = b2.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            if (zipEntry.getName().startsWith("fabric/") && zipEntry.getName().length() > "fabric/".length() && (a2 = a(zipEntry, b2)) != null) {
                hashMap.put(a2.a(), a2);
                Fabric.h().b("Fabric", String.format("Found kit:[%s] version:[%s]", a2.a(), a2.b()));
            }
        }
        if (b2 != null) {
            try {
                b2.close();
            } catch (IOException e2) {
            }
        }
        return hashMap;
    }

    private h a(ZipEntry zipEntry, ZipFile zipFile) {
        InputStream inputStream;
        try {
            inputStream = zipFile.getInputStream(zipEntry);
            try {
                Properties properties = new Properties();
                properties.load(inputStream);
                String property = properties.getProperty("fabric-identifier");
                String property2 = properties.getProperty("fabric-version");
                String property3 = properties.getProperty("fabric-build-type");
                if (TextUtils.isEmpty(property) || TextUtils.isEmpty(property2)) {
                    throw new IllegalStateException("Invalid format of fabric file," + zipEntry.getName());
                }
                h hVar = new h(property, property2, property3);
                CommonUtils.a((Closeable) inputStream);
                return hVar;
            } catch (IOException e2) {
                e = e2;
                try {
                    Fabric.h().e("Fabric", "Error when parsing fabric properties " + zipEntry.getName(), e);
                    CommonUtils.a((Closeable) inputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    CommonUtils.a((Closeable) inputStream);
                    throw th;
                }
            }
        } catch (IOException e3) {
            e = e3;
            inputStream = null;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            CommonUtils.a((Closeable) inputStream);
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public ZipFile b() {
        return new ZipFile(this.f7074a);
    }
}
