package io.fabric.sdk.android.services.network;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

/* compiled from: NetworkUtils */
public final class d {
    public static final SSLSocketFactory a(e eVar) {
        SSLContext instance = SSLContext.getInstance("TLS");
        instance.init(null, new TrustManager[]{new f(new g(eVar.getKeyStoreStream(), eVar.getKeyStorePassword()), eVar)}, null);
        return instance.getSocketFactory();
    }
}
