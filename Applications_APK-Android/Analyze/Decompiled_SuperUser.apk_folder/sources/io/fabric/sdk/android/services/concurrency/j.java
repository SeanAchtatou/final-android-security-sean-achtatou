package io.fabric.sdk.android.services.concurrency;

/* compiled from: Task */
public interface j {
    boolean isFinished();

    void setError(Throwable th);

    void setFinished(boolean z);
}
