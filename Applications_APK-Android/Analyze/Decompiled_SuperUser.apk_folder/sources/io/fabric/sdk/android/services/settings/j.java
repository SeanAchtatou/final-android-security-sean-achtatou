package io.fabric.sdk.android.services.settings;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.f;
import io.fabric.sdk.android.services.c.c;
import io.fabric.sdk.android.services.c.d;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.g;
import org.json.JSONObject;

/* compiled from: DefaultSettingsController */
class j implements r {

    /* renamed from: a  reason: collision with root package name */
    private final v f7297a;

    /* renamed from: b  reason: collision with root package name */
    private final u f7298b;

    /* renamed from: c  reason: collision with root package name */
    private final g f7299c;

    /* renamed from: d  reason: collision with root package name */
    private final g f7300d;

    /* renamed from: e  reason: collision with root package name */
    private final w f7301e;

    /* renamed from: f  reason: collision with root package name */
    private final f f7302f;

    /* renamed from: g  reason: collision with root package name */
    private final c f7303g = new d(this.f7302f);

    public j(f fVar, v vVar, g gVar, u uVar, g gVar2, w wVar) {
        this.f7302f = fVar;
        this.f7297a = vVar;
        this.f7299c = gVar;
        this.f7298b = uVar;
        this.f7300d = gVar2;
        this.f7301e = wVar;
    }

    public s a() {
        return a(SettingsCacheBehavior.USE_CACHE);
    }

    public s a(SettingsCacheBehavior settingsCacheBehavior) {
        Exception e2;
        s sVar;
        s sVar2 = null;
        try {
            if (!Fabric.i() && !d()) {
                sVar2 = b(settingsCacheBehavior);
            }
            if (sVar2 == null) {
                try {
                    JSONObject a2 = this.f7301e.a(this.f7297a);
                    if (a2 != null) {
                        sVar2 = this.f7298b.a(this.f7299c, a2);
                        this.f7300d.a(sVar2.f7337g, a2);
                        a(a2, "Loaded settings: ");
                        a(b());
                    }
                } catch (Exception e3) {
                    Exception exc = e3;
                    sVar = sVar2;
                    e2 = exc;
                    Fabric.h().e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", e2);
                    return sVar;
                }
            }
            sVar = sVar2;
            if (sVar != null) {
                return sVar;
            }
            try {
                return b(SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION);
            } catch (Exception e4) {
                e2 = e4;
            }
        } catch (Exception e5) {
            Exception exc2 = e5;
            sVar = null;
            e2 = exc2;
            Fabric.h().e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", e2);
            return sVar;
        }
    }

    private s b(SettingsCacheBehavior settingsCacheBehavior) {
        s sVar = null;
        try {
            if (SettingsCacheBehavior.SKIP_CACHE_LOOKUP.equals(settingsCacheBehavior)) {
                return null;
            }
            JSONObject a2 = this.f7300d.a();
            if (a2 != null) {
                s a3 = this.f7298b.a(this.f7299c, a2);
                if (a3 != null) {
                    a(a2, "Loaded cached settings: ");
                    long a4 = this.f7299c.a();
                    if (SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION.equals(settingsCacheBehavior) || !a3.a(a4)) {
                        try {
                            Fabric.h().a("Fabric", "Returning cached settings.");
                            return a3;
                        } catch (Exception e2) {
                            Exception exc = e2;
                            sVar = a3;
                            e = exc;
                            Fabric.h().e("Fabric", "Failed to get cached settings", e);
                            return sVar;
                        }
                    } else {
                        Fabric.h().a("Fabric", "Cached settings have expired.");
                        return null;
                    }
                } else {
                    Fabric.h().e("Fabric", "Failed to transform cached settings data.", null);
                    return null;
                }
            } else {
                Fabric.h().a("Fabric", "No cached settings data found.");
                return null;
            }
        } catch (Exception e3) {
            e = e3;
        }
    }

    private void a(JSONObject jSONObject, String str) {
        Fabric.h().a("Fabric", str + jSONObject.toString());
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return CommonUtils.a(CommonUtils.m(this.f7302f.getContext()));
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.f7303g.a().getString("existing_instance_identifier", "");
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"CommitPrefEdits"})
    public boolean a(String str) {
        SharedPreferences.Editor b2 = this.f7303g.b();
        b2.putString("existing_instance_identifier", str);
        return this.f7303g.a(b2);
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return !c().equals(b());
    }
}
