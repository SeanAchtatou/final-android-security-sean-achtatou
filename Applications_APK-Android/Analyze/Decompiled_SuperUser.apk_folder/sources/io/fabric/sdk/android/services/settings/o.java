package io.fabric.sdk.android.services.settings;

/* compiled from: PromptSettingsData */
public class o {

    /* renamed from: a  reason: collision with root package name */
    public final String f7312a;

    /* renamed from: b  reason: collision with root package name */
    public final String f7313b;

    /* renamed from: c  reason: collision with root package name */
    public final String f7314c;

    /* renamed from: d  reason: collision with root package name */
    public final boolean f7315d;

    /* renamed from: e  reason: collision with root package name */
    public final String f7316e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f7317f;

    /* renamed from: g  reason: collision with root package name */
    public final String f7318g;

    public o(String str, String str2, String str3, boolean z, String str4, boolean z2, String str5) {
        this.f7312a = str;
        this.f7313b = str2;
        this.f7314c = str3;
        this.f7315d = z;
        this.f7316e = str4;
        this.f7317f = z2;
        this.f7318g = str5;
    }
}
