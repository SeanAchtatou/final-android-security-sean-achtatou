package io.fabric.sdk.android.services.settings;

import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.f;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeliveryMechanism;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.common.d;
import io.fabric.sdk.android.services.common.m;
import io.fabric.sdk.android.services.network.c;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: Settings */
public class q {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicReference<s> f7326a;

    /* renamed from: b  reason: collision with root package name */
    private final CountDownLatch f7327b;

    /* renamed from: c  reason: collision with root package name */
    private r f7328c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f7329d;

    /* compiled from: Settings */
    static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final q f7330a = new q();
    }

    public static q a() {
        return a.f7330a;
    }

    private q() {
        this.f7326a = new AtomicReference<>();
        this.f7327b = new CountDownLatch(1);
        this.f7329d = false;
    }

    public synchronized q a(f fVar, IdManager idManager, c cVar, String str, String str2, String str3) {
        q qVar;
        if (this.f7329d) {
            qVar = this;
        } else {
            if (this.f7328c == null) {
                Context context = fVar.getContext();
                String c2 = idManager.c();
                String a2 = new d().a(context);
                String j = idManager.j();
                m mVar = new m();
                k kVar = new k();
                i iVar = new i(fVar);
                String k = CommonUtils.k(context);
                f fVar2 = fVar;
                String str4 = str3;
                l lVar = new l(fVar2, str4, String.format(Locale.US, "https://settings.crashlytics.com/spi/v2/platforms/android/apps/%s/settings", c2), cVar);
                String str5 = str2;
                String str6 = str;
                this.f7328c = new j(fVar, new v(a2, idManager.g(), idManager.f(), idManager.e(), idManager.m(), idManager.b(), idManager.n(), CommonUtils.a(CommonUtils.m(context)), str5, str6, DeliveryMechanism.a(j).a(), k), mVar, kVar, iVar, lVar);
            }
            this.f7329d = true;
            qVar = this;
        }
        return qVar;
    }

    public s b() {
        try {
            this.f7327b.await();
            return this.f7326a.get();
        } catch (InterruptedException e2) {
            Fabric.h().e("Fabric", "Interrupted while waiting for settings data.");
            return null;
        }
    }

    public synchronized boolean c() {
        s a2;
        a2 = this.f7328c.a();
        a(a2);
        return a2 != null;
    }

    public synchronized boolean d() {
        s a2;
        a2 = this.f7328c.a(SettingsCacheBehavior.SKIP_CACHE_LOOKUP);
        a(a2);
        if (a2 == null) {
            Fabric.h().e("Fabric", "Failed to force reload of settings from Crashlytics.", null);
        }
        return a2 != null;
    }

    private void a(s sVar) {
        this.f7326a.set(sVar);
        this.f7327b.countDown();
    }
}
