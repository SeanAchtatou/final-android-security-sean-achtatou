package io.fabric.sdk.android.services.settings;

import android.content.Context;
import android.graphics.BitmapFactory;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.common.CommonUtils;

/* compiled from: IconRequest */
public class n {

    /* renamed from: a  reason: collision with root package name */
    public final String f7308a;

    /* renamed from: b  reason: collision with root package name */
    public final int f7309b;

    /* renamed from: c  reason: collision with root package name */
    public final int f7310c;

    /* renamed from: d  reason: collision with root package name */
    public final int f7311d;

    public n(String str, int i, int i2, int i3) {
        this.f7308a = str;
        this.f7309b = i;
        this.f7310c = i2;
        this.f7311d = i3;
    }

    public static n a(Context context, String str) {
        if (str != null) {
            try {
                int l = CommonUtils.l(context);
                Fabric.h().a("Fabric", "App icon resource ID is " + l);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(context.getResources(), l, options);
                return new n(str, l, options.outWidth, options.outHeight);
            } catch (Exception e2) {
                Fabric.h().e("Fabric", "Failed to load icon", e2);
            }
        }
        return null;
    }
}
