package io.fabric.sdk.android.services.concurrency;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AsyncTask<Params, Progress, Result> {

    /* renamed from: a  reason: collision with root package name */
    private static final int f7187a = Runtime.getRuntime().availableProcessors();

    /* renamed from: b  reason: collision with root package name */
    public static final Executor f7188b = new ThreadPoolExecutor(f7190d, f7191e, 1, TimeUnit.SECONDS, f7193g, f7192f);

    /* renamed from: c  reason: collision with root package name */
    public static final Executor f7189c = new c();

    /* renamed from: d  reason: collision with root package name */
    private static final int f7190d = (f7187a + 1);

    /* renamed from: e  reason: collision with root package name */
    private static final int f7191e = ((f7187a * 2) + 1);

    /* renamed from: f  reason: collision with root package name */
    private static final ThreadFactory f7192f = new ThreadFactory() {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f7195a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.f7195a.getAndIncrement());
        }
    };

    /* renamed from: g  reason: collision with root package name */
    private static final BlockingQueue<Runnable> f7193g = new LinkedBlockingQueue((int) FileUtils.FileMode.MODE_IWUSR);

    /* renamed from: h  reason: collision with root package name */
    private static final b f7194h = new b();
    private static volatile Executor i = f7189c;
    private final d<Params, Result> j = new d<Params, Result>() {
        public Result call() {
            AsyncTask.this.n.set(true);
            Process.setThreadPriority(10);
            return AsyncTask.this.d(AsyncTask.this.a(this.f7209b));
        }
    };
    private final FutureTask<Result> k = new FutureTask<Result>(this.j) {
        /* access modifiers changed from: protected */
        public void done() {
            try {
                AsyncTask.this.c(get());
            } catch (InterruptedException e2) {
                Log.w("AsyncTask", e2);
            } catch (ExecutionException e3) {
                throw new RuntimeException("An error occured while executing doInBackground()", e3.getCause());
            } catch (CancellationException e4) {
                AsyncTask.this.c(null);
            }
        }
    };
    private volatile Status l = Status.PENDING;
    private final AtomicBoolean m = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final AtomicBoolean n = new AtomicBoolean();

    public enum Status {
        PENDING,
        RUNNING,
        FINISHED
    }

    /* access modifiers changed from: protected */
    public abstract Result a(Object... objArr);

    private static class c implements Executor {

        /* renamed from: a  reason: collision with root package name */
        final LinkedList<Runnable> f7205a;

        /* renamed from: b  reason: collision with root package name */
        Runnable f7206b;

        private c() {
            this.f7205a = new LinkedList<>();
        }

        public synchronized void execute(final Runnable runnable) {
            this.f7205a.offer(new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } finally {
                        c.this.a();
                    }
                }
            });
            if (this.f7206b == null) {
                a();
            }
        }

        /* access modifiers changed from: protected */
        public synchronized void a() {
            Runnable poll = this.f7205a.poll();
            this.f7206b = poll;
            if (poll != null) {
                AsyncTask.f7188b.execute(this.f7206b);
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(Result result) {
        if (!this.n.get()) {
            d(result);
        }
    }

    /* access modifiers changed from: private */
    public Result d(Result result) {
        f7194h.obtainMessage(1, new a(this, result)).sendToTarget();
        return result;
    }

    public final Status b() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
    }

    /* access modifiers changed from: protected */
    public void b(Progress... progressArr) {
    }

    /* access modifiers changed from: protected */
    public void b(Result result) {
        c();
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    public final boolean d() {
        return this.m.get();
    }

    public final boolean a(boolean z) {
        this.m.set(true);
        return this.k.cancel(z);
    }

    public final AsyncTask<Params, Progress, Result> a(Executor executor, Object... objArr) {
        if (this.l != Status.PENDING) {
            switch (this.l) {
                case RUNNING:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case FINISHED:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.l = Status.RUNNING;
        a();
        this.j.f7209b = objArr;
        executor.execute(this.k);
        return this;
    }

    /* access modifiers changed from: private */
    public void e(Result result) {
        if (d()) {
            b(result);
        } else {
            a((Object) result);
        }
        this.l = Status.FINISHED;
    }

    private static class b extends Handler {
        public b() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            a aVar = (a) message.obj;
            switch (message.what) {
                case 1:
                    aVar.f7203a.e(aVar.f7204b[0]);
                    return;
                case 2:
                    aVar.f7203a.b((Object[]) aVar.f7204b);
                    return;
                default:
                    return;
            }
        }
    }

    private static abstract class d<Params, Result> implements Callable<Result> {

        /* renamed from: b  reason: collision with root package name */
        Params[] f7209b;

        private d() {
        }
    }

    private static class a<Data> {

        /* renamed from: a  reason: collision with root package name */
        final AsyncTask f7203a;

        /* renamed from: b  reason: collision with root package name */
        final Data[] f7204b;

        a(AsyncTask asyncTask, Data... dataArr) {
            this.f7203a = asyncTask;
            this.f7204b = dataArr;
        }
    }
}
