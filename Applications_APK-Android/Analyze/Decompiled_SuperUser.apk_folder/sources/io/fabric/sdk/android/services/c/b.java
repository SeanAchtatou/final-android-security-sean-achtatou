package io.fabric.sdk.android.services.c;

import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.f;
import java.io.File;

/* compiled from: FileStoreImpl */
public class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private final Context f7101a;

    /* renamed from: b  reason: collision with root package name */
    private final String f7102b;

    /* renamed from: c  reason: collision with root package name */
    private final String f7103c;

    public b(f fVar) {
        if (fVar.getContext() == null) {
            throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
        }
        this.f7101a = fVar.getContext();
        this.f7102b = fVar.getPath();
        this.f7103c = "Android/" + this.f7101a.getPackageName();
    }

    public File a() {
        return a(this.f7101a.getFilesDir());
    }

    /* access modifiers changed from: package-private */
    public File a(File file) {
        if (file == null) {
            Fabric.h().a("Fabric", "Null File");
        } else if (file.exists() || file.mkdirs()) {
            return file;
        } else {
            Fabric.h().d("Fabric", "Couldn't create file");
        }
        return null;
    }
}
