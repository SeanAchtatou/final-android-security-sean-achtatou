package io.fabric.sdk.android.services.common;

/* compiled from: AdvertisingInfo */
class b {

    /* renamed from: a  reason: collision with root package name */
    public final String f7151a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f7152b;

    b(String str, boolean z) {
        this.f7151a = str;
        this.f7152b = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        if (this.f7152b != bVar.f7152b) {
            return false;
        }
        if (this.f7151a != null) {
            if (this.f7151a.equals(bVar.f7151a)) {
                return true;
            }
        } else if (bVar.f7151a == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        if (this.f7151a != null) {
            i = this.f7151a.hashCode();
        } else {
            i = 0;
        }
        int i3 = i * 31;
        if (this.f7152b) {
            i2 = 1;
        }
        return i3 + i2;
    }
}
