package io.fabric.sdk.android;

import io.fabric.sdk.android.services.common.n;
import io.fabric.sdk.android.services.concurrency.Priority;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import io.fabric.sdk.android.services.concurrency.c;

/* compiled from: InitializationTask */
class e<Result> extends c<Void, Void, Result> {

    /* renamed from: a  reason: collision with root package name */
    final f<Result> f7076a;

    public e(f<Result> fVar) {
        this.f7076a = fVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        n a2 = a("onPreExecute");
        try {
            boolean onPreExecute = this.f7076a.onPreExecute();
            a2.b();
            if (!onPreExecute) {
                a(true);
            }
        } catch (UnmetDependencyException e2) {
            throw e2;
        } catch (Exception e3) {
            Fabric.h().e("Fabric", "Failure onPreExecute()", e3);
            a2.b();
            a(true);
        } catch (Throwable th) {
            a2.b();
            a(true);
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public Result a(Void... voidArr) {
        n a2 = a("doInBackground");
        Result result = null;
        if (!d()) {
            result = this.f7076a.doInBackground();
        }
        a2.b();
        return result;
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        this.f7076a.onPostExecute(obj);
        this.f7076a.initializationCallback.a(obj);
    }

    /* access modifiers changed from: protected */
    public void b(Result result) {
        this.f7076a.onCancelled(result);
        this.f7076a.initializationCallback.a((Exception) new InitializationException(this.f7076a.getIdentifier() + " Initialization was cancelled"));
    }

    public Priority getPriority() {
        return Priority.HIGH;
    }

    private n a(String str) {
        n nVar = new n(this.f7076a.getIdentifier() + "." + str, "KitInitialization");
        nVar.a();
        return nVar;
    }
}
