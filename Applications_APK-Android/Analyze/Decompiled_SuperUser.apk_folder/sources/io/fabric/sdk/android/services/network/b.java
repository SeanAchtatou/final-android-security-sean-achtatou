package io.fabric.sdk.android.services.network;

import io.fabric.sdk.android.a;
import io.fabric.sdk.android.i;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: DefaultHttpRequestFactory */
public class b implements c {

    /* renamed from: a  reason: collision with root package name */
    private final i f7252a;

    /* renamed from: b  reason: collision with root package name */
    private e f7253b;

    /* renamed from: c  reason: collision with root package name */
    private SSLSocketFactory f7254c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f7255d;

    public b() {
        this(new a());
    }

    public b(i iVar) {
        this.f7252a = iVar;
    }

    public void a(e eVar) {
        if (this.f7253b != eVar) {
            this.f7253b = eVar;
            a();
        }
    }

    private synchronized void a() {
        this.f7255d = false;
        this.f7254c = null;
    }

    public HttpRequest a(HttpMethod httpMethod, String str) {
        return a(httpMethod, str, Collections.emptyMap());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.network.HttpRequest.a(java.lang.CharSequence, java.util.Map<?, ?>, boolean):io.fabric.sdk.android.services.network.HttpRequest
     arg types: [java.lang.String, java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      io.fabric.sdk.android.services.network.HttpRequest.a(java.lang.String, java.lang.String, java.lang.Number):io.fabric.sdk.android.services.network.HttpRequest
      io.fabric.sdk.android.services.network.HttpRequest.a(java.lang.String, java.lang.String, java.lang.String):io.fabric.sdk.android.services.network.HttpRequest
      io.fabric.sdk.android.services.network.HttpRequest.a(java.lang.CharSequence, java.util.Map<?, ?>, boolean):io.fabric.sdk.android.services.network.HttpRequest */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.network.HttpRequest.b(java.lang.CharSequence, java.util.Map<?, ?>, boolean):io.fabric.sdk.android.services.network.HttpRequest
     arg types: [java.lang.String, java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      io.fabric.sdk.android.services.network.HttpRequest.b(java.lang.String, java.lang.String, java.lang.String):io.fabric.sdk.android.services.network.HttpRequest
      io.fabric.sdk.android.services.network.HttpRequest.b(java.lang.CharSequence, java.util.Map<?, ?>, boolean):io.fabric.sdk.android.services.network.HttpRequest */
    public HttpRequest a(HttpMethod httpMethod, String str, Map<String, String> map) {
        HttpRequest e2;
        SSLSocketFactory b2;
        switch (httpMethod) {
            case GET:
                e2 = HttpRequest.a((CharSequence) str, (Map<?, ?>) map, true);
                break;
            case POST:
                e2 = HttpRequest.b((CharSequence) str, (Map<?, ?>) map, true);
                break;
            case PUT:
                e2 = HttpRequest.d((CharSequence) str);
                break;
            case DELETE:
                e2 = HttpRequest.e((CharSequence) str);
                break;
            default:
                throw new IllegalArgumentException("Unsupported HTTP method!");
        }
        if (!(!a(str) || this.f7253b == null || (b2 = b()) == null)) {
            ((HttpsURLConnection) e2.a()).setSSLSocketFactory(b2);
        }
        return e2;
    }

    private boolean a(String str) {
        return str != null && str.toLowerCase(Locale.US).startsWith("https");
    }

    private synchronized SSLSocketFactory b() {
        if (this.f7254c == null && !this.f7255d) {
            this.f7254c = c();
        }
        return this.f7254c;
    }

    private synchronized SSLSocketFactory c() {
        SSLSocketFactory sSLSocketFactory;
        this.f7255d = true;
        try {
            sSLSocketFactory = d.a(this.f7253b);
            this.f7252a.a("Fabric", "Custom SSL pinning enabled");
        } catch (Exception e2) {
            this.f7252a.e("Fabric", "Exception while validating pinned certs", e2);
            sSLSocketFactory = null;
        }
        return sSLSocketFactory;
    }
}
