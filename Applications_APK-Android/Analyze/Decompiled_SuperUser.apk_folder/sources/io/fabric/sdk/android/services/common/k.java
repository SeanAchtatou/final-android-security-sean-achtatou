package io.fabric.sdk.android.services.common;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: QueueFile */
public class k implements Closeable {

    /* renamed from: b  reason: collision with root package name */
    private static final Logger f7166b = Logger.getLogger(k.class.getName());

    /* renamed from: a  reason: collision with root package name */
    int f7167a;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final RandomAccessFile f7168c;

    /* renamed from: d  reason: collision with root package name */
    private int f7169d;

    /* renamed from: e  reason: collision with root package name */
    private a f7170e;

    /* renamed from: f  reason: collision with root package name */
    private a f7171f;

    /* renamed from: g  reason: collision with root package name */
    private final byte[] f7172g = new byte[16];

    /* compiled from: QueueFile */
    public interface c {
        void read(InputStream inputStream, int i);
    }

    public k(File file) {
        if (!file.exists()) {
            a(file);
        }
        this.f7168c = b(file);
        e();
    }

    private static void b(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    private static void a(byte[] bArr, int... iArr) {
        int i = 0;
        for (int b2 : iArr) {
            b(bArr, i, b2);
            i += 4;
        }
    }

    private static int a(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) + ((bArr[i + 1] & 255) << 16) + ((bArr[i + 2] & 255) << 8) + (bArr[i + 3] & 255);
    }

    private void e() {
        this.f7168c.seek(0);
        this.f7168c.readFully(this.f7172g);
        this.f7167a = a(this.f7172g, 0);
        if (((long) this.f7167a) > this.f7168c.length()) {
            throw new IOException("File is truncated. Expected length: " + this.f7167a + ", Actual length: " + this.f7168c.length());
        }
        this.f7169d = a(this.f7172g, 4);
        int a2 = a(this.f7172g, 8);
        int a3 = a(this.f7172g, 12);
        this.f7170e = a(a2);
        this.f7171f = a(a3);
    }

    private void a(int i, int i2, int i3, int i4) {
        a(this.f7172g, i, i2, i3, i4);
        this.f7168c.seek(0);
        this.f7168c.write(this.f7172g);
    }

    private a a(int i) {
        if (i == 0) {
            return a.f7176a;
        }
        this.f7168c.seek((long) i);
        return new a(i, this.f7168c.readInt());
    }

    /* JADX INFO: finally extract failed */
    private static void a(File file) {
        File file2 = new File(file.getPath() + ".tmp");
        RandomAccessFile b2 = b(file2);
        try {
            b2.setLength(4096);
            b2.seek(0);
            byte[] bArr = new byte[16];
            a(bArr, CodedOutputStream.DEFAULT_BUFFER_SIZE, 0, 0, 0);
            b2.write(bArr);
            b2.close();
            if (!file2.renameTo(file)) {
                throw new IOException("Rename failed!");
            }
        } catch (Throwable th) {
            b2.close();
            throw th;
        }
    }

    private static RandomAccessFile b(File file) {
        return new RandomAccessFile(file, "rwd");
    }

    /* access modifiers changed from: private */
    public int b(int i) {
        return i < this.f7167a ? i : (i + 16) - this.f7167a;
    }

    private void a(int i, byte[] bArr, int i2, int i3) {
        int b2 = b(i);
        if (b2 + i3 <= this.f7167a) {
            this.f7168c.seek((long) b2);
            this.f7168c.write(bArr, i2, i3);
            return;
        }
        int i4 = this.f7167a - b2;
        this.f7168c.seek((long) b2);
        this.f7168c.write(bArr, i2, i4);
        this.f7168c.seek(16);
        this.f7168c.write(bArr, i2 + i4, i3 - i4);
    }

    /* access modifiers changed from: private */
    public void b(int i, byte[] bArr, int i2, int i3) {
        int b2 = b(i);
        if (b2 + i3 <= this.f7167a) {
            this.f7168c.seek((long) b2);
            this.f7168c.readFully(bArr, i2, i3);
            return;
        }
        int i4 = this.f7167a - b2;
        this.f7168c.seek((long) b2);
        this.f7168c.readFully(bArr, i2, i4);
        this.f7168c.seek(16);
        this.f7168c.readFully(bArr, i2 + i4, i3 - i4);
    }

    public void a(byte[] bArr) {
        a(bArr, 0, bArr.length);
    }

    public synchronized void a(byte[] bArr, int i, int i2) {
        int b2;
        b(bArr, "buffer");
        if ((i | i2) < 0 || i2 > bArr.length - i) {
            throw new IndexOutOfBoundsException();
        }
        c(i2);
        boolean b3 = b();
        if (b3) {
            b2 = 16;
        } else {
            b2 = b(this.f7171f.f7177b + 4 + this.f7171f.f7178c);
        }
        a aVar = new a(b2, i2);
        b(this.f7172g, 0, i2);
        a(aVar.f7177b, this.f7172g, 0, 4);
        a(aVar.f7177b + 4, bArr, i, i2);
        a(this.f7167a, this.f7169d + 1, b3 ? aVar.f7177b : this.f7170e.f7177b, aVar.f7177b);
        this.f7171f = aVar;
        this.f7169d++;
        if (b3) {
            this.f7170e = this.f7171f;
        }
    }

    public int a() {
        if (this.f7169d == 0) {
            return 16;
        }
        if (this.f7171f.f7177b >= this.f7170e.f7177b) {
            return (this.f7171f.f7177b - this.f7170e.f7177b) + 4 + this.f7171f.f7178c + 16;
        }
        return (((this.f7171f.f7177b + 4) + this.f7171f.f7178c) + this.f7167a) - this.f7170e.f7177b;
    }

    private int f() {
        return this.f7167a - a();
    }

    public synchronized boolean b() {
        return this.f7169d == 0;
    }

    private void c(int i) {
        int i2 = i + 4;
        int f2 = f();
        if (f2 < i2) {
            int i3 = this.f7167a;
            do {
                f2 += i3;
                i3 <<= 1;
            } while (f2 < i2);
            d(i3);
            int b2 = b(this.f7171f.f7177b + 4 + this.f7171f.f7178c);
            if (b2 < this.f7170e.f7177b) {
                FileChannel channel = this.f7168c.getChannel();
                channel.position((long) this.f7167a);
                int i4 = b2 - 4;
                if (channel.transferTo(16, (long) i4, channel) != ((long) i4)) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            if (this.f7171f.f7177b < this.f7170e.f7177b) {
                int i5 = (this.f7167a + this.f7171f.f7177b) - 16;
                a(i3, this.f7169d, this.f7170e.f7177b, i5);
                this.f7171f = new a(i5, this.f7171f.f7178c);
            } else {
                a(i3, this.f7169d, this.f7170e.f7177b, this.f7171f.f7177b);
            }
            this.f7167a = i3;
        }
    }

    private void d(int i) {
        this.f7168c.setLength((long) i);
        this.f7168c.getChannel().force(true);
    }

    public synchronized void a(c cVar) {
        int i = this.f7170e.f7177b;
        for (int i2 = 0; i2 < this.f7169d; i2++) {
            a a2 = a(i);
            cVar.read(new b(a2), a2.f7178c);
            i = b(a2.f7178c + a2.f7177b + 4);
        }
    }

    /* access modifiers changed from: private */
    public static <T> T b(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    /* compiled from: QueueFile */
    private final class b extends InputStream {

        /* renamed from: b  reason: collision with root package name */
        private int f7180b;

        /* renamed from: c  reason: collision with root package name */
        private int f7181c;

        private b(a aVar) {
            this.f7180b = k.this.b(aVar.f7177b + 4);
            this.f7181c = aVar.f7178c;
        }

        public int read(byte[] bArr, int i, int i2) {
            Object unused = k.b(bArr, "buffer");
            if ((i | i2) < 0 || i2 > bArr.length - i) {
                throw new ArrayIndexOutOfBoundsException();
            } else if (this.f7181c <= 0) {
                return -1;
            } else {
                if (i2 > this.f7181c) {
                    i2 = this.f7181c;
                }
                k.this.b(this.f7180b, bArr, i, i2);
                this.f7180b = k.this.b(this.f7180b + i2);
                this.f7181c -= i2;
                return i2;
            }
        }

        public int read() {
            if (this.f7181c == 0) {
                return -1;
            }
            k.this.f7168c.seek((long) this.f7180b);
            int read = k.this.f7168c.read();
            this.f7180b = k.this.b(this.f7180b + 1);
            this.f7181c--;
            return read;
        }
    }

    public synchronized void c() {
        if (b()) {
            throw new NoSuchElementException();
        } else if (this.f7169d == 1) {
            d();
        } else {
            int b2 = b(this.f7170e.f7177b + 4 + this.f7170e.f7178c);
            b(b2, this.f7172g, 0, 4);
            int a2 = a(this.f7172g, 0);
            a(this.f7167a, this.f7169d - 1, b2, this.f7171f.f7177b);
            this.f7169d--;
            this.f7170e = new a(b2, a2);
        }
    }

    public synchronized void d() {
        a((int) CodedOutputStream.DEFAULT_BUFFER_SIZE, 0, 0, 0);
        this.f7169d = 0;
        this.f7170e = a.f7176a;
        this.f7171f = a.f7176a;
        if (this.f7167a > 4096) {
            d(CodedOutputStream.DEFAULT_BUFFER_SIZE);
        }
        this.f7167a = CodedOutputStream.DEFAULT_BUFFER_SIZE;
    }

    public synchronized void close() {
        this.f7168c.close();
    }

    public boolean a(int i, int i2) {
        return (a() + 4) + i <= i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName()).append('[');
        sb.append("fileLength=").append(this.f7167a);
        sb.append(", size=").append(this.f7169d);
        sb.append(", first=").append(this.f7170e);
        sb.append(", last=").append(this.f7171f);
        sb.append(", element lengths=[");
        try {
            a(new c() {

                /* renamed from: a  reason: collision with root package name */
                boolean f7173a = true;

                public void read(InputStream inputStream, int i) {
                    if (this.f7173a) {
                        this.f7173a = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(i);
                }
            });
        } catch (IOException e2) {
            f7166b.log(Level.WARNING, "read error", (Throwable) e2);
        }
        sb.append("]]");
        return sb.toString();
    }

    /* compiled from: QueueFile */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        static final a f7176a = new a(0, 0);

        /* renamed from: b  reason: collision with root package name */
        final int f7177b;

        /* renamed from: c  reason: collision with root package name */
        final int f7178c;

        a(int i, int i2) {
            this.f7177b = i;
            this.f7178c = i2;
        }

        public String toString() {
            return getClass().getSimpleName() + "[position = " + this.f7177b + ", length = " + this.f7178c + "]";
        }
    }
}
