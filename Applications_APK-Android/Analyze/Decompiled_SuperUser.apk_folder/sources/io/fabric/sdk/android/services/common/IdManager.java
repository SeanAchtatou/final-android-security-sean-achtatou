package io.fabric.sdk.android.services.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.f;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

public class IdManager {

    /* renamed from: d  reason: collision with root package name */
    private static final Pattern f7134d = Pattern.compile("[^\\p{Alnum}]");

    /* renamed from: e  reason: collision with root package name */
    private static final String f7135e = Pattern.quote("/");

    /* renamed from: a  reason: collision with root package name */
    AdvertisingInfoProvider f7136a;

    /* renamed from: b  reason: collision with root package name */
    b f7137b;

    /* renamed from: c  reason: collision with root package name */
    boolean f7138c;

    /* renamed from: f  reason: collision with root package name */
    private final ReentrantLock f7139f = new ReentrantLock();

    /* renamed from: g  reason: collision with root package name */
    private final j f7140g;

    /* renamed from: h  reason: collision with root package name */
    private final boolean f7141h;
    private final boolean i;
    private final Context j;
    private final String k;
    private final String l;
    private final Collection<f> m;

    public enum DeviceIdentifierType {
        WIFI_MAC_ADDRESS(1),
        BLUETOOTH_MAC_ADDRESS(2),
        FONT_TOKEN(53),
        ANDROID_ID(100),
        ANDROID_DEVICE_ID(101),
        ANDROID_SERIAL(102),
        ANDROID_ADVERTISING_ID(103);
        

        /* renamed from: h  reason: collision with root package name */
        public final int f7149h;

        private DeviceIdentifierType(int i2) {
            this.f7149h = i2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String, java.lang.String):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String, int):long
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String, java.lang.Throwable):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.io.OutputStream, byte[]):void
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String, boolean):boolean */
    public IdManager(Context context, String str, String str2, Collection<f> collection) {
        if (context == null) {
            throw new IllegalArgumentException("appContext must not be null");
        } else if (str == null) {
            throw new IllegalArgumentException("appIdentifier must not be null");
        } else if (collection == null) {
            throw new IllegalArgumentException("kits must not be null");
        } else {
            this.j = context;
            this.k = str;
            this.l = str2;
            this.m = collection;
            this.f7140g = new j();
            this.f7136a = new AdvertisingInfoProvider(context);
            this.f7141h = CommonUtils.a(context, "com.crashlytics.CollectDeviceIdentifiers", true);
            if (!this.f7141h) {
                Fabric.h().a("Fabric", "Device ID collection disabled for " + context.getPackageName());
            }
            this.i = CommonUtils.a(context, "com.crashlytics.CollectUserIdentifiers", true);
            if (!this.i) {
                Fabric.h().a("Fabric", "User information collection disabled for " + context.getPackageName());
            }
        }
    }

    public boolean a() {
        return this.i;
    }

    private String a(String str) {
        if (str == null) {
            return null;
        }
        return f7134d.matcher(str).replaceAll("").toLowerCase(Locale.US);
    }

    public String b() {
        String str = this.l;
        if (str != null) {
            return str;
        }
        SharedPreferences a2 = CommonUtils.a(this.j);
        String string = a2.getString("crashlytics.installation.id", null);
        if (string == null) {
            return a(a2);
        }
        return string;
    }

    public String c() {
        return this.k;
    }

    public String d() {
        return e() + "/" + f();
    }

    public String e() {
        return b(Build.VERSION.RELEASE);
    }

    public String f() {
        return b(Build.VERSION.INCREMENTAL);
    }

    public String g() {
        return String.format(Locale.US, "%s/%s", b(Build.MANUFACTURER), b(Build.MODEL));
    }

    private String b(String str) {
        return str.replaceAll(f7135e, "");
    }

    public String h() {
        if (!this.f7141h) {
            return "";
        }
        String n = n();
        if (n != null) {
            return n;
        }
        SharedPreferences a2 = CommonUtils.a(this.j);
        String string = a2.getString("crashlytics.installation.id", null);
        if (string == null) {
            return a(a2);
        }
        return string;
    }

    private String a(SharedPreferences sharedPreferences) {
        this.f7139f.lock();
        try {
            String string = sharedPreferences.getString("crashlytics.installation.id", null);
            if (string == null) {
                string = a(UUID.randomUUID().toString());
                sharedPreferences.edit().putString("crashlytics.installation.id", string).commit();
            }
            return string;
        } finally {
            this.f7139f.unlock();
        }
    }

    public Map<DeviceIdentifierType, String> i() {
        HashMap hashMap = new HashMap();
        for (f next : this.m) {
            if (next instanceof h) {
                for (Map.Entry next2 : ((h) next).getDeviceIdentifiers().entrySet()) {
                    a(hashMap, (DeviceIdentifierType) next2.getKey(), (String) next2.getValue());
                }
            }
        }
        a(hashMap, DeviceIdentifierType.ANDROID_ID, n());
        a(hashMap, DeviceIdentifierType.ANDROID_ADVERTISING_ID, m());
        return Collections.unmodifiableMap(hashMap);
    }

    public String j() {
        return this.f7140g.a(this.j);
    }

    /* access modifiers changed from: package-private */
    public synchronized b k() {
        if (!this.f7138c) {
            this.f7137b = this.f7136a.a();
            this.f7138c = true;
        }
        return this.f7137b;
    }

    public Boolean l() {
        b k2;
        if (!this.f7141h || (k2 = k()) == null) {
            return null;
        }
        return Boolean.valueOf(k2.f7152b);
    }

    public String m() {
        b k2;
        if (!this.f7141h || (k2 = k()) == null) {
            return null;
        }
        return k2.f7151a;
    }

    private void a(Map<DeviceIdentifierType, String> map, DeviceIdentifierType deviceIdentifierType, String str) {
        if (str != null) {
            map.put(deviceIdentifierType, str);
        }
    }

    public String n() {
        if (!this.f7141h) {
            return null;
        }
        String string = Settings.Secure.getString(this.j.getContentResolver(), "android_id");
        if (!"9774d56d682e549c".equals(string)) {
            return a(string);
        }
        return null;
    }
}
