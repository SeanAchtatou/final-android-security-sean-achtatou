package io.fabric.sdk.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import io.fabric.sdk.android.ActivityLifecycleManager;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import io.fabric.sdk.android.services.concurrency.b;
import io.fabric.sdk.android.services.concurrency.i;
import io.fabric.sdk.android.services.concurrency.j;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

public class Fabric {

    /* renamed from: a  reason: collision with root package name */
    static volatile Fabric f7051a;

    /* renamed from: b  reason: collision with root package name */
    static final i f7052b = new a();

    /* renamed from: c  reason: collision with root package name */
    final i f7053c;

    /* renamed from: d  reason: collision with root package name */
    final boolean f7054d;

    /* renamed from: e  reason: collision with root package name */
    private final Context f7055e;

    /* renamed from: f  reason: collision with root package name */
    private final Map<Class<? extends f>, f> f7056f;

    /* renamed from: g  reason: collision with root package name */
    private final ExecutorService f7057g;

    /* renamed from: h  reason: collision with root package name */
    private final Handler f7058h;
    /* access modifiers changed from: private */
    public final d<Fabric> i;
    private final d<?> j;
    private final IdManager k;
    private ActivityLifecycleManager l;
    private WeakReference<Activity> m;
    /* access modifiers changed from: private */
    public AtomicBoolean n = new AtomicBoolean(false);

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final Context f7063a;

        /* renamed from: b  reason: collision with root package name */
        private f[] f7064b;

        /* renamed from: c  reason: collision with root package name */
        private i f7065c;

        /* renamed from: d  reason: collision with root package name */
        private Handler f7066d;

        /* renamed from: e  reason: collision with root package name */
        private i f7067e;

        /* renamed from: f  reason: collision with root package name */
        private boolean f7068f;

        /* renamed from: g  reason: collision with root package name */
        private String f7069g;

        /* renamed from: h  reason: collision with root package name */
        private String f7070h;
        private d<Fabric> i;

        public Builder(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.f7063a = context;
        }

        public Builder a(f... fVarArr) {
            if (this.f7064b != null) {
                throw new IllegalStateException("Kits already set.");
            }
            this.f7064b = fVarArr;
            return this;
        }

        public Builder a(boolean z) {
            this.f7068f = z;
            return this;
        }

        public Fabric a() {
            Map a2;
            if (this.f7065c == null) {
                this.f7065c = i.a();
            }
            if (this.f7066d == null) {
                this.f7066d = new Handler(Looper.getMainLooper());
            }
            if (this.f7067e == null) {
                if (this.f7068f) {
                    this.f7067e = new a(3);
                } else {
                    this.f7067e = new a();
                }
            }
            if (this.f7070h == null) {
                this.f7070h = this.f7063a.getPackageName();
            }
            if (this.i == null) {
                this.i = d.f7075d;
            }
            if (this.f7064b == null) {
                a2 = new HashMap();
            } else {
                a2 = Fabric.b(Arrays.asList(this.f7064b));
            }
            Context applicationContext = this.f7063a.getApplicationContext();
            return new Fabric(applicationContext, a2, this.f7065c, this.f7066d, this.f7067e, this.f7068f, this.i, new IdManager(applicationContext, this.f7070h, this.f7069g, a2.values()), Fabric.d(this.f7063a));
        }
    }

    static Fabric a() {
        if (f7051a != null) {
            return f7051a;
        }
        throw new IllegalStateException("Must Initialize Fabric before using singleton()");
    }

    Fabric(Context context, Map<Class<? extends f>, f> map, i iVar, Handler handler, i iVar2, boolean z, d dVar, IdManager idManager, Activity activity) {
        this.f7055e = context;
        this.f7056f = map;
        this.f7057g = iVar;
        this.f7058h = handler;
        this.f7053c = iVar2;
        this.f7054d = z;
        this.i = dVar;
        this.j = a(map.size());
        this.k = idManager;
        a(activity);
    }

    public static Fabric a(Fabric fabric) {
        if (f7051a == null) {
            synchronized (Fabric.class) {
                if (f7051a == null) {
                    d(fabric);
                }
            }
        }
        return f7051a;
    }

    private static void d(Fabric fabric) {
        f7051a = fabric;
        fabric.j();
    }

    public Fabric a(Activity activity) {
        this.m = new WeakReference<>(activity);
        return this;
    }

    public Activity b() {
        if (this.m != null) {
            return this.m.get();
        }
        return null;
    }

    private void j() {
        this.l = new ActivityLifecycleManager(this.f7055e);
        this.l.a(new ActivityLifecycleManager.b() {
            public void onActivityCreated(Activity activity, Bundle bundle) {
                Fabric.this.a(activity);
            }

            public void onActivityStarted(Activity activity) {
                Fabric.this.a(activity);
            }

            public void onActivityResumed(Activity activity) {
                Fabric.this.a(activity);
            }
        });
        a(this.f7055e);
    }

    public String c() {
        return "1.3.17.dev";
    }

    public String d() {
        return "io.fabric.sdk.android:fabric";
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        StringBuilder sb;
        Future<Map<String, h>> b2 = b(context);
        Collection<f> g2 = g();
        j jVar = new j(b2, g2);
        ArrayList<f> arrayList = new ArrayList<>(g2);
        Collections.sort(arrayList);
        jVar.injectParameters(context, this, d.f7075d, this.k);
        for (f injectParameters : arrayList) {
            injectParameters.injectParameters(context, this, this.j, this.k);
        }
        jVar.initialize();
        if (h().a("Fabric", 3)) {
            sb = new StringBuilder("Initializing ").append(d()).append(" [Version: ").append(c()).append("], with the following kits:\n");
        } else {
            sb = null;
        }
        for (f fVar : arrayList) {
            fVar.initializationTask.addDependency((j) jVar.initializationTask);
            a(this.f7056f, fVar);
            fVar.initialize();
            if (sb != null) {
                sb.append(fVar.getIdentifier()).append(" [Version: ").append(fVar.getVersion()).append("]\n");
            }
        }
        if (sb != null) {
            h().a("Fabric", sb.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Map<Class<? extends f>, f> map, f fVar) {
        b bVar = fVar.dependsOnAnnotation;
        if (bVar != null) {
            for (Class<?> cls : bVar.a()) {
                if (cls.isInterface()) {
                    for (f next : map.values()) {
                        if (cls.isAssignableFrom(next.getClass())) {
                            fVar.initializationTask.addDependency((j) next.initializationTask);
                        }
                    }
                } else if (map.get(cls) == null) {
                    throw new UnmetDependencyException("Referenced Kit was null, does the kit exist?");
                } else {
                    fVar.initializationTask.addDependency((j) map.get(cls).initializationTask);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static Activity d(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        return null;
    }

    public ActivityLifecycleManager e() {
        return this.l;
    }

    public ExecutorService f() {
        return this.f7057g;
    }

    public Collection<f> g() {
        return this.f7056f.values();
    }

    public static <T extends f> T a(Class cls) {
        return (f) a().f7056f.get(cls);
    }

    public static i h() {
        if (f7051a == null) {
            return f7052b;
        }
        return f7051a.f7053c;
    }

    public static boolean i() {
        if (f7051a == null) {
            return false;
        }
        return f7051a.f7054d;
    }

    /* access modifiers changed from: private */
    public static Map<Class<? extends f>, f> b(Collection<? extends f> collection) {
        HashMap hashMap = new HashMap(collection.size());
        a(hashMap, collection);
        return hashMap;
    }

    private static void a(Map<Class<? extends f>, f> map, Collection<? extends f> collection) {
        for (f fVar : collection) {
            map.put(fVar.getClass(), fVar);
            if (fVar instanceof g) {
                a(map, ((g) fVar).getKits());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public d<?> a(final int i2) {
        return new d() {

            /* renamed from: a  reason: collision with root package name */
            final CountDownLatch f7060a = new CountDownLatch(i2);

            public void a(Object obj) {
                this.f7060a.countDown();
                if (this.f7060a.getCount() == 0) {
                    Fabric.this.n.set(true);
                    Fabric.this.i.a(Fabric.this);
                }
            }

            public void a(Exception exc) {
                Fabric.this.i.a(exc);
            }
        };
    }

    /* access modifiers changed from: package-private */
    public Future<Map<String, h>> b(Context context) {
        return f().submit(new c(context.getPackageCodePath()));
    }
}
