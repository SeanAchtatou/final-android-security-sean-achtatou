package io.fabric.sdk.android.services.c;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import io.fabric.sdk.android.f;

/* compiled from: PreferenceStoreImpl */
public class d implements c {

    /* renamed from: a  reason: collision with root package name */
    private final SharedPreferences f7104a;

    /* renamed from: b  reason: collision with root package name */
    private final String f7105b;

    /* renamed from: c  reason: collision with root package name */
    private final Context f7106c;

    public d(Context context, String str) {
        if (context == null) {
            throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
        }
        this.f7106c = context;
        this.f7105b = str;
        this.f7104a = this.f7106c.getSharedPreferences(this.f7105b, 0);
    }

    @Deprecated
    public d(f fVar) {
        this(fVar.getContext(), fVar.getClass().getName());
    }

    public SharedPreferences a() {
        return this.f7104a;
    }

    public SharedPreferences.Editor b() {
        return this.f7104a.edit();
    }

    @TargetApi(9)
    public boolean a(SharedPreferences.Editor editor) {
        if (Build.VERSION.SDK_INT < 9) {
            return editor.commit();
        }
        editor.apply();
        return true;
    }
}
