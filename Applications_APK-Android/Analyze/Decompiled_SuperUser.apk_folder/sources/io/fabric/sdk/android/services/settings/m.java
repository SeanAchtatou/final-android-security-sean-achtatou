package io.fabric.sdk.android.services.settings;

/* compiled from: FeaturesSettingsData */
public class m {

    /* renamed from: a  reason: collision with root package name */
    public final boolean f7304a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f7305b;

    /* renamed from: c  reason: collision with root package name */
    public final boolean f7306c;

    /* renamed from: d  reason: collision with root package name */
    public final boolean f7307d;

    public m(boolean z, boolean z2, boolean z3, boolean z4) {
        this.f7304a = z;
        this.f7305b = z2;
        this.f7306c = z3;
        this.f7307d = z4;
    }
}
