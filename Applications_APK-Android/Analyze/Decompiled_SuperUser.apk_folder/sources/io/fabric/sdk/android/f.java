package io.fabric.sdk.android;

import android.content.Context;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.concurrency.b;
import io.fabric.sdk.android.services.concurrency.j;
import java.io.File;
import java.util.Collection;

/* compiled from: Kit */
public abstract class f<Result> implements Comparable<f> {
    Context context;
    final b dependsOnAnnotation = ((b) getClass().getAnnotation(b.class));
    Fabric fabric;
    IdManager idManager;
    d<Result> initializationCallback;
    e<Result> initializationTask = new e<>(this);

    /* access modifiers changed from: protected */
    public abstract Result doInBackground();

    public abstract String getIdentifier();

    public abstract String getVersion();

    /* access modifiers changed from: package-private */
    public void injectParameters(Context context2, Fabric fabric2, d<Result> dVar, IdManager idManager2) {
        this.fabric = fabric2;
        this.context = new b(context2, getIdentifier(), getPath());
        this.initializationCallback = dVar;
        this.idManager = idManager2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.concurrency.c.a(java.util.concurrent.ExecutorService, java.lang.Object[]):void
     arg types: [java.util.concurrent.ExecutorService, java.lang.Void[]]
     candidates:
      io.fabric.sdk.android.services.concurrency.AsyncTask.a(io.fabric.sdk.android.services.concurrency.AsyncTask, java.lang.Object):java.lang.Object
      io.fabric.sdk.android.services.concurrency.AsyncTask.a(java.util.concurrent.Executor, java.lang.Object[]):io.fabric.sdk.android.services.concurrency.AsyncTask<Params, Progress, Result>
      io.fabric.sdk.android.services.concurrency.c.a(java.util.concurrent.ExecutorService, java.lang.Object[]):void */
    /* access modifiers changed from: package-private */
    public final void initialize() {
        this.initializationTask.a(this.fabric.f(), (Object[]) new Void[]{null});
    }

    /* access modifiers changed from: protected */
    public boolean onPreExecute() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Result result) {
    }

    /* access modifiers changed from: protected */
    public void onCancelled(Result result) {
    }

    /* access modifiers changed from: protected */
    public IdManager getIdManager() {
        return this.idManager;
    }

    public Context getContext() {
        return this.context;
    }

    public Fabric getFabric() {
        return this.fabric;
    }

    public String getPath() {
        return ".Fabric" + File.separator + getIdentifier();
    }

    public int compareTo(f fVar) {
        if (containsAnnotatedDependency(fVar)) {
            return 1;
        }
        if (fVar.containsAnnotatedDependency(this)) {
            return -1;
        }
        if (hasAnnotatedDependency() && !fVar.hasAnnotatedDependency()) {
            return 1;
        }
        if (hasAnnotatedDependency() || !fVar.hasAnnotatedDependency()) {
            return 0;
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public boolean containsAnnotatedDependency(f fVar) {
        if (!hasAnnotatedDependency()) {
            return false;
        }
        for (Class<?> isAssignableFrom : this.dependsOnAnnotation.a()) {
            if (isAssignableFrom.isAssignableFrom(fVar.getClass())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean hasAnnotatedDependency() {
        return this.dependsOnAnnotation != null;
    }

    /* access modifiers changed from: protected */
    public Collection<j> getDependencies() {
        return this.initializationTask.getDependencies();
    }
}
