package io.fabric.sdk.android.services.a;

import android.content.Context;

/* compiled from: AbstractValueCache */
public abstract class a<T> implements c<T> {

    /* renamed from: a  reason: collision with root package name */
    private final c<T> f7088a;

    /* access modifiers changed from: protected */
    public abstract T a(Context context);

    /* access modifiers changed from: protected */
    public abstract void a(Context context, String str);

    public a(c<T> cVar) {
        this.f7088a = cVar;
    }

    public final synchronized T a(Context context, d<String> dVar) {
        T a2;
        a2 = a(context);
        if (a2 == null) {
            a2 = this.f7088a != null ? this.f7088a.a(context, dVar) : dVar.load(context);
            b(context, a2);
        }
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.a.a.a(android.content.Context, java.lang.String):void
     arg types: [android.content.Context, T]
     candidates:
      io.fabric.sdk.android.services.a.a.a(android.content.Context, io.fabric.sdk.android.services.a.d<java.lang.String>):T
      io.fabric.sdk.android.services.a.c.a(android.content.Context, io.fabric.sdk.android.services.a.d):T
      io.fabric.sdk.android.services.a.a.a(android.content.Context, java.lang.String):void */
    private void b(Context context, T t) {
        if (t == null) {
            throw new NullPointerException();
        }
        a(context, (String) t);
    }
}
