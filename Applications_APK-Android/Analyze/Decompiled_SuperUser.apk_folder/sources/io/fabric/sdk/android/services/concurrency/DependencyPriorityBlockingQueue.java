package io.fabric.sdk.android.services.concurrency;

import io.fabric.sdk.android.services.concurrency.a;
import io.fabric.sdk.android.services.concurrency.f;
import io.fabric.sdk.android.services.concurrency.j;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class DependencyPriorityBlockingQueue<E extends a & j & f> extends PriorityBlockingQueue<E> {

    /* renamed from: a  reason: collision with root package name */
    final Queue<E> f7210a = new LinkedList();

    /* renamed from: b  reason: collision with root package name */
    private final ReentrantLock f7211b = new ReentrantLock();

    /* renamed from: a */
    public E take() {
        return b(0, null, null);
    }

    /* renamed from: b */
    public E peek() {
        try {
            return b(1, null, null);
        } catch (InterruptedException e2) {
            return null;
        }
    }

    /* renamed from: a */
    public E poll(long j, TimeUnit timeUnit) {
        return b(3, Long.valueOf(j), timeUnit);
    }

    /* renamed from: c */
    public E poll() {
        try {
            return b(2, null, null);
        } catch (InterruptedException e2) {
            return null;
        }
    }

    public int size() {
        try {
            this.f7211b.lock();
            return this.f7210a.size() + super.size();
        } finally {
            this.f7211b.unlock();
        }
    }

    public <T> T[] toArray(T[] tArr) {
        try {
            this.f7211b.lock();
            return a(super.toArray(tArr), this.f7210a.toArray(tArr));
        } finally {
            this.f7211b.unlock();
        }
    }

    public Object[] toArray() {
        try {
            this.f7211b.lock();
            return a(super.toArray(), this.f7210a.toArray());
        } finally {
            this.f7211b.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection) {
        try {
            this.f7211b.lock();
            int drainTo = super.drainTo(collection) + this.f7210a.size();
            while (!this.f7210a.isEmpty()) {
                collection.add(this.f7210a.poll());
            }
            return drainTo;
        } finally {
            this.f7211b.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection, int i) {
        try {
            this.f7211b.lock();
            int drainTo = super.drainTo(collection, i);
            while (!this.f7210a.isEmpty() && drainTo <= i) {
                collection.add(this.f7210a.poll());
                drainTo++;
            }
            return drainTo;
        } finally {
            this.f7211b.unlock();
        }
    }

    public boolean contains(Object obj) {
        try {
            this.f7211b.lock();
            return super.contains(obj) || this.f7210a.contains(obj);
        } finally {
            this.f7211b.unlock();
        }
    }

    public void clear() {
        try {
            this.f7211b.lock();
            this.f7210a.clear();
            super.clear();
        } finally {
            this.f7211b.unlock();
        }
    }

    public boolean remove(Object obj) {
        try {
            this.f7211b.lock();
            return super.remove(obj) || this.f7210a.remove(obj);
        } finally {
            this.f7211b.unlock();
        }
    }

    public boolean removeAll(Collection<?> collection) {
        try {
            this.f7211b.lock();
            return super.removeAll(collection) | this.f7210a.removeAll(collection);
        } finally {
            this.f7211b.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public E a(int i, Long l, TimeUnit timeUnit) {
        switch (i) {
            case 0:
                return (a) super.take();
            case 1:
                return (a) super.peek();
            case 2:
                return (a) super.poll();
            case 3:
                return (a) super.poll(l.longValue(), timeUnit);
            default:
                return null;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, E e2) {
        try {
            this.f7211b.lock();
            if (i == 1) {
                super.remove(e2);
            }
            return this.f7210a.offer(e2);
        } finally {
            this.f7211b.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.concurrency.DependencyPriorityBlockingQueue.a(int, io.fabric.sdk.android.services.concurrency.a):boolean
     arg types: [int, E]
     candidates:
      io.fabric.sdk.android.services.concurrency.DependencyPriorityBlockingQueue.a(long, java.util.concurrent.TimeUnit):E
      io.fabric.sdk.android.services.concurrency.DependencyPriorityBlockingQueue.a(java.lang.Object[], java.lang.Object[]):T[]
      io.fabric.sdk.android.services.concurrency.DependencyPriorityBlockingQueue.a(int, io.fabric.sdk.android.services.concurrency.a):boolean */
    /* access modifiers changed from: package-private */
    public E b(int i, Long l, TimeUnit timeUnit) {
        E a2;
        while (true) {
            a2 = a(i, l, timeUnit);
            if (a2 == null || a(a2)) {
                return a2;
            }
            a(i, (a) a2);
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(E e2) {
        return e2.areDependenciesMet();
    }

    public void d() {
        try {
            this.f7211b.lock();
            Iterator<E> it = this.f7210a.iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                if (a(aVar)) {
                    super.offer(aVar);
                    it.remove();
                }
            }
        } finally {
            this.f7211b.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public <T> T[] a(T[] tArr, T[] tArr2) {
        int length = tArr.length;
        int length2 = tArr2.length;
        T[] tArr3 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), length + length2);
        System.arraycopy(tArr, 0, tArr3, 0, length);
        System.arraycopy(tArr2, 0, tArr3, length, length2);
        return tArr3;
    }
}
