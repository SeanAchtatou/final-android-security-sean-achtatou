package io.fabric.sdk.android.services.concurrency;

import android.annotation.TargetApi;
import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: PriorityThreadPoolExecutor */
public class i extends ThreadPoolExecutor {

    /* renamed from: a  reason: collision with root package name */
    private static final int f7228a = Runtime.getRuntime().availableProcessors();

    /* renamed from: b  reason: collision with root package name */
    private static final int f7229b = (f7228a + 1);

    /* renamed from: c  reason: collision with root package name */
    private static final int f7230c = ((f7228a * 2) + 1);

    <T extends Runnable & a & j & f> i(int i, int i2, long j, TimeUnit timeUnit, DependencyPriorityBlockingQueue<T> dependencyPriorityBlockingQueue, ThreadFactory threadFactory) {
        super(i, i2, j, timeUnit, dependencyPriorityBlockingQueue, threadFactory);
        prestartAllCoreThreads();
    }

    public static <T extends Runnable & a & j & f> i a(int i, int i2) {
        return new i(i, i2, 1, TimeUnit.SECONDS, new DependencyPriorityBlockingQueue(), new a(10));
    }

    public static i a() {
        return a(f7229b, f7230c);
    }

    /* access modifiers changed from: protected */
    public <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new e(runnable, t);
    }

    /* access modifiers changed from: protected */
    public <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return new e(callable);
    }

    @TargetApi(9)
    public void execute(Runnable runnable) {
        if (h.isProperDelegate(runnable)) {
            super.execute(runnable);
        } else {
            super.execute(newTaskFor(runnable, null));
        }
    }

    /* access modifiers changed from: protected */
    public void afterExecute(Runnable runnable, Throwable th) {
        j jVar = (j) runnable;
        jVar.setFinished(true);
        jVar.setError(th);
        getQueue().d();
        super.afterExecute(runnable, th);
    }

    /* renamed from: b */
    public DependencyPriorityBlockingQueue getQueue() {
        return (DependencyPriorityBlockingQueue) super.getQueue();
    }

    /* compiled from: PriorityThreadPoolExecutor */
    protected static final class a implements ThreadFactory {

        /* renamed from: a  reason: collision with root package name */
        private final int f7231a;

        public a(int i) {
            this.f7231a = i;
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setPriority(this.f7231a);
            thread.setName("Queue");
            return thread;
        }
    }
}
