package io.fabric.sdk.android.services.settings;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.f;
import io.fabric.sdk.android.services.c.b;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import org.json.JSONObject;

/* compiled from: DefaultCachedSettingsIo */
class i implements g {

    /* renamed from: a  reason: collision with root package name */
    private final f f7296a;

    public i(f fVar) {
        this.f7296a = fVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.FileInputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    public JSONObject a() {
        FileInputStream fileInputStream;
        JSONObject jSONObject;
        FileInputStream fileInputStream2 = null;
        Fabric.h().a("Fabric", "Reading cached settings...");
        try {
            File file = new File(new b(this.f7296a).a(), "com.crashlytics.settings.json");
            if (file.exists()) {
                fileInputStream = new FileInputStream(file);
                try {
                    jSONObject = new JSONObject(CommonUtils.a((InputStream) fileInputStream));
                    fileInputStream2 = fileInputStream;
                } catch (Exception e2) {
                    e = e2;
                    try {
                        Fabric.h().e("Fabric", "Failed to fetch cached settings", e);
                        CommonUtils.a((Closeable) fileInputStream, "Error while closing settings cache file.");
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        fileInputStream2 = fileInputStream;
                        CommonUtils.a((Closeable) fileInputStream2, "Error while closing settings cache file.");
                        throw th;
                    }
                }
            } else {
                Fabric.h().a("Fabric", "No cached settings found.");
                jSONObject = null;
            }
            CommonUtils.a((Closeable) fileInputStream2, "Error while closing settings cache file.");
            return jSONObject;
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            Fabric.h().e("Fabric", "Failed to fetch cached settings", e);
            CommonUtils.a((Closeable) fileInputStream, "Error while closing settings cache file.");
            return null;
        } catch (Throwable th2) {
            th = th2;
            CommonUtils.a((Closeable) fileInputStream2, "Error while closing settings cache file.");
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.FileWriter, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    public void a(long j, JSONObject jSONObject) {
        FileWriter fileWriter;
        Fabric.h().a("Fabric", "Writing settings to cache file...");
        if (jSONObject != null) {
            FileWriter fileWriter2 = null;
            try {
                jSONObject.put("expires_at", j);
                fileWriter = new FileWriter(new File(new b(this.f7296a).a(), "com.crashlytics.settings.json"));
                try {
                    fileWriter.write(jSONObject.toString());
                    fileWriter.flush();
                    CommonUtils.a((Closeable) fileWriter, "Failed to close settings writer.");
                } catch (Exception e2) {
                    e = e2;
                    try {
                        Fabric.h().e("Fabric", "Failed to cache settings", e);
                        CommonUtils.a((Closeable) fileWriter, "Failed to close settings writer.");
                    } catch (Throwable th) {
                        th = th;
                        fileWriter2 = fileWriter;
                        CommonUtils.a((Closeable) fileWriter2, "Failed to close settings writer.");
                        throw th;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                fileWriter = null;
                Fabric.h().e("Fabric", "Failed to cache settings", e);
                CommonUtils.a((Closeable) fileWriter, "Failed to close settings writer.");
            } catch (Throwable th2) {
                th = th2;
                CommonUtils.a((Closeable) fileWriter2, "Failed to close settings writer.");
                throw th;
            }
        }
    }
}
