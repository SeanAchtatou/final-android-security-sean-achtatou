package io.fabric.sdk.android;

/* compiled from: KitInfo */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private final String f7077a;

    /* renamed from: b  reason: collision with root package name */
    private final String f7078b;

    /* renamed from: c  reason: collision with root package name */
    private final String f7079c;

    public h(String str, String str2, String str3) {
        this.f7077a = str;
        this.f7078b = str2;
        this.f7079c = str3;
    }

    public String a() {
        return this.f7077a;
    }

    public String b() {
        return this.f7078b;
    }

    public String c() {
        return this.f7079c;
    }
}
