package io.fabric.sdk.android.services.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;

public class HttpRequest {

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f7237b = new String[0];

    /* renamed from: c  reason: collision with root package name */
    private static b f7238c = b.f7250a;

    /* renamed from: a  reason: collision with root package name */
    public final URL f7239a;

    /* renamed from: d  reason: collision with root package name */
    private HttpURLConnection f7240d = null;

    /* renamed from: e  reason: collision with root package name */
    private final String f7241e;

    /* renamed from: f  reason: collision with root package name */
    private d f7242f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f7243g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f7244h = true;
    private boolean i = false;
    /* access modifiers changed from: private */
    public int j = 8192;
    private String k;
    private int l;

    public interface b {

        /* renamed from: a  reason: collision with root package name */
        public static final b f7250a = new b() {
            public HttpURLConnection a(URL url) {
                return (HttpURLConnection) url.openConnection();
            }

            public HttpURLConnection a(URL url, Proxy proxy) {
                return (HttpURLConnection) url.openConnection(proxy);
            }
        };

        HttpURLConnection a(URL url);

        HttpURLConnection a(URL url, Proxy proxy);
    }

    /* access modifiers changed from: private */
    public static String f(String str) {
        return (str == null || str.length() <= 0) ? "UTF-8" : str;
    }

    private static StringBuilder a(String str, StringBuilder sb) {
        if (str.indexOf(58) + 2 == str.lastIndexOf(47)) {
            sb.append('/');
        }
        return sb;
    }

    private static StringBuilder b(String str, StringBuilder sb) {
        int indexOf = str.indexOf(63);
        int length = sb.length() - 1;
        if (indexOf == -1) {
            sb.append('?');
        } else if (indexOf < length && str.charAt(length) != '&') {
            sb.append('&');
        }
        return sb;
    }

    public static class HttpRequestException extends RuntimeException {
        protected HttpRequestException(IOException iOException) {
            super(iOException);
        }

        /* renamed from: a */
        public IOException getCause() {
            return (IOException) super.getCause();
        }
    }

    protected static abstract class c<V> implements Callable<V> {
        /* access modifiers changed from: protected */
        public abstract V b();

        /* access modifiers changed from: protected */
        public abstract void c();

        protected c() {
        }

        public V call() {
            boolean z = true;
            try {
                V b2 = b();
                try {
                    c();
                    return b2;
                } catch (IOException e2) {
                    throw new HttpRequestException(e2);
                }
            } catch (HttpRequestException e3) {
                throw e3;
            } catch (IOException e4) {
                throw new HttpRequestException(e4);
            } catch (Throwable th) {
                th = th;
            }
            try {
                c();
            } catch (IOException e5) {
                if (!z) {
                    throw new HttpRequestException(e5);
                }
            }
            throw th;
        }
    }

    protected static abstract class a<V> extends c<V> {

        /* renamed from: a  reason: collision with root package name */
        private final Closeable f7248a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f7249b;

        protected a(Closeable closeable, boolean z) {
            this.f7248a = closeable;
            this.f7249b = z;
        }

        /* access modifiers changed from: protected */
        public void c() {
            if (this.f7248a instanceof Flushable) {
                ((Flushable) this.f7248a).flush();
            }
            if (this.f7249b) {
                try {
                    this.f7248a.close();
                } catch (IOException e2) {
                }
            } else {
                this.f7248a.close();
            }
        }
    }

    public static class d extends BufferedOutputStream {

        /* renamed from: a  reason: collision with root package name */
        private final CharsetEncoder f7251a;

        public d(OutputStream outputStream, String str, int i) {
            super(outputStream, i);
            this.f7251a = Charset.forName(HttpRequest.f(str)).newEncoder();
        }

        public d a(String str) {
            ByteBuffer encode = this.f7251a.encode(CharBuffer.wrap(str));
            super.write(encode.array(), 0, encode.limit());
            return this;
        }
    }

    public static String a(CharSequence charSequence) {
        try {
            URL url = new URL(charSequence.toString());
            String host = url.getHost();
            int port = url.getPort();
            if (port != -1) {
                host = host + ':' + Integer.toString(port);
            }
            try {
                String aSCIIString = new URI(url.getProtocol(), host, url.getPath(), url.getQuery(), null).toASCIIString();
                int indexOf = aSCIIString.indexOf(63);
                if (indexOf <= 0 || indexOf + 1 >= aSCIIString.length()) {
                    return aSCIIString;
                }
                return aSCIIString.substring(0, indexOf + 1) + aSCIIString.substring(indexOf + 1).replace("+", "%2B");
            } catch (URISyntaxException e2) {
                IOException iOException = new IOException("Parsing URI failed");
                iOException.initCause(e2);
                throw new HttpRequestException(iOException);
            }
        } catch (IOException e3) {
            throw new HttpRequestException(e3);
        }
    }

    public static String a(CharSequence charSequence, Map<?, ?> map) {
        String charSequence2 = charSequence.toString();
        if (map == null || map.isEmpty()) {
            return charSequence2;
        }
        StringBuilder sb = new StringBuilder(charSequence2);
        a(charSequence2, sb);
        b(charSequence2, sb);
        Iterator<Map.Entry<?, ?>> it = map.entrySet().iterator();
        Map.Entry next = it.next();
        sb.append(next.getKey().toString());
        sb.append('=');
        Object value = next.getValue();
        if (value != null) {
            sb.append(value);
        }
        while (it.hasNext()) {
            sb.append('&');
            Map.Entry next2 = it.next();
            sb.append(next2.getKey().toString());
            sb.append('=');
            Object value2 = next2.getValue();
            if (value2 != null) {
                sb.append(value2);
            }
        }
        return sb.toString();
    }

    public static HttpRequest b(CharSequence charSequence) {
        return new HttpRequest(charSequence, "GET");
    }

    public static HttpRequest a(CharSequence charSequence, Map<?, ?> map, boolean z) {
        String a2 = a(charSequence, map);
        if (z) {
            a2 = a((CharSequence) a2);
        }
        return b((CharSequence) a2);
    }

    public static HttpRequest c(CharSequence charSequence) {
        return new HttpRequest(charSequence, "POST");
    }

    public static HttpRequest b(CharSequence charSequence, Map<?, ?> map, boolean z) {
        String a2 = a(charSequence, map);
        if (z) {
            a2 = a((CharSequence) a2);
        }
        return c((CharSequence) a2);
    }

    public static HttpRequest d(CharSequence charSequence) {
        return new HttpRequest(charSequence, "PUT");
    }

    public static HttpRequest e(CharSequence charSequence) {
        return new HttpRequest(charSequence, "DELETE");
    }

    public HttpRequest(CharSequence charSequence, String str) {
        try {
            this.f7239a = new URL(charSequence.toString());
            this.f7241e = str;
        } catch (MalformedURLException e2) {
            throw new HttpRequestException(e2);
        }
    }

    private Proxy q() {
        return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(this.k, this.l));
    }

    private HttpURLConnection r() {
        HttpURLConnection a2;
        try {
            if (this.k != null) {
                a2 = f7238c.a(this.f7239a, q());
            } else {
                a2 = f7238c.a(this.f7239a);
            }
            a2.setRequestMethod(this.f7241e);
            return a2;
        } catch (IOException e2) {
            throw new HttpRequestException(e2);
        }
    }

    public String toString() {
        return p() + ' ' + o();
    }

    public HttpURLConnection a() {
        if (this.f7240d == null) {
            this.f7240d = r();
        }
        return this.f7240d;
    }

    public int b() {
        try {
            k();
            return a().getResponseCode();
        } catch (IOException e2) {
            throw new HttpRequestException(e2);
        }
    }

    public boolean c() {
        return 200 == b();
    }

    /* access modifiers changed from: protected */
    public ByteArrayOutputStream d() {
        int j2 = j();
        if (j2 > 0) {
            return new ByteArrayOutputStream(j2);
        }
        return new ByteArrayOutputStream();
    }

    public String a(String str) {
        ByteArrayOutputStream d2 = d();
        try {
            a(f(), d2);
            return d2.toString(f(str));
        } catch (IOException e2) {
            throw new HttpRequestException(e2);
        }
    }

    public String e() {
        return a(h());
    }

    public BufferedInputStream f() {
        return new BufferedInputStream(g(), this.j);
    }

    public InputStream g() {
        InputStream errorStream;
        if (b() < 400) {
            try {
                errorStream = a().getInputStream();
            } catch (IOException e2) {
                throw new HttpRequestException(e2);
            }
        } else {
            errorStream = a().getErrorStream();
            if (errorStream == null) {
                try {
                    errorStream = a().getInputStream();
                } catch (IOException e3) {
                    throw new HttpRequestException(e3);
                }
            }
        }
        if (!this.i || !"gzip".equals(i())) {
            return errorStream;
        }
        try {
            return new GZIPInputStream(errorStream);
        } catch (IOException e4) {
            throw new HttpRequestException(e4);
        }
    }

    public HttpRequest a(int i2) {
        a().setConnectTimeout(i2);
        return this;
    }

    public HttpRequest a(String str, String str2) {
        a().setRequestProperty(str, str2);
        return this;
    }

    public HttpRequest a(Map.Entry<String, String> entry) {
        return a(entry.getKey(), entry.getValue());
    }

    public String b(String str) {
        l();
        return a().getHeaderField(str);
    }

    public int c(String str) {
        return a(str, -1);
    }

    public int a(String str, int i2) {
        l();
        return a().getHeaderFieldInt(str, i2);
    }

    public String b(String str, String str2) {
        return c(b(str), str2);
    }

    /* access modifiers changed from: protected */
    public String c(String str, String str2) {
        int i2;
        int i3;
        String trim;
        int length;
        if (str == null || str.length() == 0) {
            return null;
        }
        int length2 = str.length();
        int indexOf = str.indexOf(59) + 1;
        if (indexOf == 0 || indexOf == length2) {
            return null;
        }
        int indexOf2 = str.indexOf(59, indexOf);
        if (indexOf2 == -1) {
            i3 = indexOf;
            i2 = length2;
        } else {
            int i4 = indexOf2;
            i3 = indexOf;
            i2 = i4;
        }
        while (i3 < i2) {
            int indexOf3 = str.indexOf(61, i3);
            if (indexOf3 == -1 || indexOf3 >= i2 || !str2.equals(str.substring(i3, indexOf3).trim()) || (length = (trim = str.substring(indexOf3 + 1, i2).trim()).length()) == 0) {
                int i5 = i2 + 1;
                int indexOf4 = str.indexOf(59, i5);
                if (indexOf4 == -1) {
                    indexOf4 = length2;
                }
                int i6 = indexOf4;
                i3 = i5;
                i2 = i6;
            } else if (length > 2 && '\"' == trim.charAt(0) && '\"' == trim.charAt(length - 1)) {
                return trim.substring(1, length - 1);
            } else {
                return trim;
            }
        }
        return null;
    }

    public String h() {
        return b("Content-Type", "charset");
    }

    public HttpRequest a(boolean z) {
        a().setUseCaches(z);
        return this;
    }

    public String i() {
        return b("Content-Encoding");
    }

    public HttpRequest d(String str) {
        return d(str, null);
    }

    public HttpRequest d(String str, String str2) {
        if (str2 == null || str2.length() <= 0) {
            return a("Content-Type", str);
        }
        return a("Content-Type", str + "; charset=" + str2);
    }

    public int j() {
        return c("Content-Length");
    }

    /* access modifiers changed from: protected */
    public HttpRequest a(InputStream inputStream, OutputStream outputStream) {
        final InputStream inputStream2 = inputStream;
        final OutputStream outputStream2 = outputStream;
        return (HttpRequest) new a<HttpRequest>(inputStream, this.f7244h) {
            /* renamed from: a */
            public HttpRequest b() {
                byte[] bArr = new byte[HttpRequest.this.j];
                while (true) {
                    int read = inputStream2.read(bArr);
                    if (read == -1) {
                        return HttpRequest.this;
                    }
                    outputStream2.write(bArr, 0, read);
                }
            }
        }.call();
    }

    /* access modifiers changed from: protected */
    public HttpRequest k() {
        if (this.f7242f != null) {
            if (this.f7243g) {
                this.f7242f.a("\r\n--00content0boundary00--\r\n");
            }
            if (this.f7244h) {
                try {
                    this.f7242f.close();
                } catch (IOException e2) {
                }
            } else {
                this.f7242f.close();
            }
            this.f7242f = null;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public HttpRequest l() {
        try {
            return k();
        } catch (IOException e2) {
            throw new HttpRequestException(e2);
        }
    }

    /* access modifiers changed from: protected */
    public HttpRequest m() {
        if (this.f7242f == null) {
            a().setDoOutput(true);
            this.f7242f = new d(a().getOutputStream(), c(a().getRequestProperty("Content-Type"), "charset"), this.j);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public HttpRequest n() {
        if (!this.f7243g) {
            this.f7243g = true;
            d("multipart/form-data; boundary=00content0boundary00").m();
            this.f7242f.a("--00content0boundary00\r\n");
        } else {
            this.f7242f.a("\r\n--00content0boundary00\r\n");
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public HttpRequest a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"").append(str);
        if (str2 != null) {
            sb.append("\"; filename=\"").append(str2);
        }
        sb.append('\"');
        f("Content-Disposition", sb.toString());
        if (str3 != null) {
            f("Content-Type", str3);
        }
        return f((CharSequence) "\r\n");
    }

    public HttpRequest e(String str, String str2) {
        return b(str, (String) null, str2);
    }

    public HttpRequest b(String str, String str2, String str3) {
        return a(str, str2, (String) null, str3);
    }

    public HttpRequest a(String str, String str2, String str3, String str4) {
        try {
            n();
            a(str, str2, str3);
            this.f7242f.a(str4);
            return this;
        } catch (IOException e2) {
            throw new HttpRequestException(e2);
        }
    }

    public HttpRequest a(String str, Number number) {
        return a(str, (String) null, number);
    }

    public HttpRequest a(String str, String str2, Number number) {
        return b(str, str2, number != null ? number.toString() : null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0020 A[SYNTHETIC, Splitter:B:16:0x0020] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public io.fabric.sdk.android.services.network.HttpRequest a(java.lang.String r4, java.lang.String r5, java.lang.String r6, java.io.File r7) {
        /*
            r3 = this;
            r2 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0015, all -> 0x0028 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0015, all -> 0x0028 }
            r0.<init>(r7)     // Catch:{ IOException -> 0x0015, all -> 0x0028 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0015, all -> 0x0028 }
            io.fabric.sdk.android.services.network.HttpRequest r0 = r3.a(r4, r5, r6, r1)     // Catch:{ IOException -> 0x002b }
            if (r1 == 0) goto L_0x0014
            r1.close()     // Catch:{ IOException -> 0x0024 }
        L_0x0014:
            return r0
        L_0x0015:
            r0 = move-exception
            r1 = r2
        L_0x0017:
            io.fabric.sdk.android.services.network.HttpRequest$HttpRequestException r2 = new io.fabric.sdk.android.services.network.HttpRequest$HttpRequestException     // Catch:{ all -> 0x001d }
            r2.<init>(r0)     // Catch:{ all -> 0x001d }
            throw r2     // Catch:{ all -> 0x001d }
        L_0x001d:
            r0 = move-exception
        L_0x001e:
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x0026 }
        L_0x0023:
            throw r0
        L_0x0024:
            r1 = move-exception
            goto L_0x0014
        L_0x0026:
            r1 = move-exception
            goto L_0x0023
        L_0x0028:
            r0 = move-exception
            r1 = r2
            goto L_0x001e
        L_0x002b:
            r0 = move-exception
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: io.fabric.sdk.android.services.network.HttpRequest.a(java.lang.String, java.lang.String, java.lang.String, java.io.File):io.fabric.sdk.android.services.network.HttpRequest");
    }

    public HttpRequest a(String str, String str2, String str3, InputStream inputStream) {
        try {
            n();
            a(str, str2, str3);
            a(inputStream, this.f7242f);
            return this;
        } catch (IOException e2) {
            throw new HttpRequestException(e2);
        }
    }

    public HttpRequest f(String str, String str2) {
        return f((CharSequence) str).f((CharSequence) ": ").f((CharSequence) str2).f((CharSequence) "\r\n");
    }

    public HttpRequest f(CharSequence charSequence) {
        try {
            m();
            this.f7242f.a(charSequence.toString());
            return this;
        } catch (IOException e2) {
            throw new HttpRequestException(e2);
        }
    }

    public URL o() {
        return a().getURL();
    }

    public String p() {
        return a().getRequestMethod();
    }
}
