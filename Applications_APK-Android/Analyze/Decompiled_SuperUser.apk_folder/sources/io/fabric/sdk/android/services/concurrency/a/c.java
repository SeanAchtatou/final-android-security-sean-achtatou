package io.fabric.sdk.android.services.concurrency.a;

/* compiled from: ExponentialBackoff */
public class c implements a {

    /* renamed from: a  reason: collision with root package name */
    private final long f7218a;

    /* renamed from: b  reason: collision with root package name */
    private final int f7219b;

    public c(long j, int i) {
        this.f7218a = j;
        this.f7219b = i;
    }

    public long getDelayMillis(int i) {
        return (long) (((double) this.f7218a) * Math.pow((double) this.f7219b, (double) i));
    }
}
