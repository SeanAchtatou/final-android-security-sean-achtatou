package io.fabric.sdk.android.services.common;

import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.a.b;
import io.fabric.sdk.android.services.a.d;

/* compiled from: InstallerPackageNameProvider */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private final d<String> f7163a = new d<String>() {
        /* renamed from: a */
        public String load(Context context) {
            String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            return installerPackageName == null ? "" : installerPackageName;
        }
    };

    /* renamed from: b  reason: collision with root package name */
    private final b<String> f7164b = new b<>();

    public String a(Context context) {
        try {
            String a2 = this.f7164b.a(context, this.f7163a);
            if ("".equals(a2)) {
                return null;
            }
            return a2;
        } catch (Exception e2) {
            Fabric.h().e("Fabric", "Failed to determine installer package name", e2);
            return null;
        }
    }
}
