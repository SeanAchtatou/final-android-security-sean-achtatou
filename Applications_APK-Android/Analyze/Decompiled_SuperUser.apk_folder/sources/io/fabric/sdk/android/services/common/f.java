package io.fabric.sdk.android.services.common;

/* compiled from: Crash */
public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    private final String f7153a;

    /* renamed from: b  reason: collision with root package name */
    private final String f7154b;

    public f(String str, String str2) {
        this.f7153a = str;
        this.f7154b = str2;
    }

    public String a() {
        return this.f7153a;
    }

    public String b() {
        return this.f7154b;
    }

    /* compiled from: Crash */
    public static class b extends f {
        public b(String str, String str2) {
            super(str, str2);
        }
    }

    /* compiled from: Crash */
    public static class a extends f {
        public a(String str, String str2) {
            super(str, str2);
        }
    }
}
