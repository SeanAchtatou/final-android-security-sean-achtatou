package io.fabric.sdk.android.services.common;

public enum DeliveryMechanism {
    DEVELOPER(1),
    USER_SIDELOAD(2),
    TEST_DISTRIBUTION(3),
    APP_STORE(4);
    

    /* renamed from: e  reason: collision with root package name */
    private final int f7133e;

    private DeliveryMechanism(int i) {
        this.f7133e = i;
    }

    public int a() {
        return this.f7133e;
    }

    public String toString() {
        return Integer.toString(this.f7133e);
    }

    public static DeliveryMechanism a(String str) {
        if ("io.crash.air".equals(str)) {
            return TEST_DISTRIBUTION;
        }
        if (str != null) {
            return APP_STORE;
        }
        return DEVELOPER;
    }
}
