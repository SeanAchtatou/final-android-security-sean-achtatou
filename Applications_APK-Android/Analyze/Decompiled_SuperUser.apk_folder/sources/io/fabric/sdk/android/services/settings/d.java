package io.fabric.sdk.android.services.settings;

import io.fabric.sdk.android.h;
import java.util.Collection;

/* compiled from: AppRequestData */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public final String f7280a;

    /* renamed from: b  reason: collision with root package name */
    public final String f7281b;

    /* renamed from: c  reason: collision with root package name */
    public final String f7282c;

    /* renamed from: d  reason: collision with root package name */
    public final String f7283d;

    /* renamed from: e  reason: collision with root package name */
    public final String f7284e;

    /* renamed from: f  reason: collision with root package name */
    public final String f7285f;

    /* renamed from: g  reason: collision with root package name */
    public final int f7286g;

    /* renamed from: h  reason: collision with root package name */
    public final String f7287h;
    public final String i;
    public final n j;
    public final Collection<h> k;

    public d(String str, String str2, String str3, String str4, String str5, String str6, int i2, String str7, String str8, n nVar, Collection<h> collection) {
        this.f7280a = str;
        this.f7281b = str2;
        this.f7282c = str3;
        this.f7283d = str4;
        this.f7284e = str5;
        this.f7285f = str6;
        this.f7286g = i2;
        this.f7287h = str7;
        this.i = str8;
        this.j = nVar;
        this.k = collection;
    }
}
