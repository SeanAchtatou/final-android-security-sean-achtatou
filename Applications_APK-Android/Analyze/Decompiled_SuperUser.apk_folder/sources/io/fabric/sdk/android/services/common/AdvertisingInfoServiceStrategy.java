package io.fabric.sdk.android.services.common;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import io.fabric.sdk.android.Fabric;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

class AdvertisingInfoServiceStrategy implements c {

    /* renamed from: a  reason: collision with root package name */
    private final Context f7112a;

    public AdvertisingInfoServiceStrategy(Context context) {
        this.f7112a = context.getApplicationContext();
    }

    public b a() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            Fabric.h().a("Fabric", "AdvertisingInfoServiceStrategy cannot be called on the main thread");
            return null;
        }
        try {
            this.f7112a.getPackageManager().getPackageInfo("com.android.vending", 0);
            a aVar = new a();
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            try {
                if (this.f7112a.bindService(intent, aVar, 1)) {
                    b bVar = new b(aVar.a());
                    b bVar2 = new b(bVar.a(), bVar.b());
                    this.f7112a.unbindService(aVar);
                    return bVar2;
                }
                Fabric.h().a("Fabric", "Could not bind to Google Play Service to capture AdvertisingId");
                return null;
            } catch (Exception e2) {
                Fabric.h().d("Fabric", "Exception in binding to Google Play Service to capture AdvertisingId", e2);
                this.f7112a.unbindService(aVar);
                return null;
            } catch (Throwable th) {
                Fabric.h().a("Fabric", "Could not bind to Google Play Service to capture AdvertisingId", th);
                return null;
            }
        } catch (PackageManager.NameNotFoundException e3) {
            Fabric.h().a("Fabric", "Unable to find Google Play Services package name");
            return null;
        } catch (Exception e4) {
            Fabric.h().a("Fabric", "Unable to determine if Google Play Services is available", e4);
            return null;
        }
    }

    private static final class a implements ServiceConnection {

        /* renamed from: a  reason: collision with root package name */
        private boolean f7113a;

        /* renamed from: b  reason: collision with root package name */
        private final LinkedBlockingQueue<IBinder> f7114b;

        private a() {
            this.f7113a = false;
            this.f7114b = new LinkedBlockingQueue<>(1);
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.f7114b.put(iBinder);
            } catch (InterruptedException e2) {
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            this.f7114b.clear();
        }

        public IBinder a() {
            if (this.f7113a) {
                Fabric.h().e("Fabric", "getBinder already called");
            }
            this.f7113a = true;
            try {
                return this.f7114b.poll(200, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e2) {
                return null;
            }
        }
    }

    private static final class b implements IInterface {

        /* renamed from: a  reason: collision with root package name */
        private final IBinder f7115a;

        public b(IBinder iBinder) {
            this.f7115a = iBinder;
        }

        public IBinder asBinder() {
            return this.f7115a;
        }

        public String a() {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            String str = null;
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.f7115a.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                str = obtain2.readString();
            } catch (Exception e2) {
                Fabric.h().a("Fabric", "Could not get parcel from Google Play Service to capture AdvertisingId");
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
            return str;
        }

        /* JADX INFO: finally extract failed */
        public boolean b() {
            boolean z = true;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                obtain.writeInt(1);
                this.f7115a.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z = false;
                }
                obtain2.recycle();
                obtain.recycle();
                return z;
            } catch (Exception e2) {
                Fabric.h().a("Fabric", "Could not get parcel from Google Play Service to capture Advertising limitAdTracking");
                obtain2.recycle();
                obtain.recycle();
                return false;
            } catch (Throwable th) {
                obtain2.recycle();
                obtain.recycle();
                throw th;
            }
        }
    }
}
