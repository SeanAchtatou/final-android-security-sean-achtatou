package io.fabric.sdk.android.services.settings;

/* compiled from: SessionSettingsData */
public class p {

    /* renamed from: a  reason: collision with root package name */
    public final int f7319a;

    /* renamed from: b  reason: collision with root package name */
    public final int f7320b;

    /* renamed from: c  reason: collision with root package name */
    public final int f7321c;

    /* renamed from: d  reason: collision with root package name */
    public final int f7322d;

    /* renamed from: e  reason: collision with root package name */
    public final int f7323e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f7324f;

    /* renamed from: g  reason: collision with root package name */
    public final int f7325g;

    public p(int i, int i2, int i3, int i4, int i5, boolean z, int i6) {
        this.f7319a = i;
        this.f7320b = i2;
        this.f7321c = i3;
        this.f7322d = i4;
        this.f7323e = i5;
        this.f7324f = z;
        this.f7325g = i6;
    }
}
