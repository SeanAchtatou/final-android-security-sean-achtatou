package io.fabric.sdk.android.services.common;

import io.fabric.sdk.android.services.common.IdManager;
import java.util.Map;

/* compiled from: DeviceIdentifierProvider */
public interface h {
    Map<IdManager.DeviceIdentifierType, String> getDeviceIdentifiers();
}
