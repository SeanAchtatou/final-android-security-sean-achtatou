package io.fabric.sdk.android.services.settings;

import android.content.res.Resources;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.f;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.l;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.c;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Locale;

/* compiled from: AbstractAppSpiCall */
abstract class a extends io.fabric.sdk.android.services.common.a {
    public a(f fVar, String str, String str2, c cVar, HttpMethod httpMethod) {
        super(fVar, str, str2, cVar, httpMethod);
    }

    public boolean a(d dVar) {
        HttpRequest b2 = b(a(getHttpRequest(), dVar), dVar);
        Fabric.h().a("Fabric", "Sending app info to " + getUrl());
        if (dVar.j != null) {
            Fabric.h().a("Fabric", "App icon hash is " + dVar.j.f7308a);
            Fabric.h().a("Fabric", "App icon size is " + dVar.j.f7310c + "x" + dVar.j.f7311d);
        }
        int b3 = b2.b();
        Fabric.h().a("Fabric", ("POST".equals(b2.p()) ? "Create" : "Update") + " app request ID: " + b2.b(io.fabric.sdk.android.services.common.a.HEADER_REQUEST_ID));
        Fabric.h().a("Fabric", "Result was " + b3);
        if (l.a(b3) == 0) {
            return true;
        }
        return false;
    }

    private HttpRequest a(HttpRequest httpRequest, d dVar) {
        return httpRequest.a(io.fabric.sdk.android.services.common.a.HEADER_API_KEY, dVar.f7280a).a(io.fabric.sdk.android.services.common.a.HEADER_CLIENT_TYPE, io.fabric.sdk.android.services.common.a.ANDROID_CLIENT_TYPE).a(io.fabric.sdk.android.services.common.a.HEADER_CLIENT_VERSION, this.kit.getVersion());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void
     arg types: [java.io.InputStream, java.lang.String]
     candidates:
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, boolean):int
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, android.content.Context):android.app.ActivityManager$RunningAppProcessInfo
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.File, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.InputStream, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(java.lang.String, java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(byte[], java.lang.String):java.lang.String
      io.fabric.sdk.android.services.common.CommonUtils.a(android.content.Context, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Flushable, java.lang.String):void
      io.fabric.sdk.android.services.common.CommonUtils.a(java.io.Closeable, java.lang.String):void */
    private HttpRequest b(HttpRequest httpRequest, d dVar) {
        String str;
        HttpRequest e2 = httpRequest.e("app[identifier]", dVar.f7281b).e("app[name]", dVar.f7285f).e("app[display_version]", dVar.f7282c).e("app[build_version]", dVar.f7283d).a("app[source]", Integer.valueOf(dVar.f7286g)).e("app[minimum_sdk_version]", dVar.f7287h).e("app[built_sdk_version]", dVar.i);
        if (!CommonUtils.c(dVar.f7284e)) {
            e2.e("app[instance_identifier]", dVar.f7284e);
        }
        if (dVar.j != null) {
            InputStream inputStream = null;
            try {
                inputStream = this.kit.getContext().getResources().openRawResource(dVar.j.f7309b);
                e2.e("app[icon][hash]", dVar.j.f7308a).a("app[icon][data]", "icon.png", "application/octet-stream", inputStream).a("app[icon][width]", Integer.valueOf(dVar.j.f7310c)).a("app[icon][height]", Integer.valueOf(dVar.j.f7311d));
            } catch (Resources.NotFoundException e3) {
                Fabric.h().e("Fabric", "Failed to find app icon with resource ID: " + dVar.j.f7309b, e3);
            } finally {
                str = "Failed to close app icon InputStream.";
                CommonUtils.a((Closeable) inputStream, str);
            }
        }
        if (dVar.k != null) {
            for (h next : dVar.k) {
                e2.e(a(next), next.b());
                e2.e(b(next), next.c());
            }
        }
        return e2;
    }

    /* access modifiers changed from: package-private */
    public String a(h hVar) {
        return String.format(Locale.US, "app[build][libraries][%s][version]", hVar.a());
    }

    /* access modifiers changed from: package-private */
    public String b(h hVar) {
        return String.format(Locale.US, "app[build][libraries][%s][type]", hVar.a());
    }
}
