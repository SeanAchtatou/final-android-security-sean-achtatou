package io.fabric.sdk.android.services.settings;

/* compiled from: SettingsData */
public class s {

    /* renamed from: a  reason: collision with root package name */
    public final e f7331a;

    /* renamed from: b  reason: collision with root package name */
    public final p f7332b;

    /* renamed from: c  reason: collision with root package name */
    public final o f7333c;

    /* renamed from: d  reason: collision with root package name */
    public final m f7334d;

    /* renamed from: e  reason: collision with root package name */
    public final b f7335e;

    /* renamed from: f  reason: collision with root package name */
    public final f f7336f;

    /* renamed from: g  reason: collision with root package name */
    public final long f7337g;

    /* renamed from: h  reason: collision with root package name */
    public final int f7338h;
    public final int i;

    public s(long j, e eVar, p pVar, o oVar, m mVar, b bVar, f fVar, int i2, int i3) {
        this.f7337g = j;
        this.f7331a = eVar;
        this.f7332b = pVar;
        this.f7333c = oVar;
        this.f7334d = mVar;
        this.f7338h = i2;
        this.i = i3;
        this.f7335e = bVar;
        this.f7336f = fVar;
    }

    public boolean a(long j) {
        return this.f7337g < j;
    }
}
