package io.fabric.sdk.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeliveryMechanism;
import io.fabric.sdk.android.services.network.b;
import io.fabric.sdk.android.services.network.c;
import io.fabric.sdk.android.services.settings.d;
import io.fabric.sdk.android.services.settings.e;
import io.fabric.sdk.android.services.settings.h;
import io.fabric.sdk.android.services.settings.n;
import io.fabric.sdk.android.services.settings.q;
import io.fabric.sdk.android.services.settings.s;
import io.fabric.sdk.android.services.settings.x;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

/* compiled from: Onboarding */
class j extends f<Boolean> {

    /* renamed from: a  reason: collision with root package name */
    private final c f7080a = new b();

    /* renamed from: b  reason: collision with root package name */
    private PackageManager f7081b;

    /* renamed from: c  reason: collision with root package name */
    private String f7082c;

    /* renamed from: d  reason: collision with root package name */
    private PackageInfo f7083d;

    /* renamed from: e  reason: collision with root package name */
    private String f7084e;

    /* renamed from: f  reason: collision with root package name */
    private String f7085f;

    /* renamed from: g  reason: collision with root package name */
    private String f7086g;

    /* renamed from: h  reason: collision with root package name */
    private String f7087h;
    private String i;
    private final Future<Map<String, h>> j;
    private final Collection<f> k;

    public j(Future<Map<String, h>> future, Collection<f> collection) {
        this.j = future;
        this.k = collection;
    }

    public String getVersion() {
        return "1.3.17.dev";
    }

    /* access modifiers changed from: protected */
    public boolean onPreExecute() {
        try {
            this.f7086g = getIdManager().j();
            this.f7081b = getContext().getPackageManager();
            this.f7082c = getContext().getPackageName();
            this.f7083d = this.f7081b.getPackageInfo(this.f7082c, 0);
            this.f7084e = Integer.toString(this.f7083d.versionCode);
            this.f7085f = this.f7083d.versionName == null ? "0.0" : this.f7083d.versionName;
            this.f7087h = this.f7081b.getApplicationLabel(getContext().getApplicationInfo()).toString();
            this.i = Integer.toString(getContext().getApplicationInfo().targetSdkVersion);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            Fabric.h().e("Fabric", "Failed init", e2);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground() {
        boolean z;
        Map hashMap;
        String k2 = CommonUtils.k(getContext());
        s c2 = c();
        if (c2 != null) {
            try {
                if (this.j != null) {
                    hashMap = this.j.get();
                } else {
                    hashMap = new HashMap();
                }
                z = a(k2, c2.f7331a, a(hashMap, this.k).values());
            } catch (Exception e2) {
                Fabric.h().e("Fabric", "Error performing auto configuration.", e2);
            }
            return Boolean.valueOf(z);
        }
        z = false;
        return Boolean.valueOf(z);
    }

    private s c() {
        try {
            q.a().a(this, this.idManager, this.f7080a, this.f7084e, this.f7085f, b()).c();
            return q.a().b();
        } catch (Exception e2) {
            Fabric.h().e("Fabric", "Error dealing with settings", e2);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public Map<String, h> a(Map<String, h> map, Collection<f> collection) {
        for (f next : collection) {
            if (!map.containsKey(next.getIdentifier())) {
                map.put(next.getIdentifier(), new h(next.getIdentifier(), next.getVersion(), "binary"));
            }
        }
        return map;
    }

    public String getIdentifier() {
        return "io.fabric.sdk.android:fabric";
    }

    private boolean a(String str, e eVar, Collection<h> collection) {
        if ("new".equals(eVar.f7289b)) {
            if (b(str, eVar, collection)) {
                return q.a().d();
            }
            Fabric.h().e("Fabric", "Failed to create app with Crashlytics service.", null);
            return false;
        } else if ("configured".equals(eVar.f7289b)) {
            return q.a().d();
        } else {
            if (!eVar.f7292e) {
                return true;
            }
            Fabric.h().a("Fabric", "Server says an update is required - forcing a full App update.");
            c(str, eVar, collection);
            return true;
        }
    }

    private boolean b(String str, e eVar, Collection<h> collection) {
        return new h(this, b(), eVar.f7290c, this.f7080a).a(a(n.a(getContext(), str), collection));
    }

    private boolean c(String str, e eVar, Collection<h> collection) {
        return a(eVar, n.a(getContext(), str), collection);
    }

    private boolean a(e eVar, n nVar, Collection<h> collection) {
        return new x(this, b(), eVar.f7290c, this.f7080a).a(a(nVar, collection));
    }

    private d a(n nVar, Collection<h> collection) {
        Context context = getContext();
        return new d(new io.fabric.sdk.android.services.common.d().a(context), getIdManager().c(), this.f7085f, this.f7084e, CommonUtils.a(CommonUtils.m(context)), this.f7087h, DeliveryMechanism.a(this.f7086g).a(), this.i, "0", nVar, collection);
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return CommonUtils.b(getContext(), "com.crashlytics.ApiEndpoint");
    }
}
