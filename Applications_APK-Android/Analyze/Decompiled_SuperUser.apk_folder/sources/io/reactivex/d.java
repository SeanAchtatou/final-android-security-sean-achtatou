package io.reactivex;

import com.lody.virtual.helper.utils.FileUtils;
import io.reactivex.a.e;
import io.reactivex.b.a;
import io.reactivex.internal.operators.flowable.FlowableCreate;
import io.reactivex.internal.operators.flowable.FlowableInternalHelper;
import io.reactivex.internal.operators.flowable.FlowableObserveOn;
import io.reactivex.internal.operators.flowable.FlowableSubscribeOn;
import io.reactivex.internal.subscribers.LambdaSubscriber;
import io.reactivex.internal.subscribers.StrictSubscriber;
import org.a.b;
import org.a.c;

/* compiled from: Flowable */
public abstract class d<T> implements b<T> {

    /* renamed from: a  reason: collision with root package name */
    static final int f7371a = Math.max(1, Integer.getInteger("rx2.buffer-size", (int) FileUtils.FileMode.MODE_IWUSR).intValue());

    /* access modifiers changed from: protected */
    public abstract void b(c cVar);

    public static int a() {
        return f7371a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.f<java.lang.Boolean>, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.BackpressureStrategy, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public static <T> d<T> a(f<Boolean> fVar, BackpressureStrategy backpressureStrategy) {
        io.reactivex.internal.a.b.a((Object) fVar, "source is null");
        io.reactivex.internal.a.b.a((Object) backpressureStrategy, "mode is null");
        return a.a(new FlowableCreate(fVar, backpressureStrategy));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.a.e, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public final <R> d<R> a(e eVar) {
        io.reactivex.internal.a.b.a((Object) eVar, "mapper is null");
        return a.a(new io.reactivex.internal.operators.flowable.b(this, eVar));
    }

    public final d<T> a(h hVar) {
        return a(hVar, false, a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.h, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public final d<T> a(h hVar, boolean z, int i) {
        io.reactivex.internal.a.b.a((Object) hVar, "scheduler is null");
        io.reactivex.internal.a.b.a(i, "bufferSize");
        return a.a(new FlowableObserveOn(this, hVar, z, i));
    }

    public final io.reactivex.disposables.b a(io.reactivex.a.d<? super Boolean> dVar, io.reactivex.a.d<? super Throwable> dVar2) {
        return a(dVar, dVar2, io.reactivex.internal.a.a.f7394c, FlowableInternalHelper.RequestMax.INSTANCE);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.a.d<? super T>, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.a.d<? super java.lang.Throwable>, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.a.a, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.a.d<? super org.a.d>, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public final io.reactivex.disposables.b a(io.reactivex.a.d<? super T> dVar, io.reactivex.a.d<? super Throwable> dVar2, io.reactivex.a.a aVar, io.reactivex.a.d<? super org.a.d> dVar3) {
        io.reactivex.internal.a.b.a((Object) dVar, "onNext is null");
        io.reactivex.internal.a.b.a((Object) dVar2, "onError is null");
        io.reactivex.internal.a.b.a((Object) aVar, "onComplete is null");
        io.reactivex.internal.a.b.a((Object) dVar3, "onSubscribe is null");
        LambdaSubscriber lambdaSubscriber = new LambdaSubscriber(dVar, dVar2, aVar, dVar3);
        a((g) lambdaSubscriber);
        return lambdaSubscriber;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [org.a.c, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public final void a(c cVar) {
        if (cVar instanceof g) {
            a((g) cVar);
            return;
        }
        io.reactivex.internal.a.b.a((Object) cVar, "s is null");
        a((g) new StrictSubscriber(cVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.g, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [org.a.c, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public final void a(g gVar) {
        io.reactivex.internal.a.b.a((Object) gVar, "s is null");
        try {
            c a2 = a.a(this, gVar);
            io.reactivex.internal.a.b.a((Object) a2, "Plugin returned null Subscriber");
            b(a2);
        } catch (NullPointerException e2) {
            throw e2;
        } catch (Throwable th) {
            io.reactivex.exceptions.a.b(th);
            a.a(th);
            NullPointerException nullPointerException = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
            nullPointerException.initCause(th);
            throw nullPointerException;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.h, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public final d<T> b(h hVar) {
        io.reactivex.internal.a.b.a((Object) hVar, "scheduler is null");
        return a(hVar, !(this instanceof FlowableCreate));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.h, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public final d<T> a(h hVar, boolean z) {
        io.reactivex.internal.a.b.a((Object) hVar, "scheduler is null");
        return a.a(new FlowableSubscribeOn(this, hVar, z));
    }
}
