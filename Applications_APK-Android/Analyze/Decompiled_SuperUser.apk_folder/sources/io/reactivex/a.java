package io.reactivex;

import io.reactivex.disposables.b;
import io.reactivex.internal.observers.EmptyCompletableObserver;

/* compiled from: Completable */
public abstract class a {
    /* access modifiers changed from: protected */
    public abstract void b(b bVar);

    private static NullPointerException a(Throwable th) {
        NullPointerException nullPointerException = new NullPointerException("Actually not, but can't pass out an exception otherwise...");
        nullPointerException.initCause(th);
        return nullPointerException;
    }

    public final b a() {
        EmptyCompletableObserver emptyCompletableObserver = new EmptyCompletableObserver();
        a(emptyCompletableObserver);
        return emptyCompletableObserver;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.b, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public final void a(b bVar) {
        io.reactivex.internal.a.b.a((Object) bVar, "s is null");
        try {
            b(io.reactivex.b.a.a(this, bVar));
        } catch (NullPointerException e2) {
            throw e2;
        } catch (Throwable th) {
            io.reactivex.exceptions.a.b(th);
            io.reactivex.b.a.a(th);
            throw a(th);
        }
    }
}
