package io.reactivex.b;

import io.reactivex.a.b;
import io.reactivex.a.d;
import io.reactivex.a.e;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.h;
import io.reactivex.internal.util.ExceptionHelper;
import java.util.concurrent.Callable;
import org.a.c;

/* compiled from: RxJavaPlugins */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    static volatile d<? super Throwable> f7354a;

    /* renamed from: b  reason: collision with root package name */
    static volatile e<? super Runnable, ? extends Runnable> f7355b;

    /* renamed from: c  reason: collision with root package name */
    static volatile e<? super Callable<h>, ? extends h> f7356c;

    /* renamed from: d  reason: collision with root package name */
    static volatile e<? super Callable<h>, ? extends h> f7357d;

    /* renamed from: e  reason: collision with root package name */
    static volatile e<? super Callable<h>, ? extends h> f7358e;

    /* renamed from: f  reason: collision with root package name */
    static volatile e<? super Callable<h>, ? extends h> f7359f;

    /* renamed from: g  reason: collision with root package name */
    static volatile e<? super h, ? extends h> f7360g;

    /* renamed from: h  reason: collision with root package name */
    static volatile e<? super h, ? extends h> f7361h;
    static volatile e<? super io.reactivex.d, ? extends io.reactivex.d> i;
    static volatile b<? super io.reactivex.d, ? super c, ? extends c> j;
    static volatile b<? super io.reactivex.a, ? super io.reactivex.b, ? extends io.reactivex.b> k;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.concurrent.Callable<io.reactivex.h>, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public static h a(Callable<h> callable) {
        io.reactivex.internal.a.b.a((Object) callable, "Scheduler Callable can't be null");
        e<? super Callable<h>, ? extends h> eVar = f7356c;
        if (eVar == null) {
            return e(callable);
        }
        return a(eVar, callable);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.concurrent.Callable<io.reactivex.h>, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public static h b(Callable<h> callable) {
        io.reactivex.internal.a.b.a((Object) callable, "Scheduler Callable can't be null");
        e<? super Callable<h>, ? extends h> eVar = f7358e;
        if (eVar == null) {
            return e(callable);
        }
        return a(eVar, callable);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.concurrent.Callable<io.reactivex.h>, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public static h c(Callable<h> callable) {
        io.reactivex.internal.a.b.a((Object) callable, "Scheduler Callable can't be null");
        e<? super Callable<h>, ? extends h> eVar = f7359f;
        if (eVar == null) {
            return e(callable);
        }
        return a(eVar, callable);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.concurrent.Callable<io.reactivex.h>, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public static h d(Callable<h> callable) {
        io.reactivex.internal.a.b.a((Object) callable, "Scheduler Callable can't be null");
        e<? super Callable<h>, ? extends h> eVar = f7357d;
        if (eVar == null) {
            return e(callable);
        }
        return a(eVar, callable);
    }

    public static void a(Throwable th) {
        d<? super Throwable> dVar = f7354a;
        if (th == null) {
            th = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        } else if (!b(th)) {
            th = new UndeliverableException(th);
        }
        if (dVar != null) {
            try {
                dVar.accept(th);
                return;
            } catch (Throwable th2) {
                th2.printStackTrace();
                c(th2);
            }
        }
        th.printStackTrace();
        c(th);
    }

    static boolean b(Throwable th) {
        if (!(th instanceof OnErrorNotImplementedException) && !(th instanceof MissingBackpressureException) && !(th instanceof IllegalStateException) && !(th instanceof NullPointerException) && !(th instanceof IllegalArgumentException) && !(th instanceof CompositeException)) {
            return false;
        }
        return true;
    }

    static void c(Throwable th) {
        Thread currentThread = Thread.currentThread();
        currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, th);
    }

    public static h a(h hVar) {
        e<? super h, ? extends h> eVar = f7360g;
        return eVar == null ? hVar : (h) a(eVar, hVar);
    }

    public static h b(h hVar) {
        e<? super h, ? extends h> eVar = f7361h;
        return eVar == null ? hVar : (h) a(eVar, hVar);
    }

    public static Runnable a(Runnable runnable) {
        e<? super Runnable, ? extends Runnable> eVar = f7355b;
        return eVar == null ? runnable : (Runnable) a(eVar, runnable);
    }

    public static <T> c<? super T> a(io.reactivex.d dVar, c cVar) {
        b<? super io.reactivex.d, ? super c, ? extends c> bVar = j;
        if (bVar != null) {
            return (c) a(bVar, dVar, cVar);
        }
        return cVar;
    }

    public static io.reactivex.b a(io.reactivex.a aVar, io.reactivex.b bVar) {
        b<? super io.reactivex.a, ? super io.reactivex.b, ? extends io.reactivex.b> bVar2 = k;
        if (bVar2 != null) {
            return (io.reactivex.b) a(bVar2, aVar, bVar);
        }
        return bVar;
    }

    public static <T> io.reactivex.d<T> a(io.reactivex.d dVar) {
        e<? super io.reactivex.d, ? extends io.reactivex.d> eVar = i;
        if (eVar != null) {
            return (io.reactivex.d) a(eVar, dVar);
        }
        return dVar;
    }

    static <T, R> R a(e eVar, Object obj) {
        try {
            return eVar.a(obj);
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    static <T, U, R> R a(b<T, U, R> bVar, T t, U u) {
        try {
            return bVar.a(t, u);
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.h, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    static h e(Callable<h> callable) {
        try {
            return (h) io.reactivex.internal.a.b.a((Object) callable.call(), "Scheduler Callable result can't be null");
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.b.a.a(io.reactivex.a.e, java.lang.Object):R
     arg types: [io.reactivex.a.e<? super java.util.concurrent.Callable<io.reactivex.h>, ? extends io.reactivex.h>, java.util.concurrent.Callable<io.reactivex.h>]
     candidates:
      io.reactivex.b.a.a(io.reactivex.a, io.reactivex.b):io.reactivex.b
      io.reactivex.b.a.a(io.reactivex.a.e<? super java.util.concurrent.Callable<io.reactivex.h>, ? extends io.reactivex.h>, java.util.concurrent.Callable<io.reactivex.h>):io.reactivex.h
      io.reactivex.b.a.a(io.reactivex.d, org.a.c):org.a.c<? super T>
      io.reactivex.b.a.a(io.reactivex.a.e, java.lang.Object):R */
    static h a(e<? super Callable<h>, ? extends h> eVar, Callable<h> callable) {
        return (h) io.reactivex.internal.a.b.a(a((e) eVar, (Object) callable), "Scheduler Callable result can't be null");
    }
}
