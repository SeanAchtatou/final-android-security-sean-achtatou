package io.reactivex;

import io.reactivex.a.e;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.schedulers.SchedulerWhen;
import io.reactivex.internal.util.ExceptionHelper;
import java.util.concurrent.TimeUnit;

/* compiled from: Scheduler */
public abstract class h {
    static final long CLOCK_DRIFT_TOLERANCE_NANOSECONDS = TimeUnit.MINUTES.toNanos(Long.getLong("rx2.scheduler.drift-tolerance", 15).longValue());

    public abstract c createWorker();

    public static long clockDriftTolerance() {
        return CLOCK_DRIFT_TOLERANCE_NANOSECONDS;
    }

    public long now(TimeUnit timeUnit) {
        return timeUnit.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    public void start() {
    }

    public void shutdown() {
    }

    public io.reactivex.disposables.b scheduleDirect(Runnable runnable) {
        return scheduleDirect(runnable, 0, TimeUnit.NANOSECONDS);
    }

    public io.reactivex.disposables.b scheduleDirect(Runnable runnable, long j, TimeUnit timeUnit) {
        c createWorker = createWorker();
        a aVar = new a(io.reactivex.b.a.a(runnable), createWorker);
        createWorker.schedule(aVar, j, timeUnit);
        return aVar;
    }

    public io.reactivex.disposables.b schedulePeriodicallyDirect(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        c createWorker = createWorker();
        b bVar = new b(io.reactivex.b.a.a(runnable), createWorker);
        io.reactivex.disposables.b schedulePeriodically = createWorker.schedulePeriodically(bVar, j, j2, timeUnit);
        if (schedulePeriodically == EmptyDisposable.INSTANCE) {
            return schedulePeriodically;
        }
        return bVar;
    }

    public <S extends h & io.reactivex.disposables.b> S when(e<d<d<a>>, a> eVar) {
        return new SchedulerWhen(eVar, this);
    }

    /* compiled from: Scheduler */
    public static abstract class c implements io.reactivex.disposables.b {
        public abstract io.reactivex.disposables.b schedule(Runnable runnable, long j, TimeUnit timeUnit);

        public io.reactivex.disposables.b schedule(Runnable runnable) {
            return schedule(runnable, 0, TimeUnit.NANOSECONDS);
        }

        public io.reactivex.disposables.b schedulePeriodically(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
            SequentialDisposable sequentialDisposable = new SequentialDisposable();
            SequentialDisposable sequentialDisposable2 = new SequentialDisposable(sequentialDisposable);
            Runnable a2 = io.reactivex.b.a.a(runnable);
            long nanos = timeUnit.toNanos(j2);
            long now = now(TimeUnit.NANOSECONDS);
            io.reactivex.disposables.b schedule = schedule(new a(now + timeUnit.toNanos(j), a2, now, sequentialDisposable2, nanos), j, timeUnit);
            if (schedule == EmptyDisposable.INSTANCE) {
                return schedule;
            }
            sequentialDisposable.a(schedule);
            return sequentialDisposable2;
        }

        public long now(TimeUnit timeUnit) {
            return timeUnit.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        }

        /* compiled from: Scheduler */
        final class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final Runnable f7385a;

            /* renamed from: b  reason: collision with root package name */
            final SequentialDisposable f7386b;

            /* renamed from: c  reason: collision with root package name */
            final long f7387c;

            /* renamed from: d  reason: collision with root package name */
            long f7388d;

            /* renamed from: e  reason: collision with root package name */
            long f7389e;

            /* renamed from: f  reason: collision with root package name */
            long f7390f;

            a(long j, Runnable runnable, long j2, SequentialDisposable sequentialDisposable, long j3) {
                this.f7385a = runnable;
                this.f7386b = sequentialDisposable;
                this.f7387c = j3;
                this.f7389e = j2;
                this.f7390f = j;
            }

            public void run() {
                long j;
                this.f7385a.run();
                if (!this.f7386b.a()) {
                    long now = c.this.now(TimeUnit.NANOSECONDS);
                    if (h.CLOCK_DRIFT_TOLERANCE_NANOSECONDS + now < this.f7389e || now >= this.f7389e + this.f7387c + h.CLOCK_DRIFT_TOLERANCE_NANOSECONDS) {
                        j = this.f7387c + now;
                        long j2 = this.f7387c;
                        long j3 = this.f7388d + 1;
                        this.f7388d = j3;
                        this.f7390f = j - (j2 * j3);
                    } else {
                        long j4 = this.f7390f;
                        long j5 = this.f7388d + 1;
                        this.f7388d = j5;
                        j = j4 + (j5 * this.f7387c);
                    }
                    this.f7389e = now;
                    this.f7386b.a(c.this.schedule(this, j - now, TimeUnit.NANOSECONDS));
                }
            }
        }
    }

    /* compiled from: Scheduler */
    static class b implements io.reactivex.disposables.b, Runnable {

        /* renamed from: a  reason: collision with root package name */
        final Runnable f7382a;

        /* renamed from: b  reason: collision with root package name */
        final c f7383b;

        /* renamed from: c  reason: collision with root package name */
        volatile boolean f7384c;

        b(Runnable runnable, c cVar) {
            this.f7382a = runnable;
            this.f7383b = cVar;
        }

        public void run() {
            if (!this.f7384c) {
                try {
                    this.f7382a.run();
                } catch (Throwable th) {
                    io.reactivex.exceptions.a.b(th);
                    this.f7383b.dispose();
                    throw ExceptionHelper.a(th);
                }
            }
        }

        public void dispose() {
            this.f7384c = true;
            this.f7383b.dispose();
        }
    }

    /* compiled from: Scheduler */
    static final class a implements io.reactivex.disposables.b, Runnable {

        /* renamed from: a  reason: collision with root package name */
        final Runnable f7379a;

        /* renamed from: b  reason: collision with root package name */
        final c f7380b;

        /* renamed from: c  reason: collision with root package name */
        Thread f7381c;

        a(Runnable runnable, c cVar) {
            this.f7379a = runnable;
            this.f7380b = cVar;
        }

        public void run() {
            this.f7381c = Thread.currentThread();
            try {
                this.f7379a.run();
            } finally {
                dispose();
                this.f7381c = null;
            }
        }

        public void dispose() {
            if (this.f7381c != Thread.currentThread() || !(this.f7380b instanceof io.reactivex.internal.schedulers.e)) {
                this.f7380b.dispose();
            } else {
                ((io.reactivex.internal.schedulers.e) this.f7380b).b();
            }
        }
    }
}
