package io.reactivex.c;

import java.util.concurrent.Callable;

/* compiled from: Schedulers */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    static final io.reactivex.h f7362a = io.reactivex.b.a.d(new h());

    /* renamed from: b  reason: collision with root package name */
    static final io.reactivex.h f7363b = io.reactivex.b.a.a(new b());

    /* renamed from: c  reason: collision with root package name */
    static final io.reactivex.h f7364c = io.reactivex.b.a.b(new c());

    /* renamed from: d  reason: collision with root package name */
    static final io.reactivex.h f7365d = io.reactivex.internal.schedulers.h.a();

    /* renamed from: e  reason: collision with root package name */
    static final io.reactivex.h f7366e = io.reactivex.b.a.c(new f());

    /* renamed from: io.reactivex.c.a$a  reason: collision with other inner class name */
    /* compiled from: Schedulers */
    static final class C0103a {

        /* renamed from: a  reason: collision with root package name */
        static final io.reactivex.h f7367a = new io.reactivex.internal.schedulers.a();
    }

    /* compiled from: Schedulers */
    static final class d {

        /* renamed from: a  reason: collision with root package name */
        static final io.reactivex.h f7368a = new io.reactivex.internal.schedulers.c();
    }

    /* compiled from: Schedulers */
    static final class e {

        /* renamed from: a  reason: collision with root package name */
        static final io.reactivex.h f7369a = new io.reactivex.internal.schedulers.d();
    }

    /* compiled from: Schedulers */
    static final class g {

        /* renamed from: a  reason: collision with root package name */
        static final io.reactivex.h f7370a = new io.reactivex.internal.schedulers.g();
    }

    public static io.reactivex.h a() {
        return io.reactivex.b.a.a(f7364c);
    }

    public static io.reactivex.h b() {
        return io.reactivex.b.a.b(f7366e);
    }

    /* compiled from: Schedulers */
    static final class c implements Callable<io.reactivex.h> {
        c() {
        }

        /* renamed from: a */
        public io.reactivex.h call() {
            return d.f7368a;
        }
    }

    /* compiled from: Schedulers */
    static final class f implements Callable<io.reactivex.h> {
        f() {
        }

        /* renamed from: a */
        public io.reactivex.h call() {
            return e.f7369a;
        }
    }

    /* compiled from: Schedulers */
    static final class h implements Callable<io.reactivex.h> {
        h() {
        }

        /* renamed from: a */
        public io.reactivex.h call() {
            return g.f7370a;
        }
    }

    /* compiled from: Schedulers */
    static final class b implements Callable<io.reactivex.h> {
        b() {
        }

        /* renamed from: a */
        public io.reactivex.h call() {
            return C0103a.f7367a;
        }
    }
}
