package io.reactivex.processors;

import io.reactivex.internal.a.b;
import io.reactivex.internal.queue.a;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.a.c;
import org.a.d;

public final class UnicastProcessor<T> extends a<T> {

    /* renamed from: b  reason: collision with root package name */
    final a<T> f7594b;

    /* renamed from: c  reason: collision with root package name */
    final AtomicReference<Runnable> f7595c;

    /* renamed from: d  reason: collision with root package name */
    final boolean f7596d;

    /* renamed from: e  reason: collision with root package name */
    volatile boolean f7597e;

    /* renamed from: f  reason: collision with root package name */
    Throwable f7598f;

    /* renamed from: g  reason: collision with root package name */
    final AtomicReference<c<? super T>> f7599g;

    /* renamed from: h  reason: collision with root package name */
    volatile boolean f7600h;
    final AtomicBoolean i;
    final BasicIntQueueSubscription<T> j;
    final AtomicLong k;
    boolean l;

    public static <T> UnicastProcessor<T> c() {
        return new UnicastProcessor<>(a());
    }

    UnicastProcessor(int i2) {
        this(i2, null, true);
    }

    UnicastProcessor(int i2, Runnable runnable, boolean z) {
        this.f7594b = new a<>(b.a(i2, "capacityHint"));
        this.f7595c = new AtomicReference<>(runnable);
        this.f7596d = z;
        this.f7599g = new AtomicReference<>();
        this.i = new AtomicBoolean();
        this.j = new UnicastQueueSubscription();
        this.k = new AtomicLong();
    }

    /* access modifiers changed from: package-private */
    public void e() {
        Runnable runnable = this.f7595c.get();
        if (runnable != null && this.f7595c.compareAndSet(runnable, null)) {
            runnable.run();
        }
    }

    /* access modifiers changed from: package-private */
    public void c(c<? super T> cVar) {
        long j2;
        int i2 = 1;
        a<T> aVar = this.f7594b;
        boolean z = !this.f7596d;
        while (true) {
            int i3 = i2;
            long j3 = this.k.get();
            long j4 = 0;
            while (true) {
                j2 = j4;
                if (j3 == j2) {
                    break;
                }
                boolean z2 = this.f7597e;
                T a2 = aVar.a();
                boolean z3 = a2 == null;
                if (!a(z, z2, z3, cVar, aVar)) {
                    if (z3) {
                        break;
                    }
                    cVar.c(a2);
                    j4 = 1 + j2;
                } else {
                    return;
                }
            }
            if (j3 != j2 || !a(z, this.f7597e, aVar.b(), cVar, aVar)) {
                if (!(j2 == 0 || j3 == Long.MAX_VALUE)) {
                    this.k.addAndGet(-j2);
                }
                i2 = this.j.addAndGet(-i3);
                if (i2 == 0) {
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(c<? super T> cVar) {
        int i2 = 1;
        a<T> aVar = this.f7594b;
        boolean z = !this.f7596d;
        while (!this.f7600h) {
            boolean z2 = this.f7597e;
            if (!z || !z2 || this.f7598f == null) {
                cVar.c(null);
                if (z2) {
                    this.f7599g.lazySet(null);
                    Throwable th = this.f7598f;
                    if (th != null) {
                        cVar.a(th);
                        return;
                    } else {
                        cVar.d();
                        return;
                    }
                } else {
                    i2 = this.j.addAndGet(-i2);
                    if (i2 == 0) {
                        return;
                    }
                }
            } else {
                aVar.g_();
                this.f7599g.lazySet(null);
                cVar.a(this.f7598f);
                return;
            }
        }
        aVar.g_();
        this.f7599g.lazySet(null);
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.j.getAndIncrement() == 0) {
            int i2 = 1;
            c cVar = this.f7599g.get();
            while (cVar == null) {
                i2 = this.j.addAndGet(-i2);
                if (i2 != 0) {
                    cVar = this.f7599g.get();
                } else {
                    return;
                }
            }
            if (this.l) {
                d(cVar);
            } else {
                c(cVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(boolean z, boolean z2, boolean z3, c<? super T> cVar, a<T> aVar) {
        if (this.f7600h) {
            aVar.g_();
            this.f7599g.lazySet(null);
            return true;
        }
        if (z2) {
            if (z && this.f7598f != null) {
                aVar.g_();
                this.f7599g.lazySet(null);
                cVar.a(this.f7598f);
                return true;
            } else if (z3) {
                Throwable th = this.f7598f;
                this.f7599g.lazySet(null);
                if (th != null) {
                    cVar.a(th);
                    return true;
                }
                cVar.d();
                return true;
            }
        }
        return false;
    }

    public void a(d dVar) {
        if (this.f7597e || this.f7600h) {
            dVar.c();
        } else {
            dVar.a(Long.MAX_VALUE);
        }
    }

    public void c(T t) {
        if (!this.f7597e && !this.f7600h) {
            if (t == null) {
                a(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
                return;
            }
            this.f7594b.a(t);
            f();
        }
    }

    public void a(Throwable th) {
        if (this.f7597e || this.f7600h) {
            io.reactivex.b.a.a(th);
            return;
        }
        if (th == null) {
            th = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        }
        this.f7598f = th;
        this.f7597e = true;
        e();
        f();
    }

    public void d() {
        if (!this.f7597e && !this.f7600h) {
            this.f7597e = true;
            e();
            f();
        }
    }

    /* access modifiers changed from: protected */
    public void b(c<? super T> cVar) {
        if (this.i.get() || !this.i.compareAndSet(false, true)) {
            EmptySubscription.a(new IllegalStateException("This processor allows only a single Subscriber"), cVar);
            return;
        }
        cVar.a(this.j);
        this.f7599g.set(cVar);
        if (this.f7600h) {
            this.f7599g.lazySet(null);
        } else {
            f();
        }
    }

    final class UnicastQueueSubscription extends BasicIntQueueSubscription<T> {
        UnicastQueueSubscription() {
        }

        public T a() {
            return UnicastProcessor.this.f7594b.a();
        }

        public boolean b() {
            return UnicastProcessor.this.f7594b.b();
        }

        public void g_() {
            UnicastProcessor.this.f7594b.g_();
        }

        public int a(int i) {
            if ((i & 2) == 0) {
                return 0;
            }
            UnicastProcessor.this.l = true;
            return 2;
        }

        public void a(long j) {
            if (SubscriptionHelper.b(j)) {
                io.reactivex.internal.util.b.a(UnicastProcessor.this.k, j);
                UnicastProcessor.this.f();
            }
        }

        public void c() {
            if (!UnicastProcessor.this.f7600h) {
                UnicastProcessor.this.f7600h = true;
                UnicastProcessor.this.e();
                if (!UnicastProcessor.this.l && UnicastProcessor.this.j.getAndIncrement() == 0) {
                    UnicastProcessor.this.f7594b.g_();
                    UnicastProcessor.this.f7599g.lazySet(null);
                }
            }
        }
    }
}
