package io.reactivex.processors;

import io.reactivex.d;
import io.reactivex.g;

/* compiled from: FlowableProcessor */
public abstract class a<T> extends d<T> implements g<T>, org.a.a<T, T> {
    public final a<T> b() {
        return this instanceof b ? this : new b(this);
    }
}
