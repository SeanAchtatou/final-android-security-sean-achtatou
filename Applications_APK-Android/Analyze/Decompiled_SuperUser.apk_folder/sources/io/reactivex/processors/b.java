package io.reactivex.processors;

import io.reactivex.internal.util.NotificationLite;
import io.reactivex.internal.util.a;
import org.a.c;
import org.a.d;

/* compiled from: SerializedProcessor */
final class b<T> extends a<T> {

    /* renamed from: b  reason: collision with root package name */
    final a<T> f7602b;

    /* renamed from: c  reason: collision with root package name */
    boolean f7603c;

    /* renamed from: d  reason: collision with root package name */
    a<Object> f7604d;

    /* renamed from: e  reason: collision with root package name */
    volatile boolean f7605e;

    b(a<T> aVar) {
        this.f7602b = aVar;
    }

    /* access modifiers changed from: protected */
    public void b(c<? super T> cVar) {
        this.f7602b.a(cVar);
    }

    public void a(d dVar) {
        boolean z = true;
        if (!this.f7605e) {
            synchronized (this) {
                if (!this.f7605e) {
                    if (this.f7603c) {
                        a<Object> aVar = this.f7604d;
                        if (aVar == null) {
                            aVar = new a<>(4);
                            this.f7604d = aVar;
                        }
                        aVar.a(NotificationLite.a(dVar));
                        return;
                    }
                    this.f7603c = true;
                    z = false;
                }
            }
        }
        if (z) {
            dVar.c();
            return;
        }
        this.f7602b.a(dVar);
        c();
    }

    public void c(T t) {
        if (!this.f7605e) {
            synchronized (this) {
                if (!this.f7605e) {
                    if (this.f7603c) {
                        a<Object> aVar = this.f7604d;
                        if (aVar == null) {
                            aVar = new a<>(4);
                            this.f7604d = aVar;
                        }
                        aVar.a(NotificationLite.a((Object) t));
                        return;
                    }
                    this.f7603c = true;
                    this.f7602b.c(t);
                    c();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0039, code lost:
        r2.f7602b.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000f, code lost:
        if (r0 == false) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        io.reactivex.b.a.a(r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.Throwable r3) {
        /*
            r2 = this;
            r0 = 1
            boolean r1 = r2.f7605e
            if (r1 == 0) goto L_0x0009
            io.reactivex.b.a.a(r3)
        L_0x0008:
            return
        L_0x0009:
            monitor-enter(r2)
            boolean r1 = r2.f7605e     // Catch:{ all -> 0x0031 }
            if (r1 == 0) goto L_0x0015
        L_0x000e:
            monitor-exit(r2)     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x0039
            io.reactivex.b.a.a(r3)
            goto L_0x0008
        L_0x0015:
            r0 = 1
            r2.f7605e = r0     // Catch:{ all -> 0x0031 }
            boolean r0 = r2.f7603c     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x0034
            io.reactivex.internal.util.a<java.lang.Object> r0 = r2.f7604d     // Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x0028
            io.reactivex.internal.util.a r0 = new io.reactivex.internal.util.a     // Catch:{ all -> 0x0031 }
            r1 = 4
            r0.<init>(r1)     // Catch:{ all -> 0x0031 }
            r2.f7604d = r0     // Catch:{ all -> 0x0031 }
        L_0x0028:
            java.lang.Object r1 = io.reactivex.internal.util.NotificationLite.a(r3)     // Catch:{ all -> 0x0031 }
            r0.b(r1)     // Catch:{ all -> 0x0031 }
            monitor-exit(r2)     // Catch:{ all -> 0x0031 }
            goto L_0x0008
        L_0x0031:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0031 }
            throw r0
        L_0x0034:
            r0 = 0
            r1 = 1
            r2.f7603c = r1     // Catch:{ all -> 0x0031 }
            goto L_0x000e
        L_0x0039:
            io.reactivex.processors.a<T> r0 = r2.f7602b
            r0.a(r3)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: io.reactivex.processors.b.a(java.lang.Throwable):void");
    }

    public void d() {
        if (!this.f7605e) {
            synchronized (this) {
                if (!this.f7605e) {
                    this.f7605e = true;
                    if (this.f7603c) {
                        a<Object> aVar = this.f7604d;
                        if (aVar == null) {
                            aVar = new a<>(4);
                            this.f7604d = aVar;
                        }
                        aVar.a(NotificationLite.a());
                        return;
                    }
                    this.f7603c = true;
                    this.f7602b.d();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a<Object> aVar;
        while (true) {
            synchronized (this) {
                aVar = this.f7604d;
                if (aVar == null) {
                    this.f7603c = false;
                    return;
                }
                this.f7604d = null;
            }
            aVar.a((c) this.f7602b);
        }
        while (true) {
        }
    }
}
