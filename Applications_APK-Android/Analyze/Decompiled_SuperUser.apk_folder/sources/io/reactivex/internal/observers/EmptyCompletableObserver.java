package io.reactivex.internal.observers;

import io.reactivex.disposables.b;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class EmptyCompletableObserver extends AtomicReference<b> implements io.reactivex.b, b {
    public void dispose() {
        DisposableHelper.a((AtomicReference<b>) this);
    }

    public void a() {
        lazySet(DisposableHelper.DISPOSED);
    }

    public void a(b bVar) {
        DisposableHelper.a(this, bVar);
    }
}
