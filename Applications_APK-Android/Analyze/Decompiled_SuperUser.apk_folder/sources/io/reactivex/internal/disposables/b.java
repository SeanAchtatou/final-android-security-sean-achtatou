package io.reactivex.internal.disposables;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.a;
import io.reactivex.internal.util.ExceptionHelper;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* compiled from: ListCompositeDisposable */
public final class b implements io.reactivex.disposables.b, a {

    /* renamed from: a  reason: collision with root package name */
    List<io.reactivex.disposables.b> f7406a;

    /* renamed from: b  reason: collision with root package name */
    volatile boolean f7407b;

    public void dispose() {
        if (!this.f7407b) {
            synchronized (this) {
                if (!this.f7407b) {
                    this.f7407b = true;
                    List<io.reactivex.disposables.b> list = this.f7406a;
                    this.f7406a = null;
                    a(list);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.disposables.b, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public boolean a(io.reactivex.disposables.b bVar) {
        io.reactivex.internal.a.b.a((Object) bVar, "d is null");
        if (!this.f7407b) {
            synchronized (this) {
                if (!this.f7407b) {
                    List list = this.f7406a;
                    if (list == null) {
                        list = new LinkedList();
                        this.f7406a = list;
                    }
                    list.add(bVar);
                    return true;
                }
            }
        }
        bVar.dispose();
        return false;
    }

    public boolean b(io.reactivex.disposables.b bVar) {
        if (!c(bVar)) {
            return false;
        }
        bVar.dispose();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.disposables.b, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean c(io.reactivex.disposables.b r3) {
        /*
            r2 = this;
            r0 = 0
            java.lang.String r1 = "Disposable item is null"
            io.reactivex.internal.a.b.a(r3, r1)
            boolean r1 = r2.f7407b
            if (r1 == 0) goto L_0x000b
        L_0x000a:
            return r0
        L_0x000b:
            monitor-enter(r2)
            boolean r1 = r2.f7407b     // Catch:{ all -> 0x0012 }
            if (r1 == 0) goto L_0x0015
            monitor-exit(r2)     // Catch:{ all -> 0x0012 }
            goto L_0x000a
        L_0x0012:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0012 }
            throw r0
        L_0x0015:
            java.util.List<io.reactivex.disposables.b> r1 = r2.f7406a     // Catch:{ all -> 0x0012 }
            if (r1 == 0) goto L_0x001f
            boolean r1 = r1.remove(r3)     // Catch:{ all -> 0x0012 }
            if (r1 != 0) goto L_0x0021
        L_0x001f:
            monitor-exit(r2)     // Catch:{ all -> 0x0012 }
            goto L_0x000a
        L_0x0021:
            monitor-exit(r2)     // Catch:{ all -> 0x0012 }
            r0 = 1
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: io.reactivex.internal.disposables.b.c(io.reactivex.disposables.b):boolean");
    }

    /* access modifiers changed from: package-private */
    public void a(List<io.reactivex.disposables.b> list) {
        ArrayList arrayList;
        if (list != null) {
            ArrayList arrayList2 = null;
            for (io.reactivex.disposables.b dispose : list) {
                try {
                    dispose.dispose();
                } catch (Throwable th) {
                    a.b(th);
                    if (arrayList2 == null) {
                        arrayList = new ArrayList();
                    } else {
                        arrayList = arrayList2;
                    }
                    arrayList.add(th);
                    arrayList2 = arrayList;
                }
            }
            if (arrayList2 == null) {
                return;
            }
            if (arrayList2.size() == 1) {
                throw ExceptionHelper.a((Throwable) arrayList2.get(0));
            }
            throw new CompositeException(arrayList2);
        }
    }
}
