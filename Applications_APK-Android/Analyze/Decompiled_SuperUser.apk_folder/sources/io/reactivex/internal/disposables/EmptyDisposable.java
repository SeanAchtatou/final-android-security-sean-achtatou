package io.reactivex.internal.disposables;

import io.reactivex.internal.b.b;

public enum EmptyDisposable implements b<Object> {
    INSTANCE,
    NEVER;

    public void dispose() {
    }

    public boolean a(Object obj) {
        throw new UnsupportedOperationException("Should not be called!");
    }

    public Object a() {
        return null;
    }

    public boolean b() {
        return true;
    }

    public void g_() {
    }

    public int a(int i) {
        return i & 2;
    }
}
