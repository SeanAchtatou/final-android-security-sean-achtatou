package io.reactivex.internal.operators.flowable;

import io.reactivex.BackpressureStrategy;
import io.reactivex.d;
import io.reactivex.e;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.exceptions.a;
import io.reactivex.f;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.b;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.a.c;

public final class FlowableCreate<T> extends d<T> {

    /* renamed from: b  reason: collision with root package name */
    final f<T> f7408b;

    /* renamed from: c  reason: collision with root package name */
    final BackpressureStrategy f7409c;

    public FlowableCreate(f<T> fVar, BackpressureStrategy backpressureStrategy) {
        this.f7408b = fVar;
        this.f7409c = backpressureStrategy;
    }

    public void b(c<? super T> cVar) {
        BaseEmitter latestAsyncEmitter;
        switch (this.f7409c) {
            case MISSING:
                latestAsyncEmitter = new MissingEmitter(cVar);
                break;
            case ERROR:
                latestAsyncEmitter = new ErrorAsyncEmitter(cVar);
                break;
            case DROP:
                latestAsyncEmitter = new DropAsyncEmitter(cVar);
                break;
            case LATEST:
                latestAsyncEmitter = new LatestAsyncEmitter(cVar);
                break;
            default:
                latestAsyncEmitter = new BufferAsyncEmitter(cVar, a());
                break;
        }
        cVar.a(latestAsyncEmitter);
        try {
            this.f7408b.subscribe(latestAsyncEmitter);
        } catch (Throwable th) {
            a.b(th);
            latestAsyncEmitter.a(th);
        }
    }

    static abstract class BaseEmitter<T> extends AtomicLong implements e<T>, org.a.d {

        /* renamed from: a  reason: collision with root package name */
        final c<? super T> f7411a;

        /* renamed from: b  reason: collision with root package name */
        final SequentialDisposable f7412b = new SequentialDisposable();

        BaseEmitter(c<? super T> cVar) {
            this.f7411a = cVar;
        }

        public void a() {
            b();
        }

        /* access modifiers changed from: protected */
        public void b() {
            if (!e()) {
                try {
                    this.f7411a.d();
                } finally {
                    this.f7412b.dispose();
                }
            }
        }

        public final void a(Throwable th) {
            if (!b(th)) {
                io.reactivex.b.a.a(th);
            }
        }

        public boolean b(Throwable th) {
            return c(th);
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: protected */
        public boolean c(Throwable th) {
            if (th == null) {
                th = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
            }
            if (e()) {
                return false;
            }
            try {
                this.f7411a.a(th);
                this.f7412b.dispose();
                return true;
            } catch (Throwable th2) {
                this.f7412b.dispose();
                throw th2;
            }
        }

        public final void c() {
            this.f7412b.dispose();
            d();
        }

        /* access modifiers changed from: package-private */
        public void d() {
        }

        public final boolean e() {
            return this.f7412b.a();
        }

        public final void a(long j) {
            if (SubscriptionHelper.b(j)) {
                b.a(this, j);
                f();
            }
        }

        /* access modifiers changed from: package-private */
        public void f() {
        }
    }

    static final class MissingEmitter<T> extends BaseEmitter<T> {
        MissingEmitter(c<? super T> cVar) {
            super(cVar);
        }

        public void a(T t) {
            long j;
            if (!e()) {
                if (t != null) {
                    this.f7411a.c(t);
                    do {
                        j = get();
                        if (j == 0) {
                            return;
                        }
                    } while (!compareAndSet(j, j - 1));
                    return;
                }
                a((Throwable) new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
            }
        }
    }

    static abstract class NoOverflowBaseAsyncEmitter<T> extends BaseEmitter<T> {
        /* access modifiers changed from: package-private */
        public abstract void g();

        NoOverflowBaseAsyncEmitter(c<? super T> cVar) {
            super(cVar);
        }

        public final void a(T t) {
            if (!e()) {
                if (t == null) {
                    a((Throwable) new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
                } else if (get() != 0) {
                    this.f7411a.c(t);
                    b.b(this, 1);
                } else {
                    g();
                }
            }
        }
    }

    static final class DropAsyncEmitter<T> extends NoOverflowBaseAsyncEmitter<T> {
        DropAsyncEmitter(c<? super T> cVar) {
            super(cVar);
        }

        /* access modifiers changed from: package-private */
        public void g() {
        }
    }

    static final class ErrorAsyncEmitter<T> extends NoOverflowBaseAsyncEmitter<T> {
        ErrorAsyncEmitter(c<? super T> cVar) {
            super(cVar);
        }

        /* access modifiers changed from: package-private */
        public void g() {
            a((Throwable) new MissingBackpressureException("create: could not emit value due to lack of requests"));
        }
    }

    static final class BufferAsyncEmitter<T> extends BaseEmitter<T> {

        /* renamed from: c  reason: collision with root package name */
        final io.reactivex.internal.queue.a<T> f7413c;

        /* renamed from: d  reason: collision with root package name */
        Throwable f7414d;

        /* renamed from: e  reason: collision with root package name */
        volatile boolean f7415e;

        /* renamed from: f  reason: collision with root package name */
        final AtomicInteger f7416f = new AtomicInteger();

        BufferAsyncEmitter(c<? super T> cVar, int i) {
            super(cVar);
            this.f7413c = new io.reactivex.internal.queue.a<>(i);
        }

        public void a(T t) {
            if (!this.f7415e && !e()) {
                if (t == null) {
                    a((Throwable) new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
                    return;
                }
                this.f7413c.a(t);
                g();
            }
        }

        public boolean b(Throwable th) {
            if (this.f7415e || e()) {
                return false;
            }
            if (th == null) {
                th = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
            }
            this.f7414d = th;
            this.f7415e = true;
            g();
            return true;
        }

        public void a() {
            this.f7415e = true;
            g();
        }

        /* access modifiers changed from: package-private */
        public void f() {
            g();
        }

        /* access modifiers changed from: package-private */
        public void d() {
            if (this.f7416f.getAndIncrement() == 0) {
                this.f7413c.g_();
            }
        }

        /* access modifiers changed from: package-private */
        public void g() {
            if (this.f7416f.getAndIncrement() == 0) {
                c cVar = this.f7411a;
                io.reactivex.internal.queue.a<T> aVar = this.f7413c;
                int i = 1;
                do {
                    long j = get();
                    long j2 = 0;
                    while (j2 != j) {
                        if (e()) {
                            aVar.g_();
                            return;
                        }
                        boolean z = this.f7415e;
                        T a2 = aVar.a();
                        boolean z2 = a2 == null;
                        if (z && z2) {
                            Throwable th = this.f7414d;
                            if (th != null) {
                                c(th);
                                return;
                            } else {
                                b();
                                return;
                            }
                        } else if (z2) {
                            break;
                        } else {
                            cVar.c(a2);
                            j2 = 1 + j2;
                        }
                    }
                    if (j2 == j) {
                        if (e()) {
                            aVar.g_();
                            return;
                        }
                        boolean z3 = this.f7415e;
                        boolean b2 = aVar.b();
                        if (z3 && b2) {
                            Throwable th2 = this.f7414d;
                            if (th2 != null) {
                                c(th2);
                                return;
                            } else {
                                b();
                                return;
                            }
                        }
                    }
                    if (j2 != 0) {
                        b.b(this, j2);
                    }
                    i = this.f7416f.addAndGet(-i);
                } while (i != 0);
            }
        }
    }

    static final class LatestAsyncEmitter<T> extends BaseEmitter<T> {

        /* renamed from: c  reason: collision with root package name */
        final AtomicReference<T> f7417c = new AtomicReference<>();

        /* renamed from: d  reason: collision with root package name */
        Throwable f7418d;

        /* renamed from: e  reason: collision with root package name */
        volatile boolean f7419e;

        /* renamed from: f  reason: collision with root package name */
        final AtomicInteger f7420f = new AtomicInteger();

        LatestAsyncEmitter(c<? super T> cVar) {
            super(cVar);
        }

        public void a(T t) {
            if (!this.f7419e && !e()) {
                if (t == null) {
                    a((Throwable) new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
                    return;
                }
                this.f7417c.set(t);
                g();
            }
        }

        public boolean b(Throwable th) {
            if (this.f7419e || e()) {
                return false;
            }
            if (th == null) {
                a((Throwable) new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources."));
            }
            this.f7418d = th;
            this.f7419e = true;
            g();
            return true;
        }

        public void a() {
            this.f7419e = true;
            g();
        }

        /* access modifiers changed from: package-private */
        public void f() {
            g();
        }

        /* access modifiers changed from: package-private */
        public void d() {
            if (this.f7420f.getAndIncrement() == 0) {
                this.f7417c.lazySet(null);
            }
        }

        /* access modifiers changed from: package-private */
        public void g() {
            if (this.f7420f.getAndIncrement() == 0) {
                c cVar = this.f7411a;
                AtomicReference<T> atomicReference = this.f7417c;
                int i = 1;
                do {
                    long j = get();
                    long j2 = 0;
                    while (j2 != j) {
                        if (e()) {
                            atomicReference.lazySet(null);
                            return;
                        }
                        boolean z = this.f7419e;
                        T andSet = atomicReference.getAndSet(null);
                        boolean z2 = andSet == null;
                        if (z && z2) {
                            Throwable th = this.f7418d;
                            if (th != null) {
                                c(th);
                                return;
                            } else {
                                b();
                                return;
                            }
                        } else if (z2) {
                            break;
                        } else {
                            cVar.c(andSet);
                            j2++;
                        }
                    }
                    if (j2 == j) {
                        if (e()) {
                            atomicReference.lazySet(null);
                            return;
                        }
                        boolean z3 = this.f7419e;
                        boolean z4 = atomicReference.get() == null;
                        if (z3 && z4) {
                            Throwable th2 = this.f7418d;
                            if (th2 != null) {
                                c(th2);
                                return;
                            } else {
                                b();
                                return;
                            }
                        }
                    }
                    if (j2 != 0) {
                        b.b(this, j2);
                    }
                    i = this.f7420f.addAndGet(-i);
                } while (i != 0);
            }
        }
    }
}
