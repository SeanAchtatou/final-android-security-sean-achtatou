package io.reactivex.internal.operators.flowable;

import io.reactivex.d;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.g;
import io.reactivex.h;
import io.reactivex.internal.b.a;
import io.reactivex.internal.b.f;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.b;
import java.util.concurrent.atomic.AtomicLong;
import org.a.c;

public final class FlowableObserveOn<T> extends a<T, T> {

    /* renamed from: c  reason: collision with root package name */
    final h f7423c;

    /* renamed from: d  reason: collision with root package name */
    final boolean f7424d;

    /* renamed from: e  reason: collision with root package name */
    final int f7425e;

    public FlowableObserveOn(d<T> dVar, h hVar, boolean z, int i) {
        super(dVar);
        this.f7423c = hVar;
        this.f7424d = z;
        this.f7425e = i;
    }

    public void b(c<? super T> cVar) {
        h.c createWorker = this.f7423c.createWorker();
        if (cVar instanceof a) {
            this.f7444b.a((g) new ObserveOnConditionalSubscriber((a) cVar, createWorker, this.f7424d, this.f7425e));
        } else {
            this.f7444b.a((g) new ObserveOnSubscriber(cVar, createWorker, this.f7424d, this.f7425e));
        }
    }

    static abstract class BaseObserveOnSubscriber<T> extends BasicIntQueueSubscription<T> implements g<T>, Runnable {

        /* renamed from: a  reason: collision with root package name */
        final h.c f7426a;

        /* renamed from: b  reason: collision with root package name */
        final boolean f7427b;

        /* renamed from: c  reason: collision with root package name */
        final int f7428c;

        /* renamed from: d  reason: collision with root package name */
        final int f7429d;

        /* renamed from: e  reason: collision with root package name */
        final AtomicLong f7430e = new AtomicLong();

        /* renamed from: f  reason: collision with root package name */
        org.a.d f7431f;

        /* renamed from: g  reason: collision with root package name */
        f<T> f7432g;

        /* renamed from: h  reason: collision with root package name */
        volatile boolean f7433h;
        volatile boolean i;
        Throwable j;
        int k;
        long l;
        boolean m;

        /* access modifiers changed from: package-private */
        public abstract void f();

        /* access modifiers changed from: package-private */
        public abstract void g();

        /* access modifiers changed from: package-private */
        public abstract void h();

        BaseObserveOnSubscriber(h.c cVar, boolean z, int i2) {
            this.f7426a = cVar;
            this.f7427b = z;
            this.f7428c = i2;
            this.f7429d = i2 - (i2 >> 2);
        }

        public final void c(T t) {
            if (!this.i) {
                if (this.k == 2) {
                    e();
                    return;
                }
                if (!this.f7432g.a(t)) {
                    this.f7431f.c();
                    this.j = new MissingBackpressureException("Queue is full?!");
                    this.i = true;
                }
                e();
            }
        }

        public final void a(Throwable th) {
            if (this.i) {
                io.reactivex.b.a.a(th);
                return;
            }
            this.j = th;
            this.i = true;
            e();
        }

        public final void d() {
            if (!this.i) {
                this.i = true;
                e();
            }
        }

        public final void a(long j2) {
            if (SubscriptionHelper.b(j2)) {
                b.a(this.f7430e, j2);
                e();
            }
        }

        public final void c() {
            if (!this.f7433h) {
                this.f7433h = true;
                this.f7431f.c();
                this.f7426a.dispose();
                if (getAndIncrement() == 0) {
                    this.f7432g.g_();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public final void e() {
            if (getAndIncrement() == 0) {
                this.f7426a.schedule(this);
            }
        }

        public final void run() {
            if (this.m) {
                f();
            } else if (this.k == 1) {
                g();
            } else {
                h();
            }
        }

        /* access modifiers changed from: package-private */
        public final boolean a(boolean z, boolean z2, c<?> cVar) {
            if (this.f7433h) {
                g_();
                return true;
            }
            if (z) {
                if (!this.f7427b) {
                    Throwable th = this.j;
                    if (th != null) {
                        g_();
                        cVar.a(th);
                        this.f7426a.dispose();
                        return true;
                    } else if (z2) {
                        cVar.d();
                        this.f7426a.dispose();
                        return true;
                    }
                } else if (z2) {
                    Throwable th2 = this.j;
                    if (th2 != null) {
                        cVar.a(th2);
                    } else {
                        cVar.d();
                    }
                    this.f7426a.dispose();
                    return true;
                }
            }
            return false;
        }

        public final int a(int i2) {
            if ((i2 & 2) == 0) {
                return 0;
            }
            this.m = true;
            return 2;
        }

        public final void g_() {
            this.f7432g.g_();
        }

        public final boolean b() {
            return this.f7432g.b();
        }
    }

    static final class ObserveOnSubscriber<T> extends BaseObserveOnSubscriber<T> implements g<T> {
        final c<? super T> n;

        ObserveOnSubscriber(c<? super T> cVar, h.c cVar2, boolean z, int i) {
            super(cVar2, z, i);
            this.n = cVar;
        }

        public void a(org.a.d dVar) {
            if (SubscriptionHelper.a(this.f7431f, dVar)) {
                this.f7431f = dVar;
                if (dVar instanceof io.reactivex.internal.b.d) {
                    io.reactivex.internal.b.d dVar2 = (io.reactivex.internal.b.d) dVar;
                    int a2 = dVar2.a(7);
                    if (a2 == 1) {
                        this.k = 1;
                        this.f7432g = dVar2;
                        this.i = true;
                        this.n.a(this);
                        return;
                    } else if (a2 == 2) {
                        this.k = 2;
                        this.f7432g = dVar2;
                        this.n.a(this);
                        dVar.a((long) this.f7428c);
                        return;
                    }
                }
                this.f7432g = new SpscArrayQueue(this.f7428c);
                this.n.a(this);
                dVar.a((long) this.f7428c);
            }
        }

        /* access modifiers changed from: package-private */
        public void g() {
            int i = 1;
            c<? super T> cVar = this.n;
            f fVar = this.f7432g;
            long j = this.l;
            while (true) {
                long j2 = this.f7430e.get();
                while (j != j2) {
                    try {
                        Object a2 = fVar.a();
                        if (!this.f7433h) {
                            if (a2 == null) {
                                cVar.d();
                                this.f7426a.dispose();
                                return;
                            }
                            cVar.c(a2);
                            j++;
                        } else {
                            return;
                        }
                    } catch (Throwable th) {
                        io.reactivex.exceptions.a.b(th);
                        this.f7431f.c();
                        cVar.a(th);
                        this.f7426a.dispose();
                        return;
                    }
                }
                if (this.f7433h) {
                    return;
                }
                if (fVar.b()) {
                    cVar.d();
                    this.f7426a.dispose();
                    return;
                }
                int i2 = get();
                if (i == i2) {
                    this.l = j;
                    i = addAndGet(-i);
                    if (i == 0) {
                        return;
                    }
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void h() {
            long j;
            c<? super T> cVar = this.n;
            f fVar = this.f7432g;
            long j2 = this.l;
            int i = 1;
            while (true) {
                long j3 = this.f7430e.get();
                while (j2 != j3) {
                    boolean z = this.i;
                    try {
                        Object a2 = fVar.a();
                        boolean z2 = a2 == null;
                        if (!a(z, z2, cVar)) {
                            if (z2) {
                                break;
                            }
                            cVar.c(a2);
                            long j4 = 1 + j2;
                            if (j4 == ((long) this.f7429d)) {
                                if (j3 != Long.MAX_VALUE) {
                                    j = this.f7430e.addAndGet(-j4);
                                } else {
                                    j = j3;
                                }
                                this.f7431f.a(j4);
                                j4 = 0;
                            } else {
                                j = j3;
                            }
                            j3 = j;
                            j2 = j4;
                        } else {
                            return;
                        }
                    } catch (Throwable th) {
                        io.reactivex.exceptions.a.b(th);
                        this.f7431f.c();
                        fVar.g_();
                        cVar.a(th);
                        this.f7426a.dispose();
                        return;
                    }
                }
                if (j2 != j3 || !a(this.i, fVar.b(), cVar)) {
                    int i2 = get();
                    if (i == i2) {
                        this.l = j2;
                        i = addAndGet(-i);
                        if (i == 0) {
                            return;
                        }
                    } else {
                        i = i2;
                    }
                } else {
                    return;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void f() {
            int i = 1;
            while (!this.f7433h) {
                boolean z = this.i;
                this.n.c(null);
                if (z) {
                    Throwable th = this.j;
                    if (th != null) {
                        this.n.a(th);
                    } else {
                        this.n.d();
                    }
                    this.f7426a.dispose();
                    return;
                }
                i = addAndGet(-i);
                if (i == 0) {
                    return;
                }
            }
        }

        public T a() {
            T a2 = this.f7432g.a();
            if (!(a2 == null || this.k == 1)) {
                long j = this.l + 1;
                if (j == ((long) this.f7429d)) {
                    this.l = 0;
                    this.f7431f.a(j);
                } else {
                    this.l = j;
                }
            }
            return a2;
        }
    }

    static final class ObserveOnConditionalSubscriber<T> extends BaseObserveOnSubscriber<T> {
        final a<? super T> n;
        long o;

        ObserveOnConditionalSubscriber(a<? super T> aVar, h.c cVar, boolean z, int i) {
            super(cVar, z, i);
            this.n = aVar;
        }

        public void a(org.a.d dVar) {
            if (SubscriptionHelper.a(this.f7431f, dVar)) {
                this.f7431f = dVar;
                if (dVar instanceof io.reactivex.internal.b.d) {
                    io.reactivex.internal.b.d dVar2 = (io.reactivex.internal.b.d) dVar;
                    int a2 = dVar2.a(7);
                    if (a2 == 1) {
                        this.k = 1;
                        this.f7432g = dVar2;
                        this.i = true;
                        this.n.a(this);
                        return;
                    } else if (a2 == 2) {
                        this.k = 2;
                        this.f7432g = dVar2;
                        this.n.a(this);
                        dVar.a((long) this.f7428c);
                        return;
                    }
                }
                this.f7432g = new SpscArrayQueue(this.f7428c);
                this.n.a(this);
                dVar.a((long) this.f7428c);
            }
        }

        /* access modifiers changed from: package-private */
        public void g() {
            int i = 1;
            a<? super T> aVar = this.n;
            f fVar = this.f7432g;
            long j = this.l;
            while (true) {
                long j2 = this.f7430e.get();
                while (j != j2) {
                    try {
                        Object a2 = fVar.a();
                        if (!this.f7433h) {
                            if (a2 == null) {
                                aVar.d();
                                this.f7426a.dispose();
                                return;
                            } else if (aVar.b(a2)) {
                                j++;
                            }
                        } else {
                            return;
                        }
                    } catch (Throwable th) {
                        io.reactivex.exceptions.a.b(th);
                        this.f7431f.c();
                        aVar.a(th);
                        this.f7426a.dispose();
                        return;
                    }
                }
                if (this.f7433h) {
                    return;
                }
                if (fVar.b()) {
                    aVar.d();
                    this.f7426a.dispose();
                    return;
                }
                int i2 = get();
                if (i == i2) {
                    this.l = j;
                    i = addAndGet(-i);
                    if (i == 0) {
                        return;
                    }
                } else {
                    i = i2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void h() {
            int i = 1;
            a<? super T> aVar = this.n;
            f fVar = this.f7432g;
            long j = this.l;
            long j2 = this.o;
            while (true) {
                long j3 = this.f7430e.get();
                while (j != j3) {
                    boolean z = this.i;
                    try {
                        Object a2 = fVar.a();
                        boolean z2 = a2 == null;
                        if (!a(z, z2, aVar)) {
                            if (z2) {
                                break;
                            }
                            if (aVar.b(a2)) {
                                j++;
                            }
                            j2++;
                            if (j2 == ((long) this.f7429d)) {
                                this.f7431f.a(j2);
                                j2 = 0;
                            }
                        } else {
                            return;
                        }
                    } catch (Throwable th) {
                        io.reactivex.exceptions.a.b(th);
                        this.f7431f.c();
                        fVar.g_();
                        aVar.a(th);
                        this.f7426a.dispose();
                        return;
                    }
                }
                if (j != j3 || !a(this.i, fVar.b(), aVar)) {
                    int i2 = get();
                    if (i == i2) {
                        this.l = j;
                        this.o = j2;
                        i = addAndGet(-i);
                        if (i == 0) {
                            return;
                        }
                    } else {
                        i = i2;
                    }
                } else {
                    return;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void f() {
            int i = 1;
            while (!this.f7433h) {
                boolean z = this.i;
                this.n.c(null);
                if (z) {
                    Throwable th = this.j;
                    if (th != null) {
                        this.n.a(th);
                    } else {
                        this.n.d();
                    }
                    this.f7426a.dispose();
                    return;
                }
                i = addAndGet(-i);
                if (i == 0) {
                    return;
                }
            }
        }

        public T a() {
            T a2 = this.f7432g.a();
            if (!(a2 == null || this.k == 1)) {
                long j = this.o + 1;
                if (j == ((long) this.f7429d)) {
                    this.o = 0;
                    this.f7431f.a(j);
                } else {
                    this.o = j;
                }
            }
            return a2;
        }
    }
}
