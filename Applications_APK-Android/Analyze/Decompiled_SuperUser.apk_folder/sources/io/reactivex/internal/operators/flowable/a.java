package io.reactivex.internal.operators.flowable;

import io.reactivex.d;
import io.reactivex.internal.a.b;

/* compiled from: AbstractFlowableWithUpstream */
abstract class a<T, R> extends d<R> {

    /* renamed from: b  reason: collision with root package name */
    protected final d<T> f7444b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.d<T>, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    a(d<T> dVar) {
        this.f7444b = (d) b.a((Object) dVar, "source is null");
    }
}
