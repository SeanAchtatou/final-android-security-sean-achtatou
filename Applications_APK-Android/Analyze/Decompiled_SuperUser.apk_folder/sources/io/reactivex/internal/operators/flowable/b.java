package io.reactivex.internal.operators.flowable;

import io.reactivex.a.e;
import io.reactivex.d;
import io.reactivex.g;
import org.a.c;

/* compiled from: FlowableMap */
public final class b<T, U> extends a<T, U> {

    /* renamed from: c  reason: collision with root package name */
    final e<? super T, ? extends U> f7445c;

    public b(d<T> dVar, e<? super T, ? extends U> eVar) {
        super(dVar);
        this.f7445c = eVar;
    }

    /* access modifiers changed from: protected */
    public void b(c<? super U> cVar) {
        if (cVar instanceof io.reactivex.internal.b.a) {
            this.f7444b.a((g) new a((io.reactivex.internal.b.a) cVar, this.f7445c));
        } else {
            this.f7444b.a((g) new C0105b(cVar, this.f7445c));
        }
    }

    /* renamed from: io.reactivex.internal.operators.flowable.b$b  reason: collision with other inner class name */
    /* compiled from: FlowableMap */
    static final class C0105b<T, U> extends io.reactivex.internal.subscribers.b<T, U> {

        /* renamed from: a  reason: collision with root package name */
        final e<? super T, ? extends U> f7447a;

        C0105b(c<? super U> cVar, e<? super T, ? extends U> eVar) {
            super(cVar);
            this.f7447a = eVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
         arg types: [? extends U, java.lang.String]
         candidates:
          io.reactivex.internal.a.b.a(int, int):int
          io.reactivex.internal.a.b.a(int, java.lang.String):int
          io.reactivex.internal.a.b.a(long, long):int
          io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
          io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
        public void c(T t) {
            if (!this.f7574e) {
                if (this.f7575f != 0) {
                    this.f7571b.c(null);
                    return;
                }
                try {
                    this.f7571b.c(io.reactivex.internal.a.b.a((Object) this.f7447a.a(t), "The mapper function returned a null value."));
                } catch (Throwable th) {
                    b(th);
                }
            }
        }

        public int a(int i) {
            return b(i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
         arg types: [? extends U, java.lang.String]
         candidates:
          io.reactivex.internal.a.b.a(int, int):int
          io.reactivex.internal.a.b.a(int, java.lang.String):int
          io.reactivex.internal.a.b.a(long, long):int
          io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
          io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
        public U a() {
            Object a2 = this.f7573d.a();
            if (a2 != null) {
                return io.reactivex.internal.a.b.a((Object) this.f7447a.a(a2), "The mapper function returned a null value.");
            }
            return null;
        }
    }

    /* compiled from: FlowableMap */
    static final class a<T, U> extends io.reactivex.internal.subscribers.a<T, U> {

        /* renamed from: a  reason: collision with root package name */
        final e<? super T, ? extends U> f7446a;

        a(io.reactivex.internal.b.a<? super U> aVar, e<? super T, ? extends U> eVar) {
            super(aVar);
            this.f7446a = eVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
         arg types: [? extends U, java.lang.String]
         candidates:
          io.reactivex.internal.a.b.a(int, int):int
          io.reactivex.internal.a.b.a(int, java.lang.String):int
          io.reactivex.internal.a.b.a(long, long):int
          io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
          io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
        public void c(T t) {
            if (!this.f7569e) {
                if (this.f7570f != 0) {
                    this.f7566b.c(null);
                    return;
                }
                try {
                    this.f7566b.c(io.reactivex.internal.a.b.a((Object) this.f7446a.a(t), "The mapper function returned a null value."));
                } catch (Throwable th) {
                    b(th);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
         arg types: [? extends U, java.lang.String]
         candidates:
          io.reactivex.internal.a.b.a(int, int):int
          io.reactivex.internal.a.b.a(int, java.lang.String):int
          io.reactivex.internal.a.b.a(long, long):int
          io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
          io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
        public boolean b(T t) {
            if (this.f7569e) {
                return false;
            }
            try {
                return this.f7566b.b(io.reactivex.internal.a.b.a((Object) this.f7446a.a(t), "The mapper function returned a null value."));
            } catch (Throwable th) {
                b(th);
                return true;
            }
        }

        public int a(int i) {
            return b(i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
         arg types: [? extends U, java.lang.String]
         candidates:
          io.reactivex.internal.a.b.a(int, int):int
          io.reactivex.internal.a.b.a(int, java.lang.String):int
          io.reactivex.internal.a.b.a(long, long):int
          io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
          io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
        public U a() {
            Object a2 = this.f7568d.a();
            if (a2 != null) {
                return io.reactivex.internal.a.b.a((Object) this.f7446a.a(a2), "The mapper function returned a null value.");
            }
            return null;
        }
    }
}
