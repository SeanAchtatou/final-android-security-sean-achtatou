package io.reactivex.internal.operators.flowable;

import io.reactivex.a.d;

public final class FlowableInternalHelper {

    public enum RequestMax implements d<org.a.d> {
        INSTANCE;

        /* renamed from: a */
        public void accept(org.a.d dVar) {
            dVar.a(Long.MAX_VALUE);
        }
    }
}
