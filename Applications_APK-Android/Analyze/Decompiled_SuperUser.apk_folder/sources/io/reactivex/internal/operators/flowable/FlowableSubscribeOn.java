package io.reactivex.internal.operators.flowable;

import io.reactivex.d;
import io.reactivex.g;
import io.reactivex.h;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.a.b;
import org.a.c;

public final class FlowableSubscribeOn<T> extends a<T, T> {

    /* renamed from: c  reason: collision with root package name */
    final h f7434c;

    /* renamed from: d  reason: collision with root package name */
    final boolean f7435d;

    public FlowableSubscribeOn(d<T> dVar, h hVar, boolean z) {
        super(dVar);
        this.f7434c = hVar;
        this.f7435d = z;
    }

    public void b(c<? super T> cVar) {
        h.c createWorker = this.f7434c.createWorker();
        SubscribeOnSubscriber subscribeOnSubscriber = new SubscribeOnSubscriber(cVar, createWorker, this.f7444b, this.f7435d);
        cVar.a(subscribeOnSubscriber);
        createWorker.schedule(subscribeOnSubscriber);
    }

    static final class SubscribeOnSubscriber<T> extends AtomicReference<Thread> implements g<T>, Runnable, org.a.d {

        /* renamed from: a  reason: collision with root package name */
        final c<? super T> f7436a;

        /* renamed from: b  reason: collision with root package name */
        final h.c f7437b;

        /* renamed from: c  reason: collision with root package name */
        final AtomicReference<org.a.d> f7438c = new AtomicReference<>();

        /* renamed from: d  reason: collision with root package name */
        final AtomicLong f7439d = new AtomicLong();

        /* renamed from: e  reason: collision with root package name */
        final boolean f7440e;

        /* renamed from: f  reason: collision with root package name */
        b<T> f7441f;

        SubscribeOnSubscriber(c<? super T> cVar, h.c cVar2, b<T> bVar, boolean z) {
            this.f7436a = cVar;
            this.f7437b = cVar2;
            this.f7441f = bVar;
            this.f7440e = !z;
        }

        public void run() {
            lazySet(Thread.currentThread());
            b<T> bVar = this.f7441f;
            this.f7441f = null;
            bVar.a(this);
        }

        public void a(org.a.d dVar) {
            if (SubscriptionHelper.a(this.f7438c, dVar)) {
                long andSet = this.f7439d.getAndSet(0);
                if (andSet != 0) {
                    a(andSet, dVar);
                }
            }
        }

        public void c(T t) {
            this.f7436a.c(t);
        }

        public void a(Throwable th) {
            this.f7436a.a(th);
            this.f7437b.dispose();
        }

        public void d() {
            this.f7436a.d();
            this.f7437b.dispose();
        }

        public void a(long j) {
            if (SubscriptionHelper.b(j)) {
                org.a.d dVar = this.f7438c.get();
                if (dVar != null) {
                    a(j, dVar);
                    return;
                }
                io.reactivex.internal.util.b.a(this.f7439d, j);
                org.a.d dVar2 = this.f7438c.get();
                if (dVar2 != null) {
                    long andSet = this.f7439d.getAndSet(0);
                    if (andSet != 0) {
                        a(andSet, dVar2);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(long j, org.a.d dVar) {
            if (this.f7440e || Thread.currentThread() == get()) {
                dVar.a(j);
            } else {
                this.f7437b.schedule(new a(dVar, j));
            }
        }

        public void c() {
            SubscriptionHelper.a(this.f7438c);
            this.f7437b.dispose();
        }

        static final class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            private final org.a.d f7442a;

            /* renamed from: b  reason: collision with root package name */
            private final long f7443b;

            a(org.a.d dVar, long j) {
                this.f7442a = dVar;
                this.f7443b = j;
            }

            public void run() {
                this.f7442a.a(this.f7443b);
            }
        }
    }
}
