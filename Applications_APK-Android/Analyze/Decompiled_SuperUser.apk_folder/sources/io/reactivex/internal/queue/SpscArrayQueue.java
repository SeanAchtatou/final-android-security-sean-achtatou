package io.reactivex.internal.queue;

import io.reactivex.internal.b.e;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class SpscArrayQueue<E> extends AtomicReferenceArray<E> implements e<E> {

    /* renamed from: f  reason: collision with root package name */
    private static final Integer f7448f = Integer.getInteger("jctools.spsc.max.lookahead.step", (int) CodedOutputStream.DEFAULT_BUFFER_SIZE);

    /* renamed from: a  reason: collision with root package name */
    final int f7449a = (length() - 1);

    /* renamed from: b  reason: collision with root package name */
    final AtomicLong f7450b = new AtomicLong();

    /* renamed from: c  reason: collision with root package name */
    long f7451c;

    /* renamed from: d  reason: collision with root package name */
    final AtomicLong f7452d = new AtomicLong();

    /* renamed from: e  reason: collision with root package name */
    final int f7453e;

    public SpscArrayQueue(int i) {
        super(io.reactivex.internal.util.e.a(i));
        this.f7453e = Math.min(i / 4, f7448f.intValue());
    }

    public boolean a(E e2) {
        if (e2 == null) {
            throw new NullPointerException("Null is not a valid element");
        }
        int i = this.f7449a;
        long j = this.f7450b.get();
        int a2 = a(j, i);
        if (j >= this.f7451c) {
            int i2 = this.f7453e;
            if (a(a(((long) i2) + j, i)) == null) {
                this.f7451c = ((long) i2) + j;
            } else if (a(a2) != null) {
                return false;
            }
        }
        a(a2, e2);
        a(1 + j);
        return true;
    }

    public E a() {
        long j = this.f7452d.get();
        int c2 = c(j);
        E a2 = a(c2);
        if (a2 == null) {
            return null;
        }
        b(j + 1);
        a(c2, (Object) null);
        return a2;
    }

    public boolean b() {
        return this.f7450b.get() == this.f7452d.get();
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        this.f7450b.lazySet(j);
    }

    /* access modifiers changed from: package-private */
    public void b(long j) {
        this.f7452d.lazySet(j);
    }

    public void g_() {
        while (true) {
            if (a() == null && b()) {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a(long j, int i) {
        return ((int) j) & i;
    }

    /* access modifiers changed from: package-private */
    public int c(long j) {
        return ((int) j) & this.f7449a;
    }

    /* access modifiers changed from: package-private */
    public void a(int i, E e2) {
        lazySet(i, e2);
    }

    /* access modifiers changed from: package-private */
    public E a(int i) {
        return get(i);
    }
}
