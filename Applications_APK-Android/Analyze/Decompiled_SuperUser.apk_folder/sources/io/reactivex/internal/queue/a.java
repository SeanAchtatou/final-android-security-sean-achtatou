package io.reactivex.internal.queue;

import io.reactivex.internal.b.e;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

/* compiled from: SpscLinkedArrayQueue */
public final class a<T> implements e<T> {

    /* renamed from: a  reason: collision with root package name */
    static final int f7454a = Integer.getInteger("jctools.spsc.max.lookahead.step", (int) CodedOutputStream.DEFAULT_BUFFER_SIZE).intValue();
    private static final Object j = new Object();

    /* renamed from: b  reason: collision with root package name */
    final AtomicLong f7455b = new AtomicLong();

    /* renamed from: c  reason: collision with root package name */
    int f7456c;

    /* renamed from: d  reason: collision with root package name */
    long f7457d;

    /* renamed from: e  reason: collision with root package name */
    final int f7458e;

    /* renamed from: f  reason: collision with root package name */
    AtomicReferenceArray<Object> f7459f;

    /* renamed from: g  reason: collision with root package name */
    final int f7460g;

    /* renamed from: h  reason: collision with root package name */
    AtomicReferenceArray<Object> f7461h;
    final AtomicLong i = new AtomicLong();

    public a(int i2) {
        int a2 = io.reactivex.internal.util.e.a(Math.max(8, i2));
        int i3 = a2 - 1;
        AtomicReferenceArray<Object> atomicReferenceArray = new AtomicReferenceArray<>(a2 + 1);
        this.f7459f = atomicReferenceArray;
        this.f7458e = i3;
        a(a2);
        this.f7461h = atomicReferenceArray;
        this.f7460g = i3;
        this.f7457d = (long) (i3 - 1);
        a(0L);
    }

    public boolean a(Object obj) {
        if (obj == null) {
            throw new NullPointerException("Null is not a valid element");
        }
        AtomicReferenceArray<Object> atomicReferenceArray = this.f7459f;
        long f2 = f();
        int i2 = this.f7458e;
        int a2 = a(f2, i2);
        if (f2 < this.f7457d) {
            return a(atomicReferenceArray, obj, f2, a2);
        }
        int i3 = this.f7456c;
        if (b(atomicReferenceArray, a(((long) i3) + f2, i2)) == null) {
            this.f7457d = (((long) i3) + f2) - 1;
            return a(atomicReferenceArray, obj, f2, a2);
        } else if (b(atomicReferenceArray, a(1 + f2, i2)) == null) {
            return a(atomicReferenceArray, obj, f2, a2);
        } else {
            a(atomicReferenceArray, f2, a2, obj, (long) i2);
            return true;
        }
    }

    private boolean a(AtomicReferenceArray<Object> atomicReferenceArray, T t, long j2, int i2) {
        a(atomicReferenceArray, i2, t);
        a(1 + j2);
        return true;
    }

    private void a(AtomicReferenceArray<Object> atomicReferenceArray, long j2, int i2, T t, long j3) {
        AtomicReferenceArray<Object> atomicReferenceArray2 = new AtomicReferenceArray<>(atomicReferenceArray.length());
        this.f7459f = atomicReferenceArray2;
        this.f7457d = (j2 + j3) - 1;
        a(atomicReferenceArray2, i2, t);
        a(atomicReferenceArray, atomicReferenceArray2);
        a(atomicReferenceArray, i2, j);
        a(j2 + 1);
    }

    private void a(AtomicReferenceArray<Object> atomicReferenceArray, AtomicReferenceArray<Object> atomicReferenceArray2) {
        a(atomicReferenceArray, b(atomicReferenceArray.length() - 1), atomicReferenceArray2);
    }

    private AtomicReferenceArray<Object> a(AtomicReferenceArray<Object> atomicReferenceArray, int i2) {
        int b2 = b(i2);
        AtomicReferenceArray<Object> atomicReferenceArray2 = (AtomicReferenceArray) b(atomicReferenceArray, b2);
        a(atomicReferenceArray, b2, (Object) null);
        return atomicReferenceArray2;
    }

    public T a() {
        AtomicReferenceArray<Object> atomicReferenceArray = this.f7461h;
        long g2 = g();
        int i2 = this.f7460g;
        int a2 = a(g2, i2);
        T b2 = b(atomicReferenceArray, a2);
        boolean z = b2 == j;
        if (b2 != null && !z) {
            a(atomicReferenceArray, a2, (Object) null);
            b(1 + g2);
            return b2;
        } else if (z) {
            return a(a(atomicReferenceArray, i2 + 1), g2, i2);
        } else {
            return null;
        }
    }

    private T a(AtomicReferenceArray<Object> atomicReferenceArray, long j2, int i2) {
        this.f7461h = atomicReferenceArray;
        int a2 = a(j2, i2);
        T b2 = b(atomicReferenceArray, a2);
        if (b2 != null) {
            a(atomicReferenceArray, a2, (Object) null);
            b(1 + j2);
        }
        return b2;
    }

    public void g_() {
        while (true) {
            if (a() == null && b()) {
                return;
            }
        }
    }

    public boolean b() {
        return d() == e();
    }

    private void a(int i2) {
        this.f7456c = Math.min(i2 / 4, f7454a);
    }

    private long d() {
        return this.f7455b.get();
    }

    private long e() {
        return this.i.get();
    }

    private long f() {
        return this.f7455b.get();
    }

    private long g() {
        return this.i.get();
    }

    private void a(long j2) {
        this.f7455b.lazySet(j2);
    }

    private void b(long j2) {
        this.i.lazySet(j2);
    }

    private static int a(long j2, int i2) {
        return b(((int) j2) & i2);
    }

    private static int b(int i2) {
        return i2;
    }

    private static void a(AtomicReferenceArray<Object> atomicReferenceArray, int i2, Object obj) {
        atomicReferenceArray.lazySet(i2, obj);
    }

    private static <E> Object b(AtomicReferenceArray<Object> atomicReferenceArray, int i2) {
        return atomicReferenceArray.get(i2);
    }
}
