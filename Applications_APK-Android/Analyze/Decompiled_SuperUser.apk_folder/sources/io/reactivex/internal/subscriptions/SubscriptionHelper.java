package io.reactivex.internal.subscriptions;

import io.reactivex.b.a;
import io.reactivex.exceptions.ProtocolViolationException;
import io.reactivex.internal.a.b;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.a.d;

public enum SubscriptionHelper implements d {
    CANCELLED;

    public void a(long j) {
    }

    public void c() {
    }

    public static boolean a(d dVar, d dVar2) {
        if (dVar2 == null) {
            a.a(new NullPointerException("next is null"));
            return false;
        } else if (dVar == null) {
            return true;
        } else {
            dVar2.c();
            a();
            return false;
        }
    }

    public static void a() {
        a.a(new ProtocolViolationException("Subscription already set!"));
    }

    public static boolean b(long j) {
        if (j > 0) {
            return true;
        }
        a.a(new IllegalArgumentException("n > 0 required but it was " + j));
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [org.a.d, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public static boolean a(AtomicReference<d> atomicReference, d dVar) {
        b.a((Object) dVar, "d is null");
        if (atomicReference.compareAndSet(null, dVar)) {
            return true;
        }
        dVar.c();
        if (atomicReference.get() != CANCELLED) {
            a();
        }
        return false;
    }

    public static boolean a(AtomicReference<d> atomicReference) {
        d andSet;
        if (atomicReference.get() == CANCELLED || (andSet = atomicReference.getAndSet(CANCELLED)) == CANCELLED) {
            return false;
        }
        if (andSet != null) {
            andSet.c();
        }
        return true;
    }

    public static boolean a(AtomicReference<d> atomicReference, AtomicLong atomicLong, d dVar) {
        if (!a(atomicReference, dVar)) {
            return false;
        }
        long andSet = atomicLong.getAndSet(0);
        if (andSet != 0) {
            dVar.a(andSet);
        }
        return true;
    }

    public static void a(AtomicReference<d> atomicReference, AtomicLong atomicLong, long j) {
        d dVar = atomicReference.get();
        if (dVar != null) {
            dVar.a(j);
        } else if (b(j)) {
            io.reactivex.internal.util.b.a(atomicLong, j);
            d dVar2 = atomicReference.get();
            if (dVar2 != null) {
                long andSet = atomicLong.getAndSet(0);
                if (andSet != 0) {
                    dVar2.a(andSet);
                }
            }
        }
    }
}
