package io.reactivex.internal.subscriptions;

import io.reactivex.internal.b.d;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class BasicIntQueueSubscription<T> extends AtomicInteger implements d<T> {
    public final boolean a(T t) {
        throw new UnsupportedOperationException("Should not be called!");
    }
}
