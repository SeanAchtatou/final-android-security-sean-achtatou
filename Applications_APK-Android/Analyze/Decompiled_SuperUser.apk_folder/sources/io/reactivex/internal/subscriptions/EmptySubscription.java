package io.reactivex.internal.subscriptions;

import io.reactivex.internal.b.d;
import org.a.c;

public enum EmptySubscription implements d<Object> {
    INSTANCE;

    public void a(long j) {
        SubscriptionHelper.b(j);
    }

    public void c() {
    }

    public String toString() {
        return "EmptySubscription";
    }

    public static void a(Throwable th, c<?> cVar) {
        cVar.a(INSTANCE);
        cVar.a(th);
    }

    public Object a() {
        return null;
    }

    public boolean b() {
        return true;
    }

    public void g_() {
    }

    public int a(int i) {
        return i & 2;
    }

    public boolean a(Object obj) {
        throw new UnsupportedOperationException("Should not be called!");
    }
}
