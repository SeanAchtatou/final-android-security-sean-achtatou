package io.reactivex.internal.subscribers;

import io.reactivex.internal.b.d;
import io.reactivex.internal.subscriptions.SubscriptionHelper;

/* compiled from: BasicFuseableConditionalSubscriber */
public abstract class a<T, R> implements io.reactivex.internal.b.a<T>, d<R> {

    /* renamed from: b  reason: collision with root package name */
    protected final io.reactivex.internal.b.a<? super R> f7566b;

    /* renamed from: c  reason: collision with root package name */
    protected org.a.d f7567c;

    /* renamed from: d  reason: collision with root package name */
    protected d<T> f7568d;

    /* renamed from: e  reason: collision with root package name */
    protected boolean f7569e;

    /* renamed from: f  reason: collision with root package name */
    protected int f7570f;

    public a(io.reactivex.internal.b.a<? super R> aVar) {
        this.f7566b = aVar;
    }

    public final void a(org.a.d dVar) {
        if (SubscriptionHelper.a(this.f7567c, dVar)) {
            this.f7567c = dVar;
            if (dVar instanceof d) {
                this.f7568d = (d) dVar;
            }
            if (e()) {
                this.f7566b.a(this);
                f();
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    public void a(Throwable th) {
        if (this.f7569e) {
            io.reactivex.b.a.a(th);
            return;
        }
        this.f7569e = true;
        this.f7566b.a(th);
    }

    /* access modifiers changed from: protected */
    public final void b(Throwable th) {
        io.reactivex.exceptions.a.b(th);
        this.f7567c.c();
        a(th);
    }

    public void d() {
        if (!this.f7569e) {
            this.f7569e = true;
            this.f7566b.d();
        }
    }

    /* access modifiers changed from: protected */
    public final int b(int i) {
        d<T> dVar = this.f7568d;
        if (dVar == null || (i & 4) != 0) {
            return 0;
        }
        int a2 = dVar.a(i);
        if (a2 == 0) {
            return a2;
        }
        this.f7570f = a2;
        return a2;
    }

    public void a(long j) {
        this.f7567c.a(j);
    }

    public void c() {
        this.f7567c.c();
    }

    public boolean b() {
        return this.f7568d.b();
    }

    public void g_() {
        this.f7568d.g_();
    }

    public final boolean a(R r) {
        throw new UnsupportedOperationException("Should not be called!");
    }
}
