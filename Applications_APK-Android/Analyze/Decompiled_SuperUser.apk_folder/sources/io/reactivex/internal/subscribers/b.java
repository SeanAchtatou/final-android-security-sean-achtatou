package io.reactivex.internal.subscribers;

import io.reactivex.b.a;
import io.reactivex.g;
import io.reactivex.internal.b.d;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import org.a.c;

/* compiled from: BasicFuseableSubscriber */
public abstract class b<T, R> implements g<T>, d<R> {

    /* renamed from: b  reason: collision with root package name */
    protected final c<? super R> f7571b;

    /* renamed from: c  reason: collision with root package name */
    protected org.a.d f7572c;

    /* renamed from: d  reason: collision with root package name */
    protected d<T> f7573d;

    /* renamed from: e  reason: collision with root package name */
    protected boolean f7574e;

    /* renamed from: f  reason: collision with root package name */
    protected int f7575f;

    public b(c<? super R> cVar) {
        this.f7571b = cVar;
    }

    public final void a(org.a.d dVar) {
        if (SubscriptionHelper.a(this.f7572c, dVar)) {
            this.f7572c = dVar;
            if (dVar instanceof d) {
                this.f7573d = (d) dVar;
            }
            if (e()) {
                this.f7571b.a(this);
                f();
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    public void a(Throwable th) {
        if (this.f7574e) {
            a.a(th);
            return;
        }
        this.f7574e = true;
        this.f7571b.a(th);
    }

    /* access modifiers changed from: protected */
    public final void b(Throwable th) {
        io.reactivex.exceptions.a.b(th);
        this.f7572c.c();
        a(th);
    }

    public void d() {
        if (!this.f7574e) {
            this.f7574e = true;
            this.f7571b.d();
        }
    }

    /* access modifiers changed from: protected */
    public final int b(int i) {
        d<T> dVar = this.f7573d;
        if (dVar == null || (i & 4) != 0) {
            return 0;
        }
        int a2 = dVar.a(i);
        if (a2 == 0) {
            return a2;
        }
        this.f7575f = a2;
        return a2;
    }

    public void a(long j) {
        this.f7572c.a(j);
    }

    public void c() {
        this.f7572c.c();
    }

    public boolean b() {
        return this.f7573d.b();
    }

    public void g_() {
        this.f7573d.g_();
    }

    public final boolean a(R r) {
        throw new UnsupportedOperationException("Should not be called!");
    }
}
