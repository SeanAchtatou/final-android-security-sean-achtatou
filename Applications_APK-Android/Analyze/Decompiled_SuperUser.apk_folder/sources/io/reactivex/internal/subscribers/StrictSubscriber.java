package io.reactivex.internal.subscribers;

import io.reactivex.g;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.a.c;
import org.a.d;

public class StrictSubscriber<T> extends AtomicInteger implements g<T>, d {

    /* renamed from: a  reason: collision with root package name */
    final c<? super T> f7560a;

    /* renamed from: b  reason: collision with root package name */
    final AtomicThrowable f7561b = new AtomicThrowable();

    /* renamed from: c  reason: collision with root package name */
    final AtomicLong f7562c = new AtomicLong();

    /* renamed from: d  reason: collision with root package name */
    final AtomicReference<d> f7563d = new AtomicReference<>();

    /* renamed from: e  reason: collision with root package name */
    final AtomicBoolean f7564e = new AtomicBoolean();

    /* renamed from: f  reason: collision with root package name */
    volatile boolean f7565f;

    public StrictSubscriber(c<? super T> cVar) {
        this.f7560a = cVar;
    }

    public void a(long j) {
        if (j <= 0) {
            c();
            a(new IllegalArgumentException("§3.9 violated: positive request amount required but it was " + j));
            return;
        }
        SubscriptionHelper.a(this.f7563d, this.f7562c, j);
    }

    public void c() {
        if (!this.f7565f) {
            SubscriptionHelper.a(this.f7563d);
        }
    }

    public void a(d dVar) {
        if (this.f7564e.compareAndSet(false, true)) {
            this.f7560a.a(this);
            SubscriptionHelper.a(this.f7563d, this.f7562c, dVar);
            return;
        }
        dVar.c();
        c();
        a(new IllegalStateException("§2.12 violated: onSubscribe must be called at most once"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.util.c.a(org.a.c, java.lang.Object, java.util.concurrent.atomic.AtomicInteger, io.reactivex.internal.util.AtomicThrowable):void
     arg types: [org.a.c<? super T>, T, io.reactivex.internal.subscribers.StrictSubscriber, io.reactivex.internal.util.AtomicThrowable]
     candidates:
      io.reactivex.internal.util.c.a(org.a.c<?>, java.lang.Throwable, java.util.concurrent.atomic.AtomicInteger, io.reactivex.internal.util.AtomicThrowable):void
      io.reactivex.internal.util.c.a(org.a.c, java.lang.Object, java.util.concurrent.atomic.AtomicInteger, io.reactivex.internal.util.AtomicThrowable):void */
    public void c(T t) {
        io.reactivex.internal.util.c.a((c) this.f7560a, (Object) t, (AtomicInteger) this, this.f7561b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.util.c.a(org.a.c<?>, java.lang.Throwable, java.util.concurrent.atomic.AtomicInteger, io.reactivex.internal.util.AtomicThrowable):void
     arg types: [org.a.c<? super T>, java.lang.Throwable, io.reactivex.internal.subscribers.StrictSubscriber, io.reactivex.internal.util.AtomicThrowable]
     candidates:
      io.reactivex.internal.util.c.a(org.a.c, java.lang.Object, java.util.concurrent.atomic.AtomicInteger, io.reactivex.internal.util.AtomicThrowable):void
      io.reactivex.internal.util.c.a(org.a.c<?>, java.lang.Throwable, java.util.concurrent.atomic.AtomicInteger, io.reactivex.internal.util.AtomicThrowable):void */
    public void a(Throwable th) {
        this.f7565f = true;
        io.reactivex.internal.util.c.a((c<?>) this.f7560a, th, (AtomicInteger) this, this.f7561b);
    }

    public void d() {
        this.f7565f = true;
        io.reactivex.internal.util.c.a(this.f7560a, this, this.f7561b);
    }
}
