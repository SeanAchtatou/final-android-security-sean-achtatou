package io.reactivex.internal.subscribers;

import io.reactivex.a.a;
import io.reactivex.disposables.b;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.g;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.concurrent.atomic.AtomicReference;
import org.a.d;

public final class LambdaSubscriber<T> extends AtomicReference<d> implements b, g<T>, d {

    /* renamed from: a  reason: collision with root package name */
    final io.reactivex.a.d<? super T> f7556a;

    /* renamed from: b  reason: collision with root package name */
    final io.reactivex.a.d<? super Throwable> f7557b;

    /* renamed from: c  reason: collision with root package name */
    final a f7558c;

    /* renamed from: d  reason: collision with root package name */
    final io.reactivex.a.d<? super d> f7559d;

    public LambdaSubscriber(io.reactivex.a.d<? super T> dVar, io.reactivex.a.d<? super Throwable> dVar2, a aVar, io.reactivex.a.d<? super d> dVar3) {
        this.f7556a = dVar;
        this.f7557b = dVar2;
        this.f7558c = aVar;
        this.f7559d = dVar3;
    }

    public void a(d dVar) {
        if (SubscriptionHelper.a((AtomicReference<d>) this, dVar)) {
            try {
                this.f7559d.accept(this);
            } catch (Throwable th) {
                io.reactivex.exceptions.a.b(th);
                dVar.c();
                a(th);
            }
        }
    }

    public void c(T t) {
        if (!a()) {
            try {
                this.f7556a.accept(t);
            } catch (Throwable th) {
                Throwable th2 = th;
                io.reactivex.exceptions.a.b(th2);
                ((d) get()).c();
                a(th2);
            }
        }
    }

    public void a(Throwable th) {
        if (get() != SubscriptionHelper.CANCELLED) {
            lazySet(SubscriptionHelper.CANCELLED);
            try {
                this.f7557b.accept(th);
            } catch (Throwable th2) {
                io.reactivex.exceptions.a.b(th2);
                io.reactivex.b.a.a(new CompositeException(th, th2));
            }
        } else {
            io.reactivex.b.a.a(th);
        }
    }

    public void d() {
        if (get() != SubscriptionHelper.CANCELLED) {
            lazySet(SubscriptionHelper.CANCELLED);
            try {
                this.f7558c.a();
            } catch (Throwable th) {
                io.reactivex.exceptions.a.b(th);
                io.reactivex.b.a.a(th);
            }
        }
    }

    public void dispose() {
        c();
    }

    public boolean a() {
        return get() == SubscriptionHelper.CANCELLED;
    }

    public void a(long j) {
        ((d) get()).a(j);
    }

    public void c() {
        SubscriptionHelper.a(this);
    }
}
