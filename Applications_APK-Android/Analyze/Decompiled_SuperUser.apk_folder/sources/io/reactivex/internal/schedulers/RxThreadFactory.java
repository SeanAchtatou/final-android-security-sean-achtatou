package io.reactivex.internal.schedulers;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

public final class RxThreadFactory extends AtomicLong implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    final String f7466a;

    /* renamed from: b  reason: collision with root package name */
    final int f7467b;

    /* renamed from: c  reason: collision with root package name */
    final boolean f7468c;

    public RxThreadFactory(String str) {
        this(str, 5, false);
    }

    public RxThreadFactory(String str, int i) {
        this(str, i, false);
    }

    public RxThreadFactory(String str, int i, boolean z) {
        this.f7466a = str;
        this.f7467b = i;
        this.f7468c = z;
    }

    public Thread newThread(Runnable runnable) {
        String str = this.f7466a + '-' + incrementAndGet();
        Thread aVar = this.f7468c ? new a(runnable, str) : new Thread(runnable, str);
        aVar.setPriority(this.f7467b);
        aVar.setDaemon(true);
        return aVar;
    }

    public String toString() {
        return "RxThreadFactory[" + this.f7466a + "]";
    }

    static final class a extends Thread {
        a(Runnable runnable, String str) {
            super(runnable, str);
        }
    }
}
