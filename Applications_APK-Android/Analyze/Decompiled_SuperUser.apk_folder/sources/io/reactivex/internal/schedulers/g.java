package io.reactivex.internal.schedulers;

import io.reactivex.disposables.b;
import io.reactivex.h;
import io.reactivex.internal.disposables.EmptyDisposable;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: SingleScheduler */
public final class g extends h {

    /* renamed from: c  reason: collision with root package name */
    static final RxThreadFactory f7535c = new RxThreadFactory("RxSingleScheduler", Math.max(1, Math.min(10, Integer.getInteger("rx2.single-priority", 5).intValue())), true);

    /* renamed from: d  reason: collision with root package name */
    static final ScheduledExecutorService f7536d = Executors.newScheduledThreadPool(0);

    /* renamed from: a  reason: collision with root package name */
    final ThreadFactory f7537a;

    /* renamed from: b  reason: collision with root package name */
    final AtomicReference<ScheduledExecutorService> f7538b;

    static {
        f7536d.shutdown();
    }

    public g() {
        this(f7535c);
    }

    public g(ThreadFactory threadFactory) {
        this.f7538b = new AtomicReference<>();
        this.f7537a = threadFactory;
        this.f7538b.lazySet(a(threadFactory));
    }

    static ScheduledExecutorService a(ThreadFactory threadFactory) {
        return f.a(threadFactory);
    }

    public void start() {
        ScheduledExecutorService scheduledExecutorService;
        ScheduledExecutorService scheduledExecutorService2 = null;
        do {
            scheduledExecutorService = this.f7538b.get();
            if (scheduledExecutorService != f7536d) {
                if (scheduledExecutorService2 != null) {
                    scheduledExecutorService2.shutdown();
                    return;
                }
                return;
            } else if (scheduledExecutorService2 == null) {
                scheduledExecutorService2 = a(this.f7537a);
            }
        } while (!this.f7538b.compareAndSet(scheduledExecutorService, scheduledExecutorService2));
    }

    public void shutdown() {
        ScheduledExecutorService andSet;
        if (this.f7538b.get() != f7536d && (andSet = this.f7538b.getAndSet(f7536d)) != f7536d) {
            andSet.shutdownNow();
        }
    }

    public h.c createWorker() {
        return new a(this.f7538b.get());
    }

    public b scheduleDirect(Runnable runnable, long j, TimeUnit timeUnit) {
        Future schedule;
        ScheduledDirectTask scheduledDirectTask = new ScheduledDirectTask(io.reactivex.b.a.a(runnable));
        if (j <= 0) {
            try {
                schedule = this.f7538b.get().submit(scheduledDirectTask);
            } catch (RejectedExecutionException e2) {
                io.reactivex.b.a.a(e2);
                return EmptyDisposable.INSTANCE;
            }
        } else {
            schedule = this.f7538b.get().schedule(scheduledDirectTask, j, timeUnit);
        }
        scheduledDirectTask.a(schedule);
        return scheduledDirectTask;
    }

    public b schedulePeriodicallyDirect(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        Future schedule;
        Runnable a2 = io.reactivex.b.a.a(runnable);
        if (j2 <= 0) {
            ScheduledExecutorService scheduledExecutorService = this.f7538b.get();
            b bVar = new b(a2, scheduledExecutorService);
            if (j <= 0) {
                try {
                    schedule = scheduledExecutorService.submit(bVar);
                } catch (RejectedExecutionException e2) {
                    io.reactivex.b.a.a(e2);
                    return EmptyDisposable.INSTANCE;
                }
            } else {
                schedule = scheduledExecutorService.schedule(bVar, j, timeUnit);
            }
            bVar.a(schedule);
            return bVar;
        }
        ScheduledDirectPeriodicTask scheduledDirectPeriodicTask = new ScheduledDirectPeriodicTask(a2);
        try {
            scheduledDirectPeriodicTask.a(this.f7538b.get().scheduleAtFixedRate(scheduledDirectPeriodicTask, j, j2, timeUnit));
            return scheduledDirectPeriodicTask;
        } catch (RejectedExecutionException e3) {
            io.reactivex.b.a.a(e3);
            return EmptyDisposable.INSTANCE;
        }
    }

    /* compiled from: SingleScheduler */
    static final class a extends h.c {

        /* renamed from: a  reason: collision with root package name */
        final ScheduledExecutorService f7539a;

        /* renamed from: b  reason: collision with root package name */
        final io.reactivex.disposables.a f7540b = new io.reactivex.disposables.a();

        /* renamed from: c  reason: collision with root package name */
        volatile boolean f7541c;

        a(ScheduledExecutorService scheduledExecutorService) {
            this.f7539a = scheduledExecutorService;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{<V> java.util.concurrent.ScheduledExecutorService.schedule(java.util.concurrent.Callable, long, java.util.concurrent.TimeUnit):java.util.concurrent.ScheduledFuture<V>}
         arg types: [io.reactivex.internal.schedulers.ScheduledRunnable, long, java.util.concurrent.TimeUnit]
         candidates:
          ClspMth{java.util.concurrent.ScheduledExecutorService.schedule(java.lang.Runnable, long, java.util.concurrent.TimeUnit):java.util.concurrent.ScheduledFuture<?>}
          ClspMth{<V> java.util.concurrent.ScheduledExecutorService.schedule(java.util.concurrent.Callable, long, java.util.concurrent.TimeUnit):java.util.concurrent.ScheduledFuture<V>} */
        public b schedule(Runnable runnable, long j, TimeUnit timeUnit) {
            Future schedule;
            if (this.f7541c) {
                return EmptyDisposable.INSTANCE;
            }
            ScheduledRunnable scheduledRunnable = new ScheduledRunnable(io.reactivex.b.a.a(runnable), this.f7540b);
            this.f7540b.a(scheduledRunnable);
            if (j <= 0) {
                try {
                    schedule = this.f7539a.submit((Callable) scheduledRunnable);
                } catch (RejectedExecutionException e2) {
                    dispose();
                    io.reactivex.b.a.a(e2);
                    return EmptyDisposable.INSTANCE;
                }
            } else {
                schedule = this.f7539a.schedule((Callable) scheduledRunnable, j, timeUnit);
            }
            scheduledRunnable.a(schedule);
            return scheduledRunnable;
        }

        public void dispose() {
            if (!this.f7541c) {
                this.f7541c = true;
                this.f7540b.dispose();
            }
        }
    }
}
