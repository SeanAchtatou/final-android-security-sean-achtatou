package io.reactivex.internal.schedulers;

public final class ScheduledDirectPeriodicTask extends AbstractDirectTask implements Runnable {
    public ScheduledDirectPeriodicTask(Runnable runnable) {
        super(runnable);
    }

    public void run() {
        this.f7465b = Thread.currentThread();
        try {
            this.f7464a.run();
        } catch (Throwable th) {
            this.f7465b = null;
            throw th;
        }
        this.f7465b = null;
    }
}
