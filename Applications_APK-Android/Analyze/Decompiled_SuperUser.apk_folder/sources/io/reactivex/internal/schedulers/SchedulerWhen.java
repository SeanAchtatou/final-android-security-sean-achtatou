package io.reactivex.internal.schedulers;

import io.reactivex.a.e;
import io.reactivex.h;
import io.reactivex.processors.UnicastProcessor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class SchedulerWhen extends h implements io.reactivex.disposables.b {

    /* renamed from: a  reason: collision with root package name */
    static final io.reactivex.disposables.b f7472a = new d();

    /* renamed from: b  reason: collision with root package name */
    static final io.reactivex.disposables.b f7473b = io.reactivex.disposables.c.a();

    /* renamed from: c  reason: collision with root package name */
    private final h f7474c;

    /* renamed from: d  reason: collision with root package name */
    private final io.reactivex.processors.a<io.reactivex.d<io.reactivex.a>> f7475d = UnicastProcessor.c().b();

    /* renamed from: e  reason: collision with root package name */
    private io.reactivex.disposables.b f7476e;

    public SchedulerWhen(e<io.reactivex.d<io.reactivex.d<io.reactivex.a>>, io.reactivex.a> eVar, h hVar) {
        this.f7474c = hVar;
        try {
            this.f7476e = eVar.a(this.f7475d).a();
        } catch (Throwable th) {
            io.reactivex.exceptions.a.a(th);
        }
    }

    public void dispose() {
        this.f7476e.dispose();
    }

    public h.c createWorker() {
        h.c createWorker = this.f7474c.createWorker();
        io.reactivex.processors.a b2 = UnicastProcessor.c().b();
        io.reactivex.d a2 = b2.a(new a(createWorker));
        c cVar = new c(b2, createWorker);
        this.f7475d.c(a2);
        return cVar;
    }

    static abstract class ScheduledAction extends AtomicReference<io.reactivex.disposables.b> implements io.reactivex.disposables.b {
        /* access modifiers changed from: protected */
        public abstract io.reactivex.disposables.b a(h.c cVar, io.reactivex.b bVar);

        ScheduledAction() {
            super(SchedulerWhen.f7472a);
        }

        /* access modifiers changed from: package-private */
        public void b(h.c cVar, io.reactivex.b bVar) {
            io.reactivex.disposables.b bVar2 = (io.reactivex.disposables.b) get();
            if (bVar2 != SchedulerWhen.f7473b && bVar2 == SchedulerWhen.f7472a) {
                io.reactivex.disposables.b a2 = a(cVar, bVar);
                if (!compareAndSet(SchedulerWhen.f7472a, a2)) {
                    a2.dispose();
                }
            }
        }

        public void dispose() {
            io.reactivex.disposables.b bVar;
            io.reactivex.disposables.b bVar2 = SchedulerWhen.f7473b;
            do {
                bVar = (io.reactivex.disposables.b) get();
                if (bVar == SchedulerWhen.f7473b) {
                    return;
                }
            } while (!compareAndSet(bVar, bVar2));
            if (bVar != SchedulerWhen.f7472a) {
                bVar.dispose();
            }
        }
    }

    static class ImmediateAction extends ScheduledAction {

        /* renamed from: a  reason: collision with root package name */
        private final Runnable f7480a;

        ImmediateAction(Runnable runnable) {
            this.f7480a = runnable;
        }

        /* access modifiers changed from: protected */
        public io.reactivex.disposables.b a(h.c cVar, io.reactivex.b bVar) {
            return cVar.schedule(new b(this.f7480a, bVar));
        }
    }

    static class DelayedAction extends ScheduledAction {

        /* renamed from: a  reason: collision with root package name */
        private final Runnable f7477a;

        /* renamed from: b  reason: collision with root package name */
        private final long f7478b;

        /* renamed from: c  reason: collision with root package name */
        private final TimeUnit f7479c;

        DelayedAction(Runnable runnable, long j, TimeUnit timeUnit) {
            this.f7477a = runnable;
            this.f7478b = j;
            this.f7479c = timeUnit;
        }

        /* access modifiers changed from: protected */
        public io.reactivex.disposables.b a(h.c cVar, io.reactivex.b bVar) {
            return cVar.schedule(new b(this.f7477a, bVar), this.f7478b, this.f7479c);
        }
    }

    static class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final io.reactivex.b f7484a;

        /* renamed from: b  reason: collision with root package name */
        final Runnable f7485b;

        b(Runnable runnable, io.reactivex.b bVar) {
            this.f7485b = runnable;
            this.f7484a = bVar;
        }

        public void run() {
            try {
                this.f7485b.run();
            } finally {
                this.f7484a.a();
            }
        }
    }

    static final class a implements e<ScheduledAction, io.reactivex.a> {

        /* renamed from: a  reason: collision with root package name */
        final h.c f7481a;

        a(h.c cVar) {
            this.f7481a = cVar;
        }

        public io.reactivex.a a(ScheduledAction scheduledAction) {
            return new C0106a(scheduledAction);
        }

        /* renamed from: io.reactivex.internal.schedulers.SchedulerWhen$a$a  reason: collision with other inner class name */
        final class C0106a extends io.reactivex.a {

            /* renamed from: a  reason: collision with root package name */
            final ScheduledAction f7482a;

            C0106a(ScheduledAction scheduledAction) {
                this.f7482a = scheduledAction;
            }

            /* access modifiers changed from: protected */
            public void b(io.reactivex.b bVar) {
                bVar.a(this.f7482a);
                this.f7482a.b(a.this.f7481a, bVar);
            }
        }
    }

    static final class c extends h.c {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicBoolean f7486a = new AtomicBoolean();

        /* renamed from: b  reason: collision with root package name */
        private final io.reactivex.processors.a<ScheduledAction> f7487b;

        /* renamed from: c  reason: collision with root package name */
        private final h.c f7488c;

        c(io.reactivex.processors.a<ScheduledAction> aVar, h.c cVar) {
            this.f7487b = aVar;
            this.f7488c = cVar;
        }

        public void dispose() {
            if (this.f7486a.compareAndSet(false, true)) {
                this.f7487b.d();
                this.f7488c.dispose();
            }
        }

        public io.reactivex.disposables.b schedule(Runnable runnable, long j, TimeUnit timeUnit) {
            DelayedAction delayedAction = new DelayedAction(runnable, j, timeUnit);
            this.f7487b.c(delayedAction);
            return delayedAction;
        }

        public io.reactivex.disposables.b schedule(Runnable runnable) {
            ImmediateAction immediateAction = new ImmediateAction(runnable);
            this.f7487b.c(immediateAction);
            return immediateAction;
        }
    }

    static final class d implements io.reactivex.disposables.b {
        d() {
        }

        public void dispose() {
        }
    }
}
