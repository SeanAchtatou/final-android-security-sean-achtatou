package io.reactivex.internal.schedulers;

import io.reactivex.disposables.b;
import io.reactivex.internal.a.a;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReference;

abstract class AbstractDirectTask extends AtomicReference<Future<?>> implements b {

    /* renamed from: c  reason: collision with root package name */
    protected static final FutureTask<Void> f7462c = new FutureTask<>(a.f7393b, null);

    /* renamed from: d  reason: collision with root package name */
    protected static final FutureTask<Void> f7463d = new FutureTask<>(a.f7393b, null);

    /* renamed from: a  reason: collision with root package name */
    protected final Runnable f7464a;

    /* renamed from: b  reason: collision with root package name */
    protected Thread f7465b;

    AbstractDirectTask(Runnable runnable) {
        this.f7464a = runnable;
    }

    public final void dispose() {
        Future future = (Future) get();
        if (future != f7462c && future != f7463d && compareAndSet(future, f7463d) && future != null) {
            future.cancel(this.f7465b != Thread.currentThread());
        }
    }

    public final void a(Future<?> future) {
        Future future2;
        do {
            future2 = (Future) get();
            if (future2 != f7462c) {
                if (future2 == f7463d) {
                    future.cancel(this.f7465b != Thread.currentThread());
                    return;
                }
            } else {
                return;
            }
        } while (!compareAndSet(future2, future));
    }
}
