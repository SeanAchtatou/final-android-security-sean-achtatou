package io.reactivex.internal.schedulers;

import io.reactivex.disposables.b;
import io.reactivex.internal.disposables.a;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class ScheduledRunnable extends AtomicReferenceArray<Object> implements b, Runnable, Callable<Object> {

    /* renamed from: b  reason: collision with root package name */
    static final Object f7469b = new Object();

    /* renamed from: c  reason: collision with root package name */
    static final Object f7470c = new Object();

    /* renamed from: a  reason: collision with root package name */
    final Runnable f7471a;

    public ScheduledRunnable(Runnable runnable, a aVar) {
        super(3);
        this.f7471a = runnable;
        lazySet(0, aVar);
    }

    public Object call() {
        run();
        return null;
    }

    public void run() {
        Object obj;
        Object obj2;
        lazySet(2, Thread.currentThread());
        try {
            this.f7471a.run();
        } catch (Throwable th) {
            Throwable th2 = th;
            lazySet(2, null);
            Object obj3 = get(0);
            if (!(obj3 == f7469b || obj3 == null || !compareAndSet(0, obj3, f7470c))) {
                ((a) obj3).c(this);
            }
            do {
                obj = get(1);
                if (obj == f7469b) {
                    break;
                }
            } while (!compareAndSet(1, obj, f7470c));
            throw th2;
        }
        lazySet(2, null);
        Object obj4 = get(0);
        if (!(obj4 == f7469b || obj4 == null || !compareAndSet(0, obj4, f7470c))) {
            ((a) obj4).c(this);
        }
        do {
            obj2 = get(1);
            if (obj2 == f7469b) {
                return;
            }
        } while (!compareAndSet(1, obj2, f7470c));
    }

    public void a(Future<?> future) {
        Object obj;
        boolean z = true;
        do {
            obj = get(1);
            if (obj != f7470c) {
                if (obj == f7469b) {
                    if (get(2) == Thread.currentThread()) {
                        z = false;
                    }
                    future.cancel(z);
                    return;
                }
            } else {
                return;
            }
        } while (!compareAndSet(1, obj, future));
    }

    public void dispose() {
        Object obj;
        boolean z = true;
        while (true) {
            Object obj2 = get(1);
            if (obj2 == f7470c || obj2 == f7469b) {
                break;
            } else if (compareAndSet(1, obj2, f7469b)) {
                if (obj2 != null) {
                    Future future = (Future) obj2;
                    if (get(2) == Thread.currentThread()) {
                        z = false;
                    }
                    future.cancel(z);
                }
            }
        }
        do {
            obj = get(0);
            if (obj == f7470c || obj == f7469b || obj == null) {
                return;
            }
        } while (!compareAndSet(0, obj, f7469b));
        ((a) obj).c(this);
    }
}
