package io.reactivex.internal.schedulers;

import io.reactivex.h;
import io.reactivex.internal.disposables.EmptyDisposable;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: IoScheduler */
public final class c extends h {

    /* renamed from: a  reason: collision with root package name */
    static final RxThreadFactory f7509a;

    /* renamed from: b  reason: collision with root package name */
    static final RxThreadFactory f7510b;

    /* renamed from: c  reason: collision with root package name */
    static final C0108c f7511c = new C0108c(new RxThreadFactory("RxCachedThreadSchedulerShutdown"));

    /* renamed from: f  reason: collision with root package name */
    static final a f7512f = new a(0, null, f7509a);

    /* renamed from: g  reason: collision with root package name */
    private static final TimeUnit f7513g = TimeUnit.SECONDS;

    /* renamed from: d  reason: collision with root package name */
    final ThreadFactory f7514d;

    /* renamed from: e  reason: collision with root package name */
    final AtomicReference<a> f7515e;

    static {
        f7511c.dispose();
        int max = Math.max(1, Math.min(10, Integer.getInteger("rx2.io-priority", 5).intValue()));
        f7509a = new RxThreadFactory("RxCachedThreadScheduler", max);
        f7510b = new RxThreadFactory("RxCachedWorkerPoolEvictor", max);
        f7512f.d();
    }

    /* compiled from: IoScheduler */
    static final class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final io.reactivex.disposables.a f7516a;

        /* renamed from: b  reason: collision with root package name */
        private final long f7517b;

        /* renamed from: c  reason: collision with root package name */
        private final ConcurrentLinkedQueue<C0108c> f7518c;

        /* renamed from: d  reason: collision with root package name */
        private final ScheduledExecutorService f7519d;

        /* renamed from: e  reason: collision with root package name */
        private final Future<?> f7520e;

        /* renamed from: f  reason: collision with root package name */
        private final ThreadFactory f7521f;

        a(long j, TimeUnit timeUnit, ThreadFactory threadFactory) {
            ScheduledFuture<?> scheduledFuture;
            ScheduledExecutorService scheduledExecutorService = null;
            this.f7517b = timeUnit != null ? timeUnit.toNanos(j) : 0;
            this.f7518c = new ConcurrentLinkedQueue<>();
            this.f7516a = new io.reactivex.disposables.a();
            this.f7521f = threadFactory;
            if (timeUnit != null) {
                ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(1, c.f7510b);
                scheduledExecutorService = newScheduledThreadPool;
                scheduledFuture = newScheduledThreadPool.scheduleWithFixedDelay(this, this.f7517b, this.f7517b, TimeUnit.NANOSECONDS);
            } else {
                scheduledFuture = null;
            }
            this.f7519d = scheduledExecutorService;
            this.f7520e = scheduledFuture;
        }

        public void run() {
            b();
        }

        /* access modifiers changed from: package-private */
        public C0108c a() {
            if (this.f7516a.a()) {
                return c.f7511c;
            }
            while (!this.f7518c.isEmpty()) {
                C0108c poll = this.f7518c.poll();
                if (poll != null) {
                    return poll;
                }
            }
            C0108c cVar = new C0108c(this.f7521f);
            this.f7516a.a(cVar);
            return cVar;
        }

        /* access modifiers changed from: package-private */
        public void a(C0108c cVar) {
            cVar.a(c() + this.f7517b);
            this.f7518c.offer(cVar);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (!this.f7518c.isEmpty()) {
                long c2 = c();
                Iterator<C0108c> it = this.f7518c.iterator();
                while (it.hasNext()) {
                    C0108c next = it.next();
                    if (next.a() > c2) {
                        return;
                    }
                    if (this.f7518c.remove(next)) {
                        this.f7516a.b(next);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public long c() {
            return System.nanoTime();
        }

        /* access modifiers changed from: package-private */
        public void d() {
            this.f7516a.dispose();
            if (this.f7520e != null) {
                this.f7520e.cancel(true);
            }
            if (this.f7519d != null) {
                this.f7519d.shutdownNow();
            }
        }
    }

    public c() {
        this(f7509a);
    }

    public c(ThreadFactory threadFactory) {
        this.f7514d = threadFactory;
        this.f7515e = new AtomicReference<>(f7512f);
        start();
    }

    public void start() {
        a aVar = new a(60, f7513g, this.f7514d);
        if (!this.f7515e.compareAndSet(f7512f, aVar)) {
            aVar.d();
        }
    }

    public void shutdown() {
        a aVar;
        do {
            aVar = this.f7515e.get();
            if (aVar == f7512f) {
                return;
            }
        } while (!this.f7515e.compareAndSet(aVar, f7512f));
        aVar.d();
    }

    public h.c createWorker() {
        return new b(this.f7515e.get());
    }

    /* compiled from: IoScheduler */
    static final class b extends h.c {

        /* renamed from: a  reason: collision with root package name */
        final AtomicBoolean f7522a = new AtomicBoolean();

        /* renamed from: b  reason: collision with root package name */
        private final io.reactivex.disposables.a f7523b;

        /* renamed from: c  reason: collision with root package name */
        private final a f7524c;

        /* renamed from: d  reason: collision with root package name */
        private final C0108c f7525d;

        b(a aVar) {
            this.f7524c = aVar;
            this.f7523b = new io.reactivex.disposables.a();
            this.f7525d = aVar.a();
        }

        public void dispose() {
            if (this.f7522a.compareAndSet(false, true)) {
                this.f7523b.dispose();
                this.f7524c.a(this.f7525d);
            }
        }

        public io.reactivex.disposables.b schedule(Runnable runnable, long j, TimeUnit timeUnit) {
            if (this.f7523b.a()) {
                return EmptyDisposable.INSTANCE;
            }
            return this.f7525d.a(runnable, j, timeUnit, this.f7523b);
        }
    }

    /* renamed from: io.reactivex.internal.schedulers.c$c  reason: collision with other inner class name */
    /* compiled from: IoScheduler */
    static final class C0108c extends e {

        /* renamed from: b  reason: collision with root package name */
        private long f7526b = 0;

        C0108c(ThreadFactory threadFactory) {
            super(threadFactory);
        }

        public long a() {
            return this.f7526b;
        }

        public void a(long j) {
            this.f7526b = j;
        }
    }
}
