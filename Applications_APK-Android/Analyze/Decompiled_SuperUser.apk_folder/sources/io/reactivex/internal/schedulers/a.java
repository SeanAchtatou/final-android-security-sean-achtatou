package io.reactivex.internal.schedulers;

import io.reactivex.h;
import io.reactivex.internal.disposables.EmptyDisposable;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: ComputationScheduler */
public final class a extends h {

    /* renamed from: a  reason: collision with root package name */
    static final b f7489a = new b(0, f7490b);

    /* renamed from: b  reason: collision with root package name */
    static final RxThreadFactory f7490b = new RxThreadFactory("RxComputationThreadPool", Math.max(1, Math.min(10, Integer.getInteger("rx2.computation-priority", 5).intValue())), true);

    /* renamed from: c  reason: collision with root package name */
    static final int f7491c = a(Runtime.getRuntime().availableProcessors(), Integer.getInteger("rx2.computation-threads", 0).intValue());

    /* renamed from: d  reason: collision with root package name */
    static final c f7492d = new c(new RxThreadFactory("RxComputationShutdown"));

    /* renamed from: e  reason: collision with root package name */
    final ThreadFactory f7493e;

    /* renamed from: f  reason: collision with root package name */
    final AtomicReference<b> f7494f;

    static {
        f7492d.dispose();
        f7489a.b();
    }

    static int a(int i, int i2) {
        return (i2 <= 0 || i2 > i) ? i : i2;
    }

    /* compiled from: ComputationScheduler */
    static final class b {

        /* renamed from: a  reason: collision with root package name */
        final int f7500a;

        /* renamed from: b  reason: collision with root package name */
        final c[] f7501b;

        /* renamed from: c  reason: collision with root package name */
        long f7502c;

        b(int i, ThreadFactory threadFactory) {
            this.f7500a = i;
            this.f7501b = new c[i];
            for (int i2 = 0; i2 < i; i2++) {
                this.f7501b[i2] = new c(threadFactory);
            }
        }

        public c a() {
            int i = this.f7500a;
            if (i == 0) {
                return a.f7492d;
            }
            c[] cVarArr = this.f7501b;
            long j = this.f7502c;
            this.f7502c = 1 + j;
            return cVarArr[(int) (j % ((long) i))];
        }

        public void b() {
            for (c dispose : this.f7501b) {
                dispose.dispose();
            }
        }
    }

    public a() {
        this(f7490b);
    }

    public a(ThreadFactory threadFactory) {
        this.f7493e = threadFactory;
        this.f7494f = new AtomicReference<>(f7489a);
        start();
    }

    public h.c createWorker() {
        return new C0107a(this.f7494f.get().a());
    }

    public io.reactivex.disposables.b scheduleDirect(Runnable runnable, long j, TimeUnit timeUnit) {
        return this.f7494f.get().a().a(runnable, j, timeUnit);
    }

    public io.reactivex.disposables.b schedulePeriodicallyDirect(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        return this.f7494f.get().a().a(runnable, j, j2, timeUnit);
    }

    public void start() {
        b bVar = new b(f7491c, this.f7493e);
        if (!this.f7494f.compareAndSet(f7489a, bVar)) {
            bVar.b();
        }
    }

    public void shutdown() {
        b bVar;
        do {
            bVar = this.f7494f.get();
            if (bVar == f7489a) {
                return;
            }
        } while (!this.f7494f.compareAndSet(bVar, f7489a));
        bVar.b();
    }

    /* renamed from: io.reactivex.internal.schedulers.a$a  reason: collision with other inner class name */
    /* compiled from: ComputationScheduler */
    static final class C0107a extends h.c {

        /* renamed from: a  reason: collision with root package name */
        volatile boolean f7495a;

        /* renamed from: b  reason: collision with root package name */
        private final io.reactivex.internal.disposables.b f7496b = new io.reactivex.internal.disposables.b();

        /* renamed from: c  reason: collision with root package name */
        private final io.reactivex.disposables.a f7497c = new io.reactivex.disposables.a();

        /* renamed from: d  reason: collision with root package name */
        private final io.reactivex.internal.disposables.b f7498d = new io.reactivex.internal.disposables.b();

        /* renamed from: e  reason: collision with root package name */
        private final c f7499e;

        C0107a(c cVar) {
            this.f7499e = cVar;
            this.f7498d.a(this.f7496b);
            this.f7498d.a(this.f7497c);
        }

        public void dispose() {
            if (!this.f7495a) {
                this.f7495a = true;
                this.f7498d.dispose();
            }
        }

        public io.reactivex.disposables.b schedule(Runnable runnable) {
            if (this.f7495a) {
                return EmptyDisposable.INSTANCE;
            }
            return this.f7499e.a(runnable, 0, TimeUnit.MILLISECONDS, this.f7496b);
        }

        public io.reactivex.disposables.b schedule(Runnable runnable, long j, TimeUnit timeUnit) {
            if (this.f7495a) {
                return EmptyDisposable.INSTANCE;
            }
            return this.f7499e.a(runnable, j, timeUnit, this.f7497c);
        }
    }

    /* compiled from: ComputationScheduler */
    static final class c extends e {
        c(ThreadFactory threadFactory) {
            super(threadFactory);
        }
    }
}
