package io.reactivex.internal.schedulers;

import io.reactivex.disposables.b;
import io.reactivex.h;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.disposables.a;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/* compiled from: NewThreadWorker */
public class e extends h.c implements b {

    /* renamed from: a  reason: collision with root package name */
    volatile boolean f7529a;

    /* renamed from: b  reason: collision with root package name */
    private final ScheduledExecutorService f7530b;

    public e(ThreadFactory threadFactory) {
        this.f7530b = f.a(threadFactory);
    }

    public b schedule(Runnable runnable) {
        return schedule(runnable, 0, null);
    }

    public b schedule(Runnable runnable, long j, TimeUnit timeUnit) {
        if (this.f7529a) {
            return EmptyDisposable.INSTANCE;
        }
        return a(runnable, j, timeUnit, (a) null);
    }

    public b a(Runnable runnable, long j, TimeUnit timeUnit) {
        Future schedule;
        ScheduledDirectTask scheduledDirectTask = new ScheduledDirectTask(io.reactivex.b.a.a(runnable));
        if (j <= 0) {
            try {
                schedule = this.f7530b.submit(scheduledDirectTask);
            } catch (RejectedExecutionException e2) {
                io.reactivex.b.a.a(e2);
                return EmptyDisposable.INSTANCE;
            }
        } else {
            schedule = this.f7530b.schedule(scheduledDirectTask, j, timeUnit);
        }
        scheduledDirectTask.a(schedule);
        return scheduledDirectTask;
    }

    public b a(Runnable runnable, long j, long j2, TimeUnit timeUnit) {
        Future schedule;
        Runnable a2 = io.reactivex.b.a.a(runnable);
        if (j2 <= 0) {
            b bVar = new b(a2, this.f7530b);
            if (j <= 0) {
                try {
                    schedule = this.f7530b.submit(bVar);
                } catch (RejectedExecutionException e2) {
                    io.reactivex.b.a.a(e2);
                    return EmptyDisposable.INSTANCE;
                }
            } else {
                schedule = this.f7530b.schedule(bVar, j, timeUnit);
            }
            bVar.a(schedule);
            return bVar;
        }
        ScheduledDirectPeriodicTask scheduledDirectPeriodicTask = new ScheduledDirectPeriodicTask(a2);
        try {
            scheduledDirectPeriodicTask.a(this.f7530b.scheduleAtFixedRate(scheduledDirectPeriodicTask, j, j2, timeUnit));
            return scheduledDirectPeriodicTask;
        } catch (RejectedExecutionException e3) {
            io.reactivex.b.a.a(e3);
            return EmptyDisposable.INSTANCE;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{<V> java.util.concurrent.ScheduledExecutorService.schedule(java.util.concurrent.Callable, long, java.util.concurrent.TimeUnit):java.util.concurrent.ScheduledFuture<V>}
     arg types: [io.reactivex.internal.schedulers.ScheduledRunnable, long, java.util.concurrent.TimeUnit]
     candidates:
      ClspMth{java.util.concurrent.ScheduledExecutorService.schedule(java.lang.Runnable, long, java.util.concurrent.TimeUnit):java.util.concurrent.ScheduledFuture<?>}
      ClspMth{<V> java.util.concurrent.ScheduledExecutorService.schedule(java.util.concurrent.Callable, long, java.util.concurrent.TimeUnit):java.util.concurrent.ScheduledFuture<V>} */
    public ScheduledRunnable a(Runnable runnable, long j, TimeUnit timeUnit, a aVar) {
        Future schedule;
        ScheduledRunnable scheduledRunnable = new ScheduledRunnable(io.reactivex.b.a.a(runnable), aVar);
        if (aVar == null || aVar.a(scheduledRunnable)) {
            if (j <= 0) {
                try {
                    schedule = this.f7530b.submit((Callable) scheduledRunnable);
                } catch (RejectedExecutionException e2) {
                    if (aVar != null) {
                        aVar.b(scheduledRunnable);
                    }
                    io.reactivex.b.a.a(e2);
                }
            } else {
                schedule = this.f7530b.schedule((Callable) scheduledRunnable, j, timeUnit);
            }
            scheduledRunnable.a(schedule);
        }
        return scheduledRunnable;
    }

    public void dispose() {
        if (!this.f7529a) {
            this.f7529a = true;
            this.f7530b.shutdownNow();
        }
    }

    public void b() {
        if (!this.f7529a) {
            this.f7529a = true;
            this.f7530b.shutdown();
        }
    }
}
