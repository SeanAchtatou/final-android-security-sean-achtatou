package io.reactivex.internal.schedulers;

import io.reactivex.h;
import io.reactivex.internal.disposables.EmptyDisposable;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: TrampolineScheduler */
public final class h extends io.reactivex.h {

    /* renamed from: a  reason: collision with root package name */
    private static final h f7542a = new h();

    public static h a() {
        return f7542a;
    }

    public h.c createWorker() {
        return new c();
    }

    h() {
    }

    public io.reactivex.disposables.b scheduleDirect(Runnable runnable) {
        runnable.run();
        return EmptyDisposable.INSTANCE;
    }

    public io.reactivex.disposables.b scheduleDirect(Runnable runnable, long j, TimeUnit timeUnit) {
        try {
            timeUnit.sleep(j);
            runnable.run();
        } catch (InterruptedException e2) {
            Thread.currentThread().interrupt();
            io.reactivex.b.a.a(e2);
        }
        return EmptyDisposable.INSTANCE;
    }

    /* compiled from: TrampolineScheduler */
    static final class c extends h.c implements io.reactivex.disposables.b {

        /* renamed from: a  reason: collision with root package name */
        final PriorityBlockingQueue<b> f7550a = new PriorityBlockingQueue<>();

        /* renamed from: b  reason: collision with root package name */
        final AtomicInteger f7551b = new AtomicInteger();

        /* renamed from: c  reason: collision with root package name */
        volatile boolean f7552c;

        /* renamed from: d  reason: collision with root package name */
        private final AtomicInteger f7553d = new AtomicInteger();

        c() {
        }

        public io.reactivex.disposables.b schedule(Runnable runnable) {
            return a(runnable, now(TimeUnit.MILLISECONDS));
        }

        public io.reactivex.disposables.b schedule(Runnable runnable, long j, TimeUnit timeUnit) {
            long now = now(TimeUnit.MILLISECONDS) + timeUnit.toMillis(j);
            return a(new a(runnable, this, now), now);
        }

        /* access modifiers changed from: package-private */
        public io.reactivex.disposables.b a(Runnable runnable, long j) {
            if (this.f7552c) {
                return EmptyDisposable.INSTANCE;
            }
            b bVar = new b(runnable, Long.valueOf(j), this.f7551b.incrementAndGet());
            this.f7550a.add(bVar);
            if (this.f7553d.getAndIncrement() != 0) {
                return io.reactivex.disposables.c.a(new a(bVar));
            }
            int i = 1;
            while (!this.f7552c) {
                b poll = this.f7550a.poll();
                if (poll == null) {
                    int addAndGet = this.f7553d.addAndGet(-i);
                    if (addAndGet == 0) {
                        return EmptyDisposable.INSTANCE;
                    }
                    i = addAndGet;
                } else if (!poll.f7549d) {
                    poll.f7546a.run();
                }
            }
            this.f7550a.clear();
            return EmptyDisposable.INSTANCE;
        }

        public void dispose() {
            this.f7552c = true;
        }

        /* compiled from: TrampolineScheduler */
        final class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final b f7554a;

            a(b bVar) {
                this.f7554a = bVar;
            }

            public void run() {
                this.f7554a.f7549d = true;
                c.this.f7550a.remove(this.f7554a);
            }
        }
    }

    /* compiled from: TrampolineScheduler */
    static final class b implements Comparable<b> {

        /* renamed from: a  reason: collision with root package name */
        final Runnable f7546a;

        /* renamed from: b  reason: collision with root package name */
        final long f7547b;

        /* renamed from: c  reason: collision with root package name */
        final int f7548c;

        /* renamed from: d  reason: collision with root package name */
        volatile boolean f7549d;

        b(Runnable runnable, Long l, int i) {
            this.f7546a = runnable;
            this.f7547b = l.longValue();
            this.f7548c = i;
        }

        /* renamed from: a */
        public int compareTo(b bVar) {
            int a2 = io.reactivex.internal.a.b.a(this.f7547b, bVar.f7547b);
            if (a2 == 0) {
                return io.reactivex.internal.a.b.a(this.f7548c, bVar.f7548c);
            }
            return a2;
        }
    }

    /* compiled from: TrampolineScheduler */
    static final class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final Runnable f7543a;

        /* renamed from: b  reason: collision with root package name */
        private final c f7544b;

        /* renamed from: c  reason: collision with root package name */
        private final long f7545c;

        a(Runnable runnable, c cVar, long j) {
            this.f7543a = runnable;
            this.f7544b = cVar;
            this.f7545c = j;
        }

        public void run() {
            if (!this.f7544b.f7552c) {
                long now = this.f7544b.now(TimeUnit.MILLISECONDS);
                if (this.f7545c > now) {
                    long j = this.f7545c - now;
                    if (j > 0) {
                        try {
                            Thread.sleep(j);
                        } catch (InterruptedException e2) {
                            Thread.currentThread().interrupt();
                            io.reactivex.b.a.a(e2);
                            return;
                        }
                    }
                }
                if (!this.f7544b.f7552c) {
                    this.f7543a.run();
                }
            }
        }
    }
}
