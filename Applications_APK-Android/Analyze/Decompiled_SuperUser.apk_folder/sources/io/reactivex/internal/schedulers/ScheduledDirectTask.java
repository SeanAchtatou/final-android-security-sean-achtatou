package io.reactivex.internal.schedulers;

import java.util.concurrent.Callable;

public final class ScheduledDirectTask extends AbstractDirectTask implements Callable<Void> {
    public ScheduledDirectTask(Runnable runnable) {
        super(runnable);
    }

    /* renamed from: a */
    public Void call() {
        this.f7465b = Thread.currentThread();
        try {
            this.f7464a.run();
            return null;
        } finally {
            lazySet(f7462c);
            this.f7465b = null;
        }
    }
}
