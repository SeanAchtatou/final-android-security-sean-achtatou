package io.reactivex.internal.schedulers;

import io.reactivex.h;
import java.util.concurrent.ThreadFactory;

/* compiled from: NewThreadScheduler */
public final class d extends h {

    /* renamed from: b  reason: collision with root package name */
    private static final RxThreadFactory f7527b = new RxThreadFactory("RxNewThreadScheduler", Math.max(1, Math.min(10, Integer.getInteger("rx2.newthread-priority", 5).intValue())));

    /* renamed from: a  reason: collision with root package name */
    final ThreadFactory f7528a;

    public d() {
        this(f7527b);
    }

    public d(ThreadFactory threadFactory) {
        this.f7528a = threadFactory;
    }

    public h.c createWorker() {
        return new e(this.f7528a);
    }
}
