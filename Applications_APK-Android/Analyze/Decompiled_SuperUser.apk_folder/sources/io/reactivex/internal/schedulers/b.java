package io.reactivex.internal.schedulers;

import io.reactivex.internal.a.a;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: InstantPeriodicTask */
final class b implements io.reactivex.disposables.b, Callable<Void> {

    /* renamed from: f  reason: collision with root package name */
    static final FutureTask<Void> f7503f = new FutureTask<>(a.f7393b, null);

    /* renamed from: a  reason: collision with root package name */
    final Runnable f7504a;

    /* renamed from: b  reason: collision with root package name */
    final AtomicReference<Future<?>> f7505b = new AtomicReference<>();

    /* renamed from: c  reason: collision with root package name */
    final AtomicReference<Future<?>> f7506c = new AtomicReference<>();

    /* renamed from: d  reason: collision with root package name */
    final ExecutorService f7507d;

    /* renamed from: e  reason: collision with root package name */
    Thread f7508e;

    b(Runnable runnable, ExecutorService executorService) {
        this.f7504a = runnable;
        this.f7507d = executorService;
    }

    /* renamed from: a */
    public Void call() {
        try {
            this.f7508e = Thread.currentThread();
            this.f7504a.run();
            b(this.f7507d.submit(this));
        } catch (Throwable th) {
            this.f7508e = null;
            throw th;
        }
        this.f7508e = null;
        return null;
    }

    public void dispose() {
        boolean z = true;
        Future andSet = this.f7506c.getAndSet(f7503f);
        if (!(andSet == null || andSet == f7503f)) {
            andSet.cancel(this.f7508e != Thread.currentThread());
        }
        Future andSet2 = this.f7505b.getAndSet(f7503f);
        if (andSet2 != null && andSet2 != f7503f) {
            if (this.f7508e == Thread.currentThread()) {
                z = false;
            }
            andSet2.cancel(z);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Future<?> future) {
        Future future2;
        do {
            future2 = this.f7506c.get();
            if (future2 == f7503f) {
                future.cancel(this.f7508e != Thread.currentThread());
            }
        } while (!this.f7506c.compareAndSet(future2, future));
    }

    /* access modifiers changed from: package-private */
    public void b(Future<?> future) {
        Future future2;
        do {
            future2 = this.f7505b.get();
            if (future2 == f7503f) {
                future.cancel(this.f7508e != Thread.currentThread());
            }
        } while (!this.f7505b.compareAndSet(future2, future));
    }
}
