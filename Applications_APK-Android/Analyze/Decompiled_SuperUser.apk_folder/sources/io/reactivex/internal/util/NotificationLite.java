package io.reactivex.internal.util;

import io.reactivex.internal.a.b;
import java.io.Serializable;
import org.a.c;
import org.a.d;

public enum NotificationLite {
    COMPLETE;

    static final class ErrorNotification implements Serializable {

        /* renamed from: a  reason: collision with root package name */
        final Throwable f7583a;

        ErrorNotification(Throwable th) {
            this.f7583a = th;
        }

        public String toString() {
            return "NotificationLite.Error[" + this.f7583a + "]";
        }

        public int hashCode() {
            return this.f7583a.hashCode();
        }

        public boolean equals(Object obj) {
            if (obj instanceof ErrorNotification) {
                return b.a(this.f7583a, ((ErrorNotification) obj).f7583a);
            }
            return false;
        }
    }

    static final class SubscriptionNotification implements Serializable {

        /* renamed from: a  reason: collision with root package name */
        final d f7584a;

        SubscriptionNotification(d dVar) {
            this.f7584a = dVar;
        }

        public String toString() {
            return "NotificationLite.Subscription[" + this.f7584a + "]";
        }
    }

    public static <T> Object a(T t) {
        return t;
    }

    public static Object a() {
        return COMPLETE;
    }

    public static Object a(Throwable th) {
        return new ErrorNotification(th);
    }

    public static Object a(d dVar) {
        return new SubscriptionNotification(dVar);
    }

    public static <T> boolean a(Object obj, c<? super T> cVar) {
        if (obj == COMPLETE) {
            cVar.d();
            return true;
        } else if (obj instanceof ErrorNotification) {
            cVar.a(((ErrorNotification) obj).f7583a);
            return true;
        } else if (obj instanceof SubscriptionNotification) {
            cVar.a(((SubscriptionNotification) obj).f7584a);
            return false;
        } else {
            cVar.c(obj);
            return false;
        }
    }

    public String toString() {
        return "NotificationLite.Complete";
    }
}
