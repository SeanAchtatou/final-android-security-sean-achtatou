package io.reactivex.internal.util;

import io.reactivex.disposables.b;

/* compiled from: OpenHashSet */
public final class d<T> {

    /* renamed from: a  reason: collision with root package name */
    final float f7589a;

    /* renamed from: b  reason: collision with root package name */
    int f7590b;

    /* renamed from: c  reason: collision with root package name */
    int f7591c;

    /* renamed from: d  reason: collision with root package name */
    int f7592d;

    /* renamed from: e  reason: collision with root package name */
    T[] f7593e;

    public d() {
        this(16, 0.75f);
    }

    public d(int i, float f2) {
        this.f7589a = f2;
        int a2 = e.a(i);
        this.f7590b = a2 - 1;
        this.f7592d = (int) (((float) a2) * f2);
        this.f7593e = (Object[]) new Object[a2];
    }

    public boolean a(b bVar) {
        Object obj;
        Object[] objArr = this.f7593e;
        int i = this.f7590b;
        int a2 = a(bVar.hashCode()) & i;
        Object obj2 = objArr[a2];
        if (obj2 != null) {
            if (obj2.equals(bVar)) {
                return false;
            }
            do {
                a2 = (a2 + 1) & i;
                obj = objArr[a2];
                if (obj == null) {
                }
            } while (!obj.equals(bVar));
            return false;
        }
        objArr[a2] = bVar;
        int i2 = this.f7591c + 1;
        this.f7591c = i2;
        if (i2 >= this.f7592d) {
            a();
        }
        return true;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean b(T r6) {
        /*
            r5 = this;
            r1 = 0
            T[] r2 = r5.f7593e
            int r3 = r5.f7590b
            int r0 = r6.hashCode()
            int r0 = a(r0)
            r0 = r0 & r3
            r4 = r2[r0]
            if (r4 != 0) goto L_0x0014
            r0 = r1
        L_0x0013:
            return r0
        L_0x0014:
            boolean r4 = r4.equals(r6)
            if (r4 == 0) goto L_0x001f
            boolean r0 = r5.a(r0, r2, r3)
            goto L_0x0013
        L_0x001f:
            int r0 = r0 + 1
            r0 = r0 & r3
            r4 = r2[r0]
            if (r4 != 0) goto L_0x0028
            r0 = r1
            goto L_0x0013
        L_0x0028:
            boolean r4 = r4.equals(r6)
            if (r4 == 0) goto L_0x001f
            boolean r0 = r5.a(r0, r2, r3)
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: io.reactivex.internal.util.d.b(java.lang.Object):boolean");
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, T[] tArr, int i2) {
        int i3;
        T t;
        this.f7591c--;
        while (true) {
            int i4 = i + 1;
            while (true) {
                i3 = i4 & i2;
                t = tArr[i3];
                if (t == null) {
                    tArr[i] = null;
                    return true;
                }
                int a2 = a(t.hashCode()) & i2;
                if (i <= i3) {
                    if (i >= a2 || a2 > i3) {
                        break;
                    }
                    i4 = i3 + 1;
                } else {
                    if (i >= a2 && a2 > i3) {
                        break;
                    }
                    i4 = i3 + 1;
                }
            }
            tArr[i] = t;
            i = i3;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        T[] tArr = this.f7593e;
        int length = tArr.length;
        int i = length << 1;
        int i2 = i - 1;
        T[] tArr2 = (Object[]) new Object[i];
        int i3 = length;
        int i4 = this.f7591c;
        while (true) {
            int i5 = i4 - 1;
            if (i4 != 0) {
                do {
                    i3--;
                } while (tArr[i3] == null);
                int a2 = a(tArr[i3].hashCode()) & i2;
                if (tArr2[a2] != null) {
                    do {
                        a2 = (a2 + 1) & i2;
                    } while (tArr2[a2] != null);
                }
                tArr2[a2] = tArr[i3];
                i4 = i5;
            } else {
                this.f7590b = i2;
                this.f7592d = (int) (((float) i) * this.f7589a);
                this.f7593e = tArr2;
                return;
            }
        }
    }

    static int a(int i) {
        int i2 = -1640531527 * i;
        return i2 ^ (i2 >>> 16);
    }

    public Object[] b() {
        return this.f7593e;
    }
}
