package io.reactivex.internal.util;

import org.a.c;

/* compiled from: AppendOnlyLinkedArrayList */
public class a<T> {

    /* renamed from: a  reason: collision with root package name */
    final int f7585a;

    /* renamed from: b  reason: collision with root package name */
    final Object[] f7586b;

    /* renamed from: c  reason: collision with root package name */
    Object[] f7587c = this.f7586b;

    /* renamed from: d  reason: collision with root package name */
    int f7588d;

    public a(int i) {
        this.f7585a = i;
        this.f7586b = new Object[(i + 1)];
    }

    public void a(T t) {
        int i = this.f7585a;
        int i2 = this.f7588d;
        if (i2 == i) {
            Object[] objArr = new Object[(i + 1)];
            this.f7587c[i] = objArr;
            this.f7587c = objArr;
            i2 = 0;
        }
        this.f7587c[i2] = t;
        this.f7588d = i2 + 1;
    }

    public void b(T t) {
        this.f7586b[0] = t;
    }

    public <U> boolean a(c<? super U> cVar) {
        Object[] objArr = this.f7586b;
        int i = this.f7585a;
        for (Object[] objArr2 = objArr; objArr2 != null; objArr2 = objArr2[i]) {
            int i2 = 0;
            while (i2 < i) {
                Object[] objArr3 = objArr2[i2];
                if (objArr3 == null) {
                    continue;
                    break;
                } else if (NotificationLite.a(objArr3, cVar)) {
                    return true;
                } else {
                    i2++;
                }
            }
        }
        return false;
    }
}
