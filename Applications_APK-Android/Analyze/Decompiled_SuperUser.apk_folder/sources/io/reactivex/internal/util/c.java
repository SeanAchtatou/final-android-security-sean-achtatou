package io.reactivex.internal.util;

import io.reactivex.b.a;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: HalfSerializer */
public final class c {
    public static <T> void a(org.a.c cVar, Object obj, AtomicInteger atomicInteger, AtomicThrowable atomicThrowable) {
        if (atomicInteger.get() == 0 && atomicInteger.compareAndSet(0, 1)) {
            cVar.c(obj);
            if (atomicInteger.decrementAndGet() != 0) {
                Throwable a2 = atomicThrowable.a();
                if (a2 != null) {
                    cVar.a(a2);
                } else {
                    cVar.d();
                }
            }
        }
    }

    public static void a(org.a.c<?> cVar, Throwable th, AtomicInteger atomicInteger, AtomicThrowable atomicThrowable) {
        if (!atomicThrowable.a(th)) {
            a.a(th);
        } else if (atomicInteger.getAndIncrement() == 0) {
            cVar.a(atomicThrowable.a());
        }
    }

    public static void a(org.a.c<?> cVar, AtomicInteger atomicInteger, AtomicThrowable atomicThrowable) {
        if (atomicInteger.getAndIncrement() == 0) {
            Throwable a2 = atomicThrowable.a();
            if (a2 != null) {
                cVar.a(a2);
            } else {
                cVar.d();
            }
        }
    }
}
