package io.reactivex.internal.a;

import io.reactivex.exceptions.OnErrorNotImplementedException;
import java.util.Comparator;
import java.util.concurrent.Callable;

/* compiled from: Functions */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    static final io.reactivex.a.e<Object, Object> f7392a = new g();

    /* renamed from: b  reason: collision with root package name */
    public static final Runnable f7393b = new d();

    /* renamed from: c  reason: collision with root package name */
    public static final io.reactivex.a.a f7394c = new C0104a();

    /* renamed from: d  reason: collision with root package name */
    static final io.reactivex.a.d<Object> f7395d = new b();

    /* renamed from: e  reason: collision with root package name */
    public static final io.reactivex.a.d<Throwable> f7396e = new e();

    /* renamed from: f  reason: collision with root package name */
    public static final io.reactivex.a.d<Throwable> f7397f = new k();

    /* renamed from: g  reason: collision with root package name */
    public static final io.reactivex.a.f f7398g = new c();

    /* renamed from: h  reason: collision with root package name */
    static final io.reactivex.a.g<Object> f7399h = new l();
    static final io.reactivex.a.g<Object> i = new f();
    static final Callable<Object> j = new j();
    static final Comparator<Object> k = new i();
    public static final io.reactivex.a.d<org.a.d> l = new h();

    /* compiled from: Functions */
    static final class g implements io.reactivex.a.e<Object, Object> {
        g() {
        }

        public Object a(Object obj) {
            return obj;
        }

        public String toString() {
            return "IdentityFunction";
        }
    }

    /* compiled from: Functions */
    static final class d implements Runnable {
        d() {
        }

        public void run() {
        }

        public String toString() {
            return "EmptyRunnable";
        }
    }

    /* renamed from: io.reactivex.internal.a.a$a  reason: collision with other inner class name */
    /* compiled from: Functions */
    static final class C0104a implements io.reactivex.a.a {
        C0104a() {
        }

        public void a() {
        }

        public String toString() {
            return "EmptyAction";
        }
    }

    /* compiled from: Functions */
    static final class b implements io.reactivex.a.d<Object> {
        b() {
        }

        public void accept(Object obj) {
        }

        public String toString() {
            return "EmptyConsumer";
        }
    }

    /* compiled from: Functions */
    static final class e implements io.reactivex.a.d<Throwable> {
        e() {
        }

        /* renamed from: a */
        public void accept(Throwable th) {
            io.reactivex.b.a.a(th);
        }
    }

    /* compiled from: Functions */
    static final class k implements io.reactivex.a.d<Throwable> {
        k() {
        }

        /* renamed from: a */
        public void accept(Throwable th) {
            io.reactivex.b.a.a(new OnErrorNotImplementedException(th));
        }
    }

    /* compiled from: Functions */
    static final class c implements io.reactivex.a.f {
        c() {
        }
    }

    /* compiled from: Functions */
    static final class l implements io.reactivex.a.g<Object> {
        l() {
        }
    }

    /* compiled from: Functions */
    static final class f implements io.reactivex.a.g<Object> {
        f() {
        }
    }

    /* compiled from: Functions */
    static final class j implements Callable<Object> {
        j() {
        }

        public Object call() {
            return null;
        }
    }

    /* compiled from: Functions */
    static final class i implements Comparator<Object> {
        i() {
        }

        public int compare(Object obj, Object obj2) {
            return ((Comparable) obj).compareTo(obj2);
        }
    }

    /* compiled from: Functions */
    static final class h implements io.reactivex.a.d<org.a.d> {
        h() {
        }

        /* renamed from: a */
        public void accept(org.a.d dVar) {
            dVar.a(Long.MAX_VALUE);
        }
    }
}
