package io.reactivex.internal.a;

import io.reactivex.a.c;

/* compiled from: ObjectHelper */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    static final c<Object, Object> f7400a = new a();

    public static <T> T a(Object obj, String str) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(str);
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static int a(int i, int i2) {
        if (i < i2) {
            return -1;
        }
        return i > i2 ? 1 : 0;
    }

    public static int a(long j, long j2) {
        if (j < j2) {
            return -1;
        }
        return j > j2 ? 1 : 0;
    }

    public static int a(int i, String str) {
        if (i > 0) {
            return i;
        }
        throw new IllegalArgumentException(str + " > 0 required but it was " + i);
    }

    /* compiled from: ObjectHelper */
    static final class a implements c<Object, Object> {
        a() {
        }
    }
}
