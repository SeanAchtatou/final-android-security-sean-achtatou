package io.reactivex.exceptions;

import com.lody.virtual.helper.utils.FileUtils;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public final class CompositeException extends RuntimeException {

    /* renamed from: a  reason: collision with root package name */
    private final List<Throwable> f7374a;

    /* renamed from: b  reason: collision with root package name */
    private final String f7375b;

    /* renamed from: c  reason: collision with root package name */
    private Throwable f7376c;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CompositeException(Throwable... thArr) {
        this(thArr == null ? Collections.singletonList(new NullPointerException("exceptions was null")) : Arrays.asList(thArr));
    }

    public CompositeException(Iterable<? extends Throwable> iterable) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        ArrayList arrayList = new ArrayList();
        if (iterable != null) {
            for (Throwable th : iterable) {
                if (th instanceof CompositeException) {
                    linkedHashSet.addAll(((CompositeException) th).a());
                } else if (th != null) {
                    linkedHashSet.add(th);
                } else {
                    linkedHashSet.add(new NullPointerException("Throwable was null!"));
                }
            }
        } else {
            linkedHashSet.add(new NullPointerException("errors was null"));
        }
        if (linkedHashSet.isEmpty()) {
            throw new IllegalArgumentException("errors is empty");
        }
        arrayList.addAll(linkedHashSet);
        this.f7374a = Collections.unmodifiableList(arrayList);
        this.f7375b = this.f7374a.size() + " exceptions occurred. ";
    }

    public List<Throwable> a() {
        return this.f7374a;
    }

    public String getMessage() {
        return this.f7375b;
    }

    public synchronized Throwable getCause() {
        if (this.f7376c == null) {
            Throwable compositeExceptionCausalChain = new CompositeExceptionCausalChain();
            HashSet hashSet = new HashSet();
            Throwable th = compositeExceptionCausalChain;
            for (Throwable next : this.f7374a) {
                if (!hashSet.contains(next)) {
                    hashSet.add(next);
                    RuntimeException runtimeException = next;
                    for (Throwable next2 : a(next)) {
                        if (hashSet.contains(next2)) {
                            runtimeException = new RuntimeException("Duplicate found in causal chain so cropping to prevent loop ...");
                        } else {
                            hashSet.add(next2);
                        }
                    }
                    try {
                        th.initCause(runtimeException);
                    } catch (Throwable th2) {
                    }
                    th = b(th);
                }
            }
            this.f7376c = compositeExceptionCausalChain;
        }
        return this.f7376c;
    }

    public void printStackTrace() {
        printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream printStream) {
        a(new b(printStream));
    }

    public void printStackTrace(PrintWriter printWriter) {
        a(new c(printWriter));
    }

    private void a(a aVar) {
        StringBuilder sb = new StringBuilder((int) FileUtils.FileMode.MODE_IWUSR);
        sb.append(this).append(10);
        for (StackTraceElement append : getStackTrace()) {
            sb.append("\tat ").append(append).append(10);
        }
        int i = 1;
        Iterator<Throwable> it = this.f7374a.iterator();
        while (true) {
            int i2 = i;
            if (it.hasNext()) {
                sb.append("  ComposedException ").append(i2).append(" :\n");
                a(sb, it.next(), "\t");
                i = i2 + 1;
            } else {
                aVar.a(sb.toString());
                return;
            }
        }
    }

    private void a(StringBuilder sb, Throwable th, String str) {
        sb.append(str).append(th).append(10);
        for (StackTraceElement append : th.getStackTrace()) {
            sb.append("\t\tat ").append(append).append(10);
        }
        if (th.getCause() != null) {
            sb.append("\tCaused by: ");
            a(sb, th.getCause(), "");
        }
    }

    static abstract class a {
        /* access modifiers changed from: package-private */
        public abstract void a(Object obj);

        a() {
        }
    }

    static final class b extends a {

        /* renamed from: a  reason: collision with root package name */
        private final PrintStream f7377a;

        b(PrintStream printStream) {
            this.f7377a = printStream;
        }

        /* access modifiers changed from: package-private */
        public void a(Object obj) {
            this.f7377a.println(obj);
        }
    }

    static final class c extends a {

        /* renamed from: a  reason: collision with root package name */
        private final PrintWriter f7378a;

        c(PrintWriter printWriter) {
            this.f7378a = printWriter;
        }

        /* access modifiers changed from: package-private */
        public void a(Object obj) {
            this.f7378a.println(obj);
        }
    }

    static final class CompositeExceptionCausalChain extends RuntimeException {
        CompositeExceptionCausalChain() {
        }

        public String getMessage() {
            return "Chain of Causes for CompositeException In Order Received =>";
        }
    }

    private List<Throwable> a(Throwable th) {
        ArrayList arrayList = new ArrayList();
        Throwable cause = th.getCause();
        if (cause == null || cause == th) {
            return arrayList;
        }
        while (true) {
            arrayList.add(cause);
            Throwable cause2 = cause.getCause();
            if (cause2 != null && cause2 != cause) {
                cause = cause2;
            }
        }
        return arrayList;
    }

    private Throwable b(Throwable th) {
        Throwable cause = th.getCause();
        if (cause == null || this.f7376c == cause) {
            return th;
        }
        while (true) {
            Throwable cause2 = cause.getCause();
            if (cause2 == null || cause2 == cause) {
                return cause;
            }
            cause = cause2;
        }
    }
}
