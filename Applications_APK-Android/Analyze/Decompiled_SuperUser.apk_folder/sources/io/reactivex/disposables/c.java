package io.reactivex.disposables;

import io.reactivex.internal.a.b;
import io.reactivex.internal.disposables.EmptyDisposable;

/* compiled from: Disposables */
public final class c {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public static b a(Runnable runnable) {
        b.a((Object) runnable, "run is null");
        return new RunnableDisposable(runnable);
    }

    public static b a() {
        return EmptyDisposable.INSTANCE;
    }
}
