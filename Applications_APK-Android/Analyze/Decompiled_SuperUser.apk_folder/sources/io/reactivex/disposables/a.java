package io.reactivex.disposables;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.internal.a.b;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.internal.util.d;
import java.util.ArrayList;

/* compiled from: CompositeDisposable */
public final class a implements b, io.reactivex.internal.disposables.a {

    /* renamed from: a  reason: collision with root package name */
    d<b> f7372a;

    /* renamed from: b  reason: collision with root package name */
    volatile boolean f7373b;

    public void dispose() {
        if (!this.f7373b) {
            synchronized (this) {
                if (!this.f7373b) {
                    this.f7373b = true;
                    d<b> dVar = this.f7372a;
                    this.f7372a = null;
                    a(dVar);
                }
            }
        }
    }

    public boolean a() {
        return this.f7373b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.disposables.b, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    public boolean a(b bVar) {
        b.a((Object) bVar, "d is null");
        if (!this.f7373b) {
            synchronized (this) {
                if (!this.f7373b) {
                    d<b> dVar = this.f7372a;
                    if (dVar == null) {
                        dVar = new d<>();
                        this.f7372a = dVar;
                    }
                    dVar.a(bVar);
                    return true;
                }
            }
        }
        bVar.dispose();
        return false;
    }

    public boolean b(b bVar) {
        if (!c(bVar)) {
            return false;
        }
        bVar.dispose();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T
     arg types: [io.reactivex.disposables.b, java.lang.String]
     candidates:
      io.reactivex.internal.a.b.a(int, int):int
      io.reactivex.internal.a.b.a(int, java.lang.String):int
      io.reactivex.internal.a.b.a(long, long):int
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.Object):boolean
      io.reactivex.internal.a.b.a(java.lang.Object, java.lang.String):T */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean c(io.reactivex.disposables.b r3) {
        /*
            r2 = this;
            r0 = 0
            java.lang.String r1 = "Disposable item is null"
            io.reactivex.internal.a.b.a(r3, r1)
            boolean r1 = r2.f7373b
            if (r1 == 0) goto L_0x000b
        L_0x000a:
            return r0
        L_0x000b:
            monitor-enter(r2)
            boolean r1 = r2.f7373b     // Catch:{ all -> 0x0012 }
            if (r1 == 0) goto L_0x0015
            monitor-exit(r2)     // Catch:{ all -> 0x0012 }
            goto L_0x000a
        L_0x0012:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0012 }
            throw r0
        L_0x0015:
            io.reactivex.internal.util.d<io.reactivex.disposables.b> r1 = r2.f7372a     // Catch:{ all -> 0x0012 }
            if (r1 == 0) goto L_0x001f
            boolean r1 = r1.b(r3)     // Catch:{ all -> 0x0012 }
            if (r1 != 0) goto L_0x0021
        L_0x001f:
            monitor-exit(r2)     // Catch:{ all -> 0x0012 }
            goto L_0x000a
        L_0x0021:
            monitor-exit(r2)     // Catch:{ all -> 0x0012 }
            r0 = 1
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: io.reactivex.disposables.a.c(io.reactivex.disposables.b):boolean");
    }

    /* access modifiers changed from: package-private */
    public void a(d<b> dVar) {
        ArrayList arrayList;
        if (dVar != null) {
            ArrayList arrayList2 = null;
            for (Object obj : dVar.b()) {
                if (obj instanceof b) {
                    try {
                        ((b) obj).dispose();
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        io.reactivex.exceptions.a.b(th2);
                        if (arrayList2 == null) {
                            arrayList = new ArrayList();
                        } else {
                            arrayList = arrayList2;
                        }
                        arrayList.add(th2);
                        arrayList2 = arrayList;
                    }
                }
            }
            if (arrayList2 == null) {
                return;
            }
            if (arrayList2.size() == 1) {
                throw ExceptionHelper.a((Throwable) arrayList2.get(0));
            }
            throw new CompositeException(arrayList2);
        }
    }
}
