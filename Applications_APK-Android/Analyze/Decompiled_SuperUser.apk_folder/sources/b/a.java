package b;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/* compiled from: LoadingDialog */
public class a extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private TextView f2305a;

    /* renamed from: b  reason: collision with root package name */
    private String f2306b;

    /* renamed from: c  reason: collision with root package name */
    private Context f2307c;

    /* renamed from: d  reason: collision with root package name */
    private ProgressBar f2308d;

    public a(Context context) {
        super(context);
        this.f2307c = context;
        requestWindowFeature(1);
        getWindow().setBackgroundDrawableResource(17170445);
        setContentView(a());
    }

    private View a() {
        LinearLayout linearLayout = new LinearLayout(this.f2307c);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(1);
        this.f2308d = new ProgressBar(this.f2307c);
        linearLayout.addView(this.f2308d);
        this.f2305a = new TextView(this.f2307c);
        this.f2305a.setTextAppearance(this.f2307c, 16973894);
        this.f2305a.setTextColor(Color.parseColor("#b3b3b3"));
        this.f2305a.setText("Switching to Google Play...");
        linearLayout.addView(this.f2305a);
        return linearLayout;
    }

    private void b() {
        this.f2305a.setText(this.f2306b);
    }

    public void a(String str) {
        this.f2306b = str;
        if (isShowing()) {
            b();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 84) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }
}
