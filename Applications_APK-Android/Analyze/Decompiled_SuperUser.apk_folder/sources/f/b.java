package f;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import com.daps.weather.base.c;
import com.daps.weather.base.d;
import com.daps.weather.base.e;
import com.daps.weather.base.f;
import com.daps.weather.bean.currentconditions.CurrentCondition;
import com.daps.weather.bean.forecasts.Forecast;
import com.daps.weather.bean.locations.Location;
import com.google.weathergson.Gson;
import com.google.weathergson.JsonElement;
import com.google.weathergson.JsonParser;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/* compiled from: WeatherTaskUtils */
public class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f7009a = b.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static b f7010b;

    /* renamed from: c  reason: collision with root package name */
    private Context f7011c;

    /* renamed from: d  reason: collision with root package name */
    private SQLiteDatabase f7012d = f.a(this.f7011c).getWritableDatabase();

    /* renamed from: e  reason: collision with root package name */
    private c f7013e;

    /* renamed from: f  reason: collision with root package name */
    private a f7014f;

    /* compiled from: WeatherTaskUtils */
    public interface a {
        void a(List list);
    }

    /* renamed from: f.b$b  reason: collision with other inner class name */
    /* compiled from: WeatherTaskUtils */
    public interface C0100b {
        void a(Object obj);
    }

    public static b a(Context context) {
        if (f7010b == null) {
            synchronized (b.class) {
                if (f7010b == null) {
                    f7010b = new b(context);
                }
            }
        }
        return f7010b;
    }

    b(Context context) {
        this.f7011c = context;
    }

    public void a(final String str, final String str2, final a aVar) {
        new com.daps.weather.location.b() {
            /* access modifiers changed from: protected */
            public void onPreExecute() {
                d.a(b.f7009a, "onPreExecute");
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public String doInBackground(Void... voidArr) {
                d.a(b.f7009a, "doInBackground");
                ArrayList arrayList = new ArrayList();
                arrayList.add(new BasicNameValuePair("q", str + "," + str2));
                arrayList.add(new BasicNameValuePair("apikey", "c6cdd06a25f145448c2b84bc0af0cc46"));
                try {
                    try {
                        HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(new URL(c.f3376a + "locations/v1/geoposition/search.json?" + URLEncodedUtils.format(arrayList, "UTF-8")).toString().trim()));
                        if (execute.getStatusLine().getStatusCode() == 200) {
                            return EntityUtils.toString(execute.getEntity());
                        }
                        return null;
                    } catch (ClientProtocolException e2) {
                        e2.printStackTrace();
                        return null;
                    } catch (IOException e3) {
                        e3.printStackTrace();
                        return null;
                    }
                } catch (MalformedURLException e4) {
                    e4.printStackTrace();
                    return null;
                }
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void onPostExecute(String str) {
                d.a(b.f7009a, "onPostExecute:" + str);
                List a2 = b.this.b(str, Location.class);
                if (a2 != null && a2 != null && a2.size() > 0) {
                    aVar.a(a2);
                }
            }

            /* access modifiers changed from: protected */
            public void onCancelled() {
                d.a(b.f7009a, "onPreExecute");
            }
        }.a((Object[]) new Void[0]);
    }

    /* access modifiers changed from: private */
    public List b(String str, Class cls) {
        try {
            if (!TextUtils.isEmpty(str)) {
                if (cls.getSimpleName().equals(Location.class.getSimpleName())) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("data", str);
                    contentValues.put("ts", Long.valueOf(System.currentTimeMillis()));
                    if (e.e(this.f7011c) && this.f7012d.insert("locations", null, contentValues) > 0) {
                        d.a(f7009a, "插入location成功");
                    }
                } else if (cls.getSimpleName().equals(CurrentCondition.class.getSimpleName())) {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("data", str);
                    contentValues2.put("ts", Long.valueOf(System.currentTimeMillis()));
                    if (e.e(this.f7011c) && this.f7012d.insert("currentconditions", null, contentValues2) > 0) {
                        d.a(f7009a, "插入currentconditions成功");
                    }
                }
            }
            List a2 = a(str, cls);
            d.a(f7009a, "data1:" + a2.size());
            return a2;
        } catch (Exception e2) {
            e2.printStackTrace();
            d.b(f7009a, "error:" + e2.getMessage());
            return null;
        }
    }

    public List a(String str, Class cls) {
        ArrayList arrayList = new ArrayList();
        try {
            Gson gson = new Gson();
            d.a(f7009a, "jsonString:" + str);
            Iterator it = new JsonParser().parse(str).getAsJsonArray().iterator();
            while (it.hasNext()) {
                arrayList.add(gson.fromJson((JsonElement) it.next(), cls));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            d.a(f7009a, "getObjectList:" + e2.getMessage());
        }
        return arrayList;
    }

    public void a(final String str, final a aVar) {
        new com.daps.weather.location.b() {
            /* access modifiers changed from: protected */
            public void onPreExecute() {
                d.a(b.f7009a, "getCurrentConditionsRequest_onPreExecute");
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public String doInBackground(Void... voidArr) {
                d.a(b.f7009a, "getCurrentConditionsRequest_doInBackground");
                try {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(new BasicNameValuePair("apikey", "c6cdd06a25f145448c2b84bc0af0cc46"));
                    arrayList.add(new BasicNameValuePair("details", "true"));
                    String trim = new URL(c.f3376a + "currentconditions/v1/" + str + ".json?" + URLEncodedUtils.format(arrayList, "UTF-8")).toString().trim();
                    d.a(b.f7009a, "requesdtURL:" + trim);
                    try {
                        HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(trim));
                        if (execute.getStatusLine().getStatusCode() == 200) {
                            return EntityUtils.toString(execute.getEntity());
                        }
                        return null;
                    } catch (ClientProtocolException e2) {
                        e2.printStackTrace();
                        return null;
                    } catch (IOException e3) {
                        e3.printStackTrace();
                        return null;
                    }
                } catch (MalformedURLException e4) {
                    e4.printStackTrace();
                    return null;
                }
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void onPostExecute(String str) {
                d.a(b.f7009a, "getCurrentConditionsRequest_onPostExecute:" + str);
                List a2 = b.this.b(str, CurrentCondition.class);
                if (aVar != null && a2 != null && a2.size() > 0) {
                    aVar.a(a2);
                }
            }

            /* access modifiers changed from: protected */
            public void onCancelled() {
                d.a(b.f7009a, "getCurrentConditionsRequest_onPreExecute");
            }
        }.a((Object[]) new Void[0]);
    }

    public void a(final String str, final String str2, final C0100b bVar) {
        new com.daps.weather.location.b() {
            /* access modifiers changed from: protected */
            public void onPreExecute() {
                d.a(b.f7009a, "getForecastsRequest_onPreExecute");
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public String doInBackground(Void... voidArr) {
                d.a(b.f7009a, "getForecastsRequest_doInBackground");
                try {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(new BasicNameValuePair("details", "true"));
                    arrayList.add(new BasicNameValuePair("apikey", "c6cdd06a25f145448c2b84bc0af0cc46"));
                    String trim = new URL(c.f3376a + "forecasts/v1/" + "daily/" + str2 + "day/" + str + "?" + URLEncodedUtils.format(arrayList, "UTF-8")).toString().trim();
                    d.a(b.f7009a, "requesdtURL:" + trim);
                    try {
                        HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(trim));
                        if (execute.getStatusLine().getStatusCode() == 200) {
                            return EntityUtils.toString(execute.getEntity());
                        }
                        return null;
                    } catch (ClientProtocolException e2) {
                        e2.printStackTrace();
                        return null;
                    } catch (IOException e3) {
                        e3.printStackTrace();
                        return null;
                    }
                } catch (MalformedURLException e4) {
                    e4.printStackTrace();
                    return null;
                }
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void onPostExecute(String str) {
                d.a(b.f7009a, "getForecastsRequest_onPostExecute:" + str);
                Forecast forecast = (Forecast) b.this.c(str, Forecast.class);
                if (bVar != null && forecast != null) {
                    bVar.a(forecast);
                }
            }

            /* access modifiers changed from: protected */
            public void onCancelled() {
                d.a(b.f7009a, "getForecastsRequest_onPreExecute");
            }
        }.a((Object[]) new Void[0]);
    }

    /* access modifiers changed from: private */
    public Object c(String str, Class cls) {
        try {
            if (!TextUtils.isEmpty(str)) {
                if (cls.getSimpleName().equals(Forecast.class.getSimpleName())) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("data", str);
                    contentValues.put("ts", Long.valueOf(System.currentTimeMillis()));
                    if (e.e(this.f7011c) && this.f7012d.insert("forecasts", null, contentValues) > 0) {
                        d.a(f7009a, "插入ForeCasts成功");
                    }
                } else if (cls.getSimpleName().equals(Location.class.getSimpleName())) {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("data", str);
                    contentValues2.put("ts", Long.valueOf(System.currentTimeMillis()));
                    if (e.e(this.f7011c) && this.f7012d.insert("locations", null, contentValues2) > 0) {
                        d.a(f7009a, "插入Locations成功");
                    }
                }
            }
            return new Gson().fromJson(str, cls);
        } catch (Exception e2) {
            e2.printStackTrace();
            d.b(f7009a, "error:" + e2.getMessage());
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.f7013e != null && this.f7013e.getStatus().equals(AsyncTask.Status.RUNNING)) {
            this.f7013e.cancel(true);
        }
        this.f7013e = new c(a(this.f7011c, "weather.zip"), a(this.f7011c, ""), this.f7011c, true);
        this.f7013e.execute(new Void[0]);
    }

    public void b() {
        if (d()) {
            d.a(f7009a, "已经存在了");
            return;
        }
        if (this.f7014f != null && this.f7014f.getStatus().equals(AsyncTask.Status.RUNNING)) {
            this.f7014f.cancel(true);
        }
        this.f7014f = new a("http://s.duapps.com/weather/weather.zip", a(this.f7011c, ""), this.f7011c);
        this.f7014f.execute(new Void[0]);
    }

    private static String a(Context context, String str) {
        String str2;
        try {
            if (Environment.getExternalStorageDirectory().equals("mounted")) {
                str2 = Environment.getExternalStorageDirectory().getPath() + "/" + str;
                d.a(f7009a, "使用sdcard path");
            } else {
                str2 = context.getCacheDir().getPath() + "/" + str;
                d.a(f7009a, "使用内存 path");
            }
            d.a(f7009a, "weatherPath:" + str2);
            return str2;
        } catch (Exception e2) {
            e2.printStackTrace();
            d.b(f7009a, "获取path失败:" + e2.getMessage());
            return "";
        }
    }

    private boolean d() {
        File file = new File(a(this.f7011c, "weather"));
        if (file == null || !file.exists() || !file.isDirectory() || file.list().length <= 0) {
            return false;
        }
        return true;
    }

    public Bitmap a(int i) {
        try {
            String str = a(this.f7011c, "weather") + "/weather" + i + ".png";
            d.a(f7009a, "needFile:" + str);
            return BitmapFactory.decodeFile(str);
        } catch (Exception e2) {
            e2.printStackTrace();
            d.b(f7009a, "没有图片");
            return null;
        }
    }
}
