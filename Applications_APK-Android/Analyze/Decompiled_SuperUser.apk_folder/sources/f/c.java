package f;

import android.content.Context;
import android.os.AsyncTask;
import com.daps.weather.base.d;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* compiled from: ZipExtractorTask */
public class c extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private final String f7026a = c.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private final File f7027b;

    /* renamed from: c  reason: collision with root package name */
    private final File f7028c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public int f7029d = 0;

    public c(String str, String str2, Context context, boolean z) {
        this.f7027b = new File(str);
        this.f7028c = new File(str2);
        if (!this.f7028c.exists() && !this.f7028c.mkdirs()) {
            d.b(this.f7026a, "Failed to make directories:" + this.f7028c.getAbsolutePath());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Long doInBackground(Void... voidArr) {
        d.a(this.f7026a, "解压中。。");
        return Long.valueOf(a());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Long l) {
        d.a(this.f7026a, "成功解压");
        if (isCancelled()) {
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x008a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x008b, code lost:
        r10 = r0;
        r0 = r2;
        r2 = r10;
        r3 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a6, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a7, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b8, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b9, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c5, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c6, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ca, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00d4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00d5, code lost:
        r10 = r0;
        r0 = r2;
        r2 = r10;
        r3 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0095 A[SYNTHETIC, Splitter:B:20:0x0095] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b4 A[SYNTHETIC, Splitter:B:35:0x00b4] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00c1 A[SYNTHETIC, Splitter:B:42:0x00c1] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ca A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x000a] */
    /* JADX WARNING: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:67:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:17:0x0090=Splitter:B:17:0x0090, B:32:0x00af=Splitter:B:32:0x00af} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private long a() {
        /*
            r12 = this;
            r4 = 0
            r2 = 0
            java.util.zip.ZipFile r1 = new java.util.zip.ZipFile     // Catch:{ ZipException -> 0x00db, IOException -> 0x00ab, all -> 0x00bd }
            java.io.File r0 = r12.f7027b     // Catch:{ ZipException -> 0x00db, IOException -> 0x00ab, all -> 0x00bd }
            r1.<init>(r0)     // Catch:{ ZipException -> 0x00db, IOException -> 0x00ab, all -> 0x00bd }
            long r2 = r12.a(r1)     // Catch:{ ZipException -> 0x00e0, IOException -> 0x00cf, all -> 0x00ca }
            r0 = 2
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ ZipException -> 0x00e0, IOException -> 0x00cf, all -> 0x00ca }
            r6 = 0
            r7 = 0
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ ZipException -> 0x00e0, IOException -> 0x00cf, all -> 0x00ca }
            r0[r6] = r7     // Catch:{ ZipException -> 0x00e0, IOException -> 0x00cf, all -> 0x00ca }
            r6 = 1
            int r2 = (int) r2     // Catch:{ ZipException -> 0x00e0, IOException -> 0x00cf, all -> 0x00ca }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ ZipException -> 0x00e0, IOException -> 0x00cf, all -> 0x00ca }
            r0[r6] = r2     // Catch:{ ZipException -> 0x00e0, IOException -> 0x00cf, all -> 0x00ca }
            r12.publishProgress(r0)     // Catch:{ ZipException -> 0x00e0, IOException -> 0x00cf, all -> 0x00ca }
            java.util.Enumeration r6 = r1.entries()     // Catch:{ ZipException -> 0x00e0, IOException -> 0x00cf, all -> 0x00ca }
            r2 = r4
        L_0x0029:
            boolean r0 = r6.hasMoreElements()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            if (r0 == 0) goto L_0x0099
            java.lang.Object r0 = r6.nextElement()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.util.zip.ZipEntry r0 = (java.util.zip.ZipEntry) r0     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            boolean r4 = r0.isDirectory()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            if (r4 != 0) goto L_0x0029
            java.io.File r4 = new java.io.File     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.io.File r5 = r12.f7028c     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.lang.String r7 = r0.getName()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            r4.<init>(r5, r7)     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.io.File r5 = r4.getParentFile()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            boolean r5 = r5.exists()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            if (r5 != 0) goto L_0x0077
            java.lang.String r5 = r12.f7026a     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            r7.<init>()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.lang.String r8 = "make="
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.io.File r8 = r4.getParentFile()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.lang.String r8 = r8.getAbsolutePath()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.lang.String r7 = r7.toString()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            com.daps.weather.base.d.b(r5, r7)     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.io.File r5 = r4.getParentFile()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            r5.mkdirs()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
        L_0x0077:
            f.c$a r5 = new f.c$a     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            r5.<init>(r4)     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            java.io.InputStream r0 = r1.getInputStream(r0)     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            int r0 = r12.a(r0, r5)     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            long r8 = (long) r0     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            long r2 = r2 + r8
            r5.close()     // Catch:{ ZipException -> 0x008a, IOException -> 0x00d4, all -> 0x00ca }
            goto L_0x0029
        L_0x008a:
            r0 = move-exception
            r10 = r0
            r11 = r1
            r0 = r2
            r2 = r10
            r3 = r11
        L_0x0090:
            r2.printStackTrace()     // Catch:{ all -> 0x00cc }
            if (r3 == 0) goto L_0x0098
            r3.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x0098:
            return r0
        L_0x0099:
            if (r1 == 0) goto L_0x009e
            r1.close()     // Catch:{ IOException -> 0x00a0 }
        L_0x009e:
            r0 = r2
            goto L_0x0098
        L_0x00a0:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x0098
        L_0x00a6:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0098
        L_0x00ab:
            r0 = move-exception
            r3 = r2
            r2 = r0
            r0 = r4
        L_0x00af:
            r2.printStackTrace()     // Catch:{ all -> 0x00cc }
            if (r3 == 0) goto L_0x0098
            r3.close()     // Catch:{ IOException -> 0x00b8 }
            goto L_0x0098
        L_0x00b8:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0098
        L_0x00bd:
            r0 = move-exception
            r1 = r2
        L_0x00bf:
            if (r1 == 0) goto L_0x00c4
            r1.close()     // Catch:{ IOException -> 0x00c5 }
        L_0x00c4:
            throw r0
        L_0x00c5:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00c4
        L_0x00ca:
            r0 = move-exception
            goto L_0x00bf
        L_0x00cc:
            r0 = move-exception
            r1 = r3
            goto L_0x00bf
        L_0x00cf:
            r0 = move-exception
            r2 = r0
            r3 = r1
            r0 = r4
            goto L_0x00af
        L_0x00d4:
            r0 = move-exception
            r10 = r0
            r11 = r1
            r0 = r2
            r2 = r10
            r3 = r11
            goto L_0x00af
        L_0x00db:
            r0 = move-exception
            r3 = r2
            r2 = r0
            r0 = r4
            goto L_0x0090
        L_0x00e0:
            r0 = move-exception
            r2 = r0
            r3 = r1
            r0 = r4
            goto L_0x0090
        */
        throw new UnsupportedOperationException("Method not decompiled: f.c.a():long");
    }

    private long a(ZipFile zipFile) {
        long j;
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        long j2 = 0;
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            if (zipEntry.getSize() >= 0) {
                j = zipEntry.getSize() + j2;
            } else {
                j = j2;
            }
            j2 = j;
        }
        return j2;
    }

    private int a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[8192];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 8192);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream, 8192);
        int i = 0;
        while (true) {
            try {
                int read = bufferedInputStream.read(bArr, 0, 8192);
                if (read == -1) {
                    break;
                }
                bufferedOutputStream.write(bArr, 0, read);
                i = read + i;
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    bufferedOutputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                try {
                    bufferedInputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    bufferedOutputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                try {
                    bufferedInputStream.close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
                throw th;
            }
        }
        bufferedOutputStream.flush();
        try {
            bufferedOutputStream.close();
        } catch (IOException e7) {
            e7.printStackTrace();
        }
        try {
            bufferedInputStream.close();
        } catch (IOException e8) {
            e8.printStackTrace();
        }
        return i;
    }

    /* compiled from: ZipExtractorTask */
    private final class a extends FileOutputStream {
        public a(File file) {
            super(file);
        }

        public void write(byte[] bArr, int i, int i2) {
            super.write(bArr, i, i2);
            int unused = c.this.f7029d = c.this.f7029d + i2;
            c.this.publishProgress(Integer.valueOf(c.this.f7029d));
        }
    }
}
