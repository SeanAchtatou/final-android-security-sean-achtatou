package f;

import android.content.Context;
import android.os.AsyncTask;
import com.daps.weather.base.d;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/* compiled from: DownLoaderTask */
public class a extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private static final String f7003a = a.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private URL f7004b;

    /* renamed from: c  reason: collision with root package name */
    private File f7005c;

    /* renamed from: d  reason: collision with root package name */
    private C0099a f7006d;

    /* renamed from: e  reason: collision with root package name */
    private Context f7007e;

    public a(String str, String str2, Context context) {
        try {
            this.f7004b = new URL(str);
            String name = new File(this.f7004b.getFile()).getName();
            this.f7005c = new File(str2, name);
            d.a(f7003a, "out=" + str2 + ", name=" + name + ",mUrl.getFile()=" + this.f7004b.getFile());
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Long doInBackground(Void... voidArr) {
        return Long.valueOf(a());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Long l) {
        d.a(f7003a, "下载完了");
        if (!isCancelled() && l.longValue() > 0) {
            d.a(f7003a, "去解压");
            b.a(this.f7007e).a();
        }
    }

    private long a() {
        IOException e2;
        int i;
        try {
            URLConnection openConnection = this.f7004b.openConnection();
            int contentLength = openConnection.getContentLength();
            if (!this.f7005c.exists() || ((long) contentLength) != this.f7005c.length()) {
                this.f7006d = new C0099a(this.f7005c);
                publishProgress(0, Integer.valueOf(contentLength));
                i = a(openConnection.getInputStream(), this.f7006d);
                if (!(i == contentLength || contentLength == -1)) {
                    try {
                        d.b(f7003a, "Download incomplete bytesCopied=" + i + ", length" + contentLength);
                    } catch (IOException e3) {
                        e2 = e3;
                        e2.printStackTrace();
                        return (long) i;
                    }
                }
                this.f7006d.close();
                return (long) i;
            }
            d.a(f7003a, "file " + this.f7005c.getName() + " already exits!!");
            return 1;
        } catch (IOException e4) {
            IOException iOException = e4;
            i = 0;
            e2 = iOException;
            e2.printStackTrace();
            return (long) i;
        }
    }

    private int a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[8192];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 8192);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream, 8192);
        int i = 0;
        while (true) {
            try {
                int read = bufferedInputStream.read(bArr, 0, 8192);
                if (read == -1) {
                    break;
                }
                bufferedOutputStream.write(bArr, 0, read);
                i = read + i;
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    bufferedOutputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                try {
                    bufferedInputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    bufferedOutputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                try {
                    bufferedInputStream.close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
                throw th;
            }
        }
        bufferedOutputStream.flush();
        try {
            bufferedOutputStream.close();
        } catch (IOException e7) {
            e7.printStackTrace();
        }
        try {
            bufferedInputStream.close();
        } catch (IOException e8) {
            e8.printStackTrace();
        }
        return i;
    }

    /* renamed from: f.a$a  reason: collision with other inner class name */
    /* compiled from: DownLoaderTask */
    private final class C0099a extends FileOutputStream {
        public C0099a(File file) {
            super(file);
        }

        public void write(byte[] bArr, int i, int i2) {
            super.write(bArr, i, i2);
        }
    }
}
