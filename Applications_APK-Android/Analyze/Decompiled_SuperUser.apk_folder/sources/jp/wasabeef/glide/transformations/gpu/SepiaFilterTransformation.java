package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import jp.co.cyberagent.android.gpuimage.GPUImageSepiaFilter;

public class SepiaFilterTransformation extends a {

    /* renamed from: a  reason: collision with root package name */
    private float f7630a;

    public SepiaFilterTransformation(Context context) {
        this(context, e.a(context).a());
    }

    public SepiaFilterTransformation(Context context, c cVar) {
        this(context, cVar, 1.0f);
    }

    public SepiaFilterTransformation(Context context, c cVar, float f2) {
        super(context, cVar, new GPUImageSepiaFilter());
        this.f7630a = f2;
        ((GPUImageSepiaFilter) b()).setIntensity(this.f7630a);
    }

    public String a() {
        return "SepiaFilterTransformation(intensity=" + this.f7630a + ")";
    }
}
