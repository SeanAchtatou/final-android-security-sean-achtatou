package jp.wasabeef.glide.transformations;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.f;

public class CropTransformation implements f<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private c f7616a;

    /* renamed from: b  reason: collision with root package name */
    private int f7617b;

    /* renamed from: c  reason: collision with root package name */
    private int f7618c;

    /* renamed from: d  reason: collision with root package name */
    private CropType f7619d;

    public enum CropType {
        TOP,
        CENTER,
        BOTTOM
    }

    public CropTransformation(Context context) {
        this(e.a(context).a());
    }

    public CropTransformation(c cVar) {
        this(cVar, 0, 0);
    }

    public CropTransformation(c cVar, int i, int i2) {
        this(cVar, i, i2, CropType.CENTER);
    }

    public CropTransformation(c cVar, int i, int i2, CropType cropType) {
        this.f7619d = CropType.CENTER;
        this.f7616a = cVar;
        this.f7617b = i;
        this.f7618c = i2;
        this.f7619d = cropType;
    }

    public i<Bitmap> a(i<Bitmap> iVar, int i, int i2) {
        Bitmap bitmap;
        Bitmap b2 = iVar.b();
        this.f7617b = this.f7617b == 0 ? b2.getWidth() : this.f7617b;
        this.f7618c = this.f7618c == 0 ? b2.getHeight() : this.f7618c;
        Bitmap.Config config = b2.getConfig() != null ? b2.getConfig() : Bitmap.Config.ARGB_8888;
        Bitmap a2 = this.f7616a.a(this.f7617b, this.f7618c, config);
        if (a2 == null) {
            bitmap = Bitmap.createBitmap(this.f7617b, this.f7618c, config);
        } else {
            bitmap = a2;
        }
        float max = Math.max(((float) this.f7617b) / ((float) b2.getWidth()), ((float) this.f7618c) / ((float) b2.getHeight()));
        float width = ((float) b2.getWidth()) * max;
        float height = max * ((float) b2.getHeight());
        float f2 = (((float) this.f7617b) - width) / 2.0f;
        float a3 = a(height);
        new Canvas(bitmap).drawBitmap(b2, (Rect) null, new RectF(f2, a3, width + f2, height + a3), (Paint) null);
        return com.bumptech.glide.load.resource.bitmap.c.a(bitmap, this.f7616a);
    }

    public String a() {
        return "CropTransformation(width=" + this.f7617b + ", height=" + this.f7618c + ", cropType=" + this.f7619d + ")";
    }

    private float a(float f2) {
        switch (this.f7619d) {
            case TOP:
            default:
                return 0.0f;
            case CENTER:
                return (((float) this.f7618c) - f2) / 2.0f;
            case BOTTOM:
                return ((float) this.f7618c) - f2;
        }
    }
}
