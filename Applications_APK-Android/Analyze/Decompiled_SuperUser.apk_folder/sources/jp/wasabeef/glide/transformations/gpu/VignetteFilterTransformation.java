package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import android.graphics.PointF;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import java.util.Arrays;
import jp.co.cyberagent.android.gpuimage.GPUImageVignetteFilter;

public class VignetteFilterTransformation extends a {

    /* renamed from: a  reason: collision with root package name */
    private PointF f7636a;

    /* renamed from: b  reason: collision with root package name */
    private float[] f7637b;

    /* renamed from: c  reason: collision with root package name */
    private float f7638c;

    /* renamed from: d  reason: collision with root package name */
    private float f7639d;

    public VignetteFilterTransformation(Context context) {
        this(context, e.a(context).a());
    }

    public VignetteFilterTransformation(Context context, c cVar) {
        this(context, cVar, new PointF(0.5f, 0.5f), new float[]{0.0f, 0.0f, 0.0f}, 0.0f, 0.75f);
    }

    public VignetteFilterTransformation(Context context, c cVar, PointF pointF, float[] fArr, float f2, float f3) {
        super(context, cVar, new GPUImageVignetteFilter());
        this.f7636a = pointF;
        this.f7637b = fArr;
        this.f7638c = f2;
        this.f7639d = f3;
        GPUImageVignetteFilter gPUImageVignetteFilter = (GPUImageVignetteFilter) b();
        gPUImageVignetteFilter.setVignetteCenter(this.f7636a);
        gPUImageVignetteFilter.setVignetteColor(this.f7637b);
        gPUImageVignetteFilter.setVignetteStart(this.f7638c);
        gPUImageVignetteFilter.setVignetteEnd(this.f7639d);
    }

    public String a() {
        return "VignetteFilterTransformation(center=" + this.f7636a.toString() + ",color=" + Arrays.toString(this.f7637b) + ",start=" + this.f7638c + ",end=" + this.f7639d + ")";
    }
}
