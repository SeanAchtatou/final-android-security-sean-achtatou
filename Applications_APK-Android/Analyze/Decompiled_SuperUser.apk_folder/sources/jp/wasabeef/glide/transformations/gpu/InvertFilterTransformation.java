package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import jp.co.cyberagent.android.gpuimage.GPUImageColorInvertFilter;

public class InvertFilterTransformation extends a {
    public InvertFilterTransformation(Context context) {
        this(context, e.a(context).a());
    }

    public InvertFilterTransformation(Context context, c cVar) {
        super(context, cVar, new GPUImageColorInvertFilter());
    }

    public String a() {
        return "InvertFilterTransformation()";
    }
}
