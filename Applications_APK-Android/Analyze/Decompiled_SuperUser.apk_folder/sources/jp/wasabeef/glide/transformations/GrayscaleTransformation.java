package jp.wasabeef.glide.transformations;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.f;

public class GrayscaleTransformation implements f<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private c f7625a;

    public GrayscaleTransformation(Context context) {
        this(e.a(context).a());
    }

    public GrayscaleTransformation(c cVar) {
        this.f7625a = cVar;
    }

    public i<Bitmap> a(i<Bitmap> iVar, int i, int i2) {
        Bitmap bitmap;
        Bitmap b2 = iVar.b();
        int width = b2.getWidth();
        int height = b2.getHeight();
        Bitmap.Config config = b2.getConfig() != null ? b2.getConfig() : Bitmap.Config.ARGB_8888;
        Bitmap a2 = this.f7625a.a(width, height, config);
        if (a2 == null) {
            bitmap = Bitmap.createBitmap(width, height, config);
        } else {
            bitmap = a2;
        }
        Canvas canvas = new Canvas(bitmap);
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0.0f);
        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(b2, 0.0f, 0.0f, paint);
        return com.bumptech.glide.load.resource.bitmap.c.a(bitmap, this.f7625a);
    }

    public String a() {
        return "GrayscaleTransformation()";
    }
}
