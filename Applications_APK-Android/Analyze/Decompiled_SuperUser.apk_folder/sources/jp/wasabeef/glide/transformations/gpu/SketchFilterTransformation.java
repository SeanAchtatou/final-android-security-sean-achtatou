package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import jp.co.cyberagent.android.gpuimage.GPUImageSketchFilter;

public class SketchFilterTransformation extends a {
    public SketchFilterTransformation(Context context) {
        this(context, e.a(context).a());
    }

    public SketchFilterTransformation(Context context, c cVar) {
        super(context, cVar, new GPUImageSketchFilter());
    }

    public String a() {
        return "SketchFilterTransformation()";
    }
}
