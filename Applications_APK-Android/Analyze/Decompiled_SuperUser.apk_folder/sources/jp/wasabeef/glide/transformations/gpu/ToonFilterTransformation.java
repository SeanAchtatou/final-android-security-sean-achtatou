package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import jp.co.cyberagent.android.gpuimage.GPUImageToonFilter;

public class ToonFilterTransformation extends a {

    /* renamed from: a  reason: collision with root package name */
    private float f7634a;

    /* renamed from: b  reason: collision with root package name */
    private float f7635b;

    public ToonFilterTransformation(Context context) {
        this(context, e.a(context).a());
    }

    public ToonFilterTransformation(Context context, c cVar) {
        this(context, cVar, 0.2f, 10.0f);
    }

    public ToonFilterTransformation(Context context, c cVar, float f2, float f3) {
        super(context, cVar, new GPUImageToonFilter());
        this.f7634a = f2;
        this.f7635b = f3;
        GPUImageToonFilter gPUImageToonFilter = (GPUImageToonFilter) b();
        gPUImageToonFilter.setThreshold(this.f7634a);
        gPUImageToonFilter.setQuantizationLevels(this.f7635b);
    }

    public String a() {
        return "ToonFilterTransformation(threshold=" + this.f7634a + ",quantizationLevels=" + this.f7635b + ")";
    }
}
