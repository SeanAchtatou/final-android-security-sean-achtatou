package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import jp.co.cyberagent.android.gpuimage.GPUImagePixelationFilter;

public class PixelationFilterTransformation extends a {

    /* renamed from: a  reason: collision with root package name */
    private float f7629a;

    public PixelationFilterTransformation(Context context) {
        this(context, e.a(context).a());
    }

    public PixelationFilterTransformation(Context context, c cVar) {
        this(context, cVar, 10.0f);
    }

    public PixelationFilterTransformation(Context context, c cVar, float f2) {
        super(context, cVar, new GPUImagePixelationFilter());
        this.f7629a = f2;
        ((GPUImagePixelationFilter) b()).setPixel(this.f7629a);
    }

    public String a() {
        return "PixelationFilterTransformation(pixel=" + this.f7629a + ")";
    }
}
