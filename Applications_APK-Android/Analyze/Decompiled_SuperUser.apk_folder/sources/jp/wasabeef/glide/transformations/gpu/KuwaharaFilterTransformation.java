package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import jp.co.cyberagent.android.gpuimage.GPUImageKuwaharaFilter;

public class KuwaharaFilterTransformation extends a {

    /* renamed from: a  reason: collision with root package name */
    private int f7628a;

    public KuwaharaFilterTransformation(Context context) {
        this(context, e.a(context).a());
    }

    public KuwaharaFilterTransformation(Context context, c cVar) {
        this(context, cVar, 25);
    }

    public KuwaharaFilterTransformation(Context context, c cVar, int i) {
        super(context, cVar, new GPUImageKuwaharaFilter());
        this.f7628a = i;
        ((GPUImageKuwaharaFilter) b()).setRadius(this.f7628a);
    }

    public String a() {
        return "KuwaharaFilterTransformation(radius=" + this.f7628a + ")";
    }
}
