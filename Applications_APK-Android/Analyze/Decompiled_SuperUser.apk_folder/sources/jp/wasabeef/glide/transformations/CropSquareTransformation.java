package jp.wasabeef.glide.transformations;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.f;

public class CropSquareTransformation implements f<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private c f7613a;

    /* renamed from: b  reason: collision with root package name */
    private int f7614b;

    /* renamed from: c  reason: collision with root package name */
    private int f7615c;

    public CropSquareTransformation(Context context) {
        this(e.a(context).a());
    }

    public CropSquareTransformation(c cVar) {
        this.f7613a = cVar;
    }

    public i<Bitmap> a(i<Bitmap> iVar, int i, int i2) {
        Bitmap bitmap;
        Bitmap b2 = iVar.b();
        int min = Math.min(b2.getWidth(), b2.getHeight());
        this.f7614b = (b2.getWidth() - min) / 2;
        this.f7615c = (b2.getHeight() - min) / 2;
        Bitmap a2 = this.f7613a.a(this.f7614b, this.f7615c, b2.getConfig() != null ? b2.getConfig() : Bitmap.Config.ARGB_8888);
        if (a2 == null) {
            bitmap = Bitmap.createBitmap(b2, this.f7614b, this.f7615c, min, min);
        } else {
            bitmap = a2;
        }
        return com.bumptech.glide.load.resource.bitmap.c.a(bitmap, this.f7613a);
    }

    public String a() {
        return "CropSquareTransformation(width=" + this.f7614b + ", height=" + this.f7615c + ")";
    }
}
