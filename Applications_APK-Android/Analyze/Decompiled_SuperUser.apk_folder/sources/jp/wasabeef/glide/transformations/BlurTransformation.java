package jp.wasabeef.glide.transformations;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.renderscript.RSRuntimeException;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.f;
import jp.wasabeef.glide.transformations.a.a;
import jp.wasabeef.glide.transformations.a.b;

public class BlurTransformation implements f<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private static int f7606a = 25;

    /* renamed from: b  reason: collision with root package name */
    private static int f7607b = 1;

    /* renamed from: c  reason: collision with root package name */
    private Context f7608c;

    /* renamed from: d  reason: collision with root package name */
    private c f7609d;

    /* renamed from: e  reason: collision with root package name */
    private int f7610e;

    /* renamed from: f  reason: collision with root package name */
    private int f7611f;

    public BlurTransformation(Context context) {
        this(context, e.a(context).a(), f7606a, f7607b);
    }

    public BlurTransformation(Context context, c cVar, int i, int i2) {
        this.f7608c = context.getApplicationContext();
        this.f7609d = cVar;
        this.f7610e = i;
        this.f7611f = i2;
    }

    public i<Bitmap> a(i<Bitmap> iVar, int i, int i2) {
        Bitmap a2;
        Bitmap b2 = iVar.b();
        int width = b2.getWidth();
        int height = b2.getHeight();
        int i3 = width / this.f7611f;
        int i4 = height / this.f7611f;
        Bitmap a3 = this.f7609d.a(i3, i4, Bitmap.Config.ARGB_8888);
        if (a3 == null) {
            a3 = Bitmap.createBitmap(i3, i4, Bitmap.Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(a3);
        canvas.scale(1.0f / ((float) this.f7611f), 1.0f / ((float) this.f7611f));
        Paint paint = new Paint();
        paint.setFlags(2);
        canvas.drawBitmap(b2, 0.0f, 0.0f, paint);
        if (Build.VERSION.SDK_INT >= 18) {
            try {
                a2 = b.a(this.f7608c, a3, this.f7610e);
            } catch (RSRuntimeException e2) {
                a2 = a.a(a3, this.f7610e, true);
            }
        } else {
            a2 = a.a(a3, this.f7610e, true);
        }
        return com.bumptech.glide.load.resource.bitmap.c.a(a2, this.f7609d);
    }

    public String a() {
        return "BlurTransformation(radius=" + this.f7610e + ", sampling=" + this.f7611f + ")";
    }
}
