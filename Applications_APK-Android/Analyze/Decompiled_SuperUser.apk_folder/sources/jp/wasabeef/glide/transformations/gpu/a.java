package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.f;
import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

/* compiled from: GPUFilterTransformation */
public class a implements f<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private Context f7640a;

    /* renamed from: b  reason: collision with root package name */
    private c f7641b;

    /* renamed from: c  reason: collision with root package name */
    private GPUImageFilter f7642c;

    public a(Context context, c cVar, GPUImageFilter gPUImageFilter) {
        this.f7640a = context.getApplicationContext();
        this.f7641b = cVar;
        this.f7642c = gPUImageFilter;
    }

    public i<Bitmap> a(i<Bitmap> iVar, int i, int i2) {
        GPUImage gPUImage = new GPUImage(this.f7640a);
        gPUImage.setImage(iVar.b());
        gPUImage.setFilter(this.f7642c);
        return com.bumptech.glide.load.resource.bitmap.c.a(gPUImage.getBitmapWithFilterApplied(), this.f7641b);
    }

    public String a() {
        return getClass().getSimpleName();
    }

    public <T> T b() {
        return this.f7642c;
    }
}
