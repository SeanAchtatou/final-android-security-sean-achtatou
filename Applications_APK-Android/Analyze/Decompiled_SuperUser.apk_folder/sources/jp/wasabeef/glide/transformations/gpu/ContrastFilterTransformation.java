package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;

public class ContrastFilterTransformation extends a {

    /* renamed from: a  reason: collision with root package name */
    private float f7627a;

    public ContrastFilterTransformation(Context context) {
        this(context, e.a(context).a());
    }

    public ContrastFilterTransformation(Context context, c cVar) {
        this(context, cVar, 1.0f);
    }

    public ContrastFilterTransformation(Context context, c cVar, float f2) {
        super(context, cVar, new GPUImageContrastFilter());
        this.f7627a = f2;
        ((GPUImageContrastFilter) b()).setContrast(this.f7627a);
    }

    public String a() {
        return "ContrastFilterTransformation(contrast=" + this.f7627a + ")";
    }
}
