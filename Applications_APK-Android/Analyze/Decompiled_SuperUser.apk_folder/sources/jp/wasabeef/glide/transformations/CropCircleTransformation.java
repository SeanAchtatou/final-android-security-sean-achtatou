package jp.wasabeef.glide.transformations;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import com.bumptech.glide.load.engine.i;
import com.bumptech.glide.load.f;

public class CropCircleTransformation implements f<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private c f7612a;

    public CropCircleTransformation(Context context) {
        this(e.a(context).a());
    }

    public CropCircleTransformation(c cVar) {
        this.f7612a = cVar;
    }

    public i<Bitmap> a(i<Bitmap> iVar, int i, int i2) {
        Bitmap b2 = iVar.b();
        int min = Math.min(b2.getWidth(), b2.getHeight());
        int width = (b2.getWidth() - min) / 2;
        int height = (b2.getHeight() - min) / 2;
        Bitmap a2 = this.f7612a.a(min, min, Bitmap.Config.ARGB_8888);
        if (a2 == null) {
            a2 = Bitmap.createBitmap(min, min, Bitmap.Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(a2);
        Paint paint = new Paint();
        BitmapShader bitmapShader = new BitmapShader(b2, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        if (!(width == 0 && height == 0)) {
            Matrix matrix = new Matrix();
            matrix.setTranslate((float) (-width), (float) (-height));
            bitmapShader.setLocalMatrix(matrix);
        }
        paint.setShader(bitmapShader);
        paint.setAntiAlias(true);
        float f2 = ((float) min) / 2.0f;
        canvas.drawCircle(f2, f2, f2, paint);
        return com.bumptech.glide.load.resource.bitmap.c.a(a2, this.f7612a);
    }

    public String a() {
        return "CropCircleTransformation()";
    }
}
