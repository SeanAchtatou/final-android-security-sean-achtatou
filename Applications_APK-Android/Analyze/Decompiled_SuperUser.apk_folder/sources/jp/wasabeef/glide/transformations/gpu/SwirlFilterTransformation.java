package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import android.graphics.PointF;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import jp.co.cyberagent.android.gpuimage.GPUImageSwirlFilter;

public class SwirlFilterTransformation extends a {

    /* renamed from: a  reason: collision with root package name */
    private float f7631a;

    /* renamed from: b  reason: collision with root package name */
    private float f7632b;

    /* renamed from: c  reason: collision with root package name */
    private PointF f7633c;

    public SwirlFilterTransformation(Context context) {
        this(context, e.a(context).a());
    }

    public SwirlFilterTransformation(Context context, c cVar) {
        this(context, cVar, 0.5f, 1.0f, new PointF(0.5f, 0.5f));
    }

    public SwirlFilterTransformation(Context context, c cVar, float f2, float f3, PointF pointF) {
        super(context, cVar, new GPUImageSwirlFilter());
        this.f7631a = f2;
        this.f7632b = f3;
        this.f7633c = pointF;
        GPUImageSwirlFilter gPUImageSwirlFilter = (GPUImageSwirlFilter) b();
        gPUImageSwirlFilter.setRadius(this.f7631a);
        gPUImageSwirlFilter.setAngle(this.f7632b);
        gPUImageSwirlFilter.setCenter(this.f7633c);
    }

    public String a() {
        return "SwirlFilterTransformation(radius=" + this.f7631a + ",angle=" + this.f7632b + ",center=" + this.f7633c.toString() + ")";
    }
}
