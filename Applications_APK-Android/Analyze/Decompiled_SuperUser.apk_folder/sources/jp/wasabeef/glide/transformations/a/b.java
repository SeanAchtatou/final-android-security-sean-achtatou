package jp.wasabeef.glide.transformations.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

/* compiled from: RSBlur */
public class b {
    @TargetApi(18)
    public static Bitmap a(Context context, Bitmap bitmap, int i) {
        RenderScript renderScript;
        Allocation allocation;
        Allocation allocation2;
        ScriptIntrinsicBlur scriptIntrinsicBlur = null;
        try {
            renderScript = RenderScript.create(context);
            try {
                renderScript.setMessageHandler(new RenderScript.RSMessageHandler());
                allocation = Allocation.createFromBitmap(renderScript, bitmap, Allocation.MipmapControl.MIPMAP_NONE, 1);
                try {
                    allocation2 = Allocation.createTyped(renderScript, allocation.getType());
                    try {
                        scriptIntrinsicBlur = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
                        scriptIntrinsicBlur.setInput(allocation);
                        scriptIntrinsicBlur.setRadius((float) i);
                        scriptIntrinsicBlur.forEach(allocation2);
                        allocation2.copyTo(bitmap);
                        if (renderScript != null) {
                            renderScript.destroy();
                        }
                        if (allocation != null) {
                            allocation.destroy();
                        }
                        if (allocation2 != null) {
                            allocation2.destroy();
                        }
                        if (scriptIntrinsicBlur != null) {
                            scriptIntrinsicBlur.destroy();
                        }
                        return bitmap;
                    } catch (Throwable th) {
                        th = th;
                        if (renderScript != null) {
                            renderScript.destroy();
                        }
                        if (allocation != null) {
                            allocation.destroy();
                        }
                        if (allocation2 != null) {
                            allocation2.destroy();
                        }
                        if (scriptIntrinsicBlur != null) {
                            scriptIntrinsicBlur.destroy();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    allocation2 = null;
                }
            } catch (Throwable th3) {
                th = th3;
                allocation2 = null;
                allocation = null;
            }
        } catch (Throwable th4) {
            th = th4;
            allocation2 = null;
            allocation = null;
            renderScript = null;
        }
    }
}
