package jp.wasabeef.glide.transformations.gpu;

import android.content.Context;
import com.bumptech.glide.e;
import com.bumptech.glide.load.engine.a.c;
import jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter;

public class BrightnessFilterTransformation extends a {

    /* renamed from: a  reason: collision with root package name */
    private float f7626a;

    public BrightnessFilterTransformation(Context context) {
        this(context, e.a(context).a());
    }

    public BrightnessFilterTransformation(Context context, c cVar) {
        this(context, cVar, 0.0f);
    }

    public BrightnessFilterTransformation(Context context, c cVar, float f2) {
        super(context, cVar, new GPUImageBrightnessFilter());
        this.f7626a = f2;
        ((GPUImageBrightnessFilter) b()).setBrightness(this.f7626a);
    }

    public String a() {
        return "BrightnessFilterTransformation(brightness=" + this.f7626a + ")";
    }
}
