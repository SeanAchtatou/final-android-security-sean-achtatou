package e;

import android.content.Context;
import com.daps.weather.base.SharedPrefsUtils;
import com.daps.weather.base.d;
import com.daps.weather.base.e;
import com.duapps.ad.stats.ToolStatsCore;
import com.google.android.gms.analytics.ecommerce.Promotion;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import org.json.JSONException;
import org.json.JSONStringer;

/* compiled from: ReportData */
public class a {

    /* renamed from: e.a$a  reason: collision with other inner class name */
    /* compiled from: ReportData */
    public enum C0097a {
        VIEW,
        NOTIFICATION,
        LANDING_PAGE,
        ENTER_IV,
        SUSPENSION
    }

    public static void a(Context context, C0097a aVar) {
        d.a("AdStats", "report : tas");
        try {
            ToolStatsCore.getInstance(context.getApplicationContext()).reportEvent("behavior", new JSONStringer().object().key("weather").value("tas").key("tnetwork").value(a(context)).key("tpage").value(a(aVar)).key("tlct").value(SharedPrefsUtils.g(context)).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
        } catch (JSONException e2) {
            d.b("AdStats", "reportShowEvent失败了:" + e2.getMessage());
        }
    }

    public static void a(Context context, C0097a aVar, int i) {
        d.a("AdStats", "report : tis");
        try {
            ToolStatsCore.getInstance(context.getApplicationContext()).reportEvent("behavior", new JSONStringer().object().key("weather").value("tis").key("tnetwork").value(a(context)).key("tpage").value(a(aVar)).key("tlct").value(SharedPrefsUtils.g(context)).key("ts").value(System.currentTimeMillis()).key("tdsc").value((long) i).endObject().toString(), 1);
        } catch (JSONException e2) {
            d.b("AdStats", "reportIconShowEvent失败了:" + e2.getMessage());
        }
    }

    public static void b(Context context, C0097a aVar) {
        d.a("AdStats", "report : tic");
        try {
            ToolStatsCore.getInstance(context.getApplicationContext()).reportEvent("behavior", new JSONStringer().object().key("weather").value("tic").key("tnetwork").value(a(context)).key("tpage").value(a(aVar)).key("tlct").value(SharedPrefsUtils.g(context)).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
        } catch (JSONException e2) {
            d.b("AdStats", "reportIconShowEvent失败了:" + e2.getMessage());
        }
    }

    public static void c(Context context, C0097a aVar) {
        d.a("AdStats", "report : tsuc");
        try {
            ToolStatsCore.getInstance(context.getApplicationContext()).reportEvent("behavior", new JSONStringer().object().key("weather").value("tsuc").key("tnetwork").value(a(context)).key("tpage").value(a(aVar)).key("tlct").value(SharedPrefsUtils.g(context)).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
        } catch (JSONException e2) {
            d.b("AdStats", "reportSuspensionClickEvent:" + e2.getMessage());
        }
    }

    public static void a(Context context, C0097a aVar, String str) {
        d.a("AdStats", "report : tiser");
        try {
            ToolStatsCore.getInstance(context.getApplicationContext()).reportEvent("behavior", new JSONStringer().object().key("weather").value("tiser").key("tnetwork").value(a(context)).key("tpage").value(a(aVar)).key("tqer").value(str).key("tlct").value(SharedPrefsUtils.g(context)).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
        } catch (JSONException e2) {
            d.b("AdStats", "reportIconShowEvent失败了:" + e2.getMessage());
        }
    }

    public static void a(Context context, String str) {
        d.a("AdStats", "report : tpush");
        try {
            ToolStatsCore.getInstance(context.getApplicationContext()).reportEvent("behavior", new JSONStringer().object().key("weather").value("tpush").key("tpushtype").value(str).key("tnetwork").value(a(context)).key("tpage").value(ServiceManagerNative.NOTIFICATION).key("tlct").value(SharedPrefsUtils.g(context)).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
        } catch (JSONException e2) {
            d.b("AdStats", "reportIconShowEvent失败了:" + e2.getMessage());
        }
    }

    public static void a(Context context, String str, String str2) {
        d.a("AdStats", "report : tpush");
        try {
            ToolStatsCore.getInstance(context.getApplicationContext()).reportEvent("behavior", new JSONStringer().object().key("weather").value("tpush").key("tpushtype").value(str).key("tnetwork").value(a(context)).key("tpage").value(ServiceManagerNative.NOTIFICATION).key("tlift").value(str2).key("tlct").value(SharedPrefsUtils.g(context)).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
        } catch (JSONException e2) {
            d.b("AdStats", "reportIconShowEvent失败了:" + e2.getMessage());
        }
    }

    public static void b(Context context, String str) {
        d.a("AdStats", "report : tnc");
        try {
            ToolStatsCore.getInstance(context.getApplicationContext()).reportEvent("behavior", new JSONStringer().object().key("weather").value("tnc").key("tpushtype").value(str).key("ts").value(System.currentTimeMillis()).endObject().toString(), 1);
        } catch (JSONException e2) {
            d.b("AdStats", "reportIconShowEvent失败了:" + e2.getMessage());
        }
    }

    public static String a(C0097a aVar) {
        switch (aVar) {
            case VIEW:
                return Promotion.ACTION_VIEW;
            case NOTIFICATION:
                return ServiceManagerNative.NOTIFICATION;
            case LANDING_PAGE:
                return "landpage";
            case ENTER_IV:
                return "enteriv";
            case SUSPENSION:
                return "suspension";
            default:
                return "other";
        }
    }

    private static String a(Context context) {
        if (e.a(context)) {
            return "1";
        }
        return "0";
    }
}
