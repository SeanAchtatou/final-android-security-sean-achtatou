package c;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.daps.weather.base.e;
import java.util.List;

/* compiled from: ForecastAdapter */
public class a extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f2309a;

    /* renamed from: b  reason: collision with root package name */
    private List f2310b;

    /* renamed from: c  reason: collision with root package name */
    private LayoutInflater f2311c;

    /* renamed from: d  reason: collision with root package name */
    private int f2312d;

    public a(Context context, List list) {
        this.f2309a = context;
        this.f2310b = list;
        this.f2311c = (LayoutInflater) context.getSystemService("layout_inflater");
        this.f2312d = e.b(context);
    }

    public int getCount() {
        return this.f2310b.size();
    }

    public Object getItem(int i) {
        return this.f2310b.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0068, code lost:
        r0 = (com.daps.weather.bean.forecasts.ForecastsDailyForecasts) r6.f2310b.get(r7);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r7, android.view.View r8, android.view.ViewGroup r9) {
        /*
            r6 = this;
            if (r8 != 0) goto L_0x0107
            c.a$a r1 = new c.a$a
            r1.<init>()
            android.view.LayoutInflater r0 = r6.f2311c
            int r2 = com.daps.weather.a.e.ay
            r3 = 0
            android.view.View r8 = r0.inflate(r2, r3)
            int r0 = com.daps.weather.a.d.hi
            android.view.View r0 = r8.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r1.f2315a = r0
            int r0 = com.daps.weather.a.d.hl
            android.view.View r0 = r8.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r1.f2316b = r0
            int r0 = com.daps.weather.a.d.hj
            android.view.View r0 = r8.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r1.f2317c = r0
            int r0 = com.daps.weather.a.d.hk
            android.view.View r0 = r8.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r1.f2318d = r0
            int r0 = com.daps.weather.a.d.hm
            android.view.View r0 = r8.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r1.f2319e = r0
            int r0 = com.daps.weather.a.d.hh
            android.view.View r0 = r8.findViewById(r0)
            android.widget.RelativeLayout r0 = (android.widget.RelativeLayout) r0
            r1.f2320f = r0
            r8.setTag(r1)
            int r0 = r6.f2312d
            int r0 = r0 / 6
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams
            r3 = -1
            r2.<init>(r0, r3)
            r8.setLayoutParams(r2)
        L_0x005c:
            java.util.List r0 = r6.f2310b
            if (r0 == 0) goto L_0x0106
            java.util.List r0 = r6.f2310b
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0106
            java.util.List r0 = r6.f2310b
            java.lang.Object r0 = r0.get(r7)
            com.daps.weather.bean.forecasts.ForecastsDailyForecasts r0 = (com.daps.weather.bean.forecasts.ForecastsDailyForecasts) r0
            if (r0 == 0) goto L_0x0106
            com.daps.weather.bean.forecasts.ForecastsDailyForecastsDay r2 = r0.getDay()
            int r2 = r2.getIcon()
            android.content.Context r3 = r6.f2309a
            android.graphics.Bitmap r2 = com.daps.weather.base.e.a(r3, r2)
            if (r2 == 0) goto L_0x0087
            android.widget.ImageView r3 = r1.f2318d
            r3.setImageBitmap(r2)
        L_0x0087:
            com.daps.weather.bean.forecasts.ForecastsDailyForecastsTemperature r2 = r0.getTemperature()
            com.daps.weather.bean.forecasts.ForecastsDailyForecastsTemperatureMaximum r2 = r2.getMaximum()
            double r2 = r2.getValue()
            int r2 = com.daps.weather.base.e.a(r2)
            com.daps.weather.bean.forecasts.ForecastsDailyForecastsTemperature r3 = r0.getTemperature()
            com.daps.weather.bean.forecasts.ForecastsDailyForecastsTemperatureMinimum r3 = r3.getMinimum()
            double r4 = r3.getValue()
            int r3 = com.daps.weather.base.e.a(r4)
            android.widget.TextView r4 = r1.f2316b
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r5 = "°/"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "°"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r4.setText(r2)
            int r2 = r0.getEpochDate()
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r2 = com.daps.weather.base.e.b(r2)
            android.widget.TextView r3 = r1.f2317c
            r3.setText(r2)
            int r2 = r0.getEpochDate()     // Catch:{ Exception -> 0x0110 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x0110 }
            java.lang.String r2 = com.daps.weather.base.e.e(r2)     // Catch:{ Exception -> 0x0110 }
            android.widget.TextView r3 = r1.f2315a     // Catch:{ Exception -> 0x0110 }
            r3.setText(r2)     // Catch:{ Exception -> 0x0110 }
        L_0x00eb:
            java.util.List r2 = r6.f2310b
            int r2 = r2.size()
            int r2 = r2 + -1
            if (r7 != r2) goto L_0x0115
            android.widget.ImageView r2 = r1.f2319e
            r3 = 8
            r2.setVisibility(r3)
        L_0x00fc:
            android.widget.RelativeLayout r1 = r1.f2320f
            c.a$1 r2 = new c.a$1
            r2.<init>(r0)
            r1.setOnClickListener(r2)
        L_0x0106:
            return r8
        L_0x0107:
            java.lang.Object r0 = r8.getTag()
            c.a$a r0 = (c.a.C0032a) r0
            r1 = r0
            goto L_0x005c
        L_0x0110:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00eb
        L_0x0115:
            android.widget.ImageView r2 = r1.f2319e
            r3 = 0
            r2.setVisibility(r3)
            goto L_0x00fc
        */
        throw new UnsupportedOperationException("Method not decompiled: c.a.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    /* renamed from: c.a$a  reason: collision with other inner class name */
    /* compiled from: ForecastAdapter */
    public final class C0032a {

        /* renamed from: a  reason: collision with root package name */
        public TextView f2315a;

        /* renamed from: b  reason: collision with root package name */
        public TextView f2316b;

        /* renamed from: c  reason: collision with root package name */
        public TextView f2317c;

        /* renamed from: d  reason: collision with root package name */
        public ImageView f2318d;

        /* renamed from: e  reason: collision with root package name */
        public ImageView f2319e;

        /* renamed from: f  reason: collision with root package name */
        public RelativeLayout f2320f;

        public C0032a() {
        }
    }
}
