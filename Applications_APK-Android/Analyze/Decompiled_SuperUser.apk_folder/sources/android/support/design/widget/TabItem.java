package android.support.design.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.design.R;
import android.support.v7.widget.an;
import android.util.AttributeSet;
import android.view.View;

public final class TabItem extends View {
    final int mCustomLayout;
    final Drawable mIcon;
    final CharSequence mText;

    public TabItem(Context context) {
        this(context, null);
    }

    public TabItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        an a2 = an.a(context, attributeSet, R.styleable.TabItem);
        this.mText = a2.c(R.styleable.TabItem_android_text);
        this.mIcon = a2.a(R.styleable.TabItem_android_icon);
        this.mCustomLayout = a2.g(R.styleable.TabItem_android_layout, 0);
        a2.a();
    }
}
