package android.support.design.widget;

import android.content.Context;
import android.support.v4.view.ag;
import android.support.v4.view.s;
import android.support.v4.widget.u;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;

abstract class HeaderBehavior<V extends View> extends ViewOffsetBehavior<V> {
    private static final int INVALID_POINTER = -1;
    private int mActivePointerId = -1;
    private Runnable mFlingRunnable;
    private boolean mIsBeingDragged;
    private int mLastMotionY;
    u mScroller;
    private int mTouchSlop = -1;
    private VelocityTracker mVelocityTracker;

    public HeaderBehavior() {
    }

    public HeaderBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        int findPointerIndex;
        if (this.mTouchSlop < 0) {
            this.mTouchSlop = ViewConfiguration.get(coordinatorLayout.getContext()).getScaledTouchSlop();
        }
        if (motionEvent.getAction() == 2 && this.mIsBeingDragged) {
            return true;
        }
        switch (s.a(motionEvent)) {
            case 0:
                this.mIsBeingDragged = false;
                int x = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();
                if (canDragView(v) && coordinatorLayout.isPointInChildBounds(v, x, y)) {
                    this.mLastMotionY = y;
                    this.mActivePointerId = motionEvent.getPointerId(0);
                    ensureVelocityTracker();
                    break;
                }
            case 1:
            case 3:
                this.mIsBeingDragged = false;
                this.mActivePointerId = -1;
                if (this.mVelocityTracker != null) {
                    this.mVelocityTracker.recycle();
                    this.mVelocityTracker = null;
                    break;
                }
                break;
            case 2:
                int i = this.mActivePointerId;
                if (!(i == -1 || (findPointerIndex = motionEvent.findPointerIndex(i)) == -1)) {
                    int y2 = (int) motionEvent.getY(findPointerIndex);
                    if (Math.abs(y2 - this.mLastMotionY) > this.mTouchSlop) {
                        this.mIsBeingDragged = true;
                        this.mLastMotionY = y2;
                        break;
                    }
                }
                break;
        }
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.addMovement(motionEvent);
        }
        return this.mIsBeingDragged;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ad  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.support.design.widget.CoordinatorLayout r10, V r11, android.view.MotionEvent r12) {
        /*
            r9 = this;
            r7 = 1
            r8 = -1
            r5 = 0
            int r0 = r9.mTouchSlop
            if (r0 >= 0) goto L_0x0015
            android.content.Context r0 = r10.getContext()
            android.view.ViewConfiguration r0 = android.view.ViewConfiguration.get(r0)
            int r0 = r0.getScaledTouchSlop()
            r9.mTouchSlop = r0
        L_0x0015:
            int r0 = android.support.v4.view.s.a(r12)
            switch(r0) {
                case 0: goto L_0x0027;
                case 1: goto L_0x0082;
                case 2: goto L_0x0049;
                case 3: goto L_0x00a5;
                default: goto L_0x001c;
            }
        L_0x001c:
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            if (r0 == 0) goto L_0x0025
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            r0.addMovement(r12)
        L_0x0025:
            r5 = r7
        L_0x0026:
            return r5
        L_0x0027:
            float r0 = r12.getX()
            int r0 = (int) r0
            float r1 = r12.getY()
            int r1 = (int) r1
            boolean r0 = r10.isPointInChildBounds(r11, r0, r1)
            if (r0 == 0) goto L_0x0026
            boolean r0 = r9.canDragView(r11)
            if (r0 == 0) goto L_0x0026
            r9.mLastMotionY = r1
            int r0 = r12.getPointerId(r5)
            r9.mActivePointerId = r0
            r9.ensureVelocityTracker()
            goto L_0x001c
        L_0x0049:
            int r0 = r9.mActivePointerId
            int r0 = r12.findPointerIndex(r0)
            if (r0 == r8) goto L_0x0026
            float r0 = r12.getY(r0)
            int r0 = (int) r0
            int r1 = r9.mLastMotionY
            int r3 = r1 - r0
            boolean r1 = r9.mIsBeingDragged
            if (r1 != 0) goto L_0x006d
            int r1 = java.lang.Math.abs(r3)
            int r2 = r9.mTouchSlop
            if (r1 <= r2) goto L_0x006d
            r9.mIsBeingDragged = r7
            if (r3 <= 0) goto L_0x007e
            int r1 = r9.mTouchSlop
            int r3 = r3 - r1
        L_0x006d:
            boolean r1 = r9.mIsBeingDragged
            if (r1 == 0) goto L_0x001c
            r9.mLastMotionY = r0
            int r4 = r9.getMaxDragOffset(r11)
            r0 = r9
            r1 = r10
            r2 = r11
            r0.scroll(r1, r2, r3, r4, r5)
            goto L_0x001c
        L_0x007e:
            int r1 = r9.mTouchSlop
            int r3 = r3 + r1
            goto L_0x006d
        L_0x0082:
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            if (r0 == 0) goto L_0x00a5
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            r0.addMovement(r12)
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            r1 = 1000(0x3e8, float:1.401E-42)
            r0.computeCurrentVelocity(r1)
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            int r1 = r9.mActivePointerId
            float r6 = android.support.v4.view.ae.b(r0, r1)
            int r0 = r9.getScrollRangeForDragFling(r11)
            int r4 = -r0
            r1 = r9
            r2 = r10
            r3 = r11
            r1.fling(r2, r3, r4, r5, r6)
        L_0x00a5:
            r9.mIsBeingDragged = r5
            r9.mActivePointerId = r8
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            if (r0 == 0) goto L_0x001c
            android.view.VelocityTracker r0 = r9.mVelocityTracker
            r0.recycle()
            r0 = 0
            r9.mVelocityTracker = r0
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.HeaderBehavior.onTouchEvent(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: package-private */
    public int setHeaderTopBottomOffset(CoordinatorLayout coordinatorLayout, V v, int i) {
        return setHeaderTopBottomOffset(coordinatorLayout, v, i, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    /* access modifiers changed from: package-private */
    public int setHeaderTopBottomOffset(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3) {
        int constrain;
        int topAndBottomOffset = getTopAndBottomOffset();
        if (i2 == 0 || topAndBottomOffset < i2 || topAndBottomOffset > i3 || topAndBottomOffset == (constrain = MathUtils.constrain(i, i2, i3))) {
            return 0;
        }
        setTopAndBottomOffset(constrain);
        return topAndBottomOffset - constrain;
    }

    /* access modifiers changed from: package-private */
    public int getTopBottomOffsetForScrollingSibling() {
        return getTopAndBottomOffset();
    }

    /* access modifiers changed from: package-private */
    public final int scroll(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3) {
        return setHeaderTopBottomOffset(coordinatorLayout, v, getTopBottomOffsetForScrollingSibling() - i, i2, i3);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    final boolean fling(android.support.design.widget.CoordinatorLayout r10, V r11, int r12, int r13, float r14) {
        /*
            r9 = this;
            r1 = 0
            java.lang.Runnable r0 = r9.mFlingRunnable
            if (r0 == 0) goto L_0x000d
            java.lang.Runnable r0 = r9.mFlingRunnable
            r11.removeCallbacks(r0)
            r0 = 0
            r9.mFlingRunnable = r0
        L_0x000d:
            android.support.v4.widget.u r0 = r9.mScroller
            if (r0 != 0) goto L_0x001b
            android.content.Context r0 = r11.getContext()
            android.support.v4.widget.u r0 = android.support.v4.widget.u.a(r0)
            r9.mScroller = r0
        L_0x001b:
            android.support.v4.widget.u r0 = r9.mScroller
            int r2 = r9.getTopAndBottomOffset()
            int r4 = java.lang.Math.round(r14)
            r3 = r1
            r5 = r1
            r6 = r1
            r7 = r12
            r8 = r13
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8)
            android.support.v4.widget.u r0 = r9.mScroller
            boolean r0 = r0.g()
            if (r0 == 0) goto L_0x0043
            android.support.design.widget.HeaderBehavior$FlingRunnable r0 = new android.support.design.widget.HeaderBehavior$FlingRunnable
            r0.<init>(r10, r11)
            r9.mFlingRunnable = r0
            java.lang.Runnable r0 = r9.mFlingRunnable
            android.support.v4.view.ag.a(r11, r0)
            r1 = 1
        L_0x0042:
            return r1
        L_0x0043:
            r9.onFlingFinished(r10, r11)
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.HeaderBehavior.fling(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, float):boolean");
    }

    /* access modifiers changed from: package-private */
    public void onFlingFinished(CoordinatorLayout coordinatorLayout, View view) {
    }

    /* access modifiers changed from: package-private */
    public boolean canDragView(View view) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public int getMaxDragOffset(View view) {
        return -view.getHeight();
    }

    /* access modifiers changed from: package-private */
    public int getScrollRangeForDragFling(View view) {
        return view.getHeight();
    }

    private void ensureVelocityTracker() {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
    }

    private class FlingRunnable implements Runnable {
        private final V mLayout;
        private final CoordinatorLayout mParent;

        FlingRunnable(CoordinatorLayout coordinatorLayout, V v) {
            this.mParent = coordinatorLayout;
            this.mLayout = v;
        }

        public void run() {
            if (this.mLayout != null && HeaderBehavior.this.mScroller != null) {
                if (HeaderBehavior.this.mScroller.g()) {
                    HeaderBehavior.this.setHeaderTopBottomOffset(this.mParent, this.mLayout, HeaderBehavior.this.mScroller.c());
                    ag.a(this.mLayout, this);
                    return;
                }
                HeaderBehavior.this.onFlingFinished(this.mParent, this.mLayout);
            }
        }
    }
}
