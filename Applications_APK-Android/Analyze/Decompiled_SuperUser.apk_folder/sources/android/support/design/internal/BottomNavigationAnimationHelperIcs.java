package android.support.design.internal;

import android.support.transition.AutoTransition;
import android.support.transition.TransitionSet;
import android.support.transition.q;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.ViewGroup;

class BottomNavigationAnimationHelperIcs extends BottomNavigationAnimationHelperBase {
    private static final long ACTIVE_ANIMATION_DURATION_MS = 115;
    private final TransitionSet mSet = new AutoTransition();

    BottomNavigationAnimationHelperIcs() {
        this.mSet.a(0);
        this.mSet.setDuration(ACTIVE_ANIMATION_DURATION_MS);
        this.mSet.setInterpolator(new FastOutSlowInInterpolator());
        this.mSet.a(new TextScale());
    }

    /* access modifiers changed from: package-private */
    public void beginDelayedTransition(ViewGroup viewGroup) {
        q.a(viewGroup, this.mSet);
    }
}
