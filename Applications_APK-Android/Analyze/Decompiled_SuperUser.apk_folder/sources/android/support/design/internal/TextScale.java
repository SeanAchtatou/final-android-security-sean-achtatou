package android.support.design.internal;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.support.transition.Transition;
import android.support.transition.z;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.Map;

@TargetApi(14)
public class TextScale extends Transition {
    private static final String PROPNAME_SCALE = "android:textscale:scale";

    public void captureStartValues(z zVar) {
        captureValues(zVar);
    }

    public void captureEndValues(z zVar) {
        captureValues(zVar);
    }

    private void captureValues(z zVar) {
        if (zVar.f216b instanceof TextView) {
            zVar.f215a.put(PROPNAME_SCALE, Float.valueOf(((TextView) zVar.f216b).getScaleX()));
        }
    }

    public Animator createAnimator(ViewGroup viewGroup, z zVar, z zVar2) {
        float f2 = 1.0f;
        if (zVar == null || zVar2 == null || !(zVar.f216b instanceof TextView) || !(zVar2.f216b instanceof TextView)) {
            return null;
        }
        final TextView textView = (TextView) zVar2.f216b;
        Map<String, Object> map = zVar.f215a;
        Map<String, Object> map2 = zVar2.f215a;
        float floatValue = map.get(PROPNAME_SCALE) != null ? ((Float) map.get(PROPNAME_SCALE)).floatValue() : 1.0f;
        if (map2.get(PROPNAME_SCALE) != null) {
            f2 = ((Float) map2.get(PROPNAME_SCALE)).floatValue();
        }
        if (floatValue == f2) {
            return null;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(floatValue, f2);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                textView.setScaleX(floatValue);
                textView.setScaleY(floatValue);
            }
        });
        return ofFloat;
    }
}
