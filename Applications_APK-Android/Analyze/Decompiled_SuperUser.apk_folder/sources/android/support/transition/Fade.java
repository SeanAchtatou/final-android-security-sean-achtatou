package android.support.transition;

import android.animation.Animator;
import android.os.Build;
import android.view.ViewGroup;

public class Fade extends Visibility {
    public Fade(int i) {
        super(true);
        if (Build.VERSION.SDK_INT >= 19) {
            if (i > 0) {
                this.mImpl = new f(this, i);
            } else {
                this.mImpl = new f(this);
            }
        } else if (i > 0) {
            this.mImpl = new e(this, i);
        } else {
            this.mImpl = new e(this);
        }
    }

    public Fade() {
        this(-1);
    }

    public void captureEndValues(z zVar) {
        this.mImpl.b(zVar);
    }

    public void captureStartValues(z zVar) {
        this.mImpl.c(zVar);
    }

    public Animator createAnimator(ViewGroup viewGroup, z zVar, z zVar2) {
        return this.mImpl.a(viewGroup, zVar, zVar2);
    }
}
