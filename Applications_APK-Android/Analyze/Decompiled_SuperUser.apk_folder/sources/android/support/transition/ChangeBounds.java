package android.support.transition;

import android.animation.Animator;
import android.os.Build;
import android.view.ViewGroup;

public class ChangeBounds extends Transition {
    public ChangeBounds() {
        super(true);
        if (Build.VERSION.SDK_INT < 19) {
            this.mImpl = new b(this);
        } else {
            this.mImpl = new c(this);
        }
    }

    public void captureEndValues(z zVar) {
        this.mImpl.b(zVar);
    }

    public void captureStartValues(z zVar) {
        this.mImpl.c(zVar);
    }

    public Animator createAnimator(ViewGroup viewGroup, z zVar, z zVar2) {
        return this.mImpl.a(viewGroup, zVar, zVar2);
    }
}
