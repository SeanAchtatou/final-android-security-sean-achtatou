package android.support.transition;

import android.view.View;
import com.kingouser.com.util.ShellUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: TransitionValues */
public class z {

    /* renamed from: a  reason: collision with root package name */
    public final Map<String, Object> f215a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    public View f216b;

    public boolean equals(Object obj) {
        if (!(obj instanceof z) || this.f216b != ((z) obj).f216b || !this.f215a.equals(((z) obj).f215a)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.f216b.hashCode() * 31) + this.f215a.hashCode();
    }

    public String toString() {
        String str = (("TransitionValues@" + Integer.toHexString(hashCode()) + ":\n") + "    view = " + this.f216b + ShellUtils.COMMAND_LINE_END) + "    values:";
        Iterator<String> it = this.f215a.keySet().iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            String next = it.next();
            str = str2 + "    " + next + ": " + this.f215a.get(next) + ShellUtils.COMMAND_LINE_END;
        }
    }
}
