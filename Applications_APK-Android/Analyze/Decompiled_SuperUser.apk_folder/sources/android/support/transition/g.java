package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.support.transition.TransitionPort;
import android.support.v4.view.ag;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

@TargetApi(14)
/* compiled from: FadePort */
class g extends ah {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f162a = false;

    /* renamed from: b  reason: collision with root package name */
    private int f163b;

    public g() {
        this(3);
    }

    public g(int i) {
        this.f163b = i;
    }

    private Animator a(View view, float f2, float f3, AnimatorListenerAdapter animatorListenerAdapter) {
        ObjectAnimator objectAnimator = null;
        if (f2 != f3) {
            objectAnimator = ObjectAnimator.ofFloat(view, "alpha", f2, f3);
            if (f162a) {
                Log.d("Fade", "Created animator " + objectAnimator);
            }
            if (animatorListenerAdapter != null) {
                objectAnimator.addListener(animatorListenerAdapter);
            }
        } else if (animatorListenerAdapter != null) {
            animatorListenerAdapter.onAnimationEnd(null);
        }
        return objectAnimator;
    }

    private void d(z zVar) {
        int[] iArr = new int[2];
        zVar.f216b.getLocationOnScreen(iArr);
        zVar.f215a.put("android:fade:screenX", Integer.valueOf(iArr[0]));
        zVar.f215a.put("android:fade:screenY", Integer.valueOf(iArr[1]));
    }

    public void a(z zVar) {
        super.a(zVar);
        d(zVar);
    }

    public Animator a(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        if ((this.f163b & 1) != 1 || zVar2 == null) {
            return null;
        }
        final View view = zVar2.f216b;
        if (f162a) {
            Log.d("Fade", "Fade.onAppear: startView, startVis, endView, endVis = " + (zVar != null ? zVar.f216b : null) + ", " + i + ", " + view + ", " + i2);
        }
        view.setAlpha(0.0f);
        a(new TransitionPort.TransitionListenerAdapter() {

            /* renamed from: a  reason: collision with root package name */
            boolean f164a = false;

            /* renamed from: b  reason: collision with root package name */
            float f165b;

            public void a(TransitionPort transitionPort) {
                if (!this.f164a) {
                    view.setAlpha(1.0f);
                }
            }

            public void b(TransitionPort transitionPort) {
                this.f165b = view.getAlpha();
                view.setAlpha(1.0f);
            }

            public void c(TransitionPort transitionPort) {
                view.setAlpha(this.f165b);
            }
        });
        return a(view, 0.0f, 1.0f, null);
    }

    public Animator b(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        View view;
        final View view2;
        final View view3;
        View view4;
        if ((this.f163b & 2) != 2) {
            return null;
        }
        final View view5 = zVar != null ? zVar.f216b : null;
        if (zVar2 != null) {
            view = zVar2.f216b;
        } else {
            view = null;
        }
        if (f162a) {
            Log.d("Fade", "Fade.onDisappear: startView, startVis, endView, endVis = " + view5 + ", " + i + ", " + view + ", " + i2);
        }
        if (view == null || view.getParent() == null) {
            if (view != null) {
                view3 = null;
                view2 = view;
                view5 = view;
            } else {
                if (view5 != null) {
                    if (view5.getParent() == null) {
                        view3 = null;
                        view2 = view5;
                    } else if ((view5.getParent() instanceof View) && view5.getParent().getParent() == null) {
                        int id = ((View) view5.getParent()).getId();
                        if (id == -1 || viewGroup.findViewById(id) == null || !this.q) {
                            view4 = null;
                            view5 = null;
                        } else {
                            view4 = view5;
                        }
                        view3 = null;
                        view2 = view4;
                    }
                }
                view3 = null;
                view2 = null;
                view5 = null;
            }
        } else if (i2 == 4) {
            view3 = view;
            view2 = null;
            view5 = view;
        } else if (view5 == view) {
            view3 = view;
            view2 = null;
            view5 = view;
        } else {
            view3 = null;
            view2 = view5;
        }
        if (view2 != null) {
            int intValue = ((Integer) zVar.f215a.get("android:fade:screenX")).intValue();
            int intValue2 = ((Integer) zVar.f215a.get("android:fade:screenY")).intValue();
            int[] iArr = new int[2];
            viewGroup.getLocationOnScreen(iArr);
            ag.f(view2, (intValue - iArr[0]) - view2.getLeft());
            ag.e(view2, (intValue2 - iArr[1]) - view2.getTop());
            ab.a(viewGroup).a(view2);
            final int i3 = i2;
            final ViewGroup viewGroup2 = viewGroup;
            return a(view5, 1.0f, 0.0f, new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    view5.setAlpha(1.0f);
                    if (view3 != null) {
                        view3.setVisibility(i3);
                    }
                    if (view2 != null) {
                        ab.a(viewGroup2).b(view2);
                    }
                }
            });
        } else if (view3 == null) {
            return null;
        } else {
            view3.setVisibility(0);
            final int i4 = i2;
            final ViewGroup viewGroup3 = viewGroup;
            return a(view5, 1.0f, 0.0f, new AnimatorListenerAdapter() {

                /* renamed from: a  reason: collision with root package name */
                boolean f174a = false;

                /* renamed from: b  reason: collision with root package name */
                float f175b = -1.0f;

                public void onAnimationCancel(Animator animator) {
                    this.f174a = true;
                    if (this.f175b >= 0.0f) {
                        view5.setAlpha(this.f175b);
                    }
                }

                public void onAnimationEnd(Animator animator) {
                    if (!this.f174a) {
                        view5.setAlpha(1.0f);
                    }
                    if (view3 != null && !this.f174a) {
                        view3.setVisibility(i4);
                    }
                    if (view2 != null) {
                        ab.a(viewGroup3).a(view2);
                    }
                }
            });
        }
    }
}
