package android.support.transition;

import android.animation.Animator;
import android.os.Build;
import android.view.ViewGroup;

public class TransitionSet extends Transition {
    public TransitionSet() {
        super(true);
        if (Build.VERSION.SDK_INT < 19) {
            this.mImpl = new v(this);
        } else {
            this.mImpl = new x(this);
        }
    }

    public TransitionSet a(int i) {
        ((w) this.mImpl).d(i);
        return this;
    }

    public TransitionSet a(Transition transition) {
        ((w) this.mImpl).b(transition.mImpl);
        return this;
    }

    public void captureEndValues(z zVar) {
        this.mImpl.b(zVar);
    }

    public void captureStartValues(z zVar) {
        this.mImpl.c(zVar);
    }

    public Animator createAnimator(ViewGroup viewGroup, z zVar, z zVar2) {
        return this.mImpl.a(viewGroup, zVar, zVar2);
    }
}
