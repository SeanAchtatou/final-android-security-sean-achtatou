package android.support.transition;

import android.animation.Animator;
import android.os.Build;
import android.view.ViewGroup;

public abstract class Visibility extends Transition implements af {
    public Visibility() {
        this(false);
    }

    Visibility(boolean z) {
        super(true);
        if (!z) {
            if (Build.VERSION.SDK_INT >= 19) {
                this.mImpl = new ag();
            } else {
                this.mImpl = new ad();
            }
            this.mImpl.a(this);
        }
    }

    public void captureEndValues(z zVar) {
        this.mImpl.b(zVar);
    }

    public void captureStartValues(z zVar) {
        this.mImpl.c(zVar);
    }

    public boolean a(z zVar) {
        return ((ae) this.mImpl).a(zVar);
    }

    public Animator a(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return ((ae) this.mImpl).a(viewGroup, zVar, i, zVar2, i2);
    }

    public Animator b(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return ((ae) this.mImpl).b(viewGroup, zVar, i, zVar2, i2);
    }
}
