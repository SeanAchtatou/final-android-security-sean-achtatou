package android.support.transition;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.annotation.TargetApi;
import android.support.transition.TransitionPort;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@TargetApi(14)
/* compiled from: TransitionIcs */
class l extends m {

    /* renamed from: a  reason: collision with root package name */
    TransitionPort f185a;

    /* renamed from: b  reason: collision with root package name */
    n f186b;

    /* renamed from: c  reason: collision with root package name */
    private a f187c;

    l() {
    }

    public void a(n nVar, Object obj) {
        this.f186b = nVar;
        if (obj == null) {
            this.f185a = new b(nVar);
        } else {
            this.f185a = (TransitionPort) obj;
        }
    }

    public m a(o oVar) {
        if (this.f187c == null) {
            this.f187c = new a();
            this.f185a.a(this.f187c);
        }
        this.f187c.a(oVar);
        return this;
    }

    public m b(o oVar) {
        if (this.f187c != null) {
            this.f187c.b(oVar);
            if (this.f187c.a()) {
                this.f185a.b(this.f187c);
                this.f187c = null;
            }
        }
        return this;
    }

    public m a(View view) {
        this.f185a.a(view);
        return this;
    }

    public m b(int i) {
        this.f185a.a(i);
        return this;
    }

    public void b(z zVar) {
        this.f185a.b(zVar);
    }

    public void c(z zVar) {
        this.f185a.a(zVar);
    }

    public Animator a(ViewGroup viewGroup, z zVar, z zVar2) {
        return this.f185a.a(viewGroup, zVar, zVar2);
    }

    public m a(View view, boolean z) {
        this.f185a.b(view, z);
        return this;
    }

    public m a(int i, boolean z) {
        this.f185a.b(i, z);
        return this;
    }

    public m a(Class cls, boolean z) {
        this.f185a.b(cls, z);
        return this;
    }

    public m b(View view, boolean z) {
        this.f185a.a(view, z);
        return this;
    }

    public m b(int i, boolean z) {
        this.f185a.a(i, z);
        return this;
    }

    public m b(Class cls, boolean z) {
        this.f185a.a(cls, z);
        return this;
    }

    public long a() {
        return this.f185a.b();
    }

    public m a(long j) {
        this.f185a.a(j);
        return this;
    }

    public TimeInterpolator b() {
        return this.f185a.d();
    }

    public m a(TimeInterpolator timeInterpolator) {
        this.f185a.a(timeInterpolator);
        return this;
    }

    public String c() {
        return this.f185a.k();
    }

    public long d() {
        return this.f185a.c();
    }

    public m b(long j) {
        this.f185a.b(j);
        return this;
    }

    public List<Integer> e() {
        return this.f185a.f();
    }

    public List<View> f() {
        return this.f185a.g();
    }

    public String[] g() {
        return this.f185a.a();
    }

    public z c(View view, boolean z) {
        return this.f185a.c(view, z);
    }

    public m b(View view) {
        this.f185a.b(view);
        return this;
    }

    public m a(int i) {
        this.f185a.b(i);
        return this;
    }

    public String toString() {
        return this.f185a.toString();
    }

    /* compiled from: TransitionIcs */
    private static class b extends TransitionPort {

        /* renamed from: a  reason: collision with root package name */
        private n f190a;

        public b(n nVar) {
            this.f190a = nVar;
        }

        public void a(z zVar) {
            this.f190a.captureStartValues(zVar);
        }

        public void b(z zVar) {
            this.f190a.captureEndValues(zVar);
        }

        public Animator a(ViewGroup viewGroup, z zVar, z zVar2) {
            return this.f190a.createAnimator(viewGroup, zVar, zVar2);
        }
    }

    /* compiled from: TransitionIcs */
    private class a implements TransitionPort.c {

        /* renamed from: b  reason: collision with root package name */
        private final ArrayList<o> f189b = new ArrayList<>();

        a() {
        }

        public void a(o oVar) {
            this.f189b.add(oVar);
        }

        public void b(o oVar) {
            this.f189b.remove(oVar);
        }

        public boolean a() {
            return this.f189b.isEmpty();
        }

        public void d(TransitionPort transitionPort) {
            Iterator<o> it = this.f189b.iterator();
            while (it.hasNext()) {
                it.next().a(l.this.f186b);
            }
        }

        public void a(TransitionPort transitionPort) {
            Iterator<o> it = this.f189b.iterator();
            while (it.hasNext()) {
                it.next().b(l.this.f186b);
            }
        }

        public void b(TransitionPort transitionPort) {
            Iterator<o> it = this.f189b.iterator();
            while (it.hasNext()) {
                it.next().d(l.this.f186b);
            }
        }

        public void c(TransitionPort transitionPort) {
            Iterator<o> it = this.f189b.iterator();
            while (it.hasNext()) {
                it.next().e(l.this.f186b);
            }
        }
    }
}
