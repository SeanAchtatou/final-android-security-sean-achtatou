package android.support.transition;

import android.animation.TypeEvaluator;
import android.annotation.TargetApi;
import android.graphics.Rect;

@TargetApi(14)
/* compiled from: RectEvaluator */
class i implements TypeEvaluator<Rect> {

    /* renamed from: a  reason: collision with root package name */
    private Rect f182a;

    /* renamed from: a */
    public Rect evaluate(float f2, Rect rect, Rect rect2) {
        int i = ((int) (((float) (rect2.left - rect.left)) * f2)) + rect.left;
        int i2 = ((int) (((float) (rect2.top - rect.top)) * f2)) + rect.top;
        int i3 = ((int) (((float) (rect2.right - rect.right)) * f2)) + rect.right;
        int i4 = ((int) (((float) (rect2.bottom - rect.bottom)) * f2)) + rect.bottom;
        if (this.f182a == null) {
            return new Rect(i, i2, i3, i4);
        }
        this.f182a.set(i, i2, i3, i4);
        return this.f182a;
    }
}
