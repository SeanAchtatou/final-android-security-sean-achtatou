package android.support.transition;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.annotation.TargetApi;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@TargetApi(19)
/* compiled from: TransitionKitKat */
class p extends m {

    /* renamed from: a  reason: collision with root package name */
    Transition f191a;

    /* renamed from: b  reason: collision with root package name */
    n f192b;

    /* renamed from: c  reason: collision with root package name */
    private a f193c;

    p() {
    }

    static void a(TransitionValues transitionValues, z zVar) {
        if (transitionValues != null) {
            zVar.f216b = transitionValues.view;
            if (transitionValues.values.size() > 0) {
                zVar.f215a.putAll(transitionValues.values);
            }
        }
    }

    static void a(z zVar, TransitionValues transitionValues) {
        if (zVar != null) {
            transitionValues.view = zVar.f216b;
            if (zVar.f215a.size() > 0) {
                transitionValues.values.putAll(zVar.f215a);
            }
        }
    }

    static void a(n nVar, TransitionValues transitionValues) {
        z zVar = new z();
        a(transitionValues, zVar);
        nVar.captureStartValues(zVar);
        a(zVar, transitionValues);
    }

    static void b(n nVar, TransitionValues transitionValues) {
        z zVar = new z();
        a(transitionValues, zVar);
        nVar.captureEndValues(zVar);
        a(zVar, transitionValues);
    }

    static z a(TransitionValues transitionValues) {
        if (transitionValues == null) {
            return null;
        }
        z zVar = new z();
        a(transitionValues, zVar);
        return zVar;
    }

    static TransitionValues d(z zVar) {
        if (zVar == null) {
            return null;
        }
        TransitionValues transitionValues = new TransitionValues();
        a(zVar, transitionValues);
        return transitionValues;
    }

    public void a(n nVar, Object obj) {
        this.f192b = nVar;
        if (obj == null) {
            this.f191a = new b(nVar);
        } else {
            this.f191a = (Transition) obj;
        }
    }

    public m a(o oVar) {
        if (this.f193c == null) {
            this.f193c = new a();
            this.f191a.addListener(this.f193c);
        }
        this.f193c.a(oVar);
        return this;
    }

    public m b(o oVar) {
        if (this.f193c != null) {
            this.f193c.b(oVar);
            if (this.f193c.a()) {
                this.f191a.removeListener(this.f193c);
                this.f193c = null;
            }
        }
        return this;
    }

    public m a(View view) {
        this.f191a.addTarget(view);
        return this;
    }

    public m b(int i) {
        this.f191a.addTarget(i);
        return this;
    }

    public void b(z zVar) {
        TransitionValues transitionValues = new TransitionValues();
        a(zVar, transitionValues);
        this.f191a.captureEndValues(transitionValues);
        a(transitionValues, zVar);
    }

    public void c(z zVar) {
        TransitionValues transitionValues = new TransitionValues();
        a(zVar, transitionValues);
        this.f191a.captureStartValues(transitionValues);
        a(transitionValues, zVar);
    }

    public Animator a(ViewGroup viewGroup, z zVar, z zVar2) {
        TransitionValues transitionValues;
        TransitionValues transitionValues2 = null;
        if (zVar != null) {
            transitionValues = new TransitionValues();
            a(zVar, transitionValues);
        } else {
            transitionValues = null;
        }
        if (zVar2 != null) {
            transitionValues2 = new TransitionValues();
            a(zVar2, transitionValues2);
        }
        return this.f191a.createAnimator(viewGroup, transitionValues, transitionValues2);
    }

    public m a(View view, boolean z) {
        this.f191a.excludeChildren(view, z);
        return this;
    }

    public m a(int i, boolean z) {
        this.f191a.excludeChildren(i, z);
        return this;
    }

    public m a(Class cls, boolean z) {
        this.f191a.excludeChildren(cls, z);
        return this;
    }

    public m b(View view, boolean z) {
        this.f191a.excludeTarget(view, z);
        return this;
    }

    public m b(int i, boolean z) {
        this.f191a.excludeTarget(i, z);
        return this;
    }

    public m b(Class cls, boolean z) {
        this.f191a.excludeTarget(cls, z);
        return this;
    }

    public long a() {
        return this.f191a.getDuration();
    }

    public m a(long j) {
        this.f191a.setDuration(j);
        return this;
    }

    public TimeInterpolator b() {
        return this.f191a.getInterpolator();
    }

    public m a(TimeInterpolator timeInterpolator) {
        this.f191a.setInterpolator(timeInterpolator);
        return this;
    }

    public String c() {
        return this.f191a.getName();
    }

    public long d() {
        return this.f191a.getStartDelay();
    }

    public m b(long j) {
        this.f191a.setStartDelay(j);
        return this;
    }

    public List<Integer> e() {
        return this.f191a.getTargetIds();
    }

    public List<View> f() {
        return this.f191a.getTargets();
    }

    public String[] g() {
        return this.f191a.getTransitionProperties();
    }

    public z c(View view, boolean z) {
        z zVar = new z();
        a(this.f191a.getTransitionValues(view, z), zVar);
        return zVar;
    }

    public m b(View view) {
        this.f191a.removeTarget(view);
        return this;
    }

    public m a(int i) {
        if (i > 0) {
            e().remove(Integer.valueOf(i));
        }
        return this;
    }

    public String toString() {
        return this.f191a.toString();
    }

    /* compiled from: TransitionKitKat */
    private static class b extends Transition {

        /* renamed from: a  reason: collision with root package name */
        private n f196a;

        public b(n nVar) {
            this.f196a = nVar;
        }

        public void captureStartValues(TransitionValues transitionValues) {
            p.a(this.f196a, transitionValues);
        }

        public void captureEndValues(TransitionValues transitionValues) {
            p.b(this.f196a, transitionValues);
        }

        public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
            return this.f196a.createAnimator(viewGroup, p.a(transitionValues), p.a(transitionValues2));
        }
    }

    /* compiled from: TransitionKitKat */
    private class a implements Transition.TransitionListener {

        /* renamed from: b  reason: collision with root package name */
        private final ArrayList<o> f195b = new ArrayList<>();

        a() {
        }

        /* access modifiers changed from: package-private */
        public void a(o oVar) {
            this.f195b.add(oVar);
        }

        /* access modifiers changed from: package-private */
        public void b(o oVar) {
            this.f195b.remove(oVar);
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.f195b.isEmpty();
        }

        public void onTransitionStart(Transition transition) {
            Iterator<o> it = this.f195b.iterator();
            while (it.hasNext()) {
                it.next().a(p.this.f192b);
            }
        }

        public void onTransitionEnd(Transition transition) {
            Iterator<o> it = this.f195b.iterator();
            while (it.hasNext()) {
                it.next().b(p.this.f192b);
            }
        }

        public void onTransitionCancel(Transition transition) {
            Iterator<o> it = this.f195b.iterator();
            while (it.hasNext()) {
                it.next().c(p.this.f192b);
            }
        }

        public void onTransitionPause(Transition transition) {
            Iterator<o> it = this.f195b.iterator();
            while (it.hasNext()) {
                it.next().d(p.this.f192b);
            }
        }

        public void onTransitionResume(Transition transition) {
            Iterator<o> it = this.f195b.iterator();
            while (it.hasNext()) {
                it.next().e(p.this.f192b);
            }
        }
    }
}
