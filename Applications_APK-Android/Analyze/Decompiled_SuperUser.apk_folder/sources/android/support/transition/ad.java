package android.support.transition;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.view.ViewGroup;

@TargetApi(14)
/* compiled from: VisibilityIcs */
class ad extends l implements ae {
    ad() {
    }

    public void a(n nVar, Object obj) {
        this.f186b = nVar;
        if (obj == null) {
            this.f185a = new a((af) nVar);
        } else {
            this.f185a = (ah) obj;
        }
    }

    public boolean a(z zVar) {
        return ((ah) this.f185a).c(zVar);
    }

    public Animator a(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return ((ah) this.f185a).a(viewGroup, zVar, i, zVar2, i2);
    }

    public Animator b(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return ((ah) this.f185a).b(viewGroup, zVar, i, zVar2, i2);
    }

    /* compiled from: VisibilityIcs */
    private static class a extends ah {

        /* renamed from: a  reason: collision with root package name */
        private af f140a;

        a(af afVar) {
            this.f140a = afVar;
        }

        public void a(z zVar) {
            this.f140a.captureStartValues(zVar);
        }

        public void b(z zVar) {
            this.f140a.captureEndValues(zVar);
        }

        public Animator a(ViewGroup viewGroup, z zVar, z zVar2) {
            return this.f140a.createAnimator(viewGroup, zVar, zVar2);
        }

        public boolean c(z zVar) {
            return this.f140a.a(zVar);
        }

        public Animator a(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
            return this.f140a.a(viewGroup, zVar, i, zVar2, i2);
        }

        public Animator b(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
            return this.f140a.b(viewGroup, zVar, i, zVar2, i2);
        }
    }
}
