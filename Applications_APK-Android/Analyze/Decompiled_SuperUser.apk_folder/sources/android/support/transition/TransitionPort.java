package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.annotation.TargetApi;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.e;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@TargetApi(14)
abstract class TransitionPort implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private static ThreadLocal<ArrayMap<Animator, a>> f116a = new ThreadLocal<>();

    /* renamed from: b  reason: collision with root package name */
    private String f117b = getClass().getName();

    /* renamed from: c  reason: collision with root package name */
    private aa f118c = new aa();

    /* renamed from: d  reason: collision with root package name */
    long f119d = -1;

    /* renamed from: e  reason: collision with root package name */
    long f120e = -1;

    /* renamed from: f  reason: collision with root package name */
    TimeInterpolator f121f = null;

    /* renamed from: g  reason: collision with root package name */
    ArrayList<Integer> f122g = new ArrayList<>();

    /* renamed from: h  reason: collision with root package name */
    ArrayList<View> f123h = new ArrayList<>();
    ArrayList<Integer> i = null;
    ArrayList<View> j = null;
    ArrayList<Class> k = null;
    ArrayList<Integer> l = null;
    ArrayList<View> m = null;
    ArrayList<Class> n = null;
    y o = null;
    ViewGroup p = null;
    boolean q = false;
    int r = 0;
    boolean s = false;
    ArrayList<c> t = null;
    ArrayList<Animator> u = new ArrayList<>();
    ArrayList<Animator> v = new ArrayList<>();
    private aa w = new aa();
    private boolean x = false;

    public interface c {
        void a(TransitionPort transitionPort);

        void b(TransitionPort transitionPort);

        void c(TransitionPort transitionPort);

        void d(TransitionPort transitionPort);
    }

    public abstract void a(z zVar);

    public abstract void b(z zVar);

    private static ArrayMap<Animator, a> l() {
        ArrayMap<Animator, a> arrayMap = f116a.get();
        if (arrayMap != null) {
            return arrayMap;
        }
        ArrayMap<Animator, a> arrayMap2 = new ArrayMap<>();
        f116a.set(arrayMap2);
        return arrayMap2;
    }

    public long b() {
        return this.f120e;
    }

    public TransitionPort a(long j2) {
        this.f120e = j2;
        return this;
    }

    public long c() {
        return this.f119d;
    }

    public TransitionPort b(long j2) {
        this.f119d = j2;
        return this;
    }

    public TimeInterpolator d() {
        return this.f121f;
    }

    public TransitionPort a(TimeInterpolator timeInterpolator) {
        this.f121f = timeInterpolator;
        return this;
    }

    public String[] a() {
        return null;
    }

    public Animator a(ViewGroup viewGroup, z zVar, z zVar2) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup, aa aaVar, aa aaVar2) {
        Animator a2;
        View view;
        z zVar;
        Animator animator;
        z zVar2;
        ArrayMap arrayMap = new ArrayMap(aaVar2.f131a);
        SparseArray sparseArray = new SparseArray(aaVar2.f132b.size());
        for (int i2 = 0; i2 < aaVar2.f132b.size(); i2++) {
            sparseArray.put(aaVar2.f132b.keyAt(i2), aaVar2.f132b.valueAt(i2));
        }
        e eVar = new e(aaVar2.f133c.b());
        for (int i3 = 0; i3 < aaVar2.f133c.b(); i3++) {
            eVar.b(aaVar2.f133c.b(i3), aaVar2.f133c.c(i3));
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (View next : aaVar.f131a.keySet()) {
            boolean z = false;
            if (next.getParent() instanceof ListView) {
                z = true;
            }
            if (!z) {
                int id = next.getId();
                z zVar3 = aaVar.f131a.get(next) != null ? aaVar.f131a.get(next) : aaVar.f132b.get(id);
                if (aaVar2.f131a.get(next) != null) {
                    zVar2 = aaVar2.f131a.get(next);
                    arrayMap.remove(next);
                } else if (id != -1) {
                    zVar2 = aaVar2.f132b.get(id);
                    View view2 = null;
                    for (View view3 : arrayMap.keySet()) {
                        if (view3.getId() != id) {
                            view3 = view2;
                        }
                        view2 = view3;
                    }
                    if (view2 != null) {
                        arrayMap.remove(view2);
                    }
                } else {
                    zVar2 = null;
                }
                sparseArray.remove(id);
                if (a(next, (long) id)) {
                    arrayList.add(zVar3);
                    arrayList2.add(zVar2);
                }
            } else {
                ListView listView = (ListView) next.getParent();
                if (listView.getAdapter().hasStableIds()) {
                    long itemIdAtPosition = listView.getItemIdAtPosition(listView.getPositionForView(next));
                    eVar.c(itemIdAtPosition);
                    arrayList.add(aaVar.f133c.a(itemIdAtPosition));
                    arrayList2.add(null);
                }
            }
        }
        int b2 = aaVar.f133c.b();
        for (int i4 = 0; i4 < b2; i4++) {
            long b3 = aaVar.f133c.b(i4);
            if (a((View) null, b3)) {
                eVar.c(b3);
                arrayList.add(aaVar.f133c.a(b3));
                arrayList2.add(aaVar2.f133c.a(b3));
            }
        }
        for (View view4 : arrayMap.keySet()) {
            int id2 = view4.getId();
            if (a(view4, (long) id2)) {
                z zVar4 = aaVar.f131a.get(view4) != null ? aaVar.f131a.get(view4) : aaVar.f132b.get(id2);
                sparseArray.remove(id2);
                arrayList.add(zVar4);
                arrayList2.add((z) arrayMap.get(view4));
            }
        }
        int size = sparseArray.size();
        for (int i5 = 0; i5 < size; i5++) {
            int keyAt = sparseArray.keyAt(i5);
            if (a((View) null, (long) keyAt)) {
                arrayList.add(aaVar.f132b.get(keyAt));
                arrayList2.add((z) sparseArray.get(keyAt));
            }
        }
        int b4 = eVar.b();
        for (int i6 = 0; i6 < b4; i6++) {
            long b5 = eVar.b(i6);
            arrayList.add(aaVar.f133c.a(b5));
            arrayList2.add((z) eVar.a(b5));
        }
        ArrayMap<Animator, a> l2 = l();
        int i7 = 0;
        while (true) {
            int i8 = i7;
            if (i8 < arrayList.size()) {
                z zVar5 = (z) arrayList.get(i8);
                z zVar6 = (z) arrayList2.get(i8);
                if (!(zVar5 == null && zVar6 == null) && ((zVar5 == null || !zVar5.equals(zVar6)) && (a2 = a(viewGroup, zVar5, zVar6)) != null)) {
                    if (zVar6 != null) {
                        View view5 = zVar6.f216b;
                        String[] a3 = a();
                        if (view5 != null && a3 != null && a3.length > 0) {
                            z zVar7 = new z();
                            zVar7.f216b = view5;
                            z zVar8 = aaVar2.f131a.get(view5);
                            if (zVar8 != null) {
                                for (int i9 = 0; i9 < a3.length; i9++) {
                                    zVar7.f215a.put(a3[i9], zVar8.f215a.get(a3[i9]));
                                }
                            }
                            int size2 = l2.size();
                            int i10 = 0;
                            while (true) {
                                if (i10 >= size2) {
                                    zVar = zVar7;
                                    animator = a2;
                                    break;
                                }
                                a aVar = l2.get(l2.b(i10));
                                if (aVar.f129c != null && aVar.f127a == view5 && (((aVar.f128b == null && k() == null) || aVar.f128b.equals(k())) && aVar.f129c.equals(zVar7))) {
                                    animator = null;
                                    zVar = zVar7;
                                    break;
                                }
                                i10++;
                            }
                        } else {
                            zVar = null;
                            animator = a2;
                        }
                        a2 = animator;
                        view = view5;
                    } else {
                        view = zVar5.f216b;
                        zVar = null;
                    }
                    if (a2 != null) {
                        l2.put(a2, new a(view, k(), ai.a(viewGroup), zVar));
                        this.u.add(a2);
                    }
                }
                i7 = i8 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, long j2) {
        if (this.i != null && this.i.contains(Integer.valueOf((int) j2))) {
            return false;
        }
        if (this.j != null && this.j.contains(view)) {
            return false;
        }
        if (!(this.k == null || view == null)) {
            int size = this.k.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (this.k.get(i2).isInstance(view)) {
                    return false;
                }
            }
        }
        if (this.f122g.size() == 0 && this.f123h.size() == 0) {
            return true;
        }
        if (this.f122g.size() > 0) {
            for (int i3 = 0; i3 < this.f122g.size(); i3++) {
                if (((long) this.f122g.get(i3).intValue()) == j2) {
                    return true;
                }
            }
        }
        if (view == null || this.f123h.size() <= 0) {
            return false;
        }
        for (int i4 = 0; i4 < this.f123h.size(); i4++) {
            if (this.f123h.get(i4) == view) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void e() {
        h();
        ArrayMap<Animator, a> l2 = l();
        Iterator<Animator> it = this.u.iterator();
        while (it.hasNext()) {
            Animator next = it.next();
            if (l2.containsKey(next)) {
                h();
                a(next, l2);
            }
        }
        this.u.clear();
        i();
    }

    private void a(Animator animator, final ArrayMap<Animator, a> arrayMap) {
        if (animator != null) {
            animator.addListener(new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animator) {
                    TransitionPort.this.v.add(animator);
                }

                public void onAnimationEnd(Animator animator) {
                    arrayMap.remove(animator);
                    TransitionPort.this.v.remove(animator);
                }
            });
            a(animator);
        }
    }

    public TransitionPort a(int i2) {
        if (i2 > 0) {
            this.f122g.add(Integer.valueOf(i2));
        }
        return this;
    }

    public TransitionPort b(int i2) {
        if (i2 > 0) {
            this.f122g.remove(Integer.valueOf(i2));
        }
        return this;
    }

    public TransitionPort a(int i2, boolean z) {
        this.i = a(this.i, i2, z);
        return this;
    }

    public TransitionPort b(int i2, boolean z) {
        this.l = a(this.l, i2, z);
        return this;
    }

    private ArrayList<Integer> a(ArrayList<Integer> arrayList, int i2, boolean z) {
        if (i2 <= 0) {
            return arrayList;
        }
        if (z) {
            return b.a(arrayList, Integer.valueOf(i2));
        }
        return b.b(arrayList, Integer.valueOf(i2));
    }

    public TransitionPort a(View view, boolean z) {
        this.j = a(this.j, view, z);
        return this;
    }

    public TransitionPort b(View view, boolean z) {
        this.m = a(this.m, view, z);
        return this;
    }

    private ArrayList<View> a(ArrayList<View> arrayList, View view, boolean z) {
        if (view == null) {
            return arrayList;
        }
        if (z) {
            return b.a(arrayList, view);
        }
        return b.b(arrayList, view);
    }

    public TransitionPort a(Class cls, boolean z) {
        this.k = a(this.k, cls, z);
        return this;
    }

    public TransitionPort b(Class cls, boolean z) {
        this.n = a(this.n, cls, z);
        return this;
    }

    private ArrayList<Class> a(ArrayList<Class> arrayList, Class cls, boolean z) {
        if (cls == null) {
            return arrayList;
        }
        if (z) {
            return b.a(arrayList, cls);
        }
        return b.b(arrayList, cls);
    }

    public TransitionPort a(View view) {
        this.f123h.add(view);
        return this;
    }

    public TransitionPort b(View view) {
        if (view != null) {
            this.f123h.remove(view);
        }
        return this;
    }

    public List<Integer> f() {
        return this.f122g;
    }

    public List<View> g() {
        return this.f123h;
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup, boolean z) {
        a(z);
        if (this.f122g.size() > 0 || this.f123h.size() > 0) {
            if (this.f122g.size() > 0) {
                for (int i2 = 0; i2 < this.f122g.size(); i2++) {
                    int intValue = this.f122g.get(i2).intValue();
                    View findViewById = viewGroup.findViewById(intValue);
                    if (findViewById != null) {
                        z zVar = new z();
                        zVar.f216b = findViewById;
                        if (z) {
                            a(zVar);
                        } else {
                            b(zVar);
                        }
                        if (z) {
                            this.f118c.f131a.put(findViewById, zVar);
                            if (intValue >= 0) {
                                this.f118c.f132b.put(intValue, zVar);
                            }
                        } else {
                            this.w.f131a.put(findViewById, zVar);
                            if (intValue >= 0) {
                                this.w.f132b.put(intValue, zVar);
                            }
                        }
                    }
                }
            }
            if (this.f123h.size() > 0) {
                for (int i3 = 0; i3 < this.f123h.size(); i3++) {
                    View view = this.f123h.get(i3);
                    if (view != null) {
                        z zVar2 = new z();
                        zVar2.f216b = view;
                        if (z) {
                            a(zVar2);
                        } else {
                            b(zVar2);
                        }
                        if (z) {
                            this.f118c.f131a.put(view, zVar2);
                        } else {
                            this.w.f131a.put(view, zVar2);
                        }
                    }
                }
                return;
            }
            return;
        }
        d(viewGroup, z);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (z) {
            this.f118c.f131a.clear();
            this.f118c.f132b.clear();
            this.f118c.f133c.c();
            return;
        }
        this.w.f131a.clear();
        this.w.f132b.clear();
        this.w.f133c.c();
    }

    private void d(View view, boolean z) {
        boolean z2;
        int i2;
        long itemIdAtPosition;
        if (view != null) {
            if (view.getParent() instanceof ListView) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (!z2 || ((ListView) view.getParent()).getAdapter().hasStableIds()) {
                if (!z2) {
                    i2 = view.getId();
                    itemIdAtPosition = -1;
                } else {
                    ListView listView = (ListView) view.getParent();
                    i2 = -1;
                    itemIdAtPosition = listView.getItemIdAtPosition(listView.getPositionForView(view));
                }
                if (this.i != null && this.i.contains(Integer.valueOf(i2))) {
                    return;
                }
                if (this.j == null || !this.j.contains(view)) {
                    if (!(this.k == null || view == null)) {
                        int size = this.k.size();
                        int i3 = 0;
                        while (i3 < size) {
                            if (!this.k.get(i3).isInstance(view)) {
                                i3++;
                            } else {
                                return;
                            }
                        }
                    }
                    z zVar = new z();
                    zVar.f216b = view;
                    if (z) {
                        a(zVar);
                    } else {
                        b(zVar);
                    }
                    if (z) {
                        if (!z2) {
                            this.f118c.f131a.put(view, zVar);
                            if (i2 >= 0) {
                                this.f118c.f132b.put(i2, zVar);
                            }
                        } else {
                            this.f118c.f133c.b(itemIdAtPosition, zVar);
                        }
                    } else if (!z2) {
                        this.w.f131a.put(view, zVar);
                        if (i2 >= 0) {
                            this.w.f132b.put(i2, zVar);
                        }
                    } else {
                        this.w.f133c.b(itemIdAtPosition, zVar);
                    }
                    if (!(view instanceof ViewGroup)) {
                        return;
                    }
                    if (this.l != null && this.l.contains(Integer.valueOf(i2))) {
                        return;
                    }
                    if (this.m == null || !this.m.contains(view)) {
                        if (!(this.n == null || view == null)) {
                            int size2 = this.n.size();
                            int i4 = 0;
                            while (i4 < size2) {
                                if (!this.n.get(i4).isInstance(view)) {
                                    i4++;
                                } else {
                                    return;
                                }
                            }
                        }
                        ViewGroup viewGroup = (ViewGroup) view;
                        for (int i5 = 0; i5 < viewGroup.getChildCount(); i5++) {
                            d(viewGroup.getChildAt(i5), z);
                        }
                    }
                }
            }
        }
    }

    public z c(View view, boolean z) {
        if (this.o != null) {
            return this.o.c(view, z);
        }
        aa aaVar = z ? this.f118c : this.w;
        z zVar = aaVar.f131a.get(view);
        if (zVar != null) {
            return zVar;
        }
        int id = view.getId();
        if (id >= 0) {
            zVar = aaVar.f132b.get(id);
        }
        if (zVar != null || !(view.getParent() instanceof ListView)) {
            return zVar;
        }
        ListView listView = (ListView) view.getParent();
        return aaVar.f133c.a(listView.getItemIdAtPosition(listView.getPositionForView(view)));
    }

    public void c(View view) {
        if (!this.x) {
            ArrayMap<Animator, a> l2 = l();
            int size = l2.size();
            ai a2 = ai.a(view);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                a c2 = l2.c(i2);
                if (c2.f127a != null && a2.equals(c2.f130d)) {
                    l2.b(i2).cancel();
                }
            }
            if (this.t != null && this.t.size() > 0) {
                ArrayList arrayList = (ArrayList) this.t.clone();
                int size2 = arrayList.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    ((c) arrayList.get(i3)).b(this);
                }
            }
            this.s = true;
        }
    }

    public void d(View view) {
        if (this.s) {
            if (!this.x) {
                ArrayMap<Animator, a> l2 = l();
                int size = l2.size();
                ai a2 = ai.a(view);
                for (int i2 = size - 1; i2 >= 0; i2--) {
                    a c2 = l2.c(i2);
                    if (c2.f127a != null && a2.equals(c2.f130d)) {
                        l2.b(i2).end();
                    }
                }
                if (this.t != null && this.t.size() > 0) {
                    ArrayList arrayList = (ArrayList) this.t.clone();
                    int size2 = arrayList.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        ((c) arrayList.get(i3)).c(this);
                    }
                }
            }
            this.s = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup) {
        a aVar;
        z zVar;
        boolean z;
        ArrayMap<Animator, a> l2 = l();
        for (int size = l2.size() - 1; size >= 0; size--) {
            Animator b2 = l2.b(size);
            if (!(b2 == null || (aVar = l2.get(b2)) == null || aVar.f127a == null || aVar.f127a.getContext() != viewGroup.getContext())) {
                z zVar2 = aVar.f129c;
                View view = aVar.f127a;
                z zVar3 = this.w.f131a != null ? this.w.f131a.get(view) : null;
                if (zVar3 == null) {
                    zVar = this.w.f132b.get(view.getId());
                } else {
                    zVar = zVar3;
                }
                if (zVar2 != null && zVar != null) {
                    Iterator<String> it = zVar2.f215a.keySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        String next = it.next();
                        Object obj = zVar2.f215a.get(next);
                        Object obj2 = zVar.f215a.get(next);
                        if (obj != null && obj2 != null && !obj.equals(obj2)) {
                            z = true;
                            break;
                        }
                    }
                }
                z = false;
                if (z) {
                    if (b2.isRunning() || b2.isStarted()) {
                        b2.cancel();
                    } else {
                        l2.remove(b2);
                    }
                }
            }
        }
        a(viewGroup, this.f118c, this.w);
        e();
    }

    /* access modifiers changed from: protected */
    public void a(Animator animator) {
        if (animator == null) {
            i();
            return;
        }
        if (b() >= 0) {
            animator.setDuration(b());
        }
        if (c() >= 0) {
            animator.setStartDelay(c());
        }
        if (d() != null) {
            animator.setInterpolator(d());
        }
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                TransitionPort.this.i();
                animator.removeListener(this);
            }
        });
        animator.start();
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (this.r == 0) {
            if (this.t != null && this.t.size() > 0) {
                ArrayList arrayList = (ArrayList) this.t.clone();
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((c) arrayList.get(i2)).d(this);
                }
            }
            this.x = false;
        }
        this.r++;
    }

    /* access modifiers changed from: protected */
    public void i() {
        this.r--;
        if (this.r == 0) {
            if (this.t != null && this.t.size() > 0) {
                ArrayList arrayList = (ArrayList) this.t.clone();
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((c) arrayList.get(i2)).a(this);
                }
            }
            for (int i3 = 0; i3 < this.f118c.f133c.b(); i3++) {
                View view = this.f118c.f133c.c(i3).f216b;
            }
            for (int i4 = 0; i4 < this.w.f133c.b(); i4++) {
                View view2 = this.w.f133c.c(i4).f216b;
            }
            this.x = true;
        }
    }

    public TransitionPort a(c cVar) {
        if (this.t == null) {
            this.t = new ArrayList<>();
        }
        this.t.add(cVar);
        return this;
    }

    public TransitionPort b(c cVar) {
        if (this.t != null) {
            this.t.remove(cVar);
            if (this.t.size() == 0) {
                this.t = null;
            }
        }
        return this;
    }

    public String toString() {
        return a("");
    }

    /* renamed from: j */
    public TransitionPort clone() {
        try {
            TransitionPort transitionPort = (TransitionPort) super.clone();
            try {
                transitionPort.u = new ArrayList<>();
                transitionPort.f118c = new aa();
                transitionPort.w = new aa();
                return transitionPort;
            } catch (CloneNotSupportedException e2) {
                return transitionPort;
            }
        } catch (CloneNotSupportedException e3) {
            return null;
        }
    }

    public String k() {
        return this.f117b;
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        String str2;
        String str3 = str + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ": ";
        if (this.f120e != -1) {
            str3 = str3 + "dur(" + this.f120e + ") ";
        }
        if (this.f119d != -1) {
            str3 = str3 + "dly(" + this.f119d + ") ";
        }
        if (this.f121f != null) {
            str3 = str3 + "interp(" + this.f121f + ") ";
        }
        if (this.f122g.size() <= 0 && this.f123h.size() <= 0) {
            return str3;
        }
        String str4 = str3 + "tgts(";
        if (this.f122g.size() > 0) {
            str2 = str4;
            for (int i2 = 0; i2 < this.f122g.size(); i2++) {
                if (i2 > 0) {
                    str2 = str2 + ", ";
                }
                str2 = str2 + this.f122g.get(i2);
            }
        } else {
            str2 = str4;
        }
        if (this.f123h.size() > 0) {
            for (int i3 = 0; i3 < this.f123h.size(); i3++) {
                if (i3 > 0) {
                    str2 = str2 + ", ";
                }
                str2 = str2 + this.f123h.get(i3);
            }
        }
        return str2 + ")";
    }

    public static class TransitionListenerAdapter implements c {
        public void d(TransitionPort transitionPort) {
        }

        public void a(TransitionPort transitionPort) {
        }

        public void b(TransitionPort transitionPort) {
        }

        public void c(TransitionPort transitionPort) {
        }
    }

    private static class a {

        /* renamed from: a  reason: collision with root package name */
        View f127a;

        /* renamed from: b  reason: collision with root package name */
        String f128b;

        /* renamed from: c  reason: collision with root package name */
        z f129c;

        /* renamed from: d  reason: collision with root package name */
        ai f130d;

        a(View view, String str, ai aiVar, z zVar) {
            this.f127a = view;
            this.f128b = str;
            this.f129c = zVar;
            this.f130d = aiVar;
        }
    }

    private static class b {
        static <T> ArrayList<T> a(ArrayList<T> arrayList, T t) {
            if (arrayList == null) {
                arrayList = new ArrayList<>();
            }
            if (!arrayList.contains(t)) {
                arrayList.add(t);
            }
            return arrayList;
        }

        static <T> ArrayList<T> b(ArrayList<T> arrayList, T t) {
            if (arrayList == null) {
                return arrayList;
            }
            arrayList.remove(t);
            if (arrayList.isEmpty()) {
                return null;
            }
            return arrayList;
        }
    }
}
