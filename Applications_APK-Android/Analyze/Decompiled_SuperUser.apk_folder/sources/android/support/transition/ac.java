package android.support.transition;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ag;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.lang.reflect.Method;
import java.util.ArrayList;

@TargetApi(14)
/* compiled from: ViewOverlay */
class ac {

    /* renamed from: a  reason: collision with root package name */
    protected a f134a;

    ac(Context context, ViewGroup viewGroup, View view) {
        this.f134a = new a(context, viewGroup, view, this);
    }

    static ViewGroup c(View view) {
        View view2 = view;
        while (view2 != null) {
            if (view2.getId() == 16908290 && (view2 instanceof ViewGroup)) {
                return (ViewGroup) view2;
            }
            if (view2.getParent() instanceof ViewGroup) {
                view2 = (ViewGroup) view2.getParent();
            }
        }
        return null;
    }

    public static ac d(View view) {
        ViewGroup c2 = c(view);
        if (c2 == null) {
            return null;
        }
        int childCount = c2.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = c2.getChildAt(i);
            if (childAt instanceof a) {
                return ((a) childAt).f139e;
            }
        }
        return new ab(c2.getContext(), c2, view);
    }

    public void a(Drawable drawable) {
        this.f134a.a(drawable);
    }

    public void b(Drawable drawable) {
        this.f134a.b(drawable);
    }

    /* compiled from: ViewOverlay */
    static class a extends ViewGroup {

        /* renamed from: a  reason: collision with root package name */
        static Method f135a;

        /* renamed from: b  reason: collision with root package name */
        ViewGroup f136b;

        /* renamed from: c  reason: collision with root package name */
        View f137c;

        /* renamed from: d  reason: collision with root package name */
        ArrayList<Drawable> f138d = null;

        /* renamed from: e  reason: collision with root package name */
        ac f139e;

        static {
            Class<ViewGroup> cls = ViewGroup.class;
            try {
                f135a = cls.getDeclaredMethod("invalidateChildInParentFast", Integer.TYPE, Integer.TYPE, Rect.class);
            } catch (NoSuchMethodException e2) {
            }
        }

        a(Context context, ViewGroup viewGroup, View view, ac acVar) {
            super(context);
            this.f136b = viewGroup;
            this.f137c = view;
            setRight(viewGroup.getWidth());
            setBottom(viewGroup.getHeight());
            viewGroup.addView(this);
            this.f139e = acVar;
        }

        public boolean dispatchTouchEvent(MotionEvent motionEvent) {
            return false;
        }

        public void a(Drawable drawable) {
            if (this.f138d == null) {
                this.f138d = new ArrayList<>();
            }
            if (!this.f138d.contains(drawable)) {
                this.f138d.add(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback(this);
            }
        }

        public void b(Drawable drawable) {
            if (this.f138d != null) {
                this.f138d.remove(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback(null);
            }
        }

        /* access modifiers changed from: protected */
        public boolean verifyDrawable(Drawable drawable) {
            return super.verifyDrawable(drawable) || (this.f138d != null && this.f138d.contains(drawable));
        }

        public void a(View view) {
            if (view.getParent() instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view.getParent();
                if (!(viewGroup == this.f136b || viewGroup.getParent() == null)) {
                    int[] iArr = new int[2];
                    int[] iArr2 = new int[2];
                    viewGroup.getLocationOnScreen(iArr);
                    this.f136b.getLocationOnScreen(iArr2);
                    ag.f(view, iArr[0] - iArr2[0]);
                    ag.e(view, iArr[1] - iArr2[1]);
                }
                viewGroup.removeView(view);
                if (view.getParent() != null) {
                    viewGroup.removeView(view);
                }
            }
            super.addView(view, getChildCount() - 1);
        }

        public void b(View view) {
            super.removeView(view);
            if (a()) {
                this.f136b.removeView(this);
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            if (getChildCount() == 0 && (this.f138d == null || this.f138d.size() == 0)) {
                return true;
            }
            return false;
        }

        public void invalidateDrawable(Drawable drawable) {
            invalidate(drawable.getBounds());
        }

        /* access modifiers changed from: protected */
        public void dispatchDraw(Canvas canvas) {
            int[] iArr = new int[2];
            int[] iArr2 = new int[2];
            ViewGroup viewGroup = (ViewGroup) getParent();
            this.f136b.getLocationOnScreen(iArr);
            this.f137c.getLocationOnScreen(iArr2);
            canvas.translate((float) (iArr2[0] - iArr[0]), (float) (iArr2[1] - iArr[1]));
            canvas.clipRect(new Rect(0, 0, this.f137c.getWidth(), this.f137c.getHeight()));
            super.dispatchDraw(canvas);
            int size = this.f138d == null ? 0 : this.f138d.size();
            for (int i = 0; i < size; i++) {
                this.f138d.get(i).draw(canvas);
            }
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        }

        private void a(int[] iArr) {
            int[] iArr2 = new int[2];
            int[] iArr3 = new int[2];
            ViewGroup viewGroup = (ViewGroup) getParent();
            this.f136b.getLocationOnScreen(iArr2);
            this.f137c.getLocationOnScreen(iArr3);
            iArr[0] = iArr3[0] - iArr2[0];
            iArr[1] = iArr3[1] - iArr2[1];
        }

        public ViewParent invalidateChildInParent(int[] iArr, Rect rect) {
            if (this.f136b != null) {
                rect.offset(iArr[0], iArr[1]);
                if (this.f136b instanceof ViewGroup) {
                    iArr[0] = 0;
                    iArr[1] = 0;
                    int[] iArr2 = new int[2];
                    a(iArr2);
                    rect.offset(iArr2[0], iArr2[1]);
                    return super.invalidateChildInParent(iArr, rect);
                }
                invalidate(rect);
            }
            return null;
        }
    }
}
