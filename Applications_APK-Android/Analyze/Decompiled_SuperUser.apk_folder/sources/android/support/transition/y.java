package android.support.transition;

import android.animation.TimeInterpolator;
import android.annotation.TargetApi;
import android.support.transition.TransitionPort;
import android.util.AndroidRuntimeException;
import android.view.View;
import android.view.ViewGroup;
import com.kingouser.com.util.ShellUtils;
import java.util.ArrayList;
import java.util.Iterator;

@TargetApi(14)
/* compiled from: TransitionSetPort */
class y extends TransitionPort {

    /* renamed from: a  reason: collision with root package name */
    ArrayList<TransitionPort> f209a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    int f210b;

    /* renamed from: c  reason: collision with root package name */
    boolean f211c = false;
    private boolean w = true;

    public y c(int i) {
        switch (i) {
            case 0:
                this.w = true;
                break;
            case 1:
                this.w = false;
                break;
            default:
                throw new AndroidRuntimeException("Invalid parameter for TransitionSet ordering: " + i);
        }
        return this;
    }

    public y a(TransitionPort transitionPort) {
        if (transitionPort != null) {
            this.f209a.add(transitionPort);
            transitionPort.o = this;
            if (this.f120e >= 0) {
                transitionPort.a(this.f120e);
            }
        }
        return this;
    }

    /* renamed from: c */
    public y a(long j) {
        super.a(j);
        if (this.f120e >= 0) {
            int size = this.f209a.size();
            for (int i = 0; i < size; i++) {
                this.f209a.get(i).a(j);
            }
        }
        return this;
    }

    /* renamed from: d */
    public y b(long j) {
        return (y) super.b(j);
    }

    /* renamed from: b */
    public y a(TimeInterpolator timeInterpolator) {
        return (y) super.a(timeInterpolator);
    }

    /* renamed from: e */
    public y a(View view) {
        return (y) super.a(view);
    }

    /* renamed from: d */
    public y a(int i) {
        return (y) super.a(i);
    }

    /* renamed from: c */
    public y a(TransitionPort.c cVar) {
        return (y) super.a(cVar);
    }

    /* renamed from: e */
    public y b(int i) {
        return (y) super.b(i);
    }

    /* renamed from: f */
    public y b(View view) {
        return (y) super.b(view);
    }

    /* renamed from: d */
    public y b(TransitionPort.c cVar) {
        return (y) super.b(cVar);
    }

    private void m() {
        a aVar = new a(this);
        Iterator<TransitionPort> it = this.f209a.iterator();
        while (it.hasNext()) {
            it.next().a(aVar);
        }
        this.f210b = this.f209a.size();
    }

    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup, aa aaVar, aa aaVar2) {
        Iterator<TransitionPort> it = this.f209a.iterator();
        while (it.hasNext()) {
            it.next().a(viewGroup, aaVar, aaVar2);
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (this.f209a.isEmpty()) {
            h();
            i();
            return;
        }
        m();
        if (!this.w) {
            int i = 1;
            while (true) {
                int i2 = i;
                if (i2 >= this.f209a.size()) {
                    break;
                }
                final TransitionPort transitionPort = this.f209a.get(i2);
                this.f209a.get(i2 - 1).a(new TransitionPort.TransitionListenerAdapter() {
                    public void a(TransitionPort transitionPort) {
                        transitionPort.e();
                        transitionPort.b(this);
                    }
                });
                i = i2 + 1;
            }
            TransitionPort transitionPort2 = this.f209a.get(0);
            if (transitionPort2 != null) {
                transitionPort2.e();
                return;
            }
            return;
        }
        Iterator<TransitionPort> it = this.f209a.iterator();
        while (it.hasNext()) {
            it.next().e();
        }
    }

    public void a(z zVar) {
        int id = zVar.f216b.getId();
        if (a(zVar.f216b, (long) id)) {
            Iterator<TransitionPort> it = this.f209a.iterator();
            while (it.hasNext()) {
                TransitionPort next = it.next();
                if (next.a(zVar.f216b, (long) id)) {
                    next.a(zVar);
                }
            }
        }
    }

    public void b(z zVar) {
        int id = zVar.f216b.getId();
        if (a(zVar.f216b, (long) id)) {
            Iterator<TransitionPort> it = this.f209a.iterator();
            while (it.hasNext()) {
                TransitionPort next = it.next();
                if (next.a(zVar.f216b, (long) id)) {
                    next.b(zVar);
                }
            }
        }
    }

    public void c(View view) {
        super.c(view);
        int size = this.f209a.size();
        for (int i = 0; i < size; i++) {
            this.f209a.get(i).c(view);
        }
    }

    public void d(View view) {
        super.d(view);
        int size = this.f209a.size();
        for (int i = 0; i < size; i++) {
            this.f209a.get(i).d(view);
        }
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        String a2 = super.a(str);
        int i = 0;
        while (i < this.f209a.size()) {
            String str2 = a2 + ShellUtils.COMMAND_LINE_END + this.f209a.get(i).a(str + "  ");
            i++;
            a2 = str2;
        }
        return a2;
    }

    /* renamed from: l */
    public y j() {
        y yVar = (y) super.clone();
        yVar.f209a = new ArrayList<>();
        int size = this.f209a.size();
        for (int i = 0; i < size; i++) {
            yVar.a(this.f209a.get(i).clone());
        }
        return yVar;
    }

    /* compiled from: TransitionSetPort */
    static class a extends TransitionPort.TransitionListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        y f214a;

        a(y yVar) {
            this.f214a = yVar;
        }

        public void d(TransitionPort transitionPort) {
            if (!this.f214a.f211c) {
                this.f214a.h();
                this.f214a.f211c = true;
            }
        }

        public void a(TransitionPort transitionPort) {
            y yVar = this.f214a;
            yVar.f210b--;
            if (this.f214a.f210b == 0) {
                this.f214a.f211c = false;
                this.f214a.i();
            }
            transitionPort.b(this);
        }
    }
}
