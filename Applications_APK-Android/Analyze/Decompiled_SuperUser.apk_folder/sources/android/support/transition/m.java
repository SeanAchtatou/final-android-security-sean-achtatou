package android.support.transition;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

/* compiled from: TransitionImpl */
abstract class m {
    public abstract long a();

    public abstract Animator a(ViewGroup viewGroup, z zVar, z zVar2);

    public abstract m a(int i);

    public abstract m a(int i, boolean z);

    public abstract m a(long j);

    public abstract m a(TimeInterpolator timeInterpolator);

    public abstract m a(o oVar);

    public abstract m a(View view);

    public abstract m a(View view, boolean z);

    public abstract m a(Class cls, boolean z);

    public abstract void a(n nVar, Object obj);

    public abstract TimeInterpolator b();

    public abstract m b(int i);

    public abstract m b(int i, boolean z);

    public abstract m b(long j);

    public abstract m b(o oVar);

    public abstract m b(View view);

    public abstract m b(View view, boolean z);

    public abstract m b(Class cls, boolean z);

    public abstract void b(z zVar);

    public abstract z c(View view, boolean z);

    public abstract String c();

    public abstract void c(z zVar);

    public abstract long d();

    public abstract List<Integer> e();

    public abstract List<View> f();

    public abstract String[] g();

    m() {
    }

    public void a(n nVar) {
        a(nVar, (Object) null);
    }
}
