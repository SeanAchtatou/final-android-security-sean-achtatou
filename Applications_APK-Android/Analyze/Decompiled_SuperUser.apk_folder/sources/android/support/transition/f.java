package android.support.transition;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.transition.Fade;
import android.view.ViewGroup;

@TargetApi(19)
/* compiled from: FadeKitKat */
class f extends p implements ae {
    public f(n nVar) {
        a(nVar, new Fade());
    }

    public f(n nVar, int i) {
        a(nVar, new Fade(i));
    }

    public boolean a(z zVar) {
        return ((Fade) this.f191a).isVisible(d(zVar));
    }

    public Animator a(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return ((Fade) this.f191a).onAppear(viewGroup, d(zVar), i, d(zVar2), i2);
    }

    public Animator b(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return ((Fade) this.f191a).onDisappear(viewGroup, d(zVar), i, d(zVar2), i2);
    }
}
