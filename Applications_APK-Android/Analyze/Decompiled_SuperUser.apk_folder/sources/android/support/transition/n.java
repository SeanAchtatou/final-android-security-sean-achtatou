package android.support.transition;

import android.animation.Animator;
import android.view.ViewGroup;

/* compiled from: TransitionInterface */
interface n {
    void captureEndValues(z zVar);

    void captureStartValues(z zVar);

    Animator createAnimator(ViewGroup viewGroup, z zVar, z zVar2);
}
