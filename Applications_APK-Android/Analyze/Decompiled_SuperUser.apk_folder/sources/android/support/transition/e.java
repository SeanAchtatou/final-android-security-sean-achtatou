package android.support.transition;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.view.ViewGroup;

@TargetApi(14)
/* compiled from: FadeIcs */
class e extends l implements ae {
    public e(n nVar) {
        a(nVar, new g());
    }

    public e(n nVar, int i) {
        a(nVar, new g(i));
    }

    public boolean a(z zVar) {
        return ((g) this.f185a).c(zVar);
    }

    public Animator a(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return ((g) this.f185a).a(viewGroup, zVar, i, zVar2, i2);
    }

    public Animator b(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return ((g) this.f185a).b(viewGroup, zVar, i, zVar, i);
    }
}
