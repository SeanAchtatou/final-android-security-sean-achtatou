package android.support.transition;

import android.annotation.TargetApi;
import android.support.transition.h;
import android.view.View;
import android.view.ViewGroup;

@TargetApi(14)
/* compiled from: ScenePort */
final class j {

    /* renamed from: a  reason: collision with root package name */
    Runnable f183a;

    /* renamed from: b  reason: collision with root package name */
    private ViewGroup f184b;

    static void a(View view, j jVar) {
        view.setTag(h.a.transition_current_scene, jVar);
    }

    static j a(View view) {
        return (j) view.getTag(h.a.transition_current_scene);
    }

    public void a() {
        if (a(this.f184b) == this && this.f183a != null) {
            this.f183a.run();
        }
    }
}
