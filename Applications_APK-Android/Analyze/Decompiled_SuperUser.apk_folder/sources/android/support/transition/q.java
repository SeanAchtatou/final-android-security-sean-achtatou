package android.support.transition;

import android.os.Build;
import android.view.ViewGroup;

/* compiled from: TransitionManager */
public class q {

    /* renamed from: a  reason: collision with root package name */
    private static t f197a;

    static {
        if (Build.VERSION.SDK_INT < 19) {
            f197a = new s();
        } else {
            f197a = new u();
        }
    }

    public static void a(ViewGroup viewGroup, Transition transition) {
        f197a.a(viewGroup, transition == null ? null : transition.mImpl);
    }
}
