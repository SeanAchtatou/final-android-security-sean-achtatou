package android.support.transition;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

public abstract class Transition implements n {
    m mImpl;

    public interface TransitionListener extends o<Transition> {
    }

    public abstract void captureEndValues(z zVar);

    public abstract void captureStartValues(z zVar);

    public Transition() {
        this(false);
    }

    Transition(boolean z) {
        if (!z) {
            if (Build.VERSION.SDK_INT >= 23) {
                this.mImpl = new k();
            } else if (Build.VERSION.SDK_INT >= 19) {
                this.mImpl = new p();
            } else {
                this.mImpl = new l();
            }
            this.mImpl.a(this);
        }
    }

    public Transition addListener(TransitionListener transitionListener) {
        this.mImpl.a(transitionListener);
        return this;
    }

    public Transition addTarget(View view) {
        this.mImpl.a(view);
        return this;
    }

    public Transition addTarget(int i) {
        this.mImpl.b(i);
        return this;
    }

    public Animator createAnimator(ViewGroup viewGroup, z zVar, z zVar2) {
        return null;
    }

    public Transition excludeChildren(View view, boolean z) {
        this.mImpl.a(view, z);
        return this;
    }

    public Transition excludeChildren(int i, boolean z) {
        this.mImpl.a(i, z);
        return this;
    }

    public Transition excludeChildren(Class cls, boolean z) {
        this.mImpl.a(cls, z);
        return this;
    }

    public Transition excludeTarget(View view, boolean z) {
        this.mImpl.b(view, z);
        return this;
    }

    public Transition excludeTarget(int i, boolean z) {
        this.mImpl.b(i, z);
        return this;
    }

    public Transition excludeTarget(Class cls, boolean z) {
        this.mImpl.b(cls, z);
        return this;
    }

    public long getDuration() {
        return this.mImpl.a();
    }

    public Transition setDuration(long j) {
        this.mImpl.a(j);
        return this;
    }

    public TimeInterpolator getInterpolator() {
        return this.mImpl.b();
    }

    public Transition setInterpolator(TimeInterpolator timeInterpolator) {
        this.mImpl.a(timeInterpolator);
        return this;
    }

    public String getName() {
        return this.mImpl.c();
    }

    public long getStartDelay() {
        return this.mImpl.d();
    }

    public Transition setStartDelay(long j) {
        this.mImpl.b(j);
        return this;
    }

    public List<Integer> getTargetIds() {
        return this.mImpl.e();
    }

    public List<View> getTargets() {
        return this.mImpl.f();
    }

    public String[] getTransitionProperties() {
        return this.mImpl.g();
    }

    public z getTransitionValues(View view, boolean z) {
        return this.mImpl.c(view, z);
    }

    public Transition removeListener(TransitionListener transitionListener) {
        this.mImpl.b(transitionListener);
        return this;
    }

    public Transition removeTarget(View view) {
        this.mImpl.b(view);
        return this;
    }

    public Transition removeTarget(int i) {
        this.mImpl.a(i);
        return this;
    }

    public String toString() {
        return this.mImpl.toString();
    }
}
