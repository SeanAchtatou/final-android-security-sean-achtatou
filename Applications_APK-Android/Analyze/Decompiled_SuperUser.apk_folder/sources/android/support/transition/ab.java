package android.support.transition;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

@TargetApi(14)
/* compiled from: ViewGroupOverlay */
class ab extends ac {
    ab(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    public static ab a(ViewGroup viewGroup) {
        return (ab) ac.d(viewGroup);
    }

    public void a(View view) {
        this.f134a.a(view);
    }

    public void b(View view) {
        this.f134a.b(view);
    }
}
