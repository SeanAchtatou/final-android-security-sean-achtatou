package android.support.transition;

import android.annotation.TargetApi;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.e;
import android.util.SparseArray;
import android.view.View;

@TargetApi(14)
/* compiled from: TransitionValuesMaps */
class aa {

    /* renamed from: a  reason: collision with root package name */
    public ArrayMap<View, z> f131a = new ArrayMap<>();

    /* renamed from: b  reason: collision with root package name */
    public SparseArray<z> f132b = new SparseArray<>();

    /* renamed from: c  reason: collision with root package name */
    public e<z> f133c = new e<>();

    aa() {
    }
}
