package android.support.transition;

import android.annotation.TargetApi;
import android.support.transition.TransitionPort;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ag;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

@TargetApi(14)
/* compiled from: TransitionManagerPort */
class r {

    /* renamed from: a  reason: collision with root package name */
    static ArrayList<ViewGroup> f198a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f199b = new String[0];

    /* renamed from: c  reason: collision with root package name */
    private static String f200c = "TransitionManager";

    /* renamed from: d  reason: collision with root package name */
    private static TransitionPort f201d = new a();

    /* renamed from: e  reason: collision with root package name */
    private static ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<TransitionPort>>>> f202e = new ThreadLocal<>();

    static ArrayMap<ViewGroup, ArrayList<TransitionPort>> a() {
        WeakReference weakReference = f202e.get();
        if (weakReference == null || weakReference.get() == null) {
            weakReference = new WeakReference(new ArrayMap());
            f202e.set(weakReference);
        }
        return (ArrayMap) weakReference.get();
    }

    private static void b(ViewGroup viewGroup, TransitionPort transitionPort) {
        if (transitionPort != null && viewGroup != null) {
            a aVar = new a(transitionPort, viewGroup);
            viewGroup.addOnAttachStateChangeListener(aVar);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(aVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.transition.TransitionPort.a(android.view.ViewGroup, boolean):void
     arg types: [android.view.ViewGroup, int]
     candidates:
      android.support.transition.TransitionPort.a(android.animation.Animator, android.support.v4.util.ArrayMap<android.animation.Animator, android.support.transition.TransitionPort$a>):void
      android.support.transition.TransitionPort.a(int, boolean):android.support.transition.TransitionPort
      android.support.transition.TransitionPort.a(android.view.View, boolean):android.support.transition.TransitionPort
      android.support.transition.TransitionPort.a(java.lang.Class, boolean):android.support.transition.TransitionPort
      android.support.transition.TransitionPort.a(android.view.View, long):boolean
      android.support.transition.TransitionPort.a(android.view.ViewGroup, boolean):void */
    private static void c(ViewGroup viewGroup, TransitionPort transitionPort) {
        ArrayList arrayList = a().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                ((TransitionPort) it.next()).c(viewGroup);
            }
        }
        if (transitionPort != null) {
            transitionPort.a(viewGroup, true);
        }
        j a2 = j.a(viewGroup);
        if (a2 != null) {
            a2.a();
        }
    }

    public static void a(ViewGroup viewGroup, TransitionPort transitionPort) {
        if (!f198a.contains(viewGroup) && ag.F(viewGroup)) {
            f198a.add(viewGroup);
            if (transitionPort == null) {
                transitionPort = f201d;
            }
            TransitionPort j = transitionPort.clone();
            c(viewGroup, j);
            j.a(viewGroup, null);
            b(viewGroup, j);
        }
    }

    /* compiled from: TransitionManagerPort */
    private static class a implements View.OnAttachStateChangeListener, ViewTreeObserver.OnPreDrawListener {

        /* renamed from: a  reason: collision with root package name */
        TransitionPort f203a;

        /* renamed from: b  reason: collision with root package name */
        ViewGroup f204b;

        a(TransitionPort transitionPort, ViewGroup viewGroup) {
            this.f203a = transitionPort;
            this.f204b = viewGroup;
        }

        private void a() {
            this.f204b.getViewTreeObserver().removeOnPreDrawListener(this);
            this.f204b.removeOnAttachStateChangeListener(this);
        }

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            a();
            r.f198a.remove(this.f204b);
            ArrayList arrayList = r.a().get(this.f204b);
            if (arrayList != null && arrayList.size() > 0) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((TransitionPort) it.next()).d(this.f204b);
                }
            }
            this.f203a.a(true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.transition.TransitionPort.a(android.view.ViewGroup, boolean):void
         arg types: [android.view.ViewGroup, int]
         candidates:
          android.support.transition.TransitionPort.a(android.animation.Animator, android.support.v4.util.ArrayMap<android.animation.Animator, android.support.transition.TransitionPort$a>):void
          android.support.transition.TransitionPort.a(int, boolean):android.support.transition.TransitionPort
          android.support.transition.TransitionPort.a(android.view.View, boolean):android.support.transition.TransitionPort
          android.support.transition.TransitionPort.a(java.lang.Class, boolean):android.support.transition.TransitionPort
          android.support.transition.TransitionPort.a(android.view.View, long):boolean
          android.support.transition.TransitionPort.a(android.view.ViewGroup, boolean):void */
        public boolean onPreDraw() {
            ArrayList arrayList;
            ArrayList arrayList2;
            a();
            r.f198a.remove(this.f204b);
            final ArrayMap<ViewGroup, ArrayList<TransitionPort>> a2 = r.a();
            ArrayList arrayList3 = a2.get(this.f204b);
            if (arrayList3 == null) {
                ArrayList arrayList4 = new ArrayList();
                a2.put(this.f204b, arrayList4);
                arrayList = arrayList4;
                arrayList2 = null;
            } else if (arrayList3.size() > 0) {
                arrayList = arrayList3;
                arrayList2 = new ArrayList(arrayList3);
            } else {
                arrayList = arrayList3;
                arrayList2 = null;
            }
            arrayList.add(this.f203a);
            this.f203a.a(new TransitionPort.TransitionListenerAdapter() {
                public void a(TransitionPort transitionPort) {
                    ((ArrayList) a2.get(a.this.f204b)).remove(transitionPort);
                }
            });
            this.f203a.a(this.f204b, false);
            if (arrayList2 != null) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    ((TransitionPort) it.next()).d(this.f204b);
                }
            }
            this.f203a.a(this.f204b);
            return true;
        }
    }
}
