package android.support.transition;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.view.View;
import android.view.ViewGroup;

@TargetApi(14)
/* compiled from: VisibilityPort */
abstract class ah extends TransitionPort {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f142a = {"android:visibility:visibility", "android:visibility:parent"};

    ah() {
    }

    public String[] a() {
        return f142a;
    }

    private void d(z zVar) {
        zVar.f215a.put("android:visibility:visibility", Integer.valueOf(zVar.f216b.getVisibility()));
        zVar.f215a.put("android:visibility:parent", zVar.f216b.getParent());
    }

    public void a(z zVar) {
        d(zVar);
    }

    public void b(z zVar) {
        d(zVar);
    }

    public boolean c(z zVar) {
        if (zVar == null) {
            return false;
        }
        return ((Integer) zVar.f215a.get("android:visibility:visibility")).intValue() == 0 && ((View) zVar.f215a.get("android:visibility:parent")) != null;
    }

    private a a(z zVar, z zVar2) {
        a aVar = new a();
        aVar.f143a = false;
        aVar.f144b = false;
        if (zVar != null) {
            aVar.f145c = ((Integer) zVar.f215a.get("android:visibility:visibility")).intValue();
            aVar.f147e = (ViewGroup) zVar.f215a.get("android:visibility:parent");
        } else {
            aVar.f145c = -1;
            aVar.f147e = null;
        }
        if (zVar2 != null) {
            aVar.f146d = ((Integer) zVar2.f215a.get("android:visibility:visibility")).intValue();
            aVar.f148f = (ViewGroup) zVar2.f215a.get("android:visibility:parent");
        } else {
            aVar.f146d = -1;
            aVar.f148f = null;
        }
        if (!(zVar == null || zVar2 == null)) {
            if (aVar.f145c == aVar.f146d && aVar.f147e == aVar.f148f) {
                return aVar;
            }
            if (aVar.f145c != aVar.f146d) {
                if (aVar.f145c == 0) {
                    aVar.f144b = false;
                    aVar.f143a = true;
                } else if (aVar.f146d == 0) {
                    aVar.f144b = true;
                    aVar.f143a = true;
                }
            } else if (aVar.f147e != aVar.f148f) {
                if (aVar.f148f == null) {
                    aVar.f144b = false;
                    aVar.f143a = true;
                } else if (aVar.f147e == null) {
                    aVar.f144b = true;
                    aVar.f143a = true;
                }
            }
        }
        if (zVar == null) {
            aVar.f144b = true;
            aVar.f143a = true;
        } else if (zVar2 == null) {
            aVar.f144b = false;
            aVar.f143a = true;
        }
        return aVar;
    }

    public Animator a(ViewGroup viewGroup, z zVar, z zVar2) {
        View view;
        int i;
        boolean z;
        boolean z2 = false;
        int i2 = -1;
        a a2 = a(zVar, zVar2);
        if (a2.f143a) {
            if (this.f123h.size() > 0 || this.f122g.size() > 0) {
                View view2 = zVar != null ? zVar.f216b : null;
                if (zVar2 != null) {
                    view = zVar2.f216b;
                } else {
                    view = null;
                }
                if (view2 != null) {
                    i = view2.getId();
                } else {
                    i = -1;
                }
                if (view != null) {
                    i2 = view.getId();
                }
                if (a(view2, (long) i) || a(view, (long) i2)) {
                    z = true;
                } else {
                    z = false;
                }
                z2 = z;
            }
            if (!(!z2 && a2.f147e == null && a2.f148f == null)) {
                if (a2.f144b) {
                    return a(viewGroup, zVar, a2.f145c, zVar2, a2.f146d);
                }
                return b(viewGroup, zVar, a2.f145c, zVar2, a2.f146d);
            }
        }
        return null;
    }

    public Animator a(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return null;
    }

    public Animator b(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return null;
    }

    /* compiled from: VisibilityPort */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        boolean f143a;

        /* renamed from: b  reason: collision with root package name */
        boolean f144b;

        /* renamed from: c  reason: collision with root package name */
        int f145c;

        /* renamed from: d  reason: collision with root package name */
        int f146d;

        /* renamed from: e  reason: collision with root package name */
        ViewGroup f147e;

        /* renamed from: f  reason: collision with root package name */
        ViewGroup f148f;

        a() {
        }
    }
}
