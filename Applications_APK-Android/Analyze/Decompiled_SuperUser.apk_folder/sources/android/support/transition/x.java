package android.support.transition;

import android.annotation.TargetApi;
import android.transition.TransitionSet;

@TargetApi(19)
/* compiled from: TransitionSetKitKat */
class x extends p implements w {

    /* renamed from: c  reason: collision with root package name */
    private TransitionSet f208c = new TransitionSet();

    public x(n nVar) {
        a(nVar, this.f208c);
    }

    /* renamed from: c */
    public x d(int i) {
        this.f208c.setOrdering(i);
        return this;
    }

    /* renamed from: a */
    public x b(m mVar) {
        this.f208c.addTransition(((p) mVar).f191a);
        return this;
    }
}
