package android.support.transition;

import android.annotation.TargetApi;

@TargetApi(14)
/* compiled from: TransitionSetIcs */
class v extends l implements w {

    /* renamed from: c  reason: collision with root package name */
    private y f207c = new y();

    public v(n nVar) {
        a(nVar, this.f207c);
    }

    /* renamed from: c */
    public v d(int i) {
        this.f207c.c(i);
        return this;
    }

    /* renamed from: a */
    public v b(m mVar) {
        this.f207c.a(((l) mVar).f185a);
        return this;
    }
}
