package android.support.transition;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.transition.TransitionValues;
import android.transition.Visibility;
import android.view.ViewGroup;

@TargetApi(19)
/* compiled from: VisibilityKitKat */
class ag extends p implements ae {
    ag() {
    }

    public void a(n nVar, Object obj) {
        this.f192b = nVar;
        if (obj == null) {
            this.f191a = new a((af) nVar);
        } else {
            this.f191a = (Visibility) obj;
        }
    }

    public boolean a(z zVar) {
        return ((Visibility) this.f191a).isVisible(d(zVar));
    }

    public Animator a(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return ((Visibility) this.f191a).onAppear(viewGroup, d(zVar), i, d(zVar2), i2);
    }

    public Animator b(ViewGroup viewGroup, z zVar, int i, z zVar2, int i2) {
        return ((Visibility) this.f191a).onDisappear(viewGroup, d(zVar), i, d(zVar2), i2);
    }

    /* compiled from: VisibilityKitKat */
    private static class a extends Visibility {

        /* renamed from: a  reason: collision with root package name */
        private final af f141a;

        a(af afVar) {
            this.f141a = afVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.transition.p.a(android.support.transition.n, android.transition.TransitionValues):void
         arg types: [android.support.transition.af, android.transition.TransitionValues]
         candidates:
          android.support.transition.p.a(android.support.transition.z, android.transition.TransitionValues):void
          android.support.transition.p.a(android.transition.TransitionValues, android.support.transition.z):void
          android.support.transition.p.a(int, boolean):android.support.transition.m
          android.support.transition.p.a(android.view.View, boolean):android.support.transition.m
          android.support.transition.p.a(java.lang.Class, boolean):android.support.transition.m
          android.support.transition.p.a(android.support.transition.n, java.lang.Object):void
          android.support.transition.m.a(int, boolean):android.support.transition.m
          android.support.transition.m.a(android.view.View, boolean):android.support.transition.m
          android.support.transition.m.a(java.lang.Class, boolean):android.support.transition.m
          android.support.transition.m.a(android.support.transition.n, java.lang.Object):void
          android.support.transition.p.a(android.support.transition.n, android.transition.TransitionValues):void */
        public void captureStartValues(TransitionValues transitionValues) {
            p.a((n) this.f141a, transitionValues);
        }

        public void captureEndValues(TransitionValues transitionValues) {
            p.b(this.f141a, transitionValues);
        }

        public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
            return this.f141a.createAnimator(viewGroup, p.a(transitionValues), p.a(transitionValues2));
        }

        public boolean isVisible(TransitionValues transitionValues) {
            if (transitionValues == null) {
                return false;
            }
            z zVar = new z();
            p.a(transitionValues, zVar);
            return this.f141a.a(zVar);
        }

        public Animator onAppear(ViewGroup viewGroup, TransitionValues transitionValues, int i, TransitionValues transitionValues2, int i2) {
            return this.f141a.a(viewGroup, p.a(transitionValues), i, p.a(transitionValues2), i2);
        }

        public Animator onDisappear(ViewGroup viewGroup, TransitionValues transitionValues, int i, TransitionValues transitionValues2, int i2) {
            return this.f141a.b(viewGroup, p.a(transitionValues), i, p.a(transitionValues2), i2);
        }
    }
}
