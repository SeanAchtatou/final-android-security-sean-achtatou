package android.support.transition;

import android.annotation.TargetApi;
import android.os.IBinder;
import android.view.View;

@TargetApi(14)
/* compiled from: WindowIdPort */
class ai {

    /* renamed from: a  reason: collision with root package name */
    private final IBinder f149a;

    private ai(IBinder iBinder) {
        this.f149a = iBinder;
    }

    static ai a(View view) {
        return new ai(view.getWindowToken());
    }

    public boolean equals(Object obj) {
        return (obj instanceof ai) && ((ai) obj).f149a.equals(this.f149a);
    }
}
