package android.support.transition;

import android.annotation.TargetApi;
import android.transition.TransitionManager;
import android.view.ViewGroup;

@TargetApi(19)
/* compiled from: TransitionManagerStaticsKitKat */
class u extends t {
    u() {
    }

    public void a(ViewGroup viewGroup, m mVar) {
        TransitionManager.beginDelayedTransition(viewGroup, mVar == null ? null : ((p) mVar).f191a);
    }
}
