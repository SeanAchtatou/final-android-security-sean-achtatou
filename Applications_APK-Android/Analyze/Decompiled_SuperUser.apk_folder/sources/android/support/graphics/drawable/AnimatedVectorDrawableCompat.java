package android.support.graphics.drawable;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.util.ArrayMap;
import android.util.AttributeSet;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;

@SuppressLint({"NewApi"})
public class AnimatedVectorDrawableCompat extends e implements b {

    /* renamed from: a  reason: collision with root package name */
    final Drawable.Callback f45a;

    /* renamed from: c  reason: collision with root package name */
    private a f46c;

    /* renamed from: d  reason: collision with root package name */
    private Context f47d;

    /* renamed from: e  reason: collision with root package name */
    private ArgbEvaluator f48e;

    /* renamed from: f  reason: collision with root package name */
    private Animator.AnimatorListener f49f;

    /* renamed from: g  reason: collision with root package name */
    private ArrayList<Object> f50g;

    public /* bridge */ /* synthetic */ void clearColorFilter() {
        super.clearColorFilter();
    }

    public /* bridge */ /* synthetic */ ColorFilter getColorFilter() {
        return super.getColorFilter();
    }

    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    public /* bridge */ /* synthetic */ boolean getPadding(Rect rect) {
        return super.getPadding(rect);
    }

    public /* bridge */ /* synthetic */ int[] getState() {
        return super.getState();
    }

    public /* bridge */ /* synthetic */ Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    public /* bridge */ /* synthetic */ void jumpToCurrentState() {
        super.jumpToCurrentState();
    }

    public /* bridge */ /* synthetic */ void setChangingConfigurations(int i) {
        super.setChangingConfigurations(i);
    }

    public /* bridge */ /* synthetic */ void setColorFilter(int i, PorterDuff.Mode mode) {
        super.setColorFilter(i, mode);
    }

    public /* bridge */ /* synthetic */ void setFilterBitmap(boolean z) {
        super.setFilterBitmap(z);
    }

    public /* bridge */ /* synthetic */ void setHotspot(float f2, float f3) {
        super.setHotspot(f2, f3);
    }

    public /* bridge */ /* synthetic */ void setHotspotBounds(int i, int i2, int i3, int i4) {
        super.setHotspotBounds(i, i2, i3, i4);
    }

    public /* bridge */ /* synthetic */ boolean setState(int[] iArr) {
        return super.setState(iArr);
    }

    AnimatedVectorDrawableCompat() {
        this(null, null, null);
    }

    private AnimatedVectorDrawableCompat(Context context) {
        this(context, null, null);
    }

    private AnimatedVectorDrawableCompat(Context context, a aVar, Resources resources) {
        this.f48e = null;
        this.f49f = null;
        this.f50g = null;
        this.f45a = new Drawable.Callback() {
            public void invalidateDrawable(Drawable drawable) {
                AnimatedVectorDrawableCompat.this.invalidateSelf();
            }

            public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
                AnimatedVectorDrawableCompat.this.scheduleSelf(runnable, j);
            }

            public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
                AnimatedVectorDrawableCompat.this.unscheduleSelf(runnable);
            }
        };
        this.f47d = context;
        if (aVar != null) {
            this.f46c = aVar;
        } else {
            this.f46c = new a(context, aVar, this.f45a, resources);
        }
    }

    public Drawable mutate() {
        if (this.f108b != null) {
            this.f108b.mutate();
        }
        return this;
    }

    public static AnimatedVectorDrawableCompat a(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat(context);
        animatedVectorDrawableCompat.inflate(resources, xmlPullParser, attributeSet, theme);
        return animatedVectorDrawableCompat;
    }

    public Drawable.ConstantState getConstantState() {
        if (this.f108b != null) {
            return new b(this.f108b.getConstantState());
        }
        return null;
    }

    public int getChangingConfigurations() {
        if (this.f108b != null) {
            return this.f108b.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f46c.f52a;
    }

    public void draw(Canvas canvas) {
        if (this.f108b != null) {
            this.f108b.draw(canvas);
            return;
        }
        this.f46c.f53b.draw(canvas);
        if (this.f46c.f54c.isStarted()) {
            invalidateSelf();
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f108b != null) {
            this.f108b.setBounds(rect);
        } else {
            this.f46c.f53b.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        if (this.f108b != null) {
            return this.f108b.setState(iArr);
        }
        return this.f46c.f53b.setState(iArr);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        if (this.f108b != null) {
            return this.f108b.setLevel(i);
        }
        return this.f46c.f53b.setLevel(i);
    }

    public int getAlpha() {
        if (this.f108b != null) {
            return android.support.v4.b.a.a.c(this.f108b);
        }
        return this.f46c.f53b.getAlpha();
    }

    public void setAlpha(int i) {
        if (this.f108b != null) {
            this.f108b.setAlpha(i);
        } else {
            this.f46c.f53b.setAlpha(i);
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f108b != null) {
            this.f108b.setColorFilter(colorFilter);
        } else {
            this.f46c.f53b.setColorFilter(colorFilter);
        }
    }

    public void setTint(int i) {
        if (this.f108b != null) {
            android.support.v4.b.a.a.a(this.f108b, i);
        } else {
            this.f46c.f53b.setTint(i);
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.f108b != null) {
            android.support.v4.b.a.a.a(this.f108b, colorStateList);
        } else {
            this.f46c.f53b.setTintList(colorStateList);
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (this.f108b != null) {
            android.support.v4.b.a.a.a(this.f108b, mode);
        } else {
            this.f46c.f53b.setTintMode(mode);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        if (this.f108b != null) {
            return this.f108b.setVisible(z, z2);
        }
        this.f46c.f53b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    public boolean isStateful() {
        if (this.f108b != null) {
            return this.f108b.isStateful();
        }
        return this.f46c.f53b.isStateful();
    }

    public int getOpacity() {
        if (this.f108b != null) {
            return this.f108b.getOpacity();
        }
        return this.f46c.f53b.getOpacity();
    }

    public int getIntrinsicWidth() {
        if (this.f108b != null) {
            return this.f108b.getIntrinsicWidth();
        }
        return this.f46c.f53b.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        if (this.f108b != null) {
            return this.f108b.getIntrinsicHeight();
        }
        return this.f46c.f53b.getIntrinsicHeight();
    }

    public boolean isAutoMirrored() {
        if (this.f108b != null) {
            return android.support.v4.b.a.a.b(this.f108b);
        }
        return this.f46c.f53b.isAutoMirrored();
    }

    public void setAutoMirrored(boolean z) {
        if (this.f108b != null) {
            this.f108b.setAutoMirrored(z);
        } else {
            this.f46c.f53b.setAutoMirrored(z);
        }
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        if (this.f108b != null) {
            android.support.v4.b.a.a.a(this.f108b, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    TypedArray a2 = e.a(resources, theme, attributeSet, a.f102e);
                    int resourceId = a2.getResourceId(0, 0);
                    if (resourceId != 0) {
                        VectorDrawableCompat a3 = VectorDrawableCompat.a(resources, resourceId, theme);
                        a3.a(false);
                        a3.setCallback(this.f45a);
                        if (this.f46c.f53b != null) {
                            this.f46c.f53b.setCallback(null);
                        }
                        this.f46c.f53b = a3;
                    }
                    a2.recycle();
                } else if ("target".equals(name)) {
                    TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, a.f103f);
                    String string = obtainAttributes.getString(0);
                    int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        if (this.f47d != null) {
                            a(string, AnimatorInflater.loadAnimator(this.f47d, resourceId2));
                        } else {
                            obtainAttributes.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    obtainAttributes.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
        this.f46c.a();
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        inflate(resources, xmlPullParser, attributeSet, null);
    }

    public void applyTheme(Resources.Theme theme) {
        if (this.f108b != null) {
            android.support.v4.b.a.a.a(this.f108b, theme);
        }
    }

    public boolean canApplyTheme() {
        if (this.f108b != null) {
            return android.support.v4.b.a.a.d(this.f108b);
        }
        return false;
    }

    private static class b extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        private final Drawable.ConstantState f57a;

        public b(Drawable.ConstantState constantState) {
            this.f57a = constantState;
        }

        public Drawable newDrawable() {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.f108b = this.f57a.newDrawable();
            animatedVectorDrawableCompat.f108b.setCallback(animatedVectorDrawableCompat.f45a);
            return animatedVectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.f108b = this.f57a.newDrawable(resources);
            animatedVectorDrawableCompat.f108b.setCallback(animatedVectorDrawableCompat.f45a);
            return animatedVectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.f108b = this.f57a.newDrawable(resources, theme);
            animatedVectorDrawableCompat.f108b.setCallback(animatedVectorDrawableCompat.f45a);
            return animatedVectorDrawableCompat;
        }

        public boolean canApplyTheme() {
            return this.f57a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f57a.getChangingConfigurations();
        }
    }

    private static class a extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        int f52a;

        /* renamed from: b  reason: collision with root package name */
        VectorDrawableCompat f53b;

        /* renamed from: c  reason: collision with root package name */
        AnimatorSet f54c;

        /* renamed from: d  reason: collision with root package name */
        ArrayMap<Animator, String> f55d;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public ArrayList<Animator> f56e;

        public a(Context context, a aVar, Drawable.Callback callback, Resources resources) {
            if (aVar != null) {
                this.f52a = aVar.f52a;
                if (aVar.f53b != null) {
                    Drawable.ConstantState constantState = aVar.f53b.getConstantState();
                    if (resources != null) {
                        this.f53b = (VectorDrawableCompat) constantState.newDrawable(resources);
                    } else {
                        this.f53b = (VectorDrawableCompat) constantState.newDrawable();
                    }
                    this.f53b = (VectorDrawableCompat) this.f53b.mutate();
                    this.f53b.setCallback(callback);
                    this.f53b.setBounds(aVar.f53b.getBounds());
                    this.f53b.a(false);
                }
                if (aVar.f56e != null) {
                    int size = aVar.f56e.size();
                    this.f56e = new ArrayList<>(size);
                    this.f55d = new ArrayMap<>(size);
                    for (int i = 0; i < size; i++) {
                        Animator animator = aVar.f56e.get(i);
                        Animator clone = animator.clone();
                        String str = aVar.f55d.get(animator);
                        clone.setTarget(this.f53b.a(str));
                        this.f56e.add(clone);
                        this.f55d.put(clone, str);
                    }
                    a();
                }
            }
        }

        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        public int getChangingConfigurations() {
            return this.f52a;
        }

        public void a() {
            if (this.f54c == null) {
                this.f54c = new AnimatorSet();
            }
            this.f54c.playTogether(this.f56e);
        }
    }

    private void a(Animator animator) {
        ArrayList<Animator> childAnimations;
        if ((animator instanceof AnimatorSet) && (childAnimations = ((AnimatorSet) animator).getChildAnimations()) != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= childAnimations.size()) {
                    break;
                }
                a(childAnimations.get(i2));
                i = i2 + 1;
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.f48e == null) {
                    this.f48e = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.f48e);
            }
        }
    }

    private void a(String str, Animator animator) {
        animator.setTarget(this.f46c.f53b.a(str));
        if (Build.VERSION.SDK_INT < 21) {
            a(animator);
        }
        if (this.f46c.f56e == null) {
            ArrayList unused = this.f46c.f56e = new ArrayList();
            this.f46c.f55d = new ArrayMap<>();
        }
        this.f46c.f56e.add(animator);
        this.f46c.f55d.put(animator, str);
    }

    public boolean isRunning() {
        if (this.f108b != null) {
            return ((AnimatedVectorDrawable) this.f108b).isRunning();
        }
        return this.f46c.f54c.isRunning();
    }

    public void start() {
        if (this.f108b != null) {
            ((AnimatedVectorDrawable) this.f108b).start();
        } else if (!this.f46c.f54c.isStarted()) {
            this.f46c.f54c.start();
            invalidateSelf();
        }
    }

    public void stop() {
        if (this.f108b != null) {
            ((AnimatedVectorDrawable) this.f108b).stop();
        } else {
            this.f46c.f54c.end();
        }
    }
}
