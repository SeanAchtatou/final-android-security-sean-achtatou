package android.support.graphics.drawable;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.v4.b.a.a;
import android.support.v4.b.a.m;
import android.util.AttributeSet;

/* compiled from: VectorDrawableCommon */
abstract class e extends Drawable implements m {

    /* renamed from: b  reason: collision with root package name */
    Drawable f108b;

    e() {
    }

    protected static TypedArray a(Resources resources, Resources.Theme theme, AttributeSet attributeSet, int[] iArr) {
        if (theme == null) {
            return resources.obtainAttributes(attributeSet, iArr);
        }
        return theme.obtainStyledAttributes(attributeSet, iArr, 0, 0);
    }

    public void setColorFilter(int i, PorterDuff.Mode mode) {
        if (this.f108b != null) {
            this.f108b.setColorFilter(i, mode);
        } else {
            super.setColorFilter(i, mode);
        }
    }

    public ColorFilter getColorFilter() {
        if (this.f108b != null) {
            return a.e(this.f108b);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        if (this.f108b != null) {
            return this.f108b.setLevel(i);
        }
        return super.onLevelChange(i);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f108b != null) {
            this.f108b.setBounds(rect);
        } else {
            super.onBoundsChange(rect);
        }
    }

    public void setHotspot(float f2, float f3) {
        if (this.f108b != null) {
            a.a(this.f108b, f2, f3);
        }
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        if (this.f108b != null) {
            a.a(this.f108b, i, i2, i3, i4);
        }
    }

    public void setFilterBitmap(boolean z) {
        if (this.f108b != null) {
            this.f108b.setFilterBitmap(z);
        }
    }

    public void jumpToCurrentState() {
        if (this.f108b != null) {
            a.a(this.f108b);
        }
    }

    public void applyTheme(Resources.Theme theme) {
        if (this.f108b != null) {
            a.a(this.f108b, theme);
        }
    }

    public void clearColorFilter() {
        if (this.f108b != null) {
            this.f108b.clearColorFilter();
        } else {
            super.clearColorFilter();
        }
    }

    public Drawable getCurrent() {
        if (this.f108b != null) {
            return this.f108b.getCurrent();
        }
        return super.getCurrent();
    }

    public int getMinimumWidth() {
        if (this.f108b != null) {
            return this.f108b.getMinimumWidth();
        }
        return super.getMinimumWidth();
    }

    public int getMinimumHeight() {
        if (this.f108b != null) {
            return this.f108b.getMinimumHeight();
        }
        return super.getMinimumHeight();
    }

    public boolean getPadding(Rect rect) {
        if (this.f108b != null) {
            return this.f108b.getPadding(rect);
        }
        return super.getPadding(rect);
    }

    public int[] getState() {
        if (this.f108b != null) {
            return this.f108b.getState();
        }
        return super.getState();
    }

    public Region getTransparentRegion() {
        if (this.f108b != null) {
            return this.f108b.getTransparentRegion();
        }
        return super.getTransparentRegion();
    }

    public void setChangingConfigurations(int i) {
        if (this.f108b != null) {
            this.f108b.setChangingConfigurations(i);
        } else {
            super.setChangingConfigurations(i);
        }
    }

    public boolean setState(int[] iArr) {
        if (this.f108b != null) {
            return this.f108b.setState(iArr);
        }
        return super.setState(iArr);
    }
}
