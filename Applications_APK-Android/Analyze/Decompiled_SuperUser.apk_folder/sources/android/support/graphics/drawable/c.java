package android.support.graphics.drawable;

import android.graphics.Path;
import android.util.Log;
import java.util.ArrayList;

/* compiled from: PathParser */
class c {
    static float[] a(float[] fArr, int i, int i2) {
        if (i > i2) {
            throw new IllegalArgumentException();
        }
        int length = fArr.length;
        if (i < 0 || i > length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int i3 = i2 - i;
        int min = Math.min(i3, length - i);
        float[] fArr2 = new float[i3];
        System.arraycopy(fArr, i, fArr2, 0, min);
        return fArr2;
    }

    public static b[] a(String str) {
        if (str == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int i = 1;
        int i2 = 0;
        while (i < str.length()) {
            int a2 = a(str, i);
            String trim = str.substring(i2, a2).trim();
            if (trim.length() > 0) {
                a(arrayList, trim.charAt(0), b(trim));
            }
            i = a2 + 1;
            i2 = a2;
        }
        if (i - i2 == 1 && i2 < str.length()) {
            a(arrayList, str.charAt(i2), new float[0]);
        }
        return (b[]) arrayList.toArray(new b[arrayList.size()]);
    }

    public static b[] a(b[] bVarArr) {
        if (bVarArr == null) {
            return null;
        }
        b[] bVarArr2 = new b[bVarArr.length];
        for (int i = 0; i < bVarArr.length; i++) {
            bVarArr2[i] = new b(bVarArr[i]);
        }
        return bVarArr2;
    }

    public static boolean a(b[] bVarArr, b[] bVarArr2) {
        if (bVarArr == null || bVarArr2 == null || bVarArr.length != bVarArr2.length) {
            return false;
        }
        for (int i = 0; i < bVarArr.length; i++) {
            if (bVarArr[i].f106a != bVarArr2[i].f106a || bVarArr[i].f107b.length != bVarArr2[i].f107b.length) {
                return false;
            }
        }
        return true;
    }

    public static void b(b[] bVarArr, b[] bVarArr2) {
        for (int i = 0; i < bVarArr2.length; i++) {
            bVarArr[i].f106a = bVarArr2[i].f106a;
            for (int i2 = 0; i2 < bVarArr2[i].f107b.length; i2++) {
                bVarArr[i].f107b[i2] = bVarArr2[i].f107b[i2];
            }
        }
    }

    private static int a(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (((charAt - 'A') * (charAt - 'Z') <= 0 || (charAt - 'a') * (charAt - 'z') <= 0) && charAt != 'e' && charAt != 'E') {
                break;
            }
            i++;
        }
        return i;
    }

    private static void a(ArrayList<b> arrayList, char c2, float[] fArr) {
        arrayList.add(new b(c2, fArr));
    }

    /* compiled from: PathParser */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        int f104a;

        /* renamed from: b  reason: collision with root package name */
        boolean f105b;

        a() {
        }
    }

    private static float[] b(String str) {
        int i;
        if ((str.charAt(0) == 'z') || (str.charAt(0) == 'Z')) {
            return new float[0];
        }
        try {
            float[] fArr = new float[str.length()];
            a aVar = new a();
            int length = str.length();
            int i2 = 0;
            int i3 = 1;
            while (i3 < length) {
                a(str, i3, aVar);
                int i4 = aVar.f104a;
                if (i3 < i4) {
                    i = i2 + 1;
                    fArr[i2] = Float.parseFloat(str.substring(i3, i4));
                } else {
                    i = i2;
                }
                if (aVar.f105b) {
                    i3 = i4;
                    i2 = i;
                } else {
                    i3 = i4 + 1;
                    i2 = i;
                }
            }
            return a(fArr, 0, i2);
        } catch (NumberFormatException e2) {
            throw new RuntimeException("error in parsing \"" + str + "\"", e2);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static void a(String str, int i, a aVar) {
        aVar.f105b = false;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        for (int i2 = i; i2 < str.length(); i2++) {
            switch (str.charAt(i2)) {
                case ' ':
                case ',':
                    z = false;
                    z3 = true;
                    break;
                case '-':
                    if (i2 != i && !z) {
                        aVar.f105b = true;
                        z = false;
                        z3 = true;
                        break;
                    }
                    z = false;
                    break;
                case '.':
                    if (z2) {
                        aVar.f105b = true;
                        z = false;
                        z3 = true;
                        break;
                    } else {
                        z = false;
                        z2 = true;
                        break;
                    }
                case 'E':
                case 'e':
                    z = true;
                    break;
                default:
                    z = false;
                    break;
            }
            if (z3) {
                aVar.f104a = i2;
            }
        }
        aVar.f104a = i2;
    }

    /* compiled from: PathParser */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        char f106a;

        /* renamed from: b  reason: collision with root package name */
        float[] f107b;

        b(char c2, float[] fArr) {
            this.f106a = c2;
            this.f107b = fArr;
        }

        b(b bVar) {
            this.f106a = bVar.f106a;
            this.f107b = c.a(bVar.f107b, 0, bVar.f107b.length);
        }

        public static void a(b[] bVarArr, Path path) {
            float[] fArr = new float[6];
            char c2 = 'm';
            for (int i = 0; i < bVarArr.length; i++) {
                a(path, fArr, c2, bVarArr[i].f106a, bVarArr[i].f107b);
                c2 = bVarArr[i].f106a;
            }
        }

        private static void a(Path path, float[] fArr, char c2, char c3, float[] fArr2) {
            int i;
            boolean z;
            float f2;
            float f3;
            float f4;
            float f5;
            float f6;
            float f7;
            boolean z2;
            float f8;
            float f9;
            float f10;
            float f11;
            float f12;
            float f13;
            float f14 = fArr[0];
            float f15 = fArr[1];
            float f16 = fArr[2];
            float f17 = fArr[3];
            float f18 = fArr[4];
            float f19 = fArr[5];
            switch (c3) {
                case 'A':
                case 'a':
                    i = 7;
                    break;
                case 'C':
                case 'c':
                    i = 6;
                    break;
                case 'H':
                case 'V':
                case 'h':
                case 'v':
                    i = 1;
                    break;
                case 'L':
                case 'M':
                case 'T':
                case 'l':
                case 'm':
                case 't':
                    i = 2;
                    break;
                case 'Q':
                case 'S':
                case 'q':
                case 's':
                    i = 4;
                    break;
                case 'Z':
                case 'z':
                    path.close();
                    path.moveTo(f18, f19);
                    f17 = f19;
                    f16 = f18;
                    f15 = f19;
                    f14 = f18;
                    i = 2;
                    break;
                default:
                    i = 2;
                    break;
            }
            int i2 = 0;
            float f20 = f19;
            float f21 = f18;
            float f22 = f15;
            float f23 = f14;
            while (i2 < fArr2.length) {
                switch (c3) {
                    case 'A':
                        float f24 = fArr2[i2 + 5];
                        float f25 = fArr2[i2 + 6];
                        float f26 = fArr2[i2 + 0];
                        float f27 = fArr2[i2 + 1];
                        float f28 = fArr2[i2 + 2];
                        boolean z3 = fArr2[i2 + 3] != 0.0f;
                        if (fArr2[i2 + 4] != 0.0f) {
                            z = true;
                        } else {
                            z = false;
                        }
                        a(path, f23, f22, f24, f25, f26, f27, f28, z3, z);
                        float f29 = fArr2[i2 + 5];
                        float f30 = fArr2[i2 + 6];
                        f2 = f20;
                        f3 = f21;
                        f4 = f29;
                        f5 = f30;
                        f6 = f29;
                        f7 = f30;
                        break;
                    case 'C':
                        path.cubicTo(fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3], fArr2[i2 + 4], fArr2[i2 + 5]);
                        float f31 = fArr2[i2 + 4];
                        float f32 = fArr2[i2 + 5];
                        f4 = fArr2[i2 + 2];
                        f5 = f32;
                        f6 = f31;
                        f2 = f20;
                        f3 = f21;
                        f7 = fArr2[i2 + 3];
                        break;
                    case 'H':
                        path.lineTo(fArr2[i2 + 0], f22);
                        f2 = f20;
                        f4 = f16;
                        f5 = f22;
                        f6 = fArr2[i2 + 0];
                        f7 = f17;
                        f3 = f21;
                        break;
                    case 'L':
                        path.lineTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                        float f33 = fArr2[i2 + 0];
                        f4 = f16;
                        f5 = fArr2[i2 + 1];
                        f6 = f33;
                        f2 = f20;
                        f3 = f21;
                        f7 = f17;
                        break;
                    case 'M':
                        f3 = fArr2[i2 + 0];
                        f2 = fArr2[i2 + 1];
                        if (i2 <= 0) {
                            path.moveTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                            f4 = f16;
                            f5 = f2;
                            f6 = f3;
                            f7 = f17;
                            break;
                        } else {
                            path.lineTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                            f4 = f16;
                            f5 = f2;
                            f6 = f3;
                            f2 = f20;
                            f3 = f21;
                            f7 = f17;
                            break;
                        }
                    case 'Q':
                        path.quadTo(fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3]);
                        float f34 = fArr2[i2 + 0];
                        float f35 = fArr2[i2 + 1];
                        float f36 = fArr2[i2 + 2];
                        f4 = f34;
                        f5 = fArr2[i2 + 3];
                        f6 = f36;
                        f2 = f20;
                        f3 = f21;
                        f7 = f35;
                        break;
                    case 'S':
                        if (c2 == 'c' || c2 == 's' || c2 == 'C' || c2 == 'S') {
                            f11 = (2.0f * f23) - f16;
                            f10 = (2.0f * f22) - f17;
                        } else {
                            f10 = f22;
                            f11 = f23;
                        }
                        path.cubicTo(f11, f10, fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3]);
                        float f37 = fArr2[i2 + 0];
                        float f38 = fArr2[i2 + 1];
                        float f39 = fArr2[i2 + 2];
                        f4 = f37;
                        f5 = fArr2[i2 + 3];
                        f6 = f39;
                        f2 = f20;
                        f3 = f21;
                        f7 = f38;
                        break;
                    case 'T':
                        if (c2 == 'q' || c2 == 't' || c2 == 'Q' || c2 == 'T') {
                            f23 = (2.0f * f23) - f16;
                            f22 = (2.0f * f22) - f17;
                        }
                        path.quadTo(f23, f22, fArr2[i2 + 0], fArr2[i2 + 1]);
                        float f40 = fArr2[i2 + 0];
                        f7 = f22;
                        f4 = f23;
                        f5 = fArr2[i2 + 1];
                        f6 = f40;
                        f2 = f20;
                        f3 = f21;
                        break;
                    case 'V':
                        path.lineTo(f23, fArr2[i2 + 0]);
                        f3 = f21;
                        f4 = f16;
                        f5 = fArr2[i2 + 0];
                        f6 = f23;
                        f7 = f17;
                        f2 = f20;
                        break;
                    case 'a':
                        float f41 = fArr2[i2 + 5] + f23;
                        float f42 = fArr2[i2 + 6] + f22;
                        float f43 = fArr2[i2 + 0];
                        float f44 = fArr2[i2 + 1];
                        float f45 = fArr2[i2 + 2];
                        boolean z4 = fArr2[i2 + 3] != 0.0f;
                        if (fArr2[i2 + 4] != 0.0f) {
                            z2 = true;
                        } else {
                            z2 = false;
                        }
                        a(path, f23, f22, f41, f42, f43, f44, f45, z4, z2);
                        float f46 = f23 + fArr2[i2 + 5];
                        float f47 = fArr2[i2 + 6] + f22;
                        f2 = f20;
                        f3 = f21;
                        f4 = f46;
                        f5 = f47;
                        f6 = f46;
                        f7 = f47;
                        break;
                    case 'c':
                        path.rCubicTo(fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3], fArr2[i2 + 4], fArr2[i2 + 5]);
                        float f48 = f23 + fArr2[i2 + 2];
                        float f49 = f23 + fArr2[i2 + 4];
                        f4 = f48;
                        f5 = fArr2[i2 + 5] + f22;
                        f6 = f49;
                        f2 = f20;
                        f3 = f21;
                        f7 = fArr2[i2 + 3] + f22;
                        break;
                    case 'h':
                        path.rLineTo(fArr2[i2 + 0], 0.0f);
                        f2 = f20;
                        f4 = f16;
                        f5 = f22;
                        f6 = f23 + fArr2[i2 + 0];
                        f7 = f17;
                        f3 = f21;
                        break;
                    case 'l':
                        path.rLineTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                        float f50 = f23 + fArr2[i2 + 0];
                        f4 = f16;
                        f5 = fArr2[i2 + 1] + f22;
                        f6 = f50;
                        f2 = f20;
                        f3 = f21;
                        f7 = f17;
                        break;
                    case 'm':
                        f3 = f23 + fArr2[i2 + 0];
                        f2 = fArr2[i2 + 1] + f22;
                        if (i2 <= 0) {
                            path.rMoveTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                            f4 = f16;
                            f5 = f2;
                            f6 = f3;
                            f7 = f17;
                            break;
                        } else {
                            path.rLineTo(fArr2[i2 + 0], fArr2[i2 + 1]);
                            f4 = f16;
                            f5 = f2;
                            f6 = f3;
                            f2 = f20;
                            f3 = f21;
                            f7 = f17;
                            break;
                        }
                    case 'q':
                        path.rQuadTo(fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3]);
                        float f51 = f23 + fArr2[i2 + 0];
                        float f52 = f23 + fArr2[i2 + 2];
                        f4 = f51;
                        f5 = fArr2[i2 + 3] + f22;
                        f6 = f52;
                        f2 = f20;
                        f3 = f21;
                        f7 = fArr2[i2 + 1] + f22;
                        break;
                    case 's':
                        if (c2 == 'c' || c2 == 's' || c2 == 'C' || c2 == 'S') {
                            f13 = f23 - f16;
                            f12 = f22 - f17;
                        } else {
                            f12 = 0.0f;
                            f13 = 0.0f;
                        }
                        path.rCubicTo(f13, f12, fArr2[i2 + 0], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3]);
                        float f53 = f23 + fArr2[i2 + 0];
                        float f54 = f23 + fArr2[i2 + 2];
                        f4 = f53;
                        f5 = fArr2[i2 + 3] + f22;
                        f6 = f54;
                        f2 = f20;
                        f3 = f21;
                        f7 = fArr2[i2 + 1] + f22;
                        break;
                    case 't':
                        if (c2 == 'q' || c2 == 't' || c2 == 'Q' || c2 == 'T') {
                            f8 = f23 - f16;
                            f9 = f22 - f17;
                        } else {
                            f9 = 0.0f;
                            f8 = 0.0f;
                        }
                        path.rQuadTo(f8, f9, fArr2[i2 + 0], fArr2[i2 + 1]);
                        float f55 = f23 + fArr2[i2 + 0];
                        f4 = f23 + f8;
                        f5 = fArr2[i2 + 1] + f22;
                        f6 = f55;
                        f2 = f20;
                        f3 = f21;
                        f7 = f9 + f22;
                        break;
                    case 'v':
                        path.rLineTo(0.0f, fArr2[i2 + 0]);
                        f3 = f21;
                        f4 = f16;
                        f5 = fArr2[i2 + 0] + f22;
                        f6 = f23;
                        f7 = f17;
                        f2 = f20;
                        break;
                    default:
                        f2 = f20;
                        f3 = f21;
                        f4 = f16;
                        f5 = f22;
                        f6 = f23;
                        f7 = f17;
                        break;
                }
                i2 += i;
                f20 = f2;
                f21 = f3;
                f22 = f5;
                f23 = f6;
                c2 = c3;
                f17 = f7;
                f16 = f4;
            }
            fArr[0] = f23;
            fArr[1] = f22;
            fArr[2] = f16;
            fArr[3] = f17;
            fArr[4] = f21;
            fArr[5] = f20;
        }

        private static void a(Path path, float f2, float f3, float f4, float f5, float f6, float f7, float f8, boolean z, boolean z2) {
            double d2;
            double d3;
            double radians = Math.toRadians((double) f8);
            double cos = Math.cos(radians);
            double sin = Math.sin(radians);
            double d4 = ((((double) f2) * cos) + (((double) f3) * sin)) / ((double) f6);
            double d5 = ((((double) (-f2)) * sin) + (((double) f3) * cos)) / ((double) f7);
            double d6 = ((((double) f4) * cos) + (((double) f5) * sin)) / ((double) f6);
            double d7 = ((((double) (-f4)) * sin) + (((double) f5) * cos)) / ((double) f7);
            double d8 = d4 - d6;
            double d9 = d5 - d7;
            double d10 = (d4 + d6) / 2.0d;
            double d11 = (d5 + d7) / 2.0d;
            double d12 = (d8 * d8) + (d9 * d9);
            if (d12 == 0.0d) {
                Log.w("PathParser", " Points are coincident");
                return;
            }
            double d13 = (1.0d / d12) - 0.25d;
            if (d13 < 0.0d) {
                Log.w("PathParser", "Points are too far apart " + d12);
                float sqrt = (float) (Math.sqrt(d12) / 1.99999d);
                a(path, f2, f3, f4, f5, f6 * sqrt, f7 * sqrt, f8, z, z2);
                return;
            }
            double sqrt2 = Math.sqrt(d13);
            double d14 = d8 * sqrt2;
            double d15 = d9 * sqrt2;
            if (z == z2) {
                d2 = d10 - d15;
                d3 = d14 + d11;
            } else {
                d2 = d15 + d10;
                d3 = d11 - d14;
            }
            double atan2 = Math.atan2(d5 - d3, d4 - d2);
            double atan22 = Math.atan2(d7 - d3, d6 - d2) - atan2;
            if (z2 != (atan22 >= 0.0d)) {
                if (atan22 > 0.0d) {
                    atan22 -= 6.283185307179586d;
                } else {
                    atan22 += 6.283185307179586d;
                }
            }
            double d16 = ((double) f6) * d2;
            double d17 = d3 * ((double) f7);
            a(path, (d16 * cos) - (d17 * sin), (d16 * sin) + (d17 * cos), (double) f6, (double) f7, (double) f2, (double) f3, radians, atan2, atan22);
        }

        private static void a(Path path, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10) {
            int ceil = (int) Math.ceil(Math.abs((4.0d * d10) / 3.141592653589793d));
            double cos = Math.cos(d8);
            double sin = Math.sin(d8);
            double cos2 = Math.cos(d9);
            double sin2 = Math.sin(d9);
            double d11 = (((-d4) * cos) * sin2) - ((d5 * sin) * cos2);
            double d12 = d10 / ((double) ceil);
            int i = 0;
            double d13 = (sin2 * (-d4) * sin) + (cos2 * d5 * cos);
            double d14 = d11;
            while (i < ceil) {
                double d15 = d9 + d12;
                double sin3 = Math.sin(d15);
                double cos3 = Math.cos(d15);
                double d16 = (((d4 * cos) * cos3) + d2) - ((d5 * sin) * sin3);
                double d17 = (d5 * cos * sin3) + (d4 * sin * cos3) + d3;
                double d18 = (((-d4) * cos) * sin3) - ((d5 * sin) * cos3);
                double d19 = (cos3 * d5 * cos) + (sin3 * (-d4) * sin);
                double tan = Math.tan((d15 - d9) / 2.0d);
                double sqrt = ((Math.sqrt((tan * (3.0d * tan)) + 4.0d) - 1.0d) * Math.sin(d15 - d9)) / 3.0d;
                path.rLineTo(0.0f, 0.0f);
                path.cubicTo((float) ((d14 * sqrt) + d6), (float) (d7 + (d13 * sqrt)), (float) (d16 - (sqrt * d18)), (float) (d17 - (sqrt * d19)), (float) d16, (float) d17);
                i++;
                d14 = d18;
                d9 = d15;
                d7 = d17;
                d6 = d16;
                d13 = d19;
            }
        }
    }
}
