package android.support.graphics.drawable;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.graphics.drawable.c;
import android.support.v4.util.ArrayMap;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.os.VUserInfo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class VectorDrawableCompat extends e {

    /* renamed from: a  reason: collision with root package name */
    static final PorterDuff.Mode f58a = PorterDuff.Mode.SRC_IN;

    /* renamed from: c  reason: collision with root package name */
    private f f59c;

    /* renamed from: d  reason: collision with root package name */
    private PorterDuffColorFilter f60d;

    /* renamed from: e  reason: collision with root package name */
    private ColorFilter f61e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f62f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f63g;

    /* renamed from: h  reason: collision with root package name */
    private Drawable.ConstantState f64h;
    private final float[] i;
    private final Matrix j;
    private final Rect k;

    public /* bridge */ /* synthetic */ void applyTheme(Resources.Theme theme) {
        super.applyTheme(theme);
    }

    public /* bridge */ /* synthetic */ void clearColorFilter() {
        super.clearColorFilter();
    }

    public /* bridge */ /* synthetic */ ColorFilter getColorFilter() {
        return super.getColorFilter();
    }

    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    public /* bridge */ /* synthetic */ boolean getPadding(Rect rect) {
        return super.getPadding(rect);
    }

    public /* bridge */ /* synthetic */ int[] getState() {
        return super.getState();
    }

    public /* bridge */ /* synthetic */ Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    public /* bridge */ /* synthetic */ void jumpToCurrentState() {
        super.jumpToCurrentState();
    }

    public /* bridge */ /* synthetic */ void setChangingConfigurations(int i2) {
        super.setChangingConfigurations(i2);
    }

    public /* bridge */ /* synthetic */ void setColorFilter(int i2, PorterDuff.Mode mode) {
        super.setColorFilter(i2, mode);
    }

    public /* bridge */ /* synthetic */ void setFilterBitmap(boolean z) {
        super.setFilterBitmap(z);
    }

    public /* bridge */ /* synthetic */ void setHotspot(float f2, float f3) {
        super.setHotspot(f2, f3);
    }

    public /* bridge */ /* synthetic */ void setHotspotBounds(int i2, int i3, int i4, int i5) {
        super.setHotspotBounds(i2, i3, i4, i5);
    }

    public /* bridge */ /* synthetic */ boolean setState(int[] iArr) {
        return super.setState(iArr);
    }

    VectorDrawableCompat() {
        this.f63g = true;
        this.i = new float[9];
        this.j = new Matrix();
        this.k = new Rect();
        this.f59c = new f();
    }

    VectorDrawableCompat(f fVar) {
        this.f63g = true;
        this.i = new float[9];
        this.j = new Matrix();
        this.k = new Rect();
        this.f59c = fVar;
        this.f60d = a(this.f60d, fVar.f91c, fVar.f92d);
    }

    public Drawable mutate() {
        if (this.f108b != null) {
            this.f108b.mutate();
        } else if (!this.f62f && super.mutate() == this) {
            this.f59c = new f(this.f59c);
            this.f62f = true;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public Object a(String str) {
        return this.f59c.f90b.f88h.get(str);
    }

    public Drawable.ConstantState getConstantState() {
        if (this.f108b != null) {
            return new g(this.f108b.getConstantState());
        }
        this.f59c.f89a = getChangingConfigurations();
        return this.f59c;
    }

    public void draw(Canvas canvas) {
        if (this.f108b != null) {
            this.f108b.draw(canvas);
            return;
        }
        copyBounds(this.k);
        if (this.k.width() > 0 && this.k.height() > 0) {
            ColorFilter colorFilter = this.f61e == null ? this.f60d : this.f61e;
            canvas.getMatrix(this.j);
            this.j.getValues(this.i);
            float abs = Math.abs(this.i[0]);
            float abs2 = Math.abs(this.i[4]);
            float abs3 = Math.abs(this.i[1]);
            float abs4 = Math.abs(this.i[3]);
            if (!(abs3 == 0.0f && abs4 == 0.0f)) {
                abs2 = 1.0f;
                abs = 1.0f;
            }
            int min = Math.min((int) FileUtils.FileMode.MODE_ISUID, (int) (abs * ((float) this.k.width())));
            int min2 = Math.min((int) FileUtils.FileMode.MODE_ISUID, (int) (abs2 * ((float) this.k.height())));
            if (min > 0 && min2 > 0) {
                int save = canvas.save();
                canvas.translate((float) this.k.left, (float) this.k.top);
                if (a()) {
                    canvas.translate((float) this.k.width(), 0.0f);
                    canvas.scale(-1.0f, 1.0f);
                }
                this.k.offsetTo(0, 0);
                this.f59c.b(min, min2);
                if (!this.f63g) {
                    this.f59c.a(min, min2);
                } else if (!this.f59c.b()) {
                    this.f59c.a(min, min2);
                    this.f59c.c();
                }
                this.f59c.a(canvas, colorFilter, this.k);
                canvas.restoreToCount(save);
            }
        }
    }

    public int getAlpha() {
        if (this.f108b != null) {
            return android.support.v4.b.a.a.c(this.f108b);
        }
        return this.f59c.f90b.getRootAlpha();
    }

    public void setAlpha(int i2) {
        if (this.f108b != null) {
            this.f108b.setAlpha(i2);
        } else if (this.f59c.f90b.getRootAlpha() != i2) {
            this.f59c.f90b.setRootAlpha(i2);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f108b != null) {
            this.f108b.setColorFilter(colorFilter);
            return;
        }
        this.f61e = colorFilter;
        invalidateSelf();
    }

    /* access modifiers changed from: package-private */
    public PorterDuffColorFilter a(PorterDuffColorFilter porterDuffColorFilter, ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    @SuppressLint({"NewApi"})
    public void setTint(int i2) {
        if (this.f108b != null) {
            android.support.v4.b.a.a.a(this.f108b, i2);
        } else {
            setTintList(ColorStateList.valueOf(i2));
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.f108b != null) {
            android.support.v4.b.a.a.a(this.f108b, colorStateList);
            return;
        }
        f fVar = this.f59c;
        if (fVar.f91c != colorStateList) {
            fVar.f91c = colorStateList;
            this.f60d = a(this.f60d, colorStateList, fVar.f92d);
            invalidateSelf();
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (this.f108b != null) {
            android.support.v4.b.a.a.a(this.f108b, mode);
            return;
        }
        f fVar = this.f59c;
        if (fVar.f92d != mode) {
            fVar.f92d = mode;
            this.f60d = a(this.f60d, fVar.f91c, mode);
            invalidateSelf();
        }
    }

    public boolean isStateful() {
        if (this.f108b != null) {
            return this.f108b.isStateful();
        }
        return super.isStateful() || !(this.f59c == null || this.f59c.f91c == null || !this.f59c.f91c.isStateful());
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        if (this.f108b != null) {
            return this.f108b.setState(iArr);
        }
        f fVar = this.f59c;
        if (fVar.f91c == null || fVar.f92d == null) {
            return false;
        }
        this.f60d = a(this.f60d, fVar.f91c, fVar.f92d);
        invalidateSelf();
        return true;
    }

    public int getOpacity() {
        if (this.f108b != null) {
            return this.f108b.getOpacity();
        }
        return -3;
    }

    public int getIntrinsicWidth() {
        if (this.f108b != null) {
            return this.f108b.getIntrinsicWidth();
        }
        return (int) this.f59c.f90b.f82b;
    }

    public int getIntrinsicHeight() {
        if (this.f108b != null) {
            return this.f108b.getIntrinsicHeight();
        }
        return (int) this.f59c.f90b.f83c;
    }

    public boolean canApplyTheme() {
        if (this.f108b == null) {
            return false;
        }
        android.support.v4.b.a.a.d(this.f108b);
        return false;
    }

    public boolean isAutoMirrored() {
        if (this.f108b != null) {
            return android.support.v4.b.a.a.b(this.f108b);
        }
        return this.f59c.f93e;
    }

    public void setAutoMirrored(boolean z) {
        if (this.f108b != null) {
            android.support.v4.b.a.a.a(this.f108b, z);
        } else {
            this.f59c.f93e = z;
        }
    }

    @SuppressLint({"NewApi"})
    public static VectorDrawableCompat a(Resources resources, int i2, Resources.Theme theme) {
        int next;
        if (Build.VERSION.SDK_INT >= 24) {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.f108b = android.support.v4.content.a.d.a(resources, i2, theme);
            vectorDrawableCompat.f64h = new g(vectorDrawableCompat.f108b.getConstantState());
            return vectorDrawableCompat;
        }
        try {
            XmlResourceParser xml = resources.getXml(i2);
            AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
            do {
                next = xml.next();
                if (next == 2) {
                    break;
                }
            } while (next != 1);
            if (next == 2) {
                return a(resources, xml, asAttributeSet, theme);
            }
            throw new XmlPullParserException("No start tag found");
        } catch (XmlPullParserException e2) {
            Log.e("VectorDrawableCompat", "parser error", e2);
        } catch (IOException e3) {
            Log.e("VectorDrawableCompat", "parser error", e3);
        }
        return null;
    }

    @SuppressLint({"NewApi"})
    public static VectorDrawableCompat a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
        vectorDrawableCompat.inflate(resources, xmlPullParser, attributeSet, theme);
        return vectorDrawableCompat;
    }

    static int a(int i2, float f2) {
        return (((int) (((float) Color.alpha(i2)) * f2)) << 24) | (16777215 & i2);
    }

    @SuppressLint({"NewApi"})
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        if (this.f108b != null) {
            this.f108b.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, null);
        }
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        if (this.f108b != null) {
            android.support.v4.b.a.a.a(this.f108b, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        f fVar = this.f59c;
        fVar.f90b = new e();
        TypedArray a2 = a(resources, theme, attributeSet, a.f98a);
        a(a2, xmlPullParser);
        a2.recycle();
        fVar.f89a = getChangingConfigurations();
        fVar.k = true;
        b(resources, xmlPullParser, attributeSet, theme);
        this.f60d = a(this.f60d, fVar.f91c, fVar.f92d);
    }

    private static PorterDuff.Mode a(int i2, PorterDuff.Mode mode) {
        switch (i2) {
            case 3:
                return PorterDuff.Mode.SRC_OVER;
            case 4:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            default:
                return mode;
            case 5:
                return PorterDuff.Mode.SRC_IN;
            case 9:
                return PorterDuff.Mode.SRC_ATOP;
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                if (Build.VERSION.SDK_INT >= 11) {
                    return PorterDuff.Mode.ADD;
                }
                return mode;
        }
    }

    private void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
        f fVar = this.f59c;
        e eVar = fVar.f90b;
        fVar.f92d = a(d.a(typedArray, xmlPullParser, "tintMode", 6, -1), PorterDuff.Mode.SRC_IN);
        ColorStateList colorStateList = typedArray.getColorStateList(1);
        if (colorStateList != null) {
            fVar.f91c = colorStateList;
        }
        fVar.f93e = d.a(typedArray, xmlPullParser, "autoMirrored", 5, fVar.f93e);
        eVar.f84d = d.a(typedArray, xmlPullParser, "viewportWidth", 7, eVar.f84d);
        eVar.f85e = d.a(typedArray, xmlPullParser, "viewportHeight", 8, eVar.f85e);
        if (eVar.f84d <= 0.0f) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
        } else if (eVar.f85e <= 0.0f) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
        } else {
            eVar.f82b = typedArray.getDimension(3, eVar.f82b);
            eVar.f83c = typedArray.getDimension(2, eVar.f83c);
            if (eVar.f82b <= 0.0f) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires width > 0");
            } else if (eVar.f83c <= 0.0f) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires height > 0");
            } else {
                eVar.setAlpha(d.a(typedArray, xmlPullParser, "alpha", 4, eVar.getAlpha()));
                String string = typedArray.getString(0);
                if (string != null) {
                    eVar.f87g = string;
                    eVar.f88h.put(string, eVar);
                }
            }
        }
    }

    private void b(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        boolean z;
        f fVar = this.f59c;
        e eVar = fVar.f90b;
        Stack stack = new Stack();
        stack.push(eVar.f81a);
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        boolean z2 = true;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                c cVar = (c) stack.peek();
                if ("path".equals(name)) {
                    b bVar = new b();
                    bVar.a(resources, attributeSet, theme, xmlPullParser);
                    cVar.f73a.add(bVar);
                    if (bVar.getPathName() != null) {
                        eVar.f88h.put(bVar.getPathName(), bVar);
                    }
                    z = false;
                    fVar.f89a = bVar.o | fVar.f89a;
                } else if ("clip-path".equals(name)) {
                    a aVar = new a();
                    aVar.a(resources, attributeSet, theme, xmlPullParser);
                    cVar.f73a.add(aVar);
                    if (aVar.getPathName() != null) {
                        eVar.f88h.put(aVar.getPathName(), aVar);
                    }
                    fVar.f89a |= aVar.o;
                    z = z2;
                } else {
                    if ("group".equals(name)) {
                        c cVar2 = new c();
                        cVar2.a(resources, attributeSet, theme, xmlPullParser);
                        cVar.f73a.add(cVar2);
                        stack.push(cVar2);
                        if (cVar2.getGroupName() != null) {
                            eVar.f88h.put(cVar2.getGroupName(), cVar2);
                        }
                        fVar.f89a |= cVar2.f75c;
                    }
                    z = z2;
                }
                z2 = z;
            } else if (eventType == 3 && "group".equals(xmlPullParser.getName())) {
                stack.pop();
            }
            eventType = xmlPullParser.next();
        }
        if (z2) {
            StringBuffer stringBuffer = new StringBuffer();
            if (stringBuffer.length() > 0) {
                stringBuffer.append(" or ");
            }
            stringBuffer.append("path");
            throw new XmlPullParserException("no " + ((Object) stringBuffer) + " defined");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f63g = z;
    }

    @SuppressLint({"NewApi"})
    private boolean a() {
        boolean z = true;
        if (Build.VERSION.SDK_INT < 17) {
            return false;
        }
        if (!isAutoMirrored() || getLayoutDirection() != 1) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f108b != null) {
            this.f108b.setBounds(rect);
        }
    }

    public int getChangingConfigurations() {
        if (this.f108b != null) {
            return this.f108b.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f59c.getChangingConfigurations();
    }

    public void invalidateSelf() {
        if (this.f108b != null) {
            this.f108b.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    public void scheduleSelf(Runnable runnable, long j2) {
        if (this.f108b != null) {
            this.f108b.scheduleSelf(runnable, j2);
        } else {
            super.scheduleSelf(runnable, j2);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        if (this.f108b != null) {
            return this.f108b.setVisible(z, z2);
        }
        return super.setVisible(z, z2);
    }

    public void unscheduleSelf(Runnable runnable) {
        if (this.f108b != null) {
            this.f108b.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }

    private static class g extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        private final Drawable.ConstantState f97a;

        public g(Drawable.ConstantState constantState) {
            this.f97a = constantState;
        }

        public Drawable newDrawable() {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.f108b = (VectorDrawable) this.f97a.newDrawable();
            return vectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources) {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.f108b = (VectorDrawable) this.f97a.newDrawable(resources);
            return vectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.f108b = (VectorDrawable) this.f97a.newDrawable(resources, theme);
            return vectorDrawableCompat;
        }

        public boolean canApplyTheme() {
            return this.f97a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f97a.getChangingConfigurations();
        }
    }

    private static class f extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        int f89a;

        /* renamed from: b  reason: collision with root package name */
        e f90b;

        /* renamed from: c  reason: collision with root package name */
        ColorStateList f91c;

        /* renamed from: d  reason: collision with root package name */
        PorterDuff.Mode f92d;

        /* renamed from: e  reason: collision with root package name */
        boolean f93e;

        /* renamed from: f  reason: collision with root package name */
        Bitmap f94f;

        /* renamed from: g  reason: collision with root package name */
        ColorStateList f95g;

        /* renamed from: h  reason: collision with root package name */
        PorterDuff.Mode f96h;
        int i;
        boolean j;
        boolean k;
        Paint l;

        public f(f fVar) {
            this.f91c = null;
            this.f92d = VectorDrawableCompat.f58a;
            if (fVar != null) {
                this.f89a = fVar.f89a;
                this.f90b = new e(fVar.f90b);
                if (fVar.f90b.n != null) {
                    Paint unused = this.f90b.n = new Paint(fVar.f90b.n);
                }
                if (fVar.f90b.m != null) {
                    Paint unused2 = this.f90b.m = new Paint(fVar.f90b.m);
                }
                this.f91c = fVar.f91c;
                this.f92d = fVar.f92d;
                this.f93e = fVar.f93e;
            }
        }

        public void a(Canvas canvas, ColorFilter colorFilter, Rect rect) {
            canvas.drawBitmap(this.f94f, (Rect) null, rect, a(colorFilter));
        }

        public boolean a() {
            return this.f90b.getRootAlpha() < 255;
        }

        public Paint a(ColorFilter colorFilter) {
            if (!a() && colorFilter == null) {
                return null;
            }
            if (this.l == null) {
                this.l = new Paint();
                this.l.setFilterBitmap(true);
            }
            this.l.setAlpha(this.f90b.getRootAlpha());
            this.l.setColorFilter(colorFilter);
            return this.l;
        }

        public void a(int i2, int i3) {
            this.f94f.eraseColor(0);
            this.f90b.a(new Canvas(this.f94f), i2, i3, (ColorFilter) null);
        }

        public void b(int i2, int i3) {
            if (this.f94f == null || !c(i2, i3)) {
                this.f94f = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
                this.k = true;
            }
        }

        public boolean c(int i2, int i3) {
            if (i2 == this.f94f.getWidth() && i3 == this.f94f.getHeight()) {
                return true;
            }
            return false;
        }

        public boolean b() {
            if (!this.k && this.f95g == this.f91c && this.f96h == this.f92d && this.j == this.f93e && this.i == this.f90b.getRootAlpha()) {
                return true;
            }
            return false;
        }

        public void c() {
            this.f95g = this.f91c;
            this.f96h = this.f92d;
            this.i = this.f90b.getRootAlpha();
            this.j = this.f93e;
            this.k = false;
        }

        public f() {
            this.f91c = null;
            this.f92d = VectorDrawableCompat.f58a;
            this.f90b = new e();
        }

        public Drawable newDrawable() {
            return new VectorDrawableCompat(this);
        }

        public Drawable newDrawable(Resources resources) {
            return new VectorDrawableCompat(this);
        }

        public int getChangingConfigurations() {
            return this.f89a;
        }
    }

    private static class e {
        private static final Matrix k = new Matrix();

        /* renamed from: a  reason: collision with root package name */
        final c f81a;

        /* renamed from: b  reason: collision with root package name */
        float f82b;

        /* renamed from: c  reason: collision with root package name */
        float f83c;

        /* renamed from: d  reason: collision with root package name */
        float f84d;

        /* renamed from: e  reason: collision with root package name */
        float f85e;

        /* renamed from: f  reason: collision with root package name */
        int f86f;

        /* renamed from: g  reason: collision with root package name */
        String f87g;

        /* renamed from: h  reason: collision with root package name */
        final ArrayMap<String, Object> f88h;
        private final Path i;
        private final Path j;
        private final Matrix l;
        /* access modifiers changed from: private */
        public Paint m;
        /* access modifiers changed from: private */
        public Paint n;
        private PathMeasure o;
        private int p;

        public e() {
            this.l = new Matrix();
            this.f82b = 0.0f;
            this.f83c = 0.0f;
            this.f84d = 0.0f;
            this.f85e = 0.0f;
            this.f86f = VUserInfo.FLAG_MASK_USER_TYPE;
            this.f87g = null;
            this.f88h = new ArrayMap<>();
            this.f81a = new c();
            this.i = new Path();
            this.j = new Path();
        }

        public void setRootAlpha(int i2) {
            this.f86f = i2;
        }

        public int getRootAlpha() {
            return this.f86f;
        }

        public void setAlpha(float f2) {
            setRootAlpha((int) (255.0f * f2));
        }

        public float getAlpha() {
            return ((float) getRootAlpha()) / 255.0f;
        }

        public e(e eVar) {
            this.l = new Matrix();
            this.f82b = 0.0f;
            this.f83c = 0.0f;
            this.f84d = 0.0f;
            this.f85e = 0.0f;
            this.f86f = VUserInfo.FLAG_MASK_USER_TYPE;
            this.f87g = null;
            this.f88h = new ArrayMap<>();
            this.f81a = new c(eVar.f81a, this.f88h);
            this.i = new Path(eVar.i);
            this.j = new Path(eVar.j);
            this.f82b = eVar.f82b;
            this.f83c = eVar.f83c;
            this.f84d = eVar.f84d;
            this.f85e = eVar.f85e;
            this.p = eVar.p;
            this.f86f = eVar.f86f;
            this.f87g = eVar.f87g;
            if (eVar.f87g != null) {
                this.f88h.put(eVar.f87g, this);
            }
        }

        private void a(c cVar, Matrix matrix, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            cVar.f76d.set(matrix);
            cVar.f76d.preConcat(cVar.k);
            canvas.save();
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 < cVar.f73a.size()) {
                    Object obj = cVar.f73a.get(i5);
                    if (obj instanceof c) {
                        a((c) obj, cVar.f76d, canvas, i2, i3, colorFilter);
                    } else if (obj instanceof d) {
                        a(cVar, (d) obj, canvas, i2, i3, colorFilter);
                    }
                    i4 = i5 + 1;
                } else {
                    canvas.restore();
                    return;
                }
            }
        }

        public void a(Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            a(this.f81a, k, canvas, i2, i3, colorFilter);
        }

        private void a(c cVar, d dVar, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            float f2 = ((float) i2) / this.f84d;
            float f3 = ((float) i3) / this.f85e;
            float min = Math.min(f2, f3);
            Matrix a2 = cVar.f76d;
            this.l.set(a2);
            this.l.postScale(f2, f3);
            float a3 = a(a2);
            if (a3 != 0.0f) {
                dVar.a(this.i);
                Path path = this.i;
                this.j.reset();
                if (dVar.a()) {
                    this.j.addPath(path, this.l);
                    canvas.clipPath(this.j);
                    return;
                }
                b bVar = (b) dVar;
                if (!(bVar.f71g == 0.0f && bVar.f72h == 1.0f)) {
                    float f4 = (bVar.f71g + bVar.i) % 1.0f;
                    float f5 = (bVar.f72h + bVar.i) % 1.0f;
                    if (this.o == null) {
                        this.o = new PathMeasure();
                    }
                    this.o.setPath(this.i, false);
                    float length = this.o.getLength();
                    float f6 = f4 * length;
                    float f7 = f5 * length;
                    path.reset();
                    if (f6 > f7) {
                        this.o.getSegment(f6, length, path, true);
                        this.o.getSegment(0.0f, f7, path, true);
                    } else {
                        this.o.getSegment(f6, f7, path, true);
                    }
                    path.rLineTo(0.0f, 0.0f);
                }
                this.j.addPath(path, this.l);
                if (bVar.f67c != 0) {
                    if (this.n == null) {
                        this.n = new Paint();
                        this.n.setStyle(Paint.Style.FILL);
                        this.n.setAntiAlias(true);
                    }
                    Paint paint = this.n;
                    paint.setColor(VectorDrawableCompat.a(bVar.f67c, bVar.f70f));
                    paint.setColorFilter(colorFilter);
                    canvas.drawPath(this.j, paint);
                }
                if (bVar.f65a != 0) {
                    if (this.m == null) {
                        this.m = new Paint();
                        this.m.setStyle(Paint.Style.STROKE);
                        this.m.setAntiAlias(true);
                    }
                    Paint paint2 = this.m;
                    if (bVar.k != null) {
                        paint2.setStrokeJoin(bVar.k);
                    }
                    if (bVar.j != null) {
                        paint2.setStrokeCap(bVar.j);
                    }
                    paint2.setStrokeMiter(bVar.l);
                    paint2.setColor(VectorDrawableCompat.a(bVar.f65a, bVar.f68d));
                    paint2.setColorFilter(colorFilter);
                    paint2.setStrokeWidth(a3 * min * bVar.f66b);
                    canvas.drawPath(this.j, paint2);
                }
            }
        }

        private static float a(float f2, float f3, float f4, float f5) {
            return (f2 * f5) - (f3 * f4);
        }

        private float a(Matrix matrix) {
            float[] fArr = {0.0f, 1.0f, 1.0f, 0.0f};
            matrix.mapVectors(fArr);
            float hypot = (float) Math.hypot((double) fArr[2], (double) fArr[3]);
            float a2 = a(fArr[0], fArr[1], fArr[2], fArr[3]);
            float max = Math.max((float) Math.hypot((double) fArr[0], (double) fArr[1]), hypot);
            if (max > 0.0f) {
                return Math.abs(a2) / max;
            }
            return 0.0f;
        }
    }

    private static class c {

        /* renamed from: a  reason: collision with root package name */
        final ArrayList<Object> f73a = new ArrayList<>();

        /* renamed from: b  reason: collision with root package name */
        float f74b = 0.0f;

        /* renamed from: c  reason: collision with root package name */
        int f75c;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public final Matrix f76d = new Matrix();

        /* renamed from: e  reason: collision with root package name */
        private float f77e = 0.0f;

        /* renamed from: f  reason: collision with root package name */
        private float f78f = 0.0f;

        /* renamed from: g  reason: collision with root package name */
        private float f79g = 1.0f;

        /* renamed from: h  reason: collision with root package name */
        private float f80h = 1.0f;
        private float i = 0.0f;
        private float j = 0.0f;
        /* access modifiers changed from: private */
        public final Matrix k = new Matrix();
        private int[] l;
        private String m = null;

        public c(c cVar, ArrayMap<String, Object> arrayMap) {
            d aVar;
            this.f74b = cVar.f74b;
            this.f77e = cVar.f77e;
            this.f78f = cVar.f78f;
            this.f79g = cVar.f79g;
            this.f80h = cVar.f80h;
            this.i = cVar.i;
            this.j = cVar.j;
            this.l = cVar.l;
            this.m = cVar.m;
            this.f75c = cVar.f75c;
            if (this.m != null) {
                arrayMap.put(this.m, this);
            }
            this.k.set(cVar.k);
            ArrayList<Object> arrayList = cVar.f73a;
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < arrayList.size()) {
                    Object obj = arrayList.get(i3);
                    if (obj instanceof c) {
                        this.f73a.add(new c((c) obj, arrayMap));
                    } else {
                        if (obj instanceof b) {
                            aVar = new b((b) obj);
                        } else if (obj instanceof a) {
                            aVar = new a((a) obj);
                        } else {
                            throw new IllegalStateException("Unknown object in the tree!");
                        }
                        this.f73a.add(aVar);
                        if (aVar.n != null) {
                            arrayMap.put(aVar.n, aVar);
                        }
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }

        public c() {
        }

        public String getGroupName() {
            return this.m;
        }

        public Matrix getLocalMatrix() {
            return this.k;
        }

        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray a2 = e.a(resources, theme, attributeSet, a.f99b);
            a(a2, xmlPullParser);
            a2.recycle();
        }

        private void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.l = null;
            this.f74b = d.a(typedArray, xmlPullParser, "rotation", 5, this.f74b);
            this.f77e = typedArray.getFloat(1, this.f77e);
            this.f78f = typedArray.getFloat(2, this.f78f);
            this.f79g = d.a(typedArray, xmlPullParser, "scaleX", 3, this.f79g);
            this.f80h = d.a(typedArray, xmlPullParser, "scaleY", 4, this.f80h);
            this.i = d.a(typedArray, xmlPullParser, "translateX", 6, this.i);
            this.j = d.a(typedArray, xmlPullParser, "translateY", 7, this.j);
            String string = typedArray.getString(0);
            if (string != null) {
                this.m = string;
            }
            a();
        }

        private void a() {
            this.k.reset();
            this.k.postTranslate(-this.f77e, -this.f78f);
            this.k.postScale(this.f79g, this.f80h);
            this.k.postRotate(this.f74b, 0.0f, 0.0f);
            this.k.postTranslate(this.i + this.f77e, this.j + this.f78f);
        }

        public float getRotation() {
            return this.f74b;
        }

        public void setRotation(float f2) {
            if (f2 != this.f74b) {
                this.f74b = f2;
                a();
            }
        }

        public float getPivotX() {
            return this.f77e;
        }

        public void setPivotX(float f2) {
            if (f2 != this.f77e) {
                this.f77e = f2;
                a();
            }
        }

        public float getPivotY() {
            return this.f78f;
        }

        public void setPivotY(float f2) {
            if (f2 != this.f78f) {
                this.f78f = f2;
                a();
            }
        }

        public float getScaleX() {
            return this.f79g;
        }

        public void setScaleX(float f2) {
            if (f2 != this.f79g) {
                this.f79g = f2;
                a();
            }
        }

        public float getScaleY() {
            return this.f80h;
        }

        public void setScaleY(float f2) {
            if (f2 != this.f80h) {
                this.f80h = f2;
                a();
            }
        }

        public float getTranslateX() {
            return this.i;
        }

        public void setTranslateX(float f2) {
            if (f2 != this.i) {
                this.i = f2;
                a();
            }
        }

        public float getTranslateY() {
            return this.j;
        }

        public void setTranslateY(float f2) {
            if (f2 != this.j) {
                this.j = f2;
                a();
            }
        }
    }

    private static class d {
        protected c.b[] m = null;
        String n;
        int o;

        public d() {
        }

        public d(d dVar) {
            this.n = dVar.n;
            this.o = dVar.o;
            this.m = c.a(dVar.m);
        }

        public void a(Path path) {
            path.reset();
            if (this.m != null) {
                c.b.a(this.m, path);
            }
        }

        public String getPathName() {
            return this.n;
        }

        public boolean a() {
            return false;
        }

        public c.b[] getPathData() {
            return this.m;
        }

        public void setPathData(c.b[] bVarArr) {
            if (!c.a(this.m, bVarArr)) {
                this.m = c.a(bVarArr);
            } else {
                c.b(this.m, bVarArr);
            }
        }
    }

    private static class a extends d {
        public a() {
        }

        public a(a aVar) {
            super(aVar);
        }

        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            if (d.a(xmlPullParser, "pathData")) {
                TypedArray a2 = e.a(resources, theme, attributeSet, a.f101d);
                a(a2);
                a2.recycle();
            }
        }

        private void a(TypedArray typedArray) {
            String string = typedArray.getString(0);
            if (string != null) {
                this.n = string;
            }
            String string2 = typedArray.getString(1);
            if (string2 != null) {
                this.m = c.a(string2);
            }
        }

        public boolean a() {
            return true;
        }
    }

    private static class b extends d {

        /* renamed from: a  reason: collision with root package name */
        int f65a = 0;

        /* renamed from: b  reason: collision with root package name */
        float f66b = 0.0f;

        /* renamed from: c  reason: collision with root package name */
        int f67c = 0;

        /* renamed from: d  reason: collision with root package name */
        float f68d = 1.0f;

        /* renamed from: e  reason: collision with root package name */
        int f69e;

        /* renamed from: f  reason: collision with root package name */
        float f70f = 1.0f;

        /* renamed from: g  reason: collision with root package name */
        float f71g = 0.0f;

        /* renamed from: h  reason: collision with root package name */
        float f72h = 1.0f;
        float i = 0.0f;
        Paint.Cap j = Paint.Cap.BUTT;
        Paint.Join k = Paint.Join.MITER;
        float l = 4.0f;
        private int[] p;

        public b() {
        }

        public b(b bVar) {
            super(bVar);
            this.p = bVar.p;
            this.f65a = bVar.f65a;
            this.f66b = bVar.f66b;
            this.f68d = bVar.f68d;
            this.f67c = bVar.f67c;
            this.f69e = bVar.f69e;
            this.f70f = bVar.f70f;
            this.f71g = bVar.f71g;
            this.f72h = bVar.f72h;
            this.i = bVar.i;
            this.j = bVar.j;
            this.k = bVar.k;
            this.l = bVar.l;
        }

        private Paint.Cap a(int i2, Paint.Cap cap) {
            switch (i2) {
                case 0:
                    return Paint.Cap.BUTT;
                case 1:
                    return Paint.Cap.ROUND;
                case 2:
                    return Paint.Cap.SQUARE;
                default:
                    return cap;
            }
        }

        private Paint.Join a(int i2, Paint.Join join) {
            switch (i2) {
                case 0:
                    return Paint.Join.MITER;
                case 1:
                    return Paint.Join.ROUND;
                case 2:
                    return Paint.Join.BEVEL;
                default:
                    return join;
            }
        }

        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray a2 = e.a(resources, theme, attributeSet, a.f100c);
            a(a2, xmlPullParser);
            a2.recycle();
        }

        private void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.p = null;
            if (d.a(xmlPullParser, "pathData")) {
                String string = typedArray.getString(0);
                if (string != null) {
                    this.n = string;
                }
                String string2 = typedArray.getString(2);
                if (string2 != null) {
                    this.m = c.a(string2);
                }
                this.f67c = d.b(typedArray, xmlPullParser, "fillColor", 1, this.f67c);
                this.f70f = d.a(typedArray, xmlPullParser, "fillAlpha", 12, this.f70f);
                this.j = a(d.a(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.j);
                this.k = a(d.a(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.k);
                this.l = d.a(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.l);
                this.f65a = d.b(typedArray, xmlPullParser, "strokeColor", 3, this.f65a);
                this.f68d = d.a(typedArray, xmlPullParser, "strokeAlpha", 11, this.f68d);
                this.f66b = d.a(typedArray, xmlPullParser, "strokeWidth", 4, this.f66b);
                this.f72h = d.a(typedArray, xmlPullParser, "trimPathEnd", 6, this.f72h);
                this.i = d.a(typedArray, xmlPullParser, "trimPathOffset", 7, this.i);
                this.f71g = d.a(typedArray, xmlPullParser, "trimPathStart", 5, this.f71g);
            }
        }

        /* access modifiers changed from: package-private */
        public int getStrokeColor() {
            return this.f65a;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeColor(int i2) {
            this.f65a = i2;
        }

        /* access modifiers changed from: package-private */
        public float getStrokeWidth() {
            return this.f66b;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeWidth(float f2) {
            this.f66b = f2;
        }

        /* access modifiers changed from: package-private */
        public float getStrokeAlpha() {
            return this.f68d;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeAlpha(float f2) {
            this.f68d = f2;
        }

        /* access modifiers changed from: package-private */
        public int getFillColor() {
            return this.f67c;
        }

        /* access modifiers changed from: package-private */
        public void setFillColor(int i2) {
            this.f67c = i2;
        }

        /* access modifiers changed from: package-private */
        public float getFillAlpha() {
            return this.f70f;
        }

        /* access modifiers changed from: package-private */
        public void setFillAlpha(float f2) {
            this.f70f = f2;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathStart() {
            return this.f71g;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathStart(float f2) {
            this.f71g = f2;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathEnd() {
            return this.f72h;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathEnd(float f2) {
            this.f72h = f2;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathOffset() {
            return this.i;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathOffset(float f2) {
            this.i = f2;
        }
    }
}
