package android.support.v4.util;

/* compiled from: SparseArrayCompat */
public class j<E> implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f889a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private boolean f890b;

    /* renamed from: c  reason: collision with root package name */
    private int[] f891c;

    /* renamed from: d  reason: collision with root package name */
    private Object[] f892d;

    /* renamed from: e  reason: collision with root package name */
    private int f893e;

    public j() {
        this(10);
    }

    public j(int i) {
        this.f890b = false;
        if (i == 0) {
            this.f891c = b.f855a;
            this.f892d = b.f857c;
        } else {
            int a2 = b.a(i);
            this.f891c = new int[a2];
            this.f892d = new Object[a2];
        }
        this.f893e = 0;
    }

    /* renamed from: a */
    public j<E> clone() {
        try {
            j<E> jVar = (j) super.clone();
            try {
                jVar.f891c = (int[]) this.f891c.clone();
                jVar.f892d = (Object[]) this.f892d.clone();
                return jVar;
            } catch (CloneNotSupportedException e2) {
                return jVar;
            }
        } catch (CloneNotSupportedException e3) {
            return null;
        }
    }

    public E a(int i) {
        return a(i, null);
    }

    public E a(int i, E e2) {
        int a2 = b.a(this.f891c, this.f893e, i);
        return (a2 < 0 || this.f892d[a2] == f889a) ? e2 : this.f892d[a2];
    }

    public void b(int i) {
        int a2 = b.a(this.f891c, this.f893e, i);
        if (a2 >= 0 && this.f892d[a2] != f889a) {
            this.f892d[a2] = f889a;
            this.f890b = true;
        }
    }

    public void c(int i) {
        b(i);
    }

    private void d() {
        int i = this.f893e;
        int[] iArr = this.f891c;
        Object[] objArr = this.f892d;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != f889a) {
                if (i3 != i2) {
                    iArr[i2] = iArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.f890b = false;
        this.f893e = i2;
    }

    public void b(int i, E e2) {
        int a2 = b.a(this.f891c, this.f893e, i);
        if (a2 >= 0) {
            this.f892d[a2] = e2;
            return;
        }
        int i2 = a2 ^ -1;
        if (i2 >= this.f893e || this.f892d[i2] != f889a) {
            if (this.f890b && this.f893e >= this.f891c.length) {
                d();
                i2 = b.a(this.f891c, this.f893e, i) ^ -1;
            }
            if (this.f893e >= this.f891c.length) {
                int a3 = b.a(this.f893e + 1);
                int[] iArr = new int[a3];
                Object[] objArr = new Object[a3];
                System.arraycopy(this.f891c, 0, iArr, 0, this.f891c.length);
                System.arraycopy(this.f892d, 0, objArr, 0, this.f892d.length);
                this.f891c = iArr;
                this.f892d = objArr;
            }
            if (this.f893e - i2 != 0) {
                System.arraycopy(this.f891c, i2, this.f891c, i2 + 1, this.f893e - i2);
                System.arraycopy(this.f892d, i2, this.f892d, i2 + 1, this.f893e - i2);
            }
            this.f891c[i2] = i;
            this.f892d[i2] = e2;
            this.f893e++;
            return;
        }
        this.f891c[i2] = i;
        this.f892d[i2] = e2;
    }

    public int b() {
        if (this.f890b) {
            d();
        }
        return this.f893e;
    }

    public int d(int i) {
        if (this.f890b) {
            d();
        }
        return this.f891c[i];
    }

    public E e(int i) {
        if (this.f890b) {
            d();
        }
        return this.f892d[i];
    }

    public int f(int i) {
        if (this.f890b) {
            d();
        }
        return b.a(this.f891c, this.f893e, i);
    }

    public void c() {
        int i = this.f893e;
        Object[] objArr = this.f892d;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.f893e = 0;
        this.f890b = false;
    }

    public void c(int i, E e2) {
        if (this.f893e == 0 || i > this.f891c[this.f893e - 1]) {
            if (this.f890b && this.f893e >= this.f891c.length) {
                d();
            }
            int i2 = this.f893e;
            if (i2 >= this.f891c.length) {
                int a2 = b.a(i2 + 1);
                int[] iArr = new int[a2];
                Object[] objArr = new Object[a2];
                System.arraycopy(this.f891c, 0, iArr, 0, this.f891c.length);
                System.arraycopy(this.f892d, 0, objArr, 0, this.f892d.length);
                this.f891c = iArr;
                this.f892d = objArr;
            }
            this.f891c[i2] = i;
            this.f892d[i2] = e2;
            this.f893e = i2 + 1;
            return;
        }
        b(i, e2);
    }

    public String toString() {
        if (b() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f893e * 28);
        sb.append('{');
        for (int i = 0; i < this.f893e; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(d(i));
            sb.append('=');
            Object e2 = e(i);
            if (e2 != this) {
                sb.append(e2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
