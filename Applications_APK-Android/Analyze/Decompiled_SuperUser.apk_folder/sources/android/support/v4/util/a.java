package android.support.v4.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* compiled from: ArraySet */
public final class a<E> implements Collection<E>, Set<E> {

    /* renamed from: a  reason: collision with root package name */
    static Object[] f846a;

    /* renamed from: b  reason: collision with root package name */
    static int f847b;

    /* renamed from: c  reason: collision with root package name */
    static Object[] f848c;

    /* renamed from: d  reason: collision with root package name */
    static int f849d;
    private static final int[] j = new int[0];
    private static final Object[] k = new Object[0];

    /* renamed from: e  reason: collision with root package name */
    final boolean f850e;

    /* renamed from: f  reason: collision with root package name */
    int[] f851f;

    /* renamed from: g  reason: collision with root package name */
    Object[] f852g;

    /* renamed from: h  reason: collision with root package name */
    int f853h;
    g<E, E> i;

    private int a(Object obj, int i2) {
        int i3 = this.f853h;
        if (i3 == 0) {
            return -1;
        }
        int a2 = b.a(this.f851f, i3, i2);
        if (a2 < 0 || obj.equals(this.f852g[a2])) {
            return a2;
        }
        int i4 = a2 + 1;
        while (i4 < i3 && this.f851f[i4] == i2) {
            if (obj.equals(this.f852g[i4])) {
                return i4;
            }
            i4++;
        }
        int i5 = a2 - 1;
        while (i5 >= 0 && this.f851f[i5] == i2) {
            if (obj.equals(this.f852g[i5])) {
                return i5;
            }
            i5--;
        }
        return i4 ^ -1;
    }

    private int a() {
        int i2 = this.f853h;
        if (i2 == 0) {
            return -1;
        }
        int a2 = b.a(this.f851f, i2, 0);
        if (a2 < 0 || this.f852g[a2] == null) {
            return a2;
        }
        int i3 = a2 + 1;
        while (i3 < i2 && this.f851f[i3] == 0) {
            if (this.f852g[i3] == null) {
                return i3;
            }
            i3++;
        }
        int i4 = a2 - 1;
        while (i4 >= 0 && this.f851f[i4] == 0) {
            if (this.f852g[i4] == null) {
                return i4;
            }
            i4--;
        }
        return i3 ^ -1;
    }

    private void d(int i2) {
        if (i2 == 8) {
            synchronized (a.class) {
                if (f848c != null) {
                    Object[] objArr = f848c;
                    this.f852g = objArr;
                    f848c = (Object[]) objArr[0];
                    this.f851f = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    f849d--;
                    return;
                }
            }
        } else if (i2 == 4) {
            synchronized (a.class) {
                if (f846a != null) {
                    Object[] objArr2 = f846a;
                    this.f852g = objArr2;
                    f846a = (Object[]) objArr2[0];
                    this.f851f = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    f847b--;
                    return;
                }
            }
        }
        this.f851f = new int[i2];
        this.f852g = new Object[i2];
    }

    private static void a(int[] iArr, Object[] objArr, int i2) {
        if (iArr.length == 8) {
            synchronized (a.class) {
                if (f849d < 10) {
                    objArr[0] = f848c;
                    objArr[1] = iArr;
                    for (int i3 = i2 - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    f848c = objArr;
                    f849d++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (a.class) {
                if (f847b < 10) {
                    objArr[0] = f846a;
                    objArr[1] = iArr;
                    for (int i4 = i2 - 1; i4 >= 2; i4--) {
                        objArr[i4] = null;
                    }
                    f846a = objArr;
                    f847b++;
                }
            }
        }
    }

    public a() {
        this(0, false);
    }

    public a(int i2, boolean z) {
        this.f850e = z;
        if (i2 == 0) {
            this.f851f = j;
            this.f852g = k;
        } else {
            d(i2);
        }
        this.f853h = 0;
    }

    public void clear() {
        if (this.f853h != 0) {
            a(this.f851f, this.f852g, this.f853h);
            this.f851f = j;
            this.f852g = k;
            this.f853h = 0;
        }
    }

    public void a(int i2) {
        if (this.f851f.length < i2) {
            int[] iArr = this.f851f;
            Object[] objArr = this.f852g;
            d(i2);
            if (this.f853h > 0) {
                System.arraycopy(iArr, 0, this.f851f, 0, this.f853h);
                System.arraycopy(objArr, 0, this.f852g, 0, this.f853h);
            }
            a(iArr, objArr, this.f853h);
        }
    }

    public boolean contains(Object obj) {
        return a(obj) >= 0;
    }

    public int a(Object obj) {
        if (obj == null) {
            return a();
        }
        return a(obj, this.f850e ? System.identityHashCode(obj) : obj.hashCode());
    }

    public E b(int i2) {
        return this.f852g[i2];
    }

    public boolean isEmpty() {
        return this.f853h <= 0;
    }

    public boolean add(E e2) {
        int i2;
        int a2;
        if (e2 == null) {
            a2 = a();
            i2 = 0;
        } else {
            int identityHashCode = this.f850e ? System.identityHashCode(e2) : e2.hashCode();
            i2 = identityHashCode;
            a2 = a(e2, identityHashCode);
        }
        if (a2 >= 0) {
            return false;
        }
        int i3 = a2 ^ -1;
        if (this.f853h >= this.f851f.length) {
            int i4 = this.f853h >= 8 ? this.f853h + (this.f853h >> 1) : this.f853h >= 4 ? 8 : 4;
            int[] iArr = this.f851f;
            Object[] objArr = this.f852g;
            d(i4);
            if (this.f851f.length > 0) {
                System.arraycopy(iArr, 0, this.f851f, 0, iArr.length);
                System.arraycopy(objArr, 0, this.f852g, 0, objArr.length);
            }
            a(iArr, objArr, this.f853h);
        }
        if (i3 < this.f853h) {
            System.arraycopy(this.f851f, i3, this.f851f, i3 + 1, this.f853h - i3);
            System.arraycopy(this.f852g, i3, this.f852g, i3 + 1, this.f853h - i3);
        }
        this.f851f[i3] = i2;
        this.f852g[i3] = e2;
        this.f853h++;
        return true;
    }

    public boolean remove(Object obj) {
        int a2 = a(obj);
        if (a2 < 0) {
            return false;
        }
        c(a2);
        return true;
    }

    public E c(int i2) {
        int i3 = 8;
        E e2 = this.f852g[i2];
        if (this.f853h <= 1) {
            a(this.f851f, this.f852g, this.f853h);
            this.f851f = j;
            this.f852g = k;
            this.f853h = 0;
        } else if (this.f851f.length <= 8 || this.f853h >= this.f851f.length / 3) {
            this.f853h--;
            if (i2 < this.f853h) {
                System.arraycopy(this.f851f, i2 + 1, this.f851f, i2, this.f853h - i2);
                System.arraycopy(this.f852g, i2 + 1, this.f852g, i2, this.f853h - i2);
            }
            this.f852g[this.f853h] = null;
        } else {
            if (this.f853h > 8) {
                i3 = this.f853h + (this.f853h >> 1);
            }
            int[] iArr = this.f851f;
            Object[] objArr = this.f852g;
            d(i3);
            this.f853h--;
            if (i2 > 0) {
                System.arraycopy(iArr, 0, this.f851f, 0, i2);
                System.arraycopy(objArr, 0, this.f852g, 0, i2);
            }
            if (i2 < this.f853h) {
                System.arraycopy(iArr, i2 + 1, this.f851f, i2, this.f853h - i2);
                System.arraycopy(objArr, i2 + 1, this.f852g, i2, this.f853h - i2);
            }
        }
        return e2;
    }

    public int size() {
        return this.f853h;
    }

    public Object[] toArray() {
        Object[] objArr = new Object[this.f853h];
        System.arraycopy(this.f852g, 0, objArr, 0, this.f853h);
        return objArr;
    }

    public <T> T[] toArray(T[] tArr) {
        T[] tArr2;
        if (tArr.length < this.f853h) {
            tArr2 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), this.f853h);
        } else {
            tArr2 = tArr;
        }
        System.arraycopy(this.f852g, 0, tArr2, 0, this.f853h);
        if (tArr2.length > this.f853h) {
            tArr2[this.f853h] = null;
        }
        return tArr2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Set)) {
            return false;
        }
        Set set = (Set) obj;
        if (size() != set.size()) {
            return false;
        }
        int i2 = 0;
        while (i2 < this.f853h) {
            try {
                if (!set.contains(b(i2))) {
                    return false;
                }
                i2++;
            } catch (NullPointerException e2) {
                return false;
            } catch (ClassCastException e3) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int[] iArr = this.f851f;
        int i2 = this.f853h;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            i3 += iArr[i4];
        }
        return i3;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f853h * 14);
        sb.append('{');
        for (int i2 = 0; i2 < this.f853h; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            Object b2 = b(i2);
            if (b2 != this) {
                sb.append(b2);
            } else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    private g<E, E> b() {
        if (this.i == null) {
            this.i = new g<E, E>() {
                /* access modifiers changed from: protected */
                public int a() {
                    return a.this.f853h;
                }

                /* access modifiers changed from: protected */
                public Object a(int i, int i2) {
                    return a.this.f852g[i];
                }

                /* access modifiers changed from: protected */
                public int a(Object obj) {
                    return a.this.a(obj);
                }

                /* access modifiers changed from: protected */
                public int b(Object obj) {
                    return a.this.a(obj);
                }

                /* access modifiers changed from: protected */
                public Map<E, E> b() {
                    throw new UnsupportedOperationException("not a map");
                }

                /* access modifiers changed from: protected */
                public void a(E e2, E e3) {
                    a.this.add(e2);
                }

                /* access modifiers changed from: protected */
                public E a(int i, E e2) {
                    throw new UnsupportedOperationException("not a map");
                }

                /* access modifiers changed from: protected */
                public void a(int i) {
                    a.this.c(i);
                }

                /* access modifiers changed from: protected */
                public void c() {
                    a.this.clear();
                }
            };
        }
        return this.i;
    }

    public Iterator<E> iterator() {
        return b().e().iterator();
    }

    public boolean containsAll(Collection<?> collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean addAll(Collection<? extends E> collection) {
        a(this.f853h + collection.size());
        boolean z = false;
        for (Object add : collection) {
            z |= add(add);
        }
        return z;
    }

    public boolean removeAll(Collection<?> collection) {
        boolean z = false;
        for (Object remove : collection) {
            z |= remove(remove);
        }
        return z;
    }

    public boolean retainAll(Collection<?> collection) {
        boolean z = false;
        for (int i2 = this.f853h - 1; i2 >= 0; i2--) {
            if (!collection.contains(this.f852g[i2])) {
                c(i2);
                z = true;
            }
        }
        return z;
    }
}
