package android.support.v4.util;

public final class Pools {

    public interface a<T> {
        T a();

        boolean a(T t);
    }

    public static class SimplePool<T> implements a<T> {

        /* renamed from: a  reason: collision with root package name */
        private final Object[] f843a;

        /* renamed from: b  reason: collision with root package name */
        private int f844b;

        public SimplePool(int i) {
            if (i <= 0) {
                throw new IllegalArgumentException("The max pool size must be > 0");
            }
            this.f843a = new Object[i];
        }

        public T a() {
            if (this.f844b <= 0) {
                return null;
            }
            int i = this.f844b - 1;
            T t = this.f843a[i];
            this.f843a[i] = null;
            this.f844b--;
            return t;
        }

        public boolean a(T t) {
            if (b(t)) {
                throw new IllegalStateException("Already in the pool!");
            } else if (this.f844b >= this.f843a.length) {
                return false;
            } else {
                this.f843a[this.f844b] = t;
                this.f844b++;
                return true;
            }
        }

        private boolean b(T t) {
            for (int i = 0; i < this.f844b; i++) {
                if (this.f843a[i] == t) {
                    return true;
                }
            }
            return false;
        }
    }

    public static class SynchronizedPool<T> extends SimplePool<T> {

        /* renamed from: a  reason: collision with root package name */
        private final Object f845a = new Object();

        public SynchronizedPool(int i) {
            super(i);
        }

        public T a() {
            T a2;
            synchronized (this.f845a) {
                a2 = super.a();
            }
            return a2;
        }

        public boolean a(T t) {
            boolean a2;
            synchronized (this.f845a) {
                a2 = super.a(t);
            }
            return a2;
        }
    }
}
