package android.support.v4.util;

import java.util.Map;

/* compiled from: SimpleArrayMap */
public class i<K, V> {

    /* renamed from: b  reason: collision with root package name */
    static Object[] f882b;

    /* renamed from: c  reason: collision with root package name */
    static int f883c;

    /* renamed from: d  reason: collision with root package name */
    static Object[] f884d;

    /* renamed from: e  reason: collision with root package name */
    static int f885e;

    /* renamed from: f  reason: collision with root package name */
    int[] f886f;

    /* renamed from: g  reason: collision with root package name */
    Object[] f887g;

    /* renamed from: h  reason: collision with root package name */
    int f888h;

    /* access modifiers changed from: package-private */
    public int a(Object obj, int i) {
        int i2 = this.f888h;
        if (i2 == 0) {
            return -1;
        }
        int a2 = b.a(this.f886f, i2, i);
        if (a2 < 0 || obj.equals(this.f887g[a2 << 1])) {
            return a2;
        }
        int i3 = a2 + 1;
        while (i3 < i2 && this.f886f[i3] == i) {
            if (obj.equals(this.f887g[i3 << 1])) {
                return i3;
            }
            i3++;
        }
        int i4 = a2 - 1;
        while (i4 >= 0 && this.f886f[i4] == i) {
            if (obj.equals(this.f887g[i4 << 1])) {
                return i4;
            }
            i4--;
        }
        return i3 ^ -1;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        int i = this.f888h;
        if (i == 0) {
            return -1;
        }
        int a2 = b.a(this.f886f, i, 0);
        if (a2 < 0 || this.f887g[a2 << 1] == null) {
            return a2;
        }
        int i2 = a2 + 1;
        while (i2 < i && this.f886f[i2] == 0) {
            if (this.f887g[i2 << 1] == null) {
                return i2;
            }
            i2++;
        }
        int i3 = a2 - 1;
        while (i3 >= 0 && this.f886f[i3] == 0) {
            if (this.f887g[i3 << 1] == null) {
                return i3;
            }
            i3--;
        }
        return i2 ^ -1;
    }

    private void e(int i) {
        if (i == 8) {
            synchronized (ArrayMap.class) {
                if (f884d != null) {
                    Object[] objArr = f884d;
                    this.f887g = objArr;
                    f884d = (Object[]) objArr[0];
                    this.f886f = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    f885e--;
                    return;
                }
            }
        } else if (i == 4) {
            synchronized (ArrayMap.class) {
                if (f882b != null) {
                    Object[] objArr2 = f882b;
                    this.f887g = objArr2;
                    f882b = (Object[]) objArr2[0];
                    this.f886f = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    f883c--;
                    return;
                }
            }
        }
        this.f886f = new int[i];
        this.f887g = new Object[(i << 1)];
    }

    private static void a(int[] iArr, Object[] objArr, int i) {
        if (iArr.length == 8) {
            synchronized (ArrayMap.class) {
                if (f885e < 10) {
                    objArr[0] = f884d;
                    objArr[1] = iArr;
                    for (int i2 = (i << 1) - 1; i2 >= 2; i2--) {
                        objArr[i2] = null;
                    }
                    f884d = objArr;
                    f885e++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (ArrayMap.class) {
                if (f883c < 10) {
                    objArr[0] = f882b;
                    objArr[1] = iArr;
                    for (int i3 = (i << 1) - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    f882b = objArr;
                    f883c++;
                }
            }
        }
    }

    public i() {
        this.f886f = b.f855a;
        this.f887g = b.f857c;
        this.f888h = 0;
    }

    public i(int i) {
        if (i == 0) {
            this.f886f = b.f855a;
            this.f887g = b.f857c;
        } else {
            e(i);
        }
        this.f888h = 0;
    }

    public i(i iVar) {
        this();
        if (iVar != null) {
            a(iVar);
        }
    }

    public void clear() {
        if (this.f888h != 0) {
            a(this.f886f, this.f887g, this.f888h);
            this.f886f = b.f855a;
            this.f887g = b.f857c;
            this.f888h = 0;
        }
    }

    public void a(int i) {
        if (this.f886f.length < i) {
            int[] iArr = this.f886f;
            Object[] objArr = this.f887g;
            e(i);
            if (this.f888h > 0) {
                System.arraycopy(iArr, 0, this.f886f, 0, this.f888h);
                System.arraycopy(objArr, 0, this.f887g, 0, this.f888h << 1);
            }
            a(iArr, objArr, this.f888h);
        }
    }

    public boolean containsKey(Object obj) {
        return a(obj) >= 0;
    }

    public int a(Object obj) {
        return obj == null ? a() : a(obj, obj.hashCode());
    }

    /* access modifiers changed from: package-private */
    public int b(Object obj) {
        int i = 1;
        int i2 = this.f888h * 2;
        Object[] objArr = this.f887g;
        if (obj == null) {
            while (i < i2) {
                if (objArr[i] == null) {
                    return i >> 1;
                }
                i += 2;
            }
        } else {
            while (i < i2) {
                if (obj.equals(objArr[i])) {
                    return i >> 1;
                }
                i += 2;
            }
        }
        return -1;
    }

    public boolean containsValue(Object obj) {
        return b(obj) >= 0;
    }

    public V get(Object obj) {
        int a2 = a(obj);
        if (a2 >= 0) {
            return this.f887g[(a2 << 1) + 1];
        }
        return null;
    }

    public K b(int i) {
        return this.f887g[i << 1];
    }

    public V c(int i) {
        return this.f887g[(i << 1) + 1];
    }

    public V a(int i, V v) {
        int i2 = (i << 1) + 1;
        V v2 = this.f887g[i2];
        this.f887g[i2] = v;
        return v2;
    }

    public boolean isEmpty() {
        return this.f888h <= 0;
    }

    public V put(K k, V v) {
        int hashCode;
        int a2;
        int i = 8;
        if (k == null) {
            a2 = a();
            hashCode = 0;
        } else {
            hashCode = k.hashCode();
            a2 = a(k, hashCode);
        }
        if (a2 >= 0) {
            int i2 = (a2 << 1) + 1;
            V v2 = this.f887g[i2];
            this.f887g[i2] = v;
            return v2;
        }
        int i3 = a2 ^ -1;
        if (this.f888h >= this.f886f.length) {
            if (this.f888h >= 8) {
                i = this.f888h + (this.f888h >> 1);
            } else if (this.f888h < 4) {
                i = 4;
            }
            int[] iArr = this.f886f;
            Object[] objArr = this.f887g;
            e(i);
            if (this.f886f.length > 0) {
                System.arraycopy(iArr, 0, this.f886f, 0, iArr.length);
                System.arraycopy(objArr, 0, this.f887g, 0, objArr.length);
            }
            a(iArr, objArr, this.f888h);
        }
        if (i3 < this.f888h) {
            System.arraycopy(this.f886f, i3, this.f886f, i3 + 1, this.f888h - i3);
            System.arraycopy(this.f887g, i3 << 1, this.f887g, (i3 + 1) << 1, (this.f888h - i3) << 1);
        }
        this.f886f[i3] = hashCode;
        this.f887g[i3 << 1] = k;
        this.f887g[(i3 << 1) + 1] = v;
        this.f888h++;
        return null;
    }

    public void a(i<? extends K, ? extends V> iVar) {
        int i = iVar.f888h;
        a(this.f888h + i);
        if (this.f888h != 0) {
            for (int i2 = 0; i2 < i; i2++) {
                put(iVar.b(i2), iVar.c(i2));
            }
        } else if (i > 0) {
            System.arraycopy(iVar.f886f, 0, this.f886f, 0, i);
            System.arraycopy(iVar.f887g, 0, this.f887g, 0, i << 1);
            this.f888h = i;
        }
    }

    public V remove(Object obj) {
        int a2 = a(obj);
        if (a2 >= 0) {
            return d(a2);
        }
        return null;
    }

    public V d(int i) {
        int i2 = 8;
        V v = this.f887g[(i << 1) + 1];
        if (this.f888h <= 1) {
            a(this.f886f, this.f887g, this.f888h);
            this.f886f = b.f855a;
            this.f887g = b.f857c;
            this.f888h = 0;
        } else if (this.f886f.length <= 8 || this.f888h >= this.f886f.length / 3) {
            this.f888h--;
            if (i < this.f888h) {
                System.arraycopy(this.f886f, i + 1, this.f886f, i, this.f888h - i);
                System.arraycopy(this.f887g, (i + 1) << 1, this.f887g, i << 1, (this.f888h - i) << 1);
            }
            this.f887g[this.f888h << 1] = null;
            this.f887g[(this.f888h << 1) + 1] = null;
        } else {
            if (this.f888h > 8) {
                i2 = this.f888h + (this.f888h >> 1);
            }
            int[] iArr = this.f886f;
            Object[] objArr = this.f887g;
            e(i2);
            this.f888h--;
            if (i > 0) {
                System.arraycopy(iArr, 0, this.f886f, 0, i);
                System.arraycopy(objArr, 0, this.f887g, 0, i << 1);
            }
            if (i < this.f888h) {
                System.arraycopy(iArr, i + 1, this.f886f, i, this.f888h - i);
                System.arraycopy(objArr, (i + 1) << 1, this.f887g, i << 1, (this.f888h - i) << 1);
            }
        }
        return v;
    }

    public int size() {
        return this.f888h;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof i) {
            i iVar = (i) obj;
            if (size() != iVar.size()) {
                return false;
            }
            int i = 0;
            while (i < this.f888h) {
                try {
                    Object b2 = b(i);
                    Object c2 = c(i);
                    Object obj2 = iVar.get(b2);
                    if (c2 == null) {
                        if (obj2 != null || !iVar.containsKey(b2)) {
                            return false;
                        }
                    } else if (!c2.equals(obj2)) {
                        return false;
                    }
                    i++;
                } catch (NullPointerException e2) {
                    return false;
                } catch (ClassCastException e3) {
                    return false;
                }
            }
            return true;
        } else if (!(obj instanceof Map)) {
            return false;
        } else {
            Map map = (Map) obj;
            if (size() != map.size()) {
                return false;
            }
            int i2 = 0;
            while (i2 < this.f888h) {
                try {
                    Object b3 = b(i2);
                    Object c3 = c(i2);
                    Object obj3 = map.get(b3);
                    if (c3 == null) {
                        if (obj3 != null || !map.containsKey(b3)) {
                            return false;
                        }
                    } else if (!c3.equals(obj3)) {
                        return false;
                    }
                    i2++;
                } catch (NullPointerException e4) {
                    return false;
                } catch (ClassCastException e5) {
                    return false;
                }
            }
            return true;
        }
    }

    public int hashCode() {
        int[] iArr = this.f886f;
        Object[] objArr = this.f887g;
        int i = this.f888h;
        int i2 = 1;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            Object obj = objArr[i2];
            i4 += (obj == null ? 0 : obj.hashCode()) ^ iArr[i3];
            i3++;
            i2 += 2;
        }
        return i4;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f888h * 28);
        sb.append('{');
        for (int i = 0; i < this.f888h; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            Object b2 = b(i);
            if (b2 != this) {
                sb.append(b2);
            } else {
                sb.append("(this Map)");
            }
            sb.append('=');
            Object c2 = c(i);
            if (c2 != this) {
                sb.append(c2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
