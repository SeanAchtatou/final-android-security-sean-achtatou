package android.support.v4.util;

/* compiled from: LongSparseArray */
public class e<E> implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f860a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private boolean f861b;

    /* renamed from: c  reason: collision with root package name */
    private long[] f862c;

    /* renamed from: d  reason: collision with root package name */
    private Object[] f863d;

    /* renamed from: e  reason: collision with root package name */
    private int f864e;

    public e() {
        this(10);
    }

    public e(int i) {
        this.f861b = false;
        if (i == 0) {
            this.f862c = b.f856b;
            this.f863d = b.f857c;
        } else {
            int b2 = b.b(i);
            this.f862c = new long[b2];
            this.f863d = new Object[b2];
        }
        this.f864e = 0;
    }

    /* renamed from: a */
    public e<E> clone() {
        try {
            e<E> eVar = (e) super.clone();
            try {
                eVar.f862c = (long[]) this.f862c.clone();
                eVar.f863d = (Object[]) this.f863d.clone();
                return eVar;
            } catch (CloneNotSupportedException e2) {
                return eVar;
            }
        } catch (CloneNotSupportedException e3) {
            return null;
        }
    }

    public E a(long j) {
        return a(j, null);
    }

    public E a(long j, E e2) {
        int a2 = b.a(this.f862c, this.f864e, j);
        return (a2 < 0 || this.f863d[a2] == f860a) ? e2 : this.f863d[a2];
    }

    public void b(long j) {
        int a2 = b.a(this.f862c, this.f864e, j);
        if (a2 >= 0 && this.f863d[a2] != f860a) {
            this.f863d[a2] = f860a;
            this.f861b = true;
        }
    }

    public void c(long j) {
        b(j);
    }

    public void a(int i) {
        if (this.f863d[i] != f860a) {
            this.f863d[i] = f860a;
            this.f861b = true;
        }
    }

    private void d() {
        int i = this.f864e;
        long[] jArr = this.f862c;
        Object[] objArr = this.f863d;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != f860a) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.f861b = false;
        this.f864e = i2;
    }

    public void b(long j, E e2) {
        int a2 = b.a(this.f862c, this.f864e, j);
        if (a2 >= 0) {
            this.f863d[a2] = e2;
            return;
        }
        int i = a2 ^ -1;
        if (i >= this.f864e || this.f863d[i] != f860a) {
            if (this.f861b && this.f864e >= this.f862c.length) {
                d();
                i = b.a(this.f862c, this.f864e, j) ^ -1;
            }
            if (this.f864e >= this.f862c.length) {
                int b2 = b.b(this.f864e + 1);
                long[] jArr = new long[b2];
                Object[] objArr = new Object[b2];
                System.arraycopy(this.f862c, 0, jArr, 0, this.f862c.length);
                System.arraycopy(this.f863d, 0, objArr, 0, this.f863d.length);
                this.f862c = jArr;
                this.f863d = objArr;
            }
            if (this.f864e - i != 0) {
                System.arraycopy(this.f862c, i, this.f862c, i + 1, this.f864e - i);
                System.arraycopy(this.f863d, i, this.f863d, i + 1, this.f864e - i);
            }
            this.f862c[i] = j;
            this.f863d[i] = e2;
            this.f864e++;
            return;
        }
        this.f862c[i] = j;
        this.f863d[i] = e2;
    }

    public int b() {
        if (this.f861b) {
            d();
        }
        return this.f864e;
    }

    public long b(int i) {
        if (this.f861b) {
            d();
        }
        return this.f862c[i];
    }

    public E c(int i) {
        if (this.f861b) {
            d();
        }
        return this.f863d[i];
    }

    public void c() {
        int i = this.f864e;
        Object[] objArr = this.f863d;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.f864e = 0;
        this.f861b = false;
    }

    public String toString() {
        if (b() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f864e * 28);
        sb.append('{');
        for (int i = 0; i < this.f864e; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(b(i));
            sb.append('=');
            Object c2 = c(i);
            if (c2 != this) {
                sb.append(c2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
