package android.support.v4.util;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class ArrayMap<K, V> extends i<K, V> implements Map<K, V> {

    /* renamed from: a  reason: collision with root package name */
    g<K, V> f841a;

    public ArrayMap() {
    }

    public ArrayMap(int i) {
        super(i);
    }

    public ArrayMap(i iVar) {
        super(iVar);
    }

    private g<K, V> b() {
        if (this.f841a == null) {
            this.f841a = new g<K, V>() {
                /* access modifiers changed from: protected */
                public int a() {
                    return ArrayMap.this.f888h;
                }

                /* access modifiers changed from: protected */
                public Object a(int i, int i2) {
                    return ArrayMap.this.f887g[(i << 1) + i2];
                }

                /* access modifiers changed from: protected */
                public int a(Object obj) {
                    return ArrayMap.this.a(obj);
                }

                /* access modifiers changed from: protected */
                public int b(Object obj) {
                    return ArrayMap.this.b(obj);
                }

                /* access modifiers changed from: protected */
                public Map<K, V> b() {
                    return ArrayMap.this;
                }

                /* access modifiers changed from: protected */
                public void a(K k, V v) {
                    ArrayMap.this.put(k, v);
                }

                /* access modifiers changed from: protected */
                public V a(int i, V v) {
                    return ArrayMap.this.a(i, v);
                }

                /* access modifiers changed from: protected */
                public void a(int i) {
                    ArrayMap.this.d(i);
                }

                /* access modifiers changed from: protected */
                public void c() {
                    ArrayMap.this.clear();
                }
            };
        }
        return this.f841a;
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        a(this.f888h + map.size());
        for (Map.Entry next : map.entrySet()) {
            put(next.getKey(), next.getValue());
        }
    }

    public boolean a(Collection<?> collection) {
        return g.c(this, collection);
    }

    public Set<Map.Entry<K, V>> entrySet() {
        return b().d();
    }

    public Set<K> keySet() {
        return b().e();
    }

    public Collection<V> values() {
        return b().f();
    }
}
