package android.support.v4.util;

/* compiled from: Pair */
public class h<F, S> {

    /* renamed from: a  reason: collision with root package name */
    public final F f880a;

    /* renamed from: b  reason: collision with root package name */
    public final S f881b;

    public h(F f2, S s) {
        this.f880a = f2;
        this.f881b = s;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        if (!a(hVar.f880a, this.f880a) || !a(hVar.f881b, this.f881b)) {
            return false;
        }
        return true;
    }

    private static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public int hashCode() {
        int i = 0;
        int hashCode = this.f880a == null ? 0 : this.f880a.hashCode();
        if (this.f881b != null) {
            i = this.f881b.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        return "Pair{" + String.valueOf(this.f880a) + " " + String.valueOf(this.f881b) + "}";
    }
}
