package android.support.v4.util;

import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.Writer;

/* compiled from: LogWriter */
public class d extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private final String f858a;

    /* renamed from: b  reason: collision with root package name */
    private StringBuilder f859b = new StringBuilder((int) FileUtils.FileMode.MODE_IWUSR);

    public d(String str) {
        this.f858a = str;
    }

    public void close() {
        a();
    }

    public void flush() {
        a();
    }

    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c2 = cArr[i + i3];
            if (c2 == 10) {
                a();
            } else {
                this.f859b.append(c2);
            }
        }
    }

    private void a() {
        if (this.f859b.length() > 0) {
            Log.d(this.f858a, this.f859b.toString());
            this.f859b.delete(0, this.f859b.length());
        }
    }
}
