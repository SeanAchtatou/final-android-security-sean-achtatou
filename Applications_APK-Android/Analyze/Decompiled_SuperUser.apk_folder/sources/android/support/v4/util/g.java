package android.support.v4.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* compiled from: MapCollections */
abstract class g<K, V> {

    /* renamed from: b  reason: collision with root package name */
    g<K, V>.b f865b;

    /* renamed from: c  reason: collision with root package name */
    g<K, V>.c f866c;

    /* renamed from: d  reason: collision with root package name */
    g<K, V>.e f867d;

    /* access modifiers changed from: protected */
    public abstract int a();

    /* access modifiers changed from: protected */
    public abstract int a(Object obj);

    /* access modifiers changed from: protected */
    public abstract Object a(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract V a(int i, V v);

    /* access modifiers changed from: protected */
    public abstract void a(int i);

    /* access modifiers changed from: protected */
    public abstract void a(K k, V v);

    /* access modifiers changed from: protected */
    public abstract int b(Object obj);

    /* access modifiers changed from: protected */
    public abstract Map<K, V> b();

    /* access modifiers changed from: protected */
    public abstract void c();

    g() {
    }

    /* compiled from: MapCollections */
    final class a<T> implements Iterator<T> {

        /* renamed from: a  reason: collision with root package name */
        final int f868a;

        /* renamed from: b  reason: collision with root package name */
        int f869b;

        /* renamed from: c  reason: collision with root package name */
        int f870c;

        /* renamed from: d  reason: collision with root package name */
        boolean f871d = false;

        a(int i) {
            this.f868a = i;
            this.f869b = g.this.a();
        }

        public boolean hasNext() {
            return this.f870c < this.f869b;
        }

        public T next() {
            T a2 = g.this.a(this.f870c, this.f868a);
            this.f870c++;
            this.f871d = true;
            return a2;
        }

        public void remove() {
            if (!this.f871d) {
                throw new IllegalStateException();
            }
            this.f870c--;
            this.f869b--;
            this.f871d = false;
            g.this.a(this.f870c);
        }
    }

    /* compiled from: MapCollections */
    final class d implements Iterator<Map.Entry<K, V>>, Map.Entry<K, V> {

        /* renamed from: a  reason: collision with root package name */
        int f875a;

        /* renamed from: b  reason: collision with root package name */
        int f876b;

        /* renamed from: c  reason: collision with root package name */
        boolean f877c = false;

        d() {
            this.f875a = g.this.a() - 1;
            this.f876b = -1;
        }

        public boolean hasNext() {
            return this.f876b < this.f875a;
        }

        /* renamed from: a */
        public Map.Entry<K, V> next() {
            this.f876b++;
            this.f877c = true;
            return this;
        }

        public void remove() {
            if (!this.f877c) {
                throw new IllegalStateException();
            }
            g.this.a(this.f876b);
            this.f876b--;
            this.f875a--;
            this.f877c = false;
        }

        public K getKey() {
            if (this.f877c) {
                return g.this.a(this.f876b, 0);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public V getValue() {
            if (this.f877c) {
                return g.this.a(this.f876b, 1);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public V setValue(V v) {
            if (this.f877c) {
                return g.this.a(this.f876b, v);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public final boolean equals(Object obj) {
            boolean z = true;
            if (!this.f877c) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            } else if (!(obj instanceof Map.Entry)) {
                return false;
            } else {
                Map.Entry entry = (Map.Entry) obj;
                if (!b.a(entry.getKey(), g.this.a(this.f876b, 0)) || !b.a(entry.getValue(), g.this.a(this.f876b, 1))) {
                    z = false;
                }
                return z;
            }
        }

        public final int hashCode() {
            int i = 0;
            if (!this.f877c) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            }
            Object a2 = g.this.a(this.f876b, 0);
            Object a3 = g.this.a(this.f876b, 1);
            int hashCode = a2 == null ? 0 : a2.hashCode();
            if (a3 != null) {
                i = a3.hashCode();
            }
            return i ^ hashCode;
        }

        public final String toString() {
            return getKey() + "=" + getValue();
        }
    }

    /* compiled from: MapCollections */
    final class b implements Set<Map.Entry<K, V>> {
        b() {
        }

        /* renamed from: a */
        public boolean add(Map.Entry<K, V> entry) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
            int a2 = g.this.a();
            for (Map.Entry entry : collection) {
                g.this.a(entry.getKey(), entry.getValue());
            }
            return a2 != g.this.a();
        }

        public void clear() {
            g.this.c();
        }

        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            int a2 = g.this.a(entry.getKey());
            if (a2 >= 0) {
                return b.a(g.this.a(a2, 1), entry.getValue());
            }
            return false;
        }

        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean isEmpty() {
            return g.this.a() == 0;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new d();
        }

        public boolean remove(Object obj) {
            throw new UnsupportedOperationException();
        }

        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public int size() {
            return g.this.a();
        }

        public Object[] toArray() {
            throw new UnsupportedOperationException();
        }

        public <T> T[] toArray(T[] tArr) {
            throw new UnsupportedOperationException();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.util.g.a(java.util.Set, java.lang.Object):boolean
         arg types: [android.support.v4.util.g$b, java.lang.Object]
         candidates:
          android.support.v4.util.g.a(java.util.Map, java.util.Collection<?>):boolean
          android.support.v4.util.g.a(int, int):java.lang.Object
          android.support.v4.util.g.a(int, java.lang.Object):V
          android.support.v4.util.g.a(java.lang.Object, java.lang.Object):void
          android.support.v4.util.g.a(java.lang.Object[], int):T[]
          android.support.v4.util.g.a(java.util.Set, java.lang.Object):boolean */
        public boolean equals(Object obj) {
            return g.a((Set) this, obj);
        }

        public int hashCode() {
            int hashCode;
            int a2 = g.this.a() - 1;
            int i = 0;
            while (a2 >= 0) {
                Object a3 = g.this.a(a2, 0);
                Object a4 = g.this.a(a2, 1);
                int hashCode2 = a3 == null ? 0 : a3.hashCode();
                if (a4 == null) {
                    hashCode = 0;
                } else {
                    hashCode = a4.hashCode();
                }
                a2--;
                i += hashCode ^ hashCode2;
            }
            return i;
        }
    }

    /* compiled from: MapCollections */
    final class c implements Set<K> {
        c() {
        }

        public boolean add(K k) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends K> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            g.this.c();
        }

        public boolean contains(Object obj) {
            return g.this.a(obj) >= 0;
        }

        public boolean containsAll(Collection<?> collection) {
            return g.a(g.this.b(), collection);
        }

        public boolean isEmpty() {
            return g.this.a() == 0;
        }

        public Iterator<K> iterator() {
            return new a(0);
        }

        public boolean remove(Object obj) {
            int a2 = g.this.a(obj);
            if (a2 < 0) {
                return false;
            }
            g.this.a(a2);
            return true;
        }

        public boolean removeAll(Collection<?> collection) {
            return g.b(g.this.b(), collection);
        }

        public boolean retainAll(Collection<?> collection) {
            return g.c(g.this.b(), collection);
        }

        public int size() {
            return g.this.a();
        }

        public Object[] toArray() {
            return g.this.b(0);
        }

        public <T> T[] toArray(T[] tArr) {
            return g.this.a(tArr, 0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.util.g.a(java.util.Set, java.lang.Object):boolean
         arg types: [android.support.v4.util.g$c, java.lang.Object]
         candidates:
          android.support.v4.util.g.a(java.util.Map, java.util.Collection<?>):boolean
          android.support.v4.util.g.a(int, int):java.lang.Object
          android.support.v4.util.g.a(int, java.lang.Object):V
          android.support.v4.util.g.a(java.lang.Object, java.lang.Object):void
          android.support.v4.util.g.a(java.lang.Object[], int):T[]
          android.support.v4.util.g.a(java.util.Set, java.lang.Object):boolean */
        public boolean equals(Object obj) {
            return g.a((Set) this, obj);
        }

        public int hashCode() {
            int i = 0;
            for (int a2 = g.this.a() - 1; a2 >= 0; a2--) {
                Object a3 = g.this.a(a2, 0);
                i += a3 == null ? 0 : a3.hashCode();
            }
            return i;
        }
    }

    /* compiled from: MapCollections */
    final class e implements Collection<V> {
        e() {
        }

        public boolean add(V v) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends V> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            g.this.c();
        }

        public boolean contains(Object obj) {
            return g.this.b(obj) >= 0;
        }

        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean isEmpty() {
            return g.this.a() == 0;
        }

        public Iterator<V> iterator() {
            return new a(1);
        }

        public boolean remove(Object obj) {
            int b2 = g.this.b(obj);
            if (b2 < 0) {
                return false;
            }
            g.this.a(b2);
            return true;
        }

        public boolean removeAll(Collection<?> collection) {
            int i = 0;
            int a2 = g.this.a();
            boolean z = false;
            while (i < a2) {
                if (collection.contains(g.this.a(i, 1))) {
                    g.this.a(i);
                    i--;
                    a2--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        public boolean retainAll(Collection<?> collection) {
            int i = 0;
            int a2 = g.this.a();
            boolean z = false;
            while (i < a2) {
                if (!collection.contains(g.this.a(i, 1))) {
                    g.this.a(i);
                    i--;
                    a2--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        public int size() {
            return g.this.a();
        }

        public Object[] toArray() {
            return g.this.b(1);
        }

        public <T> T[] toArray(T[] tArr) {
            return g.this.a(tArr, 1);
        }
    }

    public static <K, V> boolean a(Map<K, V> map, Collection<?> collection) {
        for (Object containsKey : collection) {
            if (!map.containsKey(containsKey)) {
                return false;
            }
        }
        return true;
    }

    public static <K, V> boolean b(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        for (Object remove : collection) {
            map.remove(remove);
        }
        return size != map.size();
    }

    public static <K, V> boolean c(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        Iterator<K> it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return size != map.size();
    }

    public Object[] b(int i) {
        int a2 = a();
        Object[] objArr = new Object[a2];
        for (int i2 = 0; i2 < a2; i2++) {
            objArr[i2] = a(i2, i);
        }
        return objArr;
    }

    public <T> T[] a(T[] tArr, int i) {
        T[] tArr2;
        int a2 = a();
        if (tArr.length < a2) {
            tArr2 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), a2);
        } else {
            tArr2 = tArr;
        }
        for (int i2 = 0; i2 < a2; i2++) {
            tArr2[i2] = a(i2, i);
        }
        if (tArr2.length > a2) {
            tArr2[a2] = null;
        }
        return tArr2;
    }

    public static <T> boolean a(Set<T> set, Object obj) {
        boolean z = true;
        if (set == obj) {
            return true;
        }
        if (!(obj instanceof Set)) {
            return false;
        }
        Set set2 = (Set) obj;
        try {
            if (set.size() != set2.size() || !set.containsAll(set2)) {
                z = false;
            }
            return z;
        } catch (ClassCastException | NullPointerException e2) {
            return false;
        }
    }

    public Set<Map.Entry<K, V>> d() {
        if (this.f865b == null) {
            this.f865b = new b();
        }
        return this.f865b;
    }

    public Set<K> e() {
        if (this.f866c == null) {
            this.f866c = new c();
        }
        return this.f866c;
    }

    public Collection<V> f() {
        if (this.f867d == null) {
            this.f867d = new e();
        }
        return this.f867d;
    }
}
