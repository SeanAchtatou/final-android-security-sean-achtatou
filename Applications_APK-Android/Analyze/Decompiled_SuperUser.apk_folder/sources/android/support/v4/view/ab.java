package android.support.v4.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.PointerIcon;

@TargetApi(24)
/* compiled from: PointerIconCompatApi24 */
class ab {
    public static Object a(Context context, int i) {
        return PointerIcon.getSystemIcon(context, i);
    }
}
