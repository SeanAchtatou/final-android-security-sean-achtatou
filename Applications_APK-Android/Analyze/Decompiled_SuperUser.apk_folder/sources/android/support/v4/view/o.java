package android.support.v4.view;

import android.annotation.TargetApi;
import android.view.ViewGroup;

@TargetApi(17)
/* compiled from: MarginLayoutParamsCompatJellybeanMr1 */
class o {
    public static int a(ViewGroup.MarginLayoutParams marginLayoutParams) {
        return marginLayoutParams.getMarginStart();
    }

    public static int b(ViewGroup.MarginLayoutParams marginLayoutParams) {
        return marginLayoutParams.getMarginEnd();
    }
}
