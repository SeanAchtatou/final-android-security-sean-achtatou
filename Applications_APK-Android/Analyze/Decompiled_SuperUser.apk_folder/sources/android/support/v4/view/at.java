package android.support.v4.view;

import android.os.Build;
import android.view.ViewGroup;

/* compiled from: ViewGroupCompat */
public final class at {

    /* renamed from: a  reason: collision with root package name */
    static final c f1008a;

    /* compiled from: ViewGroupCompat */
    interface c {
        void a(ViewGroup viewGroup, boolean z);
    }

    /* compiled from: ViewGroupCompat */
    static class f implements c {
        f() {
        }

        public void a(ViewGroup viewGroup, boolean z) {
        }
    }

    /* compiled from: ViewGroupCompat */
    static class a extends f {
        a() {
        }

        public void a(ViewGroup viewGroup, boolean z) {
            au.a(viewGroup, z);
        }
    }

    /* compiled from: ViewGroupCompat */
    static class b extends a {
        b() {
        }
    }

    /* compiled from: ViewGroupCompat */
    static class d extends b {
        d() {
        }
    }

    /* compiled from: ViewGroupCompat */
    static class e extends d {
        e() {
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f1008a = new e();
        } else if (i >= 18) {
            f1008a = new d();
        } else if (i >= 14) {
            f1008a = new b();
        } else if (i >= 11) {
            f1008a = new a();
        } else {
            f1008a = new f();
        }
    }

    public static void a(ViewGroup viewGroup, boolean z) {
        f1008a.a(viewGroup, z);
    }
}
