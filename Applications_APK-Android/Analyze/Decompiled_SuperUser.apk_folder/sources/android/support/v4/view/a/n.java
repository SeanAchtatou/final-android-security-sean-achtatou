package android.support.v4.view.a;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.a.o;
import android.support.v4.view.a.p;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AccessibilityNodeProviderCompat */
public class n {

    /* renamed from: a  reason: collision with root package name */
    private static final a f974a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f975b;

    /* compiled from: AccessibilityNodeProviderCompat */
    interface a {
        Object a(n nVar);
    }

    /* compiled from: AccessibilityNodeProviderCompat */
    static class d implements a {
        d() {
        }

        public Object a(n nVar) {
            return null;
        }
    }

    /* compiled from: AccessibilityNodeProviderCompat */
    private static class b extends d {
        b() {
        }

        public Object a(final n nVar) {
            return o.a(new o.a() {
                public boolean a(int i, int i2, Bundle bundle) {
                    return nVar.a(i, i2, bundle);
                }

                public List<Object> a(String str, int i) {
                    List<e> a2 = nVar.a(str, i);
                    if (a2 == null) {
                        return null;
                    }
                    ArrayList arrayList = new ArrayList();
                    int size = a2.size();
                    for (int i2 = 0; i2 < size; i2++) {
                        arrayList.add(a2.get(i2).a());
                    }
                    return arrayList;
                }

                public Object a(int i) {
                    e a2 = nVar.a(i);
                    if (a2 == null) {
                        return null;
                    }
                    return a2.a();
                }
            });
        }
    }

    /* compiled from: AccessibilityNodeProviderCompat */
    private static class c extends d {
        c() {
        }

        public Object a(final n nVar) {
            return p.a(new p.a() {
                public boolean a(int i, int i2, Bundle bundle) {
                    return nVar.a(i, i2, bundle);
                }

                public List<Object> a(String str, int i) {
                    List<e> a2 = nVar.a(str, i);
                    if (a2 == null) {
                        return null;
                    }
                    ArrayList arrayList = new ArrayList();
                    int size = a2.size();
                    for (int i2 = 0; i2 < size; i2++) {
                        arrayList.add(a2.get(i2).a());
                    }
                    return arrayList;
                }

                public Object a(int i) {
                    e a2 = nVar.a(i);
                    if (a2 == null) {
                        return null;
                    }
                    return a2.a();
                }

                public Object b(int i) {
                    e b2 = nVar.b(i);
                    if (b2 == null) {
                        return null;
                    }
                    return b2.a();
                }
            });
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            f974a = new c();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f974a = new b();
        } else {
            f974a = new d();
        }
    }

    public n() {
        this.f975b = f974a.a(this);
    }

    public n(Object obj) {
        this.f975b = obj;
    }

    public Object a() {
        return this.f975b;
    }

    public e a(int i) {
        return null;
    }

    public boolean a(int i, int i2, Bundle bundle) {
        return false;
    }

    public List<e> a(String str, int i) {
        return null;
    }

    public e b(int i) {
        return null;
    }
}
