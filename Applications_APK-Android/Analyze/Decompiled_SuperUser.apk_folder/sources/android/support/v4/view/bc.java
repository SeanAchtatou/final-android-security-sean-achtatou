package android.support.v4.view;

import android.view.View;

/* compiled from: ViewPropertyAnimatorListener */
public interface bc {
    void onAnimationCancel(View view);

    void onAnimationEnd(View view);

    void onAnimationStart(View view);
}
