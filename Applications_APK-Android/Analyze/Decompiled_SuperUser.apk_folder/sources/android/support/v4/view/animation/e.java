package android.support.v4.view.animation;

import android.annotation.TargetApi;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.view.animation.Interpolator;

@TargetApi(9)
/* compiled from: PathInterpolatorGingerbread */
class e implements Interpolator {

    /* renamed from: a  reason: collision with root package name */
    private final float[] f1003a;

    /* renamed from: b  reason: collision with root package name */
    private final float[] f1004b;

    public e(Path path) {
        PathMeasure pathMeasure = new PathMeasure(path, false);
        float length = pathMeasure.getLength();
        int i = ((int) (length / 0.002f)) + 1;
        this.f1003a = new float[i];
        this.f1004b = new float[i];
        float[] fArr = new float[2];
        for (int i2 = 0; i2 < i; i2++) {
            pathMeasure.getPosTan((((float) i2) * length) / ((float) (i - 1)), fArr, null);
            this.f1003a[i2] = fArr[0];
            this.f1004b[i2] = fArr[1];
        }
    }

    public e(float f2, float f3, float f4, float f5) {
        this(a(f2, f3, f4, f5));
    }

    public float getInterpolation(float f2) {
        int i;
        if (f2 <= 0.0f) {
            return 0.0f;
        }
        if (f2 >= 1.0f) {
            return 1.0f;
        }
        int i2 = 0;
        int length = this.f1003a.length - 1;
        while (length - i2 > 1) {
            int i3 = (i2 + length) / 2;
            if (f2 < this.f1003a[i3]) {
                i = i2;
            } else {
                int i4 = length;
                i = i3;
                i3 = i4;
            }
            i2 = i;
            length = i3;
        }
        float f3 = this.f1003a[length] - this.f1003a[i2];
        if (f3 == 0.0f) {
            return this.f1004b[i2];
        }
        float f4 = (f2 - this.f1003a[i2]) / f3;
        float f5 = this.f1004b[i2];
        return (f4 * (this.f1004b[length] - f5)) + f5;
    }

    private static Path a(float f2, float f3, float f4, float f5) {
        Path path = new Path();
        path.moveTo(0.0f, 0.0f);
        path.cubicTo(f2, f3, f4, f5, 1.0f, 1.0f);
        return path;
    }
}
