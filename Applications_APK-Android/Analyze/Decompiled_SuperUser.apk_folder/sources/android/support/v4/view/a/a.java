package android.support.v4.view.a;

import android.os.Build;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: AccessibilityEventCompat */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final e f955a;

    /* compiled from: AccessibilityEventCompat */
    interface e {
        int a(AccessibilityEvent accessibilityEvent);

        void a(AccessibilityEvent accessibilityEvent, int i);
    }

    /* compiled from: AccessibilityEventCompat */
    static class d implements e {
        d() {
        }

        public void a(AccessibilityEvent accessibilityEvent, int i) {
        }

        public int a(AccessibilityEvent accessibilityEvent) {
            return 0;
        }
    }

    /* renamed from: android.support.v4.view.a.a$a  reason: collision with other inner class name */
    /* compiled from: AccessibilityEventCompat */
    static class C0021a extends d {
        C0021a() {
        }
    }

    /* compiled from: AccessibilityEventCompat */
    static class b extends C0021a {
        b() {
        }
    }

    /* compiled from: AccessibilityEventCompat */
    static class c extends b {
        c() {
        }

        public void a(AccessibilityEvent accessibilityEvent, int i) {
            b.a(accessibilityEvent, i);
        }

        public int a(AccessibilityEvent accessibilityEvent) {
            return b.a(accessibilityEvent);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            f955a = new c();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f955a = new b();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f955a = new C0021a();
        } else {
            f955a = new d();
        }
    }

    public static q a(AccessibilityEvent accessibilityEvent) {
        return new q(accessibilityEvent);
    }

    public static void a(AccessibilityEvent accessibilityEvent, int i) {
        f955a.a(accessibilityEvent, i);
    }

    public static int b(AccessibilityEvent accessibilityEvent) {
        return f955a.a(accessibilityEvent);
    }
}
