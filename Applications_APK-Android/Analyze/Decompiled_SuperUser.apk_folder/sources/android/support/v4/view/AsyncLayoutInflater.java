package android.support.v4.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.util.Pools;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.concurrent.ArrayBlockingQueue;

public final class AsyncLayoutInflater {

    /* renamed from: a  reason: collision with root package name */
    LayoutInflater f899a;

    /* renamed from: b  reason: collision with root package name */
    Handler f900b;

    /* renamed from: c  reason: collision with root package name */
    c f901c;

    /* renamed from: d  reason: collision with root package name */
    private Handler.Callback f902d = new Handler.Callback() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public boolean handleMessage(Message message) {
            b bVar = (b) message.obj;
            if (bVar.f908d == null) {
                bVar.f908d = AsyncLayoutInflater.this.f899a.inflate(bVar.f907c, bVar.f906b, false);
            }
            bVar.f909e.a(bVar.f908d, bVar.f907c, bVar.f906b);
            AsyncLayoutInflater.this.f901c.a(bVar);
            return true;
        }
    };

    public interface d {
        void a(View view, int i, ViewGroup viewGroup);
    }

    public AsyncLayoutInflater(Context context) {
        this.f899a = new a(context);
        this.f900b = new Handler(this.f902d);
        this.f901c = c.a();
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        AsyncLayoutInflater f905a;

        /* renamed from: b  reason: collision with root package name */
        ViewGroup f906b;

        /* renamed from: c  reason: collision with root package name */
        int f907c;

        /* renamed from: d  reason: collision with root package name */
        View f908d;

        /* renamed from: e  reason: collision with root package name */
        d f909e;

        b() {
        }
    }

    private static class a extends LayoutInflater {

        /* renamed from: a  reason: collision with root package name */
        private static final String[] f904a = {"android.widget.", "android.webkit.", "android.app."};

        a(Context context) {
            super(context);
        }

        public LayoutInflater cloneInContext(Context context) {
            return new a(context);
        }

        /* access modifiers changed from: protected */
        public View onCreateView(String str, AttributeSet attributeSet) {
            String[] strArr = f904a;
            int length = strArr.length;
            int i = 0;
            while (i < length) {
                try {
                    View createView = createView(str, strArr[i], attributeSet);
                    if (createView != null) {
                        return createView;
                    }
                    i++;
                } catch (ClassNotFoundException e2) {
                }
            }
            return super.onCreateView(str, attributeSet);
        }
    }

    private static class c extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private static final c f910a = new c();

        /* renamed from: b  reason: collision with root package name */
        private ArrayBlockingQueue<b> f911b = new ArrayBlockingQueue<>(10);

        /* renamed from: c  reason: collision with root package name */
        private Pools.SynchronizedPool<b> f912c = new Pools.SynchronizedPool<>(10);

        private c() {
        }

        static {
            f910a.start();
        }

        public static c a() {
            return f910a;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public void run() {
            while (true) {
                try {
                    b take = this.f911b.take();
                    try {
                        take.f908d = take.f905a.f899a.inflate(take.f907c, take.f906b, false);
                    } catch (RuntimeException e2) {
                        Log.w("AsyncLayoutInflater", "Failed to inflate resource in the background! Retrying on the UI thread", e2);
                    }
                    Message.obtain(take.f905a.f900b, 0, take).sendToTarget();
                } catch (InterruptedException e3) {
                    Log.w("AsyncLayoutInflater", e3);
                }
            }
        }

        public void a(b bVar) {
            bVar.f909e = null;
            bVar.f905a = null;
            bVar.f906b = null;
            bVar.f907c = 0;
            bVar.f908d = null;
            this.f912c.a(bVar);
        }
    }
}
