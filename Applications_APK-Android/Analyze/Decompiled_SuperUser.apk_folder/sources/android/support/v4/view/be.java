package android.support.v4.view;

import android.os.Build;

/* compiled from: WindowInsetsCompat */
public class be {

    /* renamed from: a  reason: collision with root package name */
    private static final d f1029a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f1030b;

    /* compiled from: WindowInsetsCompat */
    private interface d {
        be a(Object obj);

        be a(Object obj, int i, int i2, int i3, int i4);

        int b(Object obj);

        int c(Object obj);

        int d(Object obj);

        int e(Object obj);

        boolean f(Object obj);

        boolean g(Object obj);
    }

    /* compiled from: WindowInsetsCompat */
    private static class c implements d {
        c() {
        }

        public int c(Object obj) {
            return 0;
        }

        public int e(Object obj) {
            return 0;
        }

        public int d(Object obj) {
            return 0;
        }

        public int b(Object obj) {
            return 0;
        }

        public boolean f(Object obj) {
            return false;
        }

        public boolean g(Object obj) {
            return false;
        }

        public be a(Object obj) {
            return null;
        }

        public be a(Object obj, int i, int i2, int i3, int i4) {
            return null;
        }
    }

    /* compiled from: WindowInsetsCompat */
    private static class a extends c {
        a() {
        }

        public be a(Object obj) {
            return new be(bf.a(obj));
        }

        public int b(Object obj) {
            return bf.b(obj);
        }

        public int c(Object obj) {
            return bf.c(obj);
        }

        public int d(Object obj) {
            return bf.d(obj);
        }

        public int e(Object obj) {
            return bf.e(obj);
        }

        public boolean f(Object obj) {
            return bf.f(obj);
        }

        public be a(Object obj, int i, int i2, int i3, int i4) {
            return new be(bf.a(obj, i, i2, i3, i4));
        }
    }

    /* compiled from: WindowInsetsCompat */
    private static class b extends a {
        b() {
        }

        public boolean g(Object obj) {
            return bg.a(obj);
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f1029a = new b();
        } else if (i >= 20) {
            f1029a = new a();
        } else {
            f1029a = new c();
        }
    }

    be(Object obj) {
        this.f1030b = obj;
    }

    public int a() {
        return f1029a.c(this.f1030b);
    }

    public int b() {
        return f1029a.e(this.f1030b);
    }

    public int c() {
        return f1029a.d(this.f1030b);
    }

    public int d() {
        return f1029a.b(this.f1030b);
    }

    public boolean e() {
        return f1029a.f(this.f1030b);
    }

    public boolean f() {
        return f1029a.g(this.f1030b);
    }

    public be g() {
        return f1029a.a(this.f1030b);
    }

    public be a(int i, int i2, int i3, int i4) {
        return f1029a.a(this.f1030b, i, i2, i3, i4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        be beVar = (be) obj;
        if (this.f1030b != null) {
            return this.f1030b.equals(beVar.f1030b);
        }
        if (beVar.f1030b != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.f1030b == null) {
            return 0;
        }
        return this.f1030b.hashCode();
    }

    static be a(Object obj) {
        if (obj == null) {
            return null;
        }
        return new be(obj);
    }

    static Object a(be beVar) {
        if (beVar == null) {
            return null;
        }
        return beVar.f1030b;
    }
}
