package android.support.v4.view.animation;

import android.view.animation.Interpolator;

/* compiled from: LookupTableInterpolator */
abstract class a implements Interpolator {

    /* renamed from: a  reason: collision with root package name */
    private final float[] f1001a;

    /* renamed from: b  reason: collision with root package name */
    private final float f1002b = (1.0f / ((float) (this.f1001a.length - 1)));

    public a(float[] fArr) {
        this.f1001a = fArr;
    }

    public float getInterpolation(float f2) {
        if (f2 >= 1.0f) {
            return 1.0f;
        }
        if (f2 <= 0.0f) {
            return 0.0f;
        }
        int min = Math.min((int) (((float) (this.f1001a.length - 1)) * f2), this.f1001a.length - 2);
        float f3 = (f2 - (((float) min) * this.f1002b)) / this.f1002b;
        return ((this.f1001a[min + 1] - this.f1001a[min]) * f3) + this.f1001a[min];
    }
}
