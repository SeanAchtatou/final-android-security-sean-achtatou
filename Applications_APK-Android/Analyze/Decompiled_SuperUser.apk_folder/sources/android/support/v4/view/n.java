package android.support.v4.view;

import android.os.Build;
import android.view.ViewGroup;

/* compiled from: MarginLayoutParamsCompat */
public final class n {

    /* renamed from: a  reason: collision with root package name */
    static final a f1049a;

    /* compiled from: MarginLayoutParamsCompat */
    interface a {
        int a(ViewGroup.MarginLayoutParams marginLayoutParams);

        int b(ViewGroup.MarginLayoutParams marginLayoutParams);
    }

    /* compiled from: MarginLayoutParamsCompat */
    static class b implements a {
        b() {
        }

        public int a(ViewGroup.MarginLayoutParams marginLayoutParams) {
            return marginLayoutParams.leftMargin;
        }

        public int b(ViewGroup.MarginLayoutParams marginLayoutParams) {
            return marginLayoutParams.rightMargin;
        }
    }

    /* compiled from: MarginLayoutParamsCompat */
    static class c implements a {
        c() {
        }

        public int a(ViewGroup.MarginLayoutParams marginLayoutParams) {
            return o.a(marginLayoutParams);
        }

        public int b(ViewGroup.MarginLayoutParams marginLayoutParams) {
            return o.b(marginLayoutParams);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            f1049a = new c();
        } else {
            f1049a = new b();
        }
    }

    public static int a(ViewGroup.MarginLayoutParams marginLayoutParams) {
        return f1049a.a(marginLayoutParams);
    }

    public static int b(ViewGroup.MarginLayoutParams marginLayoutParams) {
        return f1049a.b(marginLayoutParams);
    }
}
