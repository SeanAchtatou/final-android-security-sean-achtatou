package android.support.v4.view;

import android.annotation.TargetApi;
import android.view.WindowInsets;

@TargetApi(21)
/* compiled from: WindowInsetsCompatApi21 */
class bg {
    public static boolean a(Object obj) {
        return ((WindowInsets) obj).isConsumed();
    }
}
