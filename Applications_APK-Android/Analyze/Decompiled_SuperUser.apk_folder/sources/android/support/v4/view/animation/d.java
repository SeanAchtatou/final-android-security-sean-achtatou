package android.support.v4.view.animation;

import android.annotation.TargetApi;
import android.view.animation.Interpolator;

@TargetApi(9)
/* compiled from: PathInterpolatorCompatBase */
class d {
    public static Interpolator a(float f2, float f3, float f4, float f5) {
        return new e(f2, f3, f4, f5);
    }
}
