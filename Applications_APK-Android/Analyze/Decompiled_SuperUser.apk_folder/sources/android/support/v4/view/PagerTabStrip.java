package android.support.v4.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.c;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import com.lody.virtual.os.VUserInfo;

public class PagerTabStrip extends PagerTitleStrip {

    /* renamed from: g  reason: collision with root package name */
    private int f913g;

    /* renamed from: h  reason: collision with root package name */
    private int f914h;
    private int i;
    private int j;
    private int k;
    private int l;
    private final Paint m;
    private final Rect n;
    private int o;
    private boolean p;
    private boolean q;
    private int r;
    private boolean s;
    private float t;
    private float u;
    private int v;

    public PagerTabStrip(Context context) {
        this(context, null);
    }

    public PagerTabStrip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.m = new Paint();
        this.n = new Rect();
        this.o = VUserInfo.FLAG_MASK_USER_TYPE;
        this.p = false;
        this.q = false;
        this.f913g = this.f922f;
        this.m.setColor(this.f913g);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.f914h = (int) ((3.0f * f2) + 0.5f);
        this.i = (int) ((6.0f * f2) + 0.5f);
        this.j = (int) (64.0f * f2);
        this.l = (int) ((16.0f * f2) + 0.5f);
        this.r = (int) ((1.0f * f2) + 0.5f);
        this.k = (int) ((f2 * 32.0f) + 0.5f);
        this.v = ViewConfiguration.get(context).getScaledTouchSlop();
        setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());
        setTextSpacing(getTextSpacing());
        setWillNotDraw(false);
        this.f918b.setFocusable(true);
        this.f918b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PagerTabStrip.this.f917a.setCurrentItem(PagerTabStrip.this.f917a.getCurrentItem() - 1);
            }
        });
        this.f920d.setFocusable(true);
        this.f920d.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PagerTabStrip.this.f917a.setCurrentItem(PagerTabStrip.this.f917a.getCurrentItem() + 1);
            }
        });
        if (getBackground() == null) {
            this.p = true;
        }
    }

    public void setTabIndicatorColor(int i2) {
        this.f913g = i2;
        this.m.setColor(this.f913g);
        invalidate();
    }

    public void setTabIndicatorColorResource(int i2) {
        setTabIndicatorColor(c.c(getContext(), i2));
    }

    public int getTabIndicatorColor() {
        return this.f913g;
    }

    public void setPadding(int i2, int i3, int i4, int i5) {
        if (i5 < this.i) {
            i5 = this.i;
        }
        super.setPadding(i2, i3, i4, i5);
    }

    public void setTextSpacing(int i2) {
        if (i2 < this.j) {
            i2 = this.j;
        }
        super.setTextSpacing(i2);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (!this.q) {
            this.p = drawable == null;
        }
    }

    public void setBackgroundColor(int i2) {
        super.setBackgroundColor(i2);
        if (!this.q) {
            this.p = (-16777216 & i2) == 0;
        }
    }

    public void setBackgroundResource(int i2) {
        super.setBackgroundResource(i2);
        if (!this.q) {
            this.p = i2 == 0;
        }
    }

    public void setDrawFullUnderline(boolean z) {
        this.p = z;
        this.q = true;
        invalidate();
    }

    public boolean getDrawFullUnderline() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public int getMinHeight() {
        return Math.max(super.getMinHeight(), this.k);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 0 && this.s) {
            return false;
        }
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (action) {
            case 0:
                this.t = x;
                this.u = y;
                this.s = false;
                break;
            case 1:
                if (x >= ((float) (this.f919c.getLeft() - this.l))) {
                    if (x > ((float) (this.f919c.getRight() + this.l))) {
                        this.f917a.setCurrentItem(this.f917a.getCurrentItem() + 1);
                        break;
                    }
                } else {
                    this.f917a.setCurrentItem(this.f917a.getCurrentItem() - 1);
                    break;
                }
                break;
            case 2:
                if (Math.abs(x - this.t) > ((float) this.v) || Math.abs(y - this.u) > ((float) this.v)) {
                    this.s = true;
                    break;
                }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int height = getHeight();
        int left = this.f919c.getLeft() - this.l;
        int right = this.f919c.getRight() + this.l;
        int i2 = height - this.f914h;
        this.m.setColor((this.o << 24) | (this.f913g & 16777215));
        canvas.drawRect((float) left, (float) i2, (float) right, (float) height, this.m);
        if (this.p) {
            this.m.setColor(-16777216 | (this.f913g & 16777215));
            canvas.drawRect((float) getPaddingLeft(), (float) (height - this.r), (float) (getWidth() - getPaddingRight()), (float) height, this.m);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, float f2, boolean z) {
        Rect rect = this.n;
        int height = getHeight();
        int left = this.f919c.getLeft() - this.l;
        int right = this.f919c.getRight() + this.l;
        int i3 = height - this.f914h;
        rect.set(left, i3, right, height);
        super.a(i2, f2, z);
        this.o = (int) (Math.abs(f2 - 0.5f) * 2.0f * 255.0f);
        rect.union(this.f919c.getLeft() - this.l, i3, this.f919c.getRight() + this.l, height);
        invalidate(rect);
    }
}
