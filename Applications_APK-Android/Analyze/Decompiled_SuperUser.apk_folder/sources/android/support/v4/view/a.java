package android.support.v4.view;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.a.e;
import android.support.v4.view.a.n;
import android.support.v4.view.b;
import android.support.v4.view.c;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: AccessibilityDelegateCompat */
public class a {
    private static final Object DEFAULT_DELEGATE = IMPL.a();
    private static final b IMPL;
    final Object mBridge = IMPL.a(this);

    /* compiled from: AccessibilityDelegateCompat */
    interface b {
        n a(Object obj, View view);

        Object a();

        Object a(a aVar);

        void a(Object obj, View view, int i);

        void a(Object obj, View view, e eVar);

        boolean a(Object obj, View view, int i, Bundle bundle);

        boolean a(Object obj, View view, AccessibilityEvent accessibilityEvent);

        boolean a(Object obj, ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent);

        void b(Object obj, View view, AccessibilityEvent accessibilityEvent);

        void c(Object obj, View view, AccessibilityEvent accessibilityEvent);

        void d(Object obj, View view, AccessibilityEvent accessibilityEvent);
    }

    /* compiled from: AccessibilityDelegateCompat */
    static class d implements b {
        d() {
        }

        public Object a() {
            return null;
        }

        public Object a(a aVar) {
            return null;
        }

        public boolean a(Object obj, View view, AccessibilityEvent accessibilityEvent) {
            return false;
        }

        public void b(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        }

        public void a(Object obj, View view, e eVar) {
        }

        public void c(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        }

        public boolean a(Object obj, ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            return true;
        }

        public void a(Object obj, View view, int i) {
        }

        public void d(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        }

        public n a(Object obj, View view) {
            return null;
        }

        public boolean a(Object obj, View view, int i, Bundle bundle) {
            return false;
        }
    }

    /* renamed from: android.support.v4.view.a$a  reason: collision with other inner class name */
    /* compiled from: AccessibilityDelegateCompat */
    static class C0020a extends d {
        C0020a() {
        }

        public Object a() {
            return b.a();
        }

        public Object a(final a aVar) {
            return b.a(new b.a() {
                public boolean a(View view, AccessibilityEvent accessibilityEvent) {
                    return aVar.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
                }

                public void b(View view, AccessibilityEvent accessibilityEvent) {
                    aVar.onInitializeAccessibilityEvent(view, accessibilityEvent);
                }

                public void a(View view, Object obj) {
                    aVar.onInitializeAccessibilityNodeInfo(view, new e(obj));
                }

                public void c(View view, AccessibilityEvent accessibilityEvent) {
                    aVar.onPopulateAccessibilityEvent(view, accessibilityEvent);
                }

                public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
                    return aVar.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
                }

                public void a(View view, int i) {
                    aVar.sendAccessibilityEvent(view, i);
                }

                public void d(View view, AccessibilityEvent accessibilityEvent) {
                    aVar.sendAccessibilityEventUnchecked(view, accessibilityEvent);
                }
            });
        }

        public boolean a(Object obj, View view, AccessibilityEvent accessibilityEvent) {
            return b.a(obj, view, accessibilityEvent);
        }

        public void b(Object obj, View view, AccessibilityEvent accessibilityEvent) {
            b.b(obj, view, accessibilityEvent);
        }

        public void a(Object obj, View view, e eVar) {
            b.a(obj, view, eVar.a());
        }

        public void c(Object obj, View view, AccessibilityEvent accessibilityEvent) {
            b.c(obj, view, accessibilityEvent);
        }

        public boolean a(Object obj, ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            return b.a(obj, viewGroup, view, accessibilityEvent);
        }

        public void a(Object obj, View view, int i) {
            b.a(obj, view, i);
        }

        public void d(Object obj, View view, AccessibilityEvent accessibilityEvent) {
            b.d(obj, view, accessibilityEvent);
        }
    }

    /* compiled from: AccessibilityDelegateCompat */
    static class c extends C0020a {
        c() {
        }

        public Object a(final a aVar) {
            return c.a(new c.a() {
                public boolean a(View view, AccessibilityEvent accessibilityEvent) {
                    return aVar.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
                }

                public void b(View view, AccessibilityEvent accessibilityEvent) {
                    aVar.onInitializeAccessibilityEvent(view, accessibilityEvent);
                }

                public void a(View view, Object obj) {
                    aVar.onInitializeAccessibilityNodeInfo(view, new e(obj));
                }

                public void c(View view, AccessibilityEvent accessibilityEvent) {
                    aVar.onPopulateAccessibilityEvent(view, accessibilityEvent);
                }

                public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
                    return aVar.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
                }

                public void a(View view, int i) {
                    aVar.sendAccessibilityEvent(view, i);
                }

                public void d(View view, AccessibilityEvent accessibilityEvent) {
                    aVar.sendAccessibilityEventUnchecked(view, accessibilityEvent);
                }

                public Object a(View view) {
                    n accessibilityNodeProvider = aVar.getAccessibilityNodeProvider(view);
                    if (accessibilityNodeProvider != null) {
                        return accessibilityNodeProvider.a();
                    }
                    return null;
                }

                public boolean a(View view, int i, Bundle bundle) {
                    return aVar.performAccessibilityAction(view, i, bundle);
                }
            });
        }

        public n a(Object obj, View view) {
            Object a2 = c.a(obj, view);
            if (a2 != null) {
                return new n(a2);
            }
            return null;
        }

        public boolean a(Object obj, View view, int i, Bundle bundle) {
            return c.a(obj, view, i, bundle);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            IMPL = new c();
        } else if (Build.VERSION.SDK_INT >= 14) {
            IMPL = new C0020a();
        } else {
            IMPL = new d();
        }
    }

    /* access modifiers changed from: package-private */
    public Object getBridge() {
        return this.mBridge;
    }

    public void sendAccessibilityEvent(View view, int i) {
        IMPL.a(DEFAULT_DELEGATE, view, i);
    }

    public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
        IMPL.d(DEFAULT_DELEGATE, view, accessibilityEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        return IMPL.a(DEFAULT_DELEGATE, view, accessibilityEvent);
    }

    public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        IMPL.c(DEFAULT_DELEGATE, view, accessibilityEvent);
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        IMPL.b(DEFAULT_DELEGATE, view, accessibilityEvent);
    }

    public void onInitializeAccessibilityNodeInfo(View view, e eVar) {
        IMPL.a(DEFAULT_DELEGATE, view, eVar);
    }

    public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return IMPL.a(DEFAULT_DELEGATE, viewGroup, view, accessibilityEvent);
    }

    public n getAccessibilityNodeProvider(View view) {
        return IMPL.a(DEFAULT_DELEGATE, view);
    }

    public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        return IMPL.a(DEFAULT_DELEGATE, view, i, bundle);
    }
}
