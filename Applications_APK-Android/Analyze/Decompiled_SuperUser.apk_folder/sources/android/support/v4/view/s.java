package android.support.v4.view;

import android.os.Build;
import android.view.MotionEvent;
import com.lody.virtual.os.VUserInfo;

/* compiled from: MotionEventCompat */
public final class s {

    /* renamed from: a  reason: collision with root package name */
    static final d f1051a;

    /* compiled from: MotionEventCompat */
    interface d {
        float a(MotionEvent motionEvent, int i);
    }

    /* compiled from: MotionEventCompat */
    static class a implements d {
        a() {
        }

        public float a(MotionEvent motionEvent, int i) {
            return 0.0f;
        }
    }

    /* compiled from: MotionEventCompat */
    static class b extends a {
        b() {
        }

        public float a(MotionEvent motionEvent, int i) {
            return t.a(motionEvent, i);
        }
    }

    /* compiled from: MotionEventCompat */
    private static class c extends b {
        c() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            f1051a = new c();
        } else if (Build.VERSION.SDK_INT >= 12) {
            f1051a = new b();
        } else {
            f1051a = new a();
        }
    }

    public static int a(MotionEvent motionEvent) {
        return motionEvent.getAction() & VUserInfo.FLAG_MASK_USER_TYPE;
    }

    public static int b(MotionEvent motionEvent) {
        return (motionEvent.getAction() & 65280) >> 8;
    }

    public static float a(MotionEvent motionEvent, int i) {
        return f1051a.a(motionEvent, i);
    }
}
