package android.support.v4.view;

import android.os.Build;
import android.view.KeyEvent;
import com.lody.virtual.helper.utils.FileUtils;

/* compiled from: KeyEventCompat */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    static final c f1044a;

    /* compiled from: KeyEventCompat */
    interface c {
        boolean a(int i, int i2);

        boolean a(KeyEvent keyEvent);

        boolean b(int i);
    }

    /* compiled from: KeyEventCompat */
    static class a implements c {
        a() {
        }

        private static int a(int i, int i2, int i3, int i4, int i5) {
            boolean z = true;
            boolean z2 = (i2 & i3) != 0;
            int i6 = i4 | i5;
            if ((i2 & i6) == 0) {
                z = false;
            }
            if (z2) {
                if (!z) {
                    return i & (i6 ^ -1);
                }
                throw new IllegalArgumentException("bad arguments");
            } else if (z) {
                return i & (i3 ^ -1);
            } else {
                return i;
            }
        }

        public int a(int i) {
            int i2;
            if ((i & 192) != 0) {
                i2 = i | 1;
            } else {
                i2 = i;
            }
            if ((i2 & 48) != 0) {
                i2 |= 2;
            }
            return i2 & 247;
        }

        public boolean a(int i, int i2) {
            if (a(a(a(i) & 247, i2, 1, 64, FileUtils.FileMode.MODE_IWUSR), i2, 2, 16, 32) == i2) {
                return true;
            }
            return false;
        }

        public boolean b(int i) {
            return (a(i) & 247) == 0;
        }

        public boolean a(KeyEvent keyEvent) {
            return false;
        }
    }

    /* compiled from: KeyEventCompat */
    static class b extends a {
        b() {
        }

        public int a(int i) {
            return h.a(i);
        }

        public boolean a(int i, int i2) {
            return h.a(i, i2);
        }

        public boolean b(int i) {
            return h.b(i);
        }

        public boolean a(KeyEvent keyEvent) {
            return h.a(keyEvent);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f1044a = new b();
        } else {
            f1044a = new a();
        }
    }

    public static boolean a(KeyEvent keyEvent, int i) {
        return f1044a.a(keyEvent.getMetaState(), i);
    }

    public static boolean a(KeyEvent keyEvent) {
        return f1044a.b(keyEvent.getMetaState());
    }

    public static boolean b(KeyEvent keyEvent) {
        return f1044a.a(keyEvent);
    }
}
