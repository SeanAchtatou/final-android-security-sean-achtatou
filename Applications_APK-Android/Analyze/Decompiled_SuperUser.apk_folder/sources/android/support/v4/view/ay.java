package android.support.v4.view;

import android.graphics.Paint;
import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

/* compiled from: ViewPropertyAnimatorCompat */
public final class ay {

    /* renamed from: d  reason: collision with root package name */
    static final g f1010d;

    /* renamed from: a  reason: collision with root package name */
    Runnable f1011a = null;

    /* renamed from: b  reason: collision with root package name */
    Runnable f1012b = null;

    /* renamed from: c  reason: collision with root package name */
    int f1013c = -1;

    /* renamed from: e  reason: collision with root package name */
    private WeakReference<View> f1014e;

    /* compiled from: ViewPropertyAnimatorCompat */
    interface g {
        long a(ay ayVar, View view);

        void a(ay ayVar, View view, float f2);

        void a(ay ayVar, View view, long j);

        void a(ay ayVar, View view, bc bcVar);

        void a(ay ayVar, View view, bd bdVar);

        void a(ay ayVar, View view, Interpolator interpolator);

        void b(ay ayVar, View view);

        void b(ay ayVar, View view, float f2);

        void b(ay ayVar, View view, long j);

        void c(ay ayVar, View view);

        void c(ay ayVar, View view, float f2);
    }

    ay(View view) {
        this.f1014e = new WeakReference<>(view);
    }

    /* compiled from: ViewPropertyAnimatorCompat */
    static class a implements g {

        /* renamed from: a  reason: collision with root package name */
        WeakHashMap<View, Runnable> f1015a = null;

        a() {
        }

        public void a(ay ayVar, View view, long j) {
        }

        public void a(ay ayVar, View view, float f2) {
            e(ayVar, view);
        }

        public void b(ay ayVar, View view, float f2) {
            e(ayVar, view);
        }

        public void c(ay ayVar, View view, float f2) {
            e(ayVar, view);
        }

        public long a(ay ayVar, View view) {
            return 0;
        }

        public void a(ay ayVar, View view, Interpolator interpolator) {
        }

        public void b(ay ayVar, View view, long j) {
        }

        public void b(ay ayVar, View view) {
            e(ayVar, view);
        }

        public void c(ay ayVar, View view) {
            a(view);
            d(ayVar, view);
        }

        public void a(ay ayVar, View view, bc bcVar) {
            view.setTag(2113929216, bcVar);
        }

        public void a(ay ayVar, View view, bd bdVar) {
        }

        /* access modifiers changed from: package-private */
        public void d(ay ayVar, View view) {
            bc bcVar;
            Object tag = view.getTag(2113929216);
            if (tag instanceof bc) {
                bcVar = (bc) tag;
            } else {
                bcVar = null;
            }
            Runnable runnable = ayVar.f1011a;
            Runnable runnable2 = ayVar.f1012b;
            ayVar.f1011a = null;
            ayVar.f1012b = null;
            if (runnable != null) {
                runnable.run();
            }
            if (bcVar != null) {
                bcVar.onAnimationStart(view);
                bcVar.onAnimationEnd(view);
            }
            if (runnable2 != null) {
                runnable2.run();
            }
            if (this.f1015a != null) {
                this.f1015a.remove(view);
            }
        }

        /* renamed from: android.support.v4.view.ay$a$a  reason: collision with other inner class name */
        /* compiled from: ViewPropertyAnimatorCompat */
        class C0024a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            WeakReference<View> f1016a;

            /* renamed from: b  reason: collision with root package name */
            ay f1017b;

            C0024a(ay ayVar, View view) {
                this.f1016a = new WeakReference<>(view);
                this.f1017b = ayVar;
            }

            public void run() {
                View view = this.f1016a.get();
                if (view != null) {
                    a.this.d(this.f1017b, view);
                }
            }
        }

        private void a(View view) {
            Runnable runnable;
            if (this.f1015a != null && (runnable = this.f1015a.get(view)) != null) {
                view.removeCallbacks(runnable);
            }
        }

        private void e(ay ayVar, View view) {
            Runnable runnable = null;
            if (this.f1015a != null) {
                runnable = this.f1015a.get(view);
            }
            if (runnable == null) {
                runnable = new C0024a(ayVar, view);
                if (this.f1015a == null) {
                    this.f1015a = new WeakHashMap<>();
                }
                this.f1015a.put(view, runnable);
            }
            view.removeCallbacks(runnable);
            view.post(runnable);
        }
    }

    /* compiled from: ViewPropertyAnimatorCompat */
    static class b extends a {

        /* renamed from: b  reason: collision with root package name */
        WeakHashMap<View, Integer> f1019b = null;

        b() {
        }

        public void a(ay ayVar, View view, long j) {
            az.a(view, j);
        }

        public void a(ay ayVar, View view, float f2) {
            az.a(view, f2);
        }

        public void b(ay ayVar, View view, float f2) {
            az.b(view, f2);
        }

        public void c(ay ayVar, View view, float f2) {
            az.c(view, f2);
        }

        public long a(ay ayVar, View view) {
            return az.a(view);
        }

        public void a(ay ayVar, View view, Interpolator interpolator) {
            az.a(view, interpolator);
        }

        public void b(ay ayVar, View view, long j) {
            az.b(view, j);
        }

        public void b(ay ayVar, View view) {
            az.b(view);
        }

        public void c(ay ayVar, View view) {
            az.c(view);
        }

        public void a(ay ayVar, View view, bc bcVar) {
            view.setTag(2113929216, bcVar);
            az.a(view, new a(ayVar));
        }

        /* compiled from: ViewPropertyAnimatorCompat */
        static class a implements bc {

            /* renamed from: a  reason: collision with root package name */
            ay f1020a;

            /* renamed from: b  reason: collision with root package name */
            boolean f1021b;

            a(ay ayVar) {
                this.f1020a = ayVar;
            }

            public void onAnimationStart(View view) {
                bc bcVar;
                this.f1021b = false;
                if (this.f1020a.f1013c >= 0) {
                    ag.a(view, 2, (Paint) null);
                }
                if (this.f1020a.f1011a != null) {
                    Runnable runnable = this.f1020a.f1011a;
                    this.f1020a.f1011a = null;
                    runnable.run();
                }
                Object tag = view.getTag(2113929216);
                if (tag instanceof bc) {
                    bcVar = (bc) tag;
                } else {
                    bcVar = null;
                }
                if (bcVar != null) {
                    bcVar.onAnimationStart(view);
                }
            }

            public void onAnimationEnd(View view) {
                bc bcVar;
                if (this.f1020a.f1013c >= 0) {
                    ag.a(view, this.f1020a.f1013c, (Paint) null);
                    this.f1020a.f1013c = -1;
                }
                if (Build.VERSION.SDK_INT >= 16 || !this.f1021b) {
                    if (this.f1020a.f1012b != null) {
                        Runnable runnable = this.f1020a.f1012b;
                        this.f1020a.f1012b = null;
                        runnable.run();
                    }
                    Object tag = view.getTag(2113929216);
                    if (tag instanceof bc) {
                        bcVar = (bc) tag;
                    } else {
                        bcVar = null;
                    }
                    if (bcVar != null) {
                        bcVar.onAnimationEnd(view);
                    }
                    this.f1021b = true;
                }
            }

            public void onAnimationCancel(View view) {
                bc bcVar;
                Object tag = view.getTag(2113929216);
                if (tag instanceof bc) {
                    bcVar = (bc) tag;
                } else {
                    bcVar = null;
                }
                if (bcVar != null) {
                    bcVar.onAnimationCancel(view);
                }
            }
        }
    }

    /* compiled from: ViewPropertyAnimatorCompat */
    static class d extends b {
        d() {
        }

        public void a(ay ayVar, View view, bc bcVar) {
            ba.a(view, bcVar);
        }
    }

    /* compiled from: ViewPropertyAnimatorCompat */
    static class c extends d {
        c() {
        }
    }

    /* compiled from: ViewPropertyAnimatorCompat */
    static class e extends c {
        e() {
        }

        public void a(ay ayVar, View view, bd bdVar) {
            bb.a(view, bdVar);
        }
    }

    /* compiled from: ViewPropertyAnimatorCompat */
    static class f extends e {
        f() {
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f1010d = new f();
        } else if (i >= 19) {
            f1010d = new e();
        } else if (i >= 18) {
            f1010d = new c();
        } else if (i >= 16) {
            f1010d = new d();
        } else if (i >= 14) {
            f1010d = new b();
        } else {
            f1010d = new a();
        }
    }

    public ay a(long j) {
        View view = this.f1014e.get();
        if (view != null) {
            f1010d.a(this, view, j);
        }
        return this;
    }

    public ay a(float f2) {
        View view = this.f1014e.get();
        if (view != null) {
            f1010d.a(this, view, f2);
        }
        return this;
    }

    public ay b(float f2) {
        View view = this.f1014e.get();
        if (view != null) {
            f1010d.b(this, view, f2);
        }
        return this;
    }

    public ay c(float f2) {
        View view = this.f1014e.get();
        if (view != null) {
            f1010d.c(this, view, f2);
        }
        return this;
    }

    public long a() {
        View view = this.f1014e.get();
        if (view != null) {
            return f1010d.a(this, view);
        }
        return 0;
    }

    public ay a(Interpolator interpolator) {
        View view = this.f1014e.get();
        if (view != null) {
            f1010d.a(this, view, interpolator);
        }
        return this;
    }

    public ay b(long j) {
        View view = this.f1014e.get();
        if (view != null) {
            f1010d.b(this, view, j);
        }
        return this;
    }

    public void b() {
        View view = this.f1014e.get();
        if (view != null) {
            f1010d.b(this, view);
        }
    }

    public void c() {
        View view = this.f1014e.get();
        if (view != null) {
            f1010d.c(this, view);
        }
    }

    public ay a(bc bcVar) {
        View view = this.f1014e.get();
        if (view != null) {
            f1010d.a(this, view, bcVar);
        }
        return this;
    }

    public ay a(bd bdVar) {
        View view = this.f1014e.get();
        if (view != null) {
            f1010d.a(this, view, bdVar);
        }
        return this;
    }
}
