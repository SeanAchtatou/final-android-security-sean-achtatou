package android.support.v4.view;

import android.content.Context;

/* compiled from: PointerIconCompat */
public final class aa {

    /* renamed from: a  reason: collision with root package name */
    static final c f984a;

    /* renamed from: b  reason: collision with root package name */
    private Object f985b;

    /* compiled from: PointerIconCompat */
    interface c {
        Object a(Context context, int i);
    }

    private aa(Object obj) {
        this.f985b = obj;
    }

    public Object a() {
        return this.f985b;
    }

    /* compiled from: PointerIconCompat */
    static class b implements c {
        b() {
        }

        public Object a(Context context, int i) {
            return null;
        }
    }

    /* compiled from: PointerIconCompat */
    static class a extends b {
        a() {
        }

        public Object a(Context context, int i) {
            return ab.a(context, i);
        }
    }

    static {
        if (android.support.v4.os.c.a()) {
            f984a = new a();
        } else {
            f984a = new b();
        }
    }

    public static aa a(Context context, int i) {
        return new aa(f984a.a(context, i));
    }
}
