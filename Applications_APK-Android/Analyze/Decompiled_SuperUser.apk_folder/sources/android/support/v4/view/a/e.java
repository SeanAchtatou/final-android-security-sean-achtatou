package android.support.v4.view.a;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import com.lody.virtual.helper.utils.FileUtils;

/* compiled from: AccessibilityNodeInfoCompat */
public class e {

    /* renamed from: a  reason: collision with root package name */
    static final g f961a;

    /* renamed from: b  reason: collision with root package name */
    public int f962b = -1;

    /* renamed from: c  reason: collision with root package name */
    private final Object f963c;

    /* compiled from: AccessibilityNodeInfoCompat */
    interface g {
        Object a();

        Object a(int i, int i2, int i3, int i4, boolean z, boolean z2);

        Object a(int i, int i2, boolean z, int i3);

        Object a(int i, CharSequence charSequence);

        Object a(View view);

        Object a(Object obj);

        void a(Object obj, int i);

        void a(Object obj, Rect rect);

        void a(Object obj, View view);

        void a(Object obj, View view, int i);

        void a(Object obj, CharSequence charSequence);

        void a(Object obj, boolean z);

        boolean a(Object obj, Object obj2);

        int b(Object obj);

        Object b();

        void b(Object obj, int i);

        void b(Object obj, Rect rect);

        void b(Object obj, View view);

        void b(Object obj, View view, int i);

        void b(Object obj, CharSequence charSequence);

        void b(Object obj, Object obj2);

        void b(Object obj, boolean z);

        int c(Object obj);

        Object c();

        void c(Object obj, Rect rect);

        void c(Object obj, View view);

        void c(Object obj, View view, int i);

        void c(Object obj, CharSequence charSequence);

        void c(Object obj, Object obj2);

        void c(Object obj, boolean z);

        CharSequence d(Object obj);

        Object d();

        void d(Object obj, Rect rect);

        void d(Object obj, View view);

        void d(Object obj, CharSequence charSequence);

        void d(Object obj, boolean z);

        CharSequence e(Object obj);

        Object e();

        void e(Object obj, CharSequence charSequence);

        void e(Object obj, boolean z);

        CharSequence f(Object obj);

        Object f();

        void f(Object obj, boolean z);

        CharSequence g(Object obj);

        Object g();

        void g(Object obj, boolean z);

        Object h();

        void h(Object obj, boolean z);

        boolean h(Object obj);

        Object i();

        void i(Object obj, boolean z);

        boolean i(Object obj);

        void j(Object obj, boolean z);

        boolean j(Object obj);

        void k(Object obj, boolean z);

        boolean k(Object obj);

        void l(Object obj, boolean z);

        boolean l(Object obj);

        void m(Object obj, boolean z);

        boolean m(Object obj);

        boolean n(Object obj);

        boolean o(Object obj);

        boolean p(Object obj);

        boolean q(Object obj);

        void r(Object obj);

        int s(Object obj);

        boolean t(Object obj);

        boolean u(Object obj);

        String v(Object obj);
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    public static class a {
        public static final a A = new a(e.f961a.d());
        public static final a B = new a(e.f961a.f());
        public static final a C = new a(e.f961a.g());
        public static final a D = new a(e.f961a.h());

        /* renamed from: a  reason: collision with root package name */
        public static final a f964a = new a(1, null);

        /* renamed from: b  reason: collision with root package name */
        public static final a f965b = new a(2, null);

        /* renamed from: c  reason: collision with root package name */
        public static final a f966c = new a(4, null);

        /* renamed from: d  reason: collision with root package name */
        public static final a f967d = new a(8, null);

        /* renamed from: e  reason: collision with root package name */
        public static final a f968e = new a(16, null);

        /* renamed from: f  reason: collision with root package name */
        public static final a f969f = new a(32, null);

        /* renamed from: g  reason: collision with root package name */
        public static final a f970g = new a(64, null);

        /* renamed from: h  reason: collision with root package name */
        public static final a f971h = new a(FileUtils.FileMode.MODE_IWUSR, null);
        public static final a i = new a(FileUtils.FileMode.MODE_IRUSR, null);
        public static final a j = new a(FileUtils.FileMode.MODE_ISVTX, null);
        public static final a k = new a(FileUtils.FileMode.MODE_ISGID, null);
        public static final a l = new a(FileUtils.FileMode.MODE_ISUID, null);
        public static final a m = new a(CodedOutputStream.DEFAULT_BUFFER_SIZE, null);
        public static final a n = new a(8192, null);
        public static final a o = new a(16384, null);
        public static final a p = new a(32768, null);
        public static final a q = new a(65536, null);
        public static final a r = new a(131072, null);
        public static final a s = new a(262144, null);
        public static final a t = new a(524288, null);
        public static final a u = new a(1048576, null);
        public static final a v = new a(2097152, null);
        public static final a w = new a(e.f961a.b());
        public static final a x = new a(e.f961a.a());
        public static final a y = new a(e.f961a.c());
        public static final a z = new a(e.f961a.e());
        final Object E;

        public a(int i2, CharSequence charSequence) {
            this(e.f961a.a(i2, charSequence));
        }

        a(Object obj) {
            this.E = obj;
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    public static class m {

        /* renamed from: a  reason: collision with root package name */
        final Object f972a;

        public static m a(int i, int i2, boolean z, int i3) {
            return new m(e.f961a.a(i, i2, z, i3));
        }

        m(Object obj) {
            this.f972a = obj;
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    public static class n {

        /* renamed from: a  reason: collision with root package name */
        final Object f973a;

        public static n a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            return new n(e.f961a.a(i, i2, i3, i4, z, z2));
        }

        n(Object obj) {
            this.f973a = obj;
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    static class l implements g {
        l() {
        }

        public Object a(int i, CharSequence charSequence) {
            return null;
        }

        public Object i() {
            return null;
        }

        public Object a(View view) {
            return null;
        }

        public Object a(Object obj) {
            return null;
        }

        public void a(Object obj, int i) {
        }

        public boolean a(Object obj, Object obj2) {
            return false;
        }

        public void a(Object obj, View view) {
        }

        public void b(Object obj, View view, int i) {
        }

        public int b(Object obj) {
            return 0;
        }

        public void a(Object obj, Rect rect) {
        }

        public void b(Object obj, Rect rect) {
        }

        public int c(Object obj) {
            return 0;
        }

        public CharSequence d(Object obj) {
            return null;
        }

        public CharSequence e(Object obj) {
            return null;
        }

        public CharSequence f(Object obj) {
            return null;
        }

        public CharSequence g(Object obj) {
            return null;
        }

        public boolean h(Object obj) {
            return false;
        }

        public boolean i(Object obj) {
            return false;
        }

        public boolean j(Object obj) {
            return false;
        }

        public boolean k(Object obj) {
            return false;
        }

        public boolean l(Object obj) {
            return false;
        }

        public boolean m(Object obj) {
            return false;
        }

        public boolean t(Object obj) {
            return false;
        }

        public boolean u(Object obj) {
            return false;
        }

        public boolean n(Object obj) {
            return false;
        }

        public boolean o(Object obj) {
            return false;
        }

        public boolean p(Object obj) {
            return false;
        }

        public boolean q(Object obj) {
            return false;
        }

        public void b(Object obj, int i) {
        }

        public int s(Object obj) {
            return 0;
        }

        public void c(Object obj, Rect rect) {
        }

        public void d(Object obj, Rect rect) {
        }

        public void a(Object obj, boolean z) {
        }

        public void b(Object obj, boolean z) {
        }

        public void b(Object obj, CharSequence charSequence) {
        }

        public void c(Object obj, boolean z) {
        }

        public void c(Object obj, CharSequence charSequence) {
        }

        public void d(Object obj, boolean z) {
        }

        public void e(Object obj, boolean z) {
        }

        public void f(Object obj, boolean z) {
        }

        public void j(Object obj, boolean z) {
        }

        public void k(Object obj, boolean z) {
        }

        public void g(Object obj, boolean z) {
        }

        public void d(Object obj, CharSequence charSequence) {
        }

        public void b(Object obj, View view) {
        }

        public void h(Object obj, boolean z) {
        }

        public void i(Object obj, boolean z) {
        }

        public void c(Object obj, View view) {
        }

        public void a(Object obj, View view, int i) {
        }

        public void e(Object obj, CharSequence charSequence) {
        }

        public void r(Object obj) {
        }

        public void c(Object obj, View view, int i) {
        }

        public String v(Object obj) {
            return null;
        }

        public void b(Object obj, Object obj2) {
        }

        public void c(Object obj, Object obj2) {
        }

        public Object a(int i, int i2, boolean z, int i3) {
            return null;
        }

        public Object a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            return null;
        }

        public void l(Object obj, boolean z) {
        }

        public void a(Object obj, CharSequence charSequence) {
        }

        public void d(Object obj, View view) {
        }

        public void m(Object obj, boolean z) {
        }

        public Object a() {
            return null;
        }

        public Object h() {
            return null;
        }

        public Object b() {
            return null;
        }

        public Object c() {
            return null;
        }

        public Object d() {
            return null;
        }

        public Object e() {
            return null;
        }

        public Object f() {
            return null;
        }

        public Object g() {
            return null;
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    static class f extends l {
        f() {
        }

        public Object i() {
            return i.a();
        }

        public Object a(View view) {
            return i.a(view);
        }

        public Object a(Object obj) {
            return i.a(obj);
        }

        public void a(Object obj, int i) {
            i.a(obj, i);
        }

        public void a(Object obj, View view) {
            i.a(obj, view);
        }

        public int b(Object obj) {
            return i.b(obj);
        }

        public void a(Object obj, Rect rect) {
            i.a(obj, rect);
        }

        public void b(Object obj, Rect rect) {
            i.b(obj, rect);
        }

        public int c(Object obj) {
            return i.c(obj);
        }

        public CharSequence d(Object obj) {
            return i.d(obj);
        }

        public CharSequence e(Object obj) {
            return i.e(obj);
        }

        public CharSequence f(Object obj) {
            return i.f(obj);
        }

        public CharSequence g(Object obj) {
            return i.g(obj);
        }

        public boolean h(Object obj) {
            return i.h(obj);
        }

        public boolean i(Object obj) {
            return i.i(obj);
        }

        public boolean j(Object obj) {
            return i.j(obj);
        }

        public boolean k(Object obj) {
            return i.k(obj);
        }

        public boolean l(Object obj) {
            return i.l(obj);
        }

        public boolean m(Object obj) {
            return i.m(obj);
        }

        public boolean n(Object obj) {
            return i.n(obj);
        }

        public boolean o(Object obj) {
            return i.o(obj);
        }

        public boolean p(Object obj) {
            return i.p(obj);
        }

        public boolean q(Object obj) {
            return i.q(obj);
        }

        public void c(Object obj, Rect rect) {
            i.c(obj, rect);
        }

        public void d(Object obj, Rect rect) {
            i.d(obj, rect);
        }

        public void a(Object obj, boolean z) {
            i.a(obj, z);
        }

        public void b(Object obj, boolean z) {
            i.b(obj, z);
        }

        public void b(Object obj, CharSequence charSequence) {
            i.a(obj, charSequence);
        }

        public void c(Object obj, boolean z) {
            i.c(obj, z);
        }

        public void c(Object obj, CharSequence charSequence) {
            i.b(obj, charSequence);
        }

        public void d(Object obj, boolean z) {
            i.d(obj, z);
        }

        public void e(Object obj, boolean z) {
            i.e(obj, z);
        }

        public void f(Object obj, boolean z) {
            i.f(obj, z);
        }

        public void g(Object obj, boolean z) {
            i.g(obj, z);
        }

        public void d(Object obj, CharSequence charSequence) {
            i.c(obj, charSequence);
        }

        public void b(Object obj, View view) {
            i.b(obj, view);
        }

        public void h(Object obj, boolean z) {
            i.h(obj, z);
        }

        public void i(Object obj, boolean z) {
            i.i(obj, z);
        }

        public void c(Object obj, View view) {
            i.c(obj, view);
        }

        public void e(Object obj, CharSequence charSequence) {
            i.d(obj, charSequence);
        }

        public void r(Object obj) {
            i.r(obj);
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    static class h extends f {
        h() {
        }

        public void b(Object obj, View view, int i) {
            j.a(obj, view, i);
        }

        public void a(Object obj, View view, int i) {
            j.b(obj, view, i);
        }

        public boolean t(Object obj) {
            return j.a(obj);
        }

        public void j(Object obj, boolean z) {
            j.a(obj, z);
        }

        public boolean u(Object obj) {
            return j.c(obj);
        }

        public void k(Object obj, boolean z) {
            j.b(obj, z);
        }

        public void b(Object obj, int i) {
            j.a(obj, i);
        }

        public int s(Object obj) {
            return j.b(obj);
        }

        public void c(Object obj, View view, int i) {
            j.c(obj, view, i);
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    static class i extends h {
        i() {
        }

        public void d(Object obj, View view) {
            k.a(obj, view);
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    static class j extends i {
        j() {
        }

        public String v(Object obj) {
            return l.a(obj);
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    static class k extends j {
        k() {
        }

        public void b(Object obj, Object obj2) {
            m.a(obj, obj2);
        }

        public Object a(int i, int i2, boolean z, int i3) {
            return m.a(i, i2, z, i3);
        }

        public Object a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            return m.a(i, i2, i3, i4, z);
        }

        public void c(Object obj, Object obj2) {
            m.b(obj, obj2);
        }

        public void l(Object obj, boolean z) {
            m.a(obj, z);
        }

        public void m(Object obj, boolean z) {
            m.b(obj, z);
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    static class b extends k {
        b() {
        }

        public Object a(int i, CharSequence charSequence) {
            return f.a(i, charSequence);
        }

        public Object a(int i, int i2, boolean z, int i3) {
            return f.a(i, i2, z, i3);
        }

        public boolean a(Object obj, Object obj2) {
            return f.a(obj, obj2);
        }

        public Object a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            return f.a(i, i2, i3, i4, z, z2);
        }

        public void a(Object obj, CharSequence charSequence) {
            f.a(obj, charSequence);
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    static class c extends b {
        c() {
        }
    }

    /* compiled from: AccessibilityNodeInfoCompat */
    static class d extends c {
        d() {
        }

        public Object a() {
            return g.a();
        }

        public Object b() {
            return g.b();
        }

        public Object c() {
            return g.c();
        }

        public Object d() {
            return g.d();
        }

        public Object e() {
            return g.e();
        }

        public Object f() {
            return g.f();
        }

        public Object g() {
            return g.g();
        }
    }

    /* renamed from: android.support.v4.view.a.e$e  reason: collision with other inner class name */
    /* compiled from: AccessibilityNodeInfoCompat */
    static class C0023e extends d {
        C0023e() {
        }

        public Object h() {
            return h.a();
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 24) {
            f961a = new C0023e();
        } else if (Build.VERSION.SDK_INT >= 23) {
            f961a = new d();
        } else if (Build.VERSION.SDK_INT >= 22) {
            f961a = new c();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f961a = new b();
        } else if (Build.VERSION.SDK_INT >= 19) {
            f961a = new k();
        } else if (Build.VERSION.SDK_INT >= 18) {
            f961a = new j();
        } else if (Build.VERSION.SDK_INT >= 17) {
            f961a = new i();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f961a = new h();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f961a = new f();
        } else {
            f961a = new l();
        }
    }

    static e a(Object obj) {
        if (obj != null) {
            return new e(obj);
        }
        return null;
    }

    public e(Object obj) {
        this.f963c = obj;
    }

    public Object a() {
        return this.f963c;
    }

    public static e a(View view) {
        return a(f961a.a(view));
    }

    public static e b() {
        return a(f961a.i());
    }

    public static e a(e eVar) {
        return a(f961a.a(eVar.f963c));
    }

    public void b(View view) {
        f961a.c(this.f963c, view);
    }

    public void a(View view, int i2) {
        f961a.a(this.f963c, view, i2);
    }

    public int c() {
        return f961a.c(this.f963c);
    }

    public void c(View view) {
        f961a.a(this.f963c, view);
    }

    public void b(View view, int i2) {
        f961a.b(this.f963c, view, i2);
    }

    public int d() {
        return f961a.b(this.f963c);
    }

    public void a(int i2) {
        f961a.a(this.f963c, i2);
    }

    public boolean a(a aVar) {
        return f961a.a(this.f963c, aVar.E);
    }

    public void b(int i2) {
        f961a.b(this.f963c, i2);
    }

    public int e() {
        return f961a.s(this.f963c);
    }

    public void d(View view) {
        f961a.b(this.f963c, view);
    }

    public void c(View view, int i2) {
        this.f962b = i2;
        f961a.c(this.f963c, view, i2);
    }

    public void a(Rect rect) {
        f961a.a(this.f963c, rect);
    }

    public void b(Rect rect) {
        f961a.c(this.f963c, rect);
    }

    public void c(Rect rect) {
        f961a.b(this.f963c, rect);
    }

    public void d(Rect rect) {
        f961a.d(this.f963c, rect);
    }

    public boolean f() {
        return f961a.h(this.f963c);
    }

    public void a(boolean z) {
        f961a.a(this.f963c, z);
    }

    public boolean g() {
        return f961a.i(this.f963c);
    }

    public void b(boolean z) {
        f961a.b(this.f963c, z);
    }

    public boolean h() {
        return f961a.l(this.f963c);
    }

    public void c(boolean z) {
        f961a.e(this.f963c, z);
    }

    public boolean i() {
        return f961a.m(this.f963c);
    }

    public void d(boolean z) {
        f961a.f(this.f963c, z);
    }

    public boolean j() {
        return f961a.t(this.f963c);
    }

    public void e(boolean z) {
        f961a.j(this.f963c, z);
    }

    public boolean k() {
        return f961a.u(this.f963c);
    }

    public void f(boolean z) {
        f961a.k(this.f963c, z);
    }

    public boolean l() {
        return f961a.q(this.f963c);
    }

    public void g(boolean z) {
        f961a.i(this.f963c, z);
    }

    public boolean m() {
        return f961a.j(this.f963c);
    }

    public void h(boolean z) {
        f961a.c(this.f963c, z);
    }

    public boolean n() {
        return f961a.n(this.f963c);
    }

    public void i(boolean z) {
        f961a.g(this.f963c, z);
    }

    public boolean o() {
        return f961a.k(this.f963c);
    }

    public void j(boolean z) {
        f961a.d(this.f963c, z);
    }

    public boolean p() {
        return f961a.o(this.f963c);
    }

    public boolean q() {
        return f961a.p(this.f963c);
    }

    public void k(boolean z) {
        f961a.h(this.f963c, z);
    }

    public CharSequence r() {
        return f961a.f(this.f963c);
    }

    public void a(CharSequence charSequence) {
        f961a.d(this.f963c, charSequence);
    }

    public CharSequence s() {
        return f961a.d(this.f963c);
    }

    public void b(CharSequence charSequence) {
        f961a.b(this.f963c, charSequence);
    }

    public CharSequence t() {
        return f961a.g(this.f963c);
    }

    public void c(CharSequence charSequence) {
        f961a.e(this.f963c, charSequence);
    }

    public CharSequence u() {
        return f961a.e(this.f963c);
    }

    public void d(CharSequence charSequence) {
        f961a.c(this.f963c, charSequence);
    }

    public void v() {
        f961a.r(this.f963c);
    }

    public String w() {
        return f961a.v(this.f963c);
    }

    public void b(Object obj) {
        f961a.b(this.f963c, ((m) obj).f972a);
    }

    public void c(Object obj) {
        f961a.c(this.f963c, ((n) obj).f973a);
    }

    public void l(boolean z) {
        f961a.l(this.f963c, z);
    }

    public void e(CharSequence charSequence) {
        f961a.a(this.f963c, charSequence);
    }

    public void e(View view) {
        f961a.d(this.f963c, view);
    }

    public void m(boolean z) {
        f961a.m(this.f963c, z);
    }

    public int hashCode() {
        if (this.f963c == null) {
            return 0;
        }
        return this.f963c.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        if (this.f963c == null) {
            if (eVar.f963c != null) {
                return false;
            }
            return true;
        } else if (!this.f963c.equals(eVar.f963c)) {
            return false;
        } else {
            return true;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        a(rect);
        sb.append("; boundsInParent: " + rect);
        c(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ").append(r());
        sb.append("; className: ").append(s());
        sb.append("; text: ").append(t());
        sb.append("; contentDescription: ").append(u());
        sb.append("; viewId: ").append(w());
        sb.append("; checkable: ").append(f());
        sb.append("; checked: ").append(g());
        sb.append("; focusable: ").append(h());
        sb.append("; focused: ").append(i());
        sb.append("; selected: ").append(l());
        sb.append("; clickable: ").append(m());
        sb.append("; longClickable: ").append(n());
        sb.append("; enabled: ").append(o());
        sb.append("; password: ").append(p());
        sb.append("; scrollable: " + q());
        sb.append("; [");
        int d2 = d();
        while (d2 != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(d2);
            d2 &= numberOfTrailingZeros ^ -1;
            sb.append(c(numberOfTrailingZeros));
            if (d2 != 0) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    private static String c(int i2) {
        switch (i2) {
            case 1:
                return "ACTION_FOCUS";
            case 2:
                return "ACTION_CLEAR_FOCUS";
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case FileUtils.FileMode.MODE_IWUSR /*128*/:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case FileUtils.FileMode.MODE_IRUSR /*256*/:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case FileUtils.FileMode.MODE_ISVTX /*512*/:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case FileUtils.FileMode.MODE_ISGID /*1024*/:
                return "ACTION_NEXT_HTML_ELEMENT";
            case FileUtils.FileMode.MODE_ISUID /*2048*/:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case CodedOutputStream.DEFAULT_BUFFER_SIZE /*4096*/:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case 16384:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }
}
