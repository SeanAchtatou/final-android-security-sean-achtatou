package android.support.v4.view;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.view.View;

@TargetApi(19)
/* compiled from: ViewPropertyAnimatorCompatKK */
class bb {
    public static void a(final View view, final bd bdVar) {
        AnonymousClass1 r0 = null;
        if (bdVar != null) {
            r0 = new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    bdVar.a(view);
                }
            };
        }
        view.animate().setUpdateListener(r0);
    }
}
