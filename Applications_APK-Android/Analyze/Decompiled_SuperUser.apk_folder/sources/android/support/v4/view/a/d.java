package android.support.v4.view.a;

import android.annotation.TargetApi;
import android.view.accessibility.AccessibilityManager;

@TargetApi(14)
/* compiled from: AccessibilityManagerCompatIcs */
class d {
    public static boolean a(AccessibilityManager accessibilityManager) {
        return accessibilityManager.isTouchExplorationEnabled();
    }
}
