package android.support.v4.view;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.view.ViewParent;
import android.view.WindowInsets;

@TargetApi(21)
/* compiled from: ViewCompatLollipop */
class ap {

    /* renamed from: a  reason: collision with root package name */
    private static ThreadLocal<Rect> f1005a;

    /* compiled from: ViewCompatLollipop */
    public interface a {
        Object a(View view, Object obj);
    }

    public static String a(View view) {
        return view.getTransitionName();
    }

    public static void b(View view) {
        view.requestApplyInsets();
    }

    public static void a(View view, float f2) {
        view.setElevation(f2);
    }

    public static float c(View view) {
        return view.getElevation();
    }

    public static float d(View view) {
        return view.getTranslationZ();
    }

    public static void a(View view, final a aVar) {
        if (aVar == null) {
            view.setOnApplyWindowInsetsListener(null);
        } else {
            view.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
                    return (WindowInsets) aVar.a(view, windowInsets);
                }
            });
        }
    }

    static ColorStateList e(View view) {
        return view.getBackgroundTintList();
    }

    static void a(View view, ColorStateList colorStateList) {
        view.setBackgroundTintList(colorStateList);
        if (Build.VERSION.SDK_INT == 21) {
            Drawable background = view.getBackground();
            boolean z = (view.getBackgroundTintList() == null || view.getBackgroundTintMode() == null) ? false : true;
            if (background != null && z) {
                if (background.isStateful()) {
                    background.setState(view.getDrawableState());
                }
                view.setBackground(background);
            }
        }
    }

    static PorterDuff.Mode f(View view) {
        return view.getBackgroundTintMode();
    }

    static void a(View view, PorterDuff.Mode mode) {
        view.setBackgroundTintMode(mode);
        if (Build.VERSION.SDK_INT == 21) {
            Drawable background = view.getBackground();
            boolean z = (view.getBackgroundTintList() == null || view.getBackgroundTintMode() == null) ? false : true;
            if (background != null && z) {
                if (background.isStateful()) {
                    background.setState(view.getDrawableState());
                }
                view.setBackground(background);
            }
        }
    }

    public static Object a(View view, Object obj) {
        WindowInsets windowInsets = (WindowInsets) obj;
        WindowInsets onApplyWindowInsets = view.onApplyWindowInsets(windowInsets);
        if (onApplyWindowInsets != windowInsets) {
            return new WindowInsets(onApplyWindowInsets);
        }
        return obj;
    }

    public static Object b(View view, Object obj) {
        WindowInsets windowInsets = (WindowInsets) obj;
        WindowInsets dispatchApplyWindowInsets = view.dispatchApplyWindowInsets(windowInsets);
        if (dispatchApplyWindowInsets != windowInsets) {
            return new WindowInsets(dispatchApplyWindowInsets);
        }
        return obj;
    }

    public static void a(View view, boolean z) {
        view.setNestedScrollingEnabled(z);
    }

    public static boolean g(View view) {
        return view.isNestedScrollingEnabled();
    }

    public static void h(View view) {
        view.stopNestedScroll();
    }

    public static float i(View view) {
        return view.getZ();
    }

    static void a(View view, int i) {
        boolean z;
        Rect a2 = a();
        ViewParent parent = view.getParent();
        if (parent instanceof View) {
            View view2 = (View) parent;
            a2.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
            z = !a2.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        } else {
            z = false;
        }
        aj.a(view, i);
        if (z && a2.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
            ((View) parent).invalidate(a2);
        }
    }

    static void b(View view, int i) {
        boolean z;
        Rect a2 = a();
        ViewParent parent = view.getParent();
        if (parent instanceof View) {
            View view2 = (View) parent;
            a2.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
            z = !a2.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        } else {
            z = false;
        }
        aj.b(view, i);
        if (z && a2.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
            ((View) parent).invalidate(a2);
        }
    }

    private static Rect a() {
        if (f1005a == null) {
            f1005a = new ThreadLocal<>();
        }
        Rect rect = f1005a.get();
        if (rect == null) {
            rect = new Rect();
            f1005a.set(rect);
        }
        rect.setEmpty();
        return rect;
    }
}
