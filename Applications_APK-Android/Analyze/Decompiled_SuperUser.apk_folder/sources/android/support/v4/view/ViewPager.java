package android.support.v4.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.a.q;
import android.support.v4.widget.EdgeEffectCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import com.duapps.ad.AdError;
import com.lody.virtual.os.VUserInfo;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ViewPager extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    static final int[] f928a = {16842931};
    private static final h aj = new h();

    /* renamed from: e  reason: collision with root package name */
    private static final Comparator<b> f929e = new Comparator<b>() {
        /* renamed from: a */
        public int compare(b bVar, b bVar2) {
            return bVar.f949b - bVar2.f949b;
        }
    };

    /* renamed from: f  reason: collision with root package name */
    private static final Interpolator f930f = new Interpolator() {
        public float getInterpolation(float f2) {
            float f3 = f2 - 1.0f;
            return (f3 * f3 * f3 * f3 * f3) + 1.0f;
        }
    };
    private int A = 1;
    private boolean B;
    private boolean C;
    private int D;
    private int E;
    private int F;
    private float G;
    private float H;
    private float I;
    private float J;
    private int K = -1;
    private VelocityTracker L;
    private int M;
    private int N;
    private int O;
    private int P;
    private boolean Q;
    private EdgeEffectCompat R;
    private EdgeEffectCompat S;
    private boolean T = true;
    private boolean U = false;
    private boolean V;
    private int W;
    private List<e> aa;
    private e ab;
    private e ac;
    private List<d> ad;
    private f ae;
    private int af;
    private Method ag;
    private int ah;
    private ArrayList<View> ai;
    private final Runnable ak = new Runnable() {
        public void run() {
            ViewPager.this.setScrollState(0);
            ViewPager.this.c();
        }
    };
    private int al = 0;

    /* renamed from: b  reason: collision with root package name */
    z f931b;

    /* renamed from: c  reason: collision with root package name */
    int f932c;

    /* renamed from: d  reason: collision with root package name */
    private int f933d;

    /* renamed from: g  reason: collision with root package name */
    private final ArrayList<b> f934g = new ArrayList<>();

    /* renamed from: h  reason: collision with root package name */
    private final b f935h = new b();
    private final Rect i = new Rect();
    private int j = -1;
    private Parcelable k = null;
    private ClassLoader l = null;
    private Scroller m;
    private boolean n;
    private g o;
    private int p;
    private Drawable q;
    private int r;
    private int s;
    private float t = -3.4028235E38f;
    private float u = Float.MAX_VALUE;
    private int v;
    private int w;
    private boolean x;
    private boolean y;
    private boolean z;

    @Inherited
    @Target({ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface a {
    }

    public interface d {
        void onAdapterChanged(ViewPager viewPager, z zVar, z zVar2);
    }

    public interface e {
        void onPageScrollStateChanged(int i);

        void onPageScrolled(int i, float f2, int i2);

        void onPageSelected(int i);
    }

    public interface f {
        void a(View view, float f2);
    }

    static class b {

        /* renamed from: a  reason: collision with root package name */
        Object f948a;

        /* renamed from: b  reason: collision with root package name */
        int f949b;

        /* renamed from: c  reason: collision with root package name */
        boolean f950c;

        /* renamed from: d  reason: collision with root package name */
        float f951d;

        /* renamed from: e  reason: collision with root package name */
        float f952e;

        b() {
        }
    }

    public static class SimpleOnPageChangeListener implements e {
        public void onPageScrolled(int i, float f2, int i2) {
        }

        public void onPageSelected(int i) {
        }

        public void onPageScrollStateChanged(int i) {
        }
    }

    public ViewPager(Context context) {
        super(context);
        a();
    }

    public ViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, int):void
     arg types: [android.support.v4.view.ViewPager, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, float):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, int):void */
    /* access modifiers changed from: package-private */
    public void a() {
        setWillNotDraw(false);
        setDescendantFocusability(262144);
        setFocusable(true);
        Context context = getContext();
        this.m = new Scroller(context, f930f);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.F = viewConfiguration.getScaledPagingTouchSlop();
        this.M = (int) (400.0f * f2);
        this.N = viewConfiguration.getScaledMaximumFlingVelocity();
        this.R = new EdgeEffectCompat(context);
        this.S = new EdgeEffectCompat(context);
        this.O = (int) (25.0f * f2);
        this.P = (int) (2.0f * f2);
        this.D = (int) (16.0f * f2);
        ag.a(this, new c());
        if (ag.d(this) == 0) {
            ag.c((View) this, 1);
        }
        ag.a(this, new y() {

            /* renamed from: b  reason: collision with root package name */
            private final Rect f938b = new Rect();

            public be onApplyWindowInsets(View view, be beVar) {
                be a2 = ag.a(view, beVar);
                if (a2.f()) {
                    return a2;
                }
                Rect rect = this.f938b;
                rect.left = a2.a();
                rect.top = a2.b();
                rect.right = a2.c();
                rect.bottom = a2.d();
                int childCount = ViewPager.this.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    be b2 = ag.b(ViewPager.this.getChildAt(i), a2);
                    rect.left = Math.min(b2.a(), rect.left);
                    rect.top = Math.min(b2.b(), rect.top);
                    rect.right = Math.min(b2.c(), rect.right);
                    rect.bottom = Math.min(b2.d(), rect.bottom);
                }
                return a2.a(rect.left, rect.top, rect.right, rect.bottom);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.ak);
        if (this.m != null && !this.m.isFinished()) {
            this.m.abortAnimation();
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: package-private */
    public void setScrollState(int i2) {
        if (this.al != i2) {
            this.al = i2;
            if (this.ae != null) {
                b(i2 != 0);
            }
            f(i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.z.a(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.z.a(android.view.View, int, java.lang.Object):void
      android.support.v4.view.z.a(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void setAdapter(z zVar) {
        if (this.f931b != null) {
            this.f931b.c((DataSetObserver) null);
            this.f931b.a((ViewGroup) this);
            for (int i2 = 0; i2 < this.f934g.size(); i2++) {
                b bVar = this.f934g.get(i2);
                this.f931b.a((ViewGroup) this, bVar.f949b, bVar.f948a);
            }
            this.f931b.b((ViewGroup) this);
            this.f934g.clear();
            f();
            this.f932c = 0;
            scrollTo(0, 0);
        }
        z zVar2 = this.f931b;
        this.f931b = zVar;
        this.f933d = 0;
        if (this.f931b != null) {
            if (this.o == null) {
                this.o = new g();
            }
            this.f931b.c(this.o);
            this.z = false;
            boolean z2 = this.T;
            this.T = true;
            this.f933d = this.f931b.b();
            if (this.j >= 0) {
                this.f931b.a(this.k, this.l);
                a(this.j, false, true);
                this.j = -1;
                this.k = null;
                this.l = null;
            } else if (!z2) {
                c();
            } else {
                requestLayout();
            }
        }
        if (this.ad != null && !this.ad.isEmpty()) {
            int size = this.ad.size();
            for (int i3 = 0; i3 < size; i3++) {
                this.ad.get(i3).onAdapterChanged(this, zVar2, zVar);
            }
        }
    }

    private void f() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < getChildCount()) {
                if (!((LayoutParams) getChildAt(i3).getLayoutParams()).f939a) {
                    removeViewAt(i3);
                    i3--;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public z getAdapter() {
        return this.f931b;
    }

    public void a(d dVar) {
        if (this.ad == null) {
            this.ad = new ArrayList();
        }
        this.ad.add(dVar);
    }

    public void b(d dVar) {
        if (this.ad != null) {
            this.ad.remove(dVar);
        }
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void setCurrentItem(int i2) {
        boolean z2;
        this.z = false;
        if (!this.T) {
            z2 = true;
        } else {
            z2 = false;
        }
        a(i2, z2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void a(int i2, boolean z2) {
        this.z = false;
        a(i2, z2, false);
    }

    public int getCurrentItem() {
        return this.f932c;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3) {
        a(i2, z2, z3, 0);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3, int i3) {
        boolean z4 = false;
        if (this.f931b == null || this.f931b.b() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z3 || this.f932c != i2 || this.f934g.size() == 0) {
            if (i2 < 0) {
                i2 = 0;
            } else if (i2 >= this.f931b.b()) {
                i2 = this.f931b.b() - 1;
            }
            int i4 = this.A;
            if (i2 > this.f932c + i4 || i2 < this.f932c - i4) {
                for (int i5 = 0; i5 < this.f934g.size(); i5++) {
                    this.f934g.get(i5).f950c = true;
                }
            }
            if (this.f932c != i2) {
                z4 = true;
            }
            if (this.T) {
                this.f932c = i2;
                if (z4) {
                    e(i2);
                }
                requestLayout();
                return;
            }
            a(i2);
            a(i2, z2, i3, z4);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    private void a(int i2, boolean z2, int i3, boolean z3) {
        int i4;
        b b2 = b(i2);
        if (b2 != null) {
            i4 = (int) (Math.max(this.t, Math.min(b2.f952e, this.u)) * ((float) getClientWidth()));
        } else {
            i4 = 0;
        }
        if (z2) {
            a(i4, 0, i3);
            if (z3) {
                e(i2);
                return;
            }
            return;
        }
        if (z3) {
            e(i2);
        }
        a(false);
        scrollTo(i4, 0);
        d(i4);
    }

    @Deprecated
    public void setOnPageChangeListener(e eVar) {
        this.ab = eVar;
    }

    public void a(e eVar) {
        if (this.aa == null) {
            this.aa = new ArrayList();
        }
        this.aa.add(eVar);
    }

    public void b(e eVar) {
        if (this.aa != null) {
            this.aa.remove(eVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void setChildrenDrawingOrderEnabledCompat(boolean z2) {
        if (Build.VERSION.SDK_INT >= 7) {
            if (this.ag == null) {
                Class<ViewGroup> cls = ViewGroup.class;
                try {
                    this.ag = cls.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
                } catch (NoSuchMethodException e2) {
                    Log.e("ViewPager", "Can't find setChildrenDrawingOrderEnabled", e2);
                }
            }
            try {
                this.ag.invoke(this, Boolean.valueOf(z2));
            } catch (Exception e3) {
                Log.e("ViewPager", "Error changing children drawing order", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.ah == 2) {
            i3 = (i2 - 1) - i3;
        }
        return ((LayoutParams) this.ai.get(i3).getLayoutParams()).f944f;
    }

    /* access modifiers changed from: package-private */
    public e c(e eVar) {
        e eVar2 = this.ac;
        this.ac = eVar;
        return eVar2;
    }

    public int getOffscreenPageLimit() {
        return this.A;
    }

    public void setOffscreenPageLimit(int i2) {
        if (i2 < 1) {
            Log.w("ViewPager", "Requested offscreen page limit " + i2 + " too small; defaulting to " + 1);
            i2 = 1;
        }
        if (i2 != this.A) {
            this.A = i2;
            c();
        }
    }

    public void setPageMargin(int i2) {
        int i3 = this.p;
        this.p = i2;
        int width = getWidth();
        a(width, width, i2, i3);
        requestLayout();
    }

    public int getPageMargin() {
        return this.p;
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.q = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    public void setPageMarginDrawable(int i2) {
        setPageMarginDrawable(android.support.v4.content.c.a(getContext(), i2));
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.q;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.q;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public float a(float f2) {
        return (float) Math.sin((double) ((float) (((double) (f2 - 0.5f)) * 0.4712389167638204d)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4) {
        int scrollX;
        int abs;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        if (this.m != null && !this.m.isFinished()) {
            int currX = this.n ? this.m.getCurrX() : this.m.getStartX();
            this.m.abortAnimation();
            setScrollingCacheEnabled(false);
            scrollX = currX;
        } else {
            scrollX = getScrollX();
        }
        int scrollY = getScrollY();
        int i5 = i2 - scrollX;
        int i6 = i3 - scrollY;
        if (i5 == 0 && i6 == 0) {
            a(false);
            c();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int clientWidth = getClientWidth();
        int i7 = clientWidth / 2;
        float a2 = (((float) i7) * a(Math.min(1.0f, (((float) Math.abs(i5)) * 1.0f) / ((float) clientWidth)))) + ((float) i7);
        int abs2 = Math.abs(i4);
        if (abs2 > 0) {
            abs = Math.round(1000.0f * Math.abs(a2 / ((float) abs2))) * 4;
        } else {
            abs = (int) (((((float) Math.abs(i5)) / ((((float) clientWidth) * this.f931b.d(this.f932c)) + ((float) this.p))) + 1.0f) * 100.0f);
        }
        int min = Math.min(abs, 600);
        this.n = false;
        this.m.startScroll(scrollX, scrollY, i5, i6, min);
        ag.c(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.z.a(android.view.ViewGroup, int):java.lang.Object
     arg types: [android.support.v4.view.ViewPager, int]
     candidates:
      android.support.v4.view.z.a(android.view.View, int):java.lang.Object
      android.support.v4.view.z.a(android.os.Parcelable, java.lang.ClassLoader):void
      android.support.v4.view.z.a(android.view.View, java.lang.Object):boolean
      android.support.v4.view.z.a(android.view.ViewGroup, int):java.lang.Object */
    /* access modifiers changed from: package-private */
    public b a(int i2, int i3) {
        b bVar = new b();
        bVar.f949b = i2;
        bVar.f948a = this.f931b.a((ViewGroup) this, i2);
        bVar.f951d = this.f931b.d(i2);
        if (i3 < 0 || i3 >= this.f934g.size()) {
            this.f934g.add(bVar);
        } else {
            this.f934g.add(i3, bVar);
        }
        return bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.z.a(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.z.a(android.view.View, int, java.lang.Object):void
      android.support.v4.view.z.a(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public void b() {
        boolean z2;
        int i2;
        boolean z3;
        int i3;
        boolean z4;
        int b2 = this.f931b.b();
        this.f933d = b2;
        if (this.f934g.size() >= (this.A * 2) + 1 || this.f934g.size() >= b2) {
            z2 = false;
        } else {
            z2 = true;
        }
        boolean z5 = false;
        int i4 = this.f932c;
        boolean z6 = z2;
        int i5 = 0;
        while (i5 < this.f934g.size()) {
            b bVar = this.f934g.get(i5);
            int a2 = this.f931b.a(bVar.f948a);
            if (a2 == -1) {
                i2 = i5;
                z3 = z5;
                i3 = i4;
                z4 = z6;
            } else if (a2 == -2) {
                this.f934g.remove(i5);
                int i6 = i5 - 1;
                if (!z5) {
                    this.f931b.a((ViewGroup) this);
                    z5 = true;
                }
                this.f931b.a((ViewGroup) this, bVar.f949b, bVar.f948a);
                if (this.f932c == bVar.f949b) {
                    i2 = i6;
                    z3 = z5;
                    i3 = Math.max(0, Math.min(this.f932c, b2 - 1));
                    z4 = true;
                } else {
                    i2 = i6;
                    z3 = z5;
                    i3 = i4;
                    z4 = true;
                }
            } else if (bVar.f949b != a2) {
                if (bVar.f949b == this.f932c) {
                    i4 = a2;
                }
                bVar.f949b = a2;
                i2 = i5;
                z3 = z5;
                i3 = i4;
                z4 = true;
            } else {
                i2 = i5;
                z3 = z5;
                i3 = i4;
                z4 = z6;
            }
            z6 = z4;
            i4 = i3;
            z5 = z3;
            i5 = i2 + 1;
        }
        if (z5) {
            this.f931b.b((ViewGroup) this);
        }
        Collections.sort(this.f934g, f929e);
        if (z6) {
            int childCount = getChildCount();
            for (int i7 = 0; i7 < childCount; i7++) {
                LayoutParams layoutParams = (LayoutParams) getChildAt(i7).getLayoutParams();
                if (!layoutParams.f939a) {
                    layoutParams.f941c = 0.0f;
                }
            }
            a(i4, false, true);
            requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a(this.f932c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.z.b(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.z.b(android.view.View, int, java.lang.Object):void
      android.support.v4.view.z.b(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.z.a(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.z.a(android.view.View, int, java.lang.Object):void
      android.support.v4.view.z.a(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00f0, code lost:
        if (r2.f949b == r0.f932c) goto L_0x00f2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r18) {
        /*
            r17 = this;
            r2 = 0
            r0 = r17
            int r3 = r0.f932c
            r0 = r18
            if (r3 == r0) goto L_0x0323
            r0 = r17
            int r2 = r0.f932c
            r0 = r17
            android.support.v4.view.ViewPager$b r2 = r0.b(r2)
            r0 = r18
            r1 = r17
            r1.f932c = r0
            r3 = r2
        L_0x001a:
            r0 = r17
            android.support.v4.view.z r2 = r0.f931b
            if (r2 != 0) goto L_0x0024
            r17.g()
        L_0x0023:
            return
        L_0x0024:
            r0 = r17
            boolean r2 = r0.z
            if (r2 == 0) goto L_0x002e
            r17.g()
            goto L_0x0023
        L_0x002e:
            android.os.IBinder r2 = r17.getWindowToken()
            if (r2 == 0) goto L_0x0023
            r0 = r17
            android.support.v4.view.z r2 = r0.f931b
            r0 = r17
            r2.a(r0)
            r0 = r17
            int r2 = r0.A
            r4 = 0
            r0 = r17
            int r5 = r0.f932c
            int r5 = r5 - r2
            int r10 = java.lang.Math.max(r4, r5)
            r0 = r17
            android.support.v4.view.z r4 = r0.f931b
            int r11 = r4.b()
            int r4 = r11 + -1
            r0 = r17
            int r5 = r0.f932c
            int r2 = r2 + r5
            int r12 = java.lang.Math.min(r4, r2)
            r0 = r17
            int r2 = r0.f933d
            if (r11 == r2) goto L_0x00cb
            android.content.res.Resources r2 = r17.getResources()     // Catch:{ NotFoundException -> 0x00c1 }
            int r3 = r17.getId()     // Catch:{ NotFoundException -> 0x00c1 }
            java.lang.String r2 = r2.getResourceName(r3)     // Catch:{ NotFoundException -> 0x00c1 }
        L_0x0070:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r17
            int r5 = r0.f933d
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ", found: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r11)
            java.lang.String r5 = " Pager id: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " Pager class: "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.Class r4 = r17.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = " Problematic adapter: "
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r17
            android.support.v4.view.z r4 = r0.f931b
            java.lang.Class r4 = r4.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            throw r3
        L_0x00c1:
            r2 = move-exception
            int r2 = r17.getId()
            java.lang.String r2 = java.lang.Integer.toHexString(r2)
            goto L_0x0070
        L_0x00cb:
            r5 = 0
            r2 = 0
            r4 = r2
        L_0x00ce:
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            int r2 = r2.size()
            if (r4 >= r2) goto L_0x0320
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            java.lang.Object r2 = r2.get(r4)
            android.support.v4.view.ViewPager$b r2 = (android.support.v4.view.ViewPager.b) r2
            int r6 = r2.f949b
            r0 = r17
            int r7 = r0.f932c
            if (r6 < r7) goto L_0x01bc
            int r6 = r2.f949b
            r0 = r17
            int r7 = r0.f932c
            if (r6 != r7) goto L_0x0320
        L_0x00f2:
            if (r2 != 0) goto L_0x031d
            if (r11 <= 0) goto L_0x031d
            r0 = r17
            int r2 = r0.f932c
            r0 = r17
            android.support.v4.view.ViewPager$b r2 = r0.a(r2, r4)
            r9 = r2
        L_0x0101:
            if (r9 == 0) goto L_0x016d
            r8 = 0
            int r7 = r4 + -1
            if (r7 < 0) goto L_0x01c1
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.ViewPager$b r2 = (android.support.v4.view.ViewPager.b) r2
        L_0x0112:
            int r13 = r17.getClientWidth()
            if (r13 > 0) goto L_0x01c4
            r5 = 0
        L_0x0119:
            r0 = r17
            int r6 = r0.f932c
            int r6 = r6 + -1
            r15 = r6
            r6 = r8
            r8 = r15
            r16 = r7
            r7 = r4
            r4 = r16
        L_0x0127:
            if (r8 < 0) goto L_0x0131
            int r14 = (r6 > r5 ? 1 : (r6 == r5 ? 0 : -1))
            if (r14 < 0) goto L_0x0203
            if (r8 >= r10) goto L_0x0203
            if (r2 != 0) goto L_0x01d3
        L_0x0131:
            float r5 = r9.f951d
            int r8 = r7 + 1
            r2 = 1073741824(0x40000000, float:2.0)
            int r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x0168
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            int r2 = r2.size()
            if (r8 >= r2) goto L_0x0239
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            java.lang.Object r2 = r2.get(r8)
            android.support.v4.view.ViewPager$b r2 = (android.support.v4.view.ViewPager.b) r2
            r6 = r2
        L_0x0150:
            if (r13 > 0) goto L_0x023c
            r2 = 0
            r4 = r2
        L_0x0154:
            r0 = r17
            int r2 = r0.f932c
            int r2 = r2 + 1
            r15 = r2
            r2 = r6
            r6 = r8
            r8 = r15
        L_0x015e:
            if (r8 >= r11) goto L_0x0168
            int r10 = (r5 > r4 ? 1 : (r5 == r4 ? 0 : -1))
            if (r10 < 0) goto L_0x0283
            if (r8 <= r12) goto L_0x0283
            if (r2 != 0) goto L_0x0249
        L_0x0168:
            r0 = r17
            r0.a(r9, r7, r3)
        L_0x016d:
            r0 = r17
            android.support.v4.view.z r3 = r0.f931b
            r0 = r17
            int r4 = r0.f932c
            if (r9 == 0) goto L_0x02cd
            java.lang.Object r2 = r9.f948a
        L_0x0179:
            r0 = r17
            r3.b(r0, r4, r2)
            r0 = r17
            android.support.v4.view.z r2 = r0.f931b
            r0 = r17
            r2.b(r0)
            int r4 = r17.getChildCount()
            r2 = 0
            r3 = r2
        L_0x018d:
            if (r3 >= r4) goto L_0x02d0
            r0 = r17
            android.view.View r5 = r0.getChildAt(r3)
            android.view.ViewGroup$LayoutParams r2 = r5.getLayoutParams()
            android.support.v4.view.ViewPager$LayoutParams r2 = (android.support.v4.view.ViewPager.LayoutParams) r2
            r2.f944f = r3
            boolean r6 = r2.f939a
            if (r6 != 0) goto L_0x01b8
            float r6 = r2.f941c
            r7 = 0
            int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r6 != 0) goto L_0x01b8
            r0 = r17
            android.support.v4.view.ViewPager$b r5 = r0.a(r5)
            if (r5 == 0) goto L_0x01b8
            float r6 = r5.f951d
            r2.f941c = r6
            int r5 = r5.f949b
            r2.f943e = r5
        L_0x01b8:
            int r2 = r3 + 1
            r3 = r2
            goto L_0x018d
        L_0x01bc:
            int r2 = r4 + 1
            r4 = r2
            goto L_0x00ce
        L_0x01c1:
            r2 = 0
            goto L_0x0112
        L_0x01c4:
            r5 = 1073741824(0x40000000, float:2.0)
            float r6 = r9.f951d
            float r5 = r5 - r6
            int r6 = r17.getPaddingLeft()
            float r6 = (float) r6
            float r14 = (float) r13
            float r6 = r6 / r14
            float r5 = r5 + r6
            goto L_0x0119
        L_0x01d3:
            int r14 = r2.f949b
            if (r8 != r14) goto L_0x01fd
            boolean r14 = r2.f950c
            if (r14 != 0) goto L_0x01fd
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r14 = r0.f934g
            r14.remove(r4)
            r0 = r17
            android.support.v4.view.z r14 = r0.f931b
            java.lang.Object r2 = r2.f948a
            r0 = r17
            r14.a(r0, r8, r2)
            int r4 = r4 + -1
            int r7 = r7 + -1
            if (r4 < 0) goto L_0x0201
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            java.lang.Object r2 = r2.get(r4)
            android.support.v4.view.ViewPager$b r2 = (android.support.v4.view.ViewPager.b) r2
        L_0x01fd:
            int r8 = r8 + -1
            goto L_0x0127
        L_0x0201:
            r2 = 0
            goto L_0x01fd
        L_0x0203:
            if (r2 == 0) goto L_0x021d
            int r14 = r2.f949b
            if (r8 != r14) goto L_0x021d
            float r2 = r2.f951d
            float r6 = r6 + r2
            int r4 = r4 + -1
            if (r4 < 0) goto L_0x021b
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            java.lang.Object r2 = r2.get(r4)
            android.support.v4.view.ViewPager$b r2 = (android.support.v4.view.ViewPager.b) r2
            goto L_0x01fd
        L_0x021b:
            r2 = 0
            goto L_0x01fd
        L_0x021d:
            int r2 = r4 + 1
            r0 = r17
            android.support.v4.view.ViewPager$b r2 = r0.a(r8, r2)
            float r2 = r2.f951d
            float r6 = r6 + r2
            int r7 = r7 + 1
            if (r4 < 0) goto L_0x0237
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            java.lang.Object r2 = r2.get(r4)
            android.support.v4.view.ViewPager$b r2 = (android.support.v4.view.ViewPager.b) r2
            goto L_0x01fd
        L_0x0237:
            r2 = 0
            goto L_0x01fd
        L_0x0239:
            r6 = 0
            goto L_0x0150
        L_0x023c:
            int r2 = r17.getPaddingRight()
            float r2 = (float) r2
            float r4 = (float) r13
            float r2 = r2 / r4
            r4 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 + r4
            r4 = r2
            goto L_0x0154
        L_0x0249:
            int r10 = r2.f949b
            if (r8 != r10) goto L_0x0318
            boolean r10 = r2.f950c
            if (r10 != 0) goto L_0x0318
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r10 = r0.f934g
            r10.remove(r6)
            r0 = r17
            android.support.v4.view.z r10 = r0.f931b
            java.lang.Object r2 = r2.f948a
            r0 = r17
            r10.a(r0, r8, r2)
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            int r2 = r2.size()
            if (r6 >= r2) goto L_0x0281
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            java.lang.Object r2 = r2.get(r6)
            android.support.v4.view.ViewPager$b r2 = (android.support.v4.view.ViewPager.b) r2
        L_0x0277:
            r15 = r5
            r5 = r2
            r2 = r15
        L_0x027a:
            int r8 = r8 + 1
            r15 = r2
            r2 = r5
            r5 = r15
            goto L_0x015e
        L_0x0281:
            r2 = 0
            goto L_0x0277
        L_0x0283:
            if (r2 == 0) goto L_0x02a8
            int r10 = r2.f949b
            if (r8 != r10) goto L_0x02a8
            float r2 = r2.f951d
            float r5 = r5 + r2
            int r6 = r6 + 1
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            int r2 = r2.size()
            if (r6 >= r2) goto L_0x02a6
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            java.lang.Object r2 = r2.get(r6)
            android.support.v4.view.ViewPager$b r2 = (android.support.v4.view.ViewPager.b) r2
        L_0x02a2:
            r15 = r5
            r5 = r2
            r2 = r15
            goto L_0x027a
        L_0x02a6:
            r2 = 0
            goto L_0x02a2
        L_0x02a8:
            r0 = r17
            android.support.v4.view.ViewPager$b r2 = r0.a(r8, r6)
            int r6 = r6 + 1
            float r2 = r2.f951d
            float r5 = r5 + r2
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            int r2 = r2.size()
            if (r6 >= r2) goto L_0x02cb
            r0 = r17
            java.util.ArrayList<android.support.v4.view.ViewPager$b> r2 = r0.f934g
            java.lang.Object r2 = r2.get(r6)
            android.support.v4.view.ViewPager$b r2 = (android.support.v4.view.ViewPager.b) r2
        L_0x02c7:
            r15 = r5
            r5 = r2
            r2 = r15
            goto L_0x027a
        L_0x02cb:
            r2 = 0
            goto L_0x02c7
        L_0x02cd:
            r2 = 0
            goto L_0x0179
        L_0x02d0:
            r17.g()
            boolean r2 = r17.hasFocus()
            if (r2 == 0) goto L_0x0023
            android.view.View r2 = r17.findFocus()
            if (r2 == 0) goto L_0x0316
            r0 = r17
            android.support.v4.view.ViewPager$b r2 = r0.b(r2)
        L_0x02e5:
            if (r2 == 0) goto L_0x02ef
            int r2 = r2.f949b
            r0 = r17
            int r3 = r0.f932c
            if (r2 == r3) goto L_0x0023
        L_0x02ef:
            r2 = 0
        L_0x02f0:
            int r3 = r17.getChildCount()
            if (r2 >= r3) goto L_0x0023
            r0 = r17
            android.view.View r3 = r0.getChildAt(r2)
            r0 = r17
            android.support.v4.view.ViewPager$b r4 = r0.a(r3)
            if (r4 == 0) goto L_0x0313
            int r4 = r4.f949b
            r0 = r17
            int r5 = r0.f932c
            if (r4 != r5) goto L_0x0313
            r4 = 2
            boolean r3 = r3.requestFocus(r4)
            if (r3 != 0) goto L_0x0023
        L_0x0313:
            int r2 = r2 + 1
            goto L_0x02f0
        L_0x0316:
            r2 = 0
            goto L_0x02e5
        L_0x0318:
            r15 = r5
            r5 = r2
            r2 = r15
            goto L_0x027a
        L_0x031d:
            r9 = r2
            goto L_0x0101
        L_0x0320:
            r2 = r5
            goto L_0x00f2
        L_0x0323:
            r3 = r2
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.a(int):void");
    }

    private void g() {
        if (this.ah != 0) {
            if (this.ai == null) {
                this.ai = new ArrayList<>();
            } else {
                this.ai.clear();
            }
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                this.ai.add(getChildAt(i2));
            }
            Collections.sort(this.ai, aj);
        }
    }

    private void a(b bVar, int i2, b bVar2) {
        float f2;
        b bVar3;
        b bVar4;
        int b2 = this.f931b.b();
        int clientWidth = getClientWidth();
        if (clientWidth > 0) {
            f2 = ((float) this.p) / ((float) clientWidth);
        } else {
            f2 = 0.0f;
        }
        if (bVar2 != null) {
            int i3 = bVar2.f949b;
            if (i3 < bVar.f949b) {
                float f3 = bVar2.f952e + bVar2.f951d + f2;
                int i4 = i3 + 1;
                int i5 = 0;
                while (i4 <= bVar.f949b && i5 < this.f934g.size()) {
                    b bVar5 = this.f934g.get(i5);
                    while (true) {
                        bVar4 = bVar5;
                        if (i4 > bVar4.f949b && i5 < this.f934g.size() - 1) {
                            i5++;
                            bVar5 = this.f934g.get(i5);
                        }
                    }
                    while (i4 < bVar4.f949b) {
                        f3 += this.f931b.d(i4) + f2;
                        i4++;
                    }
                    bVar4.f952e = f3;
                    f3 += bVar4.f951d + f2;
                    i4++;
                }
            } else if (i3 > bVar.f949b) {
                int size = this.f934g.size() - 1;
                float f4 = bVar2.f952e;
                int i6 = i3 - 1;
                while (i6 >= bVar.f949b && size >= 0) {
                    b bVar6 = this.f934g.get(size);
                    while (true) {
                        bVar3 = bVar6;
                        if (i6 < bVar3.f949b && size > 0) {
                            size--;
                            bVar6 = this.f934g.get(size);
                        }
                    }
                    while (i6 > bVar3.f949b) {
                        f4 -= this.f931b.d(i6) + f2;
                        i6--;
                    }
                    f4 -= bVar3.f951d + f2;
                    bVar3.f952e = f4;
                    i6--;
                }
            }
        }
        int size2 = this.f934g.size();
        float f5 = bVar.f952e;
        int i7 = bVar.f949b - 1;
        this.t = bVar.f949b == 0 ? bVar.f952e : -3.4028235E38f;
        this.u = bVar.f949b == b2 + -1 ? (bVar.f952e + bVar.f951d) - 1.0f : Float.MAX_VALUE;
        for (int i8 = i2 - 1; i8 >= 0; i8--) {
            b bVar7 = this.f934g.get(i8);
            float f6 = f5;
            while (i7 > bVar7.f949b) {
                f6 -= this.f931b.d(i7) + f2;
                i7--;
            }
            f5 = f6 - (bVar7.f951d + f2);
            bVar7.f952e = f5;
            if (bVar7.f949b == 0) {
                this.t = f5;
            }
            i7--;
        }
        float f7 = bVar.f952e + bVar.f951d + f2;
        int i9 = bVar.f949b + 1;
        for (int i10 = i2 + 1; i10 < size2; i10++) {
            b bVar8 = this.f934g.get(i10);
            float f8 = f7;
            while (i9 < bVar8.f949b) {
                f8 = this.f931b.d(i9) + f2 + f8;
                i9++;
            }
            if (bVar8.f949b == b2 - 1) {
                this.u = (bVar8.f951d + f8) - 1.0f;
            }
            bVar8.f952e = f8;
            f7 = f8 + bVar8.f951d + f2;
            i9++;
        }
        this.U = false;
    }

    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = android.support.v4.os.f.a(new android.support.v4.os.g<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a  reason: collision with root package name */
        int f945a;

        /* renamed from: b  reason: collision with root package name */
        Parcelable f946b;

        /* renamed from: c  reason: collision with root package name */
        ClassLoader f947c;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f945a);
            parcel.writeParcelable(this.f946b, i);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.f945a + "}";
        }

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.f945a = parcel.readInt();
            this.f946b = parcel.readParcelable(classLoader);
            this.f947c = classLoader;
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f945a = this.f932c;
        if (this.f931b != null) {
            savedState.f946b = this.f931b.a();
        }
        return savedState;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.f931b != null) {
            this.f931b.a(savedState.f946b, savedState.f947c);
            a(savedState.f945a, false, true);
            return;
        }
        this.j = savedState.f945a;
        this.k = savedState.f946b;
        this.l = savedState.f947c;
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2;
        if (!checkLayoutParams(layoutParams)) {
            layoutParams2 = generateLayoutParams(layoutParams);
        } else {
            layoutParams2 = layoutParams;
        }
        LayoutParams layoutParams3 = (LayoutParams) layoutParams2;
        layoutParams3.f939a |= c(view);
        if (!this.x) {
            super.addView(view, i2, layoutParams2);
        } else if (layoutParams3 == null || !layoutParams3.f939a) {
            layoutParams3.f942d = true;
            addViewInLayout(view, i2, layoutParams2);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    private static boolean c(View view) {
        return view.getClass().getAnnotation(a.class) != null;
    }

    public void removeView(View view) {
        if (this.x) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    /* access modifiers changed from: package-private */
    public b a(View view) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.f934g.size()) {
                return null;
            }
            b bVar = this.f934g.get(i3);
            if (this.f931b.a(view, bVar.f948a)) {
                return bVar;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public b b(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return a(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View) parent;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public b b(int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= this.f934g.size()) {
                return null;
            }
            b bVar = this.f934g.get(i4);
            if (bVar.f949b == i2) {
                return bVar;
            }
            i3 = i4 + 1;
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.T = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r0 = 0
            int r0 = getDefaultSize(r0, r14)
            r1 = 0
            int r1 = getDefaultSize(r1, r15)
            r13.setMeasuredDimension(r0, r1)
            int r0 = r13.getMeasuredWidth()
            int r1 = r0 / 10
            int r2 = r13.D
            int r1 = java.lang.Math.min(r1, r2)
            r13.E = r1
            int r1 = r13.getPaddingLeft()
            int r0 = r0 - r1
            int r1 = r13.getPaddingRight()
            int r3 = r0 - r1
            int r0 = r13.getMeasuredHeight()
            int r1 = r13.getPaddingTop()
            int r0 = r0 - r1
            int r1 = r13.getPaddingBottom()
            int r5 = r0 - r1
            int r9 = r13.getChildCount()
            r0 = 0
            r8 = r0
        L_0x003b:
            if (r8 >= r9) goto L_0x00bc
            android.view.View r10 = r13.getChildAt(r8)
            int r0 = r10.getVisibility()
            r1 = 8
            if (r0 == r1) goto L_0x00a5
            android.view.ViewGroup$LayoutParams r0 = r10.getLayoutParams()
            android.support.v4.view.ViewPager$LayoutParams r0 = (android.support.v4.view.ViewPager.LayoutParams) r0
            if (r0 == 0) goto L_0x00a5
            boolean r1 = r0.f939a
            if (r1 == 0) goto L_0x00a5
            int r1 = r0.f940b
            r6 = r1 & 7
            int r1 = r0.f940b
            r4 = r1 & 112(0x70, float:1.57E-43)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 48
            if (r4 == r7) goto L_0x0069
            r7 = 80
            if (r4 != r7) goto L_0x00a9
        L_0x0069:
            r4 = 1
            r7 = r4
        L_0x006b:
            r4 = 3
            if (r6 == r4) goto L_0x0071
            r4 = 5
            if (r6 != r4) goto L_0x00ac
        L_0x0071:
            r4 = 1
            r6 = r4
        L_0x0073:
            if (r7 == 0) goto L_0x00af
            r2 = 1073741824(0x40000000, float:2.0)
        L_0x0077:
            int r4 = r0.width
            r11 = -2
            if (r4 == r11) goto L_0x010f
            r4 = 1073741824(0x40000000, float:2.0)
            int r2 = r0.width
            r11 = -1
            if (r2 == r11) goto L_0x010c
            int r2 = r0.width
        L_0x0085:
            int r11 = r0.height
            r12 = -2
            if (r11 == r12) goto L_0x010a
            r1 = 1073741824(0x40000000, float:2.0)
            int r11 = r0.height
            r12 = -1
            if (r11 == r12) goto L_0x010a
            int r0 = r0.height
        L_0x0093:
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r4)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            r10.measure(r2, r0)
            if (r7 == 0) goto L_0x00b4
            int r0 = r10.getMeasuredHeight()
            int r5 = r5 - r0
        L_0x00a5:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x003b
        L_0x00a9:
            r4 = 0
            r7 = r4
            goto L_0x006b
        L_0x00ac:
            r4 = 0
            r6 = r4
            goto L_0x0073
        L_0x00af:
            if (r6 == 0) goto L_0x0077
            r1 = 1073741824(0x40000000, float:2.0)
            goto L_0x0077
        L_0x00b4:
            if (r6 == 0) goto L_0x00a5
            int r0 = r10.getMeasuredWidth()
            int r3 = r3 - r0
            goto L_0x00a5
        L_0x00bc:
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r0)
            r13.v = r0
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r0)
            r13.w = r0
            r0 = 1
            r13.x = r0
            r13.c()
            r0 = 0
            r13.x = r0
            int r2 = r13.getChildCount()
            r0 = 0
            r1 = r0
        L_0x00db:
            if (r1 >= r2) goto L_0x0109
            android.view.View r4 = r13.getChildAt(r1)
            int r0 = r4.getVisibility()
            r5 = 8
            if (r0 == r5) goto L_0x0105
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            android.support.v4.view.ViewPager$LayoutParams r0 = (android.support.v4.view.ViewPager.LayoutParams) r0
            if (r0 == 0) goto L_0x00f5
            boolean r5 = r0.f939a
            if (r5 != 0) goto L_0x0105
        L_0x00f5:
            float r5 = (float) r3
            float r0 = r0.f941c
            float r0 = r0 * r5
            int r0 = (int) r0
            r5 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r5)
            int r5 = r13.w
            r4.measure(r0, r5)
        L_0x0105:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00db
        L_0x0109:
            return
        L_0x010a:
            r0 = r5
            goto L_0x0093
        L_0x010c:
            r2 = r3
            goto L_0x0085
        L_0x010f:
            r4 = r2
            r2 = r3
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            a(i2, i4, this.p, this.p);
        }
    }

    private void a(int i2, int i3, int i4, int i5) {
        if (i3 <= 0 || this.f934g.isEmpty()) {
            b b2 = b(this.f932c);
            int min = (int) ((b2 != null ? Math.min(b2.f952e, this.u) : 0.0f) * ((float) ((i2 - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                a(false);
                scrollTo(min, getScrollY());
            }
        } else if (!this.m.isFinished()) {
            this.m.setFinalX(getCurrentItem() * getClientWidth());
        } else {
            scrollTo((int) (((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4)) * (((float) getScrollX()) / ((float) (((i3 - getPaddingLeft()) - getPaddingRight()) + i5)))), getScrollY());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void */
    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        b a2;
        int i6;
        int i7;
        int i8;
        int measuredHeight;
        int i9;
        int i10;
        int childCount = getChildCount();
        int i11 = i4 - i2;
        int i12 = i5 - i3;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i13 = 0;
        int i14 = 0;
        while (i14 < childCount) {
            View childAt = getChildAt(i14);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.f939a) {
                    int i15 = layoutParams.f940b & 7;
                    int i16 = layoutParams.f940b & 112;
                    switch (i15) {
                        case 1:
                            i8 = Math.max((i11 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case 4:
                        default:
                            i8 = paddingLeft;
                            break;
                        case 3:
                            i8 = paddingLeft;
                            paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                            break;
                        case 5:
                            int measuredWidth = (i11 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            i8 = measuredWidth;
                            break;
                    }
                    switch (i16) {
                        case 16:
                            measuredHeight = Math.max((i12 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            int i17 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i17;
                            break;
                        case 48:
                            int measuredHeight2 = childAt.getMeasuredHeight() + paddingTop;
                            int i18 = paddingTop;
                            i10 = paddingBottom;
                            i9 = measuredHeight2;
                            measuredHeight = i18;
                            break;
                        case 80:
                            measuredHeight = (i12 - paddingBottom) - childAt.getMeasuredHeight();
                            int measuredHeight3 = paddingBottom + childAt.getMeasuredHeight();
                            i9 = paddingTop;
                            i10 = measuredHeight3;
                            break;
                        default:
                            measuredHeight = paddingTop;
                            int i19 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i19;
                            break;
                    }
                    int i20 = i8 + scrollX;
                    childAt.layout(i20, measuredHeight, childAt.getMeasuredWidth() + i20, childAt.getMeasuredHeight() + measuredHeight);
                    i6 = i13 + 1;
                    i7 = i9;
                    paddingBottom = i10;
                    i14++;
                    paddingLeft = paddingLeft;
                    paddingRight = paddingRight;
                    paddingTop = i7;
                    i13 = i6;
                }
            }
            i6 = i13;
            i7 = paddingTop;
            i14++;
            paddingLeft = paddingLeft;
            paddingRight = paddingRight;
            paddingTop = i7;
            i13 = i6;
        }
        int i21 = (i11 - paddingLeft) - paddingRight;
        for (int i22 = 0; i22 < childCount; i22++) {
            View childAt2 = getChildAt(i22);
            if (childAt2.getVisibility() != 8) {
                LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                if (!layoutParams2.f939a && (a2 = a(childAt2)) != null) {
                    int i23 = ((int) (a2.f952e * ((float) i21))) + paddingLeft;
                    if (layoutParams2.f942d) {
                        layoutParams2.f942d = false;
                        childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (layoutParams2.f941c * ((float) i21)), 1073741824), View.MeasureSpec.makeMeasureSpec((i12 - paddingTop) - paddingBottom, 1073741824));
                    }
                    childAt2.layout(i23, paddingTop, childAt2.getMeasuredWidth() + i23, childAt2.getMeasuredHeight() + paddingTop);
                }
            }
        }
        this.r = paddingTop;
        this.s = i12 - paddingBottom;
        this.W = i13;
        if (this.T) {
            a(this.f932c, false, 0, false);
        }
        this.T = false;
    }

    public void computeScroll() {
        this.n = true;
        if (this.m.isFinished() || !this.m.computeScrollOffset()) {
            a(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.m.getCurrX();
        int currY = this.m.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!d(currX)) {
                this.m.abortAnimation();
                scrollTo(0, currY);
            }
        }
        ag.c(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, float, int):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.ViewPager$b, int, android.support.v4.view.ViewPager$b):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void
      android.support.v4.view.ViewPager.a(int, float, int):void */
    private boolean d(int i2) {
        if (this.f934g.size() != 0) {
            b i3 = i();
            int clientWidth = getClientWidth();
            int i4 = this.p + clientWidth;
            float f2 = ((float) this.p) / ((float) clientWidth);
            int i5 = i3.f949b;
            float f3 = ((((float) i2) / ((float) clientWidth)) - i3.f952e) / (i3.f951d + f2);
            this.V = false;
            a(i5, f3, (int) (((float) i4) * f3));
            if (this.V) {
                return true;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        } else if (this.T) {
            return false;
        } else {
            this.V = false;
            a(0, 0.0f, 0);
            if (this.V) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, float f2, int i3) {
        int measuredWidth;
        int i4;
        int i5;
        if (this.W > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            int i6 = 0;
            while (i6 < childCount) {
                View childAt = getChildAt(i6);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (!layoutParams.f939a) {
                    int i7 = paddingRight;
                    i4 = paddingLeft;
                    i5 = i7;
                } else {
                    switch (layoutParams.f940b & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            int i8 = paddingRight;
                            i4 = paddingLeft;
                            i5 = i8;
                            break;
                        case 2:
                        case 4:
                        default:
                            measuredWidth = paddingLeft;
                            int i9 = paddingRight;
                            i4 = paddingLeft;
                            i5 = i9;
                            break;
                        case 3:
                            int width2 = childAt.getWidth() + paddingLeft;
                            int i10 = paddingLeft;
                            i5 = paddingRight;
                            i4 = width2;
                            measuredWidth = i10;
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            int measuredWidth2 = paddingRight + childAt.getMeasuredWidth();
                            i4 = paddingLeft;
                            i5 = measuredWidth2;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
                i6++;
                int i11 = i5;
                paddingLeft = i4;
                paddingRight = i11;
            }
        }
        b(i2, f2, i3);
        if (this.ae != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i12 = 0; i12 < childCount2; i12++) {
                View childAt2 = getChildAt(i12);
                if (!((LayoutParams) childAt2.getLayoutParams()).f939a) {
                    this.ae.a(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) getClientWidth()));
                }
            }
        }
        this.V = true;
    }

    private void b(int i2, float f2, int i3) {
        if (this.ab != null) {
            this.ab.onPageScrolled(i2, f2, i3);
        }
        if (this.aa != null) {
            int size = this.aa.size();
            for (int i4 = 0; i4 < size; i4++) {
                e eVar = this.aa.get(i4);
                if (eVar != null) {
                    eVar.onPageScrolled(i2, f2, i3);
                }
            }
        }
        if (this.ac != null) {
            this.ac.onPageScrolled(i2, f2, i3);
        }
    }

    private void e(int i2) {
        if (this.ab != null) {
            this.ab.onPageSelected(i2);
        }
        if (this.aa != null) {
            int size = this.aa.size();
            for (int i3 = 0; i3 < size; i3++) {
                e eVar = this.aa.get(i3);
                if (eVar != null) {
                    eVar.onPageSelected(i2);
                }
            }
        }
        if (this.ac != null) {
            this.ac.onPageSelected(i2);
        }
    }

    private void f(int i2) {
        if (this.ab != null) {
            this.ab.onPageScrollStateChanged(i2);
        }
        if (this.aa != null) {
            int size = this.aa.size();
            for (int i3 = 0; i3 < size; i3++) {
                e eVar = this.aa.get(i3);
                if (eVar != null) {
                    eVar.onPageScrollStateChanged(i2);
                }
            }
        }
        if (this.ac != null) {
            this.ac.onPageScrollStateChanged(i2);
        }
    }

    private void a(boolean z2) {
        boolean z3;
        boolean z4 = this.al == 2;
        if (z4) {
            setScrollingCacheEnabled(false);
            if (!this.m.isFinished()) {
                z3 = true;
            } else {
                z3 = false;
            }
            if (z3) {
                this.m.abortAnimation();
                int scrollX = getScrollX();
                int scrollY = getScrollY();
                int currX = this.m.getCurrX();
                int currY = this.m.getCurrY();
                if (!(scrollX == currX && scrollY == currY)) {
                    scrollTo(currX, currY);
                    if (currX != scrollX) {
                        d(currX);
                    }
                }
            }
        }
        this.z = false;
        boolean z5 = z4;
        for (int i2 = 0; i2 < this.f934g.size(); i2++) {
            b bVar = this.f934g.get(i2);
            if (bVar.f950c) {
                bVar.f950c = false;
                z5 = true;
            }
        }
        if (!z5) {
            return;
        }
        if (z2) {
            ag.a(this, this.ak);
        } else {
            this.ak.run();
        }
    }

    private boolean a(float f2, float f3) {
        return (f2 < ((float) this.E) && f3 > 0.0f) || (f2 > ((float) (getWidth() - this.E)) && f3 < 0.0f);
    }

    private void b(boolean z2) {
        int i2;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            if (z2) {
                i2 = this.af;
            } else {
                i2 = 0;
            }
            ag.a(getChildAt(i3), i2, (Paint) null);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & VUserInfo.FLAG_MASK_USER_TYPE;
        if (action == 3 || action == 1) {
            h();
            return false;
        }
        if (action != 0) {
            if (this.B) {
                return true;
            }
            if (this.C) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x2 = motionEvent.getX();
                this.I = x2;
                this.G = x2;
                float y2 = motionEvent.getY();
                this.J = y2;
                this.H = y2;
                this.K = motionEvent.getPointerId(0);
                this.C = false;
                this.n = true;
                this.m.computeScrollOffset();
                if (this.al == 2 && Math.abs(this.m.getFinalX() - this.m.getCurrX()) > this.P) {
                    this.m.abortAnimation();
                    this.z = false;
                    c();
                    this.B = true;
                    c(true);
                    setScrollState(1);
                    break;
                } else {
                    a(false);
                    this.B = false;
                    break;
                }
            case 2:
                int i2 = this.K;
                if (i2 != -1) {
                    int findPointerIndex = motionEvent.findPointerIndex(i2);
                    float x3 = motionEvent.getX(findPointerIndex);
                    float f2 = x3 - this.G;
                    float abs = Math.abs(f2);
                    float y3 = motionEvent.getY(findPointerIndex);
                    float abs2 = Math.abs(y3 - this.J);
                    if (f2 == 0.0f || a(this.G, f2) || !a(this, false, (int) f2, (int) x3, (int) y3)) {
                        if (abs > ((float) this.F) && 0.5f * abs > abs2) {
                            this.B = true;
                            c(true);
                            setScrollState(1);
                            this.G = f2 > 0.0f ? this.I + ((float) this.F) : this.I - ((float) this.F);
                            this.H = y3;
                            setScrollingCacheEnabled(true);
                        } else if (abs2 > ((float) this.F)) {
                            this.C = true;
                        }
                        if (this.B && b(x3)) {
                            ag.c(this);
                            break;
                        }
                    } else {
                        this.G = x3;
                        this.H = y3;
                        this.C = true;
                        return false;
                    }
                }
                break;
            case 6:
                a(motionEvent);
                break;
        }
        if (this.L == null) {
            this.L = VelocityTracker.obtain();
        }
        this.L.addMovement(motionEvent);
        return this.B;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f2;
        boolean z2 = false;
        if (this.Q) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.f931b == null || this.f931b.b() == 0) {
            return false;
        }
        if (this.L == null) {
            this.L = VelocityTracker.obtain();
        }
        this.L.addMovement(motionEvent);
        switch (motionEvent.getAction() & VUserInfo.FLAG_MASK_USER_TYPE) {
            case 0:
                this.m.abortAnimation();
                this.z = false;
                c();
                float x2 = motionEvent.getX();
                this.I = x2;
                this.G = x2;
                float y2 = motionEvent.getY();
                this.J = y2;
                this.H = y2;
                this.K = motionEvent.getPointerId(0);
                break;
            case 1:
                if (this.B) {
                    VelocityTracker velocityTracker = this.L;
                    velocityTracker.computeCurrentVelocity(AdError.NETWORK_ERROR_CODE, (float) this.N);
                    int a2 = (int) ae.a(velocityTracker, this.K);
                    this.z = true;
                    int clientWidth = getClientWidth();
                    int scrollX = getScrollX();
                    b i2 = i();
                    a(a(i2.f949b, ((((float) scrollX) / ((float) clientWidth)) - i2.f952e) / (i2.f951d + (((float) this.p) / ((float) clientWidth))), a2, (int) (motionEvent.getX(motionEvent.findPointerIndex(this.K)) - this.I)), true, true, a2);
                    z2 = h();
                    break;
                }
                break;
            case 2:
                if (!this.B) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.K);
                    if (findPointerIndex == -1) {
                        z2 = h();
                        break;
                    } else {
                        float x3 = motionEvent.getX(findPointerIndex);
                        float abs = Math.abs(x3 - this.G);
                        float y3 = motionEvent.getY(findPointerIndex);
                        float abs2 = Math.abs(y3 - this.H);
                        if (abs > ((float) this.F) && abs > abs2) {
                            this.B = true;
                            c(true);
                            if (x3 - this.I > 0.0f) {
                                f2 = this.I + ((float) this.F);
                            } else {
                                f2 = this.I - ((float) this.F);
                            }
                            this.G = f2;
                            this.H = y3;
                            setScrollState(1);
                            setScrollingCacheEnabled(true);
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                            }
                        }
                    }
                }
                if (this.B) {
                    z2 = false | b(motionEvent.getX(motionEvent.findPointerIndex(this.K)));
                    break;
                }
                break;
            case 3:
                if (this.B) {
                    a(this.f932c, true, 0, false);
                    z2 = h();
                    break;
                }
                break;
            case 5:
                int b2 = s.b(motionEvent);
                this.G = motionEvent.getX(b2);
                this.K = motionEvent.getPointerId(b2);
                break;
            case 6:
                a(motionEvent);
                this.G = motionEvent.getX(motionEvent.findPointerIndex(this.K));
                break;
        }
        if (z2) {
            ag.c(this);
        }
        return true;
    }

    private boolean h() {
        this.K = -1;
        j();
        return this.R.c() | this.S.c();
    }

    private void c(boolean z2) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(z2);
        }
    }

    private boolean b(float f2) {
        boolean z2;
        float f3;
        boolean z3 = true;
        boolean z4 = false;
        this.G = f2;
        float scrollX = ((float) getScrollX()) + (this.G - f2);
        int clientWidth = getClientWidth();
        float f4 = ((float) clientWidth) * this.t;
        float f5 = ((float) clientWidth) * this.u;
        b bVar = this.f934g.get(0);
        b bVar2 = this.f934g.get(this.f934g.size() - 1);
        if (bVar.f949b != 0) {
            f4 = bVar.f952e * ((float) clientWidth);
            z2 = false;
        } else {
            z2 = true;
        }
        if (bVar2.f949b != this.f931b.b() - 1) {
            f3 = bVar2.f952e * ((float) clientWidth);
            z3 = false;
        } else {
            f3 = f5;
        }
        if (scrollX < f4) {
            if (z2) {
                z4 = this.R.a(Math.abs(f4 - scrollX) / ((float) clientWidth));
            }
        } else if (scrollX > f3) {
            if (z3) {
                z4 = this.S.a(Math.abs(scrollX - f3) / ((float) clientWidth));
            }
            f4 = f3;
        } else {
            f4 = scrollX;
        }
        this.G += f4 - ((float) ((int) f4));
        scrollTo((int) f4, getScrollY());
        d((int) f4);
        return z4;
    }

    private b i() {
        float f2;
        int i2;
        b bVar;
        int clientWidth = getClientWidth();
        float scrollX = clientWidth > 0 ? ((float) getScrollX()) / ((float) clientWidth) : 0.0f;
        if (clientWidth > 0) {
            f2 = ((float) this.p) / ((float) clientWidth);
        } else {
            f2 = 0.0f;
        }
        float f3 = 0.0f;
        float f4 = 0.0f;
        int i3 = -1;
        int i4 = 0;
        boolean z2 = true;
        b bVar2 = null;
        while (i4 < this.f934g.size()) {
            b bVar3 = this.f934g.get(i4);
            if (z2 || bVar3.f949b == i3 + 1) {
                b bVar4 = bVar3;
                i2 = i4;
                bVar = bVar4;
            } else {
                b bVar5 = this.f935h;
                bVar5.f952e = f3 + f4 + f2;
                bVar5.f949b = i3 + 1;
                bVar5.f951d = this.f931b.d(bVar5.f949b);
                b bVar6 = bVar5;
                i2 = i4 - 1;
                bVar = bVar6;
            }
            float f5 = bVar.f952e;
            float f6 = bVar.f951d + f5 + f2;
            if (!z2 && scrollX < f5) {
                return bVar2;
            }
            if (scrollX < f6 || i2 == this.f934g.size() - 1) {
                return bVar;
            }
            f4 = f5;
            i3 = bVar.f949b;
            z2 = false;
            f3 = bVar.f951d;
            bVar2 = bVar;
            i4 = i2 + 1;
        }
        return bVar2;
    }

    private int a(int i2, float f2, int i3, int i4) {
        if (Math.abs(i4) <= this.O || Math.abs(i3) <= this.M) {
            i2 += (int) ((i2 >= this.f932c ? 0.4f : 0.6f) + f2);
        } else if (i3 <= 0) {
            i2++;
        }
        if (this.f934g.size() > 0) {
            return Math.max(this.f934g.get(0).f949b, Math.min(i2, this.f934g.get(this.f934g.size() - 1).f949b));
        }
        return i2;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z2 = false;
        int overScrollMode = getOverScrollMode();
        if (overScrollMode == 0 || (overScrollMode == 1 && this.f931b != null && this.f931b.b() > 1)) {
            if (!this.R.a()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.t * ((float) width));
                this.R.a(height, width);
                z2 = false | this.R.a(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.S.a()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.u + 1.0f)) * ((float) width2));
                this.S.a(height2, width2);
                z2 |= this.S.a(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.R.b();
            this.S.b();
        }
        if (z2) {
            ag.c(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f2;
        super.onDraw(canvas);
        if (this.p > 0 && this.q != null && this.f934g.size() > 0 && this.f931b != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f3 = ((float) this.p) / ((float) width);
            b bVar = this.f934g.get(0);
            float f4 = bVar.f952e;
            int size = this.f934g.size();
            int i2 = bVar.f949b;
            int i3 = this.f934g.get(size - 1).f949b;
            int i4 = 0;
            int i5 = i2;
            while (i5 < i3) {
                while (i5 > bVar.f949b && i4 < size) {
                    i4++;
                    bVar = this.f934g.get(i4);
                }
                if (i5 == bVar.f949b) {
                    f2 = (bVar.f952e + bVar.f951d) * ((float) width);
                    f4 = bVar.f952e + bVar.f951d + f3;
                } else {
                    float d2 = this.f931b.d(i5);
                    f2 = (f4 + d2) * ((float) width);
                    f4 += d2 + f3;
                }
                if (((float) this.p) + f2 > ((float) scrollX)) {
                    this.q.setBounds(Math.round(f2), this.r, Math.round(((float) this.p) + f2), this.s);
                    this.q.draw(canvas);
                }
                if (f2 <= ((float) (scrollX + width))) {
                    i5++;
                } else {
                    return;
                }
            }
        }
    }

    private void a(MotionEvent motionEvent) {
        int b2 = s.b(motionEvent);
        if (motionEvent.getPointerId(b2) == this.K) {
            int i2 = b2 == 0 ? 1 : 0;
            this.G = motionEvent.getX(i2);
            this.K = motionEvent.getPointerId(i2);
            if (this.L != null) {
                this.L.clear();
            }
        }
    }

    private void j() {
        this.B = false;
        this.C = false;
        if (this.L != null) {
            this.L.recycle();
            this.L = null;
        }
    }

    private void setScrollingCacheEnabled(boolean z2) {
        if (this.y != z2) {
            this.y = z2;
        }
    }

    public boolean canScrollHorizontally(int i2) {
        boolean z2 = true;
        if (this.f931b == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i2 < 0) {
            if (scrollX <= ((int) (((float) clientWidth) * this.t))) {
                z2 = false;
            }
            return z2;
        } else if (i2 <= 0) {
            return false;
        } else {
            if (scrollX >= ((int) (((float) clientWidth) * this.u))) {
                z2 = false;
            }
            return z2;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, boolean z2, int i2, int i3, int i4) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i3 + scrollX >= childAt.getLeft() && i3 + scrollX < childAt.getRight() && i4 + scrollY >= childAt.getTop() && i4 + scrollY < childAt.getBottom()) {
                    if (a(childAt, true, i2, (i3 + scrollX) - childAt.getLeft(), (i4 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        if (!z2 || !ag.a(view, -i2)) {
            return false;
        }
        return true;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || a(keyEvent);
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (keyEvent.getKeyCode()) {
            case 21:
                return c(17);
            case 22:
                return c(66);
            case 61:
                if (Build.VERSION.SDK_INT < 11) {
                    return false;
                }
                if (g.a(keyEvent)) {
                    return c(2);
                }
                if (g.a(keyEvent, 1)) {
                    return c(1);
                }
                return false;
            default:
                return false;
        }
    }

    public boolean c(int i2) {
        View view;
        boolean z2;
        boolean z3;
        View findFocus = findFocus();
        if (findFocus == this) {
            view = null;
        } else {
            if (findFocus != null) {
                ViewParent parent = findFocus.getParent();
                while (true) {
                    if (!(parent instanceof ViewGroup)) {
                        z2 = false;
                        break;
                    } else if (parent == this) {
                        z2 = true;
                        break;
                    } else {
                        parent = parent.getParent();
                    }
                }
                if (!z2) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(findFocus.getClass().getSimpleName());
                    for (ViewParent parent2 = findFocus.getParent(); parent2 instanceof ViewGroup; parent2 = parent2.getParent()) {
                        sb.append(" => ").append(parent2.getClass().getSimpleName());
                    }
                    Log.e("ViewPager", "arrowScroll tried to find focus based on non-child current focused view " + sb.toString());
                    view = null;
                }
            }
            view = findFocus;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, view, i2);
        if (findNextFocus == null || findNextFocus == view) {
            if (i2 == 17 || i2 == 1) {
                z3 = d();
            } else {
                if (i2 == 66 || i2 == 2) {
                    z3 = e();
                }
                z3 = false;
            }
        } else if (i2 == 17) {
            z3 = (view == null || a(this.i, findNextFocus).left < a(this.i, view).left) ? findNextFocus.requestFocus() : d();
        } else {
            if (i2 == 66) {
                z3 = (view == null || a(this.i, findNextFocus).left > a(this.i, view).left) ? findNextFocus.requestFocus() : e();
            }
            z3 = false;
        }
        if (z3) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i2));
        }
        return z3;
    }

    private Rect a(Rect rect, View view) {
        Rect rect2;
        if (rect == null) {
            rect2 = new Rect();
        } else {
            rect2 = rect;
        }
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        rect2.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect2.left += viewGroup.getLeft();
            rect2.right += viewGroup.getRight();
            rect2.top += viewGroup.getTop();
            rect2.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(float, float):boolean
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.ViewPager$b
      android.support.v4.view.ViewPager.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean d() {
        if (this.f932c <= 0) {
            return false;
        }
        a(this.f932c - 1, true);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(float, float):boolean
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.ViewPager$b
      android.support.v4.view.ViewPager.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean e() {
        if (this.f931b == null || this.f932c >= this.f931b.b() - 1) {
            return false;
        }
        a(this.f932c + 1, true);
        return true;
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        b a2;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.f949b == this.f932c) {
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i3 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList<View> arrayList) {
        b a2;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.f949b == this.f932c) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        int i3;
        b a2;
        int i4 = -1;
        int childCount = getChildCount();
        if ((i2 & 2) != 0) {
            i4 = 1;
            i3 = 0;
        } else {
            i3 = childCount - 1;
            childCount = -1;
        }
        while (i3 != childCount) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.f949b == this.f932c && childAt.requestFocus(i2, rect)) {
                return true;
            }
            i3 += i4;
        }
        return false;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        b a2;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.f949b == this.f932c && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    class c extends a {
        c() {
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setClassName(ViewPager.class.getName());
            q a2 = android.support.v4.view.a.a.a(accessibilityEvent);
            a2.d(a());
            if (accessibilityEvent.getEventType() == 4096 && ViewPager.this.f931b != null) {
                a2.a(ViewPager.this.f931b.b());
                a2.b(ViewPager.this.f932c);
                a2.c(ViewPager.this.f932c);
            }
        }

        public void onInitializeAccessibilityNodeInfo(View view, android.support.v4.view.a.e eVar) {
            super.onInitializeAccessibilityNodeInfo(view, eVar);
            eVar.b((CharSequence) ViewPager.class.getName());
            eVar.k(a());
            if (ViewPager.this.canScrollHorizontally(1)) {
                eVar.a((int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
            }
            if (ViewPager.this.canScrollHorizontally(-1)) {
                eVar.a(8192);
            }
        }

        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            if (super.performAccessibilityAction(view, i, bundle)) {
                return true;
            }
            switch (i) {
                case CodedOutputStream.DEFAULT_BUFFER_SIZE /*4096*/:
                    if (!ViewPager.this.canScrollHorizontally(1)) {
                        return false;
                    }
                    ViewPager.this.setCurrentItem(ViewPager.this.f932c + 1);
                    return true;
                case 8192:
                    if (!ViewPager.this.canScrollHorizontally(-1)) {
                        return false;
                    }
                    ViewPager.this.setCurrentItem(ViewPager.this.f932c - 1);
                    return true;
                default:
                    return false;
            }
        }

        private boolean a() {
            return ViewPager.this.f931b != null && ViewPager.this.f931b.b() > 1;
        }
    }

    private class g extends DataSetObserver {
        g() {
        }

        public void onChanged() {
            ViewPager.this.b();
        }

        public void onInvalidated() {
            ViewPager.this.b();
        }
    }

    public static class LayoutParams extends ViewGroup.LayoutParams {

        /* renamed from: a  reason: collision with root package name */
        public boolean f939a;

        /* renamed from: b  reason: collision with root package name */
        public int f940b;

        /* renamed from: c  reason: collision with root package name */
        float f941c = 0.0f;

        /* renamed from: d  reason: collision with root package name */
        boolean f942d;

        /* renamed from: e  reason: collision with root package name */
        int f943e;

        /* renamed from: f  reason: collision with root package name */
        int f944f;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ViewPager.f928a);
            this.f940b = obtainStyledAttributes.getInteger(0, 48);
            obtainStyledAttributes.recycle();
        }
    }

    static class h implements Comparator<View> {
        h() {
        }

        /* renamed from: a */
        public int compare(View view, View view2) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            LayoutParams layoutParams2 = (LayoutParams) view2.getLayoutParams();
            if (layoutParams.f939a != layoutParams2.f939a) {
                return layoutParams.f939a ? 1 : -1;
            }
            return layoutParams.f943e - layoutParams2.f943e;
        }
    }
}
