package android.support.v4.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

@TargetApi(9)
/* compiled from: LayoutInflaterCompatBase */
class j {

    /* compiled from: LayoutInflaterCompatBase */
    static class a implements LayoutInflater.Factory {

        /* renamed from: a  reason: collision with root package name */
        final m f1046a;

        a(m mVar) {
            this.f1046a = mVar;
        }

        public View onCreateView(String str, Context context, AttributeSet attributeSet) {
            return this.f1046a.a(null, str, context, attributeSet);
        }

        public String toString() {
            return getClass().getName() + "{" + this.f1046a + "}";
        }
    }

    static void a(LayoutInflater layoutInflater, m mVar) {
        layoutInflater.setFactory(mVar != null ? new a(mVar) : null);
    }

    static m a(LayoutInflater layoutInflater) {
        LayoutInflater.Factory factory = layoutInflater.getFactory();
        if (factory instanceof a) {
            return ((a) factory).f1046a;
        }
        return null;
    }
}
