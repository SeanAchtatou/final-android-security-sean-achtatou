package android.support.v4.view;

import android.os.Build;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

/* compiled from: ViewParentCompat */
public final class av {

    /* renamed from: a  reason: collision with root package name */
    static final b f1009a;

    /* compiled from: ViewParentCompat */
    interface b {
        void a(ViewParent viewParent, View view);

        void a(ViewParent viewParent, View view, int i, int i2, int i3, int i4);

        void a(ViewParent viewParent, View view, int i, int i2, int[] iArr);

        boolean a(ViewParent viewParent, View view, float f2, float f3);

        boolean a(ViewParent viewParent, View view, float f2, float f3, boolean z);

        boolean a(ViewParent viewParent, View view, View view2, int i);

        boolean a(ViewParent viewParent, View view, AccessibilityEvent accessibilityEvent);

        void b(ViewParent viewParent, View view, View view2, int i);
    }

    /* compiled from: ViewParentCompat */
    static class e implements b {
        e() {
        }

        public boolean a(ViewParent viewParent, View view, AccessibilityEvent accessibilityEvent) {
            if (view == null) {
                return false;
            }
            ((AccessibilityManager) view.getContext().getSystemService("accessibility")).sendAccessibilityEvent(accessibilityEvent);
            return true;
        }

        public boolean a(ViewParent viewParent, View view, View view2, int i) {
            if (viewParent instanceof w) {
                return ((w) viewParent).onStartNestedScroll(view, view2, i);
            }
            return false;
        }

        public void b(ViewParent viewParent, View view, View view2, int i) {
            if (viewParent instanceof w) {
                ((w) viewParent).onNestedScrollAccepted(view, view2, i);
            }
        }

        public void a(ViewParent viewParent, View view) {
            if (viewParent instanceof w) {
                ((w) viewParent).onStopNestedScroll(view);
            }
        }

        public void a(ViewParent viewParent, View view, int i, int i2, int i3, int i4) {
            if (viewParent instanceof w) {
                ((w) viewParent).onNestedScroll(view, i, i2, i3, i4);
            }
        }

        public void a(ViewParent viewParent, View view, int i, int i2, int[] iArr) {
            if (viewParent instanceof w) {
                ((w) viewParent).onNestedPreScroll(view, i, i2, iArr);
            }
        }

        public boolean a(ViewParent viewParent, View view, float f2, float f3, boolean z) {
            if (viewParent instanceof w) {
                return ((w) viewParent).onNestedFling(view, f2, f3, z);
            }
            return false;
        }

        public boolean a(ViewParent viewParent, View view, float f2, float f3) {
            if (viewParent instanceof w) {
                return ((w) viewParent).onNestedPreFling(view, f2, f3);
            }
            return false;
        }
    }

    /* compiled from: ViewParentCompat */
    static class a extends e {
        a() {
        }

        public boolean a(ViewParent viewParent, View view, AccessibilityEvent accessibilityEvent) {
            return aw.a(viewParent, view, accessibilityEvent);
        }
    }

    /* compiled from: ViewParentCompat */
    static class c extends a {
        c() {
        }
    }

    /* compiled from: ViewParentCompat */
    static class d extends c {
        d() {
        }

        public boolean a(ViewParent viewParent, View view, View view2, int i) {
            return ax.a(viewParent, view, view2, i);
        }

        public void b(ViewParent viewParent, View view, View view2, int i) {
            ax.b(viewParent, view, view2, i);
        }

        public void a(ViewParent viewParent, View view) {
            ax.a(viewParent, view);
        }

        public void a(ViewParent viewParent, View view, int i, int i2, int i3, int i4) {
            ax.a(viewParent, view, i, i2, i3, i4);
        }

        public void a(ViewParent viewParent, View view, int i, int i2, int[] iArr) {
            ax.a(viewParent, view, i, i2, iArr);
        }

        public boolean a(ViewParent viewParent, View view, float f2, float f3, boolean z) {
            return ax.a(viewParent, view, f2, f3, z);
        }

        public boolean a(ViewParent viewParent, View view, float f2, float f3) {
            return ax.a(viewParent, view, f2, f3);
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f1009a = new d();
        } else if (i >= 19) {
            f1009a = new c();
        } else if (i >= 14) {
            f1009a = new a();
        } else {
            f1009a = new e();
        }
    }

    public static boolean a(ViewParent viewParent, View view, AccessibilityEvent accessibilityEvent) {
        return f1009a.a(viewParent, view, accessibilityEvent);
    }

    public static boolean a(ViewParent viewParent, View view, View view2, int i) {
        return f1009a.a(viewParent, view, view2, i);
    }

    public static void b(ViewParent viewParent, View view, View view2, int i) {
        f1009a.b(viewParent, view, view2, i);
    }

    public static void a(ViewParent viewParent, View view) {
        f1009a.a(viewParent, view);
    }

    public static void a(ViewParent viewParent, View view, int i, int i2, int i3, int i4) {
        f1009a.a(viewParent, view, i, i2, i3, i4);
    }

    public static void a(ViewParent viewParent, View view, int i, int i2, int[] iArr) {
        f1009a.a(viewParent, view, i, i2, iArr);
    }

    public static boolean a(ViewParent viewParent, View view, float f2, float f3, boolean z) {
        return f1009a.a(viewParent, view, f2, f3, z);
    }

    public static boolean a(ViewParent viewParent, View view, float f2, float f3) {
        return f1009a.a(viewParent, view, f2, f3);
    }
}
