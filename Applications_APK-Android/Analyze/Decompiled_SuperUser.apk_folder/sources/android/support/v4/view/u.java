package android.support.v4.view;

/* compiled from: NestedScrollingChild */
public interface u {
    boolean isNestedScrollingEnabled();

    void setNestedScrollingEnabled(boolean z);

    void stopNestedScroll();
}
