package android.support.v4.view;

import android.content.res.ColorStateList;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ap;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.WeakHashMap;

/* compiled from: ViewCompat */
public class ag {

    /* renamed from: a  reason: collision with root package name */
    static final l f987a;

    /* compiled from: ViewCompat */
    interface l {
        void A(View view);

        boolean B(View view);

        boolean C(View view);

        ColorStateList D(View view);

        PorterDuff.Mode E(View view);

        void F(View view);

        boolean G(View view);

        float H(View view);

        boolean I(View view);

        boolean J(View view);

        Display K(View view);

        int a(int i, int i2);

        int a(int i, int i2, int i3);

        be a(View view, be beVar);

        void a(View view, float f2);

        void a(View view, int i, int i2);

        void a(View view, int i, int i2, int i3, int i4);

        void a(View view, int i, Paint paint);

        void a(View view, ColorStateList colorStateList);

        void a(View view, Paint paint);

        void a(View view, PorterDuff.Mode mode);

        void a(View view, Drawable drawable);

        void a(View view, android.support.v4.view.a.e eVar);

        void a(View view, a aVar);

        void a(View view, aa aaVar);

        void a(View view, y yVar);

        void a(View view, AccessibilityEvent accessibilityEvent);

        void a(View view, Runnable runnable);

        void a(View view, Runnable runnable, long j);

        void a(View view, boolean z);

        void a(ViewGroup viewGroup, boolean z);

        boolean a(View view);

        boolean a(View view, int i);

        boolean a(View view, int i, Bundle bundle);

        be b(View view, be beVar);

        void b(View view, float f2);

        void b(View view, int i, int i2, int i3, int i4);

        void b(View view, boolean z);

        boolean b(View view);

        boolean b(View view, int i);

        void c(View view);

        void c(View view, float f2);

        void c(View view, int i);

        void c(View view, boolean z);

        int d(View view);

        void d(View view, float f2);

        void d(View view, int i);

        void d(View view, boolean z);

        float e(View view);

        void e(View view, float f2);

        void e(View view, int i);

        int f(View view);

        void f(View view, float f2);

        void f(View view, int i);

        int g(View view);

        void g(View view, float f2);

        ViewParent h(View view);

        void h(View view, float f2);

        int i(View view);

        int j(View view);

        int k(View view);

        int l(View view);

        boolean m(View view);

        float n(View view);

        float o(View view);

        float p(View view);

        Matrix q(View view);

        int r(View view);

        int s(View view);

        ay t(View view);

        String u(View view);

        int v(View view);

        void w(View view);

        float x(View view);

        boolean z(View view);
    }

    /* compiled from: ViewCompat */
    static class b implements l {

        /* renamed from: b  reason: collision with root package name */
        private static Method f988b;

        /* renamed from: a  reason: collision with root package name */
        WeakHashMap<View, ay> f989a = null;

        b() {
        }

        public boolean a(View view, int i) {
            return (view instanceof ac) && a((ac) view, i);
        }

        public boolean b(View view, int i) {
            return (view instanceof ac) && b((ac) view, i);
        }

        public void a(View view, a aVar) {
        }

        public boolean a(View view) {
            return false;
        }

        public void a(View view, AccessibilityEvent accessibilityEvent) {
        }

        public void a(View view, android.support.v4.view.a.e eVar) {
        }

        public boolean b(View view) {
            return false;
        }

        public void c(View view) {
            view.invalidate();
        }

        public void a(View view, int i, int i2, int i3, int i4) {
            view.invalidate(i, i2, i3, i4);
        }

        public void a(View view, Runnable runnable) {
            view.postDelayed(runnable, a());
        }

        public void a(View view, Runnable runnable, long j) {
            view.postDelayed(runnable, a() + j);
        }

        /* access modifiers changed from: package-private */
        public long a() {
            return 10;
        }

        public int d(View view) {
            return 0;
        }

        public void c(View view, int i) {
        }

        public boolean a(View view, int i, Bundle bundle) {
            return false;
        }

        public float e(View view) {
            return 1.0f;
        }

        public void a(View view, int i, Paint paint) {
        }

        public int f(View view) {
            return 0;
        }

        public void a(View view, Paint paint) {
        }

        public int g(View view) {
            return 0;
        }

        public ViewParent h(View view) {
            return view.getParent();
        }

        public int a(int i, int i2, int i3) {
            return View.resolveSize(i, i2);
        }

        public int i(View view) {
            return view.getMeasuredWidth();
        }

        public int j(View view) {
            return 0;
        }

        public void d(View view, int i) {
        }

        public int k(View view) {
            return view.getPaddingLeft();
        }

        public int l(View view) {
            return view.getPaddingRight();
        }

        public void b(View view, int i, int i2, int i3, int i4) {
            view.setPadding(i, i2, i3, i4);
        }

        public boolean m(View view) {
            return true;
        }

        public float n(View view) {
            return 0.0f;
        }

        public float o(View view) {
            return 0.0f;
        }

        public float p(View view) {
            return 0.0f;
        }

        public Matrix q(View view) {
            return null;
        }

        public int r(View view) {
            return ai.d(view);
        }

        public int s(View view) {
            return ai.e(view);
        }

        public ay t(View view) {
            return new ay(view);
        }

        public void a(View view, float f2) {
        }

        public void b(View view, float f2) {
        }

        public void c(View view, float f2) {
        }

        public void d(View view, float f2) {
        }

        public void e(View view, float f2) {
        }

        public void f(View view, float f2) {
        }

        public void g(View view, float f2) {
        }

        public String u(View view) {
            return null;
        }

        public int v(View view) {
            return 0;
        }

        public void w(View view) {
        }

        public void h(View view, float f2) {
        }

        public float x(View view) {
            return 0.0f;
        }

        public float y(View view) {
            return 0.0f;
        }

        public void a(ViewGroup viewGroup, boolean z) {
            if (f988b == null) {
                Class<ViewGroup> cls = ViewGroup.class;
                try {
                    f988b = cls.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
                } catch (NoSuchMethodException e2) {
                    Log.e("ViewCompat", "Unable to find childrenDrawingOrderEnabled", e2);
                }
                f988b.setAccessible(true);
            }
            try {
                f988b.invoke(viewGroup, Boolean.valueOf(z));
            } catch (IllegalAccessException e3) {
                Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", e3);
            } catch (IllegalArgumentException e4) {
                Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", e4);
            } catch (InvocationTargetException e5) {
                Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", e5);
            }
        }

        public boolean z(View view) {
            return false;
        }

        public void a(View view, boolean z) {
        }

        public void A(View view) {
        }

        public void a(View view, y yVar) {
        }

        public be a(View view, be beVar) {
            return beVar;
        }

        public be b(View view, be beVar) {
            return beVar;
        }

        public void b(View view, boolean z) {
        }

        public void c(View view, boolean z) {
        }

        public boolean B(View view) {
            return false;
        }

        public void d(View view, boolean z) {
            if (view instanceof u) {
                ((u) view).setNestedScrollingEnabled(z);
            }
        }

        public boolean C(View view) {
            if (view instanceof u) {
                return ((u) view).isNestedScrollingEnabled();
            }
            return false;
        }

        public void a(View view, Drawable drawable) {
            view.setBackgroundDrawable(drawable);
        }

        public ColorStateList D(View view) {
            return ai.a(view);
        }

        public void a(View view, ColorStateList colorStateList) {
            ai.a(view, colorStateList);
        }

        public void a(View view, PorterDuff.Mode mode) {
            ai.a(view, mode);
        }

        public PorterDuff.Mode E(View view) {
            return ai.b(view);
        }

        private boolean a(ac acVar, int i) {
            int computeHorizontalScrollOffset = acVar.computeHorizontalScrollOffset();
            int computeHorizontalScrollRange = acVar.computeHorizontalScrollRange() - acVar.computeHorizontalScrollExtent();
            if (computeHorizontalScrollRange == 0) {
                return false;
            }
            if (i < 0) {
                if (computeHorizontalScrollOffset <= 0) {
                    return false;
                }
                return true;
            } else if (computeHorizontalScrollOffset >= computeHorizontalScrollRange - 1) {
                return false;
            } else {
                return true;
            }
        }

        private boolean b(ac acVar, int i) {
            int computeVerticalScrollOffset = acVar.computeVerticalScrollOffset();
            int computeVerticalScrollRange = acVar.computeVerticalScrollRange() - acVar.computeVerticalScrollExtent();
            if (computeVerticalScrollRange == 0) {
                return false;
            }
            if (i < 0) {
                if (computeVerticalScrollOffset <= 0) {
                    return false;
                }
                return true;
            } else if (computeVerticalScrollOffset >= computeVerticalScrollRange - 1) {
                return false;
            } else {
                return true;
            }
        }

        public void F(View view) {
            if (view instanceof u) {
                ((u) view).stopNestedScroll();
            }
        }

        public boolean G(View view) {
            return ai.c(view);
        }

        public int a(int i, int i2) {
            return i | i2;
        }

        public float H(View view) {
            return y(view) + x(view);
        }

        public boolean I(View view) {
            return ai.f(view);
        }

        public boolean J(View view) {
            return false;
        }

        public void a(View view, int i, int i2) {
        }

        public void e(View view, int i) {
            ai.b(view, i);
        }

        public void f(View view, int i) {
            ai.a(view, i);
        }

        public void a(View view, aa aaVar) {
        }

        public Display K(View view) {
            return ai.g(view);
        }
    }

    /* compiled from: ViewCompat */
    static class c extends b {
        c() {
        }

        /* access modifiers changed from: package-private */
        public long a() {
            return aj.a();
        }

        public float e(View view) {
            return aj.a(view);
        }

        public void a(View view, int i, Paint paint) {
            aj.a(view, i, paint);
        }

        public int f(View view) {
            return aj.b(view);
        }

        public void a(View view, Paint paint) {
            a(view, f(view), paint);
            view.invalidate();
        }

        public int a(int i, int i2, int i3) {
            return aj.a(i, i2, i3);
        }

        public int i(View view) {
            return aj.c(view);
        }

        public int j(View view) {
            return aj.d(view);
        }

        public float n(View view) {
            return aj.e(view);
        }

        public float o(View view) {
            return aj.f(view);
        }

        public Matrix q(View view) {
            return aj.h(view);
        }

        public void a(View view, float f2) {
            aj.a(view, f2);
        }

        public void b(View view, float f2) {
            aj.b(view, f2);
        }

        public void c(View view, float f2) {
            aj.c(view, f2);
        }

        public void d(View view, float f2) {
            aj.d(view, f2);
        }

        public void e(View view, float f2) {
            aj.e(view, f2);
        }

        public void f(View view, float f2) {
            aj.f(view, f2);
        }

        public void g(View view, float f2) {
            aj.g(view, f2);
        }

        public float p(View view) {
            return aj.g(view);
        }

        public void A(View view) {
            aj.i(view);
        }

        public void b(View view, boolean z) {
            aj.a(view, z);
        }

        public void c(View view, boolean z) {
            aj.b(view, z);
        }

        public int a(int i, int i2) {
            return aj.a(i, i2);
        }

        public void e(View view, int i) {
            aj.b(view, i);
        }

        public void f(View view, int i) {
            aj.a(view, i);
        }
    }

    /* compiled from: ViewCompat */
    static class e extends c {

        /* renamed from: b  reason: collision with root package name */
        static Field f990b;

        /* renamed from: c  reason: collision with root package name */
        static boolean f991c = false;

        e() {
        }

        public boolean a(View view, int i) {
            return ak.a(view, i);
        }

        public boolean b(View view, int i) {
            return ak.b(view, i);
        }

        public void a(View view, AccessibilityEvent accessibilityEvent) {
            ak.a(view, accessibilityEvent);
        }

        public void a(View view, android.support.v4.view.a.e eVar) {
            ak.b(view, eVar.a());
        }

        public void a(View view, a aVar) {
            Object bridge;
            if (aVar == null) {
                bridge = null;
            } else {
                bridge = aVar.getBridge();
            }
            ak.a(view, bridge);
        }

        public boolean a(View view) {
            boolean z = true;
            if (f991c) {
                return false;
            }
            if (f990b == null) {
                try {
                    f990b = View.class.getDeclaredField("mAccessibilityDelegate");
                    f990b.setAccessible(true);
                } catch (Throwable th) {
                    f991c = true;
                    return false;
                }
            }
            try {
                if (f990b.get(view) == null) {
                    z = false;
                }
                return z;
            } catch (Throwable th2) {
                f991c = true;
                return false;
            }
        }

        public ay t(View view) {
            if (this.f989a == null) {
                this.f989a = new WeakHashMap();
            }
            ay ayVar = (ay) this.f989a.get(view);
            if (ayVar != null) {
                return ayVar;
            }
            ay ayVar2 = new ay(view);
            this.f989a.put(view, ayVar2);
            return ayVar2;
        }

        public void a(View view, boolean z) {
            ak.a(view, z);
        }
    }

    /* compiled from: ViewCompat */
    static class d extends e {
        d() {
        }

        public boolean J(View view) {
            return al.a(view);
        }
    }

    /* compiled from: ViewCompat */
    static class f extends d {
        f() {
        }

        public boolean b(View view) {
            return am.a(view);
        }

        public void c(View view) {
            am.b(view);
        }

        public void a(View view, int i, int i2, int i3, int i4) {
            am.a(view, i, i2, i3, i4);
        }

        public void a(View view, Runnable runnable) {
            am.a(view, runnable);
        }

        public void a(View view, Runnable runnable, long j) {
            am.a(view, runnable, j);
        }

        public int d(View view) {
            return am.c(view);
        }

        public void c(View view, int i) {
            if (i == 4) {
                i = 2;
            }
            am.a(view, i);
        }

        public boolean a(View view, int i, Bundle bundle) {
            return am.a(view, i, bundle);
        }

        public ViewParent h(View view) {
            return am.d(view);
        }

        public int r(View view) {
            return am.e(view);
        }

        public int s(View view) {
            return am.f(view);
        }

        public void w(View view) {
            am.g(view);
        }

        public boolean z(View view) {
            return am.h(view);
        }

        public boolean m(View view) {
            return am.i(view);
        }

        public void a(View view, Drawable drawable) {
            am.a(view, drawable);
        }
    }

    /* compiled from: ViewCompat */
    static class g extends f {
        g() {
        }

        public void a(View view, Paint paint) {
            an.a(view, paint);
        }

        public int g(View view) {
            return an.a(view);
        }

        public int k(View view) {
            return an.b(view);
        }

        public int l(View view) {
            return an.c(view);
        }

        public void b(View view, int i, int i2, int i3, int i4) {
            an.a(view, i, i2, i3, i4);
        }

        public int v(View view) {
            return an.d(view);
        }

        public boolean B(View view) {
            return an.e(view);
        }

        public Display K(View view) {
            return an.f(view);
        }
    }

    /* compiled from: ViewCompat */
    static class h extends g {
        h() {
        }
    }

    /* compiled from: ViewCompat */
    static class i extends h {
        i() {
        }

        public void d(View view, int i) {
            ao.a(view, i);
        }

        public void c(View view, int i) {
            am.a(view, i);
        }

        public boolean G(View view) {
            return ao.a(view);
        }

        public boolean I(View view) {
            return ao.b(view);
        }
    }

    /* compiled from: ViewCompat */
    static class j extends i {
        j() {
        }

        public String u(View view) {
            return ap.a(view);
        }

        public void w(View view) {
            ap.b(view);
        }

        public void h(View view, float f2) {
            ap.a(view, f2);
        }

        public float x(View view) {
            return ap.c(view);
        }

        public float y(View view) {
            return ap.d(view);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ap.a(android.view.View, android.support.v4.view.ap$a):void
         arg types: [android.view.View, android.support.v4.view.ag$j$1]
         candidates:
          android.support.v4.view.ap.a(android.view.View, java.lang.Object):java.lang.Object
          android.support.v4.view.ap.a(android.view.View, float):void
          android.support.v4.view.ap.a(android.view.View, int):void
          android.support.v4.view.ap.a(android.view.View, android.content.res.ColorStateList):void
          android.support.v4.view.ap.a(android.view.View, android.graphics.PorterDuff$Mode):void
          android.support.v4.view.ap.a(android.view.View, boolean):void
          android.support.v4.view.ap.a(android.view.View, android.support.v4.view.ap$a):void */
        public void a(View view, final y yVar) {
            if (yVar == null) {
                ap.a(view, (ap.a) null);
            } else {
                ap.a(view, (ap.a) new ap.a() {
                    public Object a(View view, Object obj) {
                        return be.a(yVar.onApplyWindowInsets(view, be.a(obj)));
                    }
                });
            }
        }

        public void d(View view, boolean z) {
            ap.a(view, z);
        }

        public boolean C(View view) {
            return ap.g(view);
        }

        public void F(View view) {
            ap.h(view);
        }

        public ColorStateList D(View view) {
            return ap.e(view);
        }

        public void a(View view, ColorStateList colorStateList) {
            ap.a(view, colorStateList);
        }

        public void a(View view, PorterDuff.Mode mode) {
            ap.a(view, mode);
        }

        public PorterDuff.Mode E(View view) {
            return ap.f(view);
        }

        public be a(View view, be beVar) {
            return be.a(ap.a(view, be.a(beVar)));
        }

        public be b(View view, be beVar) {
            return be.a(ap.b(view, be.a(beVar)));
        }

        public float H(View view) {
            return ap.i(view);
        }

        public void e(View view, int i) {
            ap.b(view, i);
        }

        public void f(View view, int i) {
            ap.a(view, i);
        }
    }

    /* compiled from: ViewCompat */
    static class k extends j {
        k() {
        }

        public void a(View view, int i, int i2) {
            aq.a(view, i, i2);
        }

        public void e(View view, int i) {
            aq.b(view, i);
        }

        public void f(View view, int i) {
            aq.a(view, i);
        }
    }

    /* compiled from: ViewCompat */
    static class a extends k {
        a() {
        }

        public void a(View view, aa aaVar) {
            ah.a(view, aaVar != null ? aaVar.a() : null);
        }
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        if (android.support.v4.os.c.a()) {
            f987a = new a();
        } else if (i2 >= 23) {
            f987a = new k();
        } else if (i2 >= 21) {
            f987a = new j();
        } else if (i2 >= 19) {
            f987a = new i();
        } else if (i2 >= 18) {
            f987a = new h();
        } else if (i2 >= 17) {
            f987a = new g();
        } else if (i2 >= 16) {
            f987a = new f();
        } else if (i2 >= 15) {
            f987a = new d();
        } else if (i2 >= 14) {
            f987a = new e();
        } else if (i2 >= 11) {
            f987a = new c();
        } else {
            f987a = new b();
        }
    }

    public static boolean a(View view, int i2) {
        return f987a.a(view, i2);
    }

    public static boolean b(View view, int i2) {
        return f987a.b(view, i2);
    }

    public static void a(View view, AccessibilityEvent accessibilityEvent) {
        f987a.a(view, accessibilityEvent);
    }

    public static void a(View view, android.support.v4.view.a.e eVar) {
        f987a.a(view, eVar);
    }

    public static void a(View view, a aVar) {
        f987a.a(view, aVar);
    }

    public static boolean a(View view) {
        return f987a.a(view);
    }

    public static boolean b(View view) {
        return f987a.b(view);
    }

    public static void c(View view) {
        f987a.c(view);
    }

    public static void a(View view, int i2, int i3, int i4, int i5) {
        f987a.a(view, i2, i3, i4, i5);
    }

    public static void a(View view, Runnable runnable) {
        f987a.a(view, runnable);
    }

    public static void a(View view, Runnable runnable, long j2) {
        f987a.a(view, runnable, j2);
    }

    public static int d(View view) {
        return f987a.d(view);
    }

    public static void c(View view, int i2) {
        f987a.c(view, i2);
    }

    public static boolean a(View view, int i2, Bundle bundle) {
        return f987a.a(view, i2, bundle);
    }

    public static float e(View view) {
        return f987a.e(view);
    }

    public static void a(View view, int i2, Paint paint) {
        f987a.a(view, i2, paint);
    }

    public static int f(View view) {
        return f987a.f(view);
    }

    public static void a(View view, Paint paint) {
        f987a.a(view, paint);
    }

    public static int g(View view) {
        return f987a.g(view);
    }

    public static ViewParent h(View view) {
        return f987a.h(view);
    }

    public static int a(int i2, int i3, int i4) {
        return f987a.a(i2, i3, i4);
    }

    public static int i(View view) {
        return f987a.i(view);
    }

    public static int j(View view) {
        return f987a.j(view);
    }

    public static int a(int i2, int i3) {
        return f987a.a(i2, i3);
    }

    public static void d(View view, int i2) {
        f987a.d(view, i2);
    }

    public static int k(View view) {
        return f987a.k(view);
    }

    public static int l(View view) {
        return f987a.l(view);
    }

    public static void b(View view, int i2, int i3, int i4, int i5) {
        f987a.b(view, i2, i3, i4, i5);
    }

    public static float m(View view) {
        return f987a.n(view);
    }

    public static float n(View view) {
        return f987a.o(view);
    }

    public static Matrix o(View view) {
        return f987a.q(view);
    }

    public static int p(View view) {
        return f987a.r(view);
    }

    public static int q(View view) {
        return f987a.s(view);
    }

    public static ay r(View view) {
        return f987a.t(view);
    }

    public static void a(View view, float f2) {
        f987a.a(view, f2);
    }

    public static void b(View view, float f2) {
        f987a.b(view, f2);
    }

    public static void c(View view, float f2) {
        f987a.c(view, f2);
    }

    public static void d(View view, float f2) {
        f987a.d(view, f2);
    }

    public static void e(View view, float f2) {
        f987a.e(view, f2);
    }

    public static void f(View view, float f2) {
        f987a.f(view, f2);
    }

    public static void g(View view, float f2) {
        f987a.g(view, f2);
    }

    public static float s(View view) {
        return f987a.p(view);
    }

    public static void h(View view, float f2) {
        f987a.h(view, f2);
    }

    public static float t(View view) {
        return f987a.x(view);
    }

    public static String u(View view) {
        return f987a.u(view);
    }

    public static int v(View view) {
        return f987a.v(view);
    }

    public static void w(View view) {
        f987a.w(view);
    }

    public static void a(ViewGroup viewGroup, boolean z) {
        f987a.a(viewGroup, z);
    }

    public static boolean x(View view) {
        return f987a.z(view);
    }

    public static void a(View view, boolean z) {
        f987a.a(view, z);
    }

    public static void y(View view) {
        f987a.A(view);
    }

    public static void a(View view, y yVar) {
        f987a.a(view, yVar);
    }

    public static be a(View view, be beVar) {
        return f987a.a(view, beVar);
    }

    public static be b(View view, be beVar) {
        return f987a.b(view, beVar);
    }

    public static void b(View view, boolean z) {
        f987a.b(view, z);
    }

    public static void c(View view, boolean z) {
        f987a.c(view, z);
    }

    public static boolean z(View view) {
        return f987a.m(view);
    }

    public static boolean A(View view) {
        return f987a.B(view);
    }

    public static void a(View view, Drawable drawable) {
        f987a.a(view, drawable);
    }

    public static ColorStateList B(View view) {
        return f987a.D(view);
    }

    public static void a(View view, ColorStateList colorStateList) {
        f987a.a(view, colorStateList);
    }

    public static PorterDuff.Mode C(View view) {
        return f987a.E(view);
    }

    public static void a(View view, PorterDuff.Mode mode) {
        f987a.a(view, mode);
    }

    public static void d(View view, boolean z) {
        f987a.d(view, z);
    }

    public static boolean D(View view) {
        return f987a.C(view);
    }

    public static void E(View view) {
        f987a.F(view);
    }

    public static boolean F(View view) {
        return f987a.G(view);
    }

    public static float G(View view) {
        return f987a.H(view);
    }

    public static void e(View view, int i2) {
        f987a.f(view, i2);
    }

    public static void f(View view, int i2) {
        f987a.e(view, i2);
    }

    public static boolean H(View view) {
        return f987a.I(view);
    }

    public static boolean I(View view) {
        return f987a.J(view);
    }

    public static void a(View view, int i2, int i3) {
        f987a.a(view, i2, i3);
    }

    public static void a(View view, aa aaVar) {
        f987a.a(view, aaVar);
    }

    public static Display J(View view) {
        return f987a.K(view);
    }
}
