package android.support.v4.view;

import android.os.Build;
import android.view.ViewConfiguration;

/* compiled from: ViewConfigurationCompat */
public final class ar {

    /* renamed from: a  reason: collision with root package name */
    static final d f1007a;

    /* compiled from: ViewConfigurationCompat */
    interface d {
        boolean a(ViewConfiguration viewConfiguration);
    }

    /* compiled from: ViewConfigurationCompat */
    static class a implements d {
        a() {
        }

        public boolean a(ViewConfiguration viewConfiguration) {
            return true;
        }
    }

    /* compiled from: ViewConfigurationCompat */
    static class b extends a {
        b() {
        }

        public boolean a(ViewConfiguration viewConfiguration) {
            return false;
        }
    }

    /* compiled from: ViewConfigurationCompat */
    static class c extends b {
        c() {
        }

        public boolean a(ViewConfiguration viewConfiguration) {
            return as.a(viewConfiguration);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            f1007a = new c();
        } else if (Build.VERSION.SDK_INT >= 11) {
            f1007a = new b();
        } else {
            f1007a = new a();
        }
    }

    public static boolean a(ViewConfiguration viewConfiguration) {
        return f1007a.a(viewConfiguration);
    }
}
