package android.support.v4.view.animation;

import android.os.Build;
import android.view.animation.Interpolator;

/* compiled from: PathInterpolatorCompat */
public final class b {
    public static Interpolator a(float f2, float f3, float f4, float f5) {
        if (Build.VERSION.SDK_INT >= 21) {
            return c.a(f2, f3, f4, f5);
        }
        return d.a(f2, f3, f4, f5);
    }
}
