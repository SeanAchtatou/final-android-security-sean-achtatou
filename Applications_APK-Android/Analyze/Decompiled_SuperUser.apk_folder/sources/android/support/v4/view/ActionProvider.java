package android.support.v4.view;

import android.content.Context;
import android.util.Log;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public abstract class ActionProvider {

    /* renamed from: a  reason: collision with root package name */
    private final Context f896a;

    /* renamed from: b  reason: collision with root package name */
    private a f897b;

    /* renamed from: c  reason: collision with root package name */
    private b f898c;

    public interface a {
        void a(boolean z);
    }

    public interface b {
        void a(boolean z);
    }

    public abstract View a();

    public ActionProvider(Context context) {
        this.f896a = context;
    }

    public View a(MenuItem menuItem) {
        return a();
    }

    public boolean b() {
        return false;
    }

    public boolean c() {
        return true;
    }

    public boolean d() {
        return false;
    }

    public boolean e() {
        return false;
    }

    public void a(SubMenu subMenu) {
    }

    public void a(boolean z) {
        if (this.f897b != null) {
            this.f897b.a(z);
        }
    }

    public void a(a aVar) {
        this.f897b = aVar;
    }

    public void a(b bVar) {
        if (!(this.f898c == null || bVar == null)) {
            Log.w("ActionProvider(support)", "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this " + getClass().getSimpleName() + " instance while it is still in use somewhere else?");
        }
        this.f898c = bVar;
    }

    public void f() {
        this.f898c = null;
        this.f897b = null;
    }
}
