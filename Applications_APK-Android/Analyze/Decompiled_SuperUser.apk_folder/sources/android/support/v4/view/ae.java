package android.support.v4.view;

import android.os.Build;
import android.view.VelocityTracker;

/* compiled from: VelocityTrackerCompat */
public final class ae {

    /* renamed from: a  reason: collision with root package name */
    static final c f986a;

    /* compiled from: VelocityTrackerCompat */
    interface c {
        float a(VelocityTracker velocityTracker, int i);

        float b(VelocityTracker velocityTracker, int i);
    }

    /* compiled from: VelocityTrackerCompat */
    static class a implements c {
        a() {
        }

        public float a(VelocityTracker velocityTracker, int i) {
            return velocityTracker.getXVelocity();
        }

        public float b(VelocityTracker velocityTracker, int i) {
            return velocityTracker.getYVelocity();
        }
    }

    /* compiled from: VelocityTrackerCompat */
    static class b implements c {
        b() {
        }

        public float a(VelocityTracker velocityTracker, int i) {
            return af.a(velocityTracker, i);
        }

        public float b(VelocityTracker velocityTracker, int i) {
            return af.b(velocityTracker, i);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f986a = new b();
        } else {
            f986a = new a();
        }
    }

    public static float a(VelocityTracker velocityTracker, int i) {
        return f986a.a(velocityTracker, i);
    }

    public static float b(VelocityTracker velocityTracker, int i) {
        return f986a.b(velocityTracker, i);
    }
}
