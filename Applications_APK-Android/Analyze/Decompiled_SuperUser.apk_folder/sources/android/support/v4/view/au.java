package android.support.v4.view;

import android.annotation.TargetApi;
import android.view.ViewGroup;

@TargetApi(11)
/* compiled from: ViewGroupCompatHC */
class au {
    public static void a(ViewGroup viewGroup, boolean z) {
        viewGroup.setMotionEventSplittingEnabled(z);
    }
}
