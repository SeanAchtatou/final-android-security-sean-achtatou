package android.support.v4.view;

import android.view.View;
import android.view.ViewParent;

/* compiled from: NestedScrollingChildHelper */
public class v {

    /* renamed from: a  reason: collision with root package name */
    private final View f1052a;

    /* renamed from: b  reason: collision with root package name */
    private ViewParent f1053b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1054c;

    /* renamed from: d  reason: collision with root package name */
    private int[] f1055d;

    public v(View view) {
        this.f1052a = view;
    }

    public void a(boolean z) {
        if (this.f1054c) {
            ag.E(this.f1052a);
        }
        this.f1054c = z;
    }

    public boolean a() {
        return this.f1054c;
    }

    public boolean b() {
        return this.f1053b != null;
    }

    public boolean a(int i) {
        if (b()) {
            return true;
        }
        if (a()) {
            View view = this.f1052a;
            for (ViewParent parent = this.f1052a.getParent(); parent != null; parent = parent.getParent()) {
                if (av.a(parent, view, this.f1052a, i)) {
                    this.f1053b = parent;
                    av.b(parent, view, this.f1052a, i);
                    return true;
                }
                if (parent instanceof View) {
                    view = (View) parent;
                }
            }
        }
        return false;
    }

    public void c() {
        if (this.f1053b != null) {
            av.a(this.f1053b, this.f1052a);
            this.f1053b = null;
        }
    }

    public boolean a(int i, int i2, int i3, int i4, int[] iArr) {
        int i5;
        int i6;
        if (!a() || this.f1053b == null) {
            return false;
        }
        if (i != 0 || i2 != 0 || i3 != 0 || i4 != 0) {
            if (iArr != null) {
                this.f1052a.getLocationInWindow(iArr);
                int i7 = iArr[0];
                i5 = iArr[1];
                i6 = i7;
            } else {
                i5 = 0;
                i6 = 0;
            }
            av.a(this.f1053b, this.f1052a, i, i2, i3, i4);
            if (iArr != null) {
                this.f1052a.getLocationInWindow(iArr);
                iArr[0] = iArr[0] - i6;
                iArr[1] = iArr[1] - i5;
            }
            return true;
        } else if (iArr == null) {
            return false;
        } else {
            iArr[0] = 0;
            iArr[1] = 0;
            return false;
        }
    }

    public boolean a(int i, int i2, int[] iArr, int[] iArr2) {
        int i3;
        int i4;
        if (!a() || this.f1053b == null) {
            return false;
        }
        if (i != 0 || i2 != 0) {
            if (iArr2 != null) {
                this.f1052a.getLocationInWindow(iArr2);
                i4 = iArr2[0];
                i3 = iArr2[1];
            } else {
                i3 = 0;
                i4 = 0;
            }
            if (iArr == null) {
                if (this.f1055d == null) {
                    this.f1055d = new int[2];
                }
                iArr = this.f1055d;
            }
            iArr[0] = 0;
            iArr[1] = 0;
            av.a(this.f1053b, this.f1052a, i, i2, iArr);
            if (iArr2 != null) {
                this.f1052a.getLocationInWindow(iArr2);
                iArr2[0] = iArr2[0] - i4;
                iArr2[1] = iArr2[1] - i3;
            }
            if (iArr[0] == 0 && iArr[1] == 0) {
                return false;
            }
            return true;
        } else if (iArr2 == null) {
            return false;
        } else {
            iArr2[0] = 0;
            iArr2[1] = 0;
            return false;
        }
    }

    public boolean a(float f2, float f3, boolean z) {
        if (!a() || this.f1053b == null) {
            return false;
        }
        return av.a(this.f1053b, this.f1052a, f2, f3, z);
    }

    public boolean a(float f2, float f3) {
        if (!a() || this.f1053b == null) {
            return false;
        }
        return av.a(this.f1053b, this.f1052a, f2, f3);
    }
}
