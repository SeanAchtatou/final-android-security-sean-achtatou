package android.support.v4.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.view.View;
import android.view.animation.Interpolator;

@TargetApi(14)
/* compiled from: ViewPropertyAnimatorCompatICS */
class az {
    public static void a(View view, long j) {
        view.animate().setDuration(j);
    }

    public static void a(View view, float f2) {
        view.animate().alpha(f2);
    }

    public static void b(View view, float f2) {
        view.animate().translationX(f2);
    }

    public static void c(View view, float f2) {
        view.animate().translationY(f2);
    }

    public static long a(View view) {
        return view.animate().getDuration();
    }

    public static void a(View view, Interpolator interpolator) {
        view.animate().setInterpolator(interpolator);
    }

    public static void b(View view, long j) {
        view.animate().setStartDelay(j);
    }

    public static void b(View view) {
        view.animate().cancel();
    }

    public static void c(View view) {
        view.animate().start();
    }

    public static void a(final View view, final bc bcVar) {
        if (bcVar != null) {
            view.animate().setListener(new AnimatorListenerAdapter() {
                public void onAnimationCancel(Animator animator) {
                    bcVar.onAnimationCancel(view);
                }

                public void onAnimationEnd(Animator animator) {
                    bcVar.onAnimationEnd(view);
                }

                public void onAnimationStart(Animator animator) {
                    bcVar.onAnimationStart(view);
                }
            });
        } else {
            view.animate().setListener(null);
        }
    }
}
