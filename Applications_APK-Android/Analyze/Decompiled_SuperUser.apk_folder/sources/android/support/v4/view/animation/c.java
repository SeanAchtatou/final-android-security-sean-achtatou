package android.support.v4.view.animation;

import android.annotation.TargetApi;
import android.view.animation.Interpolator;
import android.view.animation.PathInterpolator;

@TargetApi(21)
/* compiled from: PathInterpolatorCompatApi21 */
class c {
    public static Interpolator a(float f2, float f3, float f4, float f5) {
        return new PathInterpolator(f2, f3, f4, f5);
    }
}
