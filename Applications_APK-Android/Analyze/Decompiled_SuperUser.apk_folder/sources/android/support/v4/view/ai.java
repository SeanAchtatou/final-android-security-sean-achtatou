package android.support.v4.view;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.view.Display;
import android.view.View;
import android.view.ViewParent;
import android.view.WindowManager;
import java.lang.reflect.Field;

@TargetApi(9)
/* compiled from: ViewCompatBase */
class ai {

    /* renamed from: a  reason: collision with root package name */
    private static Field f994a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f995b;

    /* renamed from: c  reason: collision with root package name */
    private static Field f996c;

    /* renamed from: d  reason: collision with root package name */
    private static boolean f997d;

    static ColorStateList a(View view) {
        if (view instanceof ad) {
            return ((ad) view).getSupportBackgroundTintList();
        }
        return null;
    }

    static void a(View view, ColorStateList colorStateList) {
        if (view instanceof ad) {
            ((ad) view).setSupportBackgroundTintList(colorStateList);
        }
    }

    static PorterDuff.Mode b(View view) {
        if (view instanceof ad) {
            return ((ad) view).getSupportBackgroundTintMode();
        }
        return null;
    }

    static void a(View view, PorterDuff.Mode mode) {
        if (view instanceof ad) {
            ((ad) view).setSupportBackgroundTintMode(mode);
        }
    }

    static boolean c(View view) {
        return view.getWidth() > 0 && view.getHeight() > 0;
    }

    static int d(View view) {
        if (!f995b) {
            try {
                f994a = View.class.getDeclaredField("mMinWidth");
                f994a.setAccessible(true);
            } catch (NoSuchFieldException e2) {
            }
            f995b = true;
        }
        if (f994a != null) {
            try {
                return ((Integer) f994a.get(view)).intValue();
            } catch (Exception e3) {
            }
        }
        return 0;
    }

    static int e(View view) {
        if (!f997d) {
            try {
                f996c = View.class.getDeclaredField("mMinHeight");
                f996c.setAccessible(true);
            } catch (NoSuchFieldException e2) {
            }
            f997d = true;
        }
        if (f996c != null) {
            try {
                return ((Integer) f996c.get(view)).intValue();
            } catch (Exception e3) {
            }
        }
        return 0;
    }

    static boolean f(View view) {
        return view.getWindowToken() != null;
    }

    static void a(View view, int i) {
        int top = view.getTop();
        view.offsetTopAndBottom(i);
        if (i != 0) {
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                int abs = Math.abs(i);
                ((View) parent).invalidate(view.getLeft(), top - abs, view.getRight(), top + view.getHeight() + abs);
                return;
            }
            view.invalidate();
        }
    }

    static void b(View view, int i) {
        int left = view.getLeft();
        view.offsetLeftAndRight(i);
        if (i != 0) {
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                int abs = Math.abs(i);
                ((View) parent).invalidate(left - abs, view.getTop(), left + view.getWidth() + abs, view.getBottom());
                return;
            }
            view.invalidate();
        }
    }

    static Display g(View view) {
        if (f(view)) {
            return ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
        }
        return null;
    }
}
