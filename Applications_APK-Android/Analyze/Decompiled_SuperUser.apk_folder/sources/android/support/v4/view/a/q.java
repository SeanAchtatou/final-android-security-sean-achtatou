package android.support.v4.view.a;

import android.os.Build;
import android.view.View;
import java.util.Collections;
import java.util.List;

/* compiled from: AccessibilityRecordCompat */
public class q {

    /* renamed from: a  reason: collision with root package name */
    private static final c f982a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f983b;

    /* compiled from: AccessibilityRecordCompat */
    interface c {
        List<CharSequence> a(Object obj);

        void a(Object obj, int i);

        void a(Object obj, View view, int i);

        void a(Object obj, CharSequence charSequence);

        void a(Object obj, boolean z);

        void b(Object obj, int i);

        void b(Object obj, CharSequence charSequence);

        void b(Object obj, boolean z);

        void c(Object obj, int i);

        void c(Object obj, boolean z);

        void d(Object obj, int i);

        void d(Object obj, boolean z);

        void e(Object obj, int i);

        void f(Object obj, int i);

        void g(Object obj, int i);
    }

    /* compiled from: AccessibilityRecordCompat */
    static class e implements c {
        e() {
        }

        public List<CharSequence> a(Object obj) {
            return Collections.emptyList();
        }

        public void a(Object obj, boolean z) {
        }

        public void a(Object obj, CharSequence charSequence) {
        }

        public void b(Object obj, CharSequence charSequence) {
        }

        public void b(Object obj, boolean z) {
        }

        public void a(Object obj, int i) {
        }

        public void b(Object obj, int i) {
        }

        public void f(Object obj, int i) {
        }

        public void g(Object obj, int i) {
        }

        public void c(Object obj, boolean z) {
        }

        public void c(Object obj, int i) {
        }

        public void d(Object obj, int i) {
        }

        public void d(Object obj, boolean z) {
        }

        public void a(Object obj, View view, int i) {
        }

        public void e(Object obj, int i) {
        }
    }

    /* compiled from: AccessibilityRecordCompat */
    static class a extends e {
        a() {
        }

        public List<CharSequence> a(Object obj) {
            return r.a(obj);
        }

        public void a(Object obj, boolean z) {
            r.a(obj, z);
        }

        public void a(Object obj, CharSequence charSequence) {
            r.a(obj, charSequence);
        }

        public void b(Object obj, CharSequence charSequence) {
            r.b(obj, charSequence);
        }

        public void b(Object obj, boolean z) {
            r.b(obj, z);
        }

        public void a(Object obj, int i) {
            r.a(obj, i);
        }

        public void b(Object obj, int i) {
            r.b(obj, i);
        }

        public void c(Object obj, boolean z) {
            r.c(obj, z);
        }

        public void c(Object obj, int i) {
            r.c(obj, i);
        }

        public void d(Object obj, int i) {
            r.d(obj, i);
        }

        public void d(Object obj, boolean z) {
            r.d(obj, z);
        }

        public void e(Object obj, int i) {
            r.e(obj, i);
        }
    }

    /* compiled from: AccessibilityRecordCompat */
    static class b extends a {
        b() {
        }

        public void f(Object obj, int i) {
            s.a(obj, i);
        }

        public void g(Object obj, int i) {
            s.b(obj, i);
        }
    }

    /* compiled from: AccessibilityRecordCompat */
    static class d extends b {
        d() {
        }

        public void a(Object obj, View view, int i) {
            t.a(obj, view, i);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f982a = new d();
        } else if (Build.VERSION.SDK_INT >= 15) {
            f982a = new b();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f982a = new a();
        } else {
            f982a = new e();
        }
    }

    @Deprecated
    public q(Object obj) {
        this.f983b = obj;
    }

    public void a(View view, int i) {
        f982a.a(this.f983b, view, i);
    }

    public void a(boolean z) {
        f982a.a(this.f983b, z);
    }

    public void b(boolean z) {
        f982a.b(this.f983b, z);
    }

    public void c(boolean z) {
        f982a.c(this.f983b, z);
    }

    public void d(boolean z) {
        f982a.d(this.f983b, z);
    }

    public void a(int i) {
        f982a.b(this.f983b, i);
    }

    public void b(int i) {
        f982a.a(this.f983b, i);
    }

    public void c(int i) {
        f982a.e(this.f983b, i);
    }

    public void d(int i) {
        f982a.c(this.f983b, i);
    }

    public void e(int i) {
        f982a.d(this.f983b, i);
    }

    public void f(int i) {
        f982a.f(this.f983b, i);
    }

    public void g(int i) {
        f982a.g(this.f983b, i);
    }

    public void a(CharSequence charSequence) {
        f982a.a(this.f983b, charSequence);
    }

    public List<CharSequence> a() {
        return f982a.a(this.f983b);
    }

    public void b(CharSequence charSequence) {
        f982a.b(this.f983b, charSequence);
    }

    public int hashCode() {
        if (this.f983b == null) {
            return 0;
        }
        return this.f983b.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        q qVar = (q) obj;
        if (this.f983b == null) {
            if (qVar.f983b != null) {
                return false;
            }
            return true;
        } else if (!this.f983b.equals(qVar.f983b)) {
            return false;
        } else {
            return true;
        }
    }
}
