package android.support.v4.view;

import android.os.Build;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

/* compiled from: MenuItemCompat */
public final class p {

    /* renamed from: a  reason: collision with root package name */
    static final d f1050a;

    /* compiled from: MenuItemCompat */
    interface d {
        MenuItem a(MenuItem menuItem, View view);

        View a(MenuItem menuItem);

        void a(MenuItem menuItem, int i);

        MenuItem b(MenuItem menuItem, int i);

        boolean b(MenuItem menuItem);

        boolean c(MenuItem menuItem);
    }

    /* compiled from: MenuItemCompat */
    public interface e {
        boolean a(MenuItem menuItem);

        boolean b(MenuItem menuItem);
    }

    /* compiled from: MenuItemCompat */
    static class a implements d {
        a() {
        }

        public void a(MenuItem menuItem, int i) {
        }

        public MenuItem a(MenuItem menuItem, View view) {
            return menuItem;
        }

        public MenuItem b(MenuItem menuItem, int i) {
            return menuItem;
        }

        public View a(MenuItem menuItem) {
            return null;
        }

        public boolean b(MenuItem menuItem) {
            return false;
        }

        public boolean c(MenuItem menuItem) {
            return false;
        }
    }

    /* compiled from: MenuItemCompat */
    static class b implements d {
        b() {
        }

        public void a(MenuItem menuItem, int i) {
            q.a(menuItem, i);
        }

        public MenuItem a(MenuItem menuItem, View view) {
            return q.a(menuItem, view);
        }

        public MenuItem b(MenuItem menuItem, int i) {
            return q.b(menuItem, i);
        }

        public View a(MenuItem menuItem) {
            return q.a(menuItem);
        }

        public boolean b(MenuItem menuItem) {
            return false;
        }

        public boolean c(MenuItem menuItem) {
            return false;
        }
    }

    /* compiled from: MenuItemCompat */
    static class c extends b {
        c() {
        }

        public boolean b(MenuItem menuItem) {
            return r.a(menuItem);
        }

        public boolean c(MenuItem menuItem) {
            return r.b(menuItem);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            f1050a = new c();
        } else if (Build.VERSION.SDK_INT >= 11) {
            f1050a = new b();
        } else {
            f1050a = new a();
        }
    }

    public static void a(MenuItem menuItem, int i) {
        if (menuItem instanceof android.support.v4.internal.view.b) {
            ((android.support.v4.internal.view.b) menuItem).setShowAsAction(i);
        } else {
            f1050a.a(menuItem, i);
        }
    }

    public static MenuItem a(MenuItem menuItem, View view) {
        if (menuItem instanceof android.support.v4.internal.view.b) {
            return ((android.support.v4.internal.view.b) menuItem).setActionView(view);
        }
        return f1050a.a(menuItem, view);
    }

    public static MenuItem b(MenuItem menuItem, int i) {
        if (menuItem instanceof android.support.v4.internal.view.b) {
            return ((android.support.v4.internal.view.b) menuItem).setActionView(i);
        }
        return f1050a.b(menuItem, i);
    }

    public static View a(MenuItem menuItem) {
        if (menuItem instanceof android.support.v4.internal.view.b) {
            return ((android.support.v4.internal.view.b) menuItem).getActionView();
        }
        return f1050a.a(menuItem);
    }

    public static MenuItem a(MenuItem menuItem, ActionProvider actionProvider) {
        if (menuItem instanceof android.support.v4.internal.view.b) {
            return ((android.support.v4.internal.view.b) menuItem).a(actionProvider);
        }
        Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
        return menuItem;
    }

    public static boolean b(MenuItem menuItem) {
        if (menuItem instanceof android.support.v4.internal.view.b) {
            return ((android.support.v4.internal.view.b) menuItem).expandActionView();
        }
        return f1050a.b(menuItem);
    }

    public static boolean c(MenuItem menuItem) {
        if (menuItem instanceof android.support.v4.internal.view.b) {
            return ((android.support.v4.internal.view.b) menuItem).isActionViewExpanded();
        }
        return f1050a.c(menuItem);
    }
}
