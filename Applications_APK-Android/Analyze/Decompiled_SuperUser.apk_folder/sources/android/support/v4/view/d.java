package android.support.v4.view;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

/* compiled from: GestureDetectorCompat */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final a f1032a;

    /* compiled from: GestureDetectorCompat */
    interface a {
        boolean a(MotionEvent motionEvent);
    }

    /* compiled from: GestureDetectorCompat */
    static class b implements a {
        private static final int j = ViewConfiguration.getLongPressTimeout();
        private static final int k = ViewConfiguration.getTapTimeout();
        private static final int l = ViewConfiguration.getDoubleTapTimeout();

        /* renamed from: a  reason: collision with root package name */
        final GestureDetector.OnGestureListener f1033a;

        /* renamed from: b  reason: collision with root package name */
        GestureDetector.OnDoubleTapListener f1034b;

        /* renamed from: c  reason: collision with root package name */
        boolean f1035c;

        /* renamed from: d  reason: collision with root package name */
        boolean f1036d;

        /* renamed from: e  reason: collision with root package name */
        MotionEvent f1037e;

        /* renamed from: f  reason: collision with root package name */
        private int f1038f;

        /* renamed from: g  reason: collision with root package name */
        private int f1039g;

        /* renamed from: h  reason: collision with root package name */
        private int f1040h;
        private int i;
        private final Handler m;
        private boolean n;
        private boolean o;
        private boolean p;
        private MotionEvent q;
        private boolean r;
        private float s;
        private float t;
        private float u;
        private float v;
        private boolean w;
        private VelocityTracker x;

        /* compiled from: GestureDetectorCompat */
        private class a extends Handler {
            a() {
            }

            a(Handler handler) {
                super(handler.getLooper());
            }

            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1:
                        b.this.f1033a.onShowPress(b.this.f1037e);
                        return;
                    case 2:
                        b.this.a();
                        return;
                    case 3:
                        if (b.this.f1034b == null) {
                            return;
                        }
                        if (!b.this.f1035c) {
                            b.this.f1034b.onSingleTapConfirmed(b.this.f1037e);
                            return;
                        } else {
                            b.this.f1036d = true;
                            return;
                        }
                    default:
                        throw new RuntimeException("Unknown message " + message);
                }
            }
        }

        public b(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
            if (handler != null) {
                this.m = new a(handler);
            } else {
                this.m = new a();
            }
            this.f1033a = onGestureListener;
            if (onGestureListener instanceof GestureDetector.OnDoubleTapListener) {
                a((GestureDetector.OnDoubleTapListener) onGestureListener);
            }
            a(context);
        }

        private void a(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null");
            } else if (this.f1033a == null) {
                throw new IllegalArgumentException("OnGestureListener must not be null");
            } else {
                this.w = true;
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
                int scaledDoubleTapSlop = viewConfiguration.getScaledDoubleTapSlop();
                this.f1040h = viewConfiguration.getScaledMinimumFlingVelocity();
                this.i = viewConfiguration.getScaledMaximumFlingVelocity();
                this.f1038f = scaledTouchSlop * scaledTouchSlop;
                this.f1039g = scaledDoubleTapSlop * scaledDoubleTapSlop;
            }
        }

        public void a(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
            this.f1034b = onDoubleTapListener;
        }

        /* JADX WARNING: Removed duplicated region for block: B:43:0x00eb  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x0104  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.view.MotionEvent r14) {
            /*
                r13 = this;
                r6 = 0
                r12 = 2
                r11 = 3
                r8 = 1
                r3 = 0
                int r9 = r14.getAction()
                android.view.VelocityTracker r0 = r13.x
                if (r0 != 0) goto L_0x0013
                android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
                r13.x = r0
            L_0x0013:
                android.view.VelocityTracker r0 = r13.x
                r0.addMovement(r14)
                r0 = r9 & 255(0xff, float:3.57E-43)
                r1 = 6
                if (r0 != r1) goto L_0x0032
                r7 = r8
            L_0x001e:
                if (r7 == 0) goto L_0x0034
                int r0 = android.support.v4.view.s.b(r14)
            L_0x0024:
                int r4 = r14.getPointerCount()
                r5 = r3
                r1 = r6
                r2 = r6
            L_0x002b:
                if (r5 >= r4) goto L_0x0041
                if (r0 != r5) goto L_0x0036
            L_0x002f:
                int r5 = r5 + 1
                goto L_0x002b
            L_0x0032:
                r7 = r3
                goto L_0x001e
            L_0x0034:
                r0 = -1
                goto L_0x0024
            L_0x0036:
                float r10 = r14.getX(r5)
                float r2 = r2 + r10
                float r10 = r14.getY(r5)
                float r1 = r1 + r10
                goto L_0x002f
            L_0x0041:
                if (r7 == 0) goto L_0x004f
                int r0 = r4 + -1
            L_0x0045:
                float r5 = (float) r0
                float r2 = r2 / r5
                float r0 = (float) r0
                float r1 = r1 / r0
                r0 = r9 & 255(0xff, float:3.57E-43)
                switch(r0) {
                    case 0: goto L_0x00a8;
                    case 1: goto L_0x01b3;
                    case 2: goto L_0x013f;
                    case 3: goto L_0x0247;
                    case 4: goto L_0x004e;
                    case 5: goto L_0x0051;
                    case 6: goto L_0x005d;
                    default: goto L_0x004e;
                }
            L_0x004e:
                return r3
            L_0x004f:
                r0 = r4
                goto L_0x0045
            L_0x0051:
                r13.s = r2
                r13.u = r2
                r13.t = r1
                r13.v = r1
                r13.c()
                goto L_0x004e
            L_0x005d:
                r13.s = r2
                r13.u = r2
                r13.t = r1
                r13.v = r1
                android.view.VelocityTracker r0 = r13.x
                r1 = 1000(0x3e8, float:1.401E-42)
                int r2 = r13.i
                float r2 = (float) r2
                r0.computeCurrentVelocity(r1, r2)
                int r1 = android.support.v4.view.s.b(r14)
                int r0 = r14.getPointerId(r1)
                android.view.VelocityTracker r2 = r13.x
                float r2 = android.support.v4.view.ae.a(r2, r0)
                android.view.VelocityTracker r5 = r13.x
                float r5 = android.support.v4.view.ae.b(r5, r0)
                r0 = r3
            L_0x0084:
                if (r0 >= r4) goto L_0x004e
                if (r0 != r1) goto L_0x008b
            L_0x0088:
                int r0 = r0 + 1
                goto L_0x0084
            L_0x008b:
                int r7 = r14.getPointerId(r0)
                android.view.VelocityTracker r8 = r13.x
                float r8 = android.support.v4.view.ae.a(r8, r7)
                float r8 = r8 * r2
                android.view.VelocityTracker r9 = r13.x
                float r7 = android.support.v4.view.ae.b(r9, r7)
                float r7 = r7 * r5
                float r7 = r7 + r8
                int r7 = (r7 > r6 ? 1 : (r7 == r6 ? 0 : -1))
                if (r7 >= 0) goto L_0x0088
                android.view.VelocityTracker r0 = r13.x
                r0.clear()
                goto L_0x004e
            L_0x00a8:
                android.view.GestureDetector$OnDoubleTapListener r0 = r13.f1034b
                if (r0 == 0) goto L_0x013d
                android.os.Handler r0 = r13.m
                boolean r0 = r0.hasMessages(r11)
                if (r0 == 0) goto L_0x00b9
                android.os.Handler r4 = r13.m
                r4.removeMessages(r11)
            L_0x00b9:
                android.view.MotionEvent r4 = r13.f1037e
                if (r4 == 0) goto L_0x0135
                android.view.MotionEvent r4 = r13.q
                if (r4 == 0) goto L_0x0135
                if (r0 == 0) goto L_0x0135
                android.view.MotionEvent r0 = r13.f1037e
                android.view.MotionEvent r4 = r13.q
                boolean r0 = r13.a(r0, r4, r14)
                if (r0 == 0) goto L_0x0135
                r13.r = r8
                android.view.GestureDetector$OnDoubleTapListener r0 = r13.f1034b
                android.view.MotionEvent r4 = r13.f1037e
                boolean r0 = r0.onDoubleTap(r4)
                r0 = r0 | r3
                android.view.GestureDetector$OnDoubleTapListener r4 = r13.f1034b
                boolean r4 = r4.onDoubleTapEvent(r14)
                r0 = r0 | r4
            L_0x00df:
                r13.s = r2
                r13.u = r2
                r13.t = r1
                r13.v = r1
                android.view.MotionEvent r1 = r13.f1037e
                if (r1 == 0) goto L_0x00f0
                android.view.MotionEvent r1 = r13.f1037e
                r1.recycle()
            L_0x00f0:
                android.view.MotionEvent r1 = android.view.MotionEvent.obtain(r14)
                r13.f1037e = r1
                r13.o = r8
                r13.p = r8
                r13.f1035c = r8
                r13.n = r3
                r13.f1036d = r3
                boolean r1 = r13.w
                if (r1 == 0) goto L_0x011c
                android.os.Handler r1 = r13.m
                r1.removeMessages(r12)
                android.os.Handler r1 = r13.m
                android.view.MotionEvent r2 = r13.f1037e
                long r2 = r2.getDownTime()
                int r4 = android.support.v4.view.d.b.k
                long r4 = (long) r4
                long r2 = r2 + r4
                int r4 = android.support.v4.view.d.b.j
                long r4 = (long) r4
                long r2 = r2 + r4
                r1.sendEmptyMessageAtTime(r12, r2)
            L_0x011c:
                android.os.Handler r1 = r13.m
                android.view.MotionEvent r2 = r13.f1037e
                long r2 = r2.getDownTime()
                int r4 = android.support.v4.view.d.b.k
                long r4 = (long) r4
                long r2 = r2 + r4
                r1.sendEmptyMessageAtTime(r8, r2)
                android.view.GestureDetector$OnGestureListener r1 = r13.f1033a
                boolean r1 = r1.onDown(r14)
                r3 = r0 | r1
                goto L_0x004e
            L_0x0135:
                android.os.Handler r0 = r13.m
                int r4 = android.support.v4.view.d.b.l
                long r4 = (long) r4
                r0.sendEmptyMessageDelayed(r11, r4)
            L_0x013d:
                r0 = r3
                goto L_0x00df
            L_0x013f:
                boolean r0 = r13.n
                if (r0 != 0) goto L_0x004e
                float r0 = r13.s
                float r0 = r0 - r2
                float r4 = r13.t
                float r4 = r4 - r1
                boolean r5 = r13.r
                if (r5 == 0) goto L_0x0156
                android.view.GestureDetector$OnDoubleTapListener r0 = r13.f1034b
                boolean r0 = r0.onDoubleTapEvent(r14)
                r3 = r3 | r0
                goto L_0x004e
            L_0x0156:
                boolean r5 = r13.o
                if (r5 == 0) goto L_0x0191
                float r5 = r13.u
                float r5 = r2 - r5
                int r5 = (int) r5
                float r6 = r13.v
                float r6 = r1 - r6
                int r6 = (int) r6
                int r5 = r5 * r5
                int r6 = r6 * r6
                int r5 = r5 + r6
                int r6 = r13.f1038f
                if (r5 <= r6) goto L_0x024f
                android.view.GestureDetector$OnGestureListener r6 = r13.f1033a
                android.view.MotionEvent r7 = r13.f1037e
                boolean r0 = r6.onScroll(r7, r14, r0, r4)
                r13.s = r2
                r13.t = r1
                r13.o = r3
                android.os.Handler r1 = r13.m
                r1.removeMessages(r11)
                android.os.Handler r1 = r13.m
                r1.removeMessages(r8)
                android.os.Handler r1 = r13.m
                r1.removeMessages(r12)
            L_0x0188:
                int r1 = r13.f1038f
                if (r5 <= r1) goto L_0x018e
                r13.p = r3
            L_0x018e:
                r3 = r0
                goto L_0x004e
            L_0x0191:
                float r5 = java.lang.Math.abs(r0)
                r6 = 1065353216(0x3f800000, float:1.0)
                int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
                if (r5 >= 0) goto L_0x01a5
                float r5 = java.lang.Math.abs(r4)
                r6 = 1065353216(0x3f800000, float:1.0)
                int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
                if (r5 < 0) goto L_0x004e
            L_0x01a5:
                android.view.GestureDetector$OnGestureListener r3 = r13.f1033a
                android.view.MotionEvent r5 = r13.f1037e
                boolean r3 = r3.onScroll(r5, r14, r0, r4)
                r13.s = r2
                r13.t = r1
                goto L_0x004e
            L_0x01b3:
                r13.f1035c = r3
                android.view.MotionEvent r1 = android.view.MotionEvent.obtain(r14)
                boolean r0 = r13.r
                if (r0 == 0) goto L_0x01ec
                android.view.GestureDetector$OnDoubleTapListener r0 = r13.f1034b
                boolean r0 = r0.onDoubleTapEvent(r14)
                r0 = r0 | r3
            L_0x01c4:
                android.view.MotionEvent r2 = r13.q
                if (r2 == 0) goto L_0x01cd
                android.view.MotionEvent r2 = r13.q
                r2.recycle()
            L_0x01cd:
                r13.q = r1
                android.view.VelocityTracker r1 = r13.x
                if (r1 == 0) goto L_0x01db
                android.view.VelocityTracker r1 = r13.x
                r1.recycle()
                r1 = 0
                r13.x = r1
            L_0x01db:
                r13.r = r3
                r13.f1036d = r3
                android.os.Handler r1 = r13.m
                r1.removeMessages(r8)
                android.os.Handler r1 = r13.m
                r1.removeMessages(r12)
                r3 = r0
                goto L_0x004e
            L_0x01ec:
                boolean r0 = r13.n
                if (r0 == 0) goto L_0x01f9
                android.os.Handler r0 = r13.m
                r0.removeMessages(r11)
                r13.n = r3
                r0 = r3
                goto L_0x01c4
            L_0x01f9:
                boolean r0 = r13.o
                if (r0 == 0) goto L_0x0211
                android.view.GestureDetector$OnGestureListener r0 = r13.f1033a
                boolean r0 = r0.onSingleTapUp(r14)
                boolean r2 = r13.f1036d
                if (r2 == 0) goto L_0x01c4
                android.view.GestureDetector$OnDoubleTapListener r2 = r13.f1034b
                if (r2 == 0) goto L_0x01c4
                android.view.GestureDetector$OnDoubleTapListener r2 = r13.f1034b
                r2.onSingleTapConfirmed(r14)
                goto L_0x01c4
            L_0x0211:
                android.view.VelocityTracker r0 = r13.x
                int r2 = r14.getPointerId(r3)
                r4 = 1000(0x3e8, float:1.401E-42)
                int r5 = r13.i
                float r5 = (float) r5
                r0.computeCurrentVelocity(r4, r5)
                float r4 = android.support.v4.view.ae.b(r0, r2)
                float r0 = android.support.v4.view.ae.a(r0, r2)
                float r2 = java.lang.Math.abs(r4)
                int r5 = r13.f1040h
                float r5 = (float) r5
                int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
                if (r2 > 0) goto L_0x023d
                float r2 = java.lang.Math.abs(r0)
                int r5 = r13.f1040h
                float r5 = (float) r5
                int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
                if (r2 <= 0) goto L_0x024c
            L_0x023d:
                android.view.GestureDetector$OnGestureListener r2 = r13.f1033a
                android.view.MotionEvent r5 = r13.f1037e
                boolean r0 = r2.onFling(r5, r14, r0, r4)
                goto L_0x01c4
            L_0x0247:
                r13.b()
                goto L_0x004e
            L_0x024c:
                r0 = r3
                goto L_0x01c4
            L_0x024f:
                r0 = r3
                goto L_0x0188
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.d.b.a(android.view.MotionEvent):boolean");
        }

        private void b() {
            this.m.removeMessages(1);
            this.m.removeMessages(2);
            this.m.removeMessages(3);
            this.x.recycle();
            this.x = null;
            this.r = false;
            this.f1035c = false;
            this.o = false;
            this.p = false;
            this.f1036d = false;
            if (this.n) {
                this.n = false;
            }
        }

        private void c() {
            this.m.removeMessages(1);
            this.m.removeMessages(2);
            this.m.removeMessages(3);
            this.r = false;
            this.o = false;
            this.p = false;
            this.f1036d = false;
            if (this.n) {
                this.n = false;
            }
        }

        private boolean a(MotionEvent motionEvent, MotionEvent motionEvent2, MotionEvent motionEvent3) {
            if (!this.p || motionEvent3.getEventTime() - motionEvent2.getEventTime() > ((long) l)) {
                return false;
            }
            int x2 = ((int) motionEvent.getX()) - ((int) motionEvent3.getX());
            int y = ((int) motionEvent.getY()) - ((int) motionEvent3.getY());
            if ((x2 * x2) + (y * y) < this.f1039g) {
                return true;
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.m.removeMessages(3);
            this.f1036d = false;
            this.n = true;
            this.f1033a.onLongPress(this.f1037e);
        }
    }

    /* compiled from: GestureDetectorCompat */
    static class c implements a {

        /* renamed from: a  reason: collision with root package name */
        private final GestureDetector f1042a;

        public c(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
            this.f1042a = new GestureDetector(context, onGestureListener, handler);
        }

        public boolean a(MotionEvent motionEvent) {
            return this.f1042a.onTouchEvent(motionEvent);
        }
    }

    public d(Context context, GestureDetector.OnGestureListener onGestureListener) {
        this(context, onGestureListener, null);
    }

    public d(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
        if (Build.VERSION.SDK_INT > 17) {
            this.f1032a = new c(context, onGestureListener, handler);
        } else {
            this.f1032a = new b(context, onGestureListener, handler);
        }
    }

    public boolean a(MotionEvent motionEvent) {
        return this.f1032a.a(motionEvent);
    }
}
