package android.support.v4.view;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: PagerAdapter */
public abstract class z {

    /* renamed from: a  reason: collision with root package name */
    private final DataSetObservable f1058a = new DataSetObservable();

    /* renamed from: b  reason: collision with root package name */
    private DataSetObserver f1059b;

    public abstract boolean a(View view, Object obj);

    public abstract int b();

    public void a(ViewGroup viewGroup) {
        a((View) viewGroup);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.z.a(android.view.View, int):java.lang.Object
     arg types: [android.view.ViewGroup, int]
     candidates:
      android.support.v4.view.z.a(android.view.ViewGroup, int):java.lang.Object
      android.support.v4.view.z.a(android.os.Parcelable, java.lang.ClassLoader):void
      android.support.v4.view.z.a(android.view.View, java.lang.Object):boolean
      android.support.v4.view.z.a(android.view.View, int):java.lang.Object */
    public Object a(ViewGroup viewGroup, int i) {
        return a((View) viewGroup, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.z.a(android.view.View, int, java.lang.Object):void
     arg types: [android.view.ViewGroup, int, java.lang.Object]
     candidates:
      android.support.v4.view.z.a(android.view.ViewGroup, int, java.lang.Object):void
      android.support.v4.view.z.a(android.view.View, int, java.lang.Object):void */
    public void a(ViewGroup viewGroup, int i, Object obj) {
        a((View) viewGroup, i, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.z.b(android.view.View, int, java.lang.Object):void
     arg types: [android.view.ViewGroup, int, java.lang.Object]
     candidates:
      android.support.v4.view.z.b(android.view.ViewGroup, int, java.lang.Object):void
      android.support.v4.view.z.b(android.view.View, int, java.lang.Object):void */
    public void b(ViewGroup viewGroup, int i, Object obj) {
        b((View) viewGroup, i, obj);
    }

    public void b(ViewGroup viewGroup) {
        b((View) viewGroup);
    }

    @Deprecated
    public void a(View view) {
    }

    @Deprecated
    public Object a(View view, int i) {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    @Deprecated
    public void a(View view, int i, Object obj) {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    @Deprecated
    public void b(View view, int i, Object obj) {
    }

    @Deprecated
    public void b(View view) {
    }

    public Parcelable a() {
        return null;
    }

    public void a(Parcelable parcelable, ClassLoader classLoader) {
    }

    public int a(Object obj) {
        return -1;
    }

    public void a(DataSetObserver dataSetObserver) {
        this.f1058a.registerObserver(dataSetObserver);
    }

    public void b(DataSetObserver dataSetObserver) {
        this.f1058a.unregisterObserver(dataSetObserver);
    }

    /* access modifiers changed from: package-private */
    public void c(DataSetObserver dataSetObserver) {
        synchronized (this) {
            this.f1059b = dataSetObserver;
        }
    }

    public CharSequence c(int i) {
        return null;
    }

    public float d(int i) {
        return 1.0f;
    }
}
