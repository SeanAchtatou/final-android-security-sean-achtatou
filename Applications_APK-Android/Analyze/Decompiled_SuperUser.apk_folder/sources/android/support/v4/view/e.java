package android.support.v4.view;

import android.graphics.Rect;
import android.os.Build;
import android.view.Gravity;

/* compiled from: GravityCompat */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    static final a f1043a;

    /* compiled from: GravityCompat */
    interface a {
        int a(int i, int i2);

        void a(int i, int i2, int i3, Rect rect, Rect rect2, int i4);
    }

    /* compiled from: GravityCompat */
    static class b implements a {
        b() {
        }

        public int a(int i, int i2) {
            return -8388609 & i;
        }

        public void a(int i, int i2, int i3, Rect rect, Rect rect2, int i4) {
            Gravity.apply(i, i2, i3, rect, rect2);
        }
    }

    /* compiled from: GravityCompat */
    static class c implements a {
        c() {
        }

        public int a(int i, int i2) {
            return f.a(i, i2);
        }

        public void a(int i, int i2, int i3, Rect rect, Rect rect2, int i4) {
            f.a(i, i2, i3, rect, rect2, i4);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            f1043a = new c();
        } else {
            f1043a = new b();
        }
    }

    public static void a(int i, int i2, int i3, Rect rect, Rect rect2, int i4) {
        f1043a.a(i, i2, i3, rect, rect2, i4);
    }

    public static int a(int i, int i2) {
        return f1043a.a(i, i2);
    }
}
