package android.support.v4.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.x;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;
import com.lody.virtual.os.VUserInfo;
import java.lang.ref.WeakReference;

@ViewPager.a
public class PagerTitleStrip extends ViewGroup {
    private static final int[] n = {16842804, 16842901, 16842904, 16842927};
    private static final int[] o = {16843660};
    private static final b q;

    /* renamed from: a  reason: collision with root package name */
    ViewPager f917a;

    /* renamed from: b  reason: collision with root package name */
    TextView f918b;

    /* renamed from: c  reason: collision with root package name */
    TextView f919c;

    /* renamed from: d  reason: collision with root package name */
    TextView f920d;

    /* renamed from: e  reason: collision with root package name */
    float f921e;

    /* renamed from: f  reason: collision with root package name */
    int f922f;

    /* renamed from: g  reason: collision with root package name */
    private int f923g;

    /* renamed from: h  reason: collision with root package name */
    private int f924h;
    private int i;
    private boolean j;
    private boolean k;
    private final a l;
    private WeakReference<z> m;
    private int p;

    interface b {
        void a(TextView textView);
    }

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            q = new d();
        } else {
            q = new c();
        }
    }

    static class c implements b {
        c() {
        }

        public void a(TextView textView) {
            textView.setSingleLine();
        }
    }

    static class d implements b {
        d() {
        }

        public void a(TextView textView) {
            PagerTitleStripIcs.a(textView);
        }
    }

    private static void setSingleLineAllCaps(TextView textView) {
        q.a(textView);
    }

    public PagerTitleStrip(Context context) {
        this(context, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PagerTitleStrip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        boolean z = false;
        this.f923g = -1;
        this.f921e = -1.0f;
        this.l = new a();
        TextView textView = new TextView(context);
        this.f918b = textView;
        addView(textView);
        TextView textView2 = new TextView(context);
        this.f919c = textView2;
        addView(textView2);
        TextView textView3 = new TextView(context);
        this.f920d = textView3;
        addView(textView3);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, n);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        if (resourceId != 0) {
            x.a(this.f918b, resourceId);
            x.a(this.f919c, resourceId);
            x.a(this.f920d, resourceId);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(1, 0);
        if (dimensionPixelSize != 0) {
            a(0, (float) dimensionPixelSize);
        }
        if (obtainStyledAttributes.hasValue(2)) {
            int color = obtainStyledAttributes.getColor(2, 0);
            this.f918b.setTextColor(color);
            this.f919c.setTextColor(color);
            this.f920d.setTextColor(color);
        }
        this.i = obtainStyledAttributes.getInteger(3, 80);
        obtainStyledAttributes.recycle();
        this.f922f = this.f919c.getTextColors().getDefaultColor();
        setNonPrimaryAlpha(0.6f);
        this.f918b.setEllipsize(TextUtils.TruncateAt.END);
        this.f919c.setEllipsize(TextUtils.TruncateAt.END);
        this.f920d.setEllipsize(TextUtils.TruncateAt.END);
        if (resourceId != 0) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(resourceId, o);
            z = obtainStyledAttributes2.getBoolean(0, false);
            obtainStyledAttributes2.recycle();
        }
        if (z) {
            setSingleLineAllCaps(this.f918b);
            setSingleLineAllCaps(this.f919c);
            setSingleLineAllCaps(this.f920d);
        } else {
            this.f918b.setSingleLine();
            this.f919c.setSingleLine();
            this.f920d.setSingleLine();
        }
        this.f924h = (int) (context.getResources().getDisplayMetrics().density * 16.0f);
    }

    public void setTextSpacing(int i2) {
        this.f924h = i2;
        requestLayout();
    }

    public int getTextSpacing() {
        return this.f924h;
    }

    public void setNonPrimaryAlpha(float f2) {
        this.p = ((int) (255.0f * f2)) & VUserInfo.FLAG_MASK_USER_TYPE;
        int i2 = (this.p << 24) | (this.f922f & 16777215);
        this.f918b.setTextColor(i2);
        this.f920d.setTextColor(i2);
    }

    public void setTextColor(int i2) {
        this.f922f = i2;
        this.f919c.setTextColor(i2);
        int i3 = (this.p << 24) | (this.f922f & 16777215);
        this.f918b.setTextColor(i3);
        this.f920d.setTextColor(i3);
    }

    public void a(int i2, float f2) {
        this.f918b.setTextSize(i2, f2);
        this.f919c.setTextSize(i2, f2);
        this.f920d.setTextSize(i2, f2);
    }

    public void setGravity(int i2) {
        this.i = i2;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (!(parent instanceof ViewPager)) {
            throw new IllegalStateException("PagerTitleStrip must be a direct child of a ViewPager.");
        }
        ViewPager viewPager = (ViewPager) parent;
        z adapter = viewPager.getAdapter();
        viewPager.c(this.l);
        viewPager.a((ViewPager.d) this.l);
        this.f917a = viewPager;
        a(this.m != null ? this.m.get() : null, adapter);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f917a != null) {
            a(this.f917a.getAdapter(), (z) null);
            this.f917a.c((ViewPager.e) null);
            this.f917a.b((ViewPager.d) this.l);
            this.f917a = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, z zVar) {
        CharSequence charSequence;
        CharSequence charSequence2;
        CharSequence charSequence3 = null;
        int b2 = zVar != null ? zVar.b() : 0;
        this.j = true;
        if (i2 < 1 || zVar == null) {
            charSequence = null;
        } else {
            charSequence = zVar.c(i2 - 1);
        }
        this.f918b.setText(charSequence);
        TextView textView = this.f919c;
        if (zVar == null || i2 >= b2) {
            charSequence2 = null;
        } else {
            charSequence2 = zVar.c(i2);
        }
        textView.setText(charSequence2);
        if (i2 + 1 < b2 && zVar != null) {
            charSequence3 = zVar.c(i2 + 1);
        }
        this.f920d.setText(charSequence3);
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(Math.max(0, (int) (((float) ((getWidth() - getPaddingLeft()) - getPaddingRight())) * 0.8f)), Integer.MIN_VALUE);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(Math.max(0, (getHeight() - getPaddingTop()) - getPaddingBottom()), Integer.MIN_VALUE);
        this.f918b.measure(makeMeasureSpec, makeMeasureSpec2);
        this.f919c.measure(makeMeasureSpec, makeMeasureSpec2);
        this.f920d.measure(makeMeasureSpec, makeMeasureSpec2);
        this.f923g = i2;
        if (!this.k) {
            a(i2, this.f921e, false);
        }
        this.j = false;
    }

    public void requestLayout() {
        if (!this.j) {
            super.requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(z zVar, z zVar2) {
        if (zVar != null) {
            zVar.b(this.l);
            this.m = null;
        }
        if (zVar2 != null) {
            zVar2.a((DataSetObserver) this.l);
            this.m = new WeakReference<>(zVar2);
        }
        if (this.f917a != null) {
            this.f923g = -1;
            this.f921e = -1.0f;
            a(this.f917a.getCurrentItem(), zVar2);
            requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, float f2, boolean z) {
        int i3;
        int i4;
        int i5;
        if (i2 != this.f923g) {
            a(i2, this.f917a.getAdapter());
        } else if (!z && f2 == this.f921e) {
            return;
        }
        this.k = true;
        int measuredWidth = this.f918b.getMeasuredWidth();
        int measuredWidth2 = this.f919c.getMeasuredWidth();
        int measuredWidth3 = this.f920d.getMeasuredWidth();
        int i6 = measuredWidth2 / 2;
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i7 = paddingRight + i6;
        int i8 = (width - (paddingLeft + i6)) - i7;
        float f3 = 0.5f + f2;
        if (f3 > 1.0f) {
            f3 -= 1.0f;
        }
        int i9 = ((width - i7) - ((int) (f3 * ((float) i8)))) - (measuredWidth2 / 2);
        int i10 = i9 + measuredWidth2;
        int baseline = this.f918b.getBaseline();
        int baseline2 = this.f919c.getBaseline();
        int baseline3 = this.f920d.getBaseline();
        int max = Math.max(Math.max(baseline, baseline2), baseline3);
        int i11 = max - baseline;
        int i12 = max - baseline2;
        int i13 = max - baseline3;
        int max2 = Math.max(Math.max(this.f918b.getMeasuredHeight() + i11, this.f919c.getMeasuredHeight() + i12), this.f920d.getMeasuredHeight() + i13);
        switch (this.i & 112) {
            case 16:
                int i14 = (((height - paddingTop) - paddingBottom) - max2) / 2;
                i3 = i14 + i11;
                i4 = i12 + i14;
                i5 = i14 + i13;
                break;
            case 80:
                int i15 = (height - paddingBottom) - max2;
                i3 = i15 + i11;
                i4 = i12 + i15;
                i5 = i15 + i13;
                break;
            default:
                i3 = paddingTop + i11;
                i4 = i12 + paddingTop;
                i5 = paddingTop + i13;
                break;
        }
        this.f919c.layout(i9, i4, i10, this.f919c.getMeasuredHeight() + i4);
        int min = Math.min(paddingLeft, (i9 - this.f924h) - measuredWidth);
        this.f918b.layout(min, i3, measuredWidth + min, this.f918b.getMeasuredHeight() + i3);
        int max3 = Math.max((width - paddingRight) - measuredWidth3, this.f924h + i10);
        this.f920d.layout(max3, i5, max3 + measuredWidth3, this.f920d.getMeasuredHeight() + i5);
        this.f921e = f2;
        this.k = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int max;
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
            throw new IllegalStateException("Must measure with an exact width");
        }
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int childMeasureSpec = getChildMeasureSpec(i3, paddingTop, -2);
        int size = View.MeasureSpec.getSize(i2);
        int childMeasureSpec2 = getChildMeasureSpec(i2, (int) (((float) size) * 0.2f), -2);
        this.f918b.measure(childMeasureSpec2, childMeasureSpec);
        this.f919c.measure(childMeasureSpec2, childMeasureSpec);
        this.f920d.measure(childMeasureSpec2, childMeasureSpec);
        if (View.MeasureSpec.getMode(i3) == 1073741824) {
            max = View.MeasureSpec.getSize(i3);
        } else {
            max = Math.max(getMinHeight(), paddingTop + this.f919c.getMeasuredHeight());
        }
        setMeasuredDimension(size, ag.a(max, i3, ag.j(this.f919c) << 16));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        float f2 = 0.0f;
        if (this.f917a != null) {
            if (this.f921e >= 0.0f) {
                f2 = this.f921e;
            }
            a(this.f923g, f2, true);
        }
    }

    /* access modifiers changed from: package-private */
    public int getMinHeight() {
        Drawable background = getBackground();
        if (background != null) {
            return background.getIntrinsicHeight();
        }
        return 0;
    }

    private class a extends DataSetObserver implements ViewPager.d, ViewPager.e {

        /* renamed from: b  reason: collision with root package name */
        private int f926b;

        a() {
        }

        public void onPageScrolled(int i, float f2, int i2) {
            if (f2 > 0.5f) {
                i++;
            }
            PagerTitleStrip.this.a(i, f2, false);
        }

        public void onPageSelected(int i) {
            float f2 = 0.0f;
            if (this.f926b == 0) {
                PagerTitleStrip.this.a(PagerTitleStrip.this.f917a.getCurrentItem(), PagerTitleStrip.this.f917a.getAdapter());
                if (PagerTitleStrip.this.f921e >= 0.0f) {
                    f2 = PagerTitleStrip.this.f921e;
                }
                PagerTitleStrip.this.a(PagerTitleStrip.this.f917a.getCurrentItem(), f2, true);
            }
        }

        public void onPageScrollStateChanged(int i) {
            this.f926b = i;
        }

        public void onAdapterChanged(ViewPager viewPager, z zVar, z zVar2) {
            PagerTitleStrip.this.a(zVar, zVar2);
        }

        public void onChanged() {
            float f2 = 0.0f;
            PagerTitleStrip.this.a(PagerTitleStrip.this.f917a.getCurrentItem(), PagerTitleStrip.this.f917a.getAdapter());
            if (PagerTitleStrip.this.f921e >= 0.0f) {
                f2 = PagerTitleStrip.this.f921e;
            }
            PagerTitleStrip.this.a(PagerTitleStrip.this.f917a.getCurrentItem(), f2, true);
        }
    }
}
