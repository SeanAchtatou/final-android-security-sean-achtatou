package android.support.v4.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.view.j;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import java.lang.reflect.Field;

@TargetApi(11)
/* compiled from: LayoutInflaterCompatHC */
class k {

    /* renamed from: a  reason: collision with root package name */
    private static Field f1047a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f1048b;

    /* compiled from: LayoutInflaterCompatHC */
    static class a extends j.a implements LayoutInflater.Factory2 {
        a(m mVar) {
            super(mVar);
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            return this.f1046a.a(view, str, context, attributeSet);
        }
    }

    static void a(LayoutInflater layoutInflater, m mVar) {
        a aVar;
        if (mVar != null) {
            aVar = new a(mVar);
        } else {
            aVar = null;
        }
        layoutInflater.setFactory2(aVar);
        LayoutInflater.Factory factory = layoutInflater.getFactory();
        if (factory instanceof LayoutInflater.Factory2) {
            a(layoutInflater, (LayoutInflater.Factory2) factory);
        } else {
            a(layoutInflater, aVar);
        }
    }

    static void a(LayoutInflater layoutInflater, LayoutInflater.Factory2 factory2) {
        if (!f1048b) {
            try {
                f1047a = LayoutInflater.class.getDeclaredField("mFactory2");
                f1047a.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.e("LayoutInflaterCompatHC", "forceSetFactory2 Could not find field 'mFactory2' on class " + LayoutInflater.class.getName() + "; inflation may have unexpected results.", e2);
            }
            f1048b = true;
        }
        if (f1047a != null) {
            try {
                f1047a.set(layoutInflater, factory2);
            } catch (IllegalAccessException e3) {
                Log.e("LayoutInflaterCompatHC", "forceSetFactory2 could not set the Factory2 on LayoutInflater " + layoutInflater + "; inflation may have unexpected results.", e3);
            }
        }
    }
}
