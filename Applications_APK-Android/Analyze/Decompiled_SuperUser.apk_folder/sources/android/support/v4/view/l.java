package android.support.v4.view;

import android.annotation.TargetApi;
import android.support.v4.view.k;
import android.view.LayoutInflater;

@TargetApi(21)
/* compiled from: LayoutInflaterCompatLollipop */
class l {
    static void a(LayoutInflater layoutInflater, m mVar) {
        layoutInflater.setFactory2(mVar != null ? new k.a(mVar) : null);
    }
}
