package android.support.v4.view.a;

import android.os.Build;
import android.view.accessibility.AccessibilityManager;

/* compiled from: AccessibilityManagerCompat */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final d f958a;

    /* compiled from: AccessibilityManagerCompat */
    interface d {
        boolean a(AccessibilityManager accessibilityManager);
    }

    @Deprecated
    /* compiled from: AccessibilityManagerCompat */
    public static abstract class e {
    }

    /* renamed from: android.support.v4.view.a.c$c  reason: collision with other inner class name */
    /* compiled from: AccessibilityManagerCompat */
    static class C0022c implements d {
        C0022c() {
        }

        public boolean a(AccessibilityManager accessibilityManager) {
            return false;
        }
    }

    /* compiled from: AccessibilityManagerCompat */
    static class a extends C0022c {
        a() {
        }

        public boolean a(AccessibilityManager accessibilityManager) {
            return d.a(accessibilityManager);
        }
    }

    /* compiled from: AccessibilityManagerCompat */
    static class b extends a {
        b() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            f958a = new b();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f958a = new a();
        } else {
            f958a = new C0022c();
        }
    }

    public static boolean a(AccessibilityManager accessibilityManager) {
        return f958a.a(accessibilityManager);
    }
}
