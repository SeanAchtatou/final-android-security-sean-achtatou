package android.support.v4.view.a;

import android.annotation.TargetApi;
import android.view.View;
import android.view.accessibility.AccessibilityRecord;

@TargetApi(16)
/* compiled from: AccessibilityRecordCompatJellyBean */
class t {
    public static void a(Object obj, View view, int i) {
        ((AccessibilityRecord) obj).setSource(view, i);
    }
}
