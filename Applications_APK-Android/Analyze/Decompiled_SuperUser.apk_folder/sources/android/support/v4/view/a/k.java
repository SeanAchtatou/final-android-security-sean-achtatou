package android.support.v4.view.a;

import android.annotation.TargetApi;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;

@TargetApi(17)
/* compiled from: AccessibilityNodeInfoCompatJellybeanMr1 */
class k {
    public static void a(Object obj, View view) {
        ((AccessibilityNodeInfo) obj).setLabelFor(view);
    }
}
