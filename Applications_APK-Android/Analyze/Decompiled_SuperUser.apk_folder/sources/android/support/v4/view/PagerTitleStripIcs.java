package android.support.v4.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.text.method.SingleLineTransformationMethod;
import android.view.View;
import android.widget.TextView;
import java.util.Locale;

@TargetApi(14)
class PagerTitleStripIcs {
    public static void a(TextView textView) {
        textView.setTransformationMethod(new SingleLineAllCapsTransform(textView.getContext()));
    }

    private static class SingleLineAllCapsTransform extends SingleLineTransformationMethod {

        /* renamed from: a  reason: collision with root package name */
        private Locale f927a;

        public SingleLineAllCapsTransform(Context context) {
            this.f927a = context.getResources().getConfiguration().locale;
        }

        public CharSequence getTransformation(CharSequence charSequence, View view) {
            CharSequence transformation = super.getTransformation(charSequence, view);
            if (transformation != null) {
                return transformation.toString().toUpperCase(this.f927a);
            }
            return null;
        }
    }
}
