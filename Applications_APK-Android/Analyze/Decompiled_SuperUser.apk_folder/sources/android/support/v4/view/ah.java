package android.support.v4.view;

import android.annotation.TargetApi;
import android.view.PointerIcon;
import android.view.View;

@TargetApi(24)
/* compiled from: ViewCompatApi24 */
class ah {
    public static void a(View view, Object obj) {
        view.setPointerIcon((PointerIcon) obj);
    }
}
