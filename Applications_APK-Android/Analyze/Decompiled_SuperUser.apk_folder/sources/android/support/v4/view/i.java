package android.support.v4.view;

import android.os.Build;
import android.view.LayoutInflater;

/* compiled from: LayoutInflaterCompat */
public final class i {

    /* renamed from: a  reason: collision with root package name */
    static final a f1045a;

    /* compiled from: LayoutInflaterCompat */
    interface a {
        m a(LayoutInflater layoutInflater);

        void a(LayoutInflater layoutInflater, m mVar);
    }

    /* compiled from: LayoutInflaterCompat */
    static class b implements a {
        b() {
        }

        public void a(LayoutInflater layoutInflater, m mVar) {
            j.a(layoutInflater, mVar);
        }

        public m a(LayoutInflater layoutInflater) {
            return j.a(layoutInflater);
        }
    }

    /* compiled from: LayoutInflaterCompat */
    static class c extends b {
        c() {
        }

        public void a(LayoutInflater layoutInflater, m mVar) {
            k.a(layoutInflater, mVar);
        }
    }

    /* compiled from: LayoutInflaterCompat */
    static class d extends c {
        d() {
        }

        public void a(LayoutInflater layoutInflater, m mVar) {
            l.a(layoutInflater, mVar);
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f1045a = new d();
        } else if (i >= 11) {
            f1045a = new c();
        } else {
            f1045a = new b();
        }
    }

    public static void a(LayoutInflater layoutInflater, m mVar) {
        f1045a.a(layoutInflater, mVar);
    }

    public static m a(LayoutInflater layoutInflater) {
        return f1045a.a(layoutInflater);
    }
}
