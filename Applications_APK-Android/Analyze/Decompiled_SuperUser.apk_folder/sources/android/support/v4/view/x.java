package android.support.v4.view;

import android.view.View;
import android.view.ViewGroup;

/* compiled from: NestedScrollingParentHelper */
public class x {

    /* renamed from: a  reason: collision with root package name */
    private final ViewGroup f1056a;

    /* renamed from: b  reason: collision with root package name */
    private int f1057b;

    public x(ViewGroup viewGroup) {
        this.f1056a = viewGroup;
    }

    public void a(View view, View view2, int i) {
        this.f1057b = i;
    }

    public int a() {
        return this.f1057b;
    }

    public void a(View view) {
        this.f1057b = 0;
    }
}
