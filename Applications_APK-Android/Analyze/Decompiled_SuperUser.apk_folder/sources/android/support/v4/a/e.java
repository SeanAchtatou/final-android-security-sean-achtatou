package android.support.v4.a;

import android.annotation.TargetApi;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

@TargetApi(9)
/* compiled from: GingerbreadAnimatorCompatProvider */
class e implements c {
    e() {
    }

    public g a() {
        return new a();
    }

    /* compiled from: GingerbreadAnimatorCompatProvider */
    private static class a implements g {

        /* renamed from: a  reason: collision with root package name */
        List<b> f218a = new ArrayList();

        /* renamed from: b  reason: collision with root package name */
        List<d> f219b = new ArrayList();

        /* renamed from: c  reason: collision with root package name */
        View f220c;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public long f221d;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public long f222e = 200;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public float f223f = 0.0f;

        /* renamed from: g  reason: collision with root package name */
        private boolean f224g = false;

        /* renamed from: h  reason: collision with root package name */
        private boolean f225h = false;
        /* access modifiers changed from: private */
        public Runnable i = new Runnable() {
            public void run() {
                float a2 = (((float) (a.this.e() - a.this.f221d)) * 1.0f) / ((float) a.this.f222e);
                if (a2 > 1.0f || a.this.f220c.getParent() == null) {
                    a2 = 1.0f;
                }
                float unused = a.this.f223f = a2;
                a.this.d();
                if (a.this.f223f >= 1.0f) {
                    a.this.g();
                } else {
                    a.this.f220c.postDelayed(a.this.i, 16);
                }
            }
        };

        /* access modifiers changed from: private */
        public void d() {
            for (int size = this.f219b.size() - 1; size >= 0; size--) {
                this.f219b.get(size).a(this);
            }
        }

        public void a(View view) {
            this.f220c = view;
        }

        public void a(b bVar) {
            this.f218a.add(bVar);
        }

        public void a(long j) {
            if (!this.f224g) {
                this.f222e = j;
            }
        }

        public void a() {
            if (!this.f224g) {
                this.f224g = true;
                f();
                this.f223f = 0.0f;
                this.f221d = e();
                this.f220c.postDelayed(this.i, 16);
            }
        }

        /* access modifiers changed from: private */
        public long e() {
            return this.f220c.getDrawingTime();
        }

        private void f() {
            for (int size = this.f218a.size() - 1; size >= 0; size--) {
                this.f218a.get(size).a(this);
            }
        }

        /* access modifiers changed from: private */
        public void g() {
            for (int size = this.f218a.size() - 1; size >= 0; size--) {
                this.f218a.get(size).b(this);
            }
        }

        private void h() {
            for (int size = this.f218a.size() - 1; size >= 0; size--) {
                this.f218a.get(size).c(this);
            }
        }

        public void b() {
            if (!this.f225h) {
                this.f225h = true;
                if (this.f224g) {
                    h();
                }
                g();
            }
        }

        public void a(d dVar) {
            this.f219b.add(dVar);
        }

        public float c() {
            return this.f223f;
        }
    }

    public void a(View view) {
    }
}
