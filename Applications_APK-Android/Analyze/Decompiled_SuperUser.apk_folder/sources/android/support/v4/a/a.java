package android.support.v4.a;

import android.os.Build;
import android.view.View;

/* compiled from: AnimatorCompatHelper */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final c f217a;

    static {
        if (Build.VERSION.SDK_INT >= 12) {
            f217a = new f();
        } else {
            f217a = new e();
        }
    }

    public static g a() {
        return f217a.a();
    }

    public static void a(View view) {
        f217a.a(view);
    }
}
