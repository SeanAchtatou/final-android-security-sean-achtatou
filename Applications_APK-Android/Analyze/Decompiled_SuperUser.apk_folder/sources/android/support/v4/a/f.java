package android.support.v4.a;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.view.View;

@TargetApi(12)
/* compiled from: HoneycombMr1AnimatorCompatProvider */
class f implements c {

    /* renamed from: a  reason: collision with root package name */
    private TimeInterpolator f227a;

    f() {
    }

    public g a() {
        return new b(ValueAnimator.ofFloat(0.0f, 1.0f));
    }

    /* compiled from: HoneycombMr1AnimatorCompatProvider */
    static class b implements g {

        /* renamed from: a  reason: collision with root package name */
        final Animator f230a;

        public b(Animator animator) {
            this.f230a = animator;
        }

        public void a(View view) {
            this.f230a.setTarget(view);
        }

        public void a(b bVar) {
            this.f230a.addListener(new a(bVar, this));
        }

        public void a(long j) {
            this.f230a.setDuration(j);
        }

        public void a() {
            this.f230a.start();
        }

        public void b() {
            this.f230a.cancel();
        }

        public void a(final d dVar) {
            if (this.f230a instanceof ValueAnimator) {
                ((ValueAnimator) this.f230a).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        dVar.a(b.this);
                    }
                });
            }
        }

        public float c() {
            return ((ValueAnimator) this.f230a).getAnimatedFraction();
        }
    }

    /* compiled from: HoneycombMr1AnimatorCompatProvider */
    static class a implements Animator.AnimatorListener {

        /* renamed from: a  reason: collision with root package name */
        final b f228a;

        /* renamed from: b  reason: collision with root package name */
        final g f229b;

        public a(b bVar, g gVar) {
            this.f228a = bVar;
            this.f229b = gVar;
        }

        public void onAnimationStart(Animator animator) {
            this.f228a.a(this.f229b);
        }

        public void onAnimationEnd(Animator animator) {
            this.f228a.b(this.f229b);
        }

        public void onAnimationCancel(Animator animator) {
            this.f228a.c(this.f229b);
        }

        public void onAnimationRepeat(Animator animator) {
            this.f228a.d(this.f229b);
        }
    }

    public void a(View view) {
        if (this.f227a == null) {
            this.f227a = new ValueAnimator().getInterpolator();
        }
        view.animate().setInterpolator(this.f227a);
    }
}
