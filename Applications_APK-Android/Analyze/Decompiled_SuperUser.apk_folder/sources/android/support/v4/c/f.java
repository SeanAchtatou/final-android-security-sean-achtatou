package android.support.v4.c;

import java.util.Locale;

/* compiled from: TextDirectionHeuristicsCompat */
public final class f {

    /* renamed from: a  reason: collision with root package name */
    public static final e f593a = new e(null, false);

    /* renamed from: b  reason: collision with root package name */
    public static final e f594b = new e(null, true);

    /* renamed from: c  reason: collision with root package name */
    public static final e f595c = new e(b.f602a, false);

    /* renamed from: d  reason: collision with root package name */
    public static final e f596d = new e(b.f602a, true);

    /* renamed from: e  reason: collision with root package name */
    public static final e f597e = new e(a.f599a, false);

    /* renamed from: f  reason: collision with root package name */
    public static final e f598f = C0015f.f605a;

    /* compiled from: TextDirectionHeuristicsCompat */
    private interface c {
        int a(CharSequence charSequence, int i, int i2);
    }

    static int a(int i) {
        switch (i) {
            case 0:
                return 1;
            case 1:
            case 2:
                return 0;
            default:
                return 2;
        }
    }

    static int b(int i) {
        switch (i) {
            case 0:
            case 14:
            case 15:
                return 1;
            case 1:
            case 2:
            case 16:
            case 17:
                return 0;
            default:
                return 2;
        }
    }

    /* compiled from: TextDirectionHeuristicsCompat */
    private static abstract class d implements e {

        /* renamed from: a  reason: collision with root package name */
        private final c f603a;

        /* access modifiers changed from: protected */
        public abstract boolean a();

        public d(c cVar) {
            this.f603a = cVar;
        }

        public boolean a(CharSequence charSequence, int i, int i2) {
            if (charSequence == null || i < 0 || i2 < 0 || charSequence.length() - i2 < i) {
                throw new IllegalArgumentException();
            } else if (this.f603a == null) {
                return a();
            } else {
                return b(charSequence, i, i2);
            }
        }

        private boolean b(CharSequence charSequence, int i, int i2) {
            switch (this.f603a.a(charSequence, i, i2)) {
                case 0:
                    return true;
                case 1:
                    return false;
                default:
                    return a();
            }
        }
    }

    /* compiled from: TextDirectionHeuristicsCompat */
    private static class e extends d {

        /* renamed from: a  reason: collision with root package name */
        private final boolean f604a;

        e(c cVar, boolean z) {
            super(cVar);
            this.f604a = z;
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return this.f604a;
        }
    }

    /* compiled from: TextDirectionHeuristicsCompat */
    private static class b implements c {

        /* renamed from: a  reason: collision with root package name */
        public static final b f602a = new b();

        public int a(CharSequence charSequence, int i, int i2) {
            int i3 = i + i2;
            int i4 = 2;
            while (i < i3 && i4 == 2) {
                i4 = f.b(Character.getDirectionality(charSequence.charAt(i)));
                i++;
            }
            return i4;
        }

        private b() {
        }
    }

    /* compiled from: TextDirectionHeuristicsCompat */
    private static class a implements c {

        /* renamed from: a  reason: collision with root package name */
        public static final a f599a = new a(true);

        /* renamed from: b  reason: collision with root package name */
        public static final a f600b = new a(false);

        /* renamed from: c  reason: collision with root package name */
        private final boolean f601c;

        public int a(CharSequence charSequence, int i, int i2) {
            int i3 = i + i2;
            boolean z = false;
            while (i < i3) {
                switch (f.a(Character.getDirectionality(charSequence.charAt(i)))) {
                    case 0:
                        if (!this.f601c) {
                            z = true;
                            break;
                        } else {
                            return 0;
                        }
                    case 1:
                        if (this.f601c) {
                            z = true;
                            break;
                        } else {
                            return 1;
                        }
                }
                i++;
            }
            if (!z) {
                return 2;
            }
            if (!this.f601c) {
                return 0;
            }
            return 1;
        }

        private a(boolean z) {
            this.f601c = z;
        }
    }

    /* renamed from: android.support.v4.c.f$f  reason: collision with other inner class name */
    /* compiled from: TextDirectionHeuristicsCompat */
    private static class C0015f extends d {

        /* renamed from: a  reason: collision with root package name */
        public static final C0015f f605a = new C0015f();

        public C0015f() {
            super(null);
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            if (g.a(Locale.getDefault()) == 1) {
                return true;
            }
            return false;
        }
    }
}
