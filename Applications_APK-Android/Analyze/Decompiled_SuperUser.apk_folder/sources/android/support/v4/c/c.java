package android.support.v4.c;

import android.annotation.TargetApi;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

@TargetApi(23)
/* compiled from: ICUCompatApi23 */
class c {

    /* renamed from: a  reason: collision with root package name */
    private static Method f590a;

    static {
        try {
            f590a = Class.forName("libcore.icu.ICU").getMethod("addLikelySubtags", Locale.class);
        } catch (Exception e2) {
            throw new IllegalStateException(e2);
        }
    }

    public static String a(Locale locale) {
        try {
            return ((Locale) f590a.invoke(null, locale)).getScript();
        } catch (InvocationTargetException e2) {
            Log.w("ICUCompatIcs", e2);
        } catch (IllegalAccessException e3) {
            Log.w("ICUCompatIcs", e3);
        }
        return locale.getScript();
    }
}
