package android.support.v4.c;

import android.os.Build;
import java.util.Locale;

/* compiled from: ICUCompat */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static final a f589a;

    /* compiled from: ICUCompat */
    interface a {
        String a(Locale locale);
    }

    /* renamed from: android.support.v4.c.b$b  reason: collision with other inner class name */
    /* compiled from: ICUCompat */
    static class C0014b implements a {
        C0014b() {
        }

        public String a(Locale locale) {
            return null;
        }
    }

    /* compiled from: ICUCompat */
    static class c implements a {
        c() {
        }

        public String a(Locale locale) {
            return d.a(locale);
        }
    }

    /* compiled from: ICUCompat */
    static class d implements a {
        d() {
        }

        public String a(Locale locale) {
            return c.a(locale);
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f589a = new d();
        } else if (i >= 14) {
            f589a = new c();
        } else {
            f589a = new C0014b();
        }
    }

    public static String a(Locale locale) {
        return f589a.a(locale);
    }
}
