package android.support.v4.c;

import android.annotation.TargetApi;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

@TargetApi(14)
/* compiled from: ICUCompatIcs */
class d {

    /* renamed from: a  reason: collision with root package name */
    private static Method f591a;

    /* renamed from: b  reason: collision with root package name */
    private static Method f592b;

    static {
        try {
            Class<?> cls = Class.forName("libcore.icu.ICU");
            if (cls != null) {
                f591a = cls.getMethod("getScript", String.class);
                f592b = cls.getMethod("addLikelySubtags", String.class);
            }
        } catch (Exception e2) {
            f591a = null;
            f592b = null;
            Log.w("ICUCompatIcs", e2);
        }
    }

    public static String a(Locale locale) {
        String b2 = b(locale);
        if (b2 != null) {
            return a(b2);
        }
        return null;
    }

    private static String a(String str) {
        try {
            if (f591a != null) {
                return (String) f591a.invoke(null, str);
            }
        } catch (IllegalAccessException e2) {
            Log.w("ICUCompatIcs", e2);
        } catch (InvocationTargetException e3) {
            Log.w("ICUCompatIcs", e3);
        }
        return null;
    }

    private static String b(Locale locale) {
        String locale2 = locale.toString();
        try {
            if (f592b != null) {
                return (String) f592b.invoke(null, locale2);
            }
        } catch (IllegalAccessException e2) {
            Log.w("ICUCompatIcs", e2);
        } catch (InvocationTargetException e3) {
            Log.w("ICUCompatIcs", e3);
        }
        return locale2;
    }
}
