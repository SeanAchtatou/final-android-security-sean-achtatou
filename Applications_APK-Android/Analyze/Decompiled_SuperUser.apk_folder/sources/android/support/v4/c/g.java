package android.support.v4.c;

import android.os.Build;
import java.util.Locale;

/* compiled from: TextUtilsCompat */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    public static final Locale f606a = new Locale("", "");

    /* renamed from: b  reason: collision with root package name */
    static String f607b = "Arab";

    /* renamed from: c  reason: collision with root package name */
    static String f608c = "Hebr";

    /* renamed from: d  reason: collision with root package name */
    private static final a f609d;

    /* compiled from: TextUtilsCompat */
    private static class a {
        a() {
        }

        public int a(Locale locale) {
            if (locale != null && !locale.equals(g.f606a)) {
                String a2 = b.a(locale);
                if (a2 == null) {
                    return b(locale);
                }
                if (a2.equalsIgnoreCase(g.f607b) || a2.equalsIgnoreCase(g.f608c)) {
                    return 1;
                }
            }
            return 0;
        }

        private static int b(Locale locale) {
            switch (Character.getDirectionality(locale.getDisplayName(locale).charAt(0))) {
                case 1:
                case 2:
                    return 1;
                default:
                    return 0;
            }
        }
    }

    /* compiled from: TextUtilsCompat */
    private static class b extends a {
        b() {
        }

        public int a(Locale locale) {
            return h.a(locale);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            f609d = new b();
        } else {
            f609d = new a();
        }
    }

    public static int a(Locale locale) {
        return f609d.a(locale);
    }
}
