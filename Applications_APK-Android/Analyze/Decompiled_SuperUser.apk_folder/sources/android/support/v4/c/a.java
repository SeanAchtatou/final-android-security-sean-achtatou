package android.support.v4.c;

import android.text.SpannableStringBuilder;
import java.util.Locale;

/* compiled from: BidiFormatter */
public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static e f572a = f.f595c;

    /* renamed from: b  reason: collision with root package name */
    private static final String f573b = Character.toString(8206);

    /* renamed from: c  reason: collision with root package name */
    private static final String f574c = Character.toString(8207);
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public static final a f575d = new a(false, 2, f572a);
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public static final a f576e = new a(true, 2, f572a);

    /* renamed from: f  reason: collision with root package name */
    private final boolean f577f;

    /* renamed from: g  reason: collision with root package name */
    private final int f578g;

    /* renamed from: h  reason: collision with root package name */
    private final e f579h;

    /* renamed from: android.support.v4.c.a$a  reason: collision with other inner class name */
    /* compiled from: BidiFormatter */
    public static final class C0013a {

        /* renamed from: a  reason: collision with root package name */
        private boolean f580a;

        /* renamed from: b  reason: collision with root package name */
        private int f581b;

        /* renamed from: c  reason: collision with root package name */
        private e f582c;

        public C0013a() {
            a(a.b(Locale.getDefault()));
        }

        private void a(boolean z) {
            this.f580a = z;
            this.f582c = a.f572a;
            this.f581b = 2;
        }

        private static a b(boolean z) {
            return z ? a.f576e : a.f575d;
        }

        public a a() {
            if (this.f581b == 2 && this.f582c == a.f572a) {
                return b(this.f580a);
            }
            return new a(this.f580a, this.f581b, this.f582c);
        }
    }

    public static a a() {
        return new C0013a().a();
    }

    private a(boolean z, int i, e eVar) {
        this.f577f = z;
        this.f578g = i;
        this.f579h = eVar;
    }

    public boolean b() {
        return (this.f578g & 2) != 0;
    }

    private String a(CharSequence charSequence, e eVar) {
        boolean a2 = eVar.a(charSequence, 0, charSequence.length());
        if (!this.f577f && (a2 || b(charSequence) == 1)) {
            return f573b;
        }
        if (!this.f577f || (a2 && b(charSequence) != -1)) {
            return "";
        }
        return f574c;
    }

    private String b(CharSequence charSequence, e eVar) {
        boolean a2 = eVar.a(charSequence, 0, charSequence.length());
        if (!this.f577f && (a2 || c(charSequence) == 1)) {
            return f573b;
        }
        if (!this.f577f || (a2 && c(charSequence) != -1)) {
            return "";
        }
        return f574c;
    }

    public CharSequence a(CharSequence charSequence, e eVar, boolean z) {
        if (charSequence == null) {
            return null;
        }
        boolean a2 = eVar.a(charSequence, 0, charSequence.length());
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (b() && z) {
            spannableStringBuilder.append((CharSequence) b(charSequence, a2 ? f.f594b : f.f593a));
        }
        if (a2 != this.f577f) {
            spannableStringBuilder.append(a2 ? (char) 8235 : 8234);
            spannableStringBuilder.append(charSequence);
            spannableStringBuilder.append(8236);
        } else {
            spannableStringBuilder.append(charSequence);
        }
        if (z) {
            spannableStringBuilder.append((CharSequence) a(charSequence, a2 ? f.f594b : f.f593a));
        }
        return spannableStringBuilder;
    }

    public CharSequence a(CharSequence charSequence) {
        return a(charSequence, this.f579h, true);
    }

    /* access modifiers changed from: private */
    public static boolean b(Locale locale) {
        return g.a(locale) == 1;
    }

    private static int b(CharSequence charSequence) {
        return new b(charSequence, false).b();
    }

    private static int c(CharSequence charSequence) {
        return new b(charSequence, false).a();
    }

    /* compiled from: BidiFormatter */
    private static class b {

        /* renamed from: a  reason: collision with root package name */
        private static final byte[] f583a = new byte[1792];

        /* renamed from: b  reason: collision with root package name */
        private final CharSequence f584b;

        /* renamed from: c  reason: collision with root package name */
        private final boolean f585c;

        /* renamed from: d  reason: collision with root package name */
        private final int f586d;

        /* renamed from: e  reason: collision with root package name */
        private int f587e;

        /* renamed from: f  reason: collision with root package name */
        private char f588f;

        static {
            for (int i = 0; i < 1792; i++) {
                f583a[i] = Character.getDirectionality(i);
            }
        }

        b(CharSequence charSequence, boolean z) {
            this.f584b = charSequence;
            this.f585c = z;
            this.f586d = charSequence.length();
        }

        /* access modifiers changed from: package-private */
        public int a() {
            this.f587e = 0;
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (this.f587e < this.f586d && i == 0) {
                switch (c()) {
                    case 0:
                        if (i3 != 0) {
                            i = i3;
                            break;
                        } else {
                            return -1;
                        }
                    case 1:
                    case 2:
                        if (i3 != 0) {
                            i = i3;
                            break;
                        } else {
                            return 1;
                        }
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    default:
                        i = i3;
                        break;
                    case 9:
                        break;
                    case 14:
                    case 15:
                        i3++;
                        i2 = -1;
                        break;
                    case 16:
                    case 17:
                        i3++;
                        i2 = 1;
                        break;
                    case 18:
                        i3--;
                        i2 = 0;
                        break;
                }
            }
            if (i == 0) {
                return 0;
            }
            if (i2 != 0) {
                return i2;
            }
            while (this.f587e > 0) {
                switch (d()) {
                    case 14:
                    case 15:
                        if (i != i3) {
                            i3--;
                            break;
                        } else {
                            return -1;
                        }
                    case 16:
                    case 17:
                        if (i != i3) {
                            i3--;
                            break;
                        } else {
                            return 1;
                        }
                    case 18:
                        i3++;
                        break;
                }
            }
            return 0;
        }

        /* access modifiers changed from: package-private */
        public int b() {
            this.f587e = this.f586d;
            int i = 0;
            int i2 = 0;
            while (this.f587e > 0) {
                switch (d()) {
                    case 0:
                        if (i2 != 0) {
                            if (i != 0) {
                                break;
                            } else {
                                i = i2;
                                break;
                            }
                        } else {
                            return -1;
                        }
                    case 1:
                    case 2:
                        if (i2 != 0) {
                            if (i != 0) {
                                break;
                            } else {
                                i = i2;
                                break;
                            }
                        } else {
                            return 1;
                        }
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    default:
                        if (i != 0) {
                            break;
                        } else {
                            i = i2;
                            break;
                        }
                    case 9:
                        break;
                    case 14:
                    case 15:
                        if (i != i2) {
                            i2--;
                            break;
                        } else {
                            return -1;
                        }
                    case 16:
                    case 17:
                        if (i != i2) {
                            i2--;
                            break;
                        } else {
                            return 1;
                        }
                    case 18:
                        i2++;
                        break;
                }
            }
            return 0;
        }

        private static byte a(char c2) {
            return c2 < 1792 ? f583a[c2] : Character.getDirectionality(c2);
        }

        /* access modifiers changed from: package-private */
        public byte c() {
            this.f588f = this.f584b.charAt(this.f587e);
            if (Character.isHighSurrogate(this.f588f)) {
                int codePointAt = Character.codePointAt(this.f584b, this.f587e);
                this.f587e += Character.charCount(codePointAt);
                return Character.getDirectionality(codePointAt);
            }
            this.f587e++;
            byte a2 = a(this.f588f);
            if (!this.f585c) {
                return a2;
            }
            if (this.f588f == '<') {
                return e();
            }
            if (this.f588f == '&') {
                return g();
            }
            return a2;
        }

        /* access modifiers changed from: package-private */
        public byte d() {
            this.f588f = this.f584b.charAt(this.f587e - 1);
            if (Character.isLowSurrogate(this.f588f)) {
                int codePointBefore = Character.codePointBefore(this.f584b, this.f587e);
                this.f587e -= Character.charCount(codePointBefore);
                return Character.getDirectionality(codePointBefore);
            }
            this.f587e--;
            byte a2 = a(this.f588f);
            if (!this.f585c) {
                return a2;
            }
            if (this.f588f == '>') {
                return f();
            }
            if (this.f588f == ';') {
                return h();
            }
            return a2;
        }

        private byte e() {
            int i = this.f587e;
            while (this.f587e < this.f586d) {
                CharSequence charSequence = this.f584b;
                int i2 = this.f587e;
                this.f587e = i2 + 1;
                this.f588f = charSequence.charAt(i2);
                if (this.f588f == '>') {
                    return 12;
                }
                if (this.f588f == '\"' || this.f588f == '\'') {
                    char c2 = this.f588f;
                    while (this.f587e < this.f586d) {
                        CharSequence charSequence2 = this.f584b;
                        int i3 = this.f587e;
                        this.f587e = i3 + 1;
                        char charAt = charSequence2.charAt(i3);
                        this.f588f = charAt;
                        if (charAt == c2) {
                            break;
                        }
                    }
                }
            }
            this.f587e = i;
            this.f588f = '<';
            return 13;
        }

        private byte f() {
            int i = this.f587e;
            while (this.f587e > 0) {
                CharSequence charSequence = this.f584b;
                int i2 = this.f587e - 1;
                this.f587e = i2;
                this.f588f = charSequence.charAt(i2);
                if (this.f588f == '<') {
                    return 12;
                }
                if (this.f588f == '>') {
                    break;
                } else if (this.f588f == '\"' || this.f588f == '\'') {
                    char c2 = this.f588f;
                    while (this.f587e > 0) {
                        CharSequence charSequence2 = this.f584b;
                        int i3 = this.f587e - 1;
                        this.f587e = i3;
                        char charAt = charSequence2.charAt(i3);
                        this.f588f = charAt;
                        if (charAt == c2) {
                            break;
                        }
                    }
                }
            }
            this.f587e = i;
            this.f588f = '>';
            return 13;
        }

        private byte g() {
            while (this.f587e < this.f586d) {
                CharSequence charSequence = this.f584b;
                int i = this.f587e;
                this.f587e = i + 1;
                char charAt = charSequence.charAt(i);
                this.f588f = charAt;
                if (charAt == ';') {
                    return 12;
                }
            }
            return 12;
        }

        private byte h() {
            int i = this.f587e;
            while (this.f587e > 0) {
                CharSequence charSequence = this.f584b;
                int i2 = this.f587e - 1;
                this.f587e = i2;
                this.f588f = charSequence.charAt(i2);
                if (this.f588f != '&') {
                    if (this.f588f == ';') {
                        break;
                    }
                } else {
                    return 12;
                }
            }
            this.f587e = i;
            this.f588f = ';';
            return 13;
        }
    }
}
