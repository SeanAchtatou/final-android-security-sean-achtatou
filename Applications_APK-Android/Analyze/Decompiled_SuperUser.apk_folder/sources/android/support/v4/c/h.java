package android.support.v4.c;

import android.annotation.TargetApi;
import android.text.TextUtils;
import java.util.Locale;

@TargetApi(17)
/* compiled from: TextUtilsCompatJellybeanMr1 */
class h {
    public static int a(Locale locale) {
        return TextUtils.getLayoutDirectionFromLocale(locale);
    }
}
