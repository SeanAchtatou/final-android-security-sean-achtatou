package android.support.v4.print;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.BitmapFactory;

@TargetApi(19)
/* compiled from: PrintHelperKitkat */
class d {

    /* renamed from: a  reason: collision with root package name */
    final Context f834a;

    /* renamed from: b  reason: collision with root package name */
    BitmapFactory.Options f835b = null;

    /* renamed from: c  reason: collision with root package name */
    protected boolean f836c = true;

    /* renamed from: d  reason: collision with root package name */
    protected boolean f837d = true;

    /* renamed from: e  reason: collision with root package name */
    int f838e = 2;

    /* renamed from: f  reason: collision with root package name */
    int f839f = 2;

    /* renamed from: g  reason: collision with root package name */
    private final Object f840g = new Object();

    d(Context context) {
        this.f834a = context;
    }
}
