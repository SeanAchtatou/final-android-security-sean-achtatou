package android.support.v4.print;

import android.content.Context;
import android.os.Build;

public final class PrintHelper {

    /* renamed from: a  reason: collision with root package name */
    g f829a;

    interface g {
    }

    public static boolean a() {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return false;
    }

    private static final class f implements g {

        /* renamed from: a  reason: collision with root package name */
        int f831a;

        /* renamed from: b  reason: collision with root package name */
        int f832b;

        /* renamed from: c  reason: collision with root package name */
        int f833c;

        private f() {
            this.f831a = 2;
            this.f832b = 2;
            this.f833c = 1;
        }
    }

    private static class d<RealHelper extends d> implements g {

        /* renamed from: a  reason: collision with root package name */
        private final RealHelper f830a;

        protected d(RealHelper realhelper) {
            this.f830a = realhelper;
        }
    }

    private static final class e extends d<d> {
        e(Context context) {
            super(new d(context));
        }
    }

    private static final class a extends d<a> {
        a(Context context) {
            super(new a(context));
        }
    }

    private static final class b extends d<b> {
        b(Context context) {
            super(new b(context));
        }
    }

    private static final class c extends d<c> {
        c(Context context) {
            super(new c(context));
        }
    }

    public PrintHelper(Context context) {
        if (!a()) {
            this.f829a = new f();
        } else if (Build.VERSION.SDK_INT >= 24) {
            this.f829a = new c(context);
        } else if (Build.VERSION.SDK_INT >= 23) {
            this.f829a = new b(context);
        } else if (Build.VERSION.SDK_INT >= 20) {
            this.f829a = new a(context);
        } else {
            this.f829a = new e(context);
        }
    }
}
