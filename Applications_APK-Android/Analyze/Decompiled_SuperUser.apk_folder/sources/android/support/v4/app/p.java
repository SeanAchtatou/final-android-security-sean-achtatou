package android.support.v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.support.v4.util.i;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: FragmentController */
public class p {

    /* renamed from: a  reason: collision with root package name */
    private final FragmentHostCallback<?> f465a;

    public static final p a(FragmentHostCallback<?> fragmentHostCallback) {
        return new p(fragmentHostCallback);
    }

    private p(FragmentHostCallback<?> fragmentHostCallback) {
        this.f465a = fragmentHostCallback;
    }

    public q a() {
        return this.f465a.k();
    }

    public Fragment a(String str) {
        return this.f465a.f282d.b(str);
    }

    public void a(Fragment fragment) {
        this.f465a.f282d.a(this.f465a, this.f465a, fragment);
    }

    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        return this.f465a.f282d.a(view, str, context, attributeSet);
    }

    public void b() {
        this.f465a.f282d.j();
    }

    public Parcelable c() {
        return this.f465a.f282d.i();
    }

    public void a(Parcelable parcelable, s sVar) {
        this.f465a.f282d.a(parcelable, sVar);
    }

    public s d() {
        return this.f465a.f282d.h();
    }

    public void e() {
        this.f465a.f282d.k();
    }

    public void f() {
        this.f465a.f282d.l();
    }

    public void g() {
        this.f465a.f282d.m();
    }

    public void h() {
        this.f465a.f282d.n();
    }

    public void i() {
        this.f465a.f282d.o();
    }

    public void j() {
        this.f465a.f282d.p();
    }

    public void k() {
        this.f465a.f282d.q();
    }

    public void l() {
        this.f465a.f282d.s();
    }

    public void a(boolean z) {
        this.f465a.f282d.a(z);
    }

    public void b(boolean z) {
        this.f465a.f282d.b(z);
    }

    public void a(Configuration configuration) {
        this.f465a.f282d.a(configuration);
    }

    public void m() {
        this.f465a.f282d.t();
    }

    public boolean a(Menu menu, MenuInflater menuInflater) {
        return this.f465a.f282d.a(menu, menuInflater);
    }

    public boolean a(Menu menu) {
        return this.f465a.f282d.a(menu);
    }

    public boolean a(MenuItem menuItem) {
        return this.f465a.f282d.a(menuItem);
    }

    public boolean b(MenuItem menuItem) {
        return this.f465a.f282d.b(menuItem);
    }

    public void b(Menu menu) {
        this.f465a.f282d.b(menu);
    }

    public boolean n() {
        return this.f465a.f282d.e();
    }

    public void o() {
        this.f465a.m();
    }

    public void c(boolean z) {
        this.f465a.a(z);
    }

    public void p() {
        this.f465a.n();
    }

    public void q() {
        this.f465a.o();
    }

    public i<String, w> r() {
        return this.f465a.p();
    }

    public void a(i<String, w> iVar) {
        this.f465a.a(iVar);
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.f465a.b(str, fileDescriptor, printWriter, strArr);
    }
}
