package android.support.v4.app;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/* compiled from: Fragment */
final class FragmentState implements Parcelable {
    public static final Parcelable.Creator<FragmentState> CREATOR = new Parcelable.Creator<FragmentState>() {
        /* renamed from: a */
        public FragmentState createFromParcel(Parcel parcel) {
            return new FragmentState(parcel);
        }

        /* renamed from: a */
        public FragmentState[] newArray(int i) {
            return new FragmentState[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final String f293a;

    /* renamed from: b  reason: collision with root package name */
    final int f294b;

    /* renamed from: c  reason: collision with root package name */
    final boolean f295c;

    /* renamed from: d  reason: collision with root package name */
    final int f296d;

    /* renamed from: e  reason: collision with root package name */
    final int f297e;

    /* renamed from: f  reason: collision with root package name */
    final String f298f;

    /* renamed from: g  reason: collision with root package name */
    final boolean f299g;

    /* renamed from: h  reason: collision with root package name */
    final boolean f300h;
    final Bundle i;
    final boolean j;
    Bundle k;
    Fragment l;

    public FragmentState(Fragment fragment) {
        this.f293a = fragment.getClass().getName();
        this.f294b = fragment.mIndex;
        this.f295c = fragment.mFromLayout;
        this.f296d = fragment.mFragmentId;
        this.f297e = fragment.mContainerId;
        this.f298f = fragment.mTag;
        this.f299g = fragment.mRetainInstance;
        this.f300h = fragment.mDetached;
        this.i = fragment.mArguments;
        this.j = fragment.mHidden;
    }

    public FragmentState(Parcel parcel) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        this.f293a = parcel.readString();
        this.f294b = parcel.readInt();
        this.f295c = parcel.readInt() != 0;
        this.f296d = parcel.readInt();
        this.f297e = parcel.readInt();
        this.f298f = parcel.readString();
        if (parcel.readInt() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.f299g = z;
        if (parcel.readInt() != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.f300h = z2;
        this.i = parcel.readBundle();
        this.j = parcel.readInt() == 0 ? false : z3;
        this.k = parcel.readBundle();
    }

    public Fragment a(FragmentHostCallback fragmentHostCallback, Fragment fragment, s sVar) {
        if (this.l == null) {
            Context i2 = fragmentHostCallback.i();
            if (this.i != null) {
                this.i.setClassLoader(i2.getClassLoader());
            }
            this.l = Fragment.instantiate(i2, this.f293a, this.i);
            if (this.k != null) {
                this.k.setClassLoader(i2.getClassLoader());
                this.l.mSavedFragmentState = this.k;
            }
            this.l.setIndex(this.f294b, fragment);
            this.l.mFromLayout = this.f295c;
            this.l.mRestored = true;
            this.l.mFragmentId = this.f296d;
            this.l.mContainerId = this.f297e;
            this.l.mTag = this.f298f;
            this.l.mRetainInstance = this.f299g;
            this.l.mDetached = this.f300h;
            this.l.mHidden = this.j;
            this.l.mFragmentManager = fragmentHostCallback.f282d;
            if (r.f466a) {
                Log.v("FragmentManager", "Instantiated fragment " + this.l);
            }
        }
        this.l.mChildNonConfig = sVar;
        return this.l;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3;
        int i4;
        int i5 = 1;
        parcel.writeString(this.f293a);
        parcel.writeInt(this.f294b);
        parcel.writeInt(this.f295c ? 1 : 0);
        parcel.writeInt(this.f296d);
        parcel.writeInt(this.f297e);
        parcel.writeString(this.f298f);
        if (this.f299g) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        parcel.writeInt(i3);
        if (this.f300h) {
            i4 = 1;
        } else {
            i4 = 0;
        }
        parcel.writeInt(i4);
        parcel.writeBundle(this.i);
        if (!this.j) {
            i5 = 0;
        }
        parcel.writeInt(i5);
        parcel.writeBundle(this.k);
    }
}
