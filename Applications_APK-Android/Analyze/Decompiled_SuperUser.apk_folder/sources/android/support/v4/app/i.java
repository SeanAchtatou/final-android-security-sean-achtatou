package android.support.v4.app;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

@TargetApi(9)
/* compiled from: BaseFragmentActivityGingerbread */
abstract class i extends SupportActivity {

    /* renamed from: a  reason: collision with root package name */
    boolean f461a;

    /* access modifiers changed from: package-private */
    public abstract View a(View view, String str, Context context, AttributeSet attributeSet);

    i() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (Build.VERSION.SDK_INT < 11 && getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(bundle);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        View a2 = a(null, str, context, attributeSet);
        if (a2 == null) {
            return super.onCreateView(str, context, attributeSet);
        }
        return a2;
    }

    public void startIntentSenderForResult(IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4) {
        if (!this.f461a && i != -1) {
            b(i);
        }
        super.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4);
    }

    static void b(int i) {
        if ((-65536 & i) != 0) {
            throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
        }
    }
}
