package android.support.v4.app;

import java.util.List;

/* compiled from: FragmentManagerNonConfig */
public class s {

    /* renamed from: a  reason: collision with root package name */
    private final List<Fragment> f489a;

    /* renamed from: b  reason: collision with root package name */
    private final List<s> f490b;

    s(List<Fragment> list, List<s> list2) {
        this.f489a = list;
        this.f490b = list2;
    }

    /* access modifiers changed from: package-private */
    public List<Fragment> a() {
        return this.f489a;
    }

    /* access modifiers changed from: package-private */
    public List<s> b() {
        return this.f490b;
    }
}
