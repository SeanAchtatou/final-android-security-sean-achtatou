package android.support.v4.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.support.v4.app.c;

@TargetApi(23)
class ActivityCompatApi23 {

    public static abstract class SharedElementCallback23 extends c.a {
    }

    public interface a {
        void a(int i);
    }

    public static void a(Activity activity, String[] strArr, int i) {
        if (activity instanceof a) {
            ((a) activity).a(i);
        }
        activity.requestPermissions(strArr, i);
    }

    public static boolean a(Activity activity, String str) {
        return activity.shouldShowRequestPermissionRationale(str);
    }
}
