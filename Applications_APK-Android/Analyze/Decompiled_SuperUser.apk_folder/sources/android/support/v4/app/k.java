package android.support.v4.app;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;

/* compiled from: BaseFragmentActivityJB */
abstract class k extends j {

    /* renamed from: b  reason: collision with root package name */
    boolean f462b;

    k() {
    }

    public void startActivityForResult(Intent intent, int i, Bundle bundle) {
        if (!this.f462b && i != -1) {
            b(i);
        }
        super.startActivityForResult(intent, i, bundle);
    }

    public void startIntentSenderForResult(IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) {
        if (!this.f461a && i != -1) {
            b(i);
        }
        super.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4, bundle);
    }
}
