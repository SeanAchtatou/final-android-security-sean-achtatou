package android.support.v4.app;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ListFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    ListAdapter f315a;

    /* renamed from: b  reason: collision with root package name */
    ListView f316b;

    /* renamed from: c  reason: collision with root package name */
    View f317c;

    /* renamed from: d  reason: collision with root package name */
    TextView f318d;

    /* renamed from: e  reason: collision with root package name */
    View f319e;

    /* renamed from: f  reason: collision with root package name */
    View f320f;

    /* renamed from: g  reason: collision with root package name */
    CharSequence f321g;

    /* renamed from: h  reason: collision with root package name */
    boolean f322h;
    private final Handler i = new Handler();
    private final Runnable j = new Runnable() {
        public void run() {
            ListFragment.this.f316b.focusableViewAvailable(ListFragment.this.f316b);
        }
    };
    private final AdapterView.OnItemClickListener k = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            ListFragment.this.a((ListView) adapterView, view, i, j);
        }
    };

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Context context = getContext();
        FrameLayout frameLayout = new FrameLayout(context);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setId(16711682);
        linearLayout.setOrientation(1);
        linearLayout.setVisibility(8);
        linearLayout.setGravity(17);
        linearLayout.addView(new ProgressBar(context, null, 16842874), new FrameLayout.LayoutParams(-2, -2));
        frameLayout.addView(linearLayout, new FrameLayout.LayoutParams(-1, -1));
        FrameLayout frameLayout2 = new FrameLayout(context);
        frameLayout2.setId(16711683);
        TextView textView = new TextView(context);
        textView.setId(16711681);
        textView.setGravity(17);
        frameLayout2.addView(textView, new FrameLayout.LayoutParams(-1, -1));
        ListView listView = new ListView(context);
        listView.setId(16908298);
        listView.setDrawSelectorOnTop(false);
        frameLayout2.addView(listView, new FrameLayout.LayoutParams(-1, -1));
        frameLayout.addView(frameLayout2, new FrameLayout.LayoutParams(-1, -1));
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        return frameLayout;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        a();
    }

    public void onDestroyView() {
        this.i.removeCallbacks(this.j);
        this.f316b = null;
        this.f322h = false;
        this.f320f = null;
        this.f319e = null;
        this.f317c = null;
        this.f318d = null;
        super.onDestroyView();
    }

    public void a(ListView listView, View view, int i2, long j2) {
    }

    public void a(ListAdapter listAdapter) {
        boolean z = false;
        boolean z2 = this.f315a != null;
        this.f315a = listAdapter;
        if (this.f316b != null) {
            this.f316b.setAdapter(listAdapter);
            if (!this.f322h && !z2) {
                if (getView().getWindowToken() != null) {
                    z = true;
                }
                a(true, z);
            }
        }
    }

    private void a(boolean z, boolean z2) {
        a();
        if (this.f319e == null) {
            throw new IllegalStateException("Can't be used with a custom content view");
        } else if (this.f322h != z) {
            this.f322h = z;
            if (z) {
                if (z2) {
                    this.f319e.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432577));
                    this.f320f.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432576));
                } else {
                    this.f319e.clearAnimation();
                    this.f320f.clearAnimation();
                }
                this.f319e.setVisibility(8);
                this.f320f.setVisibility(0);
                return;
            }
            if (z2) {
                this.f319e.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432576));
                this.f320f.startAnimation(AnimationUtils.loadAnimation(getContext(), 17432577));
            } else {
                this.f319e.clearAnimation();
                this.f320f.clearAnimation();
            }
            this.f319e.setVisibility(0);
            this.f320f.setVisibility(8);
        }
    }

    private void a() {
        if (this.f316b == null) {
            View view = getView();
            if (view == null) {
                throw new IllegalStateException("Content view not yet created");
            }
            if (view instanceof ListView) {
                this.f316b = (ListView) view;
            } else {
                this.f318d = (TextView) view.findViewById(16711681);
                if (this.f318d == null) {
                    this.f317c = view.findViewById(16908292);
                } else {
                    this.f318d.setVisibility(8);
                }
                this.f319e = view.findViewById(16711682);
                this.f320f = view.findViewById(16711683);
                View findViewById = view.findViewById(16908298);
                if (findViewById instanceof ListView) {
                    this.f316b = (ListView) findViewById;
                    if (this.f317c != null) {
                        this.f316b.setEmptyView(this.f317c);
                    } else if (this.f321g != null) {
                        this.f318d.setText(this.f321g);
                        this.f316b.setEmptyView(this.f318d);
                    }
                } else if (findViewById == null) {
                    throw new RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
                } else {
                    throw new RuntimeException("Content has view with id attribute 'android.R.id.list' that is not a ListView class");
                }
            }
            this.f322h = true;
            this.f316b.setOnItemClickListener(this.k);
            if (this.f315a != null) {
                ListAdapter listAdapter = this.f315a;
                this.f315a = null;
                a(listAdapter);
            } else if (this.f319e != null) {
                a(false, false);
            }
            this.i.post(this.j);
        }
    }
}
