package android.support.v4.app;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ac;
import android.widget.RemoteViews;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.ArrayList;
import java.util.Iterator;

@TargetApi(21)
class NotificationCompatApi21 {

    public static class Builder implements aa, ab {

        /* renamed from: a  reason: collision with root package name */
        private Notification.Builder f376a;

        /* renamed from: b  reason: collision with root package name */
        private Bundle f377b;

        /* renamed from: c  reason: collision with root package name */
        private RemoteViews f378c;

        /* renamed from: d  reason: collision with root package name */
        private RemoteViews f379d;

        /* renamed from: e  reason: collision with root package name */
        private RemoteViews f380e;

        public Builder(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, String str, ArrayList<String> arrayList, Bundle bundle, int i5, int i6, Notification notification2, String str2, boolean z5, String str3, RemoteViews remoteViews2, RemoteViews remoteViews3, RemoteViews remoteViews4) {
            this.f376a = new Notification.Builder(context).setWhen(notification.when).setShowWhen(z2).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, remoteViews).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(pendingIntent2, (notification.flags & FileUtils.FileMode.MODE_IWUSR) != 0).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z3).setPriority(i4).setProgress(i2, i3, z).setLocalOnly(z4).setGroup(str2).setGroupSummary(z5).setSortKey(str3).setCategory(str).setColor(i5).setVisibility(i6).setPublicVersion(notification2);
            this.f377b = new Bundle();
            if (bundle != null) {
                this.f377b.putAll(bundle);
            }
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                this.f376a.addPerson(it.next());
            }
            this.f378c = remoteViews2;
            this.f379d = remoteViews3;
            this.f380e = remoteViews4;
        }

        public void a(ac.a aVar) {
            NotificationCompatApi20.a(this.f376a, aVar);
        }

        public Notification.Builder a() {
            return this.f376a;
        }

        public Notification b() {
            this.f376a.setExtras(this.f377b);
            Notification build = this.f376a.build();
            if (this.f378c != null) {
                build.contentView = this.f378c;
            }
            if (this.f379d != null) {
                build.bigContentView = this.f379d;
            }
            if (this.f380e != null) {
                build.headsUpContentView = this.f380e;
            }
            return build;
        }
    }
}
