package android.support.v4.app;

import android.content.Context;
import android.os.Build;

/* compiled from: AppOpsManagerCompat */
public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static final b f446a;

    /* compiled from: AppOpsManagerCompat */
    private static class b {
        b() {
        }

        public String a(String str) {
            return null;
        }

        public int a(Context context, String str, int i, String str2) {
            return 1;
        }

        public int a(Context context, String str, String str2) {
            return 1;
        }
    }

    /* compiled from: AppOpsManagerCompat */
    private static class a extends b {
        a() {
        }

        public String a(String str) {
            return g.a(str);
        }

        public int a(Context context, String str, int i, String str2) {
            return g.a(context, str, i, str2);
        }

        public int a(Context context, String str, String str2) {
            return g.a(context, str, str2);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 23) {
            f446a = new a();
        } else {
            f446a = new b();
        }
    }

    public static String a(String str) {
        return f446a.a(str);
    }

    public static int a(Context context, String str, int i, String str2) {
        return f446a.a(context, str, i, str2);
    }

    public static int a(Context context, String str, String str2) {
        return f446a.a(context, str, str2);
    }
}
