package android.support.v4.app;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

/* compiled from: BaseFragmentActivityHoneycomb */
abstract class j extends i {
    j() {
    }

    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View a2 = a(view, str, context, attributeSet);
        if (a2 != null || Build.VERSION.SDK_INT < 11) {
            return a2;
        }
        return super.onCreateView(view, str, context, attributeSet);
    }
}
