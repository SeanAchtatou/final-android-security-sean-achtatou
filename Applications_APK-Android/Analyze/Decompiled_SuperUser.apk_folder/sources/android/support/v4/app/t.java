package android.support.v4.app;

/* compiled from: FragmentTransaction */
public abstract class t {
    public abstract t a();

    public abstract t a(int i, Fragment fragment, String str);

    public abstract t a(Fragment fragment);

    public abstract t a(Fragment fragment, String str);

    public abstract int b();

    public abstract t b(Fragment fragment);

    public abstract int c();

    public abstract t c(Fragment fragment);

    public abstract void d();

    public abstract boolean h();
}
