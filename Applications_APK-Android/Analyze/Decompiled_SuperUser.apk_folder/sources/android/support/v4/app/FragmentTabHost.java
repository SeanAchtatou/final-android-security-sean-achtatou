package android.support.v4.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TabHost;
import java.util.ArrayList;

public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<a> f301a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    private Context f302b;

    /* renamed from: c  reason: collision with root package name */
    private q f303c;

    /* renamed from: d  reason: collision with root package name */
    private int f304d;

    /* renamed from: e  reason: collision with root package name */
    private TabHost.OnTabChangeListener f305e;

    /* renamed from: f  reason: collision with root package name */
    private a f306f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f307g;

    static final class a {

        /* renamed from: a  reason: collision with root package name */
        final String f310a;

        /* renamed from: b  reason: collision with root package name */
        final Class<?> f311b;

        /* renamed from: c  reason: collision with root package name */
        final Bundle f312c;

        /* renamed from: d  reason: collision with root package name */
        Fragment f313d;
    }

    static class DummyTabFactory implements TabHost.TabContentFactory {

        /* renamed from: a  reason: collision with root package name */
        private final Context f308a;

        public DummyTabFactory(Context context) {
            this.f308a = context;
        }

        public View createTabContent(String str) {
            View view = new View(this.f308a);
            view.setMinimumWidth(0);
            view.setMinimumHeight(0);
            return view;
        }
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        String f309a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        SavedState(Parcel parcel) {
            super(parcel);
            this.f309a = parcel.readString();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.f309a);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.f309a + "}";
        }
    }

    public FragmentTabHost(Context context) {
        super(context, null);
        a(context, (AttributeSet) null);
    }

    public FragmentTabHost(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842995}, 0, 0);
        this.f304d = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        super.setOnTabChangedListener(this);
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.f305e = onTabChangeListener;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        t tVar = null;
        int size = this.f301a.size();
        for (int i = 0; i < size; i++) {
            a aVar = this.f301a.get(i);
            aVar.f313d = this.f303c.a(aVar.f310a);
            if (aVar.f313d != null && !aVar.f313d.isDetached()) {
                if (aVar.f310a.equals(currentTabTag)) {
                    this.f306f = aVar;
                } else {
                    if (tVar == null) {
                        tVar = this.f303c.a();
                    }
                    tVar.b(aVar.f313d);
                }
            }
        }
        this.f307g = true;
        t a2 = a(currentTabTag, tVar);
        if (a2 != null) {
            a2.b();
            this.f303c.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f307g = false;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f309a = getCurrentTabTag();
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.f309a);
    }

    public void onTabChanged(String str) {
        t a2;
        if (this.f307g && (a2 = a(str, (t) null)) != null) {
            a2.b();
        }
        if (this.f305e != null) {
            this.f305e.onTabChanged(str);
        }
    }

    private t a(String str, t tVar) {
        a a2 = a(str);
        if (this.f306f != a2) {
            if (tVar == null) {
                tVar = this.f303c.a();
            }
            if (!(this.f306f == null || this.f306f.f313d == null)) {
                tVar.b(this.f306f.f313d);
            }
            if (a2 != null) {
                if (a2.f313d == null) {
                    a2.f313d = Fragment.instantiate(this.f302b, a2.f311b.getName(), a2.f312c);
                    tVar.a(this.f304d, a2.f313d, a2.f310a);
                } else {
                    tVar.c(a2.f313d);
                }
            }
            this.f306f = a2;
        }
        return tVar;
    }

    private a a(String str) {
        int size = this.f301a.size();
        for (int i = 0; i < size; i++) {
            a aVar = this.f301a.get(i);
            if (aVar.f310a.equals(str)) {
                return aVar;
            }
        }
        return null;
    }
}
