package android.support.v4.app;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.lang.reflect.Method;

@TargetApi(11)
/* compiled from: ActionBarDrawerToggleHoneycomb */
class a {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f433a = {16843531};

    public static Object a(Object obj, Activity activity, int i) {
        Object obj2;
        if (obj == null) {
            obj2 = new C0009a(activity);
        } else {
            obj2 = obj;
        }
        C0009a aVar = (C0009a) obj2;
        if (aVar.f434a != null) {
            try {
                ActionBar actionBar = activity.getActionBar();
                aVar.f435b.invoke(actionBar, Integer.valueOf(i));
                if (Build.VERSION.SDK_INT <= 19) {
                    actionBar.setSubtitle(actionBar.getSubtitle());
                }
            } catch (Exception e2) {
                Log.w("ActionBarDrawerToggleHoneycomb", "Couldn't set content description via JB-MR2 API", e2);
            }
        }
        return obj2;
    }

    /* renamed from: android.support.v4.app.a$a  reason: collision with other inner class name */
    /* compiled from: ActionBarDrawerToggleHoneycomb */
    private static class C0009a {

        /* renamed from: a  reason: collision with root package name */
        public Method f434a;

        /* renamed from: b  reason: collision with root package name */
        public Method f435b;

        /* renamed from: c  reason: collision with root package name */
        public ImageView f436c;

        C0009a(Activity activity) {
            try {
                this.f434a = ActionBar.class.getDeclaredMethod("setHomeAsUpIndicator", Drawable.class);
                this.f435b = ActionBar.class.getDeclaredMethod("setHomeActionContentDescription", Integer.TYPE);
            } catch (NoSuchMethodException e2) {
                View findViewById = activity.findViewById(16908332);
                if (findViewById != null) {
                    ViewGroup viewGroup = (ViewGroup) findViewById.getParent();
                    if (viewGroup.getChildCount() == 2) {
                        View childAt = viewGroup.getChildAt(0);
                        View view = childAt.getId() != 16908332 ? childAt : viewGroup.getChildAt(1);
                        if (view instanceof ImageView) {
                            this.f436c = (ImageView) view;
                        }
                    }
                }
            }
        }
    }
}
