package android.support.v4.app;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@TargetApi(9)
/* compiled from: BundleCompatGingerbread */
class m {

    /* renamed from: a  reason: collision with root package name */
    private static Method f463a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f464b;

    public static IBinder a(Bundle bundle, String str) {
        if (!f464b) {
            try {
                f463a = Bundle.class.getMethod("getIBinder", String.class);
                f463a.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("BundleCompatGingerbread", "Failed to retrieve getIBinder method", e2);
            }
            f464b = true;
        }
        if (f463a != null) {
            try {
                return (IBinder) f463a.invoke(bundle, str);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e3) {
                Log.i("BundleCompatGingerbread", "Failed to invoke getIBinder via reflection", e3);
                f463a = null;
            }
        }
        return null;
    }
}
