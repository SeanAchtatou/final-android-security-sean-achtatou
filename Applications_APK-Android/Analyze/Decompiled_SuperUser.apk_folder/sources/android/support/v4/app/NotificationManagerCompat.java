package android.support.v4.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;
import com.duapps.ad.AdError;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class NotificationManagerCompat {

    /* renamed from: a  reason: collision with root package name */
    static final int f397a = i.a();

    /* renamed from: b  reason: collision with root package name */
    private static final Object f398b = new Object();

    /* renamed from: c  reason: collision with root package name */
    private static String f399c;

    /* renamed from: d  reason: collision with root package name */
    private static Set<String> f400d = new HashSet();

    /* renamed from: g  reason: collision with root package name */
    private static final Object f401g = new Object();

    /* renamed from: h  reason: collision with root package name */
    private static SideChannelManager f402h;
    private static final b i;

    /* renamed from: e  reason: collision with root package name */
    private final Context f403e;

    /* renamed from: f  reason: collision with root package name */
    private final NotificationManager f404f = ((NotificationManager) this.f403e.getSystemService(ServiceManagerNative.NOTIFICATION));

    interface b {
        int a();

        void a(NotificationManager notificationManager, String str, int i);

        void a(NotificationManager notificationManager, String str, int i, Notification notification);
    }

    private interface i {
        void a(INotificationSideChannel iNotificationSideChannel);
    }

    static {
        if (android.support.v4.os.c.a()) {
            i = new c();
        } else if (Build.VERSION.SDK_INT >= 19) {
            i = new f();
        } else if (Build.VERSION.SDK_INT >= 14) {
            i = new e();
        } else {
            i = new d();
        }
    }

    public static NotificationManagerCompat a(Context context) {
        return new NotificationManagerCompat(context);
    }

    private NotificationManagerCompat(Context context) {
        this.f403e = context;
    }

    static class d implements b {
        d() {
        }

        public void a(NotificationManager notificationManager, String str, int i) {
            notificationManager.cancel(str, i);
        }

        public void a(NotificationManager notificationManager, String str, int i, Notification notification) {
            notificationManager.notify(str, i, notification);
        }

        public int a() {
            return 1;
        }
    }

    static class e extends d {
        e() {
        }

        public int a() {
            return 33;
        }
    }

    static class f extends e {
        f() {
        }
    }

    static class c extends f {
        c() {
        }
    }

    public void a(int i2) {
        a((String) null, i2);
    }

    public void a(String str, int i2) {
        i.a(this.f404f, str, i2);
        if (Build.VERSION.SDK_INT <= 19) {
            a(new a(this.f403e.getPackageName(), i2, str));
        }
    }

    public void a(int i2, Notification notification) {
        a(null, i2, notification);
    }

    public void a(String str, int i2, Notification notification) {
        if (a(notification)) {
            a(new g(this.f403e.getPackageName(), i2, str, notification));
            i.a(this.f404f, str, i2);
            return;
        }
        i.a(this.f404f, str, i2, notification);
    }

    public static Set<String> b(Context context) {
        Set<String> set;
        String string = Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
        synchronized (f398b) {
            if (string != null) {
                if (!string.equals(f399c)) {
                    String[] split = string.split(":");
                    HashSet hashSet = new HashSet(split.length);
                    for (String unflattenFromString : split) {
                        ComponentName unflattenFromString2 = ComponentName.unflattenFromString(unflattenFromString);
                        if (unflattenFromString2 != null) {
                            hashSet.add(unflattenFromString2.getPackageName());
                        }
                    }
                    f400d = hashSet;
                    f399c = string;
                }
            }
            set = f400d;
        }
        return set;
    }

    private static boolean a(Notification notification) {
        Bundle a2 = NotificationCompat.a(notification);
        return a2 != null && a2.getBoolean("android.support.useSideChannel");
    }

    private void a(i iVar) {
        synchronized (f401g) {
            if (f402h == null) {
                f402h = new SideChannelManager(this.f403e.getApplicationContext());
            }
            f402h.a(iVar);
        }
    }

    private static class SideChannelManager implements ServiceConnection, Handler.Callback {

        /* renamed from: a  reason: collision with root package name */
        private final Context f405a;

        /* renamed from: b  reason: collision with root package name */
        private final HandlerThread f406b;

        /* renamed from: c  reason: collision with root package name */
        private final Handler f407c;

        /* renamed from: d  reason: collision with root package name */
        private final Map<ComponentName, a> f408d = new HashMap();

        /* renamed from: e  reason: collision with root package name */
        private Set<String> f409e = new HashSet();

        public SideChannelManager(Context context) {
            this.f405a = context;
            this.f406b = new HandlerThread("NotificationManagerCompat");
            this.f406b.start();
            this.f407c = new Handler(this.f406b.getLooper(), this);
        }

        public void a(i iVar) {
            this.f407c.obtainMessage(0, iVar).sendToTarget();
        }

        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    b((i) message.obj);
                    return true;
                case 1:
                    h hVar = (h) message.obj;
                    a(hVar.f423a, hVar.f424b);
                    return true;
                case 2:
                    a((ComponentName) message.obj);
                    return true;
                case 3:
                    b((ComponentName) message.obj);
                    return true;
                default:
                    return false;
            }
        }

        private void b(i iVar) {
            a();
            for (a next : this.f408d.values()) {
                next.f413d.add(iVar);
                d(next);
            }
        }

        private void a(ComponentName componentName, IBinder iBinder) {
            a aVar = this.f408d.get(componentName);
            if (aVar != null) {
                aVar.f412c = INotificationSideChannel.Stub.a(iBinder);
                aVar.f414e = 0;
                d(aVar);
            }
        }

        private void a(ComponentName componentName) {
            a aVar = this.f408d.get(componentName);
            if (aVar != null) {
                b(aVar);
            }
        }

        private void b(ComponentName componentName) {
            a aVar = this.f408d.get(componentName);
            if (aVar != null) {
                d(aVar);
            }
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Connected to service " + componentName);
            }
            this.f407c.obtainMessage(1, new h(componentName, iBinder)).sendToTarget();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Disconnected from service " + componentName);
            }
            this.f407c.obtainMessage(2, componentName).sendToTarget();
        }

        private void a() {
            Set<String> b2 = NotificationManagerCompat.b(this.f405a);
            if (!b2.equals(this.f409e)) {
                this.f409e = b2;
                List<ResolveInfo> queryIntentServices = this.f405a.getPackageManager().queryIntentServices(new Intent().setAction("android.support.BIND_NOTIFICATION_SIDE_CHANNEL"), 4);
                HashSet<ComponentName> hashSet = new HashSet<>();
                for (ResolveInfo next : queryIntentServices) {
                    if (b2.contains(next.serviceInfo.packageName)) {
                        ComponentName componentName = new ComponentName(next.serviceInfo.packageName, next.serviceInfo.name);
                        if (next.serviceInfo.permission != null) {
                            Log.w("NotifManCompat", "Permission present on component " + componentName + ", not adding listener record.");
                        } else {
                            hashSet.add(componentName);
                        }
                    }
                }
                for (ComponentName componentName2 : hashSet) {
                    if (!this.f408d.containsKey(componentName2)) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Adding listener record for " + componentName2);
                        }
                        this.f408d.put(componentName2, new a(componentName2));
                    }
                }
                Iterator<Map.Entry<ComponentName, a>> it = this.f408d.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry next2 = it.next();
                    if (!hashSet.contains(next2.getKey())) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Removing listener record for " + next2.getKey());
                        }
                        b((a) next2.getValue());
                        it.remove();
                    }
                }
            }
        }

        private boolean a(a aVar) {
            if (aVar.f411b) {
                return true;
            }
            aVar.f411b = this.f405a.bindService(new Intent("android.support.BIND_NOTIFICATION_SIDE_CHANNEL").setComponent(aVar.f410a), this, NotificationManagerCompat.f397a);
            if (aVar.f411b) {
                aVar.f414e = 0;
            } else {
                Log.w("NotifManCompat", "Unable to bind to listener " + aVar.f410a);
                this.f405a.unbindService(this);
            }
            return aVar.f411b;
        }

        private void b(a aVar) {
            if (aVar.f411b) {
                this.f405a.unbindService(this);
                aVar.f411b = false;
            }
            aVar.f412c = null;
        }

        private void c(a aVar) {
            if (!this.f407c.hasMessages(3, aVar.f410a)) {
                aVar.f414e++;
                if (aVar.f414e > 6) {
                    Log.w("NotifManCompat", "Giving up on delivering " + aVar.f413d.size() + " tasks to " + aVar.f410a + " after " + aVar.f414e + " retries");
                    aVar.f413d.clear();
                    return;
                }
                int i = (1 << (aVar.f414e - 1)) * AdError.NETWORK_ERROR_CODE;
                if (Log.isLoggable("NotifManCompat", 3)) {
                    Log.d("NotifManCompat", "Scheduling retry for " + i + " ms");
                }
                this.f407c.sendMessageDelayed(this.f407c.obtainMessage(3, aVar.f410a), (long) i);
            }
        }

        private void d(a aVar) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                Log.d("NotifManCompat", "Processing component " + aVar.f410a + ", " + aVar.f413d.size() + " queued tasks");
            }
            if (!aVar.f413d.isEmpty()) {
                if (!a(aVar) || aVar.f412c == null) {
                    c(aVar);
                    return;
                }
                while (true) {
                    i peek = aVar.f413d.peek();
                    if (peek == null) {
                        break;
                    }
                    try {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Sending task " + peek);
                        }
                        peek.a(aVar.f412c);
                        aVar.f413d.remove();
                    } catch (DeadObjectException e2) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            Log.d("NotifManCompat", "Remote service has died: " + aVar.f410a);
                        }
                    } catch (RemoteException e3) {
                        Log.w("NotifManCompat", "RemoteException communicating with " + aVar.f410a, e3);
                    }
                }
                if (!aVar.f413d.isEmpty()) {
                    c(aVar);
                }
            }
        }

        private static class a {

            /* renamed from: a  reason: collision with root package name */
            public final ComponentName f410a;

            /* renamed from: b  reason: collision with root package name */
            public boolean f411b = false;

            /* renamed from: c  reason: collision with root package name */
            public INotificationSideChannel f412c;

            /* renamed from: d  reason: collision with root package name */
            public LinkedList<i> f413d = new LinkedList<>();

            /* renamed from: e  reason: collision with root package name */
            public int f414e = 0;

            public a(ComponentName componentName) {
                this.f410a = componentName;
            }
        }
    }

    private static class h {

        /* renamed from: a  reason: collision with root package name */
        final ComponentName f423a;

        /* renamed from: b  reason: collision with root package name */
        final IBinder f424b;

        public h(ComponentName componentName, IBinder iBinder) {
            this.f423a = componentName;
            this.f424b = iBinder;
        }
    }

    private static class g implements i {

        /* renamed from: a  reason: collision with root package name */
        final String f419a;

        /* renamed from: b  reason: collision with root package name */
        final int f420b;

        /* renamed from: c  reason: collision with root package name */
        final String f421c;

        /* renamed from: d  reason: collision with root package name */
        final Notification f422d;

        public g(String str, int i, String str2, Notification notification) {
            this.f419a = str;
            this.f420b = i;
            this.f421c = str2;
            this.f422d = notification;
        }

        public void a(INotificationSideChannel iNotificationSideChannel) {
            iNotificationSideChannel.a(this.f419a, this.f420b, this.f421c, this.f422d);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("NotifyTask[");
            sb.append("packageName:").append(this.f419a);
            sb.append(", id:").append(this.f420b);
            sb.append(", tag:").append(this.f421c);
            sb.append("]");
            return sb.toString();
        }
    }

    private static class a implements i {

        /* renamed from: a  reason: collision with root package name */
        final String f415a;

        /* renamed from: b  reason: collision with root package name */
        final int f416b;

        /* renamed from: c  reason: collision with root package name */
        final String f417c;

        /* renamed from: d  reason: collision with root package name */
        final boolean f418d = false;

        public a(String str, int i, String str2) {
            this.f415a = str;
            this.f416b = i;
            this.f417c = str2;
        }

        public void a(INotificationSideChannel iNotificationSideChannel) {
            if (this.f418d) {
                iNotificationSideChannel.a(this.f415a);
            } else {
                iNotificationSideChannel.a(this.f415a, this.f416b, this.f417c);
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("CancelTask[");
            sb.append("packageName:").append(this.f415a);
            sb.append(", id:").append(this.f416b);
            sb.append(", tag:").append(this.f417c);
            sb.append(", all:").append(this.f418d);
            sb.append("]");
            return sb.toString();
        }
    }
}
