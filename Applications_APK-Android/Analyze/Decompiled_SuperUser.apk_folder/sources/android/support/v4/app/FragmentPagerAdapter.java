package android.support.v4.app;

import android.os.Parcelable;
import android.support.v4.view.z;
import android.view.View;
import android.view.ViewGroup;

public abstract class FragmentPagerAdapter extends z {

    /* renamed from: a  reason: collision with root package name */
    private final q f290a;

    /* renamed from: b  reason: collision with root package name */
    private t f291b = null;

    /* renamed from: c  reason: collision with root package name */
    private Fragment f292c = null;

    public abstract Fragment a(int i);

    public FragmentPagerAdapter(q qVar) {
        this.f290a = qVar;
    }

    public void a(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            throw new IllegalStateException("ViewPager with adapter " + this + " requires a view id");
        }
    }

    public Object a(ViewGroup viewGroup, int i) {
        if (this.f291b == null) {
            this.f291b = this.f290a.a();
        }
        long b2 = b(i);
        Fragment a2 = this.f290a.a(a(viewGroup.getId(), b2));
        if (a2 != null) {
            this.f291b.c(a2);
        } else {
            a2 = a(i);
            this.f291b.a(viewGroup.getId(), a2, a(viewGroup.getId(), b2));
        }
        if (a2 != this.f292c) {
            a2.setMenuVisibility(false);
            a2.setUserVisibleHint(false);
        }
        return a2;
    }

    public void a(ViewGroup viewGroup, int i, Object obj) {
        if (this.f291b == null) {
            this.f291b = this.f290a.a();
        }
        this.f291b.b((Fragment) obj);
    }

    public void b(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (fragment != this.f292c) {
            if (this.f292c != null) {
                this.f292c.setMenuVisibility(false);
                this.f292c.setUserVisibleHint(false);
            }
            if (fragment != null) {
                fragment.setMenuVisibility(true);
                fragment.setUserVisibleHint(true);
            }
            this.f292c = fragment;
        }
    }

    public void b(ViewGroup viewGroup) {
        if (this.f291b != null) {
            this.f291b.d();
            this.f291b = null;
        }
    }

    public boolean a(View view, Object obj) {
        return ((Fragment) obj).getView() == view;
    }

    public Parcelable a() {
        return null;
    }

    public void a(Parcelable parcelable, ClassLoader classLoader) {
    }

    public long b(int i) {
        return (long) i;
    }

    private static String a(int i, long j) {
        return "android:switcher:" + i + ":" + j;
    }
}
