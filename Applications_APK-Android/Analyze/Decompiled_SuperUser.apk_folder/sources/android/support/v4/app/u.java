package android.support.v4.app;

import android.graphics.Rect;
import android.os.Build;
import android.support.v4.app.h;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ag;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/* compiled from: FragmentTransition */
class u {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f491a = {0, 3, 0, 1, 5, 4, 7, 6};

    static void a(r rVar, ArrayList<h> arrayList, ArrayList<Boolean> arrayList2, int i, int i2, boolean z) {
        if (rVar.m >= 1 && Build.VERSION.SDK_INT >= 21) {
            SparseArray sparseArray = new SparseArray();
            for (int i3 = i; i3 < i2; i3++) {
                h hVar = arrayList.get(i3);
                if (arrayList2.get(i3).booleanValue()) {
                    b(hVar, sparseArray, z);
                } else {
                    a(hVar, sparseArray, z);
                }
            }
            if (sparseArray.size() != 0) {
                View view = new View(rVar.n.i());
                int size = sparseArray.size();
                for (int i4 = 0; i4 < size; i4++) {
                    int keyAt = sparseArray.keyAt(i4);
                    ArrayMap<String, String> a2 = a(keyAt, arrayList, arrayList2, i, i2);
                    a aVar = (a) sparseArray.valueAt(i4);
                    if (z) {
                        a(rVar, keyAt, aVar, view, a2);
                    } else {
                        b(rVar, keyAt, aVar, view, a2);
                    }
                }
            }
        }
    }

    private static ArrayMap<String, String> a(int i, ArrayList<h> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        ArrayList<String> arrayList3;
        ArrayList<String> arrayList4;
        ArrayMap<String, String> arrayMap = new ArrayMap<>();
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            h hVar = arrayList.get(i4);
            if (hVar.b(i)) {
                boolean booleanValue = arrayList2.get(i4).booleanValue();
                if (hVar.s != null) {
                    int size = hVar.s.size();
                    if (booleanValue) {
                        arrayList3 = hVar.s;
                        arrayList4 = hVar.t;
                    } else {
                        ArrayList<String> arrayList5 = hVar.s;
                        arrayList3 = hVar.t;
                        arrayList4 = arrayList5;
                    }
                    for (int i5 = 0; i5 < size; i5++) {
                        String str = arrayList4.get(i5);
                        String str2 = arrayList3.get(i5);
                        String remove = arrayMap.remove(str2);
                        if (remove != null) {
                            arrayMap.put(str, remove);
                        } else {
                            arrayMap.put(str, str2);
                        }
                    }
                }
            }
        }
        return arrayMap;
    }

    private static void a(r rVar, int i, a aVar, View view, ArrayMap<String, String> arrayMap) {
        ViewGroup viewGroup = null;
        if (rVar.o.a()) {
            viewGroup = (ViewGroup) rVar.o.a(i);
        }
        if (viewGroup != null) {
            Fragment fragment = aVar.f514a;
            Fragment fragment2 = aVar.f517d;
            boolean z = aVar.f515b;
            boolean z2 = aVar.f518e;
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Object a2 = a(fragment, z);
            Object b2 = b(fragment2, z2);
            Object a3 = a(viewGroup, view, arrayMap, aVar, arrayList2, arrayList, a2, b2);
            if (a2 != null || a3 != null || b2 != null) {
                ArrayList<View> b3 = b(b2, fragment2, arrayList2, view);
                ArrayList<View> b4 = b(a2, fragment, arrayList, view);
                b(b4, 4);
                Object a4 = a(a2, b2, a3, fragment, z);
                if (a4 != null) {
                    a(b2, fragment2, b3);
                    ArrayList<String> a5 = v.a((ArrayList<View>) arrayList);
                    v.a(a4, a2, b4, b2, b3, a3, arrayList);
                    v.a(viewGroup, a4);
                    v.a(viewGroup, arrayList2, arrayList, a5, arrayMap);
                    b(b4, 0);
                    v.a(a3, (ArrayList<View>) arrayList2, (ArrayList<View>) arrayList);
                }
            }
        }
    }

    private static void a(Object obj, Fragment fragment, final ArrayList<View> arrayList) {
        if (fragment != null && obj != null && fragment.mAdded && fragment.mHidden && fragment.mHiddenChanged) {
            fragment.setHideReplaced(true);
            v.b(obj, fragment.getView(), arrayList);
            ae.a(fragment.mContainer, new Runnable() {
                public void run() {
                    u.b(arrayList, 4);
                }
            });
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.v.a(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
     arg types: [android.view.ViewGroup, java.util.ArrayList, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>]
     candidates:
      android.support.v4.app.v.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      android.support.v4.app.v.a(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
      android.support.v4.app.v.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      android.support.v4.app.v.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
      android.support.v4.app.v.a(java.util.List<android.view.View>, android.view.View, int):boolean
      android.support.v4.app.v.a(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.v.a(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
     arg types: [android.view.ViewGroup, java.util.ArrayList, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>]
     candidates:
      android.support.v4.app.v.a(java.lang.Object, java.lang.Object, java.lang.Object):java.lang.Object
      android.support.v4.app.v.a(android.view.View, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void
      android.support.v4.app.v.a(java.lang.Object, android.view.View, java.util.ArrayList<android.view.View>):void
      android.support.v4.app.v.a(java.lang.Object, java.util.ArrayList<android.view.View>, java.util.ArrayList<android.view.View>):void
      android.support.v4.app.v.a(java.util.List<android.view.View>, android.view.View, int):boolean
      android.support.v4.app.v.a(android.view.ViewGroup, java.util.ArrayList<android.view.View>, java.util.Map<java.lang.String, java.lang.String>):void */
    private static void b(r rVar, int i, a aVar, View view, ArrayMap<String, String> arrayMap) {
        Object obj;
        ViewGroup viewGroup = null;
        if (rVar.o.a()) {
            viewGroup = (ViewGroup) rVar.o.a(i);
        }
        if (viewGroup != null) {
            Fragment fragment = aVar.f514a;
            Fragment fragment2 = aVar.f517d;
            boolean z = aVar.f515b;
            boolean z2 = aVar.f518e;
            Object a2 = a(fragment, z);
            Object b2 = b(fragment2, z2);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Object b3 = b(viewGroup, view, arrayMap, aVar, arrayList, arrayList2, a2, b2);
            if (a2 != null || b3 != null || b2 != null) {
                ArrayList<View> b4 = b(b2, fragment2, arrayList, view);
                if (b4 == null || b4.isEmpty()) {
                    obj = null;
                } else {
                    obj = b2;
                }
                v.b(a2, view);
                Object a3 = a(a2, obj, b3, fragment, aVar.f515b);
                if (a3 != null) {
                    ArrayList arrayList3 = new ArrayList();
                    v.a(a3, a2, arrayList3, obj, b4, b3, arrayList2);
                    a(viewGroup, fragment, view, arrayList2, a2, arrayList3, obj, b4);
                    v.a((View) viewGroup, (ArrayList<View>) arrayList2, (Map<String, String>) arrayMap);
                    v.a(viewGroup, a3);
                    v.a(viewGroup, (ArrayList<View>) arrayList2, (Map<String, String>) arrayMap);
                }
            }
        }
    }

    private static void a(ViewGroup viewGroup, Fragment fragment, View view, ArrayList<View> arrayList, Object obj, ArrayList<View> arrayList2, Object obj2, ArrayList<View> arrayList3) {
        final Object obj3 = obj;
        final View view2 = view;
        final Fragment fragment2 = fragment;
        final ArrayList<View> arrayList4 = arrayList;
        final ArrayList<View> arrayList5 = arrayList2;
        final ArrayList<View> arrayList6 = arrayList3;
        final Object obj4 = obj2;
        ae.a(viewGroup, new Runnable() {
            public void run() {
                if (obj3 != null) {
                    v.c(obj3, view2);
                    arrayList5.addAll(u.b(obj3, fragment2, arrayList4, view2));
                }
                if (arrayList6 != null) {
                    if (obj4 != null) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(view2);
                        v.b(obj4, (ArrayList<View>) arrayList6, (ArrayList<View>) arrayList);
                    }
                    arrayList6.clear();
                    arrayList6.add(view2);
                }
            }
        });
    }

    private static Object a(Fragment fragment, Fragment fragment2, boolean z) {
        Object sharedElementEnterTransition;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        if (z) {
            sharedElementEnterTransition = fragment2.getSharedElementReturnTransition();
        } else {
            sharedElementEnterTransition = fragment.getSharedElementEnterTransition();
        }
        return v.b(v.a(sharedElementEnterTransition));
    }

    private static Object a(Fragment fragment, boolean z) {
        Object enterTransition;
        if (fragment == null) {
            return null;
        }
        if (z) {
            enterTransition = fragment.getReenterTransition();
        } else {
            enterTransition = fragment.getEnterTransition();
        }
        return v.a(enterTransition);
    }

    private static Object b(Fragment fragment, boolean z) {
        Object exitTransition;
        if (fragment == null) {
            return null;
        }
        if (z) {
            exitTransition = fragment.getReturnTransition();
        } else {
            exitTransition = fragment.getExitTransition();
        }
        return v.a(exitTransition);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.u.b(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean):void
     arg types: [android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, int]
     candidates:
      android.support.v4.app.u.b(android.support.v4.app.r, int, android.support.v4.app.u$a, android.view.View, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>):void
      android.support.v4.app.u.b(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean):void */
    private static Object a(ViewGroup viewGroup, View view, ArrayMap<String, String> arrayMap, a aVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object a2;
        Object obj3;
        final Rect rect;
        final View view2 = null;
        final Fragment fragment = aVar.f514a;
        final Fragment fragment2 = aVar.f517d;
        if (fragment != null) {
            fragment.getView().setVisibility(0);
        }
        if (fragment == null || fragment2 == null) {
            return null;
        }
        final boolean z = aVar.f515b;
        if (arrayMap.isEmpty()) {
            a2 = null;
        } else {
            a2 = a(fragment, fragment2, z);
        }
        ArrayMap<String, View> b2 = b(arrayMap, a2, aVar);
        final ArrayMap<String, View> c2 = c(arrayMap, a2, aVar);
        if (arrayMap.isEmpty()) {
            if (b2 != null) {
                b2.clear();
            }
            if (c2 != null) {
                c2.clear();
                obj3 = null;
            } else {
                obj3 = null;
            }
        } else {
            a(arrayList, b2, arrayMap.keySet());
            a(arrayList2, c2, arrayMap.values());
            obj3 = a2;
        }
        if (obj == null && obj2 == null && obj3 == null) {
            return null;
        }
        b(fragment, fragment2, z, b2, true);
        if (obj3 != null) {
            arrayList2.add(view);
            v.a(obj3, view, arrayList);
            a(obj3, obj2, b2, aVar.f518e, aVar.f519f);
            rect = new Rect();
            view2 = b(c2, aVar, obj, z);
            if (view2 != null) {
                v.a(obj, rect);
            }
        } else {
            rect = null;
        }
        ae.a(viewGroup, new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.app.u.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap, boolean):void
             arg types: [android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap, int]
             candidates:
              android.support.v4.app.u.a(int, java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int):android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>
              android.support.v4.app.u.a(java.lang.Object, java.lang.Object, java.lang.Object, android.support.v4.app.Fragment, boolean):java.lang.Object
              android.support.v4.app.u.a(android.support.v4.app.h, android.support.v4.app.h$a, android.util.SparseArray<android.support.v4.app.u$a>, boolean, boolean):void
              android.support.v4.app.u.a(android.support.v4.app.r, int, android.support.v4.app.u$a, android.view.View, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>):void
              android.support.v4.app.u.a(java.lang.Object, java.lang.Object, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean, android.support.v4.app.h):void
              android.support.v4.app.u.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap, boolean):void */
            public void run() {
                u.b(fragment, fragment2, z, c2, false);
                if (view2 != null) {
                    v.a(view2, rect);
                }
            }
        });
        return obj3;
    }

    private static void a(ArrayList<View> arrayList, ArrayMap<String, View> arrayMap, Collection<String> collection) {
        for (int size = arrayMap.size() - 1; size >= 0; size--) {
            View c2 = arrayMap.c(size);
            if (collection.contains(ag.u(c2))) {
                arrayList.add(c2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.u.b(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean):void
     arg types: [android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, int]
     candidates:
      android.support.v4.app.u.b(android.support.v4.app.r, int, android.support.v4.app.u$a, android.view.View, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>):void
      android.support.v4.app.u.b(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean):void */
    private static Object b(ViewGroup viewGroup, View view, ArrayMap<String, String> arrayMap, a aVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object a2;
        final Object obj3;
        final Rect rect;
        final Fragment fragment = aVar.f514a;
        final Fragment fragment2 = aVar.f517d;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        final boolean z = aVar.f515b;
        if (arrayMap.isEmpty()) {
            a2 = null;
        } else {
            a2 = a(fragment, fragment2, z);
        }
        ArrayMap<String, View> b2 = b(arrayMap, a2, aVar);
        if (arrayMap.isEmpty()) {
            obj3 = null;
        } else {
            arrayList.addAll(b2.values());
            obj3 = a2;
        }
        if (obj == null && obj2 == null && obj3 == null) {
            return null;
        }
        b(fragment, fragment2, z, b2, true);
        if (obj3 != null) {
            rect = new Rect();
            v.a(obj3, view, arrayList);
            a(obj3, obj2, b2, aVar.f518e, aVar.f519f);
            if (obj != null) {
                v.a(obj, rect);
            }
        } else {
            rect = null;
        }
        final ArrayMap<String, String> arrayMap2 = arrayMap;
        final a aVar2 = aVar;
        final ArrayList<View> arrayList3 = arrayList2;
        final View view2 = view;
        final ArrayList<View> arrayList4 = arrayList;
        final Object obj4 = obj;
        ae.a(viewGroup, new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.app.u.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap, boolean):void
             arg types: [android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap, int]
             candidates:
              android.support.v4.app.u.a(int, java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int):android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>
              android.support.v4.app.u.a(java.lang.Object, java.lang.Object, java.lang.Object, android.support.v4.app.Fragment, boolean):java.lang.Object
              android.support.v4.app.u.a(android.support.v4.app.h, android.support.v4.app.h$a, android.util.SparseArray<android.support.v4.app.u$a>, boolean, boolean):void
              android.support.v4.app.u.a(android.support.v4.app.r, int, android.support.v4.app.u$a, android.view.View, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>):void
              android.support.v4.app.u.a(java.lang.Object, java.lang.Object, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean, android.support.v4.app.h):void
              android.support.v4.app.u.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap, boolean):void */
            public void run() {
                ArrayMap a2 = u.c(arrayMap2, obj3, aVar2);
                if (a2 != null) {
                    arrayList3.addAll(a2.values());
                    arrayList3.add(view2);
                }
                u.b(fragment, fragment2, z, a2, false);
                if (obj3 != null) {
                    v.a(obj3, (ArrayList<View>) arrayList4, (ArrayList<View>) arrayList3);
                    View a3 = u.b(a2, aVar2, obj4, z);
                    if (a3 != null) {
                        v.a(a3, rect);
                    }
                }
            }
        });
        return obj3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.v.a(java.util.Map<java.lang.String, android.view.View>, android.view.View):void
     arg types: [android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, android.view.View]
     candidates:
      android.support.v4.app.v.a(java.util.Map, java.lang.String):java.lang.String
      android.support.v4.app.v.a(android.view.View, android.graphics.Rect):void
      android.support.v4.app.v.a(android.view.ViewGroup, java.lang.Object):void
      android.support.v4.app.v.a(java.lang.Object, android.graphics.Rect):void
      android.support.v4.app.v.a(java.lang.Object, android.view.View):void
      android.support.v4.app.v.a(java.lang.Object, java.util.ArrayList<android.view.View>):void
      android.support.v4.app.v.a(java.util.ArrayList<android.view.View>, android.view.View):void
      android.support.v4.app.v.a(java.util.List<android.view.View>, android.view.View):void
      android.support.v4.app.v.a(java.util.Map<java.lang.String, android.view.View>, android.view.View):void */
    private static ArrayMap<String, View> b(ArrayMap<String, String> arrayMap, Object obj, a aVar) {
        ArrayList<String> arrayList;
        ai aiVar;
        if (arrayMap.isEmpty() || obj == null) {
            arrayMap.clear();
            return null;
        }
        Fragment fragment = aVar.f517d;
        ArrayMap<String, View> arrayMap2 = new ArrayMap<>();
        v.a((Map<String, View>) arrayMap2, fragment.getView());
        h hVar = aVar.f519f;
        if (aVar.f518e) {
            ai enterTransitionCallback = fragment.getEnterTransitionCallback();
            arrayList = hVar.t;
            aiVar = enterTransitionCallback;
        } else {
            ai exitTransitionCallback = fragment.getExitTransitionCallback();
            arrayList = hVar.s;
            aiVar = exitTransitionCallback;
        }
        arrayMap2.a((Collection<?>) arrayList);
        if (aiVar != null) {
            aiVar.a(arrayList, arrayMap2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view = arrayMap2.get(str);
                if (view == null) {
                    arrayMap.remove(str);
                } else if (!str.equals(ag.u(view))) {
                    arrayMap.put(ag.u(view), arrayMap.remove(str));
                }
            }
        } else {
            arrayMap.a((Collection<?>) arrayMap2.keySet());
        }
        return arrayMap2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.v.a(java.util.Map<java.lang.String, android.view.View>, android.view.View):void
     arg types: [android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, android.view.View]
     candidates:
      android.support.v4.app.v.a(java.util.Map, java.lang.String):java.lang.String
      android.support.v4.app.v.a(android.view.View, android.graphics.Rect):void
      android.support.v4.app.v.a(android.view.ViewGroup, java.lang.Object):void
      android.support.v4.app.v.a(java.lang.Object, android.graphics.Rect):void
      android.support.v4.app.v.a(java.lang.Object, android.view.View):void
      android.support.v4.app.v.a(java.lang.Object, java.util.ArrayList<android.view.View>):void
      android.support.v4.app.v.a(java.util.ArrayList<android.view.View>, android.view.View):void
      android.support.v4.app.v.a(java.util.List<android.view.View>, android.view.View):void
      android.support.v4.app.v.a(java.util.Map<java.lang.String, android.view.View>, android.view.View):void */
    /* access modifiers changed from: private */
    public static ArrayMap<String, View> c(ArrayMap<String, String> arrayMap, Object obj, a aVar) {
        ArrayList<String> arrayList;
        ai aiVar;
        String a2;
        Fragment fragment = aVar.f514a;
        View view = fragment.getView();
        if (arrayMap.isEmpty() || obj == null || view == null) {
            arrayMap.clear();
            return null;
        }
        ArrayMap<String, View> arrayMap2 = new ArrayMap<>();
        v.a((Map<String, View>) arrayMap2, view);
        h hVar = aVar.f516c;
        if (aVar.f515b) {
            ai exitTransitionCallback = fragment.getExitTransitionCallback();
            arrayList = hVar.s;
            aiVar = exitTransitionCallback;
        } else {
            ai enterTransitionCallback = fragment.getEnterTransitionCallback();
            arrayList = hVar.t;
            aiVar = enterTransitionCallback;
        }
        if (arrayList != null) {
            arrayMap2.a((Collection<?>) arrayList);
        }
        if (aiVar != null) {
            aiVar.a(arrayList, arrayMap2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view2 = arrayMap2.get(str);
                if (view2 == null) {
                    String a3 = a(arrayMap, str);
                    if (a3 != null) {
                        arrayMap.remove(a3);
                    }
                } else if (!str.equals(ag.u(view2)) && (a2 = a(arrayMap, str)) != null) {
                    arrayMap.put(a2, ag.u(view2));
                }
            }
        } else {
            a(arrayMap, arrayMap2);
        }
        return arrayMap2;
    }

    private static String a(ArrayMap<String, String> arrayMap, String str) {
        int size = arrayMap.size();
        for (int i = 0; i < size; i++) {
            if (str.equals(arrayMap.c(i))) {
                return arrayMap.b(i);
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static View b(ArrayMap<String, View> arrayMap, a aVar, Object obj, boolean z) {
        String str;
        h hVar = aVar.f516c;
        if (obj == null || arrayMap == null || hVar.s == null || hVar.s.isEmpty()) {
            return null;
        }
        if (z) {
            str = hVar.s.get(0);
        } else {
            str = hVar.t.get(0);
        }
        return arrayMap.get(str);
    }

    private static void a(Object obj, Object obj2, ArrayMap<String, View> arrayMap, boolean z, h hVar) {
        String str;
        if (hVar.s != null && !hVar.s.isEmpty()) {
            if (z) {
                str = hVar.t.get(0);
            } else {
                str = hVar.s.get(0);
            }
            View view = arrayMap.get(str);
            v.a(obj, view);
            if (obj2 != null) {
                v.a(obj2, view);
            }
        }
    }

    private static void a(ArrayMap<String, String> arrayMap, ArrayMap<String, View> arrayMap2) {
        for (int size = arrayMap.size() - 1; size >= 0; size--) {
            if (!arrayMap2.containsKey(arrayMap.c(size))) {
                arrayMap.d(size);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(Fragment fragment, Fragment fragment2, boolean z, ArrayMap<String, View> arrayMap, boolean z2) {
        ai enterTransitionCallback;
        if (z) {
            enterTransitionCallback = fragment2.getEnterTransitionCallback();
        } else {
            enterTransitionCallback = fragment.getEnterTransitionCallback();
        }
        if (enterTransitionCallback != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            int size = arrayMap == null ? 0 : arrayMap.size();
            for (int i = 0; i < size; i++) {
                arrayList2.add(arrayMap.b(i));
                arrayList.add(arrayMap.c(i));
            }
            if (z2) {
                enterTransitionCallback.a(arrayList2, arrayList, null);
            } else {
                enterTransitionCallback.b(arrayList2, arrayList, null);
            }
        }
    }

    /* access modifiers changed from: private */
    public static ArrayList<View> b(Object obj, Fragment fragment, ArrayList<View> arrayList, View view) {
        ArrayList<View> arrayList2 = null;
        if (obj != null) {
            arrayList2 = new ArrayList<>();
            View view2 = fragment.getView();
            if (view2 != null) {
                v.a(arrayList2, view2);
            }
            if (arrayList != null) {
                arrayList2.removeAll(arrayList);
            }
            if (!arrayList2.isEmpty()) {
                arrayList2.add(view);
                v.a(obj, arrayList2);
            }
        }
        return arrayList2;
    }

    /* access modifiers changed from: private */
    public static void b(ArrayList<View> arrayList, int i) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                arrayList.get(size).setVisibility(i);
            }
        }
    }

    private static Object a(Object obj, Object obj2, Object obj3, Fragment fragment, boolean z) {
        boolean z2 = true;
        if (!(obj == null || obj2 == null || fragment == null)) {
            z2 = z ? fragment.getAllowReturnTransitionOverlap() : fragment.getAllowEnterTransitionOverlap();
        }
        if (z2) {
            return v.a(obj2, obj, obj3);
        }
        return v.b(obj2, obj, obj3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.u.a(android.support.v4.app.h, android.support.v4.app.h$a, android.util.SparseArray<android.support.v4.app.u$a>, boolean, boolean):void
     arg types: [android.support.v4.app.h, android.support.v4.app.h$a, android.util.SparseArray<android.support.v4.app.u$a>, int, boolean]
     candidates:
      android.support.v4.app.u.a(int, java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int):android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>
      android.support.v4.app.u.a(java.lang.Object, java.lang.Object, java.lang.Object, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.u.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap, boolean):void
      android.support.v4.app.u.a(android.support.v4.app.r, int, android.support.v4.app.u$a, android.view.View, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>):void
      android.support.v4.app.u.a(java.lang.Object, java.lang.Object, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean, android.support.v4.app.h):void
      android.support.v4.app.u.a(android.support.v4.app.h, android.support.v4.app.h$a, android.util.SparseArray<android.support.v4.app.u$a>, boolean, boolean):void */
    public static void a(h hVar, SparseArray<a> sparseArray, boolean z) {
        int size = hVar.f449c.size();
        for (int i = 0; i < size; i++) {
            a(hVar, hVar.f449c.get(i), sparseArray, false, z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.u.a(android.support.v4.app.h, android.support.v4.app.h$a, android.util.SparseArray<android.support.v4.app.u$a>, boolean, boolean):void
     arg types: [android.support.v4.app.h, android.support.v4.app.h$a, android.util.SparseArray<android.support.v4.app.u$a>, int, boolean]
     candidates:
      android.support.v4.app.u.a(int, java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int):android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>
      android.support.v4.app.u.a(java.lang.Object, java.lang.Object, java.lang.Object, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.u.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean, android.support.v4.util.ArrayMap, boolean):void
      android.support.v4.app.u.a(android.support.v4.app.r, int, android.support.v4.app.u$a, android.view.View, android.support.v4.util.ArrayMap<java.lang.String, java.lang.String>):void
      android.support.v4.app.u.a(java.lang.Object, java.lang.Object, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean, android.support.v4.app.h):void
      android.support.v4.app.u.a(android.support.v4.app.h, android.support.v4.app.h$a, android.util.SparseArray<android.support.v4.app.u$a>, boolean, boolean):void */
    public static void b(h hVar, SparseArray<a> sparseArray, boolean z) {
        if (hVar.f448b.o.a()) {
            for (int size = hVar.f449c.size() - 1; size >= 0; size--) {
                a(hVar, hVar.f449c.get(size), sparseArray, true, z);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
      android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    private static void a(h hVar, h.a aVar, SparseArray<a> sparseArray, boolean z, boolean z2) {
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        boolean z9;
        boolean z10;
        a aVar2;
        a aVar3;
        Fragment fragment = aVar.f456b;
        int i = fragment.mContainerId;
        if (i != 0) {
            switch (z ? f491a[aVar.f455a] : aVar.f455a) {
                case 1:
                case 7:
                    if (z2) {
                        z9 = fragment.mIsNewlyAdded;
                    } else {
                        z9 = !fragment.mAdded && !fragment.mHidden;
                    }
                    z4 = true;
                    z5 = false;
                    z6 = false;
                    z7 = z9;
                    break;
                case 2:
                default:
                    z4 = false;
                    z5 = false;
                    z6 = false;
                    z7 = false;
                    break;
                case 3:
                case 6:
                    if (z2) {
                        z3 = !fragment.mAdded && fragment.mView != null && fragment.mView.getVisibility() == 0 && fragment.mPostponedAlpha >= 0.0f;
                    } else {
                        z3 = fragment.mAdded && !fragment.mHidden;
                    }
                    z4 = false;
                    z5 = z3;
                    z6 = true;
                    z7 = false;
                    break;
                case 4:
                    if (z2) {
                        z8 = fragment.mHiddenChanged && fragment.mAdded && fragment.mHidden;
                    } else {
                        z8 = fragment.mAdded && !fragment.mHidden;
                    }
                    z4 = false;
                    z5 = z8;
                    z6 = true;
                    z7 = false;
                    break;
                case 5:
                    if (z2) {
                        z10 = fragment.mHiddenChanged && !fragment.mHidden && fragment.mAdded;
                    } else {
                        z10 = fragment.mHidden;
                    }
                    z4 = true;
                    z5 = false;
                    z6 = false;
                    z7 = z10;
                    break;
            }
            a aVar4 = sparseArray.get(i);
            if (z7) {
                aVar2 = a(aVar4, sparseArray, i);
                aVar2.f514a = fragment;
                aVar2.f515b = z;
                aVar2.f516c = hVar;
            } else {
                aVar2 = aVar4;
            }
            if (!z2 && z4) {
                if (aVar2 != null && aVar2.f517d == fragment) {
                    aVar2.f517d = null;
                }
                r rVar = hVar.f448b;
                if (fragment.mState < 1 && rVar.m >= 1 && !hVar.u) {
                    rVar.e(fragment);
                    rVar.a(fragment, 1, 0, 0, false);
                }
            }
            if (!z5 || !(aVar2 == null || aVar2.f517d == null)) {
                aVar3 = aVar2;
            } else {
                aVar3 = a(aVar2, sparseArray, i);
                aVar3.f517d = fragment;
                aVar3.f518e = z;
                aVar3.f519f = hVar;
            }
            if (!z2 && z6 && aVar3 != null && aVar3.f514a == fragment) {
                aVar3.f514a = null;
            }
        }
    }

    private static a a(a aVar, SparseArray<a> sparseArray, int i) {
        if (aVar != null) {
            return aVar;
        }
        a aVar2 = new a();
        sparseArray.put(i, aVar2);
        return aVar2;
    }

    /* compiled from: FragmentTransition */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        public Fragment f514a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f515b;

        /* renamed from: c  reason: collision with root package name */
        public h f516c;

        /* renamed from: d  reason: collision with root package name */
        public Fragment f517d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f518e;

        /* renamed from: f  reason: collision with root package name */
        public h f519f;

        a() {
        }
    }
}
