package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.util.i;
import android.view.LayoutInflater;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class FragmentHostCallback<E> extends o {

    /* renamed from: a  reason: collision with root package name */
    private final Activity f279a;

    /* renamed from: b  reason: collision with root package name */
    final Context f280b;

    /* renamed from: c  reason: collision with root package name */
    final int f281c;

    /* renamed from: d  reason: collision with root package name */
    final r f282d;

    /* renamed from: e  reason: collision with root package name */
    private final Handler f283e;

    /* renamed from: f  reason: collision with root package name */
    private i<String, w> f284f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f285g;

    /* renamed from: h  reason: collision with root package name */
    private x f286h;
    private boolean i;
    private boolean j;

    public abstract E g();

    FragmentHostCallback(FragmentActivity fragmentActivity) {
        this(fragmentActivity, fragmentActivity, fragmentActivity.f268c, 0);
    }

    FragmentHostCallback(Activity activity, Context context, Handler handler, int i2) {
        this.f282d = new r();
        this.f279a = activity;
        this.f280b = context;
        this.f283e = handler;
        this.f281c = i2;
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public boolean a(Fragment fragment) {
        return true;
    }

    public LayoutInflater b() {
        return (LayoutInflater) this.f280b.getSystemService("layout_inflater");
    }

    public void d() {
    }

    public void a(Fragment fragment, Intent intent, int i2, Bundle bundle) {
        if (i2 != -1) {
            throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
        }
        this.f280b.startActivity(intent);
    }

    public void a(Fragment fragment, IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5, Bundle bundle) {
        if (i2 != -1) {
            throw new IllegalStateException("Starting intent sender with a requestCode requires a FragmentActivity host");
        }
        ActivityCompat.a(this.f279a, intentSender, i2, intent, i3, i4, i5, bundle);
    }

    public void a(Fragment fragment, String[] strArr, int i2) {
    }

    public boolean a(String str) {
        return false;
    }

    public boolean e() {
        return true;
    }

    public int f() {
        return this.f281c;
    }

    public View a(int i2) {
        return null;
    }

    public boolean a() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public Activity h() {
        return this.f279a;
    }

    /* access modifiers changed from: package-private */
    public Context i() {
        return this.f280b;
    }

    /* access modifiers changed from: package-private */
    public Handler j() {
        return this.f283e;
    }

    /* access modifiers changed from: package-private */
    public r k() {
        return this.f282d;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        x xVar;
        if (this.f284f != null && (xVar = (x) this.f284f.get(str)) != null && !xVar.f544f) {
            xVar.h();
            this.f284f.remove(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Fragment fragment) {
    }

    /* access modifiers changed from: package-private */
    public boolean l() {
        return this.f285g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.FragmentHostCallback.a(java.lang.String, boolean, boolean):android.support.v4.app.x
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.FragmentHostCallback.a(android.support.v4.app.Fragment, java.lang.String[], int):void
      android.support.v4.app.FragmentHostCallback.a(java.lang.String, boolean, boolean):android.support.v4.app.x */
    /* access modifiers changed from: package-private */
    public void m() {
        if (!this.j) {
            this.j = true;
            if (this.f286h != null) {
                this.f286h.b();
            } else if (!this.i) {
                this.f286h = a("(root)", this.j, false);
                if (this.f286h != null && !this.f286h.f543e) {
                    this.f286h.b();
                }
            }
            this.i = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f285g = z;
        if (this.f286h != null && this.j) {
            this.j = false;
            if (z) {
                this.f286h.d();
            } else {
                this.f286h.c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void n() {
        if (this.f286h != null) {
            this.f286h.h();
        }
    }

    /* access modifiers changed from: package-private */
    public void o() {
        if (this.f284f != null) {
            int size = this.f284f.size();
            x[] xVarArr = new x[size];
            for (int i2 = size - 1; i2 >= 0; i2--) {
                xVarArr[i2] = (x) this.f284f.c(i2);
            }
            for (int i3 = 0; i3 < size; i3++) {
                x xVar = xVarArr[i3];
                xVar.e();
                xVar.g();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public x a(String str, boolean z, boolean z2) {
        if (this.f284f == null) {
            this.f284f = new i<>();
        }
        x xVar = (x) this.f284f.get(str);
        if (xVar == null && z2) {
            x xVar2 = new x(str, this, z);
            this.f284f.put(str, xVar2);
            return xVar2;
        } else if (!z || xVar == null || xVar.f543e) {
            return xVar;
        } else {
            xVar.b();
            return xVar;
        }
    }

    /* access modifiers changed from: package-private */
    public i<String, w> p() {
        boolean z;
        if (this.f284f != null) {
            int size = this.f284f.size();
            x[] xVarArr = new x[size];
            for (int i2 = size - 1; i2 >= 0; i2--) {
                xVarArr[i2] = (x) this.f284f.c(i2);
            }
            boolean l = l();
            z = false;
            for (int i3 = 0; i3 < size; i3++) {
                x xVar = xVarArr[i3];
                if (!xVar.f544f && l) {
                    if (!xVar.f543e) {
                        xVar.b();
                    }
                    xVar.d();
                }
                if (xVar.f544f) {
                    z = true;
                } else {
                    xVar.h();
                    this.f284f.remove(xVar.f542d);
                }
            }
        } else {
            z = false;
        }
        if (z) {
            return this.f284f;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(i<String, w> iVar) {
        if (iVar != null) {
            int size = iVar.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((x) iVar.c(i2)).a(this);
            }
        }
        this.f284f = iVar;
    }

    /* access modifiers changed from: package-private */
    public void b(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.j);
        if (this.f286h != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.f286h)));
            printWriter.println(":");
            this.f286h.a(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }
}
