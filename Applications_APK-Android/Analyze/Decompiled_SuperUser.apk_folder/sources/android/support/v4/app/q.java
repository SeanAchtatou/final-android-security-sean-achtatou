package android.support.v4.app;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: FragmentManager */
public abstract class q {

    /* compiled from: FragmentManager */
    public interface b {
        void a();
    }

    public abstract Fragment a(String str);

    public abstract t a();

    public abstract void a(int i, int i2);

    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    public abstract boolean b();

    public abstract boolean c();

    /* compiled from: FragmentManager */
    public static abstract class a {
        public void a(q qVar, Fragment fragment, Context context) {
        }

        public void b(q qVar, Fragment fragment, Context context) {
        }

        public void a(q qVar, Fragment fragment, Bundle bundle) {
        }

        public void b(q qVar, Fragment fragment, Bundle bundle) {
        }

        public void a(q qVar, Fragment fragment, View view, Bundle bundle) {
        }

        public void a(q qVar, Fragment fragment) {
        }

        public void b(q qVar, Fragment fragment) {
        }

        public void c(q qVar, Fragment fragment) {
        }

        public void d(q qVar, Fragment fragment) {
        }

        public void c(q qVar, Fragment fragment, Bundle bundle) {
        }

        public void e(q qVar, Fragment fragment) {
        }

        public void f(q qVar, Fragment fragment) {
        }

        public void g(q qVar, Fragment fragment) {
        }
    }
}
