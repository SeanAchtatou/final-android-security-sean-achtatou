package android.support.v4.app;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ag;

public final class RemoteInput extends ag.a {

    /* renamed from: a  reason: collision with root package name */
    public static final ag.a.C0011a f425a = new ag.a.C0011a() {
    };

    /* renamed from: g  reason: collision with root package name */
    private static final a f426g;

    /* renamed from: b  reason: collision with root package name */
    private final String f427b;

    /* renamed from: c  reason: collision with root package name */
    private final CharSequence f428c;

    /* renamed from: d  reason: collision with root package name */
    private final CharSequence[] f429d;

    /* renamed from: e  reason: collision with root package name */
    private final boolean f430e;

    /* renamed from: f  reason: collision with root package name */
    private final Bundle f431f;

    interface a {
    }

    public String a() {
        return this.f427b;
    }

    public CharSequence b() {
        return this.f428c;
    }

    public CharSequence[] c() {
        return this.f429d;
    }

    public boolean d() {
        return this.f430e;
    }

    public Bundle e() {
        return this.f431f;
    }

    static class c implements a {
        c() {
        }
    }

    static class d implements a {
        d() {
        }
    }

    static class b implements a {
        b() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 20) {
            f426g = new b();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f426g = new d();
        } else {
            f426g = new c();
        }
    }
}
