package android.support.v4.app;

import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

/* compiled from: BundleCompat */
public final class l {
    public static IBinder a(Bundle bundle, String str) {
        if (Build.VERSION.SDK_INT >= 18) {
            return n.a(bundle, str);
        }
        return m.a(bundle, str);
    }
}
