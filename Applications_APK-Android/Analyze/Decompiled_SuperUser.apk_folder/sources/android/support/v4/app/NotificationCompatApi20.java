package android.support.v4.app;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ac;
import android.widget.RemoteViews;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.ArrayList;

@TargetApi(20)
class NotificationCompatApi20 {

    public static class Builder implements aa, ab {

        /* renamed from: a  reason: collision with root package name */
        private Notification.Builder f372a;

        /* renamed from: b  reason: collision with root package name */
        private Bundle f373b;

        /* renamed from: c  reason: collision with root package name */
        private RemoteViews f374c;

        /* renamed from: d  reason: collision with root package name */
        private RemoteViews f375d;

        public Builder(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, ArrayList<String> arrayList, Bundle bundle, String str, boolean z5, String str2, RemoteViews remoteViews2, RemoteViews remoteViews3) {
            this.f372a = new Notification.Builder(context).setWhen(notification.when).setShowWhen(z2).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, remoteViews).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(pendingIntent2, (notification.flags & FileUtils.FileMode.MODE_IWUSR) != 0).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z3).setPriority(i4).setProgress(i2, i3, z).setLocalOnly(z4).setGroup(str).setGroupSummary(z5).setSortKey(str2);
            this.f373b = new Bundle();
            if (bundle != null) {
                this.f373b.putAll(bundle);
            }
            if (arrayList != null && !arrayList.isEmpty()) {
                this.f373b.putStringArray("android.people", (String[]) arrayList.toArray(new String[arrayList.size()]));
            }
            this.f374c = remoteViews2;
            this.f375d = remoteViews3;
        }

        public void a(ac.a aVar) {
            NotificationCompatApi20.a(this.f372a, aVar);
        }

        public Notification.Builder a() {
            return this.f372a;
        }

        public Notification b() {
            this.f372a.setExtras(this.f373b);
            Notification build = this.f372a.build();
            if (this.f374c != null) {
                build.contentView = this.f374c;
            }
            if (this.f375d != null) {
                build.bigContentView = this.f375d;
            }
            return build;
        }
    }

    public static void a(Notification.Builder builder, ac.a aVar) {
        Bundle bundle;
        Notification.Action.Builder builder2 = new Notification.Action.Builder(aVar.a(), aVar.b(), aVar.c());
        if (aVar.g() != null) {
            for (RemoteInput addRemoteInput : af.a(aVar.g())) {
                builder2.addRemoteInput(addRemoteInput);
            }
        }
        if (aVar.d() != null) {
            bundle = new Bundle(aVar.d());
        } else {
            bundle = new Bundle();
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", aVar.e());
        builder2.addExtras(bundle);
        builder.addAction(builder2.build());
    }
}
