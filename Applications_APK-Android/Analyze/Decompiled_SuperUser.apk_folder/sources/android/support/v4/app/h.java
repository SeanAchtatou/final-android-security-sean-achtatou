package android.support.v4.app;

import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.r;
import android.support.v4.util.d;
import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/* compiled from: BackStackRecord */
final class h extends t implements r.c {

    /* renamed from: a  reason: collision with root package name */
    static final boolean f447a = (Build.VERSION.SDK_INT >= 21);

    /* renamed from: b  reason: collision with root package name */
    final r f448b;

    /* renamed from: c  reason: collision with root package name */
    ArrayList<a> f449c = new ArrayList<>();

    /* renamed from: d  reason: collision with root package name */
    int f450d;

    /* renamed from: e  reason: collision with root package name */
    int f451e;

    /* renamed from: f  reason: collision with root package name */
    int f452f;

    /* renamed from: g  reason: collision with root package name */
    int f453g;

    /* renamed from: h  reason: collision with root package name */
    int f454h;
    int i;
    boolean j;
    boolean k = true;
    String l;
    boolean m;
    int n = -1;
    int o;
    CharSequence p;
    int q;
    CharSequence r;
    ArrayList<String> s;
    ArrayList<String> t;
    boolean u = false;

    /* compiled from: BackStackRecord */
    static final class a {

        /* renamed from: a  reason: collision with root package name */
        int f455a;

        /* renamed from: b  reason: collision with root package name */
        Fragment f456b;

        /* renamed from: c  reason: collision with root package name */
        int f457c;

        /* renamed from: d  reason: collision with root package name */
        int f458d;

        /* renamed from: e  reason: collision with root package name */
        int f459e;

        /* renamed from: f  reason: collision with root package name */
        int f460f;

        a() {
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((int) FileUtils.FileMode.MODE_IWUSR);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.n >= 0) {
            sb.append(" #");
            sb.append(this.n);
        }
        if (this.l != null) {
            sb.append(" ");
            sb.append(this.l);
        }
        sb.append("}");
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.h.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.h.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.t
      android.support.v4.app.h.a(java.util.ArrayList<android.support.v4.app.h>, int, int):boolean
      android.support.v4.app.t.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.t
      android.support.v4.app.h.a(java.lang.String, java.io.PrintWriter, boolean):void */
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        a(str, printWriter, true);
    }

    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.l);
            printWriter.print(" mIndex=");
            printWriter.print(this.n);
            printWriter.print(" mCommitted=");
            printWriter.println(this.m);
            if (this.f454h != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.f454h));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.i));
            }
            if (!(this.f450d == 0 && this.f451e == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f450d));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.f451e));
            }
            if (!(this.f452f == 0 && this.f453g == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.f452f));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.f453g));
            }
            if (!(this.o == 0 && this.p == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.o));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.p);
            }
            if (!(this.q == 0 && this.r == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.q));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.r);
            }
        }
        if (!this.f449c.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            str + "    ";
            int size = this.f449c.size();
            for (int i2 = 0; i2 < size; i2++) {
                a aVar = this.f449c.get(i2);
                switch (aVar.f455a) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    default:
                        str2 = "cmd=" + aVar.f455a;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(aVar.f456b);
                if (z) {
                    if (!(aVar.f457c == 0 && aVar.f458d == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.f457c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.f458d));
                    }
                    if (aVar.f459e != 0 || aVar.f460f != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.f459e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.f460f));
                    }
                }
            }
        }
    }

    public h(r rVar) {
        this.f448b = rVar;
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.f449c.add(aVar);
        aVar.f457c = this.f450d;
        aVar.f458d = this.f451e;
        aVar.f459e = this.f452f;
        aVar.f460f = this.f453g;
    }

    public t a(Fragment fragment, String str) {
        a(0, fragment, str, 1);
        return this;
    }

    public t a(int i2, Fragment fragment, String str) {
        a(i2, fragment, str, 1);
        return this;
    }

    private void a(int i2, Fragment fragment, String str, int i3) {
        Class<?> cls = fragment.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            throw new IllegalStateException("Fragment " + cls.getCanonicalName() + " must be a public static class to be  properly recreated from" + " instance state.");
        }
        fragment.mFragmentManager = this.f448b;
        if (str != null) {
            if (fragment.mTag == null || str.equals(fragment.mTag)) {
                fragment.mTag = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.mTag + " now " + str);
            }
        }
        if (i2 != 0) {
            if (i2 == -1) {
                throw new IllegalArgumentException("Can't add fragment " + fragment + " with tag " + str + " to container view with no id");
            } else if (fragment.mFragmentId == 0 || fragment.mFragmentId == i2) {
                fragment.mFragmentId = i2;
                fragment.mContainerId = i2;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.mFragmentId + " now " + i2);
            }
        }
        a aVar = new a();
        aVar.f455a = i3;
        aVar.f456b = fragment;
        a(aVar);
    }

    public t a(Fragment fragment) {
        a aVar = new a();
        aVar.f455a = 3;
        aVar.f456b = fragment;
        a(aVar);
        return this;
    }

    public t b(Fragment fragment) {
        a aVar = new a();
        aVar.f455a = 6;
        aVar.f456b = fragment;
        a(aVar);
        return this;
    }

    public t c(Fragment fragment) {
        a aVar = new a();
        aVar.f455a = 7;
        aVar.f456b = fragment;
        a(aVar);
        return this;
    }

    public t a() {
        if (this.j) {
            throw new IllegalStateException("This transaction is already being added to the back stack");
        }
        this.k = false;
        return this;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (this.j) {
            if (r.f466a) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i2);
            }
            int size = this.f449c.size();
            for (int i3 = 0; i3 < size; i3++) {
                a aVar = this.f449c.get(i3);
                if (aVar.f456b != null) {
                    aVar.f456b.mBackStackNesting += i2;
                    if (r.f466a) {
                        Log.v("FragmentManager", "Bump nesting of " + aVar.f456b + " to " + aVar.f456b.mBackStackNesting);
                    }
                }
            }
        }
    }

    public int b() {
        return a(false);
    }

    public int c() {
        return a(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.b(android.support.v4.app.r$c, boolean):void
     arg types: [android.support.v4.app.h, int]
     candidates:
      android.support.v4.app.r.b(int, boolean):int
      android.support.v4.app.r.b(android.view.View, android.view.animation.Animation):void
      android.support.v4.app.r.b(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.b(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.b(android.support.v4.app.r$c, boolean):void */
    public void d() {
        a();
        this.f448b.b((r.c) this, true);
    }

    /* access modifiers changed from: package-private */
    public int a(boolean z) {
        if (this.m) {
            throw new IllegalStateException("commit already called");
        }
        if (r.f466a) {
            Log.v("FragmentManager", "Commit: " + this);
            PrintWriter printWriter = new PrintWriter(new d("FragmentManager"));
            a("  ", (FileDescriptor) null, printWriter, (String[]) null);
            printWriter.close();
        }
        this.m = true;
        if (this.j) {
            this.n = this.f448b.a(this);
        } else {
            this.n = -1;
        }
        this.f448b.a(this, z);
        return this.n;
    }

    public boolean a(ArrayList<h> arrayList, ArrayList<Boolean> arrayList2) {
        if (r.f466a) {
            Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(false);
        if (!this.j) {
            return true;
        }
        this.f448b.b(this);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i2) {
        int size = this.f449c.size();
        for (int i3 = 0; i3 < size; i3++) {
            if (this.f449c.get(i3).f456b.mContainerId == i2) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(ArrayList<h> arrayList, int i2, int i3) {
        int i4;
        if (i3 == i2) {
            return false;
        }
        int size = this.f449c.size();
        int i5 = -1;
        int i6 = 0;
        while (i6 < size) {
            int i7 = this.f449c.get(i6).f456b.mContainerId;
            if (i7 == 0 || i7 == i5) {
                i4 = i5;
            } else {
                for (int i8 = i2; i8 < i3; i8++) {
                    h hVar = arrayList.get(i8);
                    int size2 = hVar.f449c.size();
                    for (int i9 = 0; i9 < size2; i9++) {
                        if (hVar.f449c.get(i9).f456b.mContainerId == i7) {
                            return true;
                        }
                    }
                }
                i4 = i7;
            }
            i6++;
            i5 = i4;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(int, boolean):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public void e() {
        int size = this.f449c.size();
        for (int i2 = 0; i2 < size; i2++) {
            a aVar = this.f449c.get(i2);
            Fragment fragment = aVar.f456b;
            fragment.setNextTransition(this.f454h, this.i);
            switch (aVar.f455a) {
                case 1:
                    fragment.setNextAnim(aVar.f457c);
                    this.f448b.a(fragment, false);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.f455a);
                case 3:
                    fragment.setNextAnim(aVar.f458d);
                    this.f448b.g(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.f458d);
                    this.f448b.h(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.f457c);
                    this.f448b.i(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.f458d);
                    this.f448b.j(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.f457c);
                    this.f448b.k(fragment);
                    break;
            }
            if (!this.u && aVar.f455a != 1) {
                this.f448b.d(fragment);
            }
        }
        if (!this.u) {
            this.f448b.a(this.f448b.m, true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(int, boolean):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        for (int size = this.f449c.size() - 1; size >= 0; size--) {
            a aVar = this.f449c.get(size);
            Fragment fragment = aVar.f456b;
            fragment.setNextTransition(r.d(this.f454h), this.i);
            switch (aVar.f455a) {
                case 1:
                    fragment.setNextAnim(aVar.f460f);
                    this.f448b.g(fragment);
                    break;
                case 2:
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.f455a);
                case 3:
                    fragment.setNextAnim(aVar.f459e);
                    this.f448b.a(fragment, false);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.f459e);
                    this.f448b.i(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.f460f);
                    this.f448b.h(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.f459e);
                    this.f448b.k(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.f460f);
                    this.f448b.j(fragment);
                    break;
            }
            if (!this.u && aVar.f455a != 3) {
                this.f448b.d(fragment);
            }
        }
        if (!this.u && z) {
            this.f448b.a(this.f448b.m, true);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList<Fragment> arrayList) {
        boolean z;
        int i2;
        int i3 = 0;
        while (i3 < this.f449c.size()) {
            a aVar = this.f449c.get(i3);
            switch (aVar.f455a) {
                case 1:
                case 7:
                    arrayList.add(aVar.f456b);
                    break;
                case 2:
                    Fragment fragment = aVar.f456b;
                    int i4 = fragment.mContainerId;
                    int size = arrayList.size() - 1;
                    int i5 = i3;
                    boolean z2 = false;
                    while (size >= 0) {
                        Fragment fragment2 = arrayList.get(size);
                        if (fragment2.mContainerId != i4) {
                            z = z2;
                            i2 = i5;
                        } else if (fragment2 == fragment) {
                            z = true;
                            i2 = i5;
                        } else {
                            a aVar2 = new a();
                            aVar2.f455a = 3;
                            aVar2.f456b = fragment2;
                            aVar2.f457c = aVar.f457c;
                            aVar2.f459e = aVar.f459e;
                            aVar2.f458d = aVar.f458d;
                            aVar2.f460f = aVar.f460f;
                            this.f449c.add(i5, aVar2);
                            arrayList.remove(fragment2);
                            boolean z3 = z2;
                            i2 = i5 + 1;
                            z = z3;
                        }
                        size--;
                        i5 = i2;
                        z2 = z;
                    }
                    if (!z2) {
                        aVar.f455a = 1;
                        arrayList.add(fragment);
                        i3 = i5;
                        break;
                    } else {
                        this.f449c.remove(i5);
                        i3 = i5 - 1;
                        break;
                    }
                case 3:
                case 6:
                    arrayList.remove(aVar.f456b);
                    break;
            }
            i3++;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(ArrayList<Fragment> arrayList) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f449c.size()) {
                a aVar = this.f449c.get(i3);
                switch (aVar.f455a) {
                    case 1:
                    case 7:
                        arrayList.remove(aVar.f456b);
                        break;
                    case 3:
                    case 6:
                        arrayList.add(aVar.f456b);
                        break;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        for (int i2 = 0; i2 < this.f449c.size(); i2++) {
            if (b(this.f449c.get(i2))) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(Fragment.b bVar) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f449c.size()) {
                a aVar = this.f449c.get(i3);
                if (b(aVar)) {
                    aVar.f456b.setOnStartEnterTransitionListener(bVar);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private static boolean b(a aVar) {
        Fragment fragment = aVar.f456b;
        return fragment.mAdded && fragment.mView != null && !fragment.mDetached && !fragment.mHidden && fragment.isPostponed();
    }

    public String g() {
        return this.l;
    }

    public boolean h() {
        return this.f449c.isEmpty();
    }
}
