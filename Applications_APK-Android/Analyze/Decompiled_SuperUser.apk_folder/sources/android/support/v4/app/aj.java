package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: TaskStackBuilder */
public final class aj implements Iterable<Intent> {

    /* renamed from: a  reason: collision with root package name */
    private static final b f442a;

    /* renamed from: b  reason: collision with root package name */
    private final ArrayList<Intent> f443b = new ArrayList<>();

    /* renamed from: c  reason: collision with root package name */
    private final Context f444c;

    /* compiled from: TaskStackBuilder */
    public interface a {
        Intent a();
    }

    /* compiled from: TaskStackBuilder */
    interface b {
    }

    /* compiled from: TaskStackBuilder */
    static class c implements b {
        c() {
        }
    }

    /* compiled from: TaskStackBuilder */
    static class d implements b {
        d() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f442a = new d();
        } else {
            f442a = new c();
        }
    }

    private aj(Context context) {
        this.f444c = context;
    }

    public static aj a(Context context) {
        return new aj(context);
    }

    public aj a(Intent intent) {
        this.f443b.add(intent);
        return this;
    }

    public aj a(Activity activity) {
        Intent intent;
        Intent intent2 = null;
        if (activity instanceof a) {
            intent2 = ((a) activity).a();
        }
        if (intent2 == null) {
            intent = y.a(activity);
        } else {
            intent = intent2;
        }
        if (intent != null) {
            ComponentName component = intent.getComponent();
            if (component == null) {
                component = intent.resolveActivity(this.f444c.getPackageManager());
            }
            a(component);
            a(intent);
        }
        return this;
    }

    public aj a(ComponentName componentName) {
        int size = this.f443b.size();
        try {
            Intent a2 = y.a(this.f444c, componentName);
            while (a2 != null) {
                this.f443b.add(size, a2);
                a2 = y.a(this.f444c, a2.getComponent());
            }
            return this;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(e2);
        }
    }

    @Deprecated
    public Iterator<Intent> iterator() {
        return this.f443b.iterator();
    }

    public void a() {
        a((Bundle) null);
    }

    public void a(Bundle bundle) {
        if (this.f443b.isEmpty()) {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
        }
        Intent[] intentArr = (Intent[]) this.f443b.toArray(new Intent[this.f443b.size()]);
        intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
        if (!android.support.v4.content.c.a(this.f444c, intentArr, bundle)) {
            Intent intent = new Intent(intentArr[intentArr.length - 1]);
            intent.addFlags(268435456);
            this.f444c.startActivity(intent);
        }
    }
}
