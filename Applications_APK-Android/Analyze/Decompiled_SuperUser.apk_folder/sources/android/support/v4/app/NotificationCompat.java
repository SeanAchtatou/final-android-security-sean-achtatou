package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompatApi20;
import android.support.v4.app.NotificationCompatApi21;
import android.support.v4.app.NotificationCompatApi24;
import android.support.v4.app.NotificationCompatIceCreamSandwich;
import android.support.v4.app.NotificationCompatJellybean;
import android.support.v4.app.NotificationCompatKitKat;
import android.support.v4.app.ac;
import android.widget.RemoteViews;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NotificationCompat {

    /* renamed from: a  reason: collision with root package name */
    static final b f325a;

    public static class BigPictureStyle extends k {

        /* renamed from: a  reason: collision with root package name */
        Bitmap f337a;

        /* renamed from: b  reason: collision with root package name */
        Bitmap f338b;

        /* renamed from: c  reason: collision with root package name */
        boolean f339c;
    }

    public static final class CarExtender {

        /* renamed from: a  reason: collision with root package name */
        private int f349a = 0;

        public static class UnreadConversation extends ac.b {

            /* renamed from: a  reason: collision with root package name */
            static final ac.b.a f350a = new ac.b.a() {
            };
        }
    }

    public static class InboxStyle extends k {

        /* renamed from: a  reason: collision with root package name */
        ArrayList<CharSequence> f351a = new ArrayList<>();
    }

    interface b {
        Notification a(Builder builder, a aVar);

        Bundle a(Notification notification);
    }

    protected static class a {
        protected a() {
        }

        public Notification a(Builder builder, ab abVar) {
            Notification b2 = abVar.b();
            if (builder.C != null) {
                b2.contentView = builder.C;
            }
            return b2;
        }
    }

    static class f implements b {
        f() {
        }

        public Notification a(Builder builder, a aVar) {
            Notification a2 = ac.a(builder.F, builder.f341a, builder.k(), builder.j(), builder.f344d, builder.f345e);
            if (builder.j > 0) {
                a2.flags |= FileUtils.FileMode.MODE_IWUSR;
            }
            if (builder.C != null) {
                a2.contentView = builder.C;
            }
            return a2;
        }

        public Bundle a(Notification notification) {
            return null;
        }
    }

    static class g extends f {
        g() {
        }

        public Notification a(Builder builder, a aVar) {
            Notification a2 = ad.a(builder.f341a, builder.F, builder.k(), builder.j(), builder.f348h, builder.f346f, builder.i, builder.f344d, builder.f345e, builder.f347g);
            if (builder.C != null) {
                a2.contentView = builder.C;
            }
            return a2;
        }
    }

    static class h extends f {
        h() {
        }

        public Notification a(Builder builder, a aVar) {
            return aVar.a(builder, new NotificationCompatIceCreamSandwich.Builder(builder.f341a, builder.F, builder.k(), builder.j(), builder.f348h, builder.f346f, builder.i, builder.f344d, builder.f345e, builder.f347g, builder.p, builder.q, builder.r));
        }
    }

    static class i extends f {
        i() {
        }

        public Notification a(Builder builder, a aVar) {
            Bundle a2;
            NotificationCompatJellybean.Builder builder2 = new NotificationCompatJellybean.Builder(builder.f341a, builder.F, builder.k(), builder.j(), builder.f348h, builder.f346f, builder.i, builder.f344d, builder.f345e, builder.f347g, builder.p, builder.q, builder.r, builder.l, builder.j, builder.n, builder.w, builder.y, builder.s, builder.t, builder.u, builder.C, builder.D);
            NotificationCompat.a(builder2, builder.v);
            NotificationCompat.a(builder2, builder.m);
            Notification a3 = aVar.a(builder, builder2);
            if (!(builder.m == null || (a2 = a(a3)) == null)) {
                builder.m.a(a2);
            }
            return a3;
        }

        public Bundle a(Notification notification) {
            return NotificationCompatJellybean.a(notification);
        }
    }

    static class j extends i {
        j() {
        }

        public Notification a(Builder builder, a aVar) {
            NotificationCompatKitKat.Builder builder2 = new NotificationCompatKitKat.Builder(builder.f341a, builder.F, builder.k(), builder.j(), builder.f348h, builder.f346f, builder.i, builder.f344d, builder.f345e, builder.f347g, builder.p, builder.q, builder.r, builder.k, builder.l, builder.j, builder.n, builder.w, builder.G, builder.y, builder.s, builder.t, builder.u, builder.C, builder.D);
            NotificationCompat.a(builder2, builder.v);
            NotificationCompat.a(builder2, builder.m);
            return aVar.a(builder, builder2);
        }

        public Bundle a(Notification notification) {
            return NotificationCompatKitKat.a(notification);
        }
    }

    static class c extends j {
        c() {
        }

        public Notification a(Builder builder, a aVar) {
            NotificationCompatApi20.Builder builder2 = new NotificationCompatApi20.Builder(builder.f341a, builder.F, builder.k(), builder.j(), builder.f348h, builder.f346f, builder.i, builder.f344d, builder.f345e, builder.f347g, builder.p, builder.q, builder.r, builder.k, builder.l, builder.j, builder.n, builder.w, builder.G, builder.y, builder.s, builder.t, builder.u, builder.C, builder.D);
            NotificationCompat.a(builder2, builder.v);
            NotificationCompat.a(builder2, builder.m);
            Notification a2 = aVar.a(builder, builder2);
            if (builder.m != null) {
                builder.m.a(a(a2));
            }
            return a2;
        }
    }

    static class d extends c {
        d() {
        }

        public Notification a(Builder builder, a aVar) {
            NotificationCompatApi21.Builder builder2 = new NotificationCompatApi21.Builder(builder.f341a, builder.F, builder.k(), builder.j(), builder.f348h, builder.f346f, builder.i, builder.f344d, builder.f345e, builder.f347g, builder.p, builder.q, builder.r, builder.k, builder.l, builder.j, builder.n, builder.w, builder.x, builder.G, builder.y, builder.z, builder.A, builder.B, builder.s, builder.t, builder.u, builder.C, builder.D, builder.E);
            NotificationCompat.a(builder2, builder.v);
            NotificationCompat.a(builder2, builder.m);
            Notification a2 = aVar.a(builder, builder2);
            if (builder.m != null) {
                builder.m.a(a(a2));
            }
            return a2;
        }
    }

    static class e extends d {
        e() {
        }

        public Notification a(Builder builder, a aVar) {
            NotificationCompatApi24.Builder builder2 = new NotificationCompatApi24.Builder(builder.f341a, builder.F, builder.f342b, builder.f343c, builder.f348h, builder.f346f, builder.i, builder.f344d, builder.f345e, builder.f347g, builder.p, builder.q, builder.r, builder.k, builder.l, builder.j, builder.n, builder.w, builder.x, builder.G, builder.y, builder.z, builder.A, builder.B, builder.s, builder.t, builder.u, builder.o, builder.C, builder.D, builder.E);
            NotificationCompat.a(builder2, builder.v);
            NotificationCompat.b(builder2, builder.m);
            Notification a2 = aVar.a(builder, builder2);
            if (builder.m != null) {
                builder.m.a(a(a2));
            }
            return a2;
        }
    }

    static void a(aa aaVar, ArrayList<Action> arrayList) {
        Iterator<Action> it = arrayList.iterator();
        while (it.hasNext()) {
            aaVar.a(it.next());
        }
    }

    static void a(ab abVar, k kVar) {
        if (kVar == null) {
            return;
        }
        if (kVar instanceof BigTextStyle) {
            BigTextStyle bigTextStyle = (BigTextStyle) kVar;
            NotificationCompatJellybean.a(abVar, bigTextStyle.f361e, bigTextStyle.f363g, bigTextStyle.f362f, bigTextStyle.f340a);
        } else if (kVar instanceof InboxStyle) {
            InboxStyle inboxStyle = (InboxStyle) kVar;
            NotificationCompatJellybean.a(abVar, inboxStyle.f361e, inboxStyle.f363g, inboxStyle.f362f, inboxStyle.f351a);
        } else if (kVar instanceof BigPictureStyle) {
            BigPictureStyle bigPictureStyle = (BigPictureStyle) kVar;
            NotificationCompatJellybean.a(abVar, bigPictureStyle.f361e, bigPictureStyle.f363g, bigPictureStyle.f362f, bigPictureStyle.f337a, bigPictureStyle.f338b, bigPictureStyle.f339c);
        }
    }

    static void b(ab abVar, k kVar) {
        if (kVar == null) {
            return;
        }
        if (kVar instanceof MessagingStyle) {
            MessagingStyle messagingStyle = (MessagingStyle) kVar;
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            ArrayList arrayList5 = new ArrayList();
            for (MessagingStyle.a next : messagingStyle.f354c) {
                arrayList.add(next.a());
                arrayList2.add(Long.valueOf(next.b()));
                arrayList3.add(next.c());
                arrayList4.add(next.d());
                arrayList5.add(next.e());
            }
            NotificationCompatApi24.a(abVar, messagingStyle.f352a, messagingStyle.f353b, arrayList, arrayList2, arrayList3, arrayList4, arrayList5);
            return;
        }
        a(abVar, kVar);
    }

    static {
        if (android.support.v4.os.c.a()) {
            f325a = new e();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f325a = new d();
        } else if (Build.VERSION.SDK_INT >= 20) {
            f325a = new c();
        } else if (Build.VERSION.SDK_INT >= 19) {
            f325a = new j();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f325a = new i();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f325a = new h();
        } else if (Build.VERSION.SDK_INT >= 11) {
            f325a = new g();
        } else {
            f325a = new f();
        }
    }

    public static class Builder {
        int A = 0;
        Notification B;
        RemoteViews C;
        RemoteViews D;
        RemoteViews E;
        public Notification F = new Notification();
        public ArrayList<String> G;

        /* renamed from: a  reason: collision with root package name */
        public Context f341a;

        /* renamed from: b  reason: collision with root package name */
        public CharSequence f342b;

        /* renamed from: c  reason: collision with root package name */
        public CharSequence f343c;

        /* renamed from: d  reason: collision with root package name */
        PendingIntent f344d;

        /* renamed from: e  reason: collision with root package name */
        PendingIntent f345e;

        /* renamed from: f  reason: collision with root package name */
        RemoteViews f346f;

        /* renamed from: g  reason: collision with root package name */
        public Bitmap f347g;

        /* renamed from: h  reason: collision with root package name */
        public CharSequence f348h;
        public int i;
        int j;
        boolean k = true;
        public boolean l;
        public k m;
        public CharSequence n;
        public CharSequence[] o;
        int p;
        int q;
        boolean r;
        String s;
        boolean t;
        String u;
        public ArrayList<Action> v = new ArrayList<>();
        boolean w = false;
        String x;
        Bundle y;
        int z = 0;

        public Builder(Context context) {
            this.f341a = context;
            this.F.when = System.currentTimeMillis();
            this.F.audioStreamType = -1;
            this.j = 0;
            this.G = new ArrayList<>();
        }

        public Builder a(long j2) {
            this.F.when = j2;
            return this;
        }

        public Builder a(int i2) {
            this.F.icon = i2;
            return this;
        }

        public Builder a(CharSequence charSequence) {
            this.f342b = d(charSequence);
            return this;
        }

        public Builder b(CharSequence charSequence) {
            this.f343c = d(charSequence);
            return this;
        }

        public Builder a(RemoteViews remoteViews) {
            this.F.contentView = remoteViews;
            return this;
        }

        public Builder a(PendingIntent pendingIntent) {
            this.f344d = pendingIntent;
            return this;
        }

        public Builder c(CharSequence charSequence) {
            this.F.tickerText = d(charSequence);
            return this;
        }

        public Builder a(boolean z2) {
            a(2, z2);
            return this;
        }

        public Builder b(boolean z2) {
            a(16, z2);
            return this;
        }

        public Builder c(boolean z2) {
            this.w = z2;
            return this;
        }

        private void a(int i2, boolean z2) {
            if (z2) {
                this.F.flags |= i2;
                return;
            }
            this.F.flags &= i2 ^ -1;
        }

        public Builder b(int i2) {
            this.j = i2;
            return this;
        }

        public Builder a(k kVar) {
            if (this.m != kVar) {
                this.m = kVar;
                if (this.m != null) {
                    this.m.a(this);
                }
            }
            return this;
        }

        public Builder b(RemoteViews remoteViews) {
            this.D = remoteViews;
            return this;
        }

        @Deprecated
        public Notification a() {
            return b();
        }

        public Notification b() {
            return NotificationCompat.f325a.a(this, c());
        }

        /* access modifiers changed from: protected */
        public a c() {
            return new a();
        }

        protected static CharSequence d(CharSequence charSequence) {
            if (charSequence != null && charSequence.length() > 5120) {
                return charSequence.subSequence(0, 5120);
            }
            return charSequence;
        }

        public RemoteViews d() {
            return this.C;
        }

        public RemoteViews e() {
            return this.D;
        }

        public RemoteViews f() {
            return this.E;
        }

        public long g() {
            if (this.k) {
                return this.F.when;
            }
            return 0;
        }

        public int h() {
            return this.j;
        }

        public int i() {
            return this.z;
        }

        /* access modifiers changed from: protected */
        public CharSequence j() {
            return this.f343c;
        }

        /* access modifiers changed from: protected */
        public CharSequence k() {
            return this.f342b;
        }
    }

    public static abstract class k {

        /* renamed from: d  reason: collision with root package name */
        Builder f360d;

        /* renamed from: e  reason: collision with root package name */
        CharSequence f361e;

        /* renamed from: f  reason: collision with root package name */
        CharSequence f362f;

        /* renamed from: g  reason: collision with root package name */
        boolean f363g = false;

        public void a(Builder builder) {
            if (this.f360d != builder) {
                this.f360d = builder;
                if (this.f360d != null) {
                    this.f360d.a(this);
                }
            }
        }

        public void a(Bundle bundle) {
        }
    }

    public static class BigTextStyle extends k {

        /* renamed from: a  reason: collision with root package name */
        CharSequence f340a;

        public BigTextStyle a(CharSequence charSequence) {
            this.f340a = Builder.d(charSequence);
            return this;
        }
    }

    public static class MessagingStyle extends k {

        /* renamed from: a  reason: collision with root package name */
        CharSequence f352a;

        /* renamed from: b  reason: collision with root package name */
        CharSequence f353b;

        /* renamed from: c  reason: collision with root package name */
        List<a> f354c = new ArrayList();

        MessagingStyle() {
        }

        public CharSequence a() {
            return this.f352a;
        }

        public CharSequence b() {
            return this.f353b;
        }

        public List<a> c() {
            return this.f354c;
        }

        public void a(Bundle bundle) {
            super.a(bundle);
            if (this.f352a != null) {
                bundle.putCharSequence("android.selfDisplayName", this.f352a);
            }
            if (this.f353b != null) {
                bundle.putCharSequence("android.conversationTitle", this.f353b);
            }
            if (!this.f354c.isEmpty()) {
                bundle.putParcelableArray("android.messages", a.a(this.f354c));
            }
        }

        public static final class a {

            /* renamed from: a  reason: collision with root package name */
            private final CharSequence f355a;

            /* renamed from: b  reason: collision with root package name */
            private final long f356b;

            /* renamed from: c  reason: collision with root package name */
            private final CharSequence f357c;

            /* renamed from: d  reason: collision with root package name */
            private String f358d;

            /* renamed from: e  reason: collision with root package name */
            private Uri f359e;

            public CharSequence a() {
                return this.f355a;
            }

            public long b() {
                return this.f356b;
            }

            public CharSequence c() {
                return this.f357c;
            }

            public String d() {
                return this.f358d;
            }

            public Uri e() {
                return this.f359e;
            }

            private Bundle f() {
                Bundle bundle = new Bundle();
                if (this.f355a != null) {
                    bundle.putCharSequence("text", this.f355a);
                }
                bundle.putLong("time", this.f356b);
                if (this.f357c != null) {
                    bundle.putCharSequence("sender", this.f357c);
                }
                if (this.f358d != null) {
                    bundle.putString("type", this.f358d);
                }
                if (this.f359e != null) {
                    bundle.putParcelable("uri", this.f359e);
                }
                return bundle;
            }

            static Bundle[] a(List<a> list) {
                Bundle[] bundleArr = new Bundle[list.size()];
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    bundleArr[i] = list.get(i).f();
                }
                return bundleArr;
            }
        }
    }

    public static class Action extends ac.a {

        /* renamed from: e  reason: collision with root package name */
        public static final ac.a.C0010a f326e = new ac.a.C0010a() {
        };

        /* renamed from: a  reason: collision with root package name */
        final Bundle f327a;

        /* renamed from: b  reason: collision with root package name */
        public int f328b;

        /* renamed from: c  reason: collision with root package name */
        public CharSequence f329c;

        /* renamed from: d  reason: collision with root package name */
        public PendingIntent f330d;

        /* renamed from: f  reason: collision with root package name */
        private final RemoteInput[] f331f;

        /* renamed from: g  reason: collision with root package name */
        private boolean f332g;

        public int a() {
            return this.f328b;
        }

        public CharSequence b() {
            return this.f329c;
        }

        public PendingIntent c() {
            return this.f330d;
        }

        public Bundle d() {
            return this.f327a;
        }

        public boolean e() {
            return this.f332g;
        }

        /* renamed from: f */
        public RemoteInput[] g() {
            return this.f331f;
        }

        public static final class a {

            /* renamed from: a  reason: collision with root package name */
            private int f333a = 1;

            /* renamed from: b  reason: collision with root package name */
            private CharSequence f334b;

            /* renamed from: c  reason: collision with root package name */
            private CharSequence f335c;

            /* renamed from: d  reason: collision with root package name */
            private CharSequence f336d;

            /* renamed from: a */
            public a clone() {
                a aVar = new a();
                aVar.f333a = this.f333a;
                aVar.f334b = this.f334b;
                aVar.f335c = this.f335c;
                aVar.f336d = this.f336d;
                return aVar;
            }
        }
    }

    public static final class l {

        /* renamed from: a  reason: collision with root package name */
        private ArrayList<Action> f364a = new ArrayList<>();

        /* renamed from: b  reason: collision with root package name */
        private int f365b = 1;

        /* renamed from: c  reason: collision with root package name */
        private PendingIntent f366c;

        /* renamed from: d  reason: collision with root package name */
        private ArrayList<Notification> f367d = new ArrayList<>();

        /* renamed from: e  reason: collision with root package name */
        private Bitmap f368e;

        /* renamed from: f  reason: collision with root package name */
        private int f369f;

        /* renamed from: g  reason: collision with root package name */
        private int f370g = 8388613;

        /* renamed from: h  reason: collision with root package name */
        private int f371h = -1;
        private int i = 0;
        private int j;
        private int k = 80;
        private int l;
        private String m;
        private String n;

        /* renamed from: a */
        public l clone() {
            l lVar = new l();
            lVar.f364a = new ArrayList<>(this.f364a);
            lVar.f365b = this.f365b;
            lVar.f366c = this.f366c;
            lVar.f367d = new ArrayList<>(this.f367d);
            lVar.f368e = this.f368e;
            lVar.f369f = this.f369f;
            lVar.f370g = this.f370g;
            lVar.f371h = this.f371h;
            lVar.i = this.i;
            lVar.j = this.j;
            lVar.k = this.k;
            lVar.l = this.l;
            lVar.m = this.m;
            lVar.n = this.n;
            return lVar;
        }
    }

    public static Bundle a(Notification notification) {
        return f325a.a(notification);
    }
}
