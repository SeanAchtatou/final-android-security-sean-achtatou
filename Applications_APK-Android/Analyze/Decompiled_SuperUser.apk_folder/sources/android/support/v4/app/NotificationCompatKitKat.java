package android.support.v4.app;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ac;
import android.util.SparseArray;
import android.widget.RemoteViews;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.ArrayList;
import java.util.List;

@TargetApi(19)
class NotificationCompatKitKat {

    public static class Builder implements aa, ab {

        /* renamed from: a  reason: collision with root package name */
        private Notification.Builder f392a;

        /* renamed from: b  reason: collision with root package name */
        private Bundle f393b;

        /* renamed from: c  reason: collision with root package name */
        private List<Bundle> f394c = new ArrayList();

        /* renamed from: d  reason: collision with root package name */
        private RemoteViews f395d;

        /* renamed from: e  reason: collision with root package name */
        private RemoteViews f396e;

        public Builder(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, ArrayList<String> arrayList, Bundle bundle, String str, boolean z5, String str2, RemoteViews remoteViews2, RemoteViews remoteViews3) {
            this.f392a = new Notification.Builder(context).setWhen(notification.when).setShowWhen(z2).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, remoteViews).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(pendingIntent2, (notification.flags & FileUtils.FileMode.MODE_IWUSR) != 0).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z3).setPriority(i4).setProgress(i2, i3, z);
            this.f393b = new Bundle();
            if (bundle != null) {
                this.f393b.putAll(bundle);
            }
            if (arrayList != null && !arrayList.isEmpty()) {
                this.f393b.putStringArray("android.people", (String[]) arrayList.toArray(new String[arrayList.size()]));
            }
            if (z4) {
                this.f393b.putBoolean("android.support.localOnly", true);
            }
            if (str != null) {
                this.f393b.putString("android.support.groupKey", str);
                if (z5) {
                    this.f393b.putBoolean("android.support.isGroupSummary", true);
                } else {
                    this.f393b.putBoolean("android.support.useSideChannel", true);
                }
            }
            if (str2 != null) {
                this.f393b.putString("android.support.sortKey", str2);
            }
            this.f395d = remoteViews2;
            this.f396e = remoteViews3;
        }

        public void a(ac.a aVar) {
            this.f394c.add(NotificationCompatJellybean.a(this.f392a, aVar));
        }

        public Notification.Builder a() {
            return this.f392a;
        }

        public Notification b() {
            SparseArray<Bundle> a2 = NotificationCompatJellybean.a(this.f394c);
            if (a2 != null) {
                this.f393b.putSparseParcelableArray("android.support.actionExtras", a2);
            }
            this.f392a.setExtras(this.f393b);
            Notification build = this.f392a.build();
            if (this.f395d != null) {
                build.contentView = this.f395d;
            }
            if (this.f396e != null) {
                build.bigContentView = this.f396e;
            }
            return build;
        }
    }

    public static Bundle a(Notification notification) {
        return notification.extras;
    }
}
