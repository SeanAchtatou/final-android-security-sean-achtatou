package android.support.v4.app;

import android.os.Bundle;
import android.support.v4.content.Loader;

/* compiled from: LoaderManager */
public abstract class w {

    /* compiled from: LoaderManager */
    public interface a<D> {
        Loader<D> a(int i, Bundle bundle);

        void a(Loader<D> loader);

        void a(Loader<D> loader, D d2);
    }

    public boolean a() {
        return false;
    }
}
