package android.support.v4.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompatApi23;
import android.support.v4.util.i;
import android.support.v4.util.j;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class FragmentActivity extends k implements ActivityCompat.a, ActivityCompatApi23.a {

    /* renamed from: c  reason: collision with root package name */
    final Handler f268c = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (FragmentActivity.this.f272g) {
                        FragmentActivity.this.a(false);
                        return;
                    }
                    return;
                case 2:
                    FragmentActivity.this.a_();
                    FragmentActivity.this.f269d.n();
                    return;
                default:
                    super.handleMessage(message);
                    return;
            }
        }
    };

    /* renamed from: d  reason: collision with root package name */
    final p f269d = p.a(new a());

    /* renamed from: e  reason: collision with root package name */
    boolean f270e;

    /* renamed from: f  reason: collision with root package name */
    boolean f271f;

    /* renamed from: g  reason: collision with root package name */
    boolean f272g = true;

    /* renamed from: h  reason: collision with root package name */
    boolean f273h = true;
    boolean i;
    boolean j;
    boolean k;
    int l;
    j<String> m;

    public /* bridge */ /* synthetic */ View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(view, str, context, attributeSet);
    }

    public /* bridge */ /* synthetic */ View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(str, context, attributeSet);
    }

    public /* bridge */ /* synthetic */ void startActivityForResult(Intent intent, int i2, Bundle bundle) {
        super.startActivityForResult(intent, i2, bundle);
    }

    public /* bridge */ /* synthetic */ void startIntentSenderForResult(IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5) {
        super.startIntentSenderForResult(intentSender, i2, intent, i3, i4, i5);
    }

    public /* bridge */ /* synthetic */ void startIntentSenderForResult(IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5, Bundle bundle) {
        super.startIntentSenderForResult(intentSender, i2, intent, i3, i4, i5, bundle);
    }

    static final class b {

        /* renamed from: a  reason: collision with root package name */
        Object f276a;

        /* renamed from: b  reason: collision with root package name */
        s f277b;

        /* renamed from: c  reason: collision with root package name */
        i<String, w> f278c;

        b() {
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        this.f269d.b();
        int i4 = i2 >> 16;
        if (i4 != 0) {
            int i5 = i4 - 1;
            String a2 = this.m.a(i5);
            this.m.c(i5);
            if (a2 == null) {
                Log.w("FragmentActivity", "Activity result delivered for unknown Fragment.");
                return;
            }
            Fragment a3 = this.f269d.a(a2);
            if (a3 == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for who: " + a2);
            } else {
                a3.onActivityResult(65535 & i2, i3, intent);
            }
        } else {
            super.onActivityResult(i2, i3, intent);
        }
    }

    public void onBackPressed() {
        if (!this.f269d.a().c()) {
            super.onBackPressed();
        }
    }

    public void onMultiWindowModeChanged(boolean z) {
        this.f269d.a(z);
    }

    public void onPictureInPictureModeChanged(boolean z) {
        this.f269d.b(z);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.f269d.a(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        this.f269d.a((Fragment) null);
        super.onCreate(bundle);
        b bVar = (b) getLastNonConfigurationInstance();
        if (bVar != null) {
            this.f269d.a(bVar.f278c);
        }
        if (bundle != null) {
            this.f269d.a(bundle.getParcelable("android:support:fragments"), bVar != null ? bVar.f277b : null);
            if (bundle.containsKey("android:support:next_request_index")) {
                this.l = bundle.getInt("android:support:next_request_index");
                int[] intArray = bundle.getIntArray("android:support:request_indicies");
                String[] stringArray = bundle.getStringArray("android:support:request_fragment_who");
                if (intArray == null || stringArray == null || intArray.length != stringArray.length) {
                    Log.w("FragmentActivity", "Invalid requestCode mapping in savedInstanceState.");
                } else {
                    this.m = new j<>(intArray.length);
                    for (int i2 = 0; i2 < intArray.length; i2++) {
                        this.m.b(intArray[i2], stringArray[i2]);
                    }
                }
            }
        }
        if (this.m == null) {
            this.m = new j<>();
            this.l = 0;
        }
        this.f269d.e();
    }

    public boolean onCreatePanelMenu(int i2, Menu menu) {
        if (i2 != 0) {
            return super.onCreatePanelMenu(i2, menu);
        }
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i2, menu) | this.f269d.a(menu, getMenuInflater());
        if (Build.VERSION.SDK_INT >= 11) {
            return onCreatePanelMenu;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final View a(View view, String str, Context context, AttributeSet attributeSet) {
        return this.f269d.a(view, str, context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a(false);
        this.f269d.l();
        this.f269d.p();
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.f269d.m();
    }

    public boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        if (super.onMenuItemSelected(i2, menuItem)) {
            return true;
        }
        switch (i2) {
            case 0:
                return this.f269d.a(menuItem);
            case 6:
                return this.f269d.b(menuItem);
            default:
                return false;
        }
    }

    public void onPanelClosed(int i2, Menu menu) {
        switch (i2) {
            case 0:
                this.f269d.b(menu);
                break;
        }
        super.onPanelClosed(i2, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.f271f = false;
        if (this.f268c.hasMessages(2)) {
            this.f268c.removeMessages(2);
            a_();
        }
        this.f269d.i();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.f269d.b();
    }

    public void onStateNotSaved() {
        this.f269d.b();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f268c.sendEmptyMessage(2);
        this.f271f = true;
        this.f269d.n();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.f268c.removeMessages(2);
        a_();
        this.f269d.n();
    }

    /* access modifiers changed from: protected */
    public void a_() {
        this.f269d.h();
    }

    public boolean onPreparePanel(int i2, View view, Menu menu) {
        if (i2 != 0 || menu == null) {
            return super.onPreparePanel(i2, view, menu);
        }
        if (this.j) {
            this.j = false;
            menu.clear();
            onCreatePanelMenu(i2, menu);
        }
        return a(view, menu) | this.f269d.a(menu);
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, Menu menu) {
        return super.onPreparePanel(0, view, menu);
    }

    public final Object onRetainNonConfigurationInstance() {
        if (this.f272g) {
            a(true);
        }
        Object b2 = b();
        s d2 = this.f269d.d();
        i<String, w> r = this.f269d.r();
        if (d2 == null && r == null && b2 == null) {
            return null;
        }
        b bVar = new b();
        bVar.f276a = b2;
        bVar.f277b = d2;
        bVar.f278c = r;
        return bVar;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Parcelable c2 = this.f269d.c();
        if (c2 != null) {
            bundle.putParcelable("android:support:fragments", c2);
        }
        if (this.m.b() > 0) {
            bundle.putInt("android:support:next_request_index", this.l);
            int[] iArr = new int[this.m.b()];
            String[] strArr = new String[this.m.b()];
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.m.b()) {
                    iArr[i3] = this.m.d(i3);
                    strArr[i3] = this.m.e(i3);
                    i2 = i3 + 1;
                } else {
                    bundle.putIntArray("android:support:request_indicies", iArr);
                    bundle.putStringArray("android:support:request_fragment_who", strArr);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.f272g = false;
        this.f273h = false;
        this.f268c.removeMessages(1);
        if (!this.f270e) {
            this.f270e = true;
            this.f269d.f();
        }
        this.f269d.b();
        this.f269d.n();
        this.f269d.o();
        this.f269d.g();
        this.f269d.q();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.f272g = true;
        this.f268c.sendEmptyMessage(1);
        this.f269d.j();
    }

    public Object b() {
        return null;
    }

    public void c() {
        if (Build.VERSION.SDK_INT >= 11) {
            d.a(this);
        } else {
            this.j = true;
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (Build.VERSION.SDK_INT >= 11) {
        }
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String str2 = str + "  ";
        printWriter.print(str2);
        printWriter.print("mCreated=");
        printWriter.print(this.f270e);
        printWriter.print("mResumed=");
        printWriter.print(this.f271f);
        printWriter.print(" mStopped=");
        printWriter.print(this.f272g);
        printWriter.print(" mReallyStopped=");
        printWriter.println(this.f273h);
        this.f269d.a(str2, fileDescriptor, printWriter, strArr);
        this.f269d.a().a(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.println("View Hierarchy:");
        a(str + "  ", printWriter, getWindow().getDecorView());
    }

    private static String a(View view) {
        char c2;
        char c3;
        char c4;
        char c5;
        char c6;
        char c7;
        char c8;
        String str;
        char c9 = 'F';
        char c10 = '.';
        StringBuilder sb = new StringBuilder((int) FileUtils.FileMode.MODE_IWUSR);
        sb.append(view.getClass().getName());
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(view)));
        sb.append(' ');
        switch (view.getVisibility()) {
            case 0:
                sb.append('V');
                break;
            case 4:
                sb.append('I');
                break;
            case 8:
                sb.append('G');
                break;
            default:
                sb.append('.');
                break;
        }
        if (view.isFocusable()) {
            c2 = 'F';
        } else {
            c2 = '.';
        }
        sb.append(c2);
        if (view.isEnabled()) {
            c3 = 'E';
        } else {
            c3 = '.';
        }
        sb.append(c3);
        sb.append(view.willNotDraw() ? '.' : 'D');
        if (view.isHorizontalScrollBarEnabled()) {
            c4 = 'H';
        } else {
            c4 = '.';
        }
        sb.append(c4);
        if (view.isVerticalScrollBarEnabled()) {
            c5 = 'V';
        } else {
            c5 = '.';
        }
        sb.append(c5);
        if (view.isClickable()) {
            c6 = 'C';
        } else {
            c6 = '.';
        }
        sb.append(c6);
        if (view.isLongClickable()) {
            c7 = 'L';
        } else {
            c7 = '.';
        }
        sb.append(c7);
        sb.append(' ');
        if (!view.isFocused()) {
            c9 = '.';
        }
        sb.append(c9);
        if (view.isSelected()) {
            c8 = 'S';
        } else {
            c8 = '.';
        }
        sb.append(c8);
        if (view.isPressed()) {
            c10 = 'P';
        }
        sb.append(c10);
        sb.append(' ');
        sb.append(view.getLeft());
        sb.append(',');
        sb.append(view.getTop());
        sb.append('-');
        sb.append(view.getRight());
        sb.append(',');
        sb.append(view.getBottom());
        int id = view.getId();
        if (id != -1) {
            sb.append(" #");
            sb.append(Integer.toHexString(id));
            Resources resources = view.getResources();
            if (!(id == 0 || resources == null)) {
                switch (-16777216 & id) {
                    case 16777216:
                        str = io.fabric.sdk.android.services.common.a.ANDROID_CLIENT_TYPE;
                        String resourceTypeName = resources.getResourceTypeName(id);
                        String resourceEntryName = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName);
                        sb.append("/");
                        sb.append(resourceEntryName);
                        break;
                    case 2130706432:
                        str = ServiceManagerNative.APP;
                        String resourceTypeName2 = resources.getResourceTypeName(id);
                        String resourceEntryName2 = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName2);
                        sb.append("/");
                        sb.append(resourceEntryName2);
                        break;
                    default:
                        try {
                            str = resources.getResourcePackageName(id);
                            String resourceTypeName22 = resources.getResourceTypeName(id);
                            String resourceEntryName22 = resources.getResourceEntryName(id);
                            sb.append(" ");
                            sb.append(str);
                            sb.append(":");
                            sb.append(resourceTypeName22);
                            sb.append("/");
                            sb.append(resourceEntryName22);
                            break;
                        } catch (Resources.NotFoundException e2) {
                            break;
                        }
                }
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private void a(String str, PrintWriter printWriter, View view) {
        ViewGroup viewGroup;
        int childCount;
        printWriter.print(str);
        if (view == null) {
            printWriter.println("null");
            return;
        }
        printWriter.println(a(view));
        if ((view instanceof ViewGroup) && (childCount = (viewGroup = (ViewGroup) view).getChildCount()) > 0) {
            String str2 = str + "  ";
            for (int i2 = 0; i2 < childCount; i2++) {
                a(str2, printWriter, viewGroup.getChildAt(i2));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (!this.f273h) {
            this.f273h = true;
            this.i = z;
            this.f268c.removeMessages(1);
            d();
        } else if (z) {
            this.f269d.o();
            this.f269d.c(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.f269d.c(this.i);
        this.f269d.k();
    }

    public void a(Fragment fragment) {
    }

    public q e() {
        return this.f269d.a();
    }

    public void startActivityForResult(Intent intent, int i2) {
        if (!this.f462b && i2 != -1) {
            b(i2);
        }
        super.startActivityForResult(intent, i2);
    }

    public final void a(int i2) {
        if (!this.k && i2 != -1) {
            b(i2);
        }
    }

    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        int i3 = (i2 >> 16) & 65535;
        if (i3 != 0) {
            int i4 = i3 - 1;
            String a2 = this.m.a(i4);
            this.m.c(i4);
            if (a2 == null) {
                Log.w("FragmentActivity", "Activity result delivered for unknown Fragment.");
                return;
            }
            Fragment a3 = this.f269d.a(a2);
            if (a3 == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for who: " + a2);
            } else {
                a3.onRequestPermissionsResult(i2 & 65535, strArr, iArr);
            }
        }
    }

    public void a(Fragment fragment, Intent intent, int i2, Bundle bundle) {
        this.f462b = true;
        if (i2 == -1) {
            try {
                ActivityCompat.a(this, intent, -1, bundle);
            } finally {
                this.f462b = false;
            }
        } else {
            b(i2);
            ActivityCompat.a(this, intent, ((b(fragment) + 1) << 16) + (65535 & i2), bundle);
            this.f462b = false;
        }
    }

    public void a(Fragment fragment, IntentSender intentSender, int i2, Intent intent, int i3, int i4, int i5, Bundle bundle) {
        this.f461a = true;
        if (i2 == -1) {
            try {
                ActivityCompat.a(this, intentSender, i2, intent, i3, i4, i5, bundle);
            } finally {
                this.f461a = false;
            }
        } else {
            b(i2);
            ActivityCompat.a(this, intentSender, ((b(fragment) + 1) << 16) + (65535 & i2), intent, i3, i4, i5, bundle);
            this.f461a = false;
        }
    }

    private int b(Fragment fragment) {
        if (this.m.b() >= 65534) {
            throw new IllegalStateException("Too many pending Fragment activity results.");
        }
        while (this.m.f(this.l) >= 0) {
            this.l = (this.l + 1) % 65534;
        }
        int i2 = this.l;
        this.m.b(i2, fragment.mWho);
        this.l = (this.l + 1) % 65534;
        return i2;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, String[] strArr, int i2) {
        if (i2 == -1) {
            ActivityCompat.a(this, strArr, i2);
            return;
        }
        b(i2);
        try {
            this.k = true;
            ActivityCompat.a(this, strArr, ((b(fragment) + 1) << 16) + (65535 & i2));
            this.k = false;
        } catch (Throwable th) {
            this.k = false;
            throw th;
        }
    }

    class a extends FragmentHostCallback<FragmentActivity> {
        public a() {
            super(FragmentActivity.this);
        }

        @SuppressLint({"NewApi"})
        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            FragmentActivity.this.dump(str, fileDescriptor, printWriter, strArr);
        }

        public boolean a(Fragment fragment) {
            return !FragmentActivity.this.isFinishing();
        }

        public LayoutInflater b() {
            return FragmentActivity.this.getLayoutInflater().cloneInContext(FragmentActivity.this);
        }

        /* renamed from: c */
        public FragmentActivity g() {
            return FragmentActivity.this;
        }

        public void d() {
            FragmentActivity.this.c();
        }

        public void a(Fragment fragment, Intent intent, int i, Bundle bundle) {
            FragmentActivity.this.a(fragment, intent, i, bundle);
        }

        public void a(Fragment fragment, IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) {
            FragmentActivity.this.a(fragment, intentSender, i, intent, i2, i3, i4, bundle);
        }

        public void a(Fragment fragment, String[] strArr, int i) {
            FragmentActivity.this.a(fragment, strArr, i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.app.ActivityCompat.a(android.app.Activity, java.lang.String):boolean
         arg types: [android.support.v4.app.FragmentActivity, java.lang.String]
         candidates:
          android.support.v4.content.c.a(android.content.Context, int):android.graphics.drawable.Drawable
          android.support.v4.content.c.a(android.content.Context, java.lang.String):java.io.File[]
          android.support.v4.app.ActivityCompat.a(android.app.Activity, java.lang.String):boolean */
        public boolean a(String str) {
            return ActivityCompat.a((Activity) FragmentActivity.this, str);
        }

        public boolean e() {
            return FragmentActivity.this.getWindow() != null;
        }

        public int f() {
            Window window = FragmentActivity.this.getWindow();
            if (window == null) {
                return 0;
            }
            return window.getAttributes().windowAnimations;
        }

        public void b(Fragment fragment) {
            FragmentActivity.this.a(fragment);
        }

        public View a(int i) {
            return FragmentActivity.this.findViewById(i);
        }

        public boolean a() {
            Window window = FragmentActivity.this.getWindow();
            return (window == null || window.peekDecorView() == null) ? false : true;
        }
    }
}
