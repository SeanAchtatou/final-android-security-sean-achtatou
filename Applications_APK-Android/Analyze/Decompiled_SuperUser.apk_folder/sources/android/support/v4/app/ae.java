package android.support.v4.app;

import android.view.View;
import android.view.ViewTreeObserver;

/* compiled from: OneShotPreDrawListener */
class ae implements View.OnAttachStateChangeListener, ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    private final View f438a;

    /* renamed from: b  reason: collision with root package name */
    private ViewTreeObserver f439b;

    /* renamed from: c  reason: collision with root package name */
    private final Runnable f440c;

    private ae(View view, Runnable runnable) {
        this.f438a = view;
        this.f439b = view.getViewTreeObserver();
        this.f440c = runnable;
    }

    public static ae a(View view, Runnable runnable) {
        ae aeVar = new ae(view, runnable);
        view.getViewTreeObserver().addOnPreDrawListener(aeVar);
        view.addOnAttachStateChangeListener(aeVar);
        return aeVar;
    }

    public boolean onPreDraw() {
        a();
        this.f440c.run();
        return true;
    }

    public void a() {
        if (this.f439b.isAlive()) {
            this.f439b.removeOnPreDrawListener(this);
        } else {
            this.f438a.getViewTreeObserver().removeOnPreDrawListener(this);
        }
        this.f438a.removeOnAttachStateChangeListener(this);
    }

    public void onViewAttachedToWindow(View view) {
        this.f439b = view.getViewTreeObserver();
    }

    public void onViewDetachedFromWindow(View view) {
        a();
    }
}
