package android.support.v4.app;

import android.annotation.TargetApi;
import android.os.Bundle;

@TargetApi(9)
/* compiled from: RemoteInputCompatBase */
class ag {

    /* compiled from: RemoteInputCompatBase */
    public static abstract class a {

        /* renamed from: android.support.v4.app.ag$a$a  reason: collision with other inner class name */
        /* compiled from: RemoteInputCompatBase */
        public interface C0011a {
        }

        /* access modifiers changed from: protected */
        public abstract String a();

        /* access modifiers changed from: protected */
        public abstract CharSequence b();

        /* access modifiers changed from: protected */
        public abstract CharSequence[] c();

        /* access modifiers changed from: protected */
        public abstract boolean d();

        /* access modifiers changed from: protected */
        public abstract Bundle e();
    }
}
