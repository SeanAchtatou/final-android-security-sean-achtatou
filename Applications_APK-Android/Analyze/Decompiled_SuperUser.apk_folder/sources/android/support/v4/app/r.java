package android.support.v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.q;
import android.support.v4.util.h;
import android.support.v4.view.ag;
import android.support.v4.view.m;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: FragmentManager */
final class r extends q implements m {
    static final Interpolator D = new DecelerateInterpolator(2.5f);
    static final Interpolator E = new DecelerateInterpolator(1.5f);
    static final Interpolator F = new AccelerateInterpolator(2.5f);
    static final Interpolator G = new AccelerateInterpolator(1.5f);

    /* renamed from: a  reason: collision with root package name */
    static boolean f466a = false;

    /* renamed from: b  reason: collision with root package name */
    static final boolean f467b;
    static Field q = null;
    SparseArray<Parcelable> A = null;
    ArrayList<e> B;
    Runnable C = new Runnable() {
        public void run() {
            r.this.e();
        }
    };
    private CopyOnWriteArrayList<h<q.a, Boolean>> H;

    /* renamed from: c  reason: collision with root package name */
    ArrayList<c> f468c;

    /* renamed from: d  reason: collision with root package name */
    boolean f469d;

    /* renamed from: e  reason: collision with root package name */
    ArrayList<Fragment> f470e;

    /* renamed from: f  reason: collision with root package name */
    ArrayList<Fragment> f471f;

    /* renamed from: g  reason: collision with root package name */
    ArrayList<Integer> f472g;

    /* renamed from: h  reason: collision with root package name */
    ArrayList<h> f473h;
    ArrayList<Fragment> i;
    ArrayList<h> j;
    ArrayList<Integer> k;
    ArrayList<q.b> l;
    int m = 0;
    FragmentHostCallback n;
    o o;
    Fragment p;
    boolean r;
    boolean s;
    boolean t;
    String u;
    boolean v;
    ArrayList<h> w;
    ArrayList<Boolean> x;
    ArrayList<Fragment> y;
    Bundle z = null;

    /* compiled from: FragmentManager */
    static class b {

        /* renamed from: a  reason: collision with root package name */
        public static final int[] f481a = {16842755, 16842960, 16842961};
    }

    /* compiled from: FragmentManager */
    interface c {
        boolean a(ArrayList<h> arrayList, ArrayList<Boolean> arrayList2);
    }

    r() {
    }

    static {
        boolean z2 = false;
        if (Build.VERSION.SDK_INT >= 11) {
            z2 = true;
        }
        f467b = z2;
    }

    /* compiled from: FragmentManager */
    static class a implements Animation.AnimationListener {

        /* renamed from: a  reason: collision with root package name */
        private Animation.AnimationListener f477a;

        /* renamed from: b  reason: collision with root package name */
        private boolean f478b;

        /* renamed from: c  reason: collision with root package name */
        View f479c;

        public a(View view, Animation animation) {
            if (view != null && animation != null) {
                this.f479c = view;
            }
        }

        public a(View view, Animation animation, Animation.AnimationListener animationListener) {
            if (view != null && animation != null) {
                this.f477a = animationListener;
                this.f479c = view;
                this.f478b = true;
            }
        }

        public void onAnimationStart(Animation animation) {
            if (this.f477a != null) {
                this.f477a.onAnimationStart(animation);
            }
        }

        public void onAnimationEnd(Animation animation) {
            if (this.f479c != null && this.f478b) {
                if (ag.H(this.f479c) || android.support.v4.os.c.a()) {
                    this.f479c.post(new Runnable() {
                        public void run() {
                            ag.a(a.this.f479c, 0, (Paint) null);
                        }
                    });
                } else {
                    ag.a(this.f479c, 0, (Paint) null);
                }
            }
            if (this.f477a != null) {
                this.f477a.onAnimationEnd(animation);
            }
        }

        public void onAnimationRepeat(Animation animation) {
            if (this.f477a != null) {
                this.f477a.onAnimationRepeat(animation);
            }
        }
    }

    static boolean a(Animation animation) {
        if (animation instanceof AlphaAnimation) {
            return true;
        }
        if (!(animation instanceof AnimationSet)) {
            return false;
        }
        List<Animation> animations = ((AnimationSet) animation).getAnimations();
        for (int i2 = 0; i2 < animations.size(); i2++) {
            if (animations.get(i2) instanceof AlphaAnimation) {
                return true;
            }
        }
        return false;
    }

    static boolean a(View view, Animation animation) {
        return Build.VERSION.SDK_INT >= 19 && ag.f(view) == 0 && ag.z(view) && a(animation);
    }

    private void a(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new android.support.v4.util.d("FragmentManager"));
        if (this.n != null) {
            try {
                this.n.a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        } else {
            try {
                a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e3) {
                Log.e("FragmentManager", "Failed dumping state", e3);
            }
        }
        throw runtimeException;
    }

    public t a() {
        return new h(this);
    }

    public boolean b() {
        boolean e2 = e();
        y();
        return e2;
    }

    public boolean c() {
        v();
        return a((String) null, -1, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
     arg types: [android.support.v4.app.r$d, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(int, boolean):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void */
    public void a(int i2, int i3) {
        if (i2 < 0) {
            throw new IllegalArgumentException("Bad id: " + i2);
        }
        a((c) new d(null, i2, i3), false);
    }

    private boolean a(String str, int i2, int i3) {
        e();
        c(true);
        boolean a2 = a(this.w, this.x, str, i2, i3);
        if (a2) {
            this.f469d = true;
            try {
                b(this.w, this.x);
            } finally {
                x();
            }
        }
        f();
        return a2;
    }

    public void a(Bundle bundle, String str, Fragment fragment) {
        if (fragment.mIndex < 0) {
            a(new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        bundle.putInt(str, fragment.mIndex);
    }

    public Fragment a(Bundle bundle, String str) {
        int i2 = bundle.getInt(str, -1);
        if (i2 == -1) {
            return null;
        }
        if (i2 >= this.f470e.size()) {
            a(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i2));
        }
        Fragment fragment = this.f470e.get(i2);
        if (fragment != null) {
            return fragment;
        }
        a(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i2));
        return fragment;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((int) FileUtils.FileMode.MODE_IWUSR);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        if (this.p != null) {
            android.support.v4.util.c.a(this.p, sb);
        } else {
            android.support.v4.util.c.a(this.n, sb);
        }
        sb.append("}}");
        return sb.toString();
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        int size3;
        int size4;
        int size5;
        int size6;
        String str2 = str + "    ";
        if (this.f470e != null && (size6 = this.f470e.size()) > 0) {
            printWriter.print(str);
            printWriter.print("Active Fragments in ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this)));
            printWriter.println(":");
            for (int i2 = 0; i2 < size6; i2++) {
                Fragment fragment = this.f470e.get(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(fragment);
                if (fragment != null) {
                    fragment.dump(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
        if (this.f471f != null && (size5 = this.f471f.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i3 = 0; i3 < size5; i3++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(this.f471f.get(i3).toString());
            }
        }
        if (this.i != null && (size4 = this.i.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i4 = 0; i4 < size4; i4++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i4);
                printWriter.print(": ");
                printWriter.println(this.i.get(i4).toString());
            }
        }
        if (this.f473h != null && (size3 = this.f473h.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i5 = 0; i5 < size3; i5++) {
                h hVar = this.f473h.get(i5);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i5);
                printWriter.print(": ");
                printWriter.println(hVar.toString());
                hVar.a(str2, fileDescriptor, printWriter, strArr);
            }
        }
        synchronized (this) {
            if (this.j != null && (size2 = this.j.size()) > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack Indices:");
                for (int i6 = 0; i6 < size2; i6++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i6);
                    printWriter.print(": ");
                    printWriter.println(this.j.get(i6));
                }
            }
            if (this.k != null && this.k.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.k.toArray()));
            }
        }
        if (this.f468c != null && (size = this.f468c.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Pending Actions:");
            for (int i7 = 0; i7 < size; i7++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i7);
                printWriter.print(": ");
                printWriter.println(this.f468c.get(i7));
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mHost=");
        printWriter.println(this.n);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.o);
        if (this.p != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.p);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.m);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.s);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.t);
        if (this.r) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.r);
        }
        if (this.u != null) {
            printWriter.print(str);
            printWriter.print("  mNoTransactionsBecause=");
            printWriter.println(this.u);
        }
        if (this.f472g != null && this.f472g.size() > 0) {
            printWriter.print(str);
            printWriter.print("  mAvailIndices: ");
            printWriter.println(Arrays.toString(this.f472g.toArray()));
        }
    }

    static Animation a(Context context, float f2, float f3, float f4, float f5) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, f2, f3, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(D);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f4, f5);
        alphaAnimation.setInterpolator(E);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    static Animation a(Context context, float f2, float f3) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f3);
        alphaAnimation.setInterpolator(E);
        alphaAnimation.setDuration(220);
        return alphaAnimation;
    }

    /* access modifiers changed from: package-private */
    public Animation a(Fragment fragment, int i2, boolean z2, int i3) {
        Animation loadAnimation;
        Animation onCreateAnimation = fragment.onCreateAnimation(i2, z2, fragment.getNextAnim());
        if (onCreateAnimation != null) {
            return onCreateAnimation;
        }
        if (fragment.getNextAnim() != 0 && (loadAnimation = AnimationUtils.loadAnimation(this.n.i(), fragment.getNextAnim())) != null) {
            return loadAnimation;
        }
        if (i2 == 0) {
            return null;
        }
        int b2 = b(i2, z2);
        if (b2 < 0) {
            return null;
        }
        switch (b2) {
            case 1:
                return a(this.n.i(), 1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return a(this.n.i(), 1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return a(this.n.i(), 0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                return a(this.n.i(), 1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                return a(this.n.i(), 0.0f, 1.0f);
            case 6:
                return a(this.n.i(), 1.0f, 0.0f);
            default:
                if (i3 == 0 && this.n.e()) {
                    i3 = this.n.f();
                }
                if (i3 == 0) {
                    return null;
                }
                return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
      android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void a(Fragment fragment) {
        if (!fragment.mDeferStart) {
            return;
        }
        if (this.f469d) {
            this.v = true;
            return;
        }
        fragment.mDeferStart = false;
        a(fragment, this.m, 0, 0, false);
    }

    private void b(View view, Animation animation) {
        Animation.AnimationListener animationListener;
        if (view != null && animation != null && a(view, animation)) {
            try {
                if (q == null) {
                    q = Animation.class.getDeclaredField("mListener");
                    q.setAccessible(true);
                }
                animationListener = (Animation.AnimationListener) q.get(animation);
            } catch (NoSuchFieldException e2) {
                Log.e("FragmentManager", "No field with the name mListener is found in Animation class", e2);
                animationListener = null;
            } catch (IllegalAccessException e3) {
                Log.e("FragmentManager", "Cannot access Animation's mListener field", e3);
                animationListener = null;
            }
            ag.a(view, 2, (Paint) null);
            animation.setAnimationListener(new a(view, animation, animationListener));
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2) {
        return this.m >= i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
      android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, android.content.Context, boolean):void
     arg types: [android.support.v4.app.Fragment, android.content.Context, int]
     candidates:
      android.support.v4.app.r.a(android.content.Context, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String, android.support.v4.app.Fragment):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.os.Bundle, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.FragmentHostCallback, android.support.v4.app.o, android.support.v4.app.Fragment):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.b(android.support.v4.app.Fragment, android.content.Context, boolean):void
     arg types: [android.support.v4.app.Fragment, android.content.Context, int]
     candidates:
      android.support.v4.app.r.b(android.support.v4.app.Fragment, android.os.Bundle, boolean):void
      android.support.v4.app.r.b(android.support.v4.app.Fragment, android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, android.os.Bundle, boolean):void
     arg types: [android.support.v4.app.Fragment, android.os.Bundle, int]
     candidates:
      android.support.v4.app.r.a(android.content.Context, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String, android.support.v4.app.Fragment):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.content.Context, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.FragmentHostCallback, android.support.v4.app.o, android.support.v4.app.Fragment):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.b(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.b(android.view.View, float):void
      android.support.v4.view.ag.b(android.view.View, int):boolean
      android.support.v4.view.ag.b(android.view.View, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, android.view.View, android.os.Bundle, boolean):void
     arg types: [android.support.v4.app.Fragment, android.view.View, android.os.Bundle, int]
     candidates:
      android.support.v4.app.r.a(android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      android.support.v4.app.r.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.r.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.q.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.view.m.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.view.View, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.b(android.support.v4.app.Fragment, android.os.Bundle, boolean):void
     arg types: [android.support.v4.app.Fragment, android.os.Bundle, int]
     candidates:
      android.support.v4.app.r.b(android.support.v4.app.Fragment, android.content.Context, boolean):void
      android.support.v4.app.r.b(android.support.v4.app.Fragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.b(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.r.b(int, boolean):int
      android.support.v4.app.r.b(android.view.View, android.view.animation.Animation):void
      android.support.v4.app.r.b(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.b(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.b(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.c(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.r.c(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):boolean
      android.support.v4.app.r.c(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.r.a(android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      android.support.v4.app.r.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.view.View, android.os.Bundle, boolean):void
      android.support.v4.app.r.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.q.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.view.m.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    /* access modifiers changed from: package-private */
    public void a(final Fragment fragment, int i2, int i3, int i4, boolean z2) {
        Animation animation;
        ViewGroup viewGroup;
        String str;
        r k2;
        boolean z3 = true;
        if ((!fragment.mAdded || fragment.mDetached) && i2 > 1) {
            i2 = 1;
        }
        if (fragment.mRemoving && i2 > fragment.mState) {
            i2 = fragment.mState;
        }
        if (fragment.mDeferStart && fragment.mState < 4 && i2 > 3) {
            i2 = 3;
        }
        if (fragment.mState >= i2) {
            if (fragment.mState > i2) {
                switch (fragment.mState) {
                    case 5:
                        if (i2 < 5) {
                            if (f466a) {
                                Log.v("FragmentManager", "movefrom RESUMED: " + fragment);
                            }
                            fragment.performPause();
                            d(fragment, false);
                        }
                    case 4:
                        if (i2 < 4) {
                            if (f466a) {
                                Log.v("FragmentManager", "movefrom STARTED: " + fragment);
                            }
                            fragment.performStop();
                            e(fragment, false);
                        }
                    case 3:
                        if (i2 < 3) {
                            if (f466a) {
                                Log.v("FragmentManager", "movefrom STOPPED: " + fragment);
                            }
                            fragment.performReallyStop();
                        }
                    case 2:
                        if (i2 < 2) {
                            if (f466a) {
                                Log.v("FragmentManager", "movefrom ACTIVITY_CREATED: " + fragment);
                            }
                            if (fragment.mView != null && this.n.a(fragment) && fragment.mSavedViewState == null) {
                                l(fragment);
                            }
                            fragment.performDestroyView();
                            f(fragment, false);
                            if (!(fragment.mView == null || fragment.mContainer == null)) {
                                if (this.m <= 0 || this.t || fragment.mView.getVisibility() != 0 || fragment.mPostponedAlpha < 0.0f) {
                                    animation = null;
                                } else {
                                    animation = a(fragment, i3, false, i4);
                                }
                                fragment.mPostponedAlpha = 0.0f;
                                if (animation != null) {
                                    fragment.setAnimatingAway(fragment.mView);
                                    fragment.setStateAfterAnimating(i2);
                                    animation.setAnimationListener(new a(fragment.mView, animation) {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
                                         arg types: [android.support.v4.app.Fragment, int, int, int, int]
                                         candidates:
                                          android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
                                          android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
                                          android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
                                          android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
                                          android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
                                        public void onAnimationEnd(Animation animation) {
                                            super.onAnimationEnd(animation);
                                            if (fragment.getAnimatingAway() != null) {
                                                fragment.setAnimatingAway(null);
                                                r.this.a(fragment, fragment.getStateAfterAnimating(), 0, 0, false);
                                            }
                                        }
                                    });
                                    fragment.mView.startAnimation(animation);
                                }
                                fragment.mContainer.removeView(fragment.mView);
                            }
                            fragment.mContainer = null;
                            fragment.mView = null;
                            fragment.mInnerView = null;
                        }
                        break;
                    case 1:
                        if (i2 < 1) {
                            if (this.t && fragment.getAnimatingAway() != null) {
                                View animatingAway = fragment.getAnimatingAway();
                                fragment.setAnimatingAway(null);
                                animatingAway.clearAnimation();
                            }
                            if (fragment.getAnimatingAway() == null) {
                                if (f466a) {
                                    Log.v("FragmentManager", "movefrom CREATED: " + fragment);
                                }
                                if (!fragment.mRetaining) {
                                    fragment.performDestroy();
                                    g(fragment, false);
                                } else {
                                    fragment.mState = 0;
                                }
                                fragment.performDetach();
                                h(fragment, false);
                                if (!z2) {
                                    if (fragment.mRetaining) {
                                        fragment.mHost = null;
                                        fragment.mParentFragment = null;
                                        fragment.mFragmentManager = null;
                                        break;
                                    } else {
                                        f(fragment);
                                        break;
                                    }
                                }
                            } else {
                                fragment.setStateAfterAnimating(i2);
                                i2 = 1;
                                break;
                            }
                        }
                        break;
                }
            }
        } else if (!fragment.mFromLayout || fragment.mInLayout) {
            if (fragment.getAnimatingAway() != null) {
                fragment.setAnimatingAway(null);
                a(fragment, fragment.getStateAfterAnimating(), 0, 0, true);
            }
            switch (fragment.mState) {
                case 0:
                    if (f466a) {
                        Log.v("FragmentManager", "moveto CREATED: " + fragment);
                    }
                    if (fragment.mSavedFragmentState != null) {
                        fragment.mSavedFragmentState.setClassLoader(this.n.i().getClassLoader());
                        fragment.mSavedViewState = fragment.mSavedFragmentState.getSparseParcelableArray("android:view_state");
                        fragment.mTarget = a(fragment.mSavedFragmentState, "android:target_state");
                        if (fragment.mTarget != null) {
                            fragment.mTargetRequestCode = fragment.mSavedFragmentState.getInt("android:target_req_state", 0);
                        }
                        fragment.mUserVisibleHint = fragment.mSavedFragmentState.getBoolean("android:user_visible_hint", true);
                        if (!fragment.mUserVisibleHint) {
                            fragment.mDeferStart = true;
                            if (i2 > 3) {
                                i2 = 3;
                            }
                        }
                    }
                    fragment.mHost = this.n;
                    fragment.mParentFragment = this.p;
                    if (this.p != null) {
                        k2 = this.p.mChildFragmentManager;
                    } else {
                        k2 = this.n.k();
                    }
                    fragment.mFragmentManager = k2;
                    a(fragment, this.n.i(), false);
                    fragment.mCalled = false;
                    fragment.onAttach(this.n.i());
                    if (!fragment.mCalled) {
                        throw new SuperNotCalledException("Fragment " + fragment + " did not call through to super.onAttach()");
                    }
                    if (fragment.mParentFragment == null) {
                        this.n.b(fragment);
                    } else {
                        fragment.mParentFragment.onAttachFragment(fragment);
                    }
                    b(fragment, this.n.i(), false);
                    if (!fragment.mRetaining) {
                        fragment.performCreate(fragment.mSavedFragmentState);
                        a(fragment, fragment.mSavedFragmentState, false);
                    } else {
                        fragment.restoreChildFragmentState(fragment.mSavedFragmentState);
                        fragment.mState = 1;
                    }
                    fragment.mRetaining = false;
                    if (fragment.mFromLayout) {
                        fragment.mView = fragment.performCreateView(fragment.getLayoutInflater(fragment.mSavedFragmentState), null, fragment.mSavedFragmentState);
                        if (fragment.mView != null) {
                            fragment.mInnerView = fragment.mView;
                            if (Build.VERSION.SDK_INT >= 11) {
                                ag.b(fragment.mView, false);
                            } else {
                                fragment.mView = NoSaveStateFrameLayout.a(fragment.mView);
                            }
                            if (fragment.mHidden) {
                                fragment.mView.setVisibility(8);
                            }
                            fragment.onViewCreated(fragment.mView, fragment.mSavedFragmentState);
                            a(fragment, fragment.mView, fragment.mSavedFragmentState, false);
                        } else {
                            fragment.mInnerView = null;
                        }
                    }
                case 1:
                    if (i2 > 1) {
                        if (f466a) {
                            Log.v("FragmentManager", "moveto ACTIVITY_CREATED: " + fragment);
                        }
                        if (!fragment.mFromLayout) {
                            if (fragment.mContainerId != 0) {
                                if (fragment.mContainerId == -1) {
                                    a(new IllegalArgumentException("Cannot create fragment " + fragment + " for a container view with no id"));
                                }
                                viewGroup = (ViewGroup) this.o.a(fragment.mContainerId);
                                if (viewGroup == null && !fragment.mRestored) {
                                    try {
                                        str = fragment.getResources().getResourceName(fragment.mContainerId);
                                    } catch (Resources.NotFoundException e2) {
                                        str = "unknown";
                                    }
                                    a(new IllegalArgumentException("No view found for id 0x" + Integer.toHexString(fragment.mContainerId) + " (" + str + ") for fragment " + fragment));
                                }
                            } else {
                                viewGroup = null;
                            }
                            fragment.mContainer = viewGroup;
                            fragment.mView = fragment.performCreateView(fragment.getLayoutInflater(fragment.mSavedFragmentState), viewGroup, fragment.mSavedFragmentState);
                            if (fragment.mView != null) {
                                fragment.mInnerView = fragment.mView;
                                if (Build.VERSION.SDK_INT >= 11) {
                                    ag.b(fragment.mView, false);
                                } else {
                                    fragment.mView = NoSaveStateFrameLayout.a(fragment.mView);
                                }
                                if (viewGroup != null) {
                                    viewGroup.addView(fragment.mView);
                                }
                                if (fragment.mHidden) {
                                    fragment.mView.setVisibility(8);
                                }
                                fragment.onViewCreated(fragment.mView, fragment.mSavedFragmentState);
                                a(fragment, fragment.mView, fragment.mSavedFragmentState, false);
                                if (fragment.mView.getVisibility() != 0 || fragment.mContainer == null) {
                                    z3 = false;
                                }
                                fragment.mIsNewlyAdded = z3;
                            } else {
                                fragment.mInnerView = null;
                            }
                        }
                        fragment.performActivityCreated(fragment.mSavedFragmentState);
                        b(fragment, fragment.mSavedFragmentState, false);
                        if (fragment.mView != null) {
                            fragment.restoreViewState(fragment.mSavedFragmentState);
                        }
                        fragment.mSavedFragmentState = null;
                    }
                case 2:
                    if (i2 > 2) {
                        fragment.mState = 3;
                    }
                case 3:
                    if (i2 > 3) {
                        if (f466a) {
                            Log.v("FragmentManager", "moveto STARTED: " + fragment);
                        }
                        fragment.performStart();
                        b(fragment, false);
                    }
                case 4:
                    if (i2 > 4) {
                        if (f466a) {
                            Log.v("FragmentManager", "moveto RESUMED: " + fragment);
                        }
                        fragment.performResume();
                        c(fragment, false);
                        fragment.mSavedFragmentState = null;
                        fragment.mSavedViewState = null;
                        break;
                    }
                    break;
            }
        } else {
            return;
        }
        if (fragment.mState != i2) {
            Log.w("FragmentManager", "moveToState: Fragment state for " + fragment + " not updated inline; " + "expected state " + i2 + " found " + fragment.mState);
            fragment.mState = i2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
      android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(Fragment fragment) {
        a(fragment, this.m, 0, 0, false);
    }

    /* access modifiers changed from: package-private */
    public void c(Fragment fragment) {
        int i2;
        if (fragment.mView != null) {
            Animation a2 = a(fragment, fragment.getNextTransition(), !fragment.mHidden, fragment.getNextTransitionStyle());
            if (a2 != null) {
                b(fragment.mView, a2);
                fragment.mView.startAnimation(a2);
                b(fragment.mView, a2);
                a2.start();
            }
            if (!fragment.mHidden || fragment.isHideReplaced()) {
                i2 = 0;
            } else {
                i2 = 8;
            }
            fragment.mView.setVisibility(i2);
            if (fragment.isHideReplaced()) {
                fragment.setHideReplaced(false);
            }
        }
        if (fragment.mAdded && fragment.mHasMenu && fragment.mMenuVisible) {
            this.r = true;
        }
        fragment.mHiddenChanged = false;
        fragment.onHiddenChanged(fragment.mHidden);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
      android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.r.a(android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      android.support.v4.app.r.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.view.View, android.os.Bundle, boolean):void
      android.support.v4.app.r.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.q.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.view.m.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    /* access modifiers changed from: package-private */
    public void d(Fragment fragment) {
        ViewGroup viewGroup;
        int indexOfChild;
        int indexOfChild2;
        if (fragment != null) {
            int i2 = this.m;
            if (fragment.mRemoving) {
                if (fragment.isInBackStack()) {
                    i2 = Math.min(i2, 1);
                } else {
                    i2 = Math.min(i2, 0);
                }
            }
            a(fragment, i2, fragment.getNextTransition(), fragment.getNextTransitionStyle(), false);
            if (fragment.mView != null) {
                Fragment n2 = n(fragment);
                if (n2 != null && (indexOfChild2 = viewGroup.indexOfChild(fragment.mView)) < (indexOfChild = (viewGroup = fragment.mContainer).indexOfChild(n2.mView))) {
                    viewGroup.removeViewAt(indexOfChild2);
                    viewGroup.addView(fragment.mView, indexOfChild);
                }
                if (fragment.mIsNewlyAdded && fragment.mContainer != null) {
                    if (Build.VERSION.SDK_INT < 11) {
                        fragment.mView.setVisibility(0);
                    } else if (fragment.mPostponedAlpha > 0.0f) {
                        fragment.mView.setAlpha(fragment.mPostponedAlpha);
                    }
                    fragment.mPostponedAlpha = 0.0f;
                    fragment.mIsNewlyAdded = false;
                    Animation a2 = a(fragment, fragment.getNextTransition(), true, fragment.getNextTransitionStyle());
                    if (a2 != null) {
                        b(fragment.mView, a2);
                        fragment.mView.startAnimation(a2);
                    }
                }
            }
            if (fragment.mHiddenChanged) {
                c(fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2) {
        boolean z3;
        boolean z4;
        boolean z5;
        if (this.n == null && i2 != 0) {
            throw new IllegalStateException("No activity");
        } else if (z2 || i2 != this.m) {
            this.m = i2;
            if (this.f470e != null) {
                if (this.f471f != null) {
                    int size = this.f471f.size();
                    int i3 = 0;
                    z3 = false;
                    while (i3 < size) {
                        Fragment fragment = this.f471f.get(i3);
                        d(fragment);
                        if (fragment.mLoaderManager != null) {
                            z5 = fragment.mLoaderManager.a() | z3;
                        } else {
                            z5 = z3;
                        }
                        i3++;
                        z3 = z5;
                    }
                } else {
                    z3 = false;
                }
                int size2 = this.f470e.size();
                int i4 = 0;
                while (i4 < size2) {
                    Fragment fragment2 = this.f470e.get(i4);
                    if (fragment2 != null && ((fragment2.mRemoving || fragment2.mDetached) && !fragment2.mIsNewlyAdded)) {
                        d(fragment2);
                        if (fragment2.mLoaderManager != null) {
                            z4 = fragment2.mLoaderManager.a() | z3;
                            i4++;
                            z3 = z4;
                        }
                    }
                    z4 = z3;
                    i4++;
                    z3 = z4;
                }
                if (!z3) {
                    d();
                }
                if (this.r && this.n != null && this.m == 5) {
                    this.n.d();
                    this.r = false;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.f470e != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f470e.size()) {
                    Fragment fragment = this.f470e.get(i3);
                    if (fragment != null) {
                        a(fragment);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e(Fragment fragment) {
        if (fragment.mIndex < 0) {
            if (this.f472g == null || this.f472g.size() <= 0) {
                if (this.f470e == null) {
                    this.f470e = new ArrayList<>();
                }
                fragment.setIndex(this.f470e.size(), this.p);
                this.f470e.add(fragment);
            } else {
                fragment.setIndex(this.f472g.remove(this.f472g.size() - 1).intValue(), this.p);
                this.f470e.set(fragment.mIndex, fragment);
            }
            if (f466a) {
                Log.v("FragmentManager", "Allocated fragment index " + fragment);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f(Fragment fragment) {
        if (fragment.mIndex >= 0) {
            if (f466a) {
                Log.v("FragmentManager", "Freeing fragment index " + fragment);
            }
            this.f470e.set(fragment.mIndex, null);
            if (this.f472g == null) {
                this.f472g = new ArrayList<>();
            }
            this.f472g.add(Integer.valueOf(fragment.mIndex));
            this.n.b(fragment.mWho);
            fragment.initState();
        }
    }

    public void a(Fragment fragment, boolean z2) {
        if (this.f471f == null) {
            this.f471f = new ArrayList<>();
        }
        if (f466a) {
            Log.v("FragmentManager", "add: " + fragment);
        }
        e(fragment);
        if (fragment.mDetached) {
            return;
        }
        if (this.f471f.contains(fragment)) {
            throw new IllegalStateException("Fragment already added: " + fragment);
        }
        this.f471f.add(fragment);
        fragment.mAdded = true;
        fragment.mRemoving = false;
        if (fragment.mView == null) {
            fragment.mHiddenChanged = false;
        }
        if (fragment.mHasMenu && fragment.mMenuVisible) {
            this.r = true;
        }
        if (z2) {
            b(fragment);
        }
    }

    public void g(Fragment fragment) {
        if (f466a) {
            Log.v("FragmentManager", "remove: " + fragment + " nesting=" + fragment.mBackStackNesting);
        }
        boolean z2 = !fragment.isInBackStack();
        if (!fragment.mDetached || z2) {
            if (this.f471f != null) {
                this.f471f.remove(fragment);
            }
            if (fragment.mHasMenu && fragment.mMenuVisible) {
                this.r = true;
            }
            fragment.mAdded = false;
            fragment.mRemoving = true;
        }
    }

    public void h(Fragment fragment) {
        boolean z2 = true;
        if (f466a) {
            Log.v("FragmentManager", "hide: " + fragment);
        }
        if (!fragment.mHidden) {
            fragment.mHidden = true;
            if (fragment.mHiddenChanged) {
                z2 = false;
            }
            fragment.mHiddenChanged = z2;
        }
    }

    public void i(Fragment fragment) {
        boolean z2 = false;
        if (f466a) {
            Log.v("FragmentManager", "show: " + fragment);
        }
        if (fragment.mHidden) {
            fragment.mHidden = false;
            if (!fragment.mHiddenChanged) {
                z2 = true;
            }
            fragment.mHiddenChanged = z2;
        }
    }

    public void j(Fragment fragment) {
        if (f466a) {
            Log.v("FragmentManager", "detach: " + fragment);
        }
        if (!fragment.mDetached) {
            fragment.mDetached = true;
            if (fragment.mAdded) {
                if (this.f471f != null) {
                    if (f466a) {
                        Log.v("FragmentManager", "remove from detach: " + fragment);
                    }
                    this.f471f.remove(fragment);
                }
                if (fragment.mHasMenu && fragment.mMenuVisible) {
                    this.r = true;
                }
                fragment.mAdded = false;
            }
        }
    }

    public void k(Fragment fragment) {
        if (f466a) {
            Log.v("FragmentManager", "attach: " + fragment);
        }
        if (fragment.mDetached) {
            fragment.mDetached = false;
            if (!fragment.mAdded) {
                if (this.f471f == null) {
                    this.f471f = new ArrayList<>();
                }
                if (this.f471f.contains(fragment)) {
                    throw new IllegalStateException("Fragment already added: " + fragment);
                }
                if (f466a) {
                    Log.v("FragmentManager", "add from attach: " + fragment);
                }
                this.f471f.add(fragment);
                fragment.mAdded = true;
                if (fragment.mHasMenu && fragment.mMenuVisible) {
                    this.r = true;
                }
            }
        }
    }

    public Fragment b(int i2) {
        if (this.f471f != null) {
            for (int size = this.f471f.size() - 1; size >= 0; size--) {
                Fragment fragment = this.f471f.get(size);
                if (fragment != null && fragment.mFragmentId == i2) {
                    return fragment;
                }
            }
        }
        if (this.f470e != null) {
            for (int size2 = this.f470e.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = this.f470e.get(size2);
                if (fragment2 != null && fragment2.mFragmentId == i2) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public Fragment a(String str) {
        if (!(this.f471f == null || str == null)) {
            for (int size = this.f471f.size() - 1; size >= 0; size--) {
                Fragment fragment = this.f471f.get(size);
                if (fragment != null && str.equals(fragment.mTag)) {
                    return fragment;
                }
            }
        }
        if (!(this.f470e == null || str == null)) {
            for (int size2 = this.f470e.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = this.f470e.get(size2);
                if (fragment2 != null && str.equals(fragment2.mTag)) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public Fragment b(String str) {
        Fragment findFragmentByWho;
        if (!(this.f470e == null || str == null)) {
            for (int size = this.f470e.size() - 1; size >= 0; size--) {
                Fragment fragment = this.f470e.get(size);
                if (fragment != null && (findFragmentByWho = fragment.findFragmentByWho(str)) != null) {
                    return findFragmentByWho;
                }
            }
        }
        return null;
    }

    private void v() {
        if (this.s) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        } else if (this.u != null) {
            throw new IllegalStateException("Can not perform this action inside of " + this.u);
        }
    }

    public void a(c cVar, boolean z2) {
        if (!z2) {
            v();
        }
        synchronized (this) {
            if (this.t || this.n == null) {
                throw new IllegalStateException("Activity has been destroyed");
            }
            if (this.f468c == null) {
                this.f468c = new ArrayList<>();
            }
            this.f468c.add(cVar);
            w();
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        boolean z2 = true;
        synchronized (this) {
            boolean z3 = this.B != null && !this.B.isEmpty();
            if (this.f468c == null || this.f468c.size() != 1) {
                z2 = false;
            }
            if (z3 || z2) {
                this.n.j().removeCallbacks(this.C);
                this.n.j().post(this.C);
            }
        }
    }

    public int a(h hVar) {
        int i2;
        synchronized (this) {
            if (this.k == null || this.k.size() <= 0) {
                if (this.j == null) {
                    this.j = new ArrayList<>();
                }
                i2 = this.j.size();
                if (f466a) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + hVar);
                }
                this.j.add(hVar);
            } else {
                i2 = this.k.remove(this.k.size() - 1).intValue();
                if (f466a) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + hVar);
                }
                this.j.set(i2, hVar);
            }
        }
        return i2;
    }

    public void a(int i2, h hVar) {
        synchronized (this) {
            if (this.j == null) {
                this.j = new ArrayList<>();
            }
            int size = this.j.size();
            if (i2 < size) {
                if (f466a) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + hVar);
                }
                this.j.set(i2, hVar);
            } else {
                while (size < i2) {
                    this.j.add(null);
                    if (this.k == null) {
                        this.k = new ArrayList<>();
                    }
                    if (f466a) {
                        Log.v("FragmentManager", "Adding available back stack index " + size);
                    }
                    this.k.add(Integer.valueOf(size));
                    size++;
                }
                if (f466a) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + hVar);
                }
                this.j.add(hVar);
            }
        }
    }

    public void c(int i2) {
        synchronized (this) {
            this.j.set(i2, null);
            if (this.k == null) {
                this.k = new ArrayList<>();
            }
            if (f466a) {
                Log.v("FragmentManager", "Freeing back stack index " + i2);
            }
            this.k.add(Integer.valueOf(i2));
        }
    }

    private void c(boolean z2) {
        if (this.f469d) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        } else if (Looper.myLooper() != this.n.j().getLooper()) {
            throw new IllegalStateException("Must be called from main thread of fragment host");
        } else {
            if (!z2) {
                v();
            }
            if (this.w == null) {
                this.w = new ArrayList<>();
                this.x = new ArrayList<>();
            }
            this.f469d = true;
            try {
                a((ArrayList<h>) null, (ArrayList<Boolean>) null);
            } finally {
                this.f469d = false;
            }
        }
    }

    public void b(c cVar, boolean z2) {
        c(z2);
        if (cVar.a(this.w, this.x)) {
            this.f469d = true;
            try {
                b(this.w, this.x);
            } finally {
                x();
            }
        }
        f();
    }

    private void x() {
        this.f469d = false;
        this.x.clear();
        this.w.clear();
    }

    /* JADX INFO: finally extract failed */
    public boolean e() {
        c(true);
        boolean z2 = false;
        while (c(this.w, this.x)) {
            this.f469d = true;
            try {
                b(this.w, this.x);
                x();
                z2 = true;
            } catch (Throwable th) {
                x();
                throw th;
            }
        }
        f();
        return z2;
    }

    private void a(ArrayList<h> arrayList, ArrayList<Boolean> arrayList2) {
        int indexOf;
        int indexOf2;
        int i2 = 0;
        int size = this.B == null ? 0 : this.B.size();
        while (i2 < size) {
            e eVar = this.B.get(i2);
            if (arrayList != null && !eVar.f486a && (indexOf2 = arrayList.indexOf(eVar.f487b)) != -1 && arrayList2.get(indexOf2).booleanValue()) {
                eVar.e();
            } else if (eVar.c() || (arrayList != null && eVar.f487b.a(arrayList, 0, arrayList.size()))) {
                this.B.remove(i2);
                i2--;
                size--;
                if (arrayList == null || eVar.f486a || (indexOf = arrayList.indexOf(eVar.f487b)) == -1 || !arrayList2.get(indexOf).booleanValue()) {
                    eVar.d();
                } else {
                    eVar.e();
                }
            }
            i2++;
            size = size;
        }
    }

    private void b(ArrayList<h> arrayList, ArrayList<Boolean> arrayList2) {
        int i2;
        int i3 = 0;
        if (arrayList != null && !arrayList.isEmpty()) {
            if (arrayList2 == null || arrayList.size() != arrayList2.size()) {
                throw new IllegalStateException("Internal error with the back stack records");
            }
            a(arrayList, arrayList2);
            int size = arrayList.size();
            int i4 = 0;
            while (i3 < size) {
                if (!arrayList.get(i3).u) {
                    if (i4 != i3) {
                        a(arrayList, arrayList2, i4, i3);
                    }
                    int i5 = i3 + 1;
                    if (arrayList2.get(i3).booleanValue()) {
                        while (i5 < size && arrayList2.get(i5).booleanValue() && !arrayList.get(i5).u) {
                            i5++;
                        }
                    }
                    int i6 = i5;
                    a(arrayList, arrayList2, i3, i6);
                    i4 = i6;
                    i2 = i6 - 1;
                } else {
                    i2 = i3;
                }
                i3 = i2 + 1;
            }
            if (i4 != size) {
                a(arrayList, arrayList2, i4, size);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    private void a(ArrayList<h> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        int i4;
        boolean z2;
        boolean z3 = arrayList.get(i2).u;
        if (this.y == null) {
            this.y = new ArrayList<>();
        } else {
            this.y.clear();
        }
        if (this.f471f != null) {
            this.y.addAll(this.f471f);
        }
        int i5 = i2;
        boolean z4 = false;
        while (i5 < i3) {
            h hVar = arrayList.get(i5);
            if (!arrayList2.get(i5).booleanValue()) {
                hVar.a(this.y);
            } else {
                hVar.b(this.y);
            }
            if (z4 || hVar.j) {
                z2 = true;
            } else {
                z2 = false;
            }
            i5++;
            z4 = z2;
        }
        this.y.clear();
        if (!z3) {
            u.a(this, arrayList, arrayList2, i2, i3, false);
        }
        b(arrayList, arrayList2, i2, i3);
        if (z3) {
            android.support.v4.util.a aVar = new android.support.v4.util.a();
            b(aVar);
            i4 = a(arrayList, arrayList2, i2, i3, aVar);
            a(aVar);
        } else {
            i4 = i3;
        }
        if (i4 != i2 && z3) {
            u.a(this, arrayList, arrayList2, i2, i4, true);
            a(this.m, true);
        }
        while (i2 < i3) {
            h hVar2 = arrayList.get(i2);
            if (arrayList2.get(i2).booleanValue() && hVar2.n >= 0) {
                c(hVar2.n);
                hVar2.n = -1;
            }
            i2++;
        }
        if (z4) {
            g();
        }
    }

    private void a(android.support.v4.util.a<Fragment> aVar) {
        int size = aVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            Fragment b2 = aVar.b(i2);
            if (!b2.mAdded) {
                View view = b2.getView();
                if (Build.VERSION.SDK_INT < 11) {
                    b2.getView().setVisibility(4);
                } else {
                    b2.mPostponedAlpha = view.getAlpha();
                    view.setAlpha(0.0f);
                }
            }
        }
    }

    private int a(ArrayList<h> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3, android.support.v4.util.a<Fragment> aVar) {
        boolean z2;
        int i4;
        int i5 = i3 - 1;
        int i6 = i3;
        while (i5 >= i2) {
            h hVar = arrayList.get(i5);
            boolean booleanValue = arrayList2.get(i5).booleanValue();
            if (!hVar.f() || hVar.a(arrayList, i5 + 1, i3)) {
                z2 = false;
            } else {
                z2 = true;
            }
            if (z2) {
                if (this.B == null) {
                    this.B = new ArrayList<>();
                }
                e eVar = new e(hVar, booleanValue);
                this.B.add(eVar);
                hVar.a(eVar);
                if (booleanValue) {
                    hVar.e();
                } else {
                    hVar.b(false);
                }
                int i7 = i6 - 1;
                if (i5 != i7) {
                    arrayList.remove(i5);
                    arrayList.add(i7, hVar);
                }
                b(aVar);
                i4 = i7;
            } else {
                i4 = i6;
            }
            i5--;
            i6 = i4;
        }
        return i6;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    /* access modifiers changed from: private */
    public void a(h hVar, boolean z2, boolean z3, boolean z4) {
        ArrayList arrayList = new ArrayList(1);
        ArrayList arrayList2 = new ArrayList(1);
        arrayList.add(hVar);
        arrayList2.add(Boolean.valueOf(z2));
        b(arrayList, arrayList2, 0, 1);
        if (z3) {
            u.a(this, arrayList, arrayList2, 0, 1, true);
        }
        if (z4) {
            a(this.m, true);
        }
        if (this.f470e != null) {
            int size = this.f470e.size();
            for (int i2 = 0; i2 < size; i2++) {
                Fragment fragment = this.f470e.get(i2);
                if (fragment != null && fragment.mView != null && fragment.mIsNewlyAdded && hVar.b(fragment.mContainerId)) {
                    if (Build.VERSION.SDK_INT >= 11 && fragment.mPostponedAlpha > 0.0f) {
                        fragment.mView.setAlpha(fragment.mPostponedAlpha);
                    }
                    if (z4) {
                        fragment.mPostponedAlpha = 0.0f;
                    } else {
                        fragment.mPostponedAlpha = -1.0f;
                        fragment.mIsNewlyAdded = false;
                    }
                }
            }
        }
    }

    private Fragment n(Fragment fragment) {
        ViewGroup viewGroup = fragment.mContainer;
        View view = fragment.mView;
        if (viewGroup == null || view == null) {
            return null;
        }
        for (int indexOf = this.f471f.indexOf(fragment) - 1; indexOf >= 0; indexOf--) {
            Fragment fragment2 = this.f471f.get(indexOf);
            if (fragment2.mContainer == viewGroup && fragment2.mView != null) {
                return fragment2;
            }
        }
        return null;
    }

    private static void b(ArrayList<h> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        while (i2 < i3) {
            h hVar = arrayList.get(i2);
            if (arrayList2.get(i2).booleanValue()) {
                hVar.a(-1);
                hVar.b(i2 == i3 + -1);
            } else {
                hVar.a(1);
                hVar.e();
            }
            i2++;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
      android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    private void b(android.support.v4.util.a<Fragment> aVar) {
        if (this.m >= 1) {
            int min = Math.min(this.m, 4);
            int size = this.f471f == null ? 0 : this.f471f.size();
            for (int i2 = 0; i2 < size; i2++) {
                Fragment fragment = this.f471f.get(i2);
                if (fragment.mState < min) {
                    a(fragment, min, fragment.getNextAnim(), fragment.getNextTransition(), false);
                    if (fragment.mView != null && !fragment.mHidden && fragment.mIsNewlyAdded) {
                        aVar.add(fragment);
                    }
                }
            }
        }
    }

    private void y() {
        if (this.B != null) {
            while (!this.B.isEmpty()) {
                this.B.remove(0).d();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
      android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    private void z() {
        int size;
        if (this.f470e == null) {
            size = 0;
        } else {
            size = this.f470e.size();
        }
        for (int i2 = 0; i2 < size; i2++) {
            Fragment fragment = this.f470e.get(i2);
            if (!(fragment == null || fragment.getAnimatingAway() == null)) {
                int stateAfterAnimating = fragment.getStateAfterAnimating();
                View animatingAway = fragment.getAnimatingAway();
                fragment.setAnimatingAway(null);
                Animation animation = animatingAway.getAnimation();
                if (animation != null) {
                    animation.cancel();
                }
                a(fragment, stateAfterAnimating, 0, 0, false);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        if (r3 <= 0) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean c(java.util.ArrayList<android.support.v4.app.h> r5, java.util.ArrayList<java.lang.Boolean> r6) {
        /*
            r4 = this;
            r1 = 0
            monitor-enter(r4)
            java.util.ArrayList<android.support.v4.app.r$c> r0 = r4.f468c     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x000e
            java.util.ArrayList<android.support.v4.app.r$c> r0 = r4.f468c     // Catch:{ all -> 0x003e }
            int r0 = r0.size()     // Catch:{ all -> 0x003e }
            if (r0 != 0) goto L_0x0011
        L_0x000e:
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            r0 = r1
        L_0x0010:
            return r0
        L_0x0011:
            java.util.ArrayList<android.support.v4.app.r$c> r0 = r4.f468c     // Catch:{ all -> 0x003e }
            int r3 = r0.size()     // Catch:{ all -> 0x003e }
            r2 = r1
        L_0x0018:
            if (r2 >= r3) goto L_0x0029
            java.util.ArrayList<android.support.v4.app.r$c> r0 = r4.f468c     // Catch:{ all -> 0x003e }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x003e }
            android.support.v4.app.r$c r0 = (android.support.v4.app.r.c) r0     // Catch:{ all -> 0x003e }
            r0.a(r5, r6)     // Catch:{ all -> 0x003e }
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0018
        L_0x0029:
            java.util.ArrayList<android.support.v4.app.r$c> r0 = r4.f468c     // Catch:{ all -> 0x003e }
            r0.clear()     // Catch:{ all -> 0x003e }
            android.support.v4.app.FragmentHostCallback r0 = r4.n     // Catch:{ all -> 0x003e }
            android.os.Handler r0 = r0.j()     // Catch:{ all -> 0x003e }
            java.lang.Runnable r2 = r4.C     // Catch:{ all -> 0x003e }
            r0.removeCallbacks(r2)     // Catch:{ all -> 0x003e }
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            if (r3 <= 0) goto L_0x0041
            r0 = 1
            goto L_0x0010
        L_0x003e:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x003e }
            throw r0
        L_0x0041:
            r0 = r1
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.r.c(java.util.ArrayList, java.util.ArrayList):boolean");
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.v) {
            boolean z2 = false;
            for (int i2 = 0; i2 < this.f470e.size(); i2++) {
                Fragment fragment = this.f470e.get(i2);
                if (!(fragment == null || fragment.mLoaderManager == null)) {
                    z2 |= fragment.mLoaderManager.a();
                }
            }
            if (!z2) {
                this.v = false;
                d();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (this.l != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.l.size()) {
                    this.l.get(i3).a();
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(h hVar) {
        if (this.f473h == null) {
            this.f473h = new ArrayList<>();
        }
        this.f473h.add(hVar);
        g();
    }

    /* access modifiers changed from: package-private */
    public boolean a(ArrayList<h> arrayList, ArrayList<Boolean> arrayList2, String str, int i2, int i3) {
        int i4;
        if (this.f473h == null) {
            return false;
        }
        if (str == null && i2 < 0 && (i3 & 1) == 0) {
            int size = this.f473h.size() - 1;
            if (size < 0) {
                return false;
            }
            arrayList.add(this.f473h.remove(size));
            arrayList2.add(true);
        } else {
            int i5 = -1;
            if (str != null || i2 >= 0) {
                int size2 = this.f473h.size() - 1;
                while (i4 >= 0) {
                    h hVar = this.f473h.get(i4);
                    if ((str != null && str.equals(hVar.g())) || (i2 >= 0 && i2 == hVar.n)) {
                        break;
                    }
                    size2 = i4 - 1;
                }
                if (i4 < 0) {
                    return false;
                }
                if ((i3 & 1) != 0) {
                    i4--;
                    while (i4 >= 0) {
                        h hVar2 = this.f473h.get(i4);
                        if ((str == null || !str.equals(hVar2.g())) && (i2 < 0 || i2 != hVar2.n)) {
                            break;
                        }
                        i4--;
                    }
                }
                i5 = i4;
            }
            if (i5 == this.f473h.size() - 1) {
                return false;
            }
            for (int size3 = this.f473h.size() - 1; size3 > i5; size3--) {
                arrayList.add(this.f473h.remove(size3));
                arrayList2.add(true);
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public s h() {
        ArrayList arrayList;
        ArrayList arrayList2;
        boolean z2;
        s h2;
        ArrayList arrayList3;
        if (this.f470e != null) {
            int i2 = 0;
            arrayList2 = null;
            arrayList = null;
            while (i2 < this.f470e.size()) {
                Fragment fragment = this.f470e.get(i2);
                if (fragment != null) {
                    if (fragment.mRetainInstance) {
                        if (arrayList == null) {
                            arrayList = new ArrayList();
                        }
                        arrayList.add(fragment);
                        fragment.mRetaining = true;
                        fragment.mTargetIndex = fragment.mTarget != null ? fragment.mTarget.mIndex : -1;
                        if (f466a) {
                            Log.v("FragmentManager", "retainNonConfig: keeping retained " + fragment);
                        }
                    }
                    if (fragment.mChildFragmentManager == null || (h2 = fragment.mChildFragmentManager.h()) == null) {
                        z2 = false;
                    } else {
                        if (arrayList2 == null) {
                            arrayList3 = new ArrayList();
                            for (int i3 = 0; i3 < i2; i3++) {
                                arrayList3.add(null);
                            }
                        } else {
                            arrayList3 = arrayList2;
                        }
                        arrayList3.add(h2);
                        arrayList2 = arrayList3;
                        z2 = true;
                    }
                    if (arrayList2 != null && !z2) {
                        arrayList2.add(null);
                    }
                }
                i2++;
                arrayList = arrayList;
            }
        } else {
            arrayList2 = null;
            arrayList = null;
        }
        if (arrayList == null && arrayList2 == null) {
            return null;
        }
        return new s(arrayList, arrayList2);
    }

    /* access modifiers changed from: package-private */
    public void l(Fragment fragment) {
        if (fragment.mInnerView != null) {
            if (this.A == null) {
                this.A = new SparseArray<>();
            } else {
                this.A.clear();
            }
            fragment.mInnerView.saveHierarchyState(this.A);
            if (this.A.size() > 0) {
                fragment.mSavedViewState = this.A;
                this.A = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Bundle m(Fragment fragment) {
        Bundle bundle;
        if (this.z == null) {
            this.z = new Bundle();
        }
        fragment.performSaveInstanceState(this.z);
        c(fragment, this.z, false);
        if (!this.z.isEmpty()) {
            bundle = this.z;
            this.z = null;
        } else {
            bundle = null;
        }
        if (fragment.mView != null) {
            l(fragment);
        }
        if (fragment.mSavedViewState != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", fragment.mSavedViewState);
        }
        if (!fragment.mUserVisibleHint) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", fragment.mUserVisibleHint);
        }
        return bundle;
    }

    /* access modifiers changed from: package-private */
    public Parcelable i() {
        int[] iArr;
        int size;
        int size2;
        boolean z2;
        BackStackState[] backStackStateArr = null;
        y();
        z();
        e();
        if (f467b) {
            this.s = true;
        }
        if (this.f470e == null || this.f470e.size() <= 0) {
            return null;
        }
        int size3 = this.f470e.size();
        FragmentState[] fragmentStateArr = new FragmentState[size3];
        int i2 = 0;
        boolean z3 = false;
        while (i2 < size3) {
            Fragment fragment = this.f470e.get(i2);
            if (fragment != null) {
                if (fragment.mIndex < 0) {
                    a(new IllegalStateException("Failure saving state: active " + fragment + " has cleared index: " + fragment.mIndex));
                }
                FragmentState fragmentState = new FragmentState(fragment);
                fragmentStateArr[i2] = fragmentState;
                if (fragment.mState <= 0 || fragmentState.k != null) {
                    fragmentState.k = fragment.mSavedFragmentState;
                } else {
                    fragmentState.k = m(fragment);
                    if (fragment.mTarget != null) {
                        if (fragment.mTarget.mIndex < 0) {
                            a(new IllegalStateException("Failure saving state: " + fragment + " has target not in fragment manager: " + fragment.mTarget));
                        }
                        if (fragmentState.k == null) {
                            fragmentState.k = new Bundle();
                        }
                        a(fragmentState.k, "android:target_state", fragment.mTarget);
                        if (fragment.mTargetRequestCode != 0) {
                            fragmentState.k.putInt("android:target_req_state", fragment.mTargetRequestCode);
                        }
                    }
                }
                if (f466a) {
                    Log.v("FragmentManager", "Saved state of " + fragment + ": " + fragmentState.k);
                }
                z2 = true;
            } else {
                z2 = z3;
            }
            i2++;
            z3 = z2;
        }
        if (z3) {
            if (this.f471f == null || (size2 = this.f471f.size()) <= 0) {
                iArr = null;
            } else {
                iArr = new int[size2];
                for (int i3 = 0; i3 < size2; i3++) {
                    iArr[i3] = this.f471f.get(i3).mIndex;
                    if (iArr[i3] < 0) {
                        a(new IllegalStateException("Failure saving state: active " + this.f471f.get(i3) + " has cleared index: " + iArr[i3]));
                    }
                    if (f466a) {
                        Log.v("FragmentManager", "saveAllState: adding fragment #" + i3 + ": " + this.f471f.get(i3));
                    }
                }
            }
            if (this.f473h != null && (size = this.f473h.size()) > 0) {
                backStackStateArr = new BackStackState[size];
                for (int i4 = 0; i4 < size; i4++) {
                    backStackStateArr[i4] = new BackStackState(this.f473h.get(i4));
                    if (f466a) {
                        Log.v("FragmentManager", "saveAllState: adding back stack #" + i4 + ": " + this.f473h.get(i4));
                    }
                }
            }
            FragmentManagerState fragmentManagerState = new FragmentManagerState();
            fragmentManagerState.f287a = fragmentStateArr;
            fragmentManagerState.f288b = iArr;
            fragmentManagerState.f289c = backStackStateArr;
            return fragmentManagerState;
        } else if (!f466a) {
            return null;
        } else {
            Log.v("FragmentManager", "saveAllState: no fragments!");
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.h.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.h.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.t
      android.support.v4.app.h.a(java.util.ArrayList<android.support.v4.app.h>, int, int):boolean
      android.support.v4.app.t.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.t
      android.support.v4.app.h.a(java.lang.String, java.io.PrintWriter, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Parcelable parcelable, s sVar) {
        List<s> list;
        int i2;
        s sVar2;
        int i3;
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.f287a != null) {
                if (sVar != null) {
                    List<Fragment> a2 = sVar.a();
                    List<s> b2 = sVar.b();
                    if (a2 != null) {
                        i3 = a2.size();
                    } else {
                        i3 = 0;
                    }
                    for (int i4 = 0; i4 < i3; i4++) {
                        Fragment fragment = a2.get(i4);
                        if (f466a) {
                            Log.v("FragmentManager", "restoreAllState: re-attaching retained " + fragment);
                        }
                        FragmentState fragmentState = fragmentManagerState.f287a[fragment.mIndex];
                        fragmentState.l = fragment;
                        fragment.mSavedViewState = null;
                        fragment.mBackStackNesting = 0;
                        fragment.mInLayout = false;
                        fragment.mAdded = false;
                        fragment.mTarget = null;
                        if (fragmentState.k != null) {
                            fragmentState.k.setClassLoader(this.n.i().getClassLoader());
                            fragment.mSavedViewState = fragmentState.k.getSparseParcelableArray("android:view_state");
                            fragment.mSavedFragmentState = fragmentState.k;
                        }
                    }
                    list = b2;
                } else {
                    list = null;
                }
                this.f470e = new ArrayList<>(fragmentManagerState.f287a.length);
                if (this.f472g != null) {
                    this.f472g.clear();
                }
                for (int i5 = 0; i5 < fragmentManagerState.f287a.length; i5++) {
                    FragmentState fragmentState2 = fragmentManagerState.f287a[i5];
                    if (fragmentState2 != null) {
                        if (list == null || i5 >= list.size()) {
                            sVar2 = null;
                        } else {
                            sVar2 = (s) list.get(i5);
                        }
                        Fragment a3 = fragmentState2.a(this.n, this.p, sVar2);
                        if (f466a) {
                            Log.v("FragmentManager", "restoreAllState: active #" + i5 + ": " + a3);
                        }
                        this.f470e.add(a3);
                        fragmentState2.l = null;
                    } else {
                        this.f470e.add(null);
                        if (this.f472g == null) {
                            this.f472g = new ArrayList<>();
                        }
                        if (f466a) {
                            Log.v("FragmentManager", "restoreAllState: avail #" + i5);
                        }
                        this.f472g.add(Integer.valueOf(i5));
                    }
                }
                if (sVar != null) {
                    List<Fragment> a4 = sVar.a();
                    if (a4 != null) {
                        i2 = a4.size();
                    } else {
                        i2 = 0;
                    }
                    for (int i6 = 0; i6 < i2; i6++) {
                        Fragment fragment2 = a4.get(i6);
                        if (fragment2.mTargetIndex >= 0) {
                            if (fragment2.mTargetIndex < this.f470e.size()) {
                                fragment2.mTarget = this.f470e.get(fragment2.mTargetIndex);
                            } else {
                                Log.w("FragmentManager", "Re-attaching retained fragment " + fragment2 + " target no longer exists: " + fragment2.mTargetIndex);
                                fragment2.mTarget = null;
                            }
                        }
                    }
                }
                if (fragmentManagerState.f288b != null) {
                    this.f471f = new ArrayList<>(fragmentManagerState.f288b.length);
                    for (int i7 = 0; i7 < fragmentManagerState.f288b.length; i7++) {
                        Fragment fragment3 = this.f470e.get(fragmentManagerState.f288b[i7]);
                        if (fragment3 == null) {
                            a(new IllegalStateException("No instantiated fragment for index #" + fragmentManagerState.f288b[i7]));
                        }
                        fragment3.mAdded = true;
                        if (f466a) {
                            Log.v("FragmentManager", "restoreAllState: added #" + i7 + ": " + fragment3);
                        }
                        if (this.f471f.contains(fragment3)) {
                            throw new IllegalStateException("Already added!");
                        }
                        this.f471f.add(fragment3);
                    }
                } else {
                    this.f471f = null;
                }
                if (fragmentManagerState.f289c != null) {
                    this.f473h = new ArrayList<>(fragmentManagerState.f289c.length);
                    for (int i8 = 0; i8 < fragmentManagerState.f289c.length; i8++) {
                        h a5 = fragmentManagerState.f289c[i8].a(this);
                        if (f466a) {
                            Log.v("FragmentManager", "restoreAllState: back stack #" + i8 + " (index " + a5.n + "): " + a5);
                            PrintWriter printWriter = new PrintWriter(new android.support.v4.util.d("FragmentManager"));
                            a5.a("  ", printWriter, false);
                            printWriter.close();
                        }
                        this.f473h.add(a5);
                        if (a5.n >= 0) {
                            a(a5.n, a5);
                        }
                    }
                    return;
                }
                this.f473h = null;
            }
        }
    }

    public void a(FragmentHostCallback fragmentHostCallback, o oVar, Fragment fragment) {
        if (this.n != null) {
            throw new IllegalStateException("Already attached");
        }
        this.n = fragmentHostCallback;
        this.o = oVar;
        this.p = fragment;
    }

    public void j() {
        this.s = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    public void k() {
        this.s = false;
        this.f469d = true;
        a(1, false);
        this.f469d = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    public void l() {
        this.s = false;
        this.f469d = true;
        a(2, false);
        this.f469d = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    public void m() {
        this.s = false;
        this.f469d = true;
        a(4, false);
        this.f469d = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    public void n() {
        this.s = false;
        this.f469d = true;
        a(5, false);
        this.f469d = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    public void o() {
        this.f469d = true;
        a(4, false);
        this.f469d = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    public void p() {
        this.s = true;
        this.f469d = true;
        a(3, false);
        this.f469d = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    public void q() {
        this.f469d = true;
        a(2, false);
        this.f469d = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    public void r() {
        this.f469d = true;
        a(1, false);
        this.f469d = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(int, boolean):void */
    public void s() {
        this.t = true;
        e();
        this.f469d = true;
        a(0, false);
        this.f469d = false;
        this.n = null;
        this.o = null;
        this.p = null;
    }

    public void a(boolean z2) {
        if (this.f471f != null) {
            for (int size = this.f471f.size() - 1; size >= 0; size--) {
                Fragment fragment = this.f471f.get(size);
                if (fragment != null) {
                    fragment.performMultiWindowModeChanged(z2);
                }
            }
        }
    }

    public void b(boolean z2) {
        if (this.f471f != null) {
            for (int size = this.f471f.size() - 1; size >= 0; size--) {
                Fragment fragment = this.f471f.get(size);
                if (fragment != null) {
                    fragment.performPictureInPictureModeChanged(z2);
                }
            }
        }
    }

    public void a(Configuration configuration) {
        if (this.f471f != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f471f.size()) {
                    Fragment fragment = this.f471f.get(i3);
                    if (fragment != null) {
                        fragment.performConfigurationChanged(configuration);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void t() {
        if (this.f471f != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f471f.size()) {
                    Fragment fragment = this.f471f.get(i3);
                    if (fragment != null) {
                        fragment.performLowMemory();
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public boolean a(Menu menu, MenuInflater menuInflater) {
        boolean z2;
        ArrayList<Fragment> arrayList = null;
        if (this.f471f != null) {
            int i2 = 0;
            z2 = false;
            while (i2 < this.f471f.size()) {
                Fragment fragment = this.f471f.get(i2);
                if (fragment != null && fragment.performCreateOptionsMenu(menu, menuInflater)) {
                    z2 = true;
                    if (arrayList == null) {
                        arrayList = new ArrayList<>();
                    }
                    arrayList.add(fragment);
                }
                i2++;
                z2 = z2;
            }
        } else {
            z2 = false;
        }
        if (this.i != null) {
            for (int i3 = 0; i3 < this.i.size(); i3++) {
                Fragment fragment2 = this.i.get(i3);
                if (arrayList == null || !arrayList.contains(fragment2)) {
                    fragment2.onDestroyOptionsMenu();
                }
            }
        }
        this.i = arrayList;
        return z2;
    }

    public boolean a(Menu menu) {
        if (this.f471f == null) {
            return false;
        }
        boolean z2 = false;
        for (int i2 = 0; i2 < this.f471f.size(); i2++) {
            Fragment fragment = this.f471f.get(i2);
            if (fragment != null && fragment.performPrepareOptionsMenu(menu)) {
                z2 = true;
            }
        }
        return z2;
    }

    public boolean a(MenuItem menuItem) {
        if (this.f471f == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.f471f.size(); i2++) {
            Fragment fragment = this.f471f.get(i2);
            if (fragment != null && fragment.performOptionsItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public boolean b(MenuItem menuItem) {
        if (this.f471f == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.f471f.size(); i2++) {
            Fragment fragment = this.f471f.get(i2);
            if (fragment != null && fragment.performContextItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public void b(Menu menu) {
        if (this.f471f != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f471f.size()) {
                    Fragment fragment = this.f471f.get(i3);
                    if (fragment != null) {
                        fragment.performOptionsMenuClosed(menu);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, android.content.Context, boolean):void
     arg types: [android.support.v4.app.Fragment, android.content.Context, int]
     candidates:
      android.support.v4.app.r.a(android.content.Context, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String, android.support.v4.app.Fragment):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.os.Bundle, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.FragmentHostCallback, android.support.v4.app.o, android.support.v4.app.Fragment):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, Context context, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).a(fragment, context, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).a(this, fragment, context);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.b(android.support.v4.app.Fragment, android.content.Context, boolean):void
     arg types: [android.support.v4.app.Fragment, android.content.Context, int]
     candidates:
      android.support.v4.app.r.b(android.support.v4.app.Fragment, android.os.Bundle, boolean):void
      android.support.v4.app.r.b(android.support.v4.app.Fragment, android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(Fragment fragment, Context context, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).b(fragment, context, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).b(this, fragment, context);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, android.os.Bundle, boolean):void
     arg types: [android.support.v4.app.Fragment, android.os.Bundle, int]
     candidates:
      android.support.v4.app.r.a(android.content.Context, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String, android.support.v4.app.Fragment):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.content.Context, boolean):void
      android.support.v4.app.r.a(android.support.v4.app.FragmentHostCallback, android.support.v4.app.o, android.support.v4.app.Fragment):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, Bundle bundle, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).a(fragment, bundle, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).a(this, fragment, bundle);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.b(android.support.v4.app.Fragment, android.os.Bundle, boolean):void
     arg types: [android.support.v4.app.Fragment, android.os.Bundle, int]
     candidates:
      android.support.v4.app.r.b(android.support.v4.app.Fragment, android.content.Context, boolean):void
      android.support.v4.app.r.b(android.support.v4.app.Fragment, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(Fragment fragment, Bundle bundle, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).b(fragment, bundle, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).b(this, fragment, bundle);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, android.view.View, android.os.Bundle, boolean):void
     arg types: [android.support.v4.app.Fragment, android.view.View, android.os.Bundle, int]
     candidates:
      android.support.v4.app.r.a(android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int):void
      android.support.v4.app.r.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.r.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.q.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.view.m.a(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v4.app.r.a(android.support.v4.app.Fragment, android.view.View, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, View view, Bundle bundle, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).a(fragment, view, bundle, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).a(this, fragment, view, bundle);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.b(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.r.b(int, boolean):int
      android.support.v4.app.r.b(android.view.View, android.view.animation.Animation):void
      android.support.v4.app.r.b(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.b(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.b(android.support.v4.app.Fragment, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(Fragment fragment, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).b(fragment, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).a(this, fragment);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.c(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.r.c(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):boolean
      android.support.v4.app.r.c(android.support.v4.app.Fragment, boolean):void */
    /* access modifiers changed from: package-private */
    public void c(Fragment fragment, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).c(fragment, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).b(this, fragment);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(Fragment fragment, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).d(fragment, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).c(this, fragment);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e(Fragment fragment, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).e(fragment, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).d(this, fragment);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Fragment fragment, Bundle bundle, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).c(fragment, bundle, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).c(this, fragment, bundle);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f(Fragment fragment, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).f(fragment, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).e(this, fragment);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void g(Fragment fragment, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).g(fragment, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).f(this, fragment);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void h(Fragment fragment, boolean z2) {
        if (this.p != null) {
            q fragmentManager = this.p.getFragmentManager();
            if (fragmentManager instanceof r) {
                ((r) fragmentManager).h(fragment, true);
            }
        }
        if (this.H != null) {
            Iterator<h<q.a, Boolean>> it = this.H.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (!z2 || ((Boolean) next.f881b).booleanValue()) {
                    ((q.a) next.f880a).g(this, fragment);
                }
            }
        }
    }

    public static int d(int i2) {
        switch (i2) {
            case 4097:
                return 8194;
            case 4099:
                return 4099;
            case 8194:
                return 4097;
            default:
                return 0;
        }
    }

    public static int b(int i2, boolean z2) {
        switch (i2) {
            case 4097:
                return z2 ? 1 : 2;
            case 4099:
                return z2 ? 5 : 6;
            case 8194:
                return z2 ? 3 : 4;
            default:
                return -1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>):void
      android.support.v4.app.r.a(android.view.View, android.view.animation.Animation):boolean
      android.support.v4.app.r.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.r.a(int, int):void
      android.support.v4.app.r.a(int, android.support.v4.app.h):void
      android.support.v4.app.r.a(int, boolean):void
      android.support.v4.app.r.a(android.os.Parcelable, android.support.v4.app.s):void
      android.support.v4.app.r.a(android.support.v4.app.r$c, boolean):void
      android.support.v4.app.r.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.q.a(int, int):void
      android.support.v4.app.r.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
      android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
      android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        String str2;
        int i2;
        Fragment fragment;
        if (!"fragment".equals(str)) {
            return null;
        }
        String attributeValue = attributeSet.getAttributeValue(null, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.f481a);
        if (attributeValue == null) {
            str2 = obtainStyledAttributes.getString(0);
        } else {
            str2 = attributeValue;
        }
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (!Fragment.isSupportFragmentClass(this.n.i(), str2)) {
            return null;
        }
        if (view != null) {
            i2 = view.getId();
        } else {
            i2 = 0;
        }
        if (i2 == -1 && resourceId == -1 && string == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + str2);
        }
        Fragment b2 = resourceId != -1 ? b(resourceId) : null;
        if (b2 == null && string != null) {
            b2 = a(string);
        }
        if (b2 == null && i2 != -1) {
            b2 = b(i2);
        }
        if (f466a) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(resourceId) + " fname=" + str2 + " existing=" + b2);
        }
        if (b2 == null) {
            Fragment instantiate = Fragment.instantiate(context, str2);
            instantiate.mFromLayout = true;
            instantiate.mFragmentId = resourceId != 0 ? resourceId : i2;
            instantiate.mContainerId = i2;
            instantiate.mTag = string;
            instantiate.mInLayout = true;
            instantiate.mFragmentManager = this;
            instantiate.mHost = this.n;
            instantiate.onInflate(this.n.i(), attributeSet, instantiate.mSavedFragmentState);
            a(instantiate, true);
            fragment = instantiate;
        } else if (b2.mInLayout) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string + ", or parent id 0x" + Integer.toHexString(i2) + " with another fragment for " + str2);
        } else {
            b2.mInLayout = true;
            b2.mHost = this.n;
            if (!b2.mRetaining) {
                b2.onInflate(this.n.i(), attributeSet, b2.mSavedFragmentState);
            }
            fragment = b2;
        }
        if (this.m >= 1 || !fragment.mFromLayout) {
            b(fragment);
        } else {
            a(fragment, 1, 0, 0, false);
        }
        if (fragment.mView == null) {
            throw new IllegalStateException("Fragment " + str2 + " did not create a view.");
        }
        if (resourceId != 0) {
            fragment.mView.setId(resourceId);
        }
        if (fragment.mView.getTag() == null) {
            fragment.mView.setTag(string);
        }
        return fragment.mView;
    }

    /* access modifiers changed from: package-private */
    public m u() {
        return this;
    }

    /* compiled from: FragmentManager */
    private class d implements c {

        /* renamed from: a  reason: collision with root package name */
        final String f482a;

        /* renamed from: b  reason: collision with root package name */
        final int f483b;

        /* renamed from: c  reason: collision with root package name */
        final int f484c;

        d(String str, int i, int i2) {
            this.f482a = str;
            this.f483b = i;
            this.f484c = i2;
        }

        public boolean a(ArrayList<h> arrayList, ArrayList<Boolean> arrayList2) {
            return r.this.a(arrayList, arrayList2, this.f482a, this.f483b, this.f484c);
        }
    }

    /* compiled from: FragmentManager */
    static class e implements Fragment.b {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final boolean f486a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final h f487b;

        /* renamed from: c  reason: collision with root package name */
        private int f488c;

        e(h hVar, boolean z) {
            this.f486a = z;
            this.f487b = hVar;
        }

        public void a() {
            this.f488c--;
            if (this.f488c == 0) {
                this.f487b.f448b.w();
            }
        }

        public void b() {
            this.f488c++;
        }

        public boolean c() {
            return this.f488c == 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
         arg types: [android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, int]
         candidates:
          android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
          android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
          android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
          android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
          android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void */
        public void d() {
            boolean z;
            boolean z2 = false;
            if (this.f488c > 0) {
                z = true;
            } else {
                z = false;
            }
            r rVar = this.f487b.f448b;
            int size = rVar.f471f.size();
            for (int i = 0; i < size; i++) {
                Fragment fragment = rVar.f471f.get(i);
                fragment.setOnStartEnterTransitionListener(null);
                if (z && fragment.isPostponed()) {
                    fragment.startPostponedEnterTransition();
                }
            }
            r rVar2 = this.f487b.f448b;
            h hVar = this.f487b;
            boolean z3 = this.f486a;
            if (!z) {
                z2 = true;
            }
            rVar2.a(hVar, z3, z2, true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void
         arg types: [android.support.v4.app.r, android.support.v4.app.h, boolean, int, int]
         candidates:
          android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, int, int, android.support.v4.util.a<android.support.v4.app.Fragment>):int
          android.support.v4.app.r.a(android.content.Context, float, float, float, float):android.view.animation.Animation
          android.support.v4.app.r.a(android.support.v4.app.Fragment, int, int, int, boolean):void
          android.support.v4.app.r.a(java.util.ArrayList<android.support.v4.app.h>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
          android.support.v4.app.r.a(android.support.v4.app.r, android.support.v4.app.h, boolean, boolean, boolean):void */
        public void e() {
            this.f487b.f448b.a(this.f487b, this.f486a, false, false);
        }
    }
}
