package android.support.v4.app;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ag;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@TargetApi(9)
/* compiled from: NotificationCompatBase */
public class ac {

    /* renamed from: a  reason: collision with root package name */
    private static Method f437a;

    /* compiled from: NotificationCompatBase */
    public static abstract class a {

        /* renamed from: android.support.v4.app.ac$a$a  reason: collision with other inner class name */
        /* compiled from: NotificationCompatBase */
        public interface C0010a {
        }

        public abstract int a();

        public abstract CharSequence b();

        public abstract PendingIntent c();

        public abstract Bundle d();

        public abstract boolean e();

        public abstract ag.a[] g();
    }

    /* compiled from: NotificationCompatBase */
    public static abstract class b {

        /* compiled from: NotificationCompatBase */
        public interface a {
        }
    }

    public static Notification a(Notification notification, Context context, CharSequence charSequence, CharSequence charSequence2, PendingIntent pendingIntent, PendingIntent pendingIntent2) {
        if (f437a == null) {
            try {
                f437a = Notification.class.getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class);
            } catch (NoSuchMethodException e2) {
                throw new RuntimeException(e2);
            }
        }
        try {
            f437a.invoke(notification, context, charSequence, charSequence2, pendingIntent);
            notification.fullScreenIntent = pendingIntent2;
            return notification;
        } catch (IllegalAccessException | InvocationTargetException e3) {
            throw new RuntimeException(e3);
        }
    }
}
