package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.j;
import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;

/* compiled from: NavUtils */
public final class y {

    /* renamed from: a  reason: collision with root package name */
    private static final a f554a;

    /* compiled from: NavUtils */
    interface a {
        Intent a(Activity activity);

        String a(Context context, ActivityInfo activityInfo);

        boolean a(Activity activity, Intent intent);

        void b(Activity activity, Intent intent);
    }

    /* compiled from: NavUtils */
    static class b implements a {
        b() {
        }

        public Intent a(Activity activity) {
            String b2 = y.b(activity);
            if (b2 == null) {
                return null;
            }
            ComponentName componentName = new ComponentName(activity, b2);
            try {
                if (y.b(activity, componentName) == null) {
                    return j.a(componentName);
                }
                return new Intent().setComponent(componentName);
            } catch (PackageManager.NameNotFoundException e2) {
                Log.e("NavUtils", "getParentActivityIntent: bad parentActivityName '" + b2 + "' in manifest");
                return null;
            }
        }

        public boolean a(Activity activity, Intent intent) {
            String action = activity.getIntent().getAction();
            return action != null && !action.equals("android.intent.action.MAIN");
        }

        public void b(Activity activity, Intent intent) {
            intent.addFlags(67108864);
            activity.startActivity(intent);
            activity.finish();
        }

        public String a(Context context, ActivityInfo activityInfo) {
            if (activityInfo.metaData == null) {
                return null;
            }
            String string = activityInfo.metaData.getString("android.support.PARENT_ACTIVITY");
            if (string == null) {
                return null;
            }
            if (string.charAt(0) == '.') {
                return context.getPackageName() + string;
            }
            return string;
        }
    }

    /* compiled from: NavUtils */
    static class c extends b {
        c() {
        }

        public Intent a(Activity activity) {
            Intent a2 = z.a(activity);
            if (a2 == null) {
                return b(activity);
            }
            return a2;
        }

        /* access modifiers changed from: package-private */
        public Intent b(Activity activity) {
            return super.a(activity);
        }

        public boolean a(Activity activity, Intent intent) {
            return z.a(activity, intent);
        }

        public void b(Activity activity, Intent intent) {
            z.b(activity, intent);
        }

        public String a(Context context, ActivityInfo activityInfo) {
            String a2 = z.a(activityInfo);
            if (a2 == null) {
                return super.a(context, activityInfo);
            }
            return a2;
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f554a = new c();
        } else {
            f554a = new b();
        }
    }

    public static boolean a(Activity activity, Intent intent) {
        return f554a.a(activity, intent);
    }

    public static void b(Activity activity, Intent intent) {
        f554a.b(activity, intent);
    }

    public static Intent a(Activity activity) {
        return f554a.a(activity);
    }

    public static Intent a(Context context, ComponentName componentName) {
        String b2 = b(context, componentName);
        if (b2 == null) {
            return null;
        }
        ComponentName componentName2 = new ComponentName(componentName.getPackageName(), b2);
        if (b(context, componentName2) == null) {
            return j.a(componentName2);
        }
        return new Intent().setComponent(componentName2);
    }

    public static String b(Activity activity) {
        try {
            return b(activity, activity.getComponentName());
        } catch (PackageManager.NameNotFoundException e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    public static String b(Context context, ComponentName componentName) {
        return f554a.a(context, context.getPackageManager().getActivityInfo(componentName, FileUtils.FileMode.MODE_IWUSR));
    }
}
