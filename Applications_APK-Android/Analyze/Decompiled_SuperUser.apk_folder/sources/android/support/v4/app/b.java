package android.support.v4.app;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;

@TargetApi(18)
/* compiled from: ActionBarDrawerToggleJellybeanMR2 */
class b {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f445a = {16843531};

    public static Object a(Object obj, Activity activity, int i) {
        ActionBar actionBar = activity.getActionBar();
        if (actionBar != null) {
            actionBar.setHomeActionContentDescription(i);
        }
        return obj;
    }
}
