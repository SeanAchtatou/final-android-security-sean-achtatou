package android.support.v4.app;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ac;
import android.util.Log;
import android.util.SparseArray;
import android.widget.RemoteViews;
import com.lody.virtual.helper.utils.FileUtils;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@TargetApi(16)
class NotificationCompatJellybean {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f383a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static Field f384b;

    /* renamed from: c  reason: collision with root package name */
    private static boolean f385c;

    /* renamed from: d  reason: collision with root package name */
    private static final Object f386d = new Object();

    public static class Builder implements aa, ab {

        /* renamed from: a  reason: collision with root package name */
        private Notification.Builder f387a;

        /* renamed from: b  reason: collision with root package name */
        private final Bundle f388b;

        /* renamed from: c  reason: collision with root package name */
        private List<Bundle> f389c = new ArrayList();

        /* renamed from: d  reason: collision with root package name */
        private RemoteViews f390d;

        /* renamed from: e  reason: collision with root package name */
        private RemoteViews f391e;

        public Builder(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, int i4, CharSequence charSequence4, boolean z3, Bundle bundle, String str, boolean z4, String str2, RemoteViews remoteViews2, RemoteViews remoteViews3) {
            this.f387a = new Notification.Builder(context).setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, remoteViews).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(charSequence).setContentText(charSequence2).setSubText(charSequence4).setContentInfo(charSequence3).setContentIntent(pendingIntent).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(pendingIntent2, (notification.flags & FileUtils.FileMode.MODE_IWUSR) != 0).setLargeIcon(bitmap).setNumber(i).setUsesChronometer(z2).setPriority(i4).setProgress(i2, i3, z);
            this.f388b = new Bundle();
            if (bundle != null) {
                this.f388b.putAll(bundle);
            }
            if (z3) {
                this.f388b.putBoolean("android.support.localOnly", true);
            }
            if (str != null) {
                this.f388b.putString("android.support.groupKey", str);
                if (z4) {
                    this.f388b.putBoolean("android.support.isGroupSummary", true);
                } else {
                    this.f388b.putBoolean("android.support.useSideChannel", true);
                }
            }
            if (str2 != null) {
                this.f388b.putString("android.support.sortKey", str2);
            }
            this.f390d = remoteViews2;
            this.f391e = remoteViews3;
        }

        public void a(ac.a aVar) {
            this.f389c.add(NotificationCompatJellybean.a(this.f387a, aVar));
        }

        public Notification.Builder a() {
            return this.f387a;
        }

        public Notification b() {
            Notification build = this.f387a.build();
            Bundle a2 = NotificationCompatJellybean.a(build);
            Bundle bundle = new Bundle(this.f388b);
            for (String next : this.f388b.keySet()) {
                if (a2.containsKey(next)) {
                    bundle.remove(next);
                }
            }
            a2.putAll(bundle);
            SparseArray<Bundle> a3 = NotificationCompatJellybean.a(this.f389c);
            if (a3 != null) {
                NotificationCompatJellybean.a(build).putSparseParcelableArray("android.support.actionExtras", a3);
            }
            if (this.f390d != null) {
                build.contentView = this.f390d;
            }
            if (this.f391e != null) {
                build.bigContentView = this.f391e;
            }
            return build;
        }
    }

    public static void a(ab abVar, CharSequence charSequence, boolean z, CharSequence charSequence2, CharSequence charSequence3) {
        Notification.BigTextStyle bigText = new Notification.BigTextStyle(abVar.a()).setBigContentTitle(charSequence).bigText(charSequence3);
        if (z) {
            bigText.setSummaryText(charSequence2);
        }
    }

    public static void a(ab abVar, CharSequence charSequence, boolean z, CharSequence charSequence2, Bitmap bitmap, Bitmap bitmap2, boolean z2) {
        Notification.BigPictureStyle bigPicture = new Notification.BigPictureStyle(abVar.a()).setBigContentTitle(charSequence).bigPicture(bitmap);
        if (z2) {
            bigPicture.bigLargeIcon(bitmap2);
        }
        if (z) {
            bigPicture.setSummaryText(charSequence2);
        }
    }

    public static void a(ab abVar, CharSequence charSequence, boolean z, CharSequence charSequence2, ArrayList<CharSequence> arrayList) {
        Notification.InboxStyle bigContentTitle = new Notification.InboxStyle(abVar.a()).setBigContentTitle(charSequence);
        if (z) {
            bigContentTitle.setSummaryText(charSequence2);
        }
        Iterator<CharSequence> it = arrayList.iterator();
        while (it.hasNext()) {
            bigContentTitle.addLine(it.next());
        }
    }

    public static SparseArray<Bundle> a(List<Bundle> list) {
        SparseArray<Bundle> sparseArray = null;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Bundle bundle = list.get(i);
            if (bundle != null) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray<>();
                }
                sparseArray.put(i, bundle);
            }
        }
        return sparseArray;
    }

    public static Bundle a(Notification notification) {
        synchronized (f383a) {
            if (f385c) {
                return null;
            }
            try {
                if (f384b == null) {
                    Field declaredField = Notification.class.getDeclaredField("extras");
                    if (!Bundle.class.isAssignableFrom(declaredField.getType())) {
                        Log.e("NotificationCompat", "Notification.extras field is not of type Bundle");
                        f385c = true;
                        return null;
                    }
                    declaredField.setAccessible(true);
                    f384b = declaredField;
                }
                Bundle bundle = (Bundle) f384b.get(notification);
                if (bundle == null) {
                    bundle = new Bundle();
                    f384b.set(notification, bundle);
                }
                return bundle;
            } catch (IllegalAccessException e2) {
                Log.e("NotificationCompat", "Unable to access notification extras", e2);
                f385c = true;
                return null;
            } catch (NoSuchFieldException e3) {
                Log.e("NotificationCompat", "Unable to access notification extras", e3);
                f385c = true;
                return null;
            }
        }
    }

    public static Bundle a(Notification.Builder builder, ac.a aVar) {
        builder.addAction(aVar.a(), aVar.b(), aVar.c());
        Bundle bundle = new Bundle(aVar.d());
        if (aVar.g() != null) {
            bundle.putParcelableArray("android.support.remoteInputs", ah.a(aVar.g()));
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", aVar.e());
        return bundle;
    }
}
