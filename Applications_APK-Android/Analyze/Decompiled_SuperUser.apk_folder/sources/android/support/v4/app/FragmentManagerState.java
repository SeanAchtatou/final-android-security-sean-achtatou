package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: FragmentManager */
final class FragmentManagerState implements Parcelable {
    public static final Parcelable.Creator<FragmentManagerState> CREATOR = new Parcelable.Creator<FragmentManagerState>() {
        /* renamed from: a */
        public FragmentManagerState createFromParcel(Parcel parcel) {
            return new FragmentManagerState(parcel);
        }

        /* renamed from: a */
        public FragmentManagerState[] newArray(int i) {
            return new FragmentManagerState[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    FragmentState[] f287a;

    /* renamed from: b  reason: collision with root package name */
    int[] f288b;

    /* renamed from: c  reason: collision with root package name */
    BackStackState[] f289c;

    public FragmentManagerState() {
    }

    public FragmentManagerState(Parcel parcel) {
        this.f287a = (FragmentState[]) parcel.createTypedArray(FragmentState.CREATOR);
        this.f288b = parcel.createIntArray();
        this.f289c = (BackStackState[]) parcel.createTypedArray(BackStackState.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.f287a, i);
        parcel.writeIntArray(this.f288b);
        parcel.writeTypedArray(this.f289c, i);
    }
}
