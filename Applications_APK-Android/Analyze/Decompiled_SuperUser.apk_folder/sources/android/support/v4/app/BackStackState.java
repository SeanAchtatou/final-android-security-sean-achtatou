package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.h;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

/* compiled from: BackStackRecord */
final class BackStackState implements Parcelable {
    public static final Parcelable.Creator<BackStackState> CREATOR = new Parcelable.Creator<BackStackState>() {
        /* renamed from: a */
        public BackStackState createFromParcel(Parcel parcel) {
            return new BackStackState(parcel);
        }

        /* renamed from: a */
        public BackStackState[] newArray(int i) {
            return new BackStackState[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final int[] f249a;

    /* renamed from: b  reason: collision with root package name */
    final int f250b;

    /* renamed from: c  reason: collision with root package name */
    final int f251c;

    /* renamed from: d  reason: collision with root package name */
    final String f252d;

    /* renamed from: e  reason: collision with root package name */
    final int f253e;

    /* renamed from: f  reason: collision with root package name */
    final int f254f;

    /* renamed from: g  reason: collision with root package name */
    final CharSequence f255g;

    /* renamed from: h  reason: collision with root package name */
    final int f256h;
    final CharSequence i;
    final ArrayList<String> j;
    final ArrayList<String> k;
    final boolean l;

    public BackStackState(h hVar) {
        int size = hVar.f449c.size();
        this.f249a = new int[(size * 6)];
        if (!hVar.j) {
            throw new IllegalStateException("Not on back stack");
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            h.a aVar = hVar.f449c.get(i3);
            int i4 = i2 + 1;
            this.f249a[i2] = aVar.f455a;
            int i5 = i4 + 1;
            this.f249a[i4] = aVar.f456b != null ? aVar.f456b.mIndex : -1;
            int i6 = i5 + 1;
            this.f249a[i5] = aVar.f457c;
            int i7 = i6 + 1;
            this.f249a[i6] = aVar.f458d;
            int i8 = i7 + 1;
            this.f249a[i7] = aVar.f459e;
            i2 = i8 + 1;
            this.f249a[i8] = aVar.f460f;
        }
        this.f250b = hVar.f454h;
        this.f251c = hVar.i;
        this.f252d = hVar.l;
        this.f253e = hVar.n;
        this.f254f = hVar.o;
        this.f255g = hVar.p;
        this.f256h = hVar.q;
        this.i = hVar.r;
        this.j = hVar.s;
        this.k = hVar.t;
        this.l = hVar.u;
    }

    public BackStackState(Parcel parcel) {
        this.f249a = parcel.createIntArray();
        this.f250b = parcel.readInt();
        this.f251c = parcel.readInt();
        this.f252d = parcel.readString();
        this.f253e = parcel.readInt();
        this.f254f = parcel.readInt();
        this.f255g = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f256h = parcel.readInt();
        this.i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.j = parcel.createStringArrayList();
        this.k = parcel.createStringArrayList();
        this.l = parcel.readInt() != 0;
    }

    public h a(r rVar) {
        int i2 = 0;
        h hVar = new h(rVar);
        int i3 = 0;
        while (i2 < this.f249a.length) {
            h.a aVar = new h.a();
            int i4 = i2 + 1;
            aVar.f455a = this.f249a[i2];
            if (r.f466a) {
                Log.v("FragmentManager", "Instantiate " + hVar + " op #" + i3 + " base fragment #" + this.f249a[i4]);
            }
            int i5 = i4 + 1;
            int i6 = this.f249a[i4];
            if (i6 >= 0) {
                aVar.f456b = rVar.f470e.get(i6);
            } else {
                aVar.f456b = null;
            }
            int i7 = i5 + 1;
            aVar.f457c = this.f249a[i5];
            int i8 = i7 + 1;
            aVar.f458d = this.f249a[i7];
            int i9 = i8 + 1;
            aVar.f459e = this.f249a[i8];
            aVar.f460f = this.f249a[i9];
            hVar.f450d = aVar.f457c;
            hVar.f451e = aVar.f458d;
            hVar.f452f = aVar.f459e;
            hVar.f453g = aVar.f460f;
            hVar.a(aVar);
            i3++;
            i2 = i9 + 1;
        }
        hVar.f454h = this.f250b;
        hVar.i = this.f251c;
        hVar.l = this.f252d;
        hVar.n = this.f253e;
        hVar.j = true;
        hVar.o = this.f254f;
        hVar.p = this.f255g;
        hVar.q = this.f256h;
        hVar.r = this.i;
        hVar.s = this.j;
        hVar.t = this.k;
        hVar.u = this.l;
        hVar.a(1);
        return hVar;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3 = 0;
        parcel.writeIntArray(this.f249a);
        parcel.writeInt(this.f250b);
        parcel.writeInt(this.f251c);
        parcel.writeString(this.f252d);
        parcel.writeInt(this.f253e);
        parcel.writeInt(this.f254f);
        TextUtils.writeToParcel(this.f255g, parcel, 0);
        parcel.writeInt(this.f256h);
        TextUtils.writeToParcel(this.i, parcel, 0);
        parcel.writeStringList(this.j);
        parcel.writeStringList(this.k);
        if (this.l) {
            i3 = 1;
        }
        parcel.writeInt(i3);
    }
}
