package android.support.v4.app;

import android.os.Bundle;
import android.support.v4.app.w;
import android.support.v4.content.Loader;
import android.support.v4.util.c;
import android.support.v4.util.j;
import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

/* compiled from: LoaderManager */
class x extends w {

    /* renamed from: a  reason: collision with root package name */
    static boolean f539a = false;

    /* renamed from: b  reason: collision with root package name */
    final j<a> f540b = new j<>();

    /* renamed from: c  reason: collision with root package name */
    final j<a> f541c = new j<>();

    /* renamed from: d  reason: collision with root package name */
    final String f542d;

    /* renamed from: e  reason: collision with root package name */
    boolean f543e;

    /* renamed from: f  reason: collision with root package name */
    boolean f544f;

    /* renamed from: g  reason: collision with root package name */
    FragmentHostCallback f545g;

    /* compiled from: LoaderManager */
    final class a implements Loader.b<Object>, Loader.c<Object> {

        /* renamed from: a  reason: collision with root package name */
        final int f546a;

        /* renamed from: b  reason: collision with root package name */
        final Bundle f547b;

        /* renamed from: c  reason: collision with root package name */
        w.a<Object> f548c;

        /* renamed from: d  reason: collision with root package name */
        Loader<Object> f549d;

        /* renamed from: e  reason: collision with root package name */
        boolean f550e;

        /* renamed from: f  reason: collision with root package name */
        boolean f551f;

        /* renamed from: g  reason: collision with root package name */
        Object f552g;

        /* renamed from: h  reason: collision with root package name */
        boolean f553h;
        boolean i;
        boolean j;
        boolean k;
        boolean l;
        boolean m;
        a n;
        final /* synthetic */ x o;

        /* access modifiers changed from: package-private */
        public void a() {
            if (this.i && this.j) {
                this.f553h = true;
            } else if (!this.f553h) {
                this.f553h = true;
                if (x.f539a) {
                    Log.v("LoaderManager", "  Starting: " + this);
                }
                if (this.f549d == null && this.f548c != null) {
                    this.f549d = this.f548c.a(this.f546a, this.f547b);
                }
                if (this.f549d == null) {
                    return;
                }
                if (!this.f549d.getClass().isMemberClass() || Modifier.isStatic(this.f549d.getClass().getModifiers())) {
                    if (!this.m) {
                        this.f549d.a(this.f546a, this);
                        this.f549d.a((Loader.b<Object>) this);
                        this.m = true;
                    }
                    this.f549d.q();
                    return;
                }
                throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + this.f549d);
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (x.f539a) {
                Log.v("LoaderManager", "  Retaining: " + this);
            }
            this.i = true;
            this.j = this.f553h;
            this.f553h = false;
            this.f548c = null;
        }

        /* access modifiers changed from: package-private */
        public void c() {
            if (this.i) {
                if (x.f539a) {
                    Log.v("LoaderManager", "  Finished Retaining: " + this);
                }
                this.i = false;
                if (this.f553h != this.j && !this.f553h) {
                    e();
                }
            }
            if (this.f553h && this.f550e && !this.k) {
                b(this.f549d, this.f552g);
            }
        }

        /* access modifiers changed from: package-private */
        public void d() {
            if (this.f553h && this.k) {
                this.k = false;
                if (this.f550e && !this.i) {
                    b(this.f549d, this.f552g);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void e() {
            if (x.f539a) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.f553h = false;
            if (!this.i && this.f549d != null && this.m) {
                this.m = false;
                this.f549d.a((Loader.c<Object>) this);
                this.f549d.b((Loader.b<Object>) this);
                this.f549d.t();
            }
        }

        /* access modifiers changed from: package-private */
        public void f() {
            String str;
            if (x.f539a) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.l = true;
            boolean z = this.f551f;
            this.f551f = false;
            if (this.f548c != null && this.f549d != null && this.f550e && z) {
                if (x.f539a) {
                    Log.v("LoaderManager", "  Resetting: " + this);
                }
                if (this.o.f545g != null) {
                    String str2 = this.o.f545g.f282d.u;
                    this.o.f545g.f282d.u = "onLoaderReset";
                    str = str2;
                } else {
                    str = null;
                }
                try {
                    this.f548c.a(this.f549d);
                } finally {
                    if (this.o.f545g != null) {
                        this.o.f545g.f282d.u = str;
                    }
                }
            }
            this.f548c = null;
            this.f552g = null;
            this.f550e = false;
            if (this.f549d != null) {
                if (this.m) {
                    this.m = false;
                    this.f549d.a((Loader.c<Object>) this);
                    this.f549d.b((Loader.b<Object>) this);
                }
                this.f549d.u();
            }
            if (this.n != null) {
                this.n.f();
            }
        }

        public void a(Loader<Object> loader) {
            if (x.f539a) {
                Log.v("LoaderManager", "onLoadCanceled: " + this);
            }
            if (this.l) {
                if (x.f539a) {
                    Log.v("LoaderManager", "  Ignoring load canceled -- destroyed");
                }
            } else if (this.o.f540b.a(this.f546a) == this) {
                a aVar = this.n;
                if (aVar != null) {
                    if (x.f539a) {
                        Log.v("LoaderManager", "  Switching to pending loader: " + aVar);
                    }
                    this.n = null;
                    this.o.f540b.b(this.f546a, null);
                    f();
                    this.o.a(aVar);
                }
            } else if (x.f539a) {
                Log.v("LoaderManager", "  Ignoring load canceled -- not active");
            }
        }

        public void a(Loader<Object> loader, Object obj) {
            if (x.f539a) {
                Log.v("LoaderManager", "onLoadComplete: " + this);
            }
            if (this.l) {
                if (x.f539a) {
                    Log.v("LoaderManager", "  Ignoring load complete -- destroyed");
                }
            } else if (this.o.f540b.a(this.f546a) == this) {
                a aVar = this.n;
                if (aVar != null) {
                    if (x.f539a) {
                        Log.v("LoaderManager", "  Switching to pending loader: " + aVar);
                    }
                    this.n = null;
                    this.o.f540b.b(this.f546a, null);
                    f();
                    this.o.a(aVar);
                    return;
                }
                if (this.f552g != obj || !this.f550e) {
                    this.f552g = obj;
                    this.f550e = true;
                    if (this.f553h) {
                        b(loader, obj);
                    }
                }
                a a2 = this.o.f541c.a(this.f546a);
                if (!(a2 == null || a2 == this)) {
                    a2.f551f = false;
                    a2.f();
                    this.o.f541c.c(this.f546a);
                }
                if (this.o.f545g != null && !this.o.a()) {
                    this.o.f545g.f282d.d();
                }
            } else if (x.f539a) {
                Log.v("LoaderManager", "  Ignoring load complete -- not active");
            }
        }

        /* access modifiers changed from: package-private */
        public void b(Loader<Object> loader, Object obj) {
            String str;
            if (this.f548c != null) {
                if (this.o.f545g != null) {
                    String str2 = this.o.f545g.f282d.u;
                    this.o.f545g.f282d.u = "onLoadFinished";
                    str = str2;
                } else {
                    str = null;
                }
                try {
                    if (x.f539a) {
                        Log.v("LoaderManager", "  onLoadFinished in " + loader + ": " + loader.c(obj));
                    }
                    this.f548c.a(loader, obj);
                    this.f551f = true;
                } finally {
                    if (this.o.f545g != null) {
                        this.o.f545g.f282d.u = str;
                    }
                }
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.f546a);
            sb.append(" : ");
            c.a(this.f549d, sb);
            sb.append("}}");
            return sb.toString();
        }

        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.f546a);
            printWriter.print(" mArgs=");
            printWriter.println(this.f547b);
            printWriter.print(str);
            printWriter.print("mCallbacks=");
            printWriter.println(this.f548c);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.f549d);
            if (this.f549d != null) {
                this.f549d.a(str + "  ", fileDescriptor, printWriter, strArr);
            }
            if (this.f550e || this.f551f) {
                printWriter.print(str);
                printWriter.print("mHaveData=");
                printWriter.print(this.f550e);
                printWriter.print("  mDeliveredData=");
                printWriter.println(this.f551f);
                printWriter.print(str);
                printWriter.print("mData=");
                printWriter.println(this.f552g);
            }
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.f553h);
            printWriter.print(" mReportNextStart=");
            printWriter.print(this.k);
            printWriter.print(" mDestroyed=");
            printWriter.println(this.l);
            printWriter.print(str);
            printWriter.print("mRetaining=");
            printWriter.print(this.i);
            printWriter.print(" mRetainingStarted=");
            printWriter.print(this.j);
            printWriter.print(" mListenerRegistered=");
            printWriter.println(this.m);
            if (this.n != null) {
                printWriter.print(str);
                printWriter.println("Pending Loader ");
                printWriter.print(this.n);
                printWriter.println(":");
                this.n.a(str + "  ", fileDescriptor, printWriter, strArr);
            }
        }
    }

    x(String str, FragmentHostCallback fragmentHostCallback, boolean z) {
        this.f542d = str;
        this.f545g = fragmentHostCallback;
        this.f543e = z;
    }

    /* access modifiers changed from: package-private */
    public void a(FragmentHostCallback fragmentHostCallback) {
        this.f545g = fragmentHostCallback;
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.f540b.b(aVar.f546a, aVar);
        if (this.f543e) {
            aVar.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (f539a) {
            Log.v("LoaderManager", "Starting in " + this);
        }
        if (this.f543e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.f543e = true;
        for (int b2 = this.f540b.b() - 1; b2 >= 0; b2--) {
            this.f540b.e(b2).a();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (f539a) {
            Log.v("LoaderManager", "Stopping in " + this);
        }
        if (!this.f543e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        for (int b2 = this.f540b.b() - 1; b2 >= 0; b2--) {
            this.f540b.e(b2).e();
        }
        this.f543e = false;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (f539a) {
            Log.v("LoaderManager", "Retaining in " + this);
        }
        if (!this.f543e) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.f544f = true;
        this.f543e = false;
        for (int b2 = this.f540b.b() - 1; b2 >= 0; b2--) {
            this.f540b.e(b2).b();
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.f544f) {
            if (f539a) {
                Log.v("LoaderManager", "Finished Retaining in " + this);
            }
            this.f544f = false;
            for (int b2 = this.f540b.b() - 1; b2 >= 0; b2--) {
                this.f540b.e(b2).c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        for (int b2 = this.f540b.b() - 1; b2 >= 0; b2--) {
            this.f540b.e(b2).k = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        for (int b2 = this.f540b.b() - 1; b2 >= 0; b2--) {
            this.f540b.e(b2).d();
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (!this.f544f) {
            if (f539a) {
                Log.v("LoaderManager", "Destroying Active in " + this);
            }
            for (int b2 = this.f540b.b() - 1; b2 >= 0; b2--) {
                this.f540b.e(b2).f();
            }
            this.f540b.c();
        }
        if (f539a) {
            Log.v("LoaderManager", "Destroying Inactive in " + this);
        }
        for (int b3 = this.f541c.b() - 1; b3 >= 0; b3--) {
            this.f541c.e(b3).f();
        }
        this.f541c.c();
        this.f545g = null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((int) FileUtils.FileMode.MODE_IWUSR);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        c.a(this.f545g, sb);
        sb.append("}}");
        return sb.toString();
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (this.f540b.b() > 0) {
            printWriter.print(str);
            printWriter.println("Active Loaders:");
            String str2 = str + "    ";
            for (int i = 0; i < this.f540b.b(); i++) {
                a e2 = this.f540b.e(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.f540b.d(i));
                printWriter.print(": ");
                printWriter.println(e2.toString());
                e2.a(str2, fileDescriptor, printWriter, strArr);
            }
        }
        if (this.f541c.b() > 0) {
            printWriter.print(str);
            printWriter.println("Inactive Loaders:");
            String str3 = str + "    ";
            for (int i2 = 0; i2 < this.f541c.b(); i2++) {
                a e3 = this.f541c.e(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.f541c.d(i2));
                printWriter.print(": ");
                printWriter.println(e3.toString());
                e3.a(str3, fileDescriptor, printWriter, strArr);
            }
        }
    }

    public boolean a() {
        boolean z;
        int b2 = this.f540b.b();
        boolean z2 = false;
        for (int i = 0; i < b2; i++) {
            a e2 = this.f540b.e(i);
            if (!e2.f553h || e2.f551f) {
                z = false;
            } else {
                z = true;
            }
            z2 |= z;
        }
        return z2;
    }
}
