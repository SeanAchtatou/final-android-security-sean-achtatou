package android.support.v4.app;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.os.IBinder;

@TargetApi(18)
/* compiled from: BundleCompatJellybeanMR2 */
class n {
    public static IBinder a(Bundle bundle, String str) {
        return bundle.getBinder(str);
    }
}
