package android.support.v4.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Build;
import android.support.v4.view.ag;
import android.support.v4.widget.DrawerLayout;
import android.view.View;

@Deprecated
public class ActionBarDrawerToggle implements DrawerLayout.f {

    /* renamed from: b  reason: collision with root package name */
    private static final a f233b;

    /* renamed from: a  reason: collision with root package name */
    final Activity f234a;

    /* renamed from: c  reason: collision with root package name */
    private final e f235c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f236d;

    /* renamed from: e  reason: collision with root package name */
    private f f237e;

    /* renamed from: f  reason: collision with root package name */
    private final int f238f;

    /* renamed from: g  reason: collision with root package name */
    private final int f239g;

    /* renamed from: h  reason: collision with root package name */
    private Object f240h;

    private interface a {
        Object a(Object obj, Activity activity, int i);
    }

    public interface e {
        void a(int i);
    }

    private static class b implements a {
        b() {
        }

        public Object a(Object obj, Activity activity, int i) {
            return obj;
        }
    }

    @TargetApi(11)
    private static class c implements a {
        c() {
        }

        public Object a(Object obj, Activity activity, int i) {
            return a.a(obj, activity, i);
        }
    }

    @TargetApi(18)
    private static class d implements a {
        d() {
        }

        public Object a(Object obj, Activity activity, int i) {
            return b.a(obj, activity, i);
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 18) {
            f233b = new d();
        } else if (i >= 11) {
            f233b = new c();
        } else {
            f233b = new b();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void a(View view, float f2) {
        float min;
        float a2 = this.f237e.a();
        if (f2 > 0.5f) {
            min = Math.max(a2, Math.max(0.0f, f2 - 0.5f) * 2.0f);
        } else {
            min = Math.min(a2, f2 * 2.0f);
        }
        this.f237e.a(min);
    }

    public void a(View view) {
        this.f237e.a(1.0f);
        if (this.f236d) {
            b(this.f239g);
        }
    }

    public void b(View view) {
        this.f237e.a(0.0f);
        if (this.f236d) {
            b(this.f238f);
        }
    }

    public void a(int i) {
    }

    /* access modifiers changed from: package-private */
    public void b(int i) {
        if (this.f235c != null) {
            this.f235c.a(i);
        } else {
            this.f240h = f233b.a(this.f240h, this.f234a, i);
        }
    }

    private class f extends InsetDrawable implements Drawable.Callback {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ActionBarDrawerToggle f241a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f242b;

        /* renamed from: c  reason: collision with root package name */
        private final Rect f243c;

        /* renamed from: d  reason: collision with root package name */
        private float f244d;

        /* renamed from: e  reason: collision with root package name */
        private float f245e;

        public void a(float f2) {
            this.f244d = f2;
            invalidateSelf();
        }

        public float a() {
            return this.f244d;
        }

        public void draw(Canvas canvas) {
            int i = 1;
            copyBounds(this.f243c);
            canvas.save();
            boolean z = ag.g(this.f241a.f234a.getWindow().getDecorView()) == 1;
            if (z) {
                i = -1;
            }
            int width = this.f243c.width();
            canvas.translate(((float) i) * (-this.f245e) * ((float) width) * this.f244d, 0.0f);
            if (z && !this.f242b) {
                canvas.translate((float) width, 0.0f);
                canvas.scale(-1.0f, 1.0f);
            }
            super.draw(canvas);
            canvas.restore();
        }
    }
}
