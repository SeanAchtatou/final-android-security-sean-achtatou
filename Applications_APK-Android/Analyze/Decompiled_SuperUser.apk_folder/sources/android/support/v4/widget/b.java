package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.support.v4.view.ag;
import android.view.animation.Animation;
import android.widget.ImageView;

/* compiled from: CircleImageView */
class b extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    int f1189a;

    /* renamed from: b  reason: collision with root package name */
    private Animation.AnimationListener f1190b;

    b(Context context, int i) {
        super(context);
        ShapeDrawable shapeDrawable;
        float f2 = getContext().getResources().getDisplayMetrics().density;
        int i2 = (int) (1.75f * f2);
        int i3 = (int) (0.0f * f2);
        this.f1189a = (int) (3.5f * f2);
        if (a()) {
            shapeDrawable = new ShapeDrawable(new OvalShape());
            ag.h(this, f2 * 4.0f);
        } else {
            shapeDrawable = new ShapeDrawable(new a(this.f1189a));
            ag.a(this, 1, shapeDrawable.getPaint());
            shapeDrawable.getPaint().setShadowLayer((float) this.f1189a, (float) i3, (float) i2, 503316480);
            int i4 = this.f1189a;
            setPadding(i4, i4, i4, i4);
        }
        shapeDrawable.getPaint().setColor(i);
        ag.a(this, shapeDrawable);
    }

    private boolean a() {
        return Build.VERSION.SDK_INT >= 21;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (!a()) {
            setMeasuredDimension(getMeasuredWidth() + (this.f1189a * 2), getMeasuredHeight() + (this.f1189a * 2));
        }
    }

    public void a(Animation.AnimationListener animationListener) {
        this.f1190b = animationListener;
    }

    public void onAnimationStart() {
        super.onAnimationStart();
        if (this.f1190b != null) {
            this.f1190b.onAnimationStart(getAnimation());
        }
    }

    public void onAnimationEnd() {
        super.onAnimationEnd();
        if (this.f1190b != null) {
            this.f1190b.onAnimationEnd(getAnimation());
        }
    }

    public void setBackgroundColor(int i) {
        if (getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable) getBackground()).getPaint().setColor(i);
        }
    }

    /* compiled from: CircleImageView */
    private class a extends OvalShape {

        /* renamed from: b  reason: collision with root package name */
        private RadialGradient f1192b;

        /* renamed from: c  reason: collision with root package name */
        private Paint f1193c = new Paint();

        a(int i) {
            b.this.f1189a = i;
            a((int) rect().width());
        }

        /* access modifiers changed from: protected */
        public void onResize(float f2, float f3) {
            super.onResize(f2, f3);
            a((int) f2);
        }

        public void draw(Canvas canvas, Paint paint) {
            int width = b.this.getWidth();
            int height = b.this.getHeight();
            canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (width / 2), this.f1193c);
            canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) ((width / 2) - b.this.f1189a), paint);
        }

        private void a(int i) {
            this.f1192b = new RadialGradient((float) (i / 2), (float) (i / 2), (float) b.this.f1189a, new int[]{1023410176, 0}, (float[]) null, Shader.TileMode.CLAMP);
            this.f1193c.setShader(this.f1192b);
        }
    }
}
