package android.support.v4.widget;

import android.annotation.TargetApi;
import android.widget.EdgeEffect;

@TargetApi(21)
/* compiled from: EdgeEffectCompatLollipop */
class k {
    public static boolean a(Object obj, float f2, float f3) {
        ((EdgeEffect) obj).onPull(f2, f3);
        return true;
    }
}
