package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;

public final class EdgeEffectCompat {

    /* renamed from: b  reason: collision with root package name */
    private static final c f1103b;

    /* renamed from: a  reason: collision with root package name */
    private Object f1104a;

    interface c {
        Object a(Context context);

        void a(Object obj, int i, int i2);

        boolean a(Object obj);

        boolean a(Object obj, float f2);

        boolean a(Object obj, float f2, float f3);

        boolean a(Object obj, int i);

        boolean a(Object obj, Canvas canvas);

        void b(Object obj);

        boolean c(Object obj);
    }

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f1103b = new d();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f1103b = new b();
        } else {
            f1103b = new a();
        }
    }

    static class a implements c {
        a() {
        }

        public Object a(Context context) {
            return null;
        }

        public void a(Object obj, int i, int i2) {
        }

        public boolean a(Object obj) {
            return true;
        }

        public void b(Object obj) {
        }

        public boolean a(Object obj, float f2) {
            return false;
        }

        public boolean c(Object obj) {
            return false;
        }

        public boolean a(Object obj, int i) {
            return false;
        }

        public boolean a(Object obj, Canvas canvas) {
            return false;
        }

        public boolean a(Object obj, float f2, float f3) {
            return false;
        }
    }

    static class b implements c {
        b() {
        }

        public Object a(Context context) {
            return j.a(context);
        }

        public void a(Object obj, int i, int i2) {
            j.a(obj, i, i2);
        }

        public boolean a(Object obj) {
            return j.a(obj);
        }

        public void b(Object obj) {
            j.b(obj);
        }

        public boolean a(Object obj, float f2) {
            return j.a(obj, f2);
        }

        public boolean c(Object obj) {
            return j.c(obj);
        }

        public boolean a(Object obj, int i) {
            return j.a(obj, i);
        }

        public boolean a(Object obj, Canvas canvas) {
            return j.a(obj, canvas);
        }

        public boolean a(Object obj, float f2, float f3) {
            return j.a(obj, f2);
        }
    }

    static class d extends b {
        d() {
        }

        public boolean a(Object obj, float f2, float f3) {
            return k.a(obj, f2, f3);
        }
    }

    public EdgeEffectCompat(Context context) {
        this.f1104a = f1103b.a(context);
    }

    public void a(int i, int i2) {
        f1103b.a(this.f1104a, i, i2);
    }

    public boolean a() {
        return f1103b.a(this.f1104a);
    }

    public void b() {
        f1103b.b(this.f1104a);
    }

    @Deprecated
    public boolean a(float f2) {
        return f1103b.a(this.f1104a, f2);
    }

    public boolean a(float f2, float f3) {
        return f1103b.a(this.f1104a, f2, f3);
    }

    public boolean c() {
        return f1103b.c(this.f1104a);
    }

    public boolean a(int i) {
        return f1103b.a(this.f1104a, i);
    }

    public boolean a(Canvas canvas) {
        return f1103b.a(this.f1104a, canvas);
    }
}
