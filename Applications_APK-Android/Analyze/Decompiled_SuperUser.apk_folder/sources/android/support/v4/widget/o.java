package android.support.v4.widget;

import android.annotation.TargetApi;
import android.widget.ListView;

@TargetApi(19)
/* compiled from: ListViewCompatKitKat */
class o {
    static void a(ListView listView, int i) {
        listView.scrollListBy(i);
    }
}
