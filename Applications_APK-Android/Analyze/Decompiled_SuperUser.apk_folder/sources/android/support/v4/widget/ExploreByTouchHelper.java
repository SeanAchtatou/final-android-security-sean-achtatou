package android.support.v4.widget;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.util.j;
import android.support.v4.view.a.c;
import android.support.v4.view.a.e;
import android.support.v4.view.a.n;
import android.support.v4.view.a.q;
import android.support.v4.view.ag;
import android.support.v4.view.av;
import android.support.v4.widget.l;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.ArrayList;
import java.util.List;

public abstract class ExploreByTouchHelper extends android.support.v4.view.a {

    /* renamed from: a  reason: collision with root package name */
    private static final Rect f1105a = new Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
    private static final l.a<e> k = new l.a<e>() {
    };
    private static final l.b<j<e>, e> l = new l.b<j<e>, e>() {
    };

    /* renamed from: b  reason: collision with root package name */
    private final Rect f1106b;

    /* renamed from: c  reason: collision with root package name */
    private final Rect f1107c;

    /* renamed from: d  reason: collision with root package name */
    private final Rect f1108d;

    /* renamed from: e  reason: collision with root package name */
    private final int[] f1109e;

    /* renamed from: f  reason: collision with root package name */
    private final AccessibilityManager f1110f;

    /* renamed from: g  reason: collision with root package name */
    private final View f1111g;

    /* renamed from: h  reason: collision with root package name */
    private a f1112h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j;

    /* access modifiers changed from: protected */
    public abstract void a(int i2, e eVar);

    /* access modifiers changed from: protected */
    public abstract void a(List<Integer> list);

    /* access modifiers changed from: protected */
    public abstract boolean b(int i2, int i3, Bundle bundle);

    public n getAccessibilityNodeProvider(View view) {
        if (this.f1112h == null) {
            this.f1112h = new a();
        }
        return this.f1112h;
    }

    public final boolean a(int i2, int i3) {
        ViewParent parent;
        if (i2 == Integer.MIN_VALUE || !this.f1110f.isEnabled() || (parent = this.f1111g.getParent()) == null) {
            return false;
        }
        return av.a(parent, this.f1111g, b(i2, i3));
    }

    /* access modifiers changed from: protected */
    public void a(int i2, boolean z) {
    }

    private AccessibilityEvent b(int i2, int i3) {
        switch (i2) {
            case -1:
                return d(i3);
            default:
                return c(i2, i3);
        }
    }

    private AccessibilityEvent d(int i2) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
        ag.a(this.f1111g, obtain);
        return obtain;
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(view, accessibilityEvent);
        a(accessibilityEvent);
    }

    private AccessibilityEvent c(int i2, int i3) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i3);
        q a2 = android.support.v4.view.a.a.a(obtain);
        e a3 = a(i2);
        a2.a().add(a3.t());
        a2.b(a3.u());
        a2.d(a3.q());
        a2.c(a3.p());
        a2.b(a3.o());
        a2.a(a3.g());
        a(i2, obtain);
        if (!obtain.getText().isEmpty() || obtain.getContentDescription() != null) {
            a2.a(a3.s());
            a2.a(this.f1111g, i2);
            obtain.setPackageName(this.f1111g.getContext().getPackageName());
            return obtain;
        }
        throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
    }

    /* access modifiers changed from: package-private */
    public e a(int i2) {
        if (i2 == -1) {
            return a();
        }
        return e(i2);
    }

    private e a() {
        e a2 = e.a(this.f1111g);
        ag.a(this.f1111g, a2);
        ArrayList arrayList = new ArrayList();
        a(arrayList);
        if (a2.c() <= 0 || arrayList.size() <= 0) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                a2.b(this.f1111g, ((Integer) arrayList.get(i2)).intValue());
            }
            return a2;
        }
        throw new RuntimeException("Views cannot have both real and virtual children");
    }

    public void onInitializeAccessibilityNodeInfo(View view, e eVar) {
        super.onInitializeAccessibilityNodeInfo(view, eVar);
        a(eVar);
    }

    private e e(int i2) {
        boolean z;
        e b2 = e.b();
        b2.j(true);
        b2.c(true);
        b2.b((CharSequence) "android.view.View");
        b2.b(f1105a);
        b2.d(f1105a);
        b2.d(this.f1111g);
        a(i2, b2);
        if (b2.t() == null && b2.u() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        b2.a(this.f1107c);
        if (this.f1107c.equals(f1105a)) {
            throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
        }
        int d2 = b2.d();
        if ((d2 & 64) != 0) {
            throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
        } else if ((d2 & FileUtils.FileMode.MODE_IWUSR) != 0) {
            throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
        } else {
            b2.a((CharSequence) this.f1111g.getContext().getPackageName());
            b2.a(this.f1111g, i2);
            if (this.i == i2) {
                b2.f(true);
                b2.a((int) FileUtils.FileMode.MODE_IWUSR);
            } else {
                b2.f(false);
                b2.a(64);
            }
            if (this.j == i2) {
                z = true;
            } else {
                z = false;
            }
            if (z) {
                b2.a(2);
            } else if (b2.h()) {
                b2.a(1);
            }
            b2.d(z);
            this.f1111g.getLocationOnScreen(this.f1109e);
            b2.c(this.f1106b);
            if (this.f1106b.equals(f1105a)) {
                b2.a(this.f1106b);
                if (b2.f962b != -1) {
                    e b3 = e.b();
                    for (int i3 = b2.f962b; i3 != -1; i3 = b3.f962b) {
                        b3.c(this.f1111g, -1);
                        b3.b(f1105a);
                        a(i3, b3);
                        b3.a(this.f1107c);
                        this.f1106b.offset(this.f1107c.left, this.f1107c.top);
                    }
                    b3.v();
                }
                this.f1106b.offset(this.f1109e[0] - this.f1111g.getScrollX(), this.f1109e[1] - this.f1111g.getScrollY());
            }
            if (this.f1111g.getLocalVisibleRect(this.f1108d)) {
                this.f1108d.offset(this.f1109e[0] - this.f1111g.getScrollX(), this.f1109e[1] - this.f1111g.getScrollY());
                this.f1106b.intersect(this.f1108d);
                b2.d(this.f1106b);
                if (a(this.f1106b)) {
                    b2.e(true);
                }
            }
            return b2;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, Bundle bundle) {
        switch (i2) {
            case -1:
                return a(i3, bundle);
            default:
                return c(i2, i3, bundle);
        }
    }

    private boolean a(int i2, Bundle bundle) {
        return ag.a(this.f1111g, i2, bundle);
    }

    private boolean c(int i2, int i3, Bundle bundle) {
        switch (i3) {
            case 1:
                return b(i2);
            case 2:
                return c(i2);
            case 64:
                return f(i2);
            case FileUtils.FileMode.MODE_IWUSR /*128*/:
                return g(i2);
            default:
                return b(i2, i3, bundle);
        }
    }

    private boolean a(Rect rect) {
        if (rect == null || rect.isEmpty() || this.f1111g.getWindowVisibility() != 0) {
            return false;
        }
        ViewParent parent = this.f1111g.getParent();
        while (parent instanceof View) {
            View view = (View) parent;
            if (ag.e(view) <= 0.0f || view.getVisibility() != 0) {
                return false;
            }
            parent = view.getParent();
        }
        return parent != null;
    }

    private boolean f(int i2) {
        if (!this.f1110f.isEnabled() || !c.a(this.f1110f) || this.i == i2) {
            return false;
        }
        if (this.i != Integer.MIN_VALUE) {
            g(this.i);
        }
        this.i = i2;
        this.f1111g.invalidate();
        a(i2, 32768);
        return true;
    }

    private boolean g(int i2) {
        if (this.i != i2) {
            return false;
        }
        this.i = Integer.MIN_VALUE;
        this.f1111g.invalidate();
        a(i2, 65536);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.ExploreByTouchHelper.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.ExploreByTouchHelper.a(int, android.os.Bundle):boolean
      android.support.v4.widget.ExploreByTouchHelper.a(int, android.support.v4.view.a.e):void
      android.support.v4.widget.ExploreByTouchHelper.a(int, android.view.accessibility.AccessibilityEvent):void
      android.support.v4.widget.ExploreByTouchHelper.a(int, int):boolean
      android.support.v4.widget.ExploreByTouchHelper.a(int, boolean):void */
    public final boolean b(int i2) {
        if ((!this.f1111g.isFocused() && !this.f1111g.requestFocus()) || this.j == i2) {
            return false;
        }
        if (this.j != Integer.MIN_VALUE) {
            c(this.j);
        }
        this.j = i2;
        a(i2, true);
        a(i2, 8);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.ExploreByTouchHelper.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.ExploreByTouchHelper.a(int, android.os.Bundle):boolean
      android.support.v4.widget.ExploreByTouchHelper.a(int, android.support.v4.view.a.e):void
      android.support.v4.widget.ExploreByTouchHelper.a(int, android.view.accessibility.AccessibilityEvent):void
      android.support.v4.widget.ExploreByTouchHelper.a(int, int):boolean
      android.support.v4.widget.ExploreByTouchHelper.a(int, boolean):void */
    public final boolean c(int i2) {
        if (this.j != i2) {
            return false;
        }
        this.j = Integer.MIN_VALUE;
        a(i2, false);
        a(i2, 8);
        return true;
    }

    /* access modifiers changed from: protected */
    public void a(int i2, AccessibilityEvent accessibilityEvent) {
    }

    /* access modifiers changed from: protected */
    public void a(AccessibilityEvent accessibilityEvent) {
    }

    /* access modifiers changed from: protected */
    public void a(e eVar) {
    }

    private class a extends n {
        a() {
        }

        public e a(int i) {
            return e.a(ExploreByTouchHelper.this.a(i));
        }

        public boolean a(int i, int i2, Bundle bundle) {
            return ExploreByTouchHelper.this.a(i, i2, bundle);
        }

        public e b(int i) {
            int a2 = i == 2 ? ExploreByTouchHelper.this.i : ExploreByTouchHelper.this.j;
            if (a2 == Integer.MIN_VALUE) {
                return null;
            }
            return a(a2);
        }
    }
}
