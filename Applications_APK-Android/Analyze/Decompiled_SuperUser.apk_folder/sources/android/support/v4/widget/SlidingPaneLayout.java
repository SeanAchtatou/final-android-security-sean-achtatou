package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.ag;
import android.support.v4.view.s;
import android.support.v4.widget.ae;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import com.lody.virtual.os.VUserInfo;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class SlidingPaneLayout extends ViewGroup {

    /* renamed from: h  reason: collision with root package name */
    static final e f1124h;

    /* renamed from: a  reason: collision with root package name */
    View f1125a;

    /* renamed from: b  reason: collision with root package name */
    float f1126b;

    /* renamed from: c  reason: collision with root package name */
    int f1127c;

    /* renamed from: d  reason: collision with root package name */
    boolean f1128d;

    /* renamed from: e  reason: collision with root package name */
    final ae f1129e;

    /* renamed from: f  reason: collision with root package name */
    boolean f1130f;

    /* renamed from: g  reason: collision with root package name */
    final ArrayList<b> f1131g;
    private int i;
    private int j;
    private Drawable k;
    private Drawable l;
    private final int m;
    private boolean n;
    private float o;
    private int p;
    private float q;
    private float r;
    private d s;
    private boolean t;
    private final Rect u;

    public interface d {
        void a(View view);

        void a(View view, float f2);

        void b(View view);
    }

    interface e {
        void a(SlidingPaneLayout slidingPaneLayout, View view);
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 17) {
            f1124h = new h();
        } else if (i2 >= 16) {
            f1124h = new g();
        } else {
            f1124h = new f();
        }
    }

    public static class SimplePanelSlideListener implements d {
        public void a(View view, float f2) {
        }

        public void a(View view) {
        }

        public void b(View view) {
        }
    }

    public SlidingPaneLayout(Context context) {
        this(context, null);
    }

    public SlidingPaneLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, int):void
     arg types: [android.support.v4.widget.SlidingPaneLayout, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, float):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, int):void */
    public SlidingPaneLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.i = -858993460;
        this.t = true;
        this.u = new Rect();
        this.f1131g = new ArrayList<>();
        float f2 = context.getResources().getDisplayMetrics().density;
        this.m = (int) ((32.0f * f2) + 0.5f);
        ViewConfiguration.get(context);
        setWillNotDraw(false);
        ag.a(this, new a());
        ag.c((View) this, 1);
        this.f1129e = ae.a(this, 0.5f, new c());
        this.f1129e.a(f2 * 400.0f);
    }

    public void setParallaxDistance(int i2) {
        this.p = i2;
        requestLayout();
    }

    public int getParallaxDistance() {
        return this.p;
    }

    public void setSliderFadeColor(int i2) {
        this.i = i2;
    }

    public int getSliderFadeColor() {
        return this.i;
    }

    public void setCoveredFadeColor(int i2) {
        this.j = i2;
    }

    public int getCoveredFadeColor() {
        return this.j;
    }

    public void setPanelSlideListener(d dVar) {
        this.s = dVar;
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        if (this.s != null) {
            this.s.a(view, this.f1126b);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(View view) {
        if (this.s != null) {
            this.s.a(view);
        }
        sendAccessibilityEvent(32);
    }

    /* access modifiers changed from: package-private */
    public void c(View view) {
        if (this.s != null) {
            this.s.b(view);
        }
        sendAccessibilityEvent(32);
    }

    /* access modifiers changed from: package-private */
    public void d(View view) {
        int width;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        boolean f2 = f();
        int width2 = f2 ? getWidth() - getPaddingRight() : getPaddingLeft();
        if (f2) {
            width = getPaddingLeft();
        } else {
            width = getWidth() - getPaddingRight();
        }
        int paddingTop = getPaddingTop();
        int height = getHeight() - getPaddingBottom();
        if (view == null || !g(view)) {
            i2 = 0;
            i3 = 0;
            i4 = 0;
            i5 = 0;
        } else {
            i5 = view.getLeft();
            i4 = view.getRight();
            i3 = view.getTop();
            i2 = view.getBottom();
        }
        int childCount = getChildCount();
        int i8 = 0;
        while (i8 < childCount) {
            View childAt = getChildAt(i8);
            if (childAt != view) {
                if (childAt.getVisibility() != 8) {
                    int max = Math.max(f2 ? width : width2, childAt.getLeft());
                    int max2 = Math.max(paddingTop, childAt.getTop());
                    if (f2) {
                        i6 = width2;
                    } else {
                        i6 = width;
                    }
                    int min = Math.min(i6, childAt.getRight());
                    int min2 = Math.min(height, childAt.getBottom());
                    if (max < i5 || max2 < i3 || min > i4 || min2 > i2) {
                        i7 = 0;
                    } else {
                        i7 = 4;
                    }
                    childAt.setVisibility(i7);
                }
                i8++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 4) {
                childAt.setVisibility(0);
            }
        }
    }

    private static boolean g(View view) {
        if (view.isOpaque()) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            return false;
        }
        Drawable background = view.getBackground();
        if (background == null) {
            return false;
        }
        if (background.getOpacity() != -1) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.t = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.t = true;
        int size = this.f1131g.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.f1131g.get(i2).run();
        }
        this.f1131g.clear();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        int paddingTop;
        int makeMeasureSpec;
        int makeMeasureSpec2;
        int makeMeasureSpec3;
        int makeMeasureSpec4;
        int i8;
        int i9;
        boolean z;
        float f2;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode == 1073741824) {
            if (mode2 == 0) {
                if (!isInEditMode()) {
                    throw new IllegalStateException("Height must not be UNSPECIFIED");
                } else if (mode2 == 0) {
                    i4 = Integer.MIN_VALUE;
                    i5 = size;
                    i6 = 300;
                }
            }
            i4 = mode2;
            i5 = size;
            i6 = size2;
        } else if (!isInEditMode()) {
            throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
        } else if (mode == Integer.MIN_VALUE) {
            i4 = mode2;
            i5 = size;
            i6 = size2;
        } else {
            if (mode == 0) {
                i4 = mode2;
                i5 = 300;
                i6 = size2;
            }
            i4 = mode2;
            i5 = size;
            i6 = size2;
        }
        switch (i4) {
            case Integer.MIN_VALUE:
                i7 = 0;
                paddingTop = (i6 - getPaddingTop()) - getPaddingBottom();
                break;
            case 1073741824:
                i7 = (i6 - getPaddingTop()) - getPaddingBottom();
                paddingTop = i7;
                break;
            default:
                i7 = 0;
                paddingTop = -1;
                break;
        }
        boolean z2 = false;
        int paddingLeft = (i5 - getPaddingLeft()) - getPaddingRight();
        int childCount = getChildCount();
        if (childCount > 2) {
            Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.f1125a = null;
        int i10 = 0;
        int i11 = paddingLeft;
        int i12 = i7;
        float f3 = 0.0f;
        while (i10 < childCount) {
            View childAt = getChildAt(i10);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (childAt.getVisibility() == 8) {
                layoutParams.f1135c = false;
                i8 = i11;
                f2 = f3;
                i9 = i12;
                z = z2;
            } else {
                if (layoutParams.f1133a > 0.0f) {
                    f3 += layoutParams.f1133a;
                    if (layoutParams.width == 0) {
                        i8 = i11;
                        f2 = f3;
                        i9 = i12;
                        z = z2;
                    }
                }
                int i13 = layoutParams.leftMargin + layoutParams.rightMargin;
                if (layoutParams.width == -2) {
                    makeMeasureSpec3 = View.MeasureSpec.makeMeasureSpec(paddingLeft - i13, Integer.MIN_VALUE);
                } else if (layoutParams.width == -1) {
                    makeMeasureSpec3 = View.MeasureSpec.makeMeasureSpec(paddingLeft - i13, 1073741824);
                } else {
                    makeMeasureSpec3 = View.MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824);
                }
                if (layoutParams.height == -2) {
                    makeMeasureSpec4 = View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE);
                } else if (layoutParams.height == -1) {
                    makeMeasureSpec4 = View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824);
                } else {
                    makeMeasureSpec4 = View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824);
                }
                childAt.measure(makeMeasureSpec3, makeMeasureSpec4);
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                if (i4 == Integer.MIN_VALUE && measuredHeight > i12) {
                    i12 = Math.min(measuredHeight, paddingTop);
                }
                int i14 = i11 - measuredWidth;
                boolean z3 = i14 < 0;
                layoutParams.f1134b = z3;
                boolean z4 = z3 | z2;
                if (layoutParams.f1134b) {
                    this.f1125a = childAt;
                }
                i8 = i14;
                i9 = i12;
                float f4 = f3;
                z = z4;
                f2 = f4;
            }
            i10++;
            z2 = z;
            i12 = i9;
            f3 = f2;
            i11 = i8;
        }
        if (z2 || f3 > 0.0f) {
            int i15 = paddingLeft - this.m;
            for (int i16 = 0; i16 < childCount; i16++) {
                View childAt2 = getChildAt(i16);
                if (childAt2.getVisibility() != 8) {
                    LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                    if (childAt2.getVisibility() != 8) {
                        boolean z5 = layoutParams2.width == 0 && layoutParams2.f1133a > 0.0f;
                        int measuredWidth2 = z5 ? 0 : childAt2.getMeasuredWidth();
                        if (!z2 || childAt2 == this.f1125a) {
                            if (layoutParams2.f1133a > 0.0f) {
                                if (layoutParams2.width != 0) {
                                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                                } else if (layoutParams2.height == -2) {
                                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE);
                                } else if (layoutParams2.height == -1) {
                                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824);
                                } else {
                                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(layoutParams2.height, 1073741824);
                                }
                                if (z2) {
                                    int i17 = paddingLeft - (layoutParams2.rightMargin + layoutParams2.leftMargin);
                                    int makeMeasureSpec5 = View.MeasureSpec.makeMeasureSpec(i17, 1073741824);
                                    if (measuredWidth2 != i17) {
                                        childAt2.measure(makeMeasureSpec5, makeMeasureSpec);
                                    }
                                } else {
                                    childAt2.measure(View.MeasureSpec.makeMeasureSpec(((int) ((layoutParams2.f1133a * ((float) Math.max(0, i11))) / f3)) + measuredWidth2, 1073741824), makeMeasureSpec);
                                }
                            }
                        } else if (layoutParams2.width < 0 && (measuredWidth2 > i15 || layoutParams2.f1133a > 0.0f)) {
                            if (!z5) {
                                makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                            } else if (layoutParams2.height == -2) {
                                makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE);
                            } else if (layoutParams2.height == -1) {
                                makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824);
                            } else {
                                makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(layoutParams2.height, 1073741824);
                            }
                            childAt2.measure(View.MeasureSpec.makeMeasureSpec(i15, 1073741824), makeMeasureSpec2);
                        }
                    }
                }
            }
        }
        setMeasuredDimension(i5, getPaddingTop() + i12 + getPaddingBottom());
        this.n = z2;
        if (this.f1129e.a() != 0 && !z2) {
            this.f1129e.f();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int width;
        int i10;
        boolean f2 = f();
        if (f2) {
            this.f1129e.a(2);
        } else {
            this.f1129e.a(1);
        }
        int i11 = i4 - i2;
        int paddingRight = f2 ? getPaddingRight() : getPaddingLeft();
        int paddingLeft = f2 ? getPaddingLeft() : getPaddingRight();
        int paddingTop = getPaddingTop();
        int childCount = getChildCount();
        if (this.t) {
            this.f1126b = (!this.n || !this.f1130f) ? 0.0f : 1.0f;
        }
        int i12 = 0;
        int i13 = paddingRight;
        while (i12 < childCount) {
            View childAt = getChildAt(i12);
            if (childAt.getVisibility() == 8) {
                width = paddingRight;
                i10 = i13;
            } else {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                if (layoutParams.f1134b) {
                    int min = (Math.min(paddingRight, (i11 - paddingLeft) - this.m) - i13) - (layoutParams.leftMargin + layoutParams.rightMargin);
                    this.f1127c = min;
                    int i14 = f2 ? layoutParams.rightMargin : layoutParams.leftMargin;
                    layoutParams.f1135c = ((i13 + i14) + min) + (measuredWidth / 2) > i11 - paddingLeft;
                    int i15 = (int) (((float) min) * this.f1126b);
                    i7 = i13 + i14 + i15;
                    this.f1126b = ((float) i15) / ((float) this.f1127c);
                    i6 = 0;
                } else if (!this.n || this.p == 0) {
                    i6 = 0;
                    i7 = paddingRight;
                } else {
                    i6 = (int) ((1.0f - this.f1126b) * ((float) this.p));
                    i7 = paddingRight;
                }
                if (f2) {
                    i9 = (i11 - i7) + i6;
                    i8 = i9 - measuredWidth;
                } else {
                    i8 = i7 - i6;
                    i9 = i8 + measuredWidth;
                }
                childAt.layout(i8, paddingTop, i9, childAt.getMeasuredHeight() + paddingTop);
                width = childAt.getWidth() + paddingRight;
                i10 = i7;
            }
            i12++;
            paddingRight = width;
            i13 = i10;
        }
        if (this.t) {
            if (this.n) {
                if (this.p != 0) {
                    a(this.f1126b);
                }
                if (((LayoutParams) this.f1125a.getLayoutParams()).f1135c) {
                    a(this.f1125a, this.f1126b, this.i);
                }
            } else {
                for (int i16 = 0; i16 < childCount; i16++) {
                    a(getChildAt(i16), 0.0f, this.i);
                }
            }
            d(this.f1125a);
        }
        this.t = false;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            this.t = true;
        }
    }

    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        if (!isInTouchMode() && !this.n) {
            this.f1130f = view == this.f1125a;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View childAt;
        int a2 = s.a(motionEvent);
        if (!this.n && a2 == 0 && getChildCount() > 1 && (childAt = getChildAt(1)) != null) {
            this.f1130f = !this.f1129e.b(childAt, (int) motionEvent.getX(), (int) motionEvent.getY());
        }
        if (!this.n || (this.f1128d && a2 != 0)) {
            this.f1129e.e();
            return super.onInterceptTouchEvent(motionEvent);
        } else if (a2 == 3 || a2 == 1) {
            this.f1129e.e();
            return false;
        } else {
            switch (a2) {
                case 0:
                    this.f1128d = false;
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    this.q = x;
                    this.r = y;
                    if (this.f1129e.b(this.f1125a, (int) x, (int) y) && f(this.f1125a)) {
                        z = true;
                        break;
                    }
                    z = false;
                    break;
                case 1:
                default:
                    z = false;
                    break;
                case 2:
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float abs = Math.abs(x2 - this.q);
                    float abs2 = Math.abs(y2 - this.r);
                    if (abs > ((float) this.f1129e.d()) && abs2 > abs) {
                        this.f1129e.e();
                        this.f1128d = true;
                        return false;
                    }
                    z = false;
                    break;
            }
            if (this.f1129e.a(motionEvent) || z) {
                return true;
            }
            return false;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.n) {
            return super.onTouchEvent(motionEvent);
        }
        this.f1129e.b(motionEvent);
        switch (motionEvent.getAction() & VUserInfo.FLAG_MASK_USER_TYPE) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.q = x;
                this.r = y;
                return true;
            case 1:
                if (!f(this.f1125a)) {
                    return true;
                }
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                float f2 = x2 - this.q;
                float f3 = y2 - this.r;
                int d2 = this.f1129e.d();
                if ((f2 * f2) + (f3 * f3) >= ((float) (d2 * d2)) || !this.f1129e.b(this.f1125a, (int) x2, (int) y2)) {
                    return true;
                }
                a(this.f1125a, 0);
                return true;
            default:
                return true;
        }
    }

    private boolean a(View view, int i2) {
        if (!this.t && !a(0.0f, i2)) {
            return false;
        }
        this.f1130f = false;
        return true;
    }

    private boolean b(View view, int i2) {
        if (!this.t && !a(1.0f, i2)) {
            return false;
        }
        this.f1130f = true;
        return true;
    }

    public boolean b() {
        return b(this.f1125a, 0);
    }

    public boolean c() {
        return a(this.f1125a, 0);
    }

    public boolean d() {
        return !this.n || this.f1126b == 1.0f;
    }

    public boolean e() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (this.f1125a == null) {
            this.f1126b = 0.0f;
            return;
        }
        boolean f2 = f();
        LayoutParams layoutParams = (LayoutParams) this.f1125a.getLayoutParams();
        int width = this.f1125a.getWidth();
        if (f2) {
            i2 = (getWidth() - i2) - width;
        }
        this.f1126b = ((float) (i2 - ((f2 ? layoutParams.rightMargin : layoutParams.leftMargin) + (f2 ? getPaddingRight() : getPaddingLeft())))) / ((float) this.f1127c);
        if (this.p != 0) {
            a(this.f1126b);
        }
        if (layoutParams.f1135c) {
            a(this.f1125a, this.f1126b, this.i);
        }
        a(this.f1125a);
    }

    private void a(View view, float f2, int i2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f2 > 0.0f && i2 != 0) {
            int i3 = (((int) (((float) ((-16777216 & i2) >>> 24)) * f2)) << 24) | (16777215 & i2);
            if (layoutParams.f1136d == null) {
                layoutParams.f1136d = new Paint();
            }
            layoutParams.f1136d.setColorFilter(new PorterDuffColorFilter(i3, PorterDuff.Mode.SRC_OVER));
            if (ag.f(view) != 2) {
                ag.a(view, 2, layoutParams.f1136d);
            }
            e(view);
        } else if (ag.f(view) != 0) {
            if (layoutParams.f1136d != null) {
                layoutParams.f1136d.setColorFilter(null);
            }
            b bVar = new b(view);
            this.f1131g.add(bVar);
            ag.a(this, bVar);
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        boolean drawChild;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int save = canvas.save(2);
        if (this.n && !layoutParams.f1134b && this.f1125a != null) {
            canvas.getClipBounds(this.u);
            if (f()) {
                this.u.left = Math.max(this.u.left, this.f1125a.getRight());
            } else {
                this.u.right = Math.min(this.u.right, this.f1125a.getLeft());
            }
            canvas.clipRect(this.u);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            drawChild = super.drawChild(canvas, view, j2);
        } else if (!layoutParams.f1135c || this.f1126b <= 0.0f) {
            if (view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(false);
            }
            drawChild = super.drawChild(canvas, view, j2);
        } else {
            if (!view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(true);
            }
            Bitmap drawingCache = view.getDrawingCache();
            if (drawingCache != null) {
                canvas.drawBitmap(drawingCache, (float) view.getLeft(), (float) view.getTop(), layoutParams.f1136d);
                drawChild = false;
            } else {
                Log.e("SlidingPaneLayout", "drawChild: child view " + view + " returned null drawing cache");
                drawChild = super.drawChild(canvas, view, j2);
            }
        }
        canvas.restoreToCount(save);
        return drawChild;
    }

    /* access modifiers changed from: package-private */
    public void e(View view) {
        f1124h.a(this, view);
    }

    /* access modifiers changed from: package-private */
    public boolean a(float f2, int i2) {
        int paddingLeft;
        if (!this.n) {
            return false;
        }
        boolean f3 = f();
        LayoutParams layoutParams = (LayoutParams) this.f1125a.getLayoutParams();
        if (f3) {
            paddingLeft = (int) (((float) getWidth()) - ((((float) (layoutParams.rightMargin + getPaddingRight())) + (((float) this.f1127c) * f2)) + ((float) this.f1125a.getWidth())));
        } else {
            paddingLeft = (int) (((float) (layoutParams.leftMargin + getPaddingLeft())) + (((float) this.f1127c) * f2));
        }
        if (!this.f1129e.a(this.f1125a, paddingLeft, this.f1125a.getTop())) {
            return false;
        }
        a();
        ag.c(this);
        return true;
    }

    public void computeScroll() {
        if (!this.f1129e.a(true)) {
            return;
        }
        if (!this.n) {
            this.f1129e.f();
        } else {
            ag.c(this);
        }
    }

    @Deprecated
    public void setShadowDrawable(Drawable drawable) {
        setShadowDrawableLeft(drawable);
    }

    public void setShadowDrawableLeft(Drawable drawable) {
        this.k = drawable;
    }

    public void setShadowDrawableRight(Drawable drawable) {
        this.l = drawable;
    }

    @Deprecated
    public void setShadowResource(int i2) {
        setShadowDrawable(getResources().getDrawable(i2));
    }

    public void setShadowResourceLeft(int i2) {
        setShadowDrawableLeft(android.support.v4.content.c.a(getContext(), i2));
    }

    public void setShadowResourceRight(int i2) {
        setShadowDrawableRight(android.support.v4.content.c.a(getContext(), i2));
    }

    public void draw(Canvas canvas) {
        Drawable drawable;
        int left;
        int i2;
        super.draw(canvas);
        if (f()) {
            drawable = this.l;
        } else {
            drawable = this.k;
        }
        View childAt = getChildCount() > 1 ? getChildAt(1) : null;
        if (childAt != null && drawable != null) {
            int top = childAt.getTop();
            int bottom = childAt.getBottom();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            if (f()) {
                i2 = childAt.getRight();
                left = i2 + intrinsicWidth;
            } else {
                left = childAt.getLeft();
                i2 = left - intrinsicWidth;
            }
            drawable.setBounds(i2, top, left, bottom);
            drawable.draw(canvas);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(float r10) {
        /*
            r9 = this;
            r1 = 0
            r8 = 1065353216(0x3f800000, float:1.0)
            boolean r3 = r9.f()
            android.view.View r0 = r9.f1125a
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            android.support.v4.widget.SlidingPaneLayout$LayoutParams r0 = (android.support.v4.widget.SlidingPaneLayout.LayoutParams) r0
            boolean r2 = r0.f1135c
            if (r2 == 0) goto L_0x0030
            if (r3 == 0) goto L_0x002d
            int r0 = r0.rightMargin
        L_0x0017:
            if (r0 > 0) goto L_0x0030
            r0 = 1
        L_0x001a:
            int r4 = r9.getChildCount()
            r2 = r1
        L_0x001f:
            if (r2 >= r4) goto L_0x005d
            android.view.View r5 = r9.getChildAt(r2)
            android.view.View r1 = r9.f1125a
            if (r5 != r1) goto L_0x0032
        L_0x0029:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x001f
        L_0x002d:
            int r0 = r0.leftMargin
            goto L_0x0017
        L_0x0030:
            r0 = r1
            goto L_0x001a
        L_0x0032:
            float r1 = r9.o
            float r1 = r8 - r1
            int r6 = r9.p
            float r6 = (float) r6
            float r1 = r1 * r6
            int r1 = (int) r1
            r9.o = r10
            float r6 = r8 - r10
            int r7 = r9.p
            float r7 = (float) r7
            float r6 = r6 * r7
            int r6 = (int) r6
            int r1 = r1 - r6
            if (r3 == 0) goto L_0x0048
            int r1 = -r1
        L_0x0048:
            r5.offsetLeftAndRight(r1)
            if (r0 == 0) goto L_0x0029
            if (r3 == 0) goto L_0x0058
            float r1 = r9.o
            float r1 = r1 - r8
        L_0x0052:
            int r6 = r9.j
            r9.a(r5, r1, r6)
            goto L_0x0029
        L_0x0058:
            float r1 = r9.o
            float r1 = r8 - r1
            goto L_0x0052
        L_0x005d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.SlidingPaneLayout.a(float):void");
    }

    /* access modifiers changed from: package-private */
    public boolean f(View view) {
        if (view == null) {
            return false;
        }
        return this.n && ((LayoutParams) view.getLayoutParams()).f1135c && this.f1126b > 0.0f;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1137a = e() ? d() : this.f1130f;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.f1137a) {
            b();
        } else {
            c();
        }
        this.f1130f = savedState.f1137a;
    }

    private class c extends ae.a {
        c() {
        }

        public boolean tryCaptureView(View view, int i) {
            if (SlidingPaneLayout.this.f1128d) {
                return false;
            }
            return ((LayoutParams) view.getLayoutParams()).f1134b;
        }

        public void onViewDragStateChanged(int i) {
            if (SlidingPaneLayout.this.f1129e.a() != 0) {
                return;
            }
            if (SlidingPaneLayout.this.f1126b == 0.0f) {
                SlidingPaneLayout.this.d(SlidingPaneLayout.this.f1125a);
                SlidingPaneLayout.this.c(SlidingPaneLayout.this.f1125a);
                SlidingPaneLayout.this.f1130f = false;
                return;
            }
            SlidingPaneLayout.this.b(SlidingPaneLayout.this.f1125a);
            SlidingPaneLayout.this.f1130f = true;
        }

        public void onViewCaptured(View view, int i) {
            SlidingPaneLayout.this.a();
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
            SlidingPaneLayout.this.a(i);
            SlidingPaneLayout.this.invalidate();
        }

        public void onViewReleased(View view, float f2, float f3) {
            int paddingLeft;
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (SlidingPaneLayout.this.f()) {
                int paddingRight = layoutParams.rightMargin + SlidingPaneLayout.this.getPaddingRight();
                if (f2 < 0.0f || (f2 == 0.0f && SlidingPaneLayout.this.f1126b > 0.5f)) {
                    paddingRight += SlidingPaneLayout.this.f1127c;
                }
                paddingLeft = (SlidingPaneLayout.this.getWidth() - paddingRight) - SlidingPaneLayout.this.f1125a.getWidth();
            } else {
                paddingLeft = layoutParams.leftMargin + SlidingPaneLayout.this.getPaddingLeft();
                if (f2 > 0.0f || (f2 == 0.0f && SlidingPaneLayout.this.f1126b > 0.5f)) {
                    paddingLeft += SlidingPaneLayout.this.f1127c;
                }
            }
            SlidingPaneLayout.this.f1129e.a(paddingLeft, view.getTop());
            SlidingPaneLayout.this.invalidate();
        }

        public int getViewHorizontalDragRange(View view) {
            return SlidingPaneLayout.this.f1127c;
        }

        public int clampViewPositionHorizontal(View view, int i, int i2) {
            LayoutParams layoutParams = (LayoutParams) SlidingPaneLayout.this.f1125a.getLayoutParams();
            if (SlidingPaneLayout.this.f()) {
                int width = SlidingPaneLayout.this.getWidth() - ((layoutParams.rightMargin + SlidingPaneLayout.this.getPaddingRight()) + SlidingPaneLayout.this.f1125a.getWidth());
                return Math.max(Math.min(i, width), width - SlidingPaneLayout.this.f1127c);
            }
            int paddingLeft = layoutParams.leftMargin + SlidingPaneLayout.this.getPaddingLeft();
            return Math.min(Math.max(i, paddingLeft), SlidingPaneLayout.this.f1127c + paddingLeft);
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            return view.getTop();
        }

        public void onEdgeDragStarted(int i, int i2) {
            SlidingPaneLayout.this.f1129e.a(SlidingPaneLayout.this.f1125a, i2);
        }
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {

        /* renamed from: e  reason: collision with root package name */
        private static final int[] f1132e = {16843137};

        /* renamed from: a  reason: collision with root package name */
        public float f1133a = 0.0f;

        /* renamed from: b  reason: collision with root package name */
        boolean f1134b;

        /* renamed from: c  reason: collision with root package name */
        boolean f1135c;

        /* renamed from: d  reason: collision with root package name */
        Paint f1136d;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f1132e);
            this.f1133a = obtainStyledAttributes.getFloat(0, 0.0f);
            obtainStyledAttributes.recycle();
        }
    }

    static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = android.support.v4.os.f.a(new android.support.v4.os.g<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a  reason: collision with root package name */
        boolean f1137a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f1137a = parcel.readInt() != 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f1137a ? 1 : 0);
        }
    }

    static class f implements e {
        f() {
        }

        public void a(SlidingPaneLayout slidingPaneLayout, View view) {
            ag.a(slidingPaneLayout, view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        }
    }

    static class g extends f {

        /* renamed from: a  reason: collision with root package name */
        private Method f1143a;

        /* renamed from: b  reason: collision with root package name */
        private Field f1144b;

        g() {
            try {
                this.f1143a = View.class.getDeclaredMethod("getDisplayList", null);
            } catch (NoSuchMethodException e2) {
                Log.e("SlidingPaneLayout", "Couldn't fetch getDisplayList method; dimming won't work right.", e2);
            }
            try {
                this.f1144b = View.class.getDeclaredField("mRecreateDisplayList");
                this.f1144b.setAccessible(true);
            } catch (NoSuchFieldException e3) {
                Log.e("SlidingPaneLayout", "Couldn't fetch mRecreateDisplayList field; dimming will be slow.", e3);
            }
        }

        public void a(SlidingPaneLayout slidingPaneLayout, View view) {
            if (this.f1143a == null || this.f1144b == null) {
                view.invalidate();
                return;
            }
            try {
                this.f1144b.setBoolean(view, true);
                this.f1143a.invoke(view, null);
            } catch (Exception e2) {
                Log.e("SlidingPaneLayout", "Error refreshing display list state", e2);
            }
            super.a(slidingPaneLayout, view);
        }
    }

    static class h extends f {
        h() {
        }

        public void a(SlidingPaneLayout slidingPaneLayout, View view) {
            ag.a(view, ((LayoutParams) view.getLayoutParams()).f1136d);
        }
    }

    class a extends android.support.v4.view.a {

        /* renamed from: b  reason: collision with root package name */
        private final Rect f1139b = new Rect();

        a() {
        }

        public void onInitializeAccessibilityNodeInfo(View view, android.support.v4.view.a.e eVar) {
            android.support.v4.view.a.e a2 = android.support.v4.view.a.e.a(eVar);
            super.onInitializeAccessibilityNodeInfo(view, a2);
            a(eVar, a2);
            a2.v();
            eVar.b((CharSequence) SlidingPaneLayout.class.getName());
            eVar.b(view);
            ViewParent h2 = ag.h(view);
            if (h2 instanceof View) {
                eVar.d((View) h2);
            }
            int childCount = SlidingPaneLayout.this.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = SlidingPaneLayout.this.getChildAt(i);
                if (!a(childAt) && childAt.getVisibility() == 0) {
                    ag.c(childAt, 1);
                    eVar.c(childAt);
                }
            }
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setClassName(SlidingPaneLayout.class.getName());
        }

        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            if (!a(view)) {
                return super.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
            }
            return false;
        }

        public boolean a(View view) {
            return SlidingPaneLayout.this.f(view);
        }

        private void a(android.support.v4.view.a.e eVar, android.support.v4.view.a.e eVar2) {
            Rect rect = this.f1139b;
            eVar2.a(rect);
            eVar.b(rect);
            eVar2.c(rect);
            eVar.d(rect);
            eVar.e(eVar2.j());
            eVar.a(eVar2.r());
            eVar.b(eVar2.s());
            eVar.d(eVar2.u());
            eVar.j(eVar2.o());
            eVar.h(eVar2.m());
            eVar.c(eVar2.h());
            eVar.d(eVar2.i());
            eVar.f(eVar2.k());
            eVar.g(eVar2.l());
            eVar.i(eVar2.n());
            eVar.a(eVar2.d());
            eVar.b(eVar2.e());
        }
    }

    private class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final View f1140a;

        b(View view) {
            this.f1140a = view;
        }

        public void run() {
            if (this.f1140a.getParent() == SlidingPaneLayout.this) {
                ag.a(this.f1140a, 0, (Paint) null);
                SlidingPaneLayout.this.e(this.f1140a);
            }
            SlidingPaneLayout.this.f1131g.remove(this);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return ag.g(this) == 1;
    }
}
