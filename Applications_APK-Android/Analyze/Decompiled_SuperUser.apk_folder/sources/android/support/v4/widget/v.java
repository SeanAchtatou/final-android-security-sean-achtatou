package android.support.v4.widget;

import android.annotation.TargetApi;
import android.widget.OverScroller;

@TargetApi(14)
/* compiled from: ScrollerCompatIcs */
class v {
    public static float a(Object obj) {
        return ((OverScroller) obj).getCurrVelocity();
    }
}
