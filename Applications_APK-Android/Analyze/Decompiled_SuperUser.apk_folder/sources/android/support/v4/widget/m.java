package android.support.v4.widget;

import android.os.Build;
import android.widget.ListView;

/* compiled from: ListViewCompat */
public final class m {
    public static void a(ListView listView, int i) {
        if (Build.VERSION.SDK_INT >= 19) {
            o.a(listView, i);
        } else {
            n.a(listView, i);
        }
    }
}
