package android.support.v4.widget;

import android.os.Build;
import android.support.v4.view.ag;
import android.view.View;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

/* compiled from: PopupWindowCompat */
public final class q {

    /* renamed from: a  reason: collision with root package name */
    static final e f1220a;

    /* compiled from: PopupWindowCompat */
    interface e {
        void a(PopupWindow popupWindow, int i);

        void a(PopupWindow popupWindow, View view, int i, int i2, int i3);

        void a(PopupWindow popupWindow, boolean z);
    }

    /* compiled from: PopupWindowCompat */
    static class c implements e {

        /* renamed from: a  reason: collision with root package name */
        private static Method f1221a;

        /* renamed from: b  reason: collision with root package name */
        private static boolean f1222b;

        c() {
        }

        public void a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
            if ((android.support.v4.view.e.a(i3, ag.g(view)) & 7) == 5) {
                i -= popupWindow.getWidth() - view.getWidth();
            }
            popupWindow.showAsDropDown(view, i, i2);
        }

        public void a(PopupWindow popupWindow, boolean z) {
        }

        public void a(PopupWindow popupWindow, int i) {
            if (!f1222b) {
                Class<PopupWindow> cls = PopupWindow.class;
                try {
                    f1221a = cls.getDeclaredMethod("setWindowLayoutType", Integer.TYPE);
                    f1221a.setAccessible(true);
                } catch (Exception e2) {
                }
                f1222b = true;
            }
            if (f1221a != null) {
                try {
                    f1221a.invoke(popupWindow, Integer.valueOf(i));
                } catch (Exception e3) {
                }
            }
        }
    }

    /* compiled from: PopupWindowCompat */
    static class d extends c {
        d() {
        }

        public void a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
            t.a(popupWindow, view, i, i2, i3);
        }
    }

    /* compiled from: PopupWindowCompat */
    static class a extends d {
        a() {
        }

        public void a(PopupWindow popupWindow, boolean z) {
            r.a(popupWindow, z);
        }
    }

    /* compiled from: PopupWindowCompat */
    static class b extends a {
        b() {
        }

        public void a(PopupWindow popupWindow, boolean z) {
            s.a(popupWindow, z);
        }

        public void a(PopupWindow popupWindow, int i) {
            s.a(popupWindow, i);
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            f1220a = new b();
        } else if (i >= 21) {
            f1220a = new a();
        } else if (i >= 19) {
            f1220a = new d();
        } else {
            f1220a = new c();
        }
    }

    public static void a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
        f1220a.a(popupWindow, view, i, i2, i3);
    }

    public static void a(PopupWindow popupWindow, boolean z) {
        f1220a.a(popupWindow, z);
    }

    public static void a(PopupWindow popupWindow, int i) {
        f1220a.a(popupWindow, i);
    }
}
