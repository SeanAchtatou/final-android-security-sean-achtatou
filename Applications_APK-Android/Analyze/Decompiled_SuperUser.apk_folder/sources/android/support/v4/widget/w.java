package android.support.v4.widget;

import android.os.Build;

/* compiled from: SearchViewCompat */
public final class w {

    /* renamed from: a  reason: collision with root package name */
    private static final e f1226a;

    @Deprecated
    /* compiled from: SearchViewCompat */
    public static abstract class a {
    }

    @Deprecated
    /* compiled from: SearchViewCompat */
    public static abstract class b {
    }

    /* compiled from: SearchViewCompat */
    interface e {
    }

    /* compiled from: SearchViewCompat */
    static class f implements e {
        f() {
        }
    }

    /* compiled from: SearchViewCompat */
    static class c extends f {
        c() {
        }
    }

    /* compiled from: SearchViewCompat */
    static class d extends c {
        d() {
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            f1226a = new d();
        } else if (Build.VERSION.SDK_INT >= 11) {
            f1226a = new c();
        } else {
            f1226a = new f();
        }
    }
}
