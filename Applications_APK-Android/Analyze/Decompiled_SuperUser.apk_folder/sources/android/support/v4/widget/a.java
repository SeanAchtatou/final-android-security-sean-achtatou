package android.support.v4.widget;

import android.content.res.Resources;
import android.os.SystemClock;
import android.support.v4.view.ag;
import android.support.v4.view.s;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

/* compiled from: AutoScrollHelper */
public abstract class a implements View.OnTouchListener {
    private static final int r = ViewConfiguration.getTapTimeout();

    /* renamed from: a  reason: collision with root package name */
    final C0025a f1163a = new C0025a();

    /* renamed from: b  reason: collision with root package name */
    final View f1164b;

    /* renamed from: c  reason: collision with root package name */
    boolean f1165c;

    /* renamed from: d  reason: collision with root package name */
    boolean f1166d;

    /* renamed from: e  reason: collision with root package name */
    boolean f1167e;

    /* renamed from: f  reason: collision with root package name */
    private final Interpolator f1168f = new AccelerateInterpolator();

    /* renamed from: g  reason: collision with root package name */
    private Runnable f1169g;

    /* renamed from: h  reason: collision with root package name */
    private float[] f1170h = {0.0f, 0.0f};
    private float[] i = {Float.MAX_VALUE, Float.MAX_VALUE};
    private int j;
    private int k;
    private float[] l = {0.0f, 0.0f};
    private float[] m = {0.0f, 0.0f};
    private float[] n = {Float.MAX_VALUE, Float.MAX_VALUE};
    private boolean o;
    private boolean p;
    private boolean q;

    public abstract void a(int i2, int i3);

    public abstract boolean e(int i2);

    public abstract boolean f(int i2);

    public a(View view) {
        this.f1164b = view;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int i2 = (int) ((1575.0f * displayMetrics.density) + 0.5f);
        int i3 = (int) ((displayMetrics.density * 315.0f) + 0.5f);
        a((float) i2, (float) i2);
        b((float) i3, (float) i3);
        a(1);
        e(Float.MAX_VALUE, Float.MAX_VALUE);
        d(0.2f, 0.2f);
        c(1.0f, 1.0f);
        b(r);
        c(500);
        d(500);
    }

    public a a(boolean z) {
        if (this.p && !z) {
            d();
        }
        this.p = z;
        return this;
    }

    public a a(float f2, float f3) {
        this.n[0] = f2 / 1000.0f;
        this.n[1] = f3 / 1000.0f;
        return this;
    }

    public a b(float f2, float f3) {
        this.m[0] = f2 / 1000.0f;
        this.m[1] = f3 / 1000.0f;
        return this;
    }

    public a c(float f2, float f3) {
        this.l[0] = f2 / 1000.0f;
        this.l[1] = f3 / 1000.0f;
        return this;
    }

    public a a(int i2) {
        this.j = i2;
        return this;
    }

    public a d(float f2, float f3) {
        this.f1170h[0] = f2;
        this.f1170h[1] = f3;
        return this;
    }

    public a e(float f2, float f3) {
        this.i[0] = f2;
        this.i[1] = f3;
        return this;
    }

    public a b(int i2) {
        this.k = i2;
        return this;
    }

    public a c(int i2) {
        this.f1163a.a(i2);
        return this;
    }

    public a d(int i2) {
        this.f1163a.b(i2);
        return this;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z = true;
        if (!this.p) {
            return false;
        }
        switch (s.a(motionEvent)) {
            case 0:
                this.f1166d = true;
                this.o = false;
                this.f1163a.a(a(0, motionEvent.getX(), (float) view.getWidth(), (float) this.f1164b.getWidth()), a(1, motionEvent.getY(), (float) view.getHeight(), (float) this.f1164b.getHeight()));
                if (!this.f1167e && a()) {
                    c();
                    break;
                }
            case 1:
            case 3:
                d();
                break;
            case 2:
                this.f1163a.a(a(0, motionEvent.getX(), (float) view.getWidth(), (float) this.f1164b.getWidth()), a(1, motionEvent.getY(), (float) view.getHeight(), (float) this.f1164b.getHeight()));
                c();
                break;
        }
        if (!this.q || !this.f1167e) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        C0025a aVar = this.f1163a;
        int f2 = aVar.f();
        int e2 = aVar.e();
        return (f2 != 0 && f(f2)) || (e2 != 0 && e(e2));
    }

    private void c() {
        if (this.f1169g == null) {
            this.f1169g = new b();
        }
        this.f1167e = true;
        this.f1165c = true;
        if (this.o || this.k <= 0) {
            this.f1169g.run();
        } else {
            ag.a(this.f1164b, this.f1169g, (long) this.k);
        }
        this.o = true;
    }

    private void d() {
        if (this.f1165c) {
            this.f1167e = false;
        } else {
            this.f1163a.b();
        }
    }

    private float a(int i2, float f2, float f3, float f4) {
        float a2 = a(this.f1170h[i2], f3, this.i[i2], f2);
        if (a2 == 0.0f) {
            return 0.0f;
        }
        float f5 = this.l[i2];
        float f6 = this.m[i2];
        float f7 = this.n[i2];
        float f8 = f5 * f4;
        if (a2 > 0.0f) {
            return a(a2 * f8, f6, f7);
        }
        return -a((-a2) * f8, f6, f7);
    }

    private float a(float f2, float f3, float f4, float f5) {
        float interpolation;
        float a2 = a(f2 * f3, 0.0f, f4);
        float f6 = f(f3 - f5, a2) - f(f5, a2);
        if (f6 < 0.0f) {
            interpolation = -this.f1168f.getInterpolation(-f6);
        } else if (f6 <= 0.0f) {
            return 0.0f;
        } else {
            interpolation = this.f1168f.getInterpolation(f6);
        }
        return a(interpolation, -1.0f, 1.0f);
    }

    private float f(float f2, float f3) {
        if (f3 == 0.0f) {
            return 0.0f;
        }
        switch (this.j) {
            case 0:
            case 1:
                if (f2 >= f3) {
                    return 0.0f;
                }
                if (f2 >= 0.0f) {
                    return 1.0f - (f2 / f3);
                }
                if (!this.f1167e || this.j != 1) {
                    return 0.0f;
                }
                return 1.0f;
            case 2:
                if (f2 < 0.0f) {
                    return f2 / (-f3);
                }
                return 0.0f;
            default:
                return 0.0f;
        }
    }

    static int a(int i2, int i3, int i4) {
        if (i2 > i4) {
            return i4;
        }
        if (i2 < i3) {
            return i3;
        }
        return i2;
    }

    static float a(float f2, float f3, float f4) {
        if (f2 > f4) {
            return f4;
        }
        if (f2 < f3) {
            return f3;
        }
        return f2;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
        this.f1164b.onTouchEvent(obtain);
        obtain.recycle();
    }

    /* compiled from: AutoScrollHelper */
    private class b implements Runnable {
        b() {
        }

        public void run() {
            if (a.this.f1167e) {
                if (a.this.f1165c) {
                    a.this.f1165c = false;
                    a.this.f1163a.a();
                }
                C0025a aVar = a.this.f1163a;
                if (aVar.c() || !a.this.a()) {
                    a.this.f1167e = false;
                    return;
                }
                if (a.this.f1166d) {
                    a.this.f1166d = false;
                    a.this.b();
                }
                aVar.d();
                a.this.a(aVar.g(), aVar.h());
                ag.a(a.this.f1164b, this);
            }
        }
    }

    /* renamed from: android.support.v4.widget.a$a  reason: collision with other inner class name */
    /* compiled from: AutoScrollHelper */
    private static class C0025a {

        /* renamed from: a  reason: collision with root package name */
        private int f1171a;

        /* renamed from: b  reason: collision with root package name */
        private int f1172b;

        /* renamed from: c  reason: collision with root package name */
        private float f1173c;

        /* renamed from: d  reason: collision with root package name */
        private float f1174d;

        /* renamed from: e  reason: collision with root package name */
        private long f1175e = Long.MIN_VALUE;

        /* renamed from: f  reason: collision with root package name */
        private long f1176f = 0;

        /* renamed from: g  reason: collision with root package name */
        private int f1177g = 0;

        /* renamed from: h  reason: collision with root package name */
        private int f1178h = 0;
        private long i = -1;
        private float j;
        private int k;

        C0025a() {
        }

        public void a(int i2) {
            this.f1171a = i2;
        }

        public void b(int i2) {
            this.f1172b = i2;
        }

        public void a() {
            this.f1175e = AnimationUtils.currentAnimationTimeMillis();
            this.i = -1;
            this.f1176f = this.f1175e;
            this.j = 0.5f;
            this.f1177g = 0;
            this.f1178h = 0;
        }

        public void b() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.k = a.a((int) (currentAnimationTimeMillis - this.f1175e), 0, this.f1172b);
            this.j = a(currentAnimationTimeMillis);
            this.i = currentAnimationTimeMillis;
        }

        public boolean c() {
            return this.i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.i + ((long) this.k);
        }

        private float a(long j2) {
            if (j2 < this.f1175e) {
                return 0.0f;
            }
            if (this.i < 0 || j2 < this.i) {
                return a.a(((float) (j2 - this.f1175e)) / ((float) this.f1171a), 0.0f, 1.0f) * 0.5f;
            }
            return (a.a(((float) (j2 - this.i)) / ((float) this.k), 0.0f, 1.0f) * this.j) + (1.0f - this.j);
        }

        private float a(float f2) {
            return (-4.0f * f2 * f2) + (4.0f * f2);
        }

        public void d() {
            if (this.f1176f == 0) {
                throw new RuntimeException("Cannot compute scroll delta before calling start()");
            }
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            float a2 = a(a(currentAnimationTimeMillis));
            long j2 = currentAnimationTimeMillis - this.f1176f;
            this.f1176f = currentAnimationTimeMillis;
            this.f1177g = (int) (((float) j2) * a2 * this.f1173c);
            this.f1178h = (int) (((float) j2) * a2 * this.f1174d);
        }

        public void a(float f2, float f3) {
            this.f1173c = f2;
            this.f1174d = f3;
        }

        public int e() {
            return (int) (this.f1173c / Math.abs(this.f1173c));
        }

        public int f() {
            return (int) (this.f1174d / Math.abs(this.f1174d));
        }

        public int g() {
            return this.f1177g;
        }

        public int h() {
            return this.f1178h;
        }
    }
}
