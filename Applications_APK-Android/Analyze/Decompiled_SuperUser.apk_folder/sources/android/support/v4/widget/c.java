package android.support.v4.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.CompoundButton;

/* compiled from: CompoundButtonCompat */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final C0026c f1194a;

    /* renamed from: android.support.v4.widget.c$c  reason: collision with other inner class name */
    /* compiled from: CompoundButtonCompat */
    interface C0026c {
        Drawable a(CompoundButton compoundButton);

        void a(CompoundButton compoundButton, ColorStateList colorStateList);

        void a(CompoundButton compoundButton, PorterDuff.Mode mode);
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            f1194a = new a();
        } else if (i >= 21) {
            f1194a = new d();
        } else {
            f1194a = new b();
        }
    }

    /* compiled from: CompoundButtonCompat */
    static class b implements C0026c {
        b() {
        }

        public void a(CompoundButton compoundButton, ColorStateList colorStateList) {
            e.a(compoundButton, colorStateList);
        }

        public void a(CompoundButton compoundButton, PorterDuff.Mode mode) {
            e.a(compoundButton, mode);
        }

        public Drawable a(CompoundButton compoundButton) {
            return e.a(compoundButton);
        }
    }

    /* compiled from: CompoundButtonCompat */
    static class d extends b {
        d() {
        }

        public void a(CompoundButton compoundButton, ColorStateList colorStateList) {
            f.a(compoundButton, colorStateList);
        }

        public void a(CompoundButton compoundButton, PorterDuff.Mode mode) {
            f.a(compoundButton, mode);
        }
    }

    /* compiled from: CompoundButtonCompat */
    static class a extends d {
        a() {
        }

        public Drawable a(CompoundButton compoundButton) {
            return d.a(compoundButton);
        }
    }

    public static void a(CompoundButton compoundButton, ColorStateList colorStateList) {
        f1194a.a(compoundButton, colorStateList);
    }

    public static void a(CompoundButton compoundButton, PorterDuff.Mode mode) {
        f1194a.a(compoundButton, mode);
    }

    public static Drawable a(CompoundButton compoundButton) {
        return f1194a.a(compoundButton);
    }
}
