package android.support.v4.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class ContentLoadingProgressBar extends ProgressBar {

    /* renamed from: a  reason: collision with root package name */
    long f1060a;

    /* renamed from: b  reason: collision with root package name */
    boolean f1061b;

    /* renamed from: c  reason: collision with root package name */
    boolean f1062c;

    /* renamed from: d  reason: collision with root package name */
    boolean f1063d;

    /* renamed from: e  reason: collision with root package name */
    private final Runnable f1064e;

    /* renamed from: f  reason: collision with root package name */
    private final Runnable f1065f;

    public ContentLoadingProgressBar(Context context) {
        this(context, null);
    }

    public ContentLoadingProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        this.f1060a = -1;
        this.f1061b = false;
        this.f1062c = false;
        this.f1063d = false;
        this.f1064e = new Runnable() {
            public void run() {
                ContentLoadingProgressBar.this.f1061b = false;
                ContentLoadingProgressBar.this.f1060a = -1;
                ContentLoadingProgressBar.this.setVisibility(8);
            }
        };
        this.f1065f = new Runnable() {
            public void run() {
                ContentLoadingProgressBar.this.f1062c = false;
                if (!ContentLoadingProgressBar.this.f1063d) {
                    ContentLoadingProgressBar.this.f1060a = System.currentTimeMillis();
                    ContentLoadingProgressBar.this.setVisibility(0);
                }
            }
        };
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        a();
    }

    private void a() {
        removeCallbacks(this.f1064e);
        removeCallbacks(this.f1065f);
    }
}
