package android.support.v4.widget;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.support.v4.widget.g;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;

public abstract class CursorAdapter extends BaseAdapter implements g.a, Filterable {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f1068a;

    /* renamed from: b  reason: collision with root package name */
    protected boolean f1069b;

    /* renamed from: c  reason: collision with root package name */
    protected Cursor f1070c;

    /* renamed from: d  reason: collision with root package name */
    protected Context f1071d;

    /* renamed from: e  reason: collision with root package name */
    protected int f1072e;

    /* renamed from: f  reason: collision with root package name */
    protected a f1073f;

    /* renamed from: g  reason: collision with root package name */
    protected DataSetObserver f1074g;

    /* renamed from: h  reason: collision with root package name */
    protected g f1075h;
    protected FilterQueryProvider i;

    public abstract View a(Context context, Cursor cursor, ViewGroup viewGroup);

    public abstract void a(View view, Context context, Cursor cursor);

    public CursorAdapter(Context context, Cursor cursor, boolean z) {
        a(context, cursor, z ? 1 : 2);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Cursor cursor, int i2) {
        boolean z = true;
        if ((i2 & 1) == 1) {
            i2 |= 2;
            this.f1069b = true;
        } else {
            this.f1069b = false;
        }
        if (cursor == null) {
            z = false;
        }
        this.f1070c = cursor;
        this.f1068a = z;
        this.f1071d = context;
        this.f1072e = z ? cursor.getColumnIndexOrThrow(com.pureapps.cleaner.db.b.RECORD_ID) : -1;
        if ((i2 & 2) == 2) {
            this.f1073f = new a();
            this.f1074g = new b();
        } else {
            this.f1073f = null;
            this.f1074g = null;
        }
        if (z) {
            if (this.f1073f != null) {
                cursor.registerContentObserver(this.f1073f);
            }
            if (this.f1074g != null) {
                cursor.registerDataSetObserver(this.f1074g);
            }
        }
    }

    public Cursor a() {
        return this.f1070c;
    }

    public int getCount() {
        if (!this.f1068a || this.f1070c == null) {
            return 0;
        }
        return this.f1070c.getCount();
    }

    public Object getItem(int i2) {
        if (!this.f1068a || this.f1070c == null) {
            return null;
        }
        this.f1070c.moveToPosition(i2);
        return this.f1070c;
    }

    public long getItemId(int i2) {
        if (!this.f1068a || this.f1070c == null || !this.f1070c.moveToPosition(i2)) {
            return 0;
        }
        return this.f1070c.getLong(this.f1072e);
    }

    public boolean hasStableIds() {
        return true;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (!this.f1068a) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        } else if (!this.f1070c.moveToPosition(i2)) {
            throw new IllegalStateException("couldn't move cursor to position " + i2);
        } else {
            if (view == null) {
                view = a(this.f1071d, this.f1070c, viewGroup);
            }
            a(view, this.f1071d, this.f1070c);
            return view;
        }
    }

    public View getDropDownView(int i2, View view, ViewGroup viewGroup) {
        if (!this.f1068a) {
            return null;
        }
        this.f1070c.moveToPosition(i2);
        if (view == null) {
            view = b(this.f1071d, this.f1070c, viewGroup);
        }
        a(view, this.f1071d, this.f1070c);
        return view;
    }

    public View b(Context context, Cursor cursor, ViewGroup viewGroup) {
        return a(context, cursor, viewGroup);
    }

    public void a(Cursor cursor) {
        Cursor b2 = b(cursor);
        if (b2 != null) {
            b2.close();
        }
    }

    public Cursor b(Cursor cursor) {
        if (cursor == this.f1070c) {
            return null;
        }
        Cursor cursor2 = this.f1070c;
        if (cursor2 != null) {
            if (this.f1073f != null) {
                cursor2.unregisterContentObserver(this.f1073f);
            }
            if (this.f1074g != null) {
                cursor2.unregisterDataSetObserver(this.f1074g);
            }
        }
        this.f1070c = cursor;
        if (cursor != null) {
            if (this.f1073f != null) {
                cursor.registerContentObserver(this.f1073f);
            }
            if (this.f1074g != null) {
                cursor.registerDataSetObserver(this.f1074g);
            }
            this.f1072e = cursor.getColumnIndexOrThrow(com.pureapps.cleaner.db.b.RECORD_ID);
            this.f1068a = true;
            notifyDataSetChanged();
            return cursor2;
        }
        this.f1072e = -1;
        this.f1068a = false;
        notifyDataSetInvalidated();
        return cursor2;
    }

    public CharSequence c(Cursor cursor) {
        return cursor == null ? "" : cursor.toString();
    }

    public Cursor a(CharSequence charSequence) {
        if (this.i != null) {
            return this.i.runQuery(charSequence);
        }
        return this.f1070c;
    }

    public Filter getFilter() {
        if (this.f1075h == null) {
            this.f1075h = new g(this);
        }
        return this.f1075h;
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.f1069b && this.f1070c != null && !this.f1070c.isClosed()) {
            this.f1068a = this.f1070c.requery();
        }
    }

    private class a extends ContentObserver {
        a() {
            super(new Handler());
        }

        public boolean deliverSelfNotifications() {
            return true;
        }

        public void onChange(boolean z) {
            CursorAdapter.this.b();
        }
    }

    private class b extends DataSetObserver {
        b() {
        }

        public void onChanged() {
            CursorAdapter.this.f1068a = true;
            CursorAdapter.this.notifyDataSetChanged();
        }

        public void onInvalidated() {
            CursorAdapter.this.f1068a = false;
            CursorAdapter.this.notifyDataSetInvalidated();
        }
    }
}
