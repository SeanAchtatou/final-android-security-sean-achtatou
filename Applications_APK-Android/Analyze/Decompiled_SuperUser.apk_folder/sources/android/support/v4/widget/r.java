package android.support.v4.widget;

import android.annotation.TargetApi;
import android.util.Log;
import android.widget.PopupWindow;
import java.lang.reflect.Field;

@TargetApi(21)
/* compiled from: PopupWindowCompatApi21 */
class r {

    /* renamed from: a  reason: collision with root package name */
    private static Field f1223a;

    static {
        try {
            f1223a = PopupWindow.class.getDeclaredField("mOverlapAnchor");
            f1223a.setAccessible(true);
        } catch (NoSuchFieldException e2) {
            Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", e2);
        }
    }

    static void a(PopupWindow popupWindow, boolean z) {
        if (f1223a != null) {
            try {
                f1223a.set(popupWindow, Boolean.valueOf(z));
            } catch (IllegalAccessException e2) {
                Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", e2);
            }
        }
    }
}
