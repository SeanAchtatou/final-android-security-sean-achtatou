package android.support.v4.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.content.c;
import android.support.v4.view.ag;
import android.support.v4.view.s;
import android.support.v4.view.u;
import android.support.v4.view.v;
import android.support.v4.view.w;
import android.support.v4.view.x;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import com.lody.virtual.os.VUserInfo;

public class SwipeRefreshLayout extends ViewGroup implements u, w {
    private static final int[] D = {16842766};
    private static final String m = SwipeRefreshLayout.class.getSimpleName();
    private int A;
    private boolean B;
    private final DecelerateInterpolator C;
    private int E;
    private Animation F;
    private Animation G;
    private Animation H;
    private Animation I;
    private Animation J;
    private int K;
    private a L;
    private Animation.AnimationListener M;
    private final Animation N;
    private final Animation O;

    /* renamed from: a  reason: collision with root package name */
    b f1145a;

    /* renamed from: b  reason: collision with root package name */
    boolean f1146b;

    /* renamed from: c  reason: collision with root package name */
    int f1147c;

    /* renamed from: d  reason: collision with root package name */
    boolean f1148d;

    /* renamed from: e  reason: collision with root package name */
    b f1149e;

    /* renamed from: f  reason: collision with root package name */
    protected int f1150f;

    /* renamed from: g  reason: collision with root package name */
    float f1151g;

    /* renamed from: h  reason: collision with root package name */
    protected int f1152h;
    int i;
    p j;
    boolean k;
    boolean l;
    private View n;
    private int o;
    private float p;
    private float q;
    private final x r;
    private final v s;
    private final int[] t;
    private final int[] u;
    private boolean v;
    private int w;
    private float x;
    private float y;
    private boolean z;

    public interface a {
        boolean a(SwipeRefreshLayout swipeRefreshLayout, View view);
    }

    public interface b {
        void a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public void a() {
        this.f1149e.clearAnimation();
        this.j.stop();
        this.f1149e.setVisibility(8);
        setColorViewAlpha(VUserInfo.FLAG_MASK_USER_TYPE);
        if (this.f1148d) {
            setAnimationProgress(0.0f);
        } else {
            a(this.f1152h - this.f1147c, true);
        }
        this.f1147c = this.f1149e.getTop();
    }

    public void setEnabled(boolean z2) {
        super.setEnabled(z2);
        if (!z2) {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        a();
    }

    @SuppressLint({"NewApi"})
    private void setColorViewAlpha(int i2) {
        this.f1149e.getBackground().setAlpha(i2);
        this.j.setAlpha(i2);
    }

    public int getProgressViewStartOffset() {
        return this.f1152h;
    }

    public int getProgressViewEndOffset() {
        return this.i;
    }

    public void setSize(int i2) {
        if (i2 == 0 || i2 == 1) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            if (i2 == 0) {
                this.K = (int) (displayMetrics.density * 56.0f);
            } else {
                this.K = (int) (displayMetrics.density * 40.0f);
            }
            this.f1149e.setImageDrawable(null);
            this.j.a(i2);
            this.f1149e.setImageDrawable(this.j);
        }
    }

    public SwipeRefreshLayout(Context context) {
        this(context, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
     arg types: [android.support.v4.widget.SwipeRefreshLayout, int]
     candidates:
      android.support.v4.view.ag.a(int, int):int
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.a(android.view.View, float):void
      android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
      android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
      android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.ag.a(android.view.View, boolean):void
      android.support.v4.view.ag.a(android.view.View, int):boolean
      android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void */
    public SwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1146b = false;
        this.p = -1.0f;
        this.t = new int[2];
        this.u = new int[2];
        this.A = -1;
        this.E = -1;
        this.M = new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            @SuppressLint({"NewApi"})
            public void onAnimationEnd(Animation animation) {
                if (SwipeRefreshLayout.this.f1146b) {
                    SwipeRefreshLayout.this.j.setAlpha(VUserInfo.FLAG_MASK_USER_TYPE);
                    SwipeRefreshLayout.this.j.start();
                    if (SwipeRefreshLayout.this.k && SwipeRefreshLayout.this.f1145a != null) {
                        SwipeRefreshLayout.this.f1145a.a();
                    }
                    SwipeRefreshLayout.this.f1147c = SwipeRefreshLayout.this.f1149e.getTop();
                    return;
                }
                SwipeRefreshLayout.this.a();
            }
        };
        this.N = new Animation() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
             arg types: [int, int]
             candidates:
              android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
              android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
              android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
              android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
            public void applyTransformation(float f2, Transformation transformation) {
                int i;
                if (!SwipeRefreshLayout.this.l) {
                    i = SwipeRefreshLayout.this.i - Math.abs(SwipeRefreshLayout.this.f1152h);
                } else {
                    i = SwipeRefreshLayout.this.i;
                }
                SwipeRefreshLayout.this.a((((int) (((float) (i - SwipeRefreshLayout.this.f1150f)) * f2)) + SwipeRefreshLayout.this.f1150f) - SwipeRefreshLayout.this.f1149e.getTop(), false);
                SwipeRefreshLayout.this.j.a(1.0f - f2);
            }
        };
        this.O = new Animation() {
            public void applyTransformation(float f2, Transformation transformation) {
                SwipeRefreshLayout.this.a(f2);
            }
        };
        this.o = ViewConfiguration.get(context).getScaledTouchSlop();
        this.w = getResources().getInteger(17694721);
        setWillNotDraw(false);
        this.C = new DecelerateInterpolator(2.0f);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.K = (int) (40.0f * displayMetrics.density);
        c();
        ag.a((ViewGroup) this, true);
        this.i = (int) (displayMetrics.density * 64.0f);
        this.p = (float) this.i;
        this.r = new x(this);
        this.s = new v(this);
        setNestedScrollingEnabled(true);
        int i2 = -this.K;
        this.f1147c = i2;
        this.f1152h = i2;
        a(1.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, D);
        setEnabled(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.E < 0) {
            return i3;
        }
        if (i3 == i2 - 1) {
            return this.E;
        }
        if (i3 >= this.E) {
            return i3 + 1;
        }
        return i3;
    }

    private void c() {
        this.f1149e = new b(getContext(), -328966);
        this.j = new p(getContext(), this);
        this.j.b(-328966);
        this.f1149e.setImageDrawable(this.j);
        this.f1149e.setVisibility(8);
        addView(this.f1149e);
    }

    public void setOnRefreshListener(b bVar) {
        this.f1145a = bVar;
    }

    private boolean d() {
        return Build.VERSION.SDK_INT < 11;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void */
    public void setRefreshing(boolean z2) {
        int i2;
        if (!z2 || this.f1146b == z2) {
            a(z2, false);
            return;
        }
        this.f1146b = z2;
        if (!this.l) {
            i2 = this.i + this.f1152h;
        } else {
            i2 = this.i;
        }
        a(i2 - this.f1147c, true);
        this.k = false;
        b(this.M);
    }

    @SuppressLint({"NewApi"})
    private void b(Animation.AnimationListener animationListener) {
        this.f1149e.setVisibility(0);
        if (Build.VERSION.SDK_INT >= 11) {
            this.j.setAlpha(VUserInfo.FLAG_MASK_USER_TYPE);
        }
        this.F = new Animation() {
            public void applyTransformation(float f2, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(f2);
            }
        };
        this.F.setDuration((long) this.w);
        if (animationListener != null) {
            this.f1149e.a(animationListener);
        }
        this.f1149e.clearAnimation();
        this.f1149e.startAnimation(this.F);
    }

    /* access modifiers changed from: package-private */
    public void setAnimationProgress(float f2) {
        if (d()) {
            setColorViewAlpha((int) (255.0f * f2));
            return;
        }
        ag.d(this.f1149e, f2);
        ag.e(this.f1149e, f2);
    }

    private void a(boolean z2, boolean z3) {
        if (this.f1146b != z2) {
            this.k = z3;
            g();
            this.f1146b = z2;
            if (this.f1146b) {
                a(this.f1147c, this.M);
            } else {
                a(this.M);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Animation.AnimationListener animationListener) {
        this.G = new Animation() {
            public void applyTransformation(float f2, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(1.0f - f2);
            }
        };
        this.G.setDuration(150);
        this.f1149e.a(animationListener);
        this.f1149e.clearAnimation();
        this.f1149e.startAnimation(this.G);
    }

    @SuppressLint({"NewApi"})
    private void e() {
        this.H = a(this.j.getAlpha(), 76);
    }

    @SuppressLint({"NewApi"})
    private void f() {
        this.I = a(this.j.getAlpha(), (int) VUserInfo.FLAG_MASK_USER_TYPE);
    }

    @SuppressLint({"NewApi"})
    private Animation a(final int i2, final int i3) {
        if (this.f1148d && d()) {
            return null;
        }
        AnonymousClass4 r1 = new Animation() {
            public void applyTransformation(float f2, Transformation transformation) {
                SwipeRefreshLayout.this.j.setAlpha((int) (((float) i2) + (((float) (i3 - i2)) * f2)));
            }
        };
        r1.setDuration(300);
        this.f1149e.a(null);
        this.f1149e.clearAnimation();
        this.f1149e.startAnimation(r1);
        return r1;
    }

    @Deprecated
    public void setProgressBackgroundColor(int i2) {
        setProgressBackgroundColorSchemeResource(i2);
    }

    public void setProgressBackgroundColorSchemeResource(int i2) {
        setProgressBackgroundColorSchemeColor(c.c(getContext(), i2));
    }

    public void setProgressBackgroundColorSchemeColor(int i2) {
        this.f1149e.setBackgroundColor(i2);
        this.j.b(i2);
    }

    @Deprecated
    public void setColorScheme(int... iArr) {
        setColorSchemeResources(iArr);
    }

    public void setColorSchemeResources(int... iArr) {
        Context context = getContext();
        int[] iArr2 = new int[iArr.length];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr2[i2] = c.c(context, iArr[i2]);
        }
        setColorSchemeColors(iArr2);
    }

    public void setColorSchemeColors(int... iArr) {
        g();
        this.j.a(iArr);
    }

    private void g() {
        if (this.n == null) {
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                View childAt = getChildAt(i2);
                if (!childAt.equals(this.f1149e)) {
                    this.n = childAt;
                    return;
                }
            }
        }
    }

    public void setDistanceToTriggerSync(int i2) {
        this.p = (float) i2;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() != 0) {
            if (this.n == null) {
                g();
            }
            if (this.n != null) {
                View view = this.n;
                int paddingLeft = getPaddingLeft();
                int paddingTop = getPaddingTop();
                view.layout(paddingLeft, paddingTop, ((measuredWidth - getPaddingLeft()) - getPaddingRight()) + paddingLeft, ((measuredHeight - getPaddingTop()) - getPaddingBottom()) + paddingTop);
                int measuredWidth2 = this.f1149e.getMeasuredWidth();
                this.f1149e.layout((measuredWidth / 2) - (measuredWidth2 / 2), this.f1147c, (measuredWidth / 2) + (measuredWidth2 / 2), this.f1147c + this.f1149e.getMeasuredHeight());
            }
        }
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.n == null) {
            g();
        }
        if (this.n != null) {
            this.n.measure(View.MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
            this.f1149e.measure(View.MeasureSpec.makeMeasureSpec(this.K, 1073741824), View.MeasureSpec.makeMeasureSpec(this.K, 1073741824));
            this.E = -1;
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                if (getChildAt(i4) == this.f1149e) {
                    this.E = i4;
                    return;
                }
            }
        }
    }

    public int getProgressCircleDiameter() {
        return this.K;
    }

    public boolean b() {
        boolean z2 = false;
        if (this.L != null) {
            return this.L.a(this, this.n);
        }
        if (Build.VERSION.SDK_INT >= 14) {
            return ag.b(this.n, -1);
        }
        if (this.n instanceof AbsListView) {
            AbsListView absListView = (AbsListView) this.n;
            return absListView.getChildCount() > 0 && (absListView.getFirstVisiblePosition() > 0 || absListView.getChildAt(0).getTop() < absListView.getPaddingTop());
        }
        if (ag.b(this.n, -1) || this.n.getScrollY() > 0) {
            z2 = true;
        }
        return z2;
    }

    public void setOnChildScrollUpCallback(a aVar) {
        this.L = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        g();
        int a2 = s.a(motionEvent);
        if (this.B && a2 == 0) {
            this.B = false;
        }
        if (!isEnabled() || this.B || b() || this.f1146b || this.v) {
            return false;
        }
        switch (a2) {
            case 0:
                a(this.f1152h - this.f1149e.getTop(), true);
                this.A = motionEvent.getPointerId(0);
                this.z = false;
                int findPointerIndex = motionEvent.findPointerIndex(this.A);
                if (findPointerIndex >= 0) {
                    this.y = motionEvent.getY(findPointerIndex);
                    break;
                } else {
                    return false;
                }
            case 1:
            case 3:
                this.z = false;
                this.A = -1;
                break;
            case 2:
                if (this.A == -1) {
                    Log.e(m, "Got ACTION_MOVE event but don't have an active pointer id.");
                    return false;
                }
                int findPointerIndex2 = motionEvent.findPointerIndex(this.A);
                if (findPointerIndex2 >= 0) {
                    d(motionEvent.getY(findPointerIndex2));
                    break;
                } else {
                    return false;
                }
            case 6:
                a(motionEvent);
                break;
        }
        return this.z;
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        if (Build.VERSION.SDK_INT < 21 && (this.n instanceof AbsListView)) {
            return;
        }
        if (this.n == null || ag.D(this.n)) {
            super.requestDisallowInterceptTouchEvent(z2);
        }
    }

    public boolean onStartNestedScroll(View view, View view2, int i2) {
        return isEnabled() && !this.B && !this.f1146b && (i2 & 2) != 0;
    }

    public void onNestedScrollAccepted(View view, View view2, int i2) {
        this.r.a(view, view2, i2);
        startNestedScroll(i2 & 2);
        this.q = 0.0f;
        this.v = true;
    }

    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
        if (i3 > 0 && this.q > 0.0f) {
            if (((float) i3) > this.q) {
                iArr[1] = i3 - ((int) this.q);
                this.q = 0.0f;
            } else {
                this.q -= (float) i3;
                iArr[1] = i3;
            }
            b(this.q);
        }
        if (this.l && i3 > 0 && this.q == 0.0f && Math.abs(i3 - iArr[1]) > 0) {
            this.f1149e.setVisibility(8);
        }
        int[] iArr2 = this.t;
        if (dispatchNestedPreScroll(i2 - iArr[0], i3 - iArr[1], iArr2, null)) {
            iArr[0] = iArr[0] + iArr2[0];
            iArr[1] = iArr2[1] + iArr[1];
        }
    }

    public int getNestedScrollAxes() {
        return this.r.a();
    }

    public void onStopNestedScroll(View view) {
        this.r.a(view);
        this.v = false;
        if (this.q > 0.0f) {
            c(this.q);
            this.q = 0.0f;
        }
        stopNestedScroll();
    }

    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        dispatchNestedScroll(i2, i3, i4, i5, this.u);
        int i6 = this.u[1] + i5;
        if (i6 < 0 && !b()) {
            this.q = ((float) Math.abs(i6)) + this.q;
            b(this.q);
        }
    }

    public void setNestedScrollingEnabled(boolean z2) {
        this.s.a(z2);
    }

    public boolean isNestedScrollingEnabled() {
        return this.s.a();
    }

    public boolean startNestedScroll(int i2) {
        return this.s.a(i2);
    }

    public void stopNestedScroll() {
        this.s.c();
    }

    public boolean hasNestedScrollingParent() {
        return this.s.b();
    }

    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return this.s.a(i2, i3, i4, i5, iArr);
    }

    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return this.s.a(i2, i3, iArr, iArr2);
    }

    public boolean onNestedPreFling(View view, float f2, float f3) {
        return dispatchNestedPreFling(f2, f3);
    }

    public boolean onNestedFling(View view, float f2, float f3, boolean z2) {
        return dispatchNestedFling(f2, f3, z2);
    }

    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return this.s.a(f2, f3, z2);
    }

    public boolean dispatchNestedPreFling(float f2, float f3) {
        return this.s.a(f2, f3);
    }

    private boolean a(Animation animation) {
        return animation != null && animation.hasStarted() && !animation.hasEnded();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.d(android.view.View, float):void
     arg types: [android.support.v4.widget.b, int]
     candidates:
      android.support.v4.view.ag.d(android.view.View, int):void
      android.support.v4.view.ag.d(android.view.View, boolean):void
      android.support.v4.view.ag.d(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.e(android.view.View, float):void
     arg types: [android.support.v4.widget.b, int]
     candidates:
      android.support.v4.view.ag.e(android.view.View, int):void
      android.support.v4.view.ag.e(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    @SuppressLint({"NewApi"})
    private void b(float f2) {
        float f3;
        this.j.a(true);
        float min = Math.min(1.0f, Math.abs(f2 / this.p));
        float max = (((float) Math.max(((double) min) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
        float abs = Math.abs(f2) - this.p;
        if (this.l) {
            f3 = (float) (this.i - this.f1152h);
        } else {
            f3 = (float) this.i;
        }
        float max2 = Math.max(0.0f, Math.min(abs, f3 * 2.0f) / f3);
        float pow = ((float) (((double) (max2 / 4.0f)) - Math.pow((double) (max2 / 4.0f), 2.0d))) * 2.0f;
        int i2 = ((int) ((f3 * min) + (f3 * pow * 2.0f))) + this.f1152h;
        if (this.f1149e.getVisibility() != 0) {
            this.f1149e.setVisibility(0);
        }
        if (!this.f1148d) {
            ag.d((View) this.f1149e, 1.0f);
            ag.e((View) this.f1149e, 1.0f);
        }
        if (this.f1148d) {
            setAnimationProgress(Math.min(1.0f, f2 / this.p));
        }
        if (f2 < this.p) {
            if (this.j.getAlpha() > 76 && !a(this.H)) {
                e();
            }
        } else if (this.j.getAlpha() < 255 && !a(this.I)) {
            f();
        }
        this.j.a(0.0f, Math.min(0.8f, max * 0.8f));
        this.j.a(Math.min(1.0f, max));
        this.j.b((-0.25f + (max * 0.4f) + (pow * 2.0f)) * 0.5f);
        a(i2 - this.f1147c, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void */
    private void c(float f2) {
        if (f2 > this.p) {
            a(true, true);
            return;
        }
        this.f1146b = false;
        this.j.a(0.0f, 0.0f);
        AnonymousClass5 r0 = null;
        if (!this.f1148d) {
            r0 = new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    if (!SwipeRefreshLayout.this.f1148d) {
                        SwipeRefreshLayout.this.a((Animation.AnimationListener) null);
                    }
                }

                public void onAnimationRepeat(Animation animation) {
                }
            };
        }
        b(this.f1147c, r0);
        this.j.a(false);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a2 = s.a(motionEvent);
        if (this.B && a2 == 0) {
            this.B = false;
        }
        if (!isEnabled() || this.B || b() || this.f1146b || this.v) {
            return false;
        }
        switch (a2) {
            case 0:
                this.A = motionEvent.getPointerId(0);
                this.z = false;
                break;
            case 1:
                int findPointerIndex = motionEvent.findPointerIndex(this.A);
                if (findPointerIndex < 0) {
                    Log.e(m, "Got ACTION_UP event but don't have an active pointer id.");
                    return false;
                }
                if (this.z) {
                    this.z = false;
                    c((motionEvent.getY(findPointerIndex) - this.x) * 0.5f);
                }
                this.A = -1;
                return false;
            case 2:
                int findPointerIndex2 = motionEvent.findPointerIndex(this.A);
                if (findPointerIndex2 < 0) {
                    Log.e(m, "Got ACTION_MOVE event but have an invalid active pointer id.");
                    return false;
                }
                float y2 = motionEvent.getY(findPointerIndex2);
                d(y2);
                if (this.z) {
                    float f2 = (y2 - this.x) * 0.5f;
                    if (f2 > 0.0f) {
                        b(f2);
                        break;
                    } else {
                        return false;
                    }
                }
                break;
            case 3:
                return false;
            case 5:
                int b2 = s.b(motionEvent);
                if (b2 >= 0) {
                    this.A = motionEvent.getPointerId(b2);
                    break;
                } else {
                    Log.e(m, "Got ACTION_POINTER_DOWN event but have an invalid action index.");
                    return false;
                }
            case 6:
                a(motionEvent);
                break;
        }
        return true;
    }

    @SuppressLint({"NewApi"})
    private void d(float f2) {
        if (f2 - this.y > ((float) this.o) && !this.z) {
            this.x = this.y + ((float) this.o);
            this.z = true;
            this.j.setAlpha(76);
        }
    }

    private void a(int i2, Animation.AnimationListener animationListener) {
        this.f1150f = i2;
        this.N.reset();
        this.N.setDuration(200);
        this.N.setInterpolator(this.C);
        if (animationListener != null) {
            this.f1149e.a(animationListener);
        }
        this.f1149e.clearAnimation();
        this.f1149e.startAnimation(this.N);
    }

    private void b(int i2, Animation.AnimationListener animationListener) {
        if (this.f1148d) {
            c(i2, animationListener);
            return;
        }
        this.f1150f = i2;
        this.O.reset();
        this.O.setDuration(200);
        this.O.setInterpolator(this.C);
        if (animationListener != null) {
            this.f1149e.a(animationListener);
        }
        this.f1149e.clearAnimation();
        this.f1149e.startAnimation(this.O);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(float f2) {
        a((this.f1150f + ((int) (((float) (this.f1152h - this.f1150f)) * f2))) - this.f1149e.getTop(), false);
    }

    @SuppressLint({"NewApi"})
    private void c(int i2, Animation.AnimationListener animationListener) {
        this.f1150f = i2;
        if (d()) {
            this.f1151g = (float) this.j.getAlpha();
        } else {
            this.f1151g = ag.s(this.f1149e);
        }
        this.J = new Animation() {
            public void applyTransformation(float f2, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(SwipeRefreshLayout.this.f1151g + ((-SwipeRefreshLayout.this.f1151g) * f2));
                SwipeRefreshLayout.this.a(f2);
            }
        };
        this.J.setDuration(150);
        if (animationListener != null) {
            this.f1149e.a(animationListener);
        }
        this.f1149e.clearAnimation();
        this.f1149e.startAnimation(this.J);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.e(android.view.View, int):void
     arg types: [android.support.v4.widget.b, int]
     candidates:
      android.support.v4.view.ag.e(android.view.View, float):void
      android.support.v4.view.ag.e(android.view.View, int):void */
    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2) {
        this.f1149e.bringToFront();
        ag.e((View) this.f1149e, i2);
        this.f1147c = this.f1149e.getTop();
        if (z2 && Build.VERSION.SDK_INT < 11) {
            invalidate();
        }
    }

    private void a(MotionEvent motionEvent) {
        int b2 = s.b(motionEvent);
        if (motionEvent.getPointerId(b2) == this.A) {
            this.A = motionEvent.getPointerId(b2 == 0 ? 1 : 0);
        }
    }
}
