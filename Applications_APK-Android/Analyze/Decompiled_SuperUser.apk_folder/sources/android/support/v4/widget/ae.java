package android.support.v4.widget;

import android.content.Context;
import android.support.v4.view.ag;
import android.support.v4.view.s;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import com.duapps.ad.AdError;
import java.util.Arrays;

/* compiled from: ViewDragHelper */
public class ae {
    private static final Interpolator v = new Interpolator() {
        public float getInterpolation(float f2) {
            float f3 = f2 - 1.0f;
            return (f3 * f3 * f3 * f3 * f3) + 1.0f;
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private int f1180a;

    /* renamed from: b  reason: collision with root package name */
    private int f1181b;

    /* renamed from: c  reason: collision with root package name */
    private int f1182c = -1;

    /* renamed from: d  reason: collision with root package name */
    private float[] f1183d;

    /* renamed from: e  reason: collision with root package name */
    private float[] f1184e;

    /* renamed from: f  reason: collision with root package name */
    private float[] f1185f;

    /* renamed from: g  reason: collision with root package name */
    private float[] f1186g;

    /* renamed from: h  reason: collision with root package name */
    private int[] f1187h;
    private int[] i;
    private int[] j;
    private int k;
    private VelocityTracker l;
    private float m;
    private float n;
    private int o;
    private int p;
    private u q;
    private final a r;
    private View s;
    private boolean t;
    private final ViewGroup u;
    private final Runnable w = new Runnable() {
        public void run() {
            ae.this.c(0);
        }
    };

    /* compiled from: ViewDragHelper */
    public static abstract class a {
        public abstract boolean tryCaptureView(View view, int i);

        public void onViewDragStateChanged(int i) {
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
        }

        public void onViewCaptured(View view, int i) {
        }

        public void onViewReleased(View view, float f2, float f3) {
        }

        public void onEdgeTouched(int i, int i2) {
        }

        public boolean onEdgeLock(int i) {
            return false;
        }

        public void onEdgeDragStarted(int i, int i2) {
        }

        public int getOrderedChildIndex(int i) {
            return i;
        }

        public int getViewHorizontalDragRange(View view) {
            return 0;
        }

        public int getViewVerticalDragRange(View view) {
            return 0;
        }

        public int clampViewPositionHorizontal(View view, int i, int i2) {
            return 0;
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            return 0;
        }
    }

    public static ae a(ViewGroup viewGroup, a aVar) {
        return new ae(viewGroup.getContext(), viewGroup, aVar);
    }

    public static ae a(ViewGroup viewGroup, float f2, a aVar) {
        ae a2 = a(viewGroup, aVar);
        a2.f1181b = (int) (((float) a2.f1181b) * (1.0f / f2));
        return a2;
    }

    private ae(Context context, ViewGroup viewGroup, a aVar) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Parent view may not be null");
        } else if (aVar == null) {
            throw new IllegalArgumentException("Callback may not be null");
        } else {
            this.u = viewGroup;
            this.r = aVar;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.o = (int) ((context.getResources().getDisplayMetrics().density * 20.0f) + 0.5f);
            this.f1181b = viewConfiguration.getScaledTouchSlop();
            this.m = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.n = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.q = u.a(context, v);
        }
    }

    public void a(float f2) {
        this.n = f2;
    }

    public int a() {
        return this.f1180a;
    }

    public void a(int i2) {
        this.p = i2;
    }

    public int b() {
        return this.o;
    }

    public void a(View view, int i2) {
        if (view.getParent() != this.u) {
            throw new IllegalArgumentException("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (" + this.u + ")");
        }
        this.s = view;
        this.f1182c = i2;
        this.r.onViewCaptured(view, i2);
        c(1);
    }

    public View c() {
        return this.s;
    }

    public int d() {
        return this.f1181b;
    }

    public void e() {
        this.f1182c = -1;
        g();
        if (this.l != null) {
            this.l.recycle();
            this.l = null;
        }
    }

    public void f() {
        e();
        if (this.f1180a == 2) {
            int b2 = this.q.b();
            int c2 = this.q.c();
            this.q.h();
            int b3 = this.q.b();
            int c3 = this.q.c();
            this.r.onViewPositionChanged(this.s, b3, c3, b3 - b2, c3 - c2);
        }
        c(0);
    }

    public boolean a(View view, int i2, int i3) {
        this.s = view;
        this.f1182c = -1;
        boolean a2 = a(i2, i3, 0, 0);
        if (!a2 && this.f1180a == 0 && this.s != null) {
            this.s = null;
        }
        return a2;
    }

    public boolean a(int i2, int i3) {
        if (this.t) {
            return a(i2, i3, (int) android.support.v4.view.ae.a(this.l, this.f1182c), (int) android.support.v4.view.ae.b(this.l, this.f1182c));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    private boolean a(int i2, int i3, int i4, int i5) {
        int left = this.s.getLeft();
        int top = this.s.getTop();
        int i6 = i2 - left;
        int i7 = i3 - top;
        if (i6 == 0 && i7 == 0) {
            this.q.h();
            c(0);
            return false;
        }
        this.q.a(left, top, i6, i7, a(this.s, i6, i7, i4, i5));
        c(2);
        return true;
    }

    private int a(View view, int i2, int i3, int i4, int i5) {
        int b2 = b(i4, (int) this.n, (int) this.m);
        int b3 = b(i5, (int) this.n, (int) this.m);
        int abs = Math.abs(i2);
        int abs2 = Math.abs(i3);
        int abs3 = Math.abs(b2);
        int abs4 = Math.abs(b3);
        int i6 = abs3 + abs4;
        int i7 = abs + abs2;
        return (int) (((b3 != 0 ? ((float) abs4) / ((float) i6) : ((float) abs2) / ((float) i7)) * ((float) a(i3, b3, this.r.getViewVerticalDragRange(view)))) + ((b2 != 0 ? ((float) abs3) / ((float) i6) : ((float) abs) / ((float) i7)) * ((float) a(i2, b2, this.r.getViewHorizontalDragRange(view)))));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private int a(int i2, int i3, int i4) {
        int abs;
        if (i2 == 0) {
            return 0;
        }
        int width = this.u.getWidth();
        int i5 = width / 2;
        float b2 = (b(Math.min(1.0f, ((float) Math.abs(i2)) / ((float) width))) * ((float) i5)) + ((float) i5);
        int abs2 = Math.abs(i3);
        if (abs2 > 0) {
            abs = Math.round(Math.abs(b2 / ((float) abs2)) * 1000.0f) * 4;
        } else {
            abs = (int) (((((float) Math.abs(i2)) / ((float) i4)) + 1.0f) * 256.0f);
        }
        return Math.min(abs, 600);
    }

    private int b(int i2, int i3, int i4) {
        int abs = Math.abs(i2);
        if (abs < i3) {
            return 0;
        }
        if (abs <= i4) {
            return i2;
        }
        if (i2 <= 0) {
            return -i4;
        }
        return i4;
    }

    private float a(float f2, float f3, float f4) {
        float abs = Math.abs(f2);
        if (abs < f3) {
            return 0.0f;
        }
        if (abs <= f4) {
            return f2;
        }
        if (f2 <= 0.0f) {
            return -f4;
        }
        return f4;
    }

    private float b(float f2) {
        return (float) Math.sin((double) ((float) (((double) (f2 - 0.5f)) * 0.4712389167638204d)));
    }

    public boolean a(boolean z) {
        boolean z2;
        if (this.f1180a == 2) {
            boolean g2 = this.q.g();
            int b2 = this.q.b();
            int c2 = this.q.c();
            int left = b2 - this.s.getLeft();
            int top = c2 - this.s.getTop();
            if (left != 0) {
                ag.f(this.s, left);
            }
            if (top != 0) {
                ag.e(this.s, top);
            }
            if (!(left == 0 && top == 0)) {
                this.r.onViewPositionChanged(this.s, b2, c2, left, top);
            }
            if (g2 && b2 == this.q.d() && c2 == this.q.e()) {
                this.q.h();
                z2 = false;
            } else {
                z2 = g2;
            }
            if (!z2) {
                if (z) {
                    this.u.post(this.w);
                } else {
                    c(0);
                }
            }
        }
        return this.f1180a == 2;
    }

    private void a(float f2, float f3) {
        this.t = true;
        this.r.onViewReleased(this.s, f2, f3);
        this.t = false;
        if (this.f1180a == 1) {
            c(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(float[], float):void}
     arg types: [float[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void} */
    private void g() {
        if (this.f1183d != null) {
            Arrays.fill(this.f1183d, 0.0f);
            Arrays.fill(this.f1184e, 0.0f);
            Arrays.fill(this.f1185f, 0.0f);
            Arrays.fill(this.f1186g, 0.0f);
            Arrays.fill(this.f1187h, 0);
            Arrays.fill(this.i, 0);
            Arrays.fill(this.j, 0);
            this.k = 0;
        }
    }

    private void e(int i2) {
        if (this.f1183d != null && b(i2)) {
            this.f1183d[i2] = 0.0f;
            this.f1184e[i2] = 0.0f;
            this.f1185f[i2] = 0.0f;
            this.f1186g[i2] = 0.0f;
            this.f1187h[i2] = 0;
            this.i[i2] = 0;
            this.j[i2] = 0;
            this.k &= (1 << i2) ^ -1;
        }
    }

    private void f(int i2) {
        if (this.f1183d == null || this.f1183d.length <= i2) {
            float[] fArr = new float[(i2 + 1)];
            float[] fArr2 = new float[(i2 + 1)];
            float[] fArr3 = new float[(i2 + 1)];
            float[] fArr4 = new float[(i2 + 1)];
            int[] iArr = new int[(i2 + 1)];
            int[] iArr2 = new int[(i2 + 1)];
            int[] iArr3 = new int[(i2 + 1)];
            if (this.f1183d != null) {
                System.arraycopy(this.f1183d, 0, fArr, 0, this.f1183d.length);
                System.arraycopy(this.f1184e, 0, fArr2, 0, this.f1184e.length);
                System.arraycopy(this.f1185f, 0, fArr3, 0, this.f1185f.length);
                System.arraycopy(this.f1186g, 0, fArr4, 0, this.f1186g.length);
                System.arraycopy(this.f1187h, 0, iArr, 0, this.f1187h.length);
                System.arraycopy(this.i, 0, iArr2, 0, this.i.length);
                System.arraycopy(this.j, 0, iArr3, 0, this.j.length);
            }
            this.f1183d = fArr;
            this.f1184e = fArr2;
            this.f1185f = fArr3;
            this.f1186g = fArr4;
            this.f1187h = iArr;
            this.i = iArr2;
            this.j = iArr3;
        }
    }

    private void a(float f2, float f3, int i2) {
        f(i2);
        float[] fArr = this.f1183d;
        this.f1185f[i2] = f2;
        fArr[i2] = f2;
        float[] fArr2 = this.f1184e;
        this.f1186g[i2] = f3;
        fArr2[i2] = f3;
        this.f1187h[i2] = e((int) f2, (int) f3);
        this.k |= 1 << i2;
    }

    private void c(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        for (int i2 = 0; i2 < pointerCount; i2++) {
            int pointerId = motionEvent.getPointerId(i2);
            if (g(pointerId)) {
                float x = motionEvent.getX(i2);
                float y = motionEvent.getY(i2);
                this.f1185f[pointerId] = x;
                this.f1186g[pointerId] = y;
            }
        }
    }

    public boolean b(int i2) {
        return (this.k & (1 << i2)) != 0;
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        this.u.removeCallbacks(this.w);
        if (this.f1180a != i2) {
            this.f1180a = i2;
            this.r.onViewDragStateChanged(i2);
            if (this.f1180a == 0) {
                this.s = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(View view, int i2) {
        if (view == this.s && this.f1182c == i2) {
            return true;
        }
        if (view == null || !this.r.tryCaptureView(view, i2)) {
            return false;
        }
        this.f1182c = i2;
        a(view, i2);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ff, code lost:
        if (r8 != r7) goto L_0x010e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.view.MotionEvent r14) {
        /*
            r13 = this;
            int r0 = android.support.v4.view.s.a(r14)
            int r1 = android.support.v4.view.s.b(r14)
            if (r0 != 0) goto L_0x000d
            r13.e()
        L_0x000d:
            android.view.VelocityTracker r2 = r13.l
            if (r2 != 0) goto L_0x0017
            android.view.VelocityTracker r2 = android.view.VelocityTracker.obtain()
            r13.l = r2
        L_0x0017:
            android.view.VelocityTracker r2 = r13.l
            r2.addMovement(r14)
            switch(r0) {
                case 0: goto L_0x0026;
                case 1: goto L_0x0128;
                case 2: goto L_0x0092;
                case 3: goto L_0x0128;
                case 4: goto L_0x001f;
                case 5: goto L_0x005a;
                case 6: goto L_0x011f;
                default: goto L_0x001f;
            }
        L_0x001f:
            int r0 = r13.f1180a
            r1 = 1
            if (r0 != r1) goto L_0x012d
            r0 = 1
        L_0x0025:
            return r0
        L_0x0026:
            float r0 = r14.getX()
            float r1 = r14.getY()
            r2 = 0
            int r2 = r14.getPointerId(r2)
            r13.a(r0, r1, r2)
            int r0 = (int) r0
            int r1 = (int) r1
            android.view.View r0 = r13.d(r0, r1)
            android.view.View r1 = r13.s
            if (r0 != r1) goto L_0x0048
            int r1 = r13.f1180a
            r3 = 2
            if (r1 != r3) goto L_0x0048
            r13.b(r0, r2)
        L_0x0048:
            int[] r0 = r13.f1187h
            r0 = r0[r2]
            int r1 = r13.p
            r1 = r1 & r0
            if (r1 == 0) goto L_0x001f
            android.support.v4.widget.ae$a r1 = r13.r
            int r3 = r13.p
            r0 = r0 & r3
            r1.onEdgeTouched(r0, r2)
            goto L_0x001f
        L_0x005a:
            int r0 = r14.getPointerId(r1)
            float r2 = r14.getX(r1)
            float r1 = r14.getY(r1)
            r13.a(r2, r1, r0)
            int r3 = r13.f1180a
            if (r3 != 0) goto L_0x007f
            int[] r1 = r13.f1187h
            r1 = r1[r0]
            int r2 = r13.p
            r2 = r2 & r1
            if (r2 == 0) goto L_0x001f
            android.support.v4.widget.ae$a r2 = r13.r
            int r3 = r13.p
            r1 = r1 & r3
            r2.onEdgeTouched(r1, r0)
            goto L_0x001f
        L_0x007f:
            int r3 = r13.f1180a
            r4 = 2
            if (r3 != r4) goto L_0x001f
            int r2 = (int) r2
            int r1 = (int) r1
            android.view.View r1 = r13.d(r2, r1)
            android.view.View r2 = r13.s
            if (r1 != r2) goto L_0x001f
            r13.b(r1, r0)
            goto L_0x001f
        L_0x0092:
            float[] r0 = r13.f1183d
            if (r0 == 0) goto L_0x001f
            float[] r0 = r13.f1184e
            if (r0 == 0) goto L_0x001f
            int r2 = r14.getPointerCount()
            r0 = 0
            r1 = r0
        L_0x00a0:
            if (r1 >= r2) goto L_0x0107
            int r3 = r14.getPointerId(r1)
            boolean r0 = r13.g(r3)
            if (r0 != 0) goto L_0x00b0
        L_0x00ac:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00a0
        L_0x00b0:
            float r0 = r14.getX(r1)
            float r4 = r14.getY(r1)
            float[] r5 = r13.f1183d
            r5 = r5[r3]
            float r5 = r0 - r5
            float[] r6 = r13.f1184e
            r6 = r6[r3]
            float r6 = r4 - r6
            int r0 = (int) r0
            int r4 = (int) r4
            android.view.View r4 = r13.d(r0, r4)
            if (r4 == 0) goto L_0x010c
            boolean r0 = r13.a(r4, r5, r6)
            if (r0 == 0) goto L_0x010c
            r0 = 1
        L_0x00d3:
            if (r0 == 0) goto L_0x010e
            int r7 = r4.getLeft()
            int r8 = (int) r5
            int r8 = r8 + r7
            android.support.v4.widget.ae$a r9 = r13.r
            int r10 = (int) r5
            int r8 = r9.clampViewPositionHorizontal(r4, r8, r10)
            int r9 = r4.getTop()
            int r10 = (int) r6
            int r10 = r10 + r9
            android.support.v4.widget.ae$a r11 = r13.r
            int r12 = (int) r6
            int r10 = r11.clampViewPositionVertical(r4, r10, r12)
            android.support.v4.widget.ae$a r11 = r13.r
            int r11 = r11.getViewHorizontalDragRange(r4)
            android.support.v4.widget.ae$a r12 = r13.r
            int r12 = r12.getViewVerticalDragRange(r4)
            if (r11 == 0) goto L_0x0101
            if (r11 <= 0) goto L_0x010e
            if (r8 != r7) goto L_0x010e
        L_0x0101:
            if (r12 == 0) goto L_0x0107
            if (r12 <= 0) goto L_0x010e
            if (r10 != r9) goto L_0x010e
        L_0x0107:
            r13.c(r14)
            goto L_0x001f
        L_0x010c:
            r0 = 0
            goto L_0x00d3
        L_0x010e:
            r13.b(r5, r6, r3)
            int r5 = r13.f1180a
            r6 = 1
            if (r5 == r6) goto L_0x0107
            if (r0 == 0) goto L_0x00ac
            boolean r0 = r13.b(r4, r3)
            if (r0 == 0) goto L_0x00ac
            goto L_0x0107
        L_0x011f:
            int r0 = r14.getPointerId(r1)
            r13.e(r0)
            goto L_0x001f
        L_0x0128:
            r13.e()
            goto L_0x001f
        L_0x012d:
            r0 = 0
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.ae.a(android.view.MotionEvent):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.ae.a(float, float):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.ae.a(android.view.ViewGroup, android.support.v4.widget.ae$a):android.support.v4.widget.ae
      android.support.v4.widget.ae.a(android.view.View, int):void
      android.support.v4.widget.ae.a(int, int):boolean
      android.support.v4.widget.ae.a(float, float):void */
    public void b(MotionEvent motionEvent) {
        int i2;
        int i3 = 0;
        int a2 = s.a(motionEvent);
        int b2 = s.b(motionEvent);
        if (a2 == 0) {
            e();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        switch (a2) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int pointerId = motionEvent.getPointerId(0);
                View d2 = d((int) x, (int) y);
                a(x, y, pointerId);
                b(d2, pointerId);
                int i4 = this.f1187h[pointerId];
                if ((this.p & i4) != 0) {
                    this.r.onEdgeTouched(i4 & this.p, pointerId);
                    return;
                }
                return;
            case 1:
                if (this.f1180a == 1) {
                    h();
                }
                e();
                return;
            case 2:
                if (this.f1180a != 1) {
                    int pointerCount = motionEvent.getPointerCount();
                    while (i3 < pointerCount) {
                        int pointerId2 = motionEvent.getPointerId(i3);
                        if (g(pointerId2)) {
                            float x2 = motionEvent.getX(i3);
                            float y2 = motionEvent.getY(i3);
                            float f2 = x2 - this.f1183d[pointerId2];
                            float f3 = y2 - this.f1184e[pointerId2];
                            b(f2, f3, pointerId2);
                            if (this.f1180a != 1) {
                                View d3 = d((int) x2, (int) y2);
                                if (a(d3, f2, f3) && b(d3, pointerId2)) {
                                }
                            }
                            c(motionEvent);
                            return;
                        }
                        i3++;
                    }
                    c(motionEvent);
                    return;
                } else if (g(this.f1182c)) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.f1182c);
                    float x3 = motionEvent.getX(findPointerIndex);
                    float y3 = motionEvent.getY(findPointerIndex);
                    int i5 = (int) (x3 - this.f1185f[this.f1182c]);
                    int i6 = (int) (y3 - this.f1186g[this.f1182c]);
                    b(this.s.getLeft() + i5, this.s.getTop() + i6, i5, i6);
                    c(motionEvent);
                    return;
                } else {
                    return;
                }
            case 3:
                if (this.f1180a == 1) {
                    a(0.0f, 0.0f);
                }
                e();
                return;
            case 4:
            default:
                return;
            case 5:
                int pointerId3 = motionEvent.getPointerId(b2);
                float x4 = motionEvent.getX(b2);
                float y4 = motionEvent.getY(b2);
                a(x4, y4, pointerId3);
                if (this.f1180a == 0) {
                    b(d((int) x4, (int) y4), pointerId3);
                    int i7 = this.f1187h[pointerId3];
                    if ((this.p & i7) != 0) {
                        this.r.onEdgeTouched(i7 & this.p, pointerId3);
                        return;
                    }
                    return;
                } else if (c((int) x4, (int) y4)) {
                    b(this.s, pointerId3);
                    return;
                } else {
                    return;
                }
            case 6:
                int pointerId4 = motionEvent.getPointerId(b2);
                if (this.f1180a == 1 && pointerId4 == this.f1182c) {
                    int pointerCount2 = motionEvent.getPointerCount();
                    while (true) {
                        if (i3 >= pointerCount2) {
                            i2 = -1;
                        } else {
                            int pointerId5 = motionEvent.getPointerId(i3);
                            if (pointerId5 != this.f1182c) {
                                if (d((int) motionEvent.getX(i3), (int) motionEvent.getY(i3)) == this.s && b(this.s, pointerId5)) {
                                    i2 = this.f1182c;
                                }
                            }
                            i3++;
                        }
                    }
                    if (i2 == -1) {
                        h();
                    }
                }
                e(pointerId4);
                return;
        }
    }

    private void b(float f2, float f3, int i2) {
        int i3 = 1;
        if (!a(f2, f3, i2, 1)) {
            i3 = 0;
        }
        if (a(f3, f2, i2, 4)) {
            i3 |= 4;
        }
        if (a(f2, f3, i2, 2)) {
            i3 |= 2;
        }
        if (a(f3, f2, i2, 8)) {
            i3 |= 8;
        }
        if (i3 != 0) {
            int[] iArr = this.i;
            iArr[i2] = iArr[i2] | i3;
            this.r.onEdgeDragStarted(i3, i2);
        }
    }

    private boolean a(float f2, float f3, int i2, int i3) {
        float abs = Math.abs(f2);
        float abs2 = Math.abs(f3);
        if ((this.f1187h[i2] & i3) != i3 || (this.p & i3) == 0 || (this.j[i2] & i3) == i3 || (this.i[i2] & i3) == i3) {
            return false;
        }
        if (abs <= ((float) this.f1181b) && abs2 <= ((float) this.f1181b)) {
            return false;
        }
        if (abs < abs2 * 0.5f && this.r.onEdgeLock(i3)) {
            int[] iArr = this.j;
            iArr[i2] = iArr[i2] | i3;
            return false;
        } else if ((this.i[i2] & i3) != 0 || abs <= ((float) this.f1181b)) {
            return false;
        } else {
            return true;
        }
    }

    private boolean a(View view, float f2, float f3) {
        boolean z;
        boolean z2;
        if (view == null) {
            return false;
        }
        if (this.r.getViewHorizontalDragRange(view) > 0) {
            z = true;
        } else {
            z = false;
        }
        if (this.r.getViewVerticalDragRange(view) > 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!z || !z2) {
            if (z) {
                if (Math.abs(f2) <= ((float) this.f1181b)) {
                    return false;
                }
                return true;
            } else if (!z2) {
                return false;
            } else {
                if (Math.abs(f3) <= ((float) this.f1181b)) {
                    return false;
                }
                return true;
            }
        } else if ((f2 * f2) + (f3 * f3) <= ((float) (this.f1181b * this.f1181b))) {
            return false;
        } else {
            return true;
        }
    }

    public boolean d(int i2) {
        int length = this.f1183d.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (b(i2, i3)) {
                return true;
            }
        }
        return false;
    }

    public boolean b(int i2, int i3) {
        boolean z;
        if (!b(i3)) {
            return false;
        }
        boolean z2 = (i2 & 1) == 1;
        if ((i2 & 2) == 2) {
            z = true;
        } else {
            z = false;
        }
        float f2 = this.f1185f[i3] - this.f1183d[i3];
        float f3 = this.f1186g[i3] - this.f1184e[i3];
        if (!z2 || !z) {
            if (z2) {
                if (Math.abs(f2) <= ((float) this.f1181b)) {
                    return false;
                }
                return true;
            } else if (!z) {
                return false;
            } else {
                if (Math.abs(f3) <= ((float) this.f1181b)) {
                    return false;
                }
                return true;
            }
        } else if ((f2 * f2) + (f3 * f3) <= ((float) (this.f1181b * this.f1181b))) {
            return false;
        } else {
            return true;
        }
    }

    private void h() {
        this.l.computeCurrentVelocity(AdError.NETWORK_ERROR_CODE, this.m);
        a(a(android.support.v4.view.ae.a(this.l, this.f1182c), this.n, this.m), a(android.support.v4.view.ae.b(this.l, this.f1182c), this.n, this.m));
    }

    private void b(int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int left = this.s.getLeft();
        int top = this.s.getTop();
        if (i4 != 0) {
            i6 = this.r.clampViewPositionHorizontal(this.s, i2, i4);
            ag.f(this.s, i6 - left);
        } else {
            i6 = i2;
        }
        if (i5 != 0) {
            i7 = this.r.clampViewPositionVertical(this.s, i3, i5);
            ag.e(this.s, i7 - top);
        } else {
            i7 = i3;
        }
        if (i4 != 0 || i5 != 0) {
            this.r.onViewPositionChanged(this.s, i6, i7, i6 - left, i7 - top);
        }
    }

    public boolean c(int i2, int i3) {
        return b(this.s, i2, i3);
    }

    public boolean b(View view, int i2, int i3) {
        if (view != null && i2 >= view.getLeft() && i2 < view.getRight() && i3 >= view.getTop() && i3 < view.getBottom()) {
            return true;
        }
        return false;
    }

    public View d(int i2, int i3) {
        for (int childCount = this.u.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = this.u.getChildAt(this.r.getOrderedChildIndex(childCount));
            if (i2 >= childAt.getLeft() && i2 < childAt.getRight() && i3 >= childAt.getTop() && i3 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }

    private int e(int i2, int i3) {
        int i4 = 0;
        if (i2 < this.u.getLeft() + this.o) {
            i4 = 1;
        }
        if (i3 < this.u.getTop() + this.o) {
            i4 |= 4;
        }
        if (i2 > this.u.getRight() - this.o) {
            i4 |= 2;
        }
        if (i3 > this.u.getBottom() - this.o) {
            return i4 | 8;
        }
        return i4;
    }

    private boolean g(int i2) {
        if (b(i2)) {
            return true;
        }
        Log.e("ViewDragHelper", "Ignoring pointerId=" + i2 + " because ACTION_DOWN was not received " + "for this pointer before ACTION_MOVE. It likely happened because " + " ViewDragHelper did not receive all the events in the event stream.");
        return false;
    }
}
