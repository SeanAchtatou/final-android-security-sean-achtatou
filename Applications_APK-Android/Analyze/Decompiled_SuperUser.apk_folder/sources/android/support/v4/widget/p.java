package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import com.lody.virtual.os.VUserInfo;
import java.util.ArrayList;

/* compiled from: MaterialProgressDrawable */
class p extends Drawable implements Animatable {

    /* renamed from: a  reason: collision with root package name */
    static final Interpolator f1199a = new FastOutSlowInInterpolator();

    /* renamed from: d  reason: collision with root package name */
    private static final Interpolator f1200d = new LinearInterpolator();

    /* renamed from: e  reason: collision with root package name */
    private static final int[] f1201e = {-16777216};

    /* renamed from: b  reason: collision with root package name */
    float f1202b;

    /* renamed from: c  reason: collision with root package name */
    boolean f1203c;

    /* renamed from: f  reason: collision with root package name */
    private final ArrayList<Animation> f1204f = new ArrayList<>();

    /* renamed from: g  reason: collision with root package name */
    private final a f1205g;

    /* renamed from: h  reason: collision with root package name */
    private float f1206h;
    private Resources i;
    private View j;
    private Animation k;
    private double l;
    private double m;
    private final Drawable.Callback n = new Drawable.Callback() {
        public void invalidateDrawable(Drawable drawable) {
            p.this.invalidateSelf();
        }

        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            p.this.scheduleSelf(runnable, j);
        }

        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            p.this.unscheduleSelf(runnable);
        }
    };

    p(Context context, View view) {
        this.j = view;
        this.i = context.getResources();
        this.f1205g = new a(this.n);
        this.f1205g.a(f1201e);
        a(1);
        a();
    }

    private void a(double d2, double d3, double d4, double d5, float f2, float f3) {
        a aVar = this.f1205g;
        float f4 = this.i.getDisplayMetrics().density;
        this.l = ((double) f4) * d2;
        this.m = ((double) f4) * d3;
        aVar.a(((float) d5) * f4);
        aVar.a(((double) f4) * d4);
        aVar.c(0);
        aVar.a(f2 * f4, f4 * f3);
        aVar.a((int) this.l, (int) this.m);
    }

    public void a(int i2) {
        if (i2 == 0) {
            a(56.0d, 56.0d, 12.5d, 3.0d, 12.0f, 6.0f);
        } else {
            a(40.0d, 40.0d, 8.75d, 2.5d, 10.0f, 5.0f);
        }
    }

    public void a(boolean z) {
        this.f1205g.a(z);
    }

    public void a(float f2) {
        this.f1205g.e(f2);
    }

    public void a(float f2, float f3) {
        this.f1205g.b(f2);
        this.f1205g.c(f3);
    }

    public void b(float f2) {
        this.f1205g.d(f2);
    }

    public void b(int i2) {
        this.f1205g.a(i2);
    }

    public void a(int... iArr) {
        this.f1205g.a(iArr);
        this.f1205g.c(0);
    }

    public int getIntrinsicHeight() {
        return (int) this.m;
    }

    public int getIntrinsicWidth() {
        return (int) this.l;
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        int save = canvas.save();
        canvas.rotate(this.f1206h, bounds.exactCenterX(), bounds.exactCenterY());
        this.f1205g.a(canvas, bounds);
        canvas.restoreToCount(save);
    }

    public void setAlpha(int i2) {
        this.f1205g.d(i2);
    }

    public int getAlpha() {
        return this.f1205g.c();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f1205g.a(colorFilter);
    }

    /* access modifiers changed from: package-private */
    public void c(float f2) {
        this.f1206h = f2;
        invalidateSelf();
    }

    public int getOpacity() {
        return -3;
    }

    public boolean isRunning() {
        ArrayList<Animation> arrayList = this.f1204f;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            Animation animation = arrayList.get(i2);
            if (animation.hasStarted() && !animation.hasEnded()) {
                return true;
            }
        }
        return false;
    }

    public void start() {
        this.k.reset();
        this.f1205g.l();
        if (this.f1205g.i() != this.f1205g.e()) {
            this.f1203c = true;
            this.k.setDuration(666);
            this.j.startAnimation(this.k);
            return;
        }
        this.f1205g.c(0);
        this.f1205g.m();
        this.k.setDuration(1332);
        this.j.startAnimation(this.k);
    }

    public void stop() {
        this.j.clearAnimation();
        c(0.0f);
        this.f1205g.a(false);
        this.f1205g.c(0);
        this.f1205g.m();
    }

    /* access modifiers changed from: package-private */
    public float a(a aVar) {
        return (float) Math.toRadians(((double) aVar.d()) / (6.283185307179586d * aVar.j()));
    }

    private int a(float f2, int i2, int i3) {
        int intValue = Integer.valueOf(i2).intValue();
        int i4 = (intValue >> 24) & VUserInfo.FLAG_MASK_USER_TYPE;
        int i5 = (intValue >> 16) & VUserInfo.FLAG_MASK_USER_TYPE;
        int i6 = (intValue >> 8) & VUserInfo.FLAG_MASK_USER_TYPE;
        int i7 = intValue & VUserInfo.FLAG_MASK_USER_TYPE;
        int intValue2 = Integer.valueOf(i3).intValue();
        return (i7 + ((int) (((float) ((intValue2 & VUserInfo.FLAG_MASK_USER_TYPE) - i7)) * f2))) | ((i4 + ((int) (((float) (((intValue2 >> 24) & VUserInfo.FLAG_MASK_USER_TYPE) - i4)) * f2))) << 24) | ((i5 + ((int) (((float) (((intValue2 >> 16) & VUserInfo.FLAG_MASK_USER_TYPE) - i5)) * f2))) << 16) | ((((int) (((float) (((intValue2 >> 8) & VUserInfo.FLAG_MASK_USER_TYPE) - i6)) * f2)) + i6) << 8);
    }

    /* access modifiers changed from: package-private */
    public void a(float f2, a aVar) {
        if (f2 > 0.75f) {
            aVar.b(a((f2 - 0.75f) / 0.25f, aVar.h(), aVar.a()));
        }
    }

    /* access modifiers changed from: package-private */
    public void b(float f2, a aVar) {
        a(f2, aVar);
        float a2 = a(aVar);
        aVar.b((((aVar.g() - a2) - aVar.f()) * f2) + aVar.f());
        aVar.c(aVar.g());
        aVar.d(((((float) (Math.floor((double) (aVar.k() / 0.8f)) + 1.0d)) - aVar.k()) * f2) + aVar.k());
    }

    private void a() {
        final a aVar = this.f1205g;
        AnonymousClass1 r1 = new Animation() {
            public void applyTransformation(float f2, Transformation transformation) {
                if (p.this.f1203c) {
                    p.this.b(f2, aVar);
                    return;
                }
                float a2 = p.this.a(aVar);
                float g2 = aVar.g();
                float f3 = aVar.f();
                float k = aVar.k();
                p.this.a(f2, aVar);
                if (f2 <= 0.5f) {
                    aVar.b(f3 + (p.f1199a.getInterpolation(f2 / 0.5f) * (0.8f - a2)));
                }
                if (f2 > 0.5f) {
                    aVar.c(((0.8f - a2) * p.f1199a.getInterpolation((f2 - 0.5f) / 0.5f)) + g2);
                }
                aVar.d((0.25f * f2) + k);
                p.this.c((216.0f * f2) + (1080.0f * (p.this.f1202b / 5.0f)));
            }
        };
        r1.setRepeatCount(-1);
        r1.setRepeatMode(1);
        r1.setInterpolator(f1200d);
        r1.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                p.this.f1202b = 0.0f;
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
                aVar.l();
                aVar.b();
                aVar.b(aVar.i());
                if (p.this.f1203c) {
                    p.this.f1203c = false;
                    animation.setDuration(1332);
                    aVar.a(false);
                    return;
                }
                p.this.f1202b = (p.this.f1202b + 1.0f) % 5.0f;
            }
        });
        this.k = r1;
    }

    /* compiled from: MaterialProgressDrawable */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        private final RectF f1212a = new RectF();

        /* renamed from: b  reason: collision with root package name */
        private final Paint f1213b = new Paint();

        /* renamed from: c  reason: collision with root package name */
        private final Paint f1214c = new Paint();

        /* renamed from: d  reason: collision with root package name */
        private final Drawable.Callback f1215d;

        /* renamed from: e  reason: collision with root package name */
        private float f1216e = 0.0f;

        /* renamed from: f  reason: collision with root package name */
        private float f1217f = 0.0f;

        /* renamed from: g  reason: collision with root package name */
        private float f1218g = 0.0f;

        /* renamed from: h  reason: collision with root package name */
        private float f1219h = 5.0f;
        private float i = 2.5f;
        private int[] j;
        private int k;
        private float l;
        private float m;
        private float n;
        private boolean o;
        private Path p;
        private float q;
        private double r;
        private int s;
        private int t;
        private int u;
        private final Paint v = new Paint(1);
        private int w;
        private int x;

        a(Drawable.Callback callback) {
            this.f1215d = callback;
            this.f1213b.setStrokeCap(Paint.Cap.SQUARE);
            this.f1213b.setAntiAlias(true);
            this.f1213b.setStyle(Paint.Style.STROKE);
            this.f1214c.setStyle(Paint.Style.FILL);
            this.f1214c.setAntiAlias(true);
        }

        public void a(int i2) {
            this.w = i2;
        }

        public void a(float f2, float f3) {
            this.s = (int) f2;
            this.t = (int) f3;
        }

        public void a(Canvas canvas, Rect rect) {
            RectF rectF = this.f1212a;
            rectF.set(rect);
            rectF.inset(this.i, this.i);
            float f2 = (this.f1216e + this.f1218g) * 360.0f;
            float f3 = ((this.f1217f + this.f1218g) * 360.0f) - f2;
            this.f1213b.setColor(this.x);
            canvas.drawArc(rectF, f2, f3, false, this.f1213b);
            a(canvas, f2, f3, rect);
            if (this.u < 255) {
                this.v.setColor(this.w);
                this.v.setAlpha(255 - this.u);
                canvas.drawCircle(rect.exactCenterX(), rect.exactCenterY(), (float) (rect.width() / 2), this.v);
            }
        }

        private void a(Canvas canvas, float f2, float f3, Rect rect) {
            if (this.o) {
                if (this.p == null) {
                    this.p = new Path();
                    this.p.setFillType(Path.FillType.EVEN_ODD);
                } else {
                    this.p.reset();
                }
                float f4 = ((float) (((int) this.i) / 2)) * this.q;
                float cos = (float) ((this.r * Math.cos(0.0d)) + ((double) rect.exactCenterX()));
                this.p.moveTo(0.0f, 0.0f);
                this.p.lineTo(((float) this.s) * this.q, 0.0f);
                this.p.lineTo((((float) this.s) * this.q) / 2.0f, ((float) this.t) * this.q);
                this.p.offset(cos - f4, (float) ((this.r * Math.sin(0.0d)) + ((double) rect.exactCenterY())));
                this.p.close();
                this.f1214c.setColor(this.x);
                canvas.rotate((f2 + f3) - 5.0f, rect.exactCenterX(), rect.exactCenterY());
                canvas.drawPath(this.p, this.f1214c);
            }
        }

        public void a(int[] iArr) {
            this.j = iArr;
            c(0);
        }

        public void b(int i2) {
            this.x = i2;
        }

        public void c(int i2) {
            this.k = i2;
            this.x = this.j[this.k];
        }

        public int a() {
            return this.j[n()];
        }

        private int n() {
            return (this.k + 1) % this.j.length;
        }

        public void b() {
            c(n());
        }

        public void a(ColorFilter colorFilter) {
            this.f1213b.setColorFilter(colorFilter);
            o();
        }

        public void d(int i2) {
            this.u = i2;
        }

        public int c() {
            return this.u;
        }

        public void a(float f2) {
            this.f1219h = f2;
            this.f1213b.setStrokeWidth(f2);
            o();
        }

        public float d() {
            return this.f1219h;
        }

        public void b(float f2) {
            this.f1216e = f2;
            o();
        }

        public float e() {
            return this.f1216e;
        }

        public float f() {
            return this.l;
        }

        public float g() {
            return this.m;
        }

        public int h() {
            return this.j[this.k];
        }

        public void c(float f2) {
            this.f1217f = f2;
            o();
        }

        public float i() {
            return this.f1217f;
        }

        public void d(float f2) {
            this.f1218g = f2;
            o();
        }

        public void a(int i2, int i3) {
            float f2;
            float min = (float) Math.min(i2, i3);
            if (this.r <= 0.0d || min < 0.0f) {
                f2 = (float) Math.ceil((double) (this.f1219h / 2.0f));
            } else {
                f2 = (float) (((double) (min / 2.0f)) - this.r);
            }
            this.i = f2;
        }

        public void a(double d2) {
            this.r = d2;
        }

        public double j() {
            return this.r;
        }

        public void a(boolean z) {
            if (this.o != z) {
                this.o = z;
                o();
            }
        }

        public void e(float f2) {
            if (f2 != this.q) {
                this.q = f2;
                o();
            }
        }

        public float k() {
            return this.n;
        }

        public void l() {
            this.l = this.f1216e;
            this.m = this.f1217f;
            this.n = this.f1218g;
        }

        public void m() {
            this.l = 0.0f;
            this.m = 0.0f;
            this.n = 0.0f;
            b(0.0f);
            c(0.0f);
            d(0.0f);
        }

        private void o() {
            this.f1215d.invalidateDrawable(null);
        }
    }
}
