package android.support.v4.widget;

import android.annotation.TargetApi;
import android.view.View;
import android.widget.PopupWindow;

@TargetApi(19)
/* compiled from: PopupWindowCompatKitKat */
class t {
    public static void a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
        popupWindow.showAsDropDown(view, i, i2, i3);
    }
}
