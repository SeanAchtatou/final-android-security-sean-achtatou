package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.a.e;
import android.support.v4.view.a.q;
import android.support.v4.view.ac;
import android.support.v4.view.ae;
import android.support.v4.view.ag;
import android.support.v4.view.s;
import android.support.v4.view.u;
import android.support.v4.view.v;
import android.support.v4.view.w;
import android.support.v4.view.x;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import com.duapps.ad.AdError;
import com.lody.virtual.os.VUserInfo;
import java.util.ArrayList;

public class NestedScrollView extends FrameLayout implements ac, u, w {
    private static final a v = new a();
    private static final int[] w = {16843130};
    private b A;

    /* renamed from: a  reason: collision with root package name */
    private long f1115a;

    /* renamed from: b  reason: collision with root package name */
    private final Rect f1116b;

    /* renamed from: c  reason: collision with root package name */
    private u f1117c;

    /* renamed from: d  reason: collision with root package name */
    private EdgeEffectCompat f1118d;

    /* renamed from: e  reason: collision with root package name */
    private EdgeEffectCompat f1119e;

    /* renamed from: f  reason: collision with root package name */
    private int f1120f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f1121g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f1122h;
    private View i;
    private boolean j;
    private VelocityTracker k;
    private boolean l;
    private boolean m;
    private int n;
    private int o;
    private int p;
    private int q;
    private final int[] r;
    private final int[] s;
    private int t;
    private SavedState u;
    private final x x;
    private final v y;
    private float z;

    public interface b {
        void a(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4);
    }

    public NestedScrollView(Context context) {
        this(context, null);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1116b = new Rect();
        this.f1121g = true;
        this.f1122h = false;
        this.i = null;
        this.j = false;
        this.m = true;
        this.q = -1;
        this.r = new int[2];
        this.s = new int[2];
        a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w, i2, 0);
        setFillViewport(obtainStyledAttributes.getBoolean(0, false));
        obtainStyledAttributes.recycle();
        this.x = new x(this);
        this.y = new v(this);
        setNestedScrollingEnabled(true);
        ag.a(this, v);
    }

    public void setNestedScrollingEnabled(boolean z2) {
        this.y.a(z2);
    }

    public boolean isNestedScrollingEnabled() {
        return this.y.a();
    }

    public boolean startNestedScroll(int i2) {
        return this.y.a(i2);
    }

    public void stopNestedScroll() {
        this.y.c();
    }

    public boolean hasNestedScrollingParent() {
        return this.y.b();
    }

    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return this.y.a(i2, i3, i4, i5, iArr);
    }

    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return this.y.a(i2, i3, iArr, iArr2);
    }

    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return this.y.a(f2, f3, z2);
    }

    public boolean dispatchNestedPreFling(float f2, float f3) {
        return this.y.a(f2, f3);
    }

    public boolean onStartNestedScroll(View view, View view2, int i2) {
        return (i2 & 2) != 0;
    }

    public void onNestedScrollAccepted(View view, View view2, int i2) {
        this.x.a(view, view2, i2);
        startNestedScroll(2);
    }

    public void onStopNestedScroll(View view) {
        this.x.a(view);
        stopNestedScroll();
    }

    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        int scrollY = getScrollY();
        scrollBy(0, i5);
        int scrollY2 = getScrollY() - scrollY;
        dispatchNestedScroll(0, scrollY2, 0, i5 - scrollY2, null);
    }

    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
        dispatchNestedPreScroll(i2, i3, iArr, null);
    }

    public boolean onNestedFling(View view, float f2, float f3, boolean z2) {
        if (z2) {
            return false;
        }
        f((int) f3);
        return true;
    }

    public boolean onNestedPreFling(View view, float f2, float f3) {
        return dispatchNestedPreFling(f2, f3);
    }

    public int getNestedScrollAxes() {
        return this.x.a();
    }

    public boolean shouldDelayChildPressedState() {
        return true;
    }

    /* access modifiers changed from: protected */
    public float getTopFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int scrollY = getScrollY();
        if (scrollY < verticalFadingEdgeLength) {
            return ((float) scrollY) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    /* access modifiers changed from: protected */
    public float getBottomFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int bottom = (getChildAt(0).getBottom() - getScrollY()) - (getHeight() - getPaddingBottom());
        if (bottom < verticalFadingEdgeLength) {
            return ((float) bottom) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    public int getMaxScrollAmount() {
        return (int) (0.5f * ((float) getHeight()));
    }

    private void a() {
        this.f1117c = u.a(getContext(), null);
        setFocusable(true);
        setDescendantFocusability(262144);
        setWillNotDraw(false);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.n = viewConfiguration.getScaledTouchSlop();
        this.o = viewConfiguration.getScaledMinimumFlingVelocity();
        this.p = viewConfiguration.getScaledMaximumFlingVelocity();
    }

    public void addView(View view) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view);
    }

    public void addView(View view, int i2) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view, i2);
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view, layoutParams);
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
        super.addView(view, i2, layoutParams);
    }

    public void setOnScrollChangeListener(b bVar) {
        this.A = bVar;
    }

    private boolean b() {
        View childAt = getChildAt(0);
        if (childAt == null) {
            return false;
        }
        if (getHeight() < childAt.getHeight() + getPaddingTop() + getPaddingBottom()) {
            return true;
        }
        return false;
    }

    public void setFillViewport(boolean z2) {
        if (z2 != this.l) {
            this.l = z2;
            requestLayout();
        }
    }

    public void setSmoothScrollingEnabled(boolean z2) {
        this.m = z2;
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i2, int i3, int i4, int i5) {
        super.onScrollChanged(i2, i3, i4, i5);
        if (this.A != null) {
            this.A.a(this, i2, i3, i4, i5);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.l && View.MeasureSpec.getMode(i3) != 0 && getChildCount() > 0) {
            View childAt = getChildAt(0);
            int measuredHeight = getMeasuredHeight();
            if (childAt.getMeasuredHeight() < measuredHeight) {
                childAt.measure(getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight(), ((FrameLayout.LayoutParams) childAt.getLayoutParams()).width), View.MeasureSpec.makeMeasureSpec((measuredHeight - getPaddingTop()) - getPaddingBottom(), 1073741824));
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || a(keyEvent);
    }

    public boolean a(KeyEvent keyEvent) {
        boolean z2;
        int i2 = 33;
        this.f1116b.setEmpty();
        if (!b()) {
            if (!isFocused() || keyEvent.getKeyCode() == 4) {
                return false;
            }
            View findFocus = findFocus();
            if (findFocus == this) {
                findFocus = null;
            }
            View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, 130);
            if (findNextFocus == null || findNextFocus == this || !findNextFocus.requestFocus(130)) {
                z2 = false;
            } else {
                z2 = true;
            }
            return z2;
        } else if (keyEvent.getAction() != 0) {
            return false;
        } else {
            switch (keyEvent.getKeyCode()) {
                case 19:
                    if (!keyEvent.isAltPressed()) {
                        return c(33);
                    }
                    return b(33);
                case 20:
                    if (!keyEvent.isAltPressed()) {
                        return c(130);
                    }
                    return b(130);
                case 62:
                    if (!keyEvent.isShiftPressed()) {
                        i2 = 130;
                    }
                    a(i2);
                    return false;
                default:
                    return false;
            }
        }
    }

    private boolean c(int i2, int i3) {
        if (getChildCount() <= 0) {
            return false;
        }
        int scrollY = getScrollY();
        View childAt = getChildAt(0);
        if (i3 < childAt.getTop() - scrollY || i3 >= childAt.getBottom() - scrollY || i2 < childAt.getLeft() || i2 >= childAt.getRight()) {
            return false;
        }
        return true;
    }

    private void c() {
        if (this.k == null) {
            this.k = VelocityTracker.obtain();
        } else {
            this.k.clear();
        }
    }

    private void d() {
        if (this.k == null) {
            this.k = VelocityTracker.obtain();
        }
    }

    private void e() {
        if (this.k != null) {
            this.k.recycle();
            this.k = null;
        }
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        if (z2) {
            e();
        }
        super.requestDisallowInterceptTouchEvent(z2);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2 = false;
        int action = motionEvent.getAction();
        if (action == 2 && this.j) {
            return true;
        }
        switch (action & VUserInfo.FLAG_MASK_USER_TYPE) {
            case 0:
                int y2 = (int) motionEvent.getY();
                if (c((int) motionEvent.getX(), y2)) {
                    this.f1120f = y2;
                    this.q = motionEvent.getPointerId(0);
                    c();
                    this.k.addMovement(motionEvent);
                    this.f1117c.g();
                    if (!this.f1117c.a()) {
                        z2 = true;
                    }
                    this.j = z2;
                    startNestedScroll(2);
                    break;
                } else {
                    this.j = false;
                    e();
                    break;
                }
            case 1:
            case 3:
                this.j = false;
                this.q = -1;
                e();
                if (this.f1117c.a(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                    ag.c(this);
                }
                stopNestedScroll();
                break;
            case 2:
                int i2 = this.q;
                if (i2 != -1) {
                    int findPointerIndex = motionEvent.findPointerIndex(i2);
                    if (findPointerIndex != -1) {
                        int y3 = (int) motionEvent.getY(findPointerIndex);
                        if (Math.abs(y3 - this.f1120f) > this.n && (getNestedScrollAxes() & 2) == 0) {
                            this.j = true;
                            this.f1120f = y3;
                            d();
                            this.k.addMovement(motionEvent);
                            this.t = 0;
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                                break;
                            }
                        }
                    } else {
                        Log.e("NestedScrollView", "Invalid pointerId=" + i2 + " in onInterceptTouchEvent");
                        break;
                    }
                }
                break;
            case 6:
                a(motionEvent);
                break;
        }
        return this.j;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2;
        boolean z2;
        ViewParent parent;
        d();
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        int a2 = s.a(motionEvent);
        if (a2 == 0) {
            this.t = 0;
        }
        obtain.offsetLocation(0.0f, (float) this.t);
        switch (a2) {
            case 0:
                if (getChildCount() != 0) {
                    boolean z3 = !this.f1117c.a();
                    this.j = z3;
                    if (z3 && (parent = getParent()) != null) {
                        parent.requestDisallowInterceptTouchEvent(true);
                    }
                    if (!this.f1117c.a()) {
                        this.f1117c.h();
                    }
                    this.f1120f = (int) motionEvent.getY();
                    this.q = motionEvent.getPointerId(0);
                    startNestedScroll(2);
                    break;
                } else {
                    return false;
                }
            case 1:
                if (this.j) {
                    VelocityTracker velocityTracker = this.k;
                    velocityTracker.computeCurrentVelocity(AdError.NETWORK_ERROR_CODE, (float) this.p);
                    int b2 = (int) ae.b(velocityTracker, this.q);
                    if (Math.abs(b2) > this.o) {
                        f(-b2);
                    } else if (this.f1117c.a(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                        ag.c(this);
                    }
                }
                this.q = -1;
                f();
                break;
            case 2:
                int findPointerIndex = motionEvent.findPointerIndex(this.q);
                if (findPointerIndex != -1) {
                    int y2 = (int) motionEvent.getY(findPointerIndex);
                    int i3 = this.f1120f - y2;
                    if (dispatchNestedPreScroll(0, i3, this.s, this.r)) {
                        i3 -= this.s[1];
                        obtain.offsetLocation(0.0f, (float) this.r[1]);
                        this.t = this.t + this.r[1];
                    }
                    if (this.j || Math.abs(i3) <= this.n) {
                        i2 = i3;
                    } else {
                        ViewParent parent2 = getParent();
                        if (parent2 != null) {
                            parent2.requestDisallowInterceptTouchEvent(true);
                        }
                        this.j = true;
                        if (i3 > 0) {
                            i2 = i3 - this.n;
                        } else {
                            i2 = i3 + this.n;
                        }
                    }
                    if (this.j) {
                        this.f1120f = y2 - this.r[1];
                        int scrollY = getScrollY();
                        int scrollRange = getScrollRange();
                        int overScrollMode = getOverScrollMode();
                        if (overScrollMode == 0 || (overScrollMode == 1 && scrollRange > 0)) {
                            z2 = true;
                        } else {
                            z2 = false;
                        }
                        if (a(0, i2, 0, getScrollY(), 0, scrollRange, 0, 0, true) && !hasNestedScrollingParent()) {
                            this.k.clear();
                        }
                        int scrollY2 = getScrollY() - scrollY;
                        if (!dispatchNestedScroll(0, scrollY2, 0, i2 - scrollY2, this.r)) {
                            if (z2) {
                                g();
                                int i4 = scrollY + i2;
                                if (i4 < 0) {
                                    this.f1118d.a(((float) i2) / ((float) getHeight()), motionEvent.getX(findPointerIndex) / ((float) getWidth()));
                                    if (!this.f1119e.a()) {
                                        this.f1119e.c();
                                    }
                                } else if (i4 > scrollRange) {
                                    this.f1119e.a(((float) i2) / ((float) getHeight()), 1.0f - (motionEvent.getX(findPointerIndex) / ((float) getWidth())));
                                    if (!this.f1118d.a()) {
                                        this.f1118d.c();
                                    }
                                }
                                if (this.f1118d != null && (!this.f1118d.a() || !this.f1119e.a())) {
                                    ag.c(this);
                                    break;
                                }
                            }
                        } else {
                            this.f1120f = this.f1120f - this.r[1];
                            obtain.offsetLocation(0.0f, (float) this.r[1]);
                            this.t = this.t + this.r[1];
                            break;
                        }
                    }
                } else {
                    Log.e("NestedScrollView", "Invalid pointerId=" + this.q + " in onTouchEvent");
                    break;
                }
                break;
            case 3:
                if (this.j && getChildCount() > 0 && this.f1117c.a(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                    ag.c(this);
                }
                this.q = -1;
                f();
                break;
            case 5:
                int b3 = s.b(motionEvent);
                this.f1120f = (int) motionEvent.getY(b3);
                this.q = motionEvent.getPointerId(b3);
                break;
            case 6:
                a(motionEvent);
                this.f1120f = (int) motionEvent.getY(motionEvent.findPointerIndex(this.q));
                break;
        }
        if (this.k != null) {
            this.k.addMovement(obtain);
        }
        obtain.recycle();
        return true;
    }

    private void a(MotionEvent motionEvent) {
        int action = (motionEvent.getAction() & 65280) >> 8;
        if (motionEvent.getPointerId(action) == this.q) {
            int i2 = action == 0 ? 1 : 0;
            this.f1120f = (int) motionEvent.getY(i2);
            this.q = motionEvent.getPointerId(i2);
            if (this.k != null) {
                this.k.clear();
            }
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        if ((motionEvent.getSource() & 2) == 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 8:
                if (this.j) {
                    return false;
                }
                float a2 = s.a(motionEvent, 9);
                if (a2 == 0.0f) {
                    return false;
                }
                int verticalScrollFactorCompat = (int) (a2 * getVerticalScrollFactorCompat());
                int scrollRange = getScrollRange();
                int scrollY = getScrollY();
                int i2 = scrollY - verticalScrollFactorCompat;
                if (i2 < 0) {
                    scrollRange = 0;
                } else if (i2 <= scrollRange) {
                    scrollRange = i2;
                }
                if (scrollRange == scrollY) {
                    return false;
                }
                super.scrollTo(getScrollX(), scrollRange);
                return true;
            default:
                return false;
        }
    }

    private float getVerticalScrollFactorCompat() {
        if (this.z == 0.0f) {
            TypedValue typedValue = new TypedValue();
            Context context = getContext();
            if (!context.getTheme().resolveAttribute(16842829, typedValue, true)) {
                throw new IllegalStateException("Expected theme to define listPreferredItemHeight.");
            }
            this.z = typedValue.getDimension(context.getResources().getDisplayMetrics());
        }
        return this.z;
    }

    /* access modifiers changed from: protected */
    public void onOverScrolled(int i2, int i3, boolean z2, boolean z3) {
        super.scrollTo(i2, i3);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, boolean z2) {
        boolean z3;
        boolean z4;
        int overScrollMode = getOverScrollMode();
        boolean z5 = computeHorizontalScrollRange() > computeHorizontalScrollExtent();
        boolean z6 = computeVerticalScrollRange() > computeVerticalScrollExtent();
        boolean z7 = overScrollMode == 0 || (overScrollMode == 1 && z5);
        boolean z8 = overScrollMode == 0 || (overScrollMode == 1 && z6);
        int i10 = i4 + i2;
        if (!z7) {
            i8 = 0;
        }
        int i11 = i5 + i3;
        if (!z8) {
            i9 = 0;
        }
        int i12 = -i8;
        int i13 = i8 + i6;
        int i14 = -i9;
        int i15 = i9 + i7;
        if (i10 > i13) {
            z3 = true;
        } else if (i10 < i12) {
            z3 = true;
            i13 = i12;
        } else {
            z3 = false;
            i13 = i10;
        }
        if (i11 > i15) {
            z4 = true;
        } else if (i11 < i14) {
            z4 = true;
            i15 = i14;
        } else {
            z4 = false;
            i15 = i11;
        }
        if (z4) {
            this.f1117c.a(i13, i15, 0, 0, 0, getScrollRange());
        }
        onOverScrolled(i13, i15, z3, z4);
        if (z3 || z4) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int getScrollRange() {
        if (getChildCount() > 0) {
            return Math.max(0, getChildAt(0).getHeight() - ((getHeight() - getPaddingBottom()) - getPaddingTop()));
        }
        return 0;
    }

    private View a(boolean z2, int i2, int i3) {
        boolean z3;
        View view;
        ArrayList focusables = getFocusables(2);
        View view2 = null;
        boolean z4 = false;
        int size = focusables.size();
        int i4 = 0;
        while (i4 < size) {
            View view3 = (View) focusables.get(i4);
            int top = view3.getTop();
            int bottom = view3.getBottom();
            if (i2 < bottom && top < i3) {
                boolean z5 = i2 < top && bottom < i3;
                if (view2 == null) {
                    boolean z6 = z5;
                    view = view3;
                    z3 = z6;
                } else {
                    boolean z7 = (z2 && top < view2.getTop()) || (!z2 && bottom > view2.getBottom());
                    if (z4) {
                        if (z5 && z7) {
                            view = view3;
                            z3 = z4;
                        }
                    } else if (z5) {
                        view = view3;
                        z3 = true;
                    } else if (z7) {
                        view = view3;
                        z3 = z4;
                    }
                }
                i4++;
                view2 = view;
                z4 = z3;
            }
            z3 = z4;
            view = view2;
            i4++;
            view2 = view;
            z4 = z3;
        }
        return view2;
    }

    public boolean a(int i2) {
        boolean z2 = i2 == 130;
        int height = getHeight();
        if (z2) {
            this.f1116b.top = getScrollY() + height;
            int childCount = getChildCount();
            if (childCount > 0) {
                View childAt = getChildAt(childCount - 1);
                if (this.f1116b.top + height > childAt.getBottom()) {
                    this.f1116b.top = childAt.getBottom() - height;
                }
            }
        } else {
            this.f1116b.top = getScrollY() - height;
            if (this.f1116b.top < 0) {
                this.f1116b.top = 0;
            }
        }
        this.f1116b.bottom = this.f1116b.top + height;
        return a(i2, this.f1116b.top, this.f1116b.bottom);
    }

    public boolean b(int i2) {
        int childCount;
        boolean z2 = i2 == 130;
        int height = getHeight();
        this.f1116b.top = 0;
        this.f1116b.bottom = height;
        if (z2 && (childCount = getChildCount()) > 0) {
            this.f1116b.bottom = getChildAt(childCount - 1).getBottom() + getPaddingBottom();
            this.f1116b.top = this.f1116b.bottom - height;
        }
        return a(i2, this.f1116b.top, this.f1116b.bottom);
    }

    private boolean a(int i2, int i3, int i4) {
        boolean z2 = false;
        int height = getHeight();
        int scrollY = getScrollY();
        int i5 = scrollY + height;
        boolean z3 = i2 == 33;
        View a2 = a(z3, i3, i4);
        if (a2 == null) {
            a2 = this;
        }
        if (i3 < scrollY || i4 > i5) {
            e(z3 ? i3 - scrollY : i4 - i5);
            z2 = true;
        }
        if (a2 != findFocus()) {
            a2.requestFocus(i2);
        }
        return z2;
    }

    public boolean c(int i2) {
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i2);
        int maxScrollAmount = getMaxScrollAmount();
        if (findNextFocus == null || !a(findNextFocus, maxScrollAmount, getHeight())) {
            if (i2 == 33 && getScrollY() < maxScrollAmount) {
                maxScrollAmount = getScrollY();
            } else if (i2 == 130 && getChildCount() > 0) {
                int bottom = getChildAt(0).getBottom();
                int scrollY = (getScrollY() + getHeight()) - getPaddingBottom();
                if (bottom - scrollY < maxScrollAmount) {
                    maxScrollAmount = bottom - scrollY;
                }
            }
            if (maxScrollAmount == 0) {
                return false;
            }
            if (i2 != 130) {
                maxScrollAmount = -maxScrollAmount;
            }
            e(maxScrollAmount);
        } else {
            findNextFocus.getDrawingRect(this.f1116b);
            offsetDescendantRectToMyCoords(findNextFocus, this.f1116b);
            e(a(this.f1116b));
            findNextFocus.requestFocus(i2);
        }
        if (findFocus != null && findFocus.isFocused() && a(findFocus)) {
            int descendantFocusability = getDescendantFocusability();
            setDescendantFocusability(131072);
            requestFocus();
            setDescendantFocusability(descendantFocusability);
        }
        return true;
    }

    private boolean a(View view) {
        return !a(view, 0, getHeight());
    }

    private boolean a(View view, int i2, int i3) {
        view.getDrawingRect(this.f1116b);
        offsetDescendantRectToMyCoords(view, this.f1116b);
        return this.f1116b.bottom + i2 >= getScrollY() && this.f1116b.top - i2 <= getScrollY() + i3;
    }

    private void e(int i2) {
        if (i2 == 0) {
            return;
        }
        if (this.m) {
            a(0, i2);
        } else {
            scrollBy(0, i2);
        }
    }

    public final void a(int i2, int i3) {
        if (getChildCount() != 0) {
            if (AnimationUtils.currentAnimationTimeMillis() - this.f1115a > 250) {
                int max = Math.max(0, getChildAt(0).getHeight() - ((getHeight() - getPaddingBottom()) - getPaddingTop()));
                int scrollY = getScrollY();
                this.f1117c.a(getScrollX(), scrollY, 0, Math.max(0, Math.min(scrollY + i3, max)) - scrollY);
                ag.c(this);
            } else {
                if (!this.f1117c.a()) {
                    this.f1117c.h();
                }
                scrollBy(i2, i3);
            }
            this.f1115a = AnimationUtils.currentAnimationTimeMillis();
        }
    }

    public final void b(int i2, int i3) {
        a(i2 - getScrollX(), i3 - getScrollY());
    }

    public int computeVerticalScrollRange() {
        int childCount = getChildCount();
        int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
        if (childCount == 0) {
            return height;
        }
        int bottom = getChildAt(0).getBottom();
        int scrollY = getScrollY();
        int max = Math.max(0, bottom - height);
        if (scrollY < 0) {
            return bottom - scrollY;
        }
        if (scrollY > max) {
            return bottom + (scrollY - max);
        }
        return bottom;
    }

    public int computeVerticalScrollOffset() {
        return Math.max(0, super.computeVerticalScrollOffset());
    }

    public int computeVerticalScrollExtent() {
        return super.computeVerticalScrollExtent();
    }

    public int computeHorizontalScrollRange() {
        return super.computeHorizontalScrollRange();
    }

    public int computeHorizontalScrollOffset() {
        return super.computeHorizontalScrollOffset();
    }

    public int computeHorizontalScrollExtent() {
        return super.computeHorizontalScrollExtent();
    }

    /* access modifiers changed from: protected */
    public void measureChild(View view, int i2, int i3) {
        view.measure(getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight(), view.getLayoutParams().width), View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    /* access modifiers changed from: protected */
    public void measureChildWithMargins(View view, int i2, int i3, int i4, int i5) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        view.measure(getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i3, marginLayoutParams.width), View.MeasureSpec.makeMeasureSpec(marginLayoutParams.bottomMargin + marginLayoutParams.topMargin, 0));
    }

    public void computeScroll() {
        if (this.f1117c.g()) {
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int b2 = this.f1117c.b();
            int c2 = this.f1117c.c();
            if (scrollX != b2 || scrollY != c2) {
                int scrollRange = getScrollRange();
                int overScrollMode = getOverScrollMode();
                boolean z2 = overScrollMode == 0 || (overScrollMode == 1 && scrollRange > 0);
                a(b2 - scrollX, c2 - scrollY, scrollX, scrollY, 0, scrollRange, 0, 0, false);
                if (z2) {
                    g();
                    if (c2 <= 0 && scrollY > 0) {
                        this.f1118d.a((int) this.f1117c.f());
                    } else if (c2 >= scrollRange && scrollY < scrollRange) {
                        this.f1119e.a((int) this.f1117c.f());
                    }
                }
            }
        }
    }

    private void b(View view) {
        view.getDrawingRect(this.f1116b);
        offsetDescendantRectToMyCoords(view, this.f1116b);
        int a2 = a(this.f1116b);
        if (a2 != 0) {
            scrollBy(0, a2);
        }
    }

    private boolean a(Rect rect, boolean z2) {
        int a2 = a(rect);
        boolean z3 = a2 != 0;
        if (z3) {
            if (z2) {
                scrollBy(0, a2);
            } else {
                a(0, a2);
            }
        }
        return z3;
    }

    /* access modifiers changed from: protected */
    public int a(Rect rect) {
        int i2;
        int i3;
        int i4;
        if (getChildCount() == 0) {
            return 0;
        }
        int height = getHeight();
        int scrollY = getScrollY();
        int i5 = scrollY + height;
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        if (rect.top > 0) {
            scrollY += verticalFadingEdgeLength;
        }
        if (rect.bottom < getChildAt(0).getHeight()) {
            i5 -= verticalFadingEdgeLength;
        }
        if (rect.bottom > i5 && rect.top > scrollY) {
            if (rect.height() > height) {
                i4 = (rect.top - scrollY) + 0;
            } else {
                i4 = (rect.bottom - i5) + 0;
            }
            i2 = Math.min(i4, getChildAt(0).getBottom() - i5);
        } else if (rect.top >= scrollY || rect.bottom >= i5) {
            i2 = 0;
        } else {
            if (rect.height() > height) {
                i3 = 0 - (i5 - rect.bottom);
            } else {
                i3 = 0 - (scrollY - rect.top);
            }
            i2 = Math.max(i3, -getScrollY());
        }
        return i2;
    }

    public void requestChildFocus(View view, View view2) {
        if (!this.f1121g) {
            b(view2);
        } else {
            this.i = view2;
        }
        super.requestChildFocus(view, view2);
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        View findNextFocusFromRect;
        if (i2 == 2) {
            i2 = 130;
        } else if (i2 == 1) {
            i2 = 33;
        }
        if (rect == null) {
            findNextFocusFromRect = FocusFinder.getInstance().findNextFocus(this, null, i2);
        } else {
            findNextFocusFromRect = FocusFinder.getInstance().findNextFocusFromRect(this, rect, i2);
        }
        if (findNextFocusFromRect != null && !a(findNextFocusFromRect)) {
            return findNextFocusFromRect.requestFocus(i2, rect);
        }
        return false;
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z2) {
        rect.offset(view.getLeft() - view.getScrollX(), view.getTop() - view.getScrollY());
        return a(rect, z2);
    }

    public void requestLayout() {
        this.f1121g = true;
        super.requestLayout();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        this.f1121g = false;
        if (this.i != null && a(this.i, this)) {
            b(this.i);
        }
        this.i = null;
        if (!this.f1122h) {
            if (this.u != null) {
                scrollTo(getScrollX(), this.u.f1123a);
                this.u = null;
            }
            int max = Math.max(0, (getChildCount() > 0 ? getChildAt(0).getMeasuredHeight() : 0) - (((i5 - i3) - getPaddingBottom()) - getPaddingTop()));
            if (getScrollY() > max) {
                scrollTo(getScrollX(), max);
            } else if (getScrollY() < 0) {
                scrollTo(getScrollX(), 0);
            }
        }
        scrollTo(getScrollX(), getScrollY());
        this.f1122h = true;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f1122h = false;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        View findFocus = findFocus();
        if (findFocus != null && this != findFocus && a(findFocus, 0, i5)) {
            findFocus.getDrawingRect(this.f1116b);
            offsetDescendantRectToMyCoords(findFocus, this.f1116b);
            e(a(this.f1116b));
        }
    }

    private static boolean a(View view, View view2) {
        if (view == view2) {
            return true;
        }
        ViewParent parent = view.getParent();
        return (parent instanceof ViewGroup) && a((View) parent, view2);
    }

    public void d(int i2) {
        if (getChildCount() > 0) {
            int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
            int i3 = height / 2;
            int i4 = i2;
            this.f1117c.a(getScrollX(), getScrollY(), 0, i4, 0, 0, 0, Math.max(0, getChildAt(0).getHeight() - height), 0, i3);
            ag.c(this);
        }
    }

    private void f(int i2) {
        int scrollY = getScrollY();
        boolean z2 = (scrollY > 0 || i2 > 0) && (scrollY < getScrollRange() || i2 < 0);
        if (!dispatchNestedPreFling(0.0f, (float) i2)) {
            dispatchNestedFling(0.0f, (float) i2, z2);
            if (z2) {
                d(i2);
            }
        }
    }

    private void f() {
        this.j = false;
        e();
        stopNestedScroll();
        if (this.f1118d != null) {
            this.f1118d.c();
            this.f1119e.c();
        }
    }

    public void scrollTo(int i2, int i3) {
        if (getChildCount() > 0) {
            View childAt = getChildAt(0);
            int b2 = b(i2, (getWidth() - getPaddingRight()) - getPaddingLeft(), childAt.getWidth());
            int b3 = b(i3, (getHeight() - getPaddingBottom()) - getPaddingTop(), childAt.getHeight());
            if (b2 != getScrollX() || b3 != getScrollY()) {
                super.scrollTo(b2, b3);
            }
        }
    }

    private void g() {
        if (getOverScrollMode() == 2) {
            this.f1118d = null;
            this.f1119e = null;
        } else if (this.f1118d == null) {
            Context context = getContext();
            this.f1118d = new EdgeEffectCompat(context);
            this.f1119e = new EdgeEffectCompat(context);
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f1118d != null) {
            int scrollY = getScrollY();
            if (!this.f1118d.a()) {
                int save = canvas.save();
                int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
                canvas.translate((float) getPaddingLeft(), (float) Math.min(0, scrollY));
                this.f1118d.a(width, getHeight());
                if (this.f1118d.a(canvas)) {
                    ag.c(this);
                }
                canvas.restoreToCount(save);
            }
            if (!this.f1119e.a()) {
                int save2 = canvas.save();
                int width2 = (getWidth() - getPaddingLeft()) - getPaddingRight();
                int height = getHeight();
                canvas.translate((float) ((-width2) + getPaddingLeft()), (float) (Math.max(getScrollRange(), scrollY) + height));
                canvas.rotate(180.0f, (float) width2, 0.0f);
                this.f1119e.a(width2, height);
                if (this.f1119e.a(canvas)) {
                    ag.c(this);
                }
                canvas.restoreToCount(save2);
            }
        }
    }

    private static int b(int i2, int i3, int i4) {
        if (i3 >= i4 || i2 < 0) {
            return 0;
        }
        if (i3 + i2 > i4) {
            return i4 - i3;
        }
        return i2;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.u = savedState;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1123a = getScrollY();
        return savedState;
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        public int f1123a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        SavedState(Parcel parcel) {
            super(parcel);
            this.f1123a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f1123a);
        }

        public String toString() {
            return "HorizontalScrollView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " scrollPosition=" + this.f1123a + "}";
        }
    }

    static class a extends android.support.v4.view.a {
        a() {
        }

        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            if (super.performAccessibilityAction(view, i, bundle)) {
                return true;
            }
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            if (!nestedScrollView.isEnabled()) {
                return false;
            }
            switch (i) {
                case CodedOutputStream.DEFAULT_BUFFER_SIZE /*4096*/:
                    int min = Math.min(((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()) + nestedScrollView.getScrollY(), nestedScrollView.getScrollRange());
                    if (min == nestedScrollView.getScrollY()) {
                        return false;
                    }
                    nestedScrollView.b(0, min);
                    return true;
                case 8192:
                    int max = Math.max(nestedScrollView.getScrollY() - ((nestedScrollView.getHeight() - nestedScrollView.getPaddingBottom()) - nestedScrollView.getPaddingTop()), 0);
                    if (max == nestedScrollView.getScrollY()) {
                        return false;
                    }
                    nestedScrollView.b(0, max);
                    return true;
                default:
                    return false;
            }
        }

        public void onInitializeAccessibilityNodeInfo(View view, e eVar) {
            int scrollRange;
            super.onInitializeAccessibilityNodeInfo(view, eVar);
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            eVar.b((CharSequence) ScrollView.class.getName());
            if (nestedScrollView.isEnabled() && (scrollRange = nestedScrollView.getScrollRange()) > 0) {
                eVar.k(true);
                if (nestedScrollView.getScrollY() > 0) {
                    eVar.a(8192);
                }
                if (nestedScrollView.getScrollY() < scrollRange) {
                    eVar.a((int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
                }
            }
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            NestedScrollView nestedScrollView = (NestedScrollView) view;
            accessibilityEvent.setClassName(ScrollView.class.getName());
            q a2 = android.support.v4.view.a.a.a(accessibilityEvent);
            a2.d(nestedScrollView.getScrollRange() > 0);
            a2.d(nestedScrollView.getScrollX());
            a2.e(nestedScrollView.getScrollY());
            a2.f(nestedScrollView.getScrollX());
            a2.g(nestedScrollView.getScrollRange());
        }
    }
}
