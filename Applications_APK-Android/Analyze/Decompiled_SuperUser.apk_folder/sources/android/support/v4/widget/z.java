package android.support.v4.widget;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.TextView;
import java.lang.reflect.Field;

@TargetApi(9)
/* compiled from: TextViewCompatGingerbread */
class z {

    /* renamed from: a  reason: collision with root package name */
    private static Field f1228a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f1229b;

    /* renamed from: c  reason: collision with root package name */
    private static Field f1230c;

    /* renamed from: d  reason: collision with root package name */
    private static boolean f1231d;

    static int a(TextView textView) {
        if (!f1231d) {
            f1230c = a("mMaxMode");
            f1231d = true;
        }
        if (f1230c != null && a(f1230c, textView) == 1) {
            if (!f1229b) {
                f1228a = a("mMaximum");
                f1229b = true;
            }
            if (f1228a != null) {
                return a(f1228a, textView);
            }
        }
        return -1;
    }

    private static Field a(String str) {
        Field field = null;
        try {
            field = TextView.class.getDeclaredField(str);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException e2) {
            Log.e("TextViewCompatGingerbread", "Could not retrieve " + str + " field.");
            return field;
        }
    }

    private static int a(Field field, TextView textView) {
        try {
            return field.getInt(textView);
        } catch (IllegalAccessException e2) {
            Log.d("TextViewCompatGingerbread", "Could not retrieve value of " + field.getName() + " field.");
            return -1;
        }
    }

    static void a(TextView textView, int i) {
        textView.setTextAppearance(textView.getContext(), i);
    }

    static Drawable[] b(TextView textView) {
        return textView.getCompoundDrawables();
    }
}
