package android.support.v4.widget;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.TextView;

/* compiled from: TextViewCompat */
public final class x {

    /* renamed from: a  reason: collision with root package name */
    static final f f1227a;

    /* compiled from: TextViewCompat */
    interface f {
        int a(TextView textView);

        void a(TextView textView, int i);

        void a(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4);

        Drawable[] b(TextView textView);
    }

    /* compiled from: TextViewCompat */
    static class b implements f {
        b() {
        }

        public void a(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
            textView.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        }

        public int a(TextView textView) {
            return z.a(textView);
        }

        public void a(TextView textView, int i) {
            z.a(textView, i);
        }

        public Drawable[] b(TextView textView) {
            return z.b(textView);
        }
    }

    /* compiled from: TextViewCompat */
    static class e extends b {
        e() {
        }

        public int a(TextView textView) {
            return aa.a(textView);
        }
    }

    /* compiled from: TextViewCompat */
    static class c extends e {
        c() {
        }

        public void a(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
            ab.a(textView, drawable, drawable2, drawable3, drawable4);
        }

        public Drawable[] b(TextView textView) {
            return ab.a(textView);
        }
    }

    /* compiled from: TextViewCompat */
    static class d extends c {
        d() {
        }

        public void a(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
            ac.a(textView, drawable, drawable2, drawable3, drawable4);
        }

        public Drawable[] b(TextView textView) {
            return ac.a(textView);
        }
    }

    /* compiled from: TextViewCompat */
    static class a extends d {
        a() {
        }

        public void a(TextView textView, int i) {
            y.a(textView, i);
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            f1227a = new a();
        } else if (i >= 18) {
            f1227a = new d();
        } else if (i >= 17) {
            f1227a = new c();
        } else if (i >= 16) {
            f1227a = new e();
        } else {
            f1227a = new b();
        }
    }

    public static void a(TextView textView, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        f1227a.a(textView, drawable, drawable2, drawable3, drawable4);
    }

    public static int a(TextView textView) {
        return f1227a.a(textView);
    }

    public static void a(TextView textView, int i) {
        f1227a.a(textView, i);
    }

    public static Drawable[] b(TextView textView) {
        return f1227a.b(textView);
    }
}
