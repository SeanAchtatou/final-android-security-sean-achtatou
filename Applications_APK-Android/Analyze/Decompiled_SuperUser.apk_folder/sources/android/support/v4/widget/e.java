package android.support.v4.widget;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.CompoundButton;
import java.lang.reflect.Field;

@TargetApi(9)
/* compiled from: CompoundButtonCompatGingerbread */
class e {

    /* renamed from: a  reason: collision with root package name */
    private static Field f1195a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f1196b;

    static void a(CompoundButton compoundButton, ColorStateList colorStateList) {
        if (compoundButton instanceof ad) {
            ((ad) compoundButton).setSupportButtonTintList(colorStateList);
        }
    }

    static void a(CompoundButton compoundButton, PorterDuff.Mode mode) {
        if (compoundButton instanceof ad) {
            ((ad) compoundButton).setSupportButtonTintMode(mode);
        }
    }

    static Drawable a(CompoundButton compoundButton) {
        if (!f1196b) {
            try {
                f1195a = CompoundButton.class.getDeclaredField("mButtonDrawable");
                f1195a.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.i("CompoundButtonCompatGingerbread", "Failed to retrieve mButtonDrawable field", e2);
            }
            f1196b = true;
        }
        if (f1195a != null) {
            try {
                return (Drawable) f1195a.get(compoundButton);
            } catch (IllegalAccessException e3) {
                Log.i("CompoundButtonCompatGingerbread", "Failed to get button drawable via reflection", e3);
                f1195a = null;
            }
        }
        return null;
    }
}
