package android.support.v4.widget;

import android.annotation.TargetApi;
import android.widget.TextView;

@TargetApi(23)
/* compiled from: TextViewCompatApi23 */
class y {
    public static void a(TextView textView, int i) {
        textView.setTextAppearance(i);
    }
}
