package android.support.v4.widget;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SimpleCursorAdapter extends ResourceCursorAdapter {
    protected int[] j;
    protected int[] k;
    String[] l;
    private int m;
    private a n;
    private b o;

    public interface a {
        CharSequence a(Cursor cursor);
    }

    public interface b {
        boolean a(View view, Cursor cursor, int i);
    }

    public void a(View view, Context context, Cursor cursor) {
        boolean z;
        b bVar = this.o;
        int length = this.k.length;
        int[] iArr = this.j;
        int[] iArr2 = this.k;
        for (int i = 0; i < length; i++) {
            View findViewById = view.findViewById(iArr2[i]);
            if (findViewById != null) {
                if (bVar != null) {
                    z = bVar.a(findViewById, cursor, iArr[i]);
                } else {
                    z = false;
                }
                if (z) {
                    continue;
                } else {
                    String string = cursor.getString(iArr[i]);
                    if (string == null) {
                        string = "";
                    }
                    if (findViewById instanceof TextView) {
                        a((TextView) findViewById, string);
                    } else if (findViewById instanceof ImageView) {
                        a((ImageView) findViewById, string);
                    } else {
                        throw new IllegalStateException(findViewById.getClass().getName() + " is not a " + " view that can be bounds by this SimpleCursorAdapter");
                    }
                }
            }
        }
    }

    public void a(ImageView imageView, String str) {
        try {
            imageView.setImageResource(Integer.parseInt(str));
        } catch (NumberFormatException e2) {
            imageView.setImageURI(Uri.parse(str));
        }
    }

    public void a(TextView textView, String str) {
        textView.setText(str);
    }

    public CharSequence c(Cursor cursor) {
        if (this.n != null) {
            return this.n.a(cursor);
        }
        if (this.m > -1) {
            return cursor.getString(this.m);
        }
        return super.c(cursor);
    }

    private void a(Cursor cursor, String[] strArr) {
        if (cursor != null) {
            int length = strArr.length;
            if (this.j == null || this.j.length != length) {
                this.j = new int[length];
            }
            for (int i = 0; i < length; i++) {
                this.j[i] = cursor.getColumnIndexOrThrow(strArr[i]);
            }
            return;
        }
        this.j = null;
    }

    public Cursor b(Cursor cursor) {
        a(cursor, this.l);
        return super.b(cursor);
    }
}
