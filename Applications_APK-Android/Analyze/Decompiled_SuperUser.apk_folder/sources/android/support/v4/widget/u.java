package android.support.v4.widget;

import android.content.Context;
import android.os.Build;
import android.view.animation.Interpolator;
import android.widget.OverScroller;

/* compiled from: ScrollerCompat */
public final class u {

    /* renamed from: a  reason: collision with root package name */
    OverScroller f1224a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f1225b;

    public static u a(Context context) {
        return a(context, null);
    }

    public static u a(Context context, Interpolator interpolator) {
        return new u(Build.VERSION.SDK_INT >= 14, context, interpolator);
    }

    u(boolean z, Context context, Interpolator interpolator) {
        this.f1225b = z;
        this.f1224a = interpolator != null ? new OverScroller(context, interpolator) : new OverScroller(context);
    }

    public boolean a() {
        return this.f1224a.isFinished();
    }

    public int b() {
        return this.f1224a.getCurrX();
    }

    public int c() {
        return this.f1224a.getCurrY();
    }

    public int d() {
        return this.f1224a.getFinalX();
    }

    public int e() {
        return this.f1224a.getFinalY();
    }

    public float f() {
        if (this.f1225b) {
            return v.a(this.f1224a);
        }
        return 0.0f;
    }

    public boolean g() {
        return this.f1224a.computeScrollOffset();
    }

    public void a(int i, int i2, int i3, int i4) {
        this.f1224a.startScroll(i, i2, i3, i4);
    }

    public void a(int i, int i2, int i3, int i4, int i5) {
        this.f1224a.startScroll(i, i2, i3, i4, i5);
    }

    public void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.f1224a.fling(i, i2, i3, i4, i5, i6, i7, i8);
    }

    public void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        this.f1224a.fling(i, i2, i3, i4, i5, i6, i7, i8, i9, i10);
    }

    public boolean a(int i, int i2, int i3, int i4, int i5, int i6) {
        return this.f1224a.springBack(i, i2, i3, i4, i5, i6);
    }

    public void h() {
        this.f1224a.abortAnimation();
    }
}
