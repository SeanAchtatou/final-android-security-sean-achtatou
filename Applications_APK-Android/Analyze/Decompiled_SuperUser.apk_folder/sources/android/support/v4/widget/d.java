package android.support.v4.widget;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.widget.CompoundButton;

@TargetApi(23)
/* compiled from: CompoundButtonCompatApi23 */
class d {
    static Drawable a(CompoundButton compoundButton) {
        return compoundButton.getButtonDrawable();
    }
}
