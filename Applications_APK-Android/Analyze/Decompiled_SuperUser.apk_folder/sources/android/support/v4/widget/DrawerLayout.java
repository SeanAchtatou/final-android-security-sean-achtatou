package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.a.e;
import android.support.v4.view.ag;
import android.support.v4.view.at;
import android.support.v4.view.s;
import android.support.v4.widget.ae;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import com.kingouser.com.util.SystemBarTintManager;
import com.lody.virtual.os.VUserInfo;
import java.util.ArrayList;
import java.util.List;

public class DrawerLayout extends ViewGroup implements i {

    /* renamed from: a  reason: collision with root package name */
    static final int[] f1078a = {16842931};

    /* renamed from: b  reason: collision with root package name */
    static final boolean f1079b = (Build.VERSION.SDK_INT >= 19);

    /* renamed from: c  reason: collision with root package name */
    static final c f1080c;

    /* renamed from: d  reason: collision with root package name */
    private static final boolean f1081d;
    private float A;
    private Drawable B;
    private Drawable C;
    private Drawable D;
    private CharSequence E;
    private CharSequence F;
    private Object G;
    private boolean H;
    private Drawable I;
    private Drawable J;
    private Drawable K;
    private Drawable L;
    private final ArrayList<View> M;

    /* renamed from: e  reason: collision with root package name */
    private final b f1082e;

    /* renamed from: f  reason: collision with root package name */
    private float f1083f;

    /* renamed from: g  reason: collision with root package name */
    private int f1084g;

    /* renamed from: h  reason: collision with root package name */
    private int f1085h;
    private float i;
    private Paint j;
    private final ae k;
    private final ae l;
    private final g m;
    private final g n;
    private int o;
    private boolean p;
    private boolean q;
    private int r;
    private int s;
    private int t;
    private int u;
    private boolean v;
    private boolean w;
    private f x;
    private List<f> y;
    private float z;

    interface c {
        int a(Object obj);

        Drawable a(Context context);

        void a(View view);

        void a(View view, Object obj, int i);

        void a(ViewGroup.MarginLayoutParams marginLayoutParams, Object obj, int i);
    }

    public interface f {
        void a(int i);

        void a(View view);

        void a(View view, float f2);

        void b(View view);
    }

    static {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT < 21) {
            z2 = false;
        }
        f1081d = z2;
        if (Build.VERSION.SDK_INT >= 21) {
            f1080c = new d();
        } else {
            f1080c = new e();
        }
    }

    public static abstract class SimpleDrawerListener implements f {
        public void a(View view, float f2) {
        }

        public void a(View view) {
        }

        public void b(View view) {
        }

        public void a(int i) {
        }
    }

    static class e implements c {
        e() {
        }

        public void a(View view) {
        }

        public void a(View view, Object obj, int i) {
        }

        public void a(ViewGroup.MarginLayoutParams marginLayoutParams, Object obj, int i) {
        }

        public int a(Object obj) {
            return 0;
        }

        public Drawable a(Context context) {
            return null;
        }
    }

    static class d implements c {
        d() {
        }

        public void a(View view) {
            h.a(view);
        }

        public void a(View view, Object obj, int i) {
            h.a(view, obj, i);
        }

        public void a(ViewGroup.MarginLayoutParams marginLayoutParams, Object obj, int i) {
            h.a(marginLayoutParams, obj, i);
        }

        public int a(Object obj) {
            return h.a(obj);
        }

        public Drawable a(Context context) {
            return h.a(context);
        }
    }

    public DrawerLayout(Context context) {
        this(context, null);
    }

    public DrawerLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, int):void
     arg types: [android.support.v4.widget.DrawerLayout, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, float):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, int):void */
    public DrawerLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1082e = new b();
        this.f1085h = SystemBarTintManager.DEFAULT_TINT_COLOR;
        this.j = new Paint();
        this.q = true;
        this.r = 3;
        this.s = 3;
        this.t = 3;
        this.u = 3;
        this.I = null;
        this.J = null;
        this.K = null;
        this.L = null;
        setDescendantFocusability(262144);
        float f2 = getResources().getDisplayMetrics().density;
        this.f1084g = (int) ((64.0f * f2) + 0.5f);
        float f3 = 400.0f * f2;
        this.m = new g(3);
        this.n = new g(5);
        this.k = ae.a(this, 1.0f, this.m);
        this.k.a(1);
        this.k.a(f3);
        this.m.a(this.k);
        this.l = ae.a(this, 1.0f, this.n);
        this.l.a(2);
        this.l.a(f3);
        this.n.a(this.l);
        setFocusableInTouchMode(true);
        ag.c((View) this, 1);
        ag.a(this, new a());
        at.a(this, false);
        if (ag.x(this)) {
            f1080c.a((View) this);
            this.B = f1080c.a(context);
        }
        this.f1083f = f2 * 10.0f;
        this.M = new ArrayList<>();
    }

    public void setDrawerElevation(float f2) {
        this.f1083f = f2;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (g(childAt)) {
                ag.h(childAt, this.f1083f);
            }
        }
    }

    public float getDrawerElevation() {
        if (f1081d) {
            return this.f1083f;
        }
        return 0.0f;
    }

    public void a(Object obj, boolean z2) {
        this.G = obj;
        this.H = z2;
        setWillNotDraw(!z2 && getBackground() == null);
        requestLayout();
    }

    public void setScrimColor(int i2) {
        this.f1085h = i2;
        invalidate();
    }

    @Deprecated
    public void setDrawerListener(f fVar) {
        if (this.x != null) {
            b(this.x);
        }
        if (fVar != null) {
            a(fVar);
        }
        this.x = fVar;
    }

    public void a(f fVar) {
        if (fVar != null) {
            if (this.y == null) {
                this.y = new ArrayList();
            }
            this.y.add(fVar);
        }
    }

    public void b(f fVar) {
        if (fVar != null && this.y != null) {
            this.y.remove(fVar);
        }
    }

    public void setDrawerLockMode(int i2) {
        a(i2, 3);
        a(i2, 5);
    }

    public void a(int i2, int i3) {
        int a2 = android.support.v4.view.e.a(i3, ag.g(this));
        switch (i3) {
            case 3:
                this.r = i2;
                break;
            case 5:
                this.s = i2;
                break;
            case 8388611:
                this.t = i2;
                break;
            case 8388613:
                this.u = i2;
                break;
        }
        if (i2 != 0) {
            (a2 == 3 ? this.k : this.l).e();
        }
        switch (i2) {
            case 1:
                View c2 = c(a2);
                if (c2 != null) {
                    i(c2);
                    return;
                }
                return;
            case 2:
                View c3 = c(a2);
                if (c3 != null) {
                    h(c3);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public int a(int i2) {
        int g2 = ag.g(this);
        switch (i2) {
            case 3:
                if (this.r != 3) {
                    return this.r;
                }
                int i3 = g2 == 0 ? this.t : this.u;
                if (i3 != 3) {
                    return i3;
                }
                break;
            case 5:
                if (this.s != 3) {
                    return this.s;
                }
                int i4 = g2 == 0 ? this.u : this.t;
                if (i4 != 3) {
                    return i4;
                }
                break;
            case 8388611:
                if (this.t != 3) {
                    return this.t;
                }
                int i5 = g2 == 0 ? this.r : this.s;
                if (i5 != 3) {
                    return i5;
                }
                break;
            case 8388613:
                if (this.u != 3) {
                    return this.u;
                }
                int i6 = g2 == 0 ? this.s : this.r;
                if (i6 != 3) {
                    return i6;
                }
                break;
        }
        return 0;
    }

    public int a(View view) {
        if (g(view)) {
            return a(((LayoutParams) view.getLayoutParams()).f1086a);
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public CharSequence b(int i2) {
        int a2 = android.support.v4.view.e.a(i2, ag.g(this));
        if (a2 == 3) {
            return this.E;
        }
        if (a2 == 5) {
            return this.F;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, View view) {
        int i4;
        int a2 = this.k.a();
        int a3 = this.l.a();
        if (a2 == 1 || a3 == 1) {
            i4 = 1;
        } else if (a2 == 2 || a3 == 2) {
            i4 = 2;
        } else {
            i4 = 0;
        }
        if (view != null && i3 == 0) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (layoutParams.f1087b == 0.0f) {
                b(view);
            } else if (layoutParams.f1087b == 1.0f) {
                c(view);
            }
        }
        if (i4 != this.o) {
            this.o = i4;
            if (this.y != null) {
                for (int size = this.y.size() - 1; size >= 0; size--) {
                    this.y.get(size).a(i4);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.c(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.c(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.c(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(View view) {
        View rootView;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if ((layoutParams.f1089d & 1) == 1) {
            layoutParams.f1089d = 0;
            if (this.y != null) {
                for (int size = this.y.size() - 1; size >= 0; size--) {
                    this.y.get(size).b(view);
                }
            }
            c(view, false);
            if (hasWindowFocus() && (rootView = getRootView()) != null) {
                rootView.sendAccessibilityEvent(32);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.c(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.c(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.c(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void c(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if ((layoutParams.f1089d & 1) == 0) {
            layoutParams.f1089d = 1;
            if (this.y != null) {
                for (int size = this.y.size() - 1; size >= 0; size--) {
                    this.y.get(size).a(view);
                }
            }
            c(view, true);
            if (hasWindowFocus()) {
                sendAccessibilityEvent(32);
            }
        }
    }

    private void c(View view, boolean z2) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((z2 || g(childAt)) && (!z2 || childAt != view)) {
                ag.c(childAt, 4);
            } else {
                ag.c(childAt, 1);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, float f2) {
        if (this.y != null) {
            for (int size = this.y.size() - 1; size >= 0; size--) {
                this.y.get(size).a(view, f2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(View view, float f2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f2 != layoutParams.f1087b) {
            layoutParams.f1087b = f2;
            a(view, f2);
        }
    }

    /* access modifiers changed from: package-private */
    public float d(View view) {
        return ((LayoutParams) view.getLayoutParams()).f1087b;
    }

    /* access modifiers changed from: package-private */
    public int e(View view) {
        return android.support.v4.view.e.a(((LayoutParams) view.getLayoutParams()).f1086a, ag.g(this));
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, int i2) {
        return (e(view) & i2) == i2;
    }

    /* access modifiers changed from: package-private */
    public View a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((((LayoutParams) childAt.getLayoutParams()).f1089d & 1) == 1) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void c(View view, float f2) {
        float d2 = d(view);
        int width = view.getWidth();
        int i2 = ((int) (((float) width) * f2)) - ((int) (d2 * ((float) width)));
        if (!a(view, 3)) {
            i2 = -i2;
        }
        view.offsetLeftAndRight(i2);
        b(view, f2);
    }

    /* access modifiers changed from: package-private */
    public View c(int i2) {
        int a2 = android.support.v4.view.e.a(i2, ag.g(this)) & 7;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if ((e(childAt) & 7) == a2) {
                return childAt;
            }
        }
        return null;
    }

    static String d(int i2) {
        if ((i2 & 3) == 3) {
            return "LEFT";
        }
        if ((i2 & 5) == 5) {
            return "RIGHT";
        }
        return Integer.toHexString(i2);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.q = true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.q = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            int r2 = android.view.View.MeasureSpec.getMode(r14)
            int r3 = android.view.View.MeasureSpec.getMode(r15)
            int r1 = android.view.View.MeasureSpec.getSize(r14)
            int r0 = android.view.View.MeasureSpec.getSize(r15)
            r4 = 1073741824(0x40000000, float:2.0)
            if (r2 != r4) goto L_0x0018
            r4 = 1073741824(0x40000000, float:2.0)
            if (r3 == r4) goto L_0x0169
        L_0x0018:
            boolean r4 = r13.isInEditMode()
            if (r4 == 0) goto L_0x0061
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r2 != r4) goto L_0x0055
        L_0x0022:
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r3 != r2) goto L_0x005a
            r2 = r1
            r1 = r0
        L_0x0028:
            r13.setMeasuredDimension(r2, r1)
            java.lang.Object r0 = r13.G
            if (r0 == 0) goto L_0x0069
            boolean r0 = android.support.v4.view.ag.x(r13)
            if (r0 == 0) goto L_0x0069
            r0 = 1
            r3 = r0
        L_0x0037:
            int r8 = android.support.v4.view.ag.g(r13)
            r5 = 0
            r4 = 0
            int r9 = r13.getChildCount()
            r0 = 0
            r7 = r0
        L_0x0043:
            if (r7 >= r9) goto L_0x0168
            android.view.View r10 = r13.getChildAt(r7)
            int r0 = r10.getVisibility()
            r6 = 8
            if (r0 != r6) goto L_0x006c
        L_0x0051:
            int r0 = r7 + 1
            r7 = r0
            goto L_0x0043
        L_0x0055:
            if (r2 != 0) goto L_0x0022
            r1 = 300(0x12c, float:4.2E-43)
            goto L_0x0022
        L_0x005a:
            if (r3 != 0) goto L_0x0169
            r0 = 300(0x12c, float:4.2E-43)
            r2 = r1
            r1 = r0
            goto L_0x0028
        L_0x0061:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "DrawerLayout must be measured with MeasureSpec.EXACTLY."
            r0.<init>(r1)
            throw r0
        L_0x0069:
            r0 = 0
            r3 = r0
            goto L_0x0037
        L_0x006c:
            android.view.ViewGroup$LayoutParams r0 = r10.getLayoutParams()
            android.support.v4.widget.DrawerLayout$LayoutParams r0 = (android.support.v4.widget.DrawerLayout.LayoutParams) r0
            if (r3 == 0) goto L_0x0087
            int r6 = r0.f1086a
            int r6 = android.support.v4.view.e.a(r6, r8)
            boolean r11 = android.support.v4.view.ag.x(r10)
            if (r11 == 0) goto L_0x00ac
            android.support.v4.widget.DrawerLayout$c r11 = android.support.v4.widget.DrawerLayout.f1080c
            java.lang.Object r12 = r13.G
            r11.a(r10, r12, r6)
        L_0x0087:
            boolean r6 = r13.f(r10)
            if (r6 == 0) goto L_0x00b4
            int r6 = r0.leftMargin
            int r6 = r2 - r6
            int r11 = r0.rightMargin
            int r6 = r6 - r11
            r11 = 1073741824(0x40000000, float:2.0)
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r6, r11)
            int r11 = r0.topMargin
            int r11 = r1 - r11
            int r0 = r0.bottomMargin
            int r0 = r11 - r0
            r11 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r11)
            r10.measure(r6, r0)
            goto L_0x0051
        L_0x00ac:
            android.support.v4.widget.DrawerLayout$c r11 = android.support.v4.widget.DrawerLayout.f1080c
            java.lang.Object r12 = r13.G
            r11.a(r0, r12, r6)
            goto L_0x0087
        L_0x00b4:
            boolean r6 = r13.g(r10)
            if (r6 == 0) goto L_0x0139
            boolean r6 = android.support.v4.widget.DrawerLayout.f1081d
            if (r6 == 0) goto L_0x00cd
            float r6 = android.support.v4.view.ag.t(r10)
            float r11 = r13.f1083f
            int r6 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r6 == 0) goto L_0x00cd
            float r6 = r13.f1083f
            android.support.v4.view.ag.h(r10, r6)
        L_0x00cd:
            int r6 = r13.e(r10)
            r11 = r6 & 7
            r6 = 3
            if (r11 != r6) goto L_0x0114
            r6 = 1
        L_0x00d7:
            if (r6 == 0) goto L_0x00db
            if (r5 != 0) goto L_0x00df
        L_0x00db:
            if (r6 != 0) goto L_0x0116
            if (r4 == 0) goto L_0x0116
        L_0x00df:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Child drawer has absolute gravity "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = d(r11)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " but this "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "DrawerLayout"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " already has a "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "drawer view along that edge"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0114:
            r6 = 0
            goto L_0x00d7
        L_0x0116:
            if (r6 == 0) goto L_0x0137
            r5 = 1
        L_0x0119:
            int r6 = r13.f1084g
            int r11 = r0.leftMargin
            int r6 = r6 + r11
            int r11 = r0.rightMargin
            int r6 = r6 + r11
            int r11 = r0.width
            int r6 = getChildMeasureSpec(r14, r6, r11)
            int r11 = r0.topMargin
            int r12 = r0.bottomMargin
            int r11 = r11 + r12
            int r0 = r0.height
            int r0 = getChildMeasureSpec(r15, r11, r0)
            r10.measure(r6, r0)
            goto L_0x0051
        L_0x0137:
            r4 = 1
            goto L_0x0119
        L_0x0139:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Child "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r2 = " at index "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r2 = " does not have a valid layout_gravity - must be Gravity.LEFT, "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "Gravity.RIGHT or Gravity.NO_GRAVITY"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0168:
            return
        L_0x0169:
            r2 = r1
            r1 = r0
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.onMeasure(int, int):void");
    }

    private void e() {
        if (!f1081d) {
            this.C = f();
            this.D = g();
        }
    }

    private Drawable f() {
        int g2 = ag.g(this);
        if (g2 == 0) {
            if (this.I != null) {
                a(this.I, g2);
                return this.I;
            }
        } else if (this.J != null) {
            a(this.J, g2);
            return this.J;
        }
        return this.K;
    }

    private Drawable g() {
        int g2 = ag.g(this);
        if (g2 == 0) {
            if (this.J != null) {
                a(this.J, g2);
                return this.J;
            }
        } else if (this.I != null) {
            a(this.I, g2);
            return this.I;
        }
        return this.L;
    }

    private boolean a(Drawable drawable, int i2) {
        if (drawable == null || !android.support.v4.b.a.a.b(drawable)) {
            return false;
        }
        android.support.v4.b.a.a.b(drawable, i2);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        float f2;
        this.p = true;
        int i7 = i4 - i2;
        int childCount = getChildCount();
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (f(childAt)) {
                    childAt.layout(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.leftMargin + childAt.getMeasuredWidth(), layoutParams.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (a(childAt, 3)) {
                        i6 = ((int) (((float) measuredWidth) * layoutParams.f1087b)) + (-measuredWidth);
                        f2 = ((float) (measuredWidth + i6)) / ((float) measuredWidth);
                    } else {
                        i6 = i7 - ((int) (((float) measuredWidth) * layoutParams.f1087b));
                        f2 = ((float) (i7 - i6)) / ((float) measuredWidth);
                    }
                    boolean z3 = f2 != layoutParams.f1087b;
                    switch (layoutParams.f1086a & 112) {
                        case 16:
                            int i9 = i5 - i3;
                            int i10 = (i9 - measuredHeight) / 2;
                            if (i10 < layoutParams.topMargin) {
                                i10 = layoutParams.topMargin;
                            } else if (i10 + measuredHeight > i9 - layoutParams.bottomMargin) {
                                i10 = (i9 - layoutParams.bottomMargin) - measuredHeight;
                            }
                            childAt.layout(i6, i10, measuredWidth + i6, measuredHeight + i10);
                            break;
                        case 80:
                            int i11 = i5 - i3;
                            childAt.layout(i6, (i11 - layoutParams.bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i6, i11 - layoutParams.bottomMargin);
                            break;
                        default:
                            childAt.layout(i6, layoutParams.topMargin, measuredWidth + i6, measuredHeight + layoutParams.topMargin);
                            break;
                    }
                    if (z3) {
                        b(childAt, f2);
                    }
                    int i12 = layoutParams.f1087b > 0.0f ? 0 : 4;
                    if (childAt.getVisibility() != i12) {
                        childAt.setVisibility(i12);
                    }
                }
            }
        }
        this.p = false;
        this.q = false;
    }

    public void requestLayout() {
        if (!this.p) {
            super.requestLayout();
        }
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f2 = 0.0f;
        for (int i2 = 0; i2 < childCount; i2++) {
            f2 = Math.max(f2, ((LayoutParams) getChildAt(i2).getLayoutParams()).f1087b);
        }
        this.i = f2;
        if (this.k.a(true) || this.l.a(true)) {
            ag.c(this);
        }
    }

    private static boolean m(View view) {
        Drawable background = view.getBackground();
        if (background == null || background.getOpacity() != -1) {
            return false;
        }
        return true;
    }

    public void setStatusBarBackground(Drawable drawable) {
        this.B = drawable;
        invalidate();
    }

    public Drawable getStatusBarBackgroundDrawable() {
        return this.B;
    }

    public void setStatusBarBackground(int i2) {
        this.B = i2 != 0 ? android.support.v4.content.c.a(getContext(), i2) : null;
        invalidate();
    }

    public void setStatusBarBackgroundColor(int i2) {
        this.B = new ColorDrawable(i2);
        invalidate();
    }

    public void onRtlPropertiesChanged(int i2) {
        e();
    }

    public void onDraw(Canvas canvas) {
        int a2;
        super.onDraw(canvas);
        if (this.H && this.B != null && (a2 = f1080c.a(this.G)) > 0) {
            this.B.setBounds(0, 0, getWidth(), a2);
            this.B.draw(canvas);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        int i2;
        int height = getHeight();
        boolean f2 = f(view);
        int i3 = 0;
        int width = getWidth();
        int save = canvas.save();
        if (f2) {
            int childCount = getChildCount();
            int i4 = 0;
            while (i4 < childCount) {
                View childAt = getChildAt(i4);
                if (childAt != view && childAt.getVisibility() == 0 && m(childAt) && g(childAt)) {
                    if (childAt.getHeight() < height) {
                        i2 = width;
                    } else if (a(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right <= i3) {
                            right = i3;
                        }
                        i3 = right;
                        i2 = width;
                    } else {
                        i2 = childAt.getLeft();
                        if (i2 < width) {
                        }
                    }
                    i4++;
                    width = i2;
                }
                i2 = width;
                i4++;
                width = i2;
            }
            canvas.clipRect(i3, 0, width, getHeight());
        }
        int i5 = width;
        boolean drawChild = super.drawChild(canvas, view, j2);
        canvas.restoreToCount(save);
        if (this.i > 0.0f && f2) {
            this.j.setColor((((int) (((float) ((this.f1085h & -16777216) >>> 24)) * this.i)) << 24) | (this.f1085h & 16777215));
            canvas.drawRect((float) i3, 0.0f, (float) i5, (float) getHeight(), this.j);
        } else if (this.C != null && a(view, 3)) {
            int intrinsicWidth = this.C.getIntrinsicWidth();
            int right2 = view.getRight();
            float max = Math.max(0.0f, Math.min(((float) right2) / ((float) this.k.b()), 1.0f));
            this.C.setBounds(right2, view.getTop(), intrinsicWidth + right2, view.getBottom());
            this.C.setAlpha((int) (255.0f * max));
            this.C.draw(canvas);
        } else if (this.D != null && a(view, 5)) {
            int intrinsicWidth2 = this.D.getIntrinsicWidth();
            int left = view.getLeft();
            float max2 = Math.max(0.0f, Math.min(((float) (getWidth() - left)) / ((float) this.l.b()), 1.0f));
            this.D.setBounds(left - intrinsicWidth2, view.getTop(), left, view.getBottom());
            this.D.setAlpha((int) (255.0f * max2));
            this.D.draw(canvas);
        }
        return drawChild;
    }

    /* access modifiers changed from: package-private */
    public boolean f(View view) {
        return ((LayoutParams) view.getLayoutParams()).f1086a == 0;
    }

    /* access modifiers changed from: package-private */
    public boolean g(View view) {
        int a2 = android.support.v4.view.e.a(((LayoutParams) view.getLayoutParams()).f1086a, ag.g(view));
        if ((a2 & 3) != 0) {
            return true;
        }
        if ((a2 & 5) != 0) {
            return true;
        }
        return false;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        View d2;
        int a2 = s.a(motionEvent);
        boolean a3 = this.k.a(motionEvent) | this.l.a(motionEvent);
        switch (a2) {
            case 0:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                this.z = x2;
                this.A = y2;
                if (this.i <= 0.0f || (d2 = this.k.d((int) x2, (int) y2)) == null || !f(d2)) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                this.v = false;
                this.w = false;
                break;
            case 1:
            case 3:
                a(true);
                this.v = false;
                this.w = false;
                z2 = false;
                break;
            case 2:
                if (this.k.d(3)) {
                    this.m.a();
                    this.n.a();
                    z2 = false;
                    break;
                }
                z2 = false;
                break;
            default:
                z2 = false;
                break;
        }
        if (a3 || z2 || h() || this.w) {
            return true;
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        View a2;
        this.k.b(motionEvent);
        this.l.b(motionEvent);
        switch (motionEvent.getAction() & VUserInfo.FLAG_MASK_USER_TYPE) {
            case 0:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                this.z = x2;
                this.A = y2;
                this.v = false;
                this.w = false;
                break;
            case 1:
                float x3 = motionEvent.getX();
                float y3 = motionEvent.getY();
                View d2 = this.k.d((int) x3, (int) y3);
                if (d2 != null && f(d2)) {
                    float f2 = x3 - this.z;
                    float f3 = y3 - this.A;
                    int d3 = this.k.d();
                    if ((f2 * f2) + (f3 * f3) < ((float) (d3 * d3)) && (a2 = a()) != null) {
                        z2 = a(a2) == 2;
                        a(z2);
                        this.v = false;
                        break;
                    }
                }
                z2 = true;
                a(z2);
                this.v = false;
            case 3:
                a(true);
                this.v = false;
                this.w = false;
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        super.requestDisallowInterceptTouchEvent(z2);
        this.v = z2;
        if (z2) {
            a(true);
        }
    }

    public void b() {
        a(false);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        int childCount = getChildCount();
        boolean z3 = false;
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (g(childAt) && (!z2 || layoutParams.f1088c)) {
                int width = childAt.getWidth();
                if (a(childAt, 3)) {
                    z3 |= this.k.a(childAt, -width, childAt.getTop());
                } else {
                    z3 |= this.l.a(childAt, getWidth(), childAt.getTop());
                }
                layoutParams.f1088c = false;
            }
        }
        this.m.a();
        this.n.a();
        if (z3) {
            invalidate();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.a(android.graphics.drawable.Drawable, int):boolean
      android.support.v4.widget.DrawerLayout.a(int, int):void
      android.support.v4.widget.DrawerLayout.a(int, boolean):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.a(java.lang.Object, boolean):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, int):boolean
      android.support.v4.widget.i.a(java.lang.Object, boolean):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void */
    public void h(View view) {
        a(view, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.c(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.c(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.c(android.view.View, boolean):void */
    public void a(View view, boolean z2) {
        if (!g(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (this.q) {
            layoutParams.f1087b = 1.0f;
            layoutParams.f1089d = 1;
            c(view, true);
        } else if (z2) {
            layoutParams.f1089d |= 2;
            if (a(view, 3)) {
                this.k.a(view, 0, view.getTop());
            } else {
                this.l.a(view, getWidth() - view.getWidth(), view.getTop());
            }
        } else {
            c(view, 1.0f);
            a(layoutParams.f1086a, 0, view);
            view.setVisibility(0);
        }
        invalidate();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.DrawerLayout.a(android.graphics.drawable.Drawable, int):boolean
      android.support.v4.widget.DrawerLayout.a(int, int):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void
      android.support.v4.widget.DrawerLayout.a(java.lang.Object, boolean):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, int):boolean
      android.support.v4.widget.i.a(java.lang.Object, boolean):void
      android.support.v4.widget.DrawerLayout.a(int, boolean):void */
    public void e(int i2) {
        a(i2, true);
    }

    public void a(int i2, boolean z2) {
        View c2 = c(i2);
        if (c2 == null) {
            throw new IllegalArgumentException("No drawer view found with gravity " + d(i2));
        }
        a(c2, z2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.b(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.b(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.b(android.view.View, boolean):void */
    public void i(View view) {
        b(view, true);
    }

    public void b(View view, boolean z2) {
        if (!g(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (this.q) {
            layoutParams.f1087b = 0.0f;
            layoutParams.f1089d = 0;
        } else if (z2) {
            layoutParams.f1089d |= 4;
            if (a(view, 3)) {
                this.k.a(view, -view.getWidth(), view.getTop());
            } else {
                this.l.a(view, getWidth(), view.getTop());
            }
        } else {
            c(view, 0.0f);
            a(layoutParams.f1086a, 0, view);
            view.setVisibility(4);
        }
        invalidate();
    }

    public boolean j(View view) {
        if (g(view)) {
            return (((LayoutParams) view.getLayoutParams()).f1089d & 1) == 1;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public boolean f(int i2) {
        View c2 = c(i2);
        if (c2 != null) {
            return j(c2);
        }
        return false;
    }

    public boolean k(View view) {
        if (g(view)) {
            return ((LayoutParams) view.getLayoutParams()).f1087b > 0.0f;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    public boolean g(int i2) {
        View c2 = c(i2);
        if (c2 != null) {
            return k(c2);
        }
        return false;
    }

    private boolean h() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (((LayoutParams) getChildAt(i2).getLayoutParams()).f1088c) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof LayoutParams) {
            return new LayoutParams((LayoutParams) layoutParams);
        }
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        if (getDescendantFocusability() != 393216) {
            int childCount = getChildCount();
            boolean z2 = false;
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = getChildAt(i4);
                if (!g(childAt)) {
                    this.M.add(childAt);
                } else if (j(childAt)) {
                    z2 = true;
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
            if (!z2) {
                int size = this.M.size();
                for (int i5 = 0; i5 < size; i5++) {
                    View view = this.M.get(i5);
                    if (view.getVisibility() == 0) {
                        view.addFocusables(arrayList, i2, i3);
                    }
                }
            }
            this.M.clear();
        }
    }

    private boolean i() {
        return c() != null;
    }

    /* access modifiers changed from: package-private */
    public View c() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (g(childAt) && k(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (!this.w) {
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                getChildAt(i2).dispatchTouchEvent(obtain);
            }
            obtain.recycle();
            this.w = true;
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !i()) {
            return super.onKeyDown(i2, keyEvent);
        }
        keyEvent.startTracking();
        return true;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyUp(i2, keyEvent);
        }
        View c2 = c();
        if (c2 != null && a(c2) == 0) {
            b();
        }
        return c2 != null;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        View c2;
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.f1090a == 0 || (c2 = c(savedState.f1090a)) == null)) {
            h(c2);
        }
        if (savedState.f1091b != 3) {
            a(savedState.f1091b, 3);
        }
        if (savedState.f1092c != 3) {
            a(savedState.f1092c, 5);
        }
        if (savedState.f1093d != 3) {
            a(savedState.f1093d, 8388611);
        }
        if (savedState.f1094e != 3) {
            a(savedState.f1094e, 8388613);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        LayoutParams layoutParams;
        boolean z2;
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        int childCount = getChildCount();
        int i2 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            layoutParams = (LayoutParams) getChildAt(i2).getLayoutParams();
            boolean z3 = layoutParams.f1089d == 1;
            if (layoutParams.f1089d == 2) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (z3 || z2) {
                savedState.f1090a = layoutParams.f1086a;
            } else {
                i2++;
            }
        }
        savedState.f1090a = layoutParams.f1086a;
        savedState.f1091b = this.r;
        savedState.f1092c = this.s;
        savedState.f1093d = this.t;
        savedState.f1094e = this.u;
        return savedState;
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i2, layoutParams);
        if (a() != null || g(view)) {
            ag.c(view, 4);
        } else {
            ag.c(view, 1);
        }
        if (!f1079b) {
            ag.a(view, this.f1082e);
        }
    }

    static boolean l(View view) {
        return (ag.d(view) == 4 || ag.d(view) == 2) ? false : true;
    }

    protected static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = android.support.v4.os.f.a(new android.support.v4.os.g<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a  reason: collision with root package name */
        int f1090a = 0;

        /* renamed from: b  reason: collision with root package name */
        int f1091b;

        /* renamed from: c  reason: collision with root package name */
        int f1092c;

        /* renamed from: d  reason: collision with root package name */
        int f1093d;

        /* renamed from: e  reason: collision with root package name */
        int f1094e;

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f1090a = parcel.readInt();
            this.f1091b = parcel.readInt();
            this.f1092c = parcel.readInt();
            this.f1093d = parcel.readInt();
            this.f1094e = parcel.readInt();
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f1090a);
            parcel.writeInt(this.f1091b);
            parcel.writeInt(this.f1092c);
            parcel.writeInt(this.f1093d);
            parcel.writeInt(this.f1094e);
        }
    }

    private class g extends ae.a {

        /* renamed from: b  reason: collision with root package name */
        private final int f1099b;

        /* renamed from: c  reason: collision with root package name */
        private ae f1100c;

        /* renamed from: d  reason: collision with root package name */
        private final Runnable f1101d = new Runnable() {
            public void run() {
                g.this.b();
            }
        };

        g(int i) {
            this.f1099b = i;
        }

        public void a(ae aeVar) {
            this.f1100c = aeVar;
        }

        public void a() {
            DrawerLayout.this.removeCallbacks(this.f1101d);
        }

        public boolean tryCaptureView(View view, int i) {
            return DrawerLayout.this.g(view) && DrawerLayout.this.a(view, this.f1099b) && DrawerLayout.this.a(view) == 0;
        }

        public void onViewDragStateChanged(int i) {
            DrawerLayout.this.a(this.f1099b, i, this.f1100c.c());
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
            float width;
            int width2 = view.getWidth();
            if (DrawerLayout.this.a(view, 3)) {
                width = ((float) (width2 + i)) / ((float) width2);
            } else {
                width = ((float) (DrawerLayout.this.getWidth() - i)) / ((float) width2);
            }
            DrawerLayout.this.b(view, width);
            view.setVisibility(width == 0.0f ? 4 : 0);
            DrawerLayout.this.invalidate();
        }

        public void onViewCaptured(View view, int i) {
            ((LayoutParams) view.getLayoutParams()).f1088c = false;
            c();
        }

        private void c() {
            int i = 3;
            if (this.f1099b == 3) {
                i = 5;
            }
            View c2 = DrawerLayout.this.c(i);
            if (c2 != null) {
                DrawerLayout.this.i(c2);
            }
        }

        public void onViewReleased(View view, float f2, float f3) {
            int width;
            float d2 = DrawerLayout.this.d(view);
            int width2 = view.getWidth();
            if (DrawerLayout.this.a(view, 3)) {
                width = (f2 > 0.0f || (f2 == 0.0f && d2 > 0.5f)) ? 0 : -width2;
            } else {
                width = DrawerLayout.this.getWidth();
                if (f2 < 0.0f || (f2 == 0.0f && d2 > 0.5f)) {
                    width -= width2;
                }
            }
            this.f1100c.a(width, view.getTop());
            DrawerLayout.this.invalidate();
        }

        public void onEdgeTouched(int i, int i2) {
            DrawerLayout.this.postDelayed(this.f1101d, 160);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            View view;
            int i;
            int i2 = 0;
            int b2 = this.f1100c.b();
            boolean z = this.f1099b == 3;
            if (z) {
                View c2 = DrawerLayout.this.c(3);
                if (c2 != null) {
                    i2 = -c2.getWidth();
                }
                int i3 = i2 + b2;
                view = c2;
                i = i3;
            } else {
                View c3 = DrawerLayout.this.c(5);
                int width = DrawerLayout.this.getWidth() - b2;
                view = c3;
                i = width;
            }
            if (view == null) {
                return;
            }
            if (((z && view.getLeft() < i) || (!z && view.getLeft() > i)) && DrawerLayout.this.a(view) == 0) {
                this.f1100c.a(view, i, view.getTop());
                ((LayoutParams) view.getLayoutParams()).f1088c = true;
                DrawerLayout.this.invalidate();
                c();
                DrawerLayout.this.d();
            }
        }

        public boolean onEdgeLock(int i) {
            return false;
        }

        public void onEdgeDragStarted(int i, int i2) {
            View c2;
            if ((i & 1) == 1) {
                c2 = DrawerLayout.this.c(3);
            } else {
                c2 = DrawerLayout.this.c(5);
            }
            if (c2 != null && DrawerLayout.this.a(c2) == 0) {
                this.f1100c.a(c2, i2);
            }
        }

        public int getViewHorizontalDragRange(View view) {
            if (DrawerLayout.this.g(view)) {
                return view.getWidth();
            }
            return 0;
        }

        public int clampViewPositionHorizontal(View view, int i, int i2) {
            if (DrawerLayout.this.a(view, 3)) {
                return Math.max(-view.getWidth(), Math.min(i, 0));
            }
            int width = DrawerLayout.this.getWidth();
            return Math.max(width - view.getWidth(), Math.min(i, width));
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            return view.getTop();
        }
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {

        /* renamed from: a  reason: collision with root package name */
        public int f1086a = 0;

        /* renamed from: b  reason: collision with root package name */
        float f1087b;

        /* renamed from: c  reason: collision with root package name */
        boolean f1088c;

        /* renamed from: d  reason: collision with root package name */
        int f1089d;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.f1078a);
            this.f1086a = obtainStyledAttributes.getInt(0, 0);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.MarginLayoutParams) layoutParams);
            this.f1086a = layoutParams.f1086a;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }
    }

    class a extends android.support.v4.view.a {

        /* renamed from: b  reason: collision with root package name */
        private final Rect f1096b = new Rect();

        a() {
        }

        public void onInitializeAccessibilityNodeInfo(View view, android.support.v4.view.a.e eVar) {
            if (DrawerLayout.f1079b) {
                super.onInitializeAccessibilityNodeInfo(view, eVar);
            } else {
                android.support.v4.view.a.e a2 = android.support.v4.view.a.e.a(eVar);
                super.onInitializeAccessibilityNodeInfo(view, a2);
                eVar.b(view);
                ViewParent h2 = ag.h(view);
                if (h2 instanceof View) {
                    eVar.d((View) h2);
                }
                a(eVar, a2);
                a2.v();
                a(eVar, (ViewGroup) view);
            }
            eVar.b((CharSequence) DrawerLayout.class.getName());
            eVar.c(false);
            eVar.d(false);
            eVar.a(e.a.f964a);
            eVar.a(e.a.f965b);
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setClassName(DrawerLayout.class.getName());
        }

        public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            CharSequence b2;
            if (accessibilityEvent.getEventType() != 32) {
                return super.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
            }
            List<CharSequence> text = accessibilityEvent.getText();
            View c2 = DrawerLayout.this.c();
            if (!(c2 == null || (b2 = DrawerLayout.this.b(DrawerLayout.this.e(c2))) == null)) {
                text.add(b2);
            }
            return true;
        }

        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            if (DrawerLayout.f1079b || DrawerLayout.l(view)) {
                return super.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
            }
            return false;
        }

        private void a(android.support.v4.view.a.e eVar, ViewGroup viewGroup) {
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = viewGroup.getChildAt(i);
                if (DrawerLayout.l(childAt)) {
                    eVar.c(childAt);
                }
            }
        }

        private void a(android.support.v4.view.a.e eVar, android.support.v4.view.a.e eVar2) {
            Rect rect = this.f1096b;
            eVar2.a(rect);
            eVar.b(rect);
            eVar2.c(rect);
            eVar.d(rect);
            eVar.e(eVar2.j());
            eVar.a(eVar2.r());
            eVar.b(eVar2.s());
            eVar.d(eVar2.u());
            eVar.j(eVar2.o());
            eVar.h(eVar2.m());
            eVar.c(eVar2.h());
            eVar.d(eVar2.i());
            eVar.f(eVar2.k());
            eVar.g(eVar2.l());
            eVar.i(eVar2.n());
            eVar.a(eVar2.d());
        }
    }

    final class b extends android.support.v4.view.a {
        b() {
        }

        public void onInitializeAccessibilityNodeInfo(View view, android.support.v4.view.a.e eVar) {
            super.onInitializeAccessibilityNodeInfo(view, eVar);
            if (!DrawerLayout.l(view)) {
                eVar.d((View) null);
            }
        }
    }
}
