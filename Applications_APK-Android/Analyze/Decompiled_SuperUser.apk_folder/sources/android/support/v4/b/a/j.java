package android.support.v4.b.a;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.b.a.i;

@TargetApi(11)
/* compiled from: DrawableWrapperHoneycomb */
class j extends i {
    j(Drawable drawable) {
        super(drawable);
    }

    j(i.a aVar, Resources resources) {
        super(aVar, resources);
    }

    public void jumpToCurrentState() {
        this.f563c.jumpToCurrentState();
    }

    /* access modifiers changed from: package-private */
    public i.a b() {
        return new a(this.f562b, null);
    }

    /* compiled from: DrawableWrapperHoneycomb */
    private static class a extends i.a {
        a(i.a aVar, Resources resources) {
            super(aVar, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new j(this, resources);
        }
    }
}
