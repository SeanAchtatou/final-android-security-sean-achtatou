package android.support.v4.b.a;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Build;
import android.support.v4.b.a.i;

@TargetApi(21)
/* compiled from: DrawableWrapperLollipop */
class l extends k {
    l(Drawable drawable) {
        super(drawable);
    }

    l(i.a aVar, Resources resources) {
        super(aVar, resources);
    }

    public void setHotspot(float f2, float f3) {
        this.f563c.setHotspot(f2, f3);
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        this.f563c.setHotspotBounds(i, i2, i3, i4);
    }

    public void getOutline(Outline outline) {
        this.f563c.getOutline(outline);
    }

    public Rect getDirtyBounds() {
        return this.f563c.getDirtyBounds();
    }

    public void setTintList(ColorStateList colorStateList) {
        if (c()) {
            super.setTintList(colorStateList);
        } else {
            this.f563c.setTintList(colorStateList);
        }
    }

    public void setTint(int i) {
        if (c()) {
            super.setTint(i);
        } else {
            this.f563c.setTint(i);
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (c()) {
            super.setTintMode(mode);
        } else {
            this.f563c.setTintMode(mode);
        }
    }

    public boolean setState(int[] iArr) {
        if (!super.setState(iArr)) {
            return false;
        }
        invalidateSelf();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (Build.VERSION.SDK_INT != 21) {
            return false;
        }
        Drawable drawable = this.f563c;
        if ((drawable instanceof GradientDrawable) || (drawable instanceof DrawableContainer) || (drawable instanceof InsetDrawable)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public i.a b() {
        return new a(this.f562b, null);
    }

    /* compiled from: DrawableWrapperLollipop */
    private static class a extends i.a {
        a(i.a aVar, Resources resources) {
            super(aVar, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new l(this, resources);
        }
    }
}
