package android.support.v4.b.a;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* compiled from: TintAwareDrawable */
public interface m {
    void setTint(int i);

    void setTintList(ColorStateList colorStateList);

    void setTintMode(PorterDuff.Mode mode);
}
