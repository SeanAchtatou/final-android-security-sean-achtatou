package android.support.v4.b.a;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.util.Log;
import java.lang.reflect.Method;

@TargetApi(17)
/* compiled from: DrawableCompatJellybeanMr1 */
class e {

    /* renamed from: a  reason: collision with root package name */
    private static Method f557a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f558b;

    /* renamed from: c  reason: collision with root package name */
    private static Method f559c;

    /* renamed from: d  reason: collision with root package name */
    private static boolean f560d;

    public static boolean a(Drawable drawable, int i) {
        if (!f558b) {
            Class<Drawable> cls = Drawable.class;
            try {
                f557a = cls.getDeclaredMethod("setLayoutDirection", Integer.TYPE);
                f557a.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("DrawableCompatJellybeanMr1", "Failed to retrieve setLayoutDirection(int) method", e2);
            }
            f558b = true;
        }
        if (f557a != null) {
            try {
                f557a.invoke(drawable, Integer.valueOf(i));
                return true;
            } catch (Exception e3) {
                Log.i("DrawableCompatJellybeanMr1", "Failed to invoke setLayoutDirection(int) via reflection", e3);
                f557a = null;
            }
        }
        return false;
    }

    public static int a(Drawable drawable) {
        if (!f560d) {
            try {
                f559c = Drawable.class.getDeclaredMethod("getLayoutDirection", new Class[0]);
                f559c.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("DrawableCompatJellybeanMr1", "Failed to retrieve getLayoutDirection() method", e2);
            }
            f560d = true;
        }
        if (f559c != null) {
            try {
                return ((Integer) f559c.invoke(drawable, new Object[0])).intValue();
            } catch (Exception e3) {
                Log.i("DrawableCompatJellybeanMr1", "Failed to invoke getLayoutDirection() via reflection", e3);
                f559c = null;
            }
        }
        return -1;
    }
}
