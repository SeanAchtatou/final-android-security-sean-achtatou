package android.support.v4.b.a;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;

@TargetApi(23)
/* compiled from: DrawableCompatApi23 */
class b {
    public static boolean a(Drawable drawable, int i) {
        return drawable.setLayoutDirection(i);
    }

    public static int a(Drawable drawable) {
        return drawable.getLayoutDirection();
    }
}
