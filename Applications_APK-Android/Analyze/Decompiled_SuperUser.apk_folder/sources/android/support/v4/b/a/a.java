package android.support.v4.b.a;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;

/* compiled from: DrawableCompat */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    static final b f556a;

    /* compiled from: DrawableCompat */
    interface b {
        void a(Drawable drawable);

        void a(Drawable drawable, float f2, float f3);

        void a(Drawable drawable, int i);

        void a(Drawable drawable, int i, int i2, int i3, int i4);

        void a(Drawable drawable, ColorStateList colorStateList);

        void a(Drawable drawable, Resources.Theme theme);

        void a(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme);

        void a(Drawable drawable, PorterDuff.Mode mode);

        void a(Drawable drawable, boolean z);

        boolean b(Drawable drawable);

        boolean b(Drawable drawable, int i);

        Drawable c(Drawable drawable);

        int d(Drawable drawable);

        int e(Drawable drawable);

        boolean f(Drawable drawable);

        ColorFilter g(Drawable drawable);

        void h(Drawable drawable);
    }

    /* renamed from: android.support.v4.b.a.a$a  reason: collision with other inner class name */
    /* compiled from: DrawableCompat */
    static class C0012a implements b {
        C0012a() {
        }

        public void a(Drawable drawable) {
        }

        public void a(Drawable drawable, boolean z) {
        }

        public boolean b(Drawable drawable) {
            return false;
        }

        public void a(Drawable drawable, float f2, float f3) {
        }

        public void a(Drawable drawable, int i, int i2, int i3, int i4) {
        }

        public void a(Drawable drawable, int i) {
            c.a(drawable, i);
        }

        public void a(Drawable drawable, ColorStateList colorStateList) {
            c.a(drawable, colorStateList);
        }

        public void a(Drawable drawable, PorterDuff.Mode mode) {
            c.a(drawable, mode);
        }

        public Drawable c(Drawable drawable) {
            return c.a(drawable);
        }

        public boolean b(Drawable drawable, int i) {
            return false;
        }

        public int d(Drawable drawable) {
            return 0;
        }

        public int e(Drawable drawable) {
            return 0;
        }

        public void a(Drawable drawable, Resources.Theme theme) {
        }

        public boolean f(Drawable drawable) {
            return false;
        }

        public ColorFilter g(Drawable drawable) {
            return null;
        }

        public void h(Drawable drawable) {
            drawable.clearColorFilter();
        }

        public void a(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            c.a(drawable, resources, xmlPullParser, attributeSet, theme);
        }
    }

    /* compiled from: DrawableCompat */
    static class c extends C0012a {
        c() {
        }

        public void a(Drawable drawable) {
            d.a(drawable);
        }

        public Drawable c(Drawable drawable) {
            return d.b(drawable);
        }
    }

    /* compiled from: DrawableCompat */
    static class d extends c {
        d() {
        }

        public boolean b(Drawable drawable, int i) {
            return e.a(drawable, i);
        }

        public int d(Drawable drawable) {
            int a2 = e.a(drawable);
            if (a2 >= 0) {
                return a2;
            }
            return 0;
        }
    }

    /* compiled from: DrawableCompat */
    static class e extends d {
        e() {
        }

        public void a(Drawable drawable, boolean z) {
            f.a(drawable, z);
        }

        public boolean b(Drawable drawable) {
            return f.a(drawable);
        }

        public Drawable c(Drawable drawable) {
            return f.b(drawable);
        }

        public int e(Drawable drawable) {
            return f.c(drawable);
        }
    }

    /* compiled from: DrawableCompat */
    static class f extends e {
        f() {
        }

        public void a(Drawable drawable, float f2, float f3) {
            g.a(drawable, f2, f3);
        }

        public void a(Drawable drawable, int i, int i2, int i3, int i4) {
            g.a(drawable, i, i2, i3, i4);
        }

        public void a(Drawable drawable, int i) {
            g.a(drawable, i);
        }

        public void a(Drawable drawable, ColorStateList colorStateList) {
            g.a(drawable, colorStateList);
        }

        public void a(Drawable drawable, PorterDuff.Mode mode) {
            g.a(drawable, mode);
        }

        public Drawable c(Drawable drawable) {
            return g.a(drawable);
        }

        public void a(Drawable drawable, Resources.Theme theme) {
            g.a(drawable, theme);
        }

        public boolean f(Drawable drawable) {
            return g.b(drawable);
        }

        public ColorFilter g(Drawable drawable) {
            return g.c(drawable);
        }

        public void h(Drawable drawable) {
            g.d(drawable);
        }

        public void a(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            g.a(drawable, resources, xmlPullParser, attributeSet, theme);
        }
    }

    /* compiled from: DrawableCompat */
    static class g extends f {
        g() {
        }

        public boolean b(Drawable drawable, int i) {
            return b.a(drawable, i);
        }

        public int d(Drawable drawable) {
            return b.a(drawable);
        }

        public Drawable c(Drawable drawable) {
            return drawable;
        }

        public void h(Drawable drawable) {
            drawable.clearColorFilter();
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            f556a = new g();
        } else if (i >= 21) {
            f556a = new f();
        } else if (i >= 19) {
            f556a = new e();
        } else if (i >= 17) {
            f556a = new d();
        } else if (i >= 11) {
            f556a = new c();
        } else {
            f556a = new C0012a();
        }
    }

    public static void a(Drawable drawable) {
        f556a.a(drawable);
    }

    public static void a(Drawable drawable, boolean z) {
        f556a.a(drawable, z);
    }

    public static boolean b(Drawable drawable) {
        return f556a.b(drawable);
    }

    public static void a(Drawable drawable, float f2, float f3) {
        f556a.a(drawable, f2, f3);
    }

    public static void a(Drawable drawable, int i, int i2, int i3, int i4) {
        f556a.a(drawable, i, i2, i3, i4);
    }

    public static void a(Drawable drawable, int i) {
        f556a.a(drawable, i);
    }

    public static void a(Drawable drawable, ColorStateList colorStateList) {
        f556a.a(drawable, colorStateList);
    }

    public static void a(Drawable drawable, PorterDuff.Mode mode) {
        f556a.a(drawable, mode);
    }

    public static int c(Drawable drawable) {
        return f556a.e(drawable);
    }

    public static void a(Drawable drawable, Resources.Theme theme) {
        f556a.a(drawable, theme);
    }

    public static boolean d(Drawable drawable) {
        return f556a.f(drawable);
    }

    public static ColorFilter e(Drawable drawable) {
        return f556a.g(drawable);
    }

    public static void f(Drawable drawable) {
        f556a.h(drawable);
    }

    public static void a(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        f556a.a(drawable, resources, xmlPullParser, attributeSet, theme);
    }

    public static Drawable g(Drawable drawable) {
        return f556a.c(drawable);
    }

    public static <T extends Drawable> T h(Drawable drawable) {
        if (drawable instanceof h) {
            return ((h) drawable).a();
        }
        return drawable;
    }

    public static boolean b(Drawable drawable, int i) {
        return f556a.b(drawable, i);
    }

    public static int i(Drawable drawable) {
        return f556a.d(drawable);
    }
}
