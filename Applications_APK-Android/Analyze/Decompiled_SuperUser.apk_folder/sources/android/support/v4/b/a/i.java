package android.support.v4.b.a;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;

@TargetApi(9)
/* compiled from: DrawableWrapperGingerbread */
class i extends Drawable implements Drawable.Callback, h, m {

    /* renamed from: a  reason: collision with root package name */
    static final PorterDuff.Mode f561a = PorterDuff.Mode.SRC_IN;

    /* renamed from: b  reason: collision with root package name */
    a f562b;

    /* renamed from: c  reason: collision with root package name */
    Drawable f563c;

    /* renamed from: d  reason: collision with root package name */
    private int f564d;

    /* renamed from: e  reason: collision with root package name */
    private PorterDuff.Mode f565e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f566f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f567g;

    i(a aVar, Resources resources) {
        this.f562b = aVar;
        a(resources);
    }

    i(Drawable drawable) {
        this.f562b = b();
        a(drawable);
    }

    private void a(Resources resources) {
        if (this.f562b != null && this.f562b.f569b != null) {
            a(a(this.f562b.f569b, resources));
        }
    }

    /* access modifiers changed from: protected */
    public Drawable a(Drawable.ConstantState constantState, Resources resources) {
        return constantState.newDrawable(resources);
    }

    public void draw(Canvas canvas) {
        this.f563c.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f563c != null) {
            this.f563c.setBounds(rect);
        }
    }

    public void setChangingConfigurations(int i) {
        this.f563c.setChangingConfigurations(i);
    }

    public int getChangingConfigurations() {
        return (this.f562b != null ? this.f562b.getChangingConfigurations() : 0) | super.getChangingConfigurations() | this.f563c.getChangingConfigurations();
    }

    public void setDither(boolean z) {
        this.f563c.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f563c.setFilterBitmap(z);
    }

    public void setAlpha(int i) {
        this.f563c.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f563c.setColorFilter(colorFilter);
    }

    public boolean isStateful() {
        ColorStateList colorStateList = (!c() || this.f562b == null) ? null : this.f562b.f570c;
        return (colorStateList != null && colorStateList.isStateful()) || this.f563c.isStateful();
    }

    public boolean setState(int[] iArr) {
        return a(iArr) || this.f563c.setState(iArr);
    }

    public int[] getState() {
        return this.f563c.getState();
    }

    public Drawable getCurrent() {
        return this.f563c.getCurrent();
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f563c.setVisible(z, z2);
    }

    public int getOpacity() {
        return this.f563c.getOpacity();
    }

    public Region getTransparentRegion() {
        return this.f563c.getTransparentRegion();
    }

    public int getIntrinsicWidth() {
        return this.f563c.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        return this.f563c.getIntrinsicHeight();
    }

    public int getMinimumWidth() {
        return this.f563c.getMinimumWidth();
    }

    public int getMinimumHeight() {
        return this.f563c.getMinimumHeight();
    }

    public boolean getPadding(Rect rect) {
        return this.f563c.getPadding(rect);
    }

    public Drawable.ConstantState getConstantState() {
        if (this.f562b == null || !this.f562b.a()) {
            return null;
        }
        this.f562b.f568a = getChangingConfigurations();
        return this.f562b;
    }

    public Drawable mutate() {
        if (!this.f567g && super.mutate() == this) {
            this.f562b = b();
            if (this.f563c != null) {
                this.f563c.mutate();
            }
            if (this.f562b != null) {
                this.f562b.f569b = this.f563c != null ? this.f563c.getConstantState() : null;
            }
            this.f567g = true;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public a b() {
        return new b(this.f562b, null);
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f563c.setLevel(i);
    }

    public void setTint(int i) {
        setTintList(ColorStateList.valueOf(i));
    }

    public void setTintList(ColorStateList colorStateList) {
        this.f562b.f570c = colorStateList;
        a(getState());
    }

    public void setTintMode(PorterDuff.Mode mode) {
        this.f562b.f571d = mode;
        a(getState());
    }

    private boolean a(int[] iArr) {
        if (!c()) {
            return false;
        }
        ColorStateList colorStateList = this.f562b.f570c;
        PorterDuff.Mode mode = this.f562b.f571d;
        if (colorStateList == null || mode == null) {
            this.f566f = false;
            clearColorFilter();
            return false;
        }
        int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
        if (this.f566f && colorForState == this.f564d && mode == this.f565e) {
            return false;
        }
        setColorFilter(colorForState, mode);
        this.f564d = colorForState;
        this.f565e = mode;
        this.f566f = true;
        return true;
    }

    public final Drawable a() {
        return this.f563c;
    }

    public final void a(Drawable drawable) {
        if (this.f563c != null) {
            this.f563c.setCallback(null);
        }
        this.f563c = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            setVisible(drawable.isVisible(), true);
            setState(drawable.getState());
            setLevel(drawable.getLevel());
            setBounds(drawable.getBounds());
            if (this.f562b != null) {
                this.f562b.f569b = drawable.getConstantState();
            }
        }
        invalidateSelf();
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return true;
    }

    /* compiled from: DrawableWrapperGingerbread */
    protected static abstract class a extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        int f568a;

        /* renamed from: b  reason: collision with root package name */
        Drawable.ConstantState f569b;

        /* renamed from: c  reason: collision with root package name */
        ColorStateList f570c = null;

        /* renamed from: d  reason: collision with root package name */
        PorterDuff.Mode f571d = i.f561a;

        public abstract Drawable newDrawable(Resources resources);

        a(a aVar, Resources resources) {
            if (aVar != null) {
                this.f568a = aVar.f568a;
                this.f569b = aVar.f569b;
                this.f570c = aVar.f570c;
                this.f571d = aVar.f571d;
            }
        }

        public Drawable newDrawable() {
            return newDrawable(null);
        }

        public int getChangingConfigurations() {
            return (this.f569b != null ? this.f569b.getChangingConfigurations() : 0) | this.f568a;
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.f569b != null;
        }
    }

    /* compiled from: DrawableWrapperGingerbread */
    private static class b extends a {
        b(a aVar, Resources resources) {
            super(aVar, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new i(this, resources);
        }
    }
}
