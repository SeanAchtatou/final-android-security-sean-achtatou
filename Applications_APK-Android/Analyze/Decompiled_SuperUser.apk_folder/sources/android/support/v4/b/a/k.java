package android.support.v4.b.a;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.b.a.i;

@TargetApi(19)
/* compiled from: DrawableWrapperKitKat */
class k extends j {
    k(Drawable drawable) {
        super(drawable);
    }

    k(i.a aVar, Resources resources) {
        super(aVar, resources);
    }

    public void setAutoMirrored(boolean z) {
        this.f563c.setAutoMirrored(z);
    }

    public boolean isAutoMirrored() {
        return this.f563c.isAutoMirrored();
    }

    /* access modifiers changed from: package-private */
    public i.a b() {
        return new a(this.f562b, null);
    }

    /* compiled from: DrawableWrapperKitKat */
    private static class a extends i.a {
        a(i.a aVar, Resources resources) {
            super(aVar, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new k(this, resources);
        }
    }
}
