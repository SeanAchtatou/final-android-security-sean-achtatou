package android.support.v4.media;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.os.ResultReceiver;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class MediaBrowserCompat {

    /* renamed from: a  reason: collision with root package name */
    static final boolean f669a = Log.isLoggable("MediaBrowserCompat", 3);

    public static class MediaItem implements Parcelable {
        public static final Parcelable.Creator<MediaItem> CREATOR = new Parcelable.Creator<MediaItem>() {
            /* renamed from: a */
            public MediaItem createFromParcel(Parcel parcel) {
                return new MediaItem(parcel);
            }

            /* renamed from: a */
            public MediaItem[] newArray(int i) {
                return new MediaItem[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private final int f672a;

        /* renamed from: b  reason: collision with root package name */
        private final MediaDescriptionCompat f673b;

        MediaItem(Parcel parcel) {
            this.f672a = parcel.readInt();
            this.f673b = MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f672a);
            this.f673b.writeToParcel(parcel, i);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("MediaItem{");
            sb.append("mFlags=").append(this.f672a);
            sb.append(", mDescription=").append(this.f673b);
            sb.append('}');
            return sb.toString();
        }
    }

    public static abstract class a {
        public void a(MediaItem mediaItem) {
        }

        public void a(String str) {
        }
    }

    public static abstract class b {
        public void a(String str, Bundle bundle, List<MediaItem> list) {
        }

        public void a(String str, Bundle bundle) {
        }
    }

    private static class ItemReceiver extends ResultReceiver {

        /* renamed from: d  reason: collision with root package name */
        private final String f670d;

        /* renamed from: e  reason: collision with root package name */
        private final a f671e;

        /* access modifiers changed from: protected */
        public void a(int i, Bundle bundle) {
            if (bundle != null) {
                bundle.setClassLoader(MediaBrowserCompat.class.getClassLoader());
            }
            if (i != 0 || bundle == null || !bundle.containsKey("media_item")) {
                this.f671e.a(this.f670d);
                return;
            }
            Parcelable parcelable = bundle.getParcelable("media_item");
            if (parcelable == null || (parcelable instanceof MediaItem)) {
                this.f671e.a((MediaItem) parcelable);
            } else {
                this.f671e.a(this.f670d);
            }
        }
    }

    private static class SearchResultReceiver extends ResultReceiver {

        /* renamed from: d  reason: collision with root package name */
        private final String f674d;

        /* renamed from: e  reason: collision with root package name */
        private final Bundle f675e;

        /* renamed from: f  reason: collision with root package name */
        private final b f676f;

        /* access modifiers changed from: protected */
        public void a(int i, Bundle bundle) {
            if (i != 0 || bundle == null || !bundle.containsKey("search_results")) {
                this.f676f.a(this.f674d, this.f675e);
                return;
            }
            Parcelable[] parcelableArray = bundle.getParcelableArray("search_results");
            ArrayList arrayList = null;
            if (parcelableArray != null) {
                ArrayList arrayList2 = new ArrayList();
                for (Parcelable parcelable : parcelableArray) {
                    arrayList2.add((MediaItem) parcelable);
                }
                arrayList = arrayList2;
            }
            this.f676f.a(this.f674d, this.f675e, arrayList);
        }
    }
}
