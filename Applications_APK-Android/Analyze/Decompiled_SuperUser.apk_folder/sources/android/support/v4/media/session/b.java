package android.support.v4.media.session;

import android.annotation.TargetApi;
import android.media.session.MediaSession;

@TargetApi(21)
/* compiled from: MediaSessionCompatApi21 */
class b {

    /* compiled from: MediaSessionCompatApi21 */
    static class a {
        public static Object a(Object obj) {
            return ((MediaSession.QueueItem) obj).getDescription();
        }

        public static long b(Object obj) {
            return ((MediaSession.QueueItem) obj).getQueueId();
        }
    }
}
