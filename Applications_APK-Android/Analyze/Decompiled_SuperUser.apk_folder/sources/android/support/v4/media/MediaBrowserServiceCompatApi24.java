package android.support.v4.media;

import android.annotation.TargetApi;
import android.service.media.MediaBrowserService;
import android.support.v4.media.b;
import android.util.Log;
import java.lang.reflect.Field;

@TargetApi(24)
class MediaBrowserServiceCompatApi24 {

    /* renamed from: a  reason: collision with root package name */
    private static Field f737a;

    public interface ServiceCompatProxy extends b.a {
    }

    static {
        try {
            f737a = MediaBrowserService.Result.class.getDeclaredField("mFlags");
            f737a.setAccessible(true);
        } catch (NoSuchFieldException e2) {
            Log.w("MBSCompatApi24", e2);
        }
    }
}
