package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.c;
import android.support.v4.media.d;
import android.text.TextUtils;

public final class MediaDescriptionCompat implements Parcelable {
    public static final Parcelable.Creator<MediaDescriptionCompat> CREATOR = new Parcelable.Creator<MediaDescriptionCompat>() {
        /* renamed from: a */
        public MediaDescriptionCompat createFromParcel(Parcel parcel) {
            if (Build.VERSION.SDK_INT < 21) {
                return new MediaDescriptionCompat(parcel);
            }
            return MediaDescriptionCompat.a(c.a(parcel));
        }

        /* renamed from: a */
        public MediaDescriptionCompat[] newArray(int i) {
            return new MediaDescriptionCompat[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final String f738a;

    /* renamed from: b  reason: collision with root package name */
    private final CharSequence f739b;

    /* renamed from: c  reason: collision with root package name */
    private final CharSequence f740c;

    /* renamed from: d  reason: collision with root package name */
    private final CharSequence f741d;

    /* renamed from: e  reason: collision with root package name */
    private final Bitmap f742e;

    /* renamed from: f  reason: collision with root package name */
    private final Uri f743f;

    /* renamed from: g  reason: collision with root package name */
    private final Bundle f744g;

    /* renamed from: h  reason: collision with root package name */
    private final Uri f745h;
    private Object i;

    MediaDescriptionCompat(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Bitmap bitmap, Uri uri, Bundle bundle, Uri uri2) {
        this.f738a = str;
        this.f739b = charSequence;
        this.f740c = charSequence2;
        this.f741d = charSequence3;
        this.f742e = bitmap;
        this.f743f = uri;
        this.f744g = bundle;
        this.f745h = uri2;
    }

    MediaDescriptionCompat(Parcel parcel) {
        this.f738a = parcel.readString();
        this.f739b = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f740c = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f741d = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f742e = (Bitmap) parcel.readParcelable(null);
        this.f743f = (Uri) parcel.readParcelable(null);
        this.f744g = parcel.readBundle();
        this.f745h = (Uri) parcel.readParcelable(null);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (Build.VERSION.SDK_INT < 21) {
            parcel.writeString(this.f738a);
            TextUtils.writeToParcel(this.f739b, parcel, i2);
            TextUtils.writeToParcel(this.f740c, parcel, i2);
            TextUtils.writeToParcel(this.f741d, parcel, i2);
            parcel.writeParcelable(this.f742e, i2);
            parcel.writeParcelable(this.f743f, i2);
            parcel.writeBundle(this.f744g);
            parcel.writeParcelable(this.f745h, i2);
            return;
        }
        c.a(a(), parcel, i2);
    }

    public String toString() {
        return ((Object) this.f739b) + ", " + ((Object) this.f740c) + ", " + ((Object) this.f741d);
    }

    public Object a() {
        if (this.i != null || Build.VERSION.SDK_INT < 21) {
            return this.i;
        }
        Object a2 = c.a.a();
        c.a.a(a2, this.f738a);
        c.a.a(a2, this.f739b);
        c.a.b(a2, this.f740c);
        c.a.c(a2, this.f741d);
        c.a.a(a2, this.f742e);
        c.a.a(a2, this.f743f);
        Bundle bundle = this.f744g;
        if (Build.VERSION.SDK_INT < 23 && this.f745h != null) {
            if (bundle == null) {
                bundle = new Bundle();
                bundle.putBoolean("android.support.v4.media.description.NULL_BUNDLE_FLAG", true);
            }
            bundle.putParcelable("android.support.v4.media.description.MEDIA_URI", this.f745h);
        }
        c.a.a(a2, bundle);
        if (Build.VERSION.SDK_INT >= 23) {
            d.a.b(a2, this.f745h);
        }
        this.i = c.a.a(a2);
        return this.i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0077  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.support.v4.media.MediaDescriptionCompat a(java.lang.Object r6) {
        /*
            r1 = 0
            if (r6 == 0) goto L_0x0009
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 21
            if (r0 >= r2) goto L_0x000a
        L_0x0009:
            return r1
        L_0x000a:
            android.support.v4.media.MediaDescriptionCompat$a r4 = new android.support.v4.media.MediaDescriptionCompat$a
            r4.<init>()
            java.lang.String r0 = android.support.v4.media.c.a(r6)
            r4.a(r0)
            java.lang.CharSequence r0 = android.support.v4.media.c.b(r6)
            r4.a(r0)
            java.lang.CharSequence r0 = android.support.v4.media.c.c(r6)
            r4.b(r0)
            java.lang.CharSequence r0 = android.support.v4.media.c.d(r6)
            r4.c(r0)
            android.graphics.Bitmap r0 = android.support.v4.media.c.e(r6)
            r4.a(r0)
            android.net.Uri r0 = android.support.v4.media.c.f(r6)
            r4.a(r0)
            android.os.Bundle r2 = android.support.v4.media.c.g(r6)
            if (r2 != 0) goto L_0x0061
            r3 = r1
        L_0x0040:
            if (r3 == 0) goto L_0x0075
            java.lang.String r0 = "android.support.v4.media.description.NULL_BUNDLE_FLAG"
            boolean r0 = r2.containsKey(r0)
            if (r0 == 0) goto L_0x006b
            int r0 = r2.size()
            r5 = 2
            if (r0 != r5) goto L_0x006b
            r0 = r1
        L_0x0052:
            r4.a(r0)
            if (r3 == 0) goto L_0x0077
            r4.b(r3)
        L_0x005a:
            android.support.v4.media.MediaDescriptionCompat r1 = r4.a()
            r1.i = r6
            goto L_0x0009
        L_0x0061:
            java.lang.String r0 = "android.support.v4.media.description.MEDIA_URI"
            android.os.Parcelable r0 = r2.getParcelable(r0)
            android.net.Uri r0 = (android.net.Uri) r0
            r3 = r0
            goto L_0x0040
        L_0x006b:
            java.lang.String r0 = "android.support.v4.media.description.MEDIA_URI"
            r2.remove(r0)
            java.lang.String r0 = "android.support.v4.media.description.NULL_BUNDLE_FLAG"
            r2.remove(r0)
        L_0x0075:
            r0 = r2
            goto L_0x0052
        L_0x0077:
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 23
            if (r0 < r1) goto L_0x005a
            android.net.Uri r0 = android.support.v4.media.d.h(r6)
            r4.b(r0)
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.media.MediaDescriptionCompat.a(java.lang.Object):android.support.v4.media.MediaDescriptionCompat");
    }

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        private String f746a;

        /* renamed from: b  reason: collision with root package name */
        private CharSequence f747b;

        /* renamed from: c  reason: collision with root package name */
        private CharSequence f748c;

        /* renamed from: d  reason: collision with root package name */
        private CharSequence f749d;

        /* renamed from: e  reason: collision with root package name */
        private Bitmap f750e;

        /* renamed from: f  reason: collision with root package name */
        private Uri f751f;

        /* renamed from: g  reason: collision with root package name */
        private Bundle f752g;

        /* renamed from: h  reason: collision with root package name */
        private Uri f753h;

        public a a(String str) {
            this.f746a = str;
            return this;
        }

        public a a(CharSequence charSequence) {
            this.f747b = charSequence;
            return this;
        }

        public a b(CharSequence charSequence) {
            this.f748c = charSequence;
            return this;
        }

        public a c(CharSequence charSequence) {
            this.f749d = charSequence;
            return this;
        }

        public a a(Bitmap bitmap) {
            this.f750e = bitmap;
            return this;
        }

        public a a(Uri uri) {
            this.f751f = uri;
            return this;
        }

        public a a(Bundle bundle) {
            this.f752g = bundle;
            return this;
        }

        public a b(Uri uri) {
            this.f753h = uri;
            return this;
        }

        public MediaDescriptionCompat a() {
            return new MediaDescriptionCompat(this.f746a, this.f747b, this.f748c, this.f749d, this.f750e, this.f751f, this.f752g, this.f753h);
        }
    }
}
