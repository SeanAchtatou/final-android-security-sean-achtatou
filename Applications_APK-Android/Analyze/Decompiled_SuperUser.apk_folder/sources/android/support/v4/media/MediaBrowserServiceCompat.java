package android.support.v4.media;

import android.app.Service;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.app.l;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.os.ResultReceiver;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.h;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class MediaBrowserServiceCompat extends Service {

    /* renamed from: a  reason: collision with root package name */
    static final boolean f677a = Log.isLoggable("MBServiceCompat", 3);

    /* renamed from: b  reason: collision with root package name */
    final ArrayMap<IBinder, b> f678b = new ArrayMap<>();

    /* renamed from: c  reason: collision with root package name */
    b f679c;

    /* renamed from: d  reason: collision with root package name */
    final g f680d = new g();

    /* renamed from: e  reason: collision with root package name */
    MediaSessionCompat.Token f681e;

    private interface e {
        IBinder a();

        void a(String str, MediaSessionCompat.Token token, Bundle bundle);

        void a(String str, List<MediaBrowserCompat.MediaItem> list, Bundle bundle);

        void b();
    }

    public abstract a a(String str, int i, Bundle bundle);

    public abstract void a(String str, c<List<MediaBrowserCompat.MediaItem>> cVar);

    private final class g extends Handler {

        /* renamed from: b  reason: collision with root package name */
        private final d f736b = new d();

        g() {
        }

        public void handleMessage(Message message) {
            Bundle data = message.getData();
            switch (message.what) {
                case 1:
                    this.f736b.a(data.getString("data_package_name"), data.getInt("data_calling_uid"), data.getBundle("data_root_hints"), new f(message.replyTo));
                    return;
                case 2:
                    this.f736b.a(new f(message.replyTo));
                    return;
                case 3:
                    this.f736b.a(data.getString("data_media_item_id"), l.a(data, "data_callback_token"), data.getBundle("data_options"), new f(message.replyTo));
                    return;
                case 4:
                    this.f736b.a(data.getString("data_media_item_id"), l.a(data, "data_callback_token"), new f(message.replyTo));
                    return;
                case 5:
                    this.f736b.a(data.getString("data_media_item_id"), (ResultReceiver) data.getParcelable("data_result_receiver"), new f(message.replyTo));
                    return;
                case 6:
                    this.f736b.a(new f(message.replyTo), data.getBundle("data_root_hints"));
                    return;
                case 7:
                    this.f736b.b(new f(message.replyTo));
                    return;
                case 8:
                    this.f736b.a(data.getString("data_search_query"), data.getBundle("data_search_extras"), (ResultReceiver) data.getParcelable("data_result_receiver"), new f(message.replyTo));
                    return;
                default:
                    Log.w("MBServiceCompat", "Unhandled message: " + message + "\n  Service version: " + 1 + "\n  Client version: " + message.arg1);
                    return;
            }
        }

        public boolean sendMessageAtTime(Message message, long j) {
            Bundle data = message.getData();
            data.setClassLoader(MediaBrowserCompat.class.getClassLoader());
            data.putInt("data_calling_uid", Binder.getCallingUid());
            return super.sendMessageAtTime(message, j);
        }

        public void a(Runnable runnable) {
            if (Thread.currentThread() == getLooper().getThread()) {
                runnable.run();
            } else {
                post(runnable);
            }
        }
    }

    private class b {

        /* renamed from: a  reason: collision with root package name */
        String f692a;

        /* renamed from: b  reason: collision with root package name */
        Bundle f693b;

        /* renamed from: c  reason: collision with root package name */
        e f694c;

        /* renamed from: d  reason: collision with root package name */
        a f695d;

        /* renamed from: e  reason: collision with root package name */
        HashMap<String, List<h<IBinder, Bundle>>> f696e = new HashMap<>();

        b() {
        }
    }

    public static class c<T> {

        /* renamed from: a  reason: collision with root package name */
        private Object f698a;

        /* renamed from: b  reason: collision with root package name */
        private boolean f699b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f700c;

        /* renamed from: d  reason: collision with root package name */
        private int f701d;

        c(Object obj) {
            this.f698a = obj;
        }

        public void a(T t) {
            if (this.f700c) {
                throw new IllegalStateException("sendResult() called twice for: " + this.f698a);
            }
            this.f700c = true;
            a(t, this.f701d);
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.f699b || this.f700c;
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            this.f701d = i;
        }

        /* access modifiers changed from: package-private */
        public void a(T t, int i) {
        }
    }

    private class d {
        d() {
        }

        public void a(String str, int i, Bundle bundle, e eVar) {
            if (!MediaBrowserServiceCompat.this.a(str, i)) {
                throw new IllegalArgumentException("Package/uid mismatch: uid=" + i + " package=" + str);
            }
            final e eVar2 = eVar;
            final String str2 = str;
            final Bundle bundle2 = bundle;
            final int i2 = i;
            MediaBrowserServiceCompat.this.f680d.a(new Runnable() {
                public void run() {
                    IBinder a2 = eVar2.a();
                    MediaBrowserServiceCompat.this.f678b.remove(a2);
                    b bVar = new b();
                    bVar.f692a = str2;
                    bVar.f693b = bundle2;
                    bVar.f694c = eVar2;
                    bVar.f695d = MediaBrowserServiceCompat.this.a(str2, i2, bundle2);
                    if (bVar.f695d == null) {
                        Log.i("MBServiceCompat", "No root for client " + str2 + " from service " + getClass().getName());
                        try {
                            eVar2.b();
                        } catch (RemoteException e2) {
                            Log.w("MBServiceCompat", "Calling onConnectFailed() failed. Ignoring. pkg=" + str2);
                        }
                    } else {
                        try {
                            MediaBrowserServiceCompat.this.f678b.put(a2, bVar);
                            if (MediaBrowserServiceCompat.this.f681e != null) {
                                eVar2.a(bVar.f695d.a(), MediaBrowserServiceCompat.this.f681e, bVar.f695d.b());
                            }
                        } catch (RemoteException e3) {
                            Log.w("MBServiceCompat", "Calling onConnect() failed. Dropping client. pkg=" + str2);
                            MediaBrowserServiceCompat.this.f678b.remove(a2);
                        }
                    }
                }
            });
        }

        public void a(final e eVar) {
            MediaBrowserServiceCompat.this.f680d.a(new Runnable() {
                public void run() {
                    if (MediaBrowserServiceCompat.this.f678b.remove(eVar.a()) != null) {
                    }
                }
            });
        }

        public void a(String str, IBinder iBinder, Bundle bundle, e eVar) {
            final e eVar2 = eVar;
            final String str2 = str;
            final IBinder iBinder2 = iBinder;
            final Bundle bundle2 = bundle;
            MediaBrowserServiceCompat.this.f680d.a(new Runnable() {
                public void run() {
                    b bVar = MediaBrowserServiceCompat.this.f678b.get(eVar2.a());
                    if (bVar == null) {
                        Log.w("MBServiceCompat", "addSubscription for callback that isn't registered id=" + str2);
                    } else {
                        MediaBrowserServiceCompat.this.a(str2, bVar, iBinder2, bundle2);
                    }
                }
            });
        }

        public void a(final String str, final IBinder iBinder, final e eVar) {
            MediaBrowserServiceCompat.this.f680d.a(new Runnable() {
                public void run() {
                    b bVar = MediaBrowserServiceCompat.this.f678b.get(eVar.a());
                    if (bVar == null) {
                        Log.w("MBServiceCompat", "removeSubscription for callback that isn't registered id=" + str);
                    } else if (!MediaBrowserServiceCompat.this.a(str, bVar, iBinder)) {
                        Log.w("MBServiceCompat", "removeSubscription called for " + str + " which is not subscribed");
                    }
                }
            });
        }

        public void a(final String str, final ResultReceiver resultReceiver, final e eVar) {
            if (!TextUtils.isEmpty(str) && resultReceiver != null) {
                MediaBrowserServiceCompat.this.f680d.a(new Runnable() {
                    public void run() {
                        b bVar = MediaBrowserServiceCompat.this.f678b.get(eVar.a());
                        if (bVar == null) {
                            Log.w("MBServiceCompat", "getMediaItem for callback that isn't registered id=" + str);
                        } else {
                            MediaBrowserServiceCompat.this.a(str, bVar, resultReceiver);
                        }
                    }
                });
            }
        }

        public void a(final e eVar, final Bundle bundle) {
            MediaBrowserServiceCompat.this.f680d.a(new Runnable() {
                public void run() {
                    IBinder a2 = eVar.a();
                    MediaBrowserServiceCompat.this.f678b.remove(a2);
                    b bVar = new b();
                    bVar.f694c = eVar;
                    bVar.f693b = bundle;
                    MediaBrowserServiceCompat.this.f678b.put(a2, bVar);
                }
            });
        }

        public void b(final e eVar) {
            MediaBrowserServiceCompat.this.f680d.a(new Runnable() {
                public void run() {
                    MediaBrowserServiceCompat.this.f678b.remove(eVar.a());
                }
            });
        }

        public void a(String str, Bundle bundle, ResultReceiver resultReceiver, e eVar) {
            if (!TextUtils.isEmpty(str) && resultReceiver != null) {
                final e eVar2 = eVar;
                final String str2 = str;
                final Bundle bundle2 = bundle;
                final ResultReceiver resultReceiver2 = resultReceiver;
                MediaBrowserServiceCompat.this.f680d.a(new Runnable() {
                    public void run() {
                        b bVar = MediaBrowserServiceCompat.this.f678b.get(eVar2.a());
                        if (bVar == null) {
                            Log.w("MBServiceCompat", "search for callback that isn't registered query=" + str2);
                        } else {
                            MediaBrowserServiceCompat.this.a(str2, bundle2, bVar, resultReceiver2);
                        }
                    }
                });
            }
        }
    }

    private class f implements e {

        /* renamed from: a  reason: collision with root package name */
        final Messenger f733a;

        f(Messenger messenger) {
            this.f733a = messenger;
        }

        public IBinder a() {
            return this.f733a.getBinder();
        }

        public void a(String str, MediaSessionCompat.Token token, Bundle bundle) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putInt("extra_service_version", 1);
            Bundle bundle2 = new Bundle();
            bundle2.putString("data_media_item_id", str);
            bundle2.putParcelable("data_media_session_token", token);
            bundle2.putBundle("data_root_hints", bundle);
            a(1, bundle2);
        }

        public void b() {
            a(2, null);
        }

        public void a(String str, List<MediaBrowserCompat.MediaItem> list, Bundle bundle) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("data_media_item_id", str);
            bundle2.putBundle("data_options", bundle);
            if (list != null) {
                bundle2.putParcelableArrayList("data_media_item_list", list instanceof ArrayList ? (ArrayList) list : new ArrayList(list));
            }
            a(3, bundle2);
        }

        private void a(int i, Bundle bundle) {
            Message obtain = Message.obtain();
            obtain.what = i;
            obtain.arg1 = 1;
            obtain.setData(bundle);
            this.f733a.send(obtain);
        }
    }

    public void a(String str, c<List<MediaBrowserCompat.MediaItem>> cVar, Bundle bundle) {
        cVar.a(1);
        a(str, cVar);
    }

    public void b(String str, c<MediaBrowserCompat.MediaItem> cVar) {
        cVar.a(2);
        cVar.a((List<MediaBrowserCompat.MediaItem>) null);
    }

    public void a(String str, Bundle bundle, c<List<MediaBrowserCompat.MediaItem>> cVar) {
        cVar.a(4);
        cVar.a((List<MediaBrowserCompat.MediaItem>) null);
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, int i) {
        if (str == null) {
            return false;
        }
        for (String equals : getPackageManager().getPackagesForUid(i)) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, b bVar, IBinder iBinder, Bundle bundle) {
        List<h> list;
        List list2 = bVar.f696e.get(str);
        if (list2 == null) {
            list = new ArrayList<>();
        } else {
            list = list2;
        }
        for (h hVar : list) {
            if (iBinder == hVar.f880a && a.a(bundle, (Bundle) hVar.f881b)) {
                return;
            }
        }
        list.add(new h(iBinder, bundle));
        bVar.f696e.put(str, list);
        a(str, bVar, bundle);
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, b bVar, IBinder iBinder) {
        boolean z;
        if (iBinder != null) {
            List list = bVar.f696e.get(str);
            if (list != null) {
                Iterator it = list.iterator();
                z = false;
                while (it.hasNext()) {
                    if (iBinder == ((h) it.next()).f880a) {
                        it.remove();
                        z = true;
                    }
                }
                if (list.size() == 0) {
                    bVar.f696e.remove(str);
                }
            } else {
                z = false;
            }
            return z;
        } else if (bVar.f696e.remove(str) != null) {
            return true;
        } else {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, b bVar, Bundle bundle) {
        final b bVar2 = bVar;
        final String str2 = str;
        final Bundle bundle2 = bundle;
        AnonymousClass1 r0 = new c<List<MediaBrowserCompat.MediaItem>>(str) {
            /* access modifiers changed from: package-private */
            public /* bridge */ /* synthetic */ void a(Object obj, int i) {
                a((List<MediaBrowserCompat.MediaItem>) ((List) obj), i);
            }

            /* access modifiers changed from: package-private */
            public void a(List<MediaBrowserCompat.MediaItem> list, int i) {
                if (MediaBrowserServiceCompat.this.f678b.get(bVar2.f694c.a()) == bVar2) {
                    if ((i & 1) != 0) {
                        list = MediaBrowserServiceCompat.this.a(list, bundle2);
                    }
                    try {
                        bVar2.f694c.a(str2, list, bundle2);
                    } catch (RemoteException e2) {
                        Log.w("MBServiceCompat", "Calling onLoadChildren() failed for id=" + str2 + " package=" + bVar2.f692a);
                    }
                } else if (MediaBrowserServiceCompat.f677a) {
                    Log.d("MBServiceCompat", "Not sending onLoadChildren result for connection that has been disconnected. pkg=" + bVar2.f692a + " id=" + str2);
                }
            }
        };
        this.f679c = bVar;
        if (bundle == null) {
            a(str, r0);
        } else {
            a(str, r0, bundle);
        }
        this.f679c = null;
        if (!r0.a()) {
            throw new IllegalStateException("onLoadChildren must call detach() or sendResult() before returning for package=" + bVar.f692a + " id=" + str);
        }
    }

    /* access modifiers changed from: package-private */
    public List<MediaBrowserCompat.MediaItem> a(List<MediaBrowserCompat.MediaItem> list, Bundle bundle) {
        if (list == null) {
            return null;
        }
        int i = bundle.getInt("android.media.browse.extra.PAGE", -1);
        int i2 = bundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
        if (i == -1 && i2 == -1) {
            return list;
        }
        int i3 = i2 * i;
        int i4 = i3 + i2;
        if (i < 0 || i2 < 1 || i3 >= list.size()) {
            return Collections.EMPTY_LIST;
        }
        if (i4 > list.size()) {
            i4 = list.size();
        }
        return list.subList(i3, i4);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, b bVar, final ResultReceiver resultReceiver) {
        AnonymousClass2 r0 = new c<MediaBrowserCompat.MediaItem>(str) {
            /* access modifiers changed from: package-private */
            public void a(MediaBrowserCompat.MediaItem mediaItem, int i) {
                if ((i & 2) != 0) {
                    resultReceiver.b(-1, null);
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putParcelable("media_item", mediaItem);
                resultReceiver.b(0, bundle);
            }
        };
        this.f679c = bVar;
        b(str, r0);
        this.f679c = null;
        if (!r0.a()) {
            throw new IllegalStateException("onLoadItem must call detach() or sendResult() before returning for id=" + str);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, Bundle bundle, b bVar, final ResultReceiver resultReceiver) {
        AnonymousClass3 r0 = new c<List<MediaBrowserCompat.MediaItem>>(str) {
            /* access modifiers changed from: package-private */
            public /* bridge */ /* synthetic */ void a(Object obj, int i) {
                a((List<MediaBrowserCompat.MediaItem>) ((List) obj), i);
            }

            /* access modifiers changed from: package-private */
            public void a(List<MediaBrowserCompat.MediaItem> list, int i) {
                if ((i & 4) != 0 || list == null) {
                    resultReceiver.b(-1, null);
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putParcelableArray("search_results", (Parcelable[]) list.toArray(new MediaBrowserCompat.MediaItem[0]));
                resultReceiver.b(0, bundle);
            }
        };
        this.f679c = bVar;
        a(str, bundle, r0);
        this.f679c = null;
        if (!r0.a()) {
            throw new IllegalStateException("onSearch must call detach() or sendResult() before returning for query=" + str);
        }
    }

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        private final String f690a;

        /* renamed from: b  reason: collision with root package name */
        private final Bundle f691b;

        public String a() {
            return this.f690a;
        }

        public Bundle b() {
            return this.f691b;
        }
    }
}
