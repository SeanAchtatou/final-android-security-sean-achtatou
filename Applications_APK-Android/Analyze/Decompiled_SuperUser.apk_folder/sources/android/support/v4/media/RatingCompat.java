package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

public final class RatingCompat implements Parcelable {
    public static final Parcelable.Creator<RatingCompat> CREATOR = new Parcelable.Creator<RatingCompat>() {
        /* renamed from: a */
        public RatingCompat createFromParcel(Parcel parcel) {
            return new RatingCompat(parcel.readInt(), parcel.readFloat());
        }

        /* renamed from: a */
        public RatingCompat[] newArray(int i) {
            return new RatingCompat[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final int f760a;

    /* renamed from: b  reason: collision with root package name */
    private final float f761b;

    RatingCompat(int i, float f2) {
        this.f760a = i;
        this.f761b = f2;
    }

    public String toString() {
        String valueOf;
        StringBuilder append = new StringBuilder().append("Rating:style=").append(this.f760a).append(" rating=");
        if (this.f761b < 0.0f) {
            valueOf = "unrated";
        } else {
            valueOf = String.valueOf(this.f761b);
        }
        return append.append(valueOf).toString();
    }

    public int describeContents() {
        return this.f760a;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f760a);
        parcel.writeFloat(this.f761b);
    }
}
