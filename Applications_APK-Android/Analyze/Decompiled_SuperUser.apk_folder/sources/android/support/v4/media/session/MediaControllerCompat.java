package android.support.v4.media.session;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.support.v4.app.l;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.IMediaControllerCallback;
import android.support.v4.media.session.IMediaSession;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.a;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

public final class MediaControllerCompat {

    public static abstract class a implements IBinder.DeathRecipient {

        /* renamed from: a  reason: collision with root package name */
        C0018a f779a;

        /* renamed from: b  reason: collision with root package name */
        boolean f780b;

        /* renamed from: c  reason: collision with root package name */
        boolean f781c = false;

        /* renamed from: d  reason: collision with root package name */
        private final Object f782d;

        public a() {
            if (Build.VERSION.SDK_INT >= 21) {
                this.f782d = a.a(new b());
            } else {
                this.f782d = new c();
            }
        }

        public void a() {
        }

        public void a(String str, Bundle bundle) {
        }

        public void a(PlaybackStateCompat playbackStateCompat) {
        }

        public void a(MediaMetadataCompat mediaMetadataCompat) {
        }

        public void a(List<MediaSessionCompat.QueueItem> list) {
        }

        public void a(CharSequence charSequence) {
        }

        public void a(Bundle bundle) {
        }

        public void a(b bVar) {
        }

        public void a(int i) {
        }

        public void a(boolean z) {
        }

        private class b implements a.C0019a {
            b() {
            }

            public void a() {
                a.this.a();
            }

            public void a(String str, Bundle bundle) {
                if (!a.this.f780b || Build.VERSION.SDK_INT >= 23) {
                    a.this.a(str, bundle);
                }
            }

            public void a(Object obj) {
                if (!a.this.f780b) {
                    a.this.a(PlaybackStateCompat.a(obj));
                }
            }

            public void b(Object obj) {
                a.this.a(MediaMetadataCompat.a(obj));
            }

            public void a(List<?> list) {
                a.this.a(MediaSessionCompat.QueueItem.a(list));
            }

            public void a(CharSequence charSequence) {
                a.this.a(charSequence);
            }

            public void a(Bundle bundle) {
                a.this.a(bundle);
            }

            public void a(int i, int i2, int i3, int i4, int i5) {
                a.this.a(new b(i, i2, i3, i4, i5));
            }
        }

        private class c extends IMediaControllerCallback.Stub {
            c() {
            }

            public void a(String str, Bundle bundle) {
                a.this.f779a.a(1, str, bundle);
            }

            public void a() {
                a.this.f779a.a(8, null, null);
            }

            public void a(PlaybackStateCompat playbackStateCompat) {
                a.this.f779a.a(2, playbackStateCompat, null);
            }

            public void a(MediaMetadataCompat mediaMetadataCompat) {
                a.this.f779a.a(3, mediaMetadataCompat, null);
            }

            public void a(List<MediaSessionCompat.QueueItem> list) {
                a.this.f779a.a(5, list, null);
            }

            public void a(CharSequence charSequence) {
                a.this.f779a.a(6, charSequence, null);
            }

            public void a(int i) {
                a.this.f779a.a(9, Integer.valueOf(i), null);
            }

            public void a(boolean z) {
                a.this.f779a.a(10, Boolean.valueOf(z), null);
            }

            public void a(Bundle bundle) {
                a.this.f779a.a(7, bundle, null);
            }

            public void a(ParcelableVolumeInfo parcelableVolumeInfo) {
                b bVar;
                if (parcelableVolumeInfo != null) {
                    bVar = new b(parcelableVolumeInfo.f796a, parcelableVolumeInfo.f797b, parcelableVolumeInfo.f798c, parcelableVolumeInfo.f799d, parcelableVolumeInfo.f800e);
                } else {
                    bVar = null;
                }
                a.this.f779a.a(4, bVar, null);
            }
        }

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$a$a  reason: collision with other inner class name */
        private class C0018a extends Handler {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ a f783a;

            public void handleMessage(Message message) {
                if (this.f783a.f781c) {
                    switch (message.what) {
                        case 1:
                            this.f783a.a((String) message.obj, message.getData());
                            return;
                        case 2:
                            this.f783a.a((PlaybackStateCompat) message.obj);
                            return;
                        case 3:
                            this.f783a.a((MediaMetadataCompat) message.obj);
                            return;
                        case 4:
                            this.f783a.a((b) message.obj);
                            return;
                        case 5:
                            this.f783a.a((List) message.obj);
                            return;
                        case 6:
                            this.f783a.a((CharSequence) message.obj);
                            return;
                        case 7:
                            this.f783a.a((Bundle) message.obj);
                            return;
                        case 8:
                            this.f783a.a();
                            return;
                        case 9:
                            this.f783a.a(((Integer) message.obj).intValue());
                            return;
                        case 10:
                            this.f783a.a(((Boolean) message.obj).booleanValue());
                            return;
                        default:
                            return;
                    }
                }
            }

            public void a(int i, Object obj, Bundle bundle) {
                Message obtainMessage = obtainMessage(i, obj);
                obtainMessage.setData(bundle);
                obtainMessage.sendToTarget();
            }
        }
    }

    public static final class b {

        /* renamed from: a  reason: collision with root package name */
        private final int f786a;

        /* renamed from: b  reason: collision with root package name */
        private final int f787b;

        /* renamed from: c  reason: collision with root package name */
        private final int f788c;

        /* renamed from: d  reason: collision with root package name */
        private final int f789d;

        /* renamed from: e  reason: collision with root package name */
        private final int f790e;

        b(int i, int i2, int i3, int i4, int i5) {
            this.f786a = i;
            this.f787b = i2;
            this.f788c = i3;
            this.f789d = i4;
            this.f790e = i5;
        }
    }

    static class MediaControllerImplApi21 {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public IMediaSession f764a;

        /* renamed from: b  reason: collision with root package name */
        private HashMap<a, a> f765b;

        /* renamed from: c  reason: collision with root package name */
        private List<a> f766c;

        /* access modifiers changed from: private */
        public void a() {
            if (this.f764a != null) {
                synchronized (this.f766c) {
                    for (a next : this.f766c) {
                        a aVar = new a(next);
                        this.f765b.put(next, aVar);
                        next.f780b = true;
                        try {
                            this.f764a.a(aVar);
                        } catch (RemoteException e2) {
                            Log.e("MediaControllerCompat", "Dead object in registerCallback.", e2);
                        }
                    }
                    this.f766c.clear();
                }
            }
        }

        private static class ExtraBinderRequestResultReceiver extends ResultReceiver {

            /* renamed from: a  reason: collision with root package name */
            private WeakReference<MediaControllerImplApi21> f767a;

            /* access modifiers changed from: protected */
            public void onReceiveResult(int i, Bundle bundle) {
                MediaControllerImplApi21 mediaControllerImplApi21 = this.f767a.get();
                if (mediaControllerImplApi21 != null && bundle != null) {
                    IMediaSession unused = mediaControllerImplApi21.f764a = IMediaSession.Stub.a(l.a(bundle, "android.support.v4.media.session.EXTRA_BINDER"));
                    mediaControllerImplApi21.a();
                }
            }
        }

        private class a extends IMediaControllerCallback.Stub {
            /* access modifiers changed from: private */

            /* renamed from: b  reason: collision with root package name */
            public a f769b;

            a(a aVar) {
                this.f769b = aVar;
            }

            public void a(final String str, final Bundle bundle) {
                this.f769b.f779a.post(new Runnable() {
                    public void run() {
                        a.this.f769b.a(str, bundle);
                    }
                });
            }

            public void a() {
                throw new AssertionError();
            }

            public void a(final PlaybackStateCompat playbackStateCompat) {
                this.f769b.f779a.post(new Runnable() {
                    public void run() {
                        a.this.f769b.a(playbackStateCompat);
                    }
                });
            }

            public void a(MediaMetadataCompat mediaMetadataCompat) {
                throw new AssertionError();
            }

            public void a(List<MediaSessionCompat.QueueItem> list) {
                throw new AssertionError();
            }

            public void a(CharSequence charSequence) {
                throw new AssertionError();
            }

            public void a(final int i) {
                this.f769b.f779a.post(new Runnable() {
                    public void run() {
                        a.this.f769b.a(i);
                    }
                });
            }

            public void a(final boolean z) {
                this.f769b.f779a.post(new Runnable() {
                    public void run() {
                        a.this.f769b.a(z);
                    }
                });
            }

            public void a(Bundle bundle) {
                throw new AssertionError();
            }

            public void a(ParcelableVolumeInfo parcelableVolumeInfo) {
                throw new AssertionError();
            }
        }
    }
}
