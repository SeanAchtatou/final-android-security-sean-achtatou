package android.support.v4.media.session;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.session.d;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public final class PlaybackStateCompat implements Parcelable {
    public static final Parcelable.Creator<PlaybackStateCompat> CREATOR = new Parcelable.Creator<PlaybackStateCompat>() {
        /* renamed from: a */
        public PlaybackStateCompat createFromParcel(Parcel parcel) {
            return new PlaybackStateCompat(parcel);
        }

        /* renamed from: a */
        public PlaybackStateCompat[] newArray(int i) {
            return new PlaybackStateCompat[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final int f801a;

    /* renamed from: b  reason: collision with root package name */
    final long f802b;

    /* renamed from: c  reason: collision with root package name */
    final long f803c;

    /* renamed from: d  reason: collision with root package name */
    final float f804d;

    /* renamed from: e  reason: collision with root package name */
    final long f805e;

    /* renamed from: f  reason: collision with root package name */
    final int f806f;

    /* renamed from: g  reason: collision with root package name */
    final CharSequence f807g;

    /* renamed from: h  reason: collision with root package name */
    final long f808h;
    List<CustomAction> i;
    final long j;
    final Bundle k;
    private Object l;

    PlaybackStateCompat(int i2, long j2, long j3, float f2, long j4, int i3, CharSequence charSequence, long j5, List<CustomAction> list, long j6, Bundle bundle) {
        this.f801a = i2;
        this.f802b = j2;
        this.f803c = j3;
        this.f804d = f2;
        this.f805e = j4;
        this.f806f = i3;
        this.f807g = charSequence;
        this.f808h = j5;
        this.i = new ArrayList(list);
        this.j = j6;
        this.k = bundle;
    }

    PlaybackStateCompat(Parcel parcel) {
        this.f801a = parcel.readInt();
        this.f802b = parcel.readLong();
        this.f804d = parcel.readFloat();
        this.f808h = parcel.readLong();
        this.f803c = parcel.readLong();
        this.f805e = parcel.readLong();
        this.f807g = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.i = parcel.createTypedArrayList(CustomAction.CREATOR);
        this.j = parcel.readLong();
        this.k = parcel.readBundle();
        this.f806f = parcel.readInt();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PlaybackState {");
        sb.append("state=").append(this.f801a);
        sb.append(", position=").append(this.f802b);
        sb.append(", buffered position=").append(this.f803c);
        sb.append(", speed=").append(this.f804d);
        sb.append(", updated=").append(this.f808h);
        sb.append(", actions=").append(this.f805e);
        sb.append(", error code=").append(this.f806f);
        sb.append(", error message=").append(this.f807g);
        sb.append(", custom actions=").append(this.i);
        sb.append(", active item id=").append(this.j);
        sb.append("}");
        return sb.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.f801a);
        parcel.writeLong(this.f802b);
        parcel.writeFloat(this.f804d);
        parcel.writeLong(this.f808h);
        parcel.writeLong(this.f803c);
        parcel.writeLong(this.f805e);
        TextUtils.writeToParcel(this.f807g, parcel, i2);
        parcel.writeTypedList(this.i);
        parcel.writeLong(this.j);
        parcel.writeBundle(this.k);
        parcel.writeInt(this.f806f);
    }

    public static PlaybackStateCompat a(Object obj) {
        if (obj == null || Build.VERSION.SDK_INT < 21) {
            return null;
        }
        List<Object> h2 = d.h(obj);
        ArrayList arrayList = null;
        if (h2 != null) {
            arrayList = new ArrayList(h2.size());
            for (Object a2 : h2) {
                arrayList.add(CustomAction.a(a2));
            }
        }
        PlaybackStateCompat playbackStateCompat = new PlaybackStateCompat(d.a(obj), d.b(obj), d.c(obj), d.d(obj), d.e(obj), 0, d.f(obj), d.g(obj), arrayList, d.i(obj), Build.VERSION.SDK_INT >= 22 ? e.a(obj) : null);
        playbackStateCompat.l = obj;
        return playbackStateCompat;
    }

    public static final class CustomAction implements Parcelable {
        public static final Parcelable.Creator<CustomAction> CREATOR = new Parcelable.Creator<CustomAction>() {
            /* renamed from: a */
            public CustomAction createFromParcel(Parcel parcel) {
                return new CustomAction(parcel);
            }

            /* renamed from: a */
            public CustomAction[] newArray(int i) {
                return new CustomAction[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private final String f809a;

        /* renamed from: b  reason: collision with root package name */
        private final CharSequence f810b;

        /* renamed from: c  reason: collision with root package name */
        private final int f811c;

        /* renamed from: d  reason: collision with root package name */
        private final Bundle f812d;

        /* renamed from: e  reason: collision with root package name */
        private Object f813e;

        CustomAction(String str, CharSequence charSequence, int i, Bundle bundle) {
            this.f809a = str;
            this.f810b = charSequence;
            this.f811c = i;
            this.f812d = bundle;
        }

        CustomAction(Parcel parcel) {
            this.f809a = parcel.readString();
            this.f810b = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
            this.f811c = parcel.readInt();
            this.f812d = parcel.readBundle();
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f809a);
            TextUtils.writeToParcel(this.f810b, parcel, i);
            parcel.writeInt(this.f811c);
            parcel.writeBundle(this.f812d);
        }

        public int describeContents() {
            return 0;
        }

        public static CustomAction a(Object obj) {
            if (obj == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            CustomAction customAction = new CustomAction(d.a.a(obj), d.a.b(obj), d.a.c(obj), d.a.d(obj));
            customAction.f813e = obj;
            return customAction;
        }

        public String toString() {
            return "Action:mName='" + ((Object) this.f810b) + ", mIcon=" + this.f811c + ", mExtras=" + this.f812d;
        }
    }
}
