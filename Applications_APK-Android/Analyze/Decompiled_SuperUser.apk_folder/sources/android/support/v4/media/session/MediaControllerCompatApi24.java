package android.support.v4.media.session;

import android.annotation.TargetApi;
import android.support.v4.media.session.MediaControllerCompatApi23;

@TargetApi(24)
class MediaControllerCompatApi24 {

    public static class TransportControls extends MediaControllerCompatApi23.TransportControls {
    }
}
