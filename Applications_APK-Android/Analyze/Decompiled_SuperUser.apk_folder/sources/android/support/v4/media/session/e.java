package android.support.v4.media.session;

import android.annotation.TargetApi;
import android.media.session.PlaybackState;
import android.os.Bundle;

@TargetApi(22)
/* compiled from: PlaybackStateCompatApi22 */
class e {
    public static Bundle a(Object obj) {
        return ((PlaybackState) obj).getExtras();
    }
}
