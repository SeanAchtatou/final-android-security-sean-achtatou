package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableVolumeInfo implements Parcelable {
    public static final Parcelable.Creator<ParcelableVolumeInfo> CREATOR = new Parcelable.Creator<ParcelableVolumeInfo>() {
        /* renamed from: a */
        public ParcelableVolumeInfo createFromParcel(Parcel parcel) {
            return new ParcelableVolumeInfo(parcel);
        }

        /* renamed from: a */
        public ParcelableVolumeInfo[] newArray(int i) {
            return new ParcelableVolumeInfo[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public int f796a;

    /* renamed from: b  reason: collision with root package name */
    public int f797b;

    /* renamed from: c  reason: collision with root package name */
    public int f798c;

    /* renamed from: d  reason: collision with root package name */
    public int f799d;

    /* renamed from: e  reason: collision with root package name */
    public int f800e;

    public ParcelableVolumeInfo(Parcel parcel) {
        this.f796a = parcel.readInt();
        this.f798c = parcel.readInt();
        this.f799d = parcel.readInt();
        this.f800e = parcel.readInt();
        this.f797b = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f796a);
        parcel.writeInt(this.f798c);
        parcel.writeInt(this.f799d);
        parcel.writeInt(this.f800e);
        parcel.writeInt(this.f797b);
    }
}
