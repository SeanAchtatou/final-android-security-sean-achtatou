package android.support.v4.internal.view;

import android.support.v4.view.ActionProvider;
import android.support.v4.view.p;
import android.view.MenuItem;
import android.view.View;

/* compiled from: SupportMenuItem */
public interface b extends MenuItem {
    b a(ActionProvider actionProvider);

    b a(p.e eVar);

    ActionProvider a();

    boolean collapseActionView();

    boolean expandActionView();

    View getActionView();

    boolean isActionViewExpanded();

    MenuItem setActionView(int i);

    MenuItem setActionView(View view);

    void setShowAsAction(int i);

    MenuItem setShowAsActionFlags(int i);
}
