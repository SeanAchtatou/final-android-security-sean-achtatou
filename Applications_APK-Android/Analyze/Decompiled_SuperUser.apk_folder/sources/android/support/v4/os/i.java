package android.support.v4.os;

import android.annotation.TargetApi;
import android.os.Parcelable;

@TargetApi(13)
/* compiled from: ParcelableCompatHoneycombMR2 */
class i {
    static <T> Parcelable.Creator<T> a(g<T> gVar) {
        return new h(gVar);
    }
}
