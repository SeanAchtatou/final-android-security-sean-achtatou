package android.support.v4.os;

import android.os.Build;

/* compiled from: TraceCompat */
public final class j {
    public static void a(String str) {
        if (Build.VERSION.SDK_INT >= 18) {
            k.a(str);
        }
    }

    public static void a() {
        if (Build.VERSION.SDK_INT >= 18) {
            k.a();
        }
    }
}
