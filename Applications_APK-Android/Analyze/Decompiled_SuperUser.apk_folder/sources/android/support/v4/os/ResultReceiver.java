package android.support.v4.os;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.os.IResultReceiver;

public class ResultReceiver implements Parcelable {
    public static final Parcelable.Creator<ResultReceiver> CREATOR = new Parcelable.Creator<ResultReceiver>() {
        /* renamed from: a */
        public ResultReceiver createFromParcel(Parcel parcel) {
            return new ResultReceiver(parcel);
        }

        /* renamed from: a */
        public ResultReceiver[] newArray(int i) {
            return new ResultReceiver[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final boolean f816a = false;

    /* renamed from: b  reason: collision with root package name */
    final Handler f817b = null;

    /* renamed from: c  reason: collision with root package name */
    IResultReceiver f818c;

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final int f820a;

        /* renamed from: b  reason: collision with root package name */
        final Bundle f821b;

        b(int i, Bundle bundle) {
            this.f820a = i;
            this.f821b = bundle;
        }

        public void run() {
            ResultReceiver.this.a(this.f820a, this.f821b);
        }
    }

    class a extends IResultReceiver.Stub {
        a() {
        }

        public void a(int i, Bundle bundle) {
            if (ResultReceiver.this.f817b != null) {
                ResultReceiver.this.f817b.post(new b(i, bundle));
            } else {
                ResultReceiver.this.a(i, bundle);
            }
        }
    }

    public void b(int i, Bundle bundle) {
        if (this.f816a) {
            if (this.f817b != null) {
                this.f817b.post(new b(i, bundle));
            } else {
                a(i, bundle);
            }
        } else if (this.f818c != null) {
            try {
                this.f818c.a(i, bundle);
            } catch (RemoteException e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i, Bundle bundle) {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.f818c == null) {
                this.f818c = new a();
            }
            parcel.writeStrongBinder(this.f818c.asBinder());
        }
    }

    ResultReceiver(Parcel parcel) {
        this.f818c = IResultReceiver.Stub.a(parcel.readStrongBinder());
    }
}
