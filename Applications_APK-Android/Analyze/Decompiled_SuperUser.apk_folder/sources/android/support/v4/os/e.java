package android.support.v4.os;

import android.annotation.TargetApi;
import android.os.CancellationSignal;

@TargetApi(16)
/* compiled from: CancellationSignalCompatJellybean */
class e {
    public static Object a() {
        return new CancellationSignal();
    }

    public static void a(Object obj) {
        ((CancellationSignal) obj).cancel();
    }
}
