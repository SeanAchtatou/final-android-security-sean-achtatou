package android.support.v4.os;

import android.annotation.TargetApi;
import android.os.AsyncTask;

@TargetApi(11)
/* compiled from: AsyncTaskCompatHoneycomb */
class b {
    static <Params, Progress, Result> void a(AsyncTask<Params, Progress, Result> asyncTask, Params... paramsArr) {
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, paramsArr);
    }
}
