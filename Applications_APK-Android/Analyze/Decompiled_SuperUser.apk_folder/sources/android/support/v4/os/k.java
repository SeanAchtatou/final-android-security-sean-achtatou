package android.support.v4.os;

import android.annotation.TargetApi;
import android.os.Trace;

@TargetApi(18)
/* compiled from: TraceJellybeanMR2 */
class k {
    public static void a(String str) {
        Trace.beginSection(str);
    }

    public static void a() {
        Trace.endSection();
    }
}
