package android.support.v4.os;

import android.annotation.TargetApi;
import android.os.Parcel;
import android.os.Parcelable;

@TargetApi(13)
/* compiled from: ParcelableCompatHoneycombMR2 */
class h<T> implements Parcelable.ClassLoaderCreator<T> {

    /* renamed from: a  reason: collision with root package name */
    private final g<T> f828a;

    public h(g<T> gVar) {
        this.f828a = gVar;
    }

    public T createFromParcel(Parcel parcel) {
        return this.f828a.createFromParcel(parcel, null);
    }

    public T createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return this.f828a.createFromParcel(parcel, classLoader);
    }

    public T[] newArray(int i) {
        return this.f828a.newArray(i);
    }
}
