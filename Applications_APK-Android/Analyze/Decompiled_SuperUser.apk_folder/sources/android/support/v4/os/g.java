package android.support.v4.os;

import android.os.Parcel;

/* compiled from: ParcelableCompatCreatorCallbacks */
public interface g<T> {
    T createFromParcel(Parcel parcel, ClassLoader classLoader);

    T[] newArray(int i);
}
