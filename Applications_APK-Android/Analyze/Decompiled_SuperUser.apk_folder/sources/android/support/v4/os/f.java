package android.support.v4.os;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ParcelableCompat */
public final class f {
    public static <T> Parcelable.Creator<T> a(g<T> gVar) {
        if (Build.VERSION.SDK_INT >= 13) {
            return i.a(gVar);
        }
        return new a(gVar);
    }

    /* compiled from: ParcelableCompat */
    static class a<T> implements Parcelable.Creator<T> {

        /* renamed from: a  reason: collision with root package name */
        final g<T> f827a;

        public a(g<T> gVar) {
            this.f827a = gVar;
        }

        public T createFromParcel(Parcel parcel) {
            return this.f827a.createFromParcel(parcel, null);
        }

        public T[] newArray(int i) {
            return this.f827a.newArray(i);
        }
    }
}
