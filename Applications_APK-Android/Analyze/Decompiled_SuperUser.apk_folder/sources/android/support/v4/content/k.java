package android.support.v4.content;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Intent;

@TargetApi(11)
/* compiled from: IntentCompatHoneycomb */
class k {
    public static Intent a(ComponentName componentName) {
        return Intent.makeMainActivity(componentName);
    }
}
