package android.support.v4.content;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import java.io.File;

/* compiled from: ContextCompat */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f650a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static TypedValue f651b;

    protected c() {
    }

    public static boolean a(Context context, Intent[] intentArr, Bundle bundle) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 16) {
            h.a(context, intentArr, bundle);
            return true;
        } else if (i < 11) {
            return false;
        } else {
            g.a(context, intentArr);
            return true;
        }
    }

    public static File[] a(Context context, String str) {
        if (Build.VERSION.SDK_INT >= 19) {
            return i.a(context, str);
        }
        return new File[]{context.getExternalFilesDir(str)};
    }

    public static File[] a(Context context) {
        if (Build.VERSION.SDK_INT >= 19) {
            return i.a(context);
        }
        return new File[]{context.getExternalCacheDir()};
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    public static final Drawable a(Context context, int i) {
        int i2;
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 21) {
            return d.a(context, i);
        }
        if (i3 >= 16) {
            return context.getResources().getDrawable(i);
        }
        synchronized (f650a) {
            if (f651b == null) {
                f651b = new TypedValue();
            }
            context.getResources().getValue(i, f651b, true);
            i2 = f651b.resourceId;
        }
        return context.getResources().getDrawable(i2);
    }

    public static final ColorStateList b(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return e.a(context, i);
        }
        return context.getResources().getColorStateList(i);
    }

    public static final int c(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return e.b(context, i);
        }
        return context.getResources().getColor(i);
    }

    public static boolean b(Context context) {
        if (android.support.v4.os.c.a()) {
            return f.a(context);
        }
        return false;
    }
}
