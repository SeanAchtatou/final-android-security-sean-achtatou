package android.support.v4.content;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.os.OperationCanceledException;
import android.support.v4.util.k;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

public abstract class AsyncTaskLoader<D> extends Loader<D> {

    /* renamed from: a  reason: collision with root package name */
    volatile AsyncTaskLoader<D>.a f610a;

    /* renamed from: b  reason: collision with root package name */
    volatile AsyncTaskLoader<D>.a f611b;

    /* renamed from: c  reason: collision with root package name */
    long f612c;

    /* renamed from: d  reason: collision with root package name */
    long f613d;

    /* renamed from: e  reason: collision with root package name */
    Handler f614e;

    /* renamed from: f  reason: collision with root package name */
    private final Executor f615f;

    public abstract D d();

    final class a extends ModernAsyncTask<Void, Void, D> implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        boolean f616a;

        /* renamed from: d  reason: collision with root package name */
        private final CountDownLatch f618d = new CountDownLatch(1);

        a() {
        }

        /* access modifiers changed from: protected */
        public D a(Void... voidArr) {
            try {
                return AsyncTaskLoader.this.e();
            } catch (OperationCanceledException e2) {
                if (c()) {
                    return null;
                }
                throw e2;
            }
        }

        /* access modifiers changed from: protected */
        public void a(D d2) {
            try {
                AsyncTaskLoader.this.b(this, d2);
            } finally {
                this.f618d.countDown();
            }
        }

        /* access modifiers changed from: protected */
        public void b(D d2) {
            try {
                AsyncTaskLoader.this.a(this, d2);
            } finally {
                this.f618d.countDown();
            }
        }

        public void run() {
            this.f616a = false;
            AsyncTaskLoader.this.c();
        }
    }

    public AsyncTaskLoader(Context context) {
        this(context, ModernAsyncTask.f631c);
    }

    private AsyncTaskLoader(Context context, Executor executor) {
        super(context);
        this.f613d = -10000;
        this.f615f = executor;
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        r();
        this.f610a = new a();
        c();
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        boolean z = false;
        if (this.f610a != null) {
            if (!this.r) {
                this.u = true;
            }
            if (this.f611b != null) {
                if (this.f610a.f616a) {
                    this.f610a.f616a = false;
                    this.f614e.removeCallbacks(this.f610a);
                }
                this.f610a = null;
            } else if (this.f610a.f616a) {
                this.f610a.f616a = false;
                this.f614e.removeCallbacks(this.f610a);
                this.f610a = null;
            } else {
                z = this.f610a.a(false);
                if (z) {
                    this.f611b = this.f610a;
                    f();
                }
                this.f610a = null;
            }
        }
        return z;
    }

    public void a(D d2) {
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f611b == null && this.f610a != null) {
            if (this.f610a.f616a) {
                this.f610a.f616a = false;
                this.f614e.removeCallbacks(this.f610a);
            }
            if (this.f612c <= 0 || SystemClock.uptimeMillis() >= this.f613d + this.f612c) {
                this.f610a.a(this.f615f, null);
                return;
            }
            this.f610a.f616a = true;
            this.f614e.postAtTime(this.f610a, this.f613d + this.f612c);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(AsyncTaskLoader<D>.a aVar, D d2) {
        a((Object) d2);
        if (this.f611b == aVar) {
            x();
            this.f613d = SystemClock.uptimeMillis();
            this.f611b = null;
            l();
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(AsyncTaskLoader<D>.a aVar, D d2) {
        if (this.f610a != aVar) {
            a(aVar, d2);
        } else if (o()) {
            a((Object) d2);
        } else {
            w();
            this.f613d = SystemClock.uptimeMillis();
            this.f610a = null;
            b((Object) d2);
        }
    }

    /* access modifiers changed from: protected */
    public D e() {
        return d();
    }

    public void f() {
    }

    public boolean g() {
        return this.f611b != null;
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.a(str, fileDescriptor, printWriter, strArr);
        if (this.f610a != null) {
            printWriter.print(str);
            printWriter.print("mTask=");
            printWriter.print(this.f610a);
            printWriter.print(" waiting=");
            printWriter.println(this.f610a.f616a);
        }
        if (this.f611b != null) {
            printWriter.print(str);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.f611b);
            printWriter.print(" waiting=");
            printWriter.println(this.f611b.f616a);
        }
        if (this.f612c != 0) {
            printWriter.print(str);
            printWriter.print("mUpdateThrottle=");
            k.a(this.f612c, printWriter);
            printWriter.print(" mLastLoadCompleteTime=");
            k.a(this.f613d, SystemClock.uptimeMillis(), printWriter);
            printWriter.println();
        }
    }
}
