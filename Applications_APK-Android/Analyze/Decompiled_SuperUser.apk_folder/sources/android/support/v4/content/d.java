package android.support.v4.content;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;

@TargetApi(21)
/* compiled from: ContextCompatApi21 */
class d {
    public static Drawable a(Context context, int i) {
        return context.getDrawable(i);
    }
}
