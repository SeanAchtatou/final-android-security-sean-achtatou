package android.support.v4.content;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.Loader;
import android.support.v4.os.OperationCanceledException;
import android.support.v4.os.d;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;

public class CursorLoader extends AsyncTaskLoader<Cursor> {

    /* renamed from: f  reason: collision with root package name */
    final Loader<Cursor>.a f619f = new Loader.a();

    /* renamed from: g  reason: collision with root package name */
    Uri f620g;

    /* renamed from: h  reason: collision with root package name */
    String[] f621h;
    String i;
    String[] j;
    String k;
    Cursor l;
    d m;

    /* renamed from: h */
    public Cursor d() {
        Cursor a2;
        synchronized (this) {
            if (g()) {
                throw new OperationCanceledException();
            }
            this.m = new d();
        }
        try {
            a2 = a.a(m().getContentResolver(), this.f620g, this.f621h, this.i, this.j, this.k, this.m);
            if (a2 != null) {
                a2.getCount();
                a2.registerContentObserver(this.f619f);
            }
            synchronized (this) {
                this.m = null;
            }
            return a2;
        } catch (RuntimeException e2) {
            a2.close();
            throw e2;
        } catch (Throwable th) {
            synchronized (this) {
                this.m = null;
                throw th;
            }
        }
    }

    public void f() {
        super.f();
        synchronized (this) {
            if (this.m != null) {
                this.m.c();
            }
        }
    }

    /* renamed from: a */
    public void b(Cursor cursor) {
        if (!p()) {
            Cursor cursor2 = this.l;
            this.l = cursor;
            if (n()) {
                super.b((Object) cursor);
            }
            if (cursor2 != null && cursor2 != cursor && !cursor2.isClosed()) {
                cursor2.close();
            }
        } else if (cursor != null) {
            cursor.close();
        }
    }

    public CursorLoader(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.l != null) {
            b(this.l);
        }
        if (v() || this.l == null) {
            s();
        }
    }

    /* access modifiers changed from: protected */
    public void j() {
        r();
    }

    /* renamed from: b */
    public void a(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    /* access modifiers changed from: protected */
    public void k() {
        super.k();
        j();
        if (this.l != null && !this.l.isClosed()) {
            this.l.close();
        }
        this.l = null;
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.a(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("mUri=");
        printWriter.println(this.f620g);
        printWriter.print(str);
        printWriter.print("mProjection=");
        printWriter.println(Arrays.toString(this.f621h));
        printWriter.print(str);
        printWriter.print("mSelection=");
        printWriter.println(this.i);
        printWriter.print(str);
        printWriter.print("mSelectionArgs=");
        printWriter.println(Arrays.toString(this.j));
        printWriter.print(str);
        printWriter.print("mSortOrder=");
        printWriter.println(this.k);
        printWriter.print(str);
        printWriter.print("mCursor=");
        printWriter.println(this.l);
        printWriter.print(str);
        printWriter.print("mContentChanged=");
        printWriter.println(this.u);
    }
}
