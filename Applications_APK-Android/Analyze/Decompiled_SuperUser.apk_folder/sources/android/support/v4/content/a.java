package android.support.v4.content;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.v4.os.OperationCanceledException;
import android.support.v4.os.d;

/* compiled from: ContentResolverCompat */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final C0016a f648a;

    /* renamed from: android.support.v4.content.a$a  reason: collision with other inner class name */
    /* compiled from: ContentResolverCompat */
    interface C0016a {
        Cursor a(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, d dVar);
    }

    /* compiled from: ContentResolverCompat */
    static class b implements C0016a {
        b() {
        }

        public Cursor a(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, d dVar) {
            if (dVar != null) {
                dVar.b();
            }
            return contentResolver.query(uri, strArr, str, strArr2, str2);
        }
    }

    /* compiled from: ContentResolverCompat */
    static class c extends b {
        c() {
        }

        public Cursor a(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, d dVar) {
            Object obj;
            if (dVar != null) {
                try {
                    obj = dVar.d();
                } catch (Exception e2) {
                    if (b.a(e2)) {
                        throw new OperationCanceledException();
                    }
                    throw e2;
                }
            } else {
                obj = null;
            }
            return b.a(contentResolver, uri, strArr, str, strArr2, str2, obj);
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f648a = new c();
        } else {
            f648a = new b();
        }
    }

    public static Cursor a(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, d dVar) {
        return f648a.a(contentResolver, uri, strArr, str, strArr2, str2, dVar);
    }
}
