package android.support.v4.content;

import android.annotation.TargetApi;
import android.content.Context;

@TargetApi(24)
/* compiled from: ContextCompatApi24 */
class f {
    public static boolean a(Context context) {
        return context.isDeviceProtectedStorage();
    }
}
