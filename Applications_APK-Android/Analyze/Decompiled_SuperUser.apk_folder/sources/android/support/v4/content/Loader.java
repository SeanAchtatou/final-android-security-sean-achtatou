package android.support.v4.content;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class Loader<D> {
    int n;
    c<D> o;
    b<D> p;
    Context q;
    boolean r = false;
    boolean s = false;
    boolean t = true;
    boolean u = false;
    boolean v = false;

    public interface b<D> {
        void a(Loader<D> loader);
    }

    public interface c<D> {
        void a(Loader<D> loader, D d2);
    }

    public final class a extends ContentObserver {
        public a() {
            super(new Handler());
        }

        public boolean deliverSelfNotifications() {
            return true;
        }

        public void onChange(boolean z) {
            Loader.this.y();
        }
    }

    public Loader(Context context) {
        this.q = context.getApplicationContext();
    }

    public void b(Object obj) {
        if (this.o != null) {
            this.o.a(this, obj);
        }
    }

    public void l() {
        if (this.p != null) {
            this.p.a(this);
        }
    }

    public Context m() {
        return this.q;
    }

    public void a(int i, c<D> cVar) {
        if (this.o != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.o = cVar;
        this.n = i;
    }

    public void a(c<Object> cVar) {
        if (this.o == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.o != cVar) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.o = null;
        }
    }

    public void a(b<Object> bVar) {
        if (this.p != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.p = bVar;
    }

    public void b(b<Object> bVar) {
        if (this.p == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.p != bVar) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.p = null;
        }
    }

    public boolean n() {
        return this.r;
    }

    public boolean o() {
        return this.s;
    }

    public boolean p() {
        return this.t;
    }

    public final void q() {
        this.r = true;
        this.t = false;
        this.s = false;
        i();
    }

    /* access modifiers changed from: protected */
    public void i() {
    }

    public boolean r() {
        return b();
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return false;
    }

    public void s() {
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public void t() {
        this.r = false;
        j();
    }

    /* access modifiers changed from: protected */
    public void j() {
    }

    public void u() {
        k();
        this.t = true;
        this.r = false;
        this.s = false;
        this.u = false;
        this.v = false;
    }

    /* access modifiers changed from: protected */
    public void k() {
    }

    public boolean v() {
        boolean z = this.u;
        this.u = false;
        this.v |= z;
        return z;
    }

    public void w() {
        this.v = false;
    }

    public void x() {
        if (this.v) {
            y();
        }
    }

    public void y() {
        if (this.r) {
            s();
        } else {
            this.u = true;
        }
    }

    public String c(D d2) {
        StringBuilder sb = new StringBuilder(64);
        android.support.v4.util.c.a(d2, sb);
        sb.append("}");
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        android.support.v4.util.c.a(this, sb);
        sb.append(" id=");
        sb.append(this.n);
        sb.append("}");
        return sb.toString();
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.n);
        printWriter.print(" mListener=");
        printWriter.println(this.o);
        if (this.r || this.u || this.v) {
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.r);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.u);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.v);
        }
        if (this.s || this.t) {
            printWriter.print(str);
            printWriter.print("mAbandoned=");
            printWriter.print(this.s);
            printWriter.print(" mReset=");
            printWriter.println(this.t);
        }
    }
}
