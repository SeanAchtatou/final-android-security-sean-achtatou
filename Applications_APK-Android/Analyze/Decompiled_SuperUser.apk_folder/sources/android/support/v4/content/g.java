package android.support.v4.content;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;

@TargetApi(11)
/* compiled from: ContextCompatHoneycomb */
class g {
    static void a(Context context, Intent[] intentArr) {
        context.startActivities(intentArr);
    }
}
