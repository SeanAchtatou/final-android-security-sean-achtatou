package android.support.v4.content.a;

import android.annotation.TargetApi;
import android.content.res.Resources;

@TargetApi(13)
/* compiled from: ConfigurationHelperHoneycombMr2 */
class c {
    static int a(Resources resources) {
        return resources.getConfiguration().screenHeightDp;
    }

    static int b(Resources resources) {
        return resources.getConfiguration().screenWidthDp;
    }

    static int c(Resources resources) {
        return resources.getConfiguration().smallestScreenWidthDp;
    }
}
