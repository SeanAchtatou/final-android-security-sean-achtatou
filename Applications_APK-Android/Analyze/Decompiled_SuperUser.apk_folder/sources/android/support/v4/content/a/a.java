package android.support.v4.content.a;

import android.content.res.Resources;
import android.os.Build;

/* compiled from: ConfigurationHelper */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final C0017a f649a;

    /* renamed from: android.support.v4.content.a.a$a  reason: collision with other inner class name */
    /* compiled from: ConfigurationHelper */
    private interface C0017a {
        int a(Resources resources);

        int b(Resources resources);

        int c(Resources resources);
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            f649a = new d();
        } else if (i >= 13) {
            f649a = new c();
        } else {
            f649a = new b();
        }
    }

    /* compiled from: ConfigurationHelper */
    private static class b implements C0017a {
        b() {
        }

        public int a(Resources resources) {
            return b.a(resources);
        }

        public int b(Resources resources) {
            return b.b(resources);
        }

        public int c(Resources resources) {
            return b.c(resources);
        }
    }

    /* compiled from: ConfigurationHelper */
    private static class c extends b {
        c() {
        }

        public int a(Resources resources) {
            return c.a(resources);
        }

        public int b(Resources resources) {
            return c.b(resources);
        }

        public int c(Resources resources) {
            return c.c(resources);
        }
    }

    /* compiled from: ConfigurationHelper */
    private static class d extends c {
        d() {
        }
    }

    public static int a(Resources resources) {
        return f649a.a(resources);
    }

    public static int b(Resources resources) {
        return f649a.b(resources);
    }

    public static int c(Resources resources) {
        return f649a.c(resources);
    }
}
