package android.support.v4.content;

import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import com.lody.virtual.helper.utils.FileUtils;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

abstract class ModernAsyncTask<Params, Progress, Result> {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadFactory f629a = new ThreadFactory() {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f637a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "ModernAsyncTask #" + this.f637a.getAndIncrement());
        }
    };

    /* renamed from: b  reason: collision with root package name */
    private static final BlockingQueue<Runnable> f630b = new LinkedBlockingQueue(10);

    /* renamed from: c  reason: collision with root package name */
    public static final Executor f631c = new ThreadPoolExecutor(5, (int) FileUtils.FileMode.MODE_IWUSR, 1, TimeUnit.SECONDS, f630b, f629a);

    /* renamed from: d  reason: collision with root package name */
    private static b f632d;

    /* renamed from: e  reason: collision with root package name */
    private static volatile Executor f633e = f631c;

    /* renamed from: f  reason: collision with root package name */
    private final c<Params, Result> f634f = new c<Params, Result>() {
        public Result call() {
            ModernAsyncTask.this.j.set(true);
            Result result = null;
            try {
                Process.setThreadPriority(10);
                result = ModernAsyncTask.this.a(this.f647b);
                Binder.flushPendingCommands();
                ModernAsyncTask.this.d(result);
                return result;
            } catch (Throwable th) {
                ModernAsyncTask.this.d(result);
                throw th;
            }
        }
    };

    /* renamed from: g  reason: collision with root package name */
    private final FutureTask<Result> f635g = new FutureTask<Result>(this.f634f) {
        /* access modifiers changed from: protected */
        public void done() {
            try {
                ModernAsyncTask.this.c(get());
            } catch (InterruptedException e2) {
                Log.w("AsyncTask", e2);
            } catch (ExecutionException e3) {
                throw new RuntimeException("An error occurred while executing doInBackground()", e3.getCause());
            } catch (CancellationException e4) {
                ModernAsyncTask.this.c(null);
            } catch (Throwable th) {
                throw new RuntimeException("An error occurred while executing doInBackground()", th);
            }
        }
    };

    /* renamed from: h  reason: collision with root package name */
    private volatile Status f636h = Status.PENDING;
    /* access modifiers changed from: private */
    public final AtomicBoolean i = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final AtomicBoolean j = new AtomicBoolean();

    public enum Status {
        PENDING,
        RUNNING,
        FINISHED
    }

    /* access modifiers changed from: protected */
    public abstract Result a(Object... objArr);

    private static Handler d() {
        b bVar;
        synchronized (ModernAsyncTask.class) {
            if (f632d == null) {
                f632d = new b();
            }
            bVar = f632d;
        }
        return bVar;
    }

    /* access modifiers changed from: package-private */
    public void c(Result result) {
        if (!this.j.get()) {
            d(result);
        }
    }

    /* access modifiers changed from: package-private */
    public Result d(Result result) {
        d().obtainMessage(1, new a(this, result)).sendToTarget();
        return result;
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
    }

    /* access modifiers changed from: protected */
    public void b(Progress... progressArr) {
    }

    /* access modifiers changed from: protected */
    public void b(Result result) {
        b();
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public final boolean c() {
        return this.i.get();
    }

    public final boolean a(boolean z) {
        this.i.set(true);
        return this.f635g.cancel(z);
    }

    public final ModernAsyncTask<Params, Progress, Result> a(Executor executor, Params... paramsArr) {
        if (this.f636h != Status.PENDING) {
            switch (this.f636h) {
                case RUNNING:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case FINISHED:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.f636h = Status.RUNNING;
        a();
        this.f634f.f647b = paramsArr;
        executor.execute(this.f635g);
        return this;
    }

    /* access modifiers changed from: package-private */
    public void e(Result result) {
        if (c()) {
            b((Object) result);
        } else {
            a((Object) result);
        }
        this.f636h = Status.FINISHED;
    }

    private static class b extends Handler {
        public b() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            a aVar = (a) message.obj;
            switch (message.what) {
                case 1:
                    aVar.f645a.e(aVar.f646b[0]);
                    return;
                case 2:
                    aVar.f645a.b((Object[]) aVar.f646b);
                    return;
                default:
                    return;
            }
        }
    }

    private static abstract class c<Params, Result> implements Callable<Result> {

        /* renamed from: b  reason: collision with root package name */
        Params[] f647b;

        c() {
        }
    }

    private static class a<Data> {

        /* renamed from: a  reason: collision with root package name */
        final ModernAsyncTask f645a;

        /* renamed from: b  reason: collision with root package name */
        final Data[] f646b;

        a(ModernAsyncTask modernAsyncTask, Data... dataArr) {
            this.f645a = modernAsyncTask;
            this.f646b = dataArr;
        }
    }
}
