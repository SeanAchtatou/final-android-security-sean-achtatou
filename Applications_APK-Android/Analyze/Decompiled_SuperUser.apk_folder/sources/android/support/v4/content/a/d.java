package android.support.v4.content.a;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;

/* compiled from: ResourcesCompat */
public final class d {
    public static Drawable a(Resources resources, int i, Resources.Theme theme) {
        if (Build.VERSION.SDK_INT >= 21) {
            return e.a(resources, i, theme);
        }
        return resources.getDrawable(i);
    }
}
