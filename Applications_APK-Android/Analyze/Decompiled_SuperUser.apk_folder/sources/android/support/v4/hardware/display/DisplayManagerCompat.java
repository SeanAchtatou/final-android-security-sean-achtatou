package android.support.v4.hardware.display;

import android.content.Context;
import android.view.WindowManager;
import java.util.WeakHashMap;

public abstract class DisplayManagerCompat {

    /* renamed from: a  reason: collision with root package name */
    private static final WeakHashMap<Context, DisplayManagerCompat> f666a = new WeakHashMap<>();

    DisplayManagerCompat() {
    }

    private static class LegacyImpl extends DisplayManagerCompat {

        /* renamed from: a  reason: collision with root package name */
        private final WindowManager f668a;

        public LegacyImpl(Context context) {
            this.f668a = (WindowManager) context.getSystemService("window");
        }
    }

    private static class JellybeanMr1Impl extends DisplayManagerCompat {

        /* renamed from: a  reason: collision with root package name */
        private final Object f667a;

        public JellybeanMr1Impl(Context context) {
            this.f667a = a.a(context);
        }
    }
}
