package android.support.v7.app;

import android.annotation.TargetApi;
import android.app.Notification;
import android.support.v4.app.ab;

@TargetApi(16)
/* compiled from: NotificationCompatImplJellybean */
class l {
    public static void a(ab abVar, CharSequence charSequence) {
        new Notification.BigTextStyle(abVar.a()).bigText(charSequence);
    }
}
