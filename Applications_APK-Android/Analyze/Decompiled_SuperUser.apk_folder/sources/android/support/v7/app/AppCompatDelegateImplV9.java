package android.support.v7.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.y;
import android.support.v4.os.f;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.ag;
import android.support.v4.view.ay;
import android.support.v4.view.bc;
import android.support.v4.view.be;
import android.support.v4.view.i;
import android.support.v4.view.m;
import android.support.v4.widget.q;
import android.support.v7.a.a;
import android.support.v7.view.StandaloneActionMode;
import android.support.v7.view.b;
import android.support.v7.view.menu.ListMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.i;
import android.support.v7.view.menu.j;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ViewStubCompat;
import android.support.v7.widget.ao;
import android.support.v7.widget.ar;
import android.support.v7.widget.g;
import android.support.v7.widget.s;
import android.support.v7.widget.w;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.duapps.ad.AdError;
import com.lody.virtual.helper.utils.FileUtils;
import org.xmlpull.v1.XmlPullParser;

@TargetApi(9)
class AppCompatDelegateImplV9 extends c implements m, MenuBuilder.a {
    private static final boolean t = (Build.VERSION.SDK_INT < 21);
    private View A;
    private boolean B;
    private boolean C;
    private boolean D;
    private PanelFeatureState[] E;
    private PanelFeatureState F;
    private boolean G;
    private final Runnable H = new Runnable() {
        public void run() {
            if ((AppCompatDelegateImplV9.this.s & 1) != 0) {
                AppCompatDelegateImplV9.this.f(0);
            }
            if ((AppCompatDelegateImplV9.this.s & CodedOutputStream.DEFAULT_BUFFER_SIZE) != 0) {
                AppCompatDelegateImplV9.this.f(108);
            }
            AppCompatDelegateImplV9.this.r = false;
            AppCompatDelegateImplV9.this.s = 0;
        }
    };
    private boolean I;
    private Rect J;
    private Rect K;
    private h L;
    android.support.v7.view.b m;
    ActionBarContextView n;
    PopupWindow o;
    Runnable p;
    ay q = null;
    boolean r;
    int s;
    private s u;
    private a v;
    private d w;
    private boolean x;
    private ViewGroup y;
    private TextView z;

    AppCompatDelegateImplV9(Context context, Window window, a aVar) {
        super(context, window, aVar);
    }

    public void a(Bundle bundle) {
        if ((this.f1341c instanceof Activity) && y.b((Activity) this.f1341c) != null) {
            ActionBar m2 = m();
            if (m2 == null) {
                this.I = true;
            } else {
                m2.f(true);
            }
        }
    }

    public void b(Bundle bundle) {
        w();
    }

    public void l() {
        w();
        if (this.f1346h && this.f1344f == null) {
            if (this.f1341c instanceof Activity) {
                this.f1344f = new WindowDecorActionBar((Activity) this.f1341c, this.i);
            } else if (this.f1341c instanceof Dialog) {
                this.f1344f = new WindowDecorActionBar((Dialog) this.f1341c);
            }
            if (this.f1344f != null) {
                this.f1344f.f(this.I);
            }
        }
    }

    public void a(Toolbar toolbar) {
        if (this.f1341c instanceof Activity) {
            ActionBar a2 = a();
            if (a2 instanceof WindowDecorActionBar) {
                throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
            }
            this.f1345g = null;
            if (a2 != null) {
                a2.h();
            }
            if (toolbar != null) {
                n nVar = new n(toolbar, ((Activity) this.f1341c).getTitle(), this.f1342d);
                this.f1344f = nVar;
                this.f1340b.setCallback(nVar.i());
            } else {
                this.f1344f = null;
                this.f1340b.setCallback(this.f1342d);
            }
            f();
        }
    }

    public View a(int i) {
        w();
        return this.f1340b.findViewById(i);
    }

    public void a(Configuration configuration) {
        ActionBar a2;
        if (this.f1346h && this.x && (a2 = a()) != null) {
            a2.a(configuration);
        }
        g.a().a(this.f1339a);
        i();
    }

    public void d() {
        ActionBar a2 = a();
        if (a2 != null) {
            a2.g(false);
        }
    }

    public void e() {
        ActionBar a2 = a();
        if (a2 != null) {
            a2.g(true);
        }
    }

    public void a(View view) {
        w();
        ViewGroup viewGroup = (ViewGroup) this.y.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.f1341c.onContentChanged();
    }

    public void b(int i) {
        w();
        ViewGroup viewGroup = (ViewGroup) this.y.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.f1339a).inflate(i, viewGroup);
        this.f1341c.onContentChanged();
    }

    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        w();
        ViewGroup viewGroup = (ViewGroup) this.y.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.f1341c.onContentChanged();
    }

    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        w();
        ((ViewGroup) this.y.findViewById(16908290)).addView(view, layoutParams);
        this.f1341c.onContentChanged();
    }

    public void g() {
        if (this.r) {
            this.f1340b.getDecorView().removeCallbacks(this.H);
        }
        super.g();
        if (this.f1344f != null) {
            this.f1344f.h();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
    private void w() {
        if (!this.x) {
            this.y = x();
            CharSequence r2 = r();
            if (!TextUtils.isEmpty(r2)) {
                b(r2);
            }
            y();
            a(this.y);
            this.x = true;
            PanelFeatureState a2 = a(0, false);
            if (p()) {
                return;
            }
            if (a2 == null || a2.j == null) {
                d(108);
            }
        }
    }

    private ViewGroup x() {
        ViewGroup viewGroup;
        ViewGroup viewGroup2;
        Context context;
        TypedArray obtainStyledAttributes = this.f1339a.obtainStyledAttributes(a.k.AppCompatTheme);
        if (!obtainStyledAttributes.hasValue(a.k.AppCompatTheme_windowActionBar)) {
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
        if (obtainStyledAttributes.getBoolean(a.k.AppCompatTheme_windowNoTitle, false)) {
            c(1);
        } else if (obtainStyledAttributes.getBoolean(a.k.AppCompatTheme_windowActionBar, false)) {
            c(108);
        }
        if (obtainStyledAttributes.getBoolean(a.k.AppCompatTheme_windowActionBarOverlay, false)) {
            c(109);
        }
        if (obtainStyledAttributes.getBoolean(a.k.AppCompatTheme_windowActionModeOverlay, false)) {
            c(10);
        }
        this.k = obtainStyledAttributes.getBoolean(a.k.AppCompatTheme_android_windowIsFloating, false);
        obtainStyledAttributes.recycle();
        this.f1340b.getDecorView();
        LayoutInflater from = LayoutInflater.from(this.f1339a);
        if (this.l) {
            if (this.j) {
                viewGroup = (ViewGroup) from.inflate(a.h.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
            } else {
                viewGroup = (ViewGroup) from.inflate(a.h.abc_screen_simple, (ViewGroup) null);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                ag.a(viewGroup, new android.support.v4.view.y() {
                    public be onApplyWindowInsets(View view, be beVar) {
                        int b2 = beVar.b();
                        int g2 = AppCompatDelegateImplV9.this.g(b2);
                        if (b2 != g2) {
                            beVar = beVar.a(beVar.a(), g2, beVar.c(), beVar.d());
                        }
                        return ag.a(view, beVar);
                    }
                });
                viewGroup2 = viewGroup;
            } else {
                ((w) viewGroup).setOnFitSystemWindowsListener(new w.a() {
                    public void a(Rect rect) {
                        rect.top = AppCompatDelegateImplV9.this.g(rect.top);
                    }
                });
                viewGroup2 = viewGroup;
            }
        } else if (this.k) {
            this.i = false;
            this.f1346h = false;
            viewGroup2 = (ViewGroup) from.inflate(a.h.abc_dialog_title_material, (ViewGroup) null);
        } else if (this.f1346h) {
            TypedValue typedValue = new TypedValue();
            this.f1339a.getTheme().resolveAttribute(a.C0027a.actionBarTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                context = new android.support.v7.view.d(this.f1339a, typedValue.resourceId);
            } else {
                context = this.f1339a;
            }
            ViewGroup viewGroup3 = (ViewGroup) LayoutInflater.from(context).inflate(a.h.abc_screen_toolbar, (ViewGroup) null);
            this.u = (s) viewGroup3.findViewById(a.f.decor_content_parent);
            this.u.setWindowCallback(q());
            if (this.i) {
                this.u.a(109);
            }
            if (this.B) {
                this.u.a(2);
            }
            if (this.C) {
                this.u.a(5);
            }
            viewGroup2 = viewGroup3;
        } else {
            viewGroup2 = null;
        }
        if (viewGroup2 == null) {
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.f1346h + ", windowActionBarOverlay: " + this.i + ", android:windowIsFloating: " + this.k + ", windowActionModeOverlay: " + this.j + ", windowNoTitle: " + this.l + " }");
        }
        if (this.u == null) {
            this.z = (TextView) viewGroup2.findViewById(a.f.title);
        }
        ar.b(viewGroup2);
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup2.findViewById(a.f.action_bar_activity_content);
        ViewGroup viewGroup4 = (ViewGroup) this.f1340b.findViewById(16908290);
        if (viewGroup4 != null) {
            while (viewGroup4.getChildCount() > 0) {
                View childAt = viewGroup4.getChildAt(0);
                viewGroup4.removeViewAt(0);
                contentFrameLayout.addView(childAt);
            }
            viewGroup4.setId(-1);
            contentFrameLayout.setId(16908290);
            if (viewGroup4 instanceof FrameLayout) {
                ((FrameLayout) viewGroup4).setForeground(null);
            }
        }
        this.f1340b.setContentView(viewGroup2);
        contentFrameLayout.setAttachListener(new ContentFrameLayout.a() {
            public void a() {
            }

            public void b() {
                AppCompatDelegateImplV9.this.v();
            }
        });
        return viewGroup2;
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup) {
    }

    private void y() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.y.findViewById(16908290);
        View decorView = this.f1340b.getDecorView();
        contentFrameLayout.a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.f1339a.obtainStyledAttributes(a.k.AppCompatTheme);
        obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    public boolean c(int i) {
        int h2 = h(i);
        if (this.l && h2 == 108) {
            return false;
        }
        if (this.f1346h && h2 == 1) {
            this.f1346h = false;
        }
        switch (h2) {
            case 1:
                z();
                this.l = true;
                return true;
            case 2:
                z();
                this.B = true;
                return true;
            case 5:
                z();
                this.C = true;
                return true;
            case 10:
                z();
                this.j = true;
                return true;
            case 108:
                z();
                this.f1346h = true;
                return true;
            case 109:
                z();
                this.i = true;
                return true;
            default:
                return this.f1340b.requestFeature(h2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(CharSequence charSequence) {
        if (this.u != null) {
            this.u.setWindowTitle(charSequence);
        } else if (m() != null) {
            m().b(charSequence);
        } else if (this.z != null) {
            this.z.setText(charSequence);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(int i, Menu menu) {
        if (i == 108) {
            ActionBar a2 = a();
            if (a2 != null) {
                a2.h(false);
            }
        } else if (i == 0) {
            PanelFeatureState a3 = a(i, true);
            if (a3.o) {
                a(a3, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i, Menu menu) {
        if (i != 108) {
            return false;
        }
        ActionBar a2 = a();
        if (a2 == null) {
            return true;
        }
        a2.h(true);
        return true;
    }

    public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
        PanelFeatureState a2;
        Window.Callback q2 = q();
        if (q2 == null || p() || (a2 = a((Menu) menuBuilder.getRootMenu())) == null) {
            return false;
        }
        return q2.onMenuItemSelected(a2.f1293a, menuItem);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
     arg types: [android.support.v7.view.menu.MenuBuilder, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void */
    public void onMenuModeChange(MenuBuilder menuBuilder) {
        a(menuBuilder, true);
    }

    public android.support.v7.view.b b(b.a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("ActionMode callback can not be null.");
        }
        if (this.m != null) {
            this.m.c();
        }
        b bVar = new b(aVar);
        ActionBar a2 = a();
        if (a2 != null) {
            this.m = a2.a(bVar);
            if (!(this.m == null || this.f1343e == null)) {
                this.f1343e.onSupportActionModeStarted(this.m);
            }
        }
        if (this.m == null) {
            this.m = a(bVar);
        }
        return this.m;
    }

    public void f() {
        ActionBar a2 = a();
        if (a2 == null || !a2.e()) {
            d(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, float):void
     arg types: [android.support.v7.widget.ActionBarContextView, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, int):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, float):void */
    /* access modifiers changed from: package-private */
    public android.support.v7.view.b a(b.a aVar) {
        android.support.v7.view.b bVar;
        boolean z2;
        Context context;
        t();
        if (this.m != null) {
            this.m.c();
        }
        if (!(aVar instanceof b)) {
            aVar = new b(aVar);
        }
        if (this.f1343e == null || p()) {
            bVar = null;
        } else {
            try {
                bVar = this.f1343e.onWindowStartingSupportActionMode(aVar);
            } catch (AbstractMethodError e2) {
                bVar = null;
            }
        }
        if (bVar != null) {
            this.m = bVar;
        } else {
            if (this.n == null) {
                if (this.k) {
                    TypedValue typedValue = new TypedValue();
                    Resources.Theme theme = this.f1339a.getTheme();
                    theme.resolveAttribute(a.C0027a.actionBarTheme, typedValue, true);
                    if (typedValue.resourceId != 0) {
                        Resources.Theme newTheme = this.f1339a.getResources().newTheme();
                        newTheme.setTo(theme);
                        newTheme.applyStyle(typedValue.resourceId, true);
                        context = new android.support.v7.view.d(this.f1339a, 0);
                        context.getTheme().setTo(newTheme);
                    } else {
                        context = this.f1339a;
                    }
                    this.n = new ActionBarContextView(context);
                    this.o = new PopupWindow(context, (AttributeSet) null, a.C0027a.actionModePopupWindowStyle);
                    q.a(this.o, 2);
                    this.o.setContentView(this.n);
                    this.o.setWidth(-1);
                    context.getTheme().resolveAttribute(a.C0027a.actionBarSize, typedValue, true);
                    this.n.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue.data, context.getResources().getDisplayMetrics()));
                    this.o.setHeight(-2);
                    this.p = new Runnable() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: android.support.v4.view.ag.c(android.view.View, float):void
                         arg types: [android.support.v7.widget.ActionBarContextView, int]
                         candidates:
                          android.support.v4.view.ag.c(android.view.View, int):void
                          android.support.v4.view.ag.c(android.view.View, boolean):void
                          android.support.v4.view.ag.c(android.view.View, float):void */
                        public void run() {
                            AppCompatDelegateImplV9.this.o.showAtLocation(AppCompatDelegateImplV9.this.n, 55, 0, 0);
                            AppCompatDelegateImplV9.this.t();
                            if (AppCompatDelegateImplV9.this.s()) {
                                ag.c((View) AppCompatDelegateImplV9.this.n, 0.0f);
                                AppCompatDelegateImplV9.this.q = ag.r(AppCompatDelegateImplV9.this.n).a(1.0f);
                                AppCompatDelegateImplV9.this.q.a(new ViewPropertyAnimatorListenerAdapter() {
                                    public void onAnimationStart(View view) {
                                        AppCompatDelegateImplV9.this.n.setVisibility(0);
                                    }

                                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                     method: android.support.v4.view.ag.c(android.view.View, float):void
                                     arg types: [android.support.v7.widget.ActionBarContextView, int]
                                     candidates:
                                      android.support.v4.view.ag.c(android.view.View, int):void
                                      android.support.v4.view.ag.c(android.view.View, boolean):void
                                      android.support.v4.view.ag.c(android.view.View, float):void */
                                    public void onAnimationEnd(View view) {
                                        ag.c((View) AppCompatDelegateImplV9.this.n, 1.0f);
                                        AppCompatDelegateImplV9.this.q.a((bc) null);
                                        AppCompatDelegateImplV9.this.q = null;
                                    }
                                });
                                return;
                            }
                            ag.c((View) AppCompatDelegateImplV9.this.n, 1.0f);
                            AppCompatDelegateImplV9.this.n.setVisibility(0);
                        }
                    };
                } else {
                    ViewStubCompat viewStubCompat = (ViewStubCompat) this.y.findViewById(a.f.action_mode_bar_stub);
                    if (viewStubCompat != null) {
                        viewStubCompat.setLayoutInflater(LayoutInflater.from(n()));
                        this.n = (ActionBarContextView) viewStubCompat.a();
                    }
                }
            }
            if (this.n != null) {
                t();
                this.n.c();
                Context context2 = this.n.getContext();
                ActionBarContextView actionBarContextView = this.n;
                if (this.o == null) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                StandaloneActionMode standaloneActionMode = new StandaloneActionMode(context2, actionBarContextView, aVar, z2);
                if (aVar.a(standaloneActionMode, standaloneActionMode.b())) {
                    standaloneActionMode.d();
                    this.n.a(standaloneActionMode);
                    this.m = standaloneActionMode;
                    if (s()) {
                        ag.c((View) this.n, 0.0f);
                        this.q = ag.r(this.n).a(1.0f);
                        this.q.a(new ViewPropertyAnimatorListenerAdapter() {
                            public void onAnimationStart(View view) {
                                AppCompatDelegateImplV9.this.n.setVisibility(0);
                                AppCompatDelegateImplV9.this.n.sendAccessibilityEvent(32);
                                if (AppCompatDelegateImplV9.this.n.getParent() instanceof View) {
                                    ag.w((View) AppCompatDelegateImplV9.this.n.getParent());
                                }
                            }

                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: android.support.v4.view.ag.c(android.view.View, float):void
                             arg types: [android.support.v7.widget.ActionBarContextView, int]
                             candidates:
                              android.support.v4.view.ag.c(android.view.View, int):void
                              android.support.v4.view.ag.c(android.view.View, boolean):void
                              android.support.v4.view.ag.c(android.view.View, float):void */
                            public void onAnimationEnd(View view) {
                                ag.c((View) AppCompatDelegateImplV9.this.n, 1.0f);
                                AppCompatDelegateImplV9.this.q.a((bc) null);
                                AppCompatDelegateImplV9.this.q = null;
                            }
                        });
                    } else {
                        ag.c((View) this.n, 1.0f);
                        this.n.setVisibility(0);
                        this.n.sendAccessibilityEvent(32);
                        if (this.n.getParent() instanceof View) {
                            ag.w((View) this.n.getParent());
                        }
                    }
                    if (this.o != null) {
                        this.f1340b.getDecorView().post(this.p);
                    }
                } else {
                    this.m = null;
                }
            }
        }
        if (!(this.m == null || this.f1343e == null)) {
            this.f1343e.onSupportActionModeStarted(this.m);
        }
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final boolean s() {
        return this.x && this.y != null && ag.F(this.y);
    }

    /* access modifiers changed from: package-private */
    public void t() {
        if (this.q != null) {
            this.q.b();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean u() {
        if (this.m != null) {
            this.m.c();
            return true;
        }
        ActionBar a2 = a();
        if (a2 == null || !a2.f()) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
    /* access modifiers changed from: package-private */
    public boolean a(int i, KeyEvent keyEvent) {
        ActionBar a2 = a();
        if (a2 != null && a2.a(i, keyEvent)) {
            return true;
        }
        if (this.F == null || !a(this.F, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.F == null) {
                PanelFeatureState a3 = a(0, true);
                b(a3, keyEvent);
                boolean a4 = a(a3, keyEvent.getKeyCode(), keyEvent, 1);
                a3.m = false;
                if (a4) {
                    return true;
                }
            }
            return false;
        } else if (this.F == null) {
            return true;
        } else {
            this.F.n = true;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(KeyEvent keyEvent) {
        boolean z2 = true;
        if (keyEvent.getKeyCode() == 82 && this.f1341c.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z2 = false;
        }
        return z2 ? c(keyCode, keyEvent) : b(keyCode, keyEvent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean b(int i, KeyEvent keyEvent) {
        switch (i) {
            case 82:
                e(0, keyEvent);
                return true;
            case 4:
                boolean z2 = this.G;
                this.G = false;
                PanelFeatureState a2 = a(0, false);
                if (a2 == null || !a2.o) {
                    if (u()) {
                        return true;
                    }
                } else if (z2) {
                    return true;
                } else {
                    a(a2, true);
                    return true;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i, KeyEvent keyEvent) {
        boolean z2 = true;
        switch (i) {
            case 82:
                d(0, keyEvent);
                return true;
            case 4:
                if ((keyEvent.getFlags() & FileUtils.FileMode.MODE_IWUSR) == 0) {
                    z2 = false;
                }
                this.G = z2;
                break;
        }
        if (Build.VERSION.SDK_INT < 11) {
            a(i, keyEvent);
        }
        return false;
    }

    public View c(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z2;
        boolean a2;
        if (this.L == null) {
            this.L = new h();
        }
        if (t) {
            if (attributeSet instanceof XmlPullParser) {
                a2 = ((XmlPullParser) attributeSet).getDepth() > 1;
            } else {
                a2 = a((ViewParent) view);
            }
            z2 = a2;
        } else {
            z2 = false;
        }
        return this.L.a(view, str, context, attributeSet, z2, t, true, ao.a());
    }

    private boolean a(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.f1340b.getDecorView();
        for (ViewParent viewParent2 = viewParent; viewParent2 != null; viewParent2 = viewParent2.getParent()) {
            if (viewParent2 == decorView || !(viewParent2 instanceof View) || ag.H((View) viewParent2)) {
                return false;
            }
        }
        return true;
    }

    public void h() {
        LayoutInflater from = LayoutInflater.from(this.f1339a);
        if (from.getFactory() == null) {
            i.a(from, this);
        } else if (!(i.a(from) instanceof AppCompatDelegateImplV9)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    public final View a(View view, String str, Context context, AttributeSet attributeSet) {
        View b2 = b(view, str, context, attributeSet);
        return b2 != null ? b2 : c(view, str, context, attributeSet);
    }

    /* access modifiers changed from: package-private */
    public View b(View view, String str, Context context, AttributeSet attributeSet) {
        View onCreateView;
        if (!(this.f1341c instanceof LayoutInflater.Factory) || (onCreateView = ((LayoutInflater.Factory) this.f1341c).onCreateView(str, context, attributeSet)) == null) {
            return null;
        }
        return onCreateView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void */
    private void a(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        ViewGroup.LayoutParams layoutParams;
        ViewGroup.LayoutParams layoutParams2;
        boolean z2;
        int i = -1;
        if (!panelFeatureState.o && !p()) {
            if (panelFeatureState.f1293a == 0) {
                Context context = this.f1339a;
                boolean z3 = (context.getResources().getConfiguration().screenLayout & 15) == 4;
                if (context.getApplicationInfo().targetSdkVersion >= 11) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (z3 && z2) {
                    return;
                }
            }
            Window.Callback q2 = q();
            if (q2 == null || q2.onMenuOpened(panelFeatureState.f1293a, panelFeatureState.j)) {
                WindowManager windowManager = (WindowManager) this.f1339a.getSystemService("window");
                if (windowManager != null && b(panelFeatureState, keyEvent)) {
                    if (panelFeatureState.f1299g == null || panelFeatureState.q) {
                        if (panelFeatureState.f1299g == null) {
                            if (!a(panelFeatureState) || panelFeatureState.f1299g == null) {
                                return;
                            }
                        } else if (panelFeatureState.q && panelFeatureState.f1299g.getChildCount() > 0) {
                            panelFeatureState.f1299g.removeAllViews();
                        }
                        if (c(panelFeatureState) && panelFeatureState.a()) {
                            ViewGroup.LayoutParams layoutParams3 = panelFeatureState.f1300h.getLayoutParams();
                            if (layoutParams3 == null) {
                                layoutParams = new ViewGroup.LayoutParams(-2, -2);
                            } else {
                                layoutParams = layoutParams3;
                            }
                            panelFeatureState.f1299g.setBackgroundResource(panelFeatureState.f1294b);
                            ViewParent parent = panelFeatureState.f1300h.getParent();
                            if (parent != null && (parent instanceof ViewGroup)) {
                                ((ViewGroup) parent).removeView(panelFeatureState.f1300h);
                            }
                            panelFeatureState.f1299g.addView(panelFeatureState.f1300h, layoutParams);
                            if (!panelFeatureState.f1300h.hasFocus()) {
                                panelFeatureState.f1300h.requestFocus();
                            }
                            i = -2;
                        } else {
                            return;
                        }
                    } else if (panelFeatureState.i == null || (layoutParams2 = panelFeatureState.i.getLayoutParams()) == null || layoutParams2.width != -1) {
                        i = -2;
                    }
                    panelFeatureState.n = false;
                    WindowManager.LayoutParams layoutParams4 = new WindowManager.LayoutParams(i, -2, panelFeatureState.f1296d, panelFeatureState.f1297e, AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE, 8519680, -3);
                    layoutParams4.gravity = panelFeatureState.f1295c;
                    layoutParams4.windowAnimations = panelFeatureState.f1298f;
                    windowManager.addView(panelFeatureState.f1299g, layoutParams4);
                    panelFeatureState.o = true;
                    return;
                }
                return;
            }
            a(panelFeatureState, true);
        }
    }

    private boolean a(PanelFeatureState panelFeatureState) {
        panelFeatureState.a(n());
        panelFeatureState.f1299g = new c(panelFeatureState.l);
        panelFeatureState.f1295c = 81;
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void */
    private void a(MenuBuilder menuBuilder, boolean z2) {
        if (this.u == null || !this.u.e() || (android.support.v4.view.ar.a(ViewConfiguration.get(this.f1339a)) && !this.u.g())) {
            PanelFeatureState a2 = a(0, true);
            a2.q = true;
            a(a2, false);
            a(a2, (KeyEvent) null);
            return;
        }
        Window.Callback q2 = q();
        if (this.u.f() && z2) {
            this.u.i();
            if (!p()) {
                q2.onPanelClosed(108, a(0, true).j);
            }
        } else if (q2 != null && !p()) {
            if (this.r && (this.s & 1) != 0) {
                this.f1340b.getDecorView().removeCallbacks(this.H);
                this.H.run();
            }
            PanelFeatureState a3 = a(0, true);
            if (a3.j != null && !a3.r && q2.onPreparePanel(0, a3.i, a3.j)) {
                q2.onMenuOpened(108, a3.j);
                this.u.h();
            }
        }
    }

    private boolean b(PanelFeatureState panelFeatureState) {
        Context context;
        Context context2 = this.f1339a;
        if ((panelFeatureState.f1293a == 0 || panelFeatureState.f1293a == 108) && this.u != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context2.getTheme();
            theme.resolveAttribute(a.C0027a.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context2.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(a.C0027a.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(a.C0027a.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context2.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            Resources.Theme theme3 = theme2;
            if (theme3 != null) {
                context = new android.support.v7.view.d(context2, 0);
                context.getTheme().setTo(theme3);
                MenuBuilder menuBuilder = new MenuBuilder(context);
                menuBuilder.setCallback(this);
                panelFeatureState.a(menuBuilder);
                return true;
            }
        }
        context = context2;
        MenuBuilder menuBuilder2 = new MenuBuilder(context);
        menuBuilder2.setCallback(this);
        panelFeatureState.a(menuBuilder2);
        return true;
    }

    private boolean c(PanelFeatureState panelFeatureState) {
        if (panelFeatureState.i != null) {
            panelFeatureState.f1300h = panelFeatureState.i;
            return true;
        } else if (panelFeatureState.j == null) {
            return false;
        } else {
            if (this.w == null) {
                this.w = new d();
            }
            panelFeatureState.f1300h = (View) panelFeatureState.a(this.w);
            return panelFeatureState.f1300h != null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void */
    private boolean b(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        boolean z2;
        if (p()) {
            return false;
        }
        if (panelFeatureState.m) {
            return true;
        }
        if (!(this.F == null || this.F == panelFeatureState)) {
            a(this.F, false);
        }
        Window.Callback q2 = q();
        if (q2 != null) {
            panelFeatureState.i = q2.onCreatePanelView(panelFeatureState.f1293a);
        }
        boolean z3 = panelFeatureState.f1293a == 0 || panelFeatureState.f1293a == 108;
        if (z3 && this.u != null) {
            this.u.j();
        }
        if (panelFeatureState.i == null && (!z3 || !(m() instanceof n))) {
            if (panelFeatureState.j == null || panelFeatureState.r) {
                if (panelFeatureState.j == null && (!b(panelFeatureState) || panelFeatureState.j == null)) {
                    return false;
                }
                if (z3 && this.u != null) {
                    if (this.v == null) {
                        this.v = new a();
                    }
                    this.u.a(panelFeatureState.j, this.v);
                }
                panelFeatureState.j.stopDispatchingItemsChanged();
                if (!q2.onCreatePanelMenu(panelFeatureState.f1293a, panelFeatureState.j)) {
                    panelFeatureState.a((MenuBuilder) null);
                    if (!z3 || this.u == null) {
                        return false;
                    }
                    this.u.a(null, this.v);
                    return false;
                }
                panelFeatureState.r = false;
            }
            panelFeatureState.j.stopDispatchingItemsChanged();
            if (panelFeatureState.s != null) {
                panelFeatureState.j.restoreActionViewStates(panelFeatureState.s);
                panelFeatureState.s = null;
            }
            if (!q2.onPreparePanel(0, panelFeatureState.i, panelFeatureState.j)) {
                if (z3 && this.u != null) {
                    this.u.a(null, this.v);
                }
                panelFeatureState.j.startDispatchingItemsChanged();
                return false;
            }
            if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1) {
                z2 = true;
            } else {
                z2 = false;
            }
            panelFeatureState.p = z2;
            panelFeatureState.j.setQwertyMode(panelFeatureState.p);
            panelFeatureState.j.startDispatchingItemsChanged();
        }
        panelFeatureState.m = true;
        panelFeatureState.n = false;
        this.F = panelFeatureState;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(MenuBuilder menuBuilder) {
        if (!this.D) {
            this.D = true;
            this.u.k();
            Window.Callback q2 = q();
            if (q2 != null && !p()) {
                q2.onPanelClosed(108, menuBuilder);
            }
            this.D = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
    /* access modifiers changed from: package-private */
    public void e(int i) {
        a(a(i, true), true);
    }

    /* access modifiers changed from: package-private */
    public void a(PanelFeatureState panelFeatureState, boolean z2) {
        if (!z2 || panelFeatureState.f1293a != 0 || this.u == null || !this.u.f()) {
            WindowManager windowManager = (WindowManager) this.f1339a.getSystemService("window");
            if (!(windowManager == null || !panelFeatureState.o || panelFeatureState.f1299g == null)) {
                windowManager.removeView(panelFeatureState.f1299g);
                if (z2) {
                    a(panelFeatureState.f1293a, panelFeatureState, (Menu) null);
                }
            }
            panelFeatureState.m = false;
            panelFeatureState.n = false;
            panelFeatureState.o = false;
            panelFeatureState.f1300h = null;
            panelFeatureState.q = true;
            if (this.F == panelFeatureState) {
                this.F = null;
                return;
            }
            return;
        }
        a(panelFeatureState.j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
    private boolean d(int i, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() == 0) {
            PanelFeatureState a2 = a(i, true);
            if (!a2.o) {
                return b(a2, keyEvent);
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void */
    private boolean e(int i, KeyEvent keyEvent) {
        boolean z2;
        boolean z3 = true;
        if (this.m != null) {
            return false;
        }
        PanelFeatureState a2 = a(i, true);
        if (i != 0 || this.u == null || !this.u.e() || android.support.v4.view.ar.a(ViewConfiguration.get(this.f1339a))) {
            if (a2.o || a2.n) {
                boolean z4 = a2.o;
                a(a2, true);
                z3 = z4;
            } else {
                if (a2.m) {
                    if (a2.r) {
                        a2.m = false;
                        z2 = b(a2, keyEvent);
                    } else {
                        z2 = true;
                    }
                    if (z2) {
                        a(a2, keyEvent);
                    }
                }
                z3 = false;
            }
        } else if (!this.u.f()) {
            if (!p() && b(a2, keyEvent)) {
                z3 = this.u.h();
            }
            z3 = false;
        } else {
            z3 = this.u.i();
        }
        if (z3) {
            AudioManager audioManager = (AudioManager) this.f1339a.getSystemService("audio");
            if (audioManager != null) {
                audioManager.playSoundEffect(0);
            } else {
                Log.w("AppCompatDelegate", "Couldn't get audio manager");
            }
        }
        return z3;
    }

    /* access modifiers changed from: package-private */
    public void a(int i, PanelFeatureState panelFeatureState, Menu menu) {
        if (menu == null) {
            if (panelFeatureState == null && i >= 0 && i < this.E.length) {
                panelFeatureState = this.E[i];
            }
            if (panelFeatureState != null) {
                menu = panelFeatureState.j;
            }
        }
        if ((panelFeatureState == null || panelFeatureState.o) && !p()) {
            this.f1341c.onPanelClosed(i, menu);
        }
    }

    /* access modifiers changed from: package-private */
    public PanelFeatureState a(Menu menu) {
        PanelFeatureState[] panelFeatureStateArr = this.E;
        int length = panelFeatureStateArr != null ? panelFeatureStateArr.length : 0;
        for (int i = 0; i < length; i++) {
            PanelFeatureState panelFeatureState = panelFeatureStateArr[i];
            if (panelFeatureState != null && panelFeatureState.j == menu) {
                return panelFeatureState;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public PanelFeatureState a(int i, boolean z2) {
        PanelFeatureState[] panelFeatureStateArr = this.E;
        if (panelFeatureStateArr == null || panelFeatureStateArr.length <= i) {
            PanelFeatureState[] panelFeatureStateArr2 = new PanelFeatureState[(i + 1)];
            if (panelFeatureStateArr != null) {
                System.arraycopy(panelFeatureStateArr, 0, panelFeatureStateArr2, 0, panelFeatureStateArr.length);
            }
            this.E = panelFeatureStateArr2;
            panelFeatureStateArr = panelFeatureStateArr2;
        }
        PanelFeatureState panelFeatureState = panelFeatureStateArr[i];
        if (panelFeatureState != null) {
            return panelFeatureState;
        }
        PanelFeatureState panelFeatureState2 = new PanelFeatureState(i);
        panelFeatureStateArr[i] = panelFeatureState2;
        return panelFeatureState2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void */
    private boolean a(PanelFeatureState panelFeatureState, int i, KeyEvent keyEvent, int i2) {
        boolean z2 = false;
        if (!keyEvent.isSystem()) {
            if ((panelFeatureState.m || b(panelFeatureState, keyEvent)) && panelFeatureState.j != null) {
                z2 = panelFeatureState.j.performShortcut(i, keyEvent, i2);
            }
            if (z2 && (i2 & 1) == 0 && this.u == null) {
                a(panelFeatureState, true);
            }
        }
        return z2;
    }

    private void d(int i) {
        this.s |= 1 << i;
        if (!this.r) {
            ag.a(this.f1340b.getDecorView(), this.H);
            this.r = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
    /* access modifiers changed from: package-private */
    public void f(int i) {
        PanelFeatureState a2;
        PanelFeatureState a3 = a(i, true);
        if (a3.j != null) {
            Bundle bundle = new Bundle();
            a3.j.saveActionViewStates(bundle);
            if (bundle.size() > 0) {
                a3.s = bundle;
            }
            a3.j.stopDispatchingItemsChanged();
            a3.j.clear();
        }
        a3.r = true;
        a3.q = true;
        if ((i == 108 || i == 0) && this.u != null && (a2 = a(0, false)) != null) {
            a2.m = false;
            b(a2, (KeyEvent) null);
        }
    }

    /* access modifiers changed from: package-private */
    public int g(int i) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = true;
        int i2 = 0;
        if (this.n == null || !(this.n.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.n.getLayoutParams();
            if (this.n.isShown()) {
                if (this.J == null) {
                    this.J = new Rect();
                    this.K = new Rect();
                }
                Rect rect = this.J;
                Rect rect2 = this.K;
                rect.set(0, i, 0, 0);
                ar.a(this.y, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i : 0)) {
                    marginLayoutParams.topMargin = i;
                    if (this.A == null) {
                        this.A = new View(this.f1339a);
                        this.A.setBackgroundColor(this.f1339a.getResources().getColor(a.c.abc_input_method_navigation_guard));
                        this.y.addView(this.A, -1, new ViewGroup.LayoutParams(-1, i));
                        z4 = true;
                    } else {
                        ViewGroup.LayoutParams layoutParams = this.A.getLayoutParams();
                        if (layoutParams.height != i) {
                            layoutParams.height = i;
                            this.A.setLayoutParams(layoutParams);
                        }
                        z4 = true;
                    }
                } else {
                    z4 = false;
                }
                if (this.A == null) {
                    z5 = false;
                }
                if (!this.j && z5) {
                    i = 0;
                }
                boolean z6 = z4;
                z3 = z5;
                z5 = z6;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                z3 = false;
            } else {
                z5 = false;
                z3 = false;
            }
            if (z5) {
                this.n.setLayoutParams(marginLayoutParams);
            }
            z2 = z3;
        }
        if (this.A != null) {
            View view = this.A;
            if (!z2) {
                i2 = 8;
            }
            view.setVisibility(i2);
        }
        return i;
    }

    private void z() {
        if (this.x) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    private int h(int i) {
        if (i == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i != 9) {
            return i;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
      android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.c.a(int, android.view.Menu):void
      android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
      android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
    /* access modifiers changed from: package-private */
    public void v() {
        if (this.u != null) {
            this.u.k();
        }
        if (this.o != null) {
            this.f1340b.getDecorView().removeCallbacks(this.p);
            if (this.o.isShowing()) {
                try {
                    this.o.dismiss();
                } catch (IllegalArgumentException e2) {
                }
            }
            this.o = null;
        }
        t();
        PanelFeatureState a2 = a(0, false);
        if (a2 != null && a2.j != null) {
            a2.j.close();
        }
    }

    class b implements b.a {

        /* renamed from: b  reason: collision with root package name */
        private b.a f1306b;

        public b(b.a aVar) {
            this.f1306b = aVar;
        }

        public boolean a(android.support.v7.view.b bVar, Menu menu) {
            return this.f1306b.a(bVar, menu);
        }

        public boolean b(android.support.v7.view.b bVar, Menu menu) {
            return this.f1306b.b(bVar, menu);
        }

        public boolean a(android.support.v7.view.b bVar, MenuItem menuItem) {
            return this.f1306b.a(bVar, menuItem);
        }

        public void a(android.support.v7.view.b bVar) {
            this.f1306b.a(bVar);
            if (AppCompatDelegateImplV9.this.o != null) {
                AppCompatDelegateImplV9.this.f1340b.getDecorView().removeCallbacks(AppCompatDelegateImplV9.this.p);
            }
            if (AppCompatDelegateImplV9.this.n != null) {
                AppCompatDelegateImplV9.this.t();
                AppCompatDelegateImplV9.this.q = ag.r(AppCompatDelegateImplV9.this.n).a(0.0f);
                AppCompatDelegateImplV9.this.q.a(new ViewPropertyAnimatorListenerAdapter() {
                    public void onAnimationEnd(View view) {
                        AppCompatDelegateImplV9.this.n.setVisibility(8);
                        if (AppCompatDelegateImplV9.this.o != null) {
                            AppCompatDelegateImplV9.this.o.dismiss();
                        } else if (AppCompatDelegateImplV9.this.n.getParent() instanceof View) {
                            ag.w((View) AppCompatDelegateImplV9.this.n.getParent());
                        }
                        AppCompatDelegateImplV9.this.n.removeAllViews();
                        AppCompatDelegateImplV9.this.q.a((bc) null);
                        AppCompatDelegateImplV9.this.q = null;
                    }
                });
            }
            if (AppCompatDelegateImplV9.this.f1343e != null) {
                AppCompatDelegateImplV9.this.f1343e.onSupportActionModeFinished(AppCompatDelegateImplV9.this.m);
            }
            AppCompatDelegateImplV9.this.m = null;
        }
    }

    private final class d implements i.a {
        d() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
         arg types: [android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, int]
         candidates:
          android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
          android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
          android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
          android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
          android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
          android.support.v7.app.c.a(int, android.view.Menu):void
          android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
          android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
          android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
          android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void */
        public void a(MenuBuilder menuBuilder, boolean z) {
            MenuBuilder rootMenu = menuBuilder.getRootMenu();
            boolean z2 = rootMenu != menuBuilder;
            AppCompatDelegateImplV9 appCompatDelegateImplV9 = AppCompatDelegateImplV9.this;
            if (z2) {
                menuBuilder = rootMenu;
            }
            PanelFeatureState a2 = appCompatDelegateImplV9.a((Menu) menuBuilder);
            if (a2 == null) {
                return;
            }
            if (z2) {
                AppCompatDelegateImplV9.this.a(a2.f1293a, a2, rootMenu);
                AppCompatDelegateImplV9.this.a(a2, true);
                return;
            }
            AppCompatDelegateImplV9.this.a(a2, z);
        }

        public boolean a(MenuBuilder menuBuilder) {
            Window.Callback q;
            if (menuBuilder != null || !AppCompatDelegateImplV9.this.f1346h || (q = AppCompatDelegateImplV9.this.q()) == null || AppCompatDelegateImplV9.this.p()) {
                return true;
            }
            q.onMenuOpened(108, menuBuilder);
            return true;
        }
    }

    private final class a implements i.a {
        a() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            Window.Callback q = AppCompatDelegateImplV9.this.q();
            if (q == null) {
                return true;
            }
            q.onMenuOpened(108, menuBuilder);
            return true;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            AppCompatDelegateImplV9.this.a(menuBuilder);
        }
    }

    protected static final class PanelFeatureState {

        /* renamed from: a  reason: collision with root package name */
        int f1293a;

        /* renamed from: b  reason: collision with root package name */
        int f1294b;

        /* renamed from: c  reason: collision with root package name */
        int f1295c;

        /* renamed from: d  reason: collision with root package name */
        int f1296d;

        /* renamed from: e  reason: collision with root package name */
        int f1297e;

        /* renamed from: f  reason: collision with root package name */
        int f1298f;

        /* renamed from: g  reason: collision with root package name */
        ViewGroup f1299g;

        /* renamed from: h  reason: collision with root package name */
        View f1300h;
        View i;
        MenuBuilder j;
        ListMenuPresenter k;
        Context l;
        boolean m;
        boolean n;
        boolean o;
        public boolean p;
        boolean q = false;
        boolean r;
        Bundle s;

        PanelFeatureState(int i2) {
            this.f1293a = i2;
        }

        public boolean a() {
            if (this.f1300h == null) {
                return false;
            }
            if (this.i != null || this.k.a().getCount() > 0) {
                return true;
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public void a(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(a.C0027a.actionBarPopupTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            }
            newTheme.resolveAttribute(a.C0027a.panelMenuListTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            } else {
                newTheme.applyStyle(a.j.Theme_AppCompat_CompactMenu, true);
            }
            android.support.v7.view.d dVar = new android.support.v7.view.d(context, 0);
            dVar.getTheme().setTo(newTheme);
            this.l = dVar;
            TypedArray obtainStyledAttributes = dVar.obtainStyledAttributes(a.k.AppCompatTheme);
            this.f1294b = obtainStyledAttributes.getResourceId(a.k.AppCompatTheme_panelBackground, 0);
            this.f1298f = obtainStyledAttributes.getResourceId(a.k.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        /* access modifiers changed from: package-private */
        public void a(MenuBuilder menuBuilder) {
            if (menuBuilder != this.j) {
                if (this.j != null) {
                    this.j.removeMenuPresenter(this.k);
                }
                this.j = menuBuilder;
                if (menuBuilder != null && this.k != null) {
                    menuBuilder.addMenuPresenter(this.k);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public j a(i.a aVar) {
            if (this.j == null) {
                return null;
            }
            if (this.k == null) {
                this.k = new ListMenuPresenter(this.l, a.h.abc_list_menu_item_layout);
                this.k.setCallback(aVar);
                this.j.addMenuPresenter(this.k);
            }
            return this.k.a(this.f1299g);
        }

        private static class SavedState implements Parcelable {
            public static final Parcelable.Creator<SavedState> CREATOR = f.a(new android.support.v4.os.g<SavedState>() {
                /* renamed from: a */
                public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    return SavedState.a(parcel, classLoader);
                }

                /* renamed from: a */
                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            });

            /* renamed from: a  reason: collision with root package name */
            int f1301a;

            /* renamed from: b  reason: collision with root package name */
            boolean f1302b;

            /* renamed from: c  reason: collision with root package name */
            Bundle f1303c;

            SavedState() {
            }

            public int describeContents() {
                return 0;
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f1301a);
                parcel.writeInt(this.f1302b ? 1 : 0);
                if (this.f1302b) {
                    parcel.writeBundle(this.f1303c);
                }
            }

            static SavedState a(Parcel parcel, ClassLoader classLoader) {
                boolean z = true;
                SavedState savedState = new SavedState();
                savedState.f1301a = parcel.readInt();
                if (parcel.readInt() != 1) {
                    z = false;
                }
                savedState.f1302b = z;
                if (savedState.f1302b) {
                    savedState.f1303c = parcel.readBundle(classLoader);
                }
                return savedState;
            }
        }
    }

    private class c extends ContentFrameLayout {
        public c(Context context) {
            super(context);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return AppCompatDelegateImplV9.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            AppCompatDelegateImplV9.this.e(0);
            return true;
        }

        public void setBackgroundResource(int i) {
            setBackgroundDrawable(android.support.v7.c.a.b.b(getContext(), i));
        }

        private boolean a(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }
    }
}
