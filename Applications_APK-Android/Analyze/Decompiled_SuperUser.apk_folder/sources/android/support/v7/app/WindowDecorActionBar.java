package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.ag;
import android.support.v4.view.ay;
import android.support.v4.view.bc;
import android.support.v4.view.bd;
import android.support.v7.a.a;
import android.support.v7.app.ActionBar;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.b;
import android.support.v7.view.e;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.ActionBarContainer;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ActionBarOverlayLayout;
import android.support.v7.widget.ScrollingTabContainerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.t;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class WindowDecorActionBar extends ActionBar implements ActionBarOverlayLayout.a {
    static final /* synthetic */ boolean s = (!WindowDecorActionBar.class.desiredAssertionStatus());
    private static final Interpolator t = new AccelerateInterpolator();
    private static final Interpolator u = new DecelerateInterpolator();
    private static final boolean v;
    private TabImpl A;
    private int B = -1;
    private boolean C;
    private boolean D;
    private ArrayList<ActionBar.a> E = new ArrayList<>();
    private boolean F;
    private int G = 0;
    private boolean H;
    private boolean I = true;
    private boolean J;

    /* renamed from: a  reason: collision with root package name */
    Context f1314a;

    /* renamed from: b  reason: collision with root package name */
    ActionBarOverlayLayout f1315b;

    /* renamed from: c  reason: collision with root package name */
    ActionBarContainer f1316c;

    /* renamed from: d  reason: collision with root package name */
    t f1317d;

    /* renamed from: e  reason: collision with root package name */
    ActionBarContextView f1318e;

    /* renamed from: f  reason: collision with root package name */
    View f1319f;

    /* renamed from: g  reason: collision with root package name */
    ScrollingTabContainerView f1320g;

    /* renamed from: h  reason: collision with root package name */
    ActionModeImpl f1321h;
    b i;
    b.a j;
    boolean k = true;
    boolean l;
    boolean m;
    e n;
    boolean o;
    final bc p = new ViewPropertyAnimatorListenerAdapter() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.b(android.view.View, float):void
         arg types: [android.view.View, int]
         candidates:
          android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
          android.support.v4.view.ag.b(android.view.View, boolean):void
          android.support.v4.view.ag.b(android.view.View, int):boolean
          android.support.v4.view.ag.b(android.view.View, float):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.b(android.view.View, float):void
         arg types: [android.support.v7.widget.ActionBarContainer, int]
         candidates:
          android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
          android.support.v4.view.ag.b(android.view.View, boolean):void
          android.support.v4.view.ag.b(android.view.View, int):boolean
          android.support.v4.view.ag.b(android.view.View, float):void */
        public void onAnimationEnd(View view) {
            if (WindowDecorActionBar.this.k && WindowDecorActionBar.this.f1319f != null) {
                ag.b(WindowDecorActionBar.this.f1319f, 0.0f);
                ag.b((View) WindowDecorActionBar.this.f1316c, 0.0f);
            }
            WindowDecorActionBar.this.f1316c.setVisibility(8);
            WindowDecorActionBar.this.f1316c.setTransitioning(false);
            WindowDecorActionBar.this.n = null;
            WindowDecorActionBar.this.i();
            if (WindowDecorActionBar.this.f1315b != null) {
                ag.w(WindowDecorActionBar.this.f1315b);
            }
        }
    };
    final bc q = new ViewPropertyAnimatorListenerAdapter() {
        public void onAnimationEnd(View view) {
            WindowDecorActionBar.this.n = null;
            WindowDecorActionBar.this.f1316c.requestLayout();
        }
    };
    final bd r = new bd() {
        public void a(View view) {
            ((View) WindowDecorActionBar.this.f1316c.getParent()).invalidate();
        }
    };
    private Context w;
    private Activity x;
    private Dialog y;
    private ArrayList<TabImpl> z = new ArrayList<>();

    static {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT < 14) {
            z2 = false;
        }
        v = z2;
    }

    public WindowDecorActionBar(Activity activity, boolean z2) {
        this.x = activity;
        View decorView = activity.getWindow().getDecorView();
        a(decorView);
        if (!z2) {
            this.f1319f = decorView.findViewById(16908290);
        }
    }

    public WindowDecorActionBar(Dialog dialog) {
        this.y = dialog;
        a(dialog.getWindow().getDecorView());
    }

    private void a(View view) {
        boolean z2;
        this.f1315b = (ActionBarOverlayLayout) view.findViewById(a.f.decor_content_parent);
        if (this.f1315b != null) {
            this.f1315b.setActionBarVisibilityCallback(this);
        }
        this.f1317d = b(view.findViewById(a.f.action_bar));
        this.f1318e = (ActionBarContextView) view.findViewById(a.f.action_context_bar);
        this.f1316c = (ActionBarContainer) view.findViewById(a.f.action_bar_container);
        if (this.f1317d == null || this.f1318e == null || this.f1316c == null) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
        }
        this.f1314a = this.f1317d.b();
        boolean z3 = (this.f1317d.o() & 4) != 0;
        if (z3) {
            this.C = true;
        }
        android.support.v7.view.a a2 = android.support.v7.view.a.a(this.f1314a);
        if (a2.f() || z3) {
            z2 = true;
        } else {
            z2 = false;
        }
        d(z2);
        m(a2.d());
        TypedArray obtainStyledAttributes = this.f1314a.obtainStyledAttributes(null, a.k.ActionBar, a.C0027a.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(a.k.ActionBar_hideOnContentScroll, false)) {
            e(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(a.k.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    private t b(View view) {
        if (view instanceof t) {
            return (t) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException(new StringBuilder().append("Can't make a decor toolbar out of ").append(view).toString() != null ? view.getClass().getSimpleName() : "null");
    }

    public void a(float f2) {
        ag.h(this.f1316c, f2);
    }

    public void a(Configuration configuration) {
        m(android.support.v7.view.a.a(this.f1314a).d());
    }

    private void m(boolean z2) {
        boolean z3;
        boolean z4;
        boolean z5 = true;
        this.F = z2;
        if (!this.F) {
            this.f1317d.a((ScrollingTabContainerView) null);
            this.f1316c.setTabContainer(this.f1320g);
        } else {
            this.f1316c.setTabContainer(null);
            this.f1317d.a(this.f1320g);
        }
        if (j() == 2) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (this.f1320g != null) {
            if (z3) {
                this.f1320g.setVisibility(0);
                if (this.f1315b != null) {
                    ag.w(this.f1315b);
                }
            } else {
                this.f1320g.setVisibility(8);
            }
        }
        t tVar = this.f1317d;
        if (this.F || !z3) {
            z4 = false;
        } else {
            z4 = true;
        }
        tVar.a(z4);
        ActionBarOverlayLayout actionBarOverlayLayout = this.f1315b;
        if (this.F || !z3) {
            z5 = false;
        }
        actionBarOverlayLayout.setHasNonEmbeddedTabs(z5);
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (this.j != null) {
            this.j.a(this.i);
            this.i = null;
            this.j = null;
        }
    }

    public void c(int i2) {
        this.G = i2;
    }

    public void g(boolean z2) {
        this.J = z2;
        if (!z2 && this.n != null) {
            this.n.c();
        }
    }

    public void h(boolean z2) {
        if (z2 != this.D) {
            this.D = z2;
            int size = this.E.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.E.get(i2).a(z2);
            }
        }
    }

    public void a(boolean z2) {
        a(z2 ? 4 : 0, 4);
    }

    public void b(boolean z2) {
        a(z2 ? 8 : 0, 8);
    }

    public void c(boolean z2) {
        a(z2 ? 16 : 0, 16);
    }

    public void d(boolean z2) {
        this.f1317d.b(z2);
    }

    public void a(CharSequence charSequence) {
        this.f1317d.b(charSequence);
    }

    public void b(CharSequence charSequence) {
        this.f1317d.a(charSequence);
    }

    public boolean g() {
        ViewGroup a2 = this.f1317d.a();
        if (a2 == null || a2.hasFocus()) {
            return false;
        }
        a2.requestFocus();
        return true;
    }

    public void a(int i2) {
        if ((i2 & 4) != 0) {
            this.C = true;
        }
        this.f1317d.c(i2);
    }

    public void a(int i2, int i3) {
        int o2 = this.f1317d.o();
        if ((i3 & 4) != 0) {
            this.C = true;
        }
        this.f1317d.c((o2 & (i3 ^ -1)) | (i2 & i3));
    }

    public int j() {
        return this.f1317d.p();
    }

    public int a() {
        return this.f1317d.o();
    }

    public b a(b.a aVar) {
        if (this.f1321h != null) {
            this.f1321h.c();
        }
        this.f1315b.setHideOnContentScrollEnabled(false);
        this.f1318e.c();
        ActionModeImpl actionModeImpl = new ActionModeImpl(this.f1318e.getContext(), aVar);
        if (!actionModeImpl.e()) {
            return null;
        }
        this.f1321h = actionModeImpl;
        actionModeImpl.d();
        this.f1318e.a(actionModeImpl);
        l(true);
        this.f1318e.sendAccessibilityEvent(32);
        return actionModeImpl;
    }

    public void a(ActionBar.b bVar) {
        android.support.v4.app.t tVar;
        int i2;
        int i3 = -1;
        if (j() != 2) {
            if (bVar != null) {
                i2 = bVar.a();
            } else {
                i2 = -1;
            }
            this.B = i2;
            return;
        }
        if (!(this.x instanceof FragmentActivity) || this.f1317d.a().isInEditMode()) {
            tVar = null;
        } else {
            tVar = ((FragmentActivity) this.x).e().a().a();
        }
        if (this.A != bVar) {
            ScrollingTabContainerView scrollingTabContainerView = this.f1320g;
            if (bVar != null) {
                i3 = bVar.a();
            }
            scrollingTabContainerView.setTabSelected(i3);
            if (this.A != null) {
                this.A.g().b(this.A, tVar);
            }
            this.A = (TabImpl) bVar;
            if (this.A != null) {
                this.A.g().a(this.A, tVar);
            }
        } else if (this.A != null) {
            this.A.g().c(this.A, tVar);
            this.f1320g.a(bVar.a());
        }
        if (tVar != null && !tVar.h()) {
            tVar.b();
        }
    }

    public int k() {
        return this.f1316c.getHeight();
    }

    public void i(boolean z2) {
        this.k = z2;
    }

    private void p() {
        if (!this.H) {
            this.H = true;
            if (this.f1315b != null) {
                this.f1315b.setShowingForActionMode(true);
            }
            n(false);
        }
    }

    public void l() {
        if (this.m) {
            this.m = false;
            n(true);
        }
    }

    private void q() {
        if (this.H) {
            this.H = false;
            if (this.f1315b != null) {
                this.f1315b.setShowingForActionMode(false);
            }
            n(false);
        }
    }

    public void m() {
        if (!this.m) {
            this.m = true;
            n(true);
        }
    }

    public void e(boolean z2) {
        if (!z2 || this.f1315b.a()) {
            this.o = z2;
            this.f1315b.setHideOnContentScrollEnabled(z2);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    public int d() {
        return this.f1315b.getActionBarHideOffset();
    }

    static boolean a(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        if (z2 || z3) {
            return false;
        }
        return true;
    }

    private void n(boolean z2) {
        if (a(this.l, this.m, this.H)) {
            if (!this.I) {
                this.I = true;
                j(z2);
            }
        } else if (this.I) {
            this.I = false;
            k(z2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.b(android.view.View, float):void
     arg types: [android.support.v7.widget.ActionBarContainer, int]
     candidates:
      android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.b(android.view.View, boolean):void
      android.support.v4.view.ag.b(android.view.View, int):boolean
      android.support.v4.view.ag.b(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, float):void
     arg types: [android.support.v7.widget.ActionBarContainer, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, int):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.b(android.view.View, boolean):void
      android.support.v4.view.ag.b(android.view.View, int):boolean
      android.support.v4.view.ag.b(android.view.View, float):void */
    public void j(boolean z2) {
        if (this.n != null) {
            this.n.c();
        }
        this.f1316c.setVisibility(0);
        if (this.G != 0 || !v || (!this.J && !z2)) {
            ag.c((View) this.f1316c, 1.0f);
            ag.b((View) this.f1316c, 0.0f);
            if (this.k && this.f1319f != null) {
                ag.b(this.f1319f, 0.0f);
            }
            this.q.onAnimationEnd(null);
        } else {
            ag.b((View) this.f1316c, 0.0f);
            float f2 = (float) (-this.f1316c.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.f1316c.getLocationInWindow(iArr);
                f2 -= (float) iArr[1];
            }
            ag.b(this.f1316c, f2);
            e eVar = new e();
            ay c2 = ag.r(this.f1316c).c(0.0f);
            c2.a(this.r);
            eVar.a(c2);
            if (this.k && this.f1319f != null) {
                ag.b(this.f1319f, f2);
                eVar.a(ag.r(this.f1319f).c(0.0f));
            }
            eVar.a(u);
            eVar.a(250);
            eVar.a(this.q);
            this.n = eVar;
            eVar.a();
        }
        if (this.f1315b != null) {
            ag.w(this.f1315b);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, float):void
     arg types: [android.support.v7.widget.ActionBarContainer, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, int):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, float):void */
    public void k(boolean z2) {
        if (this.n != null) {
            this.n.c();
        }
        if (this.G != 0 || !v || (!this.J && !z2)) {
            this.p.onAnimationEnd(null);
            return;
        }
        ag.c((View) this.f1316c, 1.0f);
        this.f1316c.setTransitioning(true);
        e eVar = new e();
        float f2 = (float) (-this.f1316c.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.f1316c.getLocationInWindow(iArr);
            f2 -= (float) iArr[1];
        }
        ay c2 = ag.r(this.f1316c).c(f2);
        c2.a(this.r);
        eVar.a(c2);
        if (this.k && this.f1319f != null) {
            eVar.a(ag.r(this.f1319f).c(f2));
        }
        eVar.a(t);
        eVar.a(250);
        eVar.a(this.p);
        this.n = eVar;
        eVar.a();
    }

    public boolean b() {
        int k2 = k();
        return this.I && (k2 == 0 || d() < k2);
    }

    public void l(boolean z2) {
        ay a2;
        ay a3;
        if (z2) {
            p();
        } else {
            q();
        }
        if (r()) {
            if (z2) {
                a3 = this.f1317d.a(4, 100);
                a2 = this.f1318e.a(0, 200);
            } else {
                a2 = this.f1317d.a(0, 200);
                a3 = this.f1318e.a(8, 100);
            }
            e eVar = new e();
            eVar.a(a3, a2);
            eVar.a();
        } else if (z2) {
            this.f1317d.e(4);
            this.f1318e.setVisibility(0);
        } else {
            this.f1317d.e(0);
            this.f1318e.setVisibility(8);
        }
    }

    private boolean r() {
        return ag.F(this.f1316c);
    }

    public Context c() {
        if (this.w == null) {
            TypedValue typedValue = new TypedValue();
            this.f1314a.getTheme().resolveAttribute(a.C0027a.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.w = new ContextThemeWrapper(this.f1314a, i2);
            } else {
                this.w = this.f1314a;
            }
        }
        return this.w;
    }

    public void b(int i2) {
        this.f1317d.d(i2);
    }

    public void n() {
        if (this.n != null) {
            this.n.c();
            this.n = null;
        }
    }

    public void o() {
    }

    public boolean f() {
        if (this.f1317d == null || !this.f1317d.c()) {
            return false;
        }
        this.f1317d.d();
        return true;
    }

    public class ActionModeImpl extends b implements MenuBuilder.a {

        /* renamed from: b  reason: collision with root package name */
        private final Context f1326b;

        /* renamed from: c  reason: collision with root package name */
        private final MenuBuilder f1327c;

        /* renamed from: d  reason: collision with root package name */
        private b.a f1328d;

        /* renamed from: e  reason: collision with root package name */
        private WeakReference<View> f1329e;

        public ActionModeImpl(Context context, b.a aVar) {
            this.f1326b = context;
            this.f1328d = aVar;
            this.f1327c = new MenuBuilder(context).setDefaultShowAsAction(1);
            this.f1327c.setCallback(this);
        }

        public MenuInflater a() {
            return new SupportMenuInflater(this.f1326b);
        }

        public Menu b() {
            return this.f1327c;
        }

        public void c() {
            if (WindowDecorActionBar.this.f1321h == this) {
                if (!WindowDecorActionBar.a(WindowDecorActionBar.this.l, WindowDecorActionBar.this.m, false)) {
                    WindowDecorActionBar.this.i = this;
                    WindowDecorActionBar.this.j = this.f1328d;
                } else {
                    this.f1328d.a(this);
                }
                this.f1328d = null;
                WindowDecorActionBar.this.l(false);
                WindowDecorActionBar.this.f1318e.b();
                WindowDecorActionBar.this.f1317d.a().sendAccessibilityEvent(32);
                WindowDecorActionBar.this.f1315b.setHideOnContentScrollEnabled(WindowDecorActionBar.this.o);
                WindowDecorActionBar.this.f1321h = null;
            }
        }

        public void d() {
            if (WindowDecorActionBar.this.f1321h == this) {
                this.f1327c.stopDispatchingItemsChanged();
                try {
                    this.f1328d.b(this, this.f1327c);
                } finally {
                    this.f1327c.startDispatchingItemsChanged();
                }
            }
        }

        public boolean e() {
            this.f1327c.stopDispatchingItemsChanged();
            try {
                return this.f1328d.a(this, this.f1327c);
            } finally {
                this.f1327c.startDispatchingItemsChanged();
            }
        }

        public void a(View view) {
            WindowDecorActionBar.this.f1318e.setCustomView(view);
            this.f1329e = new WeakReference<>(view);
        }

        public void a(CharSequence charSequence) {
            WindowDecorActionBar.this.f1318e.setSubtitle(charSequence);
        }

        public void b(CharSequence charSequence) {
            WindowDecorActionBar.this.f1318e.setTitle(charSequence);
        }

        public void a(int i) {
            b(WindowDecorActionBar.this.f1314a.getResources().getString(i));
        }

        public void b(int i) {
            a((CharSequence) WindowDecorActionBar.this.f1314a.getResources().getString(i));
        }

        public CharSequence f() {
            return WindowDecorActionBar.this.f1318e.getTitle();
        }

        public CharSequence g() {
            return WindowDecorActionBar.this.f1318e.getSubtitle();
        }

        public void a(boolean z) {
            super.a(z);
            WindowDecorActionBar.this.f1318e.setTitleOptional(z);
        }

        public boolean h() {
            return WindowDecorActionBar.this.f1318e.d();
        }

        public View i() {
            if (this.f1329e != null) {
                return this.f1329e.get();
            }
            return null;
        }

        public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
            if (this.f1328d != null) {
                return this.f1328d.a(this, menuItem);
            }
            return false;
        }

        public void onMenuModeChange(MenuBuilder menuBuilder) {
            if (this.f1328d != null) {
                d();
                WindowDecorActionBar.this.f1318e.a();
            }
        }
    }

    public class TabImpl extends ActionBar.b {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WindowDecorActionBar f1330a;

        /* renamed from: b  reason: collision with root package name */
        private ActionBar.c f1331b;

        /* renamed from: c  reason: collision with root package name */
        private Drawable f1332c;

        /* renamed from: d  reason: collision with root package name */
        private CharSequence f1333d;

        /* renamed from: e  reason: collision with root package name */
        private CharSequence f1334e;

        /* renamed from: f  reason: collision with root package name */
        private int f1335f;

        /* renamed from: g  reason: collision with root package name */
        private View f1336g;

        public ActionBar.c g() {
            return this.f1331b;
        }

        public View d() {
            return this.f1336g;
        }

        public Drawable b() {
            return this.f1332c;
        }

        public int a() {
            return this.f1335f;
        }

        public CharSequence c() {
            return this.f1333d;
        }

        public void e() {
            this.f1330a.a(this);
        }

        public CharSequence f() {
            return this.f1334e;
        }
    }

    public void a(View view, ActionBar.LayoutParams layoutParams) {
        view.setLayoutParams(layoutParams);
        this.f1317d.a(view);
    }

    public void f(boolean z2) {
        if (!this.C) {
            a(z2);
        }
    }
}
