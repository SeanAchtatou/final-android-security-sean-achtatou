package android.support.v7.app;

import android.support.v7.view.b;

/* compiled from: AppCompatCallback */
public interface a {
    void onSupportActionModeFinished(b bVar);

    void onSupportActionModeStarted(b bVar);

    b onWindowStartingSupportActionMode(b.a aVar);
}
