package android.support.v7.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.c;
import android.support.v7.view.SupportActionModeWrapper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.Window;
import com.lody.virtual.helper.utils.FileUtils;

@TargetApi(14)
/* compiled from: AppCompatDelegateImplV14 */
class f extends e {
    private int t = -100;
    private boolean u;
    private boolean v = true;
    private b w;

    f(Context context, Window window, a aVar) {
        super(context, window, aVar);
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        if (bundle != null && this.t == -100) {
            this.t = bundle.getInt("appcompat:local_night_mode", -100);
        }
    }

    /* access modifiers changed from: package-private */
    public Window.Callback a(Window.Callback callback) {
        return new a(callback);
    }

    public boolean o() {
        return this.v;
    }

    public boolean i() {
        boolean z = false;
        int w2 = w();
        int d2 = d(w2);
        if (d2 != -1) {
            z = h(d2);
        }
        if (w2 == 0) {
            x();
            this.w.c();
        }
        this.u = true;
        return z;
    }

    public void c() {
        super.c();
        i();
    }

    public void d() {
        super.d();
        if (this.w != null) {
            this.w.d();
        }
    }

    /* access modifiers changed from: package-private */
    public int d(int i) {
        switch (i) {
            case -100:
                return -1;
            case 0:
                x();
                return this.w.a();
            default:
                return i;
        }
    }

    private int w() {
        return this.t != -100 ? this.t : j();
    }

    public void c(Bundle bundle) {
        super.c(bundle);
        if (this.t != -100) {
            bundle.putInt("appcompat:local_night_mode", this.t);
        }
    }

    public void g() {
        super.g();
        if (this.w != null) {
            this.w.d();
        }
    }

    private boolean h(int i) {
        Resources resources = this.f1339a.getResources();
        Configuration configuration = resources.getConfiguration();
        int i2 = configuration.uiMode & 48;
        int i3 = i == 2 ? 32 : 16;
        if (i2 == i3) {
            return false;
        }
        if (y()) {
            ((Activity) this.f1339a).recreate();
        } else {
            Configuration configuration2 = new Configuration(configuration);
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            configuration2.uiMode = i3 | (configuration2.uiMode & -49);
            resources.updateConfiguration(configuration2, displayMetrics);
            m.a(resources);
        }
        return true;
    }

    private void x() {
        if (this.w == null) {
            this.w = new b(p.a(this.f1339a));
        }
    }

    private boolean y() {
        if (!this.u || !(this.f1339a instanceof Activity)) {
            return false;
        }
        try {
            if ((this.f1339a.getPackageManager().getActivityInfo(new ComponentName(this.f1339a, this.f1339a.getClass()), 0).configChanges & FileUtils.FileMode.MODE_ISVTX) == 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e2);
            return true;
        }
    }

    /* compiled from: AppCompatDelegateImplV14 */
    class a extends c.a {
        a(Window.Callback callback) {
            super(callback);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (f.this.o()) {
                return a(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        /* access modifiers changed from: package-private */
        public final ActionMode a(ActionMode.Callback callback) {
            SupportActionModeWrapper.CallbackWrapper callbackWrapper = new SupportActionModeWrapper.CallbackWrapper(f.this.f1339a, callback);
            android.support.v7.view.b b2 = f.this.b(callbackWrapper);
            if (b2 != null) {
                return callbackWrapper.b(b2);
            }
            return null;
        }
    }

    /* compiled from: AppCompatDelegateImplV14 */
    final class b {

        /* renamed from: b  reason: collision with root package name */
        private p f1352b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f1353c;

        /* renamed from: d  reason: collision with root package name */
        private BroadcastReceiver f1354d;

        /* renamed from: e  reason: collision with root package name */
        private IntentFilter f1355e;

        b(p pVar) {
            this.f1352b = pVar;
            this.f1353c = pVar.a();
        }

        /* access modifiers changed from: package-private */
        public final int a() {
            this.f1353c = this.f1352b.a();
            return this.f1353c ? 2 : 1;
        }

        /* access modifiers changed from: package-private */
        public final void b() {
            boolean a2 = this.f1352b.a();
            if (a2 != this.f1353c) {
                this.f1353c = a2;
                f.this.i();
            }
        }

        /* access modifiers changed from: package-private */
        public final void c() {
            d();
            if (this.f1354d == null) {
                this.f1354d = new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        b.this.b();
                    }
                };
            }
            if (this.f1355e == null) {
                this.f1355e = new IntentFilter();
                this.f1355e.addAction("android.intent.action.TIME_SET");
                this.f1355e.addAction("android.intent.action.TIMEZONE_CHANGED");
                this.f1355e.addAction("android.intent.action.TIME_TICK");
            }
            f.this.f1339a.registerReceiver(this.f1354d, this.f1355e);
        }

        /* access modifiers changed from: package-private */
        public final void d() {
            if (this.f1354d != null) {
                f.this.f1339a.unregisterReceiver(this.f1354d);
                this.f1354d = null;
            }
        }
    }
}
