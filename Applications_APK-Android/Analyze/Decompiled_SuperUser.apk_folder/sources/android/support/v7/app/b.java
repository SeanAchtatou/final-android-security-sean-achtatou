package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.os.c;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

/* compiled from: AppCompatDelegate */
public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private static int f1337a = -1;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f1338b = false;

    public abstract ActionBar a();

    public abstract View a(int i);

    public abstract void a(Configuration configuration);

    public abstract void a(Bundle bundle);

    public abstract void a(Toolbar toolbar);

    public abstract void a(View view);

    public abstract void a(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void a(CharSequence charSequence);

    public abstract MenuInflater b();

    public abstract void b(int i);

    public abstract void b(Bundle bundle);

    public abstract void b(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void c();

    public abstract void c(Bundle bundle);

    public abstract boolean c(int i);

    public abstract void d();

    public abstract void e();

    public abstract void f();

    public abstract void g();

    public abstract void h();

    public abstract boolean i();

    public static b a(Activity activity, a aVar) {
        return a(activity, activity.getWindow(), aVar);
    }

    public static b a(Dialog dialog, a aVar) {
        return a(dialog.getContext(), dialog.getWindow(), aVar);
    }

    private static b a(Context context, Window window, a aVar) {
        int i = Build.VERSION.SDK_INT;
        if (c.a()) {
            return new d(context, window, aVar);
        }
        if (i >= 23) {
            return new g(context, window, aVar);
        }
        if (i >= 14) {
            return new f(context, window, aVar);
        }
        if (i >= 11) {
            return new e(context, window, aVar);
        }
        return new AppCompatDelegateImplV9(context, window, aVar);
    }

    b() {
    }

    public static int j() {
        return f1337a;
    }

    public static boolean k() {
        return f1338b;
    }
}
