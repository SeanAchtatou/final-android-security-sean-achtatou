package android.support.v7.app;

import android.annotation.TargetApi;
import android.app.UiModeManager;
import android.content.Context;
import android.support.v7.app.f;
import android.view.ActionMode;
import android.view.Window;

@TargetApi(23)
/* compiled from: AppCompatDelegateImplV23 */
class g extends f {
    private final UiModeManager t;

    g(Context context, Window window, a aVar) {
        super(context, window, aVar);
        this.t = (UiModeManager) context.getSystemService("uimode");
    }

    /* access modifiers changed from: package-private */
    public Window.Callback a(Window.Callback callback) {
        return new a(callback);
    }

    /* access modifiers changed from: package-private */
    public int d(int i) {
        if (i == 0 && this.t.getNightMode() == 0) {
            return -1;
        }
        return super.d(i);
    }

    /* compiled from: AppCompatDelegateImplV23 */
    class a extends f.a {
        a(Window.Callback callback) {
            super(callback);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            if (g.this.o()) {
                switch (i) {
                    case 0:
                        return a(callback);
                }
            }
            return super.onWindowStartingActionMode(callback, i);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            return null;
        }
    }
}
