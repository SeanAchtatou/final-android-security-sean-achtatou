package android.support.v7.app;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.view.View;

public class ActionBarDrawerToggle implements DrawerLayout.f {

    /* renamed from: a  reason: collision with root package name */
    boolean f1233a;

    /* renamed from: b  reason: collision with root package name */
    private final a f1234b;

    /* renamed from: c  reason: collision with root package name */
    private DrawerArrowDrawable f1235c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f1236d;

    /* renamed from: e  reason: collision with root package name */
    private final int f1237e;

    /* renamed from: f  reason: collision with root package name */
    private final int f1238f;

    public interface a {
        void a(int i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void a(View view, float f2) {
        if (this.f1236d) {
            a(Math.min(1.0f, Math.max(0.0f, f2)));
        } else {
            a(0.0f);
        }
    }

    public void a(View view) {
        a(1.0f);
        if (this.f1233a) {
            b(this.f1238f);
        }
    }

    public void b(View view) {
        a(0.0f);
        if (this.f1233a) {
            b(this.f1237e);
        }
    }

    public void a(int i) {
    }

    /* access modifiers changed from: package-private */
    public void b(int i) {
        this.f1234b.a(i);
    }

    private void a(float f2) {
        if (f2 == 1.0f) {
            this.f1235c.b(true);
        } else if (f2 == 0.0f) {
            this.f1235c.b(false);
        }
        this.f1235c.c(f2);
    }
}
