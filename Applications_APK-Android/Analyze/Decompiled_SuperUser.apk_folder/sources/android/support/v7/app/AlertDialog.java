package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.a.a;
import android.support.v7.app.AlertController;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;

public class AlertDialog extends AppCompatDialog implements DialogInterface {

    /* renamed from: a  reason: collision with root package name */
    final AlertController f1283a = new AlertController(getContext(), this, getWindow());

    protected AlertDialog(Context context, int i) {
        super(context, a(context, i));
    }

    static int a(Context context, int i) {
        if (i >= 16777216) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(a.C0027a.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.f1283a.a(charSequence);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f1283a.a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.f1283a.a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.f1283a.b(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private final AlertController.AlertParams f1284a;

        /* renamed from: b  reason: collision with root package name */
        private final int f1285b;

        public Builder(Context context) {
            this(context, AlertDialog.a(context, 0));
        }

        public Builder(Context context, int i) {
            this.f1284a = new AlertController.AlertParams(new ContextThemeWrapper(context, AlertDialog.a(context, i)));
            this.f1285b = i;
        }

        public Context a() {
            return this.f1284a.f1260a;
        }

        public Builder a(int i) {
            this.f1284a.f1265f = this.f1284a.f1260a.getText(i);
            return this;
        }

        public Builder a(CharSequence charSequence) {
            this.f1284a.f1265f = charSequence;
            return this;
        }

        public Builder a(View view) {
            this.f1284a.f1266g = view;
            return this;
        }

        public Builder b(int i) {
            this.f1284a.f1267h = this.f1284a.f1260a.getText(i);
            return this;
        }

        public Builder b(CharSequence charSequence) {
            this.f1284a.f1267h = charSequence;
            return this;
        }

        public Builder c(int i) {
            this.f1284a.f1262c = i;
            return this;
        }

        public Builder a(Drawable drawable) {
            this.f1284a.f1263d = drawable;
            return this;
        }

        public Builder a(int i, DialogInterface.OnClickListener onClickListener) {
            this.f1284a.i = this.f1284a.f1260a.getText(i);
            this.f1284a.j = onClickListener;
            return this;
        }

        public Builder a(boolean z) {
            this.f1284a.o = z;
            return this;
        }

        public Builder a(DialogInterface.OnKeyListener onKeyListener) {
            this.f1284a.r = onKeyListener;
            return this;
        }

        public Builder a(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            this.f1284a.t = listAdapter;
            this.f1284a.u = onClickListener;
            return this;
        }

        public AlertDialog b() {
            AlertDialog alertDialog = new AlertDialog(this.f1284a.f1260a, this.f1285b);
            this.f1284a.a(alertDialog.f1283a);
            alertDialog.setCancelable(this.f1284a.o);
            if (this.f1284a.o) {
                alertDialog.setCanceledOnTouchOutside(true);
            }
            alertDialog.setOnCancelListener(this.f1284a.p);
            alertDialog.setOnDismissListener(this.f1284a.q);
            if (this.f1284a.r != null) {
                alertDialog.setOnKeyListener(this.f1284a.r);
            }
            return alertDialog;
        }

        public AlertDialog c() {
            AlertDialog b2 = b();
            b2.show();
            return b2;
        }
    }
}
