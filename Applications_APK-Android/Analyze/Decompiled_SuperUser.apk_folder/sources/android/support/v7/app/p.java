package android.support.v7.app;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.content.m;
import android.util.Log;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.Calendar;

/* compiled from: TwilightManager */
class p {

    /* renamed from: a  reason: collision with root package name */
    private static p f1394a;

    /* renamed from: b  reason: collision with root package name */
    private final Context f1395b;

    /* renamed from: c  reason: collision with root package name */
    private final LocationManager f1396c;

    /* renamed from: d  reason: collision with root package name */
    private final a f1397d = new a();

    static p a(Context context) {
        if (f1394a == null) {
            Context applicationContext = context.getApplicationContext();
            f1394a = new p(applicationContext, (LocationManager) applicationContext.getSystemService(FirebaseAnalytics.Param.LOCATION));
        }
        return f1394a;
    }

    p(Context context, LocationManager locationManager) {
        this.f1395b = context;
        this.f1396c = locationManager;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        a aVar = this.f1397d;
        if (c()) {
            return aVar.f1398a;
        }
        Location b2 = b();
        if (b2 != null) {
            a(b2);
            return aVar.f1398a;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }

    private Location b() {
        Location location;
        Location location2 = null;
        if (m.a(this.f1395b, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            location = a("network");
        } else {
            location = null;
        }
        if (m.a(this.f1395b, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location2 = a("gps");
        }
        if (location2 == null || location == null) {
            if (location2 == null) {
                location2 = location;
            }
            return location2;
        } else if (location2.getTime() > location.getTime()) {
            return location2;
        } else {
            return location;
        }
    }

    private Location a(String str) {
        if (this.f1396c != null) {
            try {
                if (this.f1396c.isProviderEnabled(str)) {
                    return this.f1396c.getLastKnownLocation(str);
                }
            } catch (Exception e2) {
                Log.d("TwilightManager", "Failed to get last known location", e2);
            }
        }
        return null;
    }

    private boolean c() {
        return this.f1397d != null && this.f1397d.f1403f > System.currentTimeMillis();
    }

    private void a(Location location) {
        long j;
        long j2;
        a aVar = this.f1397d;
        long currentTimeMillis = System.currentTimeMillis();
        o a2 = o.a();
        a2.a(currentTimeMillis - 86400000, location.getLatitude(), location.getLongitude());
        long j3 = a2.f1391a;
        a2.a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = a2.f1393c == 1;
        long j4 = a2.f1392b;
        long j5 = a2.f1391a;
        a2.a(86400000 + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j6 = a2.f1392b;
        if (j4 == -1 || j5 == -1) {
            j = 43200000 + currentTimeMillis;
        } else {
            if (currentTimeMillis > j5) {
                j2 = 0 + j6;
            } else if (currentTimeMillis > j4) {
                j2 = 0 + j5;
            } else {
                j2 = 0 + j4;
            }
            j = j2 + 60000;
        }
        aVar.f1398a = z;
        aVar.f1399b = j3;
        aVar.f1400c = j4;
        aVar.f1401d = j5;
        aVar.f1402e = j6;
        aVar.f1403f = j;
    }

    /* compiled from: TwilightManager */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        boolean f1398a;

        /* renamed from: b  reason: collision with root package name */
        long f1399b;

        /* renamed from: c  reason: collision with root package name */
        long f1400c;

        /* renamed from: d  reason: collision with root package name */
        long f1401d;

        /* renamed from: e  reason: collision with root package name */
        long f1402e;

        /* renamed from: f  reason: collision with root package name */
        long f1403f;

        a() {
        }
    }
}
