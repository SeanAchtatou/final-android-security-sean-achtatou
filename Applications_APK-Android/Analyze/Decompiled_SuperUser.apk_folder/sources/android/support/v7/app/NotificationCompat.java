package android.support.v7.app;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.ab;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.a.a;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.widget.RemoteViews;
import com.kingouser.com.util.ShellUtils;
import java.util.ArrayList;
import java.util.List;

public class NotificationCompat extends android.support.v4.app.NotificationCompat {

    public static class DecoratedCustomViewStyle extends NotificationCompat.k {
    }

    public static class DecoratedMediaCustomViewStyle extends MediaStyle {
    }

    public static class MediaStyle extends NotificationCompat.k {

        /* renamed from: a  reason: collision with root package name */
        int[] f1310a = null;

        /* renamed from: b  reason: collision with root package name */
        MediaSessionCompat.Token f1311b;

        /* renamed from: c  reason: collision with root package name */
        boolean f1312c;

        /* renamed from: h  reason: collision with root package name */
        PendingIntent f1313h;
    }

    /* access modifiers changed from: private */
    @TargetApi(24)
    public static void e(ab abVar, NotificationCompat.Builder builder) {
        if (builder.m instanceof DecoratedCustomViewStyle) {
            j.a(abVar);
        } else if (builder.m instanceof DecoratedMediaCustomViewStyle) {
            j.b(abVar);
        } else if (!(builder.m instanceof NotificationCompat.MessagingStyle)) {
            f(abVar, builder);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.k.a(android.support.v4.app.ab, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
     arg types: [android.support.v4.app.ab, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>, int[], int, ?[OBJECT, ARRAY], boolean]
     candidates:
      android.support.v7.app.k.a(android.app.Notification, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):void
      android.support.v7.app.k.a(android.support.v4.app.ab, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews */
    /* access modifiers changed from: private */
    @TargetApi(21)
    public static RemoteViews f(ab abVar, NotificationCompat.Builder builder) {
        if (builder.m instanceof MediaStyle) {
            MediaStyle mediaStyle = (MediaStyle) builder.m;
            i.a(abVar, mediaStyle.f1310a, mediaStyle.f1311b != null ? mediaStyle.f1311b.a() : null);
            boolean z = builder.d() != null;
            boolean z2 = z || ((Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 23) && builder.e() != null);
            if (!(builder.m instanceof DecoratedMediaCustomViewStyle) || !z2) {
                return null;
            }
            RemoteViews a2 = k.a(abVar, builder.f341a, builder.f342b, builder.f343c, builder.f348h, builder.i, builder.f347g, builder.n, builder.l, builder.g(), builder.h(), (List) builder.v, mediaStyle.f1310a, false, (PendingIntent) null, z);
            if (z) {
                k.a(builder.f341a, a2, builder.d());
            }
            a(builder.f341a, a2, builder.i());
            return a2;
        } else if (builder.m instanceof DecoratedCustomViewStyle) {
            return a(builder);
        } else {
            return g(abVar, builder);
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(16)
    public static RemoteViews g(ab abVar, NotificationCompat.Builder builder) {
        if (builder.m instanceof NotificationCompat.MessagingStyle) {
            a((NotificationCompat.MessagingStyle) builder.m, abVar, builder);
        }
        return h(abVar, builder);
    }

    /* access modifiers changed from: private */
    public static NotificationCompat.MessagingStyle.a b(NotificationCompat.MessagingStyle messagingStyle) {
        List<NotificationCompat.MessagingStyle.a> c2 = messagingStyle.c();
        for (int size = c2.size() - 1; size >= 0; size--) {
            NotificationCompat.MessagingStyle.a aVar = c2.get(size);
            if (!TextUtils.isEmpty(aVar.c())) {
                return aVar;
            }
        }
        if (!c2.isEmpty()) {
            return c2.get(c2.size() - 1);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static CharSequence b(NotificationCompat.Builder builder, NotificationCompat.MessagingStyle messagingStyle, NotificationCompat.MessagingStyle.a aVar) {
        int i;
        CharSequence charSequence;
        CharSequence a2;
        android.support.v4.c.a a3 = android.support.v4.c.a.a();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        boolean z = Build.VERSION.SDK_INT >= 21;
        int i2 = (z || Build.VERSION.SDK_INT <= 10) ? -16777216 : -1;
        CharSequence c2 = aVar.c();
        if (TextUtils.isEmpty(aVar.c())) {
            if (messagingStyle.a() == null) {
                a2 = "";
            } else {
                a2 = messagingStyle.a();
            }
            if (z && builder.i() != 0) {
                i2 = builder.i();
            }
            CharSequence charSequence2 = a2;
            i = i2;
            charSequence = charSequence2;
        } else {
            CharSequence charSequence3 = c2;
            i = i2;
            charSequence = charSequence3;
        }
        CharSequence a4 = a3.a(charSequence);
        spannableStringBuilder.append(a4);
        spannableStringBuilder.setSpan(a(i), spannableStringBuilder.length() - a4.length(), spannableStringBuilder.length(), 33);
        spannableStringBuilder.append((CharSequence) "  ").append(a3.a(aVar.a() == null ? "" : aVar.a()));
        return spannableStringBuilder;
    }

    private static TextAppearanceSpan a(int i) {
        return new TextAppearanceSpan(null, 0, 0, ColorStateList.valueOf(i), null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder}
     arg types: [int, java.lang.String]
     candidates:
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.Editable}
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder}
     arg types: [int, java.lang.CharSequence]
     candidates:
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.Editable}
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder} */
    private static void a(NotificationCompat.MessagingStyle messagingStyle, ab abVar, NotificationCompat.Builder builder) {
        boolean z;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        List<NotificationCompat.MessagingStyle.a> c2 = messagingStyle.c();
        if (messagingStyle.b() != null || a(messagingStyle.c())) {
            z = true;
        } else {
            z = false;
        }
        for (int size = c2.size() - 1; size >= 0; size--) {
            NotificationCompat.MessagingStyle.a aVar = c2.get(size);
            CharSequence b2 = z ? b(builder, messagingStyle, aVar) : aVar.a();
            if (size != c2.size() - 1) {
                spannableStringBuilder.insert(0, (CharSequence) ShellUtils.COMMAND_LINE_END);
            }
            spannableStringBuilder.insert(0, b2);
        }
        l.a(abVar, spannableStringBuilder);
    }

    private static boolean a(List<NotificationCompat.MessagingStyle.a> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            if (list.get(size).c() == null) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    @TargetApi(14)
    public static RemoteViews h(ab abVar, NotificationCompat.Builder builder) {
        if (builder.m instanceof MediaStyle) {
            MediaStyle mediaStyle = (MediaStyle) builder.m;
            boolean z = (builder.m instanceof DecoratedMediaCustomViewStyle) && builder.d() != null;
            RemoteViews a2 = k.a(abVar, builder.f341a, builder.f342b, builder.f343c, builder.f348h, builder.i, builder.f347g, builder.n, builder.l, builder.g(), builder.h(), builder.v, mediaStyle.f1310a, mediaStyle.f1312c, mediaStyle.f1313h, z);
            if (z) {
                k.a(builder.f341a, a2, builder.d());
                return a2;
            }
        } else if (builder.m instanceof DecoratedCustomViewStyle) {
            return a(builder);
        }
        return null;
    }

    /* access modifiers changed from: private */
    @TargetApi(16)
    public static void d(Notification notification, NotificationCompat.Builder builder) {
        RemoteViews d2;
        if (builder.m instanceof MediaStyle) {
            MediaStyle mediaStyle = (MediaStyle) builder.m;
            if (builder.e() != null) {
                d2 = builder.e();
            } else {
                d2 = builder.d();
            }
            boolean z = (builder.m instanceof DecoratedMediaCustomViewStyle) && d2 != null;
            k.a(notification, builder.f341a, builder.f342b, builder.f343c, builder.f348h, builder.i, builder.f347g, builder.n, builder.l, builder.g(), builder.h(), 0, builder.v, mediaStyle.f1312c, mediaStyle.f1313h, z);
            if (z) {
                k.a(builder.f341a, notification.bigContentView, d2);
            }
        } else if (builder.m instanceof DecoratedCustomViewStyle) {
            e(notification, builder);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, int, ?[OBJECT, ARRAY]]
     candidates:
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews */
    private static RemoteViews a(NotificationCompat.Builder builder) {
        if (builder.d() == null) {
            return null;
        }
        RemoteViews a2 = k.a(builder.f341a, builder.f342b, builder.f343c, builder.f348h, builder.i, builder.F.icon, builder.f347g, builder.n, builder.l, builder.g(), builder.h(), builder.i(), a.h.notification_template_custom_big, false, (ArrayList<NotificationCompat.Action>) null);
        k.a(builder.f341a, a2, builder.d());
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, int, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>]
     candidates:
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews */
    @TargetApi(16)
    private static void e(Notification notification, NotificationCompat.Builder builder) {
        RemoteViews e2 = builder.e();
        if (e2 == null) {
            e2 = builder.d();
        }
        if (e2 != null) {
            RemoteViews a2 = k.a(builder.f341a, builder.f342b, builder.f343c, builder.f348h, builder.i, notification.icon, builder.f347g, builder.n, builder.l, builder.g(), builder.h(), builder.i(), a.h.notification_template_custom_big, false, builder.v);
            k.a(builder.f341a, a2, e2);
            notification.bigContentView = a2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, int, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>]
     candidates:
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews */
    @TargetApi(21)
    private static void f(Notification notification, NotificationCompat.Builder builder) {
        RemoteViews f2 = builder.f();
        RemoteViews d2 = f2 != null ? f2 : builder.d();
        if (f2 != null) {
            RemoteViews a2 = k.a(builder.f341a, builder.f342b, builder.f343c, builder.f348h, builder.i, notification.icon, builder.f347g, builder.n, builder.l, builder.g(), builder.h(), builder.i(), a.h.notification_template_custom_big, false, builder.v);
            k.a(builder.f341a, a2, d2);
            notification.headsUpContentView = a2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.k.a(android.app.Notification, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):void
     arg types: [android.app.Notification, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>, int, ?[OBJECT, ARRAY], int]
     candidates:
      android.support.v7.app.k.a(android.support.v4.app.ab, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.k.a(android.app.Notification, android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):void */
    /* access modifiers changed from: private */
    @TargetApi(21)
    public static void g(Notification notification, NotificationCompat.Builder builder) {
        RemoteViews d2;
        if (builder.e() != null) {
            d2 = builder.e();
        } else {
            d2 = builder.d();
        }
        if ((builder.m instanceof DecoratedMediaCustomViewStyle) && d2 != null) {
            k.a(notification, builder.f341a, builder.f342b, builder.f343c, builder.f348h, builder.i, builder.f347g, builder.n, builder.l, builder.g(), builder.h(), 0, (List) builder.v, false, (PendingIntent) null, true);
            k.a(builder.f341a, notification.bigContentView, d2);
            a(builder.f341a, notification.bigContentView, builder.i());
        } else if (builder.m instanceof DecoratedCustomViewStyle) {
            e(notification, builder);
        }
    }

    private static void a(Context context, RemoteViews remoteViews, int i) {
        if (i == 0) {
            i = context.getResources().getColor(a.c.notification_material_background_media_default_color);
        }
        remoteViews.setInt(a.f.status_bar_latest_event_content, "setBackgroundColor", i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
     arg types: [android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>, int, ?[OBJECT, ARRAY], int]
     candidates:
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean, java.util.ArrayList<android.support.v4.app.NotificationCompat$Action>):android.widget.RemoteViews
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, java.util.List, int[], boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews
      android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, java.util.List, boolean, android.app.PendingIntent, boolean):android.widget.RemoteViews */
    /* access modifiers changed from: private */
    @TargetApi(21)
    public static void h(Notification notification, NotificationCompat.Builder builder) {
        RemoteViews d2;
        if (builder.f() != null) {
            d2 = builder.f();
        } else {
            d2 = builder.d();
        }
        if ((builder.m instanceof DecoratedMediaCustomViewStyle) && d2 != null) {
            notification.headsUpContentView = k.a(builder.f341a, builder.f342b, builder.f343c, builder.f348h, builder.i, builder.f347g, builder.n, builder.l, builder.g(), builder.h(), 0, (List) builder.v, false, (PendingIntent) null, true);
            k.a(builder.f341a, notification.headsUpContentView, d2);
            a(builder.f341a, notification.headsUpContentView, builder.i());
        } else if (builder.m instanceof DecoratedCustomViewStyle) {
            f(notification, builder);
        }
    }

    public static class Builder extends NotificationCompat.Builder {
        public Builder(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public CharSequence j() {
            if (this.m instanceof NotificationCompat.MessagingStyle) {
                NotificationCompat.MessagingStyle messagingStyle = (NotificationCompat.MessagingStyle) this.m;
                NotificationCompat.MessagingStyle.a a2 = NotificationCompat.b(messagingStyle);
                CharSequence b2 = messagingStyle.b();
                if (a2 != null) {
                    if (b2 != null) {
                        return NotificationCompat.b(this, messagingStyle, a2);
                    }
                    return a2.a();
                }
            }
            return super.j();
        }

        /* access modifiers changed from: protected */
        public CharSequence k() {
            if (this.m instanceof NotificationCompat.MessagingStyle) {
                NotificationCompat.MessagingStyle messagingStyle = (NotificationCompat.MessagingStyle) this.m;
                NotificationCompat.MessagingStyle.a a2 = NotificationCompat.b(messagingStyle);
                CharSequence b2 = messagingStyle.b();
                if (!(b2 == null && a2 == null)) {
                    if (b2 != null) {
                        return b2;
                    }
                    return a2.c();
                }
            }
            return super.k();
        }

        /* access modifiers changed from: protected */
        public NotificationCompat.a c() {
            if (Build.VERSION.SDK_INT >= 24) {
                return new a();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                return new d();
            }
            if (Build.VERSION.SDK_INT >= 16) {
                return new c();
            }
            if (Build.VERSION.SDK_INT >= 14) {
                return new b();
            }
            return super.c();
        }
    }

    private static class b extends NotificationCompat.a {
        b() {
        }

        public Notification a(NotificationCompat.Builder builder, ab abVar) {
            RemoteViews a2 = NotificationCompat.h(abVar, builder);
            Notification b2 = abVar.b();
            if (a2 != null) {
                b2.contentView = a2;
            } else if (builder.d() != null) {
                b2.contentView = builder.d();
            }
            return b2;
        }
    }

    private static class c extends NotificationCompat.a {
        c() {
        }

        public Notification a(NotificationCompat.Builder builder, ab abVar) {
            RemoteViews b2 = NotificationCompat.g(abVar, builder);
            Notification b3 = abVar.b();
            if (b2 != null) {
                b3.contentView = b2;
            }
            NotificationCompat.d(b3, builder);
            return b3;
        }
    }

    private static class d extends NotificationCompat.a {
        d() {
        }

        public Notification a(NotificationCompat.Builder builder, ab abVar) {
            RemoteViews c2 = NotificationCompat.f(abVar, builder);
            Notification b2 = abVar.b();
            if (c2 != null) {
                b2.contentView = c2;
            }
            NotificationCompat.g(b2, builder);
            NotificationCompat.h(b2, builder);
            return b2;
        }
    }

    private static class a extends NotificationCompat.a {
        private a() {
        }

        public Notification a(NotificationCompat.Builder builder, ab abVar) {
            NotificationCompat.e(abVar, builder);
            return abVar.b();
        }
    }
}
