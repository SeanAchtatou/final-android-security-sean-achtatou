package android.support.v7.app;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.Window;

@TargetApi(11)
/* compiled from: AppCompatDelegateImplV11 */
class e extends AppCompatDelegateImplV9 {
    e(Context context, Window window, a aVar) {
        super(context, window, aVar);
    }

    /* access modifiers changed from: package-private */
    public View b(View view, String str, Context context, AttributeSet attributeSet) {
        return null;
    }
}
