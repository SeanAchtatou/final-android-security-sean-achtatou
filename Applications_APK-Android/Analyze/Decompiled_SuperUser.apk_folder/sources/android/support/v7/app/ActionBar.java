package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.app.t;
import android.support.v7.a.a;
import android.support.v7.view.b;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

public abstract class ActionBar {

    public interface a {
        void a(boolean z);
    }

    @Deprecated
    public static abstract class b {
        public abstract int a();

        public abstract Drawable b();

        public abstract CharSequence c();

        public abstract View d();

        public abstract void e();

        public abstract CharSequence f();
    }

    @Deprecated
    public interface c {
        void a(b bVar, t tVar);

        void b(b bVar, t tVar);

        void c(b bVar, t tVar);
    }

    public abstract int a();

    public abstract void a(int i);

    public abstract void a(View view, LayoutParams layoutParams);

    public abstract void a(CharSequence charSequence);

    public abstract void a(boolean z);

    public abstract void b(boolean z);

    public abstract boolean b();

    public abstract void c(boolean z);

    public void d(boolean z) {
    }

    public Context c() {
        return null;
    }

    public void b(int i) {
    }

    public void e(boolean z) {
        if (z) {
            throw new UnsupportedOperationException("Hide on content scroll is not supported in this action bar configuration.");
        }
    }

    public int d() {
        return 0;
    }

    public void a(float f2) {
        if (f2 != 0.0f) {
            throw new UnsupportedOperationException("Setting a non-zero elevation is not supported in this action bar configuration.");
        }
    }

    public void f(boolean z) {
    }

    public void g(boolean z) {
    }

    public void a(Configuration configuration) {
    }

    public void h(boolean z) {
    }

    public android.support.v7.view.b a(b.a aVar) {
        return null;
    }

    public boolean e() {
        return false;
    }

    public boolean a(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean f() {
        return false;
    }

    public void b(CharSequence charSequence) {
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void h() {
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {

        /* renamed from: a  reason: collision with root package name */
        public int f1232a;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.f1232a = 0;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.k.ActionBarLayout);
            this.f1232a = obtainStyledAttributes.getInt(a.k.ActionBarLayout_android_layout_gravity, 0);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.f1232a = 0;
            this.f1232a = 8388627;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.MarginLayoutParams) layoutParams);
            this.f1232a = 0;
            this.f1232a = layoutParams.f1232a;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.f1232a = 0;
        }
    }
}
