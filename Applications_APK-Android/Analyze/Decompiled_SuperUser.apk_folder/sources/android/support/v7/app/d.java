package android.support.v7.app;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v7.app.AppCompatDelegateImplV9;
import android.support.v7.app.g;
import android.view.KeyboardShortcutGroup;
import android.view.Menu;
import android.view.Window;
import java.util.List;

@TargetApi(24)
/* compiled from: AppCompatDelegateImplN */
class d extends g {
    d(Context context, Window window, a aVar) {
        super(context, window, aVar);
    }

    /* access modifiers changed from: package-private */
    public Window.Callback a(Window.Callback callback) {
        return new a(callback);
    }

    /* compiled from: AppCompatDelegateImplN */
    class a extends g.a {
        a(Window.Callback callback) {
            super(callback);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState
         arg types: [int, int]
         candidates:
          android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, android.view.KeyEvent):void
          android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.view.menu.MenuBuilder, boolean):void
          android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.Menu):void
          android.support.v7.app.AppCompatDelegateImplV9.a(android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState, boolean):void
          android.support.v7.app.AppCompatDelegateImplV9.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          android.support.v7.app.AppCompatDelegateImplV9.a(int, android.view.KeyEvent):boolean
          android.support.v7.app.c.a(int, android.view.Menu):void
          android.support.v7.app.c.a(int, android.view.KeyEvent):boolean
          android.support.v7.app.b.a(android.app.Activity, android.support.v7.app.a):android.support.v7.app.b
          android.support.v7.app.b.a(android.app.Dialog, android.support.v7.app.a):android.support.v7.app.b
          android.support.v7.app.b.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          android.support.v7.app.AppCompatDelegateImplV9.a(int, boolean):android.support.v7.app.AppCompatDelegateImplV9$PanelFeatureState */
        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
            AppCompatDelegateImplV9.PanelFeatureState a2 = d.this.a(0, true);
            if (a2 == null || a2.j == null) {
                super.onProvideKeyboardShortcuts(list, menu, i);
            } else {
                super.onProvideKeyboardShortcuts(list, a2.j, i);
            }
        }
    }
}
