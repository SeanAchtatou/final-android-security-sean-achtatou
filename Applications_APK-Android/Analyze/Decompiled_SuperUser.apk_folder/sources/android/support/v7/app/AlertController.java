package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ag;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.a.a;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CursorAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.lang.ref.WeakReference;

class AlertController {
    private boolean A = false;
    private CharSequence B;
    private CharSequence C;
    private CharSequence D;
    private int E = 0;
    private Drawable F;
    private ImageView G;
    private TextView H;
    private TextView I;
    private View J;
    private int K;
    private int L;
    private boolean M;
    private int N = 0;
    private final View.OnClickListener O = new View.OnClickListener() {
        public void onClick(View view) {
            Message message;
            if (view == AlertController.this.f1241c && AlertController.this.f1242d != null) {
                message = Message.obtain(AlertController.this.f1242d);
            } else if (view == AlertController.this.f1243e && AlertController.this.f1244f != null) {
                message = Message.obtain(AlertController.this.f1244f);
            } else if (view != AlertController.this.f1245g || AlertController.this.f1246h == null) {
                message = null;
            } else {
                message = Message.obtain(AlertController.this.f1246h);
            }
            if (message != null) {
                message.sendToTarget();
            }
            AlertController.this.p.obtainMessage(1, AlertController.this.f1239a).sendToTarget();
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final AppCompatDialog f1239a;

    /* renamed from: b  reason: collision with root package name */
    ListView f1240b;

    /* renamed from: c  reason: collision with root package name */
    Button f1241c;

    /* renamed from: d  reason: collision with root package name */
    Message f1242d;

    /* renamed from: e  reason: collision with root package name */
    Button f1243e;

    /* renamed from: f  reason: collision with root package name */
    Message f1244f;

    /* renamed from: g  reason: collision with root package name */
    Button f1245g;

    /* renamed from: h  reason: collision with root package name */
    Message f1246h;
    NestedScrollView i;
    ListAdapter j;
    int k = -1;
    int l;
    int m;
    int n;
    int o;
    Handler p;
    private final Context q;
    private final Window r;
    private CharSequence s;
    private CharSequence t;
    private View u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    private static final class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<DialogInterface> f1282a;

        public a(DialogInterface dialogInterface) {
            this.f1282a = new WeakReference<>(dialogInterface);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case -3:
                case -2:
                case -1:
                    ((DialogInterface.OnClickListener) message.obj).onClick(this.f1282a.get(), message.what);
                    return;
                case 0:
                default:
                    return;
                case 1:
                    ((DialogInterface) message.obj).dismiss();
                    return;
            }
        }
    }

    private static boolean a(Context context) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(a.C0027a.alertDialogCenterButtons, typedValue, true);
        if (typedValue.data != 0) {
            return true;
        }
        return false;
    }

    public AlertController(Context context, AppCompatDialog appCompatDialog, Window window) {
        this.q = context;
        this.f1239a = appCompatDialog;
        this.r = window;
        this.p = new a(appCompatDialog);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, a.k.AlertDialog, a.C0027a.alertDialogStyle, 0);
        this.K = obtainStyledAttributes.getResourceId(a.k.AlertDialog_android_layout, 0);
        this.L = obtainStyledAttributes.getResourceId(a.k.AlertDialog_buttonPanelSideLayout, 0);
        this.l = obtainStyledAttributes.getResourceId(a.k.AlertDialog_listLayout, 0);
        this.m = obtainStyledAttributes.getResourceId(a.k.AlertDialog_multiChoiceItemLayout, 0);
        this.n = obtainStyledAttributes.getResourceId(a.k.AlertDialog_singleChoiceItemLayout, 0);
        this.o = obtainStyledAttributes.getResourceId(a.k.AlertDialog_listItemLayout, 0);
        this.M = obtainStyledAttributes.getBoolean(a.k.AlertDialog_showTitle, true);
        obtainStyledAttributes.recycle();
        appCompatDialog.supportRequestWindowFeature(1);
    }

    static boolean a(View view) {
        if (view.onCheckIsTextEditor()) {
            return true;
        }
        if (!(view instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        while (childCount > 0) {
            childCount--;
            if (a(viewGroup.getChildAt(childCount))) {
                return true;
            }
        }
        return false;
    }

    public void a() {
        this.f1239a.setContentView(b());
        c();
    }

    private int b() {
        if (this.L == 0) {
            return this.K;
        }
        if (this.N == 1) {
            return this.L;
        }
        return this.K;
    }

    public void a(CharSequence charSequence) {
        this.s = charSequence;
        if (this.H != null) {
            this.H.setText(charSequence);
        }
    }

    public void b(View view) {
        this.J = view;
    }

    public void b(CharSequence charSequence) {
        this.t = charSequence;
        if (this.I != null) {
            this.I.setText(charSequence);
        }
    }

    public void a(int i2) {
        this.u = null;
        this.v = i2;
        this.A = false;
    }

    public void c(View view) {
        this.u = view;
        this.v = 0;
        this.A = false;
    }

    public void a(View view, int i2, int i3, int i4, int i5) {
        this.u = view;
        this.v = 0;
        this.A = true;
        this.w = i2;
        this.x = i3;
        this.y = i4;
        this.z = i5;
    }

    public void a(int i2, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, Message message) {
        if (message == null && onClickListener != null) {
            message = this.p.obtainMessage(i2, onClickListener);
        }
        switch (i2) {
            case -3:
                this.D = charSequence;
                this.f1246h = message;
                return;
            case -2:
                this.C = charSequence;
                this.f1244f = message;
                return;
            case -1:
                this.B = charSequence;
                this.f1242d = message;
                return;
            default:
                throw new IllegalArgumentException("Button does not exist");
        }
    }

    public void b(int i2) {
        this.F = null;
        this.E = i2;
        if (this.G == null) {
            return;
        }
        if (i2 != 0) {
            this.G.setVisibility(0);
            this.G.setImageResource(this.E);
            return;
        }
        this.G.setVisibility(8);
    }

    public void a(Drawable drawable) {
        this.F = drawable;
        this.E = 0;
        if (this.G == null) {
            return;
        }
        if (drawable != null) {
            this.G.setVisibility(0);
            this.G.setImageDrawable(drawable);
            return;
        }
        this.G.setVisibility(8);
    }

    public int c(int i2) {
        TypedValue typedValue = new TypedValue();
        this.q.getTheme().resolveAttribute(i2, typedValue, true);
        return typedValue.resourceId;
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        return this.i != null && this.i.a(keyEvent);
    }

    public boolean b(int i2, KeyEvent keyEvent) {
        return this.i != null && this.i.a(keyEvent);
    }

    private ViewGroup a(View view, View view2) {
        View view3;
        View view4;
        if (view == null) {
            if (view2 instanceof ViewStub) {
                view4 = ((ViewStub) view2).inflate();
            } else {
                view4 = view2;
            }
            return (ViewGroup) view4;
        }
        if (view2 != null) {
            ViewParent parent = view2.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(view2);
            }
        }
        if (view instanceof ViewStub) {
            view3 = ((ViewStub) view).inflate();
        } else {
            view3 = view;
        }
        return (ViewGroup) view3;
    }

    private void c() {
        boolean z2;
        boolean z3;
        View findViewById;
        int i2;
        View findViewById2;
        View findViewById3 = this.r.findViewById(a.f.parentPanel);
        View findViewById4 = findViewById3.findViewById(a.f.topPanel);
        View findViewById5 = findViewById3.findViewById(a.f.contentPanel);
        View findViewById6 = findViewById3.findViewById(a.f.buttonPanel);
        ViewGroup viewGroup = (ViewGroup) findViewById3.findViewById(a.f.customPanel);
        a(viewGroup);
        View findViewById7 = viewGroup.findViewById(a.f.topPanel);
        View findViewById8 = viewGroup.findViewById(a.f.contentPanel);
        View findViewById9 = viewGroup.findViewById(a.f.buttonPanel);
        ViewGroup a2 = a(findViewById7, findViewById4);
        ViewGroup a3 = a(findViewById8, findViewById5);
        ViewGroup a4 = a(findViewById9, findViewById6);
        c(a3);
        d(a4);
        b(a2);
        boolean z4 = (viewGroup == null || viewGroup.getVisibility() == 8) ? false : true;
        if (a2 == null || a2.getVisibility() == 8) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (a4 == null || a4.getVisibility() == 8) {
            z3 = false;
        } else {
            z3 = true;
        }
        if (!(z3 || a3 == null || (findViewById2 = a3.findViewById(a.f.textSpacerNoButtons)) == null)) {
            findViewById2.setVisibility(0);
        }
        if (z2) {
            if (this.i != null) {
                this.i.setClipToPadding(true);
            }
            View view = null;
            if (!(this.t == null && this.f1240b == null && !z4) && !z4) {
                view = a2.findViewById(a.f.titleDividerNoCustom);
            }
            if (view != null) {
                view.setVisibility(0);
            }
        } else if (!(a3 == null || (findViewById = a3.findViewById(a.f.textSpacerNoTitle)) == null)) {
            findViewById.setVisibility(0);
        }
        if (this.f1240b instanceof RecycleListView) {
            ((RecycleListView) this.f1240b).a(z2, z3);
        }
        if (!z4) {
            ViewGroup viewGroup2 = this.f1240b != null ? this.f1240b : this.i;
            if (viewGroup2 != null) {
                if (z2) {
                    i2 = 1;
                } else {
                    i2 = 0;
                }
                a(a3, viewGroup2, (z3 ? 2 : 0) | i2, 3);
            }
        }
        ListView listView = this.f1240b;
        if (listView != null && this.j != null) {
            listView.setAdapter(this.j);
            int i3 = this.k;
            if (i3 > -1) {
                listView.setItemChecked(i3, true);
                listView.setSelection(i3);
            }
        }
    }

    private void a(ViewGroup viewGroup, View view, int i2, int i3) {
        final View view2 = null;
        final View findViewById = this.r.findViewById(a.f.scrollIndicatorUp);
        View findViewById2 = this.r.findViewById(a.f.scrollIndicatorDown);
        if (Build.VERSION.SDK_INT >= 23) {
            ag.a(view, i2, i3);
            if (findViewById != null) {
                viewGroup.removeView(findViewById);
            }
            if (findViewById2 != null) {
                viewGroup.removeView(findViewById2);
                return;
            }
            return;
        }
        if (findViewById != null && (i2 & 1) == 0) {
            viewGroup.removeView(findViewById);
            findViewById = null;
        }
        if (findViewById2 == null || (i2 & 2) != 0) {
            view2 = findViewById2;
        } else {
            viewGroup.removeView(findViewById2);
        }
        if (findViewById != null || view2 != null) {
            if (this.t != null) {
                this.i.setOnScrollChangeListener(new NestedScrollView.b() {
                    public void a(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
                        AlertController.a(nestedScrollView, findViewById, view2);
                    }
                });
                this.i.post(new Runnable() {
                    public void run() {
                        AlertController.a(AlertController.this.i, findViewById, view2);
                    }
                });
            } else if (this.f1240b != null) {
                this.f1240b.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScrollStateChanged(AbsListView absListView, int i) {
                    }

                    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                        AlertController.a(absListView, findViewById, view2);
                    }
                });
                this.f1240b.post(new Runnable() {
                    public void run() {
                        AlertController.a(AlertController.this.f1240b, findViewById, view2);
                    }
                });
            } else {
                if (findViewById != null) {
                    viewGroup.removeView(findViewById);
                }
                if (view2 != null) {
                    viewGroup.removeView(view2);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(ViewGroup viewGroup) {
        View view;
        boolean z2 = false;
        if (this.u != null) {
            view = this.u;
        } else if (this.v != 0) {
            view = LayoutInflater.from(this.q).inflate(this.v, viewGroup, false);
        } else {
            view = null;
        }
        if (view != null) {
            z2 = true;
        }
        if (!z2 || !a(view)) {
            this.r.setFlags(131072, 131072);
        }
        if (z2) {
            FrameLayout frameLayout = (FrameLayout) this.r.findViewById(a.f.custom);
            frameLayout.addView(view, new ViewGroup.LayoutParams(-1, -1));
            if (this.A) {
                frameLayout.setPadding(this.w, this.x, this.y, this.z);
            }
            if (this.f1240b != null) {
                ((LinearLayout.LayoutParams) viewGroup.getLayoutParams()).weight = 0.0f;
                return;
            }
            return;
        }
        viewGroup.setVisibility(8);
    }

    private void b(ViewGroup viewGroup) {
        if (this.J != null) {
            viewGroup.addView(this.J, 0, new ViewGroup.LayoutParams(-1, -2));
            this.r.findViewById(a.f.title_template).setVisibility(8);
            return;
        }
        this.G = (ImageView) this.r.findViewById(16908294);
        if (!(!TextUtils.isEmpty(this.s)) || !this.M) {
            this.r.findViewById(a.f.title_template).setVisibility(8);
            this.G.setVisibility(8);
            viewGroup.setVisibility(8);
            return;
        }
        this.H = (TextView) this.r.findViewById(a.f.alertTitle);
        this.H.setText(this.s);
        if (this.E != 0) {
            this.G.setImageResource(this.E);
        } else if (this.F != null) {
            this.G.setImageDrawable(this.F);
        } else {
            this.H.setPadding(this.G.getPaddingLeft(), this.G.getPaddingTop(), this.G.getPaddingRight(), this.G.getPaddingBottom());
            this.G.setVisibility(8);
        }
    }

    private void c(ViewGroup viewGroup) {
        this.i = (NestedScrollView) this.r.findViewById(a.f.scrollView);
        this.i.setFocusable(false);
        this.i.setNestedScrollingEnabled(false);
        this.I = (TextView) viewGroup.findViewById(16908299);
        if (this.I != null) {
            if (this.t != null) {
                this.I.setText(this.t);
                return;
            }
            this.I.setVisibility(8);
            this.i.removeView(this.I);
            if (this.f1240b != null) {
                ViewGroup viewGroup2 = (ViewGroup) this.i.getParent();
                int indexOfChild = viewGroup2.indexOfChild(this.i);
                viewGroup2.removeViewAt(indexOfChild);
                viewGroup2.addView(this.f1240b, indexOfChild, new ViewGroup.LayoutParams(-1, -1));
                return;
            }
            viewGroup.setVisibility(8);
        }
    }

    static void a(View view, View view2, View view3) {
        int i2 = 0;
        if (view2 != null) {
            view2.setVisibility(ag.b(view, -1) ? 0 : 4);
        }
        if (view3 != null) {
            if (!ag.b(view, 1)) {
                i2 = 4;
            }
            view3.setVisibility(i2);
        }
    }

    private void d(ViewGroup viewGroup) {
        boolean z2;
        boolean z3 = true;
        this.f1241c = (Button) viewGroup.findViewById(16908313);
        this.f1241c.setOnClickListener(this.O);
        if (TextUtils.isEmpty(this.B)) {
            this.f1241c.setVisibility(8);
            z2 = false;
        } else {
            this.f1241c.setText(this.B);
            this.f1241c.setVisibility(0);
            z2 = true;
        }
        this.f1243e = (Button) viewGroup.findViewById(16908314);
        this.f1243e.setOnClickListener(this.O);
        if (TextUtils.isEmpty(this.C)) {
            this.f1243e.setVisibility(8);
        } else {
            this.f1243e.setText(this.C);
            this.f1243e.setVisibility(0);
            z2 |= true;
        }
        this.f1245g = (Button) viewGroup.findViewById(16908315);
        this.f1245g.setOnClickListener(this.O);
        if (TextUtils.isEmpty(this.D)) {
            this.f1245g.setVisibility(8);
        } else {
            this.f1245g.setText(this.D);
            this.f1245g.setVisibility(0);
            z2 |= true;
        }
        if (a(this.q)) {
            if (z2) {
                a(this.f1241c);
            } else if (z2) {
                a(this.f1243e);
            } else if (z2) {
                a(this.f1245g);
            }
        }
        if (!z2) {
            z3 = false;
        }
        if (!z3) {
            viewGroup.setVisibility(8);
        }
    }

    private void a(Button button) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) button.getLayoutParams();
        layoutParams.gravity = 1;
        layoutParams.weight = 0.5f;
        button.setLayoutParams(layoutParams);
    }

    public static class RecycleListView extends ListView {

        /* renamed from: a  reason: collision with root package name */
        private final int f1280a;

        /* renamed from: b  reason: collision with root package name */
        private final int f1281b;

        public RecycleListView(Context context) {
            this(context, null);
        }

        public RecycleListView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.k.RecycleListView);
            this.f1281b = obtainStyledAttributes.getDimensionPixelOffset(a.k.RecycleListView_paddingBottomNoButtons, -1);
            this.f1280a = obtainStyledAttributes.getDimensionPixelOffset(a.k.RecycleListView_paddingTopNoTitle, -1);
        }

        public void a(boolean z, boolean z2) {
            if (!z2 || !z) {
                setPadding(getPaddingLeft(), z ? getPaddingTop() : this.f1280a, getPaddingRight(), z2 ? getPaddingBottom() : this.f1281b);
            }
        }
    }

    public static class AlertParams {
        public int A;
        public boolean B = false;
        public boolean[] C;
        public boolean D;
        public boolean E;
        public int F = -1;
        public DialogInterface.OnMultiChoiceClickListener G;
        public Cursor H;
        public String I;
        public String J;
        public AdapterView.OnItemSelectedListener K;
        public a L;
        public boolean M = true;

        /* renamed from: a  reason: collision with root package name */
        public final Context f1260a;

        /* renamed from: b  reason: collision with root package name */
        public final LayoutInflater f1261b;

        /* renamed from: c  reason: collision with root package name */
        public int f1262c = 0;

        /* renamed from: d  reason: collision with root package name */
        public Drawable f1263d;

        /* renamed from: e  reason: collision with root package name */
        public int f1264e = 0;

        /* renamed from: f  reason: collision with root package name */
        public CharSequence f1265f;

        /* renamed from: g  reason: collision with root package name */
        public View f1266g;

        /* renamed from: h  reason: collision with root package name */
        public CharSequence f1267h;
        public CharSequence i;
        public DialogInterface.OnClickListener j;
        public CharSequence k;
        public DialogInterface.OnClickListener l;
        public CharSequence m;
        public DialogInterface.OnClickListener n;
        public boolean o;
        public DialogInterface.OnCancelListener p;
        public DialogInterface.OnDismissListener q;
        public DialogInterface.OnKeyListener r;
        public CharSequence[] s;
        public ListAdapter t;
        public DialogInterface.OnClickListener u;
        public int v;
        public View w;
        public int x;
        public int y;
        public int z;

        public interface a {
            void a(ListView listView);
        }

        public AlertParams(Context context) {
            this.f1260a = context;
            this.o = true;
            this.f1261b = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        public void a(AlertController alertController) {
            if (this.f1266g != null) {
                alertController.b(this.f1266g);
            } else {
                if (this.f1265f != null) {
                    alertController.a(this.f1265f);
                }
                if (this.f1263d != null) {
                    alertController.a(this.f1263d);
                }
                if (this.f1262c != 0) {
                    alertController.b(this.f1262c);
                }
                if (this.f1264e != 0) {
                    alertController.b(alertController.c(this.f1264e));
                }
            }
            if (this.f1267h != null) {
                alertController.b(this.f1267h);
            }
            if (this.i != null) {
                alertController.a(-1, this.i, this.j, (Message) null);
            }
            if (this.k != null) {
                alertController.a(-2, this.k, this.l, (Message) null);
            }
            if (this.m != null) {
                alertController.a(-3, this.m, this.n, (Message) null);
            }
            if (!(this.s == null && this.H == null && this.t == null)) {
                b(alertController);
            }
            if (this.w != null) {
                if (this.B) {
                    alertController.a(this.w, this.x, this.y, this.z, this.A);
                    return;
                }
                alertController.c(this.w);
            } else if (this.v != 0) {
                alertController.a(this.v);
            }
        }

        private void b(final AlertController alertController) {
            int i2;
            ListAdapter bVar;
            final RecycleListView recycleListView = (RecycleListView) this.f1261b.inflate(alertController.l, (ViewGroup) null);
            if (!this.D) {
                if (this.E) {
                    i2 = alertController.n;
                } else {
                    i2 = alertController.o;
                }
                if (this.H != null) {
                    bVar = new SimpleCursorAdapter(this.f1260a, i2, this.H, new String[]{this.I}, new int[]{16908308});
                } else if (this.t != null) {
                    bVar = this.t;
                } else {
                    bVar = new b(this.f1260a, i2, 16908308, this.s);
                }
            } else if (this.H == null) {
                bVar = new ArrayAdapter<CharSequence>(this.f1260a, alertController.m, 16908308, this.s) {
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        View view2 = super.getView(i, view, viewGroup);
                        if (AlertParams.this.C != null && AlertParams.this.C[i]) {
                            recycleListView.setItemChecked(i, true);
                        }
                        return view2;
                    }
                };
            } else {
                final AlertController alertController2 = alertController;
                bVar = new CursorAdapter(this.f1260a, this.H, false) {

                    /* renamed from: d  reason: collision with root package name */
                    private final int f1273d;

                    /* renamed from: e  reason: collision with root package name */
                    private final int f1274e;

                    {
                        Cursor cursor = getCursor();
                        this.f1273d = cursor.getColumnIndexOrThrow(AlertParams.this.I);
                        this.f1274e = cursor.getColumnIndexOrThrow(AlertParams.this.J);
                    }

                    public void bindView(View view, Context context, Cursor cursor) {
                        ((CheckedTextView) view.findViewById(16908308)).setText(cursor.getString(this.f1273d));
                        recycleListView.setItemChecked(cursor.getPosition(), cursor.getInt(this.f1274e) == 1);
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
                     arg types: [int, android.view.ViewGroup, int]
                     candidates:
                      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
                      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
                    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
                        return AlertParams.this.f1261b.inflate(alertController2.m, viewGroup, false);
                    }
                };
            }
            if (this.L != null) {
                this.L.a(recycleListView);
            }
            alertController.j = bVar;
            alertController.k = this.F;
            if (this.u != null) {
                recycleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        AlertParams.this.u.onClick(alertController.f1239a, i);
                        if (!AlertParams.this.E) {
                            alertController.f1239a.dismiss();
                        }
                    }
                });
            } else if (this.G != null) {
                recycleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        if (AlertParams.this.C != null) {
                            AlertParams.this.C[i] = recycleListView.isItemChecked(i);
                        }
                        AlertParams.this.G.onClick(alertController.f1239a, i, recycleListView.isItemChecked(i));
                    }
                });
            }
            if (this.K != null) {
                recycleListView.setOnItemSelectedListener(this.K);
            }
            if (this.E) {
                recycleListView.setChoiceMode(1);
            } else if (this.D) {
                recycleListView.setChoiceMode(2);
            }
            alertController.f1240b = recycleListView;
        }
    }

    private static class b extends ArrayAdapter<CharSequence> {
        public b(Context context, int i, int i2, CharSequence[] charSequenceArr) {
            super(context, i, i2, charSequenceArr);
        }

        public boolean hasStableIds() {
            return true;
        }

        public long getItemId(int i) {
            return (long) i;
        }
    }
}
