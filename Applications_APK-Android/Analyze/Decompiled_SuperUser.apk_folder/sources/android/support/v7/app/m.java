package android.support.v7.app;

import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import android.util.LongSparseArray;
import java.lang.reflect.Field;
import java.util.Map;

/* compiled from: ResourcesFlusher */
class m {

    /* renamed from: a  reason: collision with root package name */
    private static Field f1367a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f1368b;

    /* renamed from: c  reason: collision with root package name */
    private static Class f1369c;

    /* renamed from: d  reason: collision with root package name */
    private static boolean f1370d;

    /* renamed from: e  reason: collision with root package name */
    private static Field f1371e;

    /* renamed from: f  reason: collision with root package name */
    private static boolean f1372f;

    /* renamed from: g  reason: collision with root package name */
    private static Field f1373g;

    /* renamed from: h  reason: collision with root package name */
    private static boolean f1374h;

    static boolean a(Resources resources) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            return d(resources);
        }
        if (i >= 23) {
            return c(resources);
        }
        if (i >= 21) {
            return b(resources);
        }
        return false;
    }

    private static boolean b(Resources resources) {
        Map map;
        if (!f1368b) {
            try {
                f1367a = Resources.class.getDeclaredField("mDrawableCache");
                f1367a.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", e2);
            }
            f1368b = true;
        }
        if (f1367a != null) {
            try {
                map = (Map) f1367a.get(resources);
            } catch (IllegalAccessException e3) {
                Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", e3);
                map = null;
            }
            if (map != null) {
                map.clear();
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0039 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean c(android.content.res.Resources r6) {
        /*
            r1 = 0
            r0 = 1
            boolean r2 = android.support.v7.app.m.f1368b
            if (r2 != 0) goto L_0x0018
            java.lang.Class<android.content.res.Resources> r2 = android.content.res.Resources.class
            java.lang.String r3 = "mDrawableCache"
            java.lang.reflect.Field r2 = r2.getDeclaredField(r3)     // Catch:{ NoSuchFieldException -> 0x0026 }
            android.support.v7.app.m.f1367a = r2     // Catch:{ NoSuchFieldException -> 0x0026 }
            java.lang.reflect.Field r2 = android.support.v7.app.m.f1367a     // Catch:{ NoSuchFieldException -> 0x0026 }
            r3 = 1
            r2.setAccessible(r3)     // Catch:{ NoSuchFieldException -> 0x0026 }
        L_0x0016:
            android.support.v7.app.m.f1368b = r0
        L_0x0018:
            r3 = 0
            java.lang.reflect.Field r2 = android.support.v7.app.m.f1367a
            if (r2 == 0) goto L_0x0037
            java.lang.reflect.Field r2 = android.support.v7.app.m.f1367a     // Catch:{ IllegalAccessException -> 0x002f }
            java.lang.Object r2 = r2.get(r6)     // Catch:{ IllegalAccessException -> 0x002f }
        L_0x0023:
            if (r2 != 0) goto L_0x0039
        L_0x0025:
            return r1
        L_0x0026:
            r2 = move-exception
            java.lang.String r3 = "ResourcesFlusher"
            java.lang.String r4 = "Could not retrieve Resources#mDrawableCache field"
            android.util.Log.e(r3, r4, r2)
            goto L_0x0016
        L_0x002f:
            r2 = move-exception
            java.lang.String r4 = "ResourcesFlusher"
            java.lang.String r5 = "Could not retrieve value from Resources#mDrawableCache"
            android.util.Log.e(r4, r5, r2)
        L_0x0037:
            r2 = r3
            goto L_0x0023
        L_0x0039:
            if (r2 == 0) goto L_0x0043
            boolean r2 = a(r2)
            if (r2 == 0) goto L_0x0043
        L_0x0041:
            r1 = r0
            goto L_0x0025
        L_0x0043:
            r0 = r1
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.m.c(android.content.res.Resources):boolean");
    }

    private static boolean d(Resources resources) {
        Object obj;
        Object obj2;
        boolean z = true;
        if (!f1374h) {
            try {
                f1373g = Resources.class.getDeclaredField("mResourcesImpl");
                f1373g.setAccessible(true);
            } catch (NoSuchFieldException e2) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mResourcesImpl field", e2);
            }
            f1374h = true;
        }
        if (f1373g == null) {
            return false;
        }
        try {
            obj = f1373g.get(resources);
        } catch (IllegalAccessException e3) {
            Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mResourcesImpl", e3);
            obj = null;
        }
        if (obj == null) {
            return false;
        }
        if (!f1368b) {
            try {
                f1367a = obj.getClass().getDeclaredField("mDrawableCache");
                f1367a.setAccessible(true);
            } catch (NoSuchFieldException e4) {
                Log.e("ResourcesFlusher", "Could not retrieve ResourcesImpl#mDrawableCache field", e4);
            }
            f1368b = true;
        }
        if (f1367a != null) {
            try {
                obj2 = f1367a.get(obj);
            } catch (IllegalAccessException e5) {
                Log.e("ResourcesFlusher", "Could not retrieve value from ResourcesImpl#mDrawableCache", e5);
            }
            if (obj2 == null || !a(obj2)) {
                z = false;
            }
            return z;
        }
        obj2 = null;
        z = false;
        return z;
    }

    private static boolean a(Object obj) {
        LongSparseArray longSparseArray;
        if (!f1370d) {
            try {
                f1369c = Class.forName("android.content.res.ThemedResourceCache");
            } catch (ClassNotFoundException e2) {
                Log.e("ResourcesFlusher", "Could not find ThemedResourceCache class", e2);
            }
            f1370d = true;
        }
        if (f1369c == null) {
            return false;
        }
        if (!f1372f) {
            try {
                f1371e = f1369c.getDeclaredField("mUnthemedEntries");
                f1371e.setAccessible(true);
            } catch (NoSuchFieldException e3) {
                Log.e("ResourcesFlusher", "Could not retrieve ThemedResourceCache#mUnthemedEntries field", e3);
            }
            f1372f = true;
        }
        if (f1371e == null) {
            return false;
        }
        try {
            longSparseArray = (LongSparseArray) f1371e.get(obj);
        } catch (IllegalAccessException e4) {
            Log.e("ResourcesFlusher", "Could not retrieve value from ThemedResourceCache#mUnthemedEntries", e4);
            longSparseArray = null;
        }
        if (longSparseArray == null) {
            return false;
        }
        longSparseArray.clear();
        return true;
    }
}
