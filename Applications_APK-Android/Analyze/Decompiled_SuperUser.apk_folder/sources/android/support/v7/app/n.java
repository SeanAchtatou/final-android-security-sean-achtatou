package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v4.view.ag;
import android.support.v7.a.a;
import android.support.v7.app.ActionBar;
import android.support.v7.view.f;
import android.support.v7.view.menu.ListMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.i;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ToolbarWidgetWrapper;
import android.support.v7.widget.t;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import java.util.ArrayList;

/* compiled from: ToolbarActionBar */
class n extends ActionBar {

    /* renamed from: a  reason: collision with root package name */
    t f1375a;

    /* renamed from: b  reason: collision with root package name */
    boolean f1376b;

    /* renamed from: c  reason: collision with root package name */
    Window.Callback f1377c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f1378d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f1379e;

    /* renamed from: f  reason: collision with root package name */
    private ArrayList<ActionBar.a> f1380f = new ArrayList<>();

    /* renamed from: g  reason: collision with root package name */
    private ListMenuPresenter f1381g;

    /* renamed from: h  reason: collision with root package name */
    private final Runnable f1382h = new Runnable() {
        public void run() {
            n.this.j();
        }
    };
    private final Toolbar.b i = new Toolbar.b() {
        public boolean a(MenuItem menuItem) {
            return n.this.f1377c.onMenuItemSelected(0, menuItem);
        }
    };

    public n(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.f1375a = new ToolbarWidgetWrapper(toolbar, false);
        this.f1377c = new d(callback);
        this.f1375a.a(this.f1377c);
        toolbar.setOnMenuItemClickListener(this.i);
        this.f1375a.a(charSequence);
    }

    public Window.Callback i() {
        return this.f1377c;
    }

    public void a(View view, ActionBar.LayoutParams layoutParams) {
        if (view != null) {
            view.setLayoutParams(layoutParams);
        }
        this.f1375a.a(view);
    }

    public void d(boolean z) {
    }

    public void a(float f2) {
        ag.h(this.f1375a.a(), f2);
    }

    public Context c() {
        return this.f1375a.b();
    }

    public void b(int i2) {
        this.f1375a.d(i2);
    }

    public void f(boolean z) {
    }

    public void g(boolean z) {
    }

    public void a(Configuration configuration) {
        super.a(configuration);
    }

    public void a(CharSequence charSequence) {
        this.f1375a.b(charSequence);
    }

    public void b(CharSequence charSequence) {
        this.f1375a.a(charSequence);
    }

    public boolean g() {
        ViewGroup a2 = this.f1375a.a();
        if (a2 == null || a2.hasFocus()) {
            return false;
        }
        a2.requestFocus();
        return true;
    }

    public void a(int i2) {
        a(i2, -1);
    }

    public void a(int i2, int i3) {
        this.f1375a.c((this.f1375a.o() & (i3 ^ -1)) | (i2 & i3));
    }

    public void a(boolean z) {
        a(z ? 4 : 0, 4);
    }

    public void b(boolean z) {
        a(z ? 8 : 0, 8);
    }

    public void c(boolean z) {
        a(z ? 16 : 0, 16);
    }

    public int a() {
        return this.f1375a.o();
    }

    public boolean b() {
        return this.f1375a.q() == 0;
    }

    public boolean e() {
        this.f1375a.a().removeCallbacks(this.f1382h);
        ag.a(this.f1375a.a(), this.f1382h);
        return true;
    }

    public boolean f() {
        if (!this.f1375a.c()) {
            return false;
        }
        this.f1375a.d();
        return true;
    }

    /* access modifiers changed from: package-private */
    public void j() {
        Menu k = k();
        MenuBuilder menuBuilder = k instanceof MenuBuilder ? (MenuBuilder) k : null;
        if (menuBuilder != null) {
            menuBuilder.stopDispatchingItemsChanged();
        }
        try {
            k.clear();
            if (!this.f1377c.onCreatePanelMenu(0, k) || !this.f1377c.onPreparePanel(0, null, k)) {
                k.clear();
            }
        } finally {
            if (menuBuilder != null) {
                menuBuilder.startDispatchingItemsChanged();
            }
        }
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        boolean z;
        Menu k = k();
        if (k != null) {
            if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1) {
                z = true;
            } else {
                z = false;
            }
            k.setQwertyMode(z);
            k.performShortcut(i2, keyEvent, 0);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        this.f1375a.a().removeCallbacks(this.f1382h);
    }

    public void h(boolean z) {
        if (z != this.f1379e) {
            this.f1379e = z;
            int size = this.f1380f.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f1380f.get(i2).a(z);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public View a(Menu menu) {
        b(menu);
        if (menu == null || this.f1381g == null || this.f1381g.a().getCount() <= 0) {
            return null;
        }
        return (View) this.f1381g.a(this.f1375a.a());
    }

    private void b(Menu menu) {
        if (this.f1381g == null && (menu instanceof MenuBuilder)) {
            MenuBuilder menuBuilder = (MenuBuilder) menu;
            Context b2 = this.f1375a.b();
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = b2.getResources().newTheme();
            newTheme.setTo(b2.getTheme());
            newTheme.resolveAttribute(a.C0027a.actionBarPopupTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            }
            newTheme.resolveAttribute(a.C0027a.panelMenuListTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            } else {
                newTheme.applyStyle(a.j.Theme_AppCompat_CompactMenu, true);
            }
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(b2, 0);
            contextThemeWrapper.getTheme().setTo(newTheme);
            this.f1381g = new ListMenuPresenter(contextThemeWrapper, a.h.abc_list_menu_item_layout);
            this.f1381g.setCallback(new c());
            menuBuilder.addMenuPresenter(this.f1381g);
        }
    }

    /* compiled from: ToolbarActionBar */
    private class d extends f {
        public d(Window.Callback callback) {
            super(callback);
        }

        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel && !n.this.f1376b) {
                n.this.f1375a.m();
                n.this.f1376b = true;
            }
            return onPreparePanel;
        }

        public View onCreatePanelView(int i) {
            switch (i) {
                case 0:
                    Menu r = n.this.f1375a.r();
                    if (onPreparePanel(i, null, r) && onMenuOpened(i, r)) {
                        return n.this.a(r);
                    }
            }
            return super.onCreatePanelView(i);
        }
    }

    private Menu k() {
        if (!this.f1378d) {
            this.f1375a.a(new a(), new b());
            this.f1378d = true;
        }
        return this.f1375a.r();
    }

    /* compiled from: ToolbarActionBar */
    private final class a implements i.a {

        /* renamed from: b  reason: collision with root package name */
        private boolean f1386b;

        a() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            if (n.this.f1377c == null) {
                return false;
            }
            n.this.f1377c.onMenuOpened(108, menuBuilder);
            return true;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            if (!this.f1386b) {
                this.f1386b = true;
                n.this.f1375a.n();
                if (n.this.f1377c != null) {
                    n.this.f1377c.onPanelClosed(108, menuBuilder);
                }
                this.f1386b = false;
            }
        }
    }

    /* compiled from: ToolbarActionBar */
    private final class c implements i.a {
        c() {
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            if (n.this.f1377c != null) {
                n.this.f1377c.onPanelClosed(0, menuBuilder);
            }
        }

        public boolean a(MenuBuilder menuBuilder) {
            if (menuBuilder != null || n.this.f1377c == null) {
                return true;
            }
            n.this.f1377c.onMenuOpened(0, menuBuilder);
            return true;
        }
    }

    /* compiled from: ToolbarActionBar */
    private final class b implements MenuBuilder.a {
        b() {
        }

        public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
            return false;
        }

        public void onMenuModeChange(MenuBuilder menuBuilder) {
            if (n.this.f1377c == null) {
                return;
            }
            if (n.this.f1375a.i()) {
                n.this.f1377c.onPanelClosed(108, menuBuilder);
            } else if (n.this.f1377c.onPreparePanel(0, null, menuBuilder)) {
                n.this.f1377c.onMenuOpened(108, menuBuilder);
            }
        }
    }
}
