package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.aj;
import android.support.v4.app.y;
import android.support.v4.view.g;
import android.support.v7.view.b;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ao;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class AppCompatActivity extends FragmentActivity implements aj.a, a {
    private b n;
    private int o = 0;
    private boolean p;
    private Resources q;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        b i = i();
        i.h();
        i.a(bundle);
        if (i.i() && this.o != 0) {
            if (Build.VERSION.SDK_INT >= 23) {
                onApplyThemeResource(getTheme(), this.o, false);
            } else {
                setTheme(this.o);
            }
        }
        super.onCreate(bundle);
    }

    public void setTheme(int i) {
        super.setTheme(i);
        this.o = i;
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        i().b(bundle);
    }

    public ActionBar f() {
        return i().a();
    }

    public void a(Toolbar toolbar) {
        i().a(toolbar);
    }

    public MenuInflater getMenuInflater() {
        return i().b();
    }

    public void setContentView(int i) {
        i().b(i);
    }

    public void setContentView(View view) {
        i().a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        i().a(view, layoutParams);
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        i().b(view, layoutParams);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        i().a(configuration);
        if (this.q != null) {
            this.q.updateConfiguration(configuration, super.getResources().getDisplayMetrics());
        }
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        i().e();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        i().c();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        i().d();
    }

    public View findViewById(int i) {
        return i().a(i);
    }

    public final boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        ActionBar f2 = f();
        if (menuItem.getItemId() != 16908332 || f2 == null || (f2.a() & 4) == 0) {
            return false;
        }
        return g();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        i().g();
    }

    /* access modifiers changed from: protected */
    public void onTitleChanged(CharSequence charSequence, int i) {
        super.onTitleChanged(charSequence, i);
        i().a(charSequence);
    }

    public void c() {
        i().f();
    }

    public void invalidateOptionsMenu() {
        i().f();
    }

    public void onSupportActionModeStarted(b bVar) {
    }

    public void onSupportActionModeFinished(b bVar) {
    }

    public b onWindowStartingSupportActionMode(b.a aVar) {
        return null;
    }

    public void a(aj ajVar) {
        ajVar.a((Activity) this);
    }

    public void b(aj ajVar) {
    }

    public boolean g() {
        Intent a2 = a();
        if (a2 == null) {
            return false;
        }
        if (a(a2)) {
            aj a3 = aj.a((Context) this);
            a(a3);
            b(a3);
            a3.a();
            try {
                ActivityCompat.a((Activity) this);
            } catch (IllegalStateException e2) {
                finish();
            }
        } else {
            b(a2);
        }
        return true;
    }

    public Intent a() {
        return y.a(this);
    }

    public boolean a(Intent intent) {
        return y.a(this, intent);
    }

    public void b(Intent intent) {
        y.b(this, intent);
    }

    public void onContentChanged() {
        h();
    }

    @Deprecated
    public void h() {
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return super.onMenuOpened(i, menu);
    }

    public void onPanelClosed(int i, Menu menu) {
        super.onPanelClosed(i, menu);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        i().c(bundle);
    }

    public b i() {
        if (this.n == null) {
            this.n = b.a(this, this);
        }
        return this.n;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (g.b(keyEvent) && keyEvent.getUnicodeChar(keyEvent.getMetaState() & -28673) == 60) {
            int action = keyEvent.getAction();
            if (action == 0) {
                ActionBar f2 = f();
                if (f2 != null && f2.b() && f2.g()) {
                    this.p = true;
                    return true;
                }
            } else if (action == 1 && this.p) {
                this.p = false;
                return true;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public Resources getResources() {
        if (this.q == null && ao.a()) {
            this.q = new ao(this, super.getResources());
        }
        return this.q == null ? super.getResources() : this.q;
    }
}
