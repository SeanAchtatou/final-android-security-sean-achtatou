package android.support.v7.app;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.ab;
import android.support.v4.app.ac;
import android.support.v7.a.a;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.List;

@TargetApi(9)
/* compiled from: NotificationCompatImplBase */
class k {
    @TargetApi(11)
    public static <T extends ac.a> RemoteViews a(ab abVar, Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i2, List list, int[] iArr, boolean z2, PendingIntent pendingIntent, boolean z3) {
        RemoteViews a2 = a(context, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, i2, list, iArr, z2, pendingIntent, z3);
        abVar.a().setContent(a2);
        if (z2) {
            abVar.a().setOngoing(true);
        }
        return a2;
    }

    @TargetApi(11)
    private static <T extends ac.a> RemoteViews a(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i2, List list, int[] iArr, boolean z2, PendingIntent pendingIntent, boolean z3) {
        int min;
        RemoteViews a2 = a(context, charSequence, charSequence2, charSequence3, i, 0, bitmap, charSequence4, z, j, i2, 0, z3 ? a.h.notification_template_media_custom : a.h.notification_template_media, true);
        int size = list.size();
        if (iArr == null) {
            min = 0;
        } else {
            min = Math.min(iArr.length, 3);
        }
        a2.removeAllViews(a.f.media_actions);
        if (min > 0) {
            for (int i3 = 0; i3 < min; i3++) {
                if (i3 >= size) {
                    throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", Integer.valueOf(i3), Integer.valueOf(size - 1)));
                }
                a2.addView(a.f.media_actions, a(context, (ac.a) list.get(iArr[i3])));
            }
        }
        if (z2) {
            a2.setViewVisibility(a.f.end_padder, 8);
            a2.setViewVisibility(a.f.cancel_action, 0);
            a2.setOnClickPendingIntent(a.f.cancel_action, pendingIntent);
            a2.setInt(a.f.cancel_action, "setAlpha", context.getResources().getInteger(a.g.cancel_button_image_alpha));
        } else {
            a2.setViewVisibility(a.f.end_padder, 0);
            a2.setViewVisibility(a.f.cancel_action, 8);
        }
        return a2;
    }

    @TargetApi(16)
    public static <T extends ac.a> void a(Notification notification, Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i2, int i3, List list, boolean z2, PendingIntent pendingIntent, boolean z3) {
        notification.bigContentView = a(context, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, i2, i3, list, z2, pendingIntent, z3);
        if (z2) {
            notification.flags |= 2;
        }
    }

    @TargetApi(11)
    public static <T extends ac.a> RemoteViews a(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i2, int i3, List list, boolean z2, PendingIntent pendingIntent, boolean z3) {
        int min = Math.min(list.size(), 5);
        RemoteViews a2 = a(context, charSequence, charSequence2, charSequence3, i, 0, bitmap, charSequence4, z, j, i2, i3, a(z3, min), false);
        a2.removeAllViews(a.f.media_actions);
        if (min > 0) {
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 >= min) {
                    break;
                }
                a2.addView(a.f.media_actions, a(context, (ac.a) list.get(i5)));
                i4 = i5 + 1;
            }
        }
        if (z2) {
            a2.setViewVisibility(a.f.cancel_action, 0);
            a2.setInt(a.f.cancel_action, "setAlpha", context.getResources().getInteger(a.g.cancel_button_image_alpha));
            a2.setOnClickPendingIntent(a.f.cancel_action, pendingIntent);
        } else {
            a2.setViewVisibility(a.f.cancel_action, 8);
        }
        return a2;
    }

    @TargetApi(11)
    private static RemoteViews a(Context context, ac.a aVar) {
        boolean z = aVar.c() == null;
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), a.h.notification_media_action);
        remoteViews.setImageViewResource(a.f.action0, aVar.a());
        if (!z) {
            remoteViews.setOnClickPendingIntent(a.f.action0, aVar.c());
        }
        if (Build.VERSION.SDK_INT >= 15) {
            remoteViews.setContentDescription(a.f.action0, aVar.b());
        }
        return remoteViews;
    }

    @TargetApi(11)
    private static int a(boolean z, int i) {
        return i <= 3 ? z ? a.h.notification_template_big_media_narrow_custom : a.h.notification_template_big_media_narrow : z ? a.h.notification_template_big_media_custom : a.h.notification_template_big_media;
    }

    public static RemoteViews a(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, int i2, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i3, int i4, int i5, boolean z2, ArrayList<NotificationCompat.Action> arrayList) {
        boolean z3;
        int size;
        int i6;
        RemoteViews a2 = a(context, charSequence, charSequence2, charSequence3, i, i2, bitmap, charSequence4, z, j, i3, i4, i5, z2);
        a2.removeAllViews(a.f.actions);
        if (arrayList == null || (size = arrayList.size()) <= 0) {
            z3 = false;
        } else {
            if (size > 3) {
                i6 = 3;
            } else {
                i6 = size;
            }
            for (int i7 = 0; i7 < i6; i7++) {
                a2.addView(a.f.actions, a(context, arrayList.get(i7)));
            }
            z3 = true;
        }
        int i8 = z3 ? 0 : 8;
        a2.setViewVisibility(a.f.actions, i8);
        a2.setViewVisibility(a.f.action_divider, i8);
        return a2;
    }

    private static RemoteViews a(Context context, NotificationCompat.Action action) {
        int a2;
        boolean z = action.f330d == null;
        String packageName = context.getPackageName();
        if (z) {
            a2 = b();
        } else {
            a2 = a();
        }
        RemoteViews remoteViews = new RemoteViews(packageName, a2);
        remoteViews.setImageViewBitmap(a.f.action_image, a(context, action.a(), context.getResources().getColor(a.c.notification_action_color_filter)));
        remoteViews.setTextViewText(a.f.action_text, action.f329c);
        if (!z) {
            remoteViews.setOnClickPendingIntent(a.f.action_container, action.f330d);
        }
        if (Build.VERSION.SDK_INT >= 15) {
            remoteViews.setContentDescription(a.f.action_container, action.f329c);
        }
        return remoteViews;
    }

    private static Bitmap a(Context context, int i, int i2) {
        return a(context, i, i2, 0);
    }

    private static Bitmap a(Context context, int i, int i2, int i3) {
        Drawable drawable = context.getResources().getDrawable(i);
        int intrinsicWidth = i3 == 0 ? drawable.getIntrinsicWidth() : i3;
        if (i3 == 0) {
            i3 = drawable.getIntrinsicHeight();
        }
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, i3, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, intrinsicWidth, i3);
        if (i2 != 0) {
            drawable.mutate().setColorFilter(new PorterDuffColorFilter(i2, PorterDuff.Mode.SRC_IN));
        }
        drawable.draw(new Canvas(createBitmap));
        return createBitmap;
    }

    private static int a() {
        return a.h.notification_action;
    }

    private static int b() {
        return a.h.notification_action_tombstone;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01e9  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01ed  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.widget.RemoteViews a(android.content.Context r13, java.lang.CharSequence r14, java.lang.CharSequence r15, java.lang.CharSequence r16, int r17, int r18, android.graphics.Bitmap r19, java.lang.CharSequence r20, boolean r21, long r22, int r24, int r25, int r26, boolean r27) {
        /*
            android.content.res.Resources r6 = r13.getResources()
            android.widget.RemoteViews r2 = new android.widget.RemoteViews
            java.lang.String r3 = r13.getPackageName()
            r0 = r26
            r2.<init>(r3, r0)
            r4 = 0
            r5 = 0
            r3 = -1
            r0 = r24
            if (r0 >= r3) goto L_0x011e
            r3 = 1
        L_0x0017:
            int r7 = android.os.Build.VERSION.SDK_INT
            r8 = 16
            if (r7 < r8) goto L_0x0037
            int r7 = android.os.Build.VERSION.SDK_INT
            r8 = 21
            if (r7 >= r8) goto L_0x0037
            if (r3 == 0) goto L_0x0121
            int r3 = android.support.v7.a.a.f.notification_background
            java.lang.String r7 = "setBackgroundResource"
            int r8 = android.support.v7.a.a.e.notification_bg_low
            r2.setInt(r3, r7, r8)
            int r3 = android.support.v7.a.a.f.icon
            java.lang.String r7 = "setBackgroundResource"
            int r8 = android.support.v7.a.a.e.notification_template_icon_low_bg
            r2.setInt(r3, r7, r8)
        L_0x0037:
            if (r19 == 0) goto L_0x014c
            int r3 = android.os.Build.VERSION.SDK_INT
            r7 = 16
            if (r3 < r7) goto L_0x0135
            int r3 = android.support.v7.a.a.f.icon
            r7 = 0
            r2.setViewVisibility(r3, r7)
            int r3 = android.support.v7.a.a.f.icon
            r0 = r19
            r2.setImageViewBitmap(r3, r0)
        L_0x004c:
            if (r18 == 0) goto L_0x0077
            int r3 = android.support.v7.a.a.d.notification_right_icon_size
            int r3 = r6.getDimensionPixelSize(r3)
            int r7 = android.support.v7.a.a.d.notification_small_icon_background_padding
            int r7 = r6.getDimensionPixelSize(r7)
            int r7 = r7 * 2
            int r7 = r3 - r7
            int r8 = android.os.Build.VERSION.SDK_INT
            r9 = 21
            if (r8 < r9) goto L_0x013e
            r0 = r18
            r1 = r25
            android.graphics.Bitmap r3 = a(r13, r0, r3, r7, r1)
            int r7 = android.support.v7.a.a.f.right_icon
            r2.setImageViewBitmap(r7, r3)
        L_0x0071:
            int r3 = android.support.v7.a.a.f.right_icon
            r7 = 0
            r2.setViewVisibility(r3, r7)
        L_0x0077:
            if (r14 == 0) goto L_0x007e
            int r3 = android.support.v7.a.a.f.title
            r2.setTextViewText(r3, r14)
        L_0x007e:
            if (r15 == 0) goto L_0x01f0
            int r3 = android.support.v7.a.a.f.text
            r2.setTextViewText(r3, r15)
            r3 = 1
        L_0x0086:
            int r4 = android.os.Build.VERSION.SDK_INT
            r7 = 21
            if (r4 >= r7) goto L_0x018a
            if (r19 == 0) goto L_0x018a
            r8 = 1
        L_0x008f:
            if (r16 == 0) goto L_0x018d
            int r3 = android.support.v7.a.a.f.info
            r0 = r16
            r2.setTextViewText(r3, r0)
            int r3 = android.support.v7.a.a.f.info
            r4 = 0
            r2.setViewVisibility(r3, r4)
            r3 = 1
            r8 = 1
            r9 = r3
        L_0x00a1:
            if (r20 == 0) goto L_0x01d1
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 16
            if (r3 < r4) goto L_0x01d1
            int r3 = android.support.v7.a.a.f.text
            r0 = r20
            r2.setTextViewText(r3, r0)
            if (r15 == 0) goto L_0x01ca
            int r3 = android.support.v7.a.a.f.text2
            r2.setTextViewText(r3, r15)
            int r3 = android.support.v7.a.a.f.text2
            r4 = 0
            r2.setViewVisibility(r3, r4)
            r3 = 1
        L_0x00be:
            if (r3 == 0) goto L_0x00de
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 16
            if (r3 < r4) goto L_0x00de
            if (r27 == 0) goto L_0x00d5
            int r3 = android.support.v7.a.a.d.notification_subtext_size
            int r3 = r6.getDimensionPixelSize(r3)
            float r3 = (float) r3
            int r4 = android.support.v7.a.a.f.text
            r5 = 0
            r2.setTextViewTextSize(r4, r5, r3)
        L_0x00d5:
            int r3 = android.support.v7.a.a.f.line1
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2.setViewPadding(r3, r4, r5, r6, r7)
        L_0x00de:
            r4 = 0
            int r3 = (r22 > r4 ? 1 : (r22 == r4 ? 0 : -1))
            if (r3 == 0) goto L_0x01ed
            if (r21 == 0) goto L_0x01d4
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 16
            if (r3 < r4) goto L_0x01d4
            int r3 = android.support.v7.a.a.f.chronometer
            r4 = 0
            r2.setViewVisibility(r3, r4)
            int r3 = android.support.v7.a.a.f.chronometer
            java.lang.String r4 = "setBase"
            long r6 = android.os.SystemClock.elapsedRealtime()
            long r10 = java.lang.System.currentTimeMillis()
            long r6 = r6 - r10
            long r6 = r6 + r22
            r2.setLong(r3, r4, r6)
            int r3 = android.support.v7.a.a.f.chronometer
            java.lang.String r4 = "setStarted"
            r5 = 1
            r2.setBoolean(r3, r4, r5)
        L_0x010c:
            r3 = 1
        L_0x010d:
            int r4 = android.support.v7.a.a.f.right_side
            if (r3 == 0) goto L_0x01e5
            r3 = 0
        L_0x0112:
            r2.setViewVisibility(r4, r3)
            int r4 = android.support.v7.a.a.f.line3
            if (r9 == 0) goto L_0x01e9
            r3 = 0
        L_0x011a:
            r2.setViewVisibility(r4, r3)
            return r2
        L_0x011e:
            r3 = 0
            goto L_0x0017
        L_0x0121:
            int r3 = android.support.v7.a.a.f.notification_background
            java.lang.String r7 = "setBackgroundResource"
            int r8 = android.support.v7.a.a.e.notification_bg
            r2.setInt(r3, r7, r8)
            int r3 = android.support.v7.a.a.f.icon
            java.lang.String r7 = "setBackgroundResource"
            int r8 = android.support.v7.a.a.e.notification_template_icon_bg
            r2.setInt(r3, r7, r8)
            goto L_0x0037
        L_0x0135:
            int r3 = android.support.v7.a.a.f.icon
            r7 = 8
            r2.setViewVisibility(r3, r7)
            goto L_0x004c
        L_0x013e:
            int r3 = android.support.v7.a.a.f.right_icon
            r7 = -1
            r0 = r18
            android.graphics.Bitmap r7 = a(r13, r0, r7)
            r2.setImageViewBitmap(r3, r7)
            goto L_0x0071
        L_0x014c:
            if (r18 == 0) goto L_0x0077
            int r3 = android.support.v7.a.a.f.icon
            r7 = 0
            r2.setViewVisibility(r3, r7)
            int r3 = android.os.Build.VERSION.SDK_INT
            r7 = 21
            if (r3 < r7) goto L_0x017c
            int r3 = android.support.v7.a.a.d.notification_large_icon_width
            int r3 = r6.getDimensionPixelSize(r3)
            int r7 = android.support.v7.a.a.d.notification_big_circle_margin
            int r7 = r6.getDimensionPixelSize(r7)
            int r3 = r3 - r7
            int r7 = android.support.v7.a.a.d.notification_small_icon_size_as_large
            int r7 = r6.getDimensionPixelSize(r7)
            r0 = r18
            r1 = r25
            android.graphics.Bitmap r3 = a(r13, r0, r3, r7, r1)
            int r7 = android.support.v7.a.a.f.icon
            r2.setImageViewBitmap(r7, r3)
            goto L_0x0077
        L_0x017c:
            int r3 = android.support.v7.a.a.f.icon
            r7 = -1
            r0 = r18
            android.graphics.Bitmap r7 = a(r13, r0, r7)
            r2.setImageViewBitmap(r3, r7)
            goto L_0x0077
        L_0x018a:
            r8 = 0
            goto L_0x008f
        L_0x018d:
            if (r17 <= 0) goto L_0x01c0
            int r3 = android.support.v7.a.a.g.status_bar_notification_info_maxnum
            int r3 = r6.getInteger(r3)
            r0 = r17
            if (r0 <= r3) goto L_0x01af
            int r3 = android.support.v7.a.a.f.info
            int r4 = android.support.v7.a.a.i.status_bar_notification_info_overflow
            java.lang.String r4 = r6.getString(r4)
            r2.setTextViewText(r3, r4)
        L_0x01a4:
            int r3 = android.support.v7.a.a.f.info
            r4 = 0
            r2.setViewVisibility(r3, r4)
            r3 = 1
            r8 = 1
            r9 = r3
            goto L_0x00a1
        L_0x01af:
            java.text.NumberFormat r3 = java.text.NumberFormat.getIntegerInstance()
            int r4 = android.support.v7.a.a.f.info
            r0 = r17
            long r8 = (long) r0
            java.lang.String r3 = r3.format(r8)
            r2.setTextViewText(r4, r3)
            goto L_0x01a4
        L_0x01c0:
            int r4 = android.support.v7.a.a.f.info
            r7 = 8
            r2.setViewVisibility(r4, r7)
            r9 = r3
            goto L_0x00a1
        L_0x01ca:
            int r3 = android.support.v7.a.a.f.text2
            r4 = 8
            r2.setViewVisibility(r3, r4)
        L_0x01d1:
            r3 = r5
            goto L_0x00be
        L_0x01d4:
            int r3 = android.support.v7.a.a.f.time
            r4 = 0
            r2.setViewVisibility(r3, r4)
            int r3 = android.support.v7.a.a.f.time
            java.lang.String r4 = "setTime"
            r0 = r22
            r2.setLong(r3, r4, r0)
            goto L_0x010c
        L_0x01e5:
            r3 = 8
            goto L_0x0112
        L_0x01e9:
            r3 = 8
            goto L_0x011a
        L_0x01ed:
            r3 = r8
            goto L_0x010d
        L_0x01f0:
            r3 = r4
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.k.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, int, android.graphics.Bitmap, java.lang.CharSequence, boolean, long, int, int, int, boolean):android.widget.RemoteViews");
    }

    public static Bitmap a(Context context, int i, int i2, int i3, int i4) {
        int i5 = a.e.notification_icon_background;
        if (i4 == 0) {
            i4 = 0;
        }
        Bitmap a2 = a(context, i5, i4, i2);
        Canvas canvas = new Canvas(a2);
        Drawable mutate = context.getResources().getDrawable(i).mutate();
        mutate.setFilterBitmap(true);
        int i6 = (i2 - i3) / 2;
        mutate.setBounds(i6, i6, i3 + i6, i3 + i6);
        mutate.setColorFilter(new PorterDuffColorFilter(-1, PorterDuff.Mode.SRC_ATOP));
        mutate.draw(canvas);
        return a2;
    }

    public static void a(Context context, RemoteViews remoteViews, RemoteViews remoteViews2) {
        a(remoteViews);
        remoteViews.removeAllViews(a.f.notification_main_column);
        remoteViews.addView(a.f.notification_main_column, remoteViews2.clone());
        remoteViews.setViewVisibility(a.f.notification_main_column, 0);
        if (Build.VERSION.SDK_INT >= 21) {
            remoteViews.setViewPadding(a.f.notification_main_column_container, 0, a(context), 0, 0);
        }
    }

    private static void a(RemoteViews remoteViews) {
        remoteViews.setViewVisibility(a.f.title, 8);
        remoteViews.setViewVisibility(a.f.text2, 8);
        remoteViews.setViewVisibility(a.f.text, 8);
    }

    public static int a(Context context) {
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(a.d.notification_top_pad);
        int dimensionPixelSize2 = context.getResources().getDimensionPixelSize(a.d.notification_top_pad_large_text);
        float a2 = (a(context.getResources().getConfiguration().fontScale, 1.0f, 1.3f) - 1.0f) / 0.29999995f;
        return Math.round((((float) dimensionPixelSize) * (1.0f - a2)) + (((float) dimensionPixelSize2) * a2));
    }

    public static float a(float f2, float f3, float f4) {
        if (f2 < f3) {
            return f3;
        }
        return f2 > f4 ? f4 : f2;
    }
}
