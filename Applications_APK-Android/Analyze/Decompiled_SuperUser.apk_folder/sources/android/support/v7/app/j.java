package android.support.v7.app;

import android.annotation.TargetApi;
import android.app.Notification;
import android.support.v4.app.ab;

@TargetApi(24)
/* compiled from: NotificationCompatImpl24 */
class j {
    public static void a(ab abVar) {
        abVar.a().setStyle(new Notification.DecoratedCustomViewStyle());
    }

    public static void b(ab abVar) {
        abVar.a().setStyle(new Notification.DecoratedMediaCustomViewStyle());
    }
}
