package android.support.v7.app;

/* compiled from: TwilightCalculator */
class o {

    /* renamed from: d  reason: collision with root package name */
    private static o f1390d;

    /* renamed from: a  reason: collision with root package name */
    public long f1391a;

    /* renamed from: b  reason: collision with root package name */
    public long f1392b;

    /* renamed from: c  reason: collision with root package name */
    public int f1393c;

    o() {
    }

    static o a() {
        if (f1390d == null) {
            f1390d = new o();
        }
        return f1390d;
    }

    public void a(long j, double d2, double d3) {
        float f2 = ((float) (j - 946728000000L)) / 8.64E7f;
        float f3 = 6.24006f + (0.01720197f * f2);
        double sin = ((double) f3) + (0.03341960161924362d * Math.sin((double) f3)) + (3.4906598739326E-4d * Math.sin((double) (2.0f * f3))) + (5.236000106378924E-6d * Math.sin((double) (3.0f * f3))) + 1.796593063d + 3.141592653589793d;
        double d4 = (-d3) / 360.0d;
        double sin2 = (Math.sin((double) f3) * 0.0053d) + d4 + ((double) (((float) Math.round(((double) (f2 - 9.0E-4f)) - d4)) + 9.0E-4f)) + (-0.0069d * Math.sin(2.0d * sin));
        double asin = Math.asin(Math.sin(sin) * Math.sin(0.4092797040939331d));
        double d5 = 0.01745329238474369d * d2;
        double sin3 = (Math.sin(-0.10471975803375244d) - (Math.sin(d5) * Math.sin(asin))) / (Math.cos(asin) * Math.cos(d5));
        if (sin3 >= 1.0d) {
            this.f1393c = 1;
            this.f1391a = -1;
            this.f1392b = -1;
        } else if (sin3 <= -1.0d) {
            this.f1393c = 0;
            this.f1391a = -1;
            this.f1392b = -1;
        } else {
            float acos = (float) (Math.acos(sin3) / 6.283185307179586d);
            this.f1391a = Math.round((((double) acos) + sin2) * 8.64E7d) + 946728000000L;
            this.f1392b = Math.round((sin2 - ((double) acos)) * 8.64E7d) + 946728000000L;
            if (this.f1392b >= j || this.f1391a <= j) {
                this.f1393c = 1;
            } else {
                this.f1393c = 0;
            }
        }
    }
}
