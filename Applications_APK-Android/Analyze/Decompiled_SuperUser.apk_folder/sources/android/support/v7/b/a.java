package android.support.v7.b;

import com.kingouser.com.R;

/* compiled from: R */
public final class a {

    /* renamed from: android.support.v7.b.a$a  reason: collision with other inner class name */
    /* compiled from: R */
    public static final class C0028a {

        /* renamed from: cardview_dark_background */
        public static final int ac = 2131558439;

        /* renamed from: cardview_light_background */
        public static final int ad = 2131558440;

        /* renamed from: cardview_shadow_end_color */
        public static final int ae = 2131558441;

        /* renamed from: cardview_shadow_start_color */
        public static final int af = 2131558442;
    }

    /* compiled from: R */
    public static final class b {

        /* renamed from: cardview_compat_inset_shadow */
        public static final int d5 = 2131230875;

        /* renamed from: cardview_default_elevation */
        public static final int d6 = 2131230876;

        /* renamed from: cardview_default_radius */
        public static final int d7 = 2131230877;
    }

    /* compiled from: R */
    public static final class c {

        /* renamed from: Base_CardView */
        public static final int dx = 2131361987;

        /* renamed from: CardView */
        public static final int db = 2131361963;

        /* renamed from: CardView_Dark */
        public static final int f5 = 2131362032;

        /* renamed from: CardView_Light */
        public static final int f6 = 2131362033;
    }

    /* compiled from: R */
    public static final class d {
        public static final int[] CardView = {16843071, 16843072, R.attr.el, R.attr.em, R.attr.en, R.attr.eo, R.attr.ep, R.attr.eq, R.attr.er, R.attr.es, R.attr.et, R.attr.eu, R.attr.ev};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 7;
        public static final int CardView_cardUseCompatPadding = 6;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 12;
        public static final int CardView_contentPaddingLeft = 9;
        public static final int CardView_contentPaddingRight = 10;
        public static final int CardView_contentPaddingTop = 11;
    }
}
