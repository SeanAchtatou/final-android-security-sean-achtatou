package android.support.v7.d;

import com.kingouser.com.R;

/* compiled from: R */
public final class a {

    /* renamed from: android.support.v7.d.a$a  reason: collision with other inner class name */
    /* compiled from: R */
    public static final class C0029a {
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131230927;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131230928;
        public static final int item_touch_helper_swipe_escape_velocity = 2131230929;
    }

    /* compiled from: R */
    public static final class b {
        public static final int item_touch_helper_previous_elevation = 2131623941;
    }

    /* compiled from: R */
    public static final class c {
        public static final int[] RecyclerView = {16842948, 16842993, R.attr.h0, R.attr.h1, R.attr.h2, R.attr.h3};
        public static final int RecyclerView_android_descendantFocusability = 1;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_layoutManager = 2;
        public static final int RecyclerView_reverseLayout = 4;
        public static final int RecyclerView_spanCount = 3;
        public static final int RecyclerView_stackFromEnd = 5;
    }
}
