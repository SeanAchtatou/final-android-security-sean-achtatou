package android.support.v7.util;

import java.util.Comparator;

public class SortedList<T> {

    public static abstract class a<T2> implements Comparator<T2> {
        public abstract int compare(T2 t2, T2 t22);
    }

    public static class BatchedCallback<T2> extends a<T2> {

        /* renamed from: a  reason: collision with root package name */
        final a<T2> f1419a;

        public int compare(T2 t2, T2 t22) {
            return this.f1419a.compare(t2, t22);
        }
    }
}
