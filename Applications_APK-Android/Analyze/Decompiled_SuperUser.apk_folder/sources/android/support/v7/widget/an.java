package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.c.a.b;
import android.util.AttributeSet;

/* compiled from: TintTypedArray */
public class an {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2143a;

    /* renamed from: b  reason: collision with root package name */
    private final TypedArray f2144b;

    public static an a(Context context, AttributeSet attributeSet, int[] iArr) {
        return new an(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    public static an a(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new an(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    public static an a(Context context, int i, int[] iArr) {
        return new an(context, context.obtainStyledAttributes(i, iArr));
    }

    private an(Context context, TypedArray typedArray) {
        this.f2143a = context;
        this.f2144b = typedArray;
    }

    public Drawable a(int i) {
        int resourceId;
        if (!this.f2144b.hasValue(i) || (resourceId = this.f2144b.getResourceId(i, 0)) == 0) {
            return this.f2144b.getDrawable(i);
        }
        return b.b(this.f2143a, resourceId);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.g.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, int, int]
     candidates:
      android.support.v7.widget.g.a(android.content.res.ColorStateList, android.graphics.PorterDuff$Mode, int[]):android.graphics.PorterDuffColorFilter
      android.support.v7.widget.g.a(android.content.Context, int, android.content.res.ColorStateList):void
      android.support.v7.widget.g.a(android.graphics.drawable.Drawable, int, android.graphics.PorterDuff$Mode):void
      android.support.v7.widget.g.a(android.graphics.drawable.Drawable, android.support.v7.widget.al, int[]):void
      android.support.v7.widget.g.a(android.content.Context, int, android.graphics.drawable.Drawable):boolean
      android.support.v7.widget.g.a(android.content.Context, long, android.graphics.drawable.Drawable):boolean
      android.support.v7.widget.g.a(android.content.Context, android.support.v7.widget.ao, int):android.graphics.drawable.Drawable
      android.support.v7.widget.g.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable */
    public Drawable b(int i) {
        int resourceId;
        if (!this.f2144b.hasValue(i) || (resourceId = this.f2144b.getResourceId(i, 0)) == 0) {
            return null;
        }
        return g.a().a(this.f2143a, resourceId, true);
    }

    public CharSequence c(int i) {
        return this.f2144b.getText(i);
    }

    public String d(int i) {
        return this.f2144b.getString(i);
    }

    public boolean a(int i, boolean z) {
        return this.f2144b.getBoolean(i, z);
    }

    public int a(int i, int i2) {
        return this.f2144b.getInt(i, i2);
    }

    public float a(int i, float f2) {
        return this.f2144b.getFloat(i, f2);
    }

    public int b(int i, int i2) {
        return this.f2144b.getColor(i, i2);
    }

    public ColorStateList e(int i) {
        int resourceId;
        ColorStateList a2;
        return (!this.f2144b.hasValue(i) || (resourceId = this.f2144b.getResourceId(i, 0)) == 0 || (a2 = b.a(this.f2143a, resourceId)) == null) ? this.f2144b.getColorStateList(i) : a2;
    }

    public int c(int i, int i2) {
        return this.f2144b.getInteger(i, i2);
    }

    public int d(int i, int i2) {
        return this.f2144b.getDimensionPixelOffset(i, i2);
    }

    public int e(int i, int i2) {
        return this.f2144b.getDimensionPixelSize(i, i2);
    }

    public int f(int i, int i2) {
        return this.f2144b.getLayoutDimension(i, i2);
    }

    public int g(int i, int i2) {
        return this.f2144b.getResourceId(i, i2);
    }

    public CharSequence[] f(int i) {
        return this.f2144b.getTextArray(i);
    }

    public boolean g(int i) {
        return this.f2144b.hasValue(i);
    }

    public void a() {
        this.f2144b.recycle();
    }
}
