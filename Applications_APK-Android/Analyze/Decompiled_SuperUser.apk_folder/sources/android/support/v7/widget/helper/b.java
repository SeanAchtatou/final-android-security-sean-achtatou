package android.support.v7.widget.helper;

import android.graphics.Canvas;
import android.support.v4.view.ag;
import android.support.v7.d.a;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/* compiled from: ItemTouchUIUtilImpl */
class b {

    /* compiled from: ItemTouchUIUtilImpl */
    static class c extends C0031b {
        c() {
        }

        public void a(Canvas canvas, RecyclerView recyclerView, View view, float f2, float f3, int i, boolean z) {
            if (z && view.getTag(a.b.item_touch_helper_previous_elevation) == null) {
                Float valueOf = Float.valueOf(ag.t(view));
                ag.h(view, 1.0f + a(recyclerView, view));
                view.setTag(a.b.item_touch_helper_previous_elevation, valueOf);
            }
            super.a(canvas, recyclerView, view, f2, f3, i, z);
        }

        private float a(RecyclerView recyclerView, View view) {
            int childCount = recyclerView.getChildCount();
            float f2 = 0.0f;
            for (int i = 0; i < childCount; i++) {
                View childAt = recyclerView.getChildAt(i);
                if (childAt != view) {
                    float t = ag.t(childAt);
                    if (t > f2) {
                        f2 = t;
                    }
                }
            }
            return f2;
        }

        public void a(View view) {
            Object tag = view.getTag(a.b.item_touch_helper_previous_elevation);
            if (tag != null && (tag instanceof Float)) {
                ag.h(view, ((Float) tag).floatValue());
            }
            view.setTag(a.b.item_touch_helper_previous_elevation, null);
            super.a(view);
        }
    }

    /* renamed from: android.support.v7.widget.helper.b$b  reason: collision with other inner class name */
    /* compiled from: ItemTouchUIUtilImpl */
    static class C0031b implements a {
        C0031b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.a(android.view.View, float):void
         arg types: [android.view.View, int]
         candidates:
          android.support.v4.view.ag.a(int, int):int
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
          android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
          android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
          android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
          android.support.v4.view.ag.a(android.view.View, boolean):void
          android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
          android.support.v4.view.ag.a(android.view.View, int):boolean
          android.support.v4.view.ag.a(android.view.View, float):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.b(android.view.View, float):void
         arg types: [android.view.View, int]
         candidates:
          android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
          android.support.v4.view.ag.b(android.view.View, boolean):void
          android.support.v4.view.ag.b(android.view.View, int):boolean
          android.support.v4.view.ag.b(android.view.View, float):void */
        public void a(View view) {
            ag.a(view, 0.0f);
            ag.b(view, 0.0f);
        }

        public void b(View view) {
        }

        public void a(Canvas canvas, RecyclerView recyclerView, View view, float f2, float f3, int i, boolean z) {
            ag.a(view, f2);
            ag.b(view, f3);
        }

        public void b(Canvas canvas, RecyclerView recyclerView, View view, float f2, float f3, int i, boolean z) {
        }
    }

    /* compiled from: ItemTouchUIUtilImpl */
    static class a implements a {
        a() {
        }

        private void a(Canvas canvas, RecyclerView recyclerView, View view, float f2, float f3) {
            canvas.save();
            canvas.translate(f2, f3);
            recyclerView.drawChild(canvas, view, 0);
            canvas.restore();
        }

        public void a(View view) {
            view.setVisibility(0);
        }

        public void b(View view) {
            view.setVisibility(4);
        }

        public void a(Canvas canvas, RecyclerView recyclerView, View view, float f2, float f3, int i, boolean z) {
            if (i != 2) {
                a(canvas, recyclerView, view, f2, f3);
            }
        }

        public void b(Canvas canvas, RecyclerView recyclerView, View view, float f2, float f3, int i, boolean z) {
            if (i == 2) {
                a(canvas, recyclerView, view, f2, f3);
            }
        }
    }
}
