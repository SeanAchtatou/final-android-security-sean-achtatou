package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.ad;
import android.support.v7.a.a;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.CheckBox;

public class AppCompatCheckBox extends CheckBox implements ad {

    /* renamed from: a  reason: collision with root package name */
    private f f1667a;

    public AppCompatCheckBox(Context context) {
        this(context, null);
    }

    public AppCompatCheckBox(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0027a.checkboxStyle);
    }

    public AppCompatCheckBox(Context context, AttributeSet attributeSet, int i) {
        super(ak.a(context), attributeSet, i);
        this.f1667a = new f(this);
        this.f1667a.a(attributeSet, i);
    }

    public void setButtonDrawable(Drawable drawable) {
        super.setButtonDrawable(drawable);
        if (this.f1667a != null) {
            this.f1667a.c();
        }
    }

    public void setButtonDrawable(int i) {
        setButtonDrawable(b.b(getContext(), i));
    }

    public int getCompoundPaddingLeft() {
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        return this.f1667a != null ? this.f1667a.a(compoundPaddingLeft) : compoundPaddingLeft;
    }

    public void setSupportButtonTintList(ColorStateList colorStateList) {
        if (this.f1667a != null) {
            this.f1667a.a(colorStateList);
        }
    }

    public ColorStateList getSupportButtonTintList() {
        if (this.f1667a != null) {
            return this.f1667a.a();
        }
        return null;
    }

    public void setSupportButtonTintMode(PorterDuff.Mode mode) {
        if (this.f1667a != null) {
            this.f1667a.a(mode);
        }
    }

    public PorterDuff.Mode getSupportButtonTintMode() {
        if (this.f1667a != null) {
            return this.f1667a.b();
        }
        return null;
    }
}
