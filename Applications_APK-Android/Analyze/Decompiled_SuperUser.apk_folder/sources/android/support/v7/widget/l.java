package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.widget.TextView;

@TargetApi(17)
/* compiled from: AppCompatTextHelperV17 */
class l extends k {

    /* renamed from: b  reason: collision with root package name */
    private al f2255b;

    /* renamed from: c  reason: collision with root package name */
    private al f2256c;

    l(TextView textView) {
        super(textView);
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        super.a(attributeSet, i);
        Context context = this.f2250a.getContext();
        g a2 = g.a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.k.AppCompatTextHelper, i, 0);
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTextHelper_android_drawableStart)) {
            this.f2255b = a(context, a2, obtainStyledAttributes.getResourceId(a.k.AppCompatTextHelper_android_drawableStart, 0));
        }
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTextHelper_android_drawableEnd)) {
            this.f2256c = a(context, a2, obtainStyledAttributes.getResourceId(a.k.AppCompatTextHelper_android_drawableEnd, 0));
        }
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        super.a();
        if (this.f2255b != null || this.f2256c != null) {
            Drawable[] compoundDrawablesRelative = this.f2250a.getCompoundDrawablesRelative();
            a(compoundDrawablesRelative[0], this.f2255b);
            a(compoundDrawablesRelative[2], this.f2256c);
        }
    }
}
