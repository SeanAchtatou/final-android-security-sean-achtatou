package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ad;
import android.util.AttributeSet;
import android.widget.ImageView;

public class AppCompatImageView extends ImageView implements ad {

    /* renamed from: a  reason: collision with root package name */
    private e f1670a;

    /* renamed from: b  reason: collision with root package name */
    private h f1671b;

    public AppCompatImageView(Context context) {
        this(context, null);
    }

    public AppCompatImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AppCompatImageView(Context context, AttributeSet attributeSet, int i) {
        super(ak.a(context), attributeSet, i);
        this.f1670a = new e(this);
        this.f1670a.a(attributeSet, i);
        this.f1671b = new h(this);
        this.f1671b.a(attributeSet, i);
    }

    public void setImageResource(int i) {
        this.f1671b.a(i);
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1670a != null) {
            this.f1670a.a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1670a != null) {
            this.f1670a.a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1670a != null) {
            this.f1670a.a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1670a != null) {
            return this.f1670a.a();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.f1670a != null) {
            this.f1670a.a(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (this.f1670a != null) {
            return this.f1670a.b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1670a != null) {
            this.f1670a.c();
        }
    }

    public boolean hasOverlappingRendering() {
        return this.f1671b.a() && super.hasOverlappingRendering();
    }
}
