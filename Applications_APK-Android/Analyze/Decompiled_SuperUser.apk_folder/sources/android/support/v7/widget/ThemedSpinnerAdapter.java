package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.widget.SpinnerAdapter;

public interface ThemedSpinnerAdapter extends SpinnerAdapter {
    Resources.Theme a();

    void a(Resources.Theme theme);

    public static final class Helper {

        /* renamed from: a  reason: collision with root package name */
        private final Context f2054a;

        /* renamed from: b  reason: collision with root package name */
        private final LayoutInflater f2055b;

        public Helper(Context context) {
            this.f2054a = context;
            this.f2055b = LayoutInflater.from(context);
        }
    }
}
