package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ag;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.widget.SeekBar;

/* compiled from: AppCompatSeekBarHelper */
class j extends i {

    /* renamed from: a  reason: collision with root package name */
    private final SeekBar f2244a;

    /* renamed from: b  reason: collision with root package name */
    private Drawable f2245b;

    /* renamed from: c  reason: collision with root package name */
    private ColorStateList f2246c = null;

    /* renamed from: d  reason: collision with root package name */
    private PorterDuff.Mode f2247d = null;

    /* renamed from: e  reason: collision with root package name */
    private boolean f2248e = false;

    /* renamed from: f  reason: collision with root package name */
    private boolean f2249f = false;

    j(SeekBar seekBar) {
        super(seekBar);
        this.f2244a = seekBar;
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        super.a(attributeSet, i);
        an a2 = an.a(this.f2244a.getContext(), attributeSet, a.k.AppCompatSeekBar, i, 0);
        Drawable b2 = a2.b(a.k.AppCompatSeekBar_android_thumb);
        if (b2 != null) {
            this.f2244a.setThumb(b2);
        }
        a(a2.a(a.k.AppCompatSeekBar_tickMark));
        if (a2.g(a.k.AppCompatSeekBar_tickMarkTintMode)) {
            this.f2247d = u.a(a2.a(a.k.AppCompatSeekBar_tickMarkTintMode, -1), this.f2247d);
            this.f2249f = true;
        }
        if (a2.g(a.k.AppCompatSeekBar_tickMarkTint)) {
            this.f2246c = a2.e(a.k.AppCompatSeekBar_tickMarkTint);
            this.f2248e = true;
        }
        a2.a();
        d();
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable) {
        if (this.f2245b != null) {
            this.f2245b.setCallback(null);
        }
        this.f2245b = drawable;
        if (drawable != null) {
            drawable.setCallback(this.f2244a);
            android.support.v4.b.a.a.b(drawable, ag.g(this.f2244a));
            if (drawable.isStateful()) {
                drawable.setState(this.f2244a.getDrawableState());
            }
            d();
        }
        this.f2244a.invalidate();
    }

    private void d() {
        if (this.f2245b == null) {
            return;
        }
        if (this.f2248e || this.f2249f) {
            this.f2245b = android.support.v4.b.a.a.g(this.f2245b.mutate());
            if (this.f2248e) {
                android.support.v4.b.a.a.a(this.f2245b, this.f2246c);
            }
            if (this.f2249f) {
                android.support.v4.b.a.a.a(this.f2245b, this.f2247d);
            }
            if (this.f2245b.isStateful()) {
                this.f2245b.setState(this.f2244a.getDrawableState());
            }
        }
    }

    /* access modifiers changed from: package-private */
    @TargetApi(11)
    public void b() {
        if (this.f2245b != null) {
            this.f2245b.jumpToCurrentState();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Drawable drawable = this.f2245b;
        if (drawable != null && drawable.isStateful() && drawable.setState(this.f2244a.getDrawableState())) {
            this.f2244a.invalidateDrawable(drawable);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas) {
        int max;
        int i = 1;
        if (this.f2245b != null && (max = this.f2244a.getMax()) > 1) {
            int intrinsicWidth = this.f2245b.getIntrinsicWidth();
            int intrinsicHeight = this.f2245b.getIntrinsicHeight();
            int i2 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
            if (intrinsicHeight >= 0) {
                i = intrinsicHeight / 2;
            }
            this.f2245b.setBounds(-i2, -i, i2, i);
            float width = ((float) ((this.f2244a.getWidth() - this.f2244a.getPaddingLeft()) - this.f2244a.getPaddingRight())) / ((float) max);
            int save = canvas.save();
            canvas.translate((float) this.f2244a.getPaddingLeft(), (float) (this.f2244a.getHeight() / 2));
            for (int i3 = 0; i3 <= max; i3++) {
                this.f2245b.draw(canvas);
                canvas.translate(width, 0.0f);
            }
            canvas.restoreToCount(save);
        }
    }
}
