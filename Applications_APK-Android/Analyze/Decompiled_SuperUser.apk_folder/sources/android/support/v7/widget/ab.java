package android.support.v7.widget;

import android.support.v7.widget.d;
import java.util.List;

/* compiled from: OpReorderer */
class ab {

    /* renamed from: a  reason: collision with root package name */
    final a f2092a;

    /* compiled from: OpReorderer */
    interface a {
        d.b a(int i, int i2, int i3, Object obj);

        void a(d.b bVar);
    }

    public ab(a aVar) {
        this.f2092a = aVar;
    }

    /* access modifiers changed from: package-private */
    public void a(List<d.b> list) {
        while (true) {
            int b2 = b(list);
            if (b2 != -1) {
                a(list, b2, b2 + 1);
            } else {
                return;
            }
        }
    }

    private void a(List<d.b> list, int i, int i2) {
        d.b bVar = list.get(i);
        d.b bVar2 = list.get(i2);
        switch (bVar2.f2185a) {
            case 1:
                c(list, i, bVar, i2, bVar2);
                return;
            case 2:
                a(list, i, bVar, i2, bVar2);
                return;
            case 3:
            default:
                return;
            case 4:
                b(list, i, bVar, i2, bVar2);
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List<d.b> list, int i, d.b bVar, int i2, d.b bVar2) {
        boolean z;
        d.b bVar3;
        boolean z2 = false;
        if (bVar.f2186b < bVar.f2188d) {
            z = bVar2.f2186b == bVar.f2186b && bVar2.f2188d == bVar.f2188d - bVar.f2186b;
        } else if (bVar2.f2186b == bVar.f2188d + 1 && bVar2.f2188d == bVar.f2186b - bVar.f2188d) {
            z2 = true;
            z = true;
        } else {
            z = false;
            z2 = true;
        }
        if (bVar.f2188d < bVar2.f2186b) {
            bVar2.f2186b--;
        } else if (bVar.f2188d < bVar2.f2186b + bVar2.f2188d) {
            bVar2.f2188d--;
            bVar.f2185a = 2;
            bVar.f2188d = 1;
            if (bVar2.f2188d == 0) {
                list.remove(i2);
                this.f2092a.a(bVar2);
                return;
            }
            return;
        }
        if (bVar.f2186b <= bVar2.f2186b) {
            bVar2.f2186b++;
            bVar3 = null;
        } else if (bVar.f2186b < bVar2.f2186b + bVar2.f2188d) {
            bVar3 = this.f2092a.a(2, bVar.f2186b + 1, (bVar2.f2186b + bVar2.f2188d) - bVar.f2186b, null);
            bVar2.f2188d = bVar.f2186b - bVar2.f2186b;
        } else {
            bVar3 = null;
        }
        if (z) {
            list.set(i, bVar2);
            list.remove(i2);
            this.f2092a.a(bVar);
            return;
        }
        if (z2) {
            if (bVar3 != null) {
                if (bVar.f2186b > bVar3.f2186b) {
                    bVar.f2186b -= bVar3.f2188d;
                }
                if (bVar.f2188d > bVar3.f2186b) {
                    bVar.f2188d -= bVar3.f2188d;
                }
            }
            if (bVar.f2186b > bVar2.f2186b) {
                bVar.f2186b -= bVar2.f2188d;
            }
            if (bVar.f2188d > bVar2.f2186b) {
                bVar.f2188d -= bVar2.f2188d;
            }
        } else {
            if (bVar3 != null) {
                if (bVar.f2186b >= bVar3.f2186b) {
                    bVar.f2186b -= bVar3.f2188d;
                }
                if (bVar.f2188d >= bVar3.f2186b) {
                    bVar.f2188d -= bVar3.f2188d;
                }
            }
            if (bVar.f2186b >= bVar2.f2186b) {
                bVar.f2186b -= bVar2.f2188d;
            }
            if (bVar.f2188d >= bVar2.f2186b) {
                bVar.f2188d -= bVar2.f2188d;
            }
        }
        list.set(i, bVar2);
        if (bVar.f2186b != bVar.f2188d) {
            list.set(i2, bVar);
        } else {
            list.remove(i2);
        }
        if (bVar3 != null) {
            list.add(i, bVar3);
        }
    }

    private void c(List<d.b> list, int i, d.b bVar, int i2, d.b bVar2) {
        int i3 = 0;
        if (bVar.f2188d < bVar2.f2186b) {
            i3 = -1;
        }
        if (bVar.f2186b < bVar2.f2186b) {
            i3++;
        }
        if (bVar2.f2186b <= bVar.f2186b) {
            bVar.f2186b += bVar2.f2188d;
        }
        if (bVar2.f2186b <= bVar.f2188d) {
            bVar.f2188d += bVar2.f2188d;
        }
        bVar2.f2186b = i3 + bVar2.f2186b;
        list.set(i, bVar2);
        list.set(i2, bVar);
    }

    /* access modifiers changed from: package-private */
    public void b(List<d.b> list, int i, d.b bVar, int i2, d.b bVar2) {
        d.b bVar3;
        d.b bVar4 = null;
        if (bVar.f2188d < bVar2.f2186b) {
            bVar2.f2186b--;
            bVar3 = null;
        } else if (bVar.f2188d < bVar2.f2186b + bVar2.f2188d) {
            bVar2.f2188d--;
            bVar3 = this.f2092a.a(4, bVar.f2186b, 1, bVar2.f2187c);
        } else {
            bVar3 = null;
        }
        if (bVar.f2186b <= bVar2.f2186b) {
            bVar2.f2186b++;
        } else if (bVar.f2186b < bVar2.f2186b + bVar2.f2188d) {
            int i3 = (bVar2.f2186b + bVar2.f2188d) - bVar.f2186b;
            bVar4 = this.f2092a.a(4, bVar.f2186b + 1, i3, bVar2.f2187c);
            bVar2.f2188d -= i3;
        }
        list.set(i2, bVar);
        if (bVar2.f2188d > 0) {
            list.set(i, bVar2);
        } else {
            list.remove(i);
            this.f2092a.a(bVar2);
        }
        if (bVar3 != null) {
            list.add(i, bVar3);
        }
        if (bVar4 != null) {
            list.add(i, bVar4);
        }
    }

    private int b(List<d.b> list) {
        boolean z;
        boolean z2 = false;
        int size = list.size() - 1;
        while (size >= 0) {
            if (list.get(size).f2185a != 8) {
                z = true;
            } else if (z2) {
                return size;
            } else {
                z = z2;
            }
            size--;
            z2 = z;
        }
        return -1;
    }
}
