package android.support.v7.widget;

import android.os.Bundle;
import android.support.v4.view.a;
import android.support.v4.view.a.e;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

public class RecyclerViewAccessibilityDelegate extends a {

    /* renamed from: a  reason: collision with root package name */
    final RecyclerView f1933a;

    /* renamed from: b  reason: collision with root package name */
    final a f1934b = new a() {
        public void onInitializeAccessibilityNodeInfo(View view, e eVar) {
            super.onInitializeAccessibilityNodeInfo(view, eVar);
            if (!RecyclerViewAccessibilityDelegate.this.a() && RecyclerViewAccessibilityDelegate.this.f1933a.getLayoutManager() != null) {
                RecyclerViewAccessibilityDelegate.this.f1933a.getLayoutManager().a(view, eVar);
            }
        }

        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            if (super.performAccessibilityAction(view, i, bundle)) {
                return true;
            }
            if (RecyclerViewAccessibilityDelegate.this.a() || RecyclerViewAccessibilityDelegate.this.f1933a.getLayoutManager() == null) {
                return false;
            }
            return RecyclerViewAccessibilityDelegate.this.f1933a.getLayoutManager().a(view, i, bundle);
        }
    };

    public RecyclerViewAccessibilityDelegate(RecyclerView recyclerView) {
        this.f1933a = recyclerView;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.f1933a.hasPendingAdapterUpdates();
    }

    public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        if (super.performAccessibilityAction(view, i, bundle)) {
            return true;
        }
        if (a() || this.f1933a.getLayoutManager() == null) {
            return false;
        }
        return this.f1933a.getLayoutManager().a(i, bundle);
    }

    public void onInitializeAccessibilityNodeInfo(View view, e eVar) {
        super.onInitializeAccessibilityNodeInfo(view, eVar);
        eVar.b((CharSequence) RecyclerView.class.getName());
        if (!a() && this.f1933a.getLayoutManager() != null) {
            this.f1933a.getLayoutManager().a(eVar);
        }
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(view, accessibilityEvent);
        accessibilityEvent.setClassName(RecyclerView.class.getName());
        if ((view instanceof RecyclerView) && !a()) {
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().a(accessibilityEvent);
            }
        }
    }

    public a b() {
        return this.f1934b;
    }
}
