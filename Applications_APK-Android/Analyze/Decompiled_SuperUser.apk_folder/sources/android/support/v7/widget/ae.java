package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

@TargetApi(21)
/* compiled from: RoundRectDrawable */
class ae extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    private float f2097a;

    /* renamed from: b  reason: collision with root package name */
    private final Paint f2098b;

    /* renamed from: c  reason: collision with root package name */
    private final RectF f2099c;

    /* renamed from: d  reason: collision with root package name */
    private final Rect f2100d;

    /* renamed from: e  reason: collision with root package name */
    private float f2101e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f2102f = false;

    /* renamed from: g  reason: collision with root package name */
    private boolean f2103g = true;

    /* renamed from: h  reason: collision with root package name */
    private ColorStateList f2104h;
    private PorterDuffColorFilter i;
    private ColorStateList j;
    private PorterDuff.Mode k = PorterDuff.Mode.SRC_IN;

    public ae(ColorStateList colorStateList, float f2) {
        this.f2097a = f2;
        this.f2098b = new Paint(5);
        b(colorStateList);
        this.f2099c = new RectF();
        this.f2100d = new Rect();
    }

    private void b(ColorStateList colorStateList) {
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        this.f2104h = colorStateList;
        this.f2098b.setColor(this.f2104h.getColorForState(getState(), this.f2104h.getDefaultColor()));
    }

    /* access modifiers changed from: package-private */
    public void a(float f2, boolean z, boolean z2) {
        if (f2 != this.f2101e || this.f2102f != z || this.f2103g != z2) {
            this.f2101e = f2;
            this.f2102f = z;
            this.f2103g = z2;
            a((Rect) null);
            invalidateSelf();
        }
    }

    /* access modifiers changed from: package-private */
    public float a() {
        return this.f2101e;
    }

    public void draw(Canvas canvas) {
        boolean z;
        Paint paint = this.f2098b;
        if (this.i == null || paint.getColorFilter() != null) {
            z = false;
        } else {
            paint.setColorFilter(this.i);
            z = true;
        }
        canvas.drawRoundRect(this.f2099c, this.f2097a, this.f2097a, paint);
        if (z) {
            paint.setColorFilter(null);
        }
    }

    private void a(Rect rect) {
        if (rect == null) {
            rect = getBounds();
        }
        this.f2099c.set((float) rect.left, (float) rect.top, (float) rect.right, (float) rect.bottom);
        this.f2100d.set(rect);
        if (this.f2102f) {
            float a2 = af.a(this.f2101e, this.f2097a, this.f2103g);
            this.f2100d.inset((int) Math.ceil((double) af.b(this.f2101e, this.f2097a, this.f2103g)), (int) Math.ceil((double) a2));
            this.f2099c.set(this.f2100d);
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        a(rect);
    }

    public void getOutline(Outline outline) {
        outline.setRoundRect(this.f2100d, this.f2097a);
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        if (f2 != this.f2097a) {
            this.f2097a = f2;
            a((Rect) null);
            invalidateSelf();
        }
    }

    public void setAlpha(int i2) {
        this.f2098b.setAlpha(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f2098b.setColorFilter(colorFilter);
    }

    public int getOpacity() {
        return -3;
    }

    public float b() {
        return this.f2097a;
    }

    public void a(ColorStateList colorStateList) {
        b(colorStateList);
        invalidateSelf();
    }

    public ColorStateList c() {
        return this.f2104h;
    }

    public void setTintList(ColorStateList colorStateList) {
        this.j = colorStateList;
        this.i = a(this.j, this.k);
        invalidateSelf();
    }

    public void setTintMode(PorterDuff.Mode mode) {
        this.k = mode;
        this.i = a(this.j, this.k);
        invalidateSelf();
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        int colorForState = this.f2104h.getColorForState(iArr, this.f2104h.getDefaultColor());
        boolean z = colorForState != this.f2098b.getColor();
        if (z) {
            this.f2098b.setColor(colorForState);
        }
        if (this.j == null || this.k == null) {
            return z;
        }
        this.i = a(this.j, this.k);
        return true;
    }

    public boolean isStateful() {
        return (this.j != null && this.j.isStateful()) || (this.f2104h != null && this.f2104h.isStateful()) || super.isStateful();
    }

    private PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }
}
