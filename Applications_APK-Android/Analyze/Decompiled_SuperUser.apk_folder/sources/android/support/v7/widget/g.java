package android.support.v7.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.e;
import android.support.v4.util.f;
import android.support.v4.util.j;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;

/* compiled from: AppCompatDrawableManager */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static final PorterDuff.Mode f2201a = PorterDuff.Mode.SRC_IN;

    /* renamed from: b  reason: collision with root package name */
    private static g f2202b;

    /* renamed from: c  reason: collision with root package name */
    private static final b f2203c = new b(6);

    /* renamed from: d  reason: collision with root package name */
    private static final int[] f2204d = {a.e.abc_textfield_search_default_mtrl_alpha, a.e.abc_textfield_default_mtrl_alpha, a.e.abc_ab_share_pack_mtrl_alpha};

    /* renamed from: e  reason: collision with root package name */
    private static final int[] f2205e = {a.e.abc_ic_commit_search_api_mtrl_alpha, a.e.abc_seekbar_tick_mark_material, a.e.abc_ic_menu_share_mtrl_alpha, a.e.abc_ic_menu_copy_mtrl_am_alpha, a.e.abc_ic_menu_cut_mtrl_alpha, a.e.abc_ic_menu_selectall_mtrl_alpha, a.e.abc_ic_menu_paste_mtrl_am_alpha};

    /* renamed from: f  reason: collision with root package name */
    private static final int[] f2206f = {a.e.abc_textfield_activated_mtrl_alpha, a.e.abc_textfield_search_activated_mtrl_alpha, a.e.abc_cab_background_top_mtrl_alpha, a.e.abc_text_cursor_material, a.e.abc_text_select_handle_left_mtrl_dark, a.e.abc_text_select_handle_middle_mtrl_dark, a.e.abc_text_select_handle_right_mtrl_dark, a.e.abc_text_select_handle_left_mtrl_light, a.e.abc_text_select_handle_middle_mtrl_light, a.e.abc_text_select_handle_right_mtrl_light};

    /* renamed from: g  reason: collision with root package name */
    private static final int[] f2207g = {a.e.abc_popup_background_mtrl_mult, a.e.abc_cab_background_internal_bg, a.e.abc_menu_hardkey_panel_mtrl_mult};

    /* renamed from: h  reason: collision with root package name */
    private static final int[] f2208h = {a.e.abc_tab_indicator_material, a.e.abc_textfield_search_material};
    private static final int[] i = {a.e.abc_btn_check_material, a.e.abc_btn_radio_material};
    private WeakHashMap<Context, j<ColorStateList>> j;
    private ArrayMap<String, c> k;
    private j<String> l;
    private final Object m = new Object();
    private final WeakHashMap<Context, e<WeakReference<Drawable.ConstantState>>> n = new WeakHashMap<>(0);
    private TypedValue o;
    private boolean p;

    /* compiled from: AppCompatDrawableManager */
    private interface c {
        Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme);
    }

    public static g a() {
        if (f2202b == null) {
            f2202b = new g();
            a(f2202b);
        }
        return f2202b;
    }

    private static void a(g gVar) {
        if (Build.VERSION.SDK_INT < 24) {
            gVar.a("vector", new d());
            if (Build.VERSION.SDK_INT >= 11) {
                gVar.a("animated-vector", new a());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.g.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, int, int]
     candidates:
      android.support.v7.widget.g.a(android.content.res.ColorStateList, android.graphics.PorterDuff$Mode, int[]):android.graphics.PorterDuffColorFilter
      android.support.v7.widget.g.a(android.content.Context, int, android.content.res.ColorStateList):void
      android.support.v7.widget.g.a(android.graphics.drawable.Drawable, int, android.graphics.PorterDuff$Mode):void
      android.support.v7.widget.g.a(android.graphics.drawable.Drawable, android.support.v7.widget.al, int[]):void
      android.support.v7.widget.g.a(android.content.Context, int, android.graphics.drawable.Drawable):boolean
      android.support.v7.widget.g.a(android.content.Context, long, android.graphics.drawable.Drawable):boolean
      android.support.v7.widget.g.a(android.content.Context, android.support.v7.widget.ao, int):android.graphics.drawable.Drawable
      android.support.v7.widget.g.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable */
    public Drawable a(Context context, int i2) {
        return a(context, i2, false);
    }

    /* access modifiers changed from: package-private */
    public Drawable a(Context context, int i2, boolean z) {
        e(context);
        Drawable d2 = d(context, i2);
        if (d2 == null) {
            d2 = c(context, i2);
        }
        if (d2 == null) {
            d2 = android.support.v4.content.c.a(context, i2);
        }
        if (d2 != null) {
            d2 = a(context, i2, z, d2);
        }
        if (d2 != null) {
            u.b(d2);
        }
        return d2;
    }

    public void a(Context context) {
        synchronized (this.m) {
            e eVar = this.n.get(context);
            if (eVar != null) {
                eVar.c();
            }
        }
    }

    private static long a(TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    private Drawable c(Context context, int i2) {
        if (this.o == null) {
            this.o = new TypedValue();
        }
        TypedValue typedValue = this.o;
        context.getResources().getValue(i2, typedValue, true);
        long a2 = a(typedValue);
        Drawable a3 = a(context, a2);
        if (a3 == null) {
            if (i2 == a.e.abc_cab_background_top_material) {
                a3 = new LayerDrawable(new Drawable[]{a(context, a.e.abc_cab_background_internal_bg), a(context, a.e.abc_cab_background_top_mtrl_alpha)});
            }
            if (a3 != null) {
                a3.setChangingConfigurations(typedValue.changingConfigurations);
                a(context, a2, a3);
            }
        }
        return a3;
    }

    private Drawable a(Context context, int i2, boolean z, Drawable drawable) {
        ColorStateList b2 = b(context, i2);
        if (b2 != null) {
            if (u.c(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable g2 = android.support.v4.b.a.a.g(drawable);
            android.support.v4.b.a.a.a(g2, b2);
            PorterDuff.Mode a2 = a(i2);
            if (a2 == null) {
                return g2;
            }
            android.support.v4.b.a.a.a(g2, a2);
            return g2;
        } else if (i2 == a.e.abc_seekbar_track_material) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            a(layerDrawable.findDrawableByLayerId(16908288), aj.a(context, a.C0027a.colorControlNormal), f2201a);
            a(layerDrawable.findDrawableByLayerId(16908303), aj.a(context, a.C0027a.colorControlNormal), f2201a);
            a(layerDrawable.findDrawableByLayerId(16908301), aj.a(context, a.C0027a.colorControlActivated), f2201a);
            return drawable;
        } else if (i2 == a.e.abc_ratingbar_material || i2 == a.e.abc_ratingbar_indicator_material || i2 == a.e.abc_ratingbar_small_material) {
            LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
            a(layerDrawable2.findDrawableByLayerId(16908288), aj.c(context, a.C0027a.colorControlNormal), f2201a);
            a(layerDrawable2.findDrawableByLayerId(16908303), aj.a(context, a.C0027a.colorControlActivated), f2201a);
            a(layerDrawable2.findDrawableByLayerId(16908301), aj.a(context, a.C0027a.colorControlActivated), f2201a);
            return drawable;
        } else if (a(context, i2, drawable) || !z) {
            return drawable;
        } else {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable d(android.content.Context r10, int r11) {
        /*
            r9 = this;
            r1 = 0
            r8 = 2
            r7 = 1
            android.support.v4.util.ArrayMap<java.lang.String, android.support.v7.widget.g$c> r0 = r9.k
            if (r0 == 0) goto L_0x00bf
            android.support.v4.util.ArrayMap<java.lang.String, android.support.v7.widget.g$c> r0 = r9.k
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00bf
            android.support.v4.util.j<java.lang.String> r0 = r9.l
            if (r0 == 0) goto L_0x002f
            android.support.v4.util.j<java.lang.String> r0 = r9.l
            java.lang.Object r0 = r0.a(r11)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = "appcompat_skip_skip"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x002d
            if (r0 == 0) goto L_0x0036
            android.support.v4.util.ArrayMap<java.lang.String, android.support.v7.widget.g$c> r2 = r9.k
            java.lang.Object r0 = r2.get(r0)
            if (r0 != 0) goto L_0x0036
        L_0x002d:
            r0 = r1
        L_0x002e:
            return r0
        L_0x002f:
            android.support.v4.util.j r0 = new android.support.v4.util.j
            r0.<init>()
            r9.l = r0
        L_0x0036:
            android.util.TypedValue r0 = r9.o
            if (r0 != 0) goto L_0x0041
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            r9.o = r0
        L_0x0041:
            android.util.TypedValue r2 = r9.o
            android.content.res.Resources r0 = r10.getResources()
            r0.getValue(r11, r2, r7)
            long r4 = a(r2)
            android.graphics.drawable.Drawable r1 = r9.a(r10, r4)
            if (r1 == 0) goto L_0x0056
            r0 = r1
            goto L_0x002e
        L_0x0056:
            java.lang.CharSequence r3 = r2.string
            if (r3 == 0) goto L_0x008a
            java.lang.CharSequence r3 = r2.string
            java.lang.String r3 = r3.toString()
            java.lang.String r6 = ".xml"
            boolean r3 = r3.endsWith(r6)
            if (r3 == 0) goto L_0x008a
            android.content.res.XmlResourceParser r3 = r0.getXml(r11)     // Catch:{ Exception -> 0x0082 }
            android.util.AttributeSet r6 = android.util.Xml.asAttributeSet(r3)     // Catch:{ Exception -> 0x0082 }
        L_0x0070:
            int r0 = r3.next()     // Catch:{ Exception -> 0x0082 }
            if (r0 == r8) goto L_0x0078
            if (r0 != r7) goto L_0x0070
        L_0x0078:
            if (r0 == r8) goto L_0x0095
            org.xmlpull.v1.XmlPullParserException r0 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ Exception -> 0x0082 }
            java.lang.String r2 = "No start tag found"
            r0.<init>(r2)     // Catch:{ Exception -> 0x0082 }
            throw r0     // Catch:{ Exception -> 0x0082 }
        L_0x0082:
            r0 = move-exception
            java.lang.String r2 = "AppCompatDrawableManager"
            java.lang.String r3 = "Exception while inflating drawable"
            android.util.Log.e(r2, r3, r0)
        L_0x008a:
            r0 = r1
        L_0x008b:
            if (r0 != 0) goto L_0x002e
            android.support.v4.util.j<java.lang.String> r1 = r9.l
            java.lang.String r2 = "appcompat_skip_skip"
            r1.c(r11, r2)
            goto L_0x002e
        L_0x0095:
            java.lang.String r0 = r3.getName()     // Catch:{ Exception -> 0x0082 }
            android.support.v4.util.j<java.lang.String> r7 = r9.l     // Catch:{ Exception -> 0x0082 }
            r7.c(r11, r0)     // Catch:{ Exception -> 0x0082 }
            android.support.v4.util.ArrayMap<java.lang.String, android.support.v7.widget.g$c> r7 = r9.k     // Catch:{ Exception -> 0x0082 }
            java.lang.Object r0 = r7.get(r0)     // Catch:{ Exception -> 0x0082 }
            android.support.v7.widget.g$c r0 = (android.support.v7.widget.g.c) r0     // Catch:{ Exception -> 0x0082 }
            if (r0 == 0) goto L_0x00b0
            android.content.res.Resources$Theme r7 = r10.getTheme()     // Catch:{ Exception -> 0x0082 }
            android.graphics.drawable.Drawable r1 = r0.a(r10, r3, r6, r7)     // Catch:{ Exception -> 0x0082 }
        L_0x00b0:
            if (r1 == 0) goto L_0x00bd
            int r0 = r2.changingConfigurations     // Catch:{ Exception -> 0x0082 }
            r1.setChangingConfigurations(r0)     // Catch:{ Exception -> 0x0082 }
            boolean r0 = r9.a(r10, r4, r1)     // Catch:{ Exception -> 0x0082 }
            if (r0 == 0) goto L_0x00bd
        L_0x00bd:
            r0 = r1
            goto L_0x008b
        L_0x00bf:
            r0 = r1
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.g.d(android.content.Context, int):android.graphics.drawable.Drawable");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable a(android.content.Context r5, long r6) {
        /*
            r4 = this;
            r2 = 0
            java.lang.Object r3 = r4.m
            monitor-enter(r3)
            java.util.WeakHashMap<android.content.Context, android.support.v4.util.e<java.lang.ref.WeakReference<android.graphics.drawable.Drawable$ConstantState>>> r0 = r4.n     // Catch:{ all -> 0x002b }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x002b }
            android.support.v4.util.e r0 = (android.support.v4.util.e) r0     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x0011
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
            r0 = r2
        L_0x0010:
            return r0
        L_0x0011:
            java.lang.Object r1 = r0.a(r6)     // Catch:{ all -> 0x002b }
            java.lang.ref.WeakReference r1 = (java.lang.ref.WeakReference) r1     // Catch:{ all -> 0x002b }
            if (r1 == 0) goto L_0x0031
            java.lang.Object r1 = r1.get()     // Catch:{ all -> 0x002b }
            android.graphics.drawable.Drawable$ConstantState r1 = (android.graphics.drawable.Drawable.ConstantState) r1     // Catch:{ all -> 0x002b }
            if (r1 == 0) goto L_0x002e
            android.content.res.Resources r0 = r5.getResources()     // Catch:{ all -> 0x002b }
            android.graphics.drawable.Drawable r0 = r1.newDrawable(r0)     // Catch:{ all -> 0x002b }
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
            goto L_0x0010
        L_0x002b:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
            throw r0
        L_0x002e:
            r0.b(r6)     // Catch:{ all -> 0x002b }
        L_0x0031:
            monitor-exit(r3)     // Catch:{ all -> 0x002b }
            r0 = r2
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.g.a(android.content.Context, long):android.graphics.drawable.Drawable");
    }

    private boolean a(Context context, long j2, Drawable drawable) {
        Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState == null) {
            return false;
        }
        synchronized (this.m) {
            e eVar = this.n.get(context);
            if (eVar == null) {
                eVar = new e();
                this.n.put(context, eVar);
            }
            eVar.b(j2, new WeakReference(constantState));
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public Drawable a(Context context, ao aoVar, int i2) {
        Drawable d2 = d(context, i2);
        if (d2 == null) {
            d2 = aoVar.a(i2);
        }
        if (d2 != null) {
            return a(context, i2, false, d2);
        }
        return null;
    }

    static boolean a(Context context, int i2, Drawable drawable) {
        int i3;
        int i4;
        PorterDuff.Mode mode;
        boolean z;
        PorterDuff.Mode mode2 = f2201a;
        if (a(f2204d, i2)) {
            i4 = a.C0027a.colorControlNormal;
            mode = mode2;
            z = true;
            i3 = -1;
        } else if (a(f2206f, i2)) {
            i4 = a.C0027a.colorControlActivated;
            mode = mode2;
            z = true;
            i3 = -1;
        } else if (a(f2207g, i2)) {
            z = true;
            mode = PorterDuff.Mode.MULTIPLY;
            i4 = 16842801;
            i3 = -1;
        } else if (i2 == a.e.abc_list_divider_mtrl_alpha) {
            i4 = 16842800;
            i3 = Math.round(40.8f);
            mode = mode2;
            z = true;
        } else if (i2 == a.e.abc_dialog_material_background) {
            i4 = 16842801;
            mode = mode2;
            z = true;
            i3 = -1;
        } else {
            i3 = -1;
            i4 = 0;
            mode = mode2;
            z = false;
        }
        if (!z) {
            return false;
        }
        if (u.c(drawable)) {
            drawable = drawable.mutate();
        }
        drawable.setColorFilter(a(aj.a(context, i4), mode));
        if (i3 == -1) {
            return true;
        }
        drawable.setAlpha(i3);
        return true;
    }

    private void a(String str, c cVar) {
        if (this.k == null) {
            this.k = new ArrayMap<>();
        }
        this.k.put(str, cVar);
    }

    private static boolean a(int[] iArr, int i2) {
        for (int i3 : iArr) {
            if (i3 == i2) {
                return true;
            }
        }
        return false;
    }

    static PorterDuff.Mode a(int i2) {
        if (i2 == a.e.abc_switch_thumb_material) {
            return PorterDuff.Mode.MULTIPLY;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public ColorStateList b(Context context, int i2) {
        ColorStateList e2 = e(context, i2);
        if (e2 == null) {
            if (i2 == a.e.abc_edit_text_material) {
                e2 = android.support.v7.c.a.b.a(context, a.c.abc_tint_edittext);
            } else if (i2 == a.e.abc_switch_track_mtrl_alpha) {
                e2 = android.support.v7.c.a.b.a(context, a.c.abc_tint_switch_track);
            } else if (i2 == a.e.abc_switch_thumb_material) {
                e2 = android.support.v7.c.a.b.a(context, a.c.abc_tint_switch_thumb);
            } else if (i2 == a.e.abc_btn_default_mtrl_shape) {
                e2 = b(context);
            } else if (i2 == a.e.abc_btn_borderless_material) {
                e2 = c(context);
            } else if (i2 == a.e.abc_btn_colored_material) {
                e2 = d(context);
            } else if (i2 == a.e.abc_spinner_mtrl_am_alpha || i2 == a.e.abc_spinner_textfield_background_material) {
                e2 = android.support.v7.c.a.b.a(context, a.c.abc_tint_spinner);
            } else if (a(f2205e, i2)) {
                e2 = aj.b(context, a.C0027a.colorControlNormal);
            } else if (a(f2208h, i2)) {
                e2 = android.support.v7.c.a.b.a(context, a.c.abc_tint_default);
            } else if (a(i, i2)) {
                e2 = android.support.v7.c.a.b.a(context, a.c.abc_tint_btn_checkable);
            } else if (i2 == a.e.abc_seekbar_thumb_material) {
                e2 = android.support.v7.c.a.b.a(context, a.c.abc_tint_seek_thumb);
            }
            if (e2 != null) {
                a(context, i2, e2);
            }
        }
        return e2;
    }

    private ColorStateList e(Context context, int i2) {
        if (this.j == null) {
            return null;
        }
        j jVar = this.j.get(context);
        if (jVar != null) {
            return (ColorStateList) jVar.a(i2);
        }
        return null;
    }

    private void a(Context context, int i2, ColorStateList colorStateList) {
        if (this.j == null) {
            this.j = new WeakHashMap<>();
        }
        j jVar = this.j.get(context);
        if (jVar == null) {
            jVar = new j();
            this.j.put(context, jVar);
        }
        jVar.c(i2, colorStateList);
    }

    private ColorStateList b(Context context) {
        return f(context, aj.a(context, a.C0027a.colorButtonNormal));
    }

    private ColorStateList c(Context context) {
        return f(context, 0);
    }

    private ColorStateList d(Context context) {
        return f(context, aj.a(context, a.C0027a.colorAccent));
    }

    private ColorStateList f(Context context, int i2) {
        int a2 = aj.a(context, a.C0027a.colorControlHighlight);
        int c2 = aj.c(context, a.C0027a.colorButtonNormal);
        return new ColorStateList(new int[][]{aj.f2126a, aj.f2129d, aj.f2127b, aj.f2133h}, new int[]{c2, android.support.v4.b.a.a(a2, i2), android.support.v4.b.a.a(a2, i2), i2});
    }

    /* compiled from: AppCompatDrawableManager */
    private static class b extends f<Integer, PorterDuffColorFilter> {
        public b(int i) {
            super(i);
        }

        /* access modifiers changed from: package-private */
        public PorterDuffColorFilter a(int i, PorterDuff.Mode mode) {
            return (PorterDuffColorFilter) get(Integer.valueOf(b(i, mode)));
        }

        /* access modifiers changed from: package-private */
        public PorterDuffColorFilter a(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) put(Integer.valueOf(b(i, mode)), porterDuffColorFilter);
        }

        private static int b(int i, PorterDuff.Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }
    }

    static void a(Drawable drawable, al alVar, int[] iArr) {
        if (!u.c(drawable) || drawable.mutate() == drawable) {
            if (alVar.f2141d || alVar.f2140c) {
                drawable.setColorFilter(a(alVar.f2141d ? alVar.f2138a : null, alVar.f2140c ? alVar.f2139b : f2201a, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (Build.VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d("AppCompatDrawableManager", "Mutated drawable is not the same instance as the input.");
    }

    private static PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return a(colorStateList.getColorForState(iArr, 0), mode);
    }

    public static PorterDuffColorFilter a(int i2, PorterDuff.Mode mode) {
        PorterDuffColorFilter a2 = f2203c.a(i2, mode);
        if (a2 != null) {
            return a2;
        }
        PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(i2, mode);
        f2203c.a(i2, mode, porterDuffColorFilter);
        return porterDuffColorFilter;
    }

    private static void a(Drawable drawable, int i2, PorterDuff.Mode mode) {
        if (u.c(drawable)) {
            drawable = drawable.mutate();
        }
        if (mode == null) {
            mode = f2201a;
        }
        drawable.setColorFilter(a(i2, mode));
    }

    private void e(Context context) {
        if (!this.p) {
            this.p = true;
            Drawable a2 = a(context, a.e.abc_vector_test);
            if (a2 == null || !a(a2)) {
                this.p = false;
                throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
            }
        }
    }

    private static boolean a(Drawable drawable) {
        return (drawable instanceof VectorDrawableCompat) || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }

    /* compiled from: AppCompatDrawableManager */
    private static class d implements c {
        d() {
        }

        @SuppressLint({"NewApi"})
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return VectorDrawableCompat.a(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e2) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e2);
                return null;
            }
        }
    }

    @TargetApi(11)
    /* compiled from: AppCompatDrawableManager */
    private static class a implements c {
        a() {
        }

        @SuppressLint({"NewApi"})
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return AnimatedVectorDrawableCompat.a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e2) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e2);
                return null;
            }
        }
    }
}
