package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ActionProvider;
import android.support.v7.a.a;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.BaseMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.view.menu.h;
import android.support.v7.view.menu.i;
import android.support.v7.view.menu.j;
import android.support.v7.view.menu.m;
import android.support.v7.widget.ActionMenuView;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

class ActionMenuPresenter extends BaseMenuPresenter implements ActionProvider.a {
    private b A;

    /* renamed from: g  reason: collision with root package name */
    d f1613g;

    /* renamed from: h  reason: collision with root package name */
    e f1614h;
    a i;
    c j;
    final f k = new f();
    int l;
    private Drawable m;
    private boolean n;
    private boolean o;
    private boolean p;
    private int q;
    private int r;
    private int s;
    private boolean t;
    private boolean u;
    private boolean v;
    private boolean w;
    private int x;
    private final SparseBooleanArray y = new SparseBooleanArray();
    private View z;

    public ActionMenuPresenter(Context context) {
        super(context, a.h.abc_action_menu_layout, a.h.abc_action_menu_item_layout);
    }

    public void initForMenu(Context context, MenuBuilder menuBuilder) {
        super.initForMenu(context, menuBuilder);
        Resources resources = context.getResources();
        android.support.v7.view.a a2 = android.support.v7.view.a.a(context);
        if (!this.p) {
            this.o = a2.b();
        }
        if (!this.v) {
            this.q = a2.c();
        }
        if (!this.t) {
            this.s = a2.a();
        }
        int i2 = this.q;
        if (this.o) {
            if (this.f1613g == null) {
                this.f1613g = new d(this.f1483a);
                if (this.n) {
                    this.f1613g.setImageDrawable(this.m);
                    this.m = null;
                    this.n = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.f1613g.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i2 -= this.f1613g.getMeasuredWidth();
        } else {
            this.f1613g = null;
        }
        this.r = i2;
        this.x = (int) (56.0f * resources.getDisplayMetrics().density);
        this.z = null;
    }

    public void a(Configuration configuration) {
        if (!this.t) {
            this.s = android.support.v7.view.a.a(this.f1484b).a();
        }
        if (this.f1485c != null) {
            this.f1485c.onItemsChanged(true);
        }
    }

    public void b(boolean z2) {
        this.o = z2;
        this.p = true;
    }

    public void c(boolean z2) {
        this.w = z2;
    }

    public void a(Drawable drawable) {
        if (this.f1613g != null) {
            this.f1613g.setImageDrawable(drawable);
            return;
        }
        this.n = true;
        this.m = drawable;
    }

    public Drawable b() {
        if (this.f1613g != null) {
            return this.f1613g.getDrawable();
        }
        if (this.n) {
            return this.m;
        }
        return null;
    }

    public j a(ViewGroup viewGroup) {
        j jVar = this.f1488f;
        j a2 = super.a(viewGroup);
        if (jVar != a2) {
            ((ActionMenuView) a2).setPresenter(this);
        }
        return a2;
    }

    public View a(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        View actionView = menuItemImpl.getActionView();
        if (actionView == null || menuItemImpl.n()) {
            actionView = super.a(menuItemImpl, view, viewGroup);
        }
        actionView.setVisibility(menuItemImpl.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    public void a(MenuItemImpl menuItemImpl, j.a aVar) {
        aVar.initialize(menuItemImpl, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.f1488f);
        if (this.A == null) {
            this.A = new b();
        }
        actionMenuItemView.setPopupCallback(this.A);
    }

    public boolean a(int i2, MenuItemImpl menuItemImpl) {
        return menuItemImpl.j();
    }

    public void updateMenuView(boolean z2) {
        boolean z3;
        boolean z4 = true;
        boolean z5 = false;
        ViewGroup viewGroup = (ViewGroup) ((View) this.f1488f).getParent();
        if (viewGroup != null) {
            android.support.v7.e.a.a(viewGroup);
        }
        super.updateMenuView(z2);
        ((View) this.f1488f).requestLayout();
        if (this.f1485c != null) {
            ArrayList<MenuItemImpl> actionItems = this.f1485c.getActionItems();
            int size = actionItems.size();
            for (int i2 = 0; i2 < size; i2++) {
                ActionProvider a2 = actionItems.get(i2).a();
                if (a2 != null) {
                    a2.a(this);
                }
            }
        }
        ArrayList<MenuItemImpl> nonActionItems = this.f1485c != null ? this.f1485c.getNonActionItems() : null;
        if (this.o && nonActionItems != null) {
            int size2 = nonActionItems.size();
            if (size2 == 1) {
                if (!nonActionItems.get(0).isActionViewExpanded()) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                z5 = z3;
            } else {
                if (size2 <= 0) {
                    z4 = false;
                }
                z5 = z4;
            }
        }
        if (z5) {
            if (this.f1613g == null) {
                this.f1613g = new d(this.f1483a);
            }
            ViewGroup viewGroup2 = (ViewGroup) this.f1613g.getParent();
            if (viewGroup2 != this.f1488f) {
                if (viewGroup2 != null) {
                    viewGroup2.removeView(this.f1613g);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.f1488f;
                actionMenuView.addView(this.f1613g, actionMenuView.c());
            }
        } else if (this.f1613g != null && this.f1613g.getParent() == this.f1488f) {
            ((ViewGroup) this.f1488f).removeView(this.f1613g);
        }
        ((ActionMenuView) this.f1488f).setOverflowReserved(this.o);
    }

    public boolean a(ViewGroup viewGroup, int i2) {
        if (viewGroup.getChildAt(i2) == this.f1613g) {
            return false;
        }
        return super.a(viewGroup, i2);
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        boolean z2;
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        SubMenuBuilder subMenuBuilder2 = subMenuBuilder;
        while (subMenuBuilder2.getParentMenu() != this.f1485c) {
            subMenuBuilder2 = (SubMenuBuilder) subMenuBuilder2.getParentMenu();
        }
        View a2 = a(subMenuBuilder2.getItem());
        if (a2 == null) {
            return false;
        }
        this.l = subMenuBuilder.getItem().getItemId();
        int size = subMenuBuilder.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                z2 = false;
                break;
            }
            MenuItem item = subMenuBuilder.getItem(i2);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i2++;
        }
        this.i = new a(this.f1484b, subMenuBuilder, a2);
        this.i.a(z2);
        this.i.a();
        super.onSubMenuSelected(subMenuBuilder);
        return true;
    }

    private View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.f1488f;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if ((childAt instanceof j.a) && ((j.a) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    public boolean c() {
        if (!this.o || g() || this.f1485c == null || this.f1488f == null || this.j != null || this.f1485c.getNonActionItems().isEmpty()) {
            return false;
        }
        this.j = new c(new e(this.f1484b, this.f1485c, this.f1613g, true));
        ((View) this.f1488f).post(this.j);
        super.onSubMenuSelected(null);
        return true;
    }

    public boolean d() {
        if (this.j == null || this.f1488f == null) {
            e eVar = this.f1614h;
            if (eVar == null) {
                return false;
            }
            eVar.d();
            return true;
        }
        ((View) this.f1488f).removeCallbacks(this.j);
        this.j = null;
        return true;
    }

    public boolean e() {
        return d() | f();
    }

    public boolean f() {
        if (this.i == null) {
            return false;
        }
        this.i.d();
        return true;
    }

    public boolean g() {
        return this.f1614h != null && this.f1614h.f();
    }

    public boolean h() {
        return this.j != null || g();
    }

    public boolean flagActionItems() {
        int i2;
        ArrayList<MenuItemImpl> arrayList;
        int i3;
        int i4;
        int i5;
        int i6;
        boolean z2;
        int i7;
        int i8;
        int i9;
        int i10;
        boolean z3;
        int i11;
        if (this.f1485c != null) {
            ArrayList<MenuItemImpl> visibleItems = this.f1485c.getVisibleItems();
            i2 = visibleItems.size();
            arrayList = visibleItems;
        } else {
            i2 = 0;
            arrayList = null;
        }
        int i12 = this.s;
        int i13 = this.r;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.f1488f;
        int i14 = 0;
        int i15 = 0;
        boolean z4 = false;
        int i16 = 0;
        while (i16 < i2) {
            MenuItemImpl menuItemImpl = (MenuItemImpl) arrayList.get(i16);
            if (menuItemImpl.l()) {
                i14++;
            } else if (menuItemImpl.k()) {
                i15++;
            } else {
                z4 = true;
            }
            if (!this.w || !menuItemImpl.isActionViewExpanded()) {
                i11 = i12;
            } else {
                i11 = 0;
            }
            i16++;
            i12 = i11;
        }
        if (this.o && (z4 || i14 + i15 > i12)) {
            i12--;
        }
        int i17 = i12 - i14;
        SparseBooleanArray sparseBooleanArray = this.y;
        sparseBooleanArray.clear();
        int i18 = 0;
        if (this.u) {
            i18 = i13 / this.x;
            i3 = ((i13 % this.x) / i18) + this.x;
        } else {
            i3 = 0;
        }
        int i19 = 0;
        int i20 = 0;
        int i21 = i18;
        while (i19 < i2) {
            MenuItemImpl menuItemImpl2 = (MenuItemImpl) arrayList.get(i19);
            if (menuItemImpl2.l()) {
                View a2 = a(menuItemImpl2, this.z, viewGroup);
                if (this.z == null) {
                    this.z = a2;
                }
                if (this.u) {
                    i21 -= ActionMenuView.a(a2, i3, i21, makeMeasureSpec, 0);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                i4 = a2.getMeasuredWidth();
                int i22 = i13 - i4;
                if (i20 != 0) {
                    i4 = i20;
                }
                int groupId = menuItemImpl2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                menuItemImpl2.d(true);
                i5 = i22;
                i6 = i17;
            } else if (menuItemImpl2.k()) {
                int groupId2 = menuItemImpl2.getGroupId();
                boolean z5 = sparseBooleanArray.get(groupId2);
                boolean z6 = (i17 > 0 || z5) && i13 > 0 && (!this.u || i21 > 0);
                if (z6) {
                    View a3 = a(menuItemImpl2, this.z, viewGroup);
                    if (this.z == null) {
                        this.z = a3;
                    }
                    if (this.u) {
                        int a4 = ActionMenuView.a(a3, i3, i21, makeMeasureSpec, 0);
                        int i23 = i21 - a4;
                        if (a4 == 0) {
                            z3 = false;
                        } else {
                            z3 = z6;
                        }
                        i10 = i23;
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                        boolean z7 = z6;
                        i10 = i21;
                        z3 = z7;
                    }
                    int measuredWidth = a3.getMeasuredWidth();
                    i13 -= measuredWidth;
                    if (i20 == 0) {
                        i20 = measuredWidth;
                    }
                    if (this.u) {
                        z2 = z3 & (i13 >= 0);
                        i7 = i20;
                        i8 = i10;
                    } else {
                        z2 = z3 & (i13 + i20 > 0);
                        i7 = i20;
                        i8 = i10;
                    }
                } else {
                    z2 = z6;
                    i7 = i20;
                    i8 = i21;
                }
                if (z2 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                    i9 = i17;
                } else if (z5) {
                    sparseBooleanArray.put(groupId2, false);
                    int i24 = i17;
                    for (int i25 = 0; i25 < i19; i25++) {
                        MenuItemImpl menuItemImpl3 = (MenuItemImpl) arrayList.get(i25);
                        if (menuItemImpl3.getGroupId() == groupId2) {
                            if (menuItemImpl3.j()) {
                                i24++;
                            }
                            menuItemImpl3.d(false);
                        }
                    }
                    i9 = i24;
                } else {
                    i9 = i17;
                }
                if (z2) {
                    i9--;
                }
                menuItemImpl2.d(z2);
                i4 = i7;
                i5 = i13;
                int i26 = i8;
                i6 = i9;
                i21 = i26;
            } else {
                menuItemImpl2.d(false);
                i4 = i20;
                i5 = i13;
                i6 = i17;
            }
            i19++;
            i13 = i5;
            i17 = i6;
            i20 = i4;
        }
        return true;
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z2) {
        e();
        super.onCloseMenu(menuBuilder, z2);
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState();
        savedState.f1615a = this.l;
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        if (parcelable instanceof SavedState) {
            SavedState savedState = (SavedState) parcelable;
            if (savedState.f1615a > 0 && (findItem = this.f1485c.findItem(savedState.f1615a)) != null) {
                onSubMenuSelected((SubMenuBuilder) findItem.getSubMenu());
            }
        }
    }

    public void a(boolean z2) {
        if (z2) {
            super.onSubMenuSelected(null);
        } else if (this.f1485c != null) {
            this.f1485c.close(false);
        }
    }

    public void a(ActionMenuView actionMenuView) {
        this.f1488f = actionMenuView;
        actionMenuView.initialize(this.f1485c);
    }

    private static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        public int f1615a;

        SavedState() {
        }

        SavedState(Parcel parcel) {
            this.f1615a = parcel.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f1615a);
        }
    }

    private class d extends AppCompatImageView implements ActionMenuView.a {

        /* renamed from: b  reason: collision with root package name */
        private final float[] f1621b = new float[2];

        public d(Context context) {
            super(context, null, a.C0027a.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            setOnTouchListener(new x(this, ActionMenuPresenter.this) {
                public m a() {
                    if (ActionMenuPresenter.this.f1614h == null) {
                        return null;
                    }
                    return ActionMenuPresenter.this.f1614h.b();
                }

                public boolean b() {
                    ActionMenuPresenter.this.c();
                    return true;
                }

                public boolean c() {
                    if (ActionMenuPresenter.this.j != null) {
                        return false;
                    }
                    ActionMenuPresenter.this.d();
                    return true;
                }
            });
        }

        public boolean performClick() {
            if (!super.performClick()) {
                playSoundEffect(0);
                ActionMenuPresenter.this.c();
            }
            return true;
        }

        public boolean b() {
            return false;
        }

        public boolean c() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                android.support.v4.b.a.a.a(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    private class e extends h {
        public e(Context context, MenuBuilder menuBuilder, View view, boolean z) {
            super(context, menuBuilder, view, z, a.C0027a.actionOverflowMenuStyle);
            a(8388613);
            a(ActionMenuPresenter.this.k);
        }

        /* access modifiers changed from: protected */
        public void e() {
            if (ActionMenuPresenter.this.f1485c != null) {
                ActionMenuPresenter.this.f1485c.close();
            }
            ActionMenuPresenter.this.f1614h = null;
            super.e();
        }
    }

    private class a extends h {
        public a(Context context, SubMenuBuilder subMenuBuilder, View view) {
            super(context, subMenuBuilder, view, false, a.C0027a.actionOverflowMenuStyle);
            if (!((MenuItemImpl) subMenuBuilder.getItem()).j()) {
                a(ActionMenuPresenter.this.f1613g == null ? (View) ActionMenuPresenter.this.f1488f : ActionMenuPresenter.this.f1613g);
            }
            a(ActionMenuPresenter.this.k);
        }

        /* access modifiers changed from: protected */
        public void e() {
            ActionMenuPresenter.this.i = null;
            ActionMenuPresenter.this.l = 0;
            super.e();
        }
    }

    private class f implements i.a {
        f() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            if (menuBuilder == null) {
                return false;
            }
            ActionMenuPresenter.this.l = ((SubMenuBuilder) menuBuilder).getItem().getItemId();
            i.a a2 = ActionMenuPresenter.this.a();
            return a2 != null ? a2.a(menuBuilder) : false;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            if (menuBuilder instanceof SubMenuBuilder) {
                menuBuilder.getRootMenu().close(false);
            }
            i.a a2 = ActionMenuPresenter.this.a();
            if (a2 != null) {
                a2.a(menuBuilder, z);
            }
        }
    }

    private class c implements Runnable {

        /* renamed from: b  reason: collision with root package name */
        private e f1619b;

        public c(e eVar) {
            this.f1619b = eVar;
        }

        public void run() {
            if (ActionMenuPresenter.this.f1485c != null) {
                ActionMenuPresenter.this.f1485c.changeMenuMode();
            }
            View view = (View) ActionMenuPresenter.this.f1488f;
            if (!(view == null || view.getWindowToken() == null || !this.f1619b.c())) {
                ActionMenuPresenter.this.f1614h = this.f1619b;
            }
            ActionMenuPresenter.this.j = null;
        }
    }

    private class b extends ActionMenuItemView.b {
        b() {
        }

        public m a() {
            if (ActionMenuPresenter.this.i != null) {
                return ActionMenuPresenter.this.i.b();
            }
            return null;
        }
    }
}
