package android.support.v7.widget.helper;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.a.g;
import android.support.v4.view.ae;
import android.support.v4.view.ag;
import android.support.v4.view.s;
import android.support.v7.d.a;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.b;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import com.duapps.ad.AdError;
import java.util.ArrayList;
import java.util.List;

public class ItemTouchHelper extends RecyclerView.g implements RecyclerView.i {
    private final RecyclerView.k A = new RecyclerView.k() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.RecyclerView$u, boolean):int
         arg types: [android.support.v7.widget.RecyclerView$u, int]
         candidates:
          android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.RecyclerView$u, int):void
          android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.helper.ItemTouchHelper$c, int):void
          android.support.v7.widget.RecyclerView.g.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView):void
          android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.RecyclerView$u, boolean):int */
        public boolean a(RecyclerView recyclerView, MotionEvent motionEvent) {
            int findPointerIndex;
            c b2;
            ItemTouchHelper.this.u.a(motionEvent);
            int a2 = s.a(motionEvent);
            if (a2 == 0) {
                ItemTouchHelper.this.k = motionEvent.getPointerId(0);
                ItemTouchHelper.this.f2212c = motionEvent.getX();
                ItemTouchHelper.this.f2213d = motionEvent.getY();
                ItemTouchHelper.this.c();
                if (ItemTouchHelper.this.f2211b == null && (b2 = ItemTouchHelper.this.b(motionEvent)) != null) {
                    ItemTouchHelper.this.f2212c -= b2.l;
                    ItemTouchHelper.this.f2213d -= b2.m;
                    ItemTouchHelper.this.a(b2.f2238h, true);
                    if (ItemTouchHelper.this.f2210a.remove(b2.f2238h.itemView)) {
                        ItemTouchHelper.this.l.d(ItemTouchHelper.this.p, b2.f2238h);
                    }
                    ItemTouchHelper.this.a(b2.f2238h, b2.i);
                    ItemTouchHelper.this.a(motionEvent, ItemTouchHelper.this.n, 0);
                }
            } else if (a2 == 3 || a2 == 1) {
                ItemTouchHelper.this.k = -1;
                ItemTouchHelper.this.a((RecyclerView.u) null, 0);
            } else if (ItemTouchHelper.this.k != -1 && (findPointerIndex = motionEvent.findPointerIndex(ItemTouchHelper.this.k)) >= 0) {
                ItemTouchHelper.this.a(a2, motionEvent, findPointerIndex);
            }
            if (ItemTouchHelper.this.r != null) {
                ItemTouchHelper.this.r.addMovement(motionEvent);
            }
            if (ItemTouchHelper.this.f2211b != null) {
                return true;
            }
            return false;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void b(RecyclerView recyclerView, MotionEvent motionEvent) {
            int i = 0;
            ItemTouchHelper.this.u.a(motionEvent);
            if (ItemTouchHelper.this.r != null) {
                ItemTouchHelper.this.r.addMovement(motionEvent);
            }
            if (ItemTouchHelper.this.k != -1) {
                int a2 = s.a(motionEvent);
                int findPointerIndex = motionEvent.findPointerIndex(ItemTouchHelper.this.k);
                if (findPointerIndex >= 0) {
                    ItemTouchHelper.this.a(a2, motionEvent, findPointerIndex);
                }
                RecyclerView.u uVar = ItemTouchHelper.this.f2211b;
                if (uVar != null) {
                    switch (a2) {
                        case 1:
                            break;
                        case 2:
                            if (findPointerIndex >= 0) {
                                ItemTouchHelper.this.a(motionEvent, ItemTouchHelper.this.n, findPointerIndex);
                                ItemTouchHelper.this.a(uVar);
                                ItemTouchHelper.this.p.removeCallbacks(ItemTouchHelper.this.q);
                                ItemTouchHelper.this.q.run();
                                ItemTouchHelper.this.p.invalidate();
                                return;
                            }
                            return;
                        case 3:
                            if (ItemTouchHelper.this.r != null) {
                                ItemTouchHelper.this.r.clear();
                                break;
                            }
                            break;
                        case 4:
                        case 5:
                        default:
                            return;
                        case 6:
                            int b2 = s.b(motionEvent);
                            if (motionEvent.getPointerId(b2) == ItemTouchHelper.this.k) {
                                if (b2 == 0) {
                                    i = 1;
                                }
                                ItemTouchHelper.this.k = motionEvent.getPointerId(i);
                                ItemTouchHelper.this.a(motionEvent, ItemTouchHelper.this.n, b2);
                                return;
                            }
                            return;
                    }
                    ItemTouchHelper.this.a((RecyclerView.u) null, 0);
                    ItemTouchHelper.this.k = -1;
                }
            }
        }

        public void a(boolean z) {
            if (z) {
                ItemTouchHelper.this.a((RecyclerView.u) null, 0);
            }
        }
    };
    private Rect B;
    private long C;

    /* renamed from: a  reason: collision with root package name */
    final List<View> f2210a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    RecyclerView.u f2211b = null;

    /* renamed from: c  reason: collision with root package name */
    float f2212c;

    /* renamed from: d  reason: collision with root package name */
    float f2213d;

    /* renamed from: e  reason: collision with root package name */
    float f2214e;

    /* renamed from: f  reason: collision with root package name */
    float f2215f;

    /* renamed from: g  reason: collision with root package name */
    float f2216g;

    /* renamed from: h  reason: collision with root package name */
    float f2217h;
    float i;
    float j;
    int k = -1;
    a l;
    int m = 0;
    int n;
    List<c> o = new ArrayList();
    RecyclerView p;
    final Runnable q = new Runnable() {
        public void run() {
            if (ItemTouchHelper.this.f2211b != null && ItemTouchHelper.this.b()) {
                if (ItemTouchHelper.this.f2211b != null) {
                    ItemTouchHelper.this.a(ItemTouchHelper.this.f2211b);
                }
                ItemTouchHelper.this.p.removeCallbacks(ItemTouchHelper.this.q);
                ag.a(ItemTouchHelper.this.p, this);
            }
        }
    };
    VelocityTracker r;
    View s = null;
    int t = -1;
    android.support.v4.view.d u;
    private final float[] v = new float[2];
    private int w;
    private List<RecyclerView.u> x;
    private List<Integer> y;
    private RecyclerView.d z = null;

    public static abstract class SimpleCallback extends a {
    }

    public interface d {
        void a(View view, View view2, int i, int i2);
    }

    public ItemTouchHelper(a aVar) {
        this.l = aVar;
    }

    private static boolean a(View view, float f2, float f3, float f4, float f5) {
        return f2 >= f4 && f2 <= ((float) view.getWidth()) + f4 && f3 >= f5 && f3 <= ((float) view.getHeight()) + f5;
    }

    public void a(RecyclerView recyclerView) {
        if (this.p != recyclerView) {
            if (this.p != null) {
                e();
            }
            this.p = recyclerView;
            if (this.p != null) {
                Resources resources = recyclerView.getResources();
                this.f2214e = resources.getDimension(a.C0029a.item_touch_helper_swipe_escape_velocity);
                this.f2215f = resources.getDimension(a.C0029a.item_touch_helper_swipe_escape_max_velocity);
                d();
            }
        }
    }

    private void d() {
        this.w = ViewConfiguration.get(this.p.getContext()).getScaledTouchSlop();
        this.p.addItemDecoration(this);
        this.p.addOnItemTouchListener(this.A);
        this.p.addOnChildAttachStateChangeListener(this);
        f();
    }

    private void e() {
        this.p.removeItemDecoration(this);
        this.p.removeOnItemTouchListener(this.A);
        this.p.removeOnChildAttachStateChangeListener(this);
        for (int size = this.o.size() - 1; size >= 0; size--) {
            this.l.d(this.p, this.o.get(0).f2238h);
        }
        this.o.clear();
        this.s = null;
        this.t = -1;
        g();
    }

    private void f() {
        if (this.u == null) {
            this.u = new android.support.v4.view.d(this.p.getContext(), new b());
        }
    }

    private void a(float[] fArr) {
        if ((this.n & 12) != 0) {
            fArr[0] = (this.i + this.f2216g) - ((float) this.f2211b.itemView.getLeft());
        } else {
            fArr[0] = ag.m(this.f2211b.itemView);
        }
        if ((this.n & 3) != 0) {
            fArr[1] = (this.j + this.f2217h) - ((float) this.f2211b.itemView.getTop());
        } else {
            fArr[1] = ag.n(this.f2211b.itemView);
        }
    }

    public void b(Canvas canvas, RecyclerView recyclerView, RecyclerView.r rVar) {
        float f2;
        float f3 = 0.0f;
        if (this.f2211b != null) {
            a(this.v);
            f2 = this.v[0];
            f3 = this.v[1];
        } else {
            f2 = 0.0f;
        }
        this.l.b(canvas, recyclerView, this.f2211b, this.o, this.m, f2, f3);
    }

    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.r rVar) {
        float f2;
        float f3 = 0.0f;
        this.t = -1;
        if (this.f2211b != null) {
            a(this.v);
            f2 = this.v[0];
            f3 = this.v[1];
        } else {
            f2 = 0.0f;
        }
        this.l.a(canvas, recyclerView, this.f2211b, this.o, this.m, f2, f3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.RecyclerView$u, boolean):int
     arg types: [android.support.v7.widget.RecyclerView$u, int]
     candidates:
      android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.RecyclerView$u, int):void
      android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.helper.ItemTouchHelper$c, int):void
      android.support.v7.widget.RecyclerView.g.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView):void
      android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.RecyclerView$u, boolean):int */
    /* access modifiers changed from: package-private */
    public void a(RecyclerView.u uVar, int i2) {
        final int c2;
        float f2;
        float signum;
        int i3;
        if (uVar != this.f2211b || i2 != this.m) {
            this.C = Long.MIN_VALUE;
            int i4 = this.m;
            a(uVar, true);
            this.m = i2;
            if (i2 == 2) {
                this.s = uVar.itemView;
                h();
            }
            int i5 = (1 << ((i2 * 8) + 8)) - 1;
            boolean z2 = false;
            if (this.f2211b != null) {
                RecyclerView.u uVar2 = this.f2211b;
                if (uVar2.itemView.getParent() != null) {
                    if (i4 == 2) {
                        c2 = 0;
                    } else {
                        c2 = c(uVar2);
                    }
                    g();
                    switch (c2) {
                        case 1:
                        case 2:
                            f2 = 0.0f;
                            signum = Math.signum(this.f2217h) * ((float) this.p.getHeight());
                            break;
                        case 4:
                        case 8:
                        case 16:
                        case 32:
                            signum = 0.0f;
                            f2 = Math.signum(this.f2216g) * ((float) this.p.getWidth());
                            break;
                        default:
                            f2 = 0.0f;
                            signum = 0.0f;
                            break;
                    }
                    if (i4 == 2) {
                        i3 = 8;
                    } else if (c2 > 0) {
                        i3 = 2;
                    } else {
                        i3 = 4;
                    }
                    a(this.v);
                    float f3 = this.v[0];
                    float f4 = this.v[1];
                    final RecyclerView.u uVar3 = uVar2;
                    AnonymousClass3 r0 = new c(uVar2, i3, i4, f3, f4, f2, signum) {
                        public void b(g gVar) {
                            super.b(gVar);
                            if (!this.n) {
                                if (c2 <= 0) {
                                    ItemTouchHelper.this.l.d(ItemTouchHelper.this.p, uVar3);
                                } else {
                                    ItemTouchHelper.this.f2210a.add(uVar3.itemView);
                                    this.k = true;
                                    if (c2 > 0) {
                                        ItemTouchHelper.this.a(this, c2);
                                    }
                                }
                                if (ItemTouchHelper.this.s == uVar3.itemView) {
                                    ItemTouchHelper.this.c(uVar3.itemView);
                                }
                            }
                        }
                    };
                    r0.a(this.l.a(this.p, i3, f2 - f3, signum - f4));
                    this.o.add(r0);
                    r0.a();
                    z2 = true;
                } else {
                    c(uVar2.itemView);
                    this.l.d(this.p, uVar2);
                }
                this.f2211b = null;
            }
            boolean z3 = z2;
            if (uVar != null) {
                this.n = (this.l.b(this.p, uVar) & i5) >> (this.m * 8);
                this.i = (float) uVar.itemView.getLeft();
                this.j = (float) uVar.itemView.getTop();
                this.f2211b = uVar;
                if (i2 == 2) {
                    this.f2211b.itemView.performHapticFeedback(0);
                }
            }
            ViewParent parent = this.p.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(this.f2211b != null);
            }
            if (!z3) {
                this.p.getLayoutManager().I();
            }
            this.l.b(this.f2211b, this.m);
            this.p.invalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final c cVar, final int i2) {
        this.p.post(new Runnable() {
            public void run() {
                if (ItemTouchHelper.this.p != null && ItemTouchHelper.this.p.isAttachedToWindow() && !cVar.n && cVar.f2238h.getAdapterPosition() != -1) {
                    RecyclerView.e itemAnimator = ItemTouchHelper.this.p.getItemAnimator();
                    if ((itemAnimator == null || !itemAnimator.a((RecyclerView.e.a) null)) && !ItemTouchHelper.this.a()) {
                        ItemTouchHelper.this.l.a(cVar.f2238h, i2);
                    } else {
                        ItemTouchHelper.this.p.post(this);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        int size = this.o.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (!this.o.get(i2).o) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0051, code lost:
        if (r4 < 0) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0073, code lost:
        if (r8 < 0) goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00e4, code lost:
        if (r4 > 0) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x010c, code lost:
        if (r8 > 0) goto L_0x0075;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b() {
        /*
            r14 = this;
            r12 = -9223372036854775808
            r0 = 0
            r5 = 0
            android.support.v7.widget.RecyclerView$u r1 = r14.f2211b
            if (r1 != 0) goto L_0x000b
            r14.C = r12
        L_0x000a:
            return r0
        L_0x000b:
            long r10 = java.lang.System.currentTimeMillis()
            long r2 = r14.C
            int r1 = (r2 > r12 ? 1 : (r2 == r12 ? 0 : -1))
            if (r1 != 0) goto L_0x00bb
            r6 = 0
        L_0x0017:
            android.support.v7.widget.RecyclerView r1 = r14.p
            android.support.v7.widget.RecyclerView$h r1 = r1.getLayoutManager()
            android.graphics.Rect r2 = r14.B
            if (r2 != 0) goto L_0x0028
            android.graphics.Rect r2 = new android.graphics.Rect
            r2.<init>()
            r14.B = r2
        L_0x0028:
            android.support.v7.widget.RecyclerView$u r2 = r14.f2211b
            android.view.View r2 = r2.itemView
            android.graphics.Rect r3 = r14.B
            r1.b(r2, r3)
            boolean r2 = r1.d()
            if (r2 == 0) goto L_0x00e6
            float r2 = r14.i
            float r3 = r14.f2216g
            float r2 = r2 + r3
            int r2 = (int) r2
            android.graphics.Rect r3 = r14.B
            int r3 = r3.left
            int r3 = r2 - r3
            android.support.v7.widget.RecyclerView r4 = r14.p
            int r4 = r4.getPaddingLeft()
            int r4 = r3 - r4
            float r3 = r14.f2216g
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 >= 0) goto L_0x00c1
            if (r4 >= 0) goto L_0x00c1
        L_0x0053:
            boolean r1 = r1.e()
            if (r1 == 0) goto L_0x010e
            float r1 = r14.j
            float r2 = r14.f2217h
            float r1 = r1 + r2
            int r1 = (int) r1
            android.graphics.Rect r2 = r14.B
            int r2 = r2.top
            int r2 = r1 - r2
            android.support.v7.widget.RecyclerView r3 = r14.p
            int r3 = r3.getPaddingTop()
            int r8 = r2 - r3
            float r2 = r14.f2217h
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 >= 0) goto L_0x00e9
            if (r8 >= 0) goto L_0x00e9
        L_0x0075:
            if (r4 == 0) goto L_0x0117
            android.support.v7.widget.helper.ItemTouchHelper$a r1 = r14.l
            android.support.v7.widget.RecyclerView r2 = r14.p
            android.support.v7.widget.RecyclerView$u r3 = r14.f2211b
            android.view.View r3 = r3.itemView
            int r3 = r3.getWidth()
            android.support.v7.widget.RecyclerView r5 = r14.p
            int r5 = r5.getWidth()
            int r4 = r1.a(r2, r3, r4, r5, r6)
            r9 = r4
        L_0x008e:
            if (r8 == 0) goto L_0x0115
            android.support.v7.widget.helper.ItemTouchHelper$a r1 = r14.l
            android.support.v7.widget.RecyclerView r2 = r14.p
            android.support.v7.widget.RecyclerView$u r3 = r14.f2211b
            android.view.View r3 = r3.itemView
            int r3 = r3.getHeight()
            android.support.v7.widget.RecyclerView r4 = r14.p
            int r5 = r4.getHeight()
            r4 = r8
            int r1 = r1.a(r2, r3, r4, r5, r6)
        L_0x00a7:
            if (r9 != 0) goto L_0x00ab
            if (r1 == 0) goto L_0x0111
        L_0x00ab:
            long r2 = r14.C
            int r0 = (r2 > r12 ? 1 : (r2 == r12 ? 0 : -1))
            if (r0 != 0) goto L_0x00b3
            r14.C = r10
        L_0x00b3:
            android.support.v7.widget.RecyclerView r0 = r14.p
            r0.scrollBy(r9, r1)
            r0 = 1
            goto L_0x000a
        L_0x00bb:
            long r2 = r14.C
            long r6 = r10 - r2
            goto L_0x0017
        L_0x00c1:
            float r3 = r14.f2216g
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x00e6
            android.support.v7.widget.RecyclerView$u r3 = r14.f2211b
            android.view.View r3 = r3.itemView
            int r3 = r3.getWidth()
            int r2 = r2 + r3
            android.graphics.Rect r3 = r14.B
            int r3 = r3.right
            int r2 = r2 + r3
            android.support.v7.widget.RecyclerView r3 = r14.p
            int r3 = r3.getWidth()
            android.support.v7.widget.RecyclerView r4 = r14.p
            int r4 = r4.getPaddingRight()
            int r3 = r3 - r4
            int r4 = r2 - r3
            if (r4 > 0) goto L_0x0053
        L_0x00e6:
            r4 = r0
            goto L_0x0053
        L_0x00e9:
            float r2 = r14.f2217h
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 <= 0) goto L_0x010e
            android.support.v7.widget.RecyclerView$u r2 = r14.f2211b
            android.view.View r2 = r2.itemView
            int r2 = r2.getHeight()
            int r1 = r1 + r2
            android.graphics.Rect r2 = r14.B
            int r2 = r2.bottom
            int r1 = r1 + r2
            android.support.v7.widget.RecyclerView r2 = r14.p
            int r2 = r2.getHeight()
            android.support.v7.widget.RecyclerView r3 = r14.p
            int r3 = r3.getPaddingBottom()
            int r2 = r2 - r3
            int r8 = r1 - r2
            if (r8 > 0) goto L_0x0075
        L_0x010e:
            r8 = r0
            goto L_0x0075
        L_0x0111:
            r14.C = r12
            goto L_0x000a
        L_0x0115:
            r1 = r8
            goto L_0x00a7
        L_0x0117:
            r9 = r4
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.helper.ItemTouchHelper.b():boolean");
    }

    private List<RecyclerView.u> b(RecyclerView.u uVar) {
        if (this.x == null) {
            this.x = new ArrayList();
            this.y = new ArrayList();
        } else {
            this.x.clear();
            this.y.clear();
        }
        int c2 = this.l.c();
        int round = Math.round(this.i + this.f2216g) - c2;
        int round2 = Math.round(this.j + this.f2217h) - c2;
        int width = uVar.itemView.getWidth() + round + (c2 * 2);
        int height = uVar.itemView.getHeight() + round2 + (c2 * 2);
        int i2 = (round + width) / 2;
        int i3 = (round2 + height) / 2;
        RecyclerView.h layoutManager = this.p.getLayoutManager();
        int u2 = layoutManager.u();
        for (int i4 = 0; i4 < u2; i4++) {
            View i5 = layoutManager.i(i4);
            if (i5 != uVar.itemView && i5.getBottom() >= round2 && i5.getTop() <= height && i5.getRight() >= round && i5.getLeft() <= width) {
                RecyclerView.u childViewHolder = this.p.getChildViewHolder(i5);
                if (this.l.a(this.p, this.f2211b, childViewHolder)) {
                    int abs = Math.abs(i2 - ((i5.getLeft() + i5.getRight()) / 2));
                    int abs2 = Math.abs(i3 - ((i5.getBottom() + i5.getTop()) / 2));
                    int i6 = (abs * abs) + (abs2 * abs2);
                    int size = this.x.size();
                    int i7 = 0;
                    int i8 = 0;
                    while (i8 < size && i6 > this.y.get(i8).intValue()) {
                        i7++;
                        i8++;
                    }
                    this.x.add(i7, childViewHolder);
                    this.y.add(i7, Integer.valueOf(i6));
                }
            }
        }
        return this.x;
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.u uVar) {
        if (!this.p.isLayoutRequested() && this.m == 2) {
            float b2 = this.l.b(uVar);
            int i2 = (int) (this.i + this.f2216g);
            int i3 = (int) (this.j + this.f2217h);
            if (((float) Math.abs(i3 - uVar.itemView.getTop())) >= ((float) uVar.itemView.getHeight()) * b2 || ((float) Math.abs(i2 - uVar.itemView.getLeft())) >= b2 * ((float) uVar.itemView.getWidth())) {
                List<RecyclerView.u> b3 = b(uVar);
                if (b3.size() != 0) {
                    RecyclerView.u a2 = this.l.a(uVar, b3, i2, i3);
                    if (a2 == null) {
                        this.x.clear();
                        this.y.clear();
                        return;
                    }
                    int adapterPosition = a2.getAdapterPosition();
                    int adapterPosition2 = uVar.getAdapterPosition();
                    if (this.l.b(this.p, uVar, a2)) {
                        this.l.a(this.p, uVar, adapterPosition2, a2, adapterPosition, i2, i3);
                    }
                }
            }
        }
    }

    public void a(View view) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.RecyclerView$u, boolean):int
     arg types: [android.support.v7.widget.RecyclerView$u, int]
     candidates:
      android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.RecyclerView$u, int):void
      android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.helper.ItemTouchHelper$c, int):void
      android.support.v7.widget.RecyclerView.g.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView):void
      android.support.v7.widget.helper.ItemTouchHelper.a(android.support.v7.widget.RecyclerView$u, boolean):int */
    public void b(View view) {
        c(view);
        RecyclerView.u childViewHolder = this.p.getChildViewHolder(view);
        if (childViewHolder != null) {
            if (this.f2211b == null || childViewHolder != this.f2211b) {
                a(childViewHolder, false);
                if (this.f2210a.remove(childViewHolder.itemView)) {
                    this.l.d(this.p, childViewHolder);
                    return;
                }
                return;
            }
            a((RecyclerView.u) null, 0);
        }
    }

    /* access modifiers changed from: package-private */
    public int a(RecyclerView.u uVar, boolean z2) {
        for (int size = this.o.size() - 1; size >= 0; size--) {
            c cVar = this.o.get(size);
            if (cVar.f2238h == uVar) {
                cVar.n |= z2;
                if (!cVar.o) {
                    cVar.b();
                }
                this.o.remove(size);
                return cVar.j;
            }
        }
        return 0;
    }

    public void a(Rect rect, View view, RecyclerView recyclerView, RecyclerView.r rVar) {
        rect.setEmpty();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.r != null) {
            this.r.recycle();
        }
        this.r = VelocityTracker.obtain();
    }

    private void g() {
        if (this.r != null) {
            this.r.recycle();
            this.r = null;
        }
    }

    private RecyclerView.u c(MotionEvent motionEvent) {
        View a2;
        RecyclerView.h layoutManager = this.p.getLayoutManager();
        if (this.k == -1) {
            return null;
        }
        int findPointerIndex = motionEvent.findPointerIndex(this.k);
        float abs = Math.abs(motionEvent.getX(findPointerIndex) - this.f2212c);
        float abs2 = Math.abs(motionEvent.getY(findPointerIndex) - this.f2213d);
        if (abs < ((float) this.w) && abs2 < ((float) this.w)) {
            return null;
        }
        if (abs > abs2 && layoutManager.d()) {
            return null;
        }
        if ((abs2 <= abs || !layoutManager.e()) && (a2 = a(motionEvent)) != null) {
            return this.p.getChildViewHolder(a2);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, MotionEvent motionEvent, int i3) {
        RecyclerView.u c2;
        int b2;
        if (this.f2211b != null || i2 != 2 || this.m == 2 || !this.l.b() || this.p.getScrollState() == 1 || (c2 = c(motionEvent)) == null || (b2 = (this.l.b(this.p, c2) & 65280) >> 8) == 0) {
            return false;
        }
        float x2 = motionEvent.getX(i3);
        float y2 = motionEvent.getY(i3);
        float f2 = x2 - this.f2212c;
        float f3 = y2 - this.f2213d;
        float abs = Math.abs(f2);
        float abs2 = Math.abs(f3);
        if (abs < ((float) this.w) && abs2 < ((float) this.w)) {
            return false;
        }
        if (abs > abs2) {
            if (f2 < 0.0f && (b2 & 4) == 0) {
                return false;
            }
            if (f2 > 0.0f && (b2 & 8) == 0) {
                return false;
            }
        } else if (f3 < 0.0f && (b2 & 1) == 0) {
            return false;
        } else {
            if (f3 > 0.0f && (b2 & 2) == 0) {
                return false;
            }
        }
        this.f2217h = 0.0f;
        this.f2216g = 0.0f;
        this.k = motionEvent.getPointerId(0);
        a(c2, 1);
        return true;
    }

    /* access modifiers changed from: package-private */
    public View a(MotionEvent motionEvent) {
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        if (this.f2211b != null) {
            View view = this.f2211b.itemView;
            if (a(view, x2, y2, this.i + this.f2216g, this.j + this.f2217h)) {
                return view;
            }
        }
        for (int size = this.o.size() - 1; size >= 0; size--) {
            c cVar = this.o.get(size);
            View view2 = cVar.f2238h.itemView;
            if (a(view2, x2, y2, cVar.l, cVar.m)) {
                return view2;
            }
        }
        return this.p.findChildViewUnder(x2, y2);
    }

    /* access modifiers changed from: package-private */
    public c b(MotionEvent motionEvent) {
        if (this.o.isEmpty()) {
            return null;
        }
        View a2 = a(motionEvent);
        for (int size = this.o.size() - 1; size >= 0; size--) {
            c cVar = this.o.get(size);
            if (cVar.f2238h.itemView == a2) {
                return cVar;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void a(MotionEvent motionEvent, int i2, int i3) {
        float x2 = motionEvent.getX(i3);
        float y2 = motionEvent.getY(i3);
        this.f2216g = x2 - this.f2212c;
        this.f2217h = y2 - this.f2213d;
        if ((i2 & 4) == 0) {
            this.f2216g = Math.max(0.0f, this.f2216g);
        }
        if ((i2 & 8) == 0) {
            this.f2216g = Math.min(0.0f, this.f2216g);
        }
        if ((i2 & 1) == 0) {
            this.f2217h = Math.max(0.0f, this.f2217h);
        }
        if ((i2 & 2) == 0) {
            this.f2217h = Math.min(0.0f, this.f2217h);
        }
    }

    private int c(RecyclerView.u uVar) {
        if (this.m == 2) {
            return 0;
        }
        int a2 = this.l.a(this.p, uVar);
        int d2 = (this.l.d(a2, ag.g(this.p)) & 65280) >> 8;
        if (d2 == 0) {
            return 0;
        }
        int i2 = (a2 & 65280) >> 8;
        if (Math.abs(this.f2216g) > Math.abs(this.f2217h)) {
            int b2 = b(uVar, d2);
            if (b2 > 0) {
                return (i2 & b2) == 0 ? a.a(b2, ag.g(this.p)) : b2;
            }
            int c2 = c(uVar, d2);
            if (c2 > 0) {
                return c2;
            }
            return 0;
        }
        int c3 = c(uVar, d2);
        if (c3 > 0) {
            return c3;
        }
        int b3 = b(uVar, d2);
        if (b3 > 0) {
            return (i2 & b3) == 0 ? a.a(b3, ag.g(this.p)) : b3;
        }
        return 0;
    }

    private int b(RecyclerView.u uVar, int i2) {
        int i3 = 8;
        if ((i2 & 12) != 0) {
            int i4 = this.f2216g > 0.0f ? 8 : 4;
            if (this.r != null && this.k > -1) {
                this.r.computeCurrentVelocity(AdError.NETWORK_ERROR_CODE, this.l.b(this.f2215f));
                float a2 = ae.a(this.r, this.k);
                float b2 = ae.b(this.r, this.k);
                if (a2 <= 0.0f) {
                    i3 = 4;
                }
                float abs = Math.abs(a2);
                if ((i3 & i2) != 0 && i4 == i3 && abs >= this.l.a(this.f2214e) && abs > Math.abs(b2)) {
                    return i3;
                }
            }
            float width = ((float) this.p.getWidth()) * this.l.a(uVar);
            if ((i2 & i4) != 0 && Math.abs(this.f2216g) > width) {
                return i4;
            }
        }
        return 0;
    }

    private int c(RecyclerView.u uVar, int i2) {
        int i3 = 2;
        if ((i2 & 3) != 0) {
            int i4 = this.f2217h > 0.0f ? 2 : 1;
            if (this.r != null && this.k > -1) {
                this.r.computeCurrentVelocity(AdError.NETWORK_ERROR_CODE, this.l.b(this.f2215f));
                float a2 = ae.a(this.r, this.k);
                float b2 = ae.b(this.r, this.k);
                if (b2 <= 0.0f) {
                    i3 = 1;
                }
                float abs = Math.abs(b2);
                if ((i3 & i2) != 0 && i3 == i4 && abs >= this.l.a(this.f2214e) && abs > Math.abs(a2)) {
                    return i3;
                }
            }
            float height = ((float) this.p.getHeight()) * this.l.a(uVar);
            if ((i2 & i4) != 0 && Math.abs(this.f2217h) > height) {
                return i4;
            }
        }
        return 0;
    }

    private void h() {
        if (Build.VERSION.SDK_INT < 21) {
            if (this.z == null) {
                this.z = new RecyclerView.d() {
                    public int a(int i, int i2) {
                        if (ItemTouchHelper.this.s == null) {
                            return i2;
                        }
                        int i3 = ItemTouchHelper.this.t;
                        if (i3 == -1) {
                            i3 = ItemTouchHelper.this.p.indexOfChild(ItemTouchHelper.this.s);
                            ItemTouchHelper.this.t = i3;
                        }
                        if (i2 == i - 1) {
                            return i3;
                        }
                        return i2 >= i3 ? i2 + 1 : i2;
                    }
                };
            }
            this.p.setChildDrawingOrderCallback(this.z);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(View view) {
        if (view == this.s) {
            this.s = null;
            if (this.z != null) {
                this.p.setChildDrawingOrderCallback(null);
            }
        }
    }

    public static abstract class a {

        /* renamed from: a  reason: collision with root package name */
        private static final a f2227a;

        /* renamed from: b  reason: collision with root package name */
        private static final Interpolator f2228b = new Interpolator() {
            public float getInterpolation(float f2) {
                return f2 * f2 * f2 * f2 * f2;
            }
        };

        /* renamed from: c  reason: collision with root package name */
        private static final Interpolator f2229c = new Interpolator() {
            public float getInterpolation(float f2) {
                float f3 = f2 - 1.0f;
                return (f3 * f3 * f3 * f3 * f3) + 1.0f;
            }
        };

        /* renamed from: d  reason: collision with root package name */
        private int f2230d = -1;

        public abstract int a(RecyclerView recyclerView, RecyclerView.u uVar);

        public abstract void a(RecyclerView.u uVar, int i);

        public abstract boolean b(RecyclerView recyclerView, RecyclerView.u uVar, RecyclerView.u uVar2);

        static {
            if (Build.VERSION.SDK_INT >= 21) {
                f2227a = new b.c();
            } else if (Build.VERSION.SDK_INT >= 11) {
                f2227a = new b.C0031b();
            } else {
                f2227a = new b.a();
            }
        }

        public static int a(int i, int i2) {
            int i3 = i & 789516;
            if (i3 == 0) {
                return i;
            }
            int i4 = (i3 ^ -1) & i;
            if (i2 == 0) {
                return i4 | (i3 << 2);
            }
            return i4 | ((i3 << 1) & -789517) | (((i3 << 1) & 789516) << 2);
        }

        public static int b(int i, int i2) {
            return c(0, i2 | i) | c(1, i2) | c(2, i);
        }

        public static int c(int i, int i2) {
            return i2 << (i * 8);
        }

        public int d(int i, int i2) {
            int i3 = i & 3158064;
            if (i3 == 0) {
                return i;
            }
            int i4 = (i3 ^ -1) & i;
            if (i2 == 0) {
                return i4 | (i3 >> 2);
            }
            return i4 | ((i3 >> 1) & -3158065) | (((i3 >> 1) & 3158064) >> 2);
        }

        /* access modifiers changed from: package-private */
        public final int b(RecyclerView recyclerView, RecyclerView.u uVar) {
            return d(a(recyclerView, uVar), ag.g(recyclerView));
        }

        /* access modifiers changed from: package-private */
        public boolean c(RecyclerView recyclerView, RecyclerView.u uVar) {
            return (b(recyclerView, uVar) & 16711680) != 0;
        }

        public boolean a(RecyclerView recyclerView, RecyclerView.u uVar, RecyclerView.u uVar2) {
            return true;
        }

        public boolean a() {
            return true;
        }

        public boolean b() {
            return true;
        }

        public int c() {
            return 0;
        }

        public float a(RecyclerView.u uVar) {
            return 0.5f;
        }

        public float b(RecyclerView.u uVar) {
            return 0.5f;
        }

        public float a(float f2) {
            return f2;
        }

        public float b(float f2) {
            return f2;
        }

        public RecyclerView.u a(RecyclerView.u uVar, List<RecyclerView.u> list, int i, int i2) {
            RecyclerView.u uVar2;
            int i3;
            int i4;
            int i5;
            int i6;
            RecyclerView.u uVar3;
            int bottom;
            int abs;
            int top;
            int left;
            int right;
            int abs2;
            int width = i + uVar.itemView.getWidth();
            int height = i2 + uVar.itemView.getHeight();
            RecyclerView.u uVar4 = null;
            int i7 = -1;
            int left2 = i - uVar.itemView.getLeft();
            int top2 = i2 - uVar.itemView.getTop();
            int size = list.size();
            int i8 = 0;
            while (i8 < size) {
                RecyclerView.u uVar5 = list.get(i8);
                if (left2 <= 0 || (right = uVar5.itemView.getRight() - width) >= 0 || uVar5.itemView.getRight() <= uVar.itemView.getRight() || (abs2 = Math.abs(right)) <= i7) {
                    uVar2 = uVar4;
                    i3 = i7;
                } else {
                    i3 = abs2;
                    uVar2 = uVar5;
                }
                if (left2 >= 0 || (left = uVar5.itemView.getLeft() - i) <= 0 || uVar5.itemView.getLeft() >= uVar.itemView.getLeft() || (i4 = Math.abs(left)) <= i3) {
                    i4 = i3;
                } else {
                    uVar2 = uVar5;
                }
                if (top2 >= 0 || (top = uVar5.itemView.getTop() - i2) <= 0 || uVar5.itemView.getTop() >= uVar.itemView.getTop() || (i5 = Math.abs(top)) <= i4) {
                    i5 = i4;
                } else {
                    uVar2 = uVar5;
                }
                if (top2 <= 0 || (bottom = uVar5.itemView.getBottom() - height) >= 0 || uVar5.itemView.getBottom() <= uVar.itemView.getBottom() || (abs = Math.abs(bottom)) <= i5) {
                    i6 = i5;
                    uVar3 = uVar2;
                } else {
                    int i9 = abs;
                    uVar3 = uVar5;
                    i6 = i9;
                }
                i8++;
                uVar4 = uVar3;
                i7 = i6;
            }
            return uVar4;
        }

        public void b(RecyclerView.u uVar, int i) {
            if (uVar != null) {
                f2227a.b(uVar.itemView);
            }
        }

        private int a(RecyclerView recyclerView) {
            if (this.f2230d == -1) {
                this.f2230d = recyclerView.getResources().getDimensionPixelSize(a.C0029a.item_touch_helper_max_drag_scroll_per_frame);
            }
            return this.f2230d;
        }

        public void a(RecyclerView recyclerView, RecyclerView.u uVar, int i, RecyclerView.u uVar2, int i2, int i3, int i4) {
            RecyclerView.h layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof d) {
                ((d) layoutManager).a(uVar.itemView, uVar2.itemView, i3, i4);
                return;
            }
            if (layoutManager.d()) {
                if (layoutManager.h(uVar2.itemView) <= recyclerView.getPaddingLeft()) {
                    recyclerView.scrollToPosition(i2);
                }
                if (layoutManager.j(uVar2.itemView) >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                    recyclerView.scrollToPosition(i2);
                }
            }
            if (layoutManager.e()) {
                if (layoutManager.i(uVar2.itemView) <= recyclerView.getPaddingTop()) {
                    recyclerView.scrollToPosition(i2);
                }
                if (layoutManager.k(uVar2.itemView) >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                    recyclerView.scrollToPosition(i2);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.helper.ItemTouchHelper.a.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$u, float, float, int, boolean):void
         arg types: [android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$u, float, float, int, int]
         candidates:
          android.support.v7.widget.helper.ItemTouchHelper.a.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$u, java.util.List<android.support.v7.widget.helper.ItemTouchHelper$c>, int, float, float):void
          android.support.v7.widget.helper.ItemTouchHelper.a.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$u, int, android.support.v7.widget.RecyclerView$u, int, int, int):void
          android.support.v7.widget.helper.ItemTouchHelper.a.a(android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$u, float, float, int, boolean):void */
        /* access modifiers changed from: package-private */
        public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.u uVar, List<c> list, int i, float f2, float f3) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                c cVar = list.get(i2);
                cVar.c();
                int save = canvas.save();
                a(canvas, recyclerView, cVar.f2238h, cVar.l, cVar.m, cVar.i, false);
                canvas.restoreToCount(save);
            }
            if (uVar != null) {
                int save2 = canvas.save();
                a(canvas, recyclerView, uVar, f2, f3, i, true);
                canvas.restoreToCount(save2);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.helper.ItemTouchHelper.a.b(android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$u, float, float, int, boolean):void
         arg types: [android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$u, float, float, int, int]
         candidates:
          android.support.v7.widget.helper.ItemTouchHelper.a.b(android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$u, java.util.List<android.support.v7.widget.helper.ItemTouchHelper$c>, int, float, float):void
          android.support.v7.widget.helper.ItemTouchHelper.a.b(android.graphics.Canvas, android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$u, float, float, int, boolean):void */
        /* access modifiers changed from: package-private */
        public void b(Canvas canvas, RecyclerView recyclerView, RecyclerView.u uVar, List<c> list, int i, float f2, float f3) {
            boolean z;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                c cVar = list.get(i2);
                int save = canvas.save();
                b(canvas, recyclerView, cVar.f2238h, cVar.l, cVar.m, cVar.i, false);
                canvas.restoreToCount(save);
            }
            if (uVar != null) {
                int save2 = canvas.save();
                b(canvas, recyclerView, uVar, f2, f3, i, true);
                canvas.restoreToCount(save2);
            }
            boolean z2 = false;
            int i3 = size - 1;
            while (i3 >= 0) {
                c cVar2 = list.get(i3);
                if (cVar2.o && !cVar2.k) {
                    list.remove(i3);
                    z = z2;
                } else if (!cVar2.o) {
                    z = true;
                } else {
                    z = z2;
                }
                i3--;
                z2 = z;
            }
            if (z2) {
                recyclerView.invalidate();
            }
        }

        public void d(RecyclerView recyclerView, RecyclerView.u uVar) {
            f2227a.a(uVar.itemView);
        }

        public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.u uVar, float f2, float f3, int i, boolean z) {
            f2227a.a(canvas, recyclerView, uVar.itemView, f2, f3, i, z);
        }

        public void b(Canvas canvas, RecyclerView recyclerView, RecyclerView.u uVar, float f2, float f3, int i, boolean z) {
            f2227a.b(canvas, recyclerView, uVar.itemView, f2, f3, i, z);
        }

        public long a(RecyclerView recyclerView, int i, float f2, float f3) {
            RecyclerView.e itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator == null) {
                return i == 8 ? 200 : 250;
            }
            if (i == 8) {
                return itemAnimator.e();
            }
            return itemAnimator.g();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        public int a(RecyclerView recyclerView, int i, int i2, int i3, long j) {
            float f2 = 1.0f;
            int a2 = (int) (((float) (a(recyclerView) * ((int) Math.signum((float) i2)))) * f2229c.getInterpolation(Math.min(1.0f, (((float) Math.abs(i2)) * 1.0f) / ((float) i))));
            if (j <= 2000) {
                f2 = ((float) j) / 2000.0f;
            }
            int interpolation = (int) (f2228b.getInterpolation(f2) * ((float) a2));
            if (interpolation == 0) {
                return i2 > 0 ? 1 : -1;
            }
            return interpolation;
        }
    }

    private class b extends GestureDetector.SimpleOnGestureListener {
        b() {
        }

        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        public void onLongPress(MotionEvent motionEvent) {
            RecyclerView.u childViewHolder;
            View a2 = ItemTouchHelper.this.a(motionEvent);
            if (a2 != null && (childViewHolder = ItemTouchHelper.this.p.getChildViewHolder(a2)) != null && ItemTouchHelper.this.l.c(ItemTouchHelper.this.p, childViewHolder) && motionEvent.getPointerId(0) == ItemTouchHelper.this.k) {
                int findPointerIndex = motionEvent.findPointerIndex(ItemTouchHelper.this.k);
                float x = motionEvent.getX(findPointerIndex);
                float y = motionEvent.getY(findPointerIndex);
                ItemTouchHelper.this.f2212c = x;
                ItemTouchHelper.this.f2213d = y;
                ItemTouchHelper itemTouchHelper = ItemTouchHelper.this;
                ItemTouchHelper.this.f2217h = 0.0f;
                itemTouchHelper.f2216g = 0.0f;
                if (ItemTouchHelper.this.l.a()) {
                    ItemTouchHelper.this.a(childViewHolder, 2);
                }
            }
        }
    }

    private class c implements android.support.v4.a.b {

        /* renamed from: a  reason: collision with root package name */
        private final g f2232a;

        /* renamed from: b  reason: collision with root package name */
        private float f2233b;

        /* renamed from: d  reason: collision with root package name */
        final float f2234d;

        /* renamed from: e  reason: collision with root package name */
        final float f2235e;

        /* renamed from: f  reason: collision with root package name */
        final float f2236f;

        /* renamed from: g  reason: collision with root package name */
        final float f2237g;

        /* renamed from: h  reason: collision with root package name */
        final RecyclerView.u f2238h;
        final int i;
        final int j;
        public boolean k;
        float l;
        float m;
        boolean n = false;
        boolean o = false;

        public c(RecyclerView.u uVar, int i2, int i3, float f2, float f3, float f4, float f5) {
            this.i = i3;
            this.j = i2;
            this.f2238h = uVar;
            this.f2234d = f2;
            this.f2235e = f3;
            this.f2236f = f4;
            this.f2237g = f5;
            this.f2232a = android.support.v4.a.a.a();
            this.f2232a.a(new android.support.v4.a.d(ItemTouchHelper.this) {
                public void a(g gVar) {
                    c.this.a(gVar.c());
                }
            });
            this.f2232a.a(uVar.itemView);
            this.f2232a.a(this);
            a(0.0f);
        }

        public void a(long j2) {
            this.f2232a.a(j2);
        }

        public void a() {
            this.f2238h.setIsRecyclable(false);
            this.f2232a.a();
        }

        public void b() {
            this.f2232a.b();
        }

        public void a(float f2) {
            this.f2233b = f2;
        }

        public void c() {
            if (this.f2234d == this.f2236f) {
                this.l = ag.m(this.f2238h.itemView);
            } else {
                this.l = this.f2234d + (this.f2233b * (this.f2236f - this.f2234d));
            }
            if (this.f2235e == this.f2237g) {
                this.m = ag.n(this.f2238h.itemView);
            } else {
                this.m = this.f2235e + (this.f2233b * (this.f2237g - this.f2235e));
            }
        }

        public void a(g gVar) {
        }

        public void b(g gVar) {
            if (!this.o) {
                this.f2238h.setIsRecyclable(true);
            }
            this.o = true;
        }

        public void c(g gVar) {
            a(1.0f);
        }

        public void d(g gVar) {
        }
    }
}
