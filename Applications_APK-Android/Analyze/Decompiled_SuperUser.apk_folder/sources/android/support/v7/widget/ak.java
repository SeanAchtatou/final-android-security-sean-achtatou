package android.support.v7.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: TintContextWrapper */
public class ak extends ContextWrapper {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f2134a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static ArrayList<WeakReference<ak>> f2135b;

    /* renamed from: c  reason: collision with root package name */
    private final Resources f2136c;

    /* renamed from: d  reason: collision with root package name */
    private final Resources.Theme f2137d;

    public static Context a(Context context) {
        if (!b(context)) {
            return context;
        }
        synchronized (f2134a) {
            if (f2135b == null) {
                f2135b = new ArrayList<>();
            } else {
                for (int size = f2135b.size() - 1; size >= 0; size--) {
                    WeakReference weakReference = f2135b.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        f2135b.remove(size);
                    }
                }
                for (int size2 = f2135b.size() - 1; size2 >= 0; size2--) {
                    WeakReference weakReference2 = f2135b.get(size2);
                    ak akVar = weakReference2 != null ? (ak) weakReference2.get() : null;
                    if (akVar != null && akVar.getBaseContext() == context) {
                        return akVar;
                    }
                }
            }
            ak akVar2 = new ak(context);
            f2135b.add(new WeakReference(akVar2));
            return akVar2;
        }
    }

    private static boolean b(Context context) {
        if ((context instanceof ak) || (context.getResources() instanceof am) || (context.getResources() instanceof ao)) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 21 || ao.a()) {
            return true;
        }
        return false;
    }

    private ak(Context context) {
        super(context);
        if (ao.a()) {
            this.f2136c = new ao(this, context.getResources());
            this.f2137d = this.f2136c.newTheme();
            this.f2137d.setTo(context.getTheme());
            return;
        }
        this.f2136c = new am(this, context.getResources());
        this.f2137d = null;
    }

    public Resources.Theme getTheme() {
        return this.f2137d == null ? super.getTheme() : this.f2137d;
    }

    public void setTheme(int i) {
        if (this.f2137d == null) {
            super.setTheme(i);
        } else {
            this.f2137d.applyStyle(i, true);
        }
    }

    public Resources getResources() {
        return this.f2136c;
    }

    public AssetManager getAssets() {
        return this.f2136c.getAssets();
    }
}
