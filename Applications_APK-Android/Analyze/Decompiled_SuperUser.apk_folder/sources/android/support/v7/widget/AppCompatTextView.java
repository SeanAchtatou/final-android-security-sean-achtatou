package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ad;
import android.util.AttributeSet;
import android.widget.TextView;

public class AppCompatTextView extends TextView implements ad {

    /* renamed from: a  reason: collision with root package name */
    private e f1703a;

    /* renamed from: b  reason: collision with root package name */
    private k f1704b;

    public AppCompatTextView(Context context) {
        this(context, null);
    }

    public AppCompatTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842884);
    }

    public AppCompatTextView(Context context, AttributeSet attributeSet, int i) {
        super(ak.a(context), attributeSet, i);
        this.f1703a = new e(this);
        this.f1703a.a(attributeSet, i);
        this.f1704b = k.a(this);
        this.f1704b.a(attributeSet, i);
        this.f1704b.a();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1703a != null) {
            this.f1703a.a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1703a != null) {
            this.f1703a.a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1703a != null) {
            this.f1703a.a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1703a != null) {
            return this.f1703a.a();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.f1703a != null) {
            this.f1703a.a(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (this.f1703a != null) {
            return this.f1703a.b();
        }
        return null;
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f1704b != null) {
            this.f1704b.a(context, i);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1703a != null) {
            this.f1703a.c();
        }
        if (this.f1704b != null) {
            this.f1704b.a();
        }
    }
}
