package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

/* compiled from: TintResources */
class am extends ad {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<Context> f2142a;

    public am(Context context, Resources resources) {
        super(resources);
        this.f2142a = new WeakReference<>(context);
    }

    public Drawable getDrawable(int i) {
        Drawable drawable = super.getDrawable(i);
        Context context = this.f2142a.get();
        if (!(drawable == null || context == null)) {
            g.a();
            g.a(context, i, drawable);
        }
        return drawable;
    }
}
