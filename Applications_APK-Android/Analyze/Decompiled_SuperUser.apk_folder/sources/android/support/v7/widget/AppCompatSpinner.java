package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ad;
import android.support.v4.view.ag;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.ThemedSpinnerAdapter;

public class AppCompatSpinner extends Spinner implements ad {

    /* renamed from: d  reason: collision with root package name */
    private static final int[] f1683d = {16843505};

    /* renamed from: a  reason: collision with root package name */
    b f1684a;

    /* renamed from: b  reason: collision with root package name */
    int f1685b;

    /* renamed from: c  reason: collision with root package name */
    final Rect f1686c;

    /* renamed from: e  reason: collision with root package name */
    private e f1687e;

    /* renamed from: f  reason: collision with root package name */
    private Context f1688f;

    /* renamed from: g  reason: collision with root package name */
    private x f1689g;

    /* renamed from: h  reason: collision with root package name */
    private SpinnerAdapter f1690h;
    private boolean i;

    public AppCompatSpinner(Context context) {
        this(context, null);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0027a.spinnerStyle);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, -1);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet, int i2, int i3) {
        this(context, attributeSet, i2, i3, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00e1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatSpinner(android.content.Context r9, android.util.AttributeSet r10, int r11, int r12, android.content.res.Resources.Theme r13) {
        /*
            r8 = this;
            r1 = 0
            r3 = 1
            r7 = 0
            r8.<init>(r9, r10, r11)
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            r8.f1686c = r0
            int[] r0 = android.support.v7.a.a.k.Spinner
            android.support.v7.widget.an r4 = android.support.v7.widget.an.a(r9, r10, r0, r11, r7)
            android.support.v7.widget.e r0 = new android.support.v7.widget.e
            r0.<init>(r8)
            r8.f1687e = r0
            if (r13 == 0) goto L_0x00af
            android.support.v7.view.d r0 = new android.support.v7.view.d
            r0.<init>(r9, r13)
            r8.f1688f = r0
        L_0x0023:
            android.content.Context r0 = r8.f1688f
            if (r0 == 0) goto L_0x0081
            r0 = -1
            if (r12 != r0) goto L_0x0049
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 11
            if (r0 < r2) goto L_0x00e5
            int[] r0 = android.support.v7.widget.AppCompatSpinner.f1683d     // Catch:{ Exception -> 0x00cd, all -> 0x00dd }
            r2 = 0
            android.content.res.TypedArray r2 = r9.obtainStyledAttributes(r10, r0, r11, r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00dd }
            r0 = 0
            boolean r0 = r2.hasValue(r0)     // Catch:{ Exception -> 0x00ea }
            if (r0 == 0) goto L_0x0044
            r0 = 0
            r5 = 0
            int r12 = r2.getInt(r0, r5)     // Catch:{ Exception -> 0x00ea }
        L_0x0044:
            if (r2 == 0) goto L_0x0049
            r2.recycle()
        L_0x0049:
            if (r12 != r3) goto L_0x0081
            android.support.v7.widget.AppCompatSpinner$b r0 = new android.support.v7.widget.AppCompatSpinner$b
            android.content.Context r2 = r8.f1688f
            r0.<init>(r2, r10, r11)
            android.content.Context r2 = r8.f1688f
            int[] r5 = android.support.v7.a.a.k.Spinner
            android.support.v7.widget.an r2 = android.support.v7.widget.an.a(r2, r10, r5, r11, r7)
            int r5 = android.support.v7.a.a.k.Spinner_android_dropDownWidth
            r6 = -2
            int r5 = r2.f(r5, r6)
            r8.f1685b = r5
            int r5 = android.support.v7.a.a.k.Spinner_android_popupBackground
            android.graphics.drawable.Drawable r5 = r2.a(r5)
            r0.a(r5)
            int r5 = android.support.v7.a.a.k.Spinner_android_prompt
            java.lang.String r5 = r4.d(r5)
            r0.a(r5)
            r2.a()
            r8.f1684a = r0
            android.support.v7.widget.AppCompatSpinner$1 r2 = new android.support.v7.widget.AppCompatSpinner$1
            r2.<init>(r8, r0)
            r8.f1689g = r2
        L_0x0081:
            int r0 = android.support.v7.a.a.k.Spinner_android_entries
            java.lang.CharSequence[] r0 = r4.f(r0)
            if (r0 == 0) goto L_0x0099
            android.widget.ArrayAdapter r2 = new android.widget.ArrayAdapter
            r5 = 17367048(0x1090008, float:2.5162948E-38)
            r2.<init>(r9, r5, r0)
            int r0 = android.support.v7.a.a.h.support_simple_spinner_dropdown_item
            r2.setDropDownViewResource(r0)
            r8.setAdapter(r2)
        L_0x0099:
            r4.a()
            r8.i = r3
            android.widget.SpinnerAdapter r0 = r8.f1690h
            if (r0 == 0) goto L_0x00a9
            android.widget.SpinnerAdapter r0 = r8.f1690h
            r8.setAdapter(r0)
            r8.f1690h = r1
        L_0x00a9:
            android.support.v7.widget.e r0 = r8.f1687e
            r0.a(r10, r11)
            return
        L_0x00af:
            int r0 = android.support.v7.a.a.k.Spinner_popupTheme
            int r0 = r4.g(r0, r7)
            if (r0 == 0) goto L_0x00c0
            android.support.v7.view.d r2 = new android.support.v7.view.d
            r2.<init>(r9, r0)
            r8.f1688f = r2
            goto L_0x0023
        L_0x00c0:
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 23
            if (r0 >= r2) goto L_0x00cb
            r0 = r9
        L_0x00c7:
            r8.f1688f = r0
            goto L_0x0023
        L_0x00cb:
            r0 = r1
            goto L_0x00c7
        L_0x00cd:
            r0 = move-exception
            r2 = r1
        L_0x00cf:
            java.lang.String r5 = "AppCompatSpinner"
            java.lang.String r6 = "Could not read android:spinnerMode"
            android.util.Log.i(r5, r6, r0)     // Catch:{ all -> 0x00e8 }
            if (r2 == 0) goto L_0x0049
            r2.recycle()
            goto L_0x0049
        L_0x00dd:
            r0 = move-exception
            r2 = r1
        L_0x00df:
            if (r2 == 0) goto L_0x00e4
            r2.recycle()
        L_0x00e4:
            throw r0
        L_0x00e5:
            r12 = r3
            goto L_0x0049
        L_0x00e8:
            r0 = move-exception
            goto L_0x00df
        L_0x00ea:
            r0 = move-exception
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AppCompatSpinner.<init>(android.content.Context, android.util.AttributeSet, int, int, android.content.res.Resources$Theme):void");
    }

    public Context getPopupContext() {
        if (this.f1684a != null) {
            return this.f1688f;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            return super.getPopupContext();
        }
        return null;
    }

    public void setPopupBackgroundDrawable(Drawable drawable) {
        if (this.f1684a != null) {
            this.f1684a.a(drawable);
        } else if (Build.VERSION.SDK_INT >= 16) {
            super.setPopupBackgroundDrawable(drawable);
        }
    }

    public void setPopupBackgroundResource(int i2) {
        setPopupBackgroundDrawable(android.support.v7.c.a.b.b(getPopupContext(), i2));
    }

    public Drawable getPopupBackground() {
        if (this.f1684a != null) {
            return this.f1684a.h();
        }
        if (Build.VERSION.SDK_INT >= 16) {
            return super.getPopupBackground();
        }
        return null;
    }

    public void setDropDownVerticalOffset(int i2) {
        if (this.f1684a != null) {
            this.f1684a.d(i2);
        } else if (Build.VERSION.SDK_INT >= 16) {
            super.setDropDownVerticalOffset(i2);
        }
    }

    public int getDropDownVerticalOffset() {
        if (this.f1684a != null) {
            return this.f1684a.k();
        }
        if (Build.VERSION.SDK_INT >= 16) {
            return super.getDropDownVerticalOffset();
        }
        return 0;
    }

    public void setDropDownHorizontalOffset(int i2) {
        if (this.f1684a != null) {
            this.f1684a.c(i2);
        } else if (Build.VERSION.SDK_INT >= 16) {
            super.setDropDownHorizontalOffset(i2);
        }
    }

    public int getDropDownHorizontalOffset() {
        if (this.f1684a != null) {
            return this.f1684a.j();
        }
        if (Build.VERSION.SDK_INT >= 16) {
            return super.getDropDownHorizontalOffset();
        }
        return 0;
    }

    public void setDropDownWidth(int i2) {
        if (this.f1684a != null) {
            this.f1685b = i2;
        } else if (Build.VERSION.SDK_INT >= 16) {
            super.setDropDownWidth(i2);
        }
    }

    public int getDropDownWidth() {
        if (this.f1684a != null) {
            return this.f1685b;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            return super.getDropDownWidth();
        }
        return 0;
    }

    public void setAdapter(SpinnerAdapter spinnerAdapter) {
        if (!this.i) {
            this.f1690h = spinnerAdapter;
            return;
        }
        super.setAdapter(spinnerAdapter);
        if (this.f1684a != null) {
            this.f1684a.a(new a(spinnerAdapter, (this.f1688f == null ? getContext() : this.f1688f).getTheme()));
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f1684a != null && this.f1684a.c()) {
            this.f1684a.b();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f1689g == null || !this.f1689g.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.f1684a != null && View.MeasureSpec.getMode(i2) == Integer.MIN_VALUE) {
            setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), a(getAdapter(), getBackground())), View.MeasureSpec.getSize(i2)), getMeasuredHeight());
        }
    }

    public boolean performClick() {
        if (this.f1684a == null) {
            return super.performClick();
        }
        if (!this.f1684a.c()) {
            this.f1684a.a();
        }
        return true;
    }

    public void setPrompt(CharSequence charSequence) {
        if (this.f1684a != null) {
            this.f1684a.a(charSequence);
        } else {
            super.setPrompt(charSequence);
        }
    }

    public CharSequence getPrompt() {
        return this.f1684a != null ? this.f1684a.e() : super.getPrompt();
    }

    public void setBackgroundResource(int i2) {
        super.setBackgroundResource(i2);
        if (this.f1687e != null) {
            this.f1687e.a(i2);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1687e != null) {
            this.f1687e.a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1687e != null) {
            this.f1687e.a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1687e != null) {
            return this.f1687e.a();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.f1687e != null) {
            this.f1687e.a(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (this.f1687e != null) {
            return this.f1687e.b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1687e != null) {
            this.f1687e.c();
        }
    }

    /* access modifiers changed from: package-private */
    public int a(SpinnerAdapter spinnerAdapter, Drawable drawable) {
        View view;
        if (spinnerAdapter == null) {
            return 0;
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 0);
        int max = Math.max(0, getSelectedItemPosition());
        int min = Math.min(spinnerAdapter.getCount(), max + 15);
        int max2 = Math.max(0, max - (15 - (min - max)));
        View view2 = null;
        int i2 = 0;
        int i3 = 0;
        while (max2 < min) {
            int itemViewType = spinnerAdapter.getItemViewType(max2);
            if (itemViewType != i3) {
                view = null;
            } else {
                itemViewType = i3;
                view = view2;
            }
            view2 = spinnerAdapter.getView(max2, view, this);
            if (view2.getLayoutParams() == null) {
                view2.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            }
            view2.measure(makeMeasureSpec, makeMeasureSpec2);
            i2 = Math.max(i2, view2.getMeasuredWidth());
            max2++;
            i3 = itemViewType;
        }
        if (drawable == null) {
            return i2;
        }
        drawable.getPadding(this.f1686c);
        return this.f1686c.left + this.f1686c.right + i2;
    }

    private static class a implements ListAdapter, SpinnerAdapter {

        /* renamed from: a  reason: collision with root package name */
        private SpinnerAdapter f1693a;

        /* renamed from: b  reason: collision with root package name */
        private ListAdapter f1694b;

        public a(SpinnerAdapter spinnerAdapter, Resources.Theme theme) {
            this.f1693a = spinnerAdapter;
            if (spinnerAdapter instanceof ListAdapter) {
                this.f1694b = (ListAdapter) spinnerAdapter;
            }
            if (theme == null) {
                return;
            }
            if (Build.VERSION.SDK_INT >= 23 && (spinnerAdapter instanceof ThemedSpinnerAdapter)) {
                ThemedSpinnerAdapter themedSpinnerAdapter = (ThemedSpinnerAdapter) spinnerAdapter;
                if (themedSpinnerAdapter.getDropDownViewTheme() != theme) {
                    themedSpinnerAdapter.setDropDownViewTheme(theme);
                }
            } else if (spinnerAdapter instanceof ThemedSpinnerAdapter) {
                ThemedSpinnerAdapter themedSpinnerAdapter2 = (ThemedSpinnerAdapter) spinnerAdapter;
                if (themedSpinnerAdapter2.a() == null) {
                    themedSpinnerAdapter2.a(theme);
                }
            }
        }

        public int getCount() {
            if (this.f1693a == null) {
                return 0;
            }
            return this.f1693a.getCount();
        }

        public Object getItem(int i) {
            if (this.f1693a == null) {
                return null;
            }
            return this.f1693a.getItem(i);
        }

        public long getItemId(int i) {
            if (this.f1693a == null) {
                return -1;
            }
            return this.f1693a.getItemId(i);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            return getDropDownView(i, view, viewGroup);
        }

        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            if (this.f1693a == null) {
                return null;
            }
            return this.f1693a.getDropDownView(i, view, viewGroup);
        }

        public boolean hasStableIds() {
            return this.f1693a != null && this.f1693a.hasStableIds();
        }

        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
            if (this.f1693a != null) {
                this.f1693a.registerDataSetObserver(dataSetObserver);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
            if (this.f1693a != null) {
                this.f1693a.unregisterDataSetObserver(dataSetObserver);
            }
        }

        public boolean areAllItemsEnabled() {
            ListAdapter listAdapter = this.f1694b;
            if (listAdapter != null) {
                return listAdapter.areAllItemsEnabled();
            }
            return true;
        }

        public boolean isEnabled(int i) {
            ListAdapter listAdapter = this.f1694b;
            if (listAdapter != null) {
                return listAdapter.isEnabled(i);
            }
            return true;
        }

        public int getItemViewType(int i) {
            return 0;
        }

        public int getViewTypeCount() {
            return 1;
        }

        public boolean isEmpty() {
            return getCount() == 0;
        }
    }

    private class b extends ListPopupWindow {

        /* renamed from: a  reason: collision with root package name */
        ListAdapter f1695a;

        /* renamed from: h  reason: collision with root package name */
        private CharSequence f1697h;
        private final Rect i = new Rect();

        public b(Context context, AttributeSet attributeSet, int i2) {
            super(context, attributeSet, i2);
            b(AppCompatSpinner.this);
            a(true);
            a(0);
            a(new AdapterView.OnItemClickListener(AppCompatSpinner.this) {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    AppCompatSpinner.this.setSelection(i);
                    if (AppCompatSpinner.this.getOnItemClickListener() != null) {
                        AppCompatSpinner.this.performItemClick(view, i, b.this.f1695a.getItemId(i));
                    }
                    b.this.b();
                }
            });
        }

        public void a(ListAdapter listAdapter) {
            super.a(listAdapter);
            this.f1695a = listAdapter;
        }

        public CharSequence e() {
            return this.f1697h;
        }

        public void a(CharSequence charSequence) {
            this.f1697h = charSequence;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            int i2;
            int i3;
            Drawable h2 = h();
            if (h2 != null) {
                h2.getPadding(AppCompatSpinner.this.f1686c);
                i2 = ar.a(AppCompatSpinner.this) ? AppCompatSpinner.this.f1686c.right : -AppCompatSpinner.this.f1686c.left;
            } else {
                Rect rect = AppCompatSpinner.this.f1686c;
                AppCompatSpinner.this.f1686c.right = 0;
                rect.left = 0;
                i2 = 0;
            }
            int paddingLeft = AppCompatSpinner.this.getPaddingLeft();
            int paddingRight = AppCompatSpinner.this.getPaddingRight();
            int width = AppCompatSpinner.this.getWidth();
            if (AppCompatSpinner.this.f1685b == -2) {
                int a2 = AppCompatSpinner.this.a((SpinnerAdapter) this.f1695a, h());
                int i4 = (AppCompatSpinner.this.getContext().getResources().getDisplayMetrics().widthPixels - AppCompatSpinner.this.f1686c.left) - AppCompatSpinner.this.f1686c.right;
                if (a2 <= i4) {
                    i4 = a2;
                }
                g(Math.max(i4, (width - paddingLeft) - paddingRight));
            } else if (AppCompatSpinner.this.f1685b == -1) {
                g((width - paddingLeft) - paddingRight);
            } else {
                g(AppCompatSpinner.this.f1685b);
            }
            if (ar.a(AppCompatSpinner.this)) {
                i3 = ((width - paddingRight) - l()) + i2;
            } else {
                i3 = i2 + paddingLeft;
            }
            c(i3);
        }

        public void a() {
            ViewTreeObserver viewTreeObserver;
            boolean c2 = c();
            f();
            h(2);
            super.a();
            d().setChoiceMode(1);
            i(AppCompatSpinner.this.getSelectedItemPosition());
            if (!c2 && (viewTreeObserver = AppCompatSpinner.this.getViewTreeObserver()) != null) {
                final AnonymousClass2 r1 = new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        if (!b.this.a(AppCompatSpinner.this)) {
                            b.this.b();
                            return;
                        }
                        b.this.f();
                        b.super.a();
                    }
                };
                viewTreeObserver.addOnGlobalLayoutListener(r1);
                a(new PopupWindow.OnDismissListener() {
                    public void onDismiss() {
                        ViewTreeObserver viewTreeObserver = AppCompatSpinner.this.getViewTreeObserver();
                        if (viewTreeObserver != null) {
                            viewTreeObserver.removeGlobalOnLayoutListener(r1);
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view) {
            return ag.H(view) && view.getGlobalVisibleRect(this.i);
        }
    }
}
