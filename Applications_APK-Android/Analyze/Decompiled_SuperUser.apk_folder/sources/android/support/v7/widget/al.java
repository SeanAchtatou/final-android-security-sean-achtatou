package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* compiled from: TintInfo */
class al {

    /* renamed from: a  reason: collision with root package name */
    public ColorStateList f2138a;

    /* renamed from: b  reason: collision with root package name */
    public PorterDuff.Mode f2139b;

    /* renamed from: c  reason: collision with root package name */
    public boolean f2140c;

    /* renamed from: d  reason: collision with root package name */
    public boolean f2141d;

    al() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f2138a = null;
        this.f2141d = false;
        this.f2139b = null;
        this.f2140c = false;
    }
}
