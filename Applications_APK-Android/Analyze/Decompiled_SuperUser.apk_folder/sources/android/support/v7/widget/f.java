package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.widget.c;
import android.support.v7.a.a;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.CompoundButton;

/* compiled from: AppCompatCompoundButtonHelper */
class f {

    /* renamed from: a  reason: collision with root package name */
    private final CompoundButton f2195a;

    /* renamed from: b  reason: collision with root package name */
    private ColorStateList f2196b = null;

    /* renamed from: c  reason: collision with root package name */
    private PorterDuff.Mode f2197c = null;

    /* renamed from: d  reason: collision with root package name */
    private boolean f2198d = false;

    /* renamed from: e  reason: collision with root package name */
    private boolean f2199e = false;

    /* renamed from: f  reason: collision with root package name */
    private boolean f2200f;

    f(CompoundButton compoundButton) {
        this.f2195a = compoundButton;
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        int resourceId;
        TypedArray obtainStyledAttributes = this.f2195a.getContext().obtainStyledAttributes(attributeSet, a.k.CompoundButton, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(a.k.CompoundButton_android_button) && (resourceId = obtainStyledAttributes.getResourceId(a.k.CompoundButton_android_button, 0)) != 0) {
                this.f2195a.setButtonDrawable(b.b(this.f2195a.getContext(), resourceId));
            }
            if (obtainStyledAttributes.hasValue(a.k.CompoundButton_buttonTint)) {
                c.a(this.f2195a, obtainStyledAttributes.getColorStateList(a.k.CompoundButton_buttonTint));
            }
            if (obtainStyledAttributes.hasValue(a.k.CompoundButton_buttonTintMode)) {
                c.a(this.f2195a, u.a(obtainStyledAttributes.getInt(a.k.CompoundButton_buttonTintMode, -1), null));
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        this.f2196b = colorStateList;
        this.f2198d = true;
        d();
    }

    /* access modifiers changed from: package-private */
    public ColorStateList a() {
        return this.f2196b;
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        this.f2197c = mode;
        this.f2199e = true;
        d();
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode b() {
        return this.f2197c;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f2200f) {
            this.f2200f = false;
            return;
        }
        this.f2200f = true;
        d();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        Drawable a2 = c.a(this.f2195a);
        if (a2 == null) {
            return;
        }
        if (this.f2198d || this.f2199e) {
            Drawable mutate = android.support.v4.b.a.a.g(a2).mutate();
            if (this.f2198d) {
                android.support.v4.b.a.a.a(mutate, this.f2196b);
            }
            if (this.f2199e) {
                android.support.v4.b.a.a.a(mutate, this.f2197c);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.f2195a.getDrawableState());
            }
            this.f2195a.setButtonDrawable(mutate);
        }
    }

    /* access modifiers changed from: package-private */
    public int a(int i) {
        Drawable a2;
        if (Build.VERSION.SDK_INT >= 17 || (a2 = c.a(this.f2195a)) == null) {
            return i;
        }
        return i + a2.getIntrinsicWidth();
    }
}
