package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.lang.reflect.Field;

public class ListViewCompat extends ListView {

    /* renamed from: g  reason: collision with root package name */
    private static final int[] f1838g = {0};

    /* renamed from: a  reason: collision with root package name */
    final Rect f1839a;

    /* renamed from: b  reason: collision with root package name */
    int f1840b;

    /* renamed from: c  reason: collision with root package name */
    int f1841c;

    /* renamed from: d  reason: collision with root package name */
    int f1842d;

    /* renamed from: e  reason: collision with root package name */
    int f1843e;

    /* renamed from: f  reason: collision with root package name */
    protected int f1844f;

    /* renamed from: h  reason: collision with root package name */
    private Field f1845h;
    private a i;

    public ListViewCompat(Context context) {
        this(context, null);
    }

    public ListViewCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ListViewCompat(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1839a = new Rect();
        this.f1840b = 0;
        this.f1841c = 0;
        this.f1842d = 0;
        this.f1843e = 0;
        try {
            this.f1845h = AbsListView.class.getDeclaredField("mIsChildViewEnabled");
            this.f1845h.setAccessible(true);
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        }
    }

    public void setSelector(Drawable drawable) {
        this.i = drawable != null ? new a(drawable) : null;
        super.setSelector(this.i);
        Rect rect = new Rect();
        if (drawable != null) {
            drawable.getPadding(rect);
        }
        this.f1840b = rect.left;
        this.f1841c = rect.top;
        this.f1842d = rect.right;
        this.f1843e = rect.bottom;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        setSelectorEnabled(true);
        b();
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        a(canvas);
        super.dispatchDraw(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f1844f = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void b() {
        Drawable selector = getSelector();
        if (selector != null && c()) {
            selector.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return a() && isPressed();
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        Drawable selector;
        if (!this.f1839a.isEmpty() && (selector = getSelector()) != null) {
            selector.setBounds(this.f1839a);
            selector.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, View view, float f2, float f3) {
        a(i2, view);
        Drawable selector = getSelector();
        if (selector != null && i2 != -1) {
            android.support.v4.b.a.a.a(selector, f2, f3);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, View view) {
        boolean z = true;
        Drawable selector = getSelector();
        boolean z2 = (selector == null || i2 == -1) ? false : true;
        if (z2) {
            selector.setVisible(false, false);
        }
        b(i2, view);
        if (z2) {
            Rect rect = this.f1839a;
            float exactCenterX = rect.exactCenterX();
            float exactCenterY = rect.exactCenterY();
            if (getVisibility() != 0) {
                z = false;
            }
            selector.setVisible(z, false);
            android.support.v4.b.a.a.a(selector, exactCenterX, exactCenterY);
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i2, View view) {
        Rect rect = this.f1839a;
        rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        rect.left -= this.f1840b;
        rect.top -= this.f1841c;
        rect.right += this.f1842d;
        rect.bottom += this.f1843e;
        try {
            boolean z = this.f1845h.getBoolean(this);
            if (view.isEnabled() != z) {
                this.f1845h.set(this, Boolean.valueOf(!z));
                if (i2 != -1) {
                    refreshDrawableState();
                }
            }
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }

    public int a(int i2, int i3, int i4, int i5, int i6) {
        View view;
        int makeMeasureSpec;
        int i7;
        int listPaddingTop = getListPaddingTop();
        int listPaddingBottom = getListPaddingBottom();
        getListPaddingLeft();
        getListPaddingRight();
        int dividerHeight = getDividerHeight();
        Drawable divider = getDivider();
        ListAdapter adapter = getAdapter();
        if (adapter == null) {
            return listPaddingTop + listPaddingBottom;
        }
        int i8 = listPaddingBottom + listPaddingTop;
        if (dividerHeight <= 0 || divider == null) {
            dividerHeight = 0;
        }
        int i9 = 0;
        View view2 = null;
        int i10 = 0;
        int count = adapter.getCount();
        int i11 = 0;
        while (i11 < count) {
            int itemViewType = adapter.getItemViewType(i11);
            if (itemViewType != i10) {
                int i12 = itemViewType;
                view = null;
                i10 = i12;
            } else {
                view = view2;
            }
            view2 = adapter.getView(i11, view, this);
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = generateDefaultLayoutParams();
                view2.setLayoutParams(layoutParams);
            }
            if (layoutParams.height > 0) {
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824);
            } else {
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
            }
            view2.measure(i2, makeMeasureSpec);
            view2.forceLayout();
            if (i11 > 0) {
                i7 = i8 + dividerHeight;
            } else {
                i7 = i8;
            }
            int measuredHeight = i7 + view2.getMeasuredHeight();
            if (measuredHeight < i5) {
                if (i6 >= 0 && i11 >= i6) {
                    i9 = measuredHeight;
                }
                i11++;
                i8 = measuredHeight;
            } else if (i6 < 0 || i11 <= i6 || i9 <= 0 || measuredHeight == i5) {
                return i5;
            } else {
                return i9;
            }
        }
        return i8;
    }

    /* access modifiers changed from: protected */
    public void setSelectorEnabled(boolean z) {
        if (this.i != null) {
            this.i.a(z);
        }
    }

    private static class a extends android.support.v7.graphics.drawable.a {

        /* renamed from: a  reason: collision with root package name */
        private boolean f1846a = true;

        public a(Drawable drawable) {
            super(drawable);
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z) {
            this.f1846a = z;
        }

        public boolean setState(int[] iArr) {
            if (this.f1846a) {
                return super.setState(iArr);
            }
            return false;
        }

        public void draw(Canvas canvas) {
            if (this.f1846a) {
                super.draw(canvas);
            }
        }

        public void setHotspot(float f2, float f3) {
            if (this.f1846a) {
                super.setHotspot(f2, f3);
            }
        }

        public void setHotspotBounds(int i, int i2, int i3, int i4) {
            if (this.f1846a) {
                super.setHotspotBounds(i, i2, i3, i4);
            }
        }

        public boolean setVisible(boolean z, boolean z2) {
            if (this.f1846a) {
                return super.setVisible(z, z2);
            }
            return false;
        }
    }
}
