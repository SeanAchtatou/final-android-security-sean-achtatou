package android.support.v7.widget;

import android.support.v4.os.j;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

/* compiled from: GapWorker */
final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    static final ThreadLocal<y> f2282a = new ThreadLocal<>();

    /* renamed from: e  reason: collision with root package name */
    static Comparator<b> f2283e = new Comparator<b>() {
        /* renamed from: a */
        public int compare(b bVar, b bVar2) {
            int i = -1;
            if ((bVar.f2295d == null) != (bVar2.f2295d == null)) {
                return bVar.f2295d == null ? 1 : -1;
            }
            if (bVar.f2292a != bVar2.f2292a) {
                if (!bVar.f2292a) {
                    i = 1;
                }
                return i;
            }
            int i2 = bVar2.f2293b - bVar.f2293b;
            if (i2 != 0) {
                return i2;
            }
            int i3 = bVar.f2294c - bVar2.f2294c;
            if (i3 == 0) {
                return 0;
            }
            return i3;
        }
    };

    /* renamed from: b  reason: collision with root package name */
    ArrayList<RecyclerView> f2284b = new ArrayList<>();

    /* renamed from: c  reason: collision with root package name */
    long f2285c;

    /* renamed from: d  reason: collision with root package name */
    long f2286d;

    /* renamed from: f  reason: collision with root package name */
    private ArrayList<b> f2287f = new ArrayList<>();

    y() {
    }

    /* compiled from: GapWorker */
    static class b {

        /* renamed from: a  reason: collision with root package name */
        public boolean f2292a;

        /* renamed from: b  reason: collision with root package name */
        public int f2293b;

        /* renamed from: c  reason: collision with root package name */
        public int f2294c;

        /* renamed from: d  reason: collision with root package name */
        public RecyclerView f2295d;

        /* renamed from: e  reason: collision with root package name */
        public int f2296e;

        b() {
        }

        public void a() {
            this.f2292a = false;
            this.f2293b = 0;
            this.f2294c = 0;
            this.f2295d = null;
            this.f2296e = 0;
        }
    }

    /* compiled from: GapWorker */
    static class a implements RecyclerView.h.a {

        /* renamed from: a  reason: collision with root package name */
        int f2288a;

        /* renamed from: b  reason: collision with root package name */
        int f2289b;

        /* renamed from: c  reason: collision with root package name */
        int[] f2290c;

        /* renamed from: d  reason: collision with root package name */
        int f2291d;

        a() {
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2) {
            this.f2288a = i;
            this.f2289b = i2;
        }

        /* access modifiers changed from: package-private */
        public void a(RecyclerView recyclerView, boolean z) {
            this.f2291d = 0;
            if (this.f2290c != null) {
                Arrays.fill(this.f2290c, -1);
            }
            RecyclerView.h hVar = recyclerView.mLayout;
            if (recyclerView.mAdapter != null && hVar != null && hVar.o()) {
                if (z) {
                    if (!recyclerView.mAdapterHelper.d()) {
                        hVar.a(recyclerView.mAdapter.getItemCount(), this);
                    }
                } else if (!recyclerView.hasPendingAdapterUpdates()) {
                    hVar.a(this.f2288a, this.f2289b, recyclerView.mState, this);
                }
                if (this.f2291d > hVar.x) {
                    hVar.x = this.f2291d;
                    hVar.y = z;
                    recyclerView.mRecycler.b();
                }
            }
        }

        public void b(int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            } else if (i2 < 0) {
                throw new IllegalArgumentException("Pixel distance must be non-negative");
            } else {
                int i3 = this.f2291d * 2;
                if (this.f2290c == null) {
                    this.f2290c = new int[4];
                    Arrays.fill(this.f2290c, -1);
                } else if (i3 >= this.f2290c.length) {
                    int[] iArr = this.f2290c;
                    this.f2290c = new int[(i3 * 2)];
                    System.arraycopy(iArr, 0, this.f2290c, 0, iArr.length);
                }
                this.f2290c[i3] = i;
                this.f2290c[i3 + 1] = i2;
                this.f2291d++;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i) {
            if (this.f2290c == null) {
                return false;
            }
            int i2 = this.f2291d * 2;
            for (int i3 = 0; i3 < i2; i3 += 2) {
                if (this.f2290c[i3] == i) {
                    return true;
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (this.f2290c != null) {
                Arrays.fill(this.f2290c, -1);
            }
            this.f2291d = 0;
        }
    }

    public void a(RecyclerView recyclerView) {
        this.f2284b.add(recyclerView);
    }

    public void b(RecyclerView recyclerView) {
        this.f2284b.remove(recyclerView);
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView recyclerView, int i, int i2) {
        if (recyclerView.isAttachedToWindow() && this.f2285c == 0) {
            this.f2285c = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        recyclerView.mPrefetchRegistry.a(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.y.a.a(android.support.v7.widget.RecyclerView, boolean):void
     arg types: [android.support.v7.widget.RecyclerView, int]
     candidates:
      android.support.v7.widget.y.a.a(int, int):void
      android.support.v7.widget.y.a.a(android.support.v7.widget.RecyclerView, boolean):void */
    private void a() {
        b bVar;
        boolean z;
        int i;
        int size = this.f2284b.size();
        int i2 = 0;
        int i3 = 0;
        while (i2 < size) {
            RecyclerView recyclerView = this.f2284b.get(i2);
            if (recyclerView.getWindowVisibility() == 0) {
                recyclerView.mPrefetchRegistry.a(recyclerView, false);
                i = recyclerView.mPrefetchRegistry.f2291d + i3;
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        this.f2287f.ensureCapacity(i3);
        int i4 = 0;
        for (int i5 = 0; i5 < size; i5++) {
            RecyclerView recyclerView2 = this.f2284b.get(i5);
            if (recyclerView2.getWindowVisibility() == 0) {
                a aVar = recyclerView2.mPrefetchRegistry;
                int abs = Math.abs(aVar.f2288a) + Math.abs(aVar.f2289b);
                int i6 = i4;
                for (int i7 = 0; i7 < aVar.f2291d * 2; i7 += 2) {
                    if (i6 >= this.f2287f.size()) {
                        bVar = new b();
                        this.f2287f.add(bVar);
                    } else {
                        bVar = this.f2287f.get(i6);
                    }
                    int i8 = aVar.f2290c[i7 + 1];
                    if (i8 <= abs) {
                        z = true;
                    } else {
                        z = false;
                    }
                    bVar.f2292a = z;
                    bVar.f2293b = abs;
                    bVar.f2294c = i8;
                    bVar.f2295d = recyclerView2;
                    bVar.f2296e = aVar.f2290c[i7];
                    i6++;
                }
                i4 = i6;
            }
        }
        Collections.sort(this.f2287f, f2283e);
    }

    static boolean a(RecyclerView recyclerView, int i) {
        int c2 = recyclerView.mChildHelper.c();
        for (int i2 = 0; i2 < c2; i2++) {
            RecyclerView.u childViewHolderInt = RecyclerView.getChildViewHolderInt(recyclerView.mChildHelper.d(i2));
            if (childViewHolderInt.mPosition == i && !childViewHolderInt.isInvalid()) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.RecyclerView.n.a(int, boolean, long):android.support.v7.widget.RecyclerView$u
     arg types: [int, int, long]
     candidates:
      android.support.v7.widget.RecyclerView.n.a(long, int, boolean):android.support.v7.widget.RecyclerView$u
      android.support.v7.widget.RecyclerView.n.a(int, int, boolean):void
      android.support.v7.widget.RecyclerView.n.a(android.support.v7.widget.RecyclerView$a, android.support.v7.widget.RecyclerView$a, boolean):void
      android.support.v7.widget.RecyclerView.n.a(int, boolean, long):android.support.v7.widget.RecyclerView$u */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.RecyclerView.n.a(android.support.v7.widget.RecyclerView$u, boolean):void
     arg types: [android.support.v7.widget.RecyclerView$u, int]
     candidates:
      android.support.v7.widget.RecyclerView.n.a(android.view.ViewGroup, boolean):void
      android.support.v7.widget.RecyclerView.n.a(int, boolean):android.view.View
      android.support.v7.widget.RecyclerView.n.a(int, int):void
      android.support.v7.widget.RecyclerView.n.a(android.support.v7.widget.RecyclerView$u, boolean):void */
    private RecyclerView.u a(RecyclerView recyclerView, int i, long j) {
        if (a(recyclerView, i)) {
            return null;
        }
        RecyclerView.n nVar = recyclerView.mRecycler;
        RecyclerView.u a2 = nVar.a(i, false, j);
        if (a2 == null) {
            return a2;
        }
        if (a2.isBound()) {
            nVar.a(a2.itemView);
            return a2;
        }
        nVar.a(a2, false);
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.y.a.a(android.support.v7.widget.RecyclerView, boolean):void
     arg types: [android.support.v7.widget.RecyclerView, int]
     candidates:
      android.support.v7.widget.y.a.a(int, int):void
      android.support.v7.widget.y.a.a(android.support.v7.widget.RecyclerView, boolean):void */
    private void a(RecyclerView recyclerView, long j) {
        if (recyclerView != null) {
            if (recyclerView.mDataSetHasChangedAfterLayout && recyclerView.mChildHelper.c() != 0) {
                recyclerView.removeAndRecycleViews();
            }
            a aVar = recyclerView.mPrefetchRegistry;
            aVar.a(recyclerView, true);
            if (aVar.f2291d != 0) {
                try {
                    j.a("RV Nested Prefetch");
                    recyclerView.mState.a(recyclerView.mAdapter);
                    for (int i = 0; i < aVar.f2291d * 2; i += 2) {
                        a(recyclerView, aVar.f2290c[i], j);
                    }
                } finally {
                    j.a();
                }
            }
        }
    }

    private void a(b bVar, long j) {
        RecyclerView.u a2 = a(bVar.f2295d, bVar.f2296e, bVar.f2292a ? Long.MAX_VALUE : j);
        if (a2 != null && a2.mNestedRecyclerView != null) {
            a(a2.mNestedRecyclerView.get(), j);
        }
    }

    private void b(long j) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f2287f.size()) {
                b bVar = this.f2287f.get(i2);
                if (bVar.f2295d != null) {
                    a(bVar, j);
                    bVar.a();
                    i = i2 + 1;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        a();
        b(j);
    }

    public void run() {
        long j;
        try {
            j.a("RV Prefetch");
            if (!this.f2284b.isEmpty()) {
                int size = this.f2284b.size();
                int i = 0;
                long j2 = 0;
                while (i < size) {
                    RecyclerView recyclerView = this.f2284b.get(i);
                    if (recyclerView.getWindowVisibility() == 0) {
                        j = Math.max(recyclerView.getDrawingTime(), j2);
                    } else {
                        j = j2;
                    }
                    i++;
                    j2 = j;
                }
                if (j2 == 0) {
                    this.f2285c = 0;
                    j.a();
                    return;
                }
                a(TimeUnit.MILLISECONDS.toNanos(j2) + this.f2286d);
                this.f2285c = 0;
                j.a();
            }
        } finally {
            this.f2285c = 0;
            j.a();
        }
    }
}
