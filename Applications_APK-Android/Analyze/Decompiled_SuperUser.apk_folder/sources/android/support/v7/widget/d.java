package android.support.v7.widget;

import android.support.v4.util.Pools;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ab;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AdapterHelper */
class d implements ab.a {

    /* renamed from: a  reason: collision with root package name */
    final ArrayList<b> f2177a;

    /* renamed from: b  reason: collision with root package name */
    final ArrayList<b> f2178b;

    /* renamed from: c  reason: collision with root package name */
    final a f2179c;

    /* renamed from: d  reason: collision with root package name */
    Runnable f2180d;

    /* renamed from: e  reason: collision with root package name */
    final boolean f2181e;

    /* renamed from: f  reason: collision with root package name */
    final ab f2182f;

    /* renamed from: g  reason: collision with root package name */
    private Pools.a<b> f2183g;

    /* renamed from: h  reason: collision with root package name */
    private int f2184h;

    /* compiled from: AdapterHelper */
    interface a {
        RecyclerView.u a(int i);

        void a(int i, int i2);

        void a(int i, int i2, Object obj);

        void a(b bVar);

        void b(int i, int i2);

        void b(b bVar);

        void c(int i, int i2);

        void d(int i, int i2);
    }

    d(a aVar) {
        this(aVar, false);
    }

    d(a aVar, boolean z) {
        this.f2183g = new Pools.SimplePool(30);
        this.f2177a = new ArrayList<>();
        this.f2178b = new ArrayList<>();
        this.f2184h = 0;
        this.f2179c = aVar;
        this.f2181e = z;
        this.f2182f = new ab(this);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        a(this.f2177a);
        a(this.f2178b);
        this.f2184h = 0;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f2182f.a(this.f2177a);
        int size = this.f2177a.size();
        for (int i = 0; i < size; i++) {
            b bVar = this.f2177a.get(i);
            switch (bVar.f2185a) {
                case 1:
                    f(bVar);
                    break;
                case 2:
                    c(bVar);
                    break;
                case 4:
                    d(bVar);
                    break;
                case 8:
                    b(bVar);
                    break;
            }
            if (this.f2180d != null) {
                this.f2180d.run();
            }
        }
        this.f2177a.clear();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        int size = this.f2178b.size();
        for (int i = 0; i < size; i++) {
            this.f2179c.b(this.f2178b.get(i));
        }
        a(this.f2178b);
        this.f2184h = 0;
    }

    private void b(b bVar) {
        g(bVar);
    }

    private void c(b bVar) {
        boolean z;
        int i;
        int i2;
        int i3;
        boolean z2;
        int i4 = bVar.f2186b;
        int i5 = bVar.f2186b + bVar.f2188d;
        char c2 = 65535;
        int i6 = bVar.f2186b;
        int i7 = 0;
        while (i6 < i5) {
            if (this.f2179c.a(i6) != null || d(i6)) {
                if (c2 == 0) {
                    e(a(2, i4, i7, null));
                    z2 = true;
                } else {
                    z2 = false;
                }
                c2 = 1;
            } else {
                if (c2 == 1) {
                    g(a(2, i4, i7, null));
                    z = true;
                } else {
                    z = false;
                }
                c2 = 0;
            }
            if (z) {
                i3 = i6 - i7;
                i = i5 - i7;
                i2 = 1;
            } else {
                int i8 = i6;
                i = i5;
                i2 = i7 + 1;
                i3 = i8;
            }
            i7 = i2;
            i5 = i;
            i6 = i3 + 1;
        }
        if (i7 != bVar.f2188d) {
            a(bVar);
            bVar = a(2, i4, i7, null);
        }
        if (c2 == 0) {
            e(bVar);
        } else {
            g(bVar);
        }
    }

    private void d(b bVar) {
        int i;
        int i2;
        boolean z;
        int i3 = bVar.f2186b;
        int i4 = bVar.f2186b + bVar.f2188d;
        int i5 = bVar.f2186b;
        boolean z2 = true;
        int i6 = 0;
        while (i5 < i4) {
            if (this.f2179c.a(i5) != null || d(i5)) {
                if (!z2) {
                    e(a(4, i3, i6, bVar.f2187c));
                    i6 = 0;
                    i3 = i5;
                }
                i = i3;
                i2 = i6;
                z = true;
            } else {
                if (z2) {
                    g(a(4, i3, i6, bVar.f2187c));
                    i6 = 0;
                    i3 = i5;
                }
                i = i3;
                i2 = i6;
                z = false;
            }
            i5++;
            boolean z3 = z;
            i6 = i2 + 1;
            i3 = i;
            z2 = z3;
        }
        if (i6 != bVar.f2188d) {
            Object obj = bVar.f2187c;
            a(bVar);
            bVar = a(4, i3, i6, obj);
        }
        if (!z2) {
            e(bVar);
        } else {
            g(bVar);
        }
    }

    private void e(b bVar) {
        int i;
        boolean z;
        if (bVar.f2185a == 1 || bVar.f2185a == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int d2 = d(bVar.f2186b, bVar.f2185a);
        int i2 = bVar.f2186b;
        switch (bVar.f2185a) {
            case 2:
                i = 0;
                break;
            case 3:
            default:
                throw new IllegalArgumentException("op should be remove or update." + bVar);
            case 4:
                i = 1;
                break;
        }
        int i3 = 1;
        int i4 = d2;
        int i5 = i2;
        for (int i6 = 1; i6 < bVar.f2188d; i6++) {
            int d3 = d(bVar.f2186b + (i * i6), bVar.f2185a);
            switch (bVar.f2185a) {
                case 2:
                    if (d3 != i4) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
                case 3:
                default:
                    z = false;
                    break;
                case 4:
                    if (d3 != i4 + 1) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
            }
            if (z) {
                i3++;
            } else {
                b a2 = a(bVar.f2185a, i4, i3, bVar.f2187c);
                a(a2, i5);
                a(a2);
                if (bVar.f2185a == 4) {
                    i5 += i3;
                }
                i3 = 1;
                i4 = d3;
            }
        }
        Object obj = bVar.f2187c;
        a(bVar);
        if (i3 > 0) {
            b a3 = a(bVar.f2185a, i4, i3, obj);
            a(a3, i5);
            a(a3);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar, int i) {
        this.f2179c.a(bVar);
        switch (bVar.f2185a) {
            case 2:
                this.f2179c.a(i, bVar.f2188d);
                return;
            case 3:
            default:
                throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
            case 4:
                this.f2179c.a(i, bVar.f2188d, bVar.f2187c);
                return;
        }
    }

    private int d(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6 = i;
        for (int size = this.f2178b.size() - 1; size >= 0; size--) {
            b bVar = this.f2178b.get(size);
            if (bVar.f2185a == 8) {
                if (bVar.f2186b < bVar.f2188d) {
                    i3 = bVar.f2186b;
                    i4 = bVar.f2188d;
                } else {
                    i3 = bVar.f2188d;
                    i4 = bVar.f2186b;
                }
                if (i6 < i3 || i6 > i4) {
                    if (i6 < bVar.f2186b) {
                        if (i2 == 1) {
                            bVar.f2186b++;
                            bVar.f2188d++;
                            i5 = i6;
                        } else if (i2 == 2) {
                            bVar.f2186b--;
                            bVar.f2188d--;
                        }
                    }
                    i5 = i6;
                } else if (i3 == bVar.f2186b) {
                    if (i2 == 1) {
                        bVar.f2188d++;
                    } else if (i2 == 2) {
                        bVar.f2188d--;
                    }
                    i5 = i6 + 1;
                } else {
                    if (i2 == 1) {
                        bVar.f2186b++;
                    } else if (i2 == 2) {
                        bVar.f2186b--;
                    }
                    i5 = i6 - 1;
                }
                i6 = i5;
            } else if (bVar.f2186b <= i6) {
                if (bVar.f2185a == 1) {
                    i6 -= bVar.f2188d;
                } else if (bVar.f2185a == 2) {
                    i6 += bVar.f2188d;
                }
            } else if (i2 == 1) {
                bVar.f2186b++;
            } else if (i2 == 2) {
                bVar.f2186b--;
            }
        }
        for (int size2 = this.f2178b.size() - 1; size2 >= 0; size2--) {
            b bVar2 = this.f2178b.get(size2);
            if (bVar2.f2185a == 8) {
                if (bVar2.f2188d == bVar2.f2186b || bVar2.f2188d < 0) {
                    this.f2178b.remove(size2);
                    a(bVar2);
                }
            } else if (bVar2.f2188d <= 0) {
                this.f2178b.remove(size2);
                a(bVar2);
            }
        }
        return i6;
    }

    private boolean d(int i) {
        int size = this.f2178b.size();
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.f2178b.get(i2);
            if (bVar.f2185a == 8) {
                if (a(bVar.f2188d, i2 + 1) == i) {
                    return true;
                }
            } else if (bVar.f2185a == 1) {
                int i3 = bVar.f2186b + bVar.f2188d;
                for (int i4 = bVar.f2186b; i4 < i3; i4++) {
                    if (a(i4, i2 + 1) == i) {
                        return true;
                    }
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    private void f(b bVar) {
        g(bVar);
    }

    private void g(b bVar) {
        this.f2178b.add(bVar);
        switch (bVar.f2185a) {
            case 1:
                this.f2179c.c(bVar.f2186b, bVar.f2188d);
                return;
            case 2:
                this.f2179c.b(bVar.f2186b, bVar.f2188d);
                return;
            case 3:
            case 5:
            case 6:
            case 7:
            default:
                throw new IllegalArgumentException("Unknown update op type for " + bVar);
            case 4:
                this.f2179c.a(bVar.f2186b, bVar.f2188d, bVar.f2187c);
                return;
            case 8:
                this.f2179c.d(bVar.f2186b, bVar.f2188d);
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.f2177a.size() > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i) {
        return (this.f2184h & i) != 0;
    }

    /* access modifiers changed from: package-private */
    public int b(int i) {
        return a(i, 0);
    }

    /* access modifiers changed from: package-private */
    public int a(int i, int i2) {
        int size = this.f2178b.size();
        int i3 = i;
        while (i2 < size) {
            b bVar = this.f2178b.get(i2);
            if (bVar.f2185a == 8) {
                if (bVar.f2186b == i3) {
                    i3 = bVar.f2188d;
                } else {
                    if (bVar.f2186b < i3) {
                        i3--;
                    }
                    if (bVar.f2188d <= i3) {
                        i3++;
                    }
                }
            } else if (bVar.f2186b > i3) {
                continue;
            } else if (bVar.f2185a == 2) {
                if (i3 < bVar.f2186b + bVar.f2188d) {
                    return -1;
                }
                i3 -= bVar.f2188d;
            } else if (bVar.f2185a == 1) {
                i3 += bVar.f2188d;
            }
            i2++;
        }
        return i3;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, Object obj) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.f2177a.add(a(4, i, i2, obj));
        this.f2184h |= 4;
        if (this.f2177a.size() != 1) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i, int i2) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.f2177a.add(a(1, i, i2, null));
        this.f2184h |= 1;
        if (this.f2177a.size() != 1) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i, int i2) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.f2177a.add(a(2, i, i2, null));
        this.f2184h |= 2;
        if (this.f2177a.size() != 1) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, int i3) {
        boolean z = true;
        if (i == i2) {
            return false;
        }
        if (i3 != 1) {
            throw new IllegalArgumentException("Moving more than 1 item is not supported yet");
        }
        this.f2177a.add(a(8, i, i2, null));
        this.f2184h |= 8;
        if (this.f2177a.size() != 1) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        c();
        int size = this.f2177a.size();
        for (int i = 0; i < size; i++) {
            b bVar = this.f2177a.get(i);
            switch (bVar.f2185a) {
                case 1:
                    this.f2179c.b(bVar);
                    this.f2179c.c(bVar.f2186b, bVar.f2188d);
                    break;
                case 2:
                    this.f2179c.b(bVar);
                    this.f2179c.a(bVar.f2186b, bVar.f2188d);
                    break;
                case 4:
                    this.f2179c.b(bVar);
                    this.f2179c.a(bVar.f2186b, bVar.f2188d, bVar.f2187c);
                    break;
                case 8:
                    this.f2179c.b(bVar);
                    this.f2179c.d(bVar.f2186b, bVar.f2188d);
                    break;
            }
            if (this.f2180d != null) {
                this.f2180d.run();
            }
        }
        a(this.f2177a);
        this.f2184h = 0;
    }

    public int c(int i) {
        int size = this.f2177a.size();
        int i2 = i;
        for (int i3 = 0; i3 < size; i3++) {
            b bVar = this.f2177a.get(i3);
            switch (bVar.f2185a) {
                case 1:
                    if (bVar.f2186b > i2) {
                        break;
                    } else {
                        i2 += bVar.f2188d;
                        break;
                    }
                case 2:
                    if (bVar.f2186b <= i2) {
                        if (bVar.f2186b + bVar.f2188d <= i2) {
                            i2 -= bVar.f2188d;
                            break;
                        } else {
                            return -1;
                        }
                    } else {
                        continue;
                    }
                case 8:
                    if (bVar.f2186b != i2) {
                        if (bVar.f2186b < i2) {
                            i2--;
                        }
                        if (bVar.f2188d > i2) {
                            break;
                        } else {
                            i2++;
                            break;
                        }
                    } else {
                        i2 = bVar.f2188d;
                        break;
                    }
            }
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return !this.f2178b.isEmpty() && !this.f2177a.isEmpty();
    }

    /* compiled from: AdapterHelper */
    static class b {

        /* renamed from: a  reason: collision with root package name */
        int f2185a;

        /* renamed from: b  reason: collision with root package name */
        int f2186b;

        /* renamed from: c  reason: collision with root package name */
        Object f2187c;

        /* renamed from: d  reason: collision with root package name */
        int f2188d;

        b(int i, int i2, int i3, Object obj) {
            this.f2185a = i;
            this.f2186b = i2;
            this.f2188d = i3;
            this.f2187c = obj;
        }

        /* access modifiers changed from: package-private */
        public String a() {
            switch (this.f2185a) {
                case 1:
                    return ProductAction.ACTION_ADD;
                case 2:
                    return "rm";
                case 3:
                case 5:
                case 6:
                case 7:
                default:
                    return "??";
                case 4:
                    return "up";
                case 8:
                    return "mv";
            }
        }

        public String toString() {
            return Integer.toHexString(System.identityHashCode(this)) + "[" + a() + ",s:" + this.f2186b + "c:" + this.f2188d + ",p:" + this.f2187c + "]";
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.f2185a != bVar.f2185a) {
                return false;
            }
            if (this.f2185a == 8 && Math.abs(this.f2188d - this.f2186b) == 1 && this.f2188d == bVar.f2186b && this.f2186b == bVar.f2188d) {
                return true;
            }
            if (this.f2188d != bVar.f2188d) {
                return false;
            }
            if (this.f2186b != bVar.f2186b) {
                return false;
            }
            if (this.f2187c != null) {
                if (!this.f2187c.equals(bVar.f2187c)) {
                    return false;
                }
                return true;
            } else if (bVar.f2187c != null) {
                return false;
            } else {
                return true;
            }
        }

        public int hashCode() {
            return (((this.f2185a * 31) + this.f2186b) * 31) + this.f2188d;
        }
    }

    public b a(int i, int i2, int i3, Object obj) {
        b a2 = this.f2183g.a();
        if (a2 == null) {
            return new b(i, i2, i3, obj);
        }
        a2.f2185a = i;
        a2.f2186b = i2;
        a2.f2188d = i3;
        a2.f2187c = obj;
        return a2;
    }

    public void a(b bVar) {
        if (!this.f2181e) {
            bVar.f2187c = null;
            this.f2183g.a(bVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List<b> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a(list.get(i));
        }
        list.clear();
    }
}
