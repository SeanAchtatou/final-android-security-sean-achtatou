package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Observable;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.a.e;
import android.support.v4.view.ac;
import android.support.v4.view.ae;
import android.support.v4.view.ag;
import android.support.v4.view.v;
import android.support.v4.widget.EdgeEffectCompat;
import android.support.v7.d.a;
import android.support.v7.widget.ap;
import android.support.v7.widget.aq;
import android.support.v7.widget.d;
import android.support.v7.widget.r;
import android.support.v7.widget.y;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import com.duapps.ad.AdError;
import com.lody.virtual.helper.utils.FileUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecyclerView extends ViewGroup implements ac, android.support.v4.view.u {
    static final boolean ALLOW_SIZE_IN_UNSPECIFIED_SPEC;
    /* access modifiers changed from: private */
    public static final boolean ALLOW_THREAD_GAP_WORK;
    private static final int[] CLIP_TO_PADDING_ATTR = {16842987};
    static final boolean DEBUG = false;
    static final boolean DISPATCH_TEMP_DETACH = false;
    private static final boolean FORCE_ABS_FOCUS_SEARCH_DIRECTION;
    static final boolean FORCE_INVALIDATE_DISPLAY_LIST = (Build.VERSION.SDK_INT == 18 || Build.VERSION.SDK_INT == 19 || Build.VERSION.SDK_INT == 20);
    static final long FOREVER_NS = Long.MAX_VALUE;
    public static final int HORIZONTAL = 0;
    private static final boolean IGNORE_DETACHED_FOCUSED_CHILD;
    private static final int INVALID_POINTER = -1;
    public static final int INVALID_TYPE = -1;
    private static final Class<?>[] LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE = {Context.class, AttributeSet.class, Integer.TYPE, Integer.TYPE};
    static final int MAX_SCROLL_DURATION = 2000;
    private static final int[] NESTED_SCROLLING_ATTRS = {16843830};
    public static final long NO_ID = -1;
    public static final int NO_POSITION = -1;
    static final boolean POST_UPDATES_ON_ANIMATION;
    public static final int SCROLL_STATE_DRAGGING = 1;
    public static final int SCROLL_STATE_IDLE = 0;
    public static final int SCROLL_STATE_SETTLING = 2;
    static final String TAG = "RecyclerView";
    public static final int TOUCH_SLOP_DEFAULT = 0;
    public static final int TOUCH_SLOP_PAGING = 1;
    static final String TRACE_BIND_VIEW_TAG = "RV OnBindView";
    static final String TRACE_CREATE_VIEW_TAG = "RV CreateView";
    private static final String TRACE_HANDLE_ADAPTER_UPDATES_TAG = "RV PartialInvalidate";
    static final String TRACE_NESTED_PREFETCH_TAG = "RV Nested Prefetch";
    private static final String TRACE_ON_DATA_SET_CHANGE_LAYOUT_TAG = "RV FullInvalidate";
    private static final String TRACE_ON_LAYOUT_TAG = "RV OnLayout";
    static final String TRACE_PREFETCH_TAG = "RV Prefetch";
    static final String TRACE_SCROLL_TAG = "RV Scroll";
    static final boolean VERBOSE_TRACING = false;
    public static final int VERTICAL = 1;
    static final Interpolator sQuinticInterpolator = new Interpolator() {
        public float getInterpolation(float f2) {
            float f3 = f2 - 1.0f;
            return (f3 * f3 * f3 * f3 * f3) + 1.0f;
        }
    };
    RecyclerViewAccessibilityDelegate mAccessibilityDelegate;
    private final AccessibilityManager mAccessibilityManager;
    private k mActiveOnItemTouchListener;
    a mAdapter;
    d mAdapterHelper;
    boolean mAdapterUpdateDuringMeasure;
    private EdgeEffectCompat mBottomGlow;
    private d mChildDrawingOrderCallback;
    r mChildHelper;
    boolean mClipToPadding;
    boolean mDataSetHasChangedAfterLayout;
    private int mDispatchScrollCounter;
    private int mEatRequestLayout;
    private int mEatenAccessibilityChangeFlags;
    boolean mFirstLayoutComplete;
    y mGapWorker;
    boolean mHasFixedSize;
    private boolean mIgnoreMotionEventTillDown;
    private int mInitialTouchX;
    private int mInitialTouchY;
    boolean mIsAttached;
    e mItemAnimator;
    private e.b mItemAnimatorListener;
    private Runnable mItemAnimatorRunner;
    final ArrayList<g> mItemDecorations;
    boolean mItemsAddedOrRemoved;
    boolean mItemsChanged;
    private int mLastTouchX;
    private int mLastTouchY;
    h mLayout;
    boolean mLayoutFrozen;
    private int mLayoutOrScrollCounter;
    boolean mLayoutRequestEaten;
    private EdgeEffectCompat mLeftGlow;
    private final int mMaxFlingVelocity;
    private final int mMinFlingVelocity;
    private final int[] mMinMaxLayoutPositions;
    private final int[] mNestedOffsets;
    private final p mObserver;
    private List<i> mOnChildAttachStateListeners;
    private j mOnFlingListener;
    private final ArrayList<k> mOnItemTouchListeners;
    final List<u> mPendingAccessibilityImportanceChange;
    private SavedState mPendingSavedState;
    boolean mPostedAnimatorRunner;
    y.a mPrefetchRegistry;
    private boolean mPreserveFocusAfterLayout;
    final n mRecycler;
    o mRecyclerListener;
    private EdgeEffectCompat mRightGlow;
    private final int[] mScrollConsumed;
    private float mScrollFactor;
    private l mScrollListener;
    private List<l> mScrollListeners;
    private final int[] mScrollOffset;
    private int mScrollPointerId;
    private int mScrollState;
    private v mScrollingChildHelper;
    final r mState;
    final Rect mTempRect;
    private final Rect mTempRect2;
    final RectF mTempRectF;
    private EdgeEffectCompat mTopGlow;
    private int mTouchSlop;
    final Runnable mUpdateChildViewsRunnable;
    private VelocityTracker mVelocityTracker;
    final t mViewFlinger;
    private final aq.b mViewInfoProcessCallback;
    final aq mViewInfoStore;

    public interface d {
        int a(int i, int i2);
    }

    public interface i {
        void a(View view);

        void b(View view);
    }

    public static abstract class j {
        public abstract boolean a(int i, int i2);
    }

    public interface k {
        void a(boolean z);

        boolean a(RecyclerView recyclerView, MotionEvent motionEvent);

        void b(RecyclerView recyclerView, MotionEvent motionEvent);
    }

    public interface o {
        void a(u uVar);
    }

    public static abstract class s {
        public abstract View a(n nVar, int i, int i2);
    }

    static {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        if (Build.VERSION.SDK_INT >= 23) {
            z = true;
        } else {
            z = false;
        }
        ALLOW_SIZE_IN_UNSPECIFIED_SPEC = z;
        if (Build.VERSION.SDK_INT >= 16) {
            z2 = true;
        } else {
            z2 = false;
        }
        POST_UPDATES_ON_ANIMATION = z2;
        if (Build.VERSION.SDK_INT >= 21) {
            z3 = true;
        } else {
            z3 = false;
        }
        ALLOW_THREAD_GAP_WORK = z3;
        if (Build.VERSION.SDK_INT <= 15) {
            z4 = true;
        } else {
            z4 = false;
        }
        FORCE_ABS_FOCUS_SEARCH_DIRECTION = z4;
        if (Build.VERSION.SDK_INT <= 15) {
            z5 = true;
        } else {
            z5 = false;
        }
        IGNORE_DETACHED_FOCUSED_CHILD = z5;
    }

    public RecyclerView(Context context) {
        this(context, null);
    }

    public RecyclerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, int):void
     arg types: [android.support.v7.widget.RecyclerView, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, float):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, int):void */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        boolean z;
        boolean z2 = true;
        this.mObserver = new p();
        this.mRecycler = new n();
        this.mViewInfoStore = new aq();
        this.mUpdateChildViewsRunnable = new Runnable() {
            public void run() {
                if (RecyclerView.this.mFirstLayoutComplete && !RecyclerView.this.isLayoutRequested()) {
                    if (!RecyclerView.this.mIsAttached) {
                        RecyclerView.this.requestLayout();
                    } else if (RecyclerView.this.mLayoutFrozen) {
                        RecyclerView.this.mLayoutRequestEaten = true;
                    } else {
                        RecyclerView.this.consumePendingUpdateOperations();
                    }
                }
            }
        };
        this.mTempRect = new Rect();
        this.mTempRect2 = new Rect();
        this.mTempRectF = new RectF();
        this.mItemDecorations = new ArrayList<>();
        this.mOnItemTouchListeners = new ArrayList<>();
        this.mEatRequestLayout = 0;
        this.mDataSetHasChangedAfterLayout = false;
        this.mLayoutOrScrollCounter = 0;
        this.mDispatchScrollCounter = 0;
        this.mItemAnimator = new DefaultItemAnimator();
        this.mScrollState = 0;
        this.mScrollPointerId = -1;
        this.mScrollFactor = Float.MIN_VALUE;
        this.mPreserveFocusAfterLayout = true;
        this.mViewFlinger = new t();
        this.mPrefetchRegistry = ALLOW_THREAD_GAP_WORK ? new y.a() : null;
        this.mState = new r();
        this.mItemsAddedOrRemoved = false;
        this.mItemsChanged = false;
        this.mItemAnimatorListener = new f();
        this.mPostedAnimatorRunner = false;
        this.mMinMaxLayoutPositions = new int[2];
        this.mScrollOffset = new int[2];
        this.mScrollConsumed = new int[2];
        this.mNestedOffsets = new int[2];
        this.mPendingAccessibilityImportanceChange = new ArrayList();
        this.mItemAnimatorRunner = new Runnable() {
            public void run() {
                if (RecyclerView.this.mItemAnimator != null) {
                    RecyclerView.this.mItemAnimator.a();
                }
                RecyclerView.this.mPostedAnimatorRunner = false;
            }
        };
        this.mViewInfoProcessCallback = new aq.b() {
            public void a(u uVar, e.c cVar, e.c cVar2) {
                RecyclerView.this.mRecycler.c(uVar);
                RecyclerView.this.animateDisappearance(uVar, cVar, cVar2);
            }

            public void b(u uVar, e.c cVar, e.c cVar2) {
                RecyclerView.this.animateAppearance(uVar, cVar, cVar2);
            }

            public void c(u uVar, e.c cVar, e.c cVar2) {
                uVar.setIsRecyclable(false);
                if (RecyclerView.this.mDataSetHasChangedAfterLayout) {
                    if (RecyclerView.this.mItemAnimator.a(uVar, uVar, cVar, cVar2)) {
                        RecyclerView.this.postAnimationRunner();
                    }
                } else if (RecyclerView.this.mItemAnimator.c(uVar, cVar, cVar2)) {
                    RecyclerView.this.postAnimationRunner();
                }
            }

            public void a(u uVar) {
                RecyclerView.this.mLayout.a(uVar.itemView, RecyclerView.this.mRecycler);
            }
        };
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, CLIP_TO_PADDING_ATTR, i2, 0);
            this.mClipToPadding = obtainStyledAttributes.getBoolean(0, true);
            obtainStyledAttributes.recycle();
        } else {
            this.mClipToPadding = true;
        }
        setScrollContainer(true);
        setFocusableInTouchMode(true);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
        this.mMinFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
        this.mMaxFlingVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
        if (getOverScrollMode() == 2) {
            z = true;
        } else {
            z = false;
        }
        setWillNotDraw(z);
        this.mItemAnimator.a(this.mItemAnimatorListener);
        initAdapterManager();
        initChildrenHelper();
        if (ag.d(this) == 0) {
            ag.c((View) this, 1);
        }
        this.mAccessibilityManager = (AccessibilityManager) getContext().getSystemService("accessibility");
        setAccessibilityDelegateCompat(new RecyclerViewAccessibilityDelegate(this));
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, a.c.RecyclerView, i2, 0);
            String string = obtainStyledAttributes2.getString(a.c.RecyclerView_layoutManager);
            if (obtainStyledAttributes2.getInt(a.c.RecyclerView_android_descendantFocusability, -1) == -1) {
                setDescendantFocusability(262144);
            }
            obtainStyledAttributes2.recycle();
            createLayoutManager(context, string, attributeSet, i2, 0);
            if (Build.VERSION.SDK_INT >= 21) {
                TypedArray obtainStyledAttributes3 = context.obtainStyledAttributes(attributeSet, NESTED_SCROLLING_ATTRS, i2, 0);
                z2 = obtainStyledAttributes3.getBoolean(0, true);
                obtainStyledAttributes3.recycle();
            }
        } else {
            setDescendantFocusability(262144);
        }
        setNestedScrollingEnabled(z2);
    }

    public RecyclerViewAccessibilityDelegate getCompatAccessibilityDelegate() {
        return this.mAccessibilityDelegate;
    }

    public void setAccessibilityDelegateCompat(RecyclerViewAccessibilityDelegate recyclerViewAccessibilityDelegate) {
        this.mAccessibilityDelegate = recyclerViewAccessibilityDelegate;
        ag.a(this, this.mAccessibilityDelegate);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void createLayoutManager(android.content.Context r9, java.lang.String r10, android.util.AttributeSet r11, int r12, int r13) {
        /*
            r8 = this;
            if (r10 == 0) goto L_0x0054
            java.lang.String r0 = r10.trim()
            int r1 = r0.length()
            if (r1 == 0) goto L_0x0054
            java.lang.String r3 = r8.getFullClassName(r9, r0)
            boolean r0 = r8.isInEditMode()     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            if (r0 == 0) goto L_0x0055
            java.lang.Class r0 = r8.getClass()     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
        L_0x001e:
            java.lang.Class r0 = r0.loadClass(r3)     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            java.lang.Class<android.support.v7.widget.RecyclerView$h> r1 = android.support.v7.widget.RecyclerView.h.class
            java.lang.Class r4 = r0.asSubclass(r1)     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            r1 = 0
            java.lang.Class<?>[] r0 = android.support.v7.widget.RecyclerView.LAYOUT_MANAGER_CONSTRUCTOR_SIGNATURE     // Catch:{ NoSuchMethodException -> 0x005a }
            java.lang.reflect.Constructor r2 = r4.getConstructor(r0)     // Catch:{ NoSuchMethodException -> 0x005a }
            r0 = 4
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ NoSuchMethodException -> 0x005a }
            r5 = 0
            r0[r5] = r9     // Catch:{ NoSuchMethodException -> 0x005a }
            r5 = 1
            r0[r5] = r11     // Catch:{ NoSuchMethodException -> 0x005a }
            r5 = 2
            java.lang.Integer r6 = java.lang.Integer.valueOf(r12)     // Catch:{ NoSuchMethodException -> 0x005a }
            r0[r5] = r6     // Catch:{ NoSuchMethodException -> 0x005a }
            r5 = 3
            java.lang.Integer r6 = java.lang.Integer.valueOf(r13)     // Catch:{ NoSuchMethodException -> 0x005a }
            r0[r5] = r6     // Catch:{ NoSuchMethodException -> 0x005a }
            r1 = r2
        L_0x0047:
            r2 = 1
            r1.setAccessible(r2)     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            java.lang.Object r0 = r1.newInstance(r0)     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            android.support.v7.widget.RecyclerView$h r0 = (android.support.v7.widget.RecyclerView.h) r0     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            r8.setLayoutManager(r0)     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
        L_0x0054:
            return
        L_0x0055:
            java.lang.ClassLoader r0 = r9.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            goto L_0x001e
        L_0x005a:
            r0 = move-exception
            r2 = 0
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ NoSuchMethodException -> 0x0066 }
            java.lang.reflect.Constructor r0 = r4.getConstructor(r2)     // Catch:{ NoSuchMethodException -> 0x0066 }
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0047
        L_0x0066:
            r1 = move-exception
            r1.initCause(r0)     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            r2.<init>()     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            java.lang.String r4 = r11.getPositionDescription()     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            java.lang.String r4 = ": Error creating LayoutManager "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            java.lang.String r2 = r2.toString()     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            r0.<init>(r2, r1)     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
            throw r0     // Catch:{ ClassNotFoundException -> 0x008b, InvocationTargetException -> 0x00ad, InstantiationException -> 0x00cf, IllegalAccessException -> 0x00f1, ClassCastException -> 0x0113 }
        L_0x008b:
            r0 = move-exception
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = r11.getPositionDescription()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ": Unable to find LayoutManager "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x00ad:
            r0 = move-exception
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = r11.getPositionDescription()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ": Could not instantiate the LayoutManager: "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x00cf:
            r0 = move-exception
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = r11.getPositionDescription()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ": Could not instantiate the LayoutManager: "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x00f1:
            r0 = move-exception
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = r11.getPositionDescription()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ": Cannot access non-public constructor "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x0113:
            r0 = move-exception
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = r11.getPositionDescription()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ": Class is not a LayoutManager "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.createLayoutManager(android.content.Context, java.lang.String, android.util.AttributeSet, int, int):void");
    }

    private String getFullClassName(Context context, String str) {
        if (str.charAt(0) == '.') {
            return context.getPackageName() + str;
        }
        return !str.contains(".") ? RecyclerView.class.getPackage().getName() + '.' + str : str;
    }

    private void initChildrenHelper() {
        this.mChildHelper = new r(new r.b() {
            public int a() {
                return RecyclerView.this.getChildCount();
            }

            public void a(View view, int i) {
                RecyclerView.this.addView(view, i);
                RecyclerView.this.dispatchChildAttached(view);
            }

            public int a(View view) {
                return RecyclerView.this.indexOfChild(view);
            }

            public void a(int i) {
                View childAt = RecyclerView.this.getChildAt(i);
                if (childAt != null) {
                    RecyclerView.this.dispatchChildDetached(childAt);
                }
                RecyclerView.this.removeViewAt(i);
            }

            public View b(int i) {
                return RecyclerView.this.getChildAt(i);
            }

            public void b() {
                int a2 = a();
                for (int i = 0; i < a2; i++) {
                    RecyclerView.this.dispatchChildDetached(b(i));
                }
                RecyclerView.this.removeAllViews();
            }

            public u b(View view) {
                return RecyclerView.getChildViewHolderInt(view);
            }

            public void a(View view, int i, ViewGroup.LayoutParams layoutParams) {
                u childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
                if (childViewHolderInt != null) {
                    if (childViewHolderInt.isTmpDetached() || childViewHolderInt.shouldIgnore()) {
                        childViewHolderInt.clearTmpDetachFlag();
                    } else {
                        throw new IllegalArgumentException("Called attach on a child which is not detached: " + childViewHolderInt);
                    }
                }
                RecyclerView.this.attachViewToParent(view, i, layoutParams);
            }

            public void c(int i) {
                u childViewHolderInt;
                View b2 = b(i);
                if (!(b2 == null || (childViewHolderInt = RecyclerView.getChildViewHolderInt(b2)) == null)) {
                    if (!childViewHolderInt.isTmpDetached() || childViewHolderInt.shouldIgnore()) {
                        childViewHolderInt.addFlags(FileUtils.FileMode.MODE_IRUSR);
                    } else {
                        throw new IllegalArgumentException("called detach on an already detached child " + childViewHolderInt);
                    }
                }
                RecyclerView.this.detachViewFromParent(i);
            }

            public void c(View view) {
                u childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
                if (childViewHolderInt != null) {
                    childViewHolderInt.onEnteredHiddenState(RecyclerView.this);
                }
            }

            public void d(View view) {
                u childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
                if (childViewHolderInt != null) {
                    childViewHolderInt.onLeftHiddenState(RecyclerView.this);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void initAdapterManager() {
        this.mAdapterHelper = new d(new d.a() {
            public u a(int i) {
                u findViewHolderForPosition = RecyclerView.this.findViewHolderForPosition(i, true);
                if (findViewHolderForPosition != null && !RecyclerView.this.mChildHelper.c(findViewHolderForPosition.itemView)) {
                    return findViewHolderForPosition;
                }
                return null;
            }

            public void a(int i, int i2) {
                RecyclerView.this.offsetPositionRecordsForRemove(i, i2, true);
                RecyclerView.this.mItemsAddedOrRemoved = true;
                RecyclerView.this.mState.f1919b += i2;
            }

            public void b(int i, int i2) {
                RecyclerView.this.offsetPositionRecordsForRemove(i, i2, false);
                RecyclerView.this.mItemsAddedOrRemoved = true;
            }

            public void a(int i, int i2, Object obj) {
                RecyclerView.this.viewRangeUpdate(i, i2, obj);
                RecyclerView.this.mItemsChanged = true;
            }

            public void a(d.b bVar) {
                c(bVar);
            }

            /* access modifiers changed from: package-private */
            public void c(d.b bVar) {
                switch (bVar.f2185a) {
                    case 1:
                        RecyclerView.this.mLayout.a(RecyclerView.this, bVar.f2186b, bVar.f2188d);
                        return;
                    case 2:
                        RecyclerView.this.mLayout.b(RecyclerView.this, bVar.f2186b, bVar.f2188d);
                        return;
                    case 3:
                    case 5:
                    case 6:
                    case 7:
                    default:
                        return;
                    case 4:
                        RecyclerView.this.mLayout.a(RecyclerView.this, bVar.f2186b, bVar.f2188d, bVar.f2187c);
                        return;
                    case 8:
                        RecyclerView.this.mLayout.a(RecyclerView.this, bVar.f2186b, bVar.f2188d, 1);
                        return;
                }
            }

            public void b(d.b bVar) {
                c(bVar);
            }

            public void c(int i, int i2) {
                RecyclerView.this.offsetPositionRecordsForInsert(i, i2);
                RecyclerView.this.mItemsAddedOrRemoved = true;
            }

            public void d(int i, int i2) {
                RecyclerView.this.offsetPositionRecordsForMove(i, i2);
                RecyclerView.this.mItemsAddedOrRemoved = true;
            }
        });
    }

    public void setHasFixedSize(boolean z) {
        this.mHasFixedSize = z;
    }

    public boolean hasFixedSize() {
        return this.mHasFixedSize;
    }

    public void setClipToPadding(boolean z) {
        if (z != this.mClipToPadding) {
            invalidateGlows();
        }
        this.mClipToPadding = z;
        super.setClipToPadding(z);
        if (this.mFirstLayoutComplete) {
            requestLayout();
        }
    }

    public boolean getClipToPadding() {
        return this.mClipToPadding;
    }

    public void setScrollingTouchSlop(int i2) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        switch (i2) {
            case 0:
                break;
            case 1:
                this.mTouchSlop = viewConfiguration.getScaledPagingTouchSlop();
                return;
            default:
                Log.w(TAG, "setScrollingTouchSlop(): bad argument constant " + i2 + "; using default value");
                break;
        }
        this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
    }

    public void swapAdapter(a aVar, boolean z) {
        setLayoutFrozen(false);
        setAdapterInternal(aVar, true, z);
        setDataSetChangedAfterLayout();
        requestLayout();
    }

    public void setAdapter(a aVar) {
        setLayoutFrozen(false);
        setAdapterInternal(aVar, false, true);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void removeAndRecycleViews() {
        if (this.mItemAnimator != null) {
            this.mItemAnimator.d();
        }
        if (this.mLayout != null) {
            this.mLayout.c(this.mRecycler);
            this.mLayout.b(this.mRecycler);
        }
        this.mRecycler.a();
    }

    private void setAdapterInternal(a aVar, boolean z, boolean z2) {
        if (this.mAdapter != null) {
            this.mAdapter.unregisterAdapterDataObserver(this.mObserver);
            this.mAdapter.onDetachedFromRecyclerView(this);
        }
        if (!z || z2) {
            removeAndRecycleViews();
        }
        this.mAdapterHelper.a();
        a aVar2 = this.mAdapter;
        this.mAdapter = aVar;
        if (aVar != null) {
            aVar.registerAdapterDataObserver(this.mObserver);
            aVar.onAttachedToRecyclerView(this);
        }
        if (this.mLayout != null) {
            this.mLayout.a(aVar2, this.mAdapter);
        }
        this.mRecycler.a(aVar2, this.mAdapter, z);
        this.mState.f1922e = true;
        markKnownViewsInvalid();
    }

    public a getAdapter() {
        return this.mAdapter;
    }

    public void setRecyclerListener(o oVar) {
        this.mRecyclerListener = oVar;
    }

    public int getBaseline() {
        if (this.mLayout != null) {
            return this.mLayout.t();
        }
        return super.getBaseline();
    }

    public void addOnChildAttachStateChangeListener(i iVar) {
        if (this.mOnChildAttachStateListeners == null) {
            this.mOnChildAttachStateListeners = new ArrayList();
        }
        this.mOnChildAttachStateListeners.add(iVar);
    }

    public void removeOnChildAttachStateChangeListener(i iVar) {
        if (this.mOnChildAttachStateListeners != null) {
            this.mOnChildAttachStateListeners.remove(iVar);
        }
    }

    public void clearOnChildAttachStateChangeListeners() {
        if (this.mOnChildAttachStateListeners != null) {
            this.mOnChildAttachStateListeners.clear();
        }
    }

    public void setLayoutManager(h hVar) {
        if (hVar != this.mLayout) {
            stopScroll();
            if (this.mLayout != null) {
                if (this.mItemAnimator != null) {
                    this.mItemAnimator.d();
                }
                this.mLayout.c(this.mRecycler);
                this.mLayout.b(this.mRecycler);
                this.mRecycler.a();
                if (this.mIsAttached) {
                    this.mLayout.b(this, this.mRecycler);
                }
                this.mLayout.b((RecyclerView) null);
                this.mLayout = null;
            } else {
                this.mRecycler.a();
            }
            this.mChildHelper.a();
            this.mLayout = hVar;
            if (hVar != null) {
                if (hVar.q != null) {
                    throw new IllegalArgumentException("LayoutManager " + hVar + " is already attached to a RecyclerView: " + hVar.q);
                }
                this.mLayout.b(this);
                if (this.mIsAttached) {
                    this.mLayout.c(this);
                }
            }
            this.mRecycler.b();
            requestLayout();
        }
    }

    public void setOnFlingListener(j jVar) {
        this.mOnFlingListener = jVar;
    }

    public j getOnFlingListener() {
        return this.mOnFlingListener;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (this.mPendingSavedState != null) {
            savedState.a(this.mPendingSavedState);
        } else if (this.mLayout != null) {
            savedState.f1863a = this.mLayout.c();
        } else {
            savedState.f1863a = null;
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        this.mPendingSavedState = (SavedState) parcelable;
        super.onRestoreInstanceState(this.mPendingSavedState.getSuperState());
        if (this.mLayout != null && this.mPendingSavedState.f1863a != null) {
            this.mLayout.a(this.mPendingSavedState.f1863a);
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    private void addAnimatingView(u uVar) {
        View view = uVar.itemView;
        boolean z = view.getParent() == this;
        this.mRecycler.c(getChildViewHolder(view));
        if (uVar.isTmpDetached()) {
            this.mChildHelper.a(view, -1, view.getLayoutParams(), true);
        } else if (!z) {
            this.mChildHelper.a(view, true);
        } else {
            this.mChildHelper.d(view);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean removeAnimatingView(View view) {
        eatRequestLayout();
        boolean f2 = this.mChildHelper.f(view);
        if (f2) {
            u childViewHolderInt = getChildViewHolderInt(view);
            this.mRecycler.c(childViewHolderInt);
            this.mRecycler.b(childViewHolderInt);
        }
        resumeRequestLayout(!f2);
        return f2;
    }

    public h getLayoutManager() {
        return this.mLayout;
    }

    public m getRecycledViewPool() {
        return this.mRecycler.g();
    }

    public void setRecycledViewPool(m mVar) {
        this.mRecycler.a(mVar);
    }

    public void setViewCacheExtension(s sVar) {
        this.mRecycler.a(sVar);
    }

    public void setItemViewCacheSize(int i2) {
        this.mRecycler.a(i2);
    }

    public int getScrollState() {
        return this.mScrollState;
    }

    /* access modifiers changed from: package-private */
    public void setScrollState(int i2) {
        if (i2 != this.mScrollState) {
            this.mScrollState = i2;
            if (i2 != 2) {
                stopScrollersInternal();
            }
            dispatchOnScrollStateChanged(i2);
        }
    }

    public void addItemDecoration(g gVar, int i2) {
        if (this.mLayout != null) {
            this.mLayout.a("Cannot add item decoration during a scroll  or layout");
        }
        if (this.mItemDecorations.isEmpty()) {
            setWillNotDraw(false);
        }
        if (i2 < 0) {
            this.mItemDecorations.add(gVar);
        } else {
            this.mItemDecorations.add(i2, gVar);
        }
        markItemDecorInsetsDirty();
        requestLayout();
    }

    public void addItemDecoration(g gVar) {
        addItemDecoration(gVar, -1);
    }

    public void removeItemDecoration(g gVar) {
        if (this.mLayout != null) {
            this.mLayout.a("Cannot remove item decoration during a scroll  or layout");
        }
        this.mItemDecorations.remove(gVar);
        if (this.mItemDecorations.isEmpty()) {
            setWillNotDraw(getOverScrollMode() == 2);
        }
        markItemDecorInsetsDirty();
        requestLayout();
    }

    public void setChildDrawingOrderCallback(d dVar) {
        if (dVar != this.mChildDrawingOrderCallback) {
            this.mChildDrawingOrderCallback = dVar;
            setChildrenDrawingOrderEnabled(this.mChildDrawingOrderCallback != null);
        }
    }

    @Deprecated
    public void setOnScrollListener(l lVar) {
        this.mScrollListener = lVar;
    }

    public void addOnScrollListener(l lVar) {
        if (this.mScrollListeners == null) {
            this.mScrollListeners = new ArrayList();
        }
        this.mScrollListeners.add(lVar);
    }

    public void removeOnScrollListener(l lVar) {
        if (this.mScrollListeners != null) {
            this.mScrollListeners.remove(lVar);
        }
    }

    public void clearOnScrollListeners() {
        if (this.mScrollListeners != null) {
            this.mScrollListeners.clear();
        }
    }

    public void scrollToPosition(int i2) {
        if (!this.mLayoutFrozen) {
            stopScroll();
            if (this.mLayout == null) {
                Log.e(TAG, "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument.");
                return;
            }
            this.mLayout.e(i2);
            awakenScrollBars();
        }
    }

    /* access modifiers changed from: package-private */
    public void jumpToPositionForSmoothScroller(int i2) {
        if (this.mLayout != null) {
            this.mLayout.e(i2);
            awakenScrollBars();
        }
    }

    public void smoothScrollToPosition(int i2) {
        if (!this.mLayoutFrozen) {
            if (this.mLayout == null) {
                Log.e(TAG, "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            } else {
                this.mLayout.a(this, this.mState, i2);
            }
        }
    }

    public void scrollTo(int i2, int i3) {
        Log.w(TAG, "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
    }

    public void scrollBy(int i2, int i3) {
        if (this.mLayout == null) {
            Log.e(TAG, "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.mLayoutFrozen) {
            boolean d2 = this.mLayout.d();
            boolean e2 = this.mLayout.e();
            if (d2 || e2) {
                if (!d2) {
                    i2 = 0;
                }
                if (!e2) {
                    i3 = 0;
                }
                scrollByInternal(i2, i3, null);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void consumePendingUpdateOperations() {
        if (!this.mFirstLayoutComplete || this.mDataSetHasChangedAfterLayout) {
            android.support.v4.os.j.a(TRACE_ON_DATA_SET_CHANGE_LAYOUT_TAG);
            dispatchLayout();
            android.support.v4.os.j.a();
        } else if (!this.mAdapterHelper.d()) {
        } else {
            if (this.mAdapterHelper.a(4) && !this.mAdapterHelper.a(11)) {
                android.support.v4.os.j.a(TRACE_HANDLE_ADAPTER_UPDATES_TAG);
                eatRequestLayout();
                onEnterLayoutOrScroll();
                this.mAdapterHelper.b();
                if (!this.mLayoutRequestEaten) {
                    if (hasUpdatedView()) {
                        dispatchLayout();
                    } else {
                        this.mAdapterHelper.c();
                    }
                }
                resumeRequestLayout(true);
                onExitLayoutOrScroll();
                android.support.v4.os.j.a();
            } else if (this.mAdapterHelper.d()) {
                android.support.v4.os.j.a(TRACE_ON_DATA_SET_CHANGE_LAYOUT_TAG);
                dispatchLayout();
                android.support.v4.os.j.a();
            }
        }
    }

    private boolean hasUpdatedView() {
        int b2 = this.mChildHelper.b();
        for (int i2 = 0; i2 < b2; i2++) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.b(i2));
            if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore() && childViewHolderInt.isUpdated()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean scrollByInternal(int i2, int i3, MotionEvent motionEvent) {
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        consumePendingUpdateOperations();
        if (this.mAdapter != null) {
            eatRequestLayout();
            onEnterLayoutOrScroll();
            android.support.v4.os.j.a(TRACE_SCROLL_TAG);
            if (i2 != 0) {
                i8 = this.mLayout.a(i2, this.mRecycler, this.mState);
                i7 = i2 - i8;
            } else {
                i8 = 0;
                i7 = 0;
            }
            if (i3 != 0) {
                i9 = this.mLayout.b(i3, this.mRecycler, this.mState);
                i10 = i3 - i9;
            } else {
                i9 = 0;
                i10 = 0;
            }
            android.support.v4.os.j.a();
            repositionShadowingViews();
            onExitLayoutOrScroll();
            resumeRequestLayout(false);
            i6 = i10;
            i5 = i8;
            i4 = i9;
        } else {
            i4 = 0;
            i5 = 0;
            i6 = 0;
            i7 = 0;
        }
        if (!this.mItemDecorations.isEmpty()) {
            invalidate();
        }
        if (dispatchNestedScroll(i5, i4, i7, i6, this.mScrollOffset)) {
            this.mLastTouchX -= this.mScrollOffset[0];
            this.mLastTouchY -= this.mScrollOffset[1];
            if (motionEvent != null) {
                motionEvent.offsetLocation((float) this.mScrollOffset[0], (float) this.mScrollOffset[1]);
            }
            int[] iArr = this.mNestedOffsets;
            iArr[0] = iArr[0] + this.mScrollOffset[0];
            int[] iArr2 = this.mNestedOffsets;
            iArr2[1] = iArr2[1] + this.mScrollOffset[1];
        } else if (getOverScrollMode() != 2) {
            if (motionEvent != null) {
                pullGlows(motionEvent.getX(), (float) i7, motionEvent.getY(), (float) i6);
            }
            considerReleasingGlowsOnScroll(i2, i3);
        }
        if (!(i5 == 0 && i4 == 0)) {
            dispatchOnScrolled(i5, i4);
        }
        if (!awakenScrollBars()) {
            invalidate();
        }
        if (i5 == 0 && i4 == 0) {
            return false;
        }
        return true;
    }

    public int computeHorizontalScrollOffset() {
        if (this.mLayout != null && this.mLayout.d()) {
            return this.mLayout.c(this.mState);
        }
        return 0;
    }

    public int computeHorizontalScrollExtent() {
        if (this.mLayout != null && this.mLayout.d()) {
            return this.mLayout.e(this.mState);
        }
        return 0;
    }

    public int computeHorizontalScrollRange() {
        if (this.mLayout != null && this.mLayout.d()) {
            return this.mLayout.g(this.mState);
        }
        return 0;
    }

    public int computeVerticalScrollOffset() {
        if (this.mLayout != null && this.mLayout.e()) {
            return this.mLayout.d(this.mState);
        }
        return 0;
    }

    public int computeVerticalScrollExtent() {
        if (this.mLayout != null && this.mLayout.e()) {
            return this.mLayout.f(this.mState);
        }
        return 0;
    }

    public int computeVerticalScrollRange() {
        if (this.mLayout != null && this.mLayout.e()) {
            return this.mLayout.h(this.mState);
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void eatRequestLayout() {
        this.mEatRequestLayout++;
        if (this.mEatRequestLayout == 1 && !this.mLayoutFrozen) {
            this.mLayoutRequestEaten = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void resumeRequestLayout(boolean z) {
        if (this.mEatRequestLayout < 1) {
            this.mEatRequestLayout = 1;
        }
        if (!z) {
            this.mLayoutRequestEaten = false;
        }
        if (this.mEatRequestLayout == 1) {
            if (z && this.mLayoutRequestEaten && !this.mLayoutFrozen && this.mLayout != null && this.mAdapter != null) {
                dispatchLayout();
            }
            if (!this.mLayoutFrozen) {
                this.mLayoutRequestEaten = false;
            }
        }
        this.mEatRequestLayout--;
    }

    public void setLayoutFrozen(boolean z) {
        if (z != this.mLayoutFrozen) {
            assertNotInLayoutOrScroll("Do not setLayoutFrozen in layout or scroll");
            if (!z) {
                this.mLayoutFrozen = false;
                if (!(!this.mLayoutRequestEaten || this.mLayout == null || this.mAdapter == null)) {
                    requestLayout();
                }
                this.mLayoutRequestEaten = false;
                return;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            onTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0));
            this.mLayoutFrozen = true;
            this.mIgnoreMotionEventTillDown = true;
            stopScroll();
        }
    }

    public boolean isLayoutFrozen() {
        return this.mLayoutFrozen;
    }

    public void smoothScrollBy(int i2, int i3) {
        smoothScrollBy(i2, i3, null);
    }

    public void smoothScrollBy(int i2, int i3, Interpolator interpolator) {
        int i4 = 0;
        if (this.mLayout == null) {
            Log.e(TAG, "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.mLayoutFrozen) {
            if (!this.mLayout.d()) {
                i2 = 0;
            }
            if (this.mLayout.e()) {
                i4 = i3;
            }
            if (i2 != 0 || i4 != 0) {
                this.mViewFlinger.a(i2, i4, interpolator);
            }
        }
    }

    public boolean fling(int i2, int i3) {
        boolean z;
        if (this.mLayout == null) {
            Log.e(TAG, "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            return false;
        } else if (this.mLayoutFrozen) {
            return false;
        } else {
            boolean d2 = this.mLayout.d();
            boolean e2 = this.mLayout.e();
            if (!d2 || Math.abs(i2) < this.mMinFlingVelocity) {
                i2 = 0;
            }
            if (!e2 || Math.abs(i3) < this.mMinFlingVelocity) {
                i3 = 0;
            }
            if ((i2 == 0 && i3 == 0) || dispatchNestedPreFling((float) i2, (float) i3)) {
                return false;
            }
            if (d2 || e2) {
                z = true;
            } else {
                z = false;
            }
            dispatchNestedFling((float) i2, (float) i3, z);
            if (this.mOnFlingListener != null && this.mOnFlingListener.a(i2, i3)) {
                return true;
            }
            if (!z) {
                return false;
            }
            this.mViewFlinger.a(Math.max(-this.mMaxFlingVelocity, Math.min(i2, this.mMaxFlingVelocity)), Math.max(-this.mMaxFlingVelocity, Math.min(i3, this.mMaxFlingVelocity)));
            return true;
        }
    }

    public void stopScroll() {
        setScrollState(0);
        stopScrollersInternal();
    }

    private void stopScrollersInternal() {
        this.mViewFlinger.b();
        if (this.mLayout != null) {
            this.mLayout.H();
        }
    }

    public int getMinFlingVelocity() {
        return this.mMinFlingVelocity;
    }

    public int getMaxFlingVelocity() {
        return this.mMaxFlingVelocity;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008c, code lost:
        if (r7.mBottomGlow.a(r11 / ((float) getHeight()), 1.0f - (r8 / ((float) getWidth()))) == false) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0040, code lost:
        if (r7.mTopGlow.a((-r11) / ((float) getHeight()), r8 / ((float) getWidth())) != false) goto L_0x0042;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void pullGlows(float r8, float r9, float r10, float r11) {
        /*
            r7 = this;
            r6 = 1065353216(0x3f800000, float:1.0)
            r0 = 1
            r5 = 0
            r1 = 0
            int r2 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r2 >= 0) goto L_0x0050
            r7.ensureLeftGlow()
            android.support.v4.widget.EdgeEffectCompat r2 = r7.mLeftGlow
            float r3 = -r9
            int r4 = r7.getWidth()
            float r4 = (float) r4
            float r3 = r3 / r4
            int r4 = r7.getHeight()
            float r4 = (float) r4
            float r4 = r10 / r4
            float r4 = r6 - r4
            boolean r2 = r2.a(r3, r4)
            if (r2 == 0) goto L_0x0025
            r1 = r0
        L_0x0025:
            int r2 = (r11 > r5 ? 1 : (r11 == r5 ? 0 : -1))
            if (r2 >= 0) goto L_0x006f
            r7.ensureTopGlow()
            android.support.v4.widget.EdgeEffectCompat r2 = r7.mTopGlow
            float r3 = -r11
            int r4 = r7.getHeight()
            float r4 = (float) r4
            float r3 = r3 / r4
            int r4 = r7.getWidth()
            float r4 = (float) r4
            float r4 = r8 / r4
            boolean r2 = r2.a(r3, r4)
            if (r2 == 0) goto L_0x008e
        L_0x0042:
            if (r0 != 0) goto L_0x004c
            int r0 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r0 != 0) goto L_0x004c
            int r0 = (r11 > r5 ? 1 : (r11 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x004f
        L_0x004c:
            android.support.v4.view.ag.c(r7)
        L_0x004f:
            return
        L_0x0050:
            int r2 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r2 <= 0) goto L_0x0025
            r7.ensureRightGlow()
            android.support.v4.widget.EdgeEffectCompat r2 = r7.mRightGlow
            int r3 = r7.getWidth()
            float r3 = (float) r3
            float r3 = r9 / r3
            int r4 = r7.getHeight()
            float r4 = (float) r4
            float r4 = r10 / r4
            boolean r2 = r2.a(r3, r4)
            if (r2 == 0) goto L_0x0025
            r1 = r0
            goto L_0x0025
        L_0x006f:
            int r2 = (r11 > r5 ? 1 : (r11 == r5 ? 0 : -1))
            if (r2 <= 0) goto L_0x008e
            r7.ensureBottomGlow()
            android.support.v4.widget.EdgeEffectCompat r2 = r7.mBottomGlow
            int r3 = r7.getHeight()
            float r3 = (float) r3
            float r3 = r11 / r3
            int r4 = r7.getWidth()
            float r4 = (float) r4
            float r4 = r8 / r4
            float r4 = r6 - r4
            boolean r2 = r2.a(r3, r4)
            if (r2 != 0) goto L_0x0042
        L_0x008e:
            r0 = r1
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.pullGlows(float, float, float, float):void");
    }

    private void releaseGlows() {
        boolean z = false;
        if (this.mLeftGlow != null) {
            z = this.mLeftGlow.c();
        }
        if (this.mTopGlow != null) {
            z |= this.mTopGlow.c();
        }
        if (this.mRightGlow != null) {
            z |= this.mRightGlow.c();
        }
        if (this.mBottomGlow != null) {
            z |= this.mBottomGlow.c();
        }
        if (z) {
            ag.c(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void considerReleasingGlowsOnScroll(int i2, int i3) {
        boolean z = false;
        if (this.mLeftGlow != null && !this.mLeftGlow.a() && i2 > 0) {
            z = this.mLeftGlow.c();
        }
        if (this.mRightGlow != null && !this.mRightGlow.a() && i2 < 0) {
            z |= this.mRightGlow.c();
        }
        if (this.mTopGlow != null && !this.mTopGlow.a() && i3 > 0) {
            z |= this.mTopGlow.c();
        }
        if (this.mBottomGlow != null && !this.mBottomGlow.a() && i3 < 0) {
            z |= this.mBottomGlow.c();
        }
        if (z) {
            ag.c(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void absorbGlows(int i2, int i3) {
        if (i2 < 0) {
            ensureLeftGlow();
            this.mLeftGlow.a(-i2);
        } else if (i2 > 0) {
            ensureRightGlow();
            this.mRightGlow.a(i2);
        }
        if (i3 < 0) {
            ensureTopGlow();
            this.mTopGlow.a(-i3);
        } else if (i3 > 0) {
            ensureBottomGlow();
            this.mBottomGlow.a(i3);
        }
        if (i2 != 0 || i3 != 0) {
            ag.c(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void ensureLeftGlow() {
        if (this.mLeftGlow == null) {
            this.mLeftGlow = new EdgeEffectCompat(getContext());
            if (this.mClipToPadding) {
                this.mLeftGlow.a((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.mLeftGlow.a(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ensureRightGlow() {
        if (this.mRightGlow == null) {
            this.mRightGlow = new EdgeEffectCompat(getContext());
            if (this.mClipToPadding) {
                this.mRightGlow.a((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight());
            } else {
                this.mRightGlow.a(getMeasuredHeight(), getMeasuredWidth());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ensureTopGlow() {
        if (this.mTopGlow == null) {
            this.mTopGlow = new EdgeEffectCompat(getContext());
            if (this.mClipToPadding) {
                this.mTopGlow.a((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.mTopGlow.a(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ensureBottomGlow() {
        if (this.mBottomGlow == null) {
            this.mBottomGlow = new EdgeEffectCompat(getContext());
            if (this.mClipToPadding) {
                this.mBottomGlow.a((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom());
            } else {
                this.mBottomGlow.a(getMeasuredWidth(), getMeasuredHeight());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void invalidateGlows() {
        this.mBottomGlow = null;
        this.mTopGlow = null;
        this.mRightGlow = null;
        this.mLeftGlow = null;
    }

    public View focusSearch(View view, int i2) {
        View view2;
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = true;
        View d2 = this.mLayout.d(view, i2);
        if (d2 != null) {
            return d2;
        }
        boolean z6 = this.mAdapter != null && this.mLayout != null && !isComputingLayout() && !this.mLayoutFrozen;
        FocusFinder instance = FocusFinder.getInstance();
        if (!z6 || !(i2 == 2 || i2 == 1)) {
            View findNextFocus = instance.findNextFocus(this, view, i2);
            if (findNextFocus != null || !z6) {
                view2 = findNextFocus;
            } else {
                consumePendingUpdateOperations();
                if (findContainingItemView(view) == null) {
                    return null;
                }
                eatRequestLayout();
                view2 = this.mLayout.a(view, i2, this.mRecycler, this.mState);
                resumeRequestLayout(false);
            }
        } else {
            if (this.mLayout.e()) {
                int i3 = i2 == 2 ? 130 : 33;
                if (instance.findNextFocus(this, view, i3) == null) {
                    z4 = true;
                } else {
                    z4 = false;
                }
                if (FORCE_ABS_FOCUS_SEARCH_DIRECTION) {
                    i2 = i3;
                    z = z4;
                } else {
                    z = z4;
                }
            } else {
                z = false;
            }
            if (z || !this.mLayout.d()) {
                z5 = z;
            } else {
                if (this.mLayout.s() == 1) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (i2 == 2) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                int i4 = z3 ^ z2 ? 66 : 17;
                if (instance.findNextFocus(this, view, i4) != null) {
                    z5 = false;
                }
                if (FORCE_ABS_FOCUS_SEARCH_DIRECTION) {
                    i2 = i4;
                }
            }
            if (z5) {
                consumePendingUpdateOperations();
                if (findContainingItemView(view) == null) {
                    return null;
                }
                eatRequestLayout();
                this.mLayout.a(view, i2, this.mRecycler, this.mState);
                resumeRequestLayout(false);
            }
            view2 = instance.findNextFocus(this, view, i2);
        }
        if (view2 == null || view2.hasFocusable()) {
            if (!isPreferredNextFocus(view, view2, i2)) {
                view2 = super.focusSearch(view, i2);
            }
            return view2;
        } else if (getFocusedChild() == null) {
            return super.focusSearch(view, i2);
        } else {
            requestChildOnScreen(view2, null);
            return view;
        }
    }

    private boolean isPreferredNextFocus(View view, View view2, int i2) {
        boolean z;
        boolean z2 = false;
        if (view2 == null || view2 == this) {
            return false;
        }
        if (view == null) {
            return true;
        }
        if (i2 != 2 && i2 != 1) {
            return isPreferredNextFocusAbsolute(view, view2, i2);
        }
        if (this.mLayout.s() == 1) {
            z = true;
        } else {
            z = false;
        }
        if (i2 == 2) {
            z2 = true;
        }
        if (isPreferredNextFocusAbsolute(view, view2, z2 ^ z ? 66 : 17)) {
            return true;
        }
        if (i2 == 2) {
            return isPreferredNextFocusAbsolute(view, view2, 130);
        }
        return isPreferredNextFocusAbsolute(view, view2, 33);
    }

    private boolean isPreferredNextFocusAbsolute(View view, View view2, int i2) {
        this.mTempRect.set(0, 0, view.getWidth(), view.getHeight());
        this.mTempRect2.set(0, 0, view2.getWidth(), view2.getHeight());
        offsetDescendantRectToMyCoords(view, this.mTempRect);
        offsetDescendantRectToMyCoords(view2, this.mTempRect2);
        switch (i2) {
            case 17:
                if ((this.mTempRect.right > this.mTempRect2.right || this.mTempRect.left >= this.mTempRect2.right) && this.mTempRect.left > this.mTempRect2.left) {
                    return true;
                }
                return false;
            case 33:
                if ((this.mTempRect.bottom > this.mTempRect2.bottom || this.mTempRect.top >= this.mTempRect2.bottom) && this.mTempRect.top > this.mTempRect2.top) {
                    return true;
                }
                return false;
            case 66:
                if ((this.mTempRect.left < this.mTempRect2.left || this.mTempRect.right <= this.mTempRect2.left) && this.mTempRect.right < this.mTempRect2.right) {
                    return true;
                }
                return false;
            case 130:
                if ((this.mTempRect.top < this.mTempRect2.top || this.mTempRect.bottom <= this.mTempRect2.top) && this.mTempRect.bottom < this.mTempRect2.bottom) {
                    return true;
                }
                return false;
            default:
                throw new IllegalArgumentException("direction must be absolute. received:" + i2);
        }
    }

    public void requestChildFocus(View view, View view2) {
        if (!this.mLayout.a(this, this.mState, view, view2) && view2 != null) {
            requestChildOnScreen(view, view2);
        }
        super.requestChildFocus(view, view2);
    }

    private void requestChildOnScreen(View view, View view2) {
        boolean z = true;
        View view3 = view2 != null ? view2 : view;
        this.mTempRect.set(0, 0, view3.getWidth(), view3.getHeight());
        ViewGroup.LayoutParams layoutParams = view3.getLayoutParams();
        if (layoutParams instanceof LayoutParams) {
            LayoutParams layoutParams2 = (LayoutParams) layoutParams;
            if (!layoutParams2.f1861e) {
                Rect rect = layoutParams2.f1860d;
                this.mTempRect.left -= rect.left;
                this.mTempRect.right += rect.right;
                this.mTempRect.top -= rect.top;
                Rect rect2 = this.mTempRect;
                rect2.bottom = rect.bottom + rect2.bottom;
            }
        }
        if (view2 != null) {
            offsetDescendantRectToMyCoords(view2, this.mTempRect);
            offsetRectIntoDescendantCoords(view, this.mTempRect);
        }
        h hVar = this.mLayout;
        Rect rect3 = this.mTempRect;
        boolean z2 = !this.mFirstLayoutComplete;
        if (view2 != null) {
            z = false;
        }
        hVar.a(this, view, rect3, z2, z);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        return this.mLayout.a(this, view, rect, z);
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        if (this.mLayout == null || !this.mLayout.a(this, arrayList, i2, i3)) {
            super.addFocusables(arrayList, i2, i3);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        if (isComputingLayout()) {
            return false;
        }
        return super.onRequestFocusInDescendants(i2, rect);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004f, code lost:
        if (r0 >= 30.0f) goto L_0x0051;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAttachedToWindow() {
        /*
            r4 = this;
            r0 = 1
            r1 = 0
            super.onAttachedToWindow()
            r4.mLayoutOrScrollCounter = r1
            r4.mIsAttached = r0
            boolean r2 = r4.mFirstLayoutComplete
            if (r2 == 0) goto L_0x0068
            boolean r2 = r4.isLayoutRequested()
            if (r2 != 0) goto L_0x0068
        L_0x0013:
            r4.mFirstLayoutComplete = r0
            android.support.v7.widget.RecyclerView$h r0 = r4.mLayout
            if (r0 == 0) goto L_0x001e
            android.support.v7.widget.RecyclerView$h r0 = r4.mLayout
            r0.c(r4)
        L_0x001e:
            r4.mPostedAnimatorRunner = r1
            boolean r0 = android.support.v7.widget.RecyclerView.ALLOW_THREAD_GAP_WORK
            if (r0 == 0) goto L_0x0067
            java.lang.ThreadLocal<android.support.v7.widget.y> r0 = android.support.v7.widget.y.f2282a
            java.lang.Object r0 = r0.get()
            android.support.v7.widget.y r0 = (android.support.v7.widget.y) r0
            r4.mGapWorker = r0
            android.support.v7.widget.y r0 = r4.mGapWorker
            if (r0 != 0) goto L_0x0062
            android.support.v7.widget.y r0 = new android.support.v7.widget.y
            r0.<init>()
            r4.mGapWorker = r0
            android.view.Display r0 = android.support.v4.view.ag.J(r4)
            r1 = 1114636288(0x42700000, float:60.0)
            boolean r2 = r4.isInEditMode()
            if (r2 != 0) goto L_0x006a
            if (r0 == 0) goto L_0x006a
            float r0 = r0.getRefreshRate()
            r2 = 1106247680(0x41f00000, float:30.0)
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x006a
        L_0x0051:
            android.support.v7.widget.y r1 = r4.mGapWorker
            r2 = 1315859240(0x4e6e6b28, float:1.0E9)
            float r0 = r2 / r0
            long r2 = (long) r0
            r1.f2286d = r2
            java.lang.ThreadLocal<android.support.v7.widget.y> r0 = android.support.v7.widget.y.f2282a
            android.support.v7.widget.y r1 = r4.mGapWorker
            r0.set(r1)
        L_0x0062:
            android.support.v7.widget.y r0 = r4.mGapWorker
            r0.a(r4)
        L_0x0067:
            return
        L_0x0068:
            r0 = r1
            goto L_0x0013
        L_0x006a:
            r0 = r1
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.onAttachedToWindow():void");
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mItemAnimator != null) {
            this.mItemAnimator.d();
        }
        stopScroll();
        this.mIsAttached = false;
        if (this.mLayout != null) {
            this.mLayout.b(this, this.mRecycler);
        }
        this.mPendingAccessibilityImportanceChange.clear();
        removeCallbacks(this.mItemAnimatorRunner);
        this.mViewInfoStore.b();
        if (ALLOW_THREAD_GAP_WORK) {
            this.mGapWorker.b(this);
            this.mGapWorker = null;
        }
    }

    public boolean isAttachedToWindow() {
        return this.mIsAttached;
    }

    /* access modifiers changed from: package-private */
    public void assertInLayoutOrScroll(String str) {
        if (isComputingLayout()) {
            return;
        }
        if (str == null) {
            throw new IllegalStateException("Cannot call this method unless RecyclerView is computing a layout or scrolling");
        }
        throw new IllegalStateException(str);
    }

    /* access modifiers changed from: package-private */
    public void assertNotInLayoutOrScroll(String str) {
        if (isComputingLayout()) {
            if (str == null) {
                throw new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling");
            }
            throw new IllegalStateException(str);
        } else if (this.mDispatchScrollCounter > 0) {
            Log.w(TAG, "Cannot call this method in a scroll callback. Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data. Any method call that might change the structureof the RecyclerView or the adapter contents should be postponed tothe next frame.", new IllegalStateException(""));
        }
    }

    public void addOnItemTouchListener(k kVar) {
        this.mOnItemTouchListeners.add(kVar);
    }

    public void removeOnItemTouchListener(k kVar) {
        this.mOnItemTouchListeners.remove(kVar);
        if (this.mActiveOnItemTouchListener == kVar) {
            this.mActiveOnItemTouchListener = null;
        }
    }

    private boolean dispatchOnItemTouchIntercept(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 3 || action == 0) {
            this.mActiveOnItemTouchListener = null;
        }
        int size = this.mOnItemTouchListeners.size();
        int i2 = 0;
        while (i2 < size) {
            k kVar = this.mOnItemTouchListeners.get(i2);
            if (!kVar.a(this, motionEvent) || action == 3) {
                i2++;
            } else {
                this.mActiveOnItemTouchListener = kVar;
                return true;
            }
        }
        return false;
    }

    private boolean dispatchOnItemTouch(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (this.mActiveOnItemTouchListener != null) {
            if (action == 0) {
                this.mActiveOnItemTouchListener = null;
            } else {
                this.mActiveOnItemTouchListener.b(this, motionEvent);
                if (action == 3 || action == 1) {
                    this.mActiveOnItemTouchListener = null;
                }
                return true;
            }
        }
        if (action != 0) {
            int size = this.mOnItemTouchListeners.size();
            for (int i2 = 0; i2 < size; i2++) {
                k kVar = this.mOnItemTouchListeners.get(i2);
                if (kVar.a(this, motionEvent)) {
                    this.mActiveOnItemTouchListener = kVar;
                    return true;
                }
            }
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        int i2;
        int i3 = -1;
        boolean z2 = true;
        if (this.mLayoutFrozen) {
            return false;
        }
        if (dispatchOnItemTouchIntercept(motionEvent)) {
            cancelTouch();
            return true;
        } else if (this.mLayout == null) {
            return false;
        } else {
            boolean d2 = this.mLayout.d();
            boolean e2 = this.mLayout.e();
            if (this.mVelocityTracker == null) {
                this.mVelocityTracker = VelocityTracker.obtain();
            }
            this.mVelocityTracker.addMovement(motionEvent);
            int a2 = android.support.v4.view.s.a(motionEvent);
            int b2 = android.support.v4.view.s.b(motionEvent);
            switch (a2) {
                case 0:
                    if (this.mIgnoreMotionEventTillDown) {
                        this.mIgnoreMotionEventTillDown = false;
                    }
                    this.mScrollPointerId = motionEvent.getPointerId(0);
                    int x = (int) (motionEvent.getX() + 0.5f);
                    this.mLastTouchX = x;
                    this.mInitialTouchX = x;
                    int y = (int) (motionEvent.getY() + 0.5f);
                    this.mLastTouchY = y;
                    this.mInitialTouchY = y;
                    if (this.mScrollState == 2) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        setScrollState(1);
                    }
                    int[] iArr = this.mNestedOffsets;
                    this.mNestedOffsets[1] = 0;
                    iArr[0] = 0;
                    if (d2) {
                        i2 = 1;
                    } else {
                        i2 = 0;
                    }
                    if (e2) {
                        i2 |= 2;
                    }
                    startNestedScroll(i2);
                    break;
                case 1:
                    this.mVelocityTracker.clear();
                    stopNestedScroll();
                    break;
                case 2:
                    int findPointerIndex = motionEvent.findPointerIndex(this.mScrollPointerId);
                    if (findPointerIndex >= 0) {
                        int x2 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
                        int y2 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
                        if (this.mScrollState != 1) {
                            int i4 = x2 - this.mInitialTouchX;
                            int i5 = y2 - this.mInitialTouchY;
                            if (!d2 || Math.abs(i4) <= this.mTouchSlop) {
                                z = false;
                            } else {
                                this.mLastTouchX = ((i4 < 0 ? -1 : 1) * this.mTouchSlop) + this.mInitialTouchX;
                                z = true;
                            }
                            if (e2 && Math.abs(i5) > this.mTouchSlop) {
                                int i6 = this.mInitialTouchY;
                                int i7 = this.mTouchSlop;
                                if (i5 >= 0) {
                                    i3 = 1;
                                }
                                this.mLastTouchY = i6 + (i3 * i7);
                                z = true;
                            }
                            if (z) {
                                setScrollState(1);
                                break;
                            }
                        }
                    } else {
                        Log.e(TAG, "Error processing scroll; pointer index for id " + this.mScrollPointerId + " not found. Did any MotionEvents get skipped?");
                        return false;
                    }
                    break;
                case 3:
                    cancelTouch();
                    break;
                case 5:
                    this.mScrollPointerId = motionEvent.getPointerId(b2);
                    int x3 = (int) (motionEvent.getX(b2) + 0.5f);
                    this.mLastTouchX = x3;
                    this.mInitialTouchX = x3;
                    int y3 = (int) (motionEvent.getY(b2) + 0.5f);
                    this.mLastTouchY = y3;
                    this.mInitialTouchY = y3;
                    break;
                case 6:
                    onPointerUp(motionEvent);
                    break;
            }
            if (this.mScrollState != 1) {
                z2 = false;
            }
            return z2;
        }
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        int size = this.mOnItemTouchListeners.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.mOnItemTouchListeners.get(i2).a(z);
        }
        super.requestDisallowInterceptTouchEvent(z);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f2;
        int i2;
        int i3;
        int i4;
        boolean z;
        int i5;
        boolean z2 = false;
        if (this.mLayoutFrozen || this.mIgnoreMotionEventTillDown) {
            return false;
        }
        if (dispatchOnItemTouch(motionEvent)) {
            cancelTouch();
            return true;
        } else if (this.mLayout == null) {
            return false;
        } else {
            boolean d2 = this.mLayout.d();
            boolean e2 = this.mLayout.e();
            if (this.mVelocityTracker == null) {
                this.mVelocityTracker = VelocityTracker.obtain();
            }
            MotionEvent obtain = MotionEvent.obtain(motionEvent);
            int a2 = android.support.v4.view.s.a(motionEvent);
            int b2 = android.support.v4.view.s.b(motionEvent);
            if (a2 == 0) {
                int[] iArr = this.mNestedOffsets;
                this.mNestedOffsets[1] = 0;
                iArr[0] = 0;
            }
            obtain.offsetLocation((float) this.mNestedOffsets[0], (float) this.mNestedOffsets[1]);
            switch (a2) {
                case 0:
                    this.mScrollPointerId = motionEvent.getPointerId(0);
                    int x = (int) (motionEvent.getX() + 0.5f);
                    this.mLastTouchX = x;
                    this.mInitialTouchX = x;
                    int y = (int) (motionEvent.getY() + 0.5f);
                    this.mLastTouchY = y;
                    this.mInitialTouchY = y;
                    if (d2) {
                        i5 = 1;
                    } else {
                        i5 = 0;
                    }
                    if (e2) {
                        i5 |= 2;
                    }
                    startNestedScroll(i5);
                    break;
                case 1:
                    this.mVelocityTracker.addMovement(obtain);
                    this.mVelocityTracker.computeCurrentVelocity(AdError.NETWORK_ERROR_CODE, (float) this.mMaxFlingVelocity);
                    float f3 = d2 ? -ae.a(this.mVelocityTracker, this.mScrollPointerId) : 0.0f;
                    if (e2) {
                        f2 = -ae.b(this.mVelocityTracker, this.mScrollPointerId);
                    } else {
                        f2 = 0.0f;
                    }
                    if ((f3 == 0.0f && f2 == 0.0f) || !fling((int) f3, (int) f2)) {
                        setScrollState(0);
                    }
                    resetTouch();
                    z2 = true;
                    break;
                case 2:
                    int findPointerIndex = motionEvent.findPointerIndex(this.mScrollPointerId);
                    if (findPointerIndex >= 0) {
                        int x2 = (int) (motionEvent.getX(findPointerIndex) + 0.5f);
                        int y2 = (int) (motionEvent.getY(findPointerIndex) + 0.5f);
                        int i6 = this.mLastTouchX - x2;
                        int i7 = this.mLastTouchY - y2;
                        if (dispatchNestedPreScroll(i6, i7, this.mScrollConsumed, this.mScrollOffset)) {
                            i6 -= this.mScrollConsumed[0];
                            i7 -= this.mScrollConsumed[1];
                            obtain.offsetLocation((float) this.mScrollOffset[0], (float) this.mScrollOffset[1]);
                            int[] iArr2 = this.mNestedOffsets;
                            iArr2[0] = iArr2[0] + this.mScrollOffset[0];
                            int[] iArr3 = this.mNestedOffsets;
                            iArr3[1] = iArr3[1] + this.mScrollOffset[1];
                        }
                        if (this.mScrollState != 1) {
                            if (!d2 || Math.abs(i2) <= this.mTouchSlop) {
                                z = false;
                            } else {
                                if (i2 > 0) {
                                    i2 -= this.mTouchSlop;
                                } else {
                                    i2 += this.mTouchSlop;
                                }
                                z = true;
                            }
                            if (e2 && Math.abs(i3) > this.mTouchSlop) {
                                if (i3 > 0) {
                                    i3 -= this.mTouchSlop;
                                } else {
                                    i3 += this.mTouchSlop;
                                }
                                z = true;
                            }
                            if (z) {
                                setScrollState(1);
                            }
                        }
                        if (this.mScrollState == 1) {
                            this.mLastTouchX = x2 - this.mScrollOffset[0];
                            this.mLastTouchY = y2 - this.mScrollOffset[1];
                            int i8 = d2 ? i2 : 0;
                            if (e2) {
                                i4 = i3;
                            } else {
                                i4 = 0;
                            }
                            if (scrollByInternal(i8, i4, obtain)) {
                                getParent().requestDisallowInterceptTouchEvent(true);
                            }
                            if (!(this.mGapWorker == null || (i2 == 0 && i3 == 0))) {
                                this.mGapWorker.a(this, i2, i3);
                                break;
                            }
                        }
                    } else {
                        Log.e(TAG, "Error processing scroll; pointer index for id " + this.mScrollPointerId + " not found. Did any MotionEvents get skipped?");
                        return false;
                    }
                    break;
                case 3:
                    cancelTouch();
                    break;
                case 5:
                    this.mScrollPointerId = motionEvent.getPointerId(b2);
                    int x3 = (int) (motionEvent.getX(b2) + 0.5f);
                    this.mLastTouchX = x3;
                    this.mInitialTouchX = x3;
                    int y3 = (int) (motionEvent.getY(b2) + 0.5f);
                    this.mLastTouchY = y3;
                    this.mInitialTouchY = y3;
                    break;
                case 6:
                    onPointerUp(motionEvent);
                    break;
            }
            if (!z2) {
                this.mVelocityTracker.addMovement(obtain);
            }
            obtain.recycle();
            return true;
        }
    }

    private void resetTouch() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.clear();
        }
        stopNestedScroll();
        releaseGlows();
    }

    private void cancelTouch() {
        resetTouch();
        setScrollState(0);
    }

    private void onPointerUp(MotionEvent motionEvent) {
        int b2 = android.support.v4.view.s.b(motionEvent);
        if (motionEvent.getPointerId(b2) == this.mScrollPointerId) {
            int i2 = b2 == 0 ? 1 : 0;
            this.mScrollPointerId = motionEvent.getPointerId(i2);
            int x = (int) (motionEvent.getX(i2) + 0.5f);
            this.mLastTouchX = x;
            this.mInitialTouchX = x;
            int y = (int) (motionEvent.getY(i2) + 0.5f);
            this.mLastTouchY = y;
            this.mInitialTouchY = y;
        }
    }

    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float f2;
        float f3;
        if (this.mLayout != null && !this.mLayoutFrozen && (motionEvent.getSource() & 2) != 0 && motionEvent.getAction() == 8) {
            if (this.mLayout.e()) {
                f2 = -android.support.v4.view.s.a(motionEvent, 9);
            } else {
                f2 = 0.0f;
            }
            if (this.mLayout.d()) {
                f3 = android.support.v4.view.s.a(motionEvent, 10);
            } else {
                f3 = 0.0f;
            }
            if (!(f2 == 0.0f && f3 == 0.0f)) {
                float scrollFactor = getScrollFactor();
                scrollByInternal((int) (f3 * scrollFactor), (int) (f2 * scrollFactor), motionEvent);
            }
        }
        return false;
    }

    private float getScrollFactor() {
        if (this.mScrollFactor == Float.MIN_VALUE) {
            TypedValue typedValue = new TypedValue();
            if (!getContext().getTheme().resolveAttribute(16842829, typedValue, true)) {
                return 0.0f;
            }
            this.mScrollFactor = typedValue.getDimension(getContext().getResources().getDisplayMetrics());
        }
        return this.mScrollFactor;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        boolean z = false;
        if (this.mLayout == null) {
            defaultOnMeasure(i2, i3);
        } else if (this.mLayout.w) {
            int mode = View.MeasureSpec.getMode(i2);
            int mode2 = View.MeasureSpec.getMode(i3);
            if (mode == 1073741824 && mode2 == 1073741824) {
                z = true;
            }
            this.mLayout.a(this.mRecycler, this.mState, i2, i3);
            if (!z && this.mAdapter != null) {
                if (this.mState.f1920c == 1) {
                    dispatchLayoutStep1();
                }
                this.mLayout.d(i2, i3);
                this.mState.f1925h = true;
                dispatchLayoutStep2();
                this.mLayout.e(i2, i3);
                if (this.mLayout.k()) {
                    this.mLayout.d(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824));
                    this.mState.f1925h = true;
                    dispatchLayoutStep2();
                    this.mLayout.e(i2, i3);
                }
            }
        } else if (this.mHasFixedSize) {
            this.mLayout.a(this.mRecycler, this.mState, i2, i3);
        } else {
            if (this.mAdapterUpdateDuringMeasure) {
                eatRequestLayout();
                onEnterLayoutOrScroll();
                processAdapterUpdatesAndSetAnimationFlags();
                onExitLayoutOrScroll();
                if (this.mState.j) {
                    this.mState.f1923f = true;
                } else {
                    this.mAdapterHelper.e();
                    this.mState.f1923f = false;
                }
                this.mAdapterUpdateDuringMeasure = false;
                resumeRequestLayout(false);
            }
            if (this.mAdapter != null) {
                this.mState.f1921d = this.mAdapter.getItemCount();
            } else {
                this.mState.f1921d = 0;
            }
            eatRequestLayout();
            this.mLayout.a(this.mRecycler, this.mState, i2, i3);
            resumeRequestLayout(false);
            this.mState.f1923f = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void defaultOnMeasure(int i2, int i3) {
        setMeasuredDimension(h.a(i2, getPaddingLeft() + getPaddingRight(), ag.p(this)), h.a(i3, getPaddingTop() + getPaddingBottom(), ag.q(this)));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4 || i3 != i5) {
            invalidateGlows();
        }
    }

    public void setItemAnimator(e eVar) {
        if (this.mItemAnimator != null) {
            this.mItemAnimator.d();
            this.mItemAnimator.a((e.b) null);
        }
        this.mItemAnimator = eVar;
        if (this.mItemAnimator != null) {
            this.mItemAnimator.a(this.mItemAnimatorListener);
        }
    }

    /* access modifiers changed from: package-private */
    public void onEnterLayoutOrScroll() {
        this.mLayoutOrScrollCounter++;
    }

    /* access modifiers changed from: package-private */
    public void onExitLayoutOrScroll() {
        this.mLayoutOrScrollCounter--;
        if (this.mLayoutOrScrollCounter < 1) {
            this.mLayoutOrScrollCounter = 0;
            dispatchContentChangedIfNecessary();
            dispatchPendingImportantForAccessibilityChanges();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isAccessibilityEnabled() {
        return this.mAccessibilityManager != null && this.mAccessibilityManager.isEnabled();
    }

    private void dispatchContentChangedIfNecessary() {
        int i2 = this.mEatenAccessibilityChangeFlags;
        this.mEatenAccessibilityChangeFlags = 0;
        if (i2 != 0 && isAccessibilityEnabled()) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            obtain.setEventType(FileUtils.FileMode.MODE_ISUID);
            android.support.v4.view.a.a.a(obtain, i2);
            sendAccessibilityEventUnchecked(obtain);
        }
    }

    public boolean isComputingLayout() {
        return this.mLayoutOrScrollCounter > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean shouldDeferAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        int i2;
        int i3 = 0;
        if (!isComputingLayout()) {
            return false;
        }
        if (accessibilityEvent != null) {
            i2 = android.support.v4.view.a.a.b(accessibilityEvent);
        } else {
            i2 = 0;
        }
        if (i2 != 0) {
            i3 = i2;
        }
        this.mEatenAccessibilityChangeFlags = i3 | this.mEatenAccessibilityChangeFlags;
        return true;
    }

    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        if (!shouldDeferAccessibilityEvent(accessibilityEvent)) {
            super.sendAccessibilityEventUnchecked(accessibilityEvent);
        }
    }

    public e getItemAnimator() {
        return this.mItemAnimator;
    }

    /* access modifiers changed from: package-private */
    public void postAnimationRunner() {
        if (!this.mPostedAnimatorRunner && this.mIsAttached) {
            ag.a(this, this.mItemAnimatorRunner);
            this.mPostedAnimatorRunner = true;
        }
    }

    private boolean predictiveItemAnimationsEnabled() {
        return this.mItemAnimator != null && this.mLayout.b();
    }

    private void processAdapterUpdatesAndSetAnimationFlags() {
        boolean z;
        boolean z2;
        boolean z3 = true;
        if (this.mDataSetHasChangedAfterLayout) {
            this.mAdapterHelper.a();
            this.mLayout.a(this);
        }
        if (predictiveItemAnimationsEnabled()) {
            this.mAdapterHelper.b();
        } else {
            this.mAdapterHelper.e();
        }
        if (this.mItemsAddedOrRemoved || this.mItemsChanged) {
            z = true;
        } else {
            z = false;
        }
        r rVar = this.mState;
        if (!this.mFirstLayoutComplete || this.mItemAnimator == null || ((!this.mDataSetHasChangedAfterLayout && !z && !this.mLayout.u) || (this.mDataSetHasChangedAfterLayout && !this.mAdapter.hasStableIds()))) {
            z2 = false;
        } else {
            z2 = true;
        }
        rVar.i = z2;
        r rVar2 = this.mState;
        if (!this.mState.i || !z || this.mDataSetHasChangedAfterLayout || !predictiveItemAnimationsEnabled()) {
            z3 = false;
        }
        rVar2.j = z3;
    }

    /* access modifiers changed from: package-private */
    public void dispatchLayout() {
        if (this.mAdapter == null) {
            Log.e(TAG, "No adapter attached; skipping layout");
        } else if (this.mLayout == null) {
            Log.e(TAG, "No layout manager attached; skipping layout");
        } else {
            this.mState.f1925h = false;
            if (this.mState.f1920c == 1) {
                dispatchLayoutStep1();
                this.mLayout.f(this);
                dispatchLayoutStep2();
            } else if (!this.mAdapterHelper.f() && this.mLayout.x() == getWidth() && this.mLayout.y() == getHeight()) {
                this.mLayout.f(this);
            } else {
                this.mLayout.f(this);
                dispatchLayoutStep2();
            }
            dispatchLayoutStep3();
        }
    }

    private void saveFocusInfo() {
        View view;
        int adapterPosition;
        if (!this.mPreserveFocusAfterLayout || !hasFocus() || this.mAdapter == null) {
            view = null;
        } else {
            view = getFocusedChild();
        }
        u findContainingViewHolder = view == null ? null : findContainingViewHolder(view);
        if (findContainingViewHolder == null) {
            resetFocusInfo();
            return;
        }
        this.mState.l = this.mAdapter.hasStableIds() ? findContainingViewHolder.getItemId() : -1;
        r rVar = this.mState;
        if (this.mDataSetHasChangedAfterLayout) {
            adapterPosition = -1;
        } else if (findContainingViewHolder.isRemoved()) {
            adapterPosition = findContainingViewHolder.mOldPosition;
        } else {
            adapterPosition = findContainingViewHolder.getAdapterPosition();
        }
        rVar.k = adapterPosition;
        this.mState.m = getDeepestFocusedViewWithId(findContainingViewHolder.itemView);
    }

    private void resetFocusInfo() {
        this.mState.l = -1;
        this.mState.k = -1;
        this.mState.m = -1;
    }

    private View findNextViewToFocus() {
        int i2 = this.mState.k != -1 ? this.mState.k : 0;
        int e2 = this.mState.e();
        int i3 = i2;
        while (i3 < e2) {
            u findViewHolderForAdapterPosition = findViewHolderForAdapterPosition(i3);
            if (findViewHolderForAdapterPosition == null) {
                break;
            } else if (findViewHolderForAdapterPosition.itemView.hasFocusable()) {
                return findViewHolderForAdapterPosition.itemView;
            } else {
                i3++;
            }
        }
        for (int min = Math.min(e2, i2) - 1; min >= 0; min--) {
            u findViewHolderForAdapterPosition2 = findViewHolderForAdapterPosition(min);
            if (findViewHolderForAdapterPosition2 == null) {
                return null;
            }
            if (findViewHolderForAdapterPosition2.itemView.hasFocusable()) {
                return findViewHolderForAdapterPosition2.itemView;
            }
        }
        return null;
    }

    private void recoverFocusFromState() {
        u uVar;
        View view;
        View view2 = null;
        if (this.mPreserveFocusAfterLayout && this.mAdapter != null && hasFocus() && getDescendantFocusability() != 393216) {
            if (getDescendantFocusability() != 131072 || !isFocused()) {
                if (!isFocused()) {
                    View focusedChild = getFocusedChild();
                    if (!IGNORE_DETACHED_FOCUSED_CHILD || (focusedChild.getParent() != null && focusedChild.hasFocus())) {
                        if (!this.mChildHelper.c(focusedChild)) {
                            return;
                        }
                    } else if (this.mChildHelper.b() == 0) {
                        requestFocus();
                        return;
                    }
                }
                if (this.mState.l == -1 || !this.mAdapter.hasStableIds()) {
                    uVar = null;
                } else {
                    uVar = findViewHolderForItemId(this.mState.l);
                }
                if (uVar != null && !this.mChildHelper.c(uVar.itemView) && uVar.itemView.hasFocusable()) {
                    view2 = uVar.itemView;
                } else if (this.mChildHelper.b() > 0) {
                    view2 = findNextViewToFocus();
                }
                if (view2 != null) {
                    if (((long) this.mState.m) == -1 || (view = view2.findViewById(this.mState.m)) == null || !view.isFocusable()) {
                        view = view2;
                    }
                    view.requestFocus();
                }
            }
        }
    }

    private int getDeepestFocusedViewWithId(View view) {
        int i2;
        int id = view.getId();
        while (true) {
            i2 = id;
            View view2 = view;
            if (view2.isFocused() || !(view2 instanceof ViewGroup) || !view2.hasFocus()) {
                return i2;
            }
            view = ((ViewGroup) view2).getFocusedChild();
            if (view.getId() != -1) {
                id = view.getId();
            } else {
                id = i2;
            }
        }
        return i2;
    }

    private void dispatchLayoutStep1() {
        boolean z = true;
        this.mState.a(1);
        this.mState.f1925h = false;
        eatRequestLayout();
        this.mViewInfoStore.a();
        onEnterLayoutOrScroll();
        processAdapterUpdatesAndSetAnimationFlags();
        saveFocusInfo();
        r rVar = this.mState;
        if (!this.mState.i || !this.mItemsChanged) {
            z = false;
        }
        rVar.f1924g = z;
        this.mItemsChanged = false;
        this.mItemsAddedOrRemoved = false;
        this.mState.f1923f = this.mState.j;
        this.mState.f1921d = this.mAdapter.getItemCount();
        findMinMaxChildLayoutPositions(this.mMinMaxLayoutPositions);
        if (this.mState.i) {
            int b2 = this.mChildHelper.b();
            for (int i2 = 0; i2 < b2; i2++) {
                u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.b(i2));
                if (!childViewHolderInt.shouldIgnore() && (!childViewHolderInt.isInvalid() || this.mAdapter.hasStableIds())) {
                    this.mViewInfoStore.a(childViewHolderInt, this.mItemAnimator.a(this.mState, childViewHolderInt, e.e(childViewHolderInt), childViewHolderInt.getUnmodifiedPayloads()));
                    if (this.mState.f1924g && childViewHolderInt.isUpdated() && !childViewHolderInt.isRemoved() && !childViewHolderInt.shouldIgnore() && !childViewHolderInt.isInvalid()) {
                        this.mViewInfoStore.a(getChangedHolderKey(childViewHolderInt), childViewHolderInt);
                    }
                }
            }
        }
        if (this.mState.j) {
            saveOldPositions();
            boolean z2 = this.mState.f1922e;
            this.mState.f1922e = false;
            this.mLayout.c(this.mRecycler, this.mState);
            this.mState.f1922e = z2;
            for (int i3 = 0; i3 < this.mChildHelper.b(); i3++) {
                u childViewHolderInt2 = getChildViewHolderInt(this.mChildHelper.b(i3));
                if (!childViewHolderInt2.shouldIgnore() && !this.mViewInfoStore.d(childViewHolderInt2)) {
                    int e2 = e.e(childViewHolderInt2);
                    boolean hasAnyOfTheFlags = childViewHolderInt2.hasAnyOfTheFlags(8192);
                    if (!hasAnyOfTheFlags) {
                        e2 |= CodedOutputStream.DEFAULT_BUFFER_SIZE;
                    }
                    e.c a2 = this.mItemAnimator.a(this.mState, childViewHolderInt2, e2, childViewHolderInt2.getUnmodifiedPayloads());
                    if (hasAnyOfTheFlags) {
                        recordAnimationInfoIfBouncedHiddenView(childViewHolderInt2, a2);
                    } else {
                        this.mViewInfoStore.b(childViewHolderInt2, a2);
                    }
                }
            }
            clearOldPositions();
        } else {
            clearOldPositions();
        }
        onExitLayoutOrScroll();
        resumeRequestLayout(false);
        this.mState.f1920c = 2;
    }

    private void dispatchLayoutStep2() {
        boolean z;
        eatRequestLayout();
        onEnterLayoutOrScroll();
        this.mState.a(6);
        this.mAdapterHelper.e();
        this.mState.f1921d = this.mAdapter.getItemCount();
        this.mState.f1919b = 0;
        this.mState.f1923f = false;
        this.mLayout.c(this.mRecycler, this.mState);
        this.mState.f1922e = false;
        this.mPendingSavedState = null;
        r rVar = this.mState;
        if (!this.mState.i || this.mItemAnimator == null) {
            z = false;
        } else {
            z = true;
        }
        rVar.i = z;
        this.mState.f1920c = 4;
        onExitLayoutOrScroll();
        resumeRequestLayout(false);
    }

    private void dispatchLayoutStep3() {
        this.mState.a(4);
        eatRequestLayout();
        onEnterLayoutOrScroll();
        this.mState.f1920c = 1;
        if (this.mState.i) {
            for (int b2 = this.mChildHelper.b() - 1; b2 >= 0; b2--) {
                u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.b(b2));
                if (!childViewHolderInt.shouldIgnore()) {
                    long changedHolderKey = getChangedHolderKey(childViewHolderInt);
                    e.c a2 = this.mItemAnimator.a(this.mState, childViewHolderInt);
                    u a3 = this.mViewInfoStore.a(changedHolderKey);
                    if (a3 == null || a3.shouldIgnore()) {
                        this.mViewInfoStore.c(childViewHolderInt, a2);
                    } else {
                        boolean a4 = this.mViewInfoStore.a(a3);
                        boolean a5 = this.mViewInfoStore.a(childViewHolderInt);
                        if (!a4 || a3 != childViewHolderInt) {
                            e.c b3 = this.mViewInfoStore.b(a3);
                            this.mViewInfoStore.c(childViewHolderInt, a2);
                            e.c c2 = this.mViewInfoStore.c(childViewHolderInt);
                            if (b3 == null) {
                                handleMissingPreInfoForChangeError(changedHolderKey, childViewHolderInt, a3);
                            } else {
                                animateChange(a3, childViewHolderInt, b3, c2, a4, a5);
                            }
                        } else {
                            this.mViewInfoStore.c(childViewHolderInt, a2);
                        }
                    }
                }
            }
            this.mViewInfoStore.a(this.mViewInfoProcessCallback);
        }
        this.mLayout.b(this.mRecycler);
        this.mState.f1918a = this.mState.f1921d;
        this.mDataSetHasChangedAfterLayout = false;
        this.mState.i = false;
        this.mState.j = false;
        this.mLayout.u = false;
        if (this.mRecycler.f1896b != null) {
            this.mRecycler.f1896b.clear();
        }
        if (this.mLayout.y) {
            this.mLayout.x = 0;
            this.mLayout.y = false;
            this.mRecycler.b();
        }
        this.mLayout.a(this.mState);
        onExitLayoutOrScroll();
        resumeRequestLayout(false);
        this.mViewInfoStore.a();
        if (didChildRangeChange(this.mMinMaxLayoutPositions[0], this.mMinMaxLayoutPositions[1])) {
            dispatchOnScrolled(0, 0);
        }
        recoverFocusFromState();
        resetFocusInfo();
    }

    private void handleMissingPreInfoForChangeError(long j2, u uVar, u uVar2) {
        int b2 = this.mChildHelper.b();
        int i2 = 0;
        while (i2 < b2) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.b(i2));
            if (childViewHolderInt == uVar || getChangedHolderKey(childViewHolderInt) != j2) {
                i2++;
            } else if (this.mAdapter == null || !this.mAdapter.hasStableIds()) {
                throw new IllegalStateException("Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:" + childViewHolderInt + " \n View Holder 2:" + uVar);
            } else {
                throw new IllegalStateException("Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:" + childViewHolderInt + " \n View Holder 2:" + uVar);
            }
        }
        Log.e(TAG, "Problem while matching changed view holders with the newones. The pre-layout information for the change holder " + uVar2 + " cannot be found but it is necessary for " + uVar);
    }

    /* access modifiers changed from: package-private */
    public void recordAnimationInfoIfBouncedHiddenView(u uVar, e.c cVar) {
        uVar.setFlags(0, 8192);
        if (this.mState.f1924g && uVar.isUpdated() && !uVar.isRemoved() && !uVar.shouldIgnore()) {
            this.mViewInfoStore.a(getChangedHolderKey(uVar), uVar);
        }
        this.mViewInfoStore.a(uVar, cVar);
    }

    private void findMinMaxChildLayoutPositions(int[] iArr) {
        int b2 = this.mChildHelper.b();
        if (b2 == 0) {
            iArr[0] = -1;
            iArr[1] = -1;
            return;
        }
        int i2 = Integer.MAX_VALUE;
        int i3 = Integer.MIN_VALUE;
        int i4 = 0;
        while (i4 < b2) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.b(i4));
            if (!childViewHolderInt.shouldIgnore()) {
                int layoutPosition = childViewHolderInt.getLayoutPosition();
                if (layoutPosition < i2) {
                    i2 = layoutPosition;
                }
                if (layoutPosition > i3) {
                    i3 = layoutPosition;
                }
            }
            i4++;
            i2 = i2;
        }
        iArr[0] = i2;
        iArr[1] = i3;
    }

    private boolean didChildRangeChange(int i2, int i3) {
        findMinMaxChildLayoutPositions(this.mMinMaxLayoutPositions);
        if (this.mMinMaxLayoutPositions[0] == i2 && this.mMinMaxLayoutPositions[1] == i3) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void removeDetachedView(View view, boolean z) {
        u childViewHolderInt = getChildViewHolderInt(view);
        if (childViewHolderInt != null) {
            if (childViewHolderInt.isTmpDetached()) {
                childViewHolderInt.clearTmpDetachFlag();
            } else if (!childViewHolderInt.shouldIgnore()) {
                throw new IllegalArgumentException("Called removeDetachedView with a view which is not flagged as tmp detached." + childViewHolderInt);
            }
        }
        dispatchChildDetached(view);
        super.removeDetachedView(view, z);
    }

    /* access modifiers changed from: package-private */
    public long getChangedHolderKey(u uVar) {
        return this.mAdapter.hasStableIds() ? uVar.getItemId() : (long) uVar.mPosition;
    }

    /* access modifiers changed from: package-private */
    public void animateAppearance(u uVar, e.c cVar, e.c cVar2) {
        uVar.setIsRecyclable(false);
        if (this.mItemAnimator.b(uVar, cVar, cVar2)) {
            postAnimationRunner();
        }
    }

    /* access modifiers changed from: package-private */
    public void animateDisappearance(u uVar, e.c cVar, e.c cVar2) {
        addAnimatingView(uVar);
        uVar.setIsRecyclable(false);
        if (this.mItemAnimator.a(uVar, cVar, cVar2)) {
            postAnimationRunner();
        }
    }

    private void animateChange(u uVar, u uVar2, e.c cVar, e.c cVar2, boolean z, boolean z2) {
        uVar.setIsRecyclable(false);
        if (z) {
            addAnimatingView(uVar);
        }
        if (uVar != uVar2) {
            if (z2) {
                addAnimatingView(uVar2);
            }
            uVar.mShadowedHolder = uVar2;
            addAnimatingView(uVar);
            this.mRecycler.c(uVar);
            uVar2.setIsRecyclable(false);
            uVar2.mShadowingHolder = uVar;
        }
        if (this.mItemAnimator.a(uVar, uVar2, cVar, cVar2)) {
            postAnimationRunner();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        android.support.v4.os.j.a(TRACE_ON_LAYOUT_TAG);
        dispatchLayout();
        android.support.v4.os.j.a();
        this.mFirstLayoutComplete = true;
    }

    public void requestLayout() {
        if (this.mEatRequestLayout != 0 || this.mLayoutFrozen) {
            this.mLayoutRequestEaten = true;
        } else {
            super.requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void markItemDecorInsetsDirty() {
        int c2 = this.mChildHelper.c();
        for (int i2 = 0; i2 < c2; i2++) {
            ((LayoutParams) this.mChildHelper.d(i2).getLayoutParams()).f1861e = true;
        }
        this.mRecycler.k();
    }

    public void draw(Canvas canvas) {
        boolean z;
        int i2;
        boolean z2;
        boolean z3;
        boolean z4 = true;
        boolean z5 = false;
        super.draw(canvas);
        int size = this.mItemDecorations.size();
        for (int i3 = 0; i3 < size; i3++) {
            this.mItemDecorations.get(i3).b(canvas, this, this.mState);
        }
        if (this.mLeftGlow == null || this.mLeftGlow.a()) {
            z = false;
        } else {
            int save = canvas.save();
            int paddingBottom = this.mClipToPadding ? getPaddingBottom() : 0;
            canvas.rotate(270.0f);
            canvas.translate((float) (paddingBottom + (-getHeight())), 0.0f);
            if (this.mLeftGlow == null || !this.mLeftGlow.a(canvas)) {
                z = false;
            } else {
                z = true;
            }
            canvas.restoreToCount(save);
        }
        if (this.mTopGlow != null && !this.mTopGlow.a()) {
            int save2 = canvas.save();
            if (this.mClipToPadding) {
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
            }
            if (this.mTopGlow == null || !this.mTopGlow.a(canvas)) {
                z3 = false;
            } else {
                z3 = true;
            }
            z |= z3;
            canvas.restoreToCount(save2);
        }
        if (this.mRightGlow != null && !this.mRightGlow.a()) {
            int save3 = canvas.save();
            int width = getWidth();
            if (this.mClipToPadding) {
                i2 = getPaddingTop();
            } else {
                i2 = 0;
            }
            canvas.rotate(90.0f);
            canvas.translate((float) (-i2), (float) (-width));
            if (this.mRightGlow == null || !this.mRightGlow.a(canvas)) {
                z2 = false;
            } else {
                z2 = true;
            }
            z |= z2;
            canvas.restoreToCount(save3);
        }
        if (this.mBottomGlow != null && !this.mBottomGlow.a()) {
            int save4 = canvas.save();
            canvas.rotate(180.0f);
            if (this.mClipToPadding) {
                canvas.translate((float) ((-getWidth()) + getPaddingRight()), (float) ((-getHeight()) + getPaddingBottom()));
            } else {
                canvas.translate((float) (-getWidth()), (float) (-getHeight()));
            }
            if (this.mBottomGlow != null && this.mBottomGlow.a(canvas)) {
                z5 = true;
            }
            z |= z5;
            canvas.restoreToCount(save4);
        }
        if (z || this.mItemAnimator == null || this.mItemDecorations.size() <= 0 || !this.mItemAnimator.b()) {
            z4 = z;
        }
        if (z4) {
            ag.c(this);
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = this.mItemDecorations.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.mItemDecorations.get(i2).a(canvas, this, this.mState);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && this.mLayout.a((LayoutParams) layoutParams);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        if (this.mLayout != null) {
            return this.mLayout.a();
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager");
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        if (this.mLayout != null) {
            return this.mLayout.a(getContext(), attributeSet);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager");
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (this.mLayout != null) {
            return this.mLayout.a(layoutParams);
        }
        throw new IllegalStateException("RecyclerView has no LayoutManager");
    }

    public boolean isAnimating() {
        return this.mItemAnimator != null && this.mItemAnimator.b();
    }

    /* access modifiers changed from: package-private */
    public void saveOldPositions() {
        int c2 = this.mChildHelper.c();
        for (int i2 = 0; i2 < c2; i2++) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i2));
            if (!childViewHolderInt.shouldIgnore()) {
                childViewHolderInt.saveOldPosition();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void clearOldPositions() {
        int c2 = this.mChildHelper.c();
        for (int i2 = 0; i2 < c2; i2++) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i2));
            if (!childViewHolderInt.shouldIgnore()) {
                childViewHolderInt.clearOldPosition();
            }
        }
        this.mRecycler.j();
    }

    /* access modifiers changed from: package-private */
    public void offsetPositionRecordsForMove(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int c2 = this.mChildHelper.c();
        if (i2 < i3) {
            i4 = -1;
            i5 = i3;
            i6 = i2;
        } else {
            i4 = 1;
            i5 = i2;
            i6 = i3;
        }
        for (int i7 = 0; i7 < c2; i7++) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i7));
            if (childViewHolderInt != null && childViewHolderInt.mPosition >= i6 && childViewHolderInt.mPosition <= i5) {
                if (childViewHolderInt.mPosition == i2) {
                    childViewHolderInt.offsetPosition(i3 - i2, false);
                } else {
                    childViewHolderInt.offsetPosition(i4, false);
                }
                this.mState.f1922e = true;
            }
        }
        this.mRecycler.a(i2, i3);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void offsetPositionRecordsForInsert(int i2, int i3) {
        int c2 = this.mChildHelper.c();
        for (int i4 = 0; i4 < c2; i4++) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i4));
            if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore() && childViewHolderInt.mPosition >= i2) {
                childViewHolderInt.offsetPosition(i3, false);
                this.mState.f1922e = true;
            }
        }
        this.mRecycler.b(i2, i3);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void offsetPositionRecordsForRemove(int i2, int i3, boolean z) {
        int i4 = i2 + i3;
        int c2 = this.mChildHelper.c();
        for (int i5 = 0; i5 < c2; i5++) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i5));
            if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore()) {
                if (childViewHolderInt.mPosition >= i4) {
                    childViewHolderInt.offsetPosition(-i3, z);
                    this.mState.f1922e = true;
                } else if (childViewHolderInt.mPosition >= i2) {
                    childViewHolderInt.flagRemovedAndOffsetPosition(i2 - 1, -i3, z);
                    this.mState.f1922e = true;
                }
            }
        }
        this.mRecycler.a(i2, i3, z);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void viewRangeUpdate(int i2, int i3, Object obj) {
        int c2 = this.mChildHelper.c();
        int i4 = i2 + i3;
        for (int i5 = 0; i5 < c2; i5++) {
            View d2 = this.mChildHelper.d(i5);
            u childViewHolderInt = getChildViewHolderInt(d2);
            if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore() && childViewHolderInt.mPosition >= i2 && childViewHolderInt.mPosition < i4) {
                childViewHolderInt.addFlags(2);
                childViewHolderInt.addChangePayload(obj);
                ((LayoutParams) d2.getLayoutParams()).f1861e = true;
            }
        }
        this.mRecycler.c(i2, i3);
    }

    /* access modifiers changed from: package-private */
    public boolean canReuseUpdatedViewHolder(u uVar) {
        return this.mItemAnimator == null || this.mItemAnimator.a(uVar, uVar.getUnmodifiedPayloads());
    }

    /* access modifiers changed from: package-private */
    public void setDataSetChangedAfterLayout() {
        if (!this.mDataSetHasChangedAfterLayout) {
            this.mDataSetHasChangedAfterLayout = true;
            int c2 = this.mChildHelper.c();
            for (int i2 = 0; i2 < c2; i2++) {
                u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i2));
                if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore()) {
                    childViewHolderInt.addFlags(FileUtils.FileMode.MODE_ISVTX);
                }
            }
            this.mRecycler.h();
            markKnownViewsInvalid();
        }
    }

    /* access modifiers changed from: package-private */
    public void markKnownViewsInvalid() {
        int c2 = this.mChildHelper.c();
        for (int i2 = 0; i2 < c2; i2++) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i2));
            if (childViewHolderInt != null && !childViewHolderInt.shouldIgnore()) {
                childViewHolderInt.addFlags(6);
            }
        }
        markItemDecorInsetsDirty();
        this.mRecycler.i();
    }

    public void invalidateItemDecorations() {
        if (this.mItemDecorations.size() != 0) {
            if (this.mLayout != null) {
                this.mLayout.a("Cannot invalidate item decorations during a scroll or layout");
            }
            markItemDecorInsetsDirty();
            requestLayout();
        }
    }

    public boolean getPreserveFocusAfterLayout() {
        return this.mPreserveFocusAfterLayout;
    }

    public void setPreserveFocusAfterLayout(boolean z) {
        this.mPreserveFocusAfterLayout = z;
    }

    public u getChildViewHolder(View view) {
        ViewParent parent = view.getParent();
        if (parent == null || parent == this) {
            return getChildViewHolderInt(view);
        }
        throw new IllegalArgumentException("View " + view + " is not a direct child of " + this);
    }

    public View findContainingItemView(View view) {
        ViewParent parent = view.getParent();
        View view2 = view;
        while (parent != null && parent != this && (parent instanceof View)) {
            View view3 = (View) parent;
            view2 = view3;
            parent = view3.getParent();
        }
        if (parent == this) {
            return view2;
        }
        return null;
    }

    public u findContainingViewHolder(View view) {
        View findContainingItemView = findContainingItemView(view);
        if (findContainingItemView == null) {
            return null;
        }
        return getChildViewHolder(findContainingItemView);
    }

    static u getChildViewHolderInt(View view) {
        if (view == null) {
            return null;
        }
        return ((LayoutParams) view.getLayoutParams()).f1859c;
    }

    @Deprecated
    public int getChildPosition(View view) {
        return getChildAdapterPosition(view);
    }

    public int getChildAdapterPosition(View view) {
        u childViewHolderInt = getChildViewHolderInt(view);
        if (childViewHolderInt != null) {
            return childViewHolderInt.getAdapterPosition();
        }
        return -1;
    }

    public int getChildLayoutPosition(View view) {
        u childViewHolderInt = getChildViewHolderInt(view);
        if (childViewHolderInt != null) {
            return childViewHolderInt.getLayoutPosition();
        }
        return -1;
    }

    public long getChildItemId(View view) {
        u childViewHolderInt;
        if (this.mAdapter == null || !this.mAdapter.hasStableIds() || (childViewHolderInt = getChildViewHolderInt(view)) == null) {
            return -1;
        }
        return childViewHolderInt.getItemId();
    }

    @Deprecated
    public u findViewHolderForPosition(int i2) {
        return findViewHolderForPosition(i2, false);
    }

    public u findViewHolderForLayoutPosition(int i2) {
        return findViewHolderForPosition(i2, false);
    }

    public u findViewHolderForAdapterPosition(int i2) {
        if (this.mDataSetHasChangedAfterLayout) {
            return null;
        }
        int c2 = this.mChildHelper.c();
        int i3 = 0;
        u uVar = null;
        while (i3 < c2) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i3));
            if (childViewHolderInt == null || childViewHolderInt.isRemoved() || getAdapterPositionFor(childViewHolderInt) != i2) {
                childViewHolderInt = uVar;
            } else if (!this.mChildHelper.c(childViewHolderInt.itemView)) {
                return childViewHolderInt;
            }
            i3++;
            uVar = childViewHolderInt;
        }
        return uVar;
    }

    /* access modifiers changed from: package-private */
    public u findViewHolderForPosition(int i2, boolean z) {
        int c2 = this.mChildHelper.c();
        u uVar = null;
        for (int i3 = 0; i3 < c2; i3++) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i3));
            if (childViewHolderInt != null && !childViewHolderInt.isRemoved()) {
                if (z) {
                    if (childViewHolderInt.mPosition != i2) {
                        continue;
                    }
                } else if (childViewHolderInt.getLayoutPosition() != i2) {
                    continue;
                }
                if (!this.mChildHelper.c(childViewHolderInt.itemView)) {
                    return childViewHolderInt;
                }
                uVar = childViewHolderInt;
            }
        }
        return uVar;
    }

    public u findViewHolderForItemId(long j2) {
        if (this.mAdapter == null || !this.mAdapter.hasStableIds()) {
            return null;
        }
        int c2 = this.mChildHelper.c();
        int i2 = 0;
        u uVar = null;
        while (i2 < c2) {
            u childViewHolderInt = getChildViewHolderInt(this.mChildHelper.d(i2));
            if (childViewHolderInt == null || childViewHolderInt.isRemoved() || childViewHolderInt.getItemId() != j2) {
                childViewHolderInt = uVar;
            } else if (!this.mChildHelper.c(childViewHolderInt.itemView)) {
                return childViewHolderInt;
            }
            i2++;
            uVar = childViewHolderInt;
        }
        return uVar;
    }

    public View findChildViewUnder(float f2, float f3) {
        for (int b2 = this.mChildHelper.b() - 1; b2 >= 0; b2--) {
            View b3 = this.mChildHelper.b(b2);
            float m2 = ag.m(b3);
            float n2 = ag.n(b3);
            if (f2 >= ((float) b3.getLeft()) + m2 && f2 <= m2 + ((float) b3.getRight()) && f3 >= ((float) b3.getTop()) + n2 && f3 <= ((float) b3.getBottom()) + n2) {
                return b3;
            }
        }
        return null;
    }

    public boolean drawChild(Canvas canvas, View view, long j2) {
        return super.drawChild(canvas, view, j2);
    }

    public void offsetChildrenVertical(int i2) {
        int b2 = this.mChildHelper.b();
        for (int i3 = 0; i3 < b2; i3++) {
            this.mChildHelper.b(i3).offsetTopAndBottom(i2);
        }
    }

    public void onChildAttachedToWindow(View view) {
    }

    public void onChildDetachedFromWindow(View view) {
    }

    public void offsetChildrenHorizontal(int i2) {
        int b2 = this.mChildHelper.b();
        for (int i3 = 0; i3 < b2; i3++) {
            this.mChildHelper.b(i3).offsetLeftAndRight(i2);
        }
    }

    public void getDecoratedBoundsWithMargins(View view, Rect rect) {
        getDecoratedBoundsWithMarginsInt(view, rect);
    }

    static void getDecoratedBoundsWithMarginsInt(View view, Rect rect) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        Rect rect2 = layoutParams.f1860d;
        int left = (view.getLeft() - rect2.left) - layoutParams.leftMargin;
        int top = (view.getTop() - rect2.top) - layoutParams.topMargin;
        int right = view.getRight() + rect2.right + layoutParams.rightMargin;
        int bottom = view.getBottom();
        rect.set(left, top, right, layoutParams.bottomMargin + rect2.bottom + bottom);
    }

    /* access modifiers changed from: package-private */
    public Rect getItemDecorInsetsForChild(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (!layoutParams.f1861e) {
            return layoutParams.f1860d;
        }
        if (this.mState.a() && (layoutParams.e() || layoutParams.c())) {
            return layoutParams.f1860d;
        }
        Rect rect = layoutParams.f1860d;
        rect.set(0, 0, 0, 0);
        int size = this.mItemDecorations.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.mTempRect.set(0, 0, 0, 0);
            this.mItemDecorations.get(i2).a(this.mTempRect, view, this, this.mState);
            rect.left += this.mTempRect.left;
            rect.top += this.mTempRect.top;
            rect.right += this.mTempRect.right;
            rect.bottom += this.mTempRect.bottom;
        }
        layoutParams.f1861e = false;
        return rect;
    }

    public void onScrolled(int i2, int i3) {
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnScrolled(int i2, int i3) {
        this.mDispatchScrollCounter++;
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX, scrollY);
        onScrolled(i2, i3);
        if (this.mScrollListener != null) {
            this.mScrollListener.a(this, i2, i3);
        }
        if (this.mScrollListeners != null) {
            for (int size = this.mScrollListeners.size() - 1; size >= 0; size--) {
                this.mScrollListeners.get(size).a(this, i2, i3);
            }
        }
        this.mDispatchScrollCounter--;
    }

    public void onScrollStateChanged(int i2) {
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnScrollStateChanged(int i2) {
        if (this.mLayout != null) {
            this.mLayout.l(i2);
        }
        onScrollStateChanged(i2);
        if (this.mScrollListener != null) {
            this.mScrollListener.a(this, i2);
        }
        if (this.mScrollListeners != null) {
            for (int size = this.mScrollListeners.size() - 1; size >= 0; size--) {
                this.mScrollListeners.get(size).a(this, i2);
            }
        }
    }

    public boolean hasPendingAdapterUpdates() {
        return !this.mFirstLayoutComplete || this.mDataSetHasChangedAfterLayout || this.mAdapterHelper.d();
    }

    class t implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        Interpolator f1926a = RecyclerView.sQuinticInterpolator;

        /* renamed from: c  reason: collision with root package name */
        private int f1928c;

        /* renamed from: d  reason: collision with root package name */
        private int f1929d;

        /* renamed from: e  reason: collision with root package name */
        private android.support.v4.widget.u f1930e;

        /* renamed from: f  reason: collision with root package name */
        private boolean f1931f = false;

        /* renamed from: g  reason: collision with root package name */
        private boolean f1932g = false;

        public t() {
            this.f1930e = android.support.v4.widget.u.a(RecyclerView.this.getContext(), RecyclerView.sQuinticInterpolator);
        }

        /* JADX WARNING: Removed duplicated region for block: B:101:0x01ba  */
        /* JADX WARNING: Removed duplicated region for block: B:102:0x01bd  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00b0  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00be  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00cf  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x00d6  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x00e2  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x010f  */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x0150  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r15 = this;
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$h r0 = r0.mLayout
                if (r0 != 0) goto L_0x000a
                r15.b()
            L_0x0009:
                return
            L_0x000a:
                r15.c()
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                r0.consumePendingUpdateOperations()
                android.support.v4.widget.u r7 = r15.f1930e
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$h r0 = r0.mLayout
                android.support.v7.widget.RecyclerView$q r8 = r0.t
                boolean r0 = r7.g()
                if (r0 == 0) goto L_0x0157
                int r9 = r7.b()
                int r10 = r7.c()
                int r0 = r15.f1928c
                int r11 = r9 - r0
                int r0 = r15.f1929d
                int r12 = r10 - r0
                r3 = 0
                r1 = 0
                r15.f1928c = r9
                r15.f1929d = r10
                r2 = 0
                r0 = 0
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$a r4 = r4.mAdapter
                if (r4 == 0) goto L_0x018e
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                r4.eatRequestLayout()
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                r4.onEnterLayoutOrScroll()
                java.lang.String r4 = "RV Scroll"
                android.support.v4.os.j.a(r4)
                if (r11 == 0) goto L_0x0061
                android.support.v7.widget.RecyclerView r2 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$h r2 = r2.mLayout
                android.support.v7.widget.RecyclerView r3 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$n r3 = r3.mRecycler
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$r r4 = r4.mState
                int r3 = r2.a(r11, r3, r4)
                int r2 = r11 - r3
            L_0x0061:
                if (r12 == 0) goto L_0x0075
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$h r0 = r0.mLayout
                android.support.v7.widget.RecyclerView r1 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$n r1 = r1.mRecycler
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$r r4 = r4.mState
                int r1 = r0.b(r12, r1, r4)
                int r0 = r12 - r1
            L_0x0075:
                android.support.v4.os.j.a()
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                r4.repositionShadowingViews()
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                r4.onExitLayoutOrScroll()
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                r5 = 0
                r4.resumeRequestLayout(r5)
                if (r8 == 0) goto L_0x018e
                boolean r4 = r8.g()
                if (r4 != 0) goto L_0x018e
                boolean r4 = r8.h()
                if (r4 == 0) goto L_0x018e
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$r r4 = r4.mState
                int r4 = r4.e()
                if (r4 != 0) goto L_0x0170
                r8.f()
                r14 = r2
                r2 = r1
                r1 = r14
            L_0x00a6:
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                java.util.ArrayList<android.support.v7.widget.RecyclerView$g> r4 = r4.mItemDecorations
                boolean r4 = r4.isEmpty()
                if (r4 != 0) goto L_0x00b5
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                r4.invalidate()
            L_0x00b5:
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                int r4 = r4.getOverScrollMode()
                r5 = 2
                if (r4 == r5) goto L_0x00c3
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                r4.considerReleasingGlowsOnScroll(r11, r12)
            L_0x00c3:
                if (r1 != 0) goto L_0x00c7
                if (r0 == 0) goto L_0x00fe
            L_0x00c7:
                float r4 = r7.f()
                int r5 = (int) r4
                r4 = 0
                if (r1 == r9) goto L_0x01bd
                if (r1 >= 0) goto L_0x0193
                int r4 = -r5
            L_0x00d2:
                r6 = r4
            L_0x00d3:
                r4 = 0
                if (r0 == r10) goto L_0x01ba
                if (r0 >= 0) goto L_0x019b
                int r5 = -r5
            L_0x00d9:
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                int r4 = r4.getOverScrollMode()
                r13 = 2
                if (r4 == r13) goto L_0x00e7
                android.support.v7.widget.RecyclerView r4 = android.support.v7.widget.RecyclerView.this
                r4.absorbGlows(r6, r5)
            L_0x00e7:
                if (r6 != 0) goto L_0x00f1
                if (r1 == r9) goto L_0x00f1
                int r1 = r7.d()
                if (r1 != 0) goto L_0x00fe
            L_0x00f1:
                if (r5 != 0) goto L_0x00fb
                if (r0 == r10) goto L_0x00fb
                int r0 = r7.e()
                if (r0 != 0) goto L_0x00fe
            L_0x00fb:
                r7.h()
            L_0x00fe:
                if (r3 != 0) goto L_0x0102
                if (r2 == 0) goto L_0x0107
            L_0x0102:
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                r0.dispatchOnScrolled(r3, r2)
            L_0x0107:
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                boolean r0 = r0.awakenScrollBars()
                if (r0 != 0) goto L_0x0114
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                r0.invalidate()
            L_0x0114:
                if (r12 == 0) goto L_0x01a0
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$h r0 = r0.mLayout
                boolean r0 = r0.e()
                if (r0 == 0) goto L_0x01a0
                if (r2 != r12) goto L_0x01a0
                r0 = 1
                r1 = r0
            L_0x0124:
                if (r11 == 0) goto L_0x01a3
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.RecyclerView$h r0 = r0.mLayout
                boolean r0 = r0.d()
                if (r0 == 0) goto L_0x01a3
                if (r3 != r11) goto L_0x01a3
                r0 = 1
            L_0x0133:
                if (r11 != 0) goto L_0x0137
                if (r12 == 0) goto L_0x013b
            L_0x0137:
                if (r0 != 0) goto L_0x013b
                if (r1 == 0) goto L_0x01a5
            L_0x013b:
                r0 = 1
            L_0x013c:
                boolean r1 = r7.a()
                if (r1 != 0) goto L_0x0144
                if (r0 != 0) goto L_0x01a7
            L_0x0144:
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                r1 = 0
                r0.setScrollState(r1)
                boolean r0 = android.support.v7.widget.RecyclerView.ALLOW_THREAD_GAP_WORK
                if (r0 == 0) goto L_0x0157
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.y$a r0 = r0.mPrefetchRegistry
                r0.a()
            L_0x0157:
                if (r8 == 0) goto L_0x016b
                boolean r0 = r8.g()
                if (r0 == 0) goto L_0x0164
                r0 = 0
                r1 = 0
                r8.a(r0, r1)
            L_0x0164:
                boolean r0 = r15.f1932g
                if (r0 != 0) goto L_0x016b
                r8.f()
            L_0x016b:
                r15.d()
                goto L_0x0009
            L_0x0170:
                int r5 = r8.i()
                if (r5 < r4) goto L_0x0187
                int r4 = r4 + -1
                r8.d(r4)
                int r4 = r11 - r2
                int r5 = r12 - r0
                r8.a(r4, r5)
                r14 = r2
                r2 = r1
                r1 = r14
                goto L_0x00a6
            L_0x0187:
                int r4 = r11 - r2
                int r5 = r12 - r0
                r8.a(r4, r5)
            L_0x018e:
                r14 = r2
                r2 = r1
                r1 = r14
                goto L_0x00a6
            L_0x0193:
                if (r1 <= 0) goto L_0x0198
                r4 = r5
                goto L_0x00d2
            L_0x0198:
                r4 = 0
                goto L_0x00d2
            L_0x019b:
                if (r0 > 0) goto L_0x00d9
                r5 = 0
                goto L_0x00d9
            L_0x01a0:
                r0 = 0
                r1 = r0
                goto L_0x0124
            L_0x01a3:
                r0 = 0
                goto L_0x0133
            L_0x01a5:
                r0 = 0
                goto L_0x013c
            L_0x01a7:
                r15.a()
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.y r0 = r0.mGapWorker
                if (r0 == 0) goto L_0x0157
                android.support.v7.widget.RecyclerView r0 = android.support.v7.widget.RecyclerView.this
                android.support.v7.widget.y r0 = r0.mGapWorker
                android.support.v7.widget.RecyclerView r1 = android.support.v7.widget.RecyclerView.this
                r0.a(r1, r11, r12)
                goto L_0x0157
            L_0x01ba:
                r5 = r4
                goto L_0x00d9
            L_0x01bd:
                r6 = r4
                goto L_0x00d3
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.t.run():void");
        }

        private void c() {
            this.f1932g = false;
            this.f1931f = true;
        }

        private void d() {
            this.f1931f = false;
            if (this.f1932g) {
                a();
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (this.f1931f) {
                this.f1932g = true;
                return;
            }
            RecyclerView.this.removeCallbacks(this);
            ag.a(RecyclerView.this, this);
        }

        public void a(int i, int i2) {
            RecyclerView.this.setScrollState(2);
            this.f1929d = 0;
            this.f1928c = 0;
            this.f1930e.a(0, 0, i, i2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
            a();
        }

        public void b(int i, int i2) {
            a(i, i2, 0, 0);
        }

        public void a(int i, int i2, int i3, int i4) {
            a(i, i2, b(i, i2, i3, i4));
        }

        private float a(float f2) {
            return (float) Math.sin((double) ((float) (((double) (f2 - 0.5f)) * 0.4712389167638204d)));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        private int b(int i, int i2, int i3, int i4) {
            int i5;
            int i6;
            int abs = Math.abs(i);
            int abs2 = Math.abs(i2);
            boolean z = abs > abs2;
            int sqrt = (int) Math.sqrt((double) ((i3 * i3) + (i4 * i4)));
            int sqrt2 = (int) Math.sqrt((double) ((i * i) + (i2 * i2)));
            int width = z ? RecyclerView.this.getWidth() : RecyclerView.this.getHeight();
            int i7 = width / 2;
            float a2 = (a(Math.min(1.0f, (((float) sqrt2) * 1.0f) / ((float) width))) * ((float) i7)) + ((float) i7);
            if (sqrt > 0) {
                i6 = Math.round(1000.0f * Math.abs(a2 / ((float) sqrt))) * 4;
            } else {
                if (z) {
                    i5 = abs;
                } else {
                    i5 = abs2;
                }
                i6 = (int) (((((float) i5) / ((float) width)) + 1.0f) * 300.0f);
            }
            return Math.min(i6, 2000);
        }

        public void a(int i, int i2, int i3) {
            a(i, i2, i3, RecyclerView.sQuinticInterpolator);
        }

        public void a(int i, int i2, Interpolator interpolator) {
            int b2 = b(i, i2, 0, 0);
            if (interpolator == null) {
                interpolator = RecyclerView.sQuinticInterpolator;
            }
            a(i, i2, b2, interpolator);
        }

        public void a(int i, int i2, int i3, Interpolator interpolator) {
            if (this.f1926a != interpolator) {
                this.f1926a = interpolator;
                this.f1930e = android.support.v4.widget.u.a(RecyclerView.this.getContext(), interpolator);
            }
            RecyclerView.this.setScrollState(2);
            this.f1929d = 0;
            this.f1928c = 0;
            this.f1930e.a(0, 0, i, i2, i3);
            a();
        }

        public void b() {
            RecyclerView.this.removeCallbacks(this);
            this.f1930e.h();
        }
    }

    /* access modifiers changed from: package-private */
    public void repositionShadowingViews() {
        int b2 = this.mChildHelper.b();
        for (int i2 = 0; i2 < b2; i2++) {
            View b3 = this.mChildHelper.b(i2);
            u childViewHolder = getChildViewHolder(b3);
            if (!(childViewHolder == null || childViewHolder.mShadowingHolder == null)) {
                View view = childViewHolder.mShadowingHolder.itemView;
                int left = b3.getLeft();
                int top = b3.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                }
            }
        }
    }

    private class p extends c {
        p() {
        }

        public void a() {
            RecyclerView.this.assertNotInLayoutOrScroll(null);
            RecyclerView.this.mState.f1922e = true;
            RecyclerView.this.setDataSetChangedAfterLayout();
            if (!RecyclerView.this.mAdapterHelper.d()) {
                RecyclerView.this.requestLayout();
            }
        }

        public void a(int i, int i2, Object obj) {
            RecyclerView.this.assertNotInLayoutOrScroll(null);
            if (RecyclerView.this.mAdapterHelper.a(i, i2, obj)) {
                b();
            }
        }

        public void b(int i, int i2) {
            RecyclerView.this.assertNotInLayoutOrScroll(null);
            if (RecyclerView.this.mAdapterHelper.b(i, i2)) {
                b();
            }
        }

        public void c(int i, int i2) {
            RecyclerView.this.assertNotInLayoutOrScroll(null);
            if (RecyclerView.this.mAdapterHelper.c(i, i2)) {
                b();
            }
        }

        public void a(int i, int i2, int i3) {
            RecyclerView.this.assertNotInLayoutOrScroll(null);
            if (RecyclerView.this.mAdapterHelper.a(i, i2, i3)) {
                b();
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (!RecyclerView.POST_UPDATES_ON_ANIMATION || !RecyclerView.this.mHasFixedSize || !RecyclerView.this.mIsAttached) {
                RecyclerView.this.mAdapterUpdateDuringMeasure = true;
                RecyclerView.this.requestLayout();
                return;
            }
            ag.a(RecyclerView.this, RecyclerView.this.mUpdateChildViewsRunnable);
        }
    }

    public static class m {

        /* renamed from: a  reason: collision with root package name */
        SparseArray<a> f1889a = new SparseArray<>();

        /* renamed from: b  reason: collision with root package name */
        private int f1890b = 0;

        static class a {

            /* renamed from: a  reason: collision with root package name */
            ArrayList<u> f1891a = new ArrayList<>();

            /* renamed from: b  reason: collision with root package name */
            int f1892b = 5;

            /* renamed from: c  reason: collision with root package name */
            long f1893c = 0;

            /* renamed from: d  reason: collision with root package name */
            long f1894d = 0;

            a() {
            }
        }

        public void a() {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f1889a.size()) {
                    this.f1889a.valueAt(i2).f1891a.clear();
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }

        public u a(int i) {
            a aVar = this.f1889a.get(i);
            if (aVar == null || aVar.f1891a.isEmpty()) {
                return null;
            }
            ArrayList<u> arrayList = aVar.f1891a;
            return arrayList.remove(arrayList.size() - 1);
        }

        public void a(u uVar) {
            int itemViewType = uVar.getItemViewType();
            ArrayList<u> arrayList = b(itemViewType).f1891a;
            if (this.f1889a.get(itemViewType).f1892b > arrayList.size()) {
                uVar.resetInternal();
                arrayList.add(uVar);
            }
        }

        /* access modifiers changed from: package-private */
        public long a(long j, long j2) {
            return j == 0 ? j2 : ((j / 4) * 3) + (j2 / 4);
        }

        /* access modifiers changed from: package-private */
        public void a(int i, long j) {
            a b2 = b(i);
            b2.f1893c = a(b2.f1893c, j);
        }

        /* access modifiers changed from: package-private */
        public void b(int i, long j) {
            a b2 = b(i);
            b2.f1894d = a(b2.f1894d, j);
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i, long j, long j2) {
            long j3 = b(i).f1893c;
            return j3 == 0 || j3 + j < j2;
        }

        /* access modifiers changed from: package-private */
        public boolean b(int i, long j, long j2) {
            long j3 = b(i).f1894d;
            return j3 == 0 || j3 + j < j2;
        }

        /* access modifiers changed from: package-private */
        public void a(a aVar) {
            this.f1890b++;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f1890b--;
        }

        /* access modifiers changed from: package-private */
        public void a(a aVar, a aVar2, boolean z) {
            if (aVar != null) {
                b();
            }
            if (!z && this.f1890b == 0) {
                a();
            }
            if (aVar2 != null) {
                a(aVar2);
            }
        }

        private a b(int i) {
            a aVar = this.f1889a.get(i);
            if (aVar != null) {
                return aVar;
            }
            a aVar2 = new a();
            this.f1889a.put(i, aVar2);
            return aVar2;
        }
    }

    static RecyclerView findNestedRecyclerView(View view) {
        if (!(view instanceof ViewGroup)) {
            return null;
        }
        if (view instanceof RecyclerView) {
            return (RecyclerView) view;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            RecyclerView findNestedRecyclerView = findNestedRecyclerView(viewGroup.getChildAt(i2));
            if (findNestedRecyclerView != null) {
                return findNestedRecyclerView;
            }
        }
        return null;
    }

    static void clearNestedRecyclerViewIfNotNested(u uVar) {
        if (uVar.mNestedRecyclerView != null) {
            View view = uVar.mNestedRecyclerView.get();
            while (view != null) {
                if (view != uVar.itemView) {
                    ViewParent parent = view.getParent();
                    view = parent instanceof View ? (View) parent : null;
                } else {
                    return;
                }
            }
            uVar.mNestedRecyclerView = null;
        }
    }

    /* access modifiers changed from: package-private */
    public long getNanoTime() {
        if (ALLOW_THREAD_GAP_WORK) {
            return System.nanoTime();
        }
        return 0;
    }

    public final class n {

        /* renamed from: a  reason: collision with root package name */
        final ArrayList<u> f1895a = new ArrayList<>();

        /* renamed from: b  reason: collision with root package name */
        ArrayList<u> f1896b = null;

        /* renamed from: c  reason: collision with root package name */
        final ArrayList<u> f1897c = new ArrayList<>();

        /* renamed from: d  reason: collision with root package name */
        int f1898d = 2;

        /* renamed from: e  reason: collision with root package name */
        m f1899e;

        /* renamed from: g  reason: collision with root package name */
        private final List<u> f1901g = Collections.unmodifiableList(this.f1895a);

        /* renamed from: h  reason: collision with root package name */
        private int f1902h = 2;
        private s i;

        public n() {
        }

        public void a() {
            this.f1895a.clear();
            d();
        }

        public void a(int i2) {
            this.f1902h = i2;
            b();
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f1898d = (RecyclerView.this.mLayout != null ? RecyclerView.this.mLayout.x : 0) + this.f1902h;
            for (int size = this.f1897c.size() - 1; size >= 0 && this.f1897c.size() > this.f1898d; size--) {
                d(size);
            }
        }

        public List<u> c() {
            return this.f1901g;
        }

        /* access modifiers changed from: package-private */
        public boolean a(u uVar) {
            if (uVar.isRemoved()) {
                return RecyclerView.this.mState.a();
            }
            if (uVar.mPosition < 0 || uVar.mPosition >= RecyclerView.this.mAdapter.getItemCount()) {
                throw new IndexOutOfBoundsException("Inconsistency detected. Invalid view holder adapter position" + uVar);
            } else if (!RecyclerView.this.mState.a() && RecyclerView.this.mAdapter.getItemViewType(uVar.mPosition) != uVar.getItemViewType()) {
                return false;
            } else {
                if (!RecyclerView.this.mAdapter.hasStableIds() || uVar.getItemId() == RecyclerView.this.mAdapter.getItemId(uVar.mPosition)) {
                    return true;
                }
                return false;
            }
        }

        private boolean a(u uVar, int i2, int i3, long j) {
            uVar.mOwnerRecyclerView = RecyclerView.this;
            int itemViewType = uVar.getItemViewType();
            long nanoTime = RecyclerView.this.getNanoTime();
            if (j != RecyclerView.FOREVER_NS && !this.f1899e.b(itemViewType, nanoTime, j)) {
                return false;
            }
            RecyclerView.this.mAdapter.bindViewHolder(uVar, i2);
            this.f1899e.b(uVar.getItemViewType(), RecyclerView.this.getNanoTime() - nanoTime);
            d(uVar.itemView);
            if (RecyclerView.this.mState.a()) {
                uVar.mPreLayoutPosition = i3;
            }
            return true;
        }

        public int b(int i2) {
            if (i2 >= 0 && i2 < RecyclerView.this.mState.e()) {
                return !RecyclerView.this.mState.a() ? i2 : RecyclerView.this.mAdapterHelper.b(i2);
            }
            throw new IndexOutOfBoundsException("invalid position " + i2 + ". State " + "item count is " + RecyclerView.this.mState.e());
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.RecyclerView.n.a(int, boolean):android.view.View
         arg types: [int, int]
         candidates:
          android.support.v7.widget.RecyclerView.n.a(android.view.ViewGroup, boolean):void
          android.support.v7.widget.RecyclerView.n.a(int, int):void
          android.support.v7.widget.RecyclerView.n.a(android.support.v7.widget.RecyclerView$u, boolean):void
          android.support.v7.widget.RecyclerView.n.a(int, boolean):android.view.View */
        public View c(int i2) {
            return a(i2, false);
        }

        /* access modifiers changed from: package-private */
        public View a(int i2, boolean z) {
            return a(i2, z, (long) RecyclerView.FOREVER_NS).itemView;
        }

        /* access modifiers changed from: package-private */
        public u a(int i2, boolean z, long j) {
            u uVar;
            boolean z2;
            u uVar2;
            boolean z3;
            boolean a2;
            LayoutParams layoutParams;
            boolean z4;
            RecyclerView findNestedRecyclerView;
            View a3;
            boolean z5 = true;
            if (i2 < 0 || i2 >= RecyclerView.this.mState.e()) {
                throw new IndexOutOfBoundsException("Invalid item position " + i2 + "(" + i2 + "). Item count:" + RecyclerView.this.mState.e());
            }
            if (RecyclerView.this.mState.a()) {
                u f2 = f(i2);
                z2 = f2 != null;
                uVar = f2;
            } else {
                uVar = null;
                z2 = false;
            }
            if (uVar == null && (uVar = b(i2, z)) != null) {
                if (!a(uVar)) {
                    if (!z) {
                        uVar.addFlags(4);
                        if (uVar.isScrap()) {
                            RecyclerView.this.removeDetachedView(uVar.itemView, false);
                            uVar.unScrap();
                        } else if (uVar.wasReturnedFromScrap()) {
                            uVar.clearReturnedFromScrapFlag();
                        }
                        b(uVar);
                    }
                    uVar = null;
                } else {
                    z2 = true;
                }
            }
            if (uVar == null) {
                int b2 = RecyclerView.this.mAdapterHelper.b(i2);
                if (b2 < 0 || b2 >= RecyclerView.this.mAdapter.getItemCount()) {
                    throw new IndexOutOfBoundsException("Inconsistency detected. Invalid item position " + i2 + "(offset:" + b2 + ")." + "state:" + RecyclerView.this.mState.e());
                }
                int itemViewType = RecyclerView.this.mAdapter.getItemViewType(b2);
                if (!RecyclerView.this.mAdapter.hasStableIds() || (uVar = a(RecyclerView.this.mAdapter.getItemId(b2), itemViewType, z)) == null) {
                    z4 = z2;
                } else {
                    uVar.mPosition = b2;
                    z4 = true;
                }
                if (!(uVar != null || this.i == null || (a3 = this.i.a(this, i2, itemViewType)) == null)) {
                    uVar = RecyclerView.this.getChildViewHolder(a3);
                    if (uVar == null) {
                        throw new IllegalArgumentException("getViewForPositionAndType returned a view which does not have a ViewHolder");
                    } else if (uVar.shouldIgnore()) {
                        throw new IllegalArgumentException("getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view.");
                    }
                }
                if (uVar == null && (uVar = g().a(itemViewType)) != null) {
                    uVar.resetInternal();
                    if (RecyclerView.FORCE_INVALIDATE_DISPLAY_LIST) {
                        e(uVar);
                    }
                }
                if (uVar == null) {
                    long nanoTime = RecyclerView.this.getNanoTime();
                    if (j != RecyclerView.FOREVER_NS && !this.f1899e.a(itemViewType, nanoTime, j)) {
                        return null;
                    }
                    uVar = RecyclerView.this.mAdapter.createViewHolder(RecyclerView.this, itemViewType);
                    if (RecyclerView.ALLOW_THREAD_GAP_WORK && (findNestedRecyclerView = RecyclerView.findNestedRecyclerView(uVar.itemView)) != null) {
                        uVar.mNestedRecyclerView = new WeakReference<>(findNestedRecyclerView);
                    }
                    this.f1899e.a(itemViewType, RecyclerView.this.getNanoTime() - nanoTime);
                }
                uVar2 = uVar;
                z3 = z4;
            } else {
                uVar2 = uVar;
                z3 = z2;
            }
            if (z3 && !RecyclerView.this.mState.a() && uVar2.hasAnyOfTheFlags(8192)) {
                uVar2.setFlags(0, 8192);
                if (RecyclerView.this.mState.i) {
                    RecyclerView.this.recordAnimationInfoIfBouncedHiddenView(uVar2, RecyclerView.this.mItemAnimator.a(RecyclerView.this.mState, uVar2, e.e(uVar2) | CodedOutputStream.DEFAULT_BUFFER_SIZE, uVar2.getUnmodifiedPayloads()));
                }
            }
            if (RecyclerView.this.mState.a() && uVar2.isBound()) {
                uVar2.mPreLayoutPosition = i2;
                a2 = false;
            } else if (!uVar2.isBound() || uVar2.needsUpdate() || uVar2.isInvalid()) {
                a2 = a(uVar2, RecyclerView.this.mAdapterHelper.b(i2), i2, j);
            } else {
                a2 = false;
            }
            ViewGroup.LayoutParams layoutParams2 = uVar2.itemView.getLayoutParams();
            if (layoutParams2 == null) {
                layoutParams = (LayoutParams) RecyclerView.this.generateDefaultLayoutParams();
                uVar2.itemView.setLayoutParams(layoutParams);
            } else if (!RecyclerView.this.checkLayoutParams(layoutParams2)) {
                layoutParams = (LayoutParams) RecyclerView.this.generateLayoutParams(layoutParams2);
                uVar2.itemView.setLayoutParams(layoutParams);
            } else {
                layoutParams = (LayoutParams) layoutParams2;
            }
            layoutParams.f1859c = uVar2;
            if (!z3 || !a2) {
                z5 = false;
            }
            layoutParams.f1862f = z5;
            return uVar2;
        }

        private void d(View view) {
            if (RecyclerView.this.isAccessibilityEnabled()) {
                if (ag.d(view) == 0) {
                    ag.c(view, 1);
                }
                if (!ag.a(view)) {
                    ag.a(view, RecyclerView.this.mAccessibilityDelegate.b());
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.RecyclerView.n.a(android.view.ViewGroup, boolean):void
         arg types: [android.view.ViewGroup, int]
         candidates:
          android.support.v7.widget.RecyclerView.n.a(int, boolean):android.view.View
          android.support.v7.widget.RecyclerView.n.a(int, int):void
          android.support.v7.widget.RecyclerView.n.a(android.support.v7.widget.RecyclerView$u, boolean):void
          android.support.v7.widget.RecyclerView.n.a(android.view.ViewGroup, boolean):void */
        private void e(u uVar) {
            if (uVar.itemView instanceof ViewGroup) {
                a((ViewGroup) uVar.itemView, false);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.RecyclerView.n.a(android.view.ViewGroup, boolean):void
         arg types: [android.view.ViewGroup, int]
         candidates:
          android.support.v7.widget.RecyclerView.n.a(int, boolean):android.view.View
          android.support.v7.widget.RecyclerView.n.a(int, int):void
          android.support.v7.widget.RecyclerView.n.a(android.support.v7.widget.RecyclerView$u, boolean):void
          android.support.v7.widget.RecyclerView.n.a(android.view.ViewGroup, boolean):void */
        private void a(ViewGroup viewGroup, boolean z) {
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (childAt instanceof ViewGroup) {
                    a((ViewGroup) childAt, true);
                }
            }
            if (z) {
                if (viewGroup.getVisibility() == 4) {
                    viewGroup.setVisibility(0);
                    viewGroup.setVisibility(4);
                    return;
                }
                int visibility = viewGroup.getVisibility();
                viewGroup.setVisibility(4);
                viewGroup.setVisibility(visibility);
            }
        }

        public void a(View view) {
            u childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (childViewHolderInt.isTmpDetached()) {
                RecyclerView.this.removeDetachedView(view, false);
            }
            if (childViewHolderInt.isScrap()) {
                childViewHolderInt.unScrap();
            } else if (childViewHolderInt.wasReturnedFromScrap()) {
                childViewHolderInt.clearReturnedFromScrapFlag();
            }
            b(childViewHolderInt);
        }

        /* access modifiers changed from: package-private */
        public void d() {
            for (int size = this.f1897c.size() - 1; size >= 0; size--) {
                d(size);
            }
            this.f1897c.clear();
            if (RecyclerView.ALLOW_THREAD_GAP_WORK) {
                RecyclerView.this.mPrefetchRegistry.a();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.RecyclerView.n.a(android.support.v7.widget.RecyclerView$u, boolean):void
         arg types: [android.support.v7.widget.RecyclerView$u, int]
         candidates:
          android.support.v7.widget.RecyclerView.n.a(android.view.ViewGroup, boolean):void
          android.support.v7.widget.RecyclerView.n.a(int, boolean):android.view.View
          android.support.v7.widget.RecyclerView.n.a(int, int):void
          android.support.v7.widget.RecyclerView.n.a(android.support.v7.widget.RecyclerView$u, boolean):void */
        /* access modifiers changed from: package-private */
        public void d(int i2) {
            a(this.f1897c.get(i2), true);
            this.f1897c.remove(i2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.RecyclerView.n.a(android.support.v7.widget.RecyclerView$u, boolean):void
         arg types: [android.support.v7.widget.RecyclerView$u, int]
         candidates:
          android.support.v7.widget.RecyclerView.n.a(android.view.ViewGroup, boolean):void
          android.support.v7.widget.RecyclerView.n.a(int, boolean):android.view.View
          android.support.v7.widget.RecyclerView.n.a(int, int):void
          android.support.v7.widget.RecyclerView.n.a(android.support.v7.widget.RecyclerView$u, boolean):void */
        /* access modifiers changed from: package-private */
        public void b(u uVar) {
            boolean z;
            boolean z2 = false;
            if (uVar.isScrap() || uVar.itemView.getParent() != null) {
                throw new IllegalArgumentException("Scrapped or attached views may not be recycled. isScrap:" + uVar.isScrap() + " isAttached:" + (uVar.itemView.getParent() != null));
            } else if (uVar.isTmpDetached()) {
                throw new IllegalArgumentException("Tmp detached view should be removed from RecyclerView before it can be recycled: " + uVar);
            } else if (uVar.shouldIgnore()) {
                throw new IllegalArgumentException("Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle.");
            } else {
                boolean access$700 = uVar.doesTransientStatePreventRecycling();
                if ((RecyclerView.this.mAdapter != null && access$700 && RecyclerView.this.mAdapter.onFailedToRecycleView(uVar)) || uVar.isRecyclable()) {
                    if (this.f1898d <= 0 || uVar.hasAnyOfTheFlags(526)) {
                        z = false;
                    } else {
                        int size = this.f1897c.size();
                        if (size >= this.f1898d && size > 0) {
                            d(0);
                            size--;
                        }
                        if (RecyclerView.ALLOW_THREAD_GAP_WORK && size > 0 && !RecyclerView.this.mPrefetchRegistry.a(uVar.mPosition)) {
                            int i2 = size - 1;
                            while (i2 >= 0) {
                                if (!RecyclerView.this.mPrefetchRegistry.a(this.f1897c.get(i2).mPosition)) {
                                    break;
                                }
                                i2--;
                            }
                            size = i2 + 1;
                        }
                        this.f1897c.add(size, uVar);
                        z = true;
                    }
                    if (!z) {
                        a(uVar, true);
                        z2 = true;
                    }
                } else {
                    z = false;
                }
                RecyclerView.this.mViewInfoStore.g(uVar);
                if (!z && !z2 && access$700) {
                    uVar.mOwnerRecyclerView = null;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(u uVar, boolean z) {
            RecyclerView.clearNestedRecyclerViewIfNotNested(uVar);
            ag.a(uVar.itemView, (android.support.v4.view.a) null);
            if (z) {
                d(uVar);
            }
            uVar.mOwnerRecyclerView = null;
            g().a(uVar);
        }

        /* access modifiers changed from: package-private */
        public void b(View view) {
            u childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            n unused = childViewHolderInt.mScrapContainer = null;
            boolean unused2 = childViewHolderInt.mInChangeScrap = false;
            childViewHolderInt.clearReturnedFromScrapFlag();
            b(childViewHolderInt);
        }

        /* access modifiers changed from: package-private */
        public void c(View view) {
            u childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (!childViewHolderInt.hasAnyOfTheFlags(12) && childViewHolderInt.isUpdated() && !RecyclerView.this.canReuseUpdatedViewHolder(childViewHolderInt)) {
                if (this.f1896b == null) {
                    this.f1896b = new ArrayList<>();
                }
                childViewHolderInt.setScrapContainer(this, true);
                this.f1896b.add(childViewHolderInt);
            } else if (!childViewHolderInt.isInvalid() || childViewHolderInt.isRemoved() || RecyclerView.this.mAdapter.hasStableIds()) {
                childViewHolderInt.setScrapContainer(this, false);
                this.f1895a.add(childViewHolderInt);
            } else {
                throw new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool.");
            }
        }

        /* access modifiers changed from: package-private */
        public void c(u uVar) {
            if (uVar.mInChangeScrap) {
                this.f1896b.remove(uVar);
            } else {
                this.f1895a.remove(uVar);
            }
            n unused = uVar.mScrapContainer = null;
            boolean unused2 = uVar.mInChangeScrap = false;
            uVar.clearReturnedFromScrapFlag();
        }

        /* access modifiers changed from: package-private */
        public int e() {
            return this.f1895a.size();
        }

        /* access modifiers changed from: package-private */
        public View e(int i2) {
            return this.f1895a.get(i2).itemView;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            this.f1895a.clear();
            if (this.f1896b != null) {
                this.f1896b.clear();
            }
        }

        /* access modifiers changed from: package-private */
        public u f(int i2) {
            int size;
            int b2;
            int i3 = 0;
            if (this.f1896b == null || (size = this.f1896b.size()) == 0) {
                return null;
            }
            int i4 = 0;
            while (i4 < size) {
                u uVar = this.f1896b.get(i4);
                if (uVar.wasReturnedFromScrap() || uVar.getLayoutPosition() != i2) {
                    i4++;
                } else {
                    uVar.addFlags(32);
                    return uVar;
                }
            }
            if (RecyclerView.this.mAdapter.hasStableIds() && (b2 = RecyclerView.this.mAdapterHelper.b(i2)) > 0 && b2 < RecyclerView.this.mAdapter.getItemCount()) {
                long itemId = RecyclerView.this.mAdapter.getItemId(b2);
                while (i3 < size) {
                    u uVar2 = this.f1896b.get(i3);
                    if (uVar2.wasReturnedFromScrap() || uVar2.getItemId() != itemId) {
                        i3++;
                    } else {
                        uVar2.addFlags(32);
                        return uVar2;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public u b(int i2, boolean z) {
            View c2;
            int i3 = 0;
            int size = this.f1895a.size();
            int i4 = 0;
            while (i4 < size) {
                u uVar = this.f1895a.get(i4);
                if (uVar.wasReturnedFromScrap() || uVar.getLayoutPosition() != i2 || uVar.isInvalid() || (!RecyclerView.this.mState.f1923f && uVar.isRemoved())) {
                    i4++;
                } else {
                    uVar.addFlags(32);
                    return uVar;
                }
            }
            if (z || (c2 = RecyclerView.this.mChildHelper.c(i2)) == null) {
                int size2 = this.f1897c.size();
                while (i3 < size2) {
                    u uVar2 = this.f1897c.get(i3);
                    if (uVar2.isInvalid() || uVar2.getLayoutPosition() != i2) {
                        i3++;
                    } else if (z) {
                        return uVar2;
                    } else {
                        this.f1897c.remove(i3);
                        return uVar2;
                    }
                }
                return null;
            }
            u childViewHolderInt = RecyclerView.getChildViewHolderInt(c2);
            RecyclerView.this.mChildHelper.e(c2);
            int b2 = RecyclerView.this.mChildHelper.b(c2);
            if (b2 == -1) {
                throw new IllegalStateException("layout index should not be -1 after unhiding a view:" + childViewHolderInt);
            }
            RecyclerView.this.mChildHelper.e(b2);
            c(c2);
            childViewHolderInt.addFlags(8224);
            return childViewHolderInt;
        }

        /* access modifiers changed from: package-private */
        public u a(long j, int i2, boolean z) {
            for (int size = this.f1895a.size() - 1; size >= 0; size--) {
                u uVar = this.f1895a.get(size);
                if (uVar.getItemId() == j && !uVar.wasReturnedFromScrap()) {
                    if (i2 == uVar.getItemViewType()) {
                        uVar.addFlags(32);
                        if (!uVar.isRemoved() || RecyclerView.this.mState.a()) {
                            return uVar;
                        }
                        uVar.setFlags(2, 14);
                        return uVar;
                    } else if (!z) {
                        this.f1895a.remove(size);
                        RecyclerView.this.removeDetachedView(uVar.itemView, false);
                        b(uVar.itemView);
                    }
                }
            }
            for (int size2 = this.f1897c.size() - 1; size2 >= 0; size2--) {
                u uVar2 = this.f1897c.get(size2);
                if (uVar2.getItemId() == j) {
                    if (i2 == uVar2.getItemViewType()) {
                        if (z) {
                            return uVar2;
                        }
                        this.f1897c.remove(size2);
                        return uVar2;
                    } else if (!z) {
                        d(size2);
                        return null;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public void d(u uVar) {
            if (RecyclerView.this.mRecyclerListener != null) {
                RecyclerView.this.mRecyclerListener.a(uVar);
            }
            if (RecyclerView.this.mAdapter != null) {
                RecyclerView.this.mAdapter.onViewRecycled(uVar);
            }
            if (RecyclerView.this.mState != null) {
                RecyclerView.this.mViewInfoStore.g(uVar);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(a aVar, a aVar2, boolean z) {
            a();
            g().a(aVar, aVar2, z);
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3) {
            int i4;
            int i5;
            int i6;
            if (i2 < i3) {
                i4 = -1;
                i5 = i3;
                i6 = i2;
            } else {
                i4 = 1;
                i5 = i2;
                i6 = i3;
            }
            int size = this.f1897c.size();
            for (int i7 = 0; i7 < size; i7++) {
                u uVar = this.f1897c.get(i7);
                if (uVar != null && uVar.mPosition >= i6 && uVar.mPosition <= i5) {
                    if (uVar.mPosition == i2) {
                        uVar.offsetPosition(i3 - i2, false);
                    } else {
                        uVar.offsetPosition(i4, false);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i2, int i3) {
            int size = this.f1897c.size();
            for (int i4 = 0; i4 < size; i4++) {
                u uVar = this.f1897c.get(i4);
                if (uVar != null && uVar.mPosition >= i2) {
                    uVar.offsetPosition(i3, true);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3, boolean z) {
            int i4 = i2 + i3;
            for (int size = this.f1897c.size() - 1; size >= 0; size--) {
                u uVar = this.f1897c.get(size);
                if (uVar != null) {
                    if (uVar.mPosition >= i4) {
                        uVar.offsetPosition(-i3, z);
                    } else if (uVar.mPosition >= i2) {
                        uVar.addFlags(8);
                        d(size);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(s sVar) {
            this.i = sVar;
        }

        /* access modifiers changed from: package-private */
        public void a(m mVar) {
            if (this.f1899e != null) {
                this.f1899e.b();
            }
            this.f1899e = mVar;
            if (mVar != null) {
                this.f1899e.a(RecyclerView.this.getAdapter());
            }
        }

        /* access modifiers changed from: package-private */
        public m g() {
            if (this.f1899e == null) {
                this.f1899e = new m();
            }
            return this.f1899e;
        }

        /* access modifiers changed from: package-private */
        public void c(int i2, int i3) {
            int i4;
            int i5 = i2 + i3;
            for (int size = this.f1897c.size() - 1; size >= 0; size--) {
                u uVar = this.f1897c.get(size);
                if (uVar != null && (i4 = uVar.mPosition) >= i2 && i4 < i5) {
                    uVar.addFlags(2);
                    d(size);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void h() {
            int size = this.f1897c.size();
            for (int i2 = 0; i2 < size; i2++) {
                u uVar = this.f1897c.get(i2);
                if (uVar != null) {
                    uVar.addFlags(FileUtils.FileMode.MODE_ISVTX);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void i() {
            if (RecyclerView.this.mAdapter == null || !RecyclerView.this.mAdapter.hasStableIds()) {
                d();
                return;
            }
            int size = this.f1897c.size();
            for (int i2 = 0; i2 < size; i2++) {
                u uVar = this.f1897c.get(i2);
                if (uVar != null) {
                    uVar.addFlags(6);
                    uVar.addChangePayload(null);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void j() {
            int size = this.f1897c.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f1897c.get(i2).clearOldPosition();
            }
            int size2 = this.f1895a.size();
            for (int i3 = 0; i3 < size2; i3++) {
                this.f1895a.get(i3).clearOldPosition();
            }
            if (this.f1896b != null) {
                int size3 = this.f1896b.size();
                for (int i4 = 0; i4 < size3; i4++) {
                    this.f1896b.get(i4).clearOldPosition();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void k() {
            int size = this.f1897c.size();
            for (int i2 = 0; i2 < size; i2++) {
                LayoutParams layoutParams = (LayoutParams) this.f1897c.get(i2).itemView.getLayoutParams();
                if (layoutParams != null) {
                    layoutParams.f1861e = true;
                }
            }
        }
    }

    public static abstract class a<VH extends u> {
        private boolean mHasStableIds = false;
        private final b mObservable = new b();

        public abstract int getItemCount();

        public abstract void onBindViewHolder(u uVar, int i);

        public abstract VH onCreateViewHolder(ViewGroup viewGroup, int i);

        public void onBindViewHolder(VH vh, int i, List<Object> list) {
            onBindViewHolder(vh, i);
        }

        public final VH createViewHolder(ViewGroup viewGroup, int i) {
            android.support.v4.os.j.a(RecyclerView.TRACE_CREATE_VIEW_TAG);
            VH onCreateViewHolder = onCreateViewHolder(viewGroup, i);
            onCreateViewHolder.mItemViewType = i;
            android.support.v4.os.j.a();
            return onCreateViewHolder;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: VH
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public final void bindViewHolder(VH r4, int r5) {
            /*
                r3 = this;
                r2 = 1
                r4.mPosition = r5
                boolean r0 = r3.hasStableIds()
                if (r0 == 0) goto L_0x000f
                long r0 = r3.getItemId(r5)
                r4.mItemId = r0
            L_0x000f:
                r0 = 519(0x207, float:7.27E-43)
                r4.setFlags(r2, r0)
                java.lang.String r0 = "RV OnBindView"
                android.support.v4.os.j.a(r0)
                java.util.List r0 = r4.getUnmodifiedPayloads()
                r3.onBindViewHolder(r4, r5, r0)
                r4.clearPayload()
                android.view.View r0 = r4.itemView
                android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
                boolean r1 = r0 instanceof android.support.v7.widget.RecyclerView.LayoutParams
                if (r1 == 0) goto L_0x0031
                android.support.v7.widget.RecyclerView$LayoutParams r0 = (android.support.v7.widget.RecyclerView.LayoutParams) r0
                r0.f1861e = r2
            L_0x0031:
                android.support.v4.os.j.a()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.RecyclerView.a.bindViewHolder(android.support.v7.widget.RecyclerView$u, int):void");
        }

        public int getItemViewType(int i) {
            return 0;
        }

        public void setHasStableIds(boolean z) {
            if (hasObservers()) {
                throw new IllegalStateException("Cannot change whether this adapter has stable IDs while the adapter has registered observers.");
            }
            this.mHasStableIds = z;
        }

        public long getItemId(int i) {
            return -1;
        }

        public final boolean hasStableIds() {
            return this.mHasStableIds;
        }

        public void onViewRecycled(u uVar) {
        }

        public boolean onFailedToRecycleView(VH vh) {
            return false;
        }

        public void onViewAttachedToWindow(VH vh) {
        }

        public void onViewDetachedFromWindow(VH vh) {
        }

        public final boolean hasObservers() {
            return this.mObservable.a();
        }

        public void registerAdapterDataObserver(c cVar) {
            this.mObservable.registerObserver(cVar);
        }

        public void unregisterAdapterDataObserver(c cVar) {
            this.mObservable.unregisterObserver(cVar);
        }

        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        }

        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        }

        public final void notifyDataSetChanged() {
            this.mObservable.b();
        }

        public final void notifyItemChanged(int i) {
            this.mObservable.a(i, 1);
        }

        public final void notifyItemChanged(int i, Object obj) {
            this.mObservable.a(i, 1, obj);
        }

        public final void notifyItemRangeChanged(int i, int i2) {
            this.mObservable.a(i, i2);
        }

        public final void notifyItemRangeChanged(int i, int i2, Object obj) {
            this.mObservable.a(i, i2, obj);
        }

        public final void notifyItemInserted(int i) {
            this.mObservable.b(i, 1);
        }

        public final void notifyItemMoved(int i, int i2) {
            this.mObservable.d(i, i2);
        }

        public final void notifyItemRangeInserted(int i, int i2) {
            this.mObservable.b(i, i2);
        }

        public final void notifyItemRemoved(int i) {
            this.mObservable.c(i, 1);
        }

        public final void notifyItemRangeRemoved(int i, int i2) {
            this.mObservable.c(i, i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchChildDetached(View view) {
        u childViewHolderInt = getChildViewHolderInt(view);
        onChildDetachedFromWindow(view);
        if (!(this.mAdapter == null || childViewHolderInt == null)) {
            this.mAdapter.onViewDetachedFromWindow(childViewHolderInt);
        }
        if (this.mOnChildAttachStateListeners != null) {
            for (int size = this.mOnChildAttachStateListeners.size() - 1; size >= 0; size--) {
                this.mOnChildAttachStateListeners.get(size).b(view);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchChildAttached(View view) {
        u childViewHolderInt = getChildViewHolderInt(view);
        onChildAttachedToWindow(view);
        if (!(this.mAdapter == null || childViewHolderInt == null)) {
            this.mAdapter.onViewAttachedToWindow(childViewHolderInt);
        }
        if (this.mOnChildAttachStateListeners != null) {
            for (int size = this.mOnChildAttachStateListeners.size() - 1; size >= 0; size--) {
                this.mOnChildAttachStateListeners.get(size).a(view);
            }
        }
    }

    public static abstract class h {

        /* renamed from: a  reason: collision with root package name */
        private final ap.b f1875a = new ap.b() {
            public View a(int i) {
                return h.this.i(i);
            }

            public int a() {
                return h.this.z();
            }

            public int b() {
                return h.this.x() - h.this.B();
            }

            public int a(View view) {
                return h.this.h(view) - ((LayoutParams) view.getLayoutParams()).leftMargin;
            }

            public int b(View view) {
                return ((LayoutParams) view.getLayoutParams()).rightMargin + h.this.j(view);
            }
        };

        /* renamed from: b  reason: collision with root package name */
        private final ap.b f1876b = new ap.b() {
            public View a(int i) {
                return h.this.i(i);
            }

            public int a() {
                return h.this.A();
            }

            public int b() {
                return h.this.y() - h.this.C();
            }

            public int a(View view) {
                return h.this.i(view) - ((LayoutParams) view.getLayoutParams()).topMargin;
            }

            public int b(View view) {
                return ((LayoutParams) view.getLayoutParams()).bottomMargin + h.this.k(view);
            }
        };

        /* renamed from: c  reason: collision with root package name */
        private boolean f1877c = true;

        /* renamed from: d  reason: collision with root package name */
        private boolean f1878d = true;

        /* renamed from: e  reason: collision with root package name */
        private int f1879e;

        /* renamed from: f  reason: collision with root package name */
        private int f1880f;

        /* renamed from: g  reason: collision with root package name */
        private int f1881g;

        /* renamed from: h  reason: collision with root package name */
        private int f1882h;
        r p;
        RecyclerView q;
        ap r = new ap(this.f1875a);
        ap s = new ap(this.f1876b);
        q t;
        boolean u = false;
        boolean v = false;
        boolean w = false;
        int x;
        boolean y;

        public interface a {
            void b(int i, int i2);
        }

        public static class b {

            /* renamed from: a  reason: collision with root package name */
            public int f1885a;

            /* renamed from: b  reason: collision with root package name */
            public int f1886b;

            /* renamed from: c  reason: collision with root package name */
            public boolean f1887c;

            /* renamed from: d  reason: collision with root package name */
            public boolean f1888d;
        }

        public abstract LayoutParams a();

        /* access modifiers changed from: package-private */
        public void b(RecyclerView recyclerView) {
            if (recyclerView == null) {
                this.q = null;
                this.p = null;
                this.f1881g = 0;
                this.f1882h = 0;
            } else {
                this.q = recyclerView;
                this.p = recyclerView.mChildHelper;
                this.f1881g = recyclerView.getWidth();
                this.f1882h = recyclerView.getHeight();
            }
            this.f1879e = 1073741824;
            this.f1880f = 1073741824;
        }

        /* access modifiers changed from: package-private */
        public void d(int i, int i2) {
            this.f1881g = View.MeasureSpec.getSize(i);
            this.f1879e = View.MeasureSpec.getMode(i);
            if (this.f1879e == 0 && !RecyclerView.ALLOW_SIZE_IN_UNSPECIFIED_SPEC) {
                this.f1881g = 0;
            }
            this.f1882h = View.MeasureSpec.getSize(i2);
            this.f1880f = View.MeasureSpec.getMode(i2);
            if (this.f1880f == 0 && !RecyclerView.ALLOW_SIZE_IN_UNSPECIFIED_SPEC) {
                this.f1882h = 0;
            }
        }

        /* access modifiers changed from: package-private */
        public void e(int i, int i2) {
            int i3 = Integer.MAX_VALUE;
            int i4 = Integer.MIN_VALUE;
            int u2 = u();
            if (u2 == 0) {
                this.q.defaultOnMeasure(i, i2);
                return;
            }
            int i5 = Integer.MIN_VALUE;
            int i6 = Integer.MAX_VALUE;
            for (int i7 = 0; i7 < u2; i7++) {
                View i8 = i(i7);
                Rect rect = this.q.mTempRect;
                a(i8, rect);
                if (rect.left < i6) {
                    i6 = rect.left;
                }
                if (rect.right > i5) {
                    i5 = rect.right;
                }
                if (rect.top < i3) {
                    i3 = rect.top;
                }
                if (rect.bottom > i4) {
                    i4 = rect.bottom;
                }
            }
            this.q.mTempRect.set(i6, i3, i5, i4);
            a(this.q.mTempRect, i, i2);
        }

        public void a(Rect rect, int i, int i2) {
            g(a(i, rect.width() + z() + B(), F()), a(i2, rect.height() + A() + C(), G()));
        }

        public void n() {
            if (this.q != null) {
                this.q.requestLayout();
            }
        }

        public static int a(int i, int i2, int i3) {
            int mode = View.MeasureSpec.getMode(i);
            int size = View.MeasureSpec.getSize(i);
            switch (mode) {
                case Integer.MIN_VALUE:
                    return Math.min(size, Math.max(i2, i3));
                case 1073741824:
                    return size;
                default:
                    return Math.max(i2, i3);
            }
        }

        public void a(String str) {
            if (this.q != null) {
                this.q.assertNotInLayoutOrScroll(str);
            }
        }

        public void c(boolean z) {
            this.w = z;
        }

        public boolean b() {
            return false;
        }

        public final boolean o() {
            return this.f1878d;
        }

        public void a(int i, int i2, r rVar, a aVar) {
        }

        public void a(int i, a aVar) {
        }

        /* access modifiers changed from: package-private */
        public void c(RecyclerView recyclerView) {
            this.v = true;
            d(recyclerView);
        }

        /* access modifiers changed from: package-private */
        public void b(RecyclerView recyclerView, n nVar) {
            this.v = false;
            a(recyclerView, nVar);
        }

        public boolean p() {
            return this.v;
        }

        public boolean a(Runnable runnable) {
            if (this.q != null) {
                return this.q.removeCallbacks(runnable);
            }
            return false;
        }

        public void d(RecyclerView recyclerView) {
        }

        @Deprecated
        public void e(RecyclerView recyclerView) {
        }

        public void a(RecyclerView recyclerView, n nVar) {
            e(recyclerView);
        }

        public boolean q() {
            return this.q != null && this.q.mClipToPadding;
        }

        public void c(n nVar, r rVar) {
            Log.e(RecyclerView.TAG, "You must override onLayoutChildren(Recycler recycler, State state) ");
        }

        public void a(r rVar) {
        }

        public boolean a(LayoutParams layoutParams) {
            return layoutParams != null;
        }

        public LayoutParams a(ViewGroup.LayoutParams layoutParams) {
            if (layoutParams instanceof LayoutParams) {
                return new LayoutParams((LayoutParams) layoutParams);
            }
            if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
            }
            return new LayoutParams(layoutParams);
        }

        public LayoutParams a(Context context, AttributeSet attributeSet) {
            return new LayoutParams(context, attributeSet);
        }

        public int a(int i, n nVar, r rVar) {
            return 0;
        }

        public int b(int i, n nVar, r rVar) {
            return 0;
        }

        public boolean d() {
            return false;
        }

        public boolean e() {
            return false;
        }

        public void e(int i) {
        }

        public void a(RecyclerView recyclerView, r rVar, int i) {
            Log.e(RecyclerView.TAG, "You must override smoothScrollToPosition to support smooth scrolling");
        }

        public void a(q qVar) {
            if (!(this.t == null || qVar == this.t || !this.t.h())) {
                this.t.f();
            }
            this.t = qVar;
            this.t.a(this.q, this);
        }

        public boolean r() {
            return this.t != null && this.t.h();
        }

        public int s() {
            return ag.g(this.q);
        }

        public void a(View view) {
            a(view, -1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void
         arg types: [android.view.View, int, int]
         candidates:
          android.support.v7.widget.RecyclerView.h.a(int, int, int):int
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
          android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
          android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
          android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
          android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
          android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
          android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
          android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
          android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void */
        public void a(View view, int i) {
            a(view, i, true);
        }

        public void b(View view) {
            b(view, -1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void
         arg types: [android.view.View, int, int]
         candidates:
          android.support.v7.widget.RecyclerView.h.a(int, int, int):int
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
          android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
          android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
          android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
          android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
          android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
          android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
          android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
          android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void */
        public void b(View view, int i) {
            a(view, i, false);
        }

        private void a(View view, int i, boolean z) {
            u childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (z || childViewHolderInt.isRemoved()) {
                this.q.mViewInfoStore.e(childViewHolderInt);
            } else {
                this.q.mViewInfoStore.f(childViewHolderInt);
            }
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (childViewHolderInt.wasReturnedFromScrap() || childViewHolderInt.isScrap()) {
                if (childViewHolderInt.isScrap()) {
                    childViewHolderInt.unScrap();
                } else {
                    childViewHolderInt.clearReturnedFromScrapFlag();
                }
                this.p.a(view, i, view.getLayoutParams(), false);
            } else if (view.getParent() == this.q) {
                int b2 = this.p.b(view);
                if (i == -1) {
                    i = this.p.b();
                }
                if (b2 == -1) {
                    throw new IllegalStateException("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:" + this.q.indexOfChild(view));
                } else if (b2 != i) {
                    this.q.mLayout.f(b2, i);
                }
            } else {
                this.p.a(view, i, false);
                layoutParams.f1861e = true;
                if (this.t != null && this.t.h()) {
                    this.t.b(view);
                }
            }
            if (layoutParams.f1862f) {
                childViewHolderInt.itemView.invalidate();
                layoutParams.f1862f = false;
            }
        }

        public void c(View view) {
            this.p.a(view);
        }

        public void g(int i) {
            if (i(i) != null) {
                this.p.a(i);
            }
        }

        public int t() {
            return -1;
        }

        public int d(View view) {
            return ((LayoutParams) view.getLayoutParams()).f();
        }

        public View e(View view) {
            View findContainingItemView;
            if (this.q == null || (findContainingItemView = this.q.findContainingItemView(view)) == null || this.p.c(findContainingItemView)) {
                return null;
            }
            return findContainingItemView;
        }

        public View c(int i) {
            int u2 = u();
            for (int i2 = 0; i2 < u2; i2++) {
                View i3 = i(i2);
                u childViewHolderInt = RecyclerView.getChildViewHolderInt(i3);
                if (childViewHolderInt != null && childViewHolderInt.getLayoutPosition() == i && !childViewHolderInt.shouldIgnore() && (this.q.mState.a() || !childViewHolderInt.isRemoved())) {
                    return i3;
                }
            }
            return null;
        }

        public void h(int i) {
            a(i, i(i));
        }

        private void a(int i, View view) {
            this.p.e(i);
        }

        public void a(View view, int i, LayoutParams layoutParams) {
            u childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (childViewHolderInt.isRemoved()) {
                this.q.mViewInfoStore.e(childViewHolderInt);
            } else {
                this.q.mViewInfoStore.f(childViewHolderInt);
            }
            this.p.a(view, i, layoutParams, childViewHolderInt.isRemoved());
        }

        public void c(View view, int i) {
            a(view, i, (LayoutParams) view.getLayoutParams());
        }

        public void f(int i, int i2) {
            View i3 = i(i);
            if (i3 == null) {
                throw new IllegalArgumentException("Cannot move a child from non-existing index:" + i);
            }
            h(i);
            c(i3, i2);
        }

        public void a(View view, n nVar) {
            c(view);
            nVar.a(view);
        }

        public void a(int i, n nVar) {
            View i2 = i(i);
            g(i);
            nVar.a(i2);
        }

        public int u() {
            if (this.p != null) {
                return this.p.b();
            }
            return 0;
        }

        public View i(int i) {
            if (this.p != null) {
                return this.p.b(i);
            }
            return null;
        }

        public int v() {
            return this.f1879e;
        }

        public int w() {
            return this.f1880f;
        }

        public int x() {
            return this.f1881g;
        }

        public int y() {
            return this.f1882h;
        }

        public int z() {
            if (this.q != null) {
                return this.q.getPaddingLeft();
            }
            return 0;
        }

        public int A() {
            if (this.q != null) {
                return this.q.getPaddingTop();
            }
            return 0;
        }

        public int B() {
            if (this.q != null) {
                return this.q.getPaddingRight();
            }
            return 0;
        }

        public int C() {
            if (this.q != null) {
                return this.q.getPaddingBottom();
            }
            return 0;
        }

        public View D() {
            View focusedChild;
            if (this.q == null || (focusedChild = this.q.getFocusedChild()) == null || this.p.c(focusedChild)) {
                return null;
            }
            return focusedChild;
        }

        public int E() {
            a adapter = this.q != null ? this.q.getAdapter() : null;
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }

        public void j(int i) {
            if (this.q != null) {
                this.q.offsetChildrenHorizontal(i);
            }
        }

        public void k(int i) {
            if (this.q != null) {
                this.q.offsetChildrenVertical(i);
            }
        }

        public void a(n nVar) {
            for (int u2 = u() - 1; u2 >= 0; u2--) {
                a(nVar, u2, i(u2));
            }
        }

        private void a(n nVar, int i, View view) {
            u childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (!childViewHolderInt.shouldIgnore()) {
                if (!childViewHolderInt.isInvalid() || childViewHolderInt.isRemoved() || this.q.mAdapter.hasStableIds()) {
                    h(i);
                    nVar.c(view);
                    this.q.mViewInfoStore.h(childViewHolderInt);
                    return;
                }
                g(i);
                nVar.b(childViewHolderInt);
            }
        }

        /* access modifiers changed from: package-private */
        public void b(n nVar) {
            int e2 = nVar.e();
            for (int i = e2 - 1; i >= 0; i--) {
                View e3 = nVar.e(i);
                u childViewHolderInt = RecyclerView.getChildViewHolderInt(e3);
                if (!childViewHolderInt.shouldIgnore()) {
                    childViewHolderInt.setIsRecyclable(false);
                    if (childViewHolderInt.isTmpDetached()) {
                        this.q.removeDetachedView(e3, false);
                    }
                    if (this.q.mItemAnimator != null) {
                        this.q.mItemAnimator.d(childViewHolderInt);
                    }
                    childViewHolderInt.setIsRecyclable(true);
                    nVar.b(e3);
                }
            }
            nVar.f();
            if (e2 > 0) {
                this.q.invalidate();
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view, int i, int i2, LayoutParams layoutParams) {
            return !this.f1877c || !b(view.getMeasuredWidth(), i, layoutParams.width) || !b(view.getMeasuredHeight(), i2, layoutParams.height);
        }

        /* access modifiers changed from: package-private */
        public boolean b(View view, int i, int i2, LayoutParams layoutParams) {
            return view.isLayoutRequested() || !this.f1877c || !b(view.getWidth(), i, layoutParams.width) || !b(view.getHeight(), i2, layoutParams.height);
        }

        private static boolean b(int i, int i2, int i3) {
            int mode = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i2);
            if (i3 > 0 && i != i3) {
                return false;
            }
            switch (mode) {
                case Integer.MIN_VALUE:
                    if (size < i) {
                        return false;
                    }
                    return true;
                case 0:
                    return true;
                case 1073741824:
                    if (size != i) {
                        return false;
                    }
                    return true;
                default:
                    return false;
            }
        }

        public void a(View view, int i, int i2) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            Rect itemDecorInsetsForChild = this.q.getItemDecorInsetsForChild(view);
            int i3 = itemDecorInsetsForChild.top;
            int a2 = a(x(), v(), itemDecorInsetsForChild.left + itemDecorInsetsForChild.right + i + z() + B() + layoutParams.leftMargin + layoutParams.rightMargin, layoutParams.width, d());
            int a3 = a(y(), w(), itemDecorInsetsForChild.bottom + i3 + i2 + A() + C() + layoutParams.topMargin + layoutParams.bottomMargin, layoutParams.height, e());
            if (b(view, a2, a3, layoutParams)) {
                view.measure(a2, a3);
            }
        }

        public static int a(int i, int i2, int i3, int i4, boolean z) {
            int i5 = 0;
            int max = Math.max(0, i - i3);
            if (z) {
                if (i4 >= 0) {
                    i5 = 1073741824;
                    max = i4;
                } else if (i4 == -1) {
                    switch (i2) {
                        case Integer.MIN_VALUE:
                        case 1073741824:
                            i5 = max;
                            break;
                        case 0:
                            i2 = 0;
                            break;
                        default:
                            i2 = 0;
                            break;
                    }
                    max = i5;
                    i5 = i2;
                } else {
                    if (i4 == -2) {
                        max = 0;
                    }
                    max = 0;
                }
            } else if (i4 >= 0) {
                i5 = 1073741824;
                max = i4;
            } else if (i4 == -1) {
                i5 = i2;
            } else {
                if (i4 == -2) {
                    if (i2 == Integer.MIN_VALUE || i2 == 1073741824) {
                        i5 = Integer.MIN_VALUE;
                    }
                }
                max = 0;
            }
            return View.MeasureSpec.makeMeasureSpec(max, i5);
        }

        public int f(View view) {
            Rect rect = ((LayoutParams) view.getLayoutParams()).f1860d;
            return rect.right + view.getMeasuredWidth() + rect.left;
        }

        public int g(View view) {
            Rect rect = ((LayoutParams) view.getLayoutParams()).f1860d;
            return rect.bottom + view.getMeasuredHeight() + rect.top;
        }

        public void a(View view, int i, int i2, int i3, int i4) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            Rect rect = layoutParams.f1860d;
            view.layout(rect.left + i + layoutParams.leftMargin, rect.top + i2 + layoutParams.topMargin, (i3 - rect.right) - layoutParams.rightMargin, (i4 - rect.bottom) - layoutParams.bottomMargin);
        }

        public void a(View view, boolean z, Rect rect) {
            Matrix o;
            if (z) {
                Rect rect2 = ((LayoutParams) view.getLayoutParams()).f1860d;
                rect.set(-rect2.left, -rect2.top, view.getWidth() + rect2.right, rect2.bottom + view.getHeight());
            } else {
                rect.set(0, 0, view.getWidth(), view.getHeight());
            }
            if (!(this.q == null || (o = ag.o(view)) == null || o.isIdentity())) {
                RectF rectF = this.q.mTempRectF;
                rectF.set(rect);
                o.mapRect(rectF);
                rect.set((int) Math.floor((double) rectF.left), (int) Math.floor((double) rectF.top), (int) Math.ceil((double) rectF.right), (int) Math.ceil((double) rectF.bottom));
            }
            rect.offset(view.getLeft(), view.getTop());
        }

        public void a(View view, Rect rect) {
            RecyclerView.getDecoratedBoundsWithMarginsInt(view, rect);
        }

        public int h(View view) {
            return view.getLeft() - n(view);
        }

        public int i(View view) {
            return view.getTop() - l(view);
        }

        public int j(View view) {
            return view.getRight() + o(view);
        }

        public int k(View view) {
            return view.getBottom() + m(view);
        }

        public void b(View view, Rect rect) {
            if (this.q == null) {
                rect.set(0, 0, 0, 0);
            } else {
                rect.set(this.q.getItemDecorInsetsForChild(view));
            }
        }

        public int l(View view) {
            return ((LayoutParams) view.getLayoutParams()).f1860d.top;
        }

        public int m(View view) {
            return ((LayoutParams) view.getLayoutParams()).f1860d.bottom;
        }

        public int n(View view) {
            return ((LayoutParams) view.getLayoutParams()).f1860d.left;
        }

        public int o(View view) {
            return ((LayoutParams) view.getLayoutParams()).f1860d.right;
        }

        public View a(View view, int i, n nVar, r rVar) {
            return null;
        }

        public View d(View view, int i) {
            return null;
        }

        private int[] b(RecyclerView recyclerView, View view, Rect rect, boolean z) {
            int min;
            int i;
            int min2;
            int[] iArr = new int[2];
            int z2 = z();
            int A = A();
            int x2 = x() - B();
            int y2 = y() - C();
            int left = (view.getLeft() + rect.left) - view.getScrollX();
            int top = (view.getTop() + rect.top) - view.getScrollY();
            int width = left + rect.width();
            int height = top + rect.height();
            int min3 = Math.min(0, left - z2);
            int min4 = Math.min(0, top - A);
            int max = Math.max(0, width - x2);
            int max2 = Math.max(0, height - y2);
            if (s() == 1) {
                if (max == 0) {
                    max = Math.max(min3, width - x2);
                }
                i = max;
            } else {
                if (min3 != 0) {
                    min = min3;
                } else {
                    min = Math.min(left - z2, max);
                }
                i = min;
            }
            if (min4 != 0) {
                min2 = min4;
            } else {
                min2 = Math.min(top - A, max2);
            }
            iArr[0] = i;
            iArr[1] = min2;
            return iArr;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
         arg types: [android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, int]
         candidates:
          android.support.v7.widget.RecyclerView.h.a(int, int, int, int, boolean):int
          android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, int, int):void
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, int, android.os.Bundle):boolean
          android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean */
        public boolean a(RecyclerView recyclerView, View view, Rect rect, boolean z) {
            return a(recyclerView, view, rect, z, false);
        }

        public boolean a(RecyclerView recyclerView, View view, Rect rect, boolean z, boolean z2) {
            int[] b2 = b(recyclerView, view, rect, z);
            int i = b2[0];
            int i2 = b2[1];
            if (z2 && !d(recyclerView, i, i2)) {
                return false;
            }
            if (i == 0 && i2 == 0) {
                return false;
            }
            if (z) {
                recyclerView.scrollBy(i, i2);
            } else {
                recyclerView.smoothScrollBy(i, i2);
            }
            return true;
        }

        public boolean a(View view, boolean z, boolean z2) {
            boolean z3;
            if (!this.r.a(view, 24579) || !this.s.a(view, 24579)) {
                z3 = false;
            } else {
                z3 = true;
            }
            if (z) {
                return z3;
            }
            if (z3) {
                return false;
            }
            return true;
        }

        private boolean d(RecyclerView recyclerView, int i, int i2) {
            View focusedChild = recyclerView.getFocusedChild();
            if (focusedChild == null) {
                return false;
            }
            int z = z();
            int A = A();
            int x2 = x() - B();
            int y2 = y() - C();
            Rect rect = this.q.mTempRect;
            a(focusedChild, rect);
            if (rect.left - i >= x2 || rect.right - i <= z || rect.top - i2 >= y2 || rect.bottom - i2 <= A) {
                return false;
            }
            return true;
        }

        @Deprecated
        public boolean a(RecyclerView recyclerView, View view, View view2) {
            return r() || recyclerView.isComputingLayout();
        }

        public boolean a(RecyclerView recyclerView, r rVar, View view, View view2) {
            return a(recyclerView, view, view2);
        }

        public void a(a aVar, a aVar2) {
        }

        public boolean a(RecyclerView recyclerView, ArrayList<View> arrayList, int i, int i2) {
            return false;
        }

        public void a(RecyclerView recyclerView) {
        }

        public void a(RecyclerView recyclerView, int i, int i2) {
        }

        public void b(RecyclerView recyclerView, int i, int i2) {
        }

        public void c(RecyclerView recyclerView, int i, int i2) {
        }

        public void a(RecyclerView recyclerView, int i, int i2, Object obj) {
            c(recyclerView, i, i2);
        }

        public void a(RecyclerView recyclerView, int i, int i2, int i3) {
        }

        public int e(r rVar) {
            return 0;
        }

        public int c(r rVar) {
            return 0;
        }

        public int g(r rVar) {
            return 0;
        }

        public int f(r rVar) {
            return 0;
        }

        public int d(r rVar) {
            return 0;
        }

        public int h(r rVar) {
            return 0;
        }

        public void a(n nVar, r rVar, int i, int i2) {
            this.q.defaultOnMeasure(i, i2);
        }

        public void g(int i, int i2) {
            this.q.setMeasuredDimension(i, i2);
        }

        public int F() {
            return ag.p(this.q);
        }

        public int G() {
            return ag.q(this.q);
        }

        public Parcelable c() {
            return null;
        }

        public void a(Parcelable parcelable) {
        }

        /* access modifiers changed from: package-private */
        public void H() {
            if (this.t != null) {
                this.t.f();
            }
        }

        /* access modifiers changed from: private */
        public void b(q qVar) {
            if (this.t == qVar) {
                this.t = null;
            }
        }

        public void l(int i) {
        }

        public void c(n nVar) {
            for (int u2 = u() - 1; u2 >= 0; u2--) {
                if (!RecyclerView.getChildViewHolderInt(i(u2)).shouldIgnore()) {
                    a(u2, nVar);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(android.support.v4.view.a.e eVar) {
            a(this.q.mRecycler, this.q.mState, eVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.b(android.view.View, int):boolean
         arg types: [android.support.v7.widget.RecyclerView, int]
         candidates:
          android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
          android.support.v4.view.ag.b(android.view.View, float):void
          android.support.v4.view.ag.b(android.view.View, boolean):void
          android.support.v4.view.ag.b(android.view.View, int):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.a(android.view.View, int):boolean
         arg types: [android.support.v7.widget.RecyclerView, int]
         candidates:
          android.support.v4.view.ag.a(int, int):int
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
          android.support.v4.view.ag.a(android.view.View, float):void
          android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
          android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
          android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
          android.support.v4.view.ag.a(android.view.View, boolean):void
          android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
          android.support.v4.view.ag.a(android.view.View, int):boolean */
        public void a(n nVar, r rVar, android.support.v4.view.a.e eVar) {
            if (ag.b((View) this.q, -1) || ag.a((View) this.q, -1)) {
                eVar.a(8192);
                eVar.k(true);
            }
            if (ag.b((View) this.q, 1) || ag.a((View) this.q, 1)) {
                eVar.a((int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
                eVar.k(true);
            }
            eVar.b(e.m.a(a(nVar, rVar), b(nVar, rVar), e(nVar, rVar), d(nVar, rVar)));
        }

        public void a(AccessibilityEvent accessibilityEvent) {
            a(this.q.mRecycler, this.q.mState, accessibilityEvent);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.b(android.view.View, int):boolean
         arg types: [android.support.v7.widget.RecyclerView, int]
         candidates:
          android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
          android.support.v4.view.ag.b(android.view.View, float):void
          android.support.v4.view.ag.b(android.view.View, boolean):void
          android.support.v4.view.ag.b(android.view.View, int):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.a(android.view.View, int):boolean
         arg types: [android.support.v7.widget.RecyclerView, int]
         candidates:
          android.support.v4.view.ag.a(int, int):int
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
          android.support.v4.view.ag.a(android.view.View, float):void
          android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
          android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
          android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
          android.support.v4.view.ag.a(android.view.View, boolean):void
          android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
          android.support.v4.view.ag.a(android.view.View, int):boolean */
        public void a(n nVar, r rVar, AccessibilityEvent accessibilityEvent) {
            boolean z = true;
            android.support.v4.view.a.q a2 = android.support.v4.view.a.a.a(accessibilityEvent);
            if (this.q != null && a2 != null) {
                if (!ag.b((View) this.q, 1) && !ag.b((View) this.q, -1) && !ag.a((View) this.q, -1) && !ag.a((View) this.q, 1)) {
                    z = false;
                }
                a2.d(z);
                if (this.q.mAdapter != null) {
                    a2.a(this.q.mAdapter.getItemCount());
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(View view, android.support.v4.view.a.e eVar) {
            u childViewHolderInt = RecyclerView.getChildViewHolderInt(view);
            if (childViewHolderInt != null && !childViewHolderInt.isRemoved() && !this.p.c(childViewHolderInt.itemView)) {
                a(this.q.mRecycler, this.q.mState, view, eVar);
            }
        }

        public void a(n nVar, r rVar, View view, android.support.v4.view.a.e eVar) {
            int i;
            int d2 = e() ? d(view) : 0;
            if (d()) {
                i = d(view);
            } else {
                i = 0;
            }
            eVar.c(e.n.a(d2, 1, i, 1, false, false));
        }

        public void I() {
            this.u = true;
        }

        public int d(n nVar, r rVar) {
            return 0;
        }

        public int a(n nVar, r rVar) {
            if (this.q == null || this.q.mAdapter == null || !e()) {
                return 1;
            }
            return this.q.mAdapter.getItemCount();
        }

        public int b(n nVar, r rVar) {
            if (this.q == null || this.q.mAdapter == null || !d()) {
                return 1;
            }
            return this.q.mAdapter.getItemCount();
        }

        public boolean e(n nVar, r rVar) {
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i, Bundle bundle) {
            return a(this.q.mRecycler, this.q.mState, i, bundle);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.b(android.view.View, int):boolean
         arg types: [android.support.v7.widget.RecyclerView, int]
         candidates:
          android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
          android.support.v4.view.ag.b(android.view.View, float):void
          android.support.v4.view.ag.b(android.view.View, boolean):void
          android.support.v4.view.ag.b(android.view.View, int):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.a(android.view.View, int):boolean
         arg types: [android.support.v7.widget.RecyclerView, int]
         candidates:
          android.support.v4.view.ag.a(int, int):int
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
          android.support.v4.view.ag.a(android.view.View, float):void
          android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
          android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
          android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
          android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
          android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
          android.support.v4.view.ag.a(android.view.View, boolean):void
          android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
          android.support.v4.view.ag.a(android.view.View, int):boolean */
        public boolean a(n nVar, r rVar, int i, Bundle bundle) {
            int i2;
            int i3;
            int i4;
            int i5;
            if (this.q == null) {
                return false;
            }
            switch (i) {
                case CodedOutputStream.DEFAULT_BUFFER_SIZE /*4096*/:
                    if (ag.b((View) this.q, 1)) {
                        i5 = (y() - A()) - C();
                    } else {
                        i5 = 0;
                    }
                    if (ag.a((View) this.q, 1)) {
                        i3 = i2;
                        i4 = (x() - z()) - B();
                        break;
                    }
                    i3 = i2;
                    i4 = 0;
                    break;
                case 8192:
                    if (ag.b((View) this.q, -1)) {
                        i2 = -((y() - A()) - C());
                    } else {
                        i2 = 0;
                    }
                    if (ag.a((View) this.q, -1)) {
                        i3 = i2;
                        i4 = -((x() - z()) - B());
                        break;
                    }
                    i3 = i2;
                    i4 = 0;
                    break;
                default:
                    i4 = 0;
                    i3 = 0;
                    break;
            }
            if (i3 == 0 && i4 == 0) {
                return false;
            }
            this.q.scrollBy(i4, i3);
            return true;
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view, int i, Bundle bundle) {
            return a(this.q.mRecycler, this.q.mState, view, i, bundle);
        }

        public boolean a(n nVar, r rVar, View view, int i, Bundle bundle) {
            return false;
        }

        public static b a(Context context, AttributeSet attributeSet, int i, int i2) {
            b bVar = new b();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.c.RecyclerView, i, i2);
            bVar.f1885a = obtainStyledAttributes.getInt(a.c.RecyclerView_android_orientation, 1);
            bVar.f1886b = obtainStyledAttributes.getInt(a.c.RecyclerView_spanCount, 1);
            bVar.f1887c = obtainStyledAttributes.getBoolean(a.c.RecyclerView_reverseLayout, false);
            bVar.f1888d = obtainStyledAttributes.getBoolean(a.c.RecyclerView_stackFromEnd, false);
            obtainStyledAttributes.recycle();
            return bVar;
        }

        /* access modifiers changed from: package-private */
        public void f(RecyclerView recyclerView) {
            d(View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), 1073741824));
        }

        /* access modifiers changed from: package-private */
        public boolean k() {
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean J() {
            int u2 = u();
            for (int i = 0; i < u2; i++) {
                ViewGroup.LayoutParams layoutParams = i(i).getLayoutParams();
                if (layoutParams.width < 0 && layoutParams.height < 0) {
                    return true;
                }
            }
            return false;
        }
    }

    public static abstract class g {
        public void a(Canvas canvas, RecyclerView recyclerView, r rVar) {
            a(canvas, recyclerView);
        }

        @Deprecated
        public void a(Canvas canvas, RecyclerView recyclerView) {
        }

        public void b(Canvas canvas, RecyclerView recyclerView, r rVar) {
            b(canvas, recyclerView);
        }

        @Deprecated
        public void b(Canvas canvas, RecyclerView recyclerView) {
        }

        @Deprecated
        public void a(Rect rect, int i, RecyclerView recyclerView) {
            rect.set(0, 0, 0, 0);
        }

        public void a(Rect rect, View view, RecyclerView recyclerView, r rVar) {
            a(rect, ((LayoutParams) view.getLayoutParams()).f(), recyclerView);
        }
    }

    public static class SimpleOnItemTouchListener implements k {
        public boolean a(RecyclerView recyclerView, MotionEvent motionEvent) {
            return false;
        }

        public void b(RecyclerView recyclerView, MotionEvent motionEvent) {
        }

        public void a(boolean z) {
        }
    }

    public static abstract class l {
        public void a(RecyclerView recyclerView, int i) {
        }

        public void a(RecyclerView recyclerView, int i, int i2) {
        }
    }

    public static abstract class u {
        static final int FLAG_ADAPTER_FULLUPDATE = 1024;
        static final int FLAG_ADAPTER_POSITION_UNKNOWN = 512;
        static final int FLAG_APPEARED_IN_PRE_LAYOUT = 4096;
        static final int FLAG_BOUNCED_FROM_HIDDEN_LIST = 8192;
        static final int FLAG_BOUND = 1;
        static final int FLAG_IGNORE = 128;
        static final int FLAG_INVALID = 4;
        static final int FLAG_MOVED = 2048;
        static final int FLAG_NOT_RECYCLABLE = 16;
        static final int FLAG_REMOVED = 8;
        static final int FLAG_RETURNED_FROM_SCRAP = 32;
        static final int FLAG_TMP_DETACHED = 256;
        static final int FLAG_UPDATE = 2;
        private static final List<Object> FULLUPDATE_PAYLOADS = Collections.EMPTY_LIST;
        static final int PENDING_ACCESSIBILITY_STATE_NOT_SET = -1;
        public final View itemView;
        /* access modifiers changed from: private */
        public int mFlags;
        /* access modifiers changed from: private */
        public boolean mInChangeScrap = false;
        private int mIsRecyclableCount = 0;
        long mItemId = -1;
        int mItemViewType = -1;
        WeakReference<RecyclerView> mNestedRecyclerView;
        int mOldPosition = -1;
        RecyclerView mOwnerRecyclerView;
        List<Object> mPayloads = null;
        int mPendingAccessibilityState = -1;
        int mPosition = -1;
        int mPreLayoutPosition = -1;
        /* access modifiers changed from: private */
        public n mScrapContainer = null;
        u mShadowedHolder = null;
        u mShadowingHolder = null;
        List<Object> mUnmodifiedPayloads = null;
        private int mWasImportantForAccessibilityBeforeHidden = 0;

        public u(View view) {
            if (view == null) {
                throw new IllegalArgumentException("itemView may not be null");
            }
            this.itemView = view;
        }

        /* access modifiers changed from: package-private */
        public void flagRemovedAndOffsetPosition(int i, int i2, boolean z) {
            addFlags(8);
            offsetPosition(i2, z);
            this.mPosition = i;
        }

        /* access modifiers changed from: package-private */
        public void offsetPosition(int i, boolean z) {
            if (this.mOldPosition == -1) {
                this.mOldPosition = this.mPosition;
            }
            if (this.mPreLayoutPosition == -1) {
                this.mPreLayoutPosition = this.mPosition;
            }
            if (z) {
                this.mPreLayoutPosition += i;
            }
            this.mPosition += i;
            if (this.itemView.getLayoutParams() != null) {
                ((LayoutParams) this.itemView.getLayoutParams()).f1861e = true;
            }
        }

        /* access modifiers changed from: package-private */
        public void clearOldPosition() {
            this.mOldPosition = -1;
            this.mPreLayoutPosition = -1;
        }

        /* access modifiers changed from: package-private */
        public void saveOldPosition() {
            if (this.mOldPosition == -1) {
                this.mOldPosition = this.mPosition;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean shouldIgnore() {
            return (this.mFlags & 128) != 0;
        }

        @Deprecated
        public final int getPosition() {
            return this.mPreLayoutPosition == -1 ? this.mPosition : this.mPreLayoutPosition;
        }

        public final int getLayoutPosition() {
            return this.mPreLayoutPosition == -1 ? this.mPosition : this.mPreLayoutPosition;
        }

        public final int getAdapterPosition() {
            if (this.mOwnerRecyclerView == null) {
                return -1;
            }
            return this.mOwnerRecyclerView.getAdapterPositionFor(this);
        }

        public final int getOldPosition() {
            return this.mOldPosition;
        }

        public final long getItemId() {
            return this.mItemId;
        }

        public final int getItemViewType() {
            return this.mItemViewType;
        }

        /* access modifiers changed from: package-private */
        public boolean isScrap() {
            return this.mScrapContainer != null;
        }

        /* access modifiers changed from: package-private */
        public void unScrap() {
            this.mScrapContainer.c(this);
        }

        /* access modifiers changed from: package-private */
        public boolean wasReturnedFromScrap() {
            return (this.mFlags & 32) != 0;
        }

        /* access modifiers changed from: package-private */
        public void clearReturnedFromScrapFlag() {
            this.mFlags &= -33;
        }

        /* access modifiers changed from: package-private */
        public void clearTmpDetachFlag() {
            this.mFlags &= -257;
        }

        /* access modifiers changed from: package-private */
        public void stopIgnoring() {
            this.mFlags &= -129;
        }

        /* access modifiers changed from: package-private */
        public void setScrapContainer(n nVar, boolean z) {
            this.mScrapContainer = nVar;
            this.mInChangeScrap = z;
        }

        /* access modifiers changed from: package-private */
        public boolean isInvalid() {
            return (this.mFlags & 4) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean needsUpdate() {
            return (this.mFlags & 2) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isBound() {
            return (this.mFlags & 1) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isRemoved() {
            return (this.mFlags & 8) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean hasAnyOfTheFlags(int i) {
            return (this.mFlags & i) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isTmpDetached() {
            return (this.mFlags & 256) != 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isAdapterPositionUnknown() {
            return (this.mFlags & 512) != 0 || isInvalid();
        }

        /* access modifiers changed from: package-private */
        public void setFlags(int i, int i2) {
            this.mFlags = (this.mFlags & (i2 ^ -1)) | (i & i2);
        }

        /* access modifiers changed from: package-private */
        public void addFlags(int i) {
            this.mFlags |= i;
        }

        /* access modifiers changed from: package-private */
        public void addChangePayload(Object obj) {
            if (obj == null) {
                addFlags(1024);
            } else if ((this.mFlags & 1024) == 0) {
                createPayloadsIfNeeded();
                this.mPayloads.add(obj);
            }
        }

        private void createPayloadsIfNeeded() {
            if (this.mPayloads == null) {
                this.mPayloads = new ArrayList();
                this.mUnmodifiedPayloads = Collections.unmodifiableList(this.mPayloads);
            }
        }

        /* access modifiers changed from: package-private */
        public void clearPayload() {
            if (this.mPayloads != null) {
                this.mPayloads.clear();
            }
            this.mFlags &= -1025;
        }

        /* access modifiers changed from: package-private */
        public List<Object> getUnmodifiedPayloads() {
            if ((this.mFlags & 1024) != 0) {
                return FULLUPDATE_PAYLOADS;
            }
            if (this.mPayloads == null || this.mPayloads.size() == 0) {
                return FULLUPDATE_PAYLOADS;
            }
            return this.mUnmodifiedPayloads;
        }

        /* access modifiers changed from: package-private */
        public void resetInternal() {
            this.mFlags = 0;
            this.mPosition = -1;
            this.mOldPosition = -1;
            this.mItemId = -1;
            this.mPreLayoutPosition = -1;
            this.mIsRecyclableCount = 0;
            this.mShadowedHolder = null;
            this.mShadowingHolder = null;
            clearPayload();
            this.mWasImportantForAccessibilityBeforeHidden = 0;
            this.mPendingAccessibilityState = -1;
            RecyclerView.clearNestedRecyclerViewIfNotNested(this);
        }

        /* access modifiers changed from: private */
        public void onEnteredHiddenState(RecyclerView recyclerView) {
            this.mWasImportantForAccessibilityBeforeHidden = ag.d(this.itemView);
            recyclerView.setChildImportantForAccessibilityInternal(this, 4);
        }

        /* access modifiers changed from: private */
        public void onLeftHiddenState(RecyclerView recyclerView) {
            recyclerView.setChildImportantForAccessibilityInternal(this, this.mWasImportantForAccessibilityBeforeHidden);
            this.mWasImportantForAccessibilityBeforeHidden = 0;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("ViewHolder{" + Integer.toHexString(hashCode()) + " position=" + this.mPosition + " id=" + this.mItemId + ", oldPos=" + this.mOldPosition + ", pLpos:" + this.mPreLayoutPosition);
            if (isScrap()) {
                sb.append(" scrap ").append(this.mInChangeScrap ? "[changeScrap]" : "[attachedScrap]");
            }
            if (isInvalid()) {
                sb.append(" invalid");
            }
            if (!isBound()) {
                sb.append(" unbound");
            }
            if (needsUpdate()) {
                sb.append(" update");
            }
            if (isRemoved()) {
                sb.append(" removed");
            }
            if (shouldIgnore()) {
                sb.append(" ignored");
            }
            if (isTmpDetached()) {
                sb.append(" tmpDetached");
            }
            if (!isRecyclable()) {
                sb.append(" not recyclable(" + this.mIsRecyclableCount + ")");
            }
            if (isAdapterPositionUnknown()) {
                sb.append(" undefined adapter position");
            }
            if (this.itemView.getParent() == null) {
                sb.append(" no parent");
            }
            sb.append("}");
            return sb.toString();
        }

        public final void setIsRecyclable(boolean z) {
            this.mIsRecyclableCount = z ? this.mIsRecyclableCount - 1 : this.mIsRecyclableCount + 1;
            if (this.mIsRecyclableCount < 0) {
                this.mIsRecyclableCount = 0;
                Log.e("View", "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for " + this);
            } else if (!z && this.mIsRecyclableCount == 1) {
                this.mFlags |= 16;
            } else if (z && this.mIsRecyclableCount == 0) {
                this.mFlags &= -17;
            }
        }

        public final boolean isRecyclable() {
            return (this.mFlags & 16) == 0 && !ag.b(this.itemView);
        }

        /* access modifiers changed from: private */
        public boolean shouldBeKeptAsChild() {
            return (this.mFlags & 16) != 0;
        }

        /* access modifiers changed from: private */
        public boolean doesTransientStatePreventRecycling() {
            return (this.mFlags & 16) == 0 && ag.b(this.itemView);
        }

        /* access modifiers changed from: package-private */
        public boolean isUpdated() {
            return (this.mFlags & 2) != 0;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean setChildImportantForAccessibilityInternal(u uVar, int i2) {
        if (isComputingLayout()) {
            uVar.mPendingAccessibilityState = i2;
            this.mPendingAccessibilityImportanceChange.add(uVar);
            return false;
        }
        ag.c(uVar.itemView, i2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void dispatchPendingImportantForAccessibilityChanges() {
        int i2;
        for (int size = this.mPendingAccessibilityImportanceChange.size() - 1; size >= 0; size--) {
            u uVar = this.mPendingAccessibilityImportanceChange.get(size);
            if (uVar.itemView.getParent() == this && !uVar.shouldIgnore() && (i2 = uVar.mPendingAccessibilityState) != -1) {
                ag.c(uVar.itemView, i2);
                uVar.mPendingAccessibilityState = -1;
            }
        }
        this.mPendingAccessibilityImportanceChange.clear();
    }

    /* access modifiers changed from: package-private */
    public int getAdapterPositionFor(u uVar) {
        if (uVar.hasAnyOfTheFlags(524) || !uVar.isBound()) {
            return -1;
        }
        return this.mAdapterHelper.c(uVar.mPosition);
    }

    public void setNestedScrollingEnabled(boolean z) {
        getScrollingChildHelper().a(z);
    }

    public boolean isNestedScrollingEnabled() {
        return getScrollingChildHelper().a();
    }

    public boolean startNestedScroll(int i2) {
        return getScrollingChildHelper().a(i2);
    }

    public void stopNestedScroll() {
        getScrollingChildHelper().c();
    }

    public boolean hasNestedScrollingParent() {
        return getScrollingChildHelper().b();
    }

    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return getScrollingChildHelper().a(i2, i3, i4, i5, iArr);
    }

    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return getScrollingChildHelper().a(i2, i3, iArr, iArr2);
    }

    public boolean dispatchNestedFling(float f2, float f3, boolean z) {
        return getScrollingChildHelper().a(f2, f3, z);
    }

    public boolean dispatchNestedPreFling(float f2, float f3) {
        return getScrollingChildHelper().a(f2, f3);
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {

        /* renamed from: c  reason: collision with root package name */
        u f1859c;

        /* renamed from: d  reason: collision with root package name */
        final Rect f1860d = new Rect();

        /* renamed from: e  reason: collision with root package name */
        boolean f1861e = true;

        /* renamed from: f  reason: collision with root package name */
        boolean f1862f = false;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.LayoutParams) layoutParams);
        }

        public boolean c() {
            return this.f1859c.isInvalid();
        }

        public boolean d() {
            return this.f1859c.isRemoved();
        }

        public boolean e() {
            return this.f1859c.isUpdated();
        }

        public int f() {
            return this.f1859c.getLayoutPosition();
        }
    }

    public static abstract class c {
        public void a() {
        }

        public void a(int i, int i2) {
        }

        public void a(int i, int i2, Object obj) {
            a(i, i2);
        }

        public void b(int i, int i2) {
        }

        public void c(int i, int i2) {
        }

        public void a(int i, int i2, int i3) {
        }
    }

    public static abstract class q {

        /* renamed from: a  reason: collision with root package name */
        private int f1904a = -1;

        /* renamed from: b  reason: collision with root package name */
        private RecyclerView f1905b;

        /* renamed from: c  reason: collision with root package name */
        private h f1906c;

        /* renamed from: d  reason: collision with root package name */
        private boolean f1907d;

        /* renamed from: e  reason: collision with root package name */
        private boolean f1908e;

        /* renamed from: f  reason: collision with root package name */
        private View f1909f;

        /* renamed from: g  reason: collision with root package name */
        private final a f1910g = new a(0, 0);

        public interface b {
            PointF d(int i);
        }

        /* access modifiers changed from: protected */
        public abstract void a();

        /* access modifiers changed from: protected */
        public abstract void a(int i, int i2, r rVar, a aVar);

        /* access modifiers changed from: protected */
        public abstract void a(View view, r rVar, a aVar);

        /* access modifiers changed from: protected */
        public abstract void b();

        /* access modifiers changed from: package-private */
        public void a(RecyclerView recyclerView, h hVar) {
            this.f1905b = recyclerView;
            this.f1906c = hVar;
            if (this.f1904a == -1) {
                throw new IllegalArgumentException("Invalid target position");
            }
            int unused = this.f1905b.mState.n = this.f1904a;
            this.f1908e = true;
            this.f1907d = true;
            this.f1909f = e(i());
            a();
            this.f1905b.mViewFlinger.a();
        }

        public void d(int i) {
            this.f1904a = i;
        }

        public h e() {
            return this.f1906c;
        }

        /* access modifiers changed from: protected */
        public final void f() {
            if (this.f1908e) {
                b();
                int unused = this.f1905b.mState.n = -1;
                this.f1909f = null;
                this.f1904a = -1;
                this.f1907d = false;
                this.f1908e = false;
                this.f1906c.b(this);
                this.f1906c = null;
                this.f1905b = null;
            }
        }

        public boolean g() {
            return this.f1907d;
        }

        public boolean h() {
            return this.f1908e;
        }

        public int i() {
            return this.f1904a;
        }

        /* access modifiers changed from: private */
        public void a(int i, int i2) {
            RecyclerView recyclerView = this.f1905b;
            if (!this.f1908e || this.f1904a == -1 || recyclerView == null) {
                f();
            }
            this.f1907d = false;
            if (this.f1909f != null) {
                if (a(this.f1909f) == this.f1904a) {
                    a(this.f1909f, recyclerView.mState, this.f1910g);
                    this.f1910g.a(recyclerView);
                    f();
                } else {
                    Log.e(RecyclerView.TAG, "Passed over target position while smooth scrolling.");
                    this.f1909f = null;
                }
            }
            if (this.f1908e) {
                a(i, i2, recyclerView.mState, this.f1910g);
                boolean a2 = this.f1910g.a();
                this.f1910g.a(recyclerView);
                if (!a2) {
                    return;
                }
                if (this.f1908e) {
                    this.f1907d = true;
                    recyclerView.mViewFlinger.a();
                    return;
                }
                f();
            }
        }

        public int a(View view) {
            return this.f1905b.getChildLayoutPosition(view);
        }

        public int j() {
            return this.f1905b.mLayout.u();
        }

        public View e(int i) {
            return this.f1905b.mLayout.c(i);
        }

        /* access modifiers changed from: protected */
        public void b(View view) {
            if (a(view) == i()) {
                this.f1909f = view;
            }
        }

        /* access modifiers changed from: protected */
        public void a(PointF pointF) {
            double sqrt = Math.sqrt((double) ((pointF.x * pointF.x) + (pointF.y * pointF.y)));
            pointF.x = (float) (((double) pointF.x) / sqrt);
            pointF.y = (float) (((double) pointF.y) / sqrt);
        }

        public static class a {

            /* renamed from: a  reason: collision with root package name */
            private int f1911a;

            /* renamed from: b  reason: collision with root package name */
            private int f1912b;

            /* renamed from: c  reason: collision with root package name */
            private int f1913c;

            /* renamed from: d  reason: collision with root package name */
            private int f1914d;

            /* renamed from: e  reason: collision with root package name */
            private Interpolator f1915e;

            /* renamed from: f  reason: collision with root package name */
            private boolean f1916f;

            /* renamed from: g  reason: collision with root package name */
            private int f1917g;

            public a(int i, int i2) {
                this(i, i2, Integer.MIN_VALUE, null);
            }

            public a(int i, int i2, int i3, Interpolator interpolator) {
                this.f1914d = -1;
                this.f1916f = false;
                this.f1917g = 0;
                this.f1911a = i;
                this.f1912b = i2;
                this.f1913c = i3;
                this.f1915e = interpolator;
            }

            public void a(int i) {
                this.f1914d = i;
            }

            /* access modifiers changed from: package-private */
            public boolean a() {
                return this.f1914d >= 0;
            }

            /* access modifiers changed from: package-private */
            public void a(RecyclerView recyclerView) {
                if (this.f1914d >= 0) {
                    int i = this.f1914d;
                    this.f1914d = -1;
                    recyclerView.jumpToPositionForSmoothScroller(i);
                    this.f1916f = false;
                } else if (this.f1916f) {
                    b();
                    if (this.f1915e != null) {
                        recyclerView.mViewFlinger.a(this.f1911a, this.f1912b, this.f1913c, this.f1915e);
                    } else if (this.f1913c == Integer.MIN_VALUE) {
                        recyclerView.mViewFlinger.b(this.f1911a, this.f1912b);
                    } else {
                        recyclerView.mViewFlinger.a(this.f1911a, this.f1912b, this.f1913c);
                    }
                    this.f1917g++;
                    if (this.f1917g > 10) {
                        Log.e(RecyclerView.TAG, "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary");
                    }
                    this.f1916f = false;
                } else {
                    this.f1917g = 0;
                }
            }

            private void b() {
                if (this.f1915e != null && this.f1913c < 1) {
                    throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
                } else if (this.f1913c < 1) {
                    throw new IllegalStateException("Scroll duration must be a positive number");
                }
            }

            public void a(int i, int i2, int i3, Interpolator interpolator) {
                this.f1911a = i;
                this.f1912b = i2;
                this.f1913c = i3;
                this.f1915e = interpolator;
                this.f1916f = true;
            }
        }
    }

    static class b extends Observable<c> {
        b() {
        }

        public boolean a() {
            return !this.mObservers.isEmpty();
        }

        public void b() {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((c) this.mObservers.get(size)).a();
            }
        }

        public void a(int i, int i2) {
            a(i, i2, null);
        }

        public void a(int i, int i2, Object obj) {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((c) this.mObservers.get(size)).a(i, i2, obj);
            }
        }

        public void b(int i, int i2) {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((c) this.mObservers.get(size)).b(i, i2);
            }
        }

        public void c(int i, int i2) {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((c) this.mObservers.get(size)).c(i, i2);
            }
        }

        public void d(int i, int i2) {
            for (int size = this.mObservers.size() - 1; size >= 0; size--) {
                ((c) this.mObservers.get(size)).a(i, i2, 1);
            }
        }
    }

    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = android.support.v4.os.f.a(new android.support.v4.os.g<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a  reason: collision with root package name */
        Parcelable f1863a;

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f1863a = parcel.readParcelable(classLoader == null ? h.class.getClassLoader() : classLoader);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeParcelable(this.f1863a, 0);
        }

        /* access modifiers changed from: package-private */
        public void a(SavedState savedState) {
            this.f1863a = savedState.f1863a;
        }
    }

    public static class r {

        /* renamed from: a  reason: collision with root package name */
        int f1918a = 0;

        /* renamed from: b  reason: collision with root package name */
        int f1919b = 0;

        /* renamed from: c  reason: collision with root package name */
        int f1920c = 1;

        /* renamed from: d  reason: collision with root package name */
        int f1921d = 0;

        /* renamed from: e  reason: collision with root package name */
        boolean f1922e = false;

        /* renamed from: f  reason: collision with root package name */
        boolean f1923f = false;

        /* renamed from: g  reason: collision with root package name */
        boolean f1924g = false;

        /* renamed from: h  reason: collision with root package name */
        boolean f1925h = false;
        boolean i = false;
        boolean j = false;
        int k;
        long l;
        int m;
        /* access modifiers changed from: private */
        public int n = -1;
        private SparseArray<Object> o;

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            if ((this.f1920c & i2) == 0) {
                throw new IllegalStateException("Layout state should be one of " + Integer.toBinaryString(i2) + " but it is " + Integer.toBinaryString(this.f1920c));
            }
        }

        /* access modifiers changed from: package-private */
        public void a(a aVar) {
            this.f1920c = 1;
            this.f1921d = aVar.getItemCount();
            this.f1922e = false;
            this.f1923f = false;
            this.f1924g = false;
            this.f1925h = false;
        }

        public boolean a() {
            return this.f1923f;
        }

        public boolean b() {
            return this.j;
        }

        public int c() {
            return this.n;
        }

        public boolean d() {
            return this.n != -1;
        }

        public int e() {
            return this.f1923f ? this.f1918a - this.f1919b : this.f1921d;
        }

        public String toString() {
            return "State{mTargetPosition=" + this.n + ", mData=" + this.o + ", mItemCount=" + this.f1921d + ", mPreviousLayoutItemCount=" + this.f1918a + ", mDeletedInvisibleItemCountSincePreviousLayout=" + this.f1919b + ", mStructureChanged=" + this.f1922e + ", mInPreLayout=" + this.f1923f + ", mRunSimpleAnimations=" + this.i + ", mRunPredictiveAnimations=" + this.j + '}';
        }
    }

    private class f implements e.b {
        f() {
        }

        public void a(u uVar) {
            uVar.setIsRecyclable(true);
            if (uVar.mShadowedHolder != null && uVar.mShadowingHolder == null) {
                uVar.mShadowedHolder = null;
            }
            uVar.mShadowingHolder = null;
            if (!uVar.shouldBeKeptAsChild() && !RecyclerView.this.removeAnimatingView(uVar.itemView) && uVar.isTmpDetached()) {
                RecyclerView.this.removeDetachedView(uVar.itemView, false);
            }
        }
    }

    public static abstract class e {

        /* renamed from: a  reason: collision with root package name */
        private b f1864a = null;

        /* renamed from: b  reason: collision with root package name */
        private ArrayList<a> f1865b = new ArrayList<>();

        /* renamed from: c  reason: collision with root package name */
        private long f1866c = 120;

        /* renamed from: d  reason: collision with root package name */
        private long f1867d = 120;

        /* renamed from: e  reason: collision with root package name */
        private long f1868e = 250;

        /* renamed from: f  reason: collision with root package name */
        private long f1869f = 250;

        public interface a {
            void a();
        }

        interface b {
            void a(u uVar);
        }

        public abstract void a();

        public abstract boolean a(u uVar, c cVar, c cVar2);

        public abstract boolean a(u uVar, u uVar2, c cVar, c cVar2);

        public abstract boolean b();

        public abstract boolean b(u uVar, c cVar, c cVar2);

        public abstract boolean c(u uVar, c cVar, c cVar2);

        public abstract void d();

        public abstract void d(u uVar);

        public long e() {
            return this.f1868e;
        }

        public long f() {
            return this.f1866c;
        }

        public long g() {
            return this.f1867d;
        }

        public long h() {
            return this.f1869f;
        }

        /* access modifiers changed from: package-private */
        public void a(b bVar) {
            this.f1864a = bVar;
        }

        public c a(r rVar, u uVar, int i, List<Object> list) {
            return j().a(uVar);
        }

        public c a(r rVar, u uVar) {
            return j().a(uVar);
        }

        static int e(u uVar) {
            int access$1400 = uVar.mFlags & 14;
            if (uVar.isInvalid()) {
                return 4;
            }
            if ((access$1400 & 4) != 0) {
                return access$1400;
            }
            int oldPosition = uVar.getOldPosition();
            int adapterPosition = uVar.getAdapterPosition();
            if (oldPosition == -1 || adapterPosition == -1 || oldPosition == adapterPosition) {
                return access$1400;
            }
            return access$1400 | FileUtils.FileMode.MODE_ISUID;
        }

        public final void f(u uVar) {
            g(uVar);
            if (this.f1864a != null) {
                this.f1864a.a(uVar);
            }
        }

        public void g(u uVar) {
        }

        public final boolean a(a aVar) {
            boolean b2 = b();
            if (aVar != null) {
                if (!b2) {
                    aVar.a();
                } else {
                    this.f1865b.add(aVar);
                }
            }
            return b2;
        }

        public boolean h(u uVar) {
            return true;
        }

        public boolean a(u uVar, List<Object> list) {
            return h(uVar);
        }

        public final void i() {
            int size = this.f1865b.size();
            for (int i = 0; i < size; i++) {
                this.f1865b.get(i).a();
            }
            this.f1865b.clear();
        }

        public c j() {
            return new c();
        }

        public static class c {

            /* renamed from: a  reason: collision with root package name */
            public int f1870a;

            /* renamed from: b  reason: collision with root package name */
            public int f1871b;

            /* renamed from: c  reason: collision with root package name */
            public int f1872c;

            /* renamed from: d  reason: collision with root package name */
            public int f1873d;

            public c a(u uVar) {
                return a(uVar, 0);
            }

            public c a(u uVar, int i) {
                View view = uVar.itemView;
                this.f1870a = view.getLeft();
                this.f1871b = view.getTop();
                this.f1872c = view.getRight();
                this.f1873d = view.getBottom();
                return this;
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.mChildDrawingOrderCallback == null) {
            return super.getChildDrawingOrder(i2, i3);
        }
        return this.mChildDrawingOrderCallback.a(i2, i3);
    }

    private v getScrollingChildHelper() {
        if (this.mScrollingChildHelper == null) {
            this.mScrollingChildHelper = new v(this);
        }
        return this.mScrollingChildHelper;
    }
}
