package android.support.v7.widget;

import android.annotation.TargetApi;
import android.graphics.Outline;

@TargetApi(21)
/* compiled from: ActionBarBackgroundDrawableV21 */
class b extends a {
    public b(ActionBarContainer actionBarContainer) {
        super(actionBarContainer);
    }

    public void getOutline(Outline outline) {
        if (this.f2091a.f1593d) {
            if (this.f2091a.f1592c != null) {
                this.f2091a.f1592c.getOutline(outline);
            }
        } else if (this.f2091a.f1590a != null) {
            this.f2091a.f1590a.getOutline(outline);
        }
    }
}
