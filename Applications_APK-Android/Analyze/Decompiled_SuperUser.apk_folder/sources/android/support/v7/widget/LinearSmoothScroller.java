package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import io.fabric.sdk.android.services.common.a;

public class LinearSmoothScroller extends RecyclerView.q {

    /* renamed from: a  reason: collision with root package name */
    protected final LinearInterpolator f1815a = new LinearInterpolator();

    /* renamed from: b  reason: collision with root package name */
    protected final DecelerateInterpolator f1816b = new DecelerateInterpolator();

    /* renamed from: c  reason: collision with root package name */
    protected PointF f1817c;

    /* renamed from: d  reason: collision with root package name */
    protected int f1818d = 0;

    /* renamed from: e  reason: collision with root package name */
    protected int f1819e = 0;

    /* renamed from: f  reason: collision with root package name */
    private final float f1820f;

    public LinearSmoothScroller(Context context) {
        this.f1820f = a(context.getResources().getDisplayMetrics());
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void a(View view, RecyclerView.r rVar, RecyclerView.q.a aVar) {
        int b2 = b(view, c());
        int a2 = a(view, d());
        int a3 = a((int) Math.sqrt((double) ((b2 * b2) + (a2 * a2))));
        if (a3 > 0) {
            aVar.a(-b2, -a2, a3, this.f1816b);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearSmoothScroller.a(int, int):int
     arg types: [int, int]
     candidates:
      android.support.v7.widget.LinearSmoothScroller.a(android.view.View, int):int
      android.support.v7.widget.RecyclerView.q.a(int, int):void
      android.support.v7.widget.RecyclerView.q.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$h):void
      android.support.v7.widget.LinearSmoothScroller.a(int, int):int */
    /* access modifiers changed from: protected */
    public void a(int i, int i2, RecyclerView.r rVar, RecyclerView.q.a aVar) {
        if (j() == 0) {
            f();
            return;
        }
        this.f1818d = a(this.f1818d, i);
        this.f1819e = a(this.f1819e, i2);
        if (this.f1818d == 0 && this.f1819e == 0) {
            a(aVar);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.f1819e = 0;
        this.f1818d = 0;
        this.f1817c = null;
    }

    /* access modifiers changed from: protected */
    public float a(DisplayMetrics displayMetrics) {
        return 25.0f / ((float) displayMetrics.densityDpi);
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        return (int) Math.ceil(((double) b(i)) / 0.3356d);
    }

    /* access modifiers changed from: protected */
    public int b(int i) {
        return (int) Math.ceil((double) (((float) Math.abs(i)) * this.f1820f));
    }

    /* access modifiers changed from: protected */
    public int c() {
        if (this.f1817c == null || this.f1817c.x == 0.0f) {
            return 0;
        }
        return this.f1817c.x > 0.0f ? 1 : -1;
    }

    /* access modifiers changed from: protected */
    public int d() {
        if (this.f1817c == null || this.f1817c.y == 0.0f) {
            return 0;
        }
        return this.f1817c.y > 0.0f ? 1 : -1;
    }

    /* access modifiers changed from: protected */
    public void a(RecyclerView.q.a aVar) {
        PointF c2 = c(i());
        if (c2 == null || (c2.x == 0.0f && c2.y == 0.0f)) {
            aVar.a(i());
            f();
            return;
        }
        a(c2);
        this.f1817c = c2;
        this.f1818d = (int) (c2.x * 10000.0f);
        this.f1819e = (int) (c2.y * 10000.0f);
        aVar.a((int) (((float) this.f1818d) * 1.2f), (int) (((float) this.f1819e) * 1.2f), (int) (((float) b((int) a.DEFAULT_TIMEOUT)) * 1.2f), this.f1815a);
    }

    private int a(int i, int i2) {
        int i3 = i - i2;
        if (i * i3 <= 0) {
            return 0;
        }
        return i3;
    }

    public int a(int i, int i2, int i3, int i4, int i5) {
        switch (i5) {
            case -1:
                return i3 - i;
            case 0:
                int i6 = i3 - i;
                if (i6 > 0) {
                    return i6;
                }
                int i7 = i4 - i2;
                if (i7 >= 0) {
                    return 0;
                }
                return i7;
            case 1:
                return i4 - i2;
            default:
                throw new IllegalArgumentException("snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_");
        }
    }

    public int a(View view, int i) {
        RecyclerView.h e2 = e();
        if (e2 == null || !e2.e()) {
            return 0;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        return a(e2.i(view) - layoutParams.topMargin, e2.k(view) + layoutParams.bottomMargin, e2.A(), e2.y() - e2.C(), i);
    }

    public int b(View view, int i) {
        RecyclerView.h e2 = e();
        if (e2 == null || !e2.d()) {
            return 0;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        return a(e2.h(view) - layoutParams.leftMargin, e2.j(view) + layoutParams.rightMargin, e2.z(), e2.x() - e2.B(), i);
    }

    public PointF c(int i) {
        RecyclerView.h e2 = e();
        if (e2 instanceof RecyclerView.q.b) {
            return ((RecyclerView.q.b) e2).d(i);
        }
        Log.w("LinearSmoothScroller", "You should override computeScrollVectorForPosition when the LayoutManager does not implement " + RecyclerView.q.b.class.getCanonicalName());
        return null;
    }
}
