package android.support.v7.widget;

import android.content.Context;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

public class AppCompatCheckedTextView extends CheckedTextView {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f1668a = {16843016};

    /* renamed from: b  reason: collision with root package name */
    private k f1669b;

    public AppCompatCheckedTextView(Context context) {
        this(context, null);
    }

    public AppCompatCheckedTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16843720);
    }

    public AppCompatCheckedTextView(Context context, AttributeSet attributeSet, int i) {
        super(ak.a(context), attributeSet, i);
        this.f1669b = k.a(this);
        this.f1669b.a(attributeSet, i);
        this.f1669b.a();
        an a2 = an.a(getContext(), attributeSet, f1668a, i, 0);
        setCheckMarkDrawable(a2.a(0));
        a2.a();
    }

    public void setCheckMarkDrawable(int i) {
        setCheckMarkDrawable(b.b(getContext(), i));
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f1669b != null) {
            this.f1669b.a(context, i);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1669b != null) {
            this.f1669b.a();
        }
    }
}
