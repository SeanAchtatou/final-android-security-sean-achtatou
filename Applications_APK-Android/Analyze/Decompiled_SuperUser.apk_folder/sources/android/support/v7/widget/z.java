package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/* compiled from: LayoutState */
class z {

    /* renamed from: a  reason: collision with root package name */
    boolean f2297a = true;

    /* renamed from: b  reason: collision with root package name */
    int f2298b;

    /* renamed from: c  reason: collision with root package name */
    int f2299c;

    /* renamed from: d  reason: collision with root package name */
    int f2300d;

    /* renamed from: e  reason: collision with root package name */
    int f2301e;

    /* renamed from: f  reason: collision with root package name */
    int f2302f = 0;

    /* renamed from: g  reason: collision with root package name */
    int f2303g = 0;

    /* renamed from: h  reason: collision with root package name */
    boolean f2304h;
    boolean i;

    z() {
    }

    /* access modifiers changed from: package-private */
    public boolean a(RecyclerView.r rVar) {
        return this.f2299c >= 0 && this.f2299c < rVar.e();
    }

    /* access modifiers changed from: package-private */
    public View a(RecyclerView.n nVar) {
        View c2 = nVar.c(this.f2299c);
        this.f2299c += this.f2300d;
        return c2;
    }

    public String toString() {
        return "LayoutState{mAvailable=" + this.f2298b + ", mCurrentPosition=" + this.f2299c + ", mItemDirection=" + this.f2300d + ", mLayoutDirection=" + this.f2301e + ", mStartLine=" + this.f2302f + ", mEndLine=" + this.f2303g + '}';
    }
}
