package android.support.v7.widget;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ag;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DividerItemDecoration extends RecyclerView.g {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f1767a = {16843284};

    /* renamed from: b  reason: collision with root package name */
    private Drawable f1768b;

    /* renamed from: c  reason: collision with root package name */
    private int f1769c;

    /* renamed from: d  reason: collision with root package name */
    private final Rect f1770d;

    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.r rVar) {
        if (recyclerView.getLayoutManager() != null) {
            if (this.f1769c == 1) {
                c(canvas, recyclerView);
            } else {
                d(canvas, recyclerView);
            }
        }
    }

    @SuppressLint({"NewApi"})
    private void c(Canvas canvas, RecyclerView recyclerView) {
        int width;
        int i;
        canvas.save();
        if (recyclerView.getClipToPadding()) {
            i = recyclerView.getPaddingLeft();
            width = recyclerView.getWidth() - recyclerView.getPaddingRight();
            canvas.clipRect(i, recyclerView.getPaddingTop(), width, recyclerView.getHeight() - recyclerView.getPaddingBottom());
        } else {
            width = recyclerView.getWidth();
            i = 0;
        }
        int childCount = recyclerView.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = recyclerView.getChildAt(i2);
            recyclerView.getDecoratedBoundsWithMargins(childAt, this.f1770d);
            int round = Math.round(ag.n(childAt)) + this.f1770d.bottom;
            this.f1768b.setBounds(i, round - this.f1768b.getIntrinsicHeight(), width, round);
            this.f1768b.draw(canvas);
        }
        canvas.restore();
    }

    @SuppressLint({"NewApi"})
    private void d(Canvas canvas, RecyclerView recyclerView) {
        int height;
        int i;
        canvas.save();
        if (recyclerView.getClipToPadding()) {
            i = recyclerView.getPaddingTop();
            height = recyclerView.getHeight() - recyclerView.getPaddingBottom();
            canvas.clipRect(recyclerView.getPaddingLeft(), i, recyclerView.getWidth() - recyclerView.getPaddingRight(), height);
        } else {
            height = recyclerView.getHeight();
            i = 0;
        }
        int childCount = recyclerView.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = recyclerView.getChildAt(i2);
            recyclerView.getLayoutManager().a(childAt, this.f1770d);
            int round = Math.round(ag.m(childAt)) + this.f1770d.right;
            this.f1768b.setBounds(round - this.f1768b.getIntrinsicWidth(), i, round, height);
            this.f1768b.draw(canvas);
        }
        canvas.restore();
    }

    public void a(Rect rect, View view, RecyclerView recyclerView, RecyclerView.r rVar) {
        if (this.f1769c == 1) {
            rect.set(0, 0, 0, this.f1768b.getIntrinsicHeight());
        } else {
            rect.set(0, 0, this.f1768b.getIntrinsicWidth(), 0);
        }
    }
}
