package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ay;
import android.support.v4.view.bc;
import android.support.v7.a.a;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ScrollingTabContainerView extends HorizontalScrollView implements AdapterView.OnItemSelectedListener {
    private static final Interpolator l = new DecelerateInterpolator();

    /* renamed from: a  reason: collision with root package name */
    Runnable f1936a;

    /* renamed from: b  reason: collision with root package name */
    LinearLayoutCompat f1937b;

    /* renamed from: c  reason: collision with root package name */
    int f1938c;

    /* renamed from: d  reason: collision with root package name */
    int f1939d;

    /* renamed from: e  reason: collision with root package name */
    protected ay f1940e;

    /* renamed from: f  reason: collision with root package name */
    protected final VisibilityAnimListener f1941f = new VisibilityAnimListener();

    /* renamed from: g  reason: collision with root package name */
    private b f1942g;

    /* renamed from: h  reason: collision with root package name */
    private Spinner f1943h;
    private boolean i;
    private int j;
    private int k;

    public ScrollingTabContainerView(Context context) {
        super(context);
        setHorizontalScrollBarEnabled(false);
        android.support.v7.view.a a2 = android.support.v7.view.a.a(context);
        setContentHeight(a2.e());
        this.f1939d = a2.g();
        this.f1937b = d();
        addView(this.f1937b, new ViewGroup.LayoutParams(-2, -1));
    }

    public void onMeasure(int i2, int i3) {
        boolean z = true;
        int mode = View.MeasureSpec.getMode(i2);
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.f1937b.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.f1938c = -1;
        } else {
            if (childCount > 2) {
                this.f1938c = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.f1938c = View.MeasureSpec.getSize(i2) / 2;
            }
            this.f1938c = Math.min(this.f1938c, this.f1939d);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.j, 1073741824);
        if (z2 || !this.i) {
            z = false;
        }
        if (z) {
            this.f1937b.measure(0, makeMeasureSpec);
            if (this.f1937b.getMeasuredWidth() > View.MeasureSpec.getSize(i2)) {
                b();
            } else {
                c();
            }
        } else {
            c();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.k);
        }
    }

    private boolean a() {
        return this.f1943h != null && this.f1943h.getParent() == this;
    }

    public void setAllowCollapse(boolean z) {
        this.i = z;
    }

    private void b() {
        if (!a()) {
            if (this.f1943h == null) {
                this.f1943h = e();
            }
            removeView(this.f1937b);
            addView(this.f1943h, new ViewGroup.LayoutParams(-2, -1));
            if (this.f1943h.getAdapter() == null) {
                this.f1943h.setAdapter((SpinnerAdapter) new a());
            }
            if (this.f1936a != null) {
                removeCallbacks(this.f1936a);
                this.f1936a = null;
            }
            this.f1943h.setSelection(this.k);
        }
    }

    private boolean c() {
        if (a()) {
            removeView(this.f1943h);
            addView(this.f1937b, new ViewGroup.LayoutParams(-2, -1));
            setTabSelected(this.f1943h.getSelectedItemPosition());
        }
        return false;
    }

    public void setTabSelected(int i2) {
        boolean z;
        this.k = i2;
        int childCount = this.f1937b.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.f1937b.getChildAt(i3);
            if (i3 == i2) {
                z = true;
            } else {
                z = false;
            }
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
        }
        if (this.f1943h != null && i2 >= 0) {
            this.f1943h.setSelection(i2);
        }
    }

    public void setContentHeight(int i2) {
        this.j = i2;
        requestLayout();
    }

    private LinearLayoutCompat d() {
        LinearLayoutCompat linearLayoutCompat = new LinearLayoutCompat(getContext(), null, a.C0027a.actionBarTabBarStyle);
        linearLayoutCompat.setMeasureWithLargestChildEnabled(true);
        linearLayoutCompat.setGravity(17);
        linearLayoutCompat.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
        return linearLayoutCompat;
    }

    private Spinner e() {
        AppCompatSpinner appCompatSpinner = new AppCompatSpinner(getContext(), null, a.C0027a.actionDropDownStyle);
        appCompatSpinner.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
        appCompatSpinner.setOnItemSelectedListener(this);
        return appCompatSpinner;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        android.support.v7.view.a a2 = android.support.v7.view.a.a(getContext());
        setContentHeight(a2.e());
        this.f1939d = a2.g();
    }

    public void a(int i2) {
        final View childAt = this.f1937b.getChildAt(i2);
        if (this.f1936a != null) {
            removeCallbacks(this.f1936a);
        }
        this.f1936a = new Runnable() {
            public void run() {
                ScrollingTabContainerView.this.smoothScrollTo(childAt.getLeft() - ((ScrollingTabContainerView.this.getWidth() - childAt.getWidth()) / 2), 0);
                ScrollingTabContainerView.this.f1936a = null;
            }
        };
        post(this.f1936a);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f1936a != null) {
            post(this.f1936a);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f1936a != null) {
            removeCallbacks(this.f1936a);
        }
    }

    /* access modifiers changed from: package-private */
    public c a(ActionBar.b bVar, boolean z) {
        c cVar = new c(getContext(), bVar, z);
        if (z) {
            cVar.setBackgroundDrawable(null);
            cVar.setLayoutParams(new AbsListView.LayoutParams(-1, this.j));
        } else {
            cVar.setFocusable(true);
            if (this.f1942g == null) {
                this.f1942g = new b();
            }
            cVar.setOnClickListener(this.f1942g);
        }
        return cVar;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
        ((c) view).b().e();
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private class c extends LinearLayoutCompat implements View.OnLongClickListener {

        /* renamed from: b  reason: collision with root package name */
        private final int[] f1952b = {16842964};

        /* renamed from: c  reason: collision with root package name */
        private ActionBar.b f1953c;

        /* renamed from: d  reason: collision with root package name */
        private TextView f1954d;

        /* renamed from: e  reason: collision with root package name */
        private ImageView f1955e;

        /* renamed from: f  reason: collision with root package name */
        private View f1956f;

        public c(Context context, ActionBar.b bVar, boolean z) {
            super(context, null, a.C0027a.actionBarTabStyle);
            this.f1953c = bVar;
            an a2 = an.a(context, null, this.f1952b, a.C0027a.actionBarTabStyle, 0);
            if (a2.g(0)) {
                setBackgroundDrawable(a2.a(0));
            }
            a2.a();
            if (z) {
                setGravity(8388627);
            }
            a();
        }

        public void a(ActionBar.b bVar) {
            this.f1953c = bVar;
            a();
        }

        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z) {
                sendAccessibilityEvent(4);
            }
        }

        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(ActionBar.b.class.getName());
        }

        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            if (Build.VERSION.SDK_INT >= 14) {
                accessibilityNodeInfo.setClassName(ActionBar.b.class.getName());
            }
        }

        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (ScrollingTabContainerView.this.f1938c > 0 && getMeasuredWidth() > ScrollingTabContainerView.this.f1938c) {
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(ScrollingTabContainerView.this.f1938c, 1073741824), i2);
            }
        }

        public void a() {
            boolean z;
            ActionBar.b bVar = this.f1953c;
            View d2 = bVar.d();
            if (d2 != null) {
                ViewParent parent = d2.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(d2);
                    }
                    addView(d2);
                }
                this.f1956f = d2;
                if (this.f1954d != null) {
                    this.f1954d.setVisibility(8);
                }
                if (this.f1955e != null) {
                    this.f1955e.setVisibility(8);
                    this.f1955e.setImageDrawable(null);
                    return;
                }
                return;
            }
            if (this.f1956f != null) {
                removeView(this.f1956f);
                this.f1956f = null;
            }
            Drawable b2 = bVar.b();
            CharSequence c2 = bVar.c();
            if (b2 != null) {
                if (this.f1955e == null) {
                    AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
                    LinearLayoutCompat.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(-2, -2);
                    layoutParams.f1786h = 16;
                    appCompatImageView.setLayoutParams(layoutParams);
                    addView(appCompatImageView, 0);
                    this.f1955e = appCompatImageView;
                }
                this.f1955e.setImageDrawable(b2);
                this.f1955e.setVisibility(0);
            } else if (this.f1955e != null) {
                this.f1955e.setVisibility(8);
                this.f1955e.setImageDrawable(null);
            }
            if (!TextUtils.isEmpty(c2)) {
                z = true;
            } else {
                z = false;
            }
            if (z) {
                if (this.f1954d == null) {
                    AppCompatTextView appCompatTextView = new AppCompatTextView(getContext(), null, a.C0027a.actionBarTabTextStyle);
                    appCompatTextView.setEllipsize(TextUtils.TruncateAt.END);
                    LinearLayoutCompat.LayoutParams layoutParams2 = new LinearLayoutCompat.LayoutParams(-2, -2);
                    layoutParams2.f1786h = 16;
                    appCompatTextView.setLayoutParams(layoutParams2);
                    addView(appCompatTextView);
                    this.f1954d = appCompatTextView;
                }
                this.f1954d.setText(c2);
                this.f1954d.setVisibility(0);
            } else if (this.f1954d != null) {
                this.f1954d.setVisibility(8);
                this.f1954d.setText((CharSequence) null);
            }
            if (this.f1955e != null) {
                this.f1955e.setContentDescription(bVar.f());
            }
            if (z || TextUtils.isEmpty(bVar.f())) {
                setOnLongClickListener(null);
                setLongClickable(false);
                return;
            }
            setOnLongClickListener(this);
        }

        public boolean onLongClick(View view) {
            int[] iArr = new int[2];
            getLocationOnScreen(iArr);
            Context context = getContext();
            int width = getWidth();
            int height = getHeight();
            int i = context.getResources().getDisplayMetrics().widthPixels;
            Toast makeText = Toast.makeText(context, this.f1953c.f(), 0);
            makeText.setGravity(49, (iArr[0] + (width / 2)) - (i / 2), height);
            makeText.show();
            return true;
        }

        public ActionBar.b b() {
            return this.f1953c;
        }
    }

    private class a extends BaseAdapter {
        a() {
        }

        public int getCount() {
            return ScrollingTabContainerView.this.f1937b.getChildCount();
        }

        public Object getItem(int i) {
            return ((c) ScrollingTabContainerView.this.f1937b.getChildAt(i)).b();
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                return ScrollingTabContainerView.this.a((ActionBar.b) getItem(i), true);
            }
            ((c) view).a((ActionBar.b) getItem(i));
            return view;
        }
    }

    private class b implements View.OnClickListener {
        b() {
        }

        public void onClick(View view) {
            boolean z;
            ((c) view).b().e();
            int childCount = ScrollingTabContainerView.this.f1937b.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = ScrollingTabContainerView.this.f1937b.getChildAt(i);
                if (childAt == view) {
                    z = true;
                } else {
                    z = false;
                }
                childAt.setSelected(z);
            }
        }
    }

    protected class VisibilityAnimListener implements bc {

        /* renamed from: b  reason: collision with root package name */
        private boolean f1947b = false;

        /* renamed from: c  reason: collision with root package name */
        private int f1948c;

        protected VisibilityAnimListener() {
        }

        public void onAnimationStart(View view) {
            ScrollingTabContainerView.this.setVisibility(0);
            this.f1947b = false;
        }

        public void onAnimationEnd(View view) {
            if (!this.f1947b) {
                ScrollingTabContainerView.this.f1940e = null;
                ScrollingTabContainerView.this.setVisibility(this.f1948c);
            }
        }

        public void onAnimationCancel(View view) {
            this.f1947b = true;
        }
    }
}
