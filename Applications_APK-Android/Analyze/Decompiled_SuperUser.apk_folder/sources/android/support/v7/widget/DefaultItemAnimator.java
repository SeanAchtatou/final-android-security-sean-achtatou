package android.support.v7.widget;

import android.support.v4.view.ag;
import android.support.v4.view.ay;
import android.support.v4.view.bc;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DefaultItemAnimator extends SimpleItemAnimator {

    /* renamed from: a  reason: collision with root package name */
    ArrayList<ArrayList<RecyclerView.u>> f1725a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    ArrayList<ArrayList<b>> f1726b = new ArrayList<>();

    /* renamed from: c  reason: collision with root package name */
    ArrayList<ArrayList<a>> f1727c = new ArrayList<>();

    /* renamed from: d  reason: collision with root package name */
    ArrayList<RecyclerView.u> f1728d = new ArrayList<>();

    /* renamed from: e  reason: collision with root package name */
    ArrayList<RecyclerView.u> f1729e = new ArrayList<>();

    /* renamed from: f  reason: collision with root package name */
    ArrayList<RecyclerView.u> f1730f = new ArrayList<>();

    /* renamed from: g  reason: collision with root package name */
    ArrayList<RecyclerView.u> f1731g = new ArrayList<>();
    private ArrayList<RecyclerView.u> i = new ArrayList<>();
    private ArrayList<RecyclerView.u> j = new ArrayList<>();
    private ArrayList<b> k = new ArrayList<>();
    private ArrayList<a> l = new ArrayList<>();

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        public RecyclerView.u f1762a;

        /* renamed from: b  reason: collision with root package name */
        public int f1763b;

        /* renamed from: c  reason: collision with root package name */
        public int f1764c;

        /* renamed from: d  reason: collision with root package name */
        public int f1765d;

        /* renamed from: e  reason: collision with root package name */
        public int f1766e;

        b(RecyclerView.u uVar, int i, int i2, int i3, int i4) {
            this.f1762a = uVar;
            this.f1763b = i;
            this.f1764c = i2;
            this.f1765d = i3;
            this.f1766e = i4;
        }
    }

    private static class a {

        /* renamed from: a  reason: collision with root package name */
        public RecyclerView.u f1756a;

        /* renamed from: b  reason: collision with root package name */
        public RecyclerView.u f1757b;

        /* renamed from: c  reason: collision with root package name */
        public int f1758c;

        /* renamed from: d  reason: collision with root package name */
        public int f1759d;

        /* renamed from: e  reason: collision with root package name */
        public int f1760e;

        /* renamed from: f  reason: collision with root package name */
        public int f1761f;

        private a(RecyclerView.u uVar, RecyclerView.u uVar2) {
            this.f1756a = uVar;
            this.f1757b = uVar2;
        }

        a(RecyclerView.u uVar, RecyclerView.u uVar2, int i, int i2, int i3, int i4) {
            this(uVar, uVar2);
            this.f1758c = i;
            this.f1759d = i2;
            this.f1760e = i3;
            this.f1761f = i4;
        }

        public String toString() {
            return "ChangeInfo{oldHolder=" + this.f1756a + ", newHolder=" + this.f1757b + ", fromX=" + this.f1758c + ", fromY=" + this.f1759d + ", toX=" + this.f1760e + ", toY=" + this.f1761f + '}';
        }
    }

    public void a() {
        boolean z;
        boolean z2;
        boolean z3;
        long j2;
        long j3;
        boolean z4 = !this.i.isEmpty();
        if (!this.k.isEmpty()) {
            z = true;
        } else {
            z = false;
        }
        if (!this.l.isEmpty()) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!this.j.isEmpty()) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (z4 || z || z3 || z2) {
            Iterator<RecyclerView.u> it = this.i.iterator();
            while (it.hasNext()) {
                u(it.next());
            }
            this.i.clear();
            if (z) {
                final ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.k);
                this.f1726b.add(arrayList);
                this.k.clear();
                AnonymousClass1 r8 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            b bVar = (b) it.next();
                            DefaultItemAnimator.this.b(bVar.f1762a, bVar.f1763b, bVar.f1764c, bVar.f1765d, bVar.f1766e);
                        }
                        arrayList.clear();
                        DefaultItemAnimator.this.f1726b.remove(arrayList);
                    }
                };
                if (z4) {
                    ag.a(((b) arrayList.get(0)).f1762a.itemView, r8, g());
                } else {
                    r8.run();
                }
            }
            if (z2) {
                final ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(this.l);
                this.f1727c.add(arrayList2);
                this.l.clear();
                AnonymousClass2 r82 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList2.iterator();
                        while (it.hasNext()) {
                            DefaultItemAnimator.this.a((a) it.next());
                        }
                        arrayList2.clear();
                        DefaultItemAnimator.this.f1727c.remove(arrayList2);
                    }
                };
                if (z4) {
                    ag.a(((a) arrayList2.get(0)).f1756a.itemView, r82, g());
                } else {
                    r82.run();
                }
            }
            if (z3) {
                final ArrayList arrayList3 = new ArrayList();
                arrayList3.addAll(this.j);
                this.f1725a.add(arrayList3);
                this.j.clear();
                AnonymousClass3 r12 = new Runnable() {
                    public void run() {
                        Iterator it = arrayList3.iterator();
                        while (it.hasNext()) {
                            DefaultItemAnimator.this.c((RecyclerView.u) it.next());
                        }
                        arrayList3.clear();
                        DefaultItemAnimator.this.f1725a.remove(arrayList3);
                    }
                };
                if (z4 || z || z2) {
                    long g2 = z4 ? g() : 0;
                    if (z) {
                        j2 = e();
                    } else {
                        j2 = 0;
                    }
                    if (z2) {
                        j3 = h();
                    } else {
                        j3 = 0;
                    }
                    ag.a(((RecyclerView.u) arrayList3.get(0)).itemView, r12, g2 + Math.max(j2, j3));
                    return;
                }
                r12.run();
            }
        }
    }

    public boolean a(RecyclerView.u uVar) {
        v(uVar);
        this.i.add(uVar);
        return true;
    }

    private void u(final RecyclerView.u uVar) {
        final ay r = ag.r(uVar.itemView);
        this.f1730f.add(uVar);
        r.a(g()).a(0.0f).a(new c() {
            public void onAnimationStart(View view) {
                DefaultItemAnimator.this.l(uVar);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.view.ag.c(android.view.View, float):void
             arg types: [android.view.View, int]
             candidates:
              android.support.v4.view.ag.c(android.view.View, int):void
              android.support.v4.view.ag.c(android.view.View, boolean):void
              android.support.v4.view.ag.c(android.view.View, float):void */
            public void onAnimationEnd(View view) {
                r.a((bc) null);
                ag.c(view, 1.0f);
                DefaultItemAnimator.this.i(uVar);
                DefaultItemAnimator.this.f1730f.remove(uVar);
                DefaultItemAnimator.this.c();
            }
        }).c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, int):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, float):void */
    public boolean b(RecyclerView.u uVar) {
        v(uVar);
        ag.c(uVar.itemView, 0.0f);
        this.j.add(uVar);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void c(final RecyclerView.u uVar) {
        final ay r = ag.r(uVar.itemView);
        this.f1728d.add(uVar);
        r.a(1.0f).a(f()).a(new c() {
            public void onAnimationStart(View view) {
                DefaultItemAnimator.this.n(uVar);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.view.ag.c(android.view.View, float):void
             arg types: [android.view.View, int]
             candidates:
              android.support.v4.view.ag.c(android.view.View, int):void
              android.support.v4.view.ag.c(android.view.View, boolean):void
              android.support.v4.view.ag.c(android.view.View, float):void */
            public void onAnimationCancel(View view) {
                ag.c(view, 1.0f);
            }

            public void onAnimationEnd(View view) {
                r.a((bc) null);
                DefaultItemAnimator.this.k(uVar);
                DefaultItemAnimator.this.f1728d.remove(uVar);
                DefaultItemAnimator.this.c();
            }
        }).c();
    }

    public boolean a(RecyclerView.u uVar, int i2, int i3, int i4, int i5) {
        View view = uVar.itemView;
        int m = (int) (((float) i2) + ag.m(uVar.itemView));
        int n = (int) (((float) i3) + ag.n(uVar.itemView));
        v(uVar);
        int i6 = i4 - m;
        int i7 = i5 - n;
        if (i6 == 0 && i7 == 0) {
            j(uVar);
            return false;
        }
        if (i6 != 0) {
            ag.a(view, (float) (-i6));
        }
        if (i7 != 0) {
            ag.b(view, (float) (-i7));
        }
        this.k.add(new b(uVar, m, n, i4, i5));
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(RecyclerView.u uVar, int i2, int i3, int i4, int i5) {
        View view = uVar.itemView;
        final int i6 = i4 - i2;
        final int i7 = i5 - i3;
        if (i6 != 0) {
            ag.r(view).b(0.0f);
        }
        if (i7 != 0) {
            ag.r(view).c(0.0f);
        }
        final ay r = ag.r(view);
        this.f1729e.add(uVar);
        final RecyclerView.u uVar2 = uVar;
        r.a(e()).a(new c() {
            public void onAnimationStart(View view) {
                DefaultItemAnimator.this.m(uVar2);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.view.ag.a(android.view.View, float):void
             arg types: [android.view.View, int]
             candidates:
              android.support.v4.view.ag.a(int, int):int
              android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
              android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
              android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
              android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
              android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
              android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
              android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
              android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
              android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
              android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
              android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
              android.support.v4.view.ag.a(android.view.View, boolean):void
              android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
              android.support.v4.view.ag.a(android.view.View, int):boolean
              android.support.v4.view.ag.a(android.view.View, float):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.view.ag.b(android.view.View, float):void
             arg types: [android.view.View, int]
             candidates:
              android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
              android.support.v4.view.ag.b(android.view.View, boolean):void
              android.support.v4.view.ag.b(android.view.View, int):boolean
              android.support.v4.view.ag.b(android.view.View, float):void */
            public void onAnimationCancel(View view) {
                if (i6 != 0) {
                    ag.a(view, 0.0f);
                }
                if (i7 != 0) {
                    ag.b(view, 0.0f);
                }
            }

            public void onAnimationEnd(View view) {
                r.a((bc) null);
                DefaultItemAnimator.this.j(uVar2);
                DefaultItemAnimator.this.f1729e.remove(uVar2);
                DefaultItemAnimator.this.c();
            }
        }).c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, int):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, float):void */
    public boolean a(RecyclerView.u uVar, RecyclerView.u uVar2, int i2, int i3, int i4, int i5) {
        if (uVar == uVar2) {
            return a(uVar, i2, i3, i4, i5);
        }
        float m = ag.m(uVar.itemView);
        float n = ag.n(uVar.itemView);
        float e2 = ag.e(uVar.itemView);
        v(uVar);
        int i6 = (int) (((float) (i4 - i2)) - m);
        int i7 = (int) (((float) (i5 - i3)) - n);
        ag.a(uVar.itemView, m);
        ag.b(uVar.itemView, n);
        ag.c(uVar.itemView, e2);
        if (uVar2 != null) {
            v(uVar2);
            ag.a(uVar2.itemView, (float) (-i6));
            ag.b(uVar2.itemView, (float) (-i7));
            ag.c(uVar2.itemView, 0.0f);
        }
        this.l.add(new a(uVar, uVar2, i2, i3, i4, i5));
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(final a aVar) {
        final View view = null;
        RecyclerView.u uVar = aVar.f1756a;
        View view2 = uVar == null ? null : uVar.itemView;
        RecyclerView.u uVar2 = aVar.f1757b;
        if (uVar2 != null) {
            view = uVar2.itemView;
        }
        if (view2 != null) {
            final ay a2 = ag.r(view2).a(h());
            this.f1731g.add(aVar.f1756a);
            a2.b((float) (aVar.f1760e - aVar.f1758c));
            a2.c((float) (aVar.f1761f - aVar.f1759d));
            a2.a(0.0f).a(new c() {
                public void onAnimationStart(View view) {
                    DefaultItemAnimator.this.b(aVar.f1756a, true);
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v4.view.ag.c(android.view.View, float):void
                 arg types: [android.view.View, int]
                 candidates:
                  android.support.v4.view.ag.c(android.view.View, int):void
                  android.support.v4.view.ag.c(android.view.View, boolean):void
                  android.support.v4.view.ag.c(android.view.View, float):void */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v4.view.ag.a(android.view.View, float):void
                 arg types: [android.view.View, int]
                 candidates:
                  android.support.v4.view.ag.a(int, int):int
                  android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
                  android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
                  android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
                  android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
                  android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
                  android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
                  android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
                  android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
                  android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
                  android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
                  android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
                  android.support.v4.view.ag.a(android.view.View, boolean):void
                  android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
                  android.support.v4.view.ag.a(android.view.View, int):boolean
                  android.support.v4.view.ag.a(android.view.View, float):void */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v4.view.ag.b(android.view.View, float):void
                 arg types: [android.view.View, int]
                 candidates:
                  android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
                  android.support.v4.view.ag.b(android.view.View, boolean):void
                  android.support.v4.view.ag.b(android.view.View, int):boolean
                  android.support.v4.view.ag.b(android.view.View, float):void */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v7.widget.SimpleItemAnimator.a(android.support.v7.widget.RecyclerView$u, boolean):void
                 arg types: [android.support.v7.widget.RecyclerView$u, int]
                 candidates:
                  android.support.v7.widget.DefaultItemAnimator.a(java.util.List<android.support.v7.widget.DefaultItemAnimator$a>, android.support.v7.widget.RecyclerView$u):void
                  android.support.v7.widget.DefaultItemAnimator.a(android.support.v7.widget.DefaultItemAnimator$a, android.support.v7.widget.RecyclerView$u):boolean
                  android.support.v7.widget.DefaultItemAnimator.a(android.support.v7.widget.RecyclerView$u, java.util.List<java.lang.Object>):boolean
                  android.support.v7.widget.RecyclerView.e.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$u):android.support.v7.widget.RecyclerView$e$c
                  android.support.v7.widget.RecyclerView.e.a(android.support.v7.widget.RecyclerView$u, java.util.List<java.lang.Object>):boolean
                  android.support.v7.widget.SimpleItemAnimator.a(android.support.v7.widget.RecyclerView$u, boolean):void */
                public void onAnimationEnd(View view) {
                    a2.a((bc) null);
                    ag.c(view, 1.0f);
                    ag.a(view, 0.0f);
                    ag.b(view, 0.0f);
                    DefaultItemAnimator.this.a(aVar.f1756a, true);
                    DefaultItemAnimator.this.f1731g.remove(aVar.f1756a);
                    DefaultItemAnimator.this.c();
                }
            }).c();
        }
        if (view != null) {
            final ay r = ag.r(view);
            this.f1731g.add(aVar.f1757b);
            r.b(0.0f).c(0.0f).a(h()).a(1.0f).a(new c() {
                public void onAnimationStart(View view) {
                    DefaultItemAnimator.this.b(aVar.f1757b, false);
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v4.view.ag.c(android.view.View, float):void
                 arg types: [android.view.View, int]
                 candidates:
                  android.support.v4.view.ag.c(android.view.View, int):void
                  android.support.v4.view.ag.c(android.view.View, boolean):void
                  android.support.v4.view.ag.c(android.view.View, float):void */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v4.view.ag.a(android.view.View, float):void
                 arg types: [android.view.View, int]
                 candidates:
                  android.support.v4.view.ag.a(int, int):int
                  android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
                  android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
                  android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
                  android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
                  android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
                  android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
                  android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
                  android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
                  android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
                  android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
                  android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
                  android.support.v4.view.ag.a(android.view.View, boolean):void
                  android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
                  android.support.v4.view.ag.a(android.view.View, int):boolean
                  android.support.v4.view.ag.a(android.view.View, float):void */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v4.view.ag.b(android.view.View, float):void
                 arg types: [android.view.View, int]
                 candidates:
                  android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
                  android.support.v4.view.ag.b(android.view.View, boolean):void
                  android.support.v4.view.ag.b(android.view.View, int):boolean
                  android.support.v4.view.ag.b(android.view.View, float):void */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: android.support.v7.widget.SimpleItemAnimator.a(android.support.v7.widget.RecyclerView$u, boolean):void
                 arg types: [android.support.v7.widget.RecyclerView$u, int]
                 candidates:
                  android.support.v7.widget.DefaultItemAnimator.a(java.util.List<android.support.v7.widget.DefaultItemAnimator$a>, android.support.v7.widget.RecyclerView$u):void
                  android.support.v7.widget.DefaultItemAnimator.a(android.support.v7.widget.DefaultItemAnimator$a, android.support.v7.widget.RecyclerView$u):boolean
                  android.support.v7.widget.DefaultItemAnimator.a(android.support.v7.widget.RecyclerView$u, java.util.List<java.lang.Object>):boolean
                  android.support.v7.widget.RecyclerView.e.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$u):android.support.v7.widget.RecyclerView$e$c
                  android.support.v7.widget.RecyclerView.e.a(android.support.v7.widget.RecyclerView$u, java.util.List<java.lang.Object>):boolean
                  android.support.v7.widget.SimpleItemAnimator.a(android.support.v7.widget.RecyclerView$u, boolean):void */
                public void onAnimationEnd(View view) {
                    r.a((bc) null);
                    ag.c(view, 1.0f);
                    ag.a(view, 0.0f);
                    ag.b(view, 0.0f);
                    DefaultItemAnimator.this.a(aVar.f1757b, false);
                    DefaultItemAnimator.this.f1731g.remove(aVar.f1757b);
                    DefaultItemAnimator.this.c();
                }
            }).c();
        }
    }

    private void a(List<a> list, RecyclerView.u uVar) {
        for (int size = list.size() - 1; size >= 0; size--) {
            a aVar = list.get(size);
            if (a(aVar, uVar) && aVar.f1756a == null && aVar.f1757b == null) {
                list.remove(aVar);
            }
        }
    }

    private void b(a aVar) {
        if (aVar.f1756a != null) {
            a(aVar, aVar.f1756a);
        }
        if (aVar.f1757b != null) {
            a(aVar, aVar.f1757b);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, int):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.a(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.a(int, int):int
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
      android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
      android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.ag.a(android.view.View, boolean):void
      android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
      android.support.v4.view.ag.a(android.view.View, int):boolean
      android.support.v4.view.ag.a(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.b(android.view.View, boolean):void
      android.support.v4.view.ag.b(android.view.View, int):boolean
      android.support.v4.view.ag.b(android.view.View, float):void */
    private boolean a(a aVar, RecyclerView.u uVar) {
        boolean z = false;
        if (aVar.f1757b == uVar) {
            aVar.f1757b = null;
        } else if (aVar.f1756a != uVar) {
            return false;
        } else {
            aVar.f1756a = null;
            z = true;
        }
        ag.c(uVar.itemView, 1.0f);
        ag.a(uVar.itemView, 0.0f);
        ag.b(uVar.itemView, 0.0f);
        a(uVar, z);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.b(android.view.View, boolean):void
      android.support.v4.view.ag.b(android.view.View, int):boolean
      android.support.v4.view.ag.b(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.a(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.a(int, int):int
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
      android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
      android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.ag.a(android.view.View, boolean):void
      android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
      android.support.v4.view.ag.a(android.view.View, int):boolean
      android.support.v4.view.ag.a(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, int):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, float):void */
    public void d(RecyclerView.u uVar) {
        View view = uVar.itemView;
        ag.r(view).b();
        for (int size = this.k.size() - 1; size >= 0; size--) {
            if (this.k.get(size).f1762a == uVar) {
                ag.b(view, 0.0f);
                ag.a(view, 0.0f);
                j(uVar);
                this.k.remove(size);
            }
        }
        a(this.l, uVar);
        if (this.i.remove(uVar)) {
            ag.c(view, 1.0f);
            i(uVar);
        }
        if (this.j.remove(uVar)) {
            ag.c(view, 1.0f);
            k(uVar);
        }
        for (int size2 = this.f1727c.size() - 1; size2 >= 0; size2--) {
            ArrayList arrayList = this.f1727c.get(size2);
            a(arrayList, uVar);
            if (arrayList.isEmpty()) {
                this.f1727c.remove(size2);
            }
        }
        for (int size3 = this.f1726b.size() - 1; size3 >= 0; size3--) {
            ArrayList arrayList2 = this.f1726b.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (((b) arrayList2.get(size4)).f1762a == uVar) {
                    ag.b(view, 0.0f);
                    ag.a(view, 0.0f);
                    j(uVar);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.f1726b.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.f1725a.size() - 1; size5 >= 0; size5--) {
            ArrayList arrayList3 = this.f1725a.get(size5);
            if (arrayList3.remove(uVar)) {
                ag.c(view, 1.0f);
                k(uVar);
                if (arrayList3.isEmpty()) {
                    this.f1725a.remove(size5);
                }
            }
        }
        if (this.f1730f.remove(uVar)) {
        }
        if (this.f1728d.remove(uVar)) {
        }
        if (this.f1731g.remove(uVar)) {
        }
        if (this.f1729e.remove(uVar)) {
        }
        c();
    }

    private void v(RecyclerView.u uVar) {
        android.support.v4.a.a.a(uVar.itemView);
        d(uVar);
    }

    public boolean b() {
        return !this.j.isEmpty() || !this.l.isEmpty() || !this.k.isEmpty() || !this.i.isEmpty() || !this.f1729e.isEmpty() || !this.f1730f.isEmpty() || !this.f1728d.isEmpty() || !this.f1731g.isEmpty() || !this.f1726b.isEmpty() || !this.f1725a.isEmpty() || !this.f1727c.isEmpty();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (!b()) {
            i();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.b(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.b(android.view.View, boolean):void
      android.support.v4.view.ag.b(android.view.View, int):boolean
      android.support.v4.view.ag.b(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.a(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.a(int, int):int
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.be):android.support.v4.view.be
      android.support.v4.view.ag.a(android.view.View, android.content.res.ColorStateList):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.PorterDuff$Mode):void
      android.support.v4.view.ag.a(android.view.View, android.graphics.drawable.Drawable):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a.e):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.aa):void
      android.support.v4.view.ag.a(android.view.View, android.support.v4.view.y):void
      android.support.v4.view.ag.a(android.view.View, android.view.accessibility.AccessibilityEvent):void
      android.support.v4.view.ag.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.ag.a(android.view.View, boolean):void
      android.support.v4.view.ag.a(android.view.ViewGroup, boolean):void
      android.support.v4.view.ag.a(android.view.View, int):boolean
      android.support.v4.view.ag.a(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, int):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, float):void */
    public void d() {
        for (int size = this.k.size() - 1; size >= 0; size--) {
            b bVar = this.k.get(size);
            View view = bVar.f1762a.itemView;
            ag.b(view, 0.0f);
            ag.a(view, 0.0f);
            j(bVar.f1762a);
            this.k.remove(size);
        }
        for (int size2 = this.i.size() - 1; size2 >= 0; size2--) {
            i(this.i.get(size2));
            this.i.remove(size2);
        }
        for (int size3 = this.j.size() - 1; size3 >= 0; size3--) {
            RecyclerView.u uVar = this.j.get(size3);
            ag.c(uVar.itemView, 1.0f);
            k(uVar);
            this.j.remove(size3);
        }
        for (int size4 = this.l.size() - 1; size4 >= 0; size4--) {
            b(this.l.get(size4));
        }
        this.l.clear();
        if (b()) {
            for (int size5 = this.f1726b.size() - 1; size5 >= 0; size5--) {
                ArrayList arrayList = this.f1726b.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    b bVar2 = (b) arrayList.get(size6);
                    View view2 = bVar2.f1762a.itemView;
                    ag.b(view2, 0.0f);
                    ag.a(view2, 0.0f);
                    j(bVar2.f1762a);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.f1726b.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.f1725a.size() - 1; size7 >= 0; size7--) {
                ArrayList arrayList2 = this.f1725a.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    RecyclerView.u uVar2 = (RecyclerView.u) arrayList2.get(size8);
                    ag.c(uVar2.itemView, 1.0f);
                    k(uVar2);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.f1725a.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.f1727c.size() - 1; size9 >= 0; size9--) {
                ArrayList arrayList3 = this.f1727c.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    b((a) arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.f1727c.remove(arrayList3);
                    }
                }
            }
            a(this.f1730f);
            a(this.f1729e);
            a(this.f1728d);
            a(this.f1731g);
            i();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List<RecyclerView.u> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            ag.r(list.get(size).itemView).b();
        }
    }

    public boolean a(RecyclerView.u uVar, List<Object> list) {
        return !list.isEmpty() || super.a(uVar, list);
    }

    private static class c implements bc {
        c() {
        }

        public void onAnimationStart(View view) {
        }

        public void onAnimationEnd(View view) {
        }

        public void onAnimationCancel(View view) {
        }
    }
}
