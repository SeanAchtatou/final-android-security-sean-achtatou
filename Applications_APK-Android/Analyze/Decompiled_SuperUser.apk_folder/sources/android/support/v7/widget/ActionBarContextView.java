package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ag;
import android.support.v4.view.ay;
import android.support.v7.a.a;
import android.support.v7.view.b;
import android.support.v7.view.menu.MenuBuilder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActionBarContextView extends AbsActionBarView {

    /* renamed from: g  reason: collision with root package name */
    private CharSequence f1598g;

    /* renamed from: h  reason: collision with root package name */
    private CharSequence f1599h;
    private View i;
    private View j;
    private LinearLayout k;
    private TextView l;
    private TextView m;
    private int n;
    private int o;
    private boolean p;
    private int q;

    public /* bridge */ /* synthetic */ ay a(int i2, long j2) {
        return super.a(i2, j2);
    }

    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    public /* bridge */ /* synthetic */ boolean onHoverEvent(MotionEvent motionEvent) {
        return super.onHoverEvent(motionEvent);
    }

    public /* bridge */ /* synthetic */ boolean onTouchEvent(MotionEvent motionEvent) {
        return super.onTouchEvent(motionEvent);
    }

    public /* bridge */ /* synthetic */ void setVisibility(int i2) {
        super.setVisibility(i2);
    }

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0027a.actionModeStyle);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        an a2 = an.a(context, attributeSet, a.k.ActionMode, i2, 0);
        ag.a(this, a2.a(a.k.ActionMode_background));
        this.n = a2.g(a.k.ActionMode_titleTextStyle, 0);
        this.o = a2.g(a.k.ActionMode_subtitleTextStyle, 0);
        this.f1583e = a2.f(a.k.ActionMode_height, 0);
        this.q = a2.g(a.k.ActionMode_closeItemLayout, a.h.abc_action_mode_close_item_material);
        a2.a();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f1582d != null) {
            this.f1582d.d();
            this.f1582d.f();
        }
    }

    public void setContentHeight(int i2) {
        this.f1583e = i2;
    }

    public void setCustomView(View view) {
        if (this.j != null) {
            removeView(this.j);
        }
        this.j = view;
        if (!(view == null || this.k == null)) {
            removeView(this.k);
            this.k = null;
        }
        if (view != null) {
            addView(view);
        }
        requestLayout();
    }

    public void setTitle(CharSequence charSequence) {
        this.f1598g = charSequence;
        e();
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f1599h = charSequence;
        e();
    }

    public CharSequence getTitle() {
        return this.f1598g;
    }

    public CharSequence getSubtitle() {
        return this.f1599h;
    }

    private void e() {
        int i2;
        int i3 = 8;
        boolean z = true;
        if (this.k == null) {
            LayoutInflater.from(getContext()).inflate(a.h.abc_action_bar_title_item, this);
            this.k = (LinearLayout) getChildAt(getChildCount() - 1);
            this.l = (TextView) this.k.findViewById(a.f.action_bar_title);
            this.m = (TextView) this.k.findViewById(a.f.action_bar_subtitle);
            if (this.n != 0) {
                this.l.setTextAppearance(getContext(), this.n);
            }
            if (this.o != 0) {
                this.m.setTextAppearance(getContext(), this.o);
            }
        }
        this.l.setText(this.f1598g);
        this.m.setText(this.f1599h);
        boolean z2 = !TextUtils.isEmpty(this.f1598g);
        if (TextUtils.isEmpty(this.f1599h)) {
            z = false;
        }
        TextView textView = this.m;
        if (z) {
            i2 = 0;
        } else {
            i2 = 8;
        }
        textView.setVisibility(i2);
        LinearLayout linearLayout = this.k;
        if (z2 || z) {
            i3 = 0;
        }
        linearLayout.setVisibility(i3);
        if (this.k.getParent() == null) {
            addView(this.k);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.ActionBarContextView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void a(final b bVar) {
        if (this.i == null) {
            this.i = LayoutInflater.from(getContext()).inflate(this.q, (ViewGroup) this, false);
            addView(this.i);
        } else if (this.i.getParent() == null) {
            addView(this.i);
        }
        this.i.findViewById(a.f.action_mode_close_button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                bVar.c();
            }
        });
        MenuBuilder menuBuilder = (MenuBuilder) bVar.b();
        if (this.f1582d != null) {
            this.f1582d.e();
        }
        this.f1582d = new ActionMenuPresenter(getContext());
        this.f1582d.b(true);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
        menuBuilder.addMenuPresenter(this.f1582d, this.f1580b);
        this.f1581c = (ActionMenuView) this.f1582d.a(this);
        ag.a(this.f1581c, (Drawable) null);
        addView(this.f1581c, layoutParams);
    }

    public void b() {
        if (this.i == null) {
            c();
        }
    }

    public void c() {
        removeAllViews();
        this.j = null;
        this.f1581c = null;
    }

    public boolean a() {
        if (this.f1582d != null) {
            return this.f1582d.c();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int size;
        int i4;
        int i5;
        int i6 = 1073741824;
        int i7 = 0;
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_width=\"match_parent\" (or fill_parent)");
        } else if (View.MeasureSpec.getMode(i3) == 0) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_height=\"wrap_content\"");
        } else {
            int size2 = View.MeasureSpec.getSize(i2);
            if (this.f1583e > 0) {
                size = this.f1583e;
            } else {
                size = View.MeasureSpec.getSize(i3);
            }
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int paddingLeft = (size2 - getPaddingLeft()) - getPaddingRight();
            int i8 = size - paddingTop;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i8, Integer.MIN_VALUE);
            if (this.i != null) {
                int a2 = a(this.i, paddingLeft, makeMeasureSpec, 0);
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.i.getLayoutParams();
                paddingLeft = a2 - (marginLayoutParams.rightMargin + marginLayoutParams.leftMargin);
            }
            if (this.f1581c != null && this.f1581c.getParent() == this) {
                paddingLeft = a(this.f1581c, paddingLeft, makeMeasureSpec, 0);
            }
            if (this.k != null && this.j == null) {
                if (this.p) {
                    this.k.measure(View.MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                    int measuredWidth = this.k.getMeasuredWidth();
                    boolean z = measuredWidth <= paddingLeft;
                    if (z) {
                        paddingLeft -= measuredWidth;
                    }
                    this.k.setVisibility(z ? 0 : 8);
                } else {
                    paddingLeft = a(this.k, paddingLeft, makeMeasureSpec, 0);
                }
            }
            if (this.j != null) {
                ViewGroup.LayoutParams layoutParams = this.j.getLayoutParams();
                if (layoutParams.width != -2) {
                    i4 = 1073741824;
                } else {
                    i4 = Integer.MIN_VALUE;
                }
                if (layoutParams.width >= 0) {
                    paddingLeft = Math.min(layoutParams.width, paddingLeft);
                }
                if (layoutParams.height == -2) {
                    i6 = Integer.MIN_VALUE;
                }
                if (layoutParams.height >= 0) {
                    i5 = Math.min(layoutParams.height, i8);
                } else {
                    i5 = i8;
                }
                this.j.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, i4), View.MeasureSpec.makeMeasureSpec(i5, i6));
            }
            if (this.f1583e <= 0) {
                int childCount = getChildCount();
                int i9 = 0;
                while (i7 < childCount) {
                    int measuredHeight = getChildAt(i7).getMeasuredHeight() + paddingTop;
                    if (measuredHeight <= i9) {
                        measuredHeight = i9;
                    }
                    i7++;
                    i9 = measuredHeight;
                }
                setMeasuredDimension(size2, i9);
                return;
            }
            setMeasuredDimension(size2, size);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        boolean a2 = ar.a(this);
        int paddingRight = a2 ? (i4 - i2) - getPaddingRight() : getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i5 - i3) - getPaddingTop()) - getPaddingBottom();
        if (this.i == null || this.i.getVisibility() == 8) {
            i6 = paddingRight;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.i.getLayoutParams();
            int i7 = a2 ? marginLayoutParams.rightMargin : marginLayoutParams.leftMargin;
            int i8 = a2 ? marginLayoutParams.leftMargin : marginLayoutParams.rightMargin;
            int a3 = a(paddingRight, i7, a2);
            i6 = a(a(this.i, a3, paddingTop, paddingTop2, a2) + a3, i8, a2);
        }
        if (!(this.k == null || this.j != null || this.k.getVisibility() == 8)) {
            i6 += a(this.k, i6, paddingTop, paddingTop2, a2);
        }
        if (this.j != null) {
            int a4 = a(this.j, i6, paddingTop, paddingTop2, a2) + i6;
        }
        int paddingLeft = a2 ? getPaddingLeft() : (i4 - i2) - getPaddingRight();
        if (this.f1581c != null) {
            int a5 = a(this.f1581c, paddingLeft, paddingTop, paddingTop2, !a2) + paddingLeft;
        }
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (Build.VERSION.SDK_INT < 14) {
            return;
        }
        if (accessibilityEvent.getEventType() == 32) {
            accessibilityEvent.setSource(this);
            accessibilityEvent.setClassName(getClass().getName());
            accessibilityEvent.setPackageName(getContext().getPackageName());
            accessibilityEvent.setContentDescription(this.f1598g);
            return;
        }
        super.onInitializeAccessibilityEvent(accessibilityEvent);
    }

    public void setTitleOptional(boolean z) {
        if (z != this.p) {
            requestLayout();
        }
        this.p = z;
    }

    public boolean d() {
        return this.p;
    }
}
