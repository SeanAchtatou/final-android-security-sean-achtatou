package android.support.v7.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.support.v4.view.ActionProvider;
import android.support.v7.a.a;
import android.support.v7.c.a.b;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class ShareActionProvider extends ActionProvider {

    /* renamed from: a  reason: collision with root package name */
    final Context f1990a;

    /* renamed from: b  reason: collision with root package name */
    String f1991b = "share_history.xml";

    /* renamed from: c  reason: collision with root package name */
    private int f1992c = 4;

    /* renamed from: d  reason: collision with root package name */
    private final a f1993d = new a();

    public ShareActionProvider(Context context) {
        super(context);
        this.f1990a = context;
    }

    public View a() {
        ActivityChooserView activityChooserView = new ActivityChooserView(this.f1990a);
        if (!activityChooserView.isInEditMode()) {
            activityChooserView.setActivityChooserModel(c.a(this.f1990a, this.f1991b));
        }
        TypedValue typedValue = new TypedValue();
        this.f1990a.getTheme().resolveAttribute(a.C0027a.actionModeShareDrawable, typedValue, true);
        activityChooserView.setExpandActivityOverflowButtonDrawable(b.b(this.f1990a, typedValue.resourceId));
        activityChooserView.setProvider(this);
        activityChooserView.setDefaultActionButtonContentDescription(a.i.abc_shareactionprovider_share_with_application);
        activityChooserView.setExpandActivityOverflowButtonContentDescription(a.i.abc_shareactionprovider_share_with);
        return activityChooserView;
    }

    public boolean e() {
        return true;
    }

    public void a(SubMenu subMenu) {
        subMenu.clear();
        c a2 = c.a(this.f1990a, this.f1991b);
        PackageManager packageManager = this.f1990a.getPackageManager();
        int a3 = a2.a();
        int min = Math.min(a3, this.f1992c);
        for (int i = 0; i < min; i++) {
            ResolveInfo a4 = a2.a(i);
            subMenu.add(0, i, i, a4.loadLabel(packageManager)).setIcon(a4.loadIcon(packageManager)).setOnMenuItemClickListener(this.f1993d);
        }
        if (min < a3) {
            SubMenu addSubMenu = subMenu.addSubMenu(0, min, min, this.f1990a.getString(a.i.abc_activity_chooser_view_see_all));
            for (int i2 = 0; i2 < a3; i2++) {
                ResolveInfo a5 = a2.a(i2);
                addSubMenu.add(0, i2, i2, a5.loadLabel(packageManager)).setIcon(a5.loadIcon(packageManager)).setOnMenuItemClickListener(this.f1993d);
            }
        }
    }

    private class a implements MenuItem.OnMenuItemClickListener {
        a() {
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            Intent b2 = c.a(ShareActionProvider.this.f1990a, ShareActionProvider.this.f1991b).b(menuItem.getItemId());
            if (b2 == null) {
                return true;
            }
            String action = b2.getAction();
            if ("android.intent.action.SEND".equals(action) || "android.intent.action.SEND_MULTIPLE".equals(action)) {
                ShareActionProvider.this.a(b2);
            }
            ShareActionProvider.this.f1990a.startActivity(b2);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Intent intent) {
        if (Build.VERSION.SDK_INT >= 21) {
            intent.addFlags(134742016);
        } else {
            intent.addFlags(524288);
        }
    }
}
