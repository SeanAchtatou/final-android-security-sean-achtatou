package android.support.v7.widget;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v7.widget.af;

@TargetApi(17)
/* compiled from: CardViewJellybeanMr1 */
class q extends o {
    q() {
    }

    public void a() {
        af.f2106c = new af.a() {
            public void a(Canvas canvas, RectF rectF, float f2, Paint paint) {
                canvas.drawRoundRect(rectF, f2, f2, paint);
            }
        };
    }
}
