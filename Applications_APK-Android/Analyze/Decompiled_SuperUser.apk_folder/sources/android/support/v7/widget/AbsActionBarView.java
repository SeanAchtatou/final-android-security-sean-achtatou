package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.support.v4.view.ag;
import android.support.v4.view.ay;
import android.support.v4.view.bc;
import android.support.v4.view.s;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

abstract class AbsActionBarView extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    protected final VisibilityAnimListener f1579a;

    /* renamed from: b  reason: collision with root package name */
    protected final Context f1580b;

    /* renamed from: c  reason: collision with root package name */
    protected ActionMenuView f1581c;

    /* renamed from: d  reason: collision with root package name */
    protected ActionMenuPresenter f1582d;

    /* renamed from: e  reason: collision with root package name */
    protected int f1583e;

    /* renamed from: f  reason: collision with root package name */
    protected ay f1584f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f1585g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f1586h;

    AbsActionBarView(Context context) {
        this(context, null);
    }

    AbsActionBarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    AbsActionBarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1579a = new VisibilityAnimListener();
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(a.C0027a.actionBarPopupTheme, typedValue, true) || typedValue.resourceId == 0) {
            this.f1580b = context;
        } else {
            this.f1580b = new ContextThemeWrapper(context, typedValue.resourceId);
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, a.k.ActionBar, a.C0027a.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(a.k.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        if (this.f1582d != null) {
            this.f1582d.a(configuration);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a2 = s.a(motionEvent);
        if (a2 == 0) {
            this.f1585g = false;
        }
        if (!this.f1585g) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (a2 == 0 && !onTouchEvent) {
                this.f1585g = true;
            }
        }
        if (a2 == 1 || a2 == 3) {
            this.f1585g = false;
        }
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int a2 = s.a(motionEvent);
        if (a2 == 9) {
            this.f1586h = false;
        }
        if (!this.f1586h) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (a2 == 9 && !onHoverEvent) {
                this.f1586h = true;
            }
        }
        if (a2 == 10 || a2 == 3) {
            this.f1586h = false;
        }
        return true;
    }

    public void setContentHeight(int i) {
        this.f1583e = i;
        requestLayout();
    }

    public int getContentHeight() {
        return this.f1583e;
    }

    public int getAnimatedVisibility() {
        if (this.f1584f != null) {
            return this.f1579a.f1587a;
        }
        return getVisibility();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ag.c(android.view.View, float):void
     arg types: [android.support.v7.widget.AbsActionBarView, int]
     candidates:
      android.support.v4.view.ag.c(android.view.View, int):void
      android.support.v4.view.ag.c(android.view.View, boolean):void
      android.support.v4.view.ag.c(android.view.View, float):void */
    public ay a(int i, long j) {
        if (this.f1584f != null) {
            this.f1584f.b();
        }
        if (i == 0) {
            if (getVisibility() != 0) {
                ag.c((View) this, 0.0f);
            }
            ay a2 = ag.r(this).a(1.0f);
            a2.a(j);
            a2.a(this.f1579a.a(a2, i));
            return a2;
        }
        ay a3 = ag.r(this).a(0.0f);
        a3.a(j);
        a3.a(this.f1579a.a(a3, i));
        return a3;
    }

    public void setVisibility(int i) {
        if (i != getVisibility()) {
            if (this.f1584f != null) {
                this.f1584f.b();
            }
            super.setVisibility(i);
        }
    }

    public boolean a() {
        if (this.f1582d != null) {
            return this.f1582d.c();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public int a(View view, int i, int i2, int i3) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i, Integer.MIN_VALUE), i2);
        return Math.max(0, (i - view.getMeasuredWidth()) - i3);
    }

    protected static int a(int i, int i2, boolean z) {
        return z ? i - i2 : i + i2;
    }

    /* access modifiers changed from: protected */
    public int a(View view, int i, int i2, int i3, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = ((i3 - measuredHeight) / 2) + i2;
        if (z) {
            view.layout(i - measuredWidth, i4, i, measuredHeight + i4);
        } else {
            view.layout(i, i4, i + measuredWidth, measuredHeight + i4);
        }
        return z ? -measuredWidth : measuredWidth;
    }

    protected class VisibilityAnimListener implements bc {

        /* renamed from: a  reason: collision with root package name */
        int f1587a;

        /* renamed from: c  reason: collision with root package name */
        private boolean f1589c = false;

        protected VisibilityAnimListener() {
        }

        public VisibilityAnimListener a(ay ayVar, int i) {
            AbsActionBarView.this.f1584f = ayVar;
            this.f1587a = i;
            return this;
        }

        public void onAnimationStart(View view) {
            AbsActionBarView.super.setVisibility(0);
            this.f1589c = false;
        }

        public void onAnimationEnd(View view) {
            if (!this.f1589c) {
                AbsActionBarView.this.f1584f = null;
                AbsActionBarView.super.setVisibility(this.f1587a);
            }
        }

        public void onAnimationCancel(View view) {
            this.f1589c = true;
        }
    }
}
