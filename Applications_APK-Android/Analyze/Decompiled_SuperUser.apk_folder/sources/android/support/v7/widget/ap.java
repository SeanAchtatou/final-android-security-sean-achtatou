package android.support.v7.widget;

import android.view.View;

/* compiled from: ViewBoundsCheck */
class ap {

    /* renamed from: a  reason: collision with root package name */
    final b f2146a;

    /* renamed from: b  reason: collision with root package name */
    a f2147b = new a();

    /* compiled from: ViewBoundsCheck */
    interface b {
        int a();

        int a(View view);

        View a(int i);

        int b();

        int b(View view);
    }

    ap(b bVar) {
        this.f2146a = bVar;
    }

    /* compiled from: ViewBoundsCheck */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        int f2148a = 0;

        /* renamed from: b  reason: collision with root package name */
        int f2149b;

        /* renamed from: c  reason: collision with root package name */
        int f2150c;

        /* renamed from: d  reason: collision with root package name */
        int f2151d;

        /* renamed from: e  reason: collision with root package name */
        int f2152e;

        a() {
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2, int i3, int i4) {
            this.f2149b = i;
            this.f2150c = i2;
            this.f2151d = i3;
            this.f2152e = i4;
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            this.f2148a |= i;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f2148a = 0;
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2) {
            if (i > i2) {
                return 1;
            }
            if (i == i2) {
                return 2;
            }
            return 4;
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            if ((this.f2148a & 7) != 0 && (this.f2148a & (a(this.f2151d, this.f2149b) << 0)) == 0) {
                return false;
            }
            if ((this.f2148a & 112) != 0 && (this.f2148a & (a(this.f2151d, this.f2150c) << 4)) == 0) {
                return false;
            }
            if ((this.f2148a & 1792) != 0 && (this.f2148a & (a(this.f2152e, this.f2149b) << 8)) == 0) {
                return false;
            }
            if ((this.f2148a & 28672) == 0 || (this.f2148a & (a(this.f2152e, this.f2150c) << 12)) != 0) {
                return true;
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public View a(int i, int i2, int i3, int i4) {
        int a2 = this.f2146a.a();
        int b2 = this.f2146a.b();
        int i5 = i2 > i ? 1 : -1;
        View view = null;
        while (i != i2) {
            View a3 = this.f2146a.a(i);
            this.f2147b.a(a2, b2, this.f2146a.a(a3), this.f2146a.b(a3));
            if (i3 != 0) {
                this.f2147b.a();
                this.f2147b.a(i3);
                if (this.f2147b.b()) {
                    return a3;
                }
            }
            if (i4 != 0) {
                this.f2147b.a();
                this.f2147b.a(i4);
                if (this.f2147b.b()) {
                    i += i5;
                    view = a3;
                }
            }
            a3 = view;
            i += i5;
            view = a3;
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, int i) {
        this.f2147b.a(this.f2146a.a(), this.f2146a.b(), this.f2146a.a(view), this.f2146a.b(view));
        if (i == 0) {
            return false;
        }
        this.f2147b.a();
        this.f2147b.a(i);
        return this.f2147b.b();
    }
}
