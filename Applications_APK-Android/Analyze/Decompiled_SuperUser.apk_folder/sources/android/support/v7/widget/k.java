package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.a.a;
import android.support.v7.text.AllCapsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.widget.TextView;

@TargetApi(9)
/* compiled from: AppCompatTextHelper */
class k {

    /* renamed from: a  reason: collision with root package name */
    final TextView f2250a;

    /* renamed from: b  reason: collision with root package name */
    private al f2251b;

    /* renamed from: c  reason: collision with root package name */
    private al f2252c;

    /* renamed from: d  reason: collision with root package name */
    private al f2253d;

    /* renamed from: e  reason: collision with root package name */
    private al f2254e;

    static k a(TextView textView) {
        if (Build.VERSION.SDK_INT >= 17) {
            return new l(textView);
        }
        return new k(textView);
    }

    k(TextView textView) {
        this.f2250a = textView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.an.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.an.a(int, float):float
      android.support.v7.widget.an.a(int, int):int
      android.support.v7.widget.an.a(int, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        ColorStateList colorStateList;
        boolean z;
        boolean z2;
        ColorStateList colorStateList2 = null;
        Context context = this.f2250a.getContext();
        g a2 = g.a();
        an a3 = an.a(context, attributeSet, a.k.AppCompatTextHelper, i, 0);
        int g2 = a3.g(a.k.AppCompatTextHelper_android_textAppearance, -1);
        if (a3.g(a.k.AppCompatTextHelper_android_drawableLeft)) {
            this.f2251b = a(context, a2, a3.g(a.k.AppCompatTextHelper_android_drawableLeft, 0));
        }
        if (a3.g(a.k.AppCompatTextHelper_android_drawableTop)) {
            this.f2252c = a(context, a2, a3.g(a.k.AppCompatTextHelper_android_drawableTop, 0));
        }
        if (a3.g(a.k.AppCompatTextHelper_android_drawableRight)) {
            this.f2253d = a(context, a2, a3.g(a.k.AppCompatTextHelper_android_drawableRight, 0));
        }
        if (a3.g(a.k.AppCompatTextHelper_android_drawableBottom)) {
            this.f2254e = a(context, a2, a3.g(a.k.AppCompatTextHelper_android_drawableBottom, 0));
        }
        a3.a();
        boolean z3 = this.f2250a.getTransformationMethod() instanceof PasswordTransformationMethod;
        if (g2 != -1) {
            an a4 = an.a(context, g2, a.k.TextAppearance);
            if (z3 || !a4.g(a.k.TextAppearance_textAllCaps)) {
                z = false;
                z2 = false;
            } else {
                z2 = a4.a(a.k.TextAppearance_textAllCaps, false);
                z = true;
            }
            if (Build.VERSION.SDK_INT < 23) {
                if (a4.g(a.k.TextAppearance_android_textColor)) {
                    colorStateList = a4.e(a.k.TextAppearance_android_textColor);
                } else {
                    colorStateList = null;
                }
                if (a4.g(a.k.TextAppearance_android_textColorHint)) {
                    colorStateList2 = a4.e(a.k.TextAppearance_android_textColorHint);
                }
            } else {
                colorStateList = null;
            }
            a4.a();
        } else {
            colorStateList = null;
            z = false;
            z2 = false;
        }
        an a5 = an.a(context, attributeSet, a.k.TextAppearance, i, 0);
        if (!z3 && a5.g(a.k.TextAppearance_textAllCaps)) {
            z2 = a5.a(a.k.TextAppearance_textAllCaps, false);
            z = true;
        }
        if (Build.VERSION.SDK_INT < 23) {
            if (a5.g(a.k.TextAppearance_android_textColor)) {
                colorStateList = a5.e(a.k.TextAppearance_android_textColor);
            }
            if (a5.g(a.k.TextAppearance_android_textColorHint)) {
                colorStateList2 = a5.e(a.k.TextAppearance_android_textColorHint);
            }
        }
        a5.a();
        if (colorStateList != null) {
            this.f2250a.setTextColor(colorStateList);
        }
        if (colorStateList2 != null) {
            this.f2250a.setHintTextColor(colorStateList2);
        }
        if (!z3 && z) {
            a(z2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.an.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.an.a(int, float):float
      android.support.v7.widget.an.a(int, int):int
      android.support.v7.widget.an.a(int, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void a(Context context, int i) {
        ColorStateList e2;
        an a2 = an.a(context, i, a.k.TextAppearance);
        if (a2.g(a.k.TextAppearance_textAllCaps)) {
            a(a2.a(a.k.TextAppearance_textAllCaps, false));
        }
        if (Build.VERSION.SDK_INT < 23 && a2.g(a.k.TextAppearance_android_textColor) && (e2 = a2.e(a.k.TextAppearance_android_textColor)) != null) {
            this.f2250a.setTextColor(e2);
        }
        a2.a();
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f2250a.setTransformationMethod(z ? new AllCapsTransformationMethod(this.f2250a.getContext()) : null);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.f2251b != null || this.f2252c != null || this.f2253d != null || this.f2254e != null) {
            Drawable[] compoundDrawables = this.f2250a.getCompoundDrawables();
            a(compoundDrawables[0], this.f2251b);
            a(compoundDrawables[1], this.f2252c);
            a(compoundDrawables[2], this.f2253d);
            a(compoundDrawables[3], this.f2254e);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Drawable drawable, al alVar) {
        if (drawable != null && alVar != null) {
            g.a(drawable, alVar, this.f2250a.getDrawableState());
        }
    }

    protected static al a(Context context, g gVar, int i) {
        ColorStateList b2 = gVar.b(context, i);
        if (b2 == null) {
            return null;
        }
        al alVar = new al();
        alVar.f2141d = true;
        alVar.f2138a = b2;
        return alVar;
    }
}
