package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.b;
import java.lang.ref.WeakReference;

/* compiled from: VectorEnabledTintResources */
public class ao extends Resources {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<Context> f2145a;

    public static boolean a() {
        return b.k() && Build.VERSION.SDK_INT <= 20;
    }

    public ao(Context context, Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f2145a = new WeakReference<>(context);
    }

    public Drawable getDrawable(int i) {
        Context context = this.f2145a.get();
        if (context != null) {
            return g.a().a(context, this, i);
        }
        return super.getDrawable(i);
    }

    /* access modifiers changed from: package-private */
    public final Drawable a(int i) {
        return super.getDrawable(i);
    }
}
