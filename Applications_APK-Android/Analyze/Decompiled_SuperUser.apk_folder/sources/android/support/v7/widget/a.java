package android.support.v7.widget;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

@TargetApi(9)
/* compiled from: ActionBarBackgroundDrawable */
class a extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    final ActionBarContainer f2091a;

    public a(ActionBarContainer actionBarContainer) {
        this.f2091a = actionBarContainer;
    }

    public void draw(Canvas canvas) {
        if (!this.f2091a.f1593d) {
            if (this.f2091a.f1590a != null) {
                this.f2091a.f1590a.draw(canvas);
            }
            if (this.f2091a.f1591b != null && this.f2091a.f1594e) {
                this.f2091a.f1591b.draw(canvas);
            }
        } else if (this.f2091a.f1592c != null) {
            this.f2091a.f1592c.draw(canvas);
        }
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public int getOpacity() {
        return 0;
    }
}
