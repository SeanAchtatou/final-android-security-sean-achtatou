package android.support.v7.widget;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.app.SearchableInfo;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.v4.os.f;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.ag;
import android.support.v4.view.g;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.a.a;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.actions.SearchIntents;
import com.lody.virtual.helper.utils.FileUtils;
import java.lang.reflect.Method;
import java.util.WeakHashMap;

public class SearchView extends LinearLayoutCompat implements android.support.v7.view.c {
    static final a i = new a();
    private c A;
    private b B;
    private d C;
    private View.OnClickListener D;
    private boolean E;
    private boolean F;
    private boolean G;
    private CharSequence H;
    private boolean I;
    private boolean J;
    private int K;
    private boolean L;
    private CharSequence M;
    private CharSequence N;
    private boolean O;
    private int P;
    private Bundle Q;
    private Runnable R;
    private final Runnable S;
    private Runnable T;
    private final WeakHashMap<String, Drawable.ConstantState> U;
    private final View.OnClickListener V;
    private final TextView.OnEditorActionListener W;

    /* renamed from: a  reason: collision with root package name */
    final SearchAutoComplete f1957a;
    private final AdapterView.OnItemClickListener aa;
    private final AdapterView.OnItemSelectedListener ab;
    private TextWatcher ac;

    /* renamed from: b  reason: collision with root package name */
    final ImageView f1958b;

    /* renamed from: c  reason: collision with root package name */
    final ImageView f1959c;

    /* renamed from: d  reason: collision with root package name */
    final ImageView f1960d;

    /* renamed from: e  reason: collision with root package name */
    final ImageView f1961e;

    /* renamed from: f  reason: collision with root package name */
    View.OnFocusChangeListener f1962f;

    /* renamed from: g  reason: collision with root package name */
    CursorAdapter f1963g;

    /* renamed from: h  reason: collision with root package name */
    SearchableInfo f1964h;
    View.OnKeyListener j;
    private final View k;
    private final View l;
    private final View m;
    private final View n;
    private e o;
    private Rect p;
    private Rect q;
    private int[] r;
    private int[] s;
    private final ImageView t;
    private final Drawable u;
    private final int v;
    private final int w;
    private final Intent x;
    private final Intent y;
    private final CharSequence z;

    public interface b {
        boolean a();
    }

    public interface c {
        boolean a(String str);

        boolean b(String str);
    }

    public interface d {
        boolean a(int i);

        boolean b(int i);
    }

    public SearchView(Context context) {
        this(context, null);
    }

    public SearchView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0027a.searchViewStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.SearchView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.an.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.an.a(int, float):float
      android.support.v7.widget.an.a(int, int):int
      android.support.v7.widget.an.a(int, boolean):boolean */
    public SearchView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.p = new Rect();
        this.q = new Rect();
        this.r = new int[2];
        this.s = new int[2];
        this.R = new Runnable() {
            public void run() {
                InputMethodManager inputMethodManager = (InputMethodManager) SearchView.this.getContext().getSystemService("input_method");
                if (inputMethodManager != null) {
                    SearchView.i.a(inputMethodManager, SearchView.this, 0);
                }
            }
        };
        this.S = new Runnable() {
            public void run() {
                SearchView.this.d();
            }
        };
        this.T = new Runnable() {
            public void run() {
                if (SearchView.this.f1963g != null && (SearchView.this.f1963g instanceof ai)) {
                    SearchView.this.f1963g.a((Cursor) null);
                }
            }
        };
        this.U = new WeakHashMap<>();
        this.V = new View.OnClickListener() {
            public void onClick(View view) {
                if (view == SearchView.this.f1958b) {
                    SearchView.this.g();
                } else if (view == SearchView.this.f1960d) {
                    SearchView.this.f();
                } else if (view == SearchView.this.f1959c) {
                    SearchView.this.e();
                } else if (view == SearchView.this.f1961e) {
                    SearchView.this.h();
                } else if (view == SearchView.this.f1957a) {
                    SearchView.this.k();
                }
            }
        };
        this.j = new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (SearchView.this.f1964h == null) {
                    return false;
                }
                if (SearchView.this.f1957a.isPopupShowing() && SearchView.this.f1957a.getListSelection() != -1) {
                    return SearchView.this.a(view, i, keyEvent);
                }
                if (SearchView.this.f1957a.a() || !g.a(keyEvent) || keyEvent.getAction() != 1 || i != 66) {
                    return false;
                }
                view.cancelLongPress();
                SearchView.this.a(0, (String) null, SearchView.this.f1957a.getText().toString());
                return true;
            }
        };
        this.W = new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                SearchView.this.e();
                return true;
            }
        };
        this.aa = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                SearchView.this.a(i, 0, (String) null);
            }
        };
        this.ab = new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                SearchView.this.a(i);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        };
        this.ac = new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                SearchView.this.b(charSequence);
            }

            public void afterTextChanged(Editable editable) {
            }
        };
        an a2 = an.a(context, attributeSet, a.k.SearchView, i2, 0);
        LayoutInflater.from(context).inflate(a2.g(a.k.SearchView_layout, a.h.abc_search_view), (ViewGroup) this, true);
        this.f1957a = (SearchAutoComplete) findViewById(a.f.search_src_text);
        this.f1957a.setSearchView(this);
        this.k = findViewById(a.f.search_edit_frame);
        this.l = findViewById(a.f.search_plate);
        this.m = findViewById(a.f.submit_area);
        this.f1958b = (ImageView) findViewById(a.f.search_button);
        this.f1959c = (ImageView) findViewById(a.f.search_go_btn);
        this.f1960d = (ImageView) findViewById(a.f.search_close_btn);
        this.f1961e = (ImageView) findViewById(a.f.search_voice_btn);
        this.t = (ImageView) findViewById(a.f.search_mag_icon);
        ag.a(this.l, a2.a(a.k.SearchView_queryBackground));
        ag.a(this.m, a2.a(a.k.SearchView_submitBackground));
        this.f1958b.setImageDrawable(a2.a(a.k.SearchView_searchIcon));
        this.f1959c.setImageDrawable(a2.a(a.k.SearchView_goIcon));
        this.f1960d.setImageDrawable(a2.a(a.k.SearchView_closeIcon));
        this.f1961e.setImageDrawable(a2.a(a.k.SearchView_voiceIcon));
        this.t.setImageDrawable(a2.a(a.k.SearchView_searchIcon));
        this.u = a2.a(a.k.SearchView_searchHintIcon);
        this.v = a2.g(a.k.SearchView_suggestionRowLayout, a.h.abc_search_dropdown_item_icons_2line);
        this.w = a2.g(a.k.SearchView_commitIcon, 0);
        this.f1958b.setOnClickListener(this.V);
        this.f1960d.setOnClickListener(this.V);
        this.f1959c.setOnClickListener(this.V);
        this.f1961e.setOnClickListener(this.V);
        this.f1957a.setOnClickListener(this.V);
        this.f1957a.addTextChangedListener(this.ac);
        this.f1957a.setOnEditorActionListener(this.W);
        this.f1957a.setOnItemClickListener(this.aa);
        this.f1957a.setOnItemSelectedListener(this.ab);
        this.f1957a.setOnKeyListener(this.j);
        this.f1957a.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (SearchView.this.f1962f != null) {
                    SearchView.this.f1962f.onFocusChange(SearchView.this, z);
                }
            }
        });
        setIconifiedByDefault(a2.a(a.k.SearchView_iconifiedByDefault, true));
        int e2 = a2.e(a.k.SearchView_android_maxWidth, -1);
        if (e2 != -1) {
            setMaxWidth(e2);
        }
        this.z = a2.c(a.k.SearchView_defaultQueryHint);
        this.H = a2.c(a.k.SearchView_queryHint);
        int a3 = a2.a(a.k.SearchView_android_imeOptions, -1);
        if (a3 != -1) {
            setImeOptions(a3);
        }
        int a4 = a2.a(a.k.SearchView_android_inputType, -1);
        if (a4 != -1) {
            setInputType(a4);
        }
        setFocusable(a2.a(a.k.SearchView_android_focusable, true));
        a2.a();
        this.x = new Intent("android.speech.action.WEB_SEARCH");
        this.x.addFlags(268435456);
        this.x.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
        this.y = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        this.y.addFlags(268435456);
        this.n = findViewById(this.f1957a.getDropDownAnchor());
        if (this.n != null) {
            if (Build.VERSION.SDK_INT >= 11) {
                l();
            } else {
                m();
            }
        }
        a(this.E);
        s();
    }

    @TargetApi(11)
    private void l() {
        this.n.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                SearchView.this.j();
            }
        });
    }

    private void m() {
        this.n.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                SearchView.this.j();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionRowLayout() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionCommitIconResId() {
        return this.w;
    }

    public void setSearchableInfo(SearchableInfo searchableInfo) {
        this.f1964h = searchableInfo;
        if (this.f1964h != null) {
            t();
            s();
        }
        this.L = n();
        if (this.L) {
            this.f1957a.setPrivateImeOptions("nm");
        }
        a(c());
    }

    public void setAppSearchData(Bundle bundle) {
        this.Q = bundle;
    }

    public void setImeOptions(int i2) {
        this.f1957a.setImeOptions(i2);
    }

    public int getImeOptions() {
        return this.f1957a.getImeOptions();
    }

    public void setInputType(int i2) {
        this.f1957a.setInputType(i2);
    }

    public int getInputType() {
        return this.f1957a.getInputType();
    }

    public boolean requestFocus(int i2, Rect rect) {
        if (this.J || !isFocusable()) {
            return false;
        }
        if (c()) {
            return super.requestFocus(i2, rect);
        }
        boolean requestFocus = this.f1957a.requestFocus(i2, rect);
        if (requestFocus) {
            a(false);
        }
        return requestFocus;
    }

    public void clearFocus() {
        this.J = true;
        setImeVisibility(false);
        super.clearFocus();
        this.f1957a.clearFocus();
        this.J = false;
    }

    public void setOnQueryTextListener(c cVar) {
        this.A = cVar;
    }

    public void setOnCloseListener(b bVar) {
        this.B = bVar;
    }

    public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.f1962f = onFocusChangeListener;
    }

    public void setOnSuggestionListener(d dVar) {
        this.C = dVar;
    }

    public void setOnSearchClickListener(View.OnClickListener onClickListener) {
        this.D = onClickListener;
    }

    public CharSequence getQuery() {
        return this.f1957a.getText();
    }

    public void a(CharSequence charSequence, boolean z2) {
        this.f1957a.setText(charSequence);
        if (charSequence != null) {
            this.f1957a.setSelection(this.f1957a.length());
            this.N = charSequence;
        }
        if (z2 && !TextUtils.isEmpty(charSequence)) {
            e();
        }
    }

    public void setQueryHint(CharSequence charSequence) {
        this.H = charSequence;
        s();
    }

    public CharSequence getQueryHint() {
        if (this.H != null) {
            return this.H;
        }
        if (this.f1964h == null || this.f1964h.getHintId() == 0) {
            return this.z;
        }
        return getContext().getText(this.f1964h.getHintId());
    }

    public void setIconifiedByDefault(boolean z2) {
        if (this.E != z2) {
            this.E = z2;
            a(z2);
            s();
        }
    }

    public void setIconified(boolean z2) {
        if (z2) {
            f();
        } else {
            g();
        }
    }

    public boolean c() {
        return this.F;
    }

    public void setSubmitButtonEnabled(boolean z2) {
        this.G = z2;
        a(c());
    }

    public void setQueryRefinementEnabled(boolean z2) {
        this.I = z2;
        if (this.f1963g instanceof ai) {
            ((ai) this.f1963g).a(z2 ? 2 : 1);
        }
    }

    public void setSuggestionsAdapter(CursorAdapter cursorAdapter) {
        this.f1963g = cursorAdapter;
        this.f1957a.setAdapter(this.f1963g);
    }

    public CursorAdapter getSuggestionsAdapter() {
        return this.f1963g;
    }

    public void setMaxWidth(int i2) {
        this.K = i2;
        requestLayout();
    }

    public int getMaxWidth() {
        return this.K;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (c()) {
            super.onMeasure(i2, i3);
            return;
        }
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (this.K <= 0) {
                    size = Math.min(getPreferredWidth(), size);
                    break;
                } else {
                    size = Math.min(this.K, size);
                    break;
                }
            case 0:
                if (this.K <= 0) {
                    size = getPreferredWidth();
                    break;
                } else {
                    size = this.K;
                    break;
                }
            case 1073741824:
                if (this.K > 0) {
                    size = Math.min(this.K, size);
                    break;
                }
                break;
        }
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        switch (mode2) {
            case Integer.MIN_VALUE:
                size2 = Math.min(getPreferredHeight(), size2);
                break;
            case 0:
                size2 = getPreferredHeight();
                break;
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), View.MeasureSpec.makeMeasureSpec(size2, 1073741824));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        if (z2) {
            a(this.f1957a, this.p);
            this.q.set(this.p.left, 0, this.p.right, i5 - i3);
            if (this.o == null) {
                this.o = new e(this.q, this.p, this.f1957a);
                setTouchDelegate(this.o);
                return;
            }
            this.o.a(this.q, this.p);
        }
    }

    private void a(View view, Rect rect) {
        view.getLocationInWindow(this.r);
        getLocationInWindow(this.s);
        int i2 = this.r[1] - this.s[1];
        int i3 = this.r[0] - this.s[0];
        rect.set(i3, i2, view.getWidth() + i3, view.getHeight() + i2);
    }

    private int getPreferredWidth() {
        return getContext().getResources().getDimensionPixelSize(a.d.abc_search_view_preferred_width);
    }

    private int getPreferredHeight() {
        return getContext().getResources().getDimensionPixelSize(a.d.abc_search_view_preferred_height);
    }

    private void a(boolean z2) {
        boolean z3;
        int i2;
        boolean z4 = true;
        int i3 = 8;
        this.F = z2;
        int i4 = z2 ? 0 : 8;
        if (!TextUtils.isEmpty(this.f1957a.getText())) {
            z3 = true;
        } else {
            z3 = false;
        }
        this.f1958b.setVisibility(i4);
        b(z3);
        View view = this.k;
        if (z2) {
            i2 = 8;
        } else {
            i2 = 0;
        }
        view.setVisibility(i2);
        if (this.t.getDrawable() != null && !this.E) {
            i3 = 0;
        }
        this.t.setVisibility(i3);
        q();
        if (z3) {
            z4 = false;
        }
        c(z4);
        p();
    }

    private boolean n() {
        if (this.f1964h == null || !this.f1964h.getVoiceSearchEnabled()) {
            return false;
        }
        Intent intent = null;
        if (this.f1964h.getVoiceSearchLaunchWebSearch()) {
            intent = this.x;
        } else if (this.f1964h.getVoiceSearchLaunchRecognizer()) {
            intent = this.y;
        }
        if (intent == null || getContext().getPackageManager().resolveActivity(intent, 65536) == null) {
            return false;
        }
        return true;
    }

    private boolean o() {
        return (this.G || this.L) && !c();
    }

    private void b(boolean z2) {
        int i2 = 8;
        if (this.G && o() && hasFocus() && (z2 || !this.L)) {
            i2 = 0;
        }
        this.f1959c.setVisibility(i2);
    }

    private void p() {
        int i2 = 8;
        if (o() && (this.f1959c.getVisibility() == 0 || this.f1961e.getVisibility() == 0)) {
            i2 = 0;
        }
        this.m.setVisibility(i2);
    }

    private void q() {
        boolean z2 = true;
        int i2 = 0;
        boolean z3 = !TextUtils.isEmpty(this.f1957a.getText());
        if (!z3 && (!this.E || this.O)) {
            z2 = false;
        }
        ImageView imageView = this.f1960d;
        if (!z2) {
            i2 = 8;
        }
        imageView.setVisibility(i2);
        Drawable drawable = this.f1960d.getDrawable();
        if (drawable != null) {
            drawable.setState(z3 ? ENABLED_STATE_SET : EMPTY_STATE_SET);
        }
    }

    private void r() {
        post(this.S);
    }

    /* access modifiers changed from: package-private */
    public void d() {
        int[] iArr = this.f1957a.hasFocus() ? FOCUSED_STATE_SET : EMPTY_STATE_SET;
        Drawable background = this.l.getBackground();
        if (background != null) {
            background.setState(iArr);
        }
        Drawable background2 = this.m.getBackground();
        if (background2 != null) {
            background2.setState(iArr);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.S);
        post(this.T);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: package-private */
    public void setImeVisibility(boolean z2) {
        if (z2) {
            post(this.R);
            return;
        }
        removeCallbacks(this.R);
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(CharSequence charSequence) {
        setQuery(charSequence);
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, int i2, KeyEvent keyEvent) {
        int length;
        if (this.f1964h == null || this.f1963g == null || keyEvent.getAction() != 0 || !g.a(keyEvent)) {
            return false;
        }
        if (i2 == 66 || i2 == 84 || i2 == 61) {
            return a(this.f1957a.getListSelection(), 0, (String) null);
        }
        if (i2 == 21 || i2 == 22) {
            if (i2 == 21) {
                length = 0;
            } else {
                length = this.f1957a.length();
            }
            this.f1957a.setSelection(length);
            this.f1957a.setListSelection(0);
            this.f1957a.clearListSelection();
            i.a(this.f1957a, true);
            return true;
        }
        if (!(i2 == 19 && this.f1957a.getListSelection() == 0)) {
        }
        return false;
    }

    private CharSequence c(CharSequence charSequence) {
        if (!this.E || this.u == null) {
            return charSequence;
        }
        int textSize = (int) (((double) this.f1957a.getTextSize()) * 1.25d);
        this.u.setBounds(0, 0, textSize, textSize);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
        spannableStringBuilder.setSpan(new ImageSpan(this.u), 1, 2, 33);
        spannableStringBuilder.append(charSequence);
        return spannableStringBuilder;
    }

    private void s() {
        CharSequence queryHint = getQueryHint();
        SearchAutoComplete searchAutoComplete = this.f1957a;
        if (queryHint == null) {
            queryHint = "";
        }
        searchAutoComplete.setHint(c(queryHint));
    }

    private void t() {
        int i2 = 1;
        this.f1957a.setThreshold(this.f1964h.getSuggestThreshold());
        this.f1957a.setImeOptions(this.f1964h.getImeOptions());
        int inputType = this.f1964h.getInputType();
        if ((inputType & 15) == 1) {
            inputType &= -65537;
            if (this.f1964h.getSuggestAuthority() != null) {
                inputType = inputType | 65536 | 524288;
            }
        }
        this.f1957a.setInputType(inputType);
        if (this.f1963g != null) {
            this.f1963g.a((Cursor) null);
        }
        if (this.f1964h.getSuggestAuthority() != null) {
            this.f1963g = new ai(getContext(), this, this.f1964h, this.U);
            this.f1957a.setAdapter(this.f1963g);
            ai aiVar = (ai) this.f1963g;
            if (this.I) {
                i2 = 2;
            }
            aiVar.a(i2);
        }
    }

    private void c(boolean z2) {
        int i2;
        if (!this.L || c() || !z2) {
            i2 = 8;
        } else {
            i2 = 0;
            this.f1959c.setVisibility(8);
        }
        this.f1961e.setVisibility(i2);
    }

    /* access modifiers changed from: package-private */
    public void b(CharSequence charSequence) {
        boolean z2 = true;
        Editable text = this.f1957a.getText();
        this.N = text;
        boolean z3 = !TextUtils.isEmpty(text);
        b(z3);
        if (z3) {
            z2 = false;
        }
        c(z2);
        q();
        p();
        if (this.A != null && !TextUtils.equals(charSequence, this.M)) {
            this.A.b(charSequence.toString());
        }
        this.M = charSequence.toString();
    }

    /* access modifiers changed from: package-private */
    public void e() {
        Editable text = this.f1957a.getText();
        if (text != null && TextUtils.getTrimmedLength(text) > 0) {
            if (this.A == null || !this.A.a(text.toString())) {
                if (this.f1964h != null) {
                    a(0, (String) null, text.toString());
                }
                setImeVisibility(false);
                u();
            }
        }
    }

    private void u() {
        this.f1957a.dismissDropDown();
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (!TextUtils.isEmpty(this.f1957a.getText())) {
            this.f1957a.setText("");
            this.f1957a.requestFocus();
            setImeVisibility(true);
        } else if (!this.E) {
        } else {
            if (this.B == null || !this.B.a()) {
                clearFocus();
                a(true);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        a(false);
        this.f1957a.requestFocus();
        setImeVisibility(true);
        if (this.D != null) {
            this.D.onClick(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (this.f1964h != null) {
            SearchableInfo searchableInfo = this.f1964h;
            try {
                if (searchableInfo.getVoiceSearchLaunchWebSearch()) {
                    getContext().startActivity(a(this.x, searchableInfo));
                } else if (searchableInfo.getVoiceSearchLaunchRecognizer()) {
                    getContext().startActivity(b(this.y, searchableInfo));
                }
            } catch (ActivityNotFoundException e2) {
                Log.w("SearchView", "Could not find voice search activity");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        a(c());
        r();
        if (this.f1957a.hasFocus()) {
            k();
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        r();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.SearchView.a(java.lang.CharSequence, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      android.support.v7.widget.SearchView.a(android.content.Intent, android.app.SearchableInfo):android.content.Intent
      android.support.v7.widget.SearchView.a(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.SearchView.a(java.lang.CharSequence, boolean):void */
    public void b() {
        a((CharSequence) "", false);
        clearFocus();
        a(true);
        this.f1957a.setImeOptions(this.P);
        this.O = false;
    }

    public void a() {
        if (!this.O) {
            this.O = true;
            this.P = this.f1957a.getImeOptions();
            this.f1957a.setImeOptions(this.P | 33554432);
            this.f1957a.setText("");
            setIconified(false);
        }
    }

    static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = f.a(new android.support.v4.os.g<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a  reason: collision with root package name */
        boolean f1977a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f1977a = ((Boolean) parcel.readValue(null)).booleanValue();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeValue(Boolean.valueOf(this.f1977a));
        }

        public String toString() {
            return "SearchView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " isIconified=" + this.f1977a + "}";
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1977a = c();
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        a(savedState.f1977a);
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public void j() {
        int i2;
        int i3;
        if (this.n.getWidth() > 1) {
            Resources resources = getContext().getResources();
            int paddingLeft = this.l.getPaddingLeft();
            Rect rect = new Rect();
            boolean a2 = ar.a(this);
            if (this.E) {
                i2 = resources.getDimensionPixelSize(a.d.abc_dropdownitem_text_padding_left) + resources.getDimensionPixelSize(a.d.abc_dropdownitem_icon_width);
            } else {
                i2 = 0;
            }
            this.f1957a.getDropDownBackground().getPadding(rect);
            if (a2) {
                i3 = -rect.left;
            } else {
                i3 = paddingLeft - (rect.left + i2);
            }
            this.f1957a.setDropDownHorizontalOffset(i3);
            this.f1957a.setDropDownWidth((i2 + ((this.n.getWidth() + rect.left) + rect.right)) - paddingLeft);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, String str) {
        if (this.C != null && this.C.b(i2)) {
            return false;
        }
        b(i2, 0, null);
        setImeVisibility(false);
        u();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2) {
        if (this.C != null && this.C.a(i2)) {
            return false;
        }
        b(i2);
        return true;
    }

    private void b(int i2) {
        Editable text = this.f1957a.getText();
        Cursor a2 = this.f1963g.a();
        if (a2 != null) {
            if (a2.moveToPosition(i2)) {
                CharSequence c2 = this.f1963g.c(a2);
                if (c2 != null) {
                    setQuery(c2);
                } else {
                    setQuery(text);
                }
            } else {
                setQuery(text);
            }
        }
    }

    private boolean b(int i2, int i3, String str) {
        Cursor a2 = this.f1963g.a();
        if (a2 == null || !a2.moveToPosition(i2)) {
            return false;
        }
        a(a(a2, i3, str));
        return true;
    }

    private void a(Intent intent) {
        if (intent != null) {
            try {
                getContext().startActivity(intent);
            } catch (RuntimeException e2) {
                Log.e("SearchView", "Failed launch activity: " + intent, e2);
            }
        }
    }

    private void setQuery(CharSequence charSequence) {
        this.f1957a.setText(charSequence);
        this.f1957a.setSelection(TextUtils.isEmpty(charSequence) ? 0 : charSequence.length());
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, String str, String str2) {
        getContext().startActivity(a("android.intent.action.SEARCH", null, null, str2, i2, str));
    }

    private Intent a(String str, Uri uri, String str2, String str3, int i2, String str4) {
        Intent intent = new Intent(str);
        intent.addFlags(268435456);
        if (uri != null) {
            intent.setData(uri);
        }
        intent.putExtra("user_query", this.N);
        if (str3 != null) {
            intent.putExtra(SearchIntents.EXTRA_QUERY, str3);
        }
        if (str2 != null) {
            intent.putExtra("intent_extra_data_key", str2);
        }
        if (this.Q != null) {
            intent.putExtra("app_data", this.Q);
        }
        if (i2 != 0) {
            intent.putExtra("action_key", i2);
            intent.putExtra("action_msg", str4);
        }
        intent.setComponent(this.f1964h.getSearchActivity());
        return intent;
    }

    private Intent a(Intent intent, SearchableInfo searchableInfo) {
        String flattenToShortString;
        Intent intent2 = new Intent(intent);
        ComponentName searchActivity = searchableInfo.getSearchActivity();
        if (searchActivity == null) {
            flattenToShortString = null;
        } else {
            flattenToShortString = searchActivity.flattenToShortString();
        }
        intent2.putExtra("calling_package", flattenToShortString);
        return intent2;
    }

    private Intent b(Intent intent, SearchableInfo searchableInfo) {
        String str;
        String str2;
        String str3 = null;
        ComponentName searchActivity = searchableInfo.getSearchActivity();
        Intent intent2 = new Intent("android.intent.action.SEARCH");
        intent2.setComponent(searchActivity);
        PendingIntent activity = PendingIntent.getActivity(getContext(), 0, intent2, 1073741824);
        Bundle bundle = new Bundle();
        if (this.Q != null) {
            bundle.putParcelable("app_data", this.Q);
        }
        Intent intent3 = new Intent(intent);
        String str4 = "free_form";
        int i2 = 1;
        Resources resources = getResources();
        if (searchableInfo.getVoiceLanguageModeId() != 0) {
            str4 = resources.getString(searchableInfo.getVoiceLanguageModeId());
        }
        if (searchableInfo.getVoicePromptTextId() != 0) {
            str = resources.getString(searchableInfo.getVoicePromptTextId());
        } else {
            str = null;
        }
        if (searchableInfo.getVoiceLanguageId() != 0) {
            str2 = resources.getString(searchableInfo.getVoiceLanguageId());
        } else {
            str2 = null;
        }
        if (searchableInfo.getVoiceMaxResults() != 0) {
            i2 = searchableInfo.getVoiceMaxResults();
        }
        intent3.putExtra("android.speech.extra.LANGUAGE_MODEL", str4);
        intent3.putExtra("android.speech.extra.PROMPT", str);
        intent3.putExtra("android.speech.extra.LANGUAGE", str2);
        intent3.putExtra("android.speech.extra.MAX_RESULTS", i2);
        if (searchActivity != null) {
            str3 = searchActivity.flattenToShortString();
        }
        intent3.putExtra("calling_package", str3);
        intent3.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", activity);
        intent3.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", bundle);
        return intent3;
    }

    private Intent a(Cursor cursor, int i2, String str) {
        int i3;
        String a2;
        try {
            String a3 = ai.a(cursor, "suggest_intent_action");
            if (a3 == null) {
                a3 = this.f1964h.getSuggestIntentAction();
            }
            if (a3 == null) {
                a3 = "android.intent.action.SEARCH";
            }
            String a4 = ai.a(cursor, "suggest_intent_data");
            if (a4 == null) {
                a4 = this.f1964h.getSuggestIntentData();
            }
            if (!(a4 == null || (a2 = ai.a(cursor, "suggest_intent_data_id")) == null)) {
                a4 = a4 + "/" + Uri.encode(a2);
            }
            return a(a3, a4 == null ? null : Uri.parse(a4), ai.a(cursor, "suggest_intent_extra_data"), ai.a(cursor, "suggest_intent_query"), i2, str);
        } catch (RuntimeException e2) {
            RuntimeException runtimeException = e2;
            try {
                i3 = cursor.getPosition();
            } catch (RuntimeException e3) {
                i3 = -1;
            }
            Log.w("SearchView", "Search suggestions cursor at row " + i3 + " returned exception.", runtimeException);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        i.a(this.f1957a);
        i.b(this.f1957a);
    }

    static boolean a(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }

    private static class e extends TouchDelegate {

        /* renamed from: a  reason: collision with root package name */
        private final View f1984a;

        /* renamed from: b  reason: collision with root package name */
        private final Rect f1985b = new Rect();

        /* renamed from: c  reason: collision with root package name */
        private final Rect f1986c = new Rect();

        /* renamed from: d  reason: collision with root package name */
        private final Rect f1987d = new Rect();

        /* renamed from: e  reason: collision with root package name */
        private final int f1988e;

        /* renamed from: f  reason: collision with root package name */
        private boolean f1989f;

        public e(Rect rect, Rect rect2, View view) {
            super(rect, view);
            this.f1988e = ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
            a(rect, rect2);
            this.f1984a = view;
        }

        public void a(Rect rect, Rect rect2) {
            this.f1985b.set(rect);
            this.f1987d.set(rect);
            this.f1987d.inset(-this.f1988e, -this.f1988e);
            this.f1986c.set(rect2);
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public boolean onTouchEvent(MotionEvent motionEvent) {
            boolean z;
            boolean z2 = true;
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            switch (motionEvent.getAction()) {
                case 0:
                    if (this.f1985b.contains(x, y)) {
                        this.f1989f = true;
                        z = true;
                        break;
                    }
                    z = false;
                    break;
                case 1:
                case 2:
                    z = this.f1989f;
                    if (z && !this.f1987d.contains(x, y)) {
                        z2 = false;
                        break;
                    }
                case 3:
                    z = this.f1989f;
                    this.f1989f = false;
                    break;
                default:
                    z = false;
                    break;
            }
            if (!z) {
                return false;
            }
            if (!z2 || this.f1986c.contains(x, y)) {
                motionEvent.setLocation((float) (x - this.f1986c.left), (float) (y - this.f1986c.top));
            } else {
                motionEvent.setLocation((float) (this.f1984a.getWidth() / 2), (float) (this.f1984a.getHeight() / 2));
            }
            return this.f1984a.dispatchTouchEvent(motionEvent);
        }
    }

    public static class SearchAutoComplete extends AppCompatAutoCompleteTextView {

        /* renamed from: a  reason: collision with root package name */
        private int f1978a;

        /* renamed from: b  reason: collision with root package name */
        private SearchView f1979b;

        public SearchAutoComplete(Context context) {
            this(context, null);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            this(context, attributeSet, a.C0027a.autoCompleteTextViewStyle);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            this.f1978a = getThreshold();
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            super.onFinishInflate();
            setMinWidth((int) TypedValue.applyDimension(1, (float) getSearchViewTextMinWidthDp(), getResources().getDisplayMetrics()));
        }

        /* access modifiers changed from: package-private */
        public void setSearchView(SearchView searchView) {
            this.f1979b = searchView;
        }

        public void setThreshold(int i) {
            super.setThreshold(i);
            this.f1978a = i;
        }

        /* access modifiers changed from: private */
        public boolean a() {
            return TextUtils.getTrimmedLength(getText()) == 0;
        }

        /* access modifiers changed from: protected */
        public void replaceText(CharSequence charSequence) {
        }

        public void performCompletion() {
        }

        public void onWindowFocusChanged(boolean z) {
            super.onWindowFocusChanged(z);
            if (z && this.f1979b.hasFocus() && getVisibility() == 0) {
                ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0);
                if (SearchView.a(getContext())) {
                    SearchView.i.a(this, true);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            this.f1979b.i();
        }

        public boolean enoughToFilter() {
            return this.f1978a <= 0 || super.enoughToFilter();
        }

        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            if (i == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    KeyEvent.DispatcherState keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState == null) {
                        return true;
                    }
                    keyDispatcherState.startTracking(keyEvent, this);
                    return true;
                } else if (keyEvent.getAction() == 1) {
                    KeyEvent.DispatcherState keyDispatcherState2 = getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.f1979b.clearFocus();
                        this.f1979b.setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i, keyEvent);
        }

        private int getSearchViewTextMinWidthDp() {
            Configuration configuration = getResources().getConfiguration();
            int b2 = android.support.v4.content.a.a.b(getResources());
            int a2 = android.support.v4.content.a.a.a(getResources());
            if (b2 >= 960 && a2 >= 720 && configuration.orientation == 2) {
                return FileUtils.FileMode.MODE_IRUSR;
            }
            if (b2 >= 600 || (b2 >= 640 && a2 >= 480)) {
                return 192;
            }
            return 160;
        }
    }

    private static class a {

        /* renamed from: a  reason: collision with root package name */
        private Method f1980a;

        /* renamed from: b  reason: collision with root package name */
        private Method f1981b;

        /* renamed from: c  reason: collision with root package name */
        private Method f1982c;

        /* renamed from: d  reason: collision with root package name */
        private Method f1983d;

        a() {
            try {
                this.f1980a = AutoCompleteTextView.class.getDeclaredMethod("doBeforeTextChanged", new Class[0]);
                this.f1980a.setAccessible(true);
            } catch (NoSuchMethodException e2) {
            }
            try {
                this.f1981b = AutoCompleteTextView.class.getDeclaredMethod("doAfterTextChanged", new Class[0]);
                this.f1981b.setAccessible(true);
            } catch (NoSuchMethodException e3) {
            }
            Class<AutoCompleteTextView> cls = AutoCompleteTextView.class;
            try {
                this.f1982c = cls.getMethod("ensureImeVisible", Boolean.TYPE);
                this.f1982c.setAccessible(true);
            } catch (NoSuchMethodException e4) {
            }
            Class<InputMethodManager> cls2 = InputMethodManager.class;
            try {
                this.f1983d = cls2.getMethod("showSoftInputUnchecked", Integer.TYPE, ResultReceiver.class);
                this.f1983d.setAccessible(true);
            } catch (NoSuchMethodException e5) {
            }
        }

        /* access modifiers changed from: package-private */
        public void a(AutoCompleteTextView autoCompleteTextView) {
            if (this.f1980a != null) {
                try {
                    this.f1980a.invoke(autoCompleteTextView, new Object[0]);
                } catch (Exception e2) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b(AutoCompleteTextView autoCompleteTextView) {
            if (this.f1981b != null) {
                try {
                    this.f1981b.invoke(autoCompleteTextView, new Object[0]);
                } catch (Exception e2) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(AutoCompleteTextView autoCompleteTextView, boolean z) {
            if (this.f1982c != null) {
                try {
                    this.f1982c.invoke(autoCompleteTextView, Boolean.valueOf(z));
                } catch (Exception e2) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(InputMethodManager inputMethodManager, View view, int i) {
            if (this.f1983d != null) {
                try {
                    this.f1983d.invoke(inputMethodManager, Integer.valueOf(i), null);
                    return;
                } catch (Exception e2) {
                }
            }
            inputMethodManager.showSoftInput(view, i);
        }
    }
}
