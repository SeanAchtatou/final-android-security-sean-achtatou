package android.support.v7.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.ag;
import android.support.v7.a.a;
import android.support.v7.view.menu.m;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

public class ActivityChooserView extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    final a f1642a;

    /* renamed from: b  reason: collision with root package name */
    final FrameLayout f1643b;

    /* renamed from: c  reason: collision with root package name */
    final FrameLayout f1644c;

    /* renamed from: d  reason: collision with root package name */
    ActionProvider f1645d;

    /* renamed from: e  reason: collision with root package name */
    final DataSetObserver f1646e;

    /* renamed from: f  reason: collision with root package name */
    PopupWindow.OnDismissListener f1647f;

    /* renamed from: g  reason: collision with root package name */
    boolean f1648g;

    /* renamed from: h  reason: collision with root package name */
    int f1649h;
    private final b i;
    private final LinearLayoutCompat j;
    private final Drawable k;
    private final ImageView l;
    private final ImageView m;
    private final int n;
    private final ViewTreeObserver.OnGlobalLayoutListener o;
    private ListPopupWindow p;
    private boolean q;
    private int r;

    public ActivityChooserView(Context context) {
        this(context, null);
    }

    public ActivityChooserView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.ActivityChooserView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ActivityChooserView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1646e = new DataSetObserver() {
            public void onChanged() {
                super.onChanged();
                ActivityChooserView.this.f1642a.notifyDataSetChanged();
            }

            public void onInvalidated() {
                super.onInvalidated();
                ActivityChooserView.this.f1642a.notifyDataSetInvalidated();
            }
        };
        this.o = new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (!ActivityChooserView.this.c()) {
                    return;
                }
                if (!ActivityChooserView.this.isShown()) {
                    ActivityChooserView.this.getListPopupWindow().b();
                    return;
                }
                ActivityChooserView.this.getListPopupWindow().a();
                if (ActivityChooserView.this.f1645d != null) {
                    ActivityChooserView.this.f1645d.a(true);
                }
            }
        };
        this.f1649h = 4;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.k.ActivityChooserView, i2, 0);
        this.f1649h = obtainStyledAttributes.getInt(a.k.ActivityChooserView_initialActivityCount, 4);
        Drawable drawable = obtainStyledAttributes.getDrawable(a.k.ActivityChooserView_expandActivityOverflowButtonDrawable);
        obtainStyledAttributes.recycle();
        LayoutInflater.from(getContext()).inflate(a.h.abc_activity_chooser_view, (ViewGroup) this, true);
        this.i = new b();
        this.j = (LinearLayoutCompat) findViewById(a.f.activity_chooser_view_content);
        this.k = this.j.getBackground();
        this.f1644c = (FrameLayout) findViewById(a.f.default_activity_button);
        this.f1644c.setOnClickListener(this.i);
        this.f1644c.setOnLongClickListener(this.i);
        this.m = (ImageView) this.f1644c.findViewById(a.f.image);
        FrameLayout frameLayout = (FrameLayout) findViewById(a.f.expand_activities_button);
        frameLayout.setOnClickListener(this.i);
        frameLayout.setOnTouchListener(new x(frameLayout) {
            public m a() {
                return ActivityChooserView.this.getListPopupWindow();
            }

            /* access modifiers changed from: protected */
            public boolean b() {
                ActivityChooserView.this.a();
                return true;
            }

            /* access modifiers changed from: protected */
            public boolean c() {
                ActivityChooserView.this.b();
                return true;
            }
        });
        this.f1643b = frameLayout;
        this.l = (ImageView) frameLayout.findViewById(a.f.image);
        this.l.setImageDrawable(drawable);
        this.f1642a = new a();
        this.f1642a.registerDataSetObserver(new DataSetObserver() {
            public void onChanged() {
                super.onChanged();
                ActivityChooserView.this.d();
            }
        });
        Resources resources = context.getResources();
        this.n = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(a.d.abc_config_prefDialogWidth));
    }

    public void setActivityChooserModel(c cVar) {
        this.f1642a.a(cVar);
        if (c()) {
            b();
            a();
        }
    }

    public void setExpandActivityOverflowButtonDrawable(Drawable drawable) {
        this.l.setImageDrawable(drawable);
    }

    public void setExpandActivityOverflowButtonContentDescription(int i2) {
        this.l.setContentDescription(getContext().getString(i2));
    }

    public void setProvider(ActionProvider actionProvider) {
        this.f1645d = actionProvider;
    }

    public boolean a() {
        if (c() || !this.q) {
            return false;
        }
        this.f1648g = false;
        a(this.f1649h);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        int i3;
        if (this.f1642a.e() == null) {
            throw new IllegalStateException("No data model. Did you call #setDataModel?");
        }
        getViewTreeObserver().addOnGlobalLayoutListener(this.o);
        boolean z = this.f1644c.getVisibility() == 0;
        int c2 = this.f1642a.c();
        if (z) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        if (i2 == Integer.MAX_VALUE || c2 <= i3 + i2) {
            this.f1642a.a(false);
            this.f1642a.a(i2);
        } else {
            this.f1642a.a(true);
            this.f1642a.a(i2 - 1);
        }
        ListPopupWindow listPopupWindow = getListPopupWindow();
        if (!listPopupWindow.c()) {
            if (this.f1648g || !z) {
                this.f1642a.a(true, z);
            } else {
                this.f1642a.a(false, false);
            }
            listPopupWindow.g(Math.min(this.f1642a.a(), this.n));
            listPopupWindow.a();
            if (this.f1645d != null) {
                this.f1645d.a(true);
            }
            listPopupWindow.d().setContentDescription(getContext().getString(a.i.abc_activitychooserview_choose_application));
        }
    }

    public boolean b() {
        if (!c()) {
            return true;
        }
        getListPopupWindow().b();
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            return true;
        }
        viewTreeObserver.removeGlobalOnLayoutListener(this.o);
        return true;
    }

    public boolean c() {
        return getListPopupWindow().c();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        c e2 = this.f1642a.e();
        if (e2 != null) {
            e2.registerObserver(this.f1646e);
        }
        this.q = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        c e2 = this.f1642a.e();
        if (e2 != null) {
            e2.unregisterObserver(this.f1646e);
        }
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.removeGlobalOnLayoutListener(this.o);
        }
        if (c()) {
            b();
        }
        this.q = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        LinearLayoutCompat linearLayoutCompat = this.j;
        if (this.f1644c.getVisibility() != 0) {
            i3 = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i3), 1073741824);
        }
        measureChild(linearLayoutCompat, i2, i3);
        setMeasuredDimension(linearLayoutCompat.getMeasuredWidth(), linearLayoutCompat.getMeasuredHeight());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        this.j.layout(0, 0, i4 - i2, i5 - i3);
        if (!c()) {
            b();
        }
    }

    public c getDataModel() {
        return this.f1642a.e();
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        this.f1647f = onDismissListener;
    }

    public void setInitialActivityCount(int i2) {
        this.f1649h = i2;
    }

    public void setDefaultActionButtonContentDescription(int i2) {
        this.r = i2;
    }

    /* access modifiers changed from: package-private */
    public ListPopupWindow getListPopupWindow() {
        if (this.p == null) {
            this.p = new ListPopupWindow(getContext());
            this.p.a(this.f1642a);
            this.p.b(this);
            this.p.a(true);
            this.p.a((AdapterView.OnItemClickListener) this.i);
            this.p.a((PopupWindow.OnDismissListener) this.i);
        }
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.f1642a.getCount() > 0) {
            this.f1643b.setEnabled(true);
        } else {
            this.f1643b.setEnabled(false);
        }
        int c2 = this.f1642a.c();
        int d2 = this.f1642a.d();
        if (c2 == 1 || (c2 > 1 && d2 > 0)) {
            this.f1644c.setVisibility(0);
            ResolveInfo b2 = this.f1642a.b();
            PackageManager packageManager = getContext().getPackageManager();
            this.m.setImageDrawable(b2.loadIcon(packageManager));
            if (this.r != 0) {
                CharSequence loadLabel = b2.loadLabel(packageManager);
                this.f1644c.setContentDescription(getContext().getString(this.r, loadLabel));
            }
        } else {
            this.f1644c.setVisibility(8);
        }
        if (this.f1644c.getVisibility() == 0) {
            this.j.setBackgroundDrawable(this.k);
        } else {
            this.j.setBackgroundDrawable(null);
        }
    }

    private class b implements View.OnClickListener, View.OnLongClickListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {
        b() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            switch (((a) adapterView.getAdapter()).getItemViewType(i)) {
                case 0:
                    ActivityChooserView.this.b();
                    if (!ActivityChooserView.this.f1648g) {
                        if (!ActivityChooserView.this.f1642a.f()) {
                            i++;
                        }
                        Intent b2 = ActivityChooserView.this.f1642a.e().b(i);
                        if (b2 != null) {
                            b2.addFlags(524288);
                            ActivityChooserView.this.getContext().startActivity(b2);
                            return;
                        }
                        return;
                    } else if (i > 0) {
                        ActivityChooserView.this.f1642a.e().c(i);
                        return;
                    } else {
                        return;
                    }
                case 1:
                    ActivityChooserView.this.a(Integer.MAX_VALUE);
                    return;
                default:
                    throw new IllegalArgumentException();
            }
        }

        public void onClick(View view) {
            if (view == ActivityChooserView.this.f1644c) {
                ActivityChooserView.this.b();
                Intent b2 = ActivityChooserView.this.f1642a.e().b(ActivityChooserView.this.f1642a.e().a(ActivityChooserView.this.f1642a.b()));
                if (b2 != null) {
                    b2.addFlags(524288);
                    ActivityChooserView.this.getContext().startActivity(b2);
                }
            } else if (view == ActivityChooserView.this.f1643b) {
                ActivityChooserView.this.f1648g = false;
                ActivityChooserView.this.a(ActivityChooserView.this.f1649h);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public boolean onLongClick(View view) {
            if (view == ActivityChooserView.this.f1644c) {
                if (ActivityChooserView.this.f1642a.getCount() > 0) {
                    ActivityChooserView.this.f1648g = true;
                    ActivityChooserView.this.a(ActivityChooserView.this.f1649h);
                }
                return true;
            }
            throw new IllegalArgumentException();
        }

        public void onDismiss() {
            a();
            if (ActivityChooserView.this.f1645d != null) {
                ActivityChooserView.this.f1645d.a(false);
            }
        }

        private void a() {
            if (ActivityChooserView.this.f1647f != null) {
                ActivityChooserView.this.f1647f.onDismiss();
            }
        }
    }

    private class a extends BaseAdapter {

        /* renamed from: b  reason: collision with root package name */
        private c f1656b;

        /* renamed from: c  reason: collision with root package name */
        private int f1657c = 4;

        /* renamed from: d  reason: collision with root package name */
        private boolean f1658d;

        /* renamed from: e  reason: collision with root package name */
        private boolean f1659e;

        /* renamed from: f  reason: collision with root package name */
        private boolean f1660f;

        a() {
        }

        public void a(c cVar) {
            c e2 = ActivityChooserView.this.f1642a.e();
            if (e2 != null && ActivityChooserView.this.isShown()) {
                e2.unregisterObserver(ActivityChooserView.this.f1646e);
            }
            this.f1656b = cVar;
            if (cVar != null && ActivityChooserView.this.isShown()) {
                cVar.registerObserver(ActivityChooserView.this.f1646e);
            }
            notifyDataSetChanged();
        }

        public int getItemViewType(int i) {
            if (!this.f1660f || i != getCount() - 1) {
                return 0;
            }
            return 1;
        }

        public int getViewTypeCount() {
            return 3;
        }

        public int getCount() {
            int a2 = this.f1656b.a();
            if (!this.f1658d && this.f1656b.b() != null) {
                a2--;
            }
            int min = Math.min(a2, this.f1657c);
            if (this.f1660f) {
                return min + 1;
            }
            return min;
        }

        public Object getItem(int i) {
            switch (getItemViewType(i)) {
                case 0:
                    if (!this.f1658d && this.f1656b.b() != null) {
                        i++;
                    }
                    return this.f1656b.a(i);
                case 1:
                    return null;
                default:
                    throw new IllegalArgumentException();
            }
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v4.view.ag.c(android.view.View, boolean):void
         arg types: [android.view.View, int]
         candidates:
          android.support.v4.view.ag.c(android.view.View, float):void
          android.support.v4.view.ag.c(android.view.View, int):void
          android.support.v4.view.ag.c(android.view.View, boolean):void */
        public View getView(int i, View view, ViewGroup viewGroup) {
            switch (getItemViewType(i)) {
                case 0:
                    if (view == null || view.getId() != a.f.list_item) {
                        view = LayoutInflater.from(ActivityChooserView.this.getContext()).inflate(a.h.abc_activity_chooser_view_list_item, viewGroup, false);
                    }
                    PackageManager packageManager = ActivityChooserView.this.getContext().getPackageManager();
                    ResolveInfo resolveInfo = (ResolveInfo) getItem(i);
                    ((ImageView) view.findViewById(a.f.icon)).setImageDrawable(resolveInfo.loadIcon(packageManager));
                    ((TextView) view.findViewById(a.f.title)).setText(resolveInfo.loadLabel(packageManager));
                    if (!this.f1658d || i != 0 || !this.f1659e) {
                        ag.c(view, false);
                        return view;
                    }
                    ag.c(view, true);
                    return view;
                case 1:
                    if (view != null && view.getId() == 1) {
                        return view;
                    }
                    View inflate = LayoutInflater.from(ActivityChooserView.this.getContext()).inflate(a.h.abc_activity_chooser_view_list_item, viewGroup, false);
                    inflate.setId(1);
                    ((TextView) inflate.findViewById(a.f.title)).setText(ActivityChooserView.this.getContext().getString(a.i.abc_activity_chooser_view_see_all));
                    return inflate;
                default:
                    throw new IllegalArgumentException();
            }
        }

        public int a() {
            int i = this.f1657c;
            this.f1657c = Integer.MAX_VALUE;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
            int count = getCount();
            View view = null;
            int i2 = 0;
            for (int i3 = 0; i3 < count; i3++) {
                view = getView(i3, view, null);
                view.measure(makeMeasureSpec, makeMeasureSpec2);
                i2 = Math.max(i2, view.getMeasuredWidth());
            }
            this.f1657c = i;
            return i2;
        }

        public void a(int i) {
            if (this.f1657c != i) {
                this.f1657c = i;
                notifyDataSetChanged();
            }
        }

        public ResolveInfo b() {
            return this.f1656b.b();
        }

        public void a(boolean z) {
            if (this.f1660f != z) {
                this.f1660f = z;
                notifyDataSetChanged();
            }
        }

        public int c() {
            return this.f1656b.a();
        }

        public int d() {
            return this.f1656b.c();
        }

        public c e() {
            return this.f1656b;
        }

        public void a(boolean z, boolean z2) {
            if (this.f1658d != z || this.f1659e != z2) {
                this.f1658d = z;
                this.f1659e = z2;
                notifyDataSetChanged();
            }
        }

        public boolean f() {
            return this.f1658d;
        }
    }

    public static class InnerLayout extends LinearLayoutCompat {

        /* renamed from: a  reason: collision with root package name */
        private static final int[] f1654a = {16842964};

        public InnerLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            an a2 = an.a(context, attributeSet, f1654a);
            setBackgroundDrawable(a2.a(0));
            a2.a();
        }
    }
}
