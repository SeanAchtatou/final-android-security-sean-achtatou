package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.a.q;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

public class LinearLayoutManager extends RecyclerView.h implements RecyclerView.q.b, ItemTouchHelper.d {

    /* renamed from: a  reason: collision with root package name */
    private c f1787a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1788b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1789c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f1790d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f1791e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f1792f;

    /* renamed from: g  reason: collision with root package name */
    private final b f1793g;

    /* renamed from: h  reason: collision with root package name */
    private int f1794h;
    int i;
    ac j;
    boolean k;
    int l;
    int m;
    SavedState n;
    final a o;

    public LinearLayoutManager(Context context) {
        this(context, 1, false);
    }

    public LinearLayoutManager(Context context, int i2, boolean z) {
        this.f1789c = false;
        this.k = false;
        this.f1790d = false;
        this.f1791e = true;
        this.l = -1;
        this.m = Integer.MIN_VALUE;
        this.n = null;
        this.o = new a();
        this.f1793g = new b();
        this.f1794h = 2;
        b(i2);
        b(z);
        c(true);
    }

    public LinearLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.f1789c = false;
        this.k = false;
        this.f1790d = false;
        this.f1791e = true;
        this.l = -1;
        this.m = Integer.MIN_VALUE;
        this.n = null;
        this.o = new a();
        this.f1793g = new b();
        this.f1794h = 2;
        RecyclerView.h.b a2 = a(context, attributeSet, i2, i3);
        b(a2.f1885a);
        b(a2.f1887c);
        a(a2.f1888d);
        c(true);
    }

    public RecyclerView.LayoutParams a() {
        return new RecyclerView.LayoutParams(-2, -2);
    }

    public void a(RecyclerView recyclerView, RecyclerView.n nVar) {
        super.a(recyclerView, nVar);
        if (this.f1792f) {
            c(nVar);
            nVar.a();
        }
    }

    public void a(AccessibilityEvent accessibilityEvent) {
        super.a(accessibilityEvent);
        if (u() > 0) {
            q a2 = android.support.v4.view.a.a.a(accessibilityEvent);
            a2.b(l());
            a2.c(m());
        }
    }

    public Parcelable c() {
        if (this.n != null) {
            return new SavedState(this.n);
        }
        SavedState savedState = new SavedState();
        if (u() > 0) {
            h();
            boolean z = this.f1788b ^ this.k;
            savedState.f1797c = z;
            if (z) {
                View M = M();
                savedState.f1796b = this.j.d() - this.j.b(M);
                savedState.f1795a = d(M);
                return savedState;
            }
            View L = L();
            savedState.f1795a = d(L);
            savedState.f1796b = this.j.a(L) - this.j.c();
            return savedState;
        }
        savedState.b();
        return savedState;
    }

    public void a(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.n = (SavedState) parcelable;
            n();
        }
    }

    public boolean d() {
        return this.i == 0;
    }

    public boolean e() {
        return this.i == 1;
    }

    public void a(boolean z) {
        a((String) null);
        if (this.f1790d != z) {
            this.f1790d = z;
            n();
        }
    }

    public int f() {
        return this.i;
    }

    public void b(int i2) {
        if (i2 == 0 || i2 == 1) {
            a((String) null);
            if (i2 != this.i) {
                this.i = i2;
                this.j = null;
                n();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation:" + i2);
    }

    private void K() {
        boolean z = true;
        if (this.i == 1 || !g()) {
            this.k = this.f1789c;
            return;
        }
        if (this.f1789c) {
            z = false;
        }
        this.k = z;
    }

    public void b(boolean z) {
        a((String) null);
        if (z != this.f1789c) {
            this.f1789c = z;
            n();
        }
    }

    public View c(int i2) {
        int u = u();
        if (u == 0) {
            return null;
        }
        int d2 = i2 - d(i(0));
        if (d2 >= 0 && d2 < u) {
            View i3 = i(d2);
            if (d(i3) == i2) {
                return i3;
            }
        }
        return super.c(i2);
    }

    /* access modifiers changed from: protected */
    public int b(RecyclerView.r rVar) {
        if (rVar.d()) {
            return this.j.f();
        }
        return 0;
    }

    public void a(RecyclerView recyclerView, RecyclerView.r rVar, int i2) {
        LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext());
        linearSmoothScroller.d(i2);
        a(linearSmoothScroller);
    }

    public PointF d(int i2) {
        int i3 = 1;
        boolean z = false;
        if (u() == 0) {
            return null;
        }
        if (i2 < d(i(0))) {
            z = true;
        }
        if (z != this.k) {
            i3 = -1;
        }
        if (this.i == 0) {
            return new PointF((float) i3, 0.0f);
        }
        return new PointF(0.0f, (float) i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
     arg types: [android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
     arg types: [int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.b(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
     arg types: [int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.b(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.b(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):int[]
      android.support.v7.widget.RecyclerView.h.b(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.LinearLayoutManager.b(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int */
    public void c(RecyclerView.n nVar, RecyclerView.r rVar) {
        int i2;
        int i3;
        int i4;
        int i5;
        View c2;
        int a2;
        int i6 = -1;
        if (!(this.n == null && this.l == -1) && rVar.e() == 0) {
            c(nVar);
            return;
        }
        if (this.n != null && this.n.a()) {
            this.l = this.n.f1795a;
        }
        h();
        this.f1787a.f1807a = false;
        K();
        if (!(this.o.f1801d && this.l == -1 && this.n == null)) {
            this.o.a();
            this.o.f1800c = this.k ^ this.f1790d;
            a(nVar, rVar, this.o);
            this.o.f1801d = true;
        }
        int b2 = b(rVar);
        if (this.f1787a.j >= 0) {
            i2 = 0;
        } else {
            i2 = b2;
            b2 = 0;
        }
        int c3 = i2 + this.j.c();
        int g2 = b2 + this.j.g();
        if (!(!rVar.a() || this.l == -1 || this.m == Integer.MIN_VALUE || (c2 = c(this.l)) == null)) {
            if (this.k) {
                a2 = (this.j.d() - this.j.b(c2)) - this.m;
            } else {
                a2 = this.m - (this.j.a(c2) - this.j.c());
            }
            if (a2 > 0) {
                c3 += a2;
            } else {
                g2 -= a2;
            }
        }
        if (this.o.f1800c) {
            if (this.k) {
                i6 = 1;
            }
        } else if (!this.k) {
            i6 = 1;
        }
        a(nVar, rVar, this.o, i6);
        a(nVar);
        this.f1787a.l = j();
        this.f1787a.i = rVar.a();
        if (this.o.f1800c) {
            b(this.o);
            this.f1787a.f1814h = c3;
            a(nVar, this.f1787a, rVar, false);
            int i7 = this.f1787a.f1808b;
            int i8 = this.f1787a.f1810d;
            if (this.f1787a.f1809c > 0) {
                g2 += this.f1787a.f1809c;
            }
            a(this.o);
            this.f1787a.f1814h = g2;
            this.f1787a.f1810d += this.f1787a.f1811e;
            a(nVar, this.f1787a, rVar, false);
            int i9 = this.f1787a.f1808b;
            if (this.f1787a.f1809c > 0) {
                int i10 = this.f1787a.f1809c;
                h(i8, i7);
                this.f1787a.f1814h = i10;
                a(nVar, this.f1787a, rVar, false);
                i5 = this.f1787a.f1808b;
            } else {
                i5 = i7;
            }
            i4 = i5;
            i3 = i9;
        } else {
            a(this.o);
            this.f1787a.f1814h = g2;
            a(nVar, this.f1787a, rVar, false);
            i3 = this.f1787a.f1808b;
            int i11 = this.f1787a.f1810d;
            if (this.f1787a.f1809c > 0) {
                c3 += this.f1787a.f1809c;
            }
            b(this.o);
            this.f1787a.f1814h = c3;
            this.f1787a.f1810d += this.f1787a.f1811e;
            a(nVar, this.f1787a, rVar, false);
            i4 = this.f1787a.f1808b;
            if (this.f1787a.f1809c > 0) {
                int i12 = this.f1787a.f1809c;
                a(i11, i3);
                this.f1787a.f1814h = i12;
                a(nVar, this.f1787a, rVar, false);
                i3 = this.f1787a.f1808b;
            }
        }
        if (u() > 0) {
            if (this.k ^ this.f1790d) {
                int a3 = a(i3, nVar, rVar, true);
                int i13 = i4 + a3;
                int b3 = b(i13, nVar, rVar, false);
                i4 = i13 + b3;
                i3 = i3 + a3 + b3;
            } else {
                int b4 = b(i4, nVar, rVar, true);
                int i14 = i3 + b4;
                int a4 = a(i14, nVar, rVar, false);
                i4 = i4 + b4 + a4;
                i3 = i14 + a4;
            }
        }
        b(nVar, rVar, i4, i3);
        if (!rVar.a()) {
            this.j.a();
        } else {
            this.o.a();
        }
        this.f1788b = this.f1790d;
    }

    public void a(RecyclerView.r rVar) {
        super.a(rVar);
        this.n = null;
        this.l = -1;
        this.m = Integer.MIN_VALUE;
        this.o.a();
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.n nVar, RecyclerView.r rVar, a aVar, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
     arg types: [android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int */
    private void b(RecyclerView.n nVar, RecyclerView.r rVar, int i2, int i3) {
        int e2;
        int i4;
        if (rVar.b() && u() != 0 && !rVar.a() && b()) {
            int i5 = 0;
            int i6 = 0;
            List<RecyclerView.u> c2 = nVar.c();
            int size = c2.size();
            int d2 = d(i(0));
            int i7 = 0;
            while (i7 < size) {
                RecyclerView.u uVar = c2.get(i7);
                if (uVar.isRemoved()) {
                    e2 = i6;
                    i4 = i5;
                } else {
                    if (((uVar.getLayoutPosition() < d2) != this.k ? (char) 65535 : 1) == 65535) {
                        i4 = this.j.e(uVar.itemView) + i5;
                        e2 = i6;
                    } else {
                        e2 = this.j.e(uVar.itemView) + i6;
                        i4 = i5;
                    }
                }
                i7++;
                i5 = i4;
                i6 = e2;
            }
            this.f1787a.k = c2;
            if (i5 > 0) {
                h(d(L()), i2);
                this.f1787a.f1814h = i5;
                this.f1787a.f1809c = 0;
                this.f1787a.a();
                a(nVar, this.f1787a, rVar, false);
            }
            if (i6 > 0) {
                a(d(M()), i3);
                this.f1787a.f1814h = i6;
                this.f1787a.f1809c = 0;
                this.f1787a.a();
                a(nVar, this.f1787a, rVar, false);
            }
            this.f1787a.k = null;
        }
    }

    private void a(RecyclerView.n nVar, RecyclerView.r rVar, a aVar) {
        if (!a(rVar, aVar) && !b(nVar, rVar, aVar)) {
            aVar.b();
            aVar.f1798a = this.f1790d ? rVar.e() - 1 : 0;
        }
    }

    private boolean b(RecyclerView.n nVar, RecyclerView.r rVar, a aVar) {
        View g2;
        int c2;
        boolean z = false;
        if (u() == 0) {
            return false;
        }
        View D = D();
        if (D != null && aVar.a(D, rVar)) {
            aVar.a(D);
            return true;
        } else if (this.f1788b != this.f1790d) {
            return false;
        } else {
            if (aVar.f1800c) {
                g2 = f(nVar, rVar);
            } else {
                g2 = g(nVar, rVar);
            }
            if (g2 == null) {
                return false;
            }
            aVar.b(g2);
            if (!rVar.a() && b()) {
                if (this.j.a(g2) >= this.j.d() || this.j.b(g2) < this.j.c()) {
                    z = true;
                }
                if (z) {
                    if (aVar.f1800c) {
                        c2 = this.j.d();
                    } else {
                        c2 = this.j.c();
                    }
                    aVar.f1799b = c2;
                }
            }
            return true;
        }
    }

    private boolean a(RecyclerView.r rVar, a aVar) {
        boolean z;
        int a2;
        boolean z2 = false;
        if (rVar.a() || this.l == -1) {
            return false;
        }
        if (this.l < 0 || this.l >= rVar.e()) {
            this.l = -1;
            this.m = Integer.MIN_VALUE;
            return false;
        }
        aVar.f1798a = this.l;
        if (this.n != null && this.n.a()) {
            aVar.f1800c = this.n.f1797c;
            if (aVar.f1800c) {
                aVar.f1799b = this.j.d() - this.n.f1796b;
                return true;
            }
            aVar.f1799b = this.j.c() + this.n.f1796b;
            return true;
        } else if (this.m == Integer.MIN_VALUE) {
            View c2 = c(this.l);
            if (c2 == null) {
                if (u() > 0) {
                    if (this.l < d(i(0))) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (z == this.k) {
                        z2 = true;
                    }
                    aVar.f1800c = z2;
                }
                aVar.b();
                return true;
            } else if (this.j.e(c2) > this.j.f()) {
                aVar.b();
                return true;
            } else if (this.j.a(c2) - this.j.c() < 0) {
                aVar.f1799b = this.j.c();
                aVar.f1800c = false;
                return true;
            } else if (this.j.d() - this.j.b(c2) < 0) {
                aVar.f1799b = this.j.d();
                aVar.f1800c = true;
                return true;
            } else {
                if (aVar.f1800c) {
                    a2 = this.j.b(c2) + this.j.b();
                } else {
                    a2 = this.j.a(c2);
                }
                aVar.f1799b = a2;
                return true;
            }
        } else {
            aVar.f1800c = this.k;
            if (this.k) {
                aVar.f1799b = this.j.d() - this.m;
                return true;
            }
            aVar.f1799b = this.j.c() + this.m;
            return true;
        }
    }

    private int a(int i2, RecyclerView.n nVar, RecyclerView.r rVar, boolean z) {
        int d2;
        int d3 = this.j.d() - i2;
        if (d3 <= 0) {
            return 0;
        }
        int i3 = -c(-d3, nVar, rVar);
        int i4 = i2 + i3;
        if (!z || (d2 = this.j.d() - i4) <= 0) {
            return i3;
        }
        this.j.a(d2);
        return i3 + d2;
    }

    private int b(int i2, RecyclerView.n nVar, RecyclerView.r rVar, boolean z) {
        int c2;
        int c3 = i2 - this.j.c();
        if (c3 <= 0) {
            return 0;
        }
        int i3 = -c(c3, nVar, rVar);
        int i4 = i2 + i3;
        if (!z || (c2 = i4 - this.j.c()) <= 0) {
            return i3;
        }
        this.j.a(-c2);
        return i3 - c2;
    }

    private void a(a aVar) {
        a(aVar.f1798a, aVar.f1799b);
    }

    private void a(int i2, int i3) {
        this.f1787a.f1809c = this.j.d() - i3;
        this.f1787a.f1811e = this.k ? -1 : 1;
        this.f1787a.f1810d = i2;
        this.f1787a.f1812f = 1;
        this.f1787a.f1808b = i3;
        this.f1787a.f1813g = Integer.MIN_VALUE;
    }

    private void b(a aVar) {
        h(aVar.f1798a, aVar.f1799b);
    }

    private void h(int i2, int i3) {
        this.f1787a.f1809c = i3 - this.j.c();
        this.f1787a.f1810d = i2;
        this.f1787a.f1811e = this.k ? 1 : -1;
        this.f1787a.f1812f = -1;
        this.f1787a.f1808b = i3;
        this.f1787a.f1813g = Integer.MIN_VALUE;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return s() == 1;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (this.f1787a == null) {
            this.f1787a = i();
        }
        if (this.j == null) {
            this.j = ac.a(this, this.i);
        }
    }

    /* access modifiers changed from: package-private */
    public c i() {
        return new c();
    }

    public void e(int i2) {
        this.l = i2;
        this.m = Integer.MIN_VALUE;
        if (this.n != null) {
            this.n.b();
        }
        n();
    }

    public void b(int i2, int i3) {
        this.l = i2;
        this.m = i3;
        if (this.n != null) {
            this.n.b();
        }
        n();
    }

    public int a(int i2, RecyclerView.n nVar, RecyclerView.r rVar) {
        if (this.i == 1) {
            return 0;
        }
        return c(i2, nVar, rVar);
    }

    public int b(int i2, RecyclerView.n nVar, RecyclerView.r rVar) {
        if (this.i == 0) {
            return 0;
        }
        return c(i2, nVar, rVar);
    }

    public int c(RecyclerView.r rVar) {
        return i(rVar);
    }

    public int d(RecyclerView.r rVar) {
        return i(rVar);
    }

    public int e(RecyclerView.r rVar) {
        return j(rVar);
    }

    public int f(RecyclerView.r rVar) {
        return j(rVar);
    }

    public int g(RecyclerView.r rVar) {
        return k(rVar);
    }

    public int h(RecyclerView.r rVar) {
        return k(rVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a):boolean
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(int, android.view.View):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$h, android.support.v7.widget.RecyclerView$q):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet):android.support.v7.widget.RecyclerView$LayoutParams
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$a, android.support.v7.widget.RecyclerView$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(int, android.os.Bundle):boolean
      android.support.v7.widget.LinearLayoutManager.a(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.b(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.b(android.support.v7.widget.RecyclerView$n, int):void
      android.support.v7.widget.LinearLayoutManager.b(int, int):void
      android.support.v7.widget.RecyclerView.h.b(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.b(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.b(android.view.View, int):void
      android.support.v7.widget.RecyclerView.h.b(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.LinearLayoutManager.b(boolean, boolean):android.view.View */
    private int i(RecyclerView.r rVar) {
        boolean z = false;
        if (u() == 0) {
            return 0;
        }
        h();
        ac acVar = this.j;
        View a2 = a(!this.f1791e, true);
        if (!this.f1791e) {
            z = true;
        }
        return ah.a(rVar, acVar, a2, b(z, true), this, this.f1791e, this.k);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a):boolean
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(int, android.view.View):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$h, android.support.v7.widget.RecyclerView$q):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet):android.support.v7.widget.RecyclerView$LayoutParams
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$a, android.support.v7.widget.RecyclerView$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(int, android.os.Bundle):boolean
      android.support.v7.widget.LinearLayoutManager.a(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.b(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.b(android.support.v7.widget.RecyclerView$n, int):void
      android.support.v7.widget.LinearLayoutManager.b(int, int):void
      android.support.v7.widget.RecyclerView.h.b(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.b(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.b(android.view.View, int):void
      android.support.v7.widget.RecyclerView.h.b(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.LinearLayoutManager.b(boolean, boolean):android.view.View */
    private int j(RecyclerView.r rVar) {
        boolean z = false;
        if (u() == 0) {
            return 0;
        }
        h();
        ac acVar = this.j;
        View a2 = a(!this.f1791e, true);
        if (!this.f1791e) {
            z = true;
        }
        return ah.a(rVar, acVar, a2, b(z, true), this, this.f1791e);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a):boolean
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(int, android.view.View):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$h, android.support.v7.widget.RecyclerView$q):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet):android.support.v7.widget.RecyclerView$LayoutParams
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$a, android.support.v7.widget.RecyclerView$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(int, android.os.Bundle):boolean
      android.support.v7.widget.LinearLayoutManager.a(boolean, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.b(boolean, boolean):android.view.View
     arg types: [boolean, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.b(android.support.v7.widget.RecyclerView$n, int):void
      android.support.v7.widget.LinearLayoutManager.b(int, int):void
      android.support.v7.widget.RecyclerView.h.b(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.b(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.b(android.view.View, int):void
      android.support.v7.widget.RecyclerView.h.b(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.LinearLayoutManager.b(boolean, boolean):android.view.View */
    private int k(RecyclerView.r rVar) {
        boolean z = false;
        if (u() == 0) {
            return 0;
        }
        h();
        ac acVar = this.j;
        View a2 = a(!this.f1791e, true);
        if (!this.f1791e) {
            z = true;
        }
        return ah.b(rVar, acVar, a2, b(z, true), this, this.f1791e);
    }

    private void a(int i2, int i3, boolean z, RecyclerView.r rVar) {
        int c2;
        int i4 = -1;
        int i5 = 1;
        this.f1787a.l = j();
        this.f1787a.f1814h = b(rVar);
        this.f1787a.f1812f = i2;
        if (i2 == 1) {
            this.f1787a.f1814h += this.j.g();
            View M = M();
            c cVar = this.f1787a;
            if (!this.k) {
                i4 = 1;
            }
            cVar.f1811e = i4;
            this.f1787a.f1810d = d(M) + this.f1787a.f1811e;
            this.f1787a.f1808b = this.j.b(M);
            c2 = this.j.b(M) - this.j.d();
        } else {
            View L = L();
            this.f1787a.f1814h += this.j.c();
            c cVar2 = this.f1787a;
            if (!this.k) {
                i5 = -1;
            }
            cVar2.f1811e = i5;
            this.f1787a.f1810d = d(L) + this.f1787a.f1811e;
            this.f1787a.f1808b = this.j.a(L);
            c2 = (-this.j.a(L)) + this.j.c();
        }
        this.f1787a.f1809c = i3;
        if (z) {
            this.f1787a.f1809c -= c2;
        }
        this.f1787a.f1813g = c2;
    }

    /* access modifiers changed from: package-private */
    public boolean j() {
        return this.j.h() == 0 && this.j.e() == 0;
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.r rVar, c cVar, RecyclerView.h.a aVar) {
        int i2 = cVar.f1810d;
        if (i2 >= 0 && i2 < rVar.e()) {
            aVar.b(i2, Math.max(0, cVar.f1813g));
        }
    }

    public void a(int i2, RecyclerView.h.a aVar) {
        int i3;
        boolean z;
        if (this.n == null || !this.n.a()) {
            K();
            boolean z2 = this.k;
            if (this.l == -1) {
                i3 = z2 ? i2 - 1 : 0;
                z = z2;
            } else {
                i3 = this.l;
                z = z2;
            }
        } else {
            z = this.n.f1797c;
            i3 = this.n.f1795a;
        }
        int i4 = z ? -1 : 1;
        for (int i5 = 0; i5 < this.f1794h && i3 >= 0 && i3 < i2; i5++) {
            aVar.b(i3, 0);
            i3 += i4;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
     arg types: [int, int, int, android.support.v7.widget.RecyclerView$r]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void */
    public void a(int i2, int i3, RecyclerView.r rVar, RecyclerView.h.a aVar) {
        if (this.i != 0) {
            i2 = i3;
        }
        if (u() != 0 && i2 != 0) {
            a(i2 > 0 ? 1 : -1, Math.abs(i2), true, rVar);
            a(rVar, this.f1787a, aVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
     arg types: [int, int, int, android.support.v7.widget.RecyclerView$r]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
     arg types: [android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int */
    /* access modifiers changed from: package-private */
    public int c(int i2, RecyclerView.n nVar, RecyclerView.r rVar) {
        if (u() == 0 || i2 == 0) {
            return 0;
        }
        this.f1787a.f1807a = true;
        h();
        int i3 = i2 > 0 ? 1 : -1;
        int abs = Math.abs(i2);
        a(i3, abs, true, rVar);
        int a2 = this.f1787a.f1813g + a(nVar, this.f1787a, rVar, false);
        if (a2 < 0) {
            return 0;
        }
        if (abs > a2) {
            i2 = i3 * a2;
        }
        this.j.a(-i2);
        this.f1787a.j = i2;
        return i2;
    }

    public void a(String str) {
        if (this.n == null) {
            super.a(str);
        }
    }

    private void a(RecyclerView.n nVar, int i2, int i3) {
        if (i2 != i3) {
            if (i3 > i2) {
                for (int i4 = i3 - 1; i4 >= i2; i4--) {
                    a(i4, nVar);
                }
                return;
            }
            while (i2 > i3) {
                a(i2, nVar);
                i2--;
            }
        }
    }

    private void a(RecyclerView.n nVar, int i2) {
        if (i2 >= 0) {
            int u = u();
            if (this.k) {
                for (int i3 = u - 1; i3 >= 0; i3--) {
                    View i4 = i(i3);
                    if (this.j.b(i4) > i2 || this.j.c(i4) > i2) {
                        a(nVar, u - 1, i3);
                        return;
                    }
                }
                return;
            }
            for (int i5 = 0; i5 < u; i5++) {
                View i6 = i(i5);
                if (this.j.b(i6) > i2 || this.j.c(i6) > i2) {
                    a(nVar, 0, i5);
                    return;
                }
            }
        }
    }

    private void b(RecyclerView.n nVar, int i2) {
        int u = u();
        if (i2 >= 0) {
            int e2 = this.j.e() - i2;
            if (this.k) {
                for (int i3 = 0; i3 < u; i3++) {
                    View i4 = i(i3);
                    if (this.j.a(i4) < e2 || this.j.d(i4) < e2) {
                        a(nVar, 0, i3);
                        return;
                    }
                }
                return;
            }
            for (int i5 = u - 1; i5 >= 0; i5--) {
                View i6 = i(i5);
                if (this.j.a(i6) < e2 || this.j.d(i6) < e2) {
                    a(nVar, u - 1, i5);
                    return;
                }
            }
        }
    }

    private void a(RecyclerView.n nVar, c cVar) {
        if (cVar.f1807a && !cVar.l) {
            if (cVar.f1812f == -1) {
                b(nVar, cVar.f1813g);
            } else {
                a(nVar, cVar.f1813g);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a(RecyclerView.n nVar, c cVar, RecyclerView.r rVar, boolean z) {
        int i2 = cVar.f1809c;
        if (cVar.f1813g != Integer.MIN_VALUE) {
            if (cVar.f1809c < 0) {
                cVar.f1813g += cVar.f1809c;
            }
            a(nVar, cVar);
        }
        int i3 = cVar.f1809c + cVar.f1814h;
        b bVar = this.f1793g;
        while (true) {
            if ((!cVar.l && i3 <= 0) || !cVar.a(rVar)) {
                break;
            }
            bVar.a();
            a(nVar, rVar, cVar, bVar);
            if (!bVar.f1804b) {
                cVar.f1808b += bVar.f1803a * cVar.f1812f;
                if (!bVar.f1805c || this.f1787a.k != null || !rVar.a()) {
                    cVar.f1809c -= bVar.f1803a;
                    i3 -= bVar.f1803a;
                }
                if (cVar.f1813g != Integer.MIN_VALUE) {
                    cVar.f1813g += bVar.f1803a;
                    if (cVar.f1809c < 0) {
                        cVar.f1813g += cVar.f1809c;
                    }
                    a(nVar, cVar);
                }
                if (z && bVar.f1806d) {
                    break;
                }
            } else {
                break;
            }
        }
        return i2 - cVar.f1809c;
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.n nVar, RecyclerView.r rVar, c cVar, b bVar) {
        boolean z;
        int A;
        int f2;
        int i2;
        int i3;
        int f3;
        View a2 = cVar.a(nVar);
        if (a2 == null) {
            bVar.f1804b = true;
            return;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) a2.getLayoutParams();
        if (cVar.k == null) {
            if (this.k == (cVar.f1812f == -1)) {
                b(a2);
            } else {
                b(a2, 0);
            }
        } else {
            boolean z2 = this.k;
            if (cVar.f1812f == -1) {
                z = true;
            } else {
                z = false;
            }
            if (z2 == z) {
                a(a2);
            } else {
                a(a2, 0);
            }
        }
        a(a2, 0, 0);
        bVar.f1803a = this.j.e(a2);
        if (this.i == 1) {
            if (g()) {
                f3 = x() - B();
                i2 = f3 - this.j.f(a2);
            } else {
                i2 = z();
                f3 = this.j.f(a2) + i2;
            }
            if (cVar.f1812f == -1) {
                f2 = cVar.f1808b;
                A = cVar.f1808b - bVar.f1803a;
                i3 = f3;
            } else {
                A = cVar.f1808b;
                f2 = bVar.f1803a + cVar.f1808b;
                i3 = f3;
            }
        } else {
            A = A();
            f2 = A + this.j.f(a2);
            if (cVar.f1812f == -1) {
                int i4 = cVar.f1808b;
                i2 = cVar.f1808b - bVar.f1803a;
                i3 = i4;
            } else {
                i2 = cVar.f1808b;
                i3 = cVar.f1808b + bVar.f1803a;
            }
        }
        a(a2, i2, A, i3, f2);
        if (layoutParams.d() || layoutParams.e()) {
            bVar.f1805c = true;
        }
        bVar.f1806d = a2.hasFocusable();
    }

    /* access modifiers changed from: package-private */
    public boolean k() {
        return (w() == 1073741824 || v() == 1073741824 || !J()) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public int f(int i2) {
        int i3 = Integer.MIN_VALUE;
        int i4 = 1;
        switch (i2) {
            case 1:
                if (this.i == 1 || !g()) {
                    return -1;
                }
                return 1;
            case 2:
                if (this.i == 1) {
                    return 1;
                }
                if (!g()) {
                    return 1;
                }
                return -1;
            case 17:
                if (this.i != 0) {
                    return Integer.MIN_VALUE;
                }
                return -1;
            case 33:
                if (this.i != 1) {
                    return Integer.MIN_VALUE;
                }
                return -1;
            case 66:
                if (this.i != 0) {
                    i4 = Integer.MIN_VALUE;
                }
                return i4;
            case 130:
                if (this.i == 1) {
                    i3 = 1;
                }
                return i3;
            default:
                return Integer.MIN_VALUE;
        }
    }

    private View L() {
        return i(this.k ? u() - 1 : 0);
    }

    private View M() {
        return i(this.k ? 0 : u() - 1);
    }

    private View a(boolean z, boolean z2) {
        if (this.k) {
            return a(u() - 1, -1, z, z2);
        }
        return a(0, u(), z, z2);
    }

    private View b(boolean z, boolean z2) {
        if (this.k) {
            return a(0, u(), z, z2);
        }
        return a(u() - 1, -1, z, z2);
    }

    private View f(RecyclerView.n nVar, RecyclerView.r rVar) {
        if (this.k) {
            return h(nVar, rVar);
        }
        return i(nVar, rVar);
    }

    private View g(RecyclerView.n nVar, RecyclerView.r rVar) {
        if (this.k) {
            return i(nVar, rVar);
        }
        return h(nVar, rVar);
    }

    private View h(RecyclerView.n nVar, RecyclerView.r rVar) {
        return a(nVar, rVar, 0, u(), rVar.e());
    }

    private View i(RecyclerView.n nVar, RecyclerView.r rVar) {
        return a(nVar, rVar, u() - 1, -1, rVar.e());
    }

    /* access modifiers changed from: package-private */
    public View a(RecyclerView.n nVar, RecyclerView.r rVar, int i2, int i3, int i4) {
        View view;
        View view2 = null;
        h();
        int c2 = this.j.c();
        int d2 = this.j.d();
        int i5 = i3 > i2 ? 1 : -1;
        View view3 = null;
        while (i2 != i3) {
            View i6 = i(i2);
            int d3 = d(i6);
            if (d3 >= 0 && d3 < i4) {
                if (((RecyclerView.LayoutParams) i6.getLayoutParams()).d()) {
                    if (view3 == null) {
                        view = view2;
                        i2 += i5;
                        view2 = view;
                        view3 = i6;
                    }
                } else if (this.j.a(i6) < d2 && this.j.b(i6) >= c2) {
                    return i6;
                } else {
                    if (view2 == null) {
                        view = i6;
                        i6 = view3;
                        i2 += i5;
                        view2 = view;
                        view3 = i6;
                    }
                }
            }
            view = view2;
            i6 = view3;
            i2 += i5;
            view2 = view;
            view3 = i6;
        }
        if (view2 == null) {
            view2 = view3;
        }
        return view2;
    }

    private View j(RecyclerView.n nVar, RecyclerView.r rVar) {
        if (this.k) {
            return l(nVar, rVar);
        }
        return m(nVar, rVar);
    }

    private View k(RecyclerView.n nVar, RecyclerView.r rVar) {
        if (this.k) {
            return m(nVar, rVar);
        }
        return l(nVar, rVar);
    }

    private View l(RecyclerView.n nVar, RecyclerView.r rVar) {
        return c(0, u());
    }

    private View m(RecyclerView.n nVar, RecyclerView.r rVar) {
        return c(u() - 1, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
     arg types: [int, int, int, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View */
    public int l() {
        View a2 = a(0, u(), false, true);
        if (a2 == null) {
            return -1;
        }
        return d(a2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
     arg types: [int, int, int, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View */
    public int m() {
        View a2 = a(u() - 1, -1, false, true);
        if (a2 == null) {
            return -1;
        }
        return d(a2);
    }

    /* access modifiers changed from: package-private */
    public View a(int i2, int i3, boolean z, boolean z2) {
        int i4;
        int i5 = 320;
        h();
        if (z) {
            i4 = 24579;
        } else {
            i4 = 320;
        }
        if (!z2) {
            i5 = 0;
        }
        if (this.i == 0) {
            return this.r.a(i2, i3, i4, i5);
        }
        return this.s.a(i2, i3, i4, i5);
    }

    /* access modifiers changed from: package-private */
    public View c(int i2, int i3) {
        int i4;
        int i5;
        h();
        if ((i3 > i2 ? 1 : i3 < i2 ? (char) 65535 : 0) == 0) {
            return i(i2);
        }
        if (this.j.a(i(i2)) < this.j.c()) {
            i4 = 16644;
            i5 = 16388;
        } else {
            i4 = 4161;
            i5 = 4097;
        }
        if (this.i == 0) {
            return this.r.a(i2, i3, i4, i5);
        }
        return this.s.a(i2, i3, i4, i5);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
     arg types: [int, int, int, android.support.v7.widget.RecyclerView$r]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
     arg types: [android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, int]
     candidates:
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int */
    public View a(View view, int i2, RecyclerView.n nVar, RecyclerView.r rVar) {
        View j2;
        View M;
        K();
        if (u() == 0) {
            return null;
        }
        int f2 = f(i2);
        if (f2 == Integer.MIN_VALUE) {
            return null;
        }
        h();
        h();
        a(f2, (int) (0.33333334f * ((float) this.j.f())), false, rVar);
        this.f1787a.f1813g = Integer.MIN_VALUE;
        this.f1787a.f1807a = false;
        a(nVar, this.f1787a, rVar, true);
        if (f2 == -1) {
            j2 = k(nVar, rVar);
        } else {
            j2 = j(nVar, rVar);
        }
        if (f2 == -1) {
            M = L();
        } else {
            M = M();
        }
        if (!M.hasFocusable()) {
            return j2;
        }
        if (j2 == null) {
            return null;
        }
        return M;
    }

    public boolean b() {
        return this.n == null && this.f1788b == this.f1790d;
    }

    public void a(View view, View view2, int i2, int i3) {
        a("Cannot drop a view during a scroll or layout calculation");
        h();
        K();
        int d2 = d(view);
        int d3 = d(view2);
        boolean z = d2 < d3 ? true : true;
        if (this.k) {
            if (z) {
                b(d3, this.j.d() - (this.j.a(view2) + this.j.e(view)));
            } else {
                b(d3, this.j.d() - this.j.b(view2));
            }
        } else if (z) {
            b(d3, this.j.a(view2));
        } else {
            b(d3, this.j.b(view2) - this.j.e(view));
        }
    }

    static class c {

        /* renamed from: a  reason: collision with root package name */
        boolean f1807a = true;

        /* renamed from: b  reason: collision with root package name */
        int f1808b;

        /* renamed from: c  reason: collision with root package name */
        int f1809c;

        /* renamed from: d  reason: collision with root package name */
        int f1810d;

        /* renamed from: e  reason: collision with root package name */
        int f1811e;

        /* renamed from: f  reason: collision with root package name */
        int f1812f;

        /* renamed from: g  reason: collision with root package name */
        int f1813g;

        /* renamed from: h  reason: collision with root package name */
        int f1814h = 0;
        boolean i = false;
        int j;
        List<RecyclerView.u> k = null;
        boolean l;

        c() {
        }

        /* access modifiers changed from: package-private */
        public boolean a(RecyclerView.r rVar) {
            return this.f1810d >= 0 && this.f1810d < rVar.e();
        }

        /* access modifiers changed from: package-private */
        public View a(RecyclerView.n nVar) {
            if (this.k != null) {
                return b();
            }
            View c2 = nVar.c(this.f1810d);
            this.f1810d += this.f1811e;
            return c2;
        }

        private View b() {
            int size = this.k.size();
            for (int i2 = 0; i2 < size; i2++) {
                View view = this.k.get(i2).itemView;
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                if (!layoutParams.d() && this.f1810d == layoutParams.f()) {
                    a(view);
                    return view;
                }
            }
            return null;
        }

        public void a() {
            a((View) null);
        }

        public void a(View view) {
            View b2 = b(view);
            if (b2 == null) {
                this.f1810d = -1;
            } else {
                this.f1810d = ((RecyclerView.LayoutParams) b2.getLayoutParams()).f();
            }
        }

        public View b(View view) {
            int i2;
            View view2;
            int size = this.k.size();
            View view3 = null;
            int i3 = Integer.MAX_VALUE;
            int i4 = 0;
            while (i4 < size) {
                View view4 = this.k.get(i4).itemView;
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view4.getLayoutParams();
                if (view4 != view) {
                    if (layoutParams.d()) {
                        i2 = i3;
                        view2 = view3;
                    } else {
                        i2 = (layoutParams.f() - this.f1810d) * this.f1811e;
                        if (i2 < 0) {
                            i2 = i3;
                            view2 = view3;
                        } else if (i2 < i3) {
                            if (i2 == 0) {
                                return view4;
                            }
                            view2 = view4;
                        }
                    }
                    i4++;
                    view3 = view2;
                    i3 = i2;
                }
                i2 = i3;
                view2 = view3;
                i4++;
                view3 = view2;
                i3 = i2;
            }
            return view3;
        }
    }

    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f1795a;

        /* renamed from: b  reason: collision with root package name */
        int f1796b;

        /* renamed from: c  reason: collision with root package name */
        boolean f1797c;

        public SavedState() {
        }

        SavedState(Parcel parcel) {
            boolean z = true;
            this.f1795a = parcel.readInt();
            this.f1796b = parcel.readInt();
            this.f1797c = parcel.readInt() != 1 ? false : z;
        }

        public SavedState(SavedState savedState) {
            this.f1795a = savedState.f1795a;
            this.f1796b = savedState.f1796b;
            this.f1797c = savedState.f1797c;
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.f1795a >= 0;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f1795a = -1;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f1795a);
            parcel.writeInt(this.f1796b);
            parcel.writeInt(this.f1797c ? 1 : 0);
        }
    }

    class a {

        /* renamed from: a  reason: collision with root package name */
        int f1798a;

        /* renamed from: b  reason: collision with root package name */
        int f1799b;

        /* renamed from: c  reason: collision with root package name */
        boolean f1800c;

        /* renamed from: d  reason: collision with root package name */
        boolean f1801d;

        a() {
            a();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f1798a = -1;
            this.f1799b = Integer.MIN_VALUE;
            this.f1800c = false;
            this.f1801d = false;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            int c2;
            if (this.f1800c) {
                c2 = LinearLayoutManager.this.j.d();
            } else {
                c2 = LinearLayoutManager.this.j.c();
            }
            this.f1799b = c2;
        }

        public String toString() {
            return "AnchorInfo{mPosition=" + this.f1798a + ", mCoordinate=" + this.f1799b + ", mLayoutFromEnd=" + this.f1800c + ", mValid=" + this.f1801d + '}';
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view, RecyclerView.r rVar) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return !layoutParams.d() && layoutParams.f() >= 0 && layoutParams.f() < rVar.e();
        }

        public void a(View view) {
            int b2 = LinearLayoutManager.this.j.b();
            if (b2 >= 0) {
                b(view);
                return;
            }
            this.f1798a = LinearLayoutManager.this.d(view);
            if (this.f1800c) {
                int d2 = (LinearLayoutManager.this.j.d() - b2) - LinearLayoutManager.this.j.b(view);
                this.f1799b = LinearLayoutManager.this.j.d() - d2;
                if (d2 > 0) {
                    int e2 = this.f1799b - LinearLayoutManager.this.j.e(view);
                    int c2 = LinearLayoutManager.this.j.c();
                    int min = e2 - (c2 + Math.min(LinearLayoutManager.this.j.a(view) - c2, 0));
                    if (min < 0) {
                        this.f1799b = Math.min(d2, -min) + this.f1799b;
                        return;
                    }
                    return;
                }
                return;
            }
            int a2 = LinearLayoutManager.this.j.a(view);
            int c3 = a2 - LinearLayoutManager.this.j.c();
            this.f1799b = a2;
            if (c3 > 0) {
                int d3 = (LinearLayoutManager.this.j.d() - Math.min(0, (LinearLayoutManager.this.j.d() - b2) - LinearLayoutManager.this.j.b(view))) - (a2 + LinearLayoutManager.this.j.e(view));
                if (d3 < 0) {
                    this.f1799b -= Math.min(c3, -d3);
                }
            }
        }

        public void b(View view) {
            if (this.f1800c) {
                this.f1799b = LinearLayoutManager.this.j.b(view) + LinearLayoutManager.this.j.b();
            } else {
                this.f1799b = LinearLayoutManager.this.j.a(view);
            }
            this.f1798a = LinearLayoutManager.this.d(view);
        }
    }

    protected static class b {

        /* renamed from: a  reason: collision with root package name */
        public int f1803a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f1804b;

        /* renamed from: c  reason: collision with root package name */
        public boolean f1805c;

        /* renamed from: d  reason: collision with root package name */
        public boolean f1806d;

        protected b() {
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f1803a = 0;
            this.f1804b = false;
            this.f1805c = false;
            this.f1806d = false;
        }
    }
}
