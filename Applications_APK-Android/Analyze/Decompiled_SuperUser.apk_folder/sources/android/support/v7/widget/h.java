package android.support.v7.widget;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.v7.a.a;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.ImageView;

/* compiled from: AppCompatImageHelper */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private final ImageView f2209a;

    public h(ImageView imageView) {
        this.f2209a = imageView;
    }

    public void a(AttributeSet attributeSet, int i) {
        int g2;
        an anVar = null;
        try {
            Drawable drawable = this.f2209a.getDrawable();
            if (!(drawable != null || (g2 = (anVar = an.a(this.f2209a.getContext(), attributeSet, a.k.AppCompatImageView, i, 0)).g(a.k.AppCompatImageView_srcCompat, -1)) == -1 || (drawable = b.b(this.f2209a.getContext(), g2)) == null)) {
                this.f2209a.setImageDrawable(drawable);
            }
            if (drawable != null) {
                u.b(drawable);
            }
        } finally {
            if (anVar != null) {
                anVar.a();
            }
        }
    }

    public void a(int i) {
        if (i != 0) {
            Drawable b2 = b.b(this.f2209a.getContext(), i);
            if (b2 != null) {
                u.b(b2);
            }
            this.f2209a.setImageDrawable(b2);
            return;
        }
        this.f2209a.setImageDrawable(null);
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        Drawable background = this.f2209a.getBackground();
        if (Build.VERSION.SDK_INT < 21 || !(background instanceof RippleDrawable)) {
            return true;
        }
        return false;
    }
}
