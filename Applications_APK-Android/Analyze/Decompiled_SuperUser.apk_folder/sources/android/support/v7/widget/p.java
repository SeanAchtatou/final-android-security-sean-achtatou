package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;

@TargetApi(9)
/* compiled from: CardViewImpl */
interface p {
    float a(n nVar);

    void a();

    void a(n nVar, float f2);

    void a(n nVar, Context context, ColorStateList colorStateList, float f2, float f3, float f4);

    void a(n nVar, ColorStateList colorStateList);

    float b(n nVar);

    void b(n nVar, float f2);

    float c(n nVar);

    void c(n nVar, float f2);

    float d(n nVar);

    float e(n nVar);

    void g(n nVar);

    void h(n nVar);

    ColorStateList i(n nVar);
}
