package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.v7.b.a;

@TargetApi(9)
/* compiled from: RoundRectDrawableWithShadow */
class af extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    static final double f2105a = Math.cos(Math.toRadians(45.0d));

    /* renamed from: c  reason: collision with root package name */
    static a f2106c;

    /* renamed from: b  reason: collision with root package name */
    final int f2107b;

    /* renamed from: d  reason: collision with root package name */
    Paint f2108d;

    /* renamed from: e  reason: collision with root package name */
    Paint f2109e;

    /* renamed from: f  reason: collision with root package name */
    Paint f2110f;

    /* renamed from: g  reason: collision with root package name */
    final RectF f2111g;

    /* renamed from: h  reason: collision with root package name */
    float f2112h;
    Path i;
    float j;
    float k;
    float l;
    float m;
    private ColorStateList n;
    private boolean o = true;
    private final int p;
    private final int q;
    private boolean r = true;
    private boolean s = false;

    /* compiled from: RoundRectDrawableWithShadow */
    interface a {
        void a(Canvas canvas, RectF rectF, float f2, Paint paint);
    }

    af(Resources resources, ColorStateList colorStateList, float f2, float f3, float f4) {
        this.p = resources.getColor(a.C0028a.af);
        this.q = resources.getColor(a.C0028a.ae);
        this.f2107b = resources.getDimensionPixelSize(a.b.d5);
        this.f2108d = new Paint(5);
        b(colorStateList);
        this.f2109e = new Paint(5);
        this.f2109e.setStyle(Paint.Style.FILL);
        this.f2112h = (float) ((int) (0.5f + f2));
        this.f2111g = new RectF();
        this.f2110f = new Paint(this.f2109e);
        this.f2110f.setAntiAlias(false);
        a(f3, f4);
    }

    private void b(ColorStateList colorStateList) {
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        this.n = colorStateList;
        this.f2108d.setColor(this.n.getColorForState(getState(), this.n.getDefaultColor()));
    }

    private int d(float f2) {
        int i2 = (int) (0.5f + f2);
        if (i2 % 2 == 1) {
            return i2 - 1;
        }
        return i2;
    }

    public void a(boolean z) {
        this.r = z;
        invalidateSelf();
    }

    public void setAlpha(int i2) {
        this.f2108d.setAlpha(i2);
        this.f2109e.setAlpha(i2);
        this.f2110f.setAlpha(i2);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.o = true;
    }

    /* access modifiers changed from: package-private */
    public void a(float f2, float f3) {
        if (f2 < 0.0f) {
            throw new IllegalArgumentException("Invalid shadow size " + f2 + ". Must be >= 0");
        } else if (f3 < 0.0f) {
            throw new IllegalArgumentException("Invalid max shadow size " + f3 + ". Must be >= 0");
        } else {
            float d2 = (float) d(f2);
            float d3 = (float) d(f3);
            if (d2 > d3) {
                if (!this.s) {
                    this.s = true;
                }
                d2 = d3;
            }
            if (this.m != d2 || this.k != d3) {
                this.m = d2;
                this.k = d3;
                this.l = (float) ((int) ((d2 * 1.5f) + ((float) this.f2107b) + 0.5f));
                this.j = ((float) this.f2107b) + d3;
                this.o = true;
                invalidateSelf();
            }
        }
    }

    public boolean getPadding(Rect rect) {
        int ceil = (int) Math.ceil((double) a(this.k, this.f2112h, this.r));
        int ceil2 = (int) Math.ceil((double) b(this.k, this.f2112h, this.r));
        rect.set(ceil2, ceil, ceil2, ceil);
        return true;
    }

    static float a(float f2, float f3, boolean z) {
        if (z) {
            return (float) (((double) (1.5f * f2)) + ((1.0d - f2105a) * ((double) f3)));
        }
        return 1.5f * f2;
    }

    static float b(float f2, float f3, boolean z) {
        if (z) {
            return (float) (((double) f2) + ((1.0d - f2105a) * ((double) f3)));
        }
        return f2;
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        int colorForState = this.n.getColorForState(iArr, this.n.getDefaultColor());
        if (this.f2108d.getColor() == colorForState) {
            return false;
        }
        this.f2108d.setColor(colorForState);
        this.o = true;
        invalidateSelf();
        return true;
    }

    public boolean isStateful() {
        return (this.n != null && this.n.isStateful()) || super.isStateful();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f2108d.setColorFilter(colorFilter);
    }

    public int getOpacity() {
        return -3;
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        if (f2 < 0.0f) {
            throw new IllegalArgumentException("Invalid radius " + f2 + ". Must be >= 0");
        }
        float f3 = (float) ((int) (0.5f + f2));
        if (this.f2112h != f3) {
            this.f2112h = f3;
            this.o = true;
            invalidateSelf();
        }
    }

    public void draw(Canvas canvas) {
        if (this.o) {
            b(getBounds());
            this.o = false;
        }
        canvas.translate(0.0f, this.m / 2.0f);
        a(canvas);
        canvas.translate(0.0f, (-this.m) / 2.0f);
        f2106c.a(canvas, this.f2111g, this.f2112h, this.f2108d);
    }

    private void a(Canvas canvas) {
        boolean z;
        float f2 = (-this.f2112h) - this.l;
        float f3 = this.f2112h + ((float) this.f2107b) + (this.m / 2.0f);
        boolean z2 = this.f2111g.width() - (2.0f * f3) > 0.0f;
        if (this.f2111g.height() - (2.0f * f3) > 0.0f) {
            z = true;
        } else {
            z = false;
        }
        int save = canvas.save();
        canvas.translate(this.f2111g.left + f3, this.f2111g.top + f3);
        canvas.drawPath(this.i, this.f2109e);
        if (z2) {
            canvas.drawRect(0.0f, f2, this.f2111g.width() - (2.0f * f3), -this.f2112h, this.f2110f);
        }
        canvas.restoreToCount(save);
        int save2 = canvas.save();
        canvas.translate(this.f2111g.right - f3, this.f2111g.bottom - f3);
        canvas.rotate(180.0f);
        canvas.drawPath(this.i, this.f2109e);
        if (z2) {
            canvas.drawRect(0.0f, f2, this.f2111g.width() - (2.0f * f3), this.l + (-this.f2112h), this.f2110f);
        }
        canvas.restoreToCount(save2);
        int save3 = canvas.save();
        canvas.translate(this.f2111g.left + f3, this.f2111g.bottom - f3);
        canvas.rotate(270.0f);
        canvas.drawPath(this.i, this.f2109e);
        if (z) {
            canvas.drawRect(0.0f, f2, this.f2111g.height() - (2.0f * f3), -this.f2112h, this.f2110f);
        }
        canvas.restoreToCount(save3);
        int save4 = canvas.save();
        canvas.translate(this.f2111g.right - f3, this.f2111g.top + f3);
        canvas.rotate(90.0f);
        canvas.drawPath(this.i, this.f2109e);
        if (z) {
            canvas.drawRect(0.0f, f2, this.f2111g.height() - (2.0f * f3), -this.f2112h, this.f2110f);
        }
        canvas.restoreToCount(save4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    private void g() {
        RectF rectF = new RectF(-this.f2112h, -this.f2112h, this.f2112h, this.f2112h);
        RectF rectF2 = new RectF(rectF);
        rectF2.inset(-this.l, -this.l);
        if (this.i == null) {
            this.i = new Path();
        } else {
            this.i.reset();
        }
        this.i.setFillType(Path.FillType.EVEN_ODD);
        this.i.moveTo(-this.f2112h, 0.0f);
        this.i.rLineTo(-this.l, 0.0f);
        this.i.arcTo(rectF2, 180.0f, 90.0f, false);
        this.i.arcTo(rectF, 270.0f, -90.0f, false);
        this.i.close();
        this.f2109e.setShader(new RadialGradient(0.0f, 0.0f, this.f2112h + this.l, new int[]{this.p, this.p, this.q}, new float[]{0.0f, this.f2112h / (this.f2112h + this.l), 1.0f}, Shader.TileMode.CLAMP));
        this.f2110f.setShader(new LinearGradient(0.0f, (-this.f2112h) + this.l, 0.0f, (-this.f2112h) - this.l, new int[]{this.p, this.p, this.q}, new float[]{0.0f, 0.5f, 1.0f}, Shader.TileMode.CLAMP));
        this.f2110f.setAntiAlias(false);
    }

    private void b(Rect rect) {
        float f2 = this.k * 1.5f;
        this.f2111g.set(((float) rect.left) + this.k, ((float) rect.top) + f2, ((float) rect.right) - this.k, ((float) rect.bottom) - f2);
        g();
    }

    /* access modifiers changed from: package-private */
    public float a() {
        return this.f2112h;
    }

    /* access modifiers changed from: package-private */
    public void a(Rect rect) {
        getPadding(rect);
    }

    /* access modifiers changed from: package-private */
    public void b(float f2) {
        a(f2, this.k);
    }

    /* access modifiers changed from: package-private */
    public void c(float f2) {
        a(this.m, f2);
    }

    /* access modifiers changed from: package-private */
    public float b() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public float c() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public float d() {
        return (Math.max(this.k, this.f2112h + ((float) this.f2107b) + (this.k / 2.0f)) * 2.0f) + ((this.k + ((float) this.f2107b)) * 2.0f);
    }

    /* access modifiers changed from: package-private */
    public float e() {
        return (Math.max(this.k, this.f2112h + ((float) this.f2107b) + ((this.k * 1.5f) / 2.0f)) * 2.0f) + (((this.k * 1.5f) + ((float) this.f2107b)) * 2.0f);
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        b(colorStateList);
        invalidateSelf();
    }

    /* access modifiers changed from: package-private */
    public ColorStateList f() {
        return this.n;
    }
}
