package android.support.v7.widget.helper;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/* compiled from: ItemTouchUIUtil */
public interface a {
    void a(Canvas canvas, RecyclerView recyclerView, View view, float f2, float f3, int i, boolean z);

    void a(View view);

    void b(Canvas canvas, RecyclerView recyclerView, View view, float f2, float f3, int i, boolean z);

    void b(View view);
}
