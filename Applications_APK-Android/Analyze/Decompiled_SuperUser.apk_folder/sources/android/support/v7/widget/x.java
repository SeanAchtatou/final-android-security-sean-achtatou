package android.support.v7.widget;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.view.ag;
import android.support.v4.view.s;
import android.support.v7.view.menu.m;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.ViewTreeObserver;

/* compiled from: ForwardingListener */
public abstract class x implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private final float f2269a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2270b;

    /* renamed from: c  reason: collision with root package name */
    final View f2271c;

    /* renamed from: d  reason: collision with root package name */
    private final int f2272d;

    /* renamed from: e  reason: collision with root package name */
    private Runnable f2273e;

    /* renamed from: f  reason: collision with root package name */
    private Runnable f2274f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f2275g;

    /* renamed from: h  reason: collision with root package name */
    private int f2276h;
    private final int[] i = new int[2];

    public abstract m a();

    public x(View view) {
        this.f2271c = view;
        view.setLongClickable(true);
        if (Build.VERSION.SDK_INT >= 12) {
            a(view);
        } else {
            b(view);
        }
        this.f2269a = (float) ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        this.f2270b = ViewConfiguration.getTapTimeout();
        this.f2272d = (this.f2270b + ViewConfiguration.getLongPressTimeout()) / 2;
    }

    @TargetApi(12)
    private void a(View view) {
        view.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            public void onViewAttachedToWindow(View view) {
            }

            public void onViewDetachedFromWindow(View view) {
                x.this.e();
            }
        });
    }

    private void b(View view) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            /* renamed from: a  reason: collision with root package name */
            boolean f2278a = ag.H(x.this.f2271c);

            public void onGlobalLayout() {
                boolean z = this.f2278a;
                this.f2278a = ag.H(x.this.f2271c);
                if (z && !this.f2278a) {
                    x.this.e();
                }
            }
        });
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z;
        boolean z2 = this.f2275g;
        if (!z2) {
            boolean z3 = a(motionEvent) && b();
            if (z3) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                this.f2271c.onTouchEvent(obtain);
                obtain.recycle();
            }
            z = z3;
        } else if (b(motionEvent) || !c()) {
            z = true;
        } else {
            z = false;
        }
        this.f2275g = z;
        if (z || z2) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void e() {
        this.f2275g = false;
        this.f2276h = -1;
        if (this.f2273e != null) {
            this.f2271c.removeCallbacks(this.f2273e);
        }
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        m a2 = a();
        if (a2 == null || a2.c()) {
            return true;
        }
        a2.a();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        m a2 = a();
        if (a2 == null || !a2.c()) {
            return true;
        }
        a2.b();
        return true;
    }

    private boolean a(MotionEvent motionEvent) {
        View view = this.f2271c;
        if (!view.isEnabled()) {
            return false;
        }
        switch (s.a(motionEvent)) {
            case 0:
                this.f2276h = motionEvent.getPointerId(0);
                if (this.f2273e == null) {
                    this.f2273e = new a();
                }
                view.postDelayed(this.f2273e, (long) this.f2270b);
                if (this.f2274f == null) {
                    this.f2274f = new b();
                }
                view.postDelayed(this.f2274f, (long) this.f2272d);
                return false;
            case 1:
            case 3:
                f();
                return false;
            case 2:
                int findPointerIndex = motionEvent.findPointerIndex(this.f2276h);
                if (findPointerIndex < 0 || a(view, motionEvent.getX(findPointerIndex), motionEvent.getY(findPointerIndex), this.f2269a)) {
                    return false;
                }
                f();
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return true;
            default:
                return false;
        }
    }

    private void f() {
        if (this.f2274f != null) {
            this.f2271c.removeCallbacks(this.f2274f);
        }
        if (this.f2273e != null) {
            this.f2271c.removeCallbacks(this.f2273e);
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        f();
        View view = this.f2271c;
        if (view.isEnabled() && !view.isLongClickable() && b()) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            view.onTouchEvent(obtain);
            obtain.recycle();
            this.f2275g = true;
        }
    }

    private boolean b(MotionEvent motionEvent) {
        v vVar;
        boolean z;
        boolean z2;
        View view = this.f2271c;
        m a2 = a();
        if (a2 == null || !a2.c() || (vVar = (v) a2.d()) == null || !vVar.isShown()) {
            return false;
        }
        MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
        b(view, obtainNoHistory);
        a(vVar, obtainNoHistory);
        boolean a3 = vVar.a(obtainNoHistory, this.f2276h);
        obtainNoHistory.recycle();
        int a4 = s.a(motionEvent);
        if (a4 == 1 || a4 == 3) {
            z = false;
        } else {
            z = true;
        }
        if (!a3 || !z) {
            z2 = false;
        } else {
            z2 = true;
        }
        return z2;
    }

    private static boolean a(View view, float f2, float f3, float f4) {
        return f2 >= (-f4) && f3 >= (-f4) && f2 < ((float) (view.getRight() - view.getLeft())) + f4 && f3 < ((float) (view.getBottom() - view.getTop())) + f4;
    }

    private boolean a(View view, MotionEvent motionEvent) {
        int[] iArr = this.i;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) (-iArr[0]), (float) (-iArr[1]));
        return true;
    }

    private boolean b(View view, MotionEvent motionEvent) {
        int[] iArr = this.i;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
        return true;
    }

    /* compiled from: ForwardingListener */
    private class a implements Runnable {
        a() {
        }

        public void run() {
            ViewParent parent = x.this.f2271c.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    }

    /* compiled from: ForwardingListener */
    private class b implements Runnable {
        b() {
        }

        public void run() {
            x.this.d();
        }
    }
}
