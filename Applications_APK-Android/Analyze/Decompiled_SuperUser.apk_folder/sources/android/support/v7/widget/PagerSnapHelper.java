package android.support.v7.widget;

import android.graphics.PointF;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;

public class PagerSnapHelper extends SnapHelper {

    /* renamed from: b  reason: collision with root package name */
    private ac f1851b;

    /* renamed from: c  reason: collision with root package name */
    private ac f1852c;

    public int[] a(RecyclerView.h hVar, View view) {
        int[] iArr = new int[2];
        if (hVar.d()) {
            iArr[0] = a(hVar, view, d(hVar));
        } else {
            iArr[0] = 0;
        }
        if (hVar.e()) {
            iArr[1] = a(hVar, view, c(hVar));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    public View a(RecyclerView.h hVar) {
        if (hVar.e()) {
            return a(hVar, c(hVar));
        }
        if (hVar.d()) {
            return a(hVar, d(hVar));
        }
        return null;
    }

    public int a(RecyclerView.h hVar, int i, int i2) {
        int d2;
        boolean z;
        PointF d3;
        boolean z2 = false;
        int E = hVar.E();
        if (E == 0) {
            return -1;
        }
        View view = null;
        if (hVar.e()) {
            view = b(hVar, c(hVar));
        } else if (hVar.d()) {
            view = b(hVar, d(hVar));
        }
        if (view == null || (d2 = hVar.d(view)) == -1) {
            return -1;
        }
        if (hVar.d()) {
            z = i > 0;
        } else {
            z = i2 > 0;
        }
        if ((hVar instanceof RecyclerView.q.b) && (d3 = ((RecyclerView.q.b) hVar).d(E - 1)) != null && (d3.x < 0.0f || d3.y < 0.0f)) {
            z2 = true;
        }
        if (!z2) {
            return z ? d2 + 1 : d2;
        }
        if (z) {
            return d2 - 1;
        }
        return d2;
    }

    /* access modifiers changed from: protected */
    public LinearSmoothScroller b(RecyclerView.h hVar) {
        if (!(hVar instanceof RecyclerView.q.b)) {
            return null;
        }
        return new LinearSmoothScroller(this.f1996a.getContext()) {
            /* access modifiers changed from: protected */
            public void a(View view, RecyclerView.r rVar, RecyclerView.q.a aVar) {
                int[] a2 = PagerSnapHelper.this.a(PagerSnapHelper.this.f1996a.getLayoutManager(), view);
                int i = a2[0];
                int i2 = a2[1];
                int a3 = a(Math.max(Math.abs(i), Math.abs(i2)));
                if (a3 > 0) {
                    aVar.a(i, i2, a3, this.f1816b);
                }
            }

            /* access modifiers changed from: protected */
            public float a(DisplayMetrics displayMetrics) {
                return 100.0f / ((float) displayMetrics.densityDpi);
            }

            /* access modifiers changed from: protected */
            public int b(int i) {
                return Math.min(100, super.b(i));
            }
        };
    }

    private int a(RecyclerView.h hVar, View view, ac acVar) {
        int e2;
        int e3 = (acVar.e(view) / 2) + acVar.a(view);
        if (hVar.q()) {
            e2 = acVar.c() + (acVar.f() / 2);
        } else {
            e2 = acVar.e() / 2;
        }
        return e3 - e2;
    }

    private View a(RecyclerView.h hVar, ac acVar) {
        int e2;
        View view;
        View view2 = null;
        int u = hVar.u();
        if (u != 0) {
            if (hVar.q()) {
                e2 = acVar.c() + (acVar.f() / 2);
            } else {
                e2 = acVar.e() / 2;
            }
            int i = Integer.MAX_VALUE;
            int i2 = 0;
            while (i2 < u) {
                View i3 = hVar.i(i2);
                int abs = Math.abs((acVar.a(i3) + (acVar.e(i3) / 2)) - e2);
                if (abs < i) {
                    view = i3;
                } else {
                    abs = i;
                    view = view2;
                }
                i2++;
                view2 = view;
                i = abs;
            }
        }
        return view2;
    }

    private View b(RecyclerView.h hVar, ac acVar) {
        View view;
        View view2 = null;
        int u = hVar.u();
        if (u != 0) {
            int i = Integer.MAX_VALUE;
            int i2 = 0;
            while (i2 < u) {
                View i3 = hVar.i(i2);
                int a2 = acVar.a(i3);
                if (a2 < i) {
                    view = i3;
                } else {
                    a2 = i;
                    view = view2;
                }
                i2++;
                view2 = view;
                i = a2;
            }
        }
        return view2;
    }

    private ac c(RecyclerView.h hVar) {
        if (this.f1851b == null || this.f1851b.f2093a != hVar) {
            this.f1851b = ac.b(hVar);
        }
        return this.f1851b;
    }

    private ac d(RecyclerView.h hVar) {
        if (this.f1852c == null || this.f1852c.f2093a != hVar) {
            this.f1852c = ac.a(hVar);
        }
        return this.f1852c;
    }
}
