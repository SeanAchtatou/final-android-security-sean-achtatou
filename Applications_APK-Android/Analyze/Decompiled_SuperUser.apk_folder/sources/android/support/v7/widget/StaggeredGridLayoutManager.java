package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.a.e;
import android.support.v4.view.a.q;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class StaggeredGridLayoutManager extends RecyclerView.h implements RecyclerView.q.b {
    private SavedState A;
    private int B;
    private final Rect C = new Rect();
    private final a D = new a();
    private boolean E = false;
    private boolean F = true;
    private int[] G;
    private final Runnable H = new Runnable() {
        public void run() {
            StaggeredGridLayoutManager.this.f();
        }
    };

    /* renamed from: a  reason: collision with root package name */
    b[] f2002a;

    /* renamed from: b  reason: collision with root package name */
    ac f2003b;

    /* renamed from: c  reason: collision with root package name */
    ac f2004c;

    /* renamed from: d  reason: collision with root package name */
    boolean f2005d = false;

    /* renamed from: e  reason: collision with root package name */
    boolean f2006e = false;

    /* renamed from: f  reason: collision with root package name */
    int f2007f = -1;

    /* renamed from: g  reason: collision with root package name */
    int f2008g = Integer.MIN_VALUE;

    /* renamed from: h  reason: collision with root package name */
    LazySpanLookup f2009h = new LazySpanLookup();
    private int i = -1;
    private int j;
    private int k;
    private final z l;
    private BitSet m;
    private int n = 2;
    private boolean o;
    private boolean z;

    public StaggeredGridLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        boolean z2 = true;
        RecyclerView.h.b a2 = a(context, attributeSet, i2, i3);
        b(a2.f1885a);
        a(a2.f1886b);
        a(a2.f1887c);
        c(this.n == 0 ? false : z2);
        this.l = new z();
        N();
    }

    public StaggeredGridLayoutManager(int i2, int i3) {
        boolean z2 = true;
        this.j = i3;
        a(i2);
        c(this.n == 0 ? false : z2);
        this.l = new z();
        N();
    }

    private void N() {
        this.f2003b = ac.a(this, this.j);
        this.f2004c = ac.a(this, 1 - this.j);
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        int L;
        int K;
        if (u() == 0 || this.n == 0 || !p()) {
            return false;
        }
        if (this.f2006e) {
            L = K();
            K = L();
        } else {
            L = L();
            K = K();
        }
        if (L == 0 && g() != null) {
            this.f2009h.a();
            I();
            n();
            return true;
        } else if (!this.E) {
            return false;
        } else {
            int i2 = this.f2006e ? -1 : 1;
            LazySpanLookup.FullSpanItem a2 = this.f2009h.a(L, K + 1, i2, true);
            if (a2 == null) {
                this.E = false;
                this.f2009h.a(K + 1);
                return false;
            }
            LazySpanLookup.FullSpanItem a3 = this.f2009h.a(L, a2.f2015a, i2 * -1, true);
            if (a3 == null) {
                this.f2009h.a(a2.f2015a);
            } else {
                this.f2009h.a(a3.f2015a + 1);
            }
            I();
            n();
            return true;
        }
    }

    public void l(int i2) {
        if (i2 == 0) {
            f();
        }
    }

    public void a(RecyclerView recyclerView, RecyclerView.n nVar) {
        a(this.H);
        for (int i2 = 0; i2 < this.i; i2++) {
            this.f2002a[i2].e();
        }
        recyclerView.requestLayout();
    }

    /* access modifiers changed from: package-private */
    public View g() {
        int i2;
        int i3;
        boolean z2;
        boolean z3;
        int u = u() - 1;
        BitSet bitSet = new BitSet(this.i);
        bitSet.set(0, this.i, true);
        char c2 = (this.j != 1 || !i()) ? (char) 65535 : 1;
        if (this.f2006e) {
            i2 = -1;
        } else {
            i2 = u + 1;
            u = 0;
        }
        if (u < i2) {
            i3 = 1;
        } else {
            i3 = -1;
        }
        for (int i4 = u; i4 != i2; i4 += i3) {
            View i5 = i(i4);
            LayoutParams layoutParams = (LayoutParams) i5.getLayoutParams();
            if (bitSet.get(layoutParams.f2011a.f2038e)) {
                if (a(layoutParams.f2011a)) {
                    return i5;
                }
                bitSet.clear(layoutParams.f2011a.f2038e);
            }
            if (!layoutParams.f2012b && i4 + i3 != i2) {
                View i6 = i(i4 + i3);
                if (this.f2006e) {
                    int b2 = this.f2003b.b(i5);
                    int b3 = this.f2003b.b(i6);
                    if (b2 < b3) {
                        return i5;
                    }
                    if (b2 == b3) {
                        z2 = true;
                    }
                    z2 = false;
                } else {
                    int a2 = this.f2003b.a(i5);
                    int a3 = this.f2003b.a(i6);
                    if (a2 > a3) {
                        return i5;
                    }
                    if (a2 == a3) {
                        z2 = true;
                    }
                    z2 = false;
                }
                if (!z2) {
                    continue;
                } else {
                    if (layoutParams.f2011a.f2038e - ((LayoutParams) i6.getLayoutParams()).f2011a.f2038e < 0) {
                        z3 = true;
                    } else {
                        z3 = false;
                    }
                    if (z3 != (c2 < 0)) {
                        return i5;
                    }
                }
            }
        }
        return null;
    }

    private boolean a(b bVar) {
        boolean z2 = true;
        if (this.f2006e) {
            if (bVar.d() < this.f2003b.d()) {
                return !bVar.c(bVar.f2034a.get(bVar.f2034a.size() + -1)).f2012b;
            }
        } else if (bVar.b() > this.f2003b.c()) {
            if (bVar.c(bVar.f2034a.get(0)).f2012b) {
                z2 = false;
            }
            return z2;
        }
        return false;
    }

    public void a(int i2) {
        a((String) null);
        if (i2 != this.i) {
            h();
            this.i = i2;
            this.m = new BitSet(this.i);
            this.f2002a = new b[this.i];
            for (int i3 = 0; i3 < this.i; i3++) {
                this.f2002a[i3] = new b(i3);
            }
            n();
        }
    }

    public void b(int i2) {
        if (i2 == 0 || i2 == 1) {
            a((String) null);
            if (i2 != this.j) {
                this.j = i2;
                ac acVar = this.f2003b;
                this.f2003b = this.f2004c;
                this.f2004c = acVar;
                n();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("invalid orientation.");
    }

    public void a(boolean z2) {
        a((String) null);
        if (!(this.A == null || this.A.f2026h == z2)) {
            this.A.f2026h = z2;
        }
        this.f2005d = z2;
        n();
    }

    public void a(String str) {
        if (this.A == null) {
            super.a(str);
        }
    }

    public void h() {
        this.f2009h.a();
        n();
    }

    private void O() {
        boolean z2 = true;
        if (this.j == 1 || !i()) {
            this.f2006e = this.f2005d;
            return;
        }
        if (this.f2005d) {
            z2 = false;
        }
        this.f2006e = z2;
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        return s() == 1;
    }

    public void a(Rect rect, int i2, int i3) {
        int a2;
        int a3;
        int B2 = B() + z();
        int A2 = A() + C();
        if (this.j == 1) {
            a3 = a(i3, A2 + rect.height(), G());
            a2 = a(i2, B2 + (this.k * this.i), F());
        } else {
            a2 = a(i2, B2 + rect.width(), F());
            a3 = a(i3, A2 + (this.k * this.i), G());
        }
        g(a2, a3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void
     arg types: [android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.z, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.StaggeredGridLayoutManager$b, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$LayoutParams, android.support.v7.widget.z):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$LayoutParams, boolean):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.graphics.Rect, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
      android.support.v7.widget.RecyclerView.h.a(int, int, int):int
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void */
    public void c(RecyclerView.n nVar, RecyclerView.r rVar) {
        a(nVar, rVar, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.b(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void
     arg types: [android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.b(int, int, int):int
      android.support.v7.widget.StaggeredGridLayoutManager.b(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.StaggeredGridLayoutManager.b(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.RecyclerView.h.b(int, int, int):boolean
      android.support.v7.widget.RecyclerView.h.b(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.b(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.b(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.c(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void
     arg types: [android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.c(int, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.c(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.c(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.c(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void
     arg types: [android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.z, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.StaggeredGridLayoutManager$b, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$LayoutParams, android.support.v7.widget.z):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$LayoutParams, boolean):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.graphics.Rect, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
      android.support.v7.widget.RecyclerView.h.a(int, int, int):int
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:97:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.support.v7.widget.RecyclerView.n r9, android.support.v7.widget.RecyclerView.r r10, boolean r11) {
        /*
            r8 = this;
            r7 = -1
            r2 = 1
            r1 = 0
            android.support.v7.widget.StaggeredGridLayoutManager$a r3 = r8.D
            android.support.v7.widget.StaggeredGridLayoutManager$SavedState r0 = r8.A
            if (r0 != 0) goto L_0x000d
            int r0 = r8.f2007f
            if (r0 == r7) goto L_0x001a
        L_0x000d:
            int r0 = r10.e()
            if (r0 != 0) goto L_0x001a
            r8.c(r9)
            r3.a()
        L_0x0019:
            return
        L_0x001a:
            boolean r0 = r3.f2031e
            if (r0 == 0) goto L_0x0026
            int r0 = r8.f2007f
            if (r0 != r7) goto L_0x0026
            android.support.v7.widget.StaggeredGridLayoutManager$SavedState r0 = r8.A
            if (r0 == 0) goto L_0x0087
        L_0x0026:
            r0 = r2
        L_0x0027:
            if (r0 == 0) goto L_0x0038
            r3.a()
            android.support.v7.widget.StaggeredGridLayoutManager$SavedState r4 = r8.A
            if (r4 == 0) goto L_0x0089
            r8.a(r3)
        L_0x0033:
            r8.a(r10, r3)
            r3.f2031e = r2
        L_0x0038:
            android.support.v7.widget.StaggeredGridLayoutManager$SavedState r4 = r8.A
            if (r4 != 0) goto L_0x0055
            int r4 = r8.f2007f
            if (r4 != r7) goto L_0x0055
            boolean r4 = r3.f2029c
            boolean r5 = r8.o
            if (r4 != r5) goto L_0x004e
            boolean r4 = r8.i()
            boolean r5 = r8.z
            if (r4 == r5) goto L_0x0055
        L_0x004e:
            android.support.v7.widget.StaggeredGridLayoutManager$LazySpanLookup r4 = r8.f2009h
            r4.a()
            r3.f2030d = r2
        L_0x0055:
            int r4 = r8.u()
            if (r4 <= 0) goto L_0x00b3
            android.support.v7.widget.StaggeredGridLayoutManager$SavedState r4 = r8.A
            if (r4 == 0) goto L_0x0065
            android.support.v7.widget.StaggeredGridLayoutManager$SavedState r4 = r8.A
            int r4 = r4.f2021c
            if (r4 >= r2) goto L_0x00b3
        L_0x0065:
            boolean r4 = r3.f2030d
            if (r4 == 0) goto L_0x0091
            r0 = r1
        L_0x006a:
            int r4 = r8.i
            if (r0 >= r4) goto L_0x00b3
            android.support.v7.widget.StaggeredGridLayoutManager$b[] r4 = r8.f2002a
            r4 = r4[r0]
            r4.e()
            int r4 = r3.f2028b
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r4 == r5) goto L_0x0084
            android.support.v7.widget.StaggeredGridLayoutManager$b[] r4 = r8.f2002a
            r4 = r4[r0]
            int r5 = r3.f2028b
            r4.c(r5)
        L_0x0084:
            int r0 = r0 + 1
            goto L_0x006a
        L_0x0087:
            r0 = r1
            goto L_0x0027
        L_0x0089:
            r8.O()
            boolean r4 = r8.f2006e
            r3.f2029c = r4
            goto L_0x0033
        L_0x0091:
            if (r0 != 0) goto L_0x0099
            android.support.v7.widget.StaggeredGridLayoutManager$a r0 = r8.D
            int[] r0 = r0.f2032f
            if (r0 != 0) goto L_0x0148
        L_0x0099:
            r0 = r1
        L_0x009a:
            int r4 = r8.i
            if (r0 >= r4) goto L_0x00ac
            android.support.v7.widget.StaggeredGridLayoutManager$b[] r4 = r8.f2002a
            r4 = r4[r0]
            boolean r5 = r8.f2006e
            int r6 = r3.f2028b
            r4.a(r5, r6)
            int r0 = r0 + 1
            goto L_0x009a
        L_0x00ac:
            android.support.v7.widget.StaggeredGridLayoutManager$a r0 = r8.D
            android.support.v7.widget.StaggeredGridLayoutManager$b[] r4 = r8.f2002a
            r0.a(r4)
        L_0x00b3:
            r8.a(r9)
            android.support.v7.widget.z r0 = r8.l
            r0.f2297a = r1
            r8.E = r1
            android.support.v7.widget.ac r0 = r8.f2004c
            int r0 = r0.f()
            r8.f(r0)
            int r0 = r3.f2027a
            r8.b(r0, r10)
            boolean r0 = r3.f2029c
            if (r0 == 0) goto L_0x0160
            r8.m(r7)
            android.support.v7.widget.z r0 = r8.l
            r8.a(r9, r0, r10)
            r8.m(r2)
            android.support.v7.widget.z r0 = r8.l
            int r4 = r3.f2027a
            android.support.v7.widget.z r5 = r8.l
            int r5 = r5.f2300d
            int r4 = r4 + r5
            r0.f2299c = r4
            android.support.v7.widget.z r0 = r8.l
            r8.a(r9, r0, r10)
        L_0x00e9:
            r8.P()
            int r0 = r8.u()
            if (r0 <= 0) goto L_0x00fc
            boolean r0 = r8.f2006e
            if (r0 == 0) goto L_0x017d
            r8.b(r9, r10, r2)
            r8.c(r9, r10, r1)
        L_0x00fc:
            if (r11 == 0) goto L_0x0187
            boolean r0 = r10.a()
            if (r0 != 0) goto L_0x0187
            int r0 = r8.n
            if (r0 == 0) goto L_0x0185
            int r0 = r8.u()
            if (r0 <= 0) goto L_0x0185
            boolean r0 = r8.E
            if (r0 != 0) goto L_0x0118
            android.view.View r0 = r8.g()
            if (r0 == 0) goto L_0x0185
        L_0x0118:
            r0 = r2
        L_0x0119:
            if (r0 == 0) goto L_0x0187
            java.lang.Runnable r0 = r8.H
            r8.a(r0)
            boolean r0 = r8.f()
            if (r0 == 0) goto L_0x0187
            r0 = r2
        L_0x0127:
            boolean r2 = r10.a()
            if (r2 == 0) goto L_0x0132
            android.support.v7.widget.StaggeredGridLayoutManager$a r2 = r8.D
            r2.a()
        L_0x0132:
            boolean r2 = r3.f2029c
            r8.o = r2
            boolean r2 = r8.i()
            r8.z = r2
            if (r0 == 0) goto L_0x0019
            android.support.v7.widget.StaggeredGridLayoutManager$a r0 = r8.D
            r0.a()
            r8.a(r9, r10, r1)
            goto L_0x0019
        L_0x0148:
            r0 = r1
        L_0x0149:
            int r4 = r8.i
            if (r0 >= r4) goto L_0x00b3
            android.support.v7.widget.StaggeredGridLayoutManager$b[] r4 = r8.f2002a
            r4 = r4[r0]
            r4.e()
            android.support.v7.widget.StaggeredGridLayoutManager$a r5 = r8.D
            int[] r5 = r5.f2032f
            r5 = r5[r0]
            r4.c(r5)
            int r0 = r0 + 1
            goto L_0x0149
        L_0x0160:
            r8.m(r2)
            android.support.v7.widget.z r0 = r8.l
            r8.a(r9, r0, r10)
            r8.m(r7)
            android.support.v7.widget.z r0 = r8.l
            int r4 = r3.f2027a
            android.support.v7.widget.z r5 = r8.l
            int r5 = r5.f2300d
            int r4 = r4 + r5
            r0.f2299c = r4
            android.support.v7.widget.z r0 = r8.l
            r8.a(r9, r0, r10)
            goto L_0x00e9
        L_0x017d:
            r8.c(r9, r10, r2)
            r8.b(r9, r10, r1)
            goto L_0x00fc
        L_0x0185:
            r0 = r1
            goto L_0x0119
        L_0x0187:
            r0 = r1
            goto L_0x0127
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void");
    }

    public void a(RecyclerView.r rVar) {
        super.a(rVar);
        this.f2007f = -1;
        this.f2008g = Integer.MIN_VALUE;
        this.A = null;
        this.D.a();
    }

    private void P() {
        float f2;
        float max;
        if (this.f2004c.h() != 1073741824) {
            float f3 = 0.0f;
            int u = u();
            int i2 = 0;
            while (i2 < u) {
                View i3 = i(i2);
                float e2 = (float) this.f2004c.e(i3);
                if (e2 < f3) {
                    max = f3;
                } else {
                    if (((LayoutParams) i3.getLayoutParams()).a()) {
                        f2 = (1.0f * e2) / ((float) this.i);
                    } else {
                        f2 = e2;
                    }
                    max = Math.max(f3, f2);
                }
                i2++;
                f3 = max;
            }
            int i4 = this.k;
            int round = Math.round(((float) this.i) * f3);
            if (this.f2004c.h() == Integer.MIN_VALUE) {
                round = Math.min(round, this.f2004c.f());
            }
            f(round);
            if (this.k != i4) {
                for (int i5 = 0; i5 < u; i5++) {
                    View i6 = i(i5);
                    LayoutParams layoutParams = (LayoutParams) i6.getLayoutParams();
                    if (!layoutParams.f2012b) {
                        if (!i() || this.j != 1) {
                            int i7 = layoutParams.f2011a.f2038e * this.k;
                            int i8 = layoutParams.f2011a.f2038e * i4;
                            if (this.j == 1) {
                                i6.offsetLeftAndRight(i7 - i8);
                            } else {
                                i6.offsetTopAndBottom(i7 - i8);
                            }
                        } else {
                            i6.offsetLeftAndRight(((-((this.i - 1) - layoutParams.f2011a.f2038e)) * this.k) - ((-((this.i - 1) - layoutParams.f2011a.f2038e)) * i4));
                        }
                    }
                }
            }
        }
    }

    private void a(a aVar) {
        if (this.A.f2021c > 0) {
            if (this.A.f2021c == this.i) {
                for (int i2 = 0; i2 < this.i; i2++) {
                    this.f2002a[i2].e();
                    int i3 = this.A.f2022d[i2];
                    if (i3 != Integer.MIN_VALUE) {
                        if (this.A.i) {
                            i3 += this.f2003b.d();
                        } else {
                            i3 += this.f2003b.c();
                        }
                    }
                    this.f2002a[i2].c(i3);
                }
            } else {
                this.A.a();
                this.A.f2019a = this.A.f2020b;
            }
        }
        this.z = this.A.j;
        a(this.A.f2026h);
        O();
        if (this.A.f2019a != -1) {
            this.f2007f = this.A.f2019a;
            aVar.f2029c = this.A.i;
        } else {
            aVar.f2029c = this.f2006e;
        }
        if (this.A.f2023e > 1) {
            this.f2009h.f2013a = this.A.f2024f;
            this.f2009h.f2014b = this.A.f2025g;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.r rVar, a aVar) {
        if (!b(rVar, aVar) && !c(rVar, aVar)) {
            aVar.b();
            aVar.f2027a = 0;
        }
    }

    private boolean c(RecyclerView.r rVar, a aVar) {
        int v;
        if (this.o) {
            v = w(rVar.e());
        } else {
            v = v(rVar.e());
        }
        aVar.f2027a = v;
        aVar.f2028b = Integer.MIN_VALUE;
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b(RecyclerView.r rVar, a aVar) {
        int L;
        int c2;
        boolean z2 = false;
        if (rVar.a() || this.f2007f == -1) {
            return false;
        }
        if (this.f2007f < 0 || this.f2007f >= rVar.e()) {
            this.f2007f = -1;
            this.f2008g = Integer.MIN_VALUE;
            return false;
        } else if (this.A == null || this.A.f2019a == -1 || this.A.f2021c < 1) {
            View c3 = c(this.f2007f);
            if (c3 != null) {
                if (this.f2006e) {
                    L = K();
                } else {
                    L = L();
                }
                aVar.f2027a = L;
                if (this.f2008g != Integer.MIN_VALUE) {
                    if (aVar.f2029c) {
                        aVar.f2028b = (this.f2003b.d() - this.f2008g) - this.f2003b.b(c3);
                        return true;
                    }
                    aVar.f2028b = (this.f2003b.c() + this.f2008g) - this.f2003b.a(c3);
                    return true;
                } else if (this.f2003b.e(c3) > this.f2003b.f()) {
                    if (aVar.f2029c) {
                        c2 = this.f2003b.d();
                    } else {
                        c2 = this.f2003b.c();
                    }
                    aVar.f2028b = c2;
                    return true;
                } else {
                    int a2 = this.f2003b.a(c3) - this.f2003b.c();
                    if (a2 < 0) {
                        aVar.f2028b = -a2;
                        return true;
                    }
                    int d2 = this.f2003b.d() - this.f2003b.b(c3);
                    if (d2 < 0) {
                        aVar.f2028b = d2;
                        return true;
                    }
                    aVar.f2028b = Integer.MIN_VALUE;
                    return true;
                }
            } else {
                aVar.f2027a = this.f2007f;
                if (this.f2008g == Integer.MIN_VALUE) {
                    if (u(aVar.f2027a) == 1) {
                        z2 = true;
                    }
                    aVar.f2029c = z2;
                    aVar.b();
                } else {
                    aVar.a(this.f2008g);
                }
                aVar.f2030d = true;
                return true;
            }
        } else {
            aVar.f2028b = Integer.MIN_VALUE;
            aVar.f2027a = this.f2007f;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void f(int i2) {
        this.k = i2 / this.i;
        this.B = View.MeasureSpec.makeMeasureSpec(i2, this.f2004c.h());
    }

    public boolean b() {
        return this.A == null;
    }

    public int c(RecyclerView.r rVar) {
        return b(rVar);
    }

    private int b(RecyclerView.r rVar) {
        boolean z2 = true;
        if (u() == 0) {
            return 0;
        }
        ac acVar = this.f2003b;
        View b2 = b(!this.F);
        if (this.F) {
            z2 = false;
        }
        return ah.a(rVar, acVar, b2, d(z2), this, this.F, this.f2006e);
    }

    public int d(RecyclerView.r rVar) {
        return b(rVar);
    }

    public int e(RecyclerView.r rVar) {
        return i(rVar);
    }

    private int i(RecyclerView.r rVar) {
        boolean z2 = true;
        if (u() == 0) {
            return 0;
        }
        ac acVar = this.f2003b;
        View b2 = b(!this.F);
        if (this.F) {
            z2 = false;
        }
        return ah.a(rVar, acVar, b2, d(z2), this, this.F);
    }

    public int f(RecyclerView.r rVar) {
        return i(rVar);
    }

    public int g(RecyclerView.r rVar) {
        return j(rVar);
    }

    private int j(RecyclerView.r rVar) {
        boolean z2 = true;
        if (u() == 0) {
            return 0;
        }
        ac acVar = this.f2003b;
        View b2 = b(!this.F);
        if (this.F) {
            z2 = false;
        }
        return ah.b(rVar, acVar, b2, d(z2), this, this.F);
    }

    public int h(RecyclerView.r rVar) {
        return j(rVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.RecyclerView.h.a(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(int, int, int, int, boolean):int */
    private void a(View view, LayoutParams layoutParams, boolean z2) {
        if (layoutParams.f2012b) {
            if (this.j == 1) {
                a(view, this.B, a(y(), w(), 0, layoutParams.height, true), z2);
            } else {
                a(view, a(x(), v(), 0, layoutParams.width, true), this.B, z2);
            }
        } else if (this.j == 1) {
            a(view, a(this.k, v(), 0, layoutParams.width, false), a(y(), w(), 0, layoutParams.height, true), z2);
        } else {
            a(view, a(x(), v(), 0, layoutParams.width, true), a(this.k, w(), 0, layoutParams.height, false), z2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.b(int, int, int):int
     arg types: [int, int, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.b(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void
      android.support.v7.widget.StaggeredGridLayoutManager.b(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.StaggeredGridLayoutManager.b(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.RecyclerView.h.b(int, int, int):boolean
      android.support.v7.widget.RecyclerView.h.b(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.b(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.b(int, int, int):int */
    private void a(View view, int i2, int i3, boolean z2) {
        boolean b2;
        b(view, this.C);
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int b3 = b(i2, layoutParams.leftMargin + this.C.left, layoutParams.rightMargin + this.C.right);
        int b4 = b(i3, layoutParams.topMargin + this.C.top, layoutParams.bottomMargin + this.C.bottom);
        if (z2) {
            b2 = a(view, b3, b4, layoutParams);
        } else {
            b2 = b(view, b3, b4, layoutParams);
        }
        if (b2) {
            view.measure(b3, b4);
        }
    }

    private int b(int i2, int i3, int i4) {
        if (i3 == 0 && i4 == 0) {
            return i2;
        }
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE || mode == 1073741824) {
            return View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i2) - i3) - i4), mode);
        }
        return i2;
    }

    public void a(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.A = (SavedState) parcelable;
            n();
        }
    }

    public Parcelable c() {
        int L;
        int a2;
        if (this.A != null) {
            return new SavedState(this.A);
        }
        SavedState savedState = new SavedState();
        savedState.f2026h = this.f2005d;
        savedState.i = this.o;
        savedState.j = this.z;
        if (this.f2009h == null || this.f2009h.f2013a == null) {
            savedState.f2023e = 0;
        } else {
            savedState.f2024f = this.f2009h.f2013a;
            savedState.f2023e = savedState.f2024f.length;
            savedState.f2025g = this.f2009h.f2014b;
        }
        if (u() > 0) {
            if (this.o) {
                L = K();
            } else {
                L = L();
            }
            savedState.f2019a = L;
            savedState.f2020b = j();
            savedState.f2021c = this.i;
            savedState.f2022d = new int[this.i];
            for (int i2 = 0; i2 < this.i; i2++) {
                if (this.o) {
                    a2 = this.f2002a[i2].b(Integer.MIN_VALUE);
                    if (a2 != Integer.MIN_VALUE) {
                        a2 -= this.f2003b.d();
                    }
                } else {
                    a2 = this.f2002a[i2].a(Integer.MIN_VALUE);
                    if (a2 != Integer.MIN_VALUE) {
                        a2 -= this.f2003b.c();
                    }
                }
                savedState.f2022d[i2] = a2;
            }
        } else {
            savedState.f2019a = -1;
            savedState.f2020b = -1;
            savedState.f2021c = 0;
        }
        return savedState;
    }

    public void a(RecyclerView.n nVar, RecyclerView.r rVar, View view, e eVar) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            super.a(view, eVar);
            return;
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        if (this.j == 0) {
            eVar.c(e.n.a(layoutParams2.b(), layoutParams2.f2012b ? this.i : 1, -1, -1, layoutParams2.f2012b, false));
        } else {
            eVar.c(e.n.a(-1, -1, layoutParams2.b(), layoutParams2.f2012b ? this.i : 1, layoutParams2.f2012b, false));
        }
    }

    public void a(AccessibilityEvent accessibilityEvent) {
        super.a(accessibilityEvent);
        if (u() > 0) {
            q a2 = android.support.v4.view.a.a.a(accessibilityEvent);
            View b2 = b(false);
            View d2 = d(false);
            if (b2 != null && d2 != null) {
                int d3 = d(b2);
                int d4 = d(d2);
                if (d3 < d4) {
                    a2.b(d3);
                    a2.c(d4);
                    return;
                }
                a2.b(d4);
                a2.c(d3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int j() {
        View b2;
        if (this.f2006e) {
            b2 = d(true);
        } else {
            b2 = b(true);
        }
        if (b2 == null) {
            return -1;
        }
        return d(b2);
    }

    public int a(RecyclerView.n nVar, RecyclerView.r rVar) {
        if (this.j == 0) {
            return this.i;
        }
        return super.a(nVar, rVar);
    }

    public int b(RecyclerView.n nVar, RecyclerView.r rVar) {
        if (this.j == 1) {
            return this.i;
        }
        return super.b(nVar, rVar);
    }

    /* access modifiers changed from: package-private */
    public View b(boolean z2) {
        int c2 = this.f2003b.c();
        int d2 = this.f2003b.d();
        int u = u();
        View view = null;
        for (int i2 = 0; i2 < u; i2++) {
            View i3 = i(i2);
            int a2 = this.f2003b.a(i3);
            if (this.f2003b.b(i3) > c2 && a2 < d2) {
                if (a2 >= c2 || !z2) {
                    return i3;
                }
                if (view == null) {
                    view = i3;
                }
            }
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    public View d(boolean z2) {
        int c2 = this.f2003b.c();
        int d2 = this.f2003b.d();
        View view = null;
        for (int u = u() - 1; u >= 0; u--) {
            View i2 = i(u);
            int a2 = this.f2003b.a(i2);
            int b2 = this.f2003b.b(i2);
            if (b2 > c2 && a2 < d2) {
                if (b2 <= d2 || !z2) {
                    return i2;
                }
                if (view == null) {
                    view = i2;
                }
            }
        }
        return view;
    }

    private void b(RecyclerView.n nVar, RecyclerView.r rVar, boolean z2) {
        int d2;
        int r = r(Integer.MIN_VALUE);
        if (r != Integer.MIN_VALUE && (d2 = this.f2003b.d() - r) > 0) {
            int i2 = d2 - (-c(-d2, nVar, rVar));
            if (z2 && i2 > 0) {
                this.f2003b.a(i2);
            }
        }
    }

    private void c(RecyclerView.n nVar, RecyclerView.r rVar, boolean z2) {
        int c2;
        int q = q(Integer.MAX_VALUE);
        if (q != Integer.MAX_VALUE && (c2 = q - this.f2003b.c()) > 0) {
            int c3 = c2 - c(c2, nVar, rVar);
            if (z2 && c3 > 0) {
                this.f2003b.a(-c3);
            }
        }
    }

    private void b(int i2, RecyclerView.r rVar) {
        int i3;
        int i4;
        int c2;
        boolean z2 = false;
        this.l.f2298b = 0;
        this.l.f2299c = i2;
        if (!r() || (c2 = rVar.c()) == -1) {
            i3 = 0;
            i4 = 0;
        } else {
            if (this.f2006e == (c2 < i2)) {
                i3 = this.f2003b.f();
                i4 = 0;
            } else {
                i4 = this.f2003b.f();
                i3 = 0;
            }
        }
        if (q()) {
            this.l.f2302f = this.f2003b.c() - i4;
            this.l.f2303g = i3 + this.f2003b.d();
        } else {
            this.l.f2303g = i3 + this.f2003b.e();
            this.l.f2302f = -i4;
        }
        this.l.f2304h = false;
        this.l.f2297a = true;
        z zVar = this.l;
        if (this.f2003b.h() == 0 && this.f2003b.e() == 0) {
            z2 = true;
        }
        zVar.i = z2;
    }

    private void m(int i2) {
        int i3 = 1;
        this.l.f2301e = i2;
        z zVar = this.l;
        if (this.f2006e != (i2 == -1)) {
            i3 = -1;
        }
        zVar.f2300d = i3;
    }

    public void j(int i2) {
        super.j(i2);
        for (int i3 = 0; i3 < this.i; i3++) {
            this.f2002a[i3].d(i2);
        }
    }

    public void k(int i2) {
        super.k(i2);
        for (int i3 = 0; i3 < this.i; i3++) {
            this.f2002a[i3].d(i2);
        }
    }

    public void b(RecyclerView recyclerView, int i2, int i3) {
        c(i2, i3, 2);
    }

    public void a(RecyclerView recyclerView, int i2, int i3) {
        c(i2, i3, 1);
    }

    public void a(RecyclerView recyclerView) {
        this.f2009h.a();
        n();
    }

    public void a(RecyclerView recyclerView, int i2, int i3, int i4) {
        c(i2, i3, 8);
    }

    public void a(RecyclerView recyclerView, int i2, int i3, Object obj) {
        c(i2, i3, 4);
    }

    private void c(int i2, int i3, int i4) {
        int i5;
        int i6;
        int K = this.f2006e ? K() : L();
        if (i4 != 8) {
            i5 = i2 + i3;
            i6 = i2;
        } else if (i2 < i3) {
            i5 = i3 + 1;
            i6 = i2;
        } else {
            i5 = i2 + 1;
            i6 = i3;
        }
        this.f2009h.b(i6);
        switch (i4) {
            case 1:
                this.f2009h.b(i2, i3);
                break;
            case 2:
                this.f2009h.a(i2, i3);
                break;
            case 8:
                this.f2009h.a(i2, 1);
                this.f2009h.b(i3, 1);
                break;
        }
        if (i5 > K) {
            if (i6 <= (this.f2006e ? L() : K())) {
                n();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.StaggeredGridLayoutManager.a(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$LayoutParams, boolean):void
     arg types: [android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$LayoutParams, int]
     candidates:
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.z, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.StaggeredGridLayoutManager$b, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$LayoutParams, android.support.v7.widget.z):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.graphics.Rect, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
      android.support.v7.widget.RecyclerView.h.a(int, int, int):int
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
      android.support.v7.widget.StaggeredGridLayoutManager.a(android.view.View, android.support.v7.widget.StaggeredGridLayoutManager$LayoutParams, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    private int a(RecyclerView.n nVar, z zVar, RecyclerView.r rVar) {
        int i2;
        int c2;
        int r;
        b bVar;
        int a2;
        int e2;
        int i3;
        int c3;
        int e3;
        int i4;
        int d2;
        boolean z2;
        int b2;
        this.m.set(0, this.i, true);
        if (this.l.i) {
            if (zVar.f2301e == 1) {
                i2 = Integer.MAX_VALUE;
            } else {
                i2 = Integer.MIN_VALUE;
            }
        } else if (zVar.f2301e == 1) {
            i2 = zVar.f2303g + zVar.f2298b;
        } else {
            i2 = zVar.f2302f - zVar.f2298b;
        }
        a(zVar.f2301e, i2);
        if (this.f2006e) {
            c2 = this.f2003b.d();
        } else {
            c2 = this.f2003b.c();
        }
        boolean z3 = false;
        while (zVar.a(rVar) && (this.l.i || !this.m.isEmpty())) {
            View a3 = zVar.a(nVar);
            LayoutParams layoutParams = (LayoutParams) a3.getLayoutParams();
            int f2 = layoutParams.f();
            int c4 = this.f2009h.c(f2);
            boolean z4 = c4 == -1;
            if (z4) {
                b a4 = layoutParams.f2012b ? this.f2002a[0] : a(zVar);
                this.f2009h.a(f2, a4);
                bVar = a4;
            } else {
                bVar = this.f2002a[c4];
            }
            layoutParams.f2011a = bVar;
            if (zVar.f2301e == 1) {
                b(a3);
            } else {
                b(a3, 0);
            }
            a(a3, layoutParams, false);
            if (zVar.f2301e == 1) {
                if (layoutParams.f2012b) {
                    b2 = r(c2);
                } else {
                    b2 = bVar.b(c2);
                }
                i3 = b2 + this.f2003b.e(a3);
                if (!z4 || !layoutParams.f2012b) {
                    e2 = b2;
                } else {
                    LazySpanLookup.FullSpanItem n2 = n(b2);
                    n2.f2016b = -1;
                    n2.f2015a = f2;
                    this.f2009h.a(n2);
                    e2 = b2;
                }
            } else {
                if (layoutParams.f2012b) {
                    a2 = q(c2);
                } else {
                    a2 = bVar.a(c2);
                }
                e2 = a2 - this.f2003b.e(a3);
                if (z4 && layoutParams.f2012b) {
                    LazySpanLookup.FullSpanItem o2 = o(a2);
                    o2.f2016b = 1;
                    o2.f2015a = f2;
                    this.f2009h.a(o2);
                }
                i3 = a2;
            }
            if (layoutParams.f2012b && zVar.f2300d == -1) {
                if (z4) {
                    this.E = true;
                } else {
                    if (zVar.f2301e == 1) {
                        z2 = !l();
                    } else {
                        z2 = !m();
                    }
                    if (z2) {
                        LazySpanLookup.FullSpanItem f3 = this.f2009h.f(f2);
                        if (f3 != null) {
                            f3.f2018d = true;
                        }
                        this.E = true;
                    }
                }
            }
            a(a3, layoutParams, zVar);
            if (!i() || this.j != 1) {
                if (layoutParams.f2012b) {
                    c3 = this.f2004c.c();
                } else {
                    c3 = (bVar.f2038e * this.k) + this.f2004c.c();
                }
                e3 = c3 + this.f2004c.e(a3);
                i4 = c3;
            } else {
                if (layoutParams.f2012b) {
                    d2 = this.f2004c.d();
                } else {
                    d2 = this.f2004c.d() - (((this.i - 1) - bVar.f2038e) * this.k);
                }
                i4 = d2 - this.f2004c.e(a3);
                e3 = d2;
            }
            if (this.j == 1) {
                a(a3, i4, e2, e3, i3);
            } else {
                a(a3, e2, i4, i3, e3);
            }
            if (layoutParams.f2012b) {
                a(this.l.f2301e, i2);
            } else {
                a(bVar, this.l.f2301e, i2);
            }
            a(nVar, this.l);
            if (this.l.f2304h && a3.hasFocusable()) {
                if (layoutParams.f2012b) {
                    this.m.clear();
                } else {
                    this.m.set(bVar.f2038e, false);
                }
            }
            z3 = true;
        }
        if (!z3) {
            a(nVar, this.l);
        }
        if (this.l.f2301e == -1) {
            r = this.f2003b.c() - q(this.f2003b.c());
        } else {
            r = r(this.f2003b.d()) - this.f2003b.d();
        }
        if (r > 0) {
            return Math.min(zVar.f2298b, r);
        }
        return 0;
    }

    private LazySpanLookup.FullSpanItem n(int i2) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.f2017c = new int[this.i];
        for (int i3 = 0; i3 < this.i; i3++) {
            fullSpanItem.f2017c[i3] = i2 - this.f2002a[i3].b(i2);
        }
        return fullSpanItem;
    }

    private LazySpanLookup.FullSpanItem o(int i2) {
        LazySpanLookup.FullSpanItem fullSpanItem = new LazySpanLookup.FullSpanItem();
        fullSpanItem.f2017c = new int[this.i];
        for (int i3 = 0; i3 < this.i; i3++) {
            fullSpanItem.f2017c[i3] = this.f2002a[i3].a(i2) - i2;
        }
        return fullSpanItem;
    }

    private void a(View view, LayoutParams layoutParams, z zVar) {
        if (zVar.f2301e == 1) {
            if (layoutParams.f2012b) {
                p(view);
            } else {
                layoutParams.f2011a.b(view);
            }
        } else if (layoutParams.f2012b) {
            q(view);
        } else {
            layoutParams.f2011a.a(view);
        }
    }

    private void a(RecyclerView.n nVar, z zVar) {
        int min;
        int min2;
        if (zVar.f2297a && !zVar.i) {
            if (zVar.f2298b == 0) {
                if (zVar.f2301e == -1) {
                    b(nVar, zVar.f2303g);
                } else {
                    a(nVar, zVar.f2302f);
                }
            } else if (zVar.f2301e == -1) {
                int p = zVar.f2302f - p(zVar.f2302f);
                if (p < 0) {
                    min2 = zVar.f2303g;
                } else {
                    min2 = zVar.f2303g - Math.min(p, zVar.f2298b);
                }
                b(nVar, min2);
            } else {
                int s = s(zVar.f2303g) - zVar.f2303g;
                if (s < 0) {
                    min = zVar.f2302f;
                } else {
                    min = Math.min(s, zVar.f2298b) + zVar.f2302f;
                }
                a(nVar, min);
            }
        }
    }

    private void p(View view) {
        for (int i2 = this.i - 1; i2 >= 0; i2--) {
            this.f2002a[i2].b(view);
        }
    }

    private void q(View view) {
        for (int i2 = this.i - 1; i2 >= 0; i2--) {
            this.f2002a[i2].a(view);
        }
    }

    private void a(int i2, int i3) {
        for (int i4 = 0; i4 < this.i; i4++) {
            if (!this.f2002a[i4].f2034a.isEmpty()) {
                a(this.f2002a[i4], i2, i3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    private void a(b bVar, int i2, int i3) {
        int i4 = bVar.i();
        if (i2 == -1) {
            if (i4 + bVar.b() <= i3) {
                this.m.set(bVar.f2038e, false);
            }
        } else if (bVar.d() - i4 >= i3) {
            this.m.set(bVar.f2038e, false);
        }
    }

    private int p(int i2) {
        int a2 = this.f2002a[0].a(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int a3 = this.f2002a[i3].a(i2);
            if (a3 > a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    private int q(int i2) {
        int a2 = this.f2002a[0].a(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int a3 = this.f2002a[i3].a(i2);
            if (a3 < a2) {
                a2 = a3;
            }
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public boolean l() {
        int b2 = this.f2002a[0].b(Integer.MIN_VALUE);
        for (int i2 = 1; i2 < this.i; i2++) {
            if (this.f2002a[i2].b(Integer.MIN_VALUE) != b2) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean m() {
        int a2 = this.f2002a[0].a(Integer.MIN_VALUE);
        for (int i2 = 1; i2 < this.i; i2++) {
            if (this.f2002a[i2].a(Integer.MIN_VALUE) != a2) {
                return false;
            }
        }
        return true;
    }

    private int r(int i2) {
        int b2 = this.f2002a[0].b(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int b3 = this.f2002a[i3].b(i2);
            if (b3 > b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private int s(int i2) {
        int b2 = this.f2002a[0].b(i2);
        for (int i3 = 1; i3 < this.i; i3++) {
            int b3 = this.f2002a[i3].b(i2);
            if (b3 < b2) {
                b2 = b3;
            }
        }
        return b2;
    }

    private void a(RecyclerView.n nVar, int i2) {
        while (u() > 0) {
            View i3 = i(0);
            if (this.f2003b.b(i3) <= i2 && this.f2003b.c(i3) <= i2) {
                LayoutParams layoutParams = (LayoutParams) i3.getLayoutParams();
                if (layoutParams.f2012b) {
                    int i4 = 0;
                    while (i4 < this.i) {
                        if (this.f2002a[i4].f2034a.size() != 1) {
                            i4++;
                        } else {
                            return;
                        }
                    }
                    for (int i5 = 0; i5 < this.i; i5++) {
                        this.f2002a[i5].h();
                    }
                } else if (layoutParams.f2011a.f2034a.size() != 1) {
                    layoutParams.f2011a.h();
                } else {
                    return;
                }
                a(i3, nVar);
            } else {
                return;
            }
        }
    }

    private void b(RecyclerView.n nVar, int i2) {
        int u = u() - 1;
        while (u >= 0) {
            View i3 = i(u);
            if (this.f2003b.a(i3) >= i2 && this.f2003b.d(i3) >= i2) {
                LayoutParams layoutParams = (LayoutParams) i3.getLayoutParams();
                if (layoutParams.f2012b) {
                    int i4 = 0;
                    while (i4 < this.i) {
                        if (this.f2002a[i4].f2034a.size() != 1) {
                            i4++;
                        } else {
                            return;
                        }
                    }
                    for (int i5 = 0; i5 < this.i; i5++) {
                        this.f2002a[i5].g();
                    }
                } else if (layoutParams.f2011a.f2034a.size() != 1) {
                    layoutParams.f2011a.g();
                } else {
                    return;
                }
                a(i3, nVar);
                u--;
            } else {
                return;
            }
        }
    }

    private boolean t(int i2) {
        boolean z2;
        if (this.j == 0) {
            if (i2 == -1) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (z2 != this.f2006e) {
                return true;
            }
            return false;
        }
        if (((i2 == -1) == this.f2006e) != i()) {
            return false;
        }
        return true;
    }

    private b a(z zVar) {
        int i2;
        int i3;
        b bVar;
        b bVar2;
        b bVar3 = null;
        int i4 = -1;
        if (t(zVar.f2301e)) {
            i2 = this.i - 1;
            i3 = -1;
        } else {
            i2 = 0;
            i3 = this.i;
            i4 = 1;
        }
        if (zVar.f2301e == 1) {
            int c2 = this.f2003b.c();
            int i5 = i2;
            int i6 = Integer.MAX_VALUE;
            while (i5 != i3) {
                b bVar4 = this.f2002a[i5];
                int b2 = bVar4.b(c2);
                if (b2 < i6) {
                    bVar2 = bVar4;
                } else {
                    b2 = i6;
                    bVar2 = bVar3;
                }
                i5 += i4;
                bVar3 = bVar2;
                i6 = b2;
            }
        } else {
            int d2 = this.f2003b.d();
            int i7 = i2;
            int i8 = Integer.MIN_VALUE;
            while (i7 != i3) {
                b bVar5 = this.f2002a[i7];
                int a2 = bVar5.a(d2);
                if (a2 > i8) {
                    bVar = bVar5;
                } else {
                    a2 = i8;
                    bVar = bVar3;
                }
                i7 += i4;
                bVar3 = bVar;
                i8 = a2;
            }
        }
        return bVar3;
    }

    public boolean e() {
        return this.j == 1;
    }

    public boolean d() {
        return this.j == 0;
    }

    public int a(int i2, RecyclerView.n nVar, RecyclerView.r rVar) {
        return c(i2, nVar, rVar);
    }

    public int b(int i2, RecyclerView.n nVar, RecyclerView.r rVar) {
        return c(i2, nVar, rVar);
    }

    private int u(int i2) {
        int i3 = -1;
        if (u() != 0) {
            if ((i2 < L()) == this.f2006e) {
                i3 = 1;
            }
            return i3;
        } else if (this.f2006e) {
            return 1;
        } else {
            return -1;
        }
    }

    public PointF d(int i2) {
        int u = u(i2);
        PointF pointF = new PointF();
        if (u == 0) {
            return null;
        }
        if (this.j == 0) {
            pointF.x = (float) u;
            pointF.y = 0.0f;
            return pointF;
        }
        pointF.x = 0.0f;
        pointF.y = (float) u;
        return pointF;
    }

    public void a(RecyclerView recyclerView, RecyclerView.r rVar, int i2) {
        LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext());
        linearSmoothScroller.d(i2);
        a(linearSmoothScroller);
    }

    public void e(int i2) {
        if (!(this.A == null || this.A.f2019a == i2)) {
            this.A.b();
        }
        this.f2007f = i2;
        this.f2008g = Integer.MIN_VALUE;
        n();
    }

    public void a(int i2, int i3, RecyclerView.r rVar, RecyclerView.h.a aVar) {
        int b2;
        if (this.j != 0) {
            i2 = i3;
        }
        if (u() != 0 && i2 != 0) {
            a(i2, rVar);
            if (this.G == null || this.G.length < this.i) {
                this.G = new int[this.i];
            }
            int i4 = 0;
            for (int i5 = 0; i5 < this.i; i5++) {
                if (this.l.f2300d == -1) {
                    b2 = this.l.f2302f - this.f2002a[i5].a(this.l.f2302f);
                } else {
                    b2 = this.f2002a[i5].b(this.l.f2303g) - this.l.f2303g;
                }
                if (b2 >= 0) {
                    this.G[i4] = b2;
                    i4++;
                }
            }
            Arrays.sort(this.G, 0, i4);
            for (int i6 = 0; i6 < i4 && this.l.a(rVar); i6++) {
                aVar.b(this.l.f2299c, this.G[i6]);
                this.l.f2299c += this.l.f2300d;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, RecyclerView.r rVar) {
        int i3;
        int L;
        if (i2 > 0) {
            L = K();
            i3 = 1;
        } else {
            i3 = -1;
            L = L();
        }
        this.l.f2297a = true;
        b(L, rVar);
        m(i3);
        this.l.f2299c = this.l.f2300d + L;
        this.l.f2298b = Math.abs(i2);
    }

    /* access modifiers changed from: package-private */
    public int c(int i2, RecyclerView.n nVar, RecyclerView.r rVar) {
        if (u() == 0 || i2 == 0) {
            return 0;
        }
        a(i2, rVar);
        int a2 = a(nVar, this.l, rVar);
        if (this.l.f2298b >= a2) {
            i2 = i2 < 0 ? -a2 : a2;
        }
        this.f2003b.a(-i2);
        this.o = this.f2006e;
        this.l.f2298b = 0;
        a(nVar, this.l);
        return i2;
    }

    /* access modifiers changed from: package-private */
    public int K() {
        int u = u();
        if (u == 0) {
            return 0;
        }
        return d(i(u - 1));
    }

    /* access modifiers changed from: package-private */
    public int L() {
        if (u() == 0) {
            return 0;
        }
        return d(i(0));
    }

    private int v(int i2) {
        int u = u();
        for (int i3 = 0; i3 < u; i3++) {
            int d2 = d(i(i3));
            if (d2 >= 0 && d2 < i2) {
                return d2;
            }
        }
        return 0;
    }

    private int w(int i2) {
        for (int u = u() - 1; u >= 0; u--) {
            int d2 = d(i(u));
            if (d2 >= 0 && d2 < i2) {
                return d2;
            }
        }
        return 0;
    }

    public RecyclerView.LayoutParams a() {
        if (this.j == 0) {
            return new LayoutParams(-2, -1);
        }
        return new LayoutParams(-1, -2);
    }

    public RecyclerView.LayoutParams a(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    public RecyclerView.LayoutParams a(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    public boolean a(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public int M() {
        return this.j;
    }

    public View a(View view, int i2, RecyclerView.n nVar, RecyclerView.r rVar) {
        int L;
        int k2;
        int k3;
        int k4;
        View a2;
        if (u() == 0) {
            return null;
        }
        View e2 = e(view);
        if (e2 == null) {
            return null;
        }
        O();
        int x = x(i2);
        if (x == Integer.MIN_VALUE) {
            return null;
        }
        LayoutParams layoutParams = (LayoutParams) e2.getLayoutParams();
        boolean z2 = layoutParams.f2012b;
        b bVar = layoutParams.f2011a;
        if (x == 1) {
            L = K();
        } else {
            L = L();
        }
        b(L, rVar);
        m(x);
        this.l.f2299c = this.l.f2300d + L;
        this.l.f2298b = (int) (0.33333334f * ((float) this.f2003b.f()));
        this.l.f2304h = true;
        this.l.f2297a = false;
        a(nVar, this.l, rVar);
        this.o = this.f2006e;
        if (!z2 && (a2 = bVar.a(L, x)) != null && a2 != e2) {
            return a2;
        }
        if (t(x)) {
            for (int i3 = this.i - 1; i3 >= 0; i3--) {
                View a3 = this.f2002a[i3].a(L, x);
                if (a3 != null && a3 != e2) {
                    return a3;
                }
            }
        } else {
            for (int i4 = 0; i4 < this.i; i4++) {
                View a4 = this.f2002a[i4].a(L, x);
                if (a4 != null && a4 != e2) {
                    return a4;
                }
            }
        }
        boolean z3 = (!this.f2005d) == (x == -1);
        if (!z2) {
            if (z3) {
                k4 = bVar.j();
            } else {
                k4 = bVar.k();
            }
            View c2 = c(k4);
            if (!(c2 == null || c2 == e2)) {
                return c2;
            }
        }
        if (t(x)) {
            for (int i5 = this.i - 1; i5 >= 0; i5--) {
                if (i5 != bVar.f2038e) {
                    if (z3) {
                        k3 = this.f2002a[i5].j();
                    } else {
                        k3 = this.f2002a[i5].k();
                    }
                    View c3 = c(k3);
                    if (!(c3 == null || c3 == e2)) {
                        return c3;
                    }
                }
            }
        } else {
            for (int i6 = 0; i6 < this.i; i6++) {
                if (z3) {
                    k2 = this.f2002a[i6].j();
                } else {
                    k2 = this.f2002a[i6].k();
                }
                View c4 = c(k2);
                if (c4 != null && c4 != e2) {
                    return c4;
                }
            }
        }
        return null;
    }

    private int x(int i2) {
        int i3 = Integer.MIN_VALUE;
        int i4 = 1;
        switch (i2) {
            case 1:
                if (this.j == 1 || !i()) {
                    return -1;
                }
                return 1;
            case 2:
                if (this.j == 1) {
                    return 1;
                }
                if (!i()) {
                    return 1;
                }
                return -1;
            case 17:
                if (this.j != 0) {
                    return Integer.MIN_VALUE;
                }
                return -1;
            case 33:
                if (this.j != 1) {
                    return Integer.MIN_VALUE;
                }
                return -1;
            case 66:
                if (this.j != 0) {
                    i4 = Integer.MIN_VALUE;
                }
                return i4;
            case 130:
                if (this.j == 1) {
                    i3 = 1;
                }
                return i3;
            default:
                return Integer.MIN_VALUE;
        }
    }

    public static class LayoutParams extends RecyclerView.LayoutParams {

        /* renamed from: a  reason: collision with root package name */
        b f2011a;

        /* renamed from: b  reason: collision with root package name */
        boolean f2012b;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public boolean a() {
            return this.f2012b;
        }

        public final int b() {
            if (this.f2011a == null) {
                return -1;
            }
            return this.f2011a.f2038e;
        }
    }

    class b {

        /* renamed from: a  reason: collision with root package name */
        ArrayList<View> f2034a = new ArrayList<>();

        /* renamed from: b  reason: collision with root package name */
        int f2035b = Integer.MIN_VALUE;

        /* renamed from: c  reason: collision with root package name */
        int f2036c = Integer.MIN_VALUE;

        /* renamed from: d  reason: collision with root package name */
        int f2037d = 0;

        /* renamed from: e  reason: collision with root package name */
        final int f2038e;

        b(int i) {
            this.f2038e = i;
        }

        /* access modifiers changed from: package-private */
        public int a(int i) {
            if (this.f2035b != Integer.MIN_VALUE) {
                return this.f2035b;
            }
            if (this.f2034a.size() == 0) {
                return i;
            }
            a();
            return this.f2035b;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            LazySpanLookup.FullSpanItem f2;
            View view = this.f2034a.get(0);
            LayoutParams c2 = c(view);
            this.f2035b = StaggeredGridLayoutManager.this.f2003b.a(view);
            if (c2.f2012b && (f2 = StaggeredGridLayoutManager.this.f2009h.f(c2.f())) != null && f2.f2016b == -1) {
                this.f2035b -= f2.a(this.f2038e);
            }
        }

        /* access modifiers changed from: package-private */
        public int b() {
            if (this.f2035b != Integer.MIN_VALUE) {
                return this.f2035b;
            }
            a();
            return this.f2035b;
        }

        /* access modifiers changed from: package-private */
        public int b(int i) {
            if (this.f2036c != Integer.MIN_VALUE) {
                return this.f2036c;
            }
            if (this.f2034a.size() == 0) {
                return i;
            }
            c();
            return this.f2036c;
        }

        /* access modifiers changed from: package-private */
        public void c() {
            LazySpanLookup.FullSpanItem f2;
            View view = this.f2034a.get(this.f2034a.size() - 1);
            LayoutParams c2 = c(view);
            this.f2036c = StaggeredGridLayoutManager.this.f2003b.b(view);
            if (c2.f2012b && (f2 = StaggeredGridLayoutManager.this.f2009h.f(c2.f())) != null && f2.f2016b == 1) {
                this.f2036c = f2.a(this.f2038e) + this.f2036c;
            }
        }

        /* access modifiers changed from: package-private */
        public int d() {
            if (this.f2036c != Integer.MIN_VALUE) {
                return this.f2036c;
            }
            c();
            return this.f2036c;
        }

        /* access modifiers changed from: package-private */
        public void a(View view) {
            LayoutParams c2 = c(view);
            c2.f2011a = this;
            this.f2034a.add(0, view);
            this.f2035b = Integer.MIN_VALUE;
            if (this.f2034a.size() == 1) {
                this.f2036c = Integer.MIN_VALUE;
            }
            if (c2.d() || c2.e()) {
                this.f2037d += StaggeredGridLayoutManager.this.f2003b.e(view);
            }
        }

        /* access modifiers changed from: package-private */
        public void b(View view) {
            LayoutParams c2 = c(view);
            c2.f2011a = this;
            this.f2034a.add(view);
            this.f2036c = Integer.MIN_VALUE;
            if (this.f2034a.size() == 1) {
                this.f2035b = Integer.MIN_VALUE;
            }
            if (c2.d() || c2.e()) {
                this.f2037d += StaggeredGridLayoutManager.this.f2003b.e(view);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z, int i) {
            int a2;
            if (z) {
                a2 = b(Integer.MIN_VALUE);
            } else {
                a2 = a(Integer.MIN_VALUE);
            }
            e();
            if (a2 != Integer.MIN_VALUE) {
                if (z && a2 < StaggeredGridLayoutManager.this.f2003b.d()) {
                    return;
                }
                if (z || a2 <= StaggeredGridLayoutManager.this.f2003b.c()) {
                    if (i != Integer.MIN_VALUE) {
                        a2 += i;
                    }
                    this.f2036c = a2;
                    this.f2035b = a2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void e() {
            this.f2034a.clear();
            f();
            this.f2037d = 0;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            this.f2035b = Integer.MIN_VALUE;
            this.f2036c = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        public void c(int i) {
            this.f2035b = i;
            this.f2036c = i;
        }

        /* access modifiers changed from: package-private */
        public void g() {
            int size = this.f2034a.size();
            View remove = this.f2034a.remove(size - 1);
            LayoutParams c2 = c(remove);
            c2.f2011a = null;
            if (c2.d() || c2.e()) {
                this.f2037d -= StaggeredGridLayoutManager.this.f2003b.e(remove);
            }
            if (size == 1) {
                this.f2035b = Integer.MIN_VALUE;
            }
            this.f2036c = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        public void h() {
            View remove = this.f2034a.remove(0);
            LayoutParams c2 = c(remove);
            c2.f2011a = null;
            if (this.f2034a.size() == 0) {
                this.f2036c = Integer.MIN_VALUE;
            }
            if (c2.d() || c2.e()) {
                this.f2037d -= StaggeredGridLayoutManager.this.f2003b.e(remove);
            }
            this.f2035b = Integer.MIN_VALUE;
        }

        public int i() {
            return this.f2037d;
        }

        /* access modifiers changed from: package-private */
        public LayoutParams c(View view) {
            return (LayoutParams) view.getLayoutParams();
        }

        /* access modifiers changed from: package-private */
        public void d(int i) {
            if (this.f2035b != Integer.MIN_VALUE) {
                this.f2035b += i;
            }
            if (this.f2036c != Integer.MIN_VALUE) {
                this.f2036c += i;
            }
        }

        public int j() {
            if (StaggeredGridLayoutManager.this.f2005d) {
                return a(this.f2034a.size() - 1, -1, true);
            }
            return a(0, this.f2034a.size(), true);
        }

        public int k() {
            if (StaggeredGridLayoutManager.this.f2005d) {
                return a(0, this.f2034a.size(), true);
            }
            return a(this.f2034a.size() - 1, -1, true);
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2, boolean z, boolean z2, boolean z3) {
            int c2 = StaggeredGridLayoutManager.this.f2003b.c();
            int d2 = StaggeredGridLayoutManager.this.f2003b.d();
            int i3 = i2 > i ? 1 : -1;
            while (i != i2) {
                View view = this.f2034a.get(i);
                int a2 = StaggeredGridLayoutManager.this.f2003b.a(view);
                int b2 = StaggeredGridLayoutManager.this.f2003b.b(view);
                boolean z4 = z3 ? a2 <= d2 : a2 < d2;
                boolean z5 = z3 ? b2 >= c2 : b2 > c2;
                if (z4 && z5) {
                    if (!z || !z2) {
                        if (z2) {
                            return StaggeredGridLayoutManager.this.d(view);
                        }
                        if (a2 < c2 || b2 > d2) {
                            return StaggeredGridLayoutManager.this.d(view);
                        }
                    } else if (a2 >= c2 && b2 <= d2) {
                        return StaggeredGridLayoutManager.this.d(view);
                    }
                }
                i += i3;
            }
            return -1;
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2, boolean z) {
            return a(i, i2, false, false, z);
        }

        public View a(int i, int i2) {
            View view = null;
            if (i2 == -1) {
                int size = this.f2034a.size();
                int i3 = 0;
                while (i3 < size) {
                    View view2 = this.f2034a.get(i3);
                    if ((StaggeredGridLayoutManager.this.f2005d && StaggeredGridLayoutManager.this.d(view2) <= i) || ((!StaggeredGridLayoutManager.this.f2005d && StaggeredGridLayoutManager.this.d(view2) >= i) || !view2.hasFocusable())) {
                        break;
                    }
                    i3++;
                    view = view2;
                }
                return view;
            }
            int size2 = this.f2034a.size() - 1;
            while (size2 >= 0) {
                View view3 = this.f2034a.get(size2);
                if (StaggeredGridLayoutManager.this.f2005d && StaggeredGridLayoutManager.this.d(view3) >= i) {
                    break;
                } else if (StaggeredGridLayoutManager.this.f2005d || StaggeredGridLayoutManager.this.d(view3) > i) {
                    if (!view3.hasFocusable()) {
                        break;
                    }
                    size2--;
                    view = view3;
                } else {
                    return view;
                }
            }
            return view;
        }
    }

    static class LazySpanLookup {

        /* renamed from: a  reason: collision with root package name */
        int[] f2013a;

        /* renamed from: b  reason: collision with root package name */
        List<FullSpanItem> f2014b;

        LazySpanLookup() {
        }

        /* access modifiers changed from: package-private */
        public int a(int i) {
            if (this.f2014b != null) {
                for (int size = this.f2014b.size() - 1; size >= 0; size--) {
                    if (this.f2014b.get(size).f2015a >= i) {
                        this.f2014b.remove(size);
                    }
                }
            }
            return b(i);
        }

        /* access modifiers changed from: package-private */
        public int b(int i) {
            if (this.f2013a == null || i >= this.f2013a.length) {
                return -1;
            }
            int g2 = g(i);
            if (g2 == -1) {
                Arrays.fill(this.f2013a, i, this.f2013a.length, -1);
                return this.f2013a.length;
            }
            Arrays.fill(this.f2013a, i, g2 + 1, -1);
            return g2 + 1;
        }

        /* access modifiers changed from: package-private */
        public int c(int i) {
            if (this.f2013a == null || i >= this.f2013a.length) {
                return -1;
            }
            return this.f2013a[i];
        }

        /* access modifiers changed from: package-private */
        public void a(int i, b bVar) {
            e(i);
            this.f2013a[i] = bVar.f2038e;
        }

        /* access modifiers changed from: package-private */
        public int d(int i) {
            int length = this.f2013a.length;
            while (length <= i) {
                length *= 2;
            }
            return length;
        }

        /* access modifiers changed from: package-private */
        public void e(int i) {
            if (this.f2013a == null) {
                this.f2013a = new int[(Math.max(i, 10) + 1)];
                Arrays.fill(this.f2013a, -1);
            } else if (i >= this.f2013a.length) {
                int[] iArr = this.f2013a;
                this.f2013a = new int[d(i)];
                System.arraycopy(iArr, 0, this.f2013a, 0, iArr.length);
                Arrays.fill(this.f2013a, iArr.length, this.f2013a.length, -1);
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (this.f2013a != null) {
                Arrays.fill(this.f2013a, -1);
            }
            this.f2014b = null;
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2) {
            if (this.f2013a != null && i < this.f2013a.length) {
                e(i + i2);
                System.arraycopy(this.f2013a, i + i2, this.f2013a, i, (this.f2013a.length - i) - i2);
                Arrays.fill(this.f2013a, this.f2013a.length - i2, this.f2013a.length, -1);
                c(i, i2);
            }
        }

        private void c(int i, int i2) {
            if (this.f2014b != null) {
                int i3 = i + i2;
                for (int size = this.f2014b.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.f2014b.get(size);
                    if (fullSpanItem.f2015a >= i) {
                        if (fullSpanItem.f2015a < i3) {
                            this.f2014b.remove(size);
                        } else {
                            fullSpanItem.f2015a -= i2;
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i, int i2) {
            if (this.f2013a != null && i < this.f2013a.length) {
                e(i + i2);
                System.arraycopy(this.f2013a, i, this.f2013a, i + i2, (this.f2013a.length - i) - i2);
                Arrays.fill(this.f2013a, i, i + i2, -1);
                d(i, i2);
            }
        }

        private void d(int i, int i2) {
            if (this.f2014b != null) {
                for (int size = this.f2014b.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.f2014b.get(size);
                    if (fullSpanItem.f2015a >= i) {
                        fullSpanItem.f2015a += i2;
                    }
                }
            }
        }

        private int g(int i) {
            if (this.f2014b == null) {
                return -1;
            }
            FullSpanItem f2 = f(i);
            if (f2 != null) {
                this.f2014b.remove(f2);
            }
            int size = this.f2014b.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i2 = -1;
                    break;
                } else if (this.f2014b.get(i2).f2015a >= i) {
                    break;
                } else {
                    i2++;
                }
            }
            if (i2 == -1) {
                return -1;
            }
            this.f2014b.remove(i2);
            return this.f2014b.get(i2).f2015a;
        }

        public void a(FullSpanItem fullSpanItem) {
            if (this.f2014b == null) {
                this.f2014b = new ArrayList();
            }
            int size = this.f2014b.size();
            for (int i = 0; i < size; i++) {
                FullSpanItem fullSpanItem2 = this.f2014b.get(i);
                if (fullSpanItem2.f2015a == fullSpanItem.f2015a) {
                    this.f2014b.remove(i);
                }
                if (fullSpanItem2.f2015a >= fullSpanItem.f2015a) {
                    this.f2014b.add(i, fullSpanItem);
                    return;
                }
            }
            this.f2014b.add(fullSpanItem);
        }

        public FullSpanItem f(int i) {
            if (this.f2014b == null) {
                return null;
            }
            for (int size = this.f2014b.size() - 1; size >= 0; size--) {
                FullSpanItem fullSpanItem = this.f2014b.get(size);
                if (fullSpanItem.f2015a == i) {
                    return fullSpanItem;
                }
            }
            return null;
        }

        public FullSpanItem a(int i, int i2, int i3, boolean z) {
            if (this.f2014b == null) {
                return null;
            }
            int size = this.f2014b.size();
            for (int i4 = 0; i4 < size; i4++) {
                FullSpanItem fullSpanItem = this.f2014b.get(i4);
                if (fullSpanItem.f2015a >= i2) {
                    return null;
                }
                if (fullSpanItem.f2015a >= i) {
                    if (i3 == 0 || fullSpanItem.f2016b == i3) {
                        return fullSpanItem;
                    }
                    if (z && fullSpanItem.f2018d) {
                        return fullSpanItem;
                    }
                }
            }
            return null;
        }

        static class FullSpanItem implements Parcelable {
            public static final Parcelable.Creator<FullSpanItem> CREATOR = new Parcelable.Creator<FullSpanItem>() {
                /* renamed from: a */
                public FullSpanItem createFromParcel(Parcel parcel) {
                    return new FullSpanItem(parcel);
                }

                /* renamed from: a */
                public FullSpanItem[] newArray(int i) {
                    return new FullSpanItem[i];
                }
            };

            /* renamed from: a  reason: collision with root package name */
            int f2015a;

            /* renamed from: b  reason: collision with root package name */
            int f2016b;

            /* renamed from: c  reason: collision with root package name */
            int[] f2017c;

            /* renamed from: d  reason: collision with root package name */
            boolean f2018d;

            public FullSpanItem(Parcel parcel) {
                boolean z = true;
                this.f2015a = parcel.readInt();
                this.f2016b = parcel.readInt();
                this.f2018d = parcel.readInt() != 1 ? false : z;
                int readInt = parcel.readInt();
                if (readInt > 0) {
                    this.f2017c = new int[readInt];
                    parcel.readIntArray(this.f2017c);
                }
            }

            public FullSpanItem() {
            }

            /* access modifiers changed from: package-private */
            public int a(int i) {
                if (this.f2017c == null) {
                    return 0;
                }
                return this.f2017c[i];
            }

            public int describeContents() {
                return 0;
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f2015a);
                parcel.writeInt(this.f2016b);
                parcel.writeInt(this.f2018d ? 1 : 0);
                if (this.f2017c == null || this.f2017c.length <= 0) {
                    parcel.writeInt(0);
                    return;
                }
                parcel.writeInt(this.f2017c.length);
                parcel.writeIntArray(this.f2017c);
            }

            public String toString() {
                return "FullSpanItem{mPosition=" + this.f2015a + ", mGapDir=" + this.f2016b + ", mHasUnwantedGapAfter=" + this.f2018d + ", mGapPerSpan=" + Arrays.toString(this.f2017c) + '}';
            }
        }
    }

    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f2019a;

        /* renamed from: b  reason: collision with root package name */
        int f2020b;

        /* renamed from: c  reason: collision with root package name */
        int f2021c;

        /* renamed from: d  reason: collision with root package name */
        int[] f2022d;

        /* renamed from: e  reason: collision with root package name */
        int f2023e;

        /* renamed from: f  reason: collision with root package name */
        int[] f2024f;

        /* renamed from: g  reason: collision with root package name */
        List<LazySpanLookup.FullSpanItem> f2025g;

        /* renamed from: h  reason: collision with root package name */
        boolean f2026h;
        boolean i;
        boolean j;

        public SavedState() {
        }

        SavedState(Parcel parcel) {
            boolean z;
            boolean z2 = true;
            this.f2019a = parcel.readInt();
            this.f2020b = parcel.readInt();
            this.f2021c = parcel.readInt();
            if (this.f2021c > 0) {
                this.f2022d = new int[this.f2021c];
                parcel.readIntArray(this.f2022d);
            }
            this.f2023e = parcel.readInt();
            if (this.f2023e > 0) {
                this.f2024f = new int[this.f2023e];
                parcel.readIntArray(this.f2024f);
            }
            this.f2026h = parcel.readInt() == 1;
            if (parcel.readInt() == 1) {
                z = true;
            } else {
                z = false;
            }
            this.i = z;
            this.j = parcel.readInt() != 1 ? false : z2;
            this.f2025g = parcel.readArrayList(LazySpanLookup.FullSpanItem.class.getClassLoader());
        }

        public SavedState(SavedState savedState) {
            this.f2021c = savedState.f2021c;
            this.f2019a = savedState.f2019a;
            this.f2020b = savedState.f2020b;
            this.f2022d = savedState.f2022d;
            this.f2023e = savedState.f2023e;
            this.f2024f = savedState.f2024f;
            this.f2026h = savedState.f2026h;
            this.i = savedState.i;
            this.j = savedState.j;
            this.f2025g = savedState.f2025g;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f2022d = null;
            this.f2021c = 0;
            this.f2023e = 0;
            this.f2024f = null;
            this.f2025g = null;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f2022d = null;
            this.f2021c = 0;
            this.f2019a = -1;
            this.f2020b = -1;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            int i3;
            int i4;
            int i5 = 1;
            parcel.writeInt(this.f2019a);
            parcel.writeInt(this.f2020b);
            parcel.writeInt(this.f2021c);
            if (this.f2021c > 0) {
                parcel.writeIntArray(this.f2022d);
            }
            parcel.writeInt(this.f2023e);
            if (this.f2023e > 0) {
                parcel.writeIntArray(this.f2024f);
            }
            if (this.f2026h) {
                i3 = 1;
            } else {
                i3 = 0;
            }
            parcel.writeInt(i3);
            if (this.i) {
                i4 = 1;
            } else {
                i4 = 0;
            }
            parcel.writeInt(i4);
            if (!this.j) {
                i5 = 0;
            }
            parcel.writeInt(i5);
            parcel.writeList(this.f2025g);
        }
    }

    class a {

        /* renamed from: a  reason: collision with root package name */
        int f2027a;

        /* renamed from: b  reason: collision with root package name */
        int f2028b;

        /* renamed from: c  reason: collision with root package name */
        boolean f2029c;

        /* renamed from: d  reason: collision with root package name */
        boolean f2030d;

        /* renamed from: e  reason: collision with root package name */
        boolean f2031e;

        /* renamed from: f  reason: collision with root package name */
        int[] f2032f;

        public a() {
            a();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f2027a = -1;
            this.f2028b = Integer.MIN_VALUE;
            this.f2029c = false;
            this.f2030d = false;
            this.f2031e = false;
            if (this.f2032f != null) {
                Arrays.fill(this.f2032f, -1);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(b[] bVarArr) {
            int length = bVarArr.length;
            if (this.f2032f == null || this.f2032f.length < length) {
                this.f2032f = new int[StaggeredGridLayoutManager.this.f2002a.length];
            }
            for (int i = 0; i < length; i++) {
                this.f2032f[i] = bVarArr[i].a(Integer.MIN_VALUE);
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            int c2;
            if (this.f2029c) {
                c2 = StaggeredGridLayoutManager.this.f2003b.d();
            } else {
                c2 = StaggeredGridLayoutManager.this.f2003b.c();
            }
            this.f2028b = c2;
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            if (this.f2029c) {
                this.f2028b = StaggeredGridLayoutManager.this.f2003b.d() - i;
            } else {
                this.f2028b = StaggeredGridLayoutManager.this.f2003b.c() + i;
            }
        }
    }
}
