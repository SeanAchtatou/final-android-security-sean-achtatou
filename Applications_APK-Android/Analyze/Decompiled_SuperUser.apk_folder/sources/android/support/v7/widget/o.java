package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v7.widget.af;

@TargetApi(9)
/* compiled from: CardViewGingerbread */
class o implements p {

    /* renamed from: a  reason: collision with root package name */
    final RectF f2257a = new RectF();

    o() {
    }

    public void a() {
        af.f2106c = new af.a() {
            public void a(Canvas canvas, RectF rectF, float f2, Paint paint) {
                float f3 = 2.0f * f2;
                float width = (rectF.width() - f3) - 1.0f;
                float height = (rectF.height() - f3) - 1.0f;
                if (f2 >= 1.0f) {
                    float f4 = f2 + 0.5f;
                    o.this.f2257a.set(-f4, -f4, f4, f4);
                    int save = canvas.save();
                    canvas.translate(rectF.left + f4, rectF.top + f4);
                    canvas.drawArc(o.this.f2257a, 180.0f, 90.0f, true, paint);
                    canvas.translate(width, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(o.this.f2257a, 180.0f, 90.0f, true, paint);
                    canvas.translate(height, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(o.this.f2257a, 180.0f, 90.0f, true, paint);
                    canvas.translate(width, 0.0f);
                    canvas.rotate(90.0f);
                    canvas.drawArc(o.this.f2257a, 180.0f, 90.0f, true, paint);
                    canvas.restoreToCount(save);
                    canvas.drawRect((rectF.left + f4) - 1.0f, rectF.top, 1.0f + (rectF.right - f4), rectF.top + f4, paint);
                    canvas.drawRect((rectF.left + f4) - 1.0f, rectF.bottom - f4, 1.0f + (rectF.right - f4), rectF.bottom, paint);
                }
                canvas.drawRect(rectF.left, rectF.top + f2, rectF.right, rectF.bottom - f2, paint);
            }
        };
    }

    public void a(n nVar, Context context, ColorStateList colorStateList, float f2, float f3, float f4) {
        af a2 = a(context, colorStateList, f2, f3, f4);
        a2.a(nVar.b());
        nVar.a(a2);
        f(nVar);
    }

    private af a(Context context, ColorStateList colorStateList, float f2, float f3, float f4) {
        return new af(context.getResources(), colorStateList, f2, f3, f4);
    }

    public void f(n nVar) {
        Rect rect = new Rect();
        j(nVar).a(rect);
        nVar.a((int) Math.ceil((double) b(nVar)), (int) Math.ceil((double) c(nVar)));
        nVar.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    public void g(n nVar) {
    }

    public void h(n nVar) {
        j(nVar).a(nVar.b());
        f(nVar);
    }

    public void a(n nVar, ColorStateList colorStateList) {
        j(nVar).a(colorStateList);
    }

    public ColorStateList i(n nVar) {
        return j(nVar).f();
    }

    public void a(n nVar, float f2) {
        j(nVar).a(f2);
        f(nVar);
    }

    public float d(n nVar) {
        return j(nVar).a();
    }

    public void c(n nVar, float f2) {
        j(nVar).b(f2);
    }

    public float e(n nVar) {
        return j(nVar).b();
    }

    public void b(n nVar, float f2) {
        j(nVar).c(f2);
        f(nVar);
    }

    public float a(n nVar) {
        return j(nVar).c();
    }

    public float b(n nVar) {
        return j(nVar).d();
    }

    public float c(n nVar) {
        return j(nVar).e();
    }

    private af j(n nVar) {
        return (af) nVar.c();
    }
}
