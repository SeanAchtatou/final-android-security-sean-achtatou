package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.b.a;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public class CardView extends FrameLayout {

    /* renamed from: e  reason: collision with root package name */
    private static final int[] f1707e = {16842801};

    /* renamed from: f  reason: collision with root package name */
    private static final p f1708f;

    /* renamed from: a  reason: collision with root package name */
    int f1709a;

    /* renamed from: b  reason: collision with root package name */
    int f1710b;

    /* renamed from: c  reason: collision with root package name */
    final Rect f1711c = new Rect();

    /* renamed from: d  reason: collision with root package name */
    final Rect f1712d = new Rect();

    /* renamed from: g  reason: collision with root package name */
    private boolean f1713g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f1714h;
    private final n i = new n() {

        /* renamed from: b  reason: collision with root package name */
        private Drawable f1716b;

        public void a(Drawable drawable) {
            this.f1716b = drawable;
            CardView.this.setBackgroundDrawable(drawable);
        }

        public boolean a() {
            return CardView.this.getUseCompatPadding();
        }

        public boolean b() {
            return CardView.this.getPreventCornerOverlap();
        }

        public void a(int i, int i2, int i3, int i4) {
            CardView.this.f1712d.set(i, i2, i3, i4);
            CardView.super.setPadding(CardView.this.f1711c.left + i, CardView.this.f1711c.top + i2, CardView.this.f1711c.right + i3, CardView.this.f1711c.bottom + i4);
        }

        public void a(int i, int i2) {
            if (i > CardView.this.f1709a) {
                CardView.super.setMinimumWidth(i);
            }
            if (i2 > CardView.this.f1710b) {
                CardView.super.setMinimumHeight(i2);
            }
        }

        public Drawable c() {
            return this.f1716b;
        }

        public View d() {
            return CardView.this;
        }
    };

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f1708f = new m();
        } else if (Build.VERSION.SDK_INT >= 17) {
            f1708f = new q();
        } else {
            f1708f = new o();
        }
        f1708f.a();
    }

    public CardView(Context context) {
        super(context);
        a(context, null, 0);
    }

    public CardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet, 0);
    }

    public CardView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet, i2);
    }

    public void setPadding(int i2, int i3, int i4, int i5) {
    }

    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
    }

    public boolean getUseCompatPadding() {
        return this.f1713g;
    }

    public void setUseCompatPadding(boolean z) {
        if (this.f1713g != z) {
            this.f1713g = z;
            f1708f.g(this.i);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (!(f1708f instanceof m)) {
            int mode = View.MeasureSpec.getMode(i2);
            switch (mode) {
                case Integer.MIN_VALUE:
                case 1073741824:
                    i2 = View.MeasureSpec.makeMeasureSpec(Math.max((int) Math.ceil((double) f1708f.b(this.i)), View.MeasureSpec.getSize(i2)), mode);
                    break;
            }
            int mode2 = View.MeasureSpec.getMode(i3);
            switch (mode2) {
                case Integer.MIN_VALUE:
                case 1073741824:
                    i3 = View.MeasureSpec.makeMeasureSpec(Math.max((int) Math.ceil((double) f1708f.c(this.i)), View.MeasureSpec.getSize(i3)), mode2);
                    break;
            }
            super.onMeasure(i2, i3);
            return;
        }
        super.onMeasure(i2, i3);
    }

    private void a(Context context, AttributeSet attributeSet, int i2) {
        int color;
        ColorStateList valueOf;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.d.CardView, i2, a.c.db);
        if (obtainStyledAttributes.hasValue(a.d.CardView_cardBackgroundColor)) {
            valueOf = obtainStyledAttributes.getColorStateList(a.d.CardView_cardBackgroundColor);
        } else {
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(f1707e);
            int color2 = obtainStyledAttributes2.getColor(0, 0);
            obtainStyledAttributes2.recycle();
            float[] fArr = new float[3];
            Color.colorToHSV(color2, fArr);
            if (fArr[2] > 0.5f) {
                color = getResources().getColor(a.C0028a.ad);
            } else {
                color = getResources().getColor(a.C0028a.ac);
            }
            valueOf = ColorStateList.valueOf(color);
        }
        float dimension = obtainStyledAttributes.getDimension(a.d.CardView_cardCornerRadius, 0.0f);
        float dimension2 = obtainStyledAttributes.getDimension(a.d.CardView_cardElevation, 0.0f);
        float dimension3 = obtainStyledAttributes.getDimension(a.d.CardView_cardMaxElevation, 0.0f);
        this.f1713g = obtainStyledAttributes.getBoolean(a.d.CardView_cardUseCompatPadding, false);
        this.f1714h = obtainStyledAttributes.getBoolean(a.d.CardView_cardPreventCornerOverlap, true);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(a.d.CardView_contentPadding, 0);
        this.f1711c.left = obtainStyledAttributes.getDimensionPixelSize(a.d.CardView_contentPaddingLeft, dimensionPixelSize);
        this.f1711c.top = obtainStyledAttributes.getDimensionPixelSize(a.d.CardView_contentPaddingTop, dimensionPixelSize);
        this.f1711c.right = obtainStyledAttributes.getDimensionPixelSize(a.d.CardView_contentPaddingRight, dimensionPixelSize);
        this.f1711c.bottom = obtainStyledAttributes.getDimensionPixelSize(a.d.CardView_contentPaddingBottom, dimensionPixelSize);
        if (dimension2 > dimension3) {
            dimension3 = dimension2;
        }
        this.f1709a = obtainStyledAttributes.getDimensionPixelSize(a.d.CardView_android_minWidth, 0);
        this.f1710b = obtainStyledAttributes.getDimensionPixelSize(a.d.CardView_android_minHeight, 0);
        obtainStyledAttributes.recycle();
        f1708f.a(this.i, context, valueOf, dimension, dimension2, dimension3);
    }

    public void setMinimumWidth(int i2) {
        this.f1709a = i2;
        super.setMinimumWidth(i2);
    }

    public void setMinimumHeight(int i2) {
        this.f1710b = i2;
        super.setMinimumHeight(i2);
    }

    public void setCardBackgroundColor(int i2) {
        f1708f.a(this.i, ColorStateList.valueOf(i2));
    }

    public void setCardBackgroundColor(ColorStateList colorStateList) {
        f1708f.a(this.i, colorStateList);
    }

    public ColorStateList getCardBackgroundColor() {
        return f1708f.i(this.i);
    }

    public int getContentPaddingLeft() {
        return this.f1711c.left;
    }

    public int getContentPaddingRight() {
        return this.f1711c.right;
    }

    public int getContentPaddingTop() {
        return this.f1711c.top;
    }

    public int getContentPaddingBottom() {
        return this.f1711c.bottom;
    }

    public void setRadius(float f2) {
        f1708f.a(this.i, f2);
    }

    public float getRadius() {
        return f1708f.d(this.i);
    }

    public void setCardElevation(float f2) {
        f1708f.c(this.i, f2);
    }

    public float getCardElevation() {
        return f1708f.e(this.i);
    }

    public void setMaxCardElevation(float f2) {
        f1708f.b(this.i, f2);
    }

    public float getMaxCardElevation() {
        return f1708f.a(this.i);
    }

    public boolean getPreventCornerOverlap() {
        return this.f1714h;
    }

    public void setPreventCornerOverlap(boolean z) {
        if (z != this.f1714h) {
            this.f1714h = z;
            f1708f.h(this.i);
        }
    }
}
