package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/* compiled from: ScrollbarHelper */
class ah {
    static int a(RecyclerView.r rVar, ac acVar, View view, View view2, RecyclerView.h hVar, boolean z, boolean z2) {
        int max;
        if (hVar.u() == 0 || rVar.e() == 0 || view == null || view2 == null) {
            return 0;
        }
        int min = Math.min(hVar.d(view), hVar.d(view2));
        int max2 = Math.max(hVar.d(view), hVar.d(view2));
        if (z2) {
            max = Math.max(0, (rVar.e() - max2) - 1);
        } else {
            max = Math.max(0, min);
        }
        if (!z) {
            return max;
        }
        return Math.round((((float) max) * (((float) Math.abs(acVar.b(view2) - acVar.a(view))) / ((float) (Math.abs(hVar.d(view) - hVar.d(view2)) + 1)))) + ((float) (acVar.c() - acVar.a(view))));
    }

    static int a(RecyclerView.r rVar, ac acVar, View view, View view2, RecyclerView.h hVar, boolean z) {
        if (hVar.u() == 0 || rVar.e() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(hVar.d(view) - hVar.d(view2)) + 1;
        }
        return Math.min(acVar.f(), acVar.b(view2) - acVar.a(view));
    }

    static int b(RecyclerView.r rVar, ac acVar, View view, View view2, RecyclerView.h hVar, boolean z) {
        if (hVar.u() == 0 || rVar.e() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return rVar.e();
        }
        return (int) ((((float) (acVar.b(view2) - acVar.a(view))) / ((float) (Math.abs(hVar.d(view) - hVar.d(view2)) + 1))) * ((float) rVar.e()));
    }
}
