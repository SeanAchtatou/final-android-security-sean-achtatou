package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ay;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.i;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

/* compiled from: DecorToolbar */
public interface t {
    ay a(int i, long j);

    ViewGroup a();

    void a(int i);

    void a(Drawable drawable);

    void a(i.a aVar, MenuBuilder.a aVar2);

    void a(ScrollingTabContainerView scrollingTabContainerView);

    void a(Menu menu, i.a aVar);

    void a(View view);

    void a(Window.Callback callback);

    void a(CharSequence charSequence);

    void a(boolean z);

    Context b();

    void b(int i);

    void b(CharSequence charSequence);

    void b(boolean z);

    void c(int i);

    boolean c();

    void d();

    void d(int i);

    CharSequence e();

    void e(int i);

    void f();

    void g();

    boolean h();

    boolean i();

    boolean j();

    boolean k();

    boolean l();

    void m();

    void n();

    int o();

    int p();

    int q();

    Menu r();
}
