package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.a.e;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import java.util.Arrays;

public class GridLayoutManager extends LinearLayoutManager {

    /* renamed from: a  reason: collision with root package name */
    boolean f1773a = false;

    /* renamed from: b  reason: collision with root package name */
    int f1774b = -1;

    /* renamed from: c  reason: collision with root package name */
    int[] f1775c;

    /* renamed from: d  reason: collision with root package name */
    View[] f1776d;

    /* renamed from: e  reason: collision with root package name */
    final SparseIntArray f1777e = new SparseIntArray();

    /* renamed from: f  reason: collision with root package name */
    final SparseIntArray f1778f = new SparseIntArray();

    /* renamed from: g  reason: collision with root package name */
    a f1779g = new DefaultSpanSizeLookup();

    /* renamed from: h  reason: collision with root package name */
    final Rect f1780h = new Rect();

    public GridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        a(a(context, attributeSet, i, i2).f1886b);
    }

    public GridLayoutManager(Context context, int i) {
        super(context);
        a(i);
    }

    public GridLayoutManager(Context context, int i, int i2, boolean z) {
        super(context, i2, z);
        a(i);
    }

    public void a(boolean z) {
        if (z) {
            throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
        }
        super.a(false);
    }

    public int a(RecyclerView.n nVar, RecyclerView.r rVar) {
        if (this.i == 0) {
            return this.f1774b;
        }
        if (rVar.e() < 1) {
            return 0;
        }
        return a(nVar, rVar, rVar.e() - 1) + 1;
    }

    public int b(RecyclerView.n nVar, RecyclerView.r rVar) {
        if (this.i == 1) {
            return this.f1774b;
        }
        if (rVar.e() < 1) {
            return 0;
        }
        return a(nVar, rVar, rVar.e() - 1) + 1;
    }

    public void a(RecyclerView.n nVar, RecyclerView.r rVar, View view, e eVar) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            super.a(view, eVar);
            return;
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        int a2 = a(nVar, rVar, layoutParams2.f());
        if (this.i == 0) {
            eVar.c(e.n.a(layoutParams2.a(), layoutParams2.b(), a2, 1, this.f1774b > 1 && layoutParams2.b() == this.f1774b, false));
        } else {
            eVar.c(e.n.a(a2, 1, layoutParams2.a(), layoutParams2.b(), this.f1774b > 1 && layoutParams2.b() == this.f1774b, false));
        }
    }

    public void c(RecyclerView.n nVar, RecyclerView.r rVar) {
        if (rVar.a()) {
            L();
        }
        super.c(nVar, rVar);
        K();
    }

    public void a(RecyclerView.r rVar) {
        super.a(rVar);
        this.f1773a = false;
    }

    private void K() {
        this.f1777e.clear();
        this.f1778f.clear();
    }

    private void L() {
        int u = u();
        for (int i = 0; i < u; i++) {
            LayoutParams layoutParams = (LayoutParams) i(i).getLayoutParams();
            int f2 = layoutParams.f();
            this.f1777e.put(f2, layoutParams.b());
            this.f1778f.put(f2, layoutParams.a());
        }
    }

    public void a(RecyclerView recyclerView, int i, int i2) {
        this.f1779g.a();
    }

    public void a(RecyclerView recyclerView) {
        this.f1779g.a();
    }

    public void b(RecyclerView recyclerView, int i, int i2) {
        this.f1779g.a();
    }

    public void a(RecyclerView recyclerView, int i, int i2, Object obj) {
        this.f1779g.a();
    }

    public void a(RecyclerView recyclerView, int i, int i2, int i3) {
        this.f1779g.a();
    }

    public RecyclerView.LayoutParams a() {
        if (this.i == 0) {
            return new LayoutParams(-2, -1);
        }
        return new LayoutParams(-1, -2);
    }

    public RecyclerView.LayoutParams a(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    public RecyclerView.LayoutParams a(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    public boolean a(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    private void M() {
        int y;
        if (f() == 1) {
            y = (x() - B()) - z();
        } else {
            y = (y() - C()) - A();
        }
        m(y);
    }

    public void a(Rect rect, int i, int i2) {
        int a2;
        int a3;
        if (this.f1775c == null) {
            super.a(rect, i, i2);
        }
        int B = B() + z();
        int A = A() + C();
        if (this.i == 1) {
            a3 = a(i2, A + rect.height(), G());
            a2 = a(i, B + this.f1775c[this.f1775c.length - 1], F());
        } else {
            a2 = a(i, B + rect.width(), F());
            a3 = a(i2, A + this.f1775c[this.f1775c.length - 1], G());
        }
        g(a2, a3);
    }

    private void m(int i) {
        this.f1775c = a(this.f1775c, this.f1774b, i);
    }

    static int[] a(int[] iArr, int i, int i2) {
        int i3;
        int i4 = 0;
        if (!(iArr != null && iArr.length == i + 1 && iArr[iArr.length - 1] == i2)) {
            iArr = new int[(i + 1)];
        }
        iArr[0] = 0;
        int i5 = i2 / i;
        int i6 = i2 % i;
        int i7 = 0;
        for (int i8 = 1; i8 <= i; i8++) {
            i4 += i6;
            if (i4 <= 0 || i - i4 >= i6) {
                i3 = i5;
            } else {
                i3 = i5 + 1;
                i4 -= i;
            }
            i7 += i3;
            iArr[i8] = i7;
        }
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public int a(int i, int i2) {
        if (this.i != 1 || !g()) {
            return this.f1775c[i + i2] - this.f1775c[i];
        }
        return this.f1775c[this.f1774b - i] - this.f1775c[(this.f1774b - i) - i2];
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.n nVar, RecyclerView.r rVar, LinearLayoutManager.a aVar, int i) {
        super.a(nVar, rVar, aVar, i);
        M();
        if (rVar.e() > 0 && !rVar.a()) {
            b(nVar, rVar, aVar, i);
        }
        N();
    }

    private void N() {
        if (this.f1776d == null || this.f1776d.length != this.f1774b) {
            this.f1776d = new View[this.f1774b];
        }
    }

    public int a(int i, RecyclerView.n nVar, RecyclerView.r rVar) {
        M();
        N();
        return super.a(i, nVar, rVar);
    }

    public int b(int i, RecyclerView.n nVar, RecyclerView.r rVar) {
        M();
        N();
        return super.b(i, nVar, rVar);
    }

    private void b(RecyclerView.n nVar, RecyclerView.r rVar, LinearLayoutManager.a aVar, int i) {
        boolean z = true;
        if (i != 1) {
            z = false;
        }
        int b2 = b(nVar, rVar, aVar.f1798a);
        if (z) {
            while (b2 > 0 && aVar.f1798a > 0) {
                aVar.f1798a--;
                b2 = b(nVar, rVar, aVar.f1798a);
            }
            return;
        }
        int e2 = rVar.e() - 1;
        int i2 = aVar.f1798a;
        int i3 = b2;
        while (i2 < e2) {
            int b3 = b(nVar, rVar, i2 + 1);
            if (b3 <= i3) {
                break;
            }
            i2++;
            i3 = b3;
        }
        aVar.f1798a = i2;
    }

    /* access modifiers changed from: package-private */
    public View a(RecyclerView.n nVar, RecyclerView.r rVar, int i, int i2, int i3) {
        View view;
        View view2 = null;
        h();
        int c2 = this.j.c();
        int d2 = this.j.d();
        int i4 = i2 > i ? 1 : -1;
        View view3 = null;
        while (i != i2) {
            View i5 = i(i);
            int d3 = d(i5);
            if (d3 >= 0 && d3 < i3) {
                if (b(nVar, rVar, d3) != 0) {
                    view = view2;
                    i5 = view3;
                } else if (((RecyclerView.LayoutParams) i5.getLayoutParams()).d()) {
                    if (view3 == null) {
                        view = view2;
                    }
                } else if (this.j.a(i5) < d2 && this.j.b(i5) >= c2) {
                    return i5;
                } else {
                    if (view2 == null) {
                        view = i5;
                        i5 = view3;
                    }
                }
                i += i4;
                view2 = view;
                view3 = i5;
            }
            view = view2;
            i5 = view3;
            i += i4;
            view2 = view;
            view3 = i5;
        }
        if (view2 == null) {
            view2 = view3;
        }
        return view2;
    }

    private int a(RecyclerView.n nVar, RecyclerView.r rVar, int i) {
        if (!rVar.a()) {
            return this.f1779g.c(i, this.f1774b);
        }
        int b2 = nVar.b(i);
        if (b2 != -1) {
            return this.f1779g.c(b2, this.f1774b);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. " + i);
        return 0;
    }

    private int b(RecyclerView.n nVar, RecyclerView.r rVar, int i) {
        if (!rVar.a()) {
            return this.f1779g.b(i, this.f1774b);
        }
        int i2 = this.f1778f.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int b2 = nVar.b(i);
        if (b2 != -1) {
            return this.f1779g.b(b2, this.f1774b);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i);
        return 0;
    }

    private int c(RecyclerView.n nVar, RecyclerView.r rVar, int i) {
        if (!rVar.a()) {
            return this.f1779g.a(i);
        }
        int i2 = this.f1777e.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int b2 = nVar.b(i);
        if (b2 != -1) {
            return this.f1779g.a(b2);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i);
        return 1;
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.r rVar, LinearLayoutManager.c cVar, RecyclerView.h.a aVar) {
        int i = this.f1774b;
        for (int i2 = 0; i2 < this.f1774b && cVar.a(rVar) && i > 0; i2++) {
            int i3 = cVar.f1810d;
            aVar.b(i3, Math.max(0, cVar.f1813g));
            i -= this.f1779g.a(i3);
            cVar.f1810d += cVar.f1811e;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.GridLayoutManager.a(android.view.View, int, boolean):void
     arg types: [android.view.View, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int):int
      android.support.v7.widget.GridLayoutManager.a(int[], int, int):int[]
      android.support.v7.widget.GridLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.GridLayoutManager.a(android.graphics.Rect, int, int):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a):void
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
      android.support.v7.widget.RecyclerView.h.a(int, int, int):int
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
      android.support.v7.widget.GridLayoutManager.a(android.view.View, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.GridLayoutManager.a(int, int):int
     arg types: [int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.a(float, int):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.GridLayoutManager.a(android.content.Context, android.util.AttributeSet):android.support.v7.widget.RecyclerView$LayoutParams
      android.support.v7.widget.LinearLayoutManager.a(boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a):boolean
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(int, android.view.View):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$h, android.support.v7.widget.RecyclerView$q):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet):android.support.v7.widget.RecyclerView$LayoutParams
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$a, android.support.v7.widget.RecyclerView$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(int, android.os.Bundle):boolean
      android.support.v7.widget.GridLayoutManager.a(int, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.RecyclerView.h.a(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int, boolean):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int, int):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int, int):android.view.View
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(int, int, int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.GridLayoutManager.a(android.view.View, int, int, boolean):void
     arg types: [android.view.View, int, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, android.support.v7.widget.RecyclerView$r):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$r, boolean):int
      android.support.v7.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.LinearLayoutManager$b):void
      android.support.v7.widget.LinearLayoutManager.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet, int, int):android.support.v7.widget.RecyclerView$h$b
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):android.view.View
      android.support.v7.widget.RecyclerView.h.a(int, int, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int, java.lang.Object):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, java.util.ArrayList<android.view.View>, int, int):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, android.support.v7.widget.RecyclerView$LayoutParams):boolean
      android.support.v7.widget.helper.ItemTouchHelper.d.a(android.view.View, android.view.View, int, int):void
      android.support.v7.widget.GridLayoutManager.a(android.view.View, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(RecyclerView.n nVar, RecyclerView.r rVar, LinearLayoutManager.c cVar, LinearLayoutManager.b bVar) {
        int i;
        int i2;
        int i3;
        int makeMeasureSpec;
        int a2;
        View a3;
        int i4 = this.j.i();
        boolean z = i4 != 1073741824;
        int i5 = u() > 0 ? this.f1775c[this.f1774b] : 0;
        if (z) {
            M();
        }
        boolean z2 = cVar.f1811e == 1;
        int i6 = 0;
        int i7 = 0;
        int i8 = this.f1774b;
        if (!z2) {
            i8 = b(nVar, rVar, cVar.f1810d) + c(nVar, rVar, cVar.f1810d);
        }
        while (i6 < this.f1774b && cVar.a(rVar) && i8 > 0) {
            int i9 = cVar.f1810d;
            int c2 = c(nVar, rVar, i9);
            if (c2 <= this.f1774b) {
                i8 -= c2;
                if (i8 < 0 || (a3 = cVar.a(nVar)) == null) {
                    break;
                }
                i7 += c2;
                this.f1776d[i6] = a3;
                i6++;
            } else {
                throw new IllegalArgumentException("Item at position " + i9 + " requires " + c2 + " spans but GridLayoutManager has only " + this.f1774b + " spans.");
            }
        }
        if (i6 == 0) {
            bVar.f1804b = true;
            return;
        }
        a(nVar, rVar, i6, i7, z2);
        int i10 = 0;
        float f2 = 0.0f;
        int i11 = 0;
        while (i10 < i6) {
            View view = this.f1776d[i10];
            if (cVar.k == null) {
                if (z2) {
                    b(view);
                } else {
                    b(view, 0);
                }
            } else if (z2) {
                a(view);
            } else {
                a(view, 0);
            }
            b(view, this.f1780h);
            a(view, i4, false);
            int e2 = this.j.e(view);
            if (e2 > i11) {
                i11 = e2;
            }
            float f3 = (((float) this.j.f(view)) * 1.0f) / ((float) ((LayoutParams) view.getLayoutParams()).f1782b);
            if (f3 <= f2) {
                f3 = f2;
            }
            i10++;
            f2 = f3;
        }
        if (z) {
            a(f2, i5);
            i11 = 0;
            int i12 = 0;
            while (i12 < i6) {
                View view2 = this.f1776d[i12];
                a(view2, 1073741824, true);
                int e3 = this.j.e(view2);
                if (e3 <= i11) {
                    e3 = i11;
                }
                i12++;
                i11 = e3;
            }
        }
        for (int i13 = 0; i13 < i6; i13++) {
            View view3 = this.f1776d[i13];
            if (this.j.e(view3) != i11) {
                LayoutParams layoutParams = (LayoutParams) view3.getLayoutParams();
                Rect rect = layoutParams.f1860d;
                int i14 = rect.top + rect.bottom + layoutParams.topMargin + layoutParams.bottomMargin;
                int i15 = rect.right + rect.left + layoutParams.leftMargin + layoutParams.rightMargin;
                int a4 = a(layoutParams.f1781a, layoutParams.f1782b);
                if (this.i == 1) {
                    makeMeasureSpec = a(a4, 1073741824, i15, layoutParams.width, false);
                    a2 = View.MeasureSpec.makeMeasureSpec(i11 - i14, 1073741824);
                } else {
                    makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i11 - i15, 1073741824);
                    a2 = a(a4, 1073741824, i14, layoutParams.height, false);
                }
                a(view3, makeMeasureSpec, a2, true);
            }
        }
        bVar.f1803a = i11;
        int i16 = 0;
        if (this.i == 1) {
            if (cVar.f1812f == -1) {
                i16 = cVar.f1808b;
                i3 = i16 - i11;
                i2 = 0;
                i = 0;
            } else {
                int i17 = cVar.f1808b;
                i16 = i17 + i11;
                i3 = i17;
                i2 = 0;
                i = 0;
            }
        } else if (cVar.f1812f == -1) {
            int i18 = cVar.f1808b;
            i2 = i18;
            i = i18 - i11;
            i3 = 0;
        } else {
            i = cVar.f1808b;
            i2 = i11 + i;
            i3 = 0;
        }
        int i19 = i16;
        int i20 = i3;
        int i21 = i2;
        int i22 = i;
        for (int i23 = 0; i23 < i6; i23++) {
            View view4 = this.f1776d[i23];
            LayoutParams layoutParams2 = (LayoutParams) view4.getLayoutParams();
            if (this.i != 1) {
                i20 = A() + this.f1775c[layoutParams2.f1781a];
                i19 = i20 + this.j.f(view4);
            } else if (g()) {
                i21 = z() + this.f1775c[this.f1774b - layoutParams2.f1781a];
                i22 = i21 - this.j.f(view4);
            } else {
                i22 = z() + this.f1775c[layoutParams2.f1781a];
                i21 = i22 + this.j.f(view4);
            }
            a(view4, i22, i20, i21, i19);
            if (layoutParams2.d() || layoutParams2.e()) {
                bVar.f1805c = true;
            }
            bVar.f1806d |= view4.hasFocusable();
        }
        Arrays.fill(this.f1776d, (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.GridLayoutManager.a(int, int):int
     arg types: [int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.a(float, int):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.GridLayoutManager.a(android.content.Context, android.util.AttributeSet):android.support.v7.widget.RecyclerView$LayoutParams
      android.support.v7.widget.LinearLayoutManager.a(boolean, boolean):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.LinearLayoutManager$c):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a):boolean
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(int, android.view.View):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$h, android.support.v7.widget.RecyclerView$q):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.a(android.content.Context, android.util.AttributeSet):android.support.v7.widget.RecyclerView$LayoutParams
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$a, android.support.v7.widget.RecyclerView$a):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.graphics.Rect):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, android.support.v7.widget.RecyclerView$n):void
      android.support.v7.widget.RecyclerView.h.a(int, android.os.Bundle):boolean
      android.support.v7.widget.GridLayoutManager.a(int, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.RecyclerView.h.a(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int, boolean):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int, int):android.view.View
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int, int, int):android.view.View
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      android.support.v7.widget.RecyclerView.h.a(int, int, int, int, boolean):int */
    private void a(View view, int i, boolean z) {
        int a2;
        int i2;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        Rect rect = layoutParams.f1860d;
        int i3 = rect.top + rect.bottom + layoutParams.topMargin + layoutParams.bottomMargin;
        int i4 = layoutParams.rightMargin + rect.right + rect.left + layoutParams.leftMargin;
        int a3 = a(layoutParams.f1781a, layoutParams.f1782b);
        if (this.i == 1) {
            a2 = a(a3, i, i4, layoutParams.width, false);
            i2 = a(this.j.f(), w(), i3, layoutParams.height, true);
        } else {
            int a4 = a(a3, i, i3, layoutParams.height, false);
            a2 = a(this.j.f(), v(), i4, layoutParams.width, true);
            i2 = a4;
        }
        a(view, a2, i2, z);
    }

    private void a(float f2, int i) {
        m(Math.max(Math.round(((float) this.f1774b) * f2), i));
    }

    private void a(View view, int i, int i2, boolean z) {
        boolean b2;
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        if (z) {
            b2 = a(view, i, i2, layoutParams);
        } else {
            b2 = b(view, i, i2, layoutParams);
        }
        if (b2) {
            view.measure(i, i2);
        }
    }

    private void a(RecyclerView.n nVar, RecyclerView.r rVar, int i, int i2, boolean z) {
        int i3;
        int i4;
        if (z) {
            i4 = 1;
            i3 = 0;
        } else {
            int i5 = i - 1;
            i = -1;
            i3 = i5;
            i4 = -1;
        }
        int i6 = 0;
        for (int i7 = i3; i7 != i; i7 += i4) {
            View view = this.f1776d[i7];
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            layoutParams.f1782b = c(nVar, rVar, d(view));
            layoutParams.f1781a = i6;
            i6 += layoutParams.f1782b;
        }
    }

    public void a(int i) {
        if (i != this.f1774b) {
            this.f1773a = true;
            if (i < 1) {
                throw new IllegalArgumentException("Span count should be at least 1. Provided " + i);
            }
            this.f1774b = i;
            this.f1779g.a();
            n();
        }
    }

    public static abstract class a {

        /* renamed from: a  reason: collision with root package name */
        final SparseIntArray f1783a = new SparseIntArray();

        /* renamed from: b  reason: collision with root package name */
        private boolean f1784b = false;

        public abstract int a(int i);

        public void a() {
            this.f1783a.clear();
        }

        /* access modifiers changed from: package-private */
        public int b(int i, int i2) {
            if (!this.f1784b) {
                return a(i, i2);
            }
            int i3 = this.f1783a.get(i, -1);
            if (i3 != -1) {
                return i3;
            }
            int a2 = a(i, i2);
            this.f1783a.put(i, a2);
            return a2;
        }

        public int a(int i, int i2) {
            int i3;
            int i4;
            int b2;
            int a2 = a(i);
            if (a2 == i2) {
                return 0;
            }
            if (!this.f1784b || this.f1783a.size() <= 0 || (b2 = b(i)) < 0) {
                i3 = 0;
                i4 = 0;
            } else {
                i4 = this.f1783a.get(b2) + a(b2);
                i3 = b2 + 1;
            }
            int i5 = i3;
            while (i5 < i) {
                int a3 = a(i5);
                int i6 = i4 + a3;
                if (i6 == i2) {
                    a3 = 0;
                } else if (i6 <= i2) {
                    a3 = i6;
                }
                i5++;
                i4 = a3;
            }
            if (i4 + a2 <= i2) {
                return i4;
            }
            return 0;
        }

        /* access modifiers changed from: package-private */
        public int b(int i) {
            int i2 = 0;
            int size = this.f1783a.size() - 1;
            while (i2 <= size) {
                int i3 = (i2 + size) >>> 1;
                if (this.f1783a.keyAt(i3) < i) {
                    i2 = i3 + 1;
                } else {
                    size = i3 - 1;
                }
            }
            int i4 = i2 - 1;
            if (i4 < 0 || i4 >= this.f1783a.size()) {
                return -1;
            }
            return this.f1783a.keyAt(i4);
        }

        public int c(int i, int i2) {
            int a2 = a(i);
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            while (i3 < i) {
                int a3 = a(i3);
                int i6 = i5 + a3;
                if (i6 == i2) {
                    i4++;
                    a3 = 0;
                } else if (i6 > i2) {
                    i4++;
                } else {
                    a3 = i6;
                }
                i3++;
                i5 = a3;
            }
            if (i5 + a2 > i2) {
                return i4 + 1;
            }
            return i4;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
     arg types: [android.view.View, int, int]
     candidates:
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, int):int
      android.support.v7.widget.GridLayoutManager.a(android.view.View, int, boolean):void
      android.support.v7.widget.GridLayoutManager.a(int[], int, int):int[]
      android.support.v7.widget.GridLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.GridLayoutManager.a(android.graphics.Rect, int, int):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.GridLayoutManager.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, int, int):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$a):void
      android.support.v7.widget.LinearLayoutManager.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView$r, android.support.v7.widget.LinearLayoutManager$c, android.support.v7.widget.RecyclerView$h$a):void
      android.support.v7.widget.LinearLayoutManager.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
      android.support.v7.widget.RecyclerView.h.a(int, int, int):int
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void
      android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
      android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
      android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
      android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
      android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean */
    public View a(View view, int i, RecyclerView.n nVar, RecyclerView.r rVar) {
        int i2;
        int i3;
        int u;
        int i4;
        int min;
        View view2;
        int i5;
        int i6;
        View view3;
        View e2 = e(view);
        if (e2 == null) {
            return null;
        }
        LayoutParams layoutParams = (LayoutParams) e2.getLayoutParams();
        int i7 = layoutParams.f1781a;
        int i8 = layoutParams.f1781a + layoutParams.f1782b;
        if (super.a(view, i, nVar, rVar) == null) {
            return null;
        }
        if ((f(i) == 1) != this.k) {
            i2 = u() - 1;
            i3 = -1;
            u = -1;
        } else {
            i2 = 0;
            i3 = 1;
            u = u();
        }
        boolean z = this.i == 1 && g();
        View view4 = null;
        int i9 = -1;
        int i10 = 0;
        View view5 = null;
        int i11 = -1;
        int i12 = 0;
        int a2 = a(nVar, rVar, i2);
        int i13 = i2;
        while (i13 != u) {
            int a3 = a(nVar, rVar, i13);
            View i14 = i(i13);
            if (i14 == e2) {
                break;
            }
            if (i14.hasFocusable() && a3 != a2) {
                if (view4 != null) {
                    break;
                }
            } else {
                LayoutParams layoutParams2 = (LayoutParams) i14.getLayoutParams();
                int i15 = layoutParams2.f1781a;
                int i16 = layoutParams2.f1781a + layoutParams2.f1782b;
                if (i14.hasFocusable() && i15 == i7 && i16 == i8) {
                    return i14;
                }
                boolean z2 = false;
                if ((!i14.hasFocusable() || view4 != null) && (i14.hasFocusable() || view5 != null)) {
                    int min2 = Math.min(i16, i8) - Math.max(i15, i7);
                    if (i14.hasFocusable()) {
                        if (min2 > i10) {
                            z2 = true;
                        } else if (min2 == i10) {
                            if (z == (i15 > i9)) {
                                z2 = true;
                            }
                        }
                    } else if (view4 == null && a(i14, false, true)) {
                        if (min2 > i12) {
                            z2 = true;
                        } else if (min2 == i12) {
                            if (z == (i15 > i11)) {
                                z2 = true;
                            }
                        }
                    }
                } else {
                    z2 = true;
                }
                if (z2) {
                    if (i14.hasFocusable()) {
                        int i17 = layoutParams2.f1781a;
                        int i18 = i12;
                        i4 = i11;
                        view2 = view5;
                        i5 = Math.min(i16, i8) - Math.max(i15, i7);
                        min = i18;
                        int i19 = i17;
                        view3 = i14;
                        i6 = i19;
                    } else {
                        i4 = layoutParams2.f1781a;
                        min = Math.min(i16, i8) - Math.max(i15, i7);
                        view2 = i14;
                        i5 = i10;
                        i6 = i9;
                        view3 = view4;
                    }
                    i13 += i3;
                    view4 = view3;
                    i10 = i5;
                    i9 = i6;
                    view5 = view2;
                    i11 = i4;
                    i12 = min;
                }
            }
            min = i12;
            i6 = i9;
            i4 = i11;
            view2 = view5;
            i5 = i10;
            view3 = view4;
            i13 += i3;
            view4 = view3;
            i10 = i5;
            i9 = i6;
            view5 = view2;
            i11 = i4;
            i12 = min;
        }
        if (view4 == null) {
            view4 = view5;
        }
        return view4;
    }

    public boolean b() {
        return this.n == null && !this.f1773a;
    }

    public static final class DefaultSpanSizeLookup extends a {
        public int a(int i) {
            return 1;
        }

        public int a(int i, int i2) {
            return i % i2;
        }
    }

    public static class LayoutParams extends RecyclerView.LayoutParams {

        /* renamed from: a  reason: collision with root package name */
        int f1781a = -1;

        /* renamed from: b  reason: collision with root package name */
        int f1782b = 0;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public int a() {
            return this.f1781a;
        }

        public int b() {
            return this.f1782b;
        }
    }
}
