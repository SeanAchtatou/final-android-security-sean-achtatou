package android.support.v7.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

/* compiled from: ActivityChooserModel */
class c extends DataSetObservable {

    /* renamed from: a  reason: collision with root package name */
    static final String f2160a = c.class.getSimpleName();

    /* renamed from: e  reason: collision with root package name */
    private static final Object f2161e = new Object();

    /* renamed from: f  reason: collision with root package name */
    private static final Map<String, c> f2162f = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    final Context f2163b;

    /* renamed from: c  reason: collision with root package name */
    final String f2164c;

    /* renamed from: d  reason: collision with root package name */
    boolean f2165d = true;

    /* renamed from: g  reason: collision with root package name */
    private final Object f2166g = new Object();

    /* renamed from: h  reason: collision with root package name */
    private final List<a> f2167h = new ArrayList();
    private final List<d> i = new ArrayList();
    private Intent j;
    private b k = new C0030c();
    private int l = 50;
    private boolean m = false;
    private boolean n = true;
    private boolean o = false;
    private e p;

    /* compiled from: ActivityChooserModel */
    public interface b {
        void a(Intent intent, List<a> list, List<d> list2);
    }

    /* compiled from: ActivityChooserModel */
    public interface e {
        boolean a(c cVar, Intent intent);
    }

    public static c a(Context context, String str) {
        c cVar;
        synchronized (f2161e) {
            cVar = f2162f.get(str);
            if (cVar == null) {
                cVar = new c(context, str);
                f2162f.put(str, cVar);
            }
        }
        return cVar;
    }

    private c(Context context, String str) {
        this.f2163b = context.getApplicationContext();
        if (TextUtils.isEmpty(str) || str.endsWith(".xml")) {
            this.f2164c = str;
        } else {
            this.f2164c = str + ".xml";
        }
    }

    public int a() {
        int size;
        synchronized (this.f2166g) {
            e();
            size = this.f2167h.size();
        }
        return size;
    }

    public ResolveInfo a(int i2) {
        ResolveInfo resolveInfo;
        synchronized (this.f2166g) {
            e();
            resolveInfo = this.f2167h.get(i2).f2168a;
        }
        return resolveInfo;
    }

    public int a(ResolveInfo resolveInfo) {
        synchronized (this.f2166g) {
            e();
            List<a> list = this.f2167h;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (list.get(i2).f2168a == resolveInfo) {
                    return i2;
                }
            }
            return -1;
        }
    }

    public Intent b(int i2) {
        synchronized (this.f2166g) {
            if (this.j == null) {
                return null;
            }
            e();
            a aVar = this.f2167h.get(i2);
            ComponentName componentName = new ComponentName(aVar.f2168a.activityInfo.packageName, aVar.f2168a.activityInfo.name);
            Intent intent = new Intent(this.j);
            intent.setComponent(componentName);
            if (this.p != null) {
                if (this.p.a(this, new Intent(intent))) {
                    return null;
                }
            }
            a(new d(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    public ResolveInfo b() {
        synchronized (this.f2166g) {
            e();
            if (this.f2167h.isEmpty()) {
                return null;
            }
            ResolveInfo resolveInfo = this.f2167h.get(0).f2168a;
            return resolveInfo;
        }
    }

    public void c(int i2) {
        float f2;
        synchronized (this.f2166g) {
            e();
            a aVar = this.f2167h.get(i2);
            a aVar2 = this.f2167h.get(0);
            if (aVar2 != null) {
                f2 = (aVar2.f2169b - aVar.f2169b) + 5.0f;
            } else {
                f2 = 1.0f;
            }
            a(new d(new ComponentName(aVar.f2168a.activityInfo.packageName, aVar.f2168a.activityInfo.name), System.currentTimeMillis(), f2));
        }
    }

    private void d() {
        if (!this.m) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.n) {
            this.n = false;
            if (!TextUtils.isEmpty(this.f2164c)) {
                android.support.v4.os.a.a(new f(), new ArrayList(this.i), this.f2164c);
            }
        }
    }

    public int c() {
        int size;
        synchronized (this.f2166g) {
            e();
            size = this.i.size();
        }
        return size;
    }

    private void e() {
        boolean g2 = g() | h();
        i();
        if (g2) {
            f();
            notifyChanged();
        }
    }

    private boolean f() {
        if (this.k == null || this.j == null || this.f2167h.isEmpty() || this.i.isEmpty()) {
            return false;
        }
        this.k.a(this.j, this.f2167h, Collections.unmodifiableList(this.i));
        return true;
    }

    private boolean g() {
        if (!this.o || this.j == null) {
            return false;
        }
        this.o = false;
        this.f2167h.clear();
        List<ResolveInfo> queryIntentActivities = this.f2163b.getPackageManager().queryIntentActivities(this.j, 0);
        int size = queryIntentActivities.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.f2167h.add(new a(queryIntentActivities.get(i2)));
        }
        return true;
    }

    private boolean h() {
        if (!this.f2165d || !this.n || TextUtils.isEmpty(this.f2164c)) {
            return false;
        }
        this.f2165d = false;
        this.m = true;
        j();
        return true;
    }

    private boolean a(d dVar) {
        boolean add = this.i.add(dVar);
        if (add) {
            this.n = true;
            i();
            d();
            f();
            notifyChanged();
        }
        return add;
    }

    private void i() {
        int size = this.i.size() - this.l;
        if (size > 0) {
            this.n = true;
            for (int i2 = 0; i2 < size; i2++) {
                d remove = this.i.remove(0);
            }
        }
    }

    /* compiled from: ActivityChooserModel */
    public static final class d {

        /* renamed from: a  reason: collision with root package name */
        public final ComponentName f2173a;

        /* renamed from: b  reason: collision with root package name */
        public final long f2174b;

        /* renamed from: c  reason: collision with root package name */
        public final float f2175c;

        public d(String str, long j, float f2) {
            this(ComponentName.unflattenFromString(str), j, f2);
        }

        public d(ComponentName componentName, long j, float f2) {
            this.f2173a = componentName;
            this.f2174b = j;
            this.f2175c = f2;
        }

        public int hashCode() {
            return (((((this.f2173a == null ? 0 : this.f2173a.hashCode()) + 31) * 31) + ((int) (this.f2174b ^ (this.f2174b >>> 32)))) * 31) + Float.floatToIntBits(this.f2175c);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            if (this.f2173a == null) {
                if (dVar.f2173a != null) {
                    return false;
                }
            } else if (!this.f2173a.equals(dVar.f2173a)) {
                return false;
            }
            if (this.f2174b != dVar.f2174b) {
                return false;
            }
            if (Float.floatToIntBits(this.f2175c) != Float.floatToIntBits(dVar.f2175c)) {
                return false;
            }
            return true;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("; activity:").append(this.f2173a);
            sb.append("; time:").append(this.f2174b);
            sb.append("; weight:").append(new BigDecimal((double) this.f2175c));
            sb.append("]");
            return sb.toString();
        }
    }

    /* compiled from: ActivityChooserModel */
    public final class a implements Comparable<a> {

        /* renamed from: a  reason: collision with root package name */
        public final ResolveInfo f2168a;

        /* renamed from: b  reason: collision with root package name */
        public float f2169b;

        public a(ResolveInfo resolveInfo) {
            this.f2168a = resolveInfo;
        }

        public int hashCode() {
            return Float.floatToIntBits(this.f2169b) + 31;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            if (Float.floatToIntBits(this.f2169b) != Float.floatToIntBits(((a) obj).f2169b)) {
                return false;
            }
            return true;
        }

        /* renamed from: a */
        public int compareTo(a aVar) {
            return Float.floatToIntBits(aVar.f2169b) - Float.floatToIntBits(this.f2169b);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("resolveInfo:").append(this.f2168a.toString());
            sb.append("; weight:").append(new BigDecimal((double) this.f2169b));
            sb.append("]");
            return sb.toString();
        }
    }

    /* renamed from: android.support.v7.widget.c$c  reason: collision with other inner class name */
    /* compiled from: ActivityChooserModel */
    private final class C0030c implements b {

        /* renamed from: b  reason: collision with root package name */
        private final Map<ComponentName, a> f2172b = new HashMap();

        C0030c() {
        }

        public void a(Intent intent, List<a> list, List<d> list2) {
            float f2;
            Map<ComponentName, a> map = this.f2172b;
            map.clear();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                a aVar = list.get(i);
                aVar.f2169b = 0.0f;
                map.put(new ComponentName(aVar.f2168a.activityInfo.packageName, aVar.f2168a.activityInfo.name), aVar);
            }
            float f3 = 1.0f;
            int size2 = list2.size() - 1;
            while (size2 >= 0) {
                d dVar = list2.get(size2);
                a aVar2 = map.get(dVar.f2173a);
                if (aVar2 != null) {
                    aVar2.f2169b = (dVar.f2175c * f3) + aVar2.f2169b;
                    f2 = 0.95f * f3;
                } else {
                    f2 = f3;
                }
                size2--;
                f3 = f2;
            }
            Collections.sort(list);
        }
    }

    private void j() {
        try {
            FileInputStream openFileInput = this.f2163b.openFileInput(this.f2164c);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(openFileInput, "UTF-8");
                int i2 = 0;
                while (i2 != 1 && i2 != 2) {
                    i2 = newPullParser.next();
                }
                if (!"historical-records".equals(newPullParser.getName())) {
                    throw new XmlPullParserException("Share records file does not start with historical-records tag.");
                }
                List<d> list = this.i;
                list.clear();
                while (true) {
                    int next = newPullParser.next();
                    if (next == 1) {
                        if (openFileInput != null) {
                            try {
                                openFileInput.close();
                                return;
                            } catch (IOException e2) {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else if (!(next == 3 || next == 4)) {
                        if (!"historical-record".equals(newPullParser.getName())) {
                            throw new XmlPullParserException("Share records file not well-formed.");
                        }
                        list.add(new d(newPullParser.getAttributeValue(null, ServiceManagerNative.ACTIVITY), Long.parseLong(newPullParser.getAttributeValue(null, "time")), Float.parseFloat(newPullParser.getAttributeValue(null, "weight"))));
                    }
                }
            } catch (XmlPullParserException e3) {
                Log.e(f2160a, "Error reading historical recrod file: " + this.f2164c, e3);
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e4) {
                    }
                }
            } catch (IOException e5) {
                Log.e(f2160a, "Error reading historical recrod file: " + this.f2164c, e5);
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e6) {
                    }
                }
            } catch (Throwable th) {
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e7) {
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException e8) {
        }
    }

    /* compiled from: ActivityChooserModel */
    private final class f extends AsyncTask<Object, Void, Void> {
        f() {
        }

        /* renamed from: a */
        public Void doInBackground(Object... objArr) {
            List list = (List) objArr[0];
            String str = (String) objArr[1];
            try {
                FileOutputStream openFileOutput = c.this.f2163b.openFileOutput(str, 0);
                XmlSerializer newSerializer = Xml.newSerializer();
                try {
                    newSerializer.setOutput(openFileOutput, null);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag(null, "historical-records");
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        d dVar = (d) list.remove(0);
                        newSerializer.startTag(null, "historical-record");
                        newSerializer.attribute(null, ServiceManagerNative.ACTIVITY, dVar.f2173a.flattenToString());
                        newSerializer.attribute(null, "time", String.valueOf(dVar.f2174b));
                        newSerializer.attribute(null, "weight", String.valueOf(dVar.f2175c));
                        newSerializer.endTag(null, "historical-record");
                    }
                    newSerializer.endTag(null, "historical-records");
                    newSerializer.endDocument();
                    c.this.f2165d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e2) {
                        }
                    }
                } catch (IllegalArgumentException e3) {
                    Log.e(c.f2160a, "Error writing historical record file: " + c.this.f2164c, e3);
                    c.this.f2165d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e4) {
                        }
                    }
                } catch (IllegalStateException e5) {
                    Log.e(c.f2160a, "Error writing historical record file: " + c.this.f2164c, e5);
                    c.this.f2165d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e6) {
                        }
                    }
                } catch (IOException e7) {
                    Log.e(c.f2160a, "Error writing historical record file: " + c.this.f2164c, e7);
                    c.this.f2165d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e8) {
                        }
                    }
                } catch (Throwable th) {
                    c.this.f2165d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e9) {
                        }
                    }
                    throw th;
                }
            } catch (FileNotFoundException e10) {
                Log.e(c.f2160a, "Error writing historical record file: " + str, e10);
            }
            return null;
        }
    }
}
