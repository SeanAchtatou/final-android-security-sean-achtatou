package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.ag;
import android.support.v4.view.ay;
import android.support.v7.a.a;
import android.support.v7.c.a.b;
import android.support.v7.view.menu.ActionMenuItem;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.i;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

public class ToolbarWidgetWrapper implements t {

    /* renamed from: a  reason: collision with root package name */
    Toolbar f2073a;

    /* renamed from: b  reason: collision with root package name */
    CharSequence f2074b;

    /* renamed from: c  reason: collision with root package name */
    Window.Callback f2075c;

    /* renamed from: d  reason: collision with root package name */
    boolean f2076d;

    /* renamed from: e  reason: collision with root package name */
    private int f2077e;

    /* renamed from: f  reason: collision with root package name */
    private View f2078f;

    /* renamed from: g  reason: collision with root package name */
    private View f2079g;

    /* renamed from: h  reason: collision with root package name */
    private Drawable f2080h;
    private Drawable i;
    private Drawable j;
    private boolean k;
    private CharSequence l;
    private CharSequence m;
    private ActionMenuPresenter n;
    private int o;
    private int p;
    private Drawable q;

    public ToolbarWidgetWrapper(Toolbar toolbar, boolean z) {
        this(toolbar, z, a.i.abc_action_bar_up_description, a.e.abc_ic_ab_back_material);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.Toolbar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ToolbarWidgetWrapper(Toolbar toolbar, boolean z, int i2, int i3) {
        this.o = 0;
        this.p = 0;
        this.f2073a = toolbar;
        this.f2074b = toolbar.getTitle();
        this.l = toolbar.getSubtitle();
        this.k = this.f2074b != null;
        this.j = toolbar.getNavigationIcon();
        an a2 = an.a(toolbar.getContext(), null, a.k.ActionBar, a.C0027a.actionBarStyle, 0);
        this.q = a2.a(a.k.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence c2 = a2.c(a.k.ActionBar_title);
            if (!TextUtils.isEmpty(c2)) {
                b(c2);
            }
            CharSequence c3 = a2.c(a.k.ActionBar_subtitle);
            if (!TextUtils.isEmpty(c3)) {
                c(c3);
            }
            Drawable a3 = a2.a(a.k.ActionBar_logo);
            if (a3 != null) {
                b(a3);
            }
            Drawable a4 = a2.a(a.k.ActionBar_icon);
            if (a4 != null) {
                a(a4);
            }
            if (this.j == null && this.q != null) {
                c(this.q);
            }
            c(a2.a(a.k.ActionBar_displayOptions, 0));
            int g2 = a2.g(a.k.ActionBar_customNavigationLayout, 0);
            if (g2 != 0) {
                a(LayoutInflater.from(this.f2073a.getContext()).inflate(g2, (ViewGroup) this.f2073a, false));
                c(this.f2077e | 16);
            }
            int f2 = a2.f(a.k.ActionBar_height, 0);
            if (f2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.f2073a.getLayoutParams();
                layoutParams.height = f2;
                this.f2073a.setLayoutParams(layoutParams);
            }
            int d2 = a2.d(a.k.ActionBar_contentInsetStart, -1);
            int d3 = a2.d(a.k.ActionBar_contentInsetEnd, -1);
            if (d2 >= 0 || d3 >= 0) {
                this.f2073a.a(Math.max(d2, 0), Math.max(d3, 0));
            }
            int g3 = a2.g(a.k.ActionBar_titleTextStyle, 0);
            if (g3 != 0) {
                this.f2073a.a(this.f2073a.getContext(), g3);
            }
            int g4 = a2.g(a.k.ActionBar_subtitleTextStyle, 0);
            if (g4 != 0) {
                this.f2073a.b(this.f2073a.getContext(), g4);
            }
            int g5 = a2.g(a.k.ActionBar_popupTheme, 0);
            if (g5 != 0) {
                this.f2073a.setPopupTheme(g5);
            }
        } else {
            this.f2077e = s();
        }
        a2.a();
        f(i2);
        this.m = this.f2073a.getNavigationContentDescription();
        this.f2073a.setNavigationOnClickListener(new View.OnClickListener() {

            /* renamed from: a  reason: collision with root package name */
            final ActionMenuItem f2081a = new ActionMenuItem(ToolbarWidgetWrapper.this.f2073a.getContext(), 0, 16908332, 0, 0, ToolbarWidgetWrapper.this.f2074b);

            public void onClick(View view) {
                if (ToolbarWidgetWrapper.this.f2075c != null && ToolbarWidgetWrapper.this.f2076d) {
                    ToolbarWidgetWrapper.this.f2075c.onMenuItemSelected(0, this.f2081a);
                }
            }
        });
    }

    public void f(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            if (TextUtils.isEmpty(this.f2073a.getNavigationContentDescription())) {
                g(this.p);
            }
        }
    }

    private int s() {
        if (this.f2073a.getNavigationIcon() == null) {
            return 11;
        }
        this.q = this.f2073a.getNavigationIcon();
        return 15;
    }

    public ViewGroup a() {
        return this.f2073a;
    }

    public Context b() {
        return this.f2073a.getContext();
    }

    public boolean c() {
        return this.f2073a.g();
    }

    public void d() {
        this.f2073a.h();
    }

    public void a(Window.Callback callback) {
        this.f2075c = callback;
    }

    public void a(CharSequence charSequence) {
        if (!this.k) {
            e(charSequence);
        }
    }

    public CharSequence e() {
        return this.f2073a.getTitle();
    }

    public void b(CharSequence charSequence) {
        this.k = true;
        e(charSequence);
    }

    private void e(CharSequence charSequence) {
        this.f2074b = charSequence;
        if ((this.f2077e & 8) != 0) {
            this.f2073a.setTitle(charSequence);
        }
    }

    public void c(CharSequence charSequence) {
        this.l = charSequence;
        if ((this.f2077e & 8) != 0) {
            this.f2073a.setSubtitle(charSequence);
        }
    }

    public void f() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void g() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void a(int i2) {
        a(i2 != 0 ? b.b(b(), i2) : null);
    }

    public void a(Drawable drawable) {
        this.f2080h = drawable;
        t();
    }

    public void b(int i2) {
        b(i2 != 0 ? b.b(b(), i2) : null);
    }

    public void b(Drawable drawable) {
        this.i = drawable;
        t();
    }

    private void t() {
        Drawable drawable = null;
        if ((this.f2077e & 2) != 0) {
            if ((this.f2077e & 1) != 0) {
                drawable = this.i != null ? this.i : this.f2080h;
            } else {
                drawable = this.f2080h;
            }
        }
        this.f2073a.setLogo(drawable);
    }

    public boolean h() {
        return this.f2073a.a();
    }

    public boolean i() {
        return this.f2073a.b();
    }

    public boolean j() {
        return this.f2073a.c();
    }

    public boolean k() {
        return this.f2073a.d();
    }

    public boolean l() {
        return this.f2073a.e();
    }

    public void m() {
        this.f2076d = true;
    }

    public void a(Menu menu, i.a aVar) {
        if (this.n == null) {
            this.n = new ActionMenuPresenter(this.f2073a.getContext());
            this.n.a(a.f.action_menu_presenter);
        }
        this.n.setCallback(aVar);
        this.f2073a.a((MenuBuilder) menu, this.n);
    }

    public void n() {
        this.f2073a.f();
    }

    public int o() {
        return this.f2077e;
    }

    public void c(int i2) {
        int i3 = this.f2077e ^ i2;
        this.f2077e = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    v();
                }
                u();
            }
            if ((i3 & 3) != 0) {
                t();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.f2073a.setTitle(this.f2074b);
                    this.f2073a.setSubtitle(this.l);
                } else {
                    this.f2073a.setTitle((CharSequence) null);
                    this.f2073a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && this.f2079g != null) {
                if ((i2 & 16) != 0) {
                    this.f2073a.addView(this.f2079g);
                } else {
                    this.f2073a.removeView(this.f2079g);
                }
            }
        }
    }

    public void a(ScrollingTabContainerView scrollingTabContainerView) {
        if (this.f2078f != null && this.f2078f.getParent() == this.f2073a) {
            this.f2073a.removeView(this.f2078f);
        }
        this.f2078f = scrollingTabContainerView;
        if (scrollingTabContainerView != null && this.o == 2) {
            this.f2073a.addView(this.f2078f, 0);
            Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) this.f2078f.getLayoutParams();
            layoutParams.width = -2;
            layoutParams.height = -2;
            layoutParams.f1232a = 8388691;
            scrollingTabContainerView.setAllowCollapse(true);
        }
    }

    public void a(boolean z) {
        this.f2073a.setCollapsible(z);
    }

    public void b(boolean z) {
    }

    public int p() {
        return this.o;
    }

    public void a(View view) {
        if (!(this.f2079g == null || (this.f2077e & 16) == 0)) {
            this.f2073a.removeView(this.f2079g);
        }
        this.f2079g = view;
        if (view != null && (this.f2077e & 16) != 0) {
            this.f2073a.addView(this.f2079g);
        }
    }

    public ay a(final int i2, long j2) {
        return ag.r(this.f2073a).a(i2 == 0 ? 1.0f : 0.0f).a(j2).a(new ViewPropertyAnimatorListenerAdapter() {

            /* renamed from: c  reason: collision with root package name */
            private boolean f2085c = false;

            public void onAnimationStart(View view) {
                ToolbarWidgetWrapper.this.f2073a.setVisibility(0);
            }

            public void onAnimationEnd(View view) {
                if (!this.f2085c) {
                    ToolbarWidgetWrapper.this.f2073a.setVisibility(i2);
                }
            }

            public void onAnimationCancel(View view) {
                this.f2085c = true;
            }
        });
    }

    public void c(Drawable drawable) {
        this.j = drawable;
        u();
    }

    public void d(int i2) {
        c(i2 != 0 ? b.b(b(), i2) : null);
    }

    private void u() {
        if ((this.f2077e & 4) != 0) {
            this.f2073a.setNavigationIcon(this.j != null ? this.j : this.q);
        } else {
            this.f2073a.setNavigationIcon((Drawable) null);
        }
    }

    public void d(CharSequence charSequence) {
        this.m = charSequence;
        v();
    }

    public void g(int i2) {
        d(i2 == 0 ? null : b().getString(i2));
    }

    private void v() {
        if ((this.f2077e & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.m)) {
            this.f2073a.setNavigationContentDescription(this.p);
        } else {
            this.f2073a.setNavigationContentDescription(this.m);
        }
    }

    public void e(int i2) {
        this.f2073a.setVisibility(i2);
    }

    public int q() {
        return this.f2073a.getVisibility();
    }

    public void a(i.a aVar, MenuBuilder.a aVar2) {
        this.f2073a.a(aVar, aVar2);
    }

    public Menu r() {
        return this.f2073a.getMenu();
    }
}
