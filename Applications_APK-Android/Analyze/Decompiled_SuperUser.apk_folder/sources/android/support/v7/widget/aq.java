package android.support.v7.widget;

import android.support.v4.util.ArrayMap;
import android.support.v4.util.Pools;
import android.support.v4.util.e;
import android.support.v7.widget.RecyclerView;

/* compiled from: ViewInfoStore */
class aq {

    /* renamed from: a  reason: collision with root package name */
    final ArrayMap<RecyclerView.u, a> f2153a = new ArrayMap<>();

    /* renamed from: b  reason: collision with root package name */
    final e<RecyclerView.u> f2154b = new e<>();

    /* compiled from: ViewInfoStore */
    interface b {
        void a(RecyclerView.u uVar);

        void a(RecyclerView.u uVar, RecyclerView.e.c cVar, RecyclerView.e.c cVar2);

        void b(RecyclerView.u uVar, RecyclerView.e.c cVar, RecyclerView.e.c cVar2);

        void c(RecyclerView.u uVar, RecyclerView.e.c cVar, RecyclerView.e.c cVar2);
    }

    aq() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f2153a.clear();
        this.f2154b.c();
    }

    /* access modifiers changed from: package-private */
    public void a(RecyclerView.u uVar, RecyclerView.e.c cVar) {
        a aVar = this.f2153a.get(uVar);
        if (aVar == null) {
            aVar = a.a();
            this.f2153a.put(uVar, aVar);
        }
        aVar.f2157b = cVar;
        aVar.f2156a |= 4;
    }

    /* access modifiers changed from: package-private */
    public boolean a(RecyclerView.u uVar) {
        a aVar = this.f2153a.get(uVar);
        return (aVar == null || (aVar.f2156a & 1) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.e.c b(RecyclerView.u uVar) {
        return a(uVar, 4);
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.e.c c(RecyclerView.u uVar) {
        return a(uVar, 8);
    }

    private RecyclerView.e.c a(RecyclerView.u uVar, int i) {
        a c2;
        RecyclerView.e.c cVar = null;
        int a2 = this.f2153a.a(uVar);
        if (!(a2 < 0 || (c2 = this.f2153a.c(a2)) == null || (c2.f2156a & i) == 0)) {
            c2.f2156a &= i ^ -1;
            if (i == 4) {
                cVar = c2.f2157b;
            } else if (i == 8) {
                cVar = c2.f2158c;
            } else {
                throw new IllegalArgumentException("Must provide flag PRE or POST");
            }
            if ((c2.f2156a & 12) == 0) {
                this.f2153a.d(a2);
                a.a(c2);
            }
        }
        return cVar;
    }

    /* access modifiers changed from: package-private */
    public void a(long j, RecyclerView.u uVar) {
        this.f2154b.b(j, uVar);
    }

    /* access modifiers changed from: package-private */
    public void b(RecyclerView.u uVar, RecyclerView.e.c cVar) {
        a aVar = this.f2153a.get(uVar);
        if (aVar == null) {
            aVar = a.a();
            this.f2153a.put(uVar, aVar);
        }
        aVar.f2156a |= 2;
        aVar.f2157b = cVar;
    }

    /* access modifiers changed from: package-private */
    public boolean d(RecyclerView.u uVar) {
        a aVar = this.f2153a.get(uVar);
        return (aVar == null || (aVar.f2156a & 4) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.u a(long j) {
        return this.f2154b.a(j);
    }

    /* access modifiers changed from: package-private */
    public void c(RecyclerView.u uVar, RecyclerView.e.c cVar) {
        a aVar = this.f2153a.get(uVar);
        if (aVar == null) {
            aVar = a.a();
            this.f2153a.put(uVar, aVar);
        }
        aVar.f2158c = cVar;
        aVar.f2156a |= 8;
    }

    /* access modifiers changed from: package-private */
    public void e(RecyclerView.u uVar) {
        a aVar = this.f2153a.get(uVar);
        if (aVar == null) {
            aVar = a.a();
            this.f2153a.put(uVar, aVar);
        }
        aVar.f2156a |= 1;
    }

    /* access modifiers changed from: package-private */
    public void f(RecyclerView.u uVar) {
        a aVar = this.f2153a.get(uVar);
        if (aVar != null) {
            aVar.f2156a &= -2;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar) {
        for (int size = this.f2153a.size() - 1; size >= 0; size--) {
            RecyclerView.u b2 = this.f2153a.b(size);
            a d2 = this.f2153a.d(size);
            if ((d2.f2156a & 3) == 3) {
                bVar.a(b2);
            } else if ((d2.f2156a & 1) != 0) {
                if (d2.f2157b == null) {
                    bVar.a(b2);
                } else {
                    bVar.a(b2, d2.f2157b, d2.f2158c);
                }
            } else if ((d2.f2156a & 14) == 14) {
                bVar.b(b2, d2.f2157b, d2.f2158c);
            } else if ((d2.f2156a & 12) == 12) {
                bVar.c(b2, d2.f2157b, d2.f2158c);
            } else if ((d2.f2156a & 4) != 0) {
                bVar.a(b2, d2.f2157b, null);
            } else if ((d2.f2156a & 8) != 0) {
                bVar.b(b2, d2.f2157b, d2.f2158c);
            } else if ((d2.f2156a & 2) != 0) {
            }
            a.a(d2);
        }
    }

    /* access modifiers changed from: package-private */
    public void g(RecyclerView.u uVar) {
        int b2 = this.f2154b.b() - 1;
        while (true) {
            if (b2 < 0) {
                break;
            } else if (uVar == this.f2154b.c(b2)) {
                this.f2154b.a(b2);
                break;
            } else {
                b2--;
            }
        }
        a remove = this.f2153a.remove(uVar);
        if (remove != null) {
            a.a(remove);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a.b();
    }

    public void h(RecyclerView.u uVar) {
        f(uVar);
    }

    /* compiled from: ViewInfoStore */
    static class a {

        /* renamed from: d  reason: collision with root package name */
        static Pools.a<a> f2155d = new Pools.SimplePool(20);

        /* renamed from: a  reason: collision with root package name */
        int f2156a;

        /* renamed from: b  reason: collision with root package name */
        RecyclerView.e.c f2157b;

        /* renamed from: c  reason: collision with root package name */
        RecyclerView.e.c f2158c;

        private a() {
        }

        static a a() {
            a a2 = f2155d.a();
            return a2 == null ? new a() : a2;
        }

        static void a(a aVar) {
            aVar.f2156a = 0;
            aVar.f2157b = null;
            aVar.f2158c = null;
            f2155d.a(aVar);
        }

        static void b() {
            do {
            } while (f2155d.a() != null);
        }
    }
}
