package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ag;
import android.support.v4.view.s;
import android.support.v7.a.a;
import android.support.v7.c.a.b;
import android.support.v7.text.AllCapsTransformationMethod;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.CompoundButton;
import com.duapps.ad.AdError;

public class SwitchCompat extends CompoundButton {
    private static final int[] M = {16842912};
    private int A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private TextPaint G;
    private ColorStateList H;
    private Layout I;
    private Layout J;
    private TransformationMethod K;
    private final Rect L;

    /* renamed from: a  reason: collision with root package name */
    a f2040a;

    /* renamed from: b  reason: collision with root package name */
    private Drawable f2041b;

    /* renamed from: c  reason: collision with root package name */
    private ColorStateList f2042c;

    /* renamed from: d  reason: collision with root package name */
    private PorterDuff.Mode f2043d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f2044e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f2045f;

    /* renamed from: g  reason: collision with root package name */
    private Drawable f2046g;

    /* renamed from: h  reason: collision with root package name */
    private ColorStateList f2047h;
    private PorterDuff.Mode i;
    private boolean j;
    private boolean k;
    private int l;
    private int m;
    private int n;
    private boolean o;
    private CharSequence p;
    private CharSequence q;
    private boolean r;
    private int s;
    private int t;
    private float u;
    private float v;
    private VelocityTracker w;
    private int x;
    private float y;
    private int z;

    public SwitchCompat(Context context) {
        this(context, null);
    }

    public SwitchCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0027a.switchStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.an.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.an.a(int, float):float
      android.support.v7.widget.an.a(int, int):int
      android.support.v7.widget.an.a(int, boolean):boolean */
    public SwitchCompat(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f2042c = null;
        this.f2043d = null;
        this.f2044e = false;
        this.f2045f = false;
        this.f2047h = null;
        this.i = null;
        this.j = false;
        this.k = false;
        this.w = VelocityTracker.obtain();
        this.L = new Rect();
        this.G = new TextPaint(1);
        Resources resources = getResources();
        this.G.density = resources.getDisplayMetrics().density;
        an a2 = an.a(context, attributeSet, a.k.SwitchCompat, i2, 0);
        this.f2041b = a2.a(a.k.SwitchCompat_android_thumb);
        if (this.f2041b != null) {
            this.f2041b.setCallback(this);
        }
        this.f2046g = a2.a(a.k.SwitchCompat_track);
        if (this.f2046g != null) {
            this.f2046g.setCallback(this);
        }
        this.p = a2.c(a.k.SwitchCompat_android_textOn);
        this.q = a2.c(a.k.SwitchCompat_android_textOff);
        this.r = a2.a(a.k.SwitchCompat_showText, true);
        this.l = a2.e(a.k.SwitchCompat_thumbTextPadding, 0);
        this.m = a2.e(a.k.SwitchCompat_switchMinWidth, 0);
        this.n = a2.e(a.k.SwitchCompat_switchPadding, 0);
        this.o = a2.a(a.k.SwitchCompat_splitTrack, false);
        ColorStateList e2 = a2.e(a.k.SwitchCompat_thumbTint);
        if (e2 != null) {
            this.f2042c = e2;
            this.f2044e = true;
        }
        PorterDuff.Mode a3 = u.a(a2.a(a.k.SwitchCompat_thumbTintMode, -1), null);
        if (this.f2043d != a3) {
            this.f2043d = a3;
            this.f2045f = true;
        }
        if (this.f2044e || this.f2045f) {
            b();
        }
        ColorStateList e3 = a2.e(a.k.SwitchCompat_trackTint);
        if (e3 != null) {
            this.f2047h = e3;
            this.j = true;
        }
        PorterDuff.Mode a4 = u.a(a2.a(a.k.SwitchCompat_trackTintMode, -1), null);
        if (this.i != a4) {
            this.i = a4;
            this.k = true;
        }
        if (this.j || this.k) {
            a();
        }
        int g2 = a2.g(a.k.SwitchCompat_switchTextAppearance, 0);
        if (g2 != 0) {
            a(context, g2);
        }
        a2.a();
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.t = viewConfiguration.getScaledTouchSlop();
        this.x = viewConfiguration.getScaledMinimumFlingVelocity();
        refreshDrawableState();
        setChecked(isChecked());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.an.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.an.a(int, float):float
      android.support.v7.widget.an.a(int, int):int
      android.support.v7.widget.an.a(int, boolean):boolean */
    public void a(Context context, int i2) {
        an a2 = an.a(context, i2, a.k.TextAppearance);
        ColorStateList e2 = a2.e(a.k.TextAppearance_android_textColor);
        if (e2 != null) {
            this.H = e2;
        } else {
            this.H = getTextColors();
        }
        int e3 = a2.e(a.k.TextAppearance_android_textSize, 0);
        if (!(e3 == 0 || ((float) e3) == this.G.getTextSize())) {
            this.G.setTextSize((float) e3);
            requestLayout();
        }
        a(a2.a(a.k.TextAppearance_android_typeface, -1), a2.a(a.k.TextAppearance_android_textStyle, -1));
        if (a2.a(a.k.TextAppearance_textAllCaps, false)) {
            this.K = new AllCapsTransformationMethod(getContext());
        } else {
            this.K = null;
        }
        a2.a();
    }

    private void a(int i2, int i3) {
        Typeface typeface = null;
        switch (i2) {
            case 1:
                typeface = Typeface.SANS_SERIF;
                break;
            case 2:
                typeface = Typeface.SERIF;
                break;
            case 3:
                typeface = Typeface.MONOSPACE;
                break;
        }
        a(typeface, i3);
    }

    public void a(Typeface typeface, int i2) {
        Typeface create;
        int i3;
        float f2;
        boolean z2 = false;
        if (i2 > 0) {
            if (typeface == null) {
                create = Typeface.defaultFromStyle(i2);
            } else {
                create = Typeface.create(typeface, i2);
            }
            setSwitchTypeface(create);
            if (create != null) {
                i3 = create.getStyle();
            } else {
                i3 = 0;
            }
            int i4 = (i3 ^ -1) & i2;
            TextPaint textPaint = this.G;
            if ((i4 & 1) != 0) {
                z2 = true;
            }
            textPaint.setFakeBoldText(z2);
            TextPaint textPaint2 = this.G;
            if ((i4 & 2) != 0) {
                f2 = -0.25f;
            } else {
                f2 = 0.0f;
            }
            textPaint2.setTextSkewX(f2);
            return;
        }
        this.G.setFakeBoldText(false);
        this.G.setTextSkewX(0.0f);
        setSwitchTypeface(typeface);
    }

    public void setSwitchTypeface(Typeface typeface) {
        if (this.G.getTypeface() != typeface) {
            this.G.setTypeface(typeface);
            requestLayout();
            invalidate();
        }
    }

    public void setSwitchPadding(int i2) {
        this.n = i2;
        requestLayout();
    }

    public int getSwitchPadding() {
        return this.n;
    }

    public void setSwitchMinWidth(int i2) {
        this.m = i2;
        requestLayout();
    }

    public int getSwitchMinWidth() {
        return this.m;
    }

    public void setThumbTextPadding(int i2) {
        this.l = i2;
        requestLayout();
    }

    public int getThumbTextPadding() {
        return this.l;
    }

    public void setTrackDrawable(Drawable drawable) {
        if (this.f2046g != null) {
            this.f2046g.setCallback(null);
        }
        this.f2046g = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
        requestLayout();
    }

    public void setTrackResource(int i2) {
        setTrackDrawable(b.b(getContext(), i2));
    }

    public Drawable getTrackDrawable() {
        return this.f2046g;
    }

    public void setTrackTintList(ColorStateList colorStateList) {
        this.f2047h = colorStateList;
        this.j = true;
        a();
    }

    public ColorStateList getTrackTintList() {
        return this.f2047h;
    }

    public void setTrackTintMode(PorterDuff.Mode mode) {
        this.i = mode;
        this.k = true;
        a();
    }

    public PorterDuff.Mode getTrackTintMode() {
        return this.i;
    }

    private void a() {
        if (this.f2046g == null) {
            return;
        }
        if (this.j || this.k) {
            this.f2046g = this.f2046g.mutate();
            if (this.j) {
                android.support.v4.b.a.a.a(this.f2046g, this.f2047h);
            }
            if (this.k) {
                android.support.v4.b.a.a.a(this.f2046g, this.i);
            }
            if (this.f2046g.isStateful()) {
                this.f2046g.setState(getDrawableState());
            }
        }
    }

    public void setThumbDrawable(Drawable drawable) {
        if (this.f2041b != null) {
            this.f2041b.setCallback(null);
        }
        this.f2041b = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
        requestLayout();
    }

    public void setThumbResource(int i2) {
        setThumbDrawable(b.b(getContext(), i2));
    }

    public Drawable getThumbDrawable() {
        return this.f2041b;
    }

    public void setThumbTintList(ColorStateList colorStateList) {
        this.f2042c = colorStateList;
        this.f2044e = true;
        b();
    }

    public ColorStateList getThumbTintList() {
        return this.f2042c;
    }

    public void setThumbTintMode(PorterDuff.Mode mode) {
        this.f2043d = mode;
        this.f2045f = true;
        b();
    }

    public PorterDuff.Mode getThumbTintMode() {
        return this.f2043d;
    }

    private void b() {
        if (this.f2041b == null) {
            return;
        }
        if (this.f2044e || this.f2045f) {
            this.f2041b = this.f2041b.mutate();
            if (this.f2044e) {
                android.support.v4.b.a.a.a(this.f2041b, this.f2042c);
            }
            if (this.f2045f) {
                android.support.v4.b.a.a.a(this.f2041b, this.f2043d);
            }
            if (this.f2041b.isStateful()) {
                this.f2041b.setState(getDrawableState());
            }
        }
    }

    public void setSplitTrack(boolean z2) {
        this.o = z2;
        invalidate();
    }

    public boolean getSplitTrack() {
        return this.o;
    }

    public CharSequence getTextOn() {
        return this.p;
    }

    public void setTextOn(CharSequence charSequence) {
        this.p = charSequence;
        requestLayout();
    }

    public CharSequence getTextOff() {
        return this.q;
    }

    public void setTextOff(CharSequence charSequence) {
        this.q = charSequence;
        requestLayout();
    }

    public void setShowText(boolean z2) {
        if (this.r != z2) {
            this.r = z2;
            requestLayout();
        }
    }

    public boolean getShowText() {
        return this.r;
    }

    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        if (this.r) {
            if (this.I == null) {
                this.I = a(this.p);
            }
            if (this.J == null) {
                this.J = a(this.q);
            }
        }
        Rect rect = this.L;
        if (this.f2041b != null) {
            this.f2041b.getPadding(rect);
            i5 = (this.f2041b.getIntrinsicWidth() - rect.left) - rect.right;
            i4 = this.f2041b.getIntrinsicHeight();
        } else {
            i4 = 0;
            i5 = 0;
        }
        if (this.r) {
            i6 = Math.max(this.I.getWidth(), this.J.getWidth()) + (this.l * 2);
        } else {
            i6 = 0;
        }
        this.B = Math.max(i6, i5);
        if (this.f2046g != null) {
            this.f2046g.getPadding(rect);
            i7 = this.f2046g.getIntrinsicHeight();
        } else {
            rect.setEmpty();
        }
        int i8 = rect.left;
        int i9 = rect.right;
        if (this.f2041b != null) {
            Rect a2 = u.a(this.f2041b);
            i8 = Math.max(i8, a2.left);
            i9 = Math.max(i9, a2.right);
        }
        int max = Math.max(this.m, i9 + i8 + (this.B * 2));
        int max2 = Math.max(i7, i4);
        this.z = max;
        this.A = max2;
        super.onMeasure(i2, i3);
        if (getMeasuredHeight() < max2) {
            setMeasuredDimension(ag.i(this), max2);
        }
    }

    public void onPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onPopulateAccessibilityEvent(accessibilityEvent);
        CharSequence charSequence = isChecked() ? this.p : this.q;
        if (charSequence != null) {
            accessibilityEvent.getText().add(charSequence);
        }
    }

    private Layout a(CharSequence charSequence) {
        int i2;
        CharSequence transformation = this.K != null ? this.K.getTransformation(charSequence, this) : charSequence;
        TextPaint textPaint = this.G;
        if (transformation != null) {
            i2 = (int) Math.ceil((double) Layout.getDesiredWidth(transformation, this.G));
        } else {
            i2 = 0;
        }
        return new StaticLayout(transformation, textPaint, i2, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
    }

    private boolean a(float f2, float f3) {
        if (this.f2041b == null) {
            return false;
        }
        int thumbOffset = getThumbOffset();
        this.f2041b.getPadding(this.L);
        int i2 = this.D - this.t;
        int i3 = (thumbOffset + this.C) - this.t;
        int i4 = this.B + i3 + this.L.left + this.L.right + this.t;
        int i5 = this.F + this.t;
        if (f2 <= ((float) i3) || f2 >= ((float) i4) || f3 <= ((float) i2) || f3 >= ((float) i5)) {
            return false;
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f2;
        this.w.addMovement(motionEvent);
        switch (s.a(motionEvent)) {
            case 0:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                if (isEnabled() && a(x2, y2)) {
                    this.s = 1;
                    this.u = x2;
                    this.v = y2;
                    break;
                }
            case 1:
            case 3:
                if (this.s != 2) {
                    this.s = 0;
                    this.w.clear();
                    break;
                } else {
                    b(motionEvent);
                    super.onTouchEvent(motionEvent);
                    return true;
                }
            case 2:
                switch (this.s) {
                    case 2:
                        float x3 = motionEvent.getX();
                        int thumbScrollRange = getThumbScrollRange();
                        float f3 = x3 - this.u;
                        if (thumbScrollRange != 0) {
                            f2 = f3 / ((float) thumbScrollRange);
                        } else {
                            f2 = f3 > 0.0f ? 1.0f : -1.0f;
                        }
                        if (ar.a(this)) {
                            f2 = -f2;
                        }
                        float a2 = a(f2 + this.y, 0.0f, 1.0f);
                        if (a2 != this.y) {
                            this.u = x3;
                            setThumbPosition(a2);
                        }
                        return true;
                    case 1:
                        float x4 = motionEvent.getX();
                        float y3 = motionEvent.getY();
                        if (Math.abs(x4 - this.u) > ((float) this.t) || Math.abs(y3 - this.v) > ((float) this.t)) {
                            this.s = 2;
                            getParent().requestDisallowInterceptTouchEvent(true);
                            this.u = x4;
                            this.v = y3;
                            return true;
                        }
                }
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    private void a(MotionEvent motionEvent) {
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        obtain.setAction(3);
        super.onTouchEvent(obtain);
        obtain.recycle();
    }

    private void b(MotionEvent motionEvent) {
        boolean z2 = true;
        this.s = 0;
        boolean z3 = motionEvent.getAction() == 1 && isEnabled();
        boolean isChecked = isChecked();
        if (z3) {
            this.w.computeCurrentVelocity(AdError.NETWORK_ERROR_CODE);
            float xVelocity = this.w.getXVelocity();
            if (Math.abs(xVelocity) <= ((float) this.x)) {
                z2 = getTargetCheckedState();
            } else if (ar.a(this)) {
                if (xVelocity >= 0.0f) {
                    z2 = false;
                }
            } else if (xVelocity <= 0.0f) {
                z2 = false;
            }
        } else {
            z2 = isChecked;
        }
        if (z2 != isChecked) {
            playSoundEffect(0);
        }
        setChecked(z2);
        a(motionEvent);
    }

    private void a(final boolean z2) {
        if (this.f2040a != null) {
            c();
        }
        this.f2040a = new a(this.y, z2 ? 1.0f : 0.0f);
        this.f2040a.setDuration(250);
        this.f2040a.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                if (SwitchCompat.this.f2040a == animation) {
                    SwitchCompat.this.setThumbPosition(z2 ? 1.0f : 0.0f);
                    SwitchCompat.this.f2040a = null;
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        startAnimation(this.f2040a);
    }

    private void c() {
        if (this.f2040a != null) {
            clearAnimation();
            this.f2040a = null;
        }
    }

    private boolean getTargetCheckedState() {
        return this.y > 0.5f;
    }

    /* access modifiers changed from: package-private */
    public void setThumbPosition(float f2) {
        this.y = f2;
        invalidate();
    }

    public void toggle() {
        setChecked(!isChecked());
    }

    public void setChecked(boolean z2) {
        super.setChecked(z2);
        boolean isChecked = isChecked();
        if (getWindowToken() == null || !ag.F(this) || !isShown()) {
            c();
            setThumbPosition(isChecked ? 1.0f : 0.0f);
            return;
        }
        a(isChecked);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int height;
        int i9;
        int i10 = 0;
        super.onLayout(z2, i2, i3, i4, i5);
        if (this.f2041b != null) {
            Rect rect = this.L;
            if (this.f2046g != null) {
                this.f2046g.getPadding(rect);
            } else {
                rect.setEmpty();
            }
            Rect a2 = u.a(this.f2041b);
            i6 = Math.max(0, a2.left - rect.left);
            i10 = Math.max(0, a2.right - rect.right);
        } else {
            i6 = 0;
        }
        if (ar.a(this)) {
            int paddingLeft = getPaddingLeft() + i6;
            i8 = ((this.z + paddingLeft) - i6) - i10;
            i7 = paddingLeft;
        } else {
            int width = (getWidth() - getPaddingRight()) - i10;
            i7 = i10 + i6 + (width - this.z);
            i8 = width;
        }
        switch (getGravity() & 112) {
            case 16:
                i9 = (((getPaddingTop() + getHeight()) - getPaddingBottom()) / 2) - (this.A / 2);
                height = this.A + i9;
                break;
            case 80:
                height = getHeight() - getPaddingBottom();
                i9 = height - this.A;
                break;
            default:
                i9 = getPaddingTop();
                height = this.A + i9;
                break;
        }
        this.C = i7;
        this.D = i9;
        this.F = height;
        this.E = i8;
    }

    public void draw(Canvas canvas) {
        Rect rect;
        int i2;
        int i3;
        int i4;
        Rect rect2 = this.L;
        int i5 = this.C;
        int i6 = this.D;
        int i7 = this.E;
        int i8 = this.F;
        int thumbOffset = i5 + getThumbOffset();
        if (this.f2041b != null) {
            rect = u.a(this.f2041b);
        } else {
            rect = u.f2265a;
        }
        if (this.f2046g != null) {
            this.f2046g.getPadding(rect2);
            int i9 = rect2.left + thumbOffset;
            if (rect != null) {
                if (rect.left > rect2.left) {
                    i5 += rect.left - rect2.left;
                }
                if (rect.top > rect2.top) {
                    i4 = (rect.top - rect2.top) + i6;
                } else {
                    i4 = i6;
                }
                if (rect.right > rect2.right) {
                    i7 -= rect.right - rect2.right;
                }
                i3 = rect.bottom > rect2.bottom ? i8 - (rect.bottom - rect2.bottom) : i8;
            } else {
                i3 = i8;
                i4 = i6;
            }
            this.f2046g.setBounds(i5, i4, i7, i3);
            i2 = i9;
        } else {
            i2 = thumbOffset;
        }
        if (this.f2041b != null) {
            this.f2041b.getPadding(rect2);
            int i10 = i2 - rect2.left;
            int i11 = i2 + this.B + rect2.right;
            this.f2041b.setBounds(i10, i6, i11, i8);
            Drawable background = getBackground();
            if (background != null) {
                android.support.v4.b.a.a.a(background, i10, i6, i11, i8);
            }
        }
        super.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int width;
        super.onDraw(canvas);
        Rect rect = this.L;
        Drawable drawable = this.f2046g;
        if (drawable != null) {
            drawable.getPadding(rect);
        } else {
            rect.setEmpty();
        }
        int i2 = this.D;
        int i3 = this.F;
        int i4 = i2 + rect.top;
        int i5 = i3 - rect.bottom;
        Drawable drawable2 = this.f2041b;
        if (drawable != null) {
            if (!this.o || drawable2 == null) {
                drawable.draw(canvas);
            } else {
                Rect a2 = u.a(drawable2);
                drawable2.copyBounds(rect);
                rect.left += a2.left;
                rect.right -= a2.right;
                int save = canvas.save();
                canvas.clipRect(rect, Region.Op.DIFFERENCE);
                drawable.draw(canvas);
                canvas.restoreToCount(save);
            }
        }
        int save2 = canvas.save();
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
        Layout layout = getTargetCheckedState() ? this.I : this.J;
        if (layout != null) {
            int[] drawableState = getDrawableState();
            if (this.H != null) {
                this.G.setColor(this.H.getColorForState(drawableState, 0));
            }
            this.G.drawableState = drawableState;
            if (drawable2 != null) {
                Rect bounds = drawable2.getBounds();
                width = bounds.right + bounds.left;
            } else {
                width = getWidth();
            }
            canvas.translate((float) ((width / 2) - (layout.getWidth() / 2)), (float) (((i4 + i5) / 2) - (layout.getHeight() / 2)));
            layout.draw(canvas);
        }
        canvas.restoreToCount(save2);
    }

    public int getCompoundPaddingLeft() {
        if (!ar.a(this)) {
            return super.getCompoundPaddingLeft();
        }
        int compoundPaddingLeft = super.getCompoundPaddingLeft() + this.z;
        if (!TextUtils.isEmpty(getText())) {
            return compoundPaddingLeft + this.n;
        }
        return compoundPaddingLeft;
    }

    public int getCompoundPaddingRight() {
        if (ar.a(this)) {
            return super.getCompoundPaddingRight();
        }
        int compoundPaddingRight = super.getCompoundPaddingRight() + this.z;
        if (!TextUtils.isEmpty(getText())) {
            return compoundPaddingRight + this.n;
        }
        return compoundPaddingRight;
    }

    private int getThumbOffset() {
        float f2;
        if (ar.a(this)) {
            f2 = 1.0f - this.y;
        } else {
            f2 = this.y;
        }
        return (int) ((f2 * ((float) getThumbScrollRange())) + 0.5f);
    }

    private int getThumbScrollRange() {
        Rect rect;
        if (this.f2046g == null) {
            return 0;
        }
        Rect rect2 = this.L;
        this.f2046g.getPadding(rect2);
        if (this.f2041b != null) {
            rect = u.a(this.f2041b);
        } else {
            rect = u.f2265a;
        }
        return ((((this.z - this.B) - rect2.left) - rect2.right) - rect.left) - rect.right;
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i2) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i2 + 1);
        if (isChecked()) {
            mergeDrawableStates(onCreateDrawableState, M);
        }
        return onCreateDrawableState;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        boolean z2 = false;
        Drawable drawable = this.f2041b;
        if (drawable != null && drawable.isStateful()) {
            z2 = false | drawable.setState(drawableState);
        }
        Drawable drawable2 = this.f2046g;
        if (drawable2 != null && drawable2.isStateful()) {
            z2 |= drawable2.setState(drawableState);
        }
        if (z2) {
            invalidate();
        }
    }

    public void drawableHotspotChanged(float f2, float f3) {
        if (Build.VERSION.SDK_INT >= 21) {
            super.drawableHotspotChanged(f2, f3);
        }
        if (this.f2041b != null) {
            android.support.v4.b.a.a.a(this.f2041b, f2, f3);
        }
        if (this.f2046g != null) {
            android.support.v4.b.a.a.a(this.f2046g, f2, f3);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f2041b || drawable == this.f2046g;
    }

    public void jumpDrawablesToCurrentState() {
        if (Build.VERSION.SDK_INT >= 11) {
            super.jumpDrawablesToCurrentState();
            if (this.f2041b != null) {
                this.f2041b.jumpToCurrentState();
            }
            if (this.f2046g != null) {
                this.f2046g.jumpToCurrentState();
            }
            c();
            setThumbPosition(isChecked() ? 1.0f : 0.0f);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName("android.widget.Switch");
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        if (Build.VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName("android.widget.Switch");
            CharSequence charSequence = isChecked() ? this.p : this.q;
            if (!TextUtils.isEmpty(charSequence)) {
                CharSequence text = accessibilityNodeInfo.getText();
                if (TextUtils.isEmpty(text)) {
                    accessibilityNodeInfo.setText(charSequence);
                    return;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(text).append(' ').append(charSequence);
                accessibilityNodeInfo.setText(sb);
            }
        }
    }

    private static float a(float f2, float f3, float f4) {
        if (f2 < f3) {
            return f3;
        }
        return f2 > f4 ? f4 : f2;
    }

    private class a extends Animation {

        /* renamed from: a  reason: collision with root package name */
        final float f2050a;

        /* renamed from: b  reason: collision with root package name */
        final float f2051b;

        /* renamed from: c  reason: collision with root package name */
        final float f2052c;

        a(float f2, float f3) {
            this.f2050a = f2;
            this.f2051b = f3;
            this.f2052c = f3 - f2;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f2, Transformation transformation) {
            SwitchCompat.this.setThumbPosition(this.f2050a + (this.f2052c * f2));
        }
    }
}
