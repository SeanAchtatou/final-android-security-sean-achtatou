package android.support.v7.widget;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Shader;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.support.v4.b.a.h;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import io.fabric.sdk.android.services.common.a;

/* compiled from: AppCompatProgressBarHelper */
class i {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f2241a = {16843067, 16843068};

    /* renamed from: b  reason: collision with root package name */
    private final ProgressBar f2242b;

    /* renamed from: c  reason: collision with root package name */
    private Bitmap f2243c;

    i(ProgressBar progressBar) {
        this.f2242b = progressBar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.i.a(android.graphics.drawable.Drawable, boolean):android.graphics.drawable.Drawable
     arg types: [android.graphics.drawable.Drawable, int]
     candidates:
      android.support.v7.widget.i.a(android.util.AttributeSet, int):void
      android.support.v7.widget.i.a(android.graphics.drawable.Drawable, boolean):android.graphics.drawable.Drawable */
    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        an a2 = an.a(this.f2242b.getContext(), attributeSet, f2241a, i, 0);
        Drawable b2 = a2.b(0);
        if (b2 != null) {
            this.f2242b.setIndeterminateDrawable(a(b2));
        }
        Drawable b3 = a2.b(1);
        if (b3 != null) {
            this.f2242b.setProgressDrawable(a(b3, false));
        }
        a2.a();
    }

    private Drawable a(Drawable drawable, boolean z) {
        boolean z2;
        if (drawable instanceof h) {
            Drawable a2 = ((h) drawable).a();
            if (a2 != null) {
                ((h) drawable).a(a(a2, z));
            }
        } else if (drawable instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            int numberOfLayers = layerDrawable.getNumberOfLayers();
            Drawable[] drawableArr = new Drawable[numberOfLayers];
            for (int i = 0; i < numberOfLayers; i++) {
                int id = layerDrawable.getId(i);
                Drawable drawable2 = layerDrawable.getDrawable(i);
                if (id == 16908301 || id == 16908303) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                drawableArr[i] = a(drawable2, z2);
            }
            LayerDrawable layerDrawable2 = new LayerDrawable(drawableArr);
            for (int i2 = 0; i2 < numberOfLayers; i2++) {
                layerDrawable2.setId(i2, layerDrawable.getId(i2));
            }
            return layerDrawable2;
        } else if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            if (this.f2243c == null) {
                this.f2243c = bitmap;
            }
            ShapeDrawable shapeDrawable = new ShapeDrawable(b());
            shapeDrawable.getPaint().setShader(new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.CLAMP));
            shapeDrawable.getPaint().setColorFilter(bitmapDrawable.getPaint().getColorFilter());
            return z ? new ClipDrawable(shapeDrawable, 3, 1) : shapeDrawable;
        }
        return drawable;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.i.a(android.graphics.drawable.Drawable, boolean):android.graphics.drawable.Drawable
     arg types: [android.graphics.drawable.Drawable, int]
     candidates:
      android.support.v7.widget.i.a(android.util.AttributeSet, int):void
      android.support.v7.widget.i.a(android.graphics.drawable.Drawable, boolean):android.graphics.drawable.Drawable */
    private Drawable a(Drawable drawable) {
        if (!(drawable instanceof AnimationDrawable)) {
            return drawable;
        }
        AnimationDrawable animationDrawable = (AnimationDrawable) drawable;
        int numberOfFrames = animationDrawable.getNumberOfFrames();
        AnimationDrawable animationDrawable2 = new AnimationDrawable();
        animationDrawable2.setOneShot(animationDrawable.isOneShot());
        for (int i = 0; i < numberOfFrames; i++) {
            Drawable a2 = a(animationDrawable.getFrame(i), true);
            a2.setLevel(a.DEFAULT_TIMEOUT);
            animationDrawable2.addFrame(a2, animationDrawable.getDuration(i));
        }
        animationDrawable2.setLevel(a.DEFAULT_TIMEOUT);
        return animationDrawable2;
    }

    private Shape b() {
        return new RoundRectShape(new float[]{5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f}, null, null);
    }

    /* access modifiers changed from: package-private */
    public Bitmap a() {
        return this.f2243c;
    }
}
