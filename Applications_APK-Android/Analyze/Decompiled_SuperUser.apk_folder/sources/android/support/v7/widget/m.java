package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;

@TargetApi(21)
/* compiled from: CardViewApi21 */
class m implements p {
    m() {
    }

    public void a(n nVar, Context context, ColorStateList colorStateList, float f2, float f3, float f4) {
        nVar.a(new ae(colorStateList, f2));
        View d2 = nVar.d();
        d2.setClipToOutline(true);
        d2.setElevation(f3);
        b(nVar, f4);
    }

    public void a(n nVar, float f2) {
        j(nVar).a(f2);
    }

    public void a() {
    }

    public void b(n nVar, float f2) {
        j(nVar).a(f2, nVar.a(), nVar.b());
        f(nVar);
    }

    public float a(n nVar) {
        return j(nVar).a();
    }

    public float b(n nVar) {
        return d(nVar) * 2.0f;
    }

    public float c(n nVar) {
        return d(nVar) * 2.0f;
    }

    public float d(n nVar) {
        return j(nVar).b();
    }

    public void c(n nVar, float f2) {
        nVar.d().setElevation(f2);
    }

    public float e(n nVar) {
        return nVar.d().getElevation();
    }

    public void f(n nVar) {
        if (!nVar.a()) {
            nVar.a(0, 0, 0, 0);
            return;
        }
        float a2 = a(nVar);
        float d2 = d(nVar);
        int ceil = (int) Math.ceil((double) af.b(a2, d2, nVar.b()));
        int ceil2 = (int) Math.ceil((double) af.a(a2, d2, nVar.b()));
        nVar.a(ceil, ceil2, ceil, ceil2);
    }

    public void g(n nVar) {
        b(nVar, a(nVar));
    }

    public void h(n nVar) {
        b(nVar, a(nVar));
    }

    public void a(n nVar, ColorStateList colorStateList) {
        j(nVar).a(colorStateList);
    }

    public ColorStateList i(n nVar) {
        return j(nVar).c();
    }

    private ae j(n nVar) {
        return (ae) nVar.c();
    }
}
