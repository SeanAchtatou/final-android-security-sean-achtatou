package android.support.v7.widget;

import android.content.Context;
import android.os.Build;
import android.support.v4.view.ay;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.support.v7.a.a;
import android.view.View;

/* compiled from: DropDownListView */
class v extends ListViewCompat {

    /* renamed from: g  reason: collision with root package name */
    private boolean f2267g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f2268h;
    private boolean i;
    private ay j;
    private ListViewAutoScrollHelper k;

    public v(Context context, boolean z) {
        super(context, null, a.C0027a.dropDownListViewStyle);
        this.f2268h = z;
        setCacheColorHint(0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.view.MotionEvent r9, int r10) {
        /*
            r8 = this;
            r2 = 1
            r1 = 0
            int r3 = android.support.v4.view.s.a(r9)
            switch(r3) {
                case 1: goto L_0x002d;
                case 2: goto L_0x006a;
                case 3: goto L_0x002a;
                default: goto L_0x0009;
            }
        L_0x0009:
            r0 = r1
            r3 = r2
        L_0x000b:
            if (r3 == 0) goto L_0x000f
            if (r0 == 0) goto L_0x0012
        L_0x000f:
            r8.d()
        L_0x0012:
            if (r3 == 0) goto L_0x0060
            android.support.v4.widget.ListViewAutoScrollHelper r0 = r8.k
            if (r0 != 0) goto L_0x001f
            android.support.v4.widget.ListViewAutoScrollHelper r0 = new android.support.v4.widget.ListViewAutoScrollHelper
            r0.<init>(r8)
            r8.k = r0
        L_0x001f:
            android.support.v4.widget.ListViewAutoScrollHelper r0 = r8.k
            r0.a(r2)
            android.support.v4.widget.ListViewAutoScrollHelper r0 = r8.k
            r0.onTouch(r8, r9)
        L_0x0029:
            return r3
        L_0x002a:
            r0 = r1
            r3 = r1
            goto L_0x000b
        L_0x002d:
            r0 = r1
        L_0x002e:
            int r4 = r9.findPointerIndex(r10)
            if (r4 >= 0) goto L_0x0037
            r0 = r1
            r3 = r1
            goto L_0x000b
        L_0x0037:
            float r5 = r9.getX(r4)
            int r5 = (int) r5
            float r4 = r9.getY(r4)
            int r4 = (int) r4
            int r6 = r8.pointToPosition(r5, r4)
            r7 = -1
            if (r6 != r7) goto L_0x004b
            r3 = r0
            r0 = r2
            goto L_0x000b
        L_0x004b:
            int r0 = r8.getFirstVisiblePosition()
            int r0 = r6 - r0
            android.view.View r0 = r8.getChildAt(r0)
            float r5 = (float) r5
            float r4 = (float) r4
            r8.a(r0, r6, r5, r4)
            if (r3 != r2) goto L_0x0009
            r8.a(r0, r6)
            goto L_0x0009
        L_0x0060:
            android.support.v4.widget.ListViewAutoScrollHelper r0 = r8.k
            if (r0 == 0) goto L_0x0029
            android.support.v4.widget.ListViewAutoScrollHelper r0 = r8.k
            r0.a(r1)
            goto L_0x0029
        L_0x006a:
            r0 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.v.a(android.view.MotionEvent, int):boolean");
    }

    private void a(View view, int i2) {
        performItemClick(view, i2, getItemIdAtPosition(i2));
    }

    /* access modifiers changed from: package-private */
    public void setListSelectionHidden(boolean z) {
        this.f2267g = z;
    }

    private void d() {
        this.i = false;
        setPressed(false);
        drawableStateChanged();
        View childAt = getChildAt(this.f1844f - getFirstVisiblePosition());
        if (childAt != null) {
            childAt.setPressed(false);
        }
        if (this.j != null) {
            this.j.b();
            this.j = null;
        }
    }

    private void a(View view, int i2, float f2, float f3) {
        View childAt;
        this.i = true;
        if (Build.VERSION.SDK_INT >= 21) {
            drawableHotspotChanged(f2, f3);
        }
        if (!isPressed()) {
            setPressed(true);
        }
        layoutChildren();
        if (!(this.f1844f == -1 || (childAt = getChildAt(this.f1844f - getFirstVisiblePosition())) == null || childAt == view || !childAt.isPressed())) {
            childAt.setPressed(false);
        }
        this.f1844f = i2;
        float left = f2 - ((float) view.getLeft());
        float top = f3 - ((float) view.getTop());
        if (Build.VERSION.SDK_INT >= 21) {
            view.drawableHotspotChanged(left, top);
        }
        if (!view.isPressed()) {
            view.setPressed(true);
        }
        a(i2, view, f2, f3);
        setSelectorEnabled(false);
        refreshDrawableState();
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return this.i || super.a();
    }

    public boolean isInTouchMode() {
        return (this.f2268h && this.f2267g) || super.isInTouchMode();
    }

    public boolean hasWindowFocus() {
        return this.f2268h || super.hasWindowFocus();
    }

    public boolean isFocused() {
        return this.f2268h || super.isFocused();
    }

    public boolean hasFocus() {
        return this.f2268h || super.hasFocus();
    }
}
