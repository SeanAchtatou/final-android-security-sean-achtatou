package android.support.v7.widget;

import android.support.v7.view.menu.i;
import android.view.Menu;
import android.view.Window;

/* compiled from: DecorContentParent */
public interface s {
    void a(int i);

    void a(Menu menu, i.a aVar);

    boolean e();

    boolean f();

    boolean g();

    boolean h();

    boolean i();

    void j();

    void k();

    void setWindowCallback(Window.Callback callback);

    void setWindowTitle(CharSequence charSequence);
}
