package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ad;
import android.support.v7.a.a;
import android.support.v7.c.a.b;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

public class AppCompatAutoCompleteTextView extends AutoCompleteTextView implements ad {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f1662a = {16843126};

    /* renamed from: b  reason: collision with root package name */
    private e f1663b;

    /* renamed from: c  reason: collision with root package name */
    private k f1664c;

    public AppCompatAutoCompleteTextView(Context context) {
        this(context, null);
    }

    public AppCompatAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0027a.autoCompleteTextViewStyle);
    }

    public AppCompatAutoCompleteTextView(Context context, AttributeSet attributeSet, int i) {
        super(ak.a(context), attributeSet, i);
        an a2 = an.a(getContext(), attributeSet, f1662a, i, 0);
        if (a2.g(0)) {
            setDropDownBackgroundDrawable(a2.a(0));
        }
        a2.a();
        this.f1663b = new e(this);
        this.f1663b.a(attributeSet, i);
        this.f1664c = k.a(this);
        this.f1664c.a(attributeSet, i);
        this.f1664c.a();
    }

    public void setDropDownBackgroundResource(int i) {
        setDropDownBackgroundDrawable(b.b(getContext(), i));
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f1663b != null) {
            this.f1663b.a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f1663b != null) {
            this.f1663b.a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f1663b != null) {
            this.f1663b.a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f1663b != null) {
            return this.f1663b.a();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.f1663b != null) {
            this.f1663b.a(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (this.f1663b != null) {
            return this.f1663b.b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1663b != null) {
            this.f1663b.c();
        }
        if (this.f1664c != null) {
            this.f1664c.a();
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f1664c != null) {
            this.f1664c.a(context, i);
        }
    }
}
