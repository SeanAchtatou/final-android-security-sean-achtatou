package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ag;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.view.View;

/* compiled from: AppCompatBackgroundHelper */
class e {

    /* renamed from: a  reason: collision with root package name */
    private final View f2189a;

    /* renamed from: b  reason: collision with root package name */
    private final g f2190b;

    /* renamed from: c  reason: collision with root package name */
    private int f2191c = -1;

    /* renamed from: d  reason: collision with root package name */
    private al f2192d;

    /* renamed from: e  reason: collision with root package name */
    private al f2193e;

    /* renamed from: f  reason: collision with root package name */
    private al f2194f;

    e(View view) {
        this.f2189a = view;
        this.f2190b = g.a();
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        an a2 = an.a(this.f2189a.getContext(), attributeSet, a.k.ViewBackgroundHelper, i, 0);
        try {
            if (a2.g(a.k.ViewBackgroundHelper_android_background)) {
                this.f2191c = a2.g(a.k.ViewBackgroundHelper_android_background, -1);
                ColorStateList b2 = this.f2190b.b(this.f2189a.getContext(), this.f2191c);
                if (b2 != null) {
                    b(b2);
                }
            }
            if (a2.g(a.k.ViewBackgroundHelper_backgroundTint)) {
                ag.a(this.f2189a, a2.e(a.k.ViewBackgroundHelper_backgroundTint));
            }
            if (a2.g(a.k.ViewBackgroundHelper_backgroundTintMode)) {
                ag.a(this.f2189a, u.a(a2.a(a.k.ViewBackgroundHelper_backgroundTintMode, -1), null));
            }
        } finally {
            a2.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.f2191c = i;
        b(this.f2190b != null ? this.f2190b.b(this.f2189a.getContext(), i) : null);
        c();
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable) {
        this.f2191c = -1;
        b((ColorStateList) null);
        c();
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        if (this.f2193e == null) {
            this.f2193e = new al();
        }
        this.f2193e.f2138a = colorStateList;
        this.f2193e.f2141d = true;
        c();
    }

    /* access modifiers changed from: package-private */
    public ColorStateList a() {
        if (this.f2193e != null) {
            return this.f2193e.f2138a;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        if (this.f2193e == null) {
            this.f2193e = new al();
        }
        this.f2193e.f2139b = mode;
        this.f2193e.f2140c = true;
        c();
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode b() {
        if (this.f2193e != null) {
            return this.f2193e.f2139b;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Drawable background = this.f2189a.getBackground();
        if (background == null) {
            return;
        }
        if (d() && b(background)) {
            return;
        }
        if (this.f2193e != null) {
            g.a(background, this.f2193e, this.f2189a.getDrawableState());
        } else if (this.f2192d != null) {
            g.a(background, this.f2192d, this.f2189a.getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public void b(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.f2192d == null) {
                this.f2192d = new al();
            }
            this.f2192d.f2138a = colorStateList;
            this.f2192d.f2141d = true;
        } else {
            this.f2192d = null;
        }
        c();
    }

    private boolean d() {
        int i = Build.VERSION.SDK_INT;
        if (i < 21) {
            return false;
        }
        if (i == 21 || this.f2192d != null) {
            return true;
        }
        return false;
    }

    private boolean b(Drawable drawable) {
        if (this.f2194f == null) {
            this.f2194f = new al();
        }
        al alVar = this.f2194f;
        alVar.a();
        ColorStateList B = ag.B(this.f2189a);
        if (B != null) {
            alVar.f2141d = true;
            alVar.f2138a = B;
        }
        PorterDuff.Mode C = ag.C(this.f2189a);
        if (C != null) {
            alVar.f2140c = true;
            alVar.f2139b = C;
        }
        if (!alVar.f2141d && !alVar.f2140c) {
            return false;
        }
        g.a(drawable, alVar, this.f2189a.getDrawableState());
        return true;
    }
}
