package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v7.view.menu.ListMenuItemView;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.d;
import android.transition.Transition;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

public class MenuPopupWindow extends ListPopupWindow implements aa {

    /* renamed from: a  reason: collision with root package name */
    private static Method f1847a;

    /* renamed from: b  reason: collision with root package name */
    private aa f1848b;

    static {
        Class<PopupWindow> cls = PopupWindow.class;
        try {
            f1847a = cls.getDeclaredMethod("setTouchModal", Boolean.TYPE);
        } catch (NoSuchMethodException e2) {
            Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }

    public MenuPopupWindow(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* access modifiers changed from: package-private */
    public v a(Context context, boolean z) {
        MenuDropDownListView menuDropDownListView = new MenuDropDownListView(context, z);
        menuDropDownListView.setHoverListener(this);
        return menuDropDownListView;
    }

    public void a(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.f1830g.setEnterTransition((Transition) obj);
        }
    }

    public void b(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.f1830g.setExitTransition((Transition) obj);
        }
    }

    public void a(aa aaVar) {
        this.f1848b = aaVar;
    }

    public void b(boolean z) {
        if (f1847a != null) {
            try {
                f1847a.invoke(this.f1830g, Boolean.valueOf(z));
            } catch (Exception e2) {
                Log.i("MenuPopupWindow", "Could not invoke setTouchModal() on PopupWindow. Oh well.");
            }
        }
    }

    public void b(MenuBuilder menuBuilder, MenuItem menuItem) {
        if (this.f1848b != null) {
            this.f1848b.b(menuBuilder, menuItem);
        }
    }

    public void a(MenuBuilder menuBuilder, MenuItem menuItem) {
        if (this.f1848b != null) {
            this.f1848b.a(menuBuilder, menuItem);
        }
    }

    public static class MenuDropDownListView extends v {

        /* renamed from: g  reason: collision with root package name */
        final int f1849g;

        /* renamed from: h  reason: collision with root package name */
        final int f1850h;
        private aa i;
        private MenuItem j;

        public /* bridge */ /* synthetic */ boolean a(MotionEvent motionEvent, int i2) {
            return super.a(motionEvent, i2);
        }

        public /* bridge */ /* synthetic */ boolean hasFocus() {
            return super.hasFocus();
        }

        public /* bridge */ /* synthetic */ boolean hasWindowFocus() {
            return super.hasWindowFocus();
        }

        public /* bridge */ /* synthetic */ boolean isFocused() {
            return super.isFocused();
        }

        public /* bridge */ /* synthetic */ boolean isInTouchMode() {
            return super.isInTouchMode();
        }

        public MenuDropDownListView(Context context, boolean z) {
            super(context, z);
            Configuration configuration = context.getResources().getConfiguration();
            if (Build.VERSION.SDK_INT < 17 || 1 != configuration.getLayoutDirection()) {
                this.f1849g = 22;
                this.f1850h = 21;
                return;
            }
            this.f1849g = 21;
            this.f1850h = 22;
        }

        public void setHoverListener(aa aaVar) {
            this.i = aaVar;
        }

        public boolean onKeyDown(int i2, KeyEvent keyEvent) {
            ListMenuItemView listMenuItemView = (ListMenuItemView) getSelectedView();
            if (listMenuItemView != null && i2 == this.f1849g) {
                if (listMenuItemView.isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    performItemClick(listMenuItemView, getSelectedItemPosition(), getSelectedItemId());
                }
                return true;
            } else if (listMenuItemView == null || i2 != this.f1850h) {
                return super.onKeyDown(i2, keyEvent);
            } else {
                setSelection(-1);
                ((d) getAdapter()).a().close(false);
                return true;
            }
        }

        public boolean onHoverEvent(MotionEvent motionEvent) {
            int i2;
            d dVar;
            MenuItemImpl menuItemImpl;
            int pointToPosition;
            int i3;
            if (this.i != null) {
                ListAdapter adapter = getAdapter();
                if (adapter instanceof HeaderViewListAdapter) {
                    HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
                    i2 = headerViewListAdapter.getHeadersCount();
                    dVar = (d) headerViewListAdapter.getWrappedAdapter();
                } else {
                    i2 = 0;
                    dVar = (d) adapter;
                }
                if (motionEvent.getAction() == 10 || (pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY())) == -1 || (i3 = pointToPosition - i2) < 0 || i3 >= dVar.getCount()) {
                    menuItemImpl = null;
                } else {
                    menuItemImpl = dVar.getItem(i3);
                }
                MenuItem menuItem = this.j;
                if (menuItem != menuItemImpl) {
                    MenuBuilder a2 = dVar.a();
                    if (menuItem != null) {
                        this.i.a(a2, menuItem);
                    }
                    this.j = menuItemImpl;
                    if (menuItemImpl != null) {
                        this.i.b(a2, menuItemImpl);
                    }
                }
            }
            return super.onHoverEvent(motionEvent);
        }
    }
}
