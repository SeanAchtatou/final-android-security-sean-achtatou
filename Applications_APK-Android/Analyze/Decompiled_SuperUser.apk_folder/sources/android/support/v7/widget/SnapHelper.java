package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Scroller;

public abstract class SnapHelper extends RecyclerView.j {

    /* renamed from: a  reason: collision with root package name */
    RecyclerView f1996a;

    /* renamed from: b  reason: collision with root package name */
    private Scroller f1997b;

    /* renamed from: c  reason: collision with root package name */
    private final RecyclerView.l f1998c = new RecyclerView.l() {

        /* renamed from: a  reason: collision with root package name */
        boolean f1999a = false;

        public void a(RecyclerView recyclerView, int i) {
            super.a(recyclerView, i);
            if (i == 0 && this.f1999a) {
                this.f1999a = false;
                SnapHelper.this.a();
            }
        }

        public void a(RecyclerView recyclerView, int i, int i2) {
            if (i != 0 || i2 != 0) {
                this.f1999a = true;
            }
        }
    };

    public abstract int a(RecyclerView.h hVar, int i, int i2);

    public abstract View a(RecyclerView.h hVar);

    public abstract int[] a(RecyclerView.h hVar, View view);

    public boolean a(int i, int i2) {
        RecyclerView.h layoutManager = this.f1996a.getLayoutManager();
        if (layoutManager == null || this.f1996a.getAdapter() == null) {
            return false;
        }
        int minFlingVelocity = this.f1996a.getMinFlingVelocity();
        if ((Math.abs(i2) > minFlingVelocity || Math.abs(i) > minFlingVelocity) && b(layoutManager, i, i2)) {
            return true;
        }
        return false;
    }

    public int[] b(int i, int i2) {
        this.f1997b.fling(0, 0, i, i2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return new int[]{this.f1997b.getFinalX(), this.f1997b.getFinalY()};
    }

    private boolean b(RecyclerView.h hVar, int i, int i2) {
        LinearSmoothScroller b2;
        int a2;
        if (!(hVar instanceof RecyclerView.q.b) || (b2 = b(hVar)) == null || (a2 = a(hVar, i, i2)) == -1) {
            return false;
        }
        b2.d(a2);
        hVar.a(b2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        RecyclerView.h layoutManager;
        View a2;
        if (this.f1996a != null && (layoutManager = this.f1996a.getLayoutManager()) != null && (a2 = a(layoutManager)) != null) {
            int[] a3 = a(layoutManager, a2);
            if (a3[0] != 0 || a3[1] != 0) {
                this.f1996a.smoothScrollBy(a3[0], a3[1]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public LinearSmoothScroller b(RecyclerView.h hVar) {
        if (!(hVar instanceof RecyclerView.q.b)) {
            return null;
        }
        return new LinearSmoothScroller(this.f1996a.getContext()) {
            /* access modifiers changed from: protected */
            public void a(View view, RecyclerView.r rVar, RecyclerView.q.a aVar) {
                int[] a2 = SnapHelper.this.a(SnapHelper.this.f1996a.getLayoutManager(), view);
                int i = a2[0];
                int i2 = a2[1];
                int a3 = a(Math.max(Math.abs(i), Math.abs(i2)));
                if (a3 > 0) {
                    aVar.a(i, i2, a3, this.f1816b);
                }
            }

            /* access modifiers changed from: protected */
            public float a(DisplayMetrics displayMetrics) {
                return 100.0f / ((float) displayMetrics.densityDpi);
            }
        };
    }
}
