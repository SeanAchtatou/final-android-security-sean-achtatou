package android.support.v7.widget;

/* compiled from: RtlSpacingHelper */
class ag {

    /* renamed from: a  reason: collision with root package name */
    private int f2113a = 0;

    /* renamed from: b  reason: collision with root package name */
    private int f2114b = 0;

    /* renamed from: c  reason: collision with root package name */
    private int f2115c = Integer.MIN_VALUE;

    /* renamed from: d  reason: collision with root package name */
    private int f2116d = Integer.MIN_VALUE;

    /* renamed from: e  reason: collision with root package name */
    private int f2117e = 0;

    /* renamed from: f  reason: collision with root package name */
    private int f2118f = 0;

    /* renamed from: g  reason: collision with root package name */
    private boolean f2119g = false;

    /* renamed from: h  reason: collision with root package name */
    private boolean f2120h = false;

    ag() {
    }

    public int a() {
        return this.f2113a;
    }

    public int b() {
        return this.f2114b;
    }

    public int c() {
        return this.f2119g ? this.f2114b : this.f2113a;
    }

    public int d() {
        return this.f2119g ? this.f2113a : this.f2114b;
    }

    public void a(int i, int i2) {
        this.f2115c = i;
        this.f2116d = i2;
        this.f2120h = true;
        if (this.f2119g) {
            if (i2 != Integer.MIN_VALUE) {
                this.f2113a = i2;
            }
            if (i != Integer.MIN_VALUE) {
                this.f2114b = i;
                return;
            }
            return;
        }
        if (i != Integer.MIN_VALUE) {
            this.f2113a = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f2114b = i2;
        }
    }

    public void b(int i, int i2) {
        this.f2120h = false;
        if (i != Integer.MIN_VALUE) {
            this.f2117e = i;
            this.f2113a = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f2118f = i2;
            this.f2114b = i2;
        }
    }

    public void a(boolean z) {
        if (z != this.f2119g) {
            this.f2119g = z;
            if (!this.f2120h) {
                this.f2113a = this.f2117e;
                this.f2114b = this.f2118f;
            } else if (z) {
                this.f2113a = this.f2116d != Integer.MIN_VALUE ? this.f2116d : this.f2117e;
                this.f2114b = this.f2115c != Integer.MIN_VALUE ? this.f2115c : this.f2118f;
            } else {
                this.f2113a = this.f2115c != Integer.MIN_VALUE ? this.f2115c : this.f2117e;
                this.f2114b = this.f2116d != Integer.MIN_VALUE ? this.f2116d : this.f2118f;
            }
        }
    }
}
