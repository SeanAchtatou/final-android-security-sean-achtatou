package android.support.v7.widget;

import android.graphics.PointF;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class LinearSnapHelper extends SnapHelper {

    /* renamed from: b  reason: collision with root package name */
    private ac f1821b;

    /* renamed from: c  reason: collision with root package name */
    private ac f1822c;

    public int[] a(RecyclerView.h hVar, View view) {
        int[] iArr = new int[2];
        if (hVar.d()) {
            iArr[0] = a(hVar, view, d(hVar));
        } else {
            iArr[0] = 0;
        }
        if (hVar.e()) {
            iArr[1] = a(hVar, view, c(hVar));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    public int a(RecyclerView.h hVar, int i, int i2) {
        int i3;
        int i4;
        if (!(hVar instanceof RecyclerView.q.b)) {
            return -1;
        }
        int E = hVar.E();
        if (E == 0) {
            return -1;
        }
        View a2 = a(hVar);
        if (a2 == null) {
            return -1;
        }
        int d2 = hVar.d(a2);
        if (d2 == -1) {
            return -1;
        }
        PointF d3 = ((RecyclerView.q.b) hVar).d(E - 1);
        if (d3 == null) {
            return -1;
        }
        if (hVar.d()) {
            i3 = a(hVar, d(hVar), i, 0);
            if (d3.x < 0.0f) {
                i3 = -i3;
            }
        } else {
            i3 = 0;
        }
        if (hVar.e()) {
            i4 = a(hVar, c(hVar), 0, i2);
            if (d3.y < 0.0f) {
                i4 = -i4;
            }
        } else {
            i4 = 0;
        }
        if (!hVar.e()) {
            i4 = i3;
        }
        if (i4 == 0) {
            return -1;
        }
        int i5 = d2 + i4;
        if (i5 < 0) {
            i5 = 0;
        }
        if (i5 >= E) {
            return E - 1;
        }
        return i5;
    }

    public View a(RecyclerView.h hVar) {
        if (hVar.e()) {
            return a(hVar, c(hVar));
        }
        if (hVar.d()) {
            return a(hVar, d(hVar));
        }
        return null;
    }

    private int a(RecyclerView.h hVar, View view, ac acVar) {
        int e2;
        int e3 = (acVar.e(view) / 2) + acVar.a(view);
        if (hVar.q()) {
            e2 = acVar.c() + (acVar.f() / 2);
        } else {
            e2 = acVar.e() / 2;
        }
        return e3 - e2;
    }

    private int a(RecyclerView.h hVar, ac acVar, int i, int i2) {
        int[] b2 = b(i, i2);
        float b3 = b(hVar, acVar);
        if (b3 <= 0.0f) {
            return 0;
        }
        int i3 = Math.abs(b2[0]) > Math.abs(b2[1]) ? b2[0] : b2[1];
        if (i3 > 0) {
            return (int) Math.floor((double) (((float) i3) / b3));
        }
        return (int) Math.ceil((double) (((float) i3) / b3));
    }

    private View a(RecyclerView.h hVar, ac acVar) {
        int e2;
        View view;
        View view2 = null;
        int u = hVar.u();
        if (u != 0) {
            if (hVar.q()) {
                e2 = acVar.c() + (acVar.f() / 2);
            } else {
                e2 = acVar.e() / 2;
            }
            int i = Integer.MAX_VALUE;
            int i2 = 0;
            while (i2 < u) {
                View i3 = hVar.i(i2);
                int abs = Math.abs((acVar.a(i3) + (acVar.e(i3) / 2)) - e2);
                if (abs < i) {
                    view = i3;
                } else {
                    abs = i;
                    view = view2;
                }
                i2++;
                view2 = view;
                i = abs;
            }
        }
        return view2;
    }

    private float b(RecyclerView.h hVar, ac acVar) {
        int i;
        View view;
        View view2;
        View view3 = null;
        int i2 = Integer.MAX_VALUE;
        int u = hVar.u();
        if (u == 0) {
            return 1.0f;
        }
        int i3 = 0;
        View view4 = null;
        int i4 = Integer.MIN_VALUE;
        while (i3 < u) {
            View i5 = hVar.i(i3);
            int d2 = hVar.d(i5);
            if (d2 == -1) {
                i = i2;
                view = view3;
                view2 = view4;
            } else {
                if (d2 < i2) {
                    i2 = d2;
                    view4 = i5;
                }
                if (d2 > i4) {
                    i4 = d2;
                    view2 = view4;
                    i = i2;
                    view = i5;
                } else {
                    i = i2;
                    view = view3;
                    view2 = view4;
                }
            }
            i3++;
            view4 = view2;
            view3 = view;
            i2 = i;
        }
        if (view4 == null || view3 == null) {
            return 1.0f;
        }
        int max = Math.max(acVar.b(view4), acVar.b(view3)) - Math.min(acVar.a(view4), acVar.a(view3));
        if (max == 0) {
            return 1.0f;
        }
        return (((float) max) * 1.0f) / ((float) ((i4 - i2) + 1));
    }

    private ac c(RecyclerView.h hVar) {
        if (this.f1821b == null || this.f1821b.f2093a != hVar) {
            this.f1821b = ac.b(hVar);
        }
        return this.f1821b;
    }

    private ac d(RecyclerView.h hVar) {
        if (this.f1822c == null || this.f1822c.f2093a != hVar) {
            this.f1822c = ac.a(hVar);
        }
        return this.f1822c;
    }
}
