package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.lang.ref.WeakReference;

public final class ViewStubCompat extends View {

    /* renamed from: a  reason: collision with root package name */
    private int f2086a;

    /* renamed from: b  reason: collision with root package name */
    private int f2087b;

    /* renamed from: c  reason: collision with root package name */
    private WeakReference<View> f2088c;

    /* renamed from: d  reason: collision with root package name */
    private LayoutInflater f2089d;

    /* renamed from: e  reason: collision with root package name */
    private a f2090e;

    public interface a {
        void a(ViewStubCompat viewStubCompat, View view);
    }

    public ViewStubCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ViewStubCompat(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2086a = 0;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.k.ViewStubCompat, i, 0);
        this.f2087b = obtainStyledAttributes.getResourceId(a.k.ViewStubCompat_android_inflatedId, -1);
        this.f2086a = obtainStyledAttributes.getResourceId(a.k.ViewStubCompat_android_layout, 0);
        setId(obtainStyledAttributes.getResourceId(a.k.ViewStubCompat_android_id, -1));
        obtainStyledAttributes.recycle();
        setVisibility(8);
        setWillNotDraw(true);
    }

    public int getInflatedId() {
        return this.f2087b;
    }

    public void setInflatedId(int i) {
        this.f2087b = i;
    }

    public int getLayoutResource() {
        return this.f2086a;
    }

    public void setLayoutResource(int i) {
        this.f2086a = i;
    }

    public void setLayoutInflater(LayoutInflater layoutInflater) {
        this.f2089d = layoutInflater;
    }

    public LayoutInflater getLayoutInflater() {
        return this.f2089d;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(0, 0);
    }

    public void draw(Canvas canvas) {
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
    }

    public void setVisibility(int i) {
        if (this.f2088c != null) {
            View view = this.f2088c.get();
            if (view != null) {
                view.setVisibility(i);
                return;
            }
            throw new IllegalStateException("setVisibility called on un-referenced view");
        }
        super.setVisibility(i);
        if (i == 0 || i == 4) {
            a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a() {
        LayoutInflater from;
        ViewParent parent = getParent();
        if (parent == null || !(parent instanceof ViewGroup)) {
            throw new IllegalStateException("ViewStub must have a non-null ViewGroup viewParent");
        } else if (this.f2086a != 0) {
            ViewGroup viewGroup = (ViewGroup) parent;
            if (this.f2089d != null) {
                from = this.f2089d;
            } else {
                from = LayoutInflater.from(getContext());
            }
            View inflate = from.inflate(this.f2086a, viewGroup, false);
            if (this.f2087b != -1) {
                inflate.setId(this.f2087b);
            }
            int indexOfChild = viewGroup.indexOfChild(this);
            viewGroup.removeViewInLayout(this);
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (layoutParams != null) {
                viewGroup.addView(inflate, indexOfChild, layoutParams);
            } else {
                viewGroup.addView(inflate, indexOfChild);
            }
            this.f2088c = new WeakReference<>(inflate);
            if (this.f2090e != null) {
                this.f2090e.a(this, inflate);
            }
            return inflate;
        } else {
            throw new IllegalArgumentException("ViewStub must have a valid layoutResource");
        }
    }

    public void setOnInflateListener(a aVar) {
        this.f2090e = aVar;
    }
}
