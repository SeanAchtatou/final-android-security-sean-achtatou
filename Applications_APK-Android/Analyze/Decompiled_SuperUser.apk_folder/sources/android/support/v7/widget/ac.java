package android.support.v7.widget;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/* compiled from: OrientationHelper */
public abstract class ac {

    /* renamed from: a  reason: collision with root package name */
    protected final RecyclerView.h f2093a;

    /* renamed from: b  reason: collision with root package name */
    final Rect f2094b;

    /* renamed from: c  reason: collision with root package name */
    private int f2095c;

    public abstract int a(View view);

    public abstract void a(int i);

    public abstract int b(View view);

    public abstract int c();

    public abstract int c(View view);

    public abstract int d();

    public abstract int d(View view);

    public abstract int e();

    public abstract int e(View view);

    public abstract int f();

    public abstract int f(View view);

    public abstract int g();

    public abstract int h();

    public abstract int i();

    private ac(RecyclerView.h hVar) {
        this.f2095c = Integer.MIN_VALUE;
        this.f2094b = new Rect();
        this.f2093a = hVar;
    }

    public void a() {
        this.f2095c = f();
    }

    public int b() {
        if (Integer.MIN_VALUE == this.f2095c) {
            return 0;
        }
        return f() - this.f2095c;
    }

    public static ac a(RecyclerView.h hVar, int i) {
        switch (i) {
            case 0:
                return a(hVar);
            case 1:
                return b(hVar);
            default:
                throw new IllegalArgumentException("invalid orientation");
        }
    }

    public static ac a(RecyclerView.h hVar) {
        return new ac(hVar) {
            public int d() {
                return this.f2093a.x() - this.f2093a.B();
            }

            public int e() {
                return this.f2093a.x();
            }

            public void a(int i) {
                this.f2093a.j(i);
            }

            public int c() {
                return this.f2093a.z();
            }

            public int e(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return layoutParams.rightMargin + this.f2093a.f(view) + layoutParams.leftMargin;
            }

            public int f(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return layoutParams.bottomMargin + this.f2093a.g(view) + layoutParams.topMargin;
            }

            public int b(View view) {
                return ((RecyclerView.LayoutParams) view.getLayoutParams()).rightMargin + this.f2093a.j(view);
            }

            public int a(View view) {
                return this.f2093a.h(view) - ((RecyclerView.LayoutParams) view.getLayoutParams()).leftMargin;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
             arg types: [android.view.View, int, android.graphics.Rect]
             candidates:
              android.support.v7.widget.RecyclerView.h.a(int, int, int):int
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void
              android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
              android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void */
            public int c(View view) {
                this.f2093a.a(view, true, this.f2094b);
                return this.f2094b.right;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
             arg types: [android.view.View, int, android.graphics.Rect]
             candidates:
              android.support.v7.widget.RecyclerView.h.a(int, int, int):int
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void
              android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
              android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void */
            public int d(View view) {
                this.f2093a.a(view, true, this.f2094b);
                return this.f2094b.left;
            }

            public int f() {
                return (this.f2093a.x() - this.f2093a.z()) - this.f2093a.B();
            }

            public int g() {
                return this.f2093a.B();
            }

            public int h() {
                return this.f2093a.v();
            }

            public int i() {
                return this.f2093a.w();
            }
        };
    }

    public static ac b(RecyclerView.h hVar) {
        return new ac(hVar) {
            public int d() {
                return this.f2093a.y() - this.f2093a.C();
            }

            public int e() {
                return this.f2093a.y();
            }

            public void a(int i) {
                this.f2093a.k(i);
            }

            public int c() {
                return this.f2093a.A();
            }

            public int e(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return layoutParams.bottomMargin + this.f2093a.g(view) + layoutParams.topMargin;
            }

            public int f(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return layoutParams.rightMargin + this.f2093a.f(view) + layoutParams.leftMargin;
            }

            public int b(View view) {
                return ((RecyclerView.LayoutParams) view.getLayoutParams()).bottomMargin + this.f2093a.k(view);
            }

            public int a(View view) {
                return this.f2093a.i(view) - ((RecyclerView.LayoutParams) view.getLayoutParams()).topMargin;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
             arg types: [android.view.View, int, android.graphics.Rect]
             candidates:
              android.support.v7.widget.RecyclerView.h.a(int, int, int):int
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void
              android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
              android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void */
            public int c(View view) {
                this.f2093a.a(view, true, this.f2094b);
                return this.f2094b.bottom;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void
             arg types: [android.view.View, int, android.graphics.Rect]
             candidates:
              android.support.v7.widget.RecyclerView.h.a(int, int, int):int
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, int, android.view.View):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, boolean):void
              android.support.v7.widget.RecyclerView.h.a(int, android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r):int
              android.support.v7.widget.RecyclerView.h.a(android.graphics.Rect, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.support.v4.view.a.e):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView$n, android.support.v7.widget.RecyclerView$r, android.view.accessibility.AccessibilityEvent):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.support.v7.widget.RecyclerView$r, int):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, int):void
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.support.v7.widget.RecyclerView$LayoutParams):void
              android.support.v7.widget.RecyclerView.h.a(android.support.v7.widget.RecyclerView, android.view.View, android.view.View):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, int, android.os.Bundle):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, boolean):boolean
              android.support.v7.widget.RecyclerView.h.a(android.view.View, boolean, android.graphics.Rect):void */
            public int d(View view) {
                this.f2093a.a(view, true, this.f2094b);
                return this.f2094b.top;
            }

            public int f() {
                return (this.f2093a.y() - this.f2093a.A()) - this.f2093a.C();
            }

            public int g() {
                return this.f2093a.C();
            }

            public int h() {
                return this.f2093a.w();
            }

            public int i() {
                return this.f2093a.v();
            }
        };
    }
}
