package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ag;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class ActionBarContainer extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    Drawable f1590a;

    /* renamed from: b  reason: collision with root package name */
    Drawable f1591b;

    /* renamed from: c  reason: collision with root package name */
    Drawable f1592c;

    /* renamed from: d  reason: collision with root package name */
    boolean f1593d;

    /* renamed from: e  reason: collision with root package name */
    boolean f1594e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f1595f;

    /* renamed from: g  reason: collision with root package name */
    private View f1596g;

    /* renamed from: h  reason: collision with root package name */
    private View f1597h;
    private View i;
    private int j;

    public ActionBarContainer(Context context) {
        this(context, null);
    }

    public ActionBarContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Drawable aVar;
        if (Build.VERSION.SDK_INT >= 21) {
            aVar = new b(this);
        } else {
            aVar = new a(this);
        }
        ag.a(this, aVar);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.k.ActionBar);
        this.f1590a = obtainStyledAttributes.getDrawable(a.k.ActionBar_background);
        this.f1591b = obtainStyledAttributes.getDrawable(a.k.ActionBar_backgroundStacked);
        this.j = obtainStyledAttributes.getDimensionPixelSize(a.k.ActionBar_height, -1);
        if (getId() == a.f.split_action_bar) {
            this.f1593d = true;
            this.f1592c = obtainStyledAttributes.getDrawable(a.k.ActionBar_backgroundSplit);
        }
        obtainStyledAttributes.recycle();
        setWillNotDraw(this.f1593d ? this.f1592c == null : this.f1590a == null && this.f1591b == null);
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.f1597h = findViewById(a.f.action_bar);
        this.i = findViewById(a.f.action_context_bar);
    }

    public void setPrimaryBackground(Drawable drawable) {
        boolean z = true;
        if (this.f1590a != null) {
            this.f1590a.setCallback(null);
            unscheduleDrawable(this.f1590a);
        }
        this.f1590a = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.f1597h != null) {
                this.f1590a.setBounds(this.f1597h.getLeft(), this.f1597h.getTop(), this.f1597h.getRight(), this.f1597h.getBottom());
            }
        }
        if (this.f1593d) {
            if (this.f1592c != null) {
                z = false;
            }
        } else if (!(this.f1590a == null && this.f1591b == null)) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setStackedBackground(Drawable drawable) {
        boolean z = true;
        if (this.f1591b != null) {
            this.f1591b.setCallback(null);
            unscheduleDrawable(this.f1591b);
        }
        this.f1591b = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.f1594e && this.f1591b != null) {
                this.f1591b.setBounds(this.f1596g.getLeft(), this.f1596g.getTop(), this.f1596g.getRight(), this.f1596g.getBottom());
            }
        }
        if (this.f1593d) {
            if (this.f1592c != null) {
                z = false;
            }
        } else if (!(this.f1590a == null && this.f1591b == null)) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setSplitBackground(Drawable drawable) {
        boolean z = true;
        if (this.f1592c != null) {
            this.f1592c.setCallback(null);
            unscheduleDrawable(this.f1592c);
        }
        this.f1592c = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.f1593d && this.f1592c != null) {
                this.f1592c.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            }
        }
        if (this.f1593d) {
            if (this.f1592c != null) {
                z = false;
            }
        } else if (!(this.f1590a == null && this.f1591b == null)) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setVisibility(int i2) {
        boolean z;
        super.setVisibility(i2);
        if (i2 == 0) {
            z = true;
        } else {
            z = false;
        }
        if (this.f1590a != null) {
            this.f1590a.setVisible(z, false);
        }
        if (this.f1591b != null) {
            this.f1591b.setVisible(z, false);
        }
        if (this.f1592c != null) {
            this.f1592c.setVisible(z, false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return (drawable == this.f1590a && !this.f1593d) || (drawable == this.f1591b && this.f1594e) || ((drawable == this.f1592c && this.f1593d) || super.verifyDrawable(drawable));
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f1590a != null && this.f1590a.isStateful()) {
            this.f1590a.setState(getDrawableState());
        }
        if (this.f1591b != null && this.f1591b.isStateful()) {
            this.f1591b.setState(getDrawableState());
        }
        if (this.f1592c != null && this.f1592c.isStateful()) {
            this.f1592c.setState(getDrawableState());
        }
    }

    public void jumpDrawablesToCurrentState() {
        if (Build.VERSION.SDK_INT >= 11) {
            super.jumpDrawablesToCurrentState();
            if (this.f1590a != null) {
                this.f1590a.jumpToCurrentState();
            }
            if (this.f1591b != null) {
                this.f1591b.jumpToCurrentState();
            }
            if (this.f1592c != null) {
                this.f1592c.jumpToCurrentState();
            }
        }
    }

    public void setTransitioning(boolean z) {
        this.f1595f = z;
        setDescendantFocusability(z ? 393216 : 262144);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.f1595f || super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        return true;
    }

    public void setTabContainer(ScrollingTabContainerView scrollingTabContainerView) {
        if (this.f1596g != null) {
            removeView(this.f1596g);
        }
        this.f1596g = scrollingTabContainerView;
        if (scrollingTabContainerView != null) {
            addView(scrollingTabContainerView);
            ViewGroup.LayoutParams layoutParams = scrollingTabContainerView.getLayoutParams();
            layoutParams.width = -1;
            layoutParams.height = -2;
            scrollingTabContainerView.setAllowCollapse(false);
        }
    }

    public View getTabContainer() {
        return this.f1596g;
    }

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback) {
        return null;
    }

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback, int i2) {
        if (i2 != 0) {
            return super.startActionModeForChild(view, callback, i2);
        }
        return null;
    }

    private boolean a(View view) {
        return view == null || view.getVisibility() == 8 || view.getMeasuredHeight() == 0;
    }

    private int b(View view) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        return layoutParams.bottomMargin + view.getMeasuredHeight() + layoutParams.topMargin;
    }

    public void onMeasure(int i2, int i3) {
        int i4;
        if (this.f1597h == null && View.MeasureSpec.getMode(i3) == Integer.MIN_VALUE && this.j >= 0) {
            i3 = View.MeasureSpec.makeMeasureSpec(Math.min(this.j, View.MeasureSpec.getSize(i3)), Integer.MIN_VALUE);
        }
        super.onMeasure(i2, i3);
        if (this.f1597h != null) {
            int mode = View.MeasureSpec.getMode(i3);
            if (this.f1596g != null && this.f1596g.getVisibility() != 8 && mode != 1073741824) {
                if (!a(this.f1597h)) {
                    i4 = b(this.f1597h);
                } else if (!a(this.i)) {
                    i4 = b(this.i);
                } else {
                    i4 = 0;
                }
                setMeasuredDimension(getMeasuredWidth(), Math.min(i4 + b(this.f1596g), mode == Integer.MIN_VALUE ? View.MeasureSpec.getSize(i3) : Integer.MAX_VALUE));
            }
        }
    }

    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        boolean z2;
        boolean z3 = true;
        super.onLayout(z, i2, i3, i4, i5);
        View view = this.f1596g;
        boolean z4 = (view == null || view.getVisibility() == 8) ? false : true;
        if (!(view == null || view.getVisibility() == 8)) {
            int measuredHeight = getMeasuredHeight();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
            view.layout(i2, (measuredHeight - view.getMeasuredHeight()) - layoutParams.bottomMargin, i4, measuredHeight - layoutParams.bottomMargin);
        }
        if (!this.f1593d) {
            if (this.f1590a != null) {
                if (this.f1597h.getVisibility() == 0) {
                    this.f1590a.setBounds(this.f1597h.getLeft(), this.f1597h.getTop(), this.f1597h.getRight(), this.f1597h.getBottom());
                } else if (this.i == null || this.i.getVisibility() != 0) {
                    this.f1590a.setBounds(0, 0, 0, 0);
                } else {
                    this.f1590a.setBounds(this.i.getLeft(), this.i.getTop(), this.i.getRight(), this.i.getBottom());
                }
                z2 = true;
            } else {
                z2 = false;
            }
            this.f1594e = z4;
            if (!z4 || this.f1591b == null) {
                z3 = z2;
            } else {
                this.f1591b.setBounds(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
        } else if (this.f1592c != null) {
            this.f1592c.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
        } else {
            z3 = false;
        }
        if (z3) {
            invalidate();
        }
    }
}
