package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ChildHelper */
class r {

    /* renamed from: a  reason: collision with root package name */
    final b f2260a;

    /* renamed from: b  reason: collision with root package name */
    final a f2261b = new a();

    /* renamed from: c  reason: collision with root package name */
    final List<View> f2262c = new ArrayList();

    /* compiled from: ChildHelper */
    interface b {
        int a();

        int a(View view);

        void a(int i);

        void a(View view, int i);

        void a(View view, int i, ViewGroup.LayoutParams layoutParams);

        RecyclerView.u b(View view);

        View b(int i);

        void b();

        void c(int i);

        void c(View view);

        void d(View view);
    }

    r(b bVar) {
        this.f2260a = bVar;
    }

    private void g(View view) {
        this.f2262c.add(view);
        this.f2260a.c(view);
    }

    private boolean h(View view) {
        if (!this.f2262c.remove(view)) {
            return false;
        }
        this.f2260a.d(view);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(View view, boolean z) {
        a(view, -1, z);
    }

    /* access modifiers changed from: package-private */
    public void a(View view, int i, boolean z) {
        int f2;
        if (i < 0) {
            f2 = this.f2260a.a();
        } else {
            f2 = f(i);
        }
        this.f2261b.a(f2, z);
        if (z) {
            g(view);
        }
        this.f2260a.a(view, f2);
    }

    private int f(int i) {
        if (i < 0) {
            return -1;
        }
        int a2 = this.f2260a.a();
        int i2 = i;
        while (i2 < a2) {
            int e2 = i - (i2 - this.f2261b.e(i2));
            if (e2 == 0) {
                while (this.f2261b.c(i2)) {
                    i2++;
                }
                return i2;
            }
            i2 += e2;
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        int a2 = this.f2260a.a(view);
        if (a2 >= 0) {
            if (this.f2261b.d(a2)) {
                h(view);
            }
            this.f2260a.a(a2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        int f2 = f(i);
        View b2 = this.f2260a.b(f2);
        if (b2 != null) {
            if (this.f2261b.d(f2)) {
                h(b2);
            }
            this.f2260a.a(f2);
        }
    }

    /* access modifiers changed from: package-private */
    public View b(int i) {
        return this.f2260a.b(f(i));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f2261b.a();
        for (int size = this.f2262c.size() - 1; size >= 0; size--) {
            this.f2260a.d(this.f2262c.get(size));
            this.f2262c.remove(size);
        }
        this.f2260a.b();
    }

    /* access modifiers changed from: package-private */
    public View c(int i) {
        int size = this.f2262c.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = this.f2262c.get(i2);
            RecyclerView.u b2 = this.f2260a.b(view);
            if (b2.getLayoutPosition() == i && !b2.isInvalid() && !b2.isRemoved()) {
                return view;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        int f2;
        if (i < 0) {
            f2 = this.f2260a.a();
        } else {
            f2 = f(i);
        }
        this.f2261b.a(f2, z);
        if (z) {
            g(view);
        }
        this.f2260a.a(view, f2, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.f2260a.a() - this.f2262c.size();
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.f2260a.a();
    }

    /* access modifiers changed from: package-private */
    public View d(int i) {
        return this.f2260a.b(i);
    }

    /* access modifiers changed from: package-private */
    public void e(int i) {
        int f2 = f(i);
        this.f2261b.d(f2);
        this.f2260a.c(f2);
    }

    /* access modifiers changed from: package-private */
    public int b(View view) {
        int a2 = this.f2260a.a(view);
        if (a2 != -1 && !this.f2261b.c(a2)) {
            return a2 - this.f2261b.e(a2);
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public boolean c(View view) {
        return this.f2262c.contains(view);
    }

    /* access modifiers changed from: package-private */
    public void d(View view) {
        int a2 = this.f2260a.a(view);
        if (a2 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        }
        this.f2261b.a(a2);
        g(view);
    }

    /* access modifiers changed from: package-private */
    public void e(View view) {
        int a2 = this.f2260a.a(view);
        if (a2 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        } else if (!this.f2261b.c(a2)) {
            throw new RuntimeException("trying to unhide a view that was not hidden" + view);
        } else {
            this.f2261b.b(a2);
            h(view);
        }
    }

    public String toString() {
        return this.f2261b.toString() + ", hidden list:" + this.f2262c.size();
    }

    /* access modifiers changed from: package-private */
    public boolean f(View view) {
        int a2 = this.f2260a.a(view);
        if (a2 == -1) {
            if (h(view)) {
            }
            return true;
        } else if (!this.f2261b.c(a2)) {
            return false;
        } else {
            this.f2261b.d(a2);
            if (!h(view)) {
            }
            this.f2260a.a(a2);
            return true;
        }
    }

    /* compiled from: ChildHelper */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        long f2263a = 0;

        /* renamed from: b  reason: collision with root package name */
        a f2264b;

        a() {
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            if (i >= 64) {
                b();
                this.f2264b.a(i - 64);
                return;
            }
            this.f2263a |= 1 << i;
        }

        private void b() {
            if (this.f2264b == null) {
                this.f2264b = new a();
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i) {
            if (i < 64) {
                this.f2263a &= (1 << i) ^ -1;
            } else if (this.f2264b != null) {
                this.f2264b.b(i - 64);
            }
        }

        /* access modifiers changed from: package-private */
        public boolean c(int i) {
            if (i < 64) {
                return (this.f2263a & (1 << i)) != 0;
            }
            b();
            return this.f2264b.c(i - 64);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f2263a = 0;
            if (this.f2264b != null) {
                this.f2264b.a();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i, boolean z) {
            if (i >= 64) {
                b();
                this.f2264b.a(i - 64, z);
                return;
            }
            boolean z2 = (this.f2263a & Long.MIN_VALUE) != 0;
            long j = (1 << i) - 1;
            this.f2263a = (((j ^ -1) & this.f2263a) << 1) | (this.f2263a & j);
            if (z) {
                a(i);
            } else {
                b(i);
            }
            if (z2 || this.f2264b != null) {
                b();
                this.f2264b.a(0, z2);
            }
        }

        /* access modifiers changed from: package-private */
        public boolean d(int i) {
            if (i >= 64) {
                b();
                return this.f2264b.d(i - 64);
            }
            long j = 1 << i;
            boolean z = (this.f2263a & j) != 0;
            this.f2263a &= j ^ -1;
            long j2 = j - 1;
            this.f2263a = Long.rotateRight((j2 ^ -1) & this.f2263a, 1) | (this.f2263a & j2);
            if (this.f2264b == null) {
                return z;
            }
            if (this.f2264b.c(0)) {
                a(63);
            }
            this.f2264b.d(0);
            return z;
        }

        /* access modifiers changed from: package-private */
        public int e(int i) {
            if (this.f2264b == null) {
                if (i >= 64) {
                    return Long.bitCount(this.f2263a);
                }
                return Long.bitCount(this.f2263a & ((1 << i) - 1));
            } else if (i < 64) {
                return Long.bitCount(this.f2263a & ((1 << i) - 1));
            } else {
                return this.f2264b.e(i - 64) + Long.bitCount(this.f2263a);
            }
        }

        public String toString() {
            if (this.f2264b == null) {
                return Long.toBinaryString(this.f2263a);
            }
            return this.f2264b.toString() + "xx" + Long.toBinaryString(this.f2263a);
        }
    }
}
