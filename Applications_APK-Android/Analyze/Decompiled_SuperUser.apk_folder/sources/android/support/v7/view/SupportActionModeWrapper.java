package android.support.v7.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.internal.view.a;
import android.support.v4.util.i;
import android.support.v7.view.b;
import android.support.v7.view.menu.k;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;

@TargetApi(11)
public class SupportActionModeWrapper extends ActionMode {

    /* renamed from: a  reason: collision with root package name */
    final Context f1427a;

    /* renamed from: b  reason: collision with root package name */
    final b f1428b;

    public SupportActionModeWrapper(Context context, b bVar) {
        this.f1427a = context;
        this.f1428b = bVar;
    }

    public Object getTag() {
        return this.f1428b.j();
    }

    public void setTag(Object obj) {
        this.f1428b.a(obj);
    }

    public void setTitle(CharSequence charSequence) {
        this.f1428b.b(charSequence);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f1428b.a(charSequence);
    }

    public void invalidate() {
        this.f1428b.d();
    }

    public void finish() {
        this.f1428b.c();
    }

    public Menu getMenu() {
        return k.a(this.f1427a, (a) this.f1428b.b());
    }

    public CharSequence getTitle() {
        return this.f1428b.f();
    }

    public void setTitle(int i) {
        this.f1428b.a(i);
    }

    public CharSequence getSubtitle() {
        return this.f1428b.g();
    }

    public void setSubtitle(int i) {
        this.f1428b.b(i);
    }

    public View getCustomView() {
        return this.f1428b.i();
    }

    public void setCustomView(View view) {
        this.f1428b.a(view);
    }

    public MenuInflater getMenuInflater() {
        return this.f1428b.a();
    }

    public boolean getTitleOptionalHint() {
        return this.f1428b.k();
    }

    public void setTitleOptionalHint(boolean z) {
        this.f1428b.a(z);
    }

    public boolean isTitleOptional() {
        return this.f1428b.h();
    }

    public static class CallbackWrapper implements b.a {

        /* renamed from: a  reason: collision with root package name */
        final ActionMode.Callback f1429a;

        /* renamed from: b  reason: collision with root package name */
        final Context f1430b;

        /* renamed from: c  reason: collision with root package name */
        final ArrayList<SupportActionModeWrapper> f1431c = new ArrayList<>();

        /* renamed from: d  reason: collision with root package name */
        final i<Menu, Menu> f1432d = new i<>();

        public CallbackWrapper(Context context, ActionMode.Callback callback) {
            this.f1430b = context;
            this.f1429a = callback;
        }

        public boolean a(b bVar, Menu menu) {
            return this.f1429a.onCreateActionMode(b(bVar), a(menu));
        }

        public boolean b(b bVar, Menu menu) {
            return this.f1429a.onPrepareActionMode(b(bVar), a(menu));
        }

        public boolean a(b bVar, MenuItem menuItem) {
            return this.f1429a.onActionItemClicked(b(bVar), k.a(this.f1430b, (android.support.v4.internal.view.b) menuItem));
        }

        public void a(b bVar) {
            this.f1429a.onDestroyActionMode(b(bVar));
        }

        private Menu a(Menu menu) {
            Menu menu2 = this.f1432d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            Menu a2 = k.a(this.f1430b, (a) menu);
            this.f1432d.put(menu, a2);
            return a2;
        }

        public ActionMode b(b bVar) {
            int size = this.f1431c.size();
            for (int i = 0; i < size; i++) {
                SupportActionModeWrapper supportActionModeWrapper = this.f1431c.get(i);
                if (supportActionModeWrapper != null && supportActionModeWrapper.f1428b == bVar) {
                    return supportActionModeWrapper;
                }
            }
            SupportActionModeWrapper supportActionModeWrapper2 = new SupportActionModeWrapper(this.f1430b, bVar);
            this.f1431c.add(supportActionModeWrapper2);
            return supportActionModeWrapper2;
        }
    }
}
