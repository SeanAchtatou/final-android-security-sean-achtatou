package android.support.v7.view.menu;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.internal.view.b;
import android.support.v4.view.ActionProvider;
import android.support.v7.view.menu.MenuItemWrapperICS;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;

@TargetApi(16)
/* compiled from: MenuItemWrapperJB */
class f extends MenuItemWrapperICS {
    f(Context context, b bVar) {
        super(context, bVar);
    }

    /* access modifiers changed from: package-private */
    public MenuItemWrapperICS.a a(ActionProvider actionProvider) {
        return new a(this.f1527a, actionProvider);
    }

    /* compiled from: MenuItemWrapperJB */
    class a extends MenuItemWrapperICS.a implements ActionProvider.VisibilityListener {

        /* renamed from: c  reason: collision with root package name */
        ActionProvider.b f1558c;

        public a(Context context, android.view.ActionProvider actionProvider) {
            super(context, actionProvider);
        }

        public View a(MenuItem menuItem) {
            return this.f1522a.onCreateActionView(menuItem);
        }

        public boolean b() {
            return this.f1522a.overridesItemVisibility();
        }

        public boolean c() {
            return this.f1522a.isVisible();
        }

        public void a(ActionProvider.b bVar) {
            this.f1558c = bVar;
            android.view.ActionProvider actionProvider = this.f1522a;
            if (bVar == null) {
                this = null;
            }
            actionProvider.setVisibilityListener(this);
        }

        public void onActionProviderVisibilityChanged(boolean z) {
            if (this.f1558c != null) {
                this.f1558c.a(z);
            }
        }
    }
}
