package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ag;
import android.support.v7.a.a;
import android.support.v7.view.menu.j;
import android.support.v7.widget.an;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class ListMenuItemView extends LinearLayout implements j.a {

    /* renamed from: a  reason: collision with root package name */
    private MenuItemImpl f1494a;

    /* renamed from: b  reason: collision with root package name */
    private ImageView f1495b;

    /* renamed from: c  reason: collision with root package name */
    private RadioButton f1496c;

    /* renamed from: d  reason: collision with root package name */
    private TextView f1497d;

    /* renamed from: e  reason: collision with root package name */
    private CheckBox f1498e;

    /* renamed from: f  reason: collision with root package name */
    private TextView f1499f;

    /* renamed from: g  reason: collision with root package name */
    private ImageView f1500g;

    /* renamed from: h  reason: collision with root package name */
    private Drawable f1501h;
    private int i;
    private Context j;
    private boolean k;
    private Drawable l;
    private int m;
    private LayoutInflater n;
    private boolean o;

    public ListMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0027a.listMenuViewStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.an.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.an.a(int, float):float
      android.support.v7.widget.an.a(int, int):int
      android.support.v7.widget.an.a(int, boolean):boolean */
    public ListMenuItemView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        an a2 = an.a(getContext(), attributeSet, a.k.MenuView, i2, 0);
        this.f1501h = a2.a(a.k.MenuView_android_itemBackground);
        this.i = a2.g(a.k.MenuView_android_itemTextAppearance, -1);
        this.k = a2.a(a.k.MenuView_preserveIconSpacing, false);
        this.j = context;
        this.l = a2.a(a.k.MenuView_subMenuArrow);
        a2.a();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        ag.a(this, this.f1501h);
        this.f1497d = (TextView) findViewById(a.f.title);
        if (this.i != -1) {
            this.f1497d.setTextAppearance(this.j, this.i);
        }
        this.f1499f = (TextView) findViewById(a.f.shortcut);
        this.f1500g = (ImageView) findViewById(a.f.submenuarrow);
        if (this.f1500g != null) {
            this.f1500g.setImageDrawable(this.l);
        }
    }

    public void initialize(MenuItemImpl menuItemImpl, int i2) {
        this.f1494a = menuItemImpl;
        this.m = i2;
        setVisibility(menuItemImpl.isVisible() ? 0 : 8);
        setTitle(menuItemImpl.a((j.a) this));
        setCheckable(menuItemImpl.isCheckable());
        a(menuItemImpl.f(), menuItemImpl.d());
        setIcon(menuItemImpl.getIcon());
        setEnabled(menuItemImpl.isEnabled());
        setSubMenuArrowVisible(menuItemImpl.hasSubMenu());
    }

    public void setForceShowIcon(boolean z) {
        this.o = z;
        this.k = z;
    }

    public void setTitle(CharSequence charSequence) {
        if (charSequence != null) {
            this.f1497d.setText(charSequence);
            if (this.f1497d.getVisibility() != 0) {
                this.f1497d.setVisibility(0);
            }
        } else if (this.f1497d.getVisibility() != 8) {
            this.f1497d.setVisibility(8);
        }
    }

    public MenuItemImpl getItemData() {
        return this.f1494a;
    }

    public void setCheckable(boolean z) {
        CompoundButton compoundButton;
        CompoundButton compoundButton2;
        int i2;
        if (z || this.f1496c != null || this.f1498e != null) {
            if (this.f1494a.g()) {
                if (this.f1496c == null) {
                    b();
                }
                compoundButton = this.f1496c;
                compoundButton2 = this.f1498e;
            } else {
                if (this.f1498e == null) {
                    c();
                }
                compoundButton = this.f1498e;
                compoundButton2 = this.f1496c;
            }
            if (z) {
                compoundButton.setChecked(this.f1494a.isChecked());
                if (z) {
                    i2 = 0;
                } else {
                    i2 = 8;
                }
                if (compoundButton.getVisibility() != i2) {
                    compoundButton.setVisibility(i2);
                }
                if (compoundButton2 != null && compoundButton2.getVisibility() != 8) {
                    compoundButton2.setVisibility(8);
                    return;
                }
                return;
            }
            if (this.f1498e != null) {
                this.f1498e.setVisibility(8);
            }
            if (this.f1496c != null) {
                this.f1496c.setVisibility(8);
            }
        }
    }

    public void setChecked(boolean z) {
        CompoundButton compoundButton;
        if (this.f1494a.g()) {
            if (this.f1496c == null) {
                b();
            }
            compoundButton = this.f1496c;
        } else {
            if (this.f1498e == null) {
                c();
            }
            compoundButton = this.f1498e;
        }
        compoundButton.setChecked(z);
    }

    private void setSubMenuArrowVisible(boolean z) {
        if (this.f1500g != null) {
            this.f1500g.setVisibility(z ? 0 : 8);
        }
    }

    public void a(boolean z, char c2) {
        int i2 = (!z || !this.f1494a.f()) ? 8 : 0;
        if (i2 == 0) {
            this.f1499f.setText(this.f1494a.e());
        }
        if (this.f1499f.getVisibility() != i2) {
            this.f1499f.setVisibility(i2);
        }
    }

    public void setIcon(Drawable drawable) {
        boolean z = this.f1494a.i() || this.o;
        if (!z && !this.k) {
            return;
        }
        if (this.f1495b != null || drawable != null || this.k) {
            if (this.f1495b == null) {
                a();
            }
            if (drawable != null || this.k) {
                ImageView imageView = this.f1495b;
                if (!z) {
                    drawable = null;
                }
                imageView.setImageDrawable(drawable);
                if (this.f1495b.getVisibility() != 0) {
                    this.f1495b.setVisibility(0);
                    return;
                }
                return;
            }
            this.f1495b.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.f1495b != null && this.k) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.f1495b.getLayoutParams();
            if (layoutParams.height > 0 && layoutParams2.width <= 0) {
                layoutParams2.width = layoutParams.height;
            }
        }
        super.onMeasure(i2, i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        this.f1495b = (ImageView) getInflater().inflate(a.h.abc_list_menu_item_icon, (ViewGroup) this, false);
        addView(this.f1495b, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void b() {
        this.f1496c = (RadioButton) getInflater().inflate(a.h.abc_list_menu_item_radio, (ViewGroup) this, false);
        addView(this.f1496c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void c() {
        this.f1498e = (CheckBox) getInflater().inflate(a.h.abc_list_menu_item_checkbox, (ViewGroup) this, false);
        addView(this.f1498e);
    }

    public boolean prefersCondensedTitle() {
        return false;
    }

    private LayoutInflater getInflater() {
        if (this.n == null) {
            this.n = LayoutInflater.from(getContext());
        }
        return this.n;
    }
}
