package android.support.v7.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.ar;
import android.support.v7.a.a;
import android.view.ViewConfiguration;

/* compiled from: ActionBarPolicy */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private Context f1450a;

    public static a a(Context context) {
        return new a(context);
    }

    private a(Context context) {
        this.f1450a = context;
    }

    public int a() {
        Resources resources = this.f1450a.getResources();
        int b2 = android.support.v4.content.a.a.b(resources);
        int a2 = android.support.v4.content.a.a.a(resources);
        if (android.support.v4.content.a.a.c(resources) > 600 || b2 > 600 || ((b2 > 960 && a2 > 720) || (b2 > 720 && a2 > 960))) {
            return 5;
        }
        if (b2 >= 500 || ((b2 > 640 && a2 > 480) || (b2 > 480 && a2 > 640))) {
            return 4;
        }
        if (b2 >= 360) {
            return 3;
        }
        return 2;
    }

    public boolean b() {
        if (Build.VERSION.SDK_INT < 19 && ar.a(ViewConfiguration.get(this.f1450a))) {
            return false;
        }
        return true;
    }

    public int c() {
        return this.f1450a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    public boolean d() {
        return this.f1450a.getResources().getBoolean(a.b.abc_action_bar_embed_tabs);
    }

    public int e() {
        TypedArray obtainStyledAttributes = this.f1450a.obtainStyledAttributes(null, a.k.ActionBar, a.C0027a.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(a.k.ActionBar_height, 0);
        Resources resources = this.f1450a.getResources();
        if (!d()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(a.d.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    public boolean f() {
        return this.f1450a.getApplicationInfo().targetSdkVersion < 14;
    }

    public int g() {
        return this.f1450a.getResources().getDimensionPixelSize(a.d.abc_action_bar_stacked_tab_max_width);
    }
}
