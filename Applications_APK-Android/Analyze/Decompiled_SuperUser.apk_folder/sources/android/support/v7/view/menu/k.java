package android.support.v7.view.menu;

import android.content.Context;
import android.os.Build;
import android.support.v4.internal.view.SupportSubMenu;
import android.support.v4.internal.view.a;
import android.support.v4.internal.view.b;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

/* compiled from: MenuWrapperFactory */
public final class k {
    public static Menu a(Context context, a aVar) {
        if (Build.VERSION.SDK_INT >= 14) {
            return new l(context, aVar);
        }
        throw new UnsupportedOperationException();
    }

    public static MenuItem a(Context context, b bVar) {
        if (Build.VERSION.SDK_INT >= 16) {
            return new f(context, bVar);
        }
        if (Build.VERSION.SDK_INT >= 14) {
            return new MenuItemWrapperICS(context, bVar);
        }
        throw new UnsupportedOperationException();
    }

    public static SubMenu a(Context context, SupportSubMenu supportSubMenu) {
        if (Build.VERSION.SDK_INT >= 14) {
            return new o(context, supportSubMenu);
        }
        throw new UnsupportedOperationException();
    }
}
