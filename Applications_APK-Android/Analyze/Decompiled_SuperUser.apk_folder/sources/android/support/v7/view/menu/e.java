package android.support.v7.view.menu;

import android.content.DialogInterface;
import android.os.IBinder;
import android.support.v7.a.a;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.i;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/* compiled from: MenuDialogHelper */
class e implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, DialogInterface.OnKeyListener, i.a {

    /* renamed from: a  reason: collision with root package name */
    ListMenuPresenter f1554a;

    /* renamed from: b  reason: collision with root package name */
    private MenuBuilder f1555b;

    /* renamed from: c  reason: collision with root package name */
    private AlertDialog f1556c;

    /* renamed from: d  reason: collision with root package name */
    private i.a f1557d;

    public e(MenuBuilder menuBuilder) {
        this.f1555b = menuBuilder;
    }

    public void a(IBinder iBinder) {
        MenuBuilder menuBuilder = this.f1555b;
        AlertDialog.Builder builder = new AlertDialog.Builder(menuBuilder.getContext());
        this.f1554a = new ListMenuPresenter(builder.a(), a.h.abc_list_menu_item_layout);
        this.f1554a.setCallback(this);
        this.f1555b.addMenuPresenter(this.f1554a);
        builder.a(this.f1554a.a(), this);
        View headerView = menuBuilder.getHeaderView();
        if (headerView != null) {
            builder.a(headerView);
        } else {
            builder.a(menuBuilder.getHeaderIcon()).a(menuBuilder.getHeaderTitle());
        }
        builder.a(this);
        this.f1556c = builder.b();
        this.f1556c.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.f1556c.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.f1556c.show();
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.f1556c.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.f1556c.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.f1555b.close(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.f1555b.performShortcut(i, keyEvent, 0);
    }

    public void a() {
        if (this.f1556c != null) {
            this.f1556c.dismiss();
        }
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.f1554a.onCloseMenu(this.f1555b, true);
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        if (z || menuBuilder == this.f1555b) {
            a();
        }
        if (this.f1557d != null) {
            this.f1557d.a(menuBuilder, z);
        }
    }

    public boolean a(MenuBuilder menuBuilder) {
        if (this.f1557d != null) {
            return this.f1557d.a(menuBuilder);
        }
        return false;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f1555b.performItemAction((MenuItemImpl) this.f1554a.a().getItem(i), 0);
    }
}
