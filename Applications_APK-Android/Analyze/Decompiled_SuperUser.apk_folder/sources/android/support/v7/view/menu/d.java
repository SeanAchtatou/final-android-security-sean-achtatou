package android.support.v7.view.menu;

import android.support.v7.a.a;
import android.support.v7.view.menu.j;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

/* compiled from: MenuAdapter */
public class d extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    static final int f1548a = a.h.abc_popup_menu_item_layout;

    /* renamed from: b  reason: collision with root package name */
    MenuBuilder f1549b;

    /* renamed from: c  reason: collision with root package name */
    private int f1550c = -1;

    /* renamed from: d  reason: collision with root package name */
    private boolean f1551d;

    /* renamed from: e  reason: collision with root package name */
    private final boolean f1552e;

    /* renamed from: f  reason: collision with root package name */
    private final LayoutInflater f1553f;

    public d(MenuBuilder menuBuilder, LayoutInflater layoutInflater, boolean z) {
        this.f1552e = z;
        this.f1553f = layoutInflater;
        this.f1549b = menuBuilder;
        b();
    }

    public void a(boolean z) {
        this.f1551d = z;
    }

    public int getCount() {
        ArrayList<MenuItemImpl> nonActionItems = this.f1552e ? this.f1549b.getNonActionItems() : this.f1549b.getVisibleItems();
        if (this.f1550c < 0) {
            return nonActionItems.size();
        }
        return nonActionItems.size() - 1;
    }

    public MenuBuilder a() {
        return this.f1549b;
    }

    /* renamed from: a */
    public MenuItemImpl getItem(int i) {
        ArrayList<MenuItemImpl> nonActionItems = this.f1552e ? this.f1549b.getNonActionItems() : this.f1549b.getVisibleItems();
        if (this.f1550c >= 0 && i >= this.f1550c) {
            i++;
        }
        return nonActionItems.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            view2 = this.f1553f.inflate(f1548a, viewGroup, false);
        } else {
            view2 = view;
        }
        j.a aVar = (j.a) view2;
        if (this.f1551d) {
            ((ListMenuItemView) view2).setForceShowIcon(true);
        }
        aVar.initialize(getItem(i), 0);
        return view2;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        MenuItemImpl expandedItem = this.f1549b.getExpandedItem();
        if (expandedItem != null) {
            ArrayList<MenuItemImpl> nonActionItems = this.f1549b.getNonActionItems();
            int size = nonActionItems.size();
            for (int i = 0; i < size; i++) {
                if (nonActionItems.get(i) == expandedItem) {
                    this.f1550c = i;
                    return;
                }
            }
        }
        this.f1550c = -1;
    }

    public void notifyDataSetChanged() {
        b();
        super.notifyDataSetChanged();
    }
}
