package android.support.v7.view.menu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.internal.view.b;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.p;
import android.support.v7.view.menu.j;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public final class MenuItemImpl implements b {
    private static String w;
    private static String x;
    private static String y;
    private static String z;

    /* renamed from: a  reason: collision with root package name */
    MenuBuilder f1512a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1513b;

    /* renamed from: c  reason: collision with root package name */
    private final int f1514c;

    /* renamed from: d  reason: collision with root package name */
    private final int f1515d;

    /* renamed from: e  reason: collision with root package name */
    private final int f1516e;

    /* renamed from: f  reason: collision with root package name */
    private CharSequence f1517f;

    /* renamed from: g  reason: collision with root package name */
    private CharSequence f1518g;

    /* renamed from: h  reason: collision with root package name */
    private Intent f1519h;
    private char i;
    private char j;
    private Drawable k;
    private int l = 0;
    private SubMenuBuilder m;
    private Runnable n;
    private MenuItem.OnMenuItemClickListener o;
    private int p = 16;
    private int q = 0;
    private View r;
    private ActionProvider s;
    private p.e t;
    private boolean u = false;
    private ContextMenu.ContextMenuInfo v;

    MenuItemImpl(MenuBuilder menuBuilder, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        this.f1512a = menuBuilder;
        this.f1513b = i3;
        this.f1514c = i2;
        this.f1515d = i4;
        this.f1516e = i5;
        this.f1517f = charSequence;
        this.q = i6;
    }

    public boolean b() {
        if ((this.o != null && this.o.onMenuItemClick(this)) || this.f1512a.dispatchMenuItemSelected(this.f1512a.getRootMenu(), this)) {
            return true;
        }
        if (this.n != null) {
            this.n.run();
            return true;
        }
        if (this.f1519h != null) {
            try {
                this.f1512a.getContext().startActivity(this.f1519h);
                return true;
            } catch (ActivityNotFoundException e2) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e2);
            }
        }
        if (this.s == null || !this.s.d()) {
            return false;
        }
        return true;
    }

    public boolean isEnabled() {
        return (this.p & 16) != 0;
    }

    public MenuItem setEnabled(boolean z2) {
        if (z2) {
            this.p |= 16;
        } else {
            this.p &= -17;
        }
        this.f1512a.onItemsChanged(false);
        return this;
    }

    public int getGroupId() {
        return this.f1514c;
    }

    @ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.f1513b;
    }

    public int getOrder() {
        return this.f1515d;
    }

    public int c() {
        return this.f1516e;
    }

    public Intent getIntent() {
        return this.f1519h;
    }

    public MenuItem setIntent(Intent intent) {
        this.f1519h = intent;
        return this;
    }

    public char getAlphabeticShortcut() {
        return this.j;
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        if (this.j != c2) {
            this.j = Character.toLowerCase(c2);
            this.f1512a.onItemsChanged(false);
        }
        return this;
    }

    public char getNumericShortcut() {
        return this.i;
    }

    public MenuItem setNumericShortcut(char c2) {
        if (this.i != c2) {
            this.i = c2;
            this.f1512a.onItemsChanged(false);
        }
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        this.i = c2;
        this.j = Character.toLowerCase(c3);
        this.f1512a.onItemsChanged(false);
        return this;
    }

    /* access modifiers changed from: package-private */
    public char d() {
        return this.f1512a.isQwertyMode() ? this.j : this.i;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        char d2 = d();
        if (d2 == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(w);
        switch (d2) {
            case 8:
                sb.append(y);
                break;
            case 10:
                sb.append(x);
                break;
            case ' ':
                sb.append(z);
                break;
            default:
                sb.append(d2);
                break;
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.f1512a.isShortcutsVisible() && d() != 0;
    }

    public SubMenu getSubMenu() {
        return this.m;
    }

    public boolean hasSubMenu() {
        return this.m != null;
    }

    public void a(SubMenuBuilder subMenuBuilder) {
        this.m = subMenuBuilder;
        subMenuBuilder.setHeaderTitle(getTitle());
    }

    @ViewDebug.CapturedViewProperty
    public CharSequence getTitle() {
        return this.f1517f;
    }

    /* access modifiers changed from: package-private */
    public CharSequence a(j.a aVar) {
        if (aVar == null || !aVar.prefersCondensedTitle()) {
            return getTitle();
        }
        return getTitleCondensed();
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f1517f = charSequence;
        this.f1512a.onItemsChanged(false);
        if (this.m != null) {
            this.m.setHeaderTitle(charSequence);
        }
        return this;
    }

    public MenuItem setTitle(int i2) {
        return setTitle(this.f1512a.getContext().getString(i2));
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f1518g != null ? this.f1518g : this.f1517f;
        if (Build.VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) {
            return charSequence;
        }
        return charSequence.toString();
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f1518g = charSequence;
        if (charSequence == null) {
            CharSequence charSequence2 = this.f1517f;
        }
        this.f1512a.onItemsChanged(false);
        return this;
    }

    public Drawable getIcon() {
        if (this.k != null) {
            return this.k;
        }
        if (this.l == 0) {
            return null;
        }
        Drawable b2 = android.support.v7.c.a.b.b(this.f1512a.getContext(), this.l);
        this.l = 0;
        this.k = b2;
        return b2;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.l = 0;
        this.k = drawable;
        this.f1512a.onItemsChanged(false);
        return this;
    }

    public MenuItem setIcon(int i2) {
        this.k = null;
        this.l = i2;
        this.f1512a.onItemsChanged(false);
        return this;
    }

    public boolean isCheckable() {
        return (this.p & 1) == 1;
    }

    public MenuItem setCheckable(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 1 : 0) | (this.p & -2);
        if (i2 != this.p) {
            this.f1512a.onItemsChanged(false);
        }
        return this;
    }

    public void a(boolean z2) {
        this.p = (z2 ? 4 : 0) | (this.p & -5);
    }

    public boolean g() {
        return (this.p & 4) != 0;
    }

    public boolean isChecked() {
        return (this.p & 2) == 2;
    }

    public MenuItem setChecked(boolean z2) {
        if ((this.p & 4) != 0) {
            this.f1512a.setExclusiveItemChecked(this);
        } else {
            b(z2);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        int i2;
        int i3 = this.p;
        int i4 = this.p & -3;
        if (z2) {
            i2 = 2;
        } else {
            i2 = 0;
        }
        this.p = i2 | i4;
        if (i3 != this.p) {
            this.f1512a.onItemsChanged(false);
        }
    }

    public boolean isVisible() {
        if (this.s == null || !this.s.b()) {
            if ((this.p & 8) != 0) {
                return false;
            }
            return true;
        } else if ((this.p & 8) != 0 || !this.s.c()) {
            return false;
        } else {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 0 : 8) | (this.p & -9);
        if (i2 != this.p) {
            return true;
        }
        return false;
    }

    public MenuItem setVisible(boolean z2) {
        if (c(z2)) {
            this.f1512a.onItemVisibleChanged(this);
        }
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.o = onMenuItemClickListener;
        return this;
    }

    public String toString() {
        if (this.f1517f != null) {
            return this.f1517f.toString();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.v = contextMenuInfo;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.v;
    }

    public void h() {
        this.f1512a.onItemActionRequestChanged(this);
    }

    public boolean i() {
        return this.f1512a.getOptionalIconsVisible();
    }

    public boolean j() {
        return (this.p & 32) == 32;
    }

    public boolean k() {
        return (this.q & 1) == 1;
    }

    public boolean l() {
        return (this.q & 2) == 2;
    }

    public void d(boolean z2) {
        if (z2) {
            this.p |= 32;
        } else {
            this.p &= -33;
        }
    }

    public boolean m() {
        return (this.q & 4) == 4;
    }

    public void setShowAsAction(int i2) {
        switch (i2 & 3) {
            case 0:
            case 1:
            case 2:
                break;
            default:
                throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
        this.q = i2;
        this.f1512a.onItemActionRequestChanged(this);
    }

    /* renamed from: a */
    public b setActionView(View view) {
        this.r = view;
        this.s = null;
        if (view != null && view.getId() == -1 && this.f1513b > 0) {
            view.setId(this.f1513b);
        }
        this.f1512a.onItemActionRequestChanged(this);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: a */
    public b setActionView(int i2) {
        Context context = this.f1512a.getContext();
        setActionView(LayoutInflater.from(context).inflate(i2, (ViewGroup) new LinearLayout(context), false));
        return this;
    }

    public View getActionView() {
        if (this.r != null) {
            return this.r;
        }
        if (this.s == null) {
            return null;
        }
        this.r = this.s.a(this);
        return this.r;
    }

    public MenuItem setActionProvider(android.view.ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    public android.view.ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    public ActionProvider a() {
        return this.s;
    }

    public b a(ActionProvider actionProvider) {
        if (this.s != null) {
            this.s.f();
        }
        this.r = null;
        this.s = actionProvider;
        this.f1512a.onItemsChanged(true);
        if (this.s != null) {
            this.s.a(new ActionProvider.b() {
                public void a(boolean z) {
                    MenuItemImpl.this.f1512a.onItemVisibleChanged(MenuItemImpl.this);
                }
            });
        }
        return this;
    }

    /* renamed from: b */
    public b setShowAsActionFlags(int i2) {
        setShowAsAction(i2);
        return this;
    }

    public boolean expandActionView() {
        if (!n()) {
            return false;
        }
        if (this.t == null || this.t.a(this)) {
            return this.f1512a.expandItemActionView(this);
        }
        return false;
    }

    public boolean collapseActionView() {
        if ((this.q & 8) == 0) {
            return false;
        }
        if (this.r == null) {
            return true;
        }
        if (this.t == null || this.t.b(this)) {
            return this.f1512a.collapseItemActionView(this);
        }
        return false;
    }

    public b a(p.e eVar) {
        this.t = eVar;
        return this;
    }

    public boolean n() {
        if ((this.q & 8) == 0) {
            return false;
        }
        if (this.r == null && this.s != null) {
            this.r = this.s.a(this);
        }
        if (this.r != null) {
            return true;
        }
        return false;
    }

    public void e(boolean z2) {
        this.u = z2;
        this.f1512a.onItemsChanged(false);
    }

    public boolean isActionViewExpanded() {
        return this.u;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setOnActionExpandListener()");
    }
}
