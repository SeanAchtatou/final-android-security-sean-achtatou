package android.support.v7.view.menu;

/* compiled from: BaseWrapper */
class b<T> {

    /* renamed from: b  reason: collision with root package name */
    final T f1530b;

    b(T t) {
        if (t == null) {
            throw new IllegalArgumentException("Wrapped Object can not be null.");
        }
        this.f1530b = t;
    }
}
