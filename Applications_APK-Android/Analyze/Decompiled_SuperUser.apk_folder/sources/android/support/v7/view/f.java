package android.support.v7.view;

import android.annotation.TargetApi;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SearchEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

/* compiled from: WindowCallbackWrapper */
public class f implements Window.Callback {

    /* renamed from: e  reason: collision with root package name */
    final Window.Callback f1465e;

    public f(Window.Callback callback) {
        if (callback == null) {
            throw new IllegalArgumentException("Window callback may not be null");
        }
        this.f1465e = callback;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.f1465e.dispatchKeyEvent(keyEvent);
    }

    @TargetApi(11)
    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        return this.f1465e.dispatchKeyShortcutEvent(keyEvent);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return this.f1465e.dispatchTouchEvent(motionEvent);
    }

    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        return this.f1465e.dispatchTrackballEvent(motionEvent);
    }

    @TargetApi(12)
    public boolean dispatchGenericMotionEvent(MotionEvent motionEvent) {
        return this.f1465e.dispatchGenericMotionEvent(motionEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return this.f1465e.dispatchPopulateAccessibilityEvent(accessibilityEvent);
    }

    public View onCreatePanelView(int i) {
        return this.f1465e.onCreatePanelView(i);
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        return this.f1465e.onCreatePanelMenu(i, menu);
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        return this.f1465e.onPreparePanel(i, view, menu);
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return this.f1465e.onMenuOpened(i, menu);
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        return this.f1465e.onMenuItemSelected(i, menuItem);
    }

    public void onWindowAttributesChanged(WindowManager.LayoutParams layoutParams) {
        this.f1465e.onWindowAttributesChanged(layoutParams);
    }

    public void onContentChanged() {
        this.f1465e.onContentChanged();
    }

    public void onWindowFocusChanged(boolean z) {
        this.f1465e.onWindowFocusChanged(z);
    }

    public void onAttachedToWindow() {
        this.f1465e.onAttachedToWindow();
    }

    public void onDetachedFromWindow() {
        this.f1465e.onDetachedFromWindow();
    }

    public void onPanelClosed(int i, Menu menu) {
        this.f1465e.onPanelClosed(i, menu);
    }

    @TargetApi(23)
    public boolean onSearchRequested(SearchEvent searchEvent) {
        return this.f1465e.onSearchRequested(searchEvent);
    }

    public boolean onSearchRequested() {
        return this.f1465e.onSearchRequested();
    }

    @TargetApi(11)
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
        return this.f1465e.onWindowStartingActionMode(callback);
    }

    @TargetApi(23)
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
        return this.f1465e.onWindowStartingActionMode(callback, i);
    }

    @TargetApi(11)
    public void onActionModeStarted(ActionMode actionMode) {
        this.f1465e.onActionModeStarted(actionMode);
    }

    @TargetApi(11)
    public void onActionModeFinished(ActionMode actionMode) {
        this.f1465e.onActionModeFinished(actionMode);
    }

    @TargetApi(24)
    public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
        this.f1465e.onProvideKeyboardShortcuts(list, menu, i);
    }
}
