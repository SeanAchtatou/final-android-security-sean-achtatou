package android.support.v7.view.menu;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.view.p;
import android.util.Log;
import android.view.ActionProvider;
import android.view.CollapsibleActionView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import java.lang.reflect.Method;

@TargetApi(14)
public class MenuItemWrapperICS extends a<android.support.v4.internal.view.b> implements MenuItem {

    /* renamed from: c  reason: collision with root package name */
    private Method f1521c;

    MenuItemWrapperICS(Context context, android.support.v4.internal.view.b bVar) {
        super(context, bVar);
    }

    public int getItemId() {
        return ((android.support.v4.internal.view.b) this.f1530b).getItemId();
    }

    public int getGroupId() {
        return ((android.support.v4.internal.view.b) this.f1530b).getGroupId();
    }

    public int getOrder() {
        return ((android.support.v4.internal.view.b) this.f1530b).getOrder();
    }

    public MenuItem setTitle(CharSequence charSequence) {
        ((android.support.v4.internal.view.b) this.f1530b).setTitle(charSequence);
        return this;
    }

    public MenuItem setTitle(int i) {
        ((android.support.v4.internal.view.b) this.f1530b).setTitle(i);
        return this;
    }

    public CharSequence getTitle() {
        return ((android.support.v4.internal.view.b) this.f1530b).getTitle();
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        ((android.support.v4.internal.view.b) this.f1530b).setTitleCondensed(charSequence);
        return this;
    }

    public CharSequence getTitleCondensed() {
        return ((android.support.v4.internal.view.b) this.f1530b).getTitleCondensed();
    }

    public MenuItem setIcon(Drawable drawable) {
        ((android.support.v4.internal.view.b) this.f1530b).setIcon(drawable);
        return this;
    }

    public MenuItem setIcon(int i) {
        ((android.support.v4.internal.view.b) this.f1530b).setIcon(i);
        return this;
    }

    public Drawable getIcon() {
        return ((android.support.v4.internal.view.b) this.f1530b).getIcon();
    }

    public MenuItem setIntent(Intent intent) {
        ((android.support.v4.internal.view.b) this.f1530b).setIntent(intent);
        return this;
    }

    public Intent getIntent() {
        return ((android.support.v4.internal.view.b) this.f1530b).getIntent();
    }

    public MenuItem setShortcut(char c2, char c3) {
        ((android.support.v4.internal.view.b) this.f1530b).setShortcut(c2, c3);
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        ((android.support.v4.internal.view.b) this.f1530b).setNumericShortcut(c2);
        return this;
    }

    public char getNumericShortcut() {
        return ((android.support.v4.internal.view.b) this.f1530b).getNumericShortcut();
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        ((android.support.v4.internal.view.b) this.f1530b).setAlphabeticShortcut(c2);
        return this;
    }

    public char getAlphabeticShortcut() {
        return ((android.support.v4.internal.view.b) this.f1530b).getAlphabeticShortcut();
    }

    public MenuItem setCheckable(boolean z) {
        ((android.support.v4.internal.view.b) this.f1530b).setCheckable(z);
        return this;
    }

    public boolean isCheckable() {
        return ((android.support.v4.internal.view.b) this.f1530b).isCheckable();
    }

    public MenuItem setChecked(boolean z) {
        ((android.support.v4.internal.view.b) this.f1530b).setChecked(z);
        return this;
    }

    public boolean isChecked() {
        return ((android.support.v4.internal.view.b) this.f1530b).isChecked();
    }

    public MenuItem setVisible(boolean z) {
        return ((android.support.v4.internal.view.b) this.f1530b).setVisible(z);
    }

    public boolean isVisible() {
        return ((android.support.v4.internal.view.b) this.f1530b).isVisible();
    }

    public MenuItem setEnabled(boolean z) {
        ((android.support.v4.internal.view.b) this.f1530b).setEnabled(z);
        return this;
    }

    public boolean isEnabled() {
        return ((android.support.v4.internal.view.b) this.f1530b).isEnabled();
    }

    public boolean hasSubMenu() {
        return ((android.support.v4.internal.view.b) this.f1530b).hasSubMenu();
    }

    public SubMenu getSubMenu() {
        return a(((android.support.v4.internal.view.b) this.f1530b).getSubMenu());
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        ((android.support.v4.internal.view.b) this.f1530b).setOnMenuItemClickListener(onMenuItemClickListener != null ? new d(onMenuItemClickListener) : null);
        return this;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return ((android.support.v4.internal.view.b) this.f1530b).getMenuInfo();
    }

    public void setShowAsAction(int i) {
        ((android.support.v4.internal.view.b) this.f1530b).setShowAsAction(i);
    }

    public MenuItem setShowAsActionFlags(int i) {
        ((android.support.v4.internal.view.b) this.f1530b).setShowAsActionFlags(i);
        return this;
    }

    public MenuItem setActionView(View view) {
        if (view instanceof CollapsibleActionView) {
            view = new b(view);
        }
        ((android.support.v4.internal.view.b) this.f1530b).setActionView(view);
        return this;
    }

    public MenuItem setActionView(int i) {
        ((android.support.v4.internal.view.b) this.f1530b).setActionView(i);
        View actionView = ((android.support.v4.internal.view.b) this.f1530b).getActionView();
        if (actionView instanceof CollapsibleActionView) {
            ((android.support.v4.internal.view.b) this.f1530b).setActionView(new b(actionView));
        }
        return this;
    }

    public View getActionView() {
        View actionView = ((android.support.v4.internal.view.b) this.f1530b).getActionView();
        if (actionView instanceof b) {
            return ((b) actionView).c();
        }
        return actionView;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        ((android.support.v4.internal.view.b) this.f1530b).a(actionProvider != null ? a(actionProvider) : null);
        return this;
    }

    public ActionProvider getActionProvider() {
        android.support.v4.view.ActionProvider a2 = ((android.support.v4.internal.view.b) this.f1530b).a();
        if (a2 instanceof a) {
            return ((a) a2).f1522a;
        }
        return null;
    }

    public boolean expandActionView() {
        return ((android.support.v4.internal.view.b) this.f1530b).expandActionView();
    }

    public boolean collapseActionView() {
        return ((android.support.v4.internal.view.b) this.f1530b).collapseActionView();
    }

    public boolean isActionViewExpanded() {
        return ((android.support.v4.internal.view.b) this.f1530b).isActionViewExpanded();
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        ((android.support.v4.internal.view.b) this.f1530b).a(onActionExpandListener != null ? new c(onActionExpandListener) : null);
        return this;
    }

    public void a(boolean z) {
        try {
            if (this.f1521c == null) {
                this.f1521c = ((android.support.v4.internal.view.b) this.f1530b).getClass().getDeclaredMethod("setExclusiveCheckable", Boolean.TYPE);
            }
            this.f1521c.invoke(this.f1530b, Boolean.valueOf(z));
        } catch (Exception e2) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public a a(ActionProvider actionProvider) {
        return new a(this.f1527a, actionProvider);
    }

    private class d extends b<MenuItem.OnMenuItemClickListener> implements MenuItem.OnMenuItemClickListener {
        d(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
            super(onMenuItemClickListener);
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            return ((MenuItem.OnMenuItemClickListener) this.f1530b).onMenuItemClick(MenuItemWrapperICS.this.a(menuItem));
        }
    }

    private class c extends b<MenuItem.OnActionExpandListener> implements p.e {
        c(MenuItem.OnActionExpandListener onActionExpandListener) {
            super(onActionExpandListener);
        }

        public boolean a(MenuItem menuItem) {
            return ((MenuItem.OnActionExpandListener) this.f1530b).onMenuItemActionExpand(MenuItemWrapperICS.this.a(menuItem));
        }

        public boolean b(MenuItem menuItem) {
            return ((MenuItem.OnActionExpandListener) this.f1530b).onMenuItemActionCollapse(MenuItemWrapperICS.this.a(menuItem));
        }
    }

    class a extends android.support.v4.view.ActionProvider {

        /* renamed from: a  reason: collision with root package name */
        final ActionProvider f1522a;

        public a(Context context, ActionProvider actionProvider) {
            super(context);
            this.f1522a = actionProvider;
        }

        public View a() {
            return this.f1522a.onCreateActionView();
        }

        public boolean d() {
            return this.f1522a.onPerformDefaultAction();
        }

        public boolean e() {
            return this.f1522a.hasSubMenu();
        }

        public void a(SubMenu subMenu) {
            this.f1522a.onPrepareSubMenu(MenuItemWrapperICS.this.a(subMenu));
        }
    }

    static class b extends FrameLayout implements android.support.v7.view.c {

        /* renamed from: a  reason: collision with root package name */
        final CollapsibleActionView f1524a;

        b(View view) {
            super(view.getContext());
            this.f1524a = (CollapsibleActionView) view;
            addView(view);
        }

        public void a() {
            this.f1524a.onActionViewExpanded();
        }

        public void b() {
            this.f1524a.onActionViewCollapsed();
        }

        /* access modifiers changed from: package-private */
        public View c() {
            return (View) this.f1524a;
        }
    }
}
