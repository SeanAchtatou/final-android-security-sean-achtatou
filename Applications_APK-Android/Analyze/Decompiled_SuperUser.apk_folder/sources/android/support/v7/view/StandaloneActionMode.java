package android.support.v7.view;

import android.content.Context;
import android.support.v7.view.b;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.ActionBarContextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

public class StandaloneActionMode extends b implements MenuBuilder.a {

    /* renamed from: a  reason: collision with root package name */
    private Context f1420a;

    /* renamed from: b  reason: collision with root package name */
    private ActionBarContextView f1421b;

    /* renamed from: c  reason: collision with root package name */
    private b.a f1422c;

    /* renamed from: d  reason: collision with root package name */
    private WeakReference<View> f1423d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f1424e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f1425f;

    /* renamed from: g  reason: collision with root package name */
    private MenuBuilder f1426g;

    public StandaloneActionMode(Context context, ActionBarContextView actionBarContextView, b.a aVar, boolean z) {
        this.f1420a = context;
        this.f1421b = actionBarContextView;
        this.f1422c = aVar;
        this.f1426g = new MenuBuilder(actionBarContextView.getContext()).setDefaultShowAsAction(1);
        this.f1426g.setCallback(this);
        this.f1425f = z;
    }

    public void b(CharSequence charSequence) {
        this.f1421b.setTitle(charSequence);
    }

    public void a(CharSequence charSequence) {
        this.f1421b.setSubtitle(charSequence);
    }

    public void a(int i) {
        b(this.f1420a.getString(i));
    }

    public void b(int i) {
        a((CharSequence) this.f1420a.getString(i));
    }

    public void a(boolean z) {
        super.a(z);
        this.f1421b.setTitleOptional(z);
    }

    public boolean h() {
        return this.f1421b.d();
    }

    public void a(View view) {
        this.f1421b.setCustomView(view);
        this.f1423d = view != null ? new WeakReference<>(view) : null;
    }

    public void d() {
        this.f1422c.b(this, this.f1426g);
    }

    public void c() {
        if (!this.f1424e) {
            this.f1424e = true;
            this.f1421b.sendAccessibilityEvent(32);
            this.f1422c.a(this);
        }
    }

    public Menu b() {
        return this.f1426g;
    }

    public CharSequence f() {
        return this.f1421b.getTitle();
    }

    public CharSequence g() {
        return this.f1421b.getSubtitle();
    }

    public View i() {
        if (this.f1423d != null) {
            return this.f1423d.get();
        }
        return null;
    }

    public MenuInflater a() {
        return new SupportMenuInflater(this.f1421b.getContext());
    }

    public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
        return this.f1422c.a(this, menuItem);
    }

    public void onMenuModeChange(MenuBuilder menuBuilder) {
        d();
        this.f1421b.a();
    }
}
