package android.support.v7.view.menu;

import android.content.Context;
import android.support.v4.internal.view.SupportSubMenu;
import android.support.v4.internal.view.b;
import android.support.v4.util.ArrayMap;
import android.view.MenuItem;
import android.view.SubMenu;
import java.util.Iterator;
import java.util.Map;

/* compiled from: BaseMenuWrapper */
abstract class a<T> extends b<T> {

    /* renamed from: a  reason: collision with root package name */
    final Context f1527a;

    /* renamed from: c  reason: collision with root package name */
    private Map<b, MenuItem> f1528c;

    /* renamed from: d  reason: collision with root package name */
    private Map<SupportSubMenu, SubMenu> f1529d;

    a(Context context, T t) {
        super(t);
        this.f1527a = context;
    }

    /* access modifiers changed from: package-private */
    public final MenuItem a(MenuItem menuItem) {
        if (!(menuItem instanceof b)) {
            return menuItem;
        }
        b bVar = (b) menuItem;
        if (this.f1528c == null) {
            this.f1528c = new ArrayMap();
        }
        MenuItem menuItem2 = this.f1528c.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        MenuItem a2 = k.a(this.f1527a, bVar);
        this.f1528c.put(bVar, a2);
        return a2;
    }

    /* access modifiers changed from: package-private */
    public final SubMenu a(SubMenu subMenu) {
        if (!(subMenu instanceof SupportSubMenu)) {
            return subMenu;
        }
        SupportSubMenu supportSubMenu = (SupportSubMenu) subMenu;
        if (this.f1529d == null) {
            this.f1529d = new ArrayMap();
        }
        SubMenu subMenu2 = this.f1529d.get(supportSubMenu);
        if (subMenu2 != null) {
            return subMenu2;
        }
        SubMenu a2 = k.a(this.f1527a, supportSubMenu);
        this.f1529d.put(supportSubMenu, a2);
        return a2;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.f1528c != null) {
            this.f1528c.clear();
        }
        if (this.f1529d != null) {
            this.f1529d.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        if (this.f1528c != null) {
            Iterator<b> it = this.f1528c.keySet().iterator();
            while (it.hasNext()) {
                if (i == it.next().getGroupId()) {
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i) {
        if (this.f1528c != null) {
            Iterator<b> it = this.f1528c.keySet().iterator();
            while (it.hasNext()) {
                if (i == it.next().getItemId()) {
                    it.remove();
                    return;
                }
            }
        }
    }
}
