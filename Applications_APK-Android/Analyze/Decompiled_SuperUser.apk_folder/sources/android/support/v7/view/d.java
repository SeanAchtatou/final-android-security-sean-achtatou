package android.support.v7.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.support.v7.a.a;
import android.view.LayoutInflater;

/* compiled from: ContextThemeWrapper */
public class d extends ContextWrapper {

    /* renamed from: a  reason: collision with root package name */
    private int f1453a;

    /* renamed from: b  reason: collision with root package name */
    private Resources.Theme f1454b;

    /* renamed from: c  reason: collision with root package name */
    private LayoutInflater f1455c;

    public d(Context context, int i) {
        super(context);
        this.f1453a = i;
    }

    public d(Context context, Resources.Theme theme) {
        super(context);
        this.f1454b = theme;
    }

    public void setTheme(int i) {
        if (this.f1453a != i) {
            this.f1453a = i;
            b();
        }
    }

    public int a() {
        return this.f1453a;
    }

    public Resources.Theme getTheme() {
        if (this.f1454b != null) {
            return this.f1454b;
        }
        if (this.f1453a == 0) {
            this.f1453a = a.j.Theme_AppCompat_Light;
        }
        b();
        return this.f1454b;
    }

    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.f1455c == null) {
            this.f1455c = LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.f1455c;
    }

    /* access modifiers changed from: protected */
    public void a(Resources.Theme theme, int i, boolean z) {
        theme.applyStyle(i, true);
    }

    private void b() {
        boolean z = this.f1454b == null;
        if (z) {
            this.f1454b = getResources().newTheme();
            Resources.Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.f1454b.setTo(theme);
            }
        }
        a(this.f1454b, this.f1453a, z);
    }

    public AssetManager getAssets() {
        return getResources().getAssets();
    }
}
