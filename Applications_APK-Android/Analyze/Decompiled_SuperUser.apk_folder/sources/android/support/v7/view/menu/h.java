package android.support.v7.view.menu;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.view.ag;
import android.support.v4.view.e;
import android.support.v7.a.a;
import android.support.v7.view.menu.i;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

/* compiled from: MenuPopupHelper */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private final Context f1561a;

    /* renamed from: b  reason: collision with root package name */
    private final MenuBuilder f1562b;

    /* renamed from: c  reason: collision with root package name */
    private final boolean f1563c;

    /* renamed from: d  reason: collision with root package name */
    private final int f1564d;

    /* renamed from: e  reason: collision with root package name */
    private final int f1565e;

    /* renamed from: f  reason: collision with root package name */
    private View f1566f;

    /* renamed from: g  reason: collision with root package name */
    private int f1567g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f1568h;
    private i.a i;
    private g j;
    private PopupWindow.OnDismissListener k;
    private final PopupWindow.OnDismissListener l;

    public h(Context context, MenuBuilder menuBuilder, View view, boolean z, int i2) {
        this(context, menuBuilder, view, z, i2, 0);
    }

    public h(Context context, MenuBuilder menuBuilder, View view, boolean z, int i2, int i3) {
        this.f1567g = 8388611;
        this.l = new PopupWindow.OnDismissListener() {
            public void onDismiss() {
                h.this.e();
            }
        };
        this.f1561a = context;
        this.f1562b = menuBuilder;
        this.f1566f = view;
        this.f1563c = z;
        this.f1564d = i2;
        this.f1565e = i3;
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.k = onDismissListener;
    }

    public void a(View view) {
        this.f1566f = view;
    }

    public void a(boolean z) {
        this.f1568h = z;
        if (this.j != null) {
            this.j.a(z);
        }
    }

    public void a(int i2) {
        this.f1567g = i2;
    }

    public void a() {
        if (!c()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    public g b() {
        if (this.j == null) {
            this.j = g();
        }
        return this.j;
    }

    public boolean c() {
        if (f()) {
            return true;
        }
        if (this.f1566f == null) {
            return false;
        }
        a(0, 0, false, false);
        return true;
    }

    public boolean a(int i2, int i3) {
        if (f()) {
            return true;
        }
        if (this.f1566f == null) {
            return false;
        }
        a(i2, i3, true, true);
        return true;
    }

    private g g() {
        g nVar;
        Display defaultDisplay = ((WindowManager) this.f1561a.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            defaultDisplay.getRealSize(point);
        } else if (Build.VERSION.SDK_INT >= 13) {
            defaultDisplay.getSize(point);
        } else {
            point.set(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        }
        if (Math.min(point.x, point.y) >= this.f1561a.getResources().getDimensionPixelSize(a.d.abc_cascading_menus_min_smallest_width)) {
            nVar = new c(this.f1561a, this.f1566f, this.f1564d, this.f1565e, this.f1563c);
        } else {
            nVar = new n(this.f1561a, this.f1562b, this.f1566f, this.f1564d, this.f1565e, this.f1563c);
        }
        nVar.a(this.f1562b);
        nVar.a(this.l);
        nVar.a(this.f1566f);
        nVar.setCallback(this.i);
        nVar.a(this.f1568h);
        nVar.a(this.f1567g);
        return nVar;
    }

    private void a(int i2, int i3, boolean z, boolean z2) {
        g b2 = b();
        b2.b(z2);
        if (z) {
            if ((e.a(this.f1567g, ag.g(this.f1566f)) & 7) == 5) {
                i2 -= this.f1566f.getWidth();
            }
            b2.b(i2);
            b2.c(i3);
            int i4 = (int) ((this.f1561a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            b2.a(new Rect(i2 - i4, i3 - i4, i2 + i4, i4 + i3));
        }
        b2.a();
    }

    public void d() {
        if (f()) {
            this.j.b();
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.j = null;
        if (this.k != null) {
            this.k.onDismiss();
        }
    }

    public boolean f() {
        return this.j != null && this.j.c();
    }

    public void a(i.a aVar) {
        this.i = aVar;
        if (this.j != null) {
            this.j.setCallback(aVar);
        }
    }
}
