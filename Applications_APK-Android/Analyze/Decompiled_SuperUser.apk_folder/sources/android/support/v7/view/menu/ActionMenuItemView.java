package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v4.view.ag;
import android.support.v7.a.a;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.j;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.x;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class ActionMenuItemView extends AppCompatTextView implements j.a, ActionMenuView.a, View.OnClickListener, View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    MenuItemImpl f1474a;

    /* renamed from: b  reason: collision with root package name */
    MenuBuilder.b f1475b;

    /* renamed from: c  reason: collision with root package name */
    b f1476c;

    /* renamed from: d  reason: collision with root package name */
    private CharSequence f1477d;

    /* renamed from: e  reason: collision with root package name */
    private Drawable f1478e;

    /* renamed from: f  reason: collision with root package name */
    private x f1479f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f1480g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f1481h;
    private int i;
    private int j;
    private int k;

    public static abstract class b {
        public abstract m a();
    }

    public ActionMenuItemView(Context context) {
        this(context, null);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Resources resources = context.getResources();
        this.f1480g = d();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.k.ActionMenuItemView, i2, 0);
        this.i = obtainStyledAttributes.getDimensionPixelSize(a.k.ActionMenuItemView_android_minWidth, 0);
        obtainStyledAttributes.recycle();
        this.k = (int) ((resources.getDisplayMetrics().density * 32.0f) + 0.5f);
        setOnClickListener(this);
        setOnLongClickListener(this);
        this.j = -1;
        setSaveEnabled(false);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.f1480g = d();
        e();
    }

    private boolean d() {
        Configuration configuration = getContext().getResources().getConfiguration();
        int b2 = android.support.v4.content.a.a.b(getResources());
        return b2 >= 480 || (b2 >= 640 && android.support.v4.content.a.a.a(getResources()) >= 480) || configuration.orientation == 2;
    }

    public void setPadding(int i2, int i3, int i4, int i5) {
        this.j = i2;
        super.setPadding(i2, i3, i4, i5);
    }

    public MenuItemImpl getItemData() {
        return this.f1474a;
    }

    public void initialize(MenuItemImpl menuItemImpl, int i2) {
        this.f1474a = menuItemImpl;
        setIcon(menuItemImpl.getIcon());
        setTitle(menuItemImpl.a((j.a) this));
        setId(menuItemImpl.getItemId());
        setVisibility(menuItemImpl.isVisible() ? 0 : 8);
        setEnabled(menuItemImpl.isEnabled());
        if (menuItemImpl.hasSubMenu() && this.f1479f == null) {
            this.f1479f = new a();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.f1474a.hasSubMenu() || this.f1479f == null || !this.f1479f.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public void onClick(View view) {
        if (this.f1475b != null) {
            this.f1475b.a(this.f1474a);
        }
    }

    public void setItemInvoker(MenuBuilder.b bVar) {
        this.f1475b = bVar;
    }

    public void setPopupCallback(b bVar) {
        this.f1476c = bVar;
    }

    public boolean prefersCondensedTitle() {
        return true;
    }

    public void setCheckable(boolean z) {
    }

    public void setChecked(boolean z) {
    }

    public void setExpandedFormat(boolean z) {
        if (this.f1481h != z) {
            this.f1481h = z;
            if (this.f1474a != null) {
                this.f1474a.h();
            }
        }
    }

    private void e() {
        boolean z = false;
        boolean z2 = !TextUtils.isEmpty(this.f1477d);
        if (this.f1478e == null || (this.f1474a.m() && (this.f1480g || this.f1481h))) {
            z = true;
        }
        setText(z2 & z ? this.f1477d : null);
    }

    public void setIcon(Drawable drawable) {
        this.f1478e = drawable;
        if (drawable != null) {
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (intrinsicWidth > this.k) {
                float f2 = ((float) this.k) / ((float) intrinsicWidth);
                intrinsicWidth = this.k;
                intrinsicHeight = (int) (((float) intrinsicHeight) * f2);
            }
            if (intrinsicHeight > this.k) {
                float f3 = ((float) this.k) / ((float) intrinsicHeight);
                intrinsicHeight = this.k;
                intrinsicWidth = (int) (((float) intrinsicWidth) * f3);
            }
            drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        }
        setCompoundDrawables(drawable, null, null, null);
        e();
    }

    public boolean a() {
        return !TextUtils.isEmpty(getText());
    }

    public void setTitle(CharSequence charSequence) {
        this.f1477d = charSequence;
        setContentDescription(this.f1477d);
        e();
    }

    public boolean b() {
        return a() && this.f1474a.getIcon() == null;
    }

    public boolean c() {
        return a();
    }

    public boolean onLongClick(View view) {
        if (a()) {
            return false;
        }
        int[] iArr = new int[2];
        Rect rect = new Rect();
        getLocationOnScreen(iArr);
        getWindowVisibleDisplayFrame(rect);
        Context context = getContext();
        int width = getWidth();
        int height = getHeight();
        int i2 = iArr[1] + (height / 2);
        int i3 = (width / 2) + iArr[0];
        if (ag.g(view) == 0) {
            i3 = context.getResources().getDisplayMetrics().widthPixels - i3;
        }
        Toast makeText = Toast.makeText(context, this.f1474a.getTitle(), 0);
        if (i2 < rect.height()) {
            makeText.setGravity(8388661, i3, (iArr[1] + height) - rect.top);
        } else {
            makeText.setGravity(81, 0, height);
        }
        makeText.show();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        boolean a2 = a();
        if (a2 && this.j >= 0) {
            super.setPadding(this.j, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
        super.onMeasure(i2, i3);
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int measuredWidth = getMeasuredWidth();
        int min = mode == Integer.MIN_VALUE ? Math.min(size, this.i) : this.i;
        if (mode != 1073741824 && this.i > 0 && measuredWidth < min) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(min, 1073741824), i3);
        }
        if (!a2 && this.f1478e != null) {
            super.setPadding((getMeasuredWidth() - this.f1478e.getBounds().width()) / 2, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
    }

    private class a extends x {
        public a() {
            super(ActionMenuItemView.this);
        }

        public m a() {
            if (ActionMenuItemView.this.f1476c != null) {
                return ActionMenuItemView.this.f1476c.a();
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean b() {
            m a2;
            if (ActionMenuItemView.this.f1475b == null || !ActionMenuItemView.this.f1475b.a(ActionMenuItemView.this.f1474a) || (a2 = a()) == null || !a2.c()) {
                return false;
            }
            return true;
        }
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        super.onRestoreInstanceState(null);
    }
}
