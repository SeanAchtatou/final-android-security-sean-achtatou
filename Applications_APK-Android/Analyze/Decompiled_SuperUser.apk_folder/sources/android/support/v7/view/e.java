package android.support.v7.view;

import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.ay;
import android.support.v4.view.bc;
import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ViewPropertyAnimatorCompatSet */
public class e {

    /* renamed from: a  reason: collision with root package name */
    final ArrayList<ay> f1456a = new ArrayList<>();

    /* renamed from: b  reason: collision with root package name */
    bc f1457b;

    /* renamed from: c  reason: collision with root package name */
    private long f1458c = -1;

    /* renamed from: d  reason: collision with root package name */
    private Interpolator f1459d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f1460e;

    /* renamed from: f  reason: collision with root package name */
    private final ViewPropertyAnimatorListenerAdapter f1461f = new ViewPropertyAnimatorListenerAdapter() {

        /* renamed from: b  reason: collision with root package name */
        private boolean f1463b = false;

        /* renamed from: c  reason: collision with root package name */
        private int f1464c = 0;

        public void onAnimationStart(View view) {
            if (!this.f1463b) {
                this.f1463b = true;
                if (e.this.f1457b != null) {
                    e.this.f1457b.onAnimationStart(null);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f1464c = 0;
            this.f1463b = false;
            e.this.b();
        }

        public void onAnimationEnd(View view) {
            int i = this.f1464c + 1;
            this.f1464c = i;
            if (i == e.this.f1456a.size()) {
                if (e.this.f1457b != null) {
                    e.this.f1457b.onAnimationEnd(null);
                }
                a();
            }
        }
    };

    public e a(ay ayVar) {
        if (!this.f1460e) {
            this.f1456a.add(ayVar);
        }
        return this;
    }

    public e a(ay ayVar, ay ayVar2) {
        this.f1456a.add(ayVar);
        ayVar2.b(ayVar.a());
        this.f1456a.add(ayVar2);
        return this;
    }

    public void a() {
        if (!this.f1460e) {
            Iterator<ay> it = this.f1456a.iterator();
            while (it.hasNext()) {
                ay next = it.next();
                if (this.f1458c >= 0) {
                    next.a(this.f1458c);
                }
                if (this.f1459d != null) {
                    next.a(this.f1459d);
                }
                if (this.f1457b != null) {
                    next.a(this.f1461f);
                }
                next.c();
            }
            this.f1460e = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f1460e = false;
    }

    public void c() {
        if (this.f1460e) {
            Iterator<ay> it = this.f1456a.iterator();
            while (it.hasNext()) {
                it.next().b();
            }
            this.f1460e = false;
        }
    }

    public e a(long j) {
        if (!this.f1460e) {
            this.f1458c = j;
        }
        return this;
    }

    public e a(Interpolator interpolator) {
        if (!this.f1460e) {
            this.f1459d = interpolator;
        }
        return this;
    }

    public e a(bc bcVar) {
        if (!this.f1460e) {
            this.f1457b = bcVar;
        }
        return this;
    }
}
