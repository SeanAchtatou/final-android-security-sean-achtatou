package android.support.v7.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.p;
import android.support.v7.a.a;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuItemWrapperICS;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class SupportMenuInflater extends MenuInflater {

    /* renamed from: a  reason: collision with root package name */
    static final Class<?>[] f1433a = {Context.class};

    /* renamed from: b  reason: collision with root package name */
    static final Class<?>[] f1434b = f1433a;

    /* renamed from: c  reason: collision with root package name */
    final Object[] f1435c;

    /* renamed from: d  reason: collision with root package name */
    final Object[] f1436d = this.f1435c;

    /* renamed from: e  reason: collision with root package name */
    Context f1437e;

    /* renamed from: f  reason: collision with root package name */
    private Object f1438f;

    public SupportMenuInflater(Context context) {
        super(context);
        this.f1437e = context;
        this.f1435c = new Object[]{context};
    }

    public void inflate(int i, Menu menu) {
        if (!(menu instanceof android.support.v4.internal.view.a)) {
            super.inflate(i, menu);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.f1437e.getResources().getLayout(i);
            a(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (IOException e3) {
            throw new InflateException("Error inflating menu XML", e3);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    private void a(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) {
        boolean z;
        b bVar = new b(menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException("Expecting menu, got " + name);
                }
            }
        }
        String str = null;
        boolean z2 = false;
        int i = eventType;
        boolean z3 = false;
        while (!z3) {
            switch (i) {
                case 1:
                    throw new RuntimeException("Unexpected end of document");
                case 2:
                    if (z2) {
                        z = z2;
                        continue;
                    } else {
                        String name2 = xmlPullParser.getName();
                        if (name2.equals("group")) {
                            bVar.a(attributeSet);
                            z = z2;
                        } else if (name2.equals("item")) {
                            bVar.b(attributeSet);
                            z = z2;
                        } else if (name2.equals("menu")) {
                            a(xmlPullParser, attributeSet, bVar.c());
                            z = z2;
                        } else {
                            str = name2;
                            z = true;
                        }
                    }
                    boolean z4 = z;
                    i = xmlPullParser.next();
                    z2 = z4;
                case 3:
                    String name3 = xmlPullParser.getName();
                    if (!z2 || !name3.equals(str)) {
                        if (name3.equals("group")) {
                            bVar.a();
                            z = z2;
                        } else if (name3.equals("item")) {
                            if (!bVar.d()) {
                                if (bVar.f1442a == null || !bVar.f1442a.e()) {
                                    bVar.b();
                                    z = z2;
                                } else {
                                    bVar.c();
                                    z = z2;
                                }
                            }
                        } else if (name3.equals("menu")) {
                            z3 = true;
                            z = z2;
                        }
                        boolean z42 = z;
                        i = xmlPullParser.next();
                        z2 = z42;
                    } else {
                        str = null;
                        z = false;
                        continue;
                        boolean z422 = z;
                        i = xmlPullParser.next();
                        z2 = z422;
                    }
                    break;
            }
            z = z2;
            boolean z4222 = z;
            i = xmlPullParser.next();
            z2 = z4222;
        }
    }

    /* access modifiers changed from: package-private */
    public Object a() {
        if (this.f1438f == null) {
            this.f1438f = a(this.f1437e);
        }
        return this.f1438f;
    }

    private Object a(Object obj) {
        if (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) {
            return a(((ContextWrapper) obj).getBaseContext());
        }
        return obj;
    }

    private static class a implements MenuItem.OnMenuItemClickListener {

        /* renamed from: a  reason: collision with root package name */
        private static final Class<?>[] f1439a = {MenuItem.class};

        /* renamed from: b  reason: collision with root package name */
        private Object f1440b;

        /* renamed from: c  reason: collision with root package name */
        private Method f1441c;

        public a(Object obj, String str) {
            this.f1440b = obj;
            Class<?> cls = obj.getClass();
            try {
                this.f1441c = cls.getMethod(str, f1439a);
            } catch (Exception e2) {
                InflateException inflateException = new InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e2);
                throw inflateException;
            }
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                if (this.f1441c.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.f1441c.invoke(this.f1440b, menuItem)).booleanValue();
                }
                this.f1441c.invoke(this.f1440b, menuItem);
                return true;
            } catch (Exception e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    private class b {

        /* renamed from: a  reason: collision with root package name */
        ActionProvider f1442a;

        /* renamed from: c  reason: collision with root package name */
        private Menu f1444c;

        /* renamed from: d  reason: collision with root package name */
        private int f1445d;

        /* renamed from: e  reason: collision with root package name */
        private int f1446e;

        /* renamed from: f  reason: collision with root package name */
        private int f1447f;

        /* renamed from: g  reason: collision with root package name */
        private int f1448g;

        /* renamed from: h  reason: collision with root package name */
        private boolean f1449h;
        private boolean i;
        private boolean j;
        private int k;
        private int l;
        private CharSequence m;
        private CharSequence n;
        private int o;
        private char p;
        private char q;
        private int r;
        private boolean s;
        private boolean t;
        private boolean u;
        private int v;
        private int w;
        private String x;
        private String y;
        private String z;

        public b(Menu menu) {
            this.f1444c = menu;
            a();
        }

        public void a() {
            this.f1445d = 0;
            this.f1446e = 0;
            this.f1447f = 0;
            this.f1448g = 0;
            this.f1449h = true;
            this.i = true;
        }

        public void a(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = SupportMenuInflater.this.f1437e.obtainStyledAttributes(attributeSet, a.k.MenuGroup);
            this.f1445d = obtainStyledAttributes.getResourceId(a.k.MenuGroup_android_id, 0);
            this.f1446e = obtainStyledAttributes.getInt(a.k.MenuGroup_android_menuCategory, 0);
            this.f1447f = obtainStyledAttributes.getInt(a.k.MenuGroup_android_orderInCategory, 0);
            this.f1448g = obtainStyledAttributes.getInt(a.k.MenuGroup_android_checkableBehavior, 0);
            this.f1449h = obtainStyledAttributes.getBoolean(a.k.MenuGroup_android_visible, true);
            this.i = obtainStyledAttributes.getBoolean(a.k.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }

        public void b(AttributeSet attributeSet) {
            boolean z2 = true;
            TypedArray obtainStyledAttributes = SupportMenuInflater.this.f1437e.obtainStyledAttributes(attributeSet, a.k.MenuItem);
            this.k = obtainStyledAttributes.getResourceId(a.k.MenuItem_android_id, 0);
            this.l = (obtainStyledAttributes.getInt(a.k.MenuItem_android_menuCategory, this.f1446e) & -65536) | (obtainStyledAttributes.getInt(a.k.MenuItem_android_orderInCategory, this.f1447f) & 65535);
            this.m = obtainStyledAttributes.getText(a.k.MenuItem_android_title);
            this.n = obtainStyledAttributes.getText(a.k.MenuItem_android_titleCondensed);
            this.o = obtainStyledAttributes.getResourceId(a.k.MenuItem_android_icon, 0);
            this.p = a(obtainStyledAttributes.getString(a.k.MenuItem_android_alphabeticShortcut));
            this.q = a(obtainStyledAttributes.getString(a.k.MenuItem_android_numericShortcut));
            if (obtainStyledAttributes.hasValue(a.k.MenuItem_android_checkable)) {
                this.r = obtainStyledAttributes.getBoolean(a.k.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.r = this.f1448g;
            }
            this.s = obtainStyledAttributes.getBoolean(a.k.MenuItem_android_checked, false);
            this.t = obtainStyledAttributes.getBoolean(a.k.MenuItem_android_visible, this.f1449h);
            this.u = obtainStyledAttributes.getBoolean(a.k.MenuItem_android_enabled, this.i);
            this.v = obtainStyledAttributes.getInt(a.k.MenuItem_showAsAction, -1);
            this.z = obtainStyledAttributes.getString(a.k.MenuItem_android_onClick);
            this.w = obtainStyledAttributes.getResourceId(a.k.MenuItem_actionLayout, 0);
            this.x = obtainStyledAttributes.getString(a.k.MenuItem_actionViewClass);
            this.y = obtainStyledAttributes.getString(a.k.MenuItem_actionProviderClass);
            if (this.y == null) {
                z2 = false;
            }
            if (z2 && this.w == 0 && this.x == null) {
                this.f1442a = (ActionProvider) a(this.y, SupportMenuInflater.f1434b, SupportMenuInflater.this.f1436d);
            } else {
                if (z2) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.f1442a = null;
            }
            obtainStyledAttributes.recycle();
            this.j = false;
        }

        private char a(String str) {
            if (str == null) {
                return 0;
            }
            return str.charAt(0);
        }

        private void a(MenuItem menuItem) {
            boolean z2 = true;
            menuItem.setChecked(this.s).setVisible(this.t).setEnabled(this.u).setCheckable(this.r >= 1).setTitleCondensed(this.n).setIcon(this.o).setAlphabeticShortcut(this.p).setNumericShortcut(this.q);
            if (this.v >= 0) {
                p.a(menuItem, this.v);
            }
            if (this.z != null) {
                if (SupportMenuInflater.this.f1437e.isRestricted()) {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
                menuItem.setOnMenuItemClickListener(new a(SupportMenuInflater.this.a(), this.z));
            }
            if (menuItem instanceof MenuItemImpl) {
                MenuItemImpl menuItemImpl = (MenuItemImpl) menuItem;
            }
            if (this.r >= 2) {
                if (menuItem instanceof MenuItemImpl) {
                    ((MenuItemImpl) menuItem).a(true);
                } else if (menuItem instanceof MenuItemWrapperICS) {
                    ((MenuItemWrapperICS) menuItem).a(true);
                }
            }
            if (this.x != null) {
                p.a(menuItem, (View) a(this.x, SupportMenuInflater.f1433a, SupportMenuInflater.this.f1435c));
            } else {
                z2 = false;
            }
            if (this.w > 0) {
                if (!z2) {
                    p.b(menuItem, this.w);
                } else {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            if (this.f1442a != null) {
                p.a(menuItem, this.f1442a);
            }
        }

        public void b() {
            this.j = true;
            a(this.f1444c.add(this.f1445d, this.k, this.l, this.m));
        }

        public SubMenu c() {
            this.j = true;
            SubMenu addSubMenu = this.f1444c.addSubMenu(this.f1445d, this.k, this.l, this.m);
            a(addSubMenu.getItem());
            return addSubMenu;
        }

        public boolean d() {
            return this.j;
        }

        private <T> T a(String str, Class<?>[] clsArr, Object[] objArr) {
            try {
                Constructor<?> constructor = SupportMenuInflater.this.f1437e.getClassLoader().loadClass(str).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr);
            } catch (Exception e2) {
                Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e2);
                return null;
            }
        }
    }
}
