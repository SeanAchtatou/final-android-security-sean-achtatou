package android.support.v7.view.menu;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.c;
import android.support.v4.internal.view.b;
import android.support.v4.view.p;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class ActionMenuItem implements b {

    /* renamed from: a  reason: collision with root package name */
    private final int f1466a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1467b;

    /* renamed from: c  reason: collision with root package name */
    private final int f1468c;

    /* renamed from: d  reason: collision with root package name */
    private final int f1469d;

    /* renamed from: e  reason: collision with root package name */
    private CharSequence f1470e;

    /* renamed from: f  reason: collision with root package name */
    private CharSequence f1471f;

    /* renamed from: g  reason: collision with root package name */
    private Intent f1472g;

    /* renamed from: h  reason: collision with root package name */
    private char f1473h;
    private char i;
    private Drawable j;
    private int k = 0;
    private Context l;
    private MenuItem.OnMenuItemClickListener m;
    private int n = 16;

    public ActionMenuItem(Context context, int i2, int i3, int i4, int i5, CharSequence charSequence) {
        this.l = context;
        this.f1466a = i3;
        this.f1467b = i2;
        this.f1468c = i4;
        this.f1469d = i5;
        this.f1470e = charSequence;
    }

    public char getAlphabeticShortcut() {
        return this.i;
    }

    public int getGroupId() {
        return this.f1467b;
    }

    public Drawable getIcon() {
        return this.j;
    }

    public Intent getIntent() {
        return this.f1472g;
    }

    public int getItemId() {
        return this.f1466a;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    public char getNumericShortcut() {
        return this.f1473h;
    }

    public int getOrder() {
        return this.f1469d;
    }

    public SubMenu getSubMenu() {
        return null;
    }

    public CharSequence getTitle() {
        return this.f1470e;
    }

    public CharSequence getTitleCondensed() {
        return this.f1471f != null ? this.f1471f : this.f1470e;
    }

    public boolean hasSubMenu() {
        return false;
    }

    public boolean isCheckable() {
        return (this.n & 1) != 0;
    }

    public boolean isChecked() {
        return (this.n & 2) != 0;
    }

    public boolean isEnabled() {
        return (this.n & 16) != 0;
    }

    public boolean isVisible() {
        return (this.n & 8) == 0;
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        this.i = c2;
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        this.n = (z ? 1 : 0) | (this.n & -2);
        return this;
    }

    public MenuItem setChecked(boolean z) {
        this.n = (z ? 2 : 0) | (this.n & -3);
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        this.n = (z ? 16 : 0) | (this.n & -17);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.j = drawable;
        this.k = 0;
        return this;
    }

    public MenuItem setIcon(int i2) {
        this.k = i2;
        this.j = c.a(this.l, i2);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.f1472g = intent;
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        this.f1473h = c2;
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.m = onMenuItemClickListener;
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        this.f1473h = c2;
        this.i = c3;
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f1470e = charSequence;
        return this;
    }

    public MenuItem setTitle(int i2) {
        this.f1470e = this.l.getResources().getString(i2);
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f1471f = charSequence;
        return this;
    }

    public MenuItem setVisible(boolean z) {
        this.n = (z ? 0 : 8) | (this.n & 8);
        return this;
    }

    public void setShowAsAction(int i2) {
    }

    /* renamed from: a */
    public b setActionView(View view) {
        throw new UnsupportedOperationException();
    }

    public View getActionView() {
        return null;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public b setActionView(int i2) {
        throw new UnsupportedOperationException();
    }

    public android.support.v4.view.ActionProvider a() {
        return null;
    }

    public b a(android.support.v4.view.ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: b */
    public b setShowAsActionFlags(int i2) {
        setShowAsAction(i2);
        return this;
    }

    public boolean expandActionView() {
        return false;
    }

    public boolean collapseActionView() {
        return false;
    }

    public boolean isActionViewExpanded() {
        return false;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException();
    }

    public b a(p.e eVar) {
        return this;
    }
}
