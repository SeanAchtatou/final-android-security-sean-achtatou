package android.support.v7.view.menu;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.v7.a.a;
import android.support.v7.view.menu.i;
import android.support.v7.view.menu.j;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import java.util.ArrayList;

public class ListMenuPresenter implements i, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    Context f1502a;

    /* renamed from: b  reason: collision with root package name */
    LayoutInflater f1503b;

    /* renamed from: c  reason: collision with root package name */
    MenuBuilder f1504c;

    /* renamed from: d  reason: collision with root package name */
    ExpandedMenuView f1505d;

    /* renamed from: e  reason: collision with root package name */
    int f1506e;

    /* renamed from: f  reason: collision with root package name */
    int f1507f;

    /* renamed from: g  reason: collision with root package name */
    int f1508g;

    /* renamed from: h  reason: collision with root package name */
    a f1509h;
    private i.a i;
    private int j;

    public ListMenuPresenter(Context context, int i2) {
        this(i2, 0);
        this.f1502a = context;
        this.f1503b = LayoutInflater.from(this.f1502a);
    }

    public ListMenuPresenter(int i2, int i3) {
        this.f1508g = i2;
        this.f1507f = i3;
    }

    public void initForMenu(Context context, MenuBuilder menuBuilder) {
        if (this.f1507f != 0) {
            this.f1502a = new ContextThemeWrapper(context, this.f1507f);
            this.f1503b = LayoutInflater.from(this.f1502a);
        } else if (this.f1502a != null) {
            this.f1502a = context;
            if (this.f1503b == null) {
                this.f1503b = LayoutInflater.from(this.f1502a);
            }
        }
        this.f1504c = menuBuilder;
        if (this.f1509h != null) {
            this.f1509h.notifyDataSetChanged();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public j a(ViewGroup viewGroup) {
        if (this.f1505d == null) {
            this.f1505d = (ExpandedMenuView) this.f1503b.inflate(a.h.abc_expanded_menu_layout, viewGroup, false);
            if (this.f1509h == null) {
                this.f1509h = new a();
            }
            this.f1505d.setAdapter((ListAdapter) this.f1509h);
            this.f1505d.setOnItemClickListener(this);
        }
        return this.f1505d;
    }

    public ListAdapter a() {
        if (this.f1509h == null) {
            this.f1509h = new a();
        }
        return this.f1509h;
    }

    public void updateMenuView(boolean z) {
        if (this.f1509h != null) {
            this.f1509h.notifyDataSetChanged();
        }
    }

    public void setCallback(i.a aVar) {
        this.i = aVar;
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        new e(subMenuBuilder).a((IBinder) null);
        if (this.i != null) {
            this.i.a(subMenuBuilder);
        }
        return true;
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        if (this.i != null) {
            this.i.a(menuBuilder, z);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.f1504c.performItemAction(this.f1509h.getItem(i2), this, 0);
    }

    public boolean flagActionItems() {
        return false;
    }

    public boolean expandItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean collapseItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public void a(Bundle bundle) {
        SparseArray sparseArray = new SparseArray();
        if (this.f1505d != null) {
            this.f1505d.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    public void b(Bundle bundle) {
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.f1505d.restoreHierarchyState(sparseParcelableArray);
        }
    }

    public int getId() {
        return this.j;
    }

    public Parcelable onSaveInstanceState() {
        if (this.f1505d == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        a(bundle);
        return bundle;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        b((Bundle) parcelable);
    }

    private class a extends BaseAdapter {

        /* renamed from: b  reason: collision with root package name */
        private int f1511b = -1;

        public a() {
            a();
        }

        public int getCount() {
            int size = ListMenuPresenter.this.f1504c.getNonActionItems().size() - ListMenuPresenter.this.f1506e;
            return this.f1511b < 0 ? size : size - 1;
        }

        /* renamed from: a */
        public MenuItemImpl getItem(int i) {
            ArrayList<MenuItemImpl> nonActionItems = ListMenuPresenter.this.f1504c.getNonActionItems();
            int i2 = ListMenuPresenter.this.f1506e + i;
            if (this.f1511b >= 0 && i2 >= this.f1511b) {
                i2++;
            }
            return nonActionItems.get(i2);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                view2 = ListMenuPresenter.this.f1503b.inflate(ListMenuPresenter.this.f1508g, viewGroup, false);
            } else {
                view2 = view;
            }
            ((j.a) view2).initialize(getItem(i), 0);
            return view2;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            MenuItemImpl expandedItem = ListMenuPresenter.this.f1504c.getExpandedItem();
            if (expandedItem != null) {
                ArrayList<MenuItemImpl> nonActionItems = ListMenuPresenter.this.f1504c.getNonActionItems();
                int size = nonActionItems.size();
                for (int i = 0; i < size; i++) {
                    if (nonActionItems.get(i) == expandedItem) {
                        this.f1511b = i;
                        return;
                    }
                }
            }
            this.f1511b = -1;
        }

        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }
    }
}
