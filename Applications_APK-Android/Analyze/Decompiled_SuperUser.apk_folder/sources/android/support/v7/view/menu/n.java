package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.v7.a.a;
import android.support.v7.view.menu.i;
import android.support.v7.widget.MenuPopupWindow;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

/* compiled from: StandardMenuPopup */
final class n extends g implements i, View.OnKeyListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final MenuPopupWindow f1570a;

    /* renamed from: b  reason: collision with root package name */
    View f1571b;

    /* renamed from: c  reason: collision with root package name */
    private final Context f1572c;

    /* renamed from: d  reason: collision with root package name */
    private final MenuBuilder f1573d;

    /* renamed from: e  reason: collision with root package name */
    private final d f1574e;

    /* renamed from: f  reason: collision with root package name */
    private final boolean f1575f;

    /* renamed from: g  reason: collision with root package name */
    private final int f1576g;

    /* renamed from: h  reason: collision with root package name */
    private final int f1577h;
    private final int i;
    private final ViewTreeObserver.OnGlobalLayoutListener j = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (n.this.c() && !n.this.f1570a.g()) {
                View view = n.this.f1571b;
                if (view == null || !view.isShown()) {
                    n.this.b();
                } else {
                    n.this.f1570a.a();
                }
            }
        }
    };
    private PopupWindow.OnDismissListener k;
    private View l;
    private i.a m;
    private ViewTreeObserver n;
    private boolean o;
    private boolean p;
    private int q;
    private int r = 0;
    private boolean s;

    public n(Context context, MenuBuilder menuBuilder, View view, int i2, int i3, boolean z) {
        this.f1572c = context;
        this.f1573d = menuBuilder;
        this.f1575f = z;
        this.f1574e = new d(menuBuilder, LayoutInflater.from(context), this.f1575f);
        this.f1577h = i2;
        this.i = i3;
        Resources resources = context.getResources();
        this.f1576g = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(a.d.abc_config_prefDialogWidth));
        this.l = view;
        this.f1570a = new MenuPopupWindow(this.f1572c, null, this.f1577h, this.i);
        menuBuilder.addMenuPresenter(this, context);
    }

    public void a(boolean z) {
        this.f1574e.a(z);
    }

    public void a(int i2) {
        this.r = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private boolean g() {
        if (c()) {
            return true;
        }
        if (this.o || this.l == null) {
            return false;
        }
        this.f1571b = this.l;
        this.f1570a.a((PopupWindow.OnDismissListener) this);
        this.f1570a.a((AdapterView.OnItemClickListener) this);
        this.f1570a.a(true);
        View view = this.f1571b;
        boolean z = this.n == null;
        this.n = view.getViewTreeObserver();
        if (z) {
            this.n.addOnGlobalLayoutListener(this.j);
        }
        this.f1570a.b(view);
        this.f1570a.e(this.r);
        if (!this.p) {
            this.q = a(this.f1574e, null, this.f1572c, this.f1576g);
            this.p = true;
        }
        this.f1570a.g(this.q);
        this.f1570a.h(2);
        this.f1570a.a(f());
        this.f1570a.a();
        ListView d2 = this.f1570a.d();
        d2.setOnKeyListener(this);
        if (this.s && this.f1573d.getHeaderTitle() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.f1572c).inflate(a.h.abc_popup_menu_header_item_layout, (ViewGroup) d2, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.f1573d.getHeaderTitle());
            }
            frameLayout.setEnabled(false);
            d2.addHeaderView(frameLayout, null, false);
        }
        this.f1570a.a((ListAdapter) this.f1574e);
        this.f1570a.a();
        return true;
    }

    public void a() {
        if (!g()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    public void b() {
        if (c()) {
            this.f1570a.b();
        }
    }

    public void a(MenuBuilder menuBuilder) {
    }

    public boolean c() {
        return !this.o && this.f1570a.c();
    }

    public void onDismiss() {
        this.o = true;
        this.f1573d.close();
        if (this.n != null) {
            if (!this.n.isAlive()) {
                this.n = this.f1571b.getViewTreeObserver();
            }
            this.n.removeGlobalOnLayoutListener(this.j);
            this.n = null;
        }
        if (this.k != null) {
            this.k.onDismiss();
        }
    }

    public void updateMenuView(boolean z) {
        this.p = false;
        if (this.f1574e != null) {
            this.f1574e.notifyDataSetChanged();
        }
    }

    public void setCallback(i.a aVar) {
        this.m = aVar;
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        if (subMenuBuilder.hasVisibleItems()) {
            h hVar = new h(this.f1572c, subMenuBuilder, this.f1571b, this.f1575f, this.f1577h, this.i);
            hVar.a(this.m);
            hVar.a(g.b(subMenuBuilder));
            hVar.a(this.k);
            this.k = null;
            this.f1573d.close(false);
            if (hVar.a(this.f1570a.j(), this.f1570a.k())) {
                if (this.m != null) {
                    this.m.a(subMenuBuilder);
                }
                return true;
            }
        }
        return false;
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        if (menuBuilder == this.f1573d) {
            b();
            if (this.m != null) {
                this.m.a(menuBuilder, z);
            }
        }
    }

    public boolean flagActionItems() {
        return false;
    }

    public Parcelable onSaveInstanceState() {
        return null;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
    }

    public void a(View view) {
        this.l = view;
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        b();
        return true;
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.k = onDismissListener;
    }

    public ListView d() {
        return this.f1570a.d();
    }

    public void b(int i2) {
        this.f1570a.c(i2);
    }

    public void c(int i2) {
        this.f1570a.d(i2);
    }

    public void b(boolean z) {
        this.s = z;
    }
}
