package android.support.v7.view.menu;

/* compiled from: MenuView */
public interface j {

    /* compiled from: MenuView */
    public interface a {
        MenuItemImpl getItemData();

        void initialize(MenuItemImpl menuItemImpl, int i);

        boolean prefersCondensedTitle();
    }

    void initialize(MenuBuilder menuBuilder);
}
