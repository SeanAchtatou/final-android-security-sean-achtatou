package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.view.ag;
import android.support.v4.view.e;
import android.support.v7.a.a;
import android.support.v7.view.menu.i;
import android.support.v7.widget.MenuPopupWindow;
import android.support.v7.widget.aa;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* compiled from: CascadingMenuPopup */
final class c extends g implements i, View.OnKeyListener, PopupWindow.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final Handler f1531a;

    /* renamed from: b  reason: collision with root package name */
    final List<a> f1532b = new ArrayList();

    /* renamed from: c  reason: collision with root package name */
    View f1533c;

    /* renamed from: d  reason: collision with root package name */
    boolean f1534d;

    /* renamed from: e  reason: collision with root package name */
    private final Context f1535e;

    /* renamed from: f  reason: collision with root package name */
    private final int f1536f;

    /* renamed from: g  reason: collision with root package name */
    private final int f1537g;

    /* renamed from: h  reason: collision with root package name */
    private final int f1538h;
    private final boolean i;
    private final List<MenuBuilder> j = new LinkedList();
    private final ViewTreeObserver.OnGlobalLayoutListener k = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (c.this.c() && c.this.f1532b.size() > 0 && !c.this.f1532b.get(0).f1545a.g()) {
                View view = c.this.f1533c;
                if (view == null || !view.isShown()) {
                    c.this.b();
                    return;
                }
                for (a aVar : c.this.f1532b) {
                    aVar.f1545a.a();
                }
            }
        }
    };
    private final aa l = new aa() {
        public void a(MenuBuilder menuBuilder, MenuItem menuItem) {
            c.this.f1531a.removeCallbacksAndMessages(menuBuilder);
        }

        public void b(final MenuBuilder menuBuilder, final MenuItem menuItem) {
            int i;
            final a aVar;
            c.this.f1531a.removeCallbacksAndMessages(null);
            int i2 = 0;
            int size = c.this.f1532b.size();
            while (true) {
                if (i2 >= size) {
                    i = -1;
                    break;
                } else if (menuBuilder == c.this.f1532b.get(i2).f1546b) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i != -1) {
                int i3 = i + 1;
                if (i3 < c.this.f1532b.size()) {
                    aVar = c.this.f1532b.get(i3);
                } else {
                    aVar = null;
                }
                c.this.f1531a.postAtTime(new Runnable() {
                    public void run() {
                        if (aVar != null) {
                            c.this.f1534d = true;
                            aVar.f1546b.close(false);
                            c.this.f1534d = false;
                        }
                        if (menuItem.isEnabled() && menuItem.hasSubMenu()) {
                            menuBuilder.performItemAction(menuItem, 4);
                        }
                    }
                }, menuBuilder, SystemClock.uptimeMillis() + 200);
            }
        }
    };
    private int m = 0;
    private int n = 0;
    private View o;
    private int p;
    private boolean q;
    private boolean r;
    private int s;
    private int t;
    private boolean u;
    private boolean v;
    private i.a w;
    private ViewTreeObserver x;
    private PopupWindow.OnDismissListener y;

    public c(Context context, View view, int i2, int i3, boolean z) {
        this.f1535e = context;
        this.o = view;
        this.f1537g = i2;
        this.f1538h = i3;
        this.i = z;
        this.u = false;
        this.p = h();
        Resources resources = context.getResources();
        this.f1536f = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(a.d.abc_config_prefDialogWidth));
        this.f1531a = new Handler();
    }

    public void a(boolean z) {
        this.u = z;
    }

    private MenuPopupWindow g() {
        MenuPopupWindow menuPopupWindow = new MenuPopupWindow(this.f1535e, null, this.f1537g, this.f1538h);
        menuPopupWindow.a(this.l);
        menuPopupWindow.a((AdapterView.OnItemClickListener) this);
        menuPopupWindow.a((PopupWindow.OnDismissListener) this);
        menuPopupWindow.b(this.o);
        menuPopupWindow.e(this.n);
        menuPopupWindow.a(true);
        return menuPopupWindow;
    }

    public void a() {
        if (!c()) {
            for (MenuBuilder c2 : this.j) {
                c(c2);
            }
            this.j.clear();
            this.f1533c = this.o;
            if (this.f1533c != null) {
                boolean z = this.x == null;
                this.x = this.f1533c.getViewTreeObserver();
                if (z) {
                    this.x.addOnGlobalLayoutListener(this.k);
                }
            }
        }
    }

    public void b() {
        int size = this.f1532b.size();
        if (size > 0) {
            a[] aVarArr = (a[]) this.f1532b.toArray(new a[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                a aVar = aVarArr[i2];
                if (aVar.f1545a.c()) {
                    aVar.f1545a.b();
                }
            }
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        b();
        return true;
    }

    private int h() {
        if (ag.g(this.o) == 1) {
            return 0;
        }
        return 1;
    }

    private int d(int i2) {
        ListView a2 = this.f1532b.get(this.f1532b.size() - 1).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.f1533c.getWindowVisibleDisplayFrame(rect);
        if (this.p == 1) {
            if (a2.getWidth() + iArr[0] + i2 > rect.right) {
                return 0;
            }
            return 1;
        } else if (iArr[0] - i2 < 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public void a(MenuBuilder menuBuilder) {
        menuBuilder.addMenuPresenter(this, this.f1535e);
        if (c()) {
            c(menuBuilder);
        } else {
            this.j.add(menuBuilder);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void c(MenuBuilder menuBuilder) {
        View view;
        a aVar;
        boolean z;
        int i2;
        LayoutInflater from = LayoutInflater.from(this.f1535e);
        d dVar = new d(menuBuilder, from, this.i);
        if (!c() && this.u) {
            dVar.a(true);
        } else if (c()) {
            dVar.a(g.b(menuBuilder));
        }
        int a2 = a(dVar, null, this.f1535e, this.f1536f);
        MenuPopupWindow g2 = g();
        g2.a((ListAdapter) dVar);
        g2.g(a2);
        g2.e(this.n);
        if (this.f1532b.size() > 0) {
            a aVar2 = this.f1532b.get(this.f1532b.size() - 1);
            view = a(aVar2, menuBuilder);
            aVar = aVar2;
        } else {
            view = null;
            aVar = null;
        }
        if (view != null) {
            g2.b(false);
            g2.a((Object) null);
            int d2 = d(a2);
            if (d2 == 1) {
                z = true;
            } else {
                z = false;
            }
            this.p = d2;
            int[] iArr = new int[2];
            view.getLocationInWindow(iArr);
            int j2 = aVar.f1545a.j() + iArr[0];
            int k2 = iArr[1] + aVar.f1545a.k();
            if ((this.n & 5) == 5) {
                if (z) {
                    i2 = j2 + a2;
                } else {
                    i2 = j2 - view.getWidth();
                }
            } else if (z) {
                i2 = view.getWidth() + j2;
            } else {
                i2 = j2 - a2;
            }
            g2.c(i2);
            g2.d(k2);
        } else {
            if (this.q) {
                g2.c(this.s);
            }
            if (this.r) {
                g2.d(this.t);
            }
            g2.a(f());
        }
        this.f1532b.add(new a(g2, menuBuilder, this.p));
        g2.a();
        if (aVar == null && this.v && menuBuilder.getHeaderTitle() != null) {
            ListView d3 = g2.d();
            FrameLayout frameLayout = (FrameLayout) from.inflate(a.h.abc_popup_menu_header_item_layout, (ViewGroup) d3, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(menuBuilder.getHeaderTitle());
            d3.addHeaderView(frameLayout, null, false);
            g2.a();
        }
    }

    private MenuItem a(MenuBuilder menuBuilder, MenuBuilder menuBuilder2) {
        int size = menuBuilder.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = menuBuilder.getItem(i2);
            if (item.hasSubMenu() && menuBuilder2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    private View a(a aVar, MenuBuilder menuBuilder) {
        d dVar;
        int i2;
        int i3;
        int i4 = 0;
        MenuItem a2 = a(aVar.f1546b, menuBuilder);
        if (a2 == null) {
            return null;
        }
        ListView a3 = aVar.a();
        ListAdapter adapter = a3.getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i2 = headerViewListAdapter.getHeadersCount();
            dVar = (d) headerViewListAdapter.getWrappedAdapter();
        } else {
            dVar = (d) adapter;
            i2 = 0;
        }
        int count = dVar.getCount();
        while (true) {
            if (i4 >= count) {
                i3 = -1;
                break;
            } else if (a2 == dVar.getItem(i4)) {
                i3 = i4;
                break;
            } else {
                i4++;
            }
        }
        if (i3 == -1) {
            return null;
        }
        int firstVisiblePosition = (i3 + i2) - a3.getFirstVisiblePosition();
        if (firstVisiblePosition < 0 || firstVisiblePosition >= a3.getChildCount()) {
            return null;
        }
        return a3.getChildAt(firstVisiblePosition);
    }

    public boolean c() {
        return this.f1532b.size() > 0 && this.f1532b.get(0).f1545a.c();
    }

    public void onDismiss() {
        a aVar;
        int size = this.f1532b.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                aVar = null;
                break;
            }
            aVar = this.f1532b.get(i2);
            if (!aVar.f1545a.c()) {
                break;
            }
            i2++;
        }
        if (aVar != null) {
            aVar.f1546b.close(false);
        }
    }

    public void updateMenuView(boolean z) {
        for (a a2 : this.f1532b) {
            a(a2.a().getAdapter()).notifyDataSetChanged();
        }
    }

    public void setCallback(i.a aVar) {
        this.w = aVar;
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        for (a next : this.f1532b) {
            if (subMenuBuilder == next.f1546b) {
                next.a().requestFocus();
                return true;
            }
        }
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        a(subMenuBuilder);
        if (this.w != null) {
            this.w.a(subMenuBuilder);
        }
        return true;
    }

    private int d(MenuBuilder menuBuilder) {
        int size = this.f1532b.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (menuBuilder == this.f1532b.get(i2).f1546b) {
                return i2;
            }
        }
        return -1;
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        int d2 = d(menuBuilder);
        if (d2 >= 0) {
            int i2 = d2 + 1;
            if (i2 < this.f1532b.size()) {
                this.f1532b.get(i2).f1546b.close(false);
            }
            a remove = this.f1532b.remove(d2);
            remove.f1546b.removeMenuPresenter(this);
            if (this.f1534d) {
                remove.f1545a.b((Object) null);
                remove.f1545a.b(0);
            }
            remove.f1545a.b();
            int size = this.f1532b.size();
            if (size > 0) {
                this.p = this.f1532b.get(size - 1).f1547c;
            } else {
                this.p = h();
            }
            if (size == 0) {
                b();
                if (this.w != null) {
                    this.w.a(menuBuilder, true);
                }
                if (this.x != null) {
                    if (this.x.isAlive()) {
                        this.x.removeGlobalOnLayoutListener(this.k);
                    }
                    this.x = null;
                }
                this.y.onDismiss();
            } else if (z) {
                this.f1532b.get(0).f1546b.close(false);
            }
        }
    }

    public boolean flagActionItems() {
        return false;
    }

    public Parcelable onSaveInstanceState() {
        return null;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
    }

    public void a(int i2) {
        if (this.m != i2) {
            this.m = i2;
            this.n = e.a(i2, ag.g(this.o));
        }
    }

    public void a(View view) {
        if (this.o != view) {
            this.o = view;
            this.n = e.a(this.m, ag.g(this.o));
        }
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.y = onDismissListener;
    }

    public ListView d() {
        if (this.f1532b.isEmpty()) {
            return null;
        }
        return this.f1532b.get(this.f1532b.size() - 1).a();
    }

    public void b(int i2) {
        this.q = true;
        this.s = i2;
    }

    public void c(int i2) {
        this.r = true;
        this.t = i2;
    }

    public void b(boolean z) {
        this.v = z;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return false;
    }

    /* compiled from: CascadingMenuPopup */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        public final MenuPopupWindow f1545a;

        /* renamed from: b  reason: collision with root package name */
        public final MenuBuilder f1546b;

        /* renamed from: c  reason: collision with root package name */
        public final int f1547c;

        public a(MenuPopupWindow menuPopupWindow, MenuBuilder menuBuilder, int i) {
            this.f1545a = menuPopupWindow;
            this.f1546b = menuBuilder;
            this.f1547c = i;
        }

        public ListView a() {
            return this.f1545a.d();
        }
    }
}
