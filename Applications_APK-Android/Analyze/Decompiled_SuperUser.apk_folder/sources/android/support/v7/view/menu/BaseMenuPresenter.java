package android.support.v7.view.menu;

import android.content.Context;
import android.support.v4.view.ag;
import android.support.v7.view.menu.i;
import android.support.v7.view.menu.j;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class BaseMenuPresenter implements i {

    /* renamed from: a  reason: collision with root package name */
    protected Context f1483a;

    /* renamed from: b  reason: collision with root package name */
    protected Context f1484b;

    /* renamed from: c  reason: collision with root package name */
    protected MenuBuilder f1485c;

    /* renamed from: d  reason: collision with root package name */
    protected LayoutInflater f1486d;

    /* renamed from: e  reason: collision with root package name */
    protected LayoutInflater f1487e;

    /* renamed from: f  reason: collision with root package name */
    protected j f1488f;

    /* renamed from: g  reason: collision with root package name */
    private i.a f1489g;

    /* renamed from: h  reason: collision with root package name */
    private int f1490h;
    private int i;
    private int j;

    public abstract void a(MenuItemImpl menuItemImpl, j.a aVar);

    public BaseMenuPresenter(Context context, int i2, int i3) {
        this.f1483a = context;
        this.f1486d = LayoutInflater.from(context);
        this.f1490h = i2;
        this.i = i3;
    }

    public void initForMenu(Context context, MenuBuilder menuBuilder) {
        this.f1484b = context;
        this.f1487e = LayoutInflater.from(this.f1484b);
        this.f1485c = menuBuilder;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public j a(ViewGroup viewGroup) {
        if (this.f1488f == null) {
            this.f1488f = (j) this.f1486d.inflate(this.f1490h, viewGroup, false);
            this.f1488f.initialize(this.f1485c);
            updateMenuView(true);
        }
        return this.f1488f;
    }

    public void updateMenuView(boolean z) {
        int i2;
        int i3;
        ViewGroup viewGroup = (ViewGroup) this.f1488f;
        if (viewGroup != null) {
            if (this.f1485c != null) {
                this.f1485c.flagActionItems();
                ArrayList<MenuItemImpl> visibleItems = this.f1485c.getVisibleItems();
                int size = visibleItems.size();
                int i4 = 0;
                i2 = 0;
                while (i4 < size) {
                    MenuItemImpl menuItemImpl = visibleItems.get(i4);
                    if (a(i2, menuItemImpl)) {
                        View childAt = viewGroup.getChildAt(i2);
                        MenuItemImpl itemData = childAt instanceof j.a ? ((j.a) childAt).getItemData() : null;
                        View a2 = a(menuItemImpl, childAt, viewGroup);
                        if (menuItemImpl != itemData) {
                            a2.setPressed(false);
                            ag.y(a2);
                        }
                        if (a2 != childAt) {
                            a(a2, i2);
                        }
                        i3 = i2 + 1;
                    } else {
                        i3 = i2;
                    }
                    i4++;
                    i2 = i3;
                }
            } else {
                i2 = 0;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.f1488f).addView(view, i2);
    }

    /* access modifiers changed from: protected */
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    public void setCallback(i.a aVar) {
        this.f1489g = aVar;
    }

    public i.a a() {
        return this.f1489g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public j.a b(ViewGroup viewGroup) {
        return (j.a) this.f1486d.inflate(this.i, viewGroup, false);
    }

    public View a(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        j.a b2;
        if (view instanceof j.a) {
            b2 = (j.a) view;
        } else {
            b2 = b(viewGroup);
        }
        a(menuItemImpl, b2);
        return (View) b2;
    }

    public boolean a(int i2, MenuItemImpl menuItemImpl) {
        return true;
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        if (this.f1489g != null) {
            this.f1489g.a(menuBuilder, z);
        }
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        if (this.f1489g != null) {
            return this.f1489g.a(subMenuBuilder);
        }
        return false;
    }

    public boolean flagActionItems() {
        return false;
    }

    public boolean expandItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean collapseItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public int getId() {
        return this.j;
    }

    public void a(int i2) {
        this.j = i2;
    }
}
