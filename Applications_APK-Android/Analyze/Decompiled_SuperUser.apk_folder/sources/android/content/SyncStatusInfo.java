package android.content;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class SyncStatusInfo implements Parcelable {
    public static final Parcelable.Creator<SyncStatusInfo> CREATOR = new Parcelable.Creator<SyncStatusInfo>() {
        /* renamed from: a */
        public SyncStatusInfo createFromParcel(Parcel parcel) {
            return new SyncStatusInfo(parcel);
        }

        /* renamed from: a */
        public SyncStatusInfo[] newArray(int i) {
            return new SyncStatusInfo[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public final int f20a;

    /* renamed from: b  reason: collision with root package name */
    public long f21b;

    /* renamed from: c  reason: collision with root package name */
    public int f22c;

    /* renamed from: d  reason: collision with root package name */
    public int f23d;

    /* renamed from: e  reason: collision with root package name */
    public int f24e;

    /* renamed from: f  reason: collision with root package name */
    public int f25f;

    /* renamed from: g  reason: collision with root package name */
    public int f26g;

    /* renamed from: h  reason: collision with root package name */
    public long f27h;
    public int i;
    public long j;
    public int k;
    public String l;
    public long m;
    public boolean n;
    public boolean o;
    private ArrayList<Long> p;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3 = 1;
        parcel.writeInt(2);
        parcel.writeInt(this.f20a);
        parcel.writeLong(this.f21b);
        parcel.writeInt(this.f22c);
        parcel.writeInt(this.f23d);
        parcel.writeInt(this.f24e);
        parcel.writeInt(this.f25f);
        parcel.writeInt(this.f26g);
        parcel.writeLong(this.f27h);
        parcel.writeInt(this.i);
        parcel.writeLong(this.j);
        parcel.writeInt(this.k);
        parcel.writeString(this.l);
        parcel.writeLong(this.m);
        parcel.writeInt(this.n ? 1 : 0);
        if (!this.o) {
            i3 = 0;
        }
        parcel.writeInt(i3);
        if (this.p != null) {
            parcel.writeInt(this.p.size());
            Iterator<Long> it = this.p.iterator();
            while (it.hasNext()) {
                parcel.writeLong(it.next().longValue());
            }
            return;
        }
        parcel.writeInt(-1);
    }

    public SyncStatusInfo(Parcel parcel) {
        boolean z;
        int readInt = parcel.readInt();
        if (!(readInt == 2 || readInt == 1)) {
            Log.w("SyncStatusInfo", "Unknown version: " + readInt);
        }
        this.f20a = parcel.readInt();
        this.f21b = parcel.readLong();
        this.f22c = parcel.readInt();
        this.f23d = parcel.readInt();
        this.f24e = parcel.readInt();
        this.f25f = parcel.readInt();
        this.f26g = parcel.readInt();
        this.f27h = parcel.readLong();
        this.i = parcel.readInt();
        this.j = parcel.readLong();
        this.k = parcel.readInt();
        this.l = parcel.readString();
        this.m = parcel.readLong();
        this.n = parcel.readInt() != 0;
        if (parcel.readInt() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.o = z;
        if (readInt == 1) {
            this.p = null;
            return;
        }
        int readInt2 = parcel.readInt();
        if (readInt2 < 0) {
            this.p = null;
            return;
        }
        this.p = new ArrayList<>();
        for (int i2 = 0; i2 < readInt2; i2++) {
            this.p.add(Long.valueOf(parcel.readLong()));
        }
    }
}
