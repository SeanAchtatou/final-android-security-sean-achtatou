package android.content.pm;

import android.content.ComponentName;
import android.content.IntentFilter;
import android.os.Bundle;
import java.util.ArrayList;

public class PackageParser {
    public static final int PARSE_IS_SYSTEM = 1;

    public static final class Activity extends Component<ActivityIntentInfo> {
        public ActivityInfo info;
    }

    public static class IntentInfo extends IntentFilter {
        public int banner;
        public boolean hasDefault;
        public int icon;
        public int labelRes;
        public int logo;
        public CharSequence nonLocalizedLabel;
    }

    public static class Component<II extends IntentInfo> {
        public String className;
        public ArrayList<II> intents;
        public Bundle metaData;
        public Package owner;

        public ComponentName getComponentName() {
            return null;
        }
    }

    public class Package {
        public final ArrayList<Activity> activities = new ArrayList<>(0);
        public ApplicationInfo applicationInfo;
        public ArrayList<ConfigurationInfo> configPreferences = null;
        public final ArrayList<Instrumentation> instrumentation = new ArrayList<>(0);
        public Bundle mAppMetaData;
        public Object mExtras;
        public int mPreferredOrder;
        public String mSharedUserId;
        public int mSharedUserLabel;
        public Signature[] mSignatures;
        public int mVersionCode;
        public String mVersionName;
        public String packageName;
        public final ArrayList<PermissionGroup> permissionGroups = new ArrayList<>(0);
        public final ArrayList<Permission> permissions = new ArrayList<>(0);
        public final ArrayList<Provider> providers = new ArrayList<>(0);
        public final ArrayList<Activity> receivers = new ArrayList<>(0);
        public ArrayList<FeatureInfo> reqFeatures = null;
        public final ArrayList<String> requestedPermissions = new ArrayList<>();
        public final ArrayList<Service> services = new ArrayList<>(0);
        public ArrayList<String> usesLibraries;

        public Package() {
        }
    }

    public final class Service extends Component<ServiceIntentInfo> {
        public ServiceInfo info;

        public Service() {
        }
    }

    public final class Provider extends Component<ProviderIntentInfo> {
        public ProviderInfo info;

        public Provider() {
        }
    }

    public final class Instrumentation extends Component<IntentInfo> {
        public InstrumentationInfo info;

        public Instrumentation() {
        }
    }

    public final class Permission extends Component<IntentInfo> {
        public PermissionInfo info;

        public Permission() {
        }
    }

    public final class PermissionGroup extends Component<IntentInfo> {
        public PermissionGroupInfo info;

        public PermissionGroup() {
        }
    }

    public class ActivityIntentInfo extends IntentInfo {
        public Activity activity;

        public ActivityIntentInfo() {
        }
    }

    public class ServiceIntentInfo extends IntentInfo {
        public Service service;

        public ServiceIntentInfo() {
        }
    }

    public class ProviderIntentInfo extends IntentInfo {
        public Provider provider;

        public ProviderIntentInfo() {
        }
    }
}
