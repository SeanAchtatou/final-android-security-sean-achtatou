package android.accounts;

import android.accounts.b;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* compiled from: IAccountAuthenticator */
public interface a extends IInterface {
    void a(b bVar, Account account);

    void a(b bVar, Account account, Bundle bundle);

    void a(b bVar, Account account, String str, Bundle bundle);

    void a(b bVar, Account account, String[] strArr);

    void a(b bVar, String str);

    void a(b bVar, String str, String str2, String[] strArr, Bundle bundle);

    void b(b bVar, Account account);

    void b(b bVar, Account account, Bundle bundle);

    void b(b bVar, Account account, String str, Bundle bundle);

    void b(b bVar, String str);

    /* renamed from: android.accounts.a$a  reason: collision with other inner class name */
    /* compiled from: IAccountAuthenticator */
    public static abstract class C0000a extends Binder implements a {
        public static a a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("android.accounts.IAccountAuthenticator");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof a)) {
                return new C0001a(iBinder);
            }
            return (a) queryLocalInterface;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            Account account;
            Bundle bundle;
            Account account2;
            Account account3;
            Account account4;
            Account account5;
            Bundle bundle2;
            Account account6;
            Bundle bundle3;
            Account account7;
            Bundle bundle4;
            Bundle bundle5 = null;
            switch (i) {
                case 1:
                    parcel.enforceInterface("android.accounts.IAccountAuthenticator");
                    b asInterface = b.a.asInterface(parcel.readStrongBinder());
                    String readString = parcel.readString();
                    String readString2 = parcel.readString();
                    String[] createStringArray = parcel.createStringArray();
                    if (parcel.readInt() != 0) {
                        bundle5 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    }
                    a(asInterface, readString, readString2, createStringArray, bundle5);
                    parcel2.writeNoException();
                    return true;
                case 2:
                    parcel.enforceInterface("android.accounts.IAccountAuthenticator");
                    b asInterface2 = b.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account7 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account7 = null;
                    }
                    if (parcel.readInt() != 0) {
                        bundle4 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    } else {
                        bundle4 = null;
                    }
                    a(asInterface2, account7, bundle4);
                    parcel2.writeNoException();
                    return true;
                case 3:
                    parcel.enforceInterface("android.accounts.IAccountAuthenticator");
                    b asInterface3 = b.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account6 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account6 = null;
                    }
                    String readString3 = parcel.readString();
                    if (parcel.readInt() != 0) {
                        bundle3 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    } else {
                        bundle3 = null;
                    }
                    a(asInterface3, account6, readString3, bundle3);
                    parcel2.writeNoException();
                    return true;
                case 4:
                    parcel.enforceInterface("android.accounts.IAccountAuthenticator");
                    a(b.a.asInterface(parcel.readStrongBinder()), parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 5:
                    parcel.enforceInterface("android.accounts.IAccountAuthenticator");
                    b asInterface4 = b.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account5 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account5 = null;
                    }
                    String readString4 = parcel.readString();
                    if (parcel.readInt() != 0) {
                        bundle2 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    } else {
                        bundle2 = null;
                    }
                    b(asInterface4, account5, readString4, bundle2);
                    parcel2.writeNoException();
                    return true;
                case 6:
                    parcel.enforceInterface("android.accounts.IAccountAuthenticator");
                    b(b.a.asInterface(parcel.readStrongBinder()), parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 7:
                    parcel.enforceInterface("android.accounts.IAccountAuthenticator");
                    b asInterface5 = b.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account4 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account4 = null;
                    }
                    a(asInterface5, account4, parcel.createStringArray());
                    parcel2.writeNoException();
                    return true;
                case 8:
                    parcel.enforceInterface("android.accounts.IAccountAuthenticator");
                    b asInterface6 = b.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account3 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account3 = null;
                    }
                    a(asInterface6, account3);
                    parcel2.writeNoException();
                    return true;
                case 9:
                    parcel.enforceInterface("android.accounts.IAccountAuthenticator");
                    b asInterface7 = b.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account2 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account2 = null;
                    }
                    b(asInterface7, account2);
                    parcel2.writeNoException();
                    return true;
                case 10:
                    parcel.enforceInterface("android.accounts.IAccountAuthenticator");
                    b asInterface8 = b.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account = null;
                    }
                    if (parcel.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    } else {
                        bundle = null;
                    }
                    b(asInterface8, account, bundle);
                    parcel2.writeNoException();
                    return true;
                case 1598968902:
                    parcel2.writeString("android.accounts.IAccountAuthenticator");
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }

        /* renamed from: android.accounts.a$a$a  reason: collision with other inner class name */
        /* compiled from: IAccountAuthenticator */
        private static class C0001a implements a {

            /* renamed from: a  reason: collision with root package name */
            private IBinder f17a;

            C0001a(IBinder iBinder) {
                this.f17a = iBinder;
            }

            public IBinder asBinder() {
                return this.f17a;
            }

            public void a(b bVar, String str, String str2, String[] strArr, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f17a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(b bVar, Account account, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f17a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(b bVar, Account account, String str, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f17a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(b bVar, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    obtain.writeString(str);
                    this.f17a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(b bVar, Account account, String str, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f17a.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(b bVar, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    obtain.writeString(str);
                    this.f17a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(b bVar, Account account, String[] strArr) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStringArray(strArr);
                    this.f17a.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(b bVar, Account account) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f17a.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(b bVar, Account account) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f17a.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(b bVar, Account account, Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.accounts.IAccountAuthenticator");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f17a.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
