package android.location;

import android.os.Parcel;
import android.os.Parcelable;

public final class LocationRequest implements Parcelable {
    public static final Parcelable.Creator<LocationRequest> CREATOR = new Parcelable.Creator<LocationRequest>() {
        /* renamed from: a */
        public LocationRequest createFromParcel(Parcel parcel) {
            return null;
        }

        /* renamed from: a */
        public LocationRequest[] newArray(int i) {
            return null;
        }
    };

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
    }
}
