package android.net.wifi;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Messenger;
import android.os.Parcel;

/* compiled from: IWifiScanner */
public interface a extends IInterface {
    Bundle getAvailableChannels(int i);

    Messenger getMessenger();

    /* renamed from: android.net.wifi.a$a  reason: collision with other inner class name */
    /* compiled from: IWifiScanner */
    public static abstract class C0006a extends Binder implements a {
        private static final String DESCRIPTOR = "android.net.wifi.IWifiScanner";
        static final int TRANSACTION_getAvailableChannels = 2;
        static final int TRANSACTION_getMessenger = 1;

        public C0006a() {
            attachInterface(this, DESCRIPTOR);
        }

        public static a asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof a)) {
                return new C0007a(iBinder);
            }
            return (a) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            switch (i) {
                case 1:
                    parcel.enforceInterface(DESCRIPTOR);
                    Messenger messenger = getMessenger();
                    parcel2.writeNoException();
                    if (messenger != null) {
                        parcel2.writeInt(1);
                        messenger.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 2:
                    parcel.enforceInterface(DESCRIPTOR);
                    Bundle availableChannels = getAvailableChannels(parcel.readInt());
                    parcel2.writeNoException();
                    if (availableChannels != null) {
                        parcel2.writeInt(1);
                        availableChannels.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 1598968902:
                    parcel2.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }

        /* renamed from: android.net.wifi.a$a$a  reason: collision with other inner class name */
        /* compiled from: IWifiScanner */
        private static class C0007a implements a {

            /* renamed from: a  reason: collision with root package name */
            private IBinder f29a;

            C0007a(IBinder iBinder) {
                this.f29a = iBinder;
            }

            public IBinder asBinder() {
                return this.f29a;
            }

            public Messenger getMessenger() {
                Messenger messenger;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(C0006a.DESCRIPTOR);
                    this.f29a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        messenger = (Messenger) Messenger.CREATOR.createFromParcel(obtain2);
                    } else {
                        messenger = null;
                    }
                    return messenger;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Bundle getAvailableChannels(int i) {
                Bundle bundle;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(C0006a.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.f29a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(obtain2);
                    } else {
                        bundle = null;
                    }
                    return bundle;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
