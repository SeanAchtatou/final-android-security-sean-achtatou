package me.everything.android.ui.overscroll.adapters;

import android.view.View;
import android.widget.ScrollView;

/* compiled from: ScrollViewOverScrollDecorAdapter */
public class d implements b {

    /* renamed from: a  reason: collision with root package name */
    protected final ScrollView f7652a;

    public d(ScrollView scrollView) {
        this.f7652a = scrollView;
    }

    public View a() {
        return this.f7652a;
    }

    public boolean b() {
        return !this.f7652a.canScrollVertically(-1);
    }

    public boolean c() {
        return !this.f7652a.canScrollVertically(1);
    }
}
