package me.everything.android.ui.overscroll;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.widget.ListView;
import android.widget.ScrollView;
import me.everything.android.ui.overscroll.adapters.ViewPagerOverScrollDecorAdapter;
import me.everything.android.ui.overscroll.adapters.a;
import me.everything.android.ui.overscroll.adapters.c;
import me.everything.android.ui.overscroll.adapters.d;

/* compiled from: OverScrollDecoratorHelper */
public class g {
    public static b a(RecyclerView recyclerView, int i) {
        switch (i) {
            case 0:
                return new h(new c(recyclerView));
            case 1:
                return new a(new c(recyclerView));
            default:
                throw new IllegalArgumentException("orientation");
        }
    }

    public static b a(ListView listView) {
        return new h(new a(listView));
    }

    public static b a(ScrollView scrollView) {
        return new h(new d(scrollView));
    }

    public static b a(ViewPager viewPager) {
        return new a(new ViewPagerOverScrollDecorAdapter(viewPager));
    }
}
