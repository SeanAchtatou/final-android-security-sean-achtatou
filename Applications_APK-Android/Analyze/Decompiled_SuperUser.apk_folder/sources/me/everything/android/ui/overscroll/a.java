package me.everything.android.ui.overscroll;

import android.view.MotionEvent;
import android.view.View;
import me.everything.android.ui.overscroll.f;

/* compiled from: HorizontalOverScrollBounceEffectDecorator */
public class a extends f {

    /* compiled from: HorizontalOverScrollBounceEffectDecorator */
    protected static class b extends f.e {
        protected b() {
        }

        public boolean a(View view, MotionEvent motionEvent) {
            boolean z = false;
            if (motionEvent.getHistorySize() == 0) {
                return false;
            }
            float y = motionEvent.getY(0) - motionEvent.getHistoricalY(0, 0);
            float x = motionEvent.getX(0) - motionEvent.getHistoricalX(0, 0);
            if (Math.abs(x) < Math.abs(y)) {
                return false;
            }
            this.f7671a = view.getTranslationX();
            this.f7672b = x;
            if (this.f7672b > 0.0f) {
                z = true;
            }
            this.f7673c = z;
            return true;
        }
    }

    /* renamed from: me.everything.android.ui.overscroll.a$a  reason: collision with other inner class name */
    /* compiled from: HorizontalOverScrollBounceEffectDecorator */
    protected static class C0109a extends f.a {
        public C0109a() {
            this.f7661a = View.TRANSLATION_X;
        }

        /* access modifiers changed from: protected */
        public void a(View view) {
            this.f7662b = view.getTranslationX();
            this.f7663c = (float) view.getWidth();
        }
    }

    public a(me.everything.android.ui.overscroll.adapters.b bVar) {
        this(bVar, 3.0f, 1.0f, -2.0f);
    }

    public a(me.everything.android.ui.overscroll.adapters.b bVar, float f2, float f3, float f4) {
        super(bVar, f4, f2, f3);
    }

    /* access modifiers changed from: protected */
    public f.e a() {
        return new b();
    }

    /* access modifiers changed from: protected */
    public f.a b() {
        return new C0109a();
    }

    /* access modifiers changed from: protected */
    public void a(View view, float f2) {
        view.setTranslationX(f2);
    }

    /* access modifiers changed from: protected */
    public void a(View view, float f2, MotionEvent motionEvent) {
        view.setTranslationX(f2);
        motionEvent.offsetLocation(f2 - motionEvent.getX(0), 0.0f);
    }
}
