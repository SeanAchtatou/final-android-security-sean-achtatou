package me.everything.android.ui.overscroll.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

/* compiled from: RecyclerViewOverScrollDecorAdapter */
public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    protected final RecyclerView f7647a;

    /* renamed from: b  reason: collision with root package name */
    protected final a f7648b;

    /* renamed from: c  reason: collision with root package name */
    protected boolean f7649c = false;

    /* compiled from: RecyclerViewOverScrollDecorAdapter */
    protected interface a {
        boolean a();

        boolean b();
    }

    public c(RecyclerView recyclerView) {
        int M;
        this.f7647a = recyclerView;
        RecyclerView.h layoutManager = recyclerView.getLayoutManager();
        if ((layoutManager instanceof LinearLayoutManager) || (layoutManager instanceof StaggeredGridLayoutManager)) {
            if (layoutManager instanceof LinearLayoutManager) {
                M = ((LinearLayoutManager) layoutManager).f();
            } else {
                M = ((StaggeredGridLayoutManager) layoutManager).M();
            }
            if (M == 0) {
                this.f7648b = new b();
            } else {
                this.f7648b = new C0110c();
            }
        } else {
            throw new IllegalArgumentException("Recycler views with custom layout managers are not supported by this adapter out of the box.Try implementing and providing an explicit 'impl' parameter to the other c'tors, or otherwise create a custom adapter subclass of your own.");
        }
    }

    public View a() {
        return this.f7647a;
    }

    public boolean b() {
        return !this.f7649c && this.f7648b.a();
    }

    public boolean c() {
        return !this.f7649c && this.f7648b.b();
    }

    /* compiled from: RecyclerViewOverScrollDecorAdapter */
    protected class b implements a {
        protected b() {
        }

        public boolean a() {
            return !c.this.f7647a.canScrollHorizontally(-1);
        }

        public boolean b() {
            return !c.this.f7647a.canScrollHorizontally(1);
        }
    }

    /* renamed from: me.everything.android.ui.overscroll.adapters.c$c  reason: collision with other inner class name */
    /* compiled from: RecyclerViewOverScrollDecorAdapter */
    protected class C0110c implements a {
        protected C0110c() {
        }

        public boolean a() {
            return !c.this.f7647a.canScrollVertically(-1);
        }

        public boolean b() {
            return !c.this.f7647a.canScrollVertically(1);
        }
    }
}
