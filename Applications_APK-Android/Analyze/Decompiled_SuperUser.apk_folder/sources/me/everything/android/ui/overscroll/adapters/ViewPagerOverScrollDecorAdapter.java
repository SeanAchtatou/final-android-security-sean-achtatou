package me.everything.android.ui.overscroll.adapters;

import android.support.v4.view.ViewPager;
import android.view.View;

public class ViewPagerOverScrollDecorAdapter implements ViewPager.e, b {

    /* renamed from: a  reason: collision with root package name */
    protected final ViewPager f7643a;

    /* renamed from: b  reason: collision with root package name */
    protected int f7644b = 0;

    /* renamed from: c  reason: collision with root package name */
    protected float f7645c;

    public ViewPagerOverScrollDecorAdapter(ViewPager viewPager) {
        this.f7643a = viewPager;
        this.f7643a.a(this);
        this.f7644b = this.f7643a.getCurrentItem();
        this.f7645c = 0.0f;
    }

    public View a() {
        return this.f7643a;
    }

    public boolean b() {
        return this.f7644b == 0 && this.f7645c == 0.0f;
    }

    public boolean c() {
        return this.f7644b == this.f7643a.getAdapter().b() + -1 && this.f7645c == 0.0f;
    }

    public void onPageScrolled(int i, float f2, int i2) {
        this.f7644b = i;
        this.f7645c = f2;
    }

    public void onPageSelected(int i) {
    }

    public void onPageScrollStateChanged(int i) {
    }
}
