package me.everything.android.ui.overscroll;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.util.Property;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import me.everything.android.ui.overscroll.e;

/* compiled from: OverScrollBounceEffectDecoratorBase */
public abstract class f implements View.OnTouchListener, b {

    /* renamed from: a  reason: collision with root package name */
    protected final C0111f f7653a = new C0111f();

    /* renamed from: b  reason: collision with root package name */
    protected final me.everything.android.ui.overscroll.adapters.b f7654b;

    /* renamed from: c  reason: collision with root package name */
    protected final d f7655c;

    /* renamed from: d  reason: collision with root package name */
    protected final g f7656d;

    /* renamed from: e  reason: collision with root package name */
    protected final b f7657e;

    /* renamed from: f  reason: collision with root package name */
    protected c f7658f;

    /* renamed from: g  reason: collision with root package name */
    protected c f7659g = new e.a();

    /* renamed from: h  reason: collision with root package name */
    protected d f7660h = new e.b();
    protected float i;

    /* compiled from: OverScrollBounceEffectDecoratorBase */
    protected interface c {
        int a();

        void a(c cVar);

        boolean a(MotionEvent motionEvent);

        boolean b(MotionEvent motionEvent);
    }

    /* access modifiers changed from: protected */
    public abstract e a();

    /* access modifiers changed from: protected */
    public abstract void a(View view, float f2);

    /* access modifiers changed from: protected */
    public abstract void a(View view, float f2, MotionEvent motionEvent);

    /* access modifiers changed from: protected */
    public abstract a b();

    /* compiled from: OverScrollBounceEffectDecoratorBase */
    protected static abstract class e {

        /* renamed from: a  reason: collision with root package name */
        public float f7671a;

        /* renamed from: b  reason: collision with root package name */
        public float f7672b;

        /* renamed from: c  reason: collision with root package name */
        public boolean f7673c;

        /* access modifiers changed from: protected */
        public abstract boolean a(View view, MotionEvent motionEvent);

        protected e() {
        }
    }

    /* renamed from: me.everything.android.ui.overscroll.f$f  reason: collision with other inner class name */
    /* compiled from: OverScrollBounceEffectDecoratorBase */
    protected static class C0111f {

        /* renamed from: a  reason: collision with root package name */
        protected int f7674a;

        /* renamed from: b  reason: collision with root package name */
        protected float f7675b;

        /* renamed from: c  reason: collision with root package name */
        protected boolean f7676c;

        protected C0111f() {
        }
    }

    /* compiled from: OverScrollBounceEffectDecoratorBase */
    protected static abstract class a {

        /* renamed from: a  reason: collision with root package name */
        public Property<View, Float> f7661a;

        /* renamed from: b  reason: collision with root package name */
        public float f7662b;

        /* renamed from: c  reason: collision with root package name */
        public float f7663c;

        /* access modifiers changed from: protected */
        public abstract void a(View view);

        protected a() {
        }
    }

    /* compiled from: OverScrollBounceEffectDecoratorBase */
    protected class d implements c {

        /* renamed from: a  reason: collision with root package name */
        final e f7669a;

        public d() {
            this.f7669a = f.this.a();
        }

        public int a() {
            return 0;
        }

        public boolean a(MotionEvent motionEvent) {
            if (!this.f7669a.a(f.this.f7654b.a(), motionEvent)) {
                return false;
            }
            if ((!f.this.f7654b.b() || !this.f7669a.f7673c) && (!f.this.f7654b.c() || this.f7669a.f7673c)) {
                return false;
            }
            f.this.f7653a.f7674a = motionEvent.getPointerId(0);
            f.this.f7653a.f7675b = this.f7669a.f7671a;
            f.this.f7653a.f7676c = this.f7669a.f7673c;
            f.this.a(f.this.f7656d);
            return f.this.f7656d.a(motionEvent);
        }

        public boolean b(MotionEvent motionEvent) {
            return false;
        }

        public void a(c cVar) {
            f.this.f7659g.a(f.this, cVar.a(), a());
        }
    }

    /* compiled from: OverScrollBounceEffectDecoratorBase */
    protected class g implements c {

        /* renamed from: a  reason: collision with root package name */
        protected final float f7677a;

        /* renamed from: b  reason: collision with root package name */
        protected final float f7678b;

        /* renamed from: c  reason: collision with root package name */
        final e f7679c;

        /* renamed from: d  reason: collision with root package name */
        int f7680d;

        public g(float f2, float f3) {
            this.f7679c = f.this.a();
            this.f7677a = f2;
            this.f7678b = f3;
        }

        public int a() {
            return this.f7680d;
        }

        public boolean a(MotionEvent motionEvent) {
            if (f.this.f7653a.f7674a != motionEvent.getPointerId(0)) {
                f.this.a(f.this.f7657e);
            } else {
                View a2 = f.this.f7654b.a();
                if (this.f7679c.a(a2, motionEvent)) {
                    float f2 = this.f7679c.f7672b / (this.f7679c.f7673c == f.this.f7653a.f7676c ? this.f7677a : this.f7678b);
                    float f3 = this.f7679c.f7671a + f2;
                    if ((!f.this.f7653a.f7676c || this.f7679c.f7673c || f3 > f.this.f7653a.f7675b) && (f.this.f7653a.f7676c || !this.f7679c.f7673c || f3 < f.this.f7653a.f7675b)) {
                        if (a2.getParent() != null) {
                            a2.getParent().requestDisallowInterceptTouchEvent(true);
                        }
                        long eventTime = motionEvent.getEventTime() - motionEvent.getHistoricalEventTime(0);
                        if (eventTime > 0) {
                            f.this.i = f2 / ((float) eventTime);
                        }
                        f.this.a(a2, f3);
                        f.this.f7660h.a(f.this, this.f7680d, f3);
                    } else {
                        f.this.a(a2, f.this.f7653a.f7675b, motionEvent);
                        f.this.f7660h.a(f.this, this.f7680d, 0.0f);
                        f.this.a(f.this.f7655c);
                    }
                }
            }
            return true;
        }

        public boolean b(MotionEvent motionEvent) {
            f.this.a(f.this.f7657e);
            return false;
        }

        public void a(c cVar) {
            this.f7680d = f.this.f7653a.f7676c ? 1 : 2;
            f.this.f7659g.a(f.this, cVar.a(), a());
        }
    }

    /* compiled from: OverScrollBounceEffectDecoratorBase */
    protected class b implements Animator.AnimatorListener, ValueAnimator.AnimatorUpdateListener, c {

        /* renamed from: a  reason: collision with root package name */
        protected final Interpolator f7664a = new DecelerateInterpolator();

        /* renamed from: b  reason: collision with root package name */
        protected final float f7665b;

        /* renamed from: c  reason: collision with root package name */
        protected final float f7666c;

        /* renamed from: d  reason: collision with root package name */
        protected final a f7667d;

        public b(float f2) {
            this.f7665b = f2;
            this.f7666c = 2.0f * f2;
            this.f7667d = f.this.b();
        }

        public int a() {
            return 3;
        }

        public void a(c cVar) {
            f.this.f7659g.a(f.this, cVar.a(), a());
            Animator b2 = b();
            b2.addListener(this);
            b2.start();
        }

        public boolean a(MotionEvent motionEvent) {
            return true;
        }

        public boolean b(MotionEvent motionEvent) {
            return true;
        }

        public void onAnimationEnd(Animator animator) {
            f.this.a(f.this.f7655c);
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            f.this.f7660h.a(f.this, 3, ((Float) valueAnimator.getAnimatedValue()).floatValue());
        }

        public void onAnimationStart(Animator animator) {
        }

        public void onAnimationCancel(Animator animator) {
        }

        public void onAnimationRepeat(Animator animator) {
        }

        /* access modifiers changed from: protected */
        public Animator b() {
            float f2 = 0.0f;
            View a2 = f.this.f7654b.a();
            this.f7667d.a(a2);
            if (f.this.i == 0.0f || ((f.this.i < 0.0f && f.this.f7653a.f7676c) || (f.this.i > 0.0f && !f.this.f7653a.f7676c))) {
                return a(this.f7667d.f7662b);
            }
            float f3 = (-f.this.i) / this.f7665b;
            if (f3 >= 0.0f) {
                f2 = f3;
            }
            float f4 = (((-f.this.i) * f.this.i) / this.f7666c) + this.f7667d.f7662b;
            ObjectAnimator a3 = a(a2, (int) f2, f4);
            ObjectAnimator a4 = a(f4);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playSequentially(a3, a4);
            return animatorSet;
        }

        /* access modifiers changed from: protected */
        public ObjectAnimator a(View view, int i, float f2) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, this.f7667d.f7661a, f2);
            ofFloat.setDuration((long) i);
            ofFloat.setInterpolator(this.f7664a);
            ofFloat.addUpdateListener(this);
            return ofFloat;
        }

        /* access modifiers changed from: protected */
        public ObjectAnimator a(float f2) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(f.this.f7654b.a(), this.f7667d.f7661a, f.this.f7653a.f7675b);
            ofFloat.setDuration((long) Math.max((int) ((Math.abs(f2) / this.f7667d.f7663c) * 800.0f), 200));
            ofFloat.setInterpolator(this.f7664a);
            ofFloat.addUpdateListener(this);
            return ofFloat;
        }
    }

    public f(me.everything.android.ui.overscroll.adapters.b bVar, float f2, float f3, float f4) {
        this.f7654b = bVar;
        this.f7657e = new b(f2);
        this.f7656d = new g(f3, f4);
        this.f7655c = new d();
        this.f7658f = this.f7655c;
        d();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 1:
            case 3:
                return this.f7658f.b(motionEvent);
            case 2:
                return this.f7658f.a(motionEvent);
            default:
                return false;
        }
    }

    public View c() {
        return this.f7654b.a();
    }

    /* access modifiers changed from: protected */
    public void a(c cVar) {
        c cVar2 = this.f7658f;
        this.f7658f = cVar;
        this.f7658f.a(cVar2);
    }

    /* access modifiers changed from: protected */
    public void d() {
        c().setOnTouchListener(this);
        c().setOverScrollMode(2);
    }
}
