package me.everything.android.ui.overscroll;

import android.view.MotionEvent;
import android.view.View;
import me.everything.android.ui.overscroll.f;

/* compiled from: VerticalOverScrollBounceEffectDecorator */
public class h extends f {

    /* compiled from: VerticalOverScrollBounceEffectDecorator */
    protected static class b extends f.e {
        protected b() {
        }

        public boolean a(View view, MotionEvent motionEvent) {
            boolean z = false;
            if (motionEvent.getHistorySize() == 0) {
                return false;
            }
            float y = motionEvent.getY(0) - motionEvent.getHistoricalY(0, 0);
            if (Math.abs(motionEvent.getX(0) - motionEvent.getHistoricalX(0, 0)) > Math.abs(y)) {
                return false;
            }
            this.f7671a = view.getTranslationY();
            this.f7672b = y;
            if (this.f7672b > 0.0f) {
                z = true;
            }
            this.f7673c = z;
            return true;
        }
    }

    /* compiled from: VerticalOverScrollBounceEffectDecorator */
    protected static class a extends f.a {
        public a() {
            this.f7661a = View.TRANSLATION_Y;
        }

        /* access modifiers changed from: protected */
        public void a(View view) {
            this.f7662b = view.getTranslationY();
            this.f7663c = (float) view.getHeight();
        }
    }

    public h(me.everything.android.ui.overscroll.adapters.b bVar) {
        this(bVar, 3.0f, 1.0f, -2.0f);
    }

    public h(me.everything.android.ui.overscroll.adapters.b bVar, float f2, float f3, float f4) {
        super(bVar, f4, f2, f3);
    }

    /* access modifiers changed from: protected */
    public f.e a() {
        return new b();
    }

    /* access modifiers changed from: protected */
    public f.a b() {
        return new a();
    }

    /* access modifiers changed from: protected */
    public void a(View view, float f2) {
        view.setTranslationY(f2);
    }

    /* access modifiers changed from: protected */
    public void a(View view, float f2, MotionEvent motionEvent) {
        view.setTranslationY(f2);
        motionEvent.offsetLocation(f2 - motionEvent.getY(0), 0.0f);
    }
}
