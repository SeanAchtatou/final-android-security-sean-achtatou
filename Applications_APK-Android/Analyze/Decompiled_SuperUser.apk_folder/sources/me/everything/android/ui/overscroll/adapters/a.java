package me.everything.android.ui.overscroll.adapters;

import android.view.View;
import android.widget.AbsListView;

/* compiled from: AbsListViewOverScrollDecorAdapter */
public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    protected final AbsListView f7646a;

    public a(AbsListView absListView) {
        this.f7646a = absListView;
    }

    public View a() {
        return this.f7646a;
    }

    public boolean b() {
        return this.f7646a.getChildCount() > 0 && !d();
    }

    public boolean c() {
        return this.f7646a.getChildCount() > 0 && !e();
    }

    public boolean d() {
        int top = this.f7646a.getChildAt(0).getTop();
        if (this.f7646a.getFirstVisiblePosition() > 0 || top < this.f7646a.getListPaddingTop()) {
            return true;
        }
        return false;
    }

    public boolean e() {
        int childCount = this.f7646a.getChildCount();
        return this.f7646a.getFirstVisiblePosition() + childCount < this.f7646a.getCount() || this.f7646a.getChildAt(childCount + -1).getBottom() > this.f7646a.getHeight() - this.f7646a.getListPaddingBottom();
    }
}
