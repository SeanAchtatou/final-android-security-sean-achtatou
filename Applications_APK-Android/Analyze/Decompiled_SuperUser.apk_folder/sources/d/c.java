package d;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/* compiled from: ImageUtils */
public class c {
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e2) {
            }
        }
    }

    private static boolean b(String str) {
        if (!a()) {
            return false;
        }
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        } else if (!file.isDirectory()) {
            file.mkdirs();
        }
        return true;
    }

    private static Bitmap c(String str) {
        FileInputStream fileInputStream;
        Throwable th;
        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inPurgeable = true;
        options.inInputShareable = true;
        try {
            fileInputStream = new FileInputStream(str);
            try {
                bitmap = BitmapFactory.decodeStream(fileInputStream, null, options);
                a(fileInputStream);
            } catch (FileNotFoundException e2) {
                a(fileInputStream);
                return bitmap;
            } catch (Throwable th2) {
                th = th2;
                a(fileInputStream);
                throw th;
            }
        } catch (FileNotFoundException e3) {
            fileInputStream = null;
            a(fileInputStream);
            return bitmap;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            fileInputStream = null;
            th = th4;
            a(fileInputStream);
            throw th;
        }
        return bitmap;
    }

    public static boolean a(String str, String str2) {
        return new File(str + str2).exists();
    }

    public static void b(String str, String str2) {
        new File(str, str2).delete();
    }

    public static Bitmap a(String str) {
        if (!a()) {
            return null;
        }
        return c(str);
    }

    /* JADX INFO: additional move instructions added (4) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v14, resolved type: java.io.FileInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: java.io.FileOutputStream} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r9, java.lang.String r10, android.graphics.Bitmap r11, long r12) {
        /*
            r2 = 0
            r0 = 0
            boolean r1 = a()
            if (r1 != 0) goto L_0x0009
        L_0x0008:
            return r0
        L_0x0009:
            b(r9)
            java.io.File r4 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r1 = r1.toString()
            r4.<init>(r1)
            if (r11 == 0) goto L_0x0008
            boolean r1 = r4.exists()
            if (r1 == 0) goto L_0x002d
            r4.delete()
        L_0x002d:
            r4.createNewFile()     // Catch:{ IOException -> 0x0069, all -> 0x0072 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0069, all -> 0x0072 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0069, all -> 0x0072 }
            android.graphics.Bitmap$CompressFormat r1 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ IOException -> 0x0080, all -> 0x007b }
            r5 = 100
            r11.compress(r1, r5, r3)     // Catch:{ IOException -> 0x0080, all -> 0x007b }
            r3.flush()     // Catch:{ IOException -> 0x0080, all -> 0x007b }
            r3.close()     // Catch:{ IOException -> 0x0080, all -> 0x007b }
            if (r4 == 0) goto L_0x0087
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0080, all -> 0x007b }
            r1.<init>(r4)     // Catch:{ IOException -> 0x0080, all -> 0x007b }
            int r2 = r1.available()     // Catch:{ IOException -> 0x0084, all -> 0x007d }
            if (r2 != 0) goto L_0x0058
            boolean r2 = r4.exists()     // Catch:{ IOException -> 0x0084, all -> 0x007d }
            if (r2 == 0) goto L_0x0058
            r4.delete()     // Catch:{ IOException -> 0x0084, all -> 0x007d }
        L_0x0058:
            r6 = 0
            int r2 = (r12 > r6 ? 1 : (r12 == r6 ? 0 : -1))
            if (r2 <= 0) goto L_0x0061
            r4.setLastModified(r12)     // Catch:{ IOException -> 0x0084, all -> 0x007d }
        L_0x0061:
            r0 = 1
            a(r3)
            a(r1)
            goto L_0x0008
        L_0x0069:
            r1 = move-exception
            r1 = r2
        L_0x006b:
            a(r2)
            a(r1)
            goto L_0x0008
        L_0x0072:
            r0 = move-exception
            r3 = r2
        L_0x0074:
            a(r3)
            a(r2)
            throw r0
        L_0x007b:
            r0 = move-exception
            goto L_0x0074
        L_0x007d:
            r0 = move-exception
            r2 = r1
            goto L_0x0074
        L_0x0080:
            r1 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x006b
        L_0x0084:
            r2 = move-exception
            r2 = r3
            goto L_0x006b
        L_0x0087:
            r1 = r2
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: d.c.a(java.lang.String, java.lang.String, android.graphics.Bitmap, long):boolean");
    }

    public static boolean a() {
        return Environment.getExternalStorageState().equals("mounted");
    }
}
