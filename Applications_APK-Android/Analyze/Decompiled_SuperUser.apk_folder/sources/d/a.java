package d;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import java.io.FilterInputStream;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.regex.Pattern;

/* compiled from: ImageDownloader */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final String f6957a = (Environment.getExternalStorageDirectory().getPath() + "/duapps/");

    /* renamed from: b  reason: collision with root package name */
    public static final String f6958b = (f6957a + "img_download/");

    /* renamed from: e  reason: collision with root package name */
    private static a f6959e = null;

    /* renamed from: f  reason: collision with root package name */
    private static final HashMap f6960f = new LinkedHashMap(5, 0.75f, true) {
        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry entry) {
            if (size() <= 10) {
                return false;
            }
            a.f6961g.put(entry.getKey(), new SoftReference(entry.getValue()));
            return true;
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public static final ConcurrentHashMap f6961g = new ConcurrentHashMap(5);
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Thread f6962c = Thread.currentThread();

    /* renamed from: d  reason: collision with root package name */
    private String f6963d = f6958b;

    /* renamed from: h  reason: collision with root package name */
    private final Handler f6964h = new Handler();
    private final Runnable i = new Runnable() {
        public void run() {
            a.this.b();
        }
    };
    private b j = new b() {
        public void a(String str, View view) {
        }

        public void a(String str, View view, int i) {
        }

        public void a(String str, View view, Bitmap bitmap) {
        }
    };

    private a() {
    }

    public static a a() {
        if (f6959e == null) {
            synchronized (a.class) {
                if (f6959e == null) {
                    f6959e = new a();
                }
            }
        }
        return f6959e;
    }

    public void a(String str, ImageView imageView) {
        a(str, imageView, this.j);
    }

    public void a(String str, ImageView imageView, b bVar) {
        boolean z = true;
        if (bVar == null) {
            bVar = this.j;
        }
        bVar.a(str, imageView);
        if (TextUtils.isEmpty(str)) {
            bVar.a(str, imageView, 2);
        } else if (imageView == null) {
            bVar.a(str, imageView, 3);
        } else if (Build.VERSION.SDK_INT < 11) {
            bVar.a(str, imageView, 1);
        } else {
            d();
            Bitmap a2 = a(str);
            if (a2 == null) {
                String b2 = b(str);
                if (c.a(this.f6963d, b2)) {
                    a2 = c.a(this.f6963d + b2);
                    if (a2 == null) {
                        c.b(this.f6963d, b2);
                    } else {
                        z = false;
                    }
                }
                if (z) {
                    b(str, imageView, bVar);
                    return;
                }
                a(str, a2);
                imageView.setImageBitmap(a2);
                imageView.setBackgroundResource(17170445);
                imageView.setVisibility(0);
                bVar.a(str, imageView, a2);
                return;
            }
            imageView.setImageBitmap(a2);
            imageView.setBackgroundResource(17170445);
            imageView.setVisibility(0);
            bVar.a(str, imageView, a2);
        }
    }

    private void b(String str, ImageView imageView, b bVar) {
        C0096a aVar;
        if (b(str, imageView)) {
            try {
                aVar = new C0096a(imageView, bVar);
            } catch (Throwable th) {
                aVar = null;
            }
            if (aVar == null) {
                bVar.a(str, imageView, 5);
                return;
            }
            b bVar2 = new b(null, aVar);
            if (imageView != null) {
                imageView.setImageDrawable(bVar2);
            }
            ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) AsyncTask.THREAD_POOL_EXECUTOR;
            threadPoolExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardOldestPolicy());
            aVar.executeOnExecutor(threadPoolExecutor, str);
            return;
        }
        bVar.a(str, imageView, 4);
    }

    private static boolean b(String str, ImageView imageView) {
        C0096a b2 = b(imageView);
        if (b2 == null) {
            return true;
        }
        String a2 = b2.f6968b;
        if (a2 != null && a2.equals(str)) {
            return false;
        }
        b2.cancel(true);
        return true;
    }

    /* access modifiers changed from: private */
    public static C0096a b(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof b) {
                return ((b) drawable).a();
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x007a A[SYNTHETIC, Splitter:B:38:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0084 A[SYNTHETIC, Splitter:B:44:0x0084] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x008f A[SYNTHETIC, Splitter:B:50:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:68:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:69:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap a(java.lang.String r8, d.b r9) {
        /*
            r7 = this;
            r0 = 0
            if (r8 == 0) goto L_0x0044
            java.lang.String r1 = ""
            boolean r1 = r1.equals(r8)
            if (r1 != 0) goto L_0x0044
            java.net.URL r1 = new java.net.URL     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0076, SecurityException -> 0x0080, all -> 0x008a }
            r1.<init>(r8)     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0076, SecurityException -> 0x0080, all -> 0x008a }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0076, SecurityException -> 0x0080, all -> 0x008a }
            r2 = 10000(0x2710, float:1.4013E-41)
            r1.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0076, SecurityException -> 0x0080, all -> 0x008a }
            r2 = 10000(0x2710, float:1.4013E-41)
            r1.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0076, SecurityException -> 0x0080, all -> 0x008a }
            r1.connect()     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0076, SecurityException -> 0x0080, all -> 0x008a }
            java.io.InputStream r2 = r1.getInputStream()     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0076, SecurityException -> 0x0080, all -> 0x008a }
            java.lang.String r3 = b(r8)     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
            java.net.URL r4 = r1.getURL()     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
            if (r4 == 0) goto L_0x0045
            java.net.URL r4 = r1.getURL()     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
            java.lang.String r4 = r4.toString()     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
            java.lang.String r5 = "dashi_default_head_middle.gif"
            boolean r4 = r4.endsWith(r5)     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
            if (r4 == 0) goto L_0x0045
            if (r2 == 0) goto L_0x0044
            r2.close()     // Catch:{ IOException -> 0x0093, NumberFormatException -> 0x0095 }
        L_0x0044:
            return r0
        L_0x0045:
            long r4 = r1.getLastModified()     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
            if (r2 == 0) goto L_0x0064
            d.a$c r1 = new d.a$c     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
            r1.<init>(r2)     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
            if (r1 == 0) goto L_0x0064
            if (r3 == 0) goto L_0x005d
            java.lang.String r6 = r7.f6963d     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
            d.c.a(r6, r3, r1, r4)     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x00ab, SecurityException -> 0x00a9, all -> 0x00a7 }
        L_0x005d:
            if (r2 == 0) goto L_0x0062
            r2.close()     // Catch:{ IOException -> 0x0097, NumberFormatException -> 0x0099 }
        L_0x0062:
            r0 = r1
            goto L_0x0044
        L_0x0064:
            if (r2 == 0) goto L_0x0044
            r2.close()     // Catch:{ IOException -> 0x006a, NumberFormatException -> 0x009b }
            goto L_0x0044
        L_0x006a:
            r1 = move-exception
            goto L_0x0044
        L_0x006c:
            r1 = move-exception
            r1 = r0
        L_0x006e:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ IOException -> 0x0074, NumberFormatException -> 0x009d }
            goto L_0x0044
        L_0x0074:
            r1 = move-exception
            goto L_0x0044
        L_0x0076:
            r1 = move-exception
            r2 = r0
        L_0x0078:
            if (r2 == 0) goto L_0x0044
            r2.close()     // Catch:{ IOException -> 0x007e, NumberFormatException -> 0x009f }
            goto L_0x0044
        L_0x007e:
            r1 = move-exception
            goto L_0x0044
        L_0x0080:
            r1 = move-exception
            r2 = r0
        L_0x0082:
            if (r2 == 0) goto L_0x0044
            r2.close()     // Catch:{ IOException -> 0x0088, NumberFormatException -> 0x00a1 }
            goto L_0x0044
        L_0x0088:
            r1 = move-exception
            goto L_0x0044
        L_0x008a:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x008d:
            if (r2 == 0) goto L_0x0092
            r2.close()     // Catch:{ IOException -> 0x00a3, NumberFormatException -> 0x00a5 }
        L_0x0092:
            throw r0
        L_0x0093:
            r1 = move-exception
            goto L_0x0044
        L_0x0095:
            r1 = move-exception
            goto L_0x0044
        L_0x0097:
            r0 = move-exception
            goto L_0x0062
        L_0x0099:
            r0 = move-exception
            goto L_0x0062
        L_0x009b:
            r1 = move-exception
            goto L_0x0044
        L_0x009d:
            r1 = move-exception
            goto L_0x0044
        L_0x009f:
            r1 = move-exception
            goto L_0x0044
        L_0x00a1:
            r1 = move-exception
            goto L_0x0044
        L_0x00a3:
            r1 = move-exception
            goto L_0x0092
        L_0x00a5:
            r1 = move-exception
            goto L_0x0092
        L_0x00a7:
            r0 = move-exception
            goto L_0x008d
        L_0x00a9:
            r1 = move-exception
            goto L_0x0082
        L_0x00ab:
            r1 = move-exception
            goto L_0x0078
        L_0x00ad:
            r1 = move-exception
            r1 = r2
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: d.a.a(java.lang.String, d.b):android.graphics.Bitmap");
    }

    /* compiled from: ImageDownloader */
    public static class c extends FilterInputStream {
        public c(InputStream inputStream) {
            super(inputStream);
        }

        public long skip(long j) {
            long j2 = 0;
            while (j2 < j) {
                long skip = this.in.skip(j - j2);
                if (skip == 0) {
                    if (read() < 0) {
                        break;
                    }
                    skip = 1;
                }
                j2 = skip + j2;
            }
            return j2;
        }
    }

    /* renamed from: d.a$a  reason: collision with other inner class name */
    /* compiled from: ImageDownloader */
    class C0096a extends AsyncTask {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public String f6968b;

        /* renamed from: c  reason: collision with root package name */
        private final WeakReference f6969c;

        /* renamed from: d  reason: collision with root package name */
        private b f6970d;

        public C0096a(ImageView imageView, b bVar) {
            this.f6969c = new WeakReference(imageView);
            this.f6970d = bVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Bitmap doInBackground(String... strArr) {
            this.f6968b = strArr[0];
            return a.this.a(this.f6968b, this.f6970d);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Bitmap bitmap) {
            C0096a a2;
            ImageView imageView = null;
            if (isCancelled()) {
                bitmap = null;
            }
            a.this.a(this.f6968b, bitmap);
            if (this.f6969c != null) {
                imageView = (ImageView) this.f6969c.get();
            }
            if (bitmap != null) {
                this.f6970d.a(this.f6968b, imageView, bitmap);
            } else {
                this.f6970d.a(this.f6968b, imageView, 6);
            }
            if (imageView != null && (a2 = a.b(imageView)) != null && this == a2 && a.this.f6962c != null && a.this.f6962c == Thread.currentThread() && bitmap != null) {
                imageView.setImageBitmap(bitmap);
                imageView.setBackgroundResource(17170445);
                imageView.setVisibility(0);
            }
        }
    }

    /* compiled from: ImageDownloader */
    static class b extends BitmapDrawable {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference f6971a;

        public b(Bitmap bitmap, C0096a aVar) {
            super(bitmap);
            this.f6971a = new WeakReference(aVar);
        }

        public C0096a a() {
            return (C0096a) this.f6971a.get();
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, Bitmap bitmap) {
        if (bitmap != null) {
            synchronized (f6960f) {
                f6960f.put(str, bitmap);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0024, code lost:
        r0 = (android.graphics.Bitmap) r0.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002a, code lost:
        if (r0 != null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        d.a.f6961g.remove(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0031, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        r0 = (java.lang.ref.SoftReference) d.a.f6961g.get(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        if (r0 == null) goto L_0x0031;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(java.lang.String r3) {
        /*
            java.util.HashMap r1 = d.a.f6960f
            monitor-enter(r1)
            java.util.HashMap r0 = d.a.f6960f     // Catch:{ all -> 0x0033 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x0033 }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x0019
            java.util.HashMap r2 = d.a.f6960f     // Catch:{ all -> 0x0033 }
            r2.remove(r3)     // Catch:{ all -> 0x0033 }
            java.util.HashMap r2 = d.a.f6960f     // Catch:{ all -> 0x0033 }
            r2.put(r3, r0)     // Catch:{ all -> 0x0033 }
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
        L_0x0018:
            return r0
        L_0x0019:
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            java.util.concurrent.ConcurrentHashMap r0 = d.a.f6961g
            java.lang.Object r0 = r0.get(r3)
            java.lang.ref.SoftReference r0 = (java.lang.ref.SoftReference) r0
            if (r0 == 0) goto L_0x0031
            java.lang.Object r0 = r0.get()
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            if (r0 != 0) goto L_0x0018
            java.util.concurrent.ConcurrentHashMap r0 = d.a.f6961g
            r0.remove(r3)
        L_0x0031:
            r0 = 0
            goto L_0x0018
        L_0x0033:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: d.a.a(java.lang.String):android.graphics.Bitmap");
    }

    public void b() {
        f6960f.clear();
        f6961g.clear();
    }

    private void d() {
        this.f6964h.removeCallbacks(this.i);
        this.f6964h.postDelayed(this.i, 10000);
    }

    public static String b(String str) {
        return Pattern.compile("[^a-zA-Z0-9]").matcher(str).replaceAll("").trim();
    }
}
