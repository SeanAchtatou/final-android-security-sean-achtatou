package okio;

import java.util.zip.Deflater;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;

/* compiled from: DeflaterSink */
public final class f implements p {

    /* renamed from: a  reason: collision with root package name */
    private final d f8204a;

    /* renamed from: b  reason: collision with root package name */
    private final Deflater f8205b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f8206c;

    public f(p pVar, Deflater deflater) {
        this(k.a(pVar), deflater);
    }

    f(d dVar, Deflater deflater) {
        if (dVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (deflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f8204a = dVar;
            this.f8205b = deflater;
        }
    }

    public void a_(c cVar, long j) {
        s.a(cVar.f8203b, 0, j);
        while (j > 0) {
            n nVar = cVar.f8202a;
            int min = (int) Math.min(j, (long) (nVar.f8232c - nVar.f8231b));
            this.f8205b.setInput(nVar.f8230a, nVar.f8231b, min);
            a(false);
            cVar.f8203b -= (long) min;
            nVar.f8231b += min;
            if (nVar.f8231b == nVar.f8232c) {
                cVar.f8202a = nVar.a();
                o.a(nVar);
            }
            j -= (long) min;
        }
    }

    @IgnoreJRERequirement
    private void a(boolean z) {
        n e2;
        int deflate;
        c c2 = this.f8204a.c();
        while (true) {
            e2 = c2.e(1);
            if (z) {
                deflate = this.f8205b.deflate(e2.f8230a, e2.f8232c, 8192 - e2.f8232c, 2);
            } else {
                deflate = this.f8205b.deflate(e2.f8230a, e2.f8232c, 8192 - e2.f8232c);
            }
            if (deflate > 0) {
                e2.f8232c += deflate;
                c2.f8203b += (long) deflate;
                this.f8204a.u();
            } else if (this.f8205b.needsInput()) {
                break;
            }
        }
        if (e2.f8231b == e2.f8232c) {
            c2.f8202a = e2.a();
            o.a(e2);
        }
    }

    public void flush() {
        a(true);
        this.f8204a.flush();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f8205b.finish();
        a(false);
    }

    public void close() {
        if (!this.f8206c) {
            Throwable th = null;
            try {
                b();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f8205b.end();
                th = th;
            } catch (Throwable th3) {
                th = th3;
                if (th != null) {
                    th = th;
                }
            }
            try {
                this.f8204a.close();
            } catch (Throwable th4) {
                if (th == null) {
                    th = th4;
                }
            }
            this.f8206c = true;
            if (th != null) {
                s.a(th);
            }
        }
    }

    public r a() {
        return this.f8204a.a();
    }

    public String toString() {
        return "DeflaterSink(" + this.f8204a + ")";
    }
}
