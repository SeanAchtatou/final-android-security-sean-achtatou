package okio;

/* compiled from: Segment */
final class n {

    /* renamed from: a  reason: collision with root package name */
    final byte[] f8230a;

    /* renamed from: b  reason: collision with root package name */
    int f8231b;

    /* renamed from: c  reason: collision with root package name */
    int f8232c;

    /* renamed from: d  reason: collision with root package name */
    boolean f8233d;

    /* renamed from: e  reason: collision with root package name */
    boolean f8234e;

    /* renamed from: f  reason: collision with root package name */
    n f8235f;

    /* renamed from: g  reason: collision with root package name */
    n f8236g;

    n() {
        this.f8230a = new byte[8192];
        this.f8234e = true;
        this.f8233d = false;
    }

    n(n nVar) {
        this(nVar.f8230a, nVar.f8231b, nVar.f8232c);
        nVar.f8233d = true;
    }

    n(byte[] bArr, int i, int i2) {
        this.f8230a = bArr;
        this.f8231b = i;
        this.f8232c = i2;
        this.f8234e = false;
        this.f8233d = true;
    }

    public n a() {
        n nVar = this.f8235f != this ? this.f8235f : null;
        this.f8236g.f8235f = this.f8235f;
        this.f8235f.f8236g = this.f8236g;
        this.f8235f = null;
        this.f8236g = null;
        return nVar;
    }

    public n a(n nVar) {
        nVar.f8236g = this;
        nVar.f8235f = this.f8235f;
        this.f8235f.f8236g = nVar;
        this.f8235f = nVar;
        return nVar;
    }

    public n a(int i) {
        n a2;
        if (i <= 0 || i > this.f8232c - this.f8231b) {
            throw new IllegalArgumentException();
        }
        if (i >= 1024) {
            a2 = new n(this);
        } else {
            a2 = o.a();
            System.arraycopy(this.f8230a, this.f8231b, a2.f8230a, 0, i);
        }
        a2.f8232c = a2.f8231b + i;
        this.f8231b += i;
        this.f8236g.a(a2);
        return a2;
    }

    public void b() {
        if (this.f8236g == this) {
            throw new IllegalStateException();
        } else if (this.f8236g.f8234e) {
            int i = this.f8232c - this.f8231b;
            if (i <= (this.f8236g.f8233d ? 0 : this.f8236g.f8231b) + (8192 - this.f8236g.f8232c)) {
                a(this.f8236g, i);
                a();
                o.a(this);
            }
        }
    }

    public void a(n nVar, int i) {
        if (!nVar.f8234e) {
            throw new IllegalArgumentException();
        }
        if (nVar.f8232c + i > 8192) {
            if (nVar.f8233d) {
                throw new IllegalArgumentException();
            } else if ((nVar.f8232c + i) - nVar.f8231b > 8192) {
                throw new IllegalArgumentException();
            } else {
                System.arraycopy(nVar.f8230a, nVar.f8231b, nVar.f8230a, 0, nVar.f8232c - nVar.f8231b);
                nVar.f8232c -= nVar.f8231b;
                nVar.f8231b = 0;
            }
        }
        System.arraycopy(this.f8230a, this.f8231b, nVar.f8230a, nVar.f8232c, i);
        nVar.f8232c += i;
        this.f8231b += i;
    }
}
