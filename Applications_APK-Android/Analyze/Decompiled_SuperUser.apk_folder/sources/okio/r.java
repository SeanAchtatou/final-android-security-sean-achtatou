package okio;

import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

/* compiled from: Timeout */
public class r {

    /* renamed from: b  reason: collision with root package name */
    public static final r f8239b = new r() {
        public r a(long j, TimeUnit timeUnit) {
            return this;
        }

        public r a(long j) {
            return this;
        }

        public void g() {
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private boolean f8240a;

    /* renamed from: c  reason: collision with root package name */
    private long f8241c;

    /* renamed from: d  reason: collision with root package name */
    private long f8242d;

    public r a(long j, TimeUnit timeUnit) {
        if (j < 0) {
            throw new IllegalArgumentException("timeout < 0: " + j);
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            this.f8242d = timeUnit.toNanos(j);
            return this;
        }
    }

    public long c_() {
        return this.f8242d;
    }

    public boolean d_() {
        return this.f8240a;
    }

    public long d() {
        if (this.f8240a) {
            return this.f8241c;
        }
        throw new IllegalStateException("No deadline");
    }

    public r a(long j) {
        this.f8240a = true;
        this.f8241c = j;
        return this;
    }

    public r e_() {
        this.f8242d = 0;
        return this;
    }

    public r f_() {
        this.f8240a = false;
        return this;
    }

    public void g() {
        if (Thread.interrupted()) {
            throw new InterruptedIOException("thread interrupted");
        } else if (this.f8240a && this.f8241c - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }
}
