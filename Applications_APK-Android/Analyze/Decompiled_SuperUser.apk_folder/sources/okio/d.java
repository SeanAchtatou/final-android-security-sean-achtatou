package okio;

/* compiled from: BufferedSink */
public interface d extends p {
    long a(q qVar);

    d b(String str);

    d b(ByteString byteString);

    c c();

    d c(byte[] bArr);

    d c(byte[] bArr, int i, int i2);

    d e();

    void flush();

    d g(int i);

    d h(int i);

    d i(int i);

    d j(long j);

    d k(long j);

    d u();
}
