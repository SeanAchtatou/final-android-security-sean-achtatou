package okio;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

/* compiled from: AsyncTimeout */
public class a extends r {

    /* renamed from: a  reason: collision with root package name */
    private static final long f8189a = TimeUnit.SECONDS.toMillis(60);

    /* renamed from: c  reason: collision with root package name */
    private static final long f8190c = TimeUnit.MILLISECONDS.toNanos(f8189a);
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public static a f8191d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f8192e;

    /* renamed from: f  reason: collision with root package name */
    private a f8193f;

    /* renamed from: g  reason: collision with root package name */
    private long f8194g;

    public final void c() {
        if (this.f8192e) {
            throw new IllegalStateException("Unbalanced enter/exit");
        }
        long c_ = c_();
        boolean d_ = d_();
        if (c_ != 0 || d_) {
            this.f8192e = true;
            a(this, c_, d_);
        }
    }

    private static synchronized void a(a aVar, long j, boolean z) {
        synchronized (a.class) {
            if (f8191d == null) {
                f8191d = new a();
                new C0115a().start();
            }
            long nanoTime = System.nanoTime();
            if (j != 0 && z) {
                aVar.f8194g = Math.min(j, aVar.d() - nanoTime) + nanoTime;
            } else if (j != 0) {
                aVar.f8194g = nanoTime + j;
            } else if (z) {
                aVar.f8194g = aVar.d();
            } else {
                throw new AssertionError();
            }
            long b2 = aVar.b(nanoTime);
            a aVar2 = f8191d;
            while (aVar2.f8193f != null && b2 >= aVar2.f8193f.b(nanoTime)) {
                aVar2 = aVar2.f8193f;
            }
            aVar.f8193f = aVar2.f8193f;
            aVar2.f8193f = aVar;
            if (aVar2 == f8191d) {
                a.class.notify();
            }
        }
    }

    public final boolean b_() {
        if (!this.f8192e) {
            return false;
        }
        this.f8192e = false;
        return b(this);
    }

    private static synchronized boolean b(a aVar) {
        boolean z;
        synchronized (a.class) {
            a aVar2 = f8191d;
            while (true) {
                if (aVar2 == null) {
                    z = true;
                    break;
                } else if (aVar2.f8193f == aVar) {
                    aVar2.f8193f = aVar.f8193f;
                    aVar.f8193f = null;
                    z = false;
                    break;
                } else {
                    aVar2 = aVar2.f8193f;
                }
            }
        }
        return z;
    }

    private long b(long j) {
        return this.f8194g - j;
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public final p a(final p pVar) {
        return new p() {
            public void a_(c cVar, long j) {
                s.a(cVar.f8203b, 0, j);
                long j2 = j;
                while (j2 > 0) {
                    n nVar = cVar.f8202a;
                    long j3 = 0;
                    while (true) {
                        if (j3 >= 65536) {
                            break;
                        }
                        long j4 = ((long) (cVar.f8202a.f8232c - cVar.f8202a.f8231b)) + j3;
                        if (j4 >= j2) {
                            j3 = j2;
                            break;
                        } else {
                            nVar = nVar.f8235f;
                            j3 = j4;
                        }
                    }
                    a.this.c();
                    try {
                        pVar.a_(cVar, j3);
                        j2 -= j3;
                        a.this.a(true);
                    } catch (IOException e2) {
                        throw a.this.b(e2);
                    } catch (Throwable th) {
                        a.this.a(false);
                        throw th;
                    }
                }
            }

            public void flush() {
                a.this.c();
                try {
                    pVar.flush();
                    a.this.a(true);
                } catch (IOException e2) {
                    throw a.this.b(e2);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public void close() {
                a.this.c();
                try {
                    pVar.close();
                    a.this.a(true);
                } catch (IOException e2) {
                    throw a.this.b(e2);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public r a() {
                return a.this;
            }

            public String toString() {
                return "AsyncTimeout.sink(" + pVar + ")";
            }
        };
    }

    public final q a(final q qVar) {
        return new q() {
            public long a(c cVar, long j) {
                a.this.c();
                try {
                    long a2 = qVar.a(cVar, j);
                    a.this.a(true);
                    return a2;
                } catch (IOException e2) {
                    throw a.this.b(e2);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public void close() {
                try {
                    qVar.close();
                    a.this.a(true);
                } catch (IOException e2) {
                    throw a.this.b(e2);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public r a() {
                return a.this;
            }

            public String toString() {
                return "AsyncTimeout.source(" + qVar + ")";
            }
        };
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        if (b_() && z) {
            throw a((IOException) null);
        }
    }

    /* access modifiers changed from: package-private */
    public final IOException b(IOException iOException) {
        return !b_() ? iOException : a(iOException);
    }

    /* access modifiers changed from: protected */
    public IOException a(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    /* renamed from: okio.a$a  reason: collision with other inner class name */
    /* compiled from: AsyncTimeout */
    private static final class C0115a extends Thread {
        public C0115a() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            r0.a();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r3 = this;
            L_0x0000:
                java.lang.Class<okio.a> r1 = okio.a.class
                monitor-enter(r1)     // Catch:{ InterruptedException -> 0x000e }
                okio.a r0 = okio.a.e()     // Catch:{ all -> 0x000b }
                if (r0 != 0) goto L_0x0010
                monitor-exit(r1)     // Catch:{ all -> 0x000b }
                goto L_0x0000
            L_0x000b:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x000b }
                throw r0     // Catch:{ InterruptedException -> 0x000e }
            L_0x000e:
                r0 = move-exception
                goto L_0x0000
            L_0x0010:
                okio.a r2 = okio.a.f8191d     // Catch:{ all -> 0x000b }
                if (r0 != r2) goto L_0x001c
                r0 = 0
                okio.a unused = okio.a.f8191d = r0     // Catch:{ all -> 0x000b }
                monitor-exit(r1)     // Catch:{ all -> 0x000b }
                return
            L_0x001c:
                monitor-exit(r1)     // Catch:{ all -> 0x000b }
                r0.a()     // Catch:{ InterruptedException -> 0x000e }
                goto L_0x0000
            */
            throw new UnsupportedOperationException("Method not decompiled: okio.a.C0115a.run():void");
        }
    }

    static a e() {
        a aVar = f8191d.f8193f;
        if (aVar == null) {
            long nanoTime = System.nanoTime();
            a.class.wait(f8189a);
            if (f8191d.f8193f != null || System.nanoTime() - nanoTime < f8190c) {
                return null;
            }
            return f8191d;
        }
        long b2 = aVar.b(System.nanoTime());
        if (b2 > 0) {
            long j = b2 / 1000000;
            a.class.wait(j, (int) (b2 - (1000000 * j)));
            return null;
        }
        f8191d.f8193f = aVar.f8193f;
        aVar.f8193f = null;
        return aVar;
    }
}
