package okio;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

/* compiled from: GzipSource */
public final class i implements q {

    /* renamed from: a  reason: collision with root package name */
    private int f8209a = 0;

    /* renamed from: b  reason: collision with root package name */
    private final e f8210b;

    /* renamed from: c  reason: collision with root package name */
    private final Inflater f8211c;

    /* renamed from: d  reason: collision with root package name */
    private final j f8212d;

    /* renamed from: e  reason: collision with root package name */
    private final CRC32 f8213e = new CRC32();

    public i(q qVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.f8211c = new Inflater(true);
        this.f8210b = k.a(qVar);
        this.f8212d = new j(this.f8210b, this.f8211c);
    }

    public long a(c cVar, long j) {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (j == 0) {
            return 0;
        } else {
            if (this.f8209a == 0) {
                b();
                this.f8209a = 1;
            }
            if (this.f8209a == 1) {
                long j2 = cVar.f8203b;
                long a2 = this.f8212d.a(cVar, j);
                if (a2 != -1) {
                    a(cVar, j2, a2);
                    return a2;
                }
                this.f8209a = 2;
            }
            if (this.f8209a == 2) {
                c();
                this.f8209a = 3;
                if (!this.f8210b.f()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    private void b() {
        boolean z;
        this.f8210b.a(10);
        byte b2 = this.f8210b.c().b(3L);
        if (((b2 >> 1) & 1) == 1) {
            z = true;
        } else {
            z = false;
        }
        if (z) {
            a(this.f8210b.c(), 0, 10);
        }
        a("ID1ID2", 8075, this.f8210b.i());
        this.f8210b.g(8);
        if (((b2 >> 2) & 1) == 1) {
            this.f8210b.a(2);
            if (z) {
                a(this.f8210b.c(), 0, 2);
            }
            short k = this.f8210b.c().k();
            this.f8210b.a((long) k);
            if (z) {
                a(this.f8210b.c(), 0, (long) k);
            }
            this.f8210b.g((long) k);
        }
        if (((b2 >> 3) & 1) == 1) {
            long a2 = this.f8210b.a((byte) 0);
            if (a2 == -1) {
                throw new EOFException();
            }
            if (z) {
                a(this.f8210b.c(), 0, 1 + a2);
            }
            this.f8210b.g(1 + a2);
        }
        if (((b2 >> 4) & 1) == 1) {
            long a3 = this.f8210b.a((byte) 0);
            if (a3 == -1) {
                throw new EOFException();
            }
            if (z) {
                a(this.f8210b.c(), 0, 1 + a3);
            }
            this.f8210b.g(1 + a3);
        }
        if (z) {
            a("FHCRC", this.f8210b.k(), (short) ((int) this.f8213e.getValue()));
            this.f8213e.reset();
        }
    }

    private void c() {
        a("CRC", this.f8210b.l(), (int) this.f8213e.getValue());
        a("ISIZE", this.f8210b.l(), (int) this.f8211c.getBytesWritten());
    }

    public r a() {
        return this.f8210b.a();
    }

    public void close() {
        this.f8212d.close();
    }

    private void a(c cVar, long j, long j2) {
        n nVar = cVar.f8202a;
        while (j >= ((long) (nVar.f8232c - nVar.f8231b))) {
            j -= (long) (nVar.f8232c - nVar.f8231b);
            nVar = nVar.f8235f;
        }
        while (j2 > 0) {
            int i = (int) (((long) nVar.f8231b) + j);
            int min = (int) Math.min((long) (nVar.f8232c - i), j2);
            this.f8213e.update(nVar.f8230a, i, min);
            j2 -= (long) min;
            nVar = nVar.f8235f;
            j = 0;
        }
    }

    private void a(String str, int i, int i2) {
        if (i2 != i) {
            throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", str, Integer.valueOf(i2), Integer.valueOf(i)));
        }
    }
}
