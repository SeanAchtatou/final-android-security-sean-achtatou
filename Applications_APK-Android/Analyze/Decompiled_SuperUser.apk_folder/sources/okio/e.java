package okio;

import java.nio.charset.Charset;

/* compiled from: BufferedSource */
public interface e extends q {
    long a(byte b2);

    String a(Charset charset);

    void a(long j);

    boolean a(long j, ByteString byteString);

    ByteString c(long j);

    c c();

    boolean f();

    byte[] f(long j);

    void g(long j);

    byte h();

    short i();

    int j();

    short k();

    int l();

    long m();

    String p();

    byte[] q();
}
