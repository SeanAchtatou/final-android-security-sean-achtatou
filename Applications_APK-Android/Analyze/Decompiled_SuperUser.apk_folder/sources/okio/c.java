package okio;

import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.os.VUserInfo;
import java.io.EOFException;
import java.nio.charset.Charset;

/* compiled from: Buffer */
public final class c implements Cloneable, d, e {

    /* renamed from: c  reason: collision with root package name */
    private static final byte[] f8201c = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: a  reason: collision with root package name */
    n f8202a;

    /* renamed from: b  reason: collision with root package name */
    long f8203b;

    public long b() {
        return this.f8203b;
    }

    public c c() {
        return this;
    }

    /* renamed from: d */
    public c u() {
        return this;
    }

    public d e() {
        return this;
    }

    public boolean f() {
        return this.f8203b == 0;
    }

    public void a(long j) {
        if (this.f8203b < j) {
            throw new EOFException();
        }
    }

    public c a(c cVar, long j, long j2) {
        if (cVar == null) {
            throw new IllegalArgumentException("out == null");
        }
        s.a(this.f8203b, j, j2);
        if (j2 != 0) {
            cVar.f8203b += j2;
            n nVar = this.f8202a;
            while (j >= ((long) (nVar.f8232c - nVar.f8231b))) {
                j -= (long) (nVar.f8232c - nVar.f8231b);
                nVar = nVar.f8235f;
            }
            while (j2 > 0) {
                n nVar2 = new n(nVar);
                nVar2.f8231b = (int) (((long) nVar2.f8231b) + j);
                nVar2.f8232c = Math.min(nVar2.f8231b + ((int) j2), nVar2.f8232c);
                if (cVar.f8202a == null) {
                    nVar2.f8236g = nVar2;
                    nVar2.f8235f = nVar2;
                    cVar.f8202a = nVar2;
                } else {
                    cVar.f8202a.f8236g.a(nVar2);
                }
                j2 -= (long) (nVar2.f8232c - nVar2.f8231b);
                nVar = nVar.f8235f;
                j = 0;
            }
        }
        return this;
    }

    public long g() {
        long j = this.f8203b;
        if (j == 0) {
            return 0;
        }
        n nVar = this.f8202a.f8236g;
        if (nVar.f8232c >= 8192 || !nVar.f8234e) {
            return j;
        }
        return j - ((long) (nVar.f8232c - nVar.f8231b));
    }

    public byte h() {
        if (this.f8203b == 0) {
            throw new IllegalStateException("size == 0");
        }
        n nVar = this.f8202a;
        int i = nVar.f8231b;
        int i2 = nVar.f8232c;
        int i3 = i + 1;
        byte b2 = nVar.f8230a[i];
        this.f8203b--;
        if (i3 == i2) {
            this.f8202a = nVar.a();
            o.a(nVar);
        } else {
            nVar.f8231b = i3;
        }
        return b2;
    }

    public byte b(long j) {
        s.a(this.f8203b, j, 1);
        n nVar = this.f8202a;
        while (true) {
            int i = nVar.f8232c - nVar.f8231b;
            if (j < ((long) i)) {
                return nVar.f8230a[nVar.f8231b + ((int) j)];
            }
            j -= (long) i;
            nVar = nVar.f8235f;
        }
    }

    public short i() {
        if (this.f8203b < 2) {
            throw new IllegalStateException("size < 2: " + this.f8203b);
        }
        n nVar = this.f8202a;
        int i = nVar.f8231b;
        int i2 = nVar.f8232c;
        if (i2 - i < 2) {
            return (short) (((h() & 255) << 8) | (h() & 255));
        }
        byte[] bArr = nVar.f8230a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b2 = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
        this.f8203b -= 2;
        if (i4 == i2) {
            this.f8202a = nVar.a();
            o.a(nVar);
        } else {
            nVar.f8231b = i4;
        }
        return (short) b2;
    }

    public int j() {
        if (this.f8203b < 4) {
            throw new IllegalStateException("size < 4: " + this.f8203b);
        }
        n nVar = this.f8202a;
        int i = nVar.f8231b;
        int i2 = nVar.f8232c;
        if (i2 - i < 4) {
            return ((h() & 255) << 24) | ((h() & 255) << 16) | ((h() & 255) << 8) | (h() & 255);
        }
        byte[] bArr = nVar.f8230a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b2 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
        int i5 = i4 + 1;
        byte b3 = b2 | ((bArr[i4] & 255) << 8);
        int i6 = i5 + 1;
        byte b4 = b3 | (bArr[i5] & 255);
        this.f8203b -= 4;
        if (i6 == i2) {
            this.f8202a = nVar.a();
            o.a(nVar);
            return b4;
        }
        nVar.f8231b = i6;
        return b4;
    }

    public short k() {
        return s.a(i());
    }

    public int l() {
        return s.a(j());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009c, code lost:
        if (r7 != r12) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009e, code lost:
        r0.f8202a = r10.a();
        okio.o.a(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a9, code lost:
        if (r2 != false) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c9, code lost:
        r10.f8231b = r7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x007e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long m() {
        /*
            r18 = this;
            r0 = r18
            long r2 = r0.f8203b
            r4 = 0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 != 0) goto L_0x0012
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r3 = "size == 0"
            r2.<init>(r3)
            throw r2
        L_0x0012:
            r4 = 0
            r3 = 0
            r2 = 0
        L_0x0016:
            r0 = r18
            okio.n r10 = r0.f8202a
            byte[] r11 = r10.f8230a
            int r6 = r10.f8231b
            int r12 = r10.f8232c
            r7 = r6
        L_0x0021:
            if (r7 >= r12) goto L_0x009c
            byte r8 = r11[r7]
            r6 = 48
            if (r8 < r6) goto L_0x0062
            r6 = 57
            if (r8 > r6) goto L_0x0062
            int r6 = r8 + -48
        L_0x002f:
            r14 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r14 = r14 & r4
            r16 = 0
            int r9 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r9 == 0) goto L_0x00bd
            okio.c r2 = new okio.c
            r2.<init>()
            okio.c r2 = r2.j(r4)
            okio.c r2 = r2.i(r8)
            java.lang.NumberFormatException r3 = new java.lang.NumberFormatException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Number too large: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r2 = r2.o()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            throw r3
        L_0x0062:
            r6 = 97
            if (r8 < r6) goto L_0x006f
            r6 = 102(0x66, float:1.43E-43)
            if (r8 > r6) goto L_0x006f
            int r6 = r8 + -97
            int r6 = r6 + 10
            goto L_0x002f
        L_0x006f:
            r6 = 65
            if (r8 < r6) goto L_0x007c
            r6 = 70
            if (r8 > r6) goto L_0x007c
            int r6 = r8 + -65
            int r6 = r6 + 10
            goto L_0x002f
        L_0x007c:
            if (r3 != 0) goto L_0x009b
            java.lang.NumberFormatException r2 = new java.lang.NumberFormatException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Expected leading [0-9a-fA-F] character but was 0x"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = java.lang.Integer.toHexString(r8)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        L_0x009b:
            r2 = 1
        L_0x009c:
            if (r7 != r12) goto L_0x00c9
            okio.n r6 = r10.a()
            r0 = r18
            r0.f8202a = r6
            okio.o.a(r10)
        L_0x00a9:
            if (r2 != 0) goto L_0x00b1
            r0 = r18
            okio.n r6 = r0.f8202a
            if (r6 != 0) goto L_0x0016
        L_0x00b1:
            r0 = r18
            long r6 = r0.f8203b
            long r2 = (long) r3
            long r2 = r6 - r2
            r0 = r18
            r0.f8203b = r2
            return r4
        L_0x00bd:
            r8 = 4
            long r4 = r4 << r8
            long r8 = (long) r6
            long r8 = r8 | r4
            int r4 = r7 + 1
            int r3 = r3 + 1
            r7 = r4
            r4 = r8
            goto L_0x0021
        L_0x00c9:
            r10.f8231b = r7
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.c.m():long");
    }

    public ByteString n() {
        return new ByteString(q());
    }

    public ByteString c(long j) {
        return new ByteString(f(j));
    }

    public String o() {
        try {
            return a(this.f8203b, s.f8243a);
        } catch (EOFException e2) {
            throw new AssertionError(e2);
        }
    }

    public String d(long j) {
        return a(j, s.f8243a);
    }

    public String a(Charset charset) {
        try {
            return a(this.f8203b, charset);
        } catch (EOFException e2) {
            throw new AssertionError(e2);
        }
    }

    public String a(long j, Charset charset) {
        s.a(this.f8203b, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        } else if (j == 0) {
            return "";
        } else {
            n nVar = this.f8202a;
            if (((long) nVar.f8231b) + j > ((long) nVar.f8232c)) {
                return new String(f(j), charset);
            }
            String str = new String(nVar.f8230a, nVar.f8231b, (int) j, charset);
            nVar.f8231b = (int) (((long) nVar.f8231b) + j);
            this.f8203b -= j;
            if (nVar.f8231b != nVar.f8232c) {
                return str;
            }
            this.f8202a = nVar.a();
            o.a(nVar);
            return str;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public String p() {
        long a2 = a((byte) 10);
        if (a2 != -1) {
            return e(a2);
        }
        c cVar = new c();
        a(cVar, 0, Math.min(32L, this.f8203b));
        throw new EOFException("\\n not found: size=" + b() + " content=" + cVar.n().e() + "…");
    }

    /* access modifiers changed from: package-private */
    public String e(long j) {
        if (j <= 0 || b(j - 1) != 13) {
            String d2 = d(j);
            g(1L);
            return d2;
        }
        String d3 = d(j - 1);
        g(2L);
        return d3;
    }

    public byte[] q() {
        try {
            return f(this.f8203b);
        } catch (EOFException e2) {
            throw new AssertionError(e2);
        }
    }

    public byte[] f(long j) {
        s.a(this.f8203b, 0, j);
        if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        }
        byte[] bArr = new byte[((int) j)];
        a(bArr);
        return bArr;
    }

    public void a(byte[] bArr) {
        int i = 0;
        while (i < bArr.length) {
            int a2 = a(bArr, i, bArr.length - i);
            if (a2 == -1) {
                throw new EOFException();
            }
            i += a2;
        }
    }

    public int a(byte[] bArr, int i, int i2) {
        s.a((long) bArr.length, (long) i, (long) i2);
        n nVar = this.f8202a;
        if (nVar == null) {
            return -1;
        }
        int min = Math.min(i2, nVar.f8232c - nVar.f8231b);
        System.arraycopy(nVar.f8230a, nVar.f8231b, bArr, i, min);
        nVar.f8231b += min;
        this.f8203b -= (long) min;
        if (nVar.f8231b != nVar.f8232c) {
            return min;
        }
        this.f8202a = nVar.a();
        o.a(nVar);
        return min;
    }

    public void r() {
        try {
            g(this.f8203b);
        } catch (EOFException e2) {
            throw new AssertionError(e2);
        }
    }

    public void g(long j) {
        while (j > 0) {
            if (this.f8202a == null) {
                throw new EOFException();
            }
            int min = (int) Math.min(j, (long) (this.f8202a.f8232c - this.f8202a.f8231b));
            this.f8203b -= (long) min;
            j -= (long) min;
            n nVar = this.f8202a;
            nVar.f8231b = min + nVar.f8231b;
            if (this.f8202a.f8231b == this.f8202a.f8232c) {
                n nVar2 = this.f8202a;
                this.f8202a = nVar2.a();
                o.a(nVar2);
            }
        }
    }

    /* renamed from: a */
    public c b(ByteString byteString) {
        if (byteString == null) {
            throw new IllegalArgumentException("byteString == null");
        }
        byteString.a(this);
        return this;
    }

    /* renamed from: a */
    public c b(String str) {
        return a(str, 0, str.length());
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 152 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public okio.c a(java.lang.String r10, int r11, int r12) {
        /*
            r9 = this;
            r8 = 57343(0xdfff, float:8.0355E-41)
            r7 = 128(0x80, float:1.794E-43)
            if (r10 != 0) goto L_0x000f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "string == null"
            r0.<init>(r1)
            throw r0
        L_0x000f:
            if (r11 >= 0) goto L_0x002a
            java.lang.IllegalAccessError r0 = new java.lang.IllegalAccessError
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "beginIndex < 0: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x002a:
            if (r12 >= r11) goto L_0x004f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "endIndex < beginIndex: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " < "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x004f:
            int r0 = r10.length()
            if (r12 <= r0) goto L_0x0090
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "endIndex > string.length: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " > "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r10.length()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x007c:
            r0 = 0
        L_0x007d:
            r2 = 56319(0xdbff, float:7.892E-41)
            if (r1 > r2) goto L_0x0089
            r2 = 56320(0xdc00, float:7.8921E-41)
            if (r0 < r2) goto L_0x0089
            if (r0 <= r8) goto L_0x0114
        L_0x0089:
            r0 = 63
            r9.i(r0)
            int r11 = r11 + 1
        L_0x0090:
            if (r11 >= r12) goto L_0x0145
            char r1 = r10.charAt(r11)
            if (r1 >= r7) goto L_0x00d2
            r0 = 1
            okio.n r2 = r9.e(r0)
            byte[] r3 = r2.f8230a
            int r0 = r2.f8232c
            int r4 = r0 - r11
            int r0 = 8192 - r4
            int r5 = java.lang.Math.min(r12, r0)
            int r0 = r11 + 1
            int r6 = r4 + r11
            byte r1 = (byte) r1
            r3[r6] = r1
        L_0x00b0:
            if (r0 >= r5) goto L_0x00b8
            char r6 = r10.charAt(r0)
            if (r6 < r7) goto L_0x00ca
        L_0x00b8:
            int r1 = r0 + r4
            int r3 = r2.f8232c
            int r1 = r1 - r3
            int r3 = r2.f8232c
            int r3 = r3 + r1
            r2.f8232c = r3
            long r2 = r9.f8203b
            long r4 = (long) r1
            long r2 = r2 + r4
            r9.f8203b = r2
        L_0x00c8:
            r11 = r0
            goto L_0x0090
        L_0x00ca:
            int r1 = r0 + 1
            int r0 = r0 + r4
            byte r6 = (byte) r6
            r3[r0] = r6
            r0 = r1
            goto L_0x00b0
        L_0x00d2:
            r0 = 2048(0x800, float:2.87E-42)
            if (r1 >= r0) goto L_0x00e7
            int r0 = r1 >> 6
            r0 = r0 | 192(0xc0, float:2.69E-43)
            r9.i(r0)
            r0 = r1 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.i(r0)
            int r0 = r11 + 1
            goto L_0x00c8
        L_0x00e7:
            r0 = 55296(0xd800, float:7.7486E-41)
            if (r1 < r0) goto L_0x00ee
            if (r1 <= r8) goto L_0x0108
        L_0x00ee:
            int r0 = r1 >> 12
            r0 = r0 | 224(0xe0, float:3.14E-43)
            r9.i(r0)
            int r0 = r1 >> 6
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.i(r0)
            r0 = r1 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.i(r0)
            int r0 = r11 + 1
            goto L_0x00c8
        L_0x0108:
            int r0 = r11 + 1
            if (r0 >= r12) goto L_0x007c
            int r0 = r11 + 1
            char r0 = r10.charAt(r0)
            goto L_0x007d
        L_0x0114:
            r2 = 65536(0x10000, float:9.18355E-41)
            r3 = -55297(0xffffffffffff27ff, float:NaN)
            r1 = r1 & r3
            int r1 = r1 << 10
            r3 = -56321(0xffffffffffff23ff, float:NaN)
            r0 = r0 & r3
            r0 = r0 | r1
            int r0 = r0 + r2
            int r1 = r0 >> 18
            r1 = r1 | 240(0xf0, float:3.36E-43)
            r9.i(r1)
            int r1 = r0 >> 12
            r1 = r1 & 63
            r1 = r1 | 128(0x80, float:1.794E-43)
            r9.i(r1)
            int r1 = r0 >> 6
            r1 = r1 & 63
            r1 = r1 | 128(0x80, float:1.794E-43)
            r9.i(r1)
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.i(r0)
            int r0 = r11 + 2
            goto L_0x00c8
        L_0x0145:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.c.a(java.lang.String, int, int):okio.c");
    }

    public c a(int i) {
        if (i < 128) {
            i(i);
        } else if (i < 2048) {
            i((i >> 6) | 192);
            i((i & 63) | FileUtils.FileMode.MODE_IWUSR);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                i((i >> 12) | 224);
                i(((i >> 6) & 63) | FileUtils.FileMode.MODE_IWUSR);
                i((i & 63) | FileUtils.FileMode.MODE_IWUSR);
            } else {
                throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
            }
        } else if (i <= 1114111) {
            i((i >> 18) | 240);
            i(((i >> 12) & 63) | FileUtils.FileMode.MODE_IWUSR);
            i(((i >> 6) & 63) | FileUtils.FileMode.MODE_IWUSR);
            i((i & 63) | FileUtils.FileMode.MODE_IWUSR);
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    /* renamed from: b */
    public c c(byte[] bArr) {
        if (bArr != null) {
            return c(bArr, 0, bArr.length);
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: b */
    public c c(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("source == null");
        }
        s.a((long) bArr.length, (long) i, (long) i2);
        int i3 = i + i2;
        while (i < i3) {
            n e2 = e(1);
            int min = Math.min(i3 - i, 8192 - e2.f8232c);
            System.arraycopy(bArr, i, e2.f8230a, e2.f8232c, min);
            i += min;
            e2.f8232c = min + e2.f8232c;
        }
        this.f8203b += (long) i2;
        return this;
    }

    public long a(q qVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long a2 = qVar.a(this, 8192);
            if (a2 == -1) {
                return j;
            }
            j += a2;
        }
    }

    /* renamed from: b */
    public c i(int i) {
        n e2 = e(1);
        byte[] bArr = e2.f8230a;
        int i2 = e2.f8232c;
        e2.f8232c = i2 + 1;
        bArr[i2] = (byte) i;
        this.f8203b++;
        return this;
    }

    /* renamed from: c */
    public c h(int i) {
        n e2 = e(2);
        byte[] bArr = e2.f8230a;
        int i2 = e2.f8232c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & VUserInfo.FLAG_MASK_USER_TYPE);
        bArr[i3] = (byte) (i & VUserInfo.FLAG_MASK_USER_TYPE);
        e2.f8232c = i3 + 1;
        this.f8203b += 2;
        return this;
    }

    /* renamed from: d */
    public c g(int i) {
        n e2 = e(4);
        byte[] bArr = e2.f8230a;
        int i2 = e2.f8232c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & VUserInfo.FLAG_MASK_USER_TYPE);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & VUserInfo.FLAG_MASK_USER_TYPE);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & VUserInfo.FLAG_MASK_USER_TYPE);
        bArr[i5] = (byte) (i & VUserInfo.FLAG_MASK_USER_TYPE);
        e2.f8232c = i5 + 1;
        this.f8203b += 4;
        return this;
    }

    /* renamed from: h */
    public c k(long j) {
        boolean z;
        long j2;
        int i;
        if (j == 0) {
            return i(48);
        }
        if (j < 0) {
            j2 = -j;
            if (j2 < 0) {
                return b("-9223372036854775808");
            }
            z = true;
        } else {
            z = false;
            j2 = j;
        }
        if (j2 < 100000000) {
            i = j2 < 10000 ? j2 < 100 ? j2 < 10 ? 1 : 2 : j2 < 1000 ? 3 : 4 : j2 < 1000000 ? j2 < 100000 ? 5 : 6 : j2 < 10000000 ? 7 : 8;
        } else {
            i = j2 < 1000000000000L ? j2 < 10000000000L ? j2 < 1000000000 ? 9 : 10 : j2 < 100000000000L ? 11 : 12 : j2 < 1000000000000000L ? j2 < 10000000000000L ? 13 : j2 < 100000000000000L ? 14 : 15 : j2 < 100000000000000000L ? j2 < 10000000000000000L ? 16 : 17 : j2 < 1000000000000000000L ? 18 : 19;
        }
        if (z) {
            i++;
        }
        n e2 = e(i);
        byte[] bArr = e2.f8230a;
        int i2 = e2.f8232c + i;
        while (j2 != 0) {
            i2--;
            bArr[i2] = f8201c[(int) (j2 % 10)];
            j2 /= 10;
        }
        if (z) {
            bArr[i2 - 1] = 45;
        }
        e2.f8232c += i;
        this.f8203b = ((long) i) + this.f8203b;
        return this;
    }

    /* renamed from: i */
    public c j(long j) {
        if (j == 0) {
            return i(48);
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        n e2 = e(numberOfTrailingZeros);
        byte[] bArr = e2.f8230a;
        int i = e2.f8232c;
        for (int i2 = (e2.f8232c + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = f8201c[(int) (15 & j)];
            j >>>= 4;
        }
        e2.f8232c += numberOfTrailingZeros;
        this.f8203b = ((long) numberOfTrailingZeros) + this.f8203b;
        return this;
    }

    /* access modifiers changed from: package-private */
    public n e(int i) {
        if (i < 1 || i > 8192) {
            throw new IllegalArgumentException();
        } else if (this.f8202a == null) {
            this.f8202a = o.a();
            n nVar = this.f8202a;
            n nVar2 = this.f8202a;
            n nVar3 = this.f8202a;
            nVar2.f8236g = nVar3;
            nVar.f8235f = nVar3;
            return nVar3;
        } else {
            n nVar4 = this.f8202a.f8236g;
            if (nVar4.f8232c + i > 8192 || !nVar4.f8234e) {
                return nVar4.a(o.a());
            }
            return nVar4;
        }
    }

    public void a_(c cVar, long j) {
        if (cVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (cVar == this) {
            throw new IllegalArgumentException("source == this");
        } else {
            s.a(cVar.f8203b, 0, j);
            while (j > 0) {
                if (j < ((long) (cVar.f8202a.f8232c - cVar.f8202a.f8231b))) {
                    n nVar = this.f8202a != null ? this.f8202a.f8236g : null;
                    if (nVar != null && nVar.f8234e) {
                        if ((((long) nVar.f8232c) + j) - ((long) (nVar.f8233d ? 0 : nVar.f8231b)) <= 8192) {
                            cVar.f8202a.a(nVar, (int) j);
                            cVar.f8203b -= j;
                            this.f8203b += j;
                            return;
                        }
                    }
                    cVar.f8202a = cVar.f8202a.a((int) j);
                }
                n nVar2 = cVar.f8202a;
                long j2 = (long) (nVar2.f8232c - nVar2.f8231b);
                cVar.f8202a = nVar2.a();
                if (this.f8202a == null) {
                    this.f8202a = nVar2;
                    n nVar3 = this.f8202a;
                    n nVar4 = this.f8202a;
                    n nVar5 = this.f8202a;
                    nVar4.f8236g = nVar5;
                    nVar3.f8235f = nVar5;
                } else {
                    this.f8202a.f8236g.a(nVar2).b();
                }
                cVar.f8203b -= j2;
                this.f8203b += j2;
                j -= j2;
            }
        }
    }

    public long a(c cVar, long j) {
        if (cVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f8203b == 0) {
            return -1;
        } else {
            if (j > this.f8203b) {
                j = this.f8203b;
            }
            cVar.a_(this, j);
            return j;
        }
    }

    public long a(byte b2) {
        return a(b2, 0);
    }

    public long a(byte b2, long j) {
        n nVar;
        long j2;
        long j3 = 0;
        if (j < 0) {
            throw new IllegalArgumentException("fromIndex < 0");
        }
        n nVar2 = this.f8202a;
        if (nVar2 == null) {
            return -1;
        }
        if (this.f8203b - j >= j) {
            n nVar3 = nVar2;
            while (true) {
                long j4 = ((long) (nVar.f8232c - nVar.f8231b)) + j2;
                if (j4 >= j) {
                    break;
                }
                nVar3 = nVar.f8235f;
                j3 = j4;
            }
        } else {
            j2 = this.f8203b;
            nVar = nVar2;
            while (j2 > j) {
                nVar = nVar.f8236g;
                j2 -= (long) (nVar.f8232c - nVar.f8231b);
            }
        }
        while (j2 < this.f8203b) {
            byte[] bArr = nVar.f8230a;
            int i = nVar.f8232c;
            for (int i2 = (int) ((((long) nVar.f8231b) + j) - j2); i2 < i; i2++) {
                if (bArr[i2] == b2) {
                    return j2 + ((long) (i2 - nVar.f8231b));
                }
            }
            j2 += (long) (nVar.f8232c - nVar.f8231b);
            nVar = nVar.f8235f;
            j = j2;
        }
        return -1;
    }

    public boolean a(long j, ByteString byteString) {
        return a(j, byteString, 0, byteString.g());
    }

    public boolean a(long j, ByteString byteString, int i, int i2) {
        if (j < 0 || i < 0 || i2 < 0 || this.f8203b - j < ((long) i2) || byteString.g() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (b(((long) i3) + j) != byteString.a(i + i3)) {
                return false;
            }
        }
        return true;
    }

    public void flush() {
    }

    public void close() {
    }

    public r a() {
        return r.f8239b;
    }

    public boolean equals(Object obj) {
        long j = 0;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (this.f8203b != cVar.f8203b) {
            return false;
        }
        if (this.f8203b == 0) {
            return true;
        }
        n nVar = this.f8202a;
        n nVar2 = cVar.f8202a;
        int i = nVar.f8231b;
        int i2 = nVar2.f8231b;
        while (j < this.f8203b) {
            long min = (long) Math.min(nVar.f8232c - i, nVar2.f8232c - i2);
            int i3 = 0;
            while (((long) i3) < min) {
                int i4 = i + 1;
                byte b2 = nVar.f8230a[i];
                int i5 = i2 + 1;
                if (b2 != nVar2.f8230a[i2]) {
                    return false;
                }
                i3++;
                i2 = i5;
                i = i4;
            }
            if (i == nVar.f8232c) {
                nVar = nVar.f8235f;
                i = nVar.f8231b;
            }
            if (i2 == nVar2.f8232c) {
                nVar2 = nVar2.f8235f;
                i2 = nVar2.f8231b;
            }
            j += min;
        }
        return true;
    }

    public int hashCode() {
        n nVar = this.f8202a;
        if (nVar == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = nVar.f8231b;
            int i3 = nVar.f8232c;
            while (i2 < i3) {
                i2++;
                i = nVar.f8230a[i2] + (i * 31);
            }
            nVar = nVar.f8235f;
        } while (nVar != this.f8202a);
        return i;
    }

    public String toString() {
        return t().toString();
    }

    /* renamed from: s */
    public c clone() {
        c cVar = new c();
        if (this.f8203b == 0) {
            return cVar;
        }
        cVar.f8202a = new n(this.f8202a);
        n nVar = cVar.f8202a;
        n nVar2 = cVar.f8202a;
        n nVar3 = cVar.f8202a;
        nVar2.f8236g = nVar3;
        nVar.f8235f = nVar3;
        for (n nVar4 = this.f8202a.f8235f; nVar4 != this.f8202a; nVar4 = nVar4.f8235f) {
            cVar.f8202a.f8236g.a(new n(nVar4));
        }
        cVar.f8203b = this.f8203b;
        return cVar;
    }

    public ByteString t() {
        if (this.f8203b <= 2147483647L) {
            return f((int) this.f8203b);
        }
        throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.f8203b);
    }

    public ByteString f(int i) {
        if (i == 0) {
            return ByteString.f8183b;
        }
        return new SegmentedByteString(this, i);
    }
}
