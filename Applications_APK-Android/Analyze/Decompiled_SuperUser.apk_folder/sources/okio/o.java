package okio;

/* compiled from: SegmentPool */
final class o {

    /* renamed from: a  reason: collision with root package name */
    static n f8237a;

    /* renamed from: b  reason: collision with root package name */
    static long f8238b;

    private o() {
    }

    static n a() {
        synchronized (o.class) {
            if (f8237a == null) {
                return new n();
            }
            n nVar = f8237a;
            f8237a = nVar.f8235f;
            nVar.f8235f = null;
            f8238b -= 8192;
            return nVar;
        }
    }

    static void a(n nVar) {
        if (nVar.f8235f != null || nVar.f8236g != null) {
            throw new IllegalArgumentException();
        } else if (!nVar.f8233d) {
            synchronized (o.class) {
                if (f8238b + 8192 <= 65536) {
                    f8238b += 8192;
                    nVar.f8235f = f8237a;
                    nVar.f8232c = 0;
                    nVar.f8231b = 0;
                    f8237a = nVar;
                }
            }
        }
    }
}
