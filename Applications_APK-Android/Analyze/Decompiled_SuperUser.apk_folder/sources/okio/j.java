package okio;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/* compiled from: InflaterSource */
public final class j implements q {

    /* renamed from: a  reason: collision with root package name */
    private final e f8214a;

    /* renamed from: b  reason: collision with root package name */
    private final Inflater f8215b;

    /* renamed from: c  reason: collision with root package name */
    private int f8216c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f8217d;

    public j(q qVar, Inflater inflater) {
        this(k.a(qVar), inflater);
    }

    j(e eVar, Inflater inflater) {
        if (eVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (inflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f8214a = eVar;
            this.f8215b = inflater;
        }
    }

    public long a(c cVar, long j) {
        boolean b2;
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f8217d) {
            throw new IllegalStateException("closed");
        } else if (j == 0) {
            return 0;
        } else {
            do {
                b2 = b();
                try {
                    n e2 = cVar.e(1);
                    int inflate = this.f8215b.inflate(e2.f8230a, e2.f8232c, 8192 - e2.f8232c);
                    if (inflate > 0) {
                        e2.f8232c += inflate;
                        cVar.f8203b += (long) inflate;
                        return (long) inflate;
                    } else if (this.f8215b.finished() || this.f8215b.needsDictionary()) {
                        c();
                        if (e2.f8231b == e2.f8232c) {
                            cVar.f8202a = e2.a();
                            o.a(e2);
                        }
                        return -1;
                    }
                } catch (DataFormatException e3) {
                    throw new IOException(e3);
                }
            } while (!b2);
            throw new EOFException("source exhausted prematurely");
        }
    }

    public boolean b() {
        if (!this.f8215b.needsInput()) {
            return false;
        }
        c();
        if (this.f8215b.getRemaining() != 0) {
            throw new IllegalStateException("?");
        } else if (this.f8214a.f()) {
            return true;
        } else {
            n nVar = this.f8214a.c().f8202a;
            this.f8216c = nVar.f8232c - nVar.f8231b;
            this.f8215b.setInput(nVar.f8230a, nVar.f8231b, this.f8216c);
            return false;
        }
    }

    private void c() {
        if (this.f8216c != 0) {
            int remaining = this.f8216c - this.f8215b.getRemaining();
            this.f8216c -= remaining;
            this.f8214a.g((long) remaining);
        }
    }

    public r a() {
        return this.f8214a.a();
    }

    public void close() {
        if (!this.f8217d) {
            this.f8215b.end();
            this.f8217d = true;
            this.f8214a.close();
        }
    }
}
