package okio;

/* compiled from: RealBufferedSink */
final class l implements d {

    /* renamed from: a  reason: collision with root package name */
    public final c f8224a = new c();

    /* renamed from: b  reason: collision with root package name */
    public final p f8225b;

    /* renamed from: c  reason: collision with root package name */
    boolean f8226c;

    l(p pVar) {
        if (pVar == null) {
            throw new NullPointerException("sink == null");
        }
        this.f8225b = pVar;
    }

    public c c() {
        return this.f8224a;
    }

    public void a_(c cVar, long j) {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        this.f8224a.a_(cVar, j);
        u();
    }

    public d b(ByteString byteString) {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        this.f8224a.b(byteString);
        return u();
    }

    public d b(String str) {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        this.f8224a.b(str);
        return u();
    }

    public d c(byte[] bArr) {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        this.f8224a.c(bArr);
        return u();
    }

    public d c(byte[] bArr, int i, int i2) {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        this.f8224a.c(bArr, i, i2);
        return u();
    }

    public long a(q qVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long a2 = qVar.a(this.f8224a, 8192);
            if (a2 == -1) {
                return j;
            }
            j += a2;
            u();
        }
    }

    public d i(int i) {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        this.f8224a.i(i);
        return u();
    }

    public d h(int i) {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        this.f8224a.h(i);
        return u();
    }

    public d g(int i) {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        this.f8224a.g(i);
        return u();
    }

    public d k(long j) {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        this.f8224a.k(j);
        return u();
    }

    public d j(long j) {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        this.f8224a.j(j);
        return u();
    }

    public d u() {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        long g2 = this.f8224a.g();
        if (g2 > 0) {
            this.f8225b.a_(this.f8224a, g2);
        }
        return this;
    }

    public d e() {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        long b2 = this.f8224a.b();
        if (b2 > 0) {
            this.f8225b.a_(this.f8224a, b2);
        }
        return this;
    }

    public void flush() {
        if (this.f8226c) {
            throw new IllegalStateException("closed");
        }
        if (this.f8224a.f8203b > 0) {
            this.f8225b.a_(this.f8224a, this.f8224a.f8203b);
        }
        this.f8225b.flush();
    }

    public void close() {
        if (!this.f8226c) {
            Throwable th = null;
            try {
                if (this.f8224a.f8203b > 0) {
                    this.f8225b.a_(this.f8224a, this.f8224a.f8203b);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f8225b.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.f8226c = true;
            if (th != null) {
                s.a(th);
            }
        }
    }

    public r a() {
        return this.f8225b.a();
    }

    public String toString() {
        return "buffer(" + this.f8225b + ")";
    }
}
