package okio;

import java.io.EOFException;
import java.nio.charset.Charset;

/* compiled from: RealBufferedSource */
final class m implements e {

    /* renamed from: a  reason: collision with root package name */
    public final c f8227a = new c();

    /* renamed from: b  reason: collision with root package name */
    public final q f8228b;

    /* renamed from: c  reason: collision with root package name */
    boolean f8229c;

    m(q qVar) {
        if (qVar == null) {
            throw new NullPointerException("source == null");
        }
        this.f8228b = qVar;
    }

    public c c() {
        return this.f8227a;
    }

    public long a(c cVar, long j) {
        if (cVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f8229c) {
            throw new IllegalStateException("closed");
        } else if (this.f8227a.f8203b == 0 && this.f8228b.a(this.f8227a, 8192) == -1) {
            return -1;
        } else {
            return this.f8227a.a(cVar, Math.min(j, this.f8227a.f8203b));
        }
    }

    public boolean f() {
        if (!this.f8229c) {
            return this.f8227a.f() && this.f8228b.a(this.f8227a, 8192) == -1;
        }
        throw new IllegalStateException("closed");
    }

    public void a(long j) {
        if (!b(j)) {
            throw new EOFException();
        }
    }

    public boolean b(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f8229c) {
            throw new IllegalStateException("closed");
        } else {
            while (this.f8227a.f8203b < j) {
                if (this.f8228b.a(this.f8227a, 8192) == -1) {
                    return false;
                }
            }
            return true;
        }
    }

    public byte h() {
        a(1);
        return this.f8227a.h();
    }

    public ByteString c(long j) {
        a(j);
        return this.f8227a.c(j);
    }

    public byte[] q() {
        this.f8227a.a(this.f8228b);
        return this.f8227a.q();
    }

    public byte[] f(long j) {
        a(j);
        return this.f8227a.f(j);
    }

    public String a(Charset charset) {
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        }
        this.f8227a.a(this.f8228b);
        return this.f8227a.a(charset);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public String p() {
        long a2 = a((byte) 10);
        if (a2 != -1) {
            return this.f8227a.e(a2);
        }
        c cVar = new c();
        this.f8227a.a(cVar, 0, Math.min(32L, this.f8227a.b()));
        throw new EOFException("\\n not found: size=" + this.f8227a.b() + " content=" + cVar.n().e() + "…");
    }

    public short i() {
        a(2);
        return this.f8227a.i();
    }

    public short k() {
        a(2);
        return this.f8227a.k();
    }

    public int j() {
        a(4);
        return this.f8227a.j();
    }

    public int l() {
        a(4);
        return this.f8227a.l();
    }

    public long m() {
        a(1);
        int i = 0;
        while (true) {
            if (!b((long) (i + 1))) {
                break;
            }
            byte b2 = this.f8227a.b((long) i);
            if ((b2 >= 48 && b2 <= 57) || ((b2 >= 97 && b2 <= 102) || (b2 >= 65 && b2 <= 70))) {
                i++;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", Byte.valueOf(b2)));
            }
        }
        return this.f8227a.m();
    }

    public void g(long j) {
        if (this.f8229c) {
            throw new IllegalStateException("closed");
        }
        while (j > 0) {
            if (this.f8227a.f8203b == 0 && this.f8228b.a(this.f8227a, 8192) == -1) {
                throw new EOFException();
            }
            long min = Math.min(j, this.f8227a.b());
            this.f8227a.g(min);
            j -= min;
        }
    }

    public long a(byte b2) {
        return a(b2, 0);
    }

    public long a(byte b2, long j) {
        if (this.f8229c) {
            throw new IllegalStateException("closed");
        }
        while (true) {
            long a2 = this.f8227a.a(b2, j);
            if (a2 != -1) {
                return a2;
            }
            long j2 = this.f8227a.f8203b;
            if (this.f8228b.a(this.f8227a, 8192) == -1) {
                return -1;
            }
            j = Math.max(j, j2);
        }
    }

    public boolean a(long j, ByteString byteString) {
        return a(j, byteString, 0, byteString.g());
    }

    public boolean a(long j, ByteString byteString, int i, int i2) {
        if (this.f8229c) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || i < 0 || i2 < 0 || byteString.g() - i < i2) {
            return false;
        } else {
            for (int i3 = 0; i3 < i2; i3++) {
                long j2 = ((long) i3) + j;
                if (!b(1 + j2) || this.f8227a.b(j2) != byteString.a(i + i3)) {
                    return false;
                }
            }
            return true;
        }
    }

    public void close() {
        if (!this.f8229c) {
            this.f8229c = true;
            this.f8228b.close();
            this.f8227a.r();
        }
    }

    public r a() {
        return this.f8228b.a();
    }

    public String toString() {
        return "buffer(" + this.f8228b + ")";
    }
}
