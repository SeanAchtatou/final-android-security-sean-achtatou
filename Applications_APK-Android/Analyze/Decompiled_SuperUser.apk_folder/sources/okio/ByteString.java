package okio;

import com.kingouser.com.util.ShellUtils;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class ByteString implements Serializable, Comparable<ByteString> {

    /* renamed from: a  reason: collision with root package name */
    static final char[] f8182a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* renamed from: b  reason: collision with root package name */
    public static final ByteString f8183b = a(new byte[0]);

    /* renamed from: c  reason: collision with root package name */
    final byte[] f8184c;

    /* renamed from: d  reason: collision with root package name */
    transient int f8185d;

    /* renamed from: e  reason: collision with root package name */
    transient String f8186e;

    ByteString(byte[] bArr) {
        this.f8184c = bArr;
    }

    public static ByteString a(byte... bArr) {
        if (bArr != null) {
            return new ByteString((byte[]) bArr.clone());
        }
        throw new IllegalArgumentException("data == null");
    }

    public static ByteString a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("s == null");
        }
        ByteString byteString = new ByteString(str.getBytes(s.f8243a));
        byteString.f8186e = str;
        return byteString;
    }

    public String a() {
        String str = this.f8186e;
        if (str != null) {
            return str;
        }
        String str2 = new String(this.f8184c, s.f8243a);
        this.f8186e = str2;
        return str2;
    }

    public String b() {
        return b.a(this.f8184c);
    }

    public ByteString c() {
        return c("SHA-1");
    }

    public ByteString d() {
        return c("SHA-256");
    }

    private ByteString c(String str) {
        try {
            return a(MessageDigest.getInstance(str).digest(this.f8184c));
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    public String e() {
        char[] cArr = new char[(this.f8184c.length * 2)];
        int i = 0;
        for (byte b2 : this.f8184c) {
            int i2 = i + 1;
            cArr[i] = f8182a[(b2 >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = f8182a[b2 & 15];
        }
        return new String(cArr);
    }

    public static ByteString b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("hex == null");
        } else if (str.length() % 2 != 0) {
            throw new IllegalArgumentException("Unexpected hex string: " + str);
        } else {
            byte[] bArr = new byte[(str.length() / 2)];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) ((a(str.charAt(i * 2)) << 4) + a(str.charAt((i * 2) + 1)));
            }
            return a(bArr);
        }
    }

    private static int a(char c2) {
        if (c2 >= '0' && c2 <= '9') {
            return c2 - '0';
        }
        if (c2 >= 'a' && c2 <= 'f') {
            return (c2 - 'a') + 10;
        }
        if (c2 >= 'A' && c2 <= 'F') {
            return (c2 - 'A') + 10;
        }
        throw new IllegalArgumentException("Unexpected hex digit: " + c2);
    }

    public ByteString f() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f8184c.length) {
                return this;
            }
            byte b2 = this.f8184c[i2];
            if (b2 < 65 || b2 > 90) {
                i = i2 + 1;
            } else {
                byte[] bArr = (byte[]) this.f8184c.clone();
                bArr[i2] = (byte) (b2 + 32);
                for (int i3 = i2 + 1; i3 < bArr.length; i3++) {
                    byte b3 = bArr[i3];
                    if (b3 >= 65 && b3 <= 90) {
                        bArr[i3] = (byte) (b3 + 32);
                    }
                }
                return new ByteString(bArr);
            }
        }
    }

    public ByteString a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException("beginIndex < 0");
        } else if (i2 > this.f8184c.length) {
            throw new IllegalArgumentException("endIndex > length(" + this.f8184c.length + ")");
        } else {
            int i3 = i2 - i;
            if (i3 < 0) {
                throw new IllegalArgumentException("endIndex < beginIndex");
            } else if (i == 0 && i2 == this.f8184c.length) {
                return this;
            } else {
                byte[] bArr = new byte[i3];
                System.arraycopy(this.f8184c, i, bArr, 0, i3);
                return new ByteString(bArr);
            }
        }
    }

    public byte a(int i) {
        return this.f8184c[i];
    }

    public int g() {
        return this.f8184c.length;
    }

    public byte[] h() {
        return (byte[]) this.f8184c.clone();
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        cVar.c(this.f8184c, 0, this.f8184c.length);
    }

    public boolean a(int i, ByteString byteString, int i2, int i3) {
        return byteString.a(i2, this.f8184c, i, i3);
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        return i >= 0 && i <= this.f8184c.length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && s.a(this.f8184c, i, bArr, i2, i3);
    }

    public final boolean a(ByteString byteString) {
        return a(0, byteString, 0, byteString.g());
    }

    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ByteString) || ((ByteString) obj).g() != this.f8184c.length || !((ByteString) obj).a(0, this.f8184c, 0, this.f8184c.length)) {
            z = false;
        } else {
            z = true;
        }
        return z;
    }

    public int hashCode() {
        int i = this.f8185d;
        if (i != 0) {
            return i;
        }
        int hashCode = Arrays.hashCode(this.f8184c);
        this.f8185d = hashCode;
        return hashCode;
    }

    /* renamed from: b */
    public int compareTo(ByteString byteString) {
        int g2 = g();
        int g3 = byteString.g();
        int min = Math.min(g2, g3);
        int i = 0;
        while (i < min) {
            byte a2 = a(i) & 255;
            byte a3 = byteString.a(i) & 255;
            if (a2 == a3) {
                i++;
            } else if (a2 < a3) {
                return -1;
            } else {
                return 1;
            }
        }
        if (g2 == g3) {
            return 0;
        }
        if (g2 >= g3) {
            return 1;
        }
        return -1;
    }

    public String toString() {
        if (this.f8184c.length == 0) {
            return "[size=0]";
        }
        String a2 = a();
        int a3 = a(a2, 64);
        if (a3 != -1) {
            String replace = a2.substring(0, a3).replace("\\", "\\\\").replace(ShellUtils.COMMAND_LINE_END, "\\n").replace("\r", "\\r");
            return a3 < a2.length() ? "[size=" + this.f8184c.length + " text=" + replace + "…]" : "[text=" + replace + "]";
        } else if (this.f8184c.length <= 64) {
            return "[hex=" + e() + "]";
        } else {
            return "[size=" + this.f8184c.length + " hex=" + a(0, 64).e() + "…]";
        }
    }

    static int a(String str, int i) {
        int i2 = 0;
        int length = str.length();
        int i3 = 0;
        while (i2 < length) {
            if (i3 == i) {
                return i2;
            }
            int codePointAt = str.codePointAt(i2);
            if ((Character.isISOControl(codePointAt) && codePointAt != 10 && codePointAt != 13) || codePointAt == 65533) {
                return -1;
            }
            i3++;
            i2 += Character.charCount(codePointAt);
        }
        return str.length();
    }
}
