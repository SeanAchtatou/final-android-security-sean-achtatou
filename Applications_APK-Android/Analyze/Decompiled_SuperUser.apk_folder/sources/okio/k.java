package okio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: Okio */
public final class k {

    /* renamed from: a  reason: collision with root package name */
    static final Logger f8218a = Logger.getLogger(k.class.getName());

    private k() {
    }

    public static e a(q qVar) {
        return new m(qVar);
    }

    public static d a(p pVar) {
        return new l(pVar);
    }

    private static p a(final OutputStream outputStream, final r rVar) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (rVar != null) {
            return new p() {
                public void a_(c cVar, long j) {
                    s.a(cVar.f8203b, 0, j);
                    while (j > 0) {
                        rVar.g();
                        n nVar = cVar.f8202a;
                        int min = (int) Math.min(j, (long) (nVar.f8232c - nVar.f8231b));
                        outputStream.write(nVar.f8230a, nVar.f8231b, min);
                        nVar.f8231b += min;
                        j -= (long) min;
                        cVar.f8203b -= (long) min;
                        if (nVar.f8231b == nVar.f8232c) {
                            cVar.f8202a = nVar.a();
                            o.a(nVar);
                        }
                    }
                }

                public void flush() {
                    outputStream.flush();
                }

                public void close() {
                    outputStream.close();
                }

                public r a() {
                    return rVar;
                }

                public String toString() {
                    return "sink(" + outputStream + ")";
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    public static p a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        a c2 = c(socket);
        return c2.a(a(socket.getOutputStream(), c2));
    }

    private static q a(final InputStream inputStream, final r rVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (rVar != null) {
            return new q() {
                public long a(c cVar, long j) {
                    if (j < 0) {
                        throw new IllegalArgumentException("byteCount < 0: " + j);
                    } else if (j == 0) {
                        return 0;
                    } else {
                        try {
                            rVar.g();
                            n e2 = cVar.e(1);
                            int read = inputStream.read(e2.f8230a, e2.f8232c, (int) Math.min(j, (long) (8192 - e2.f8232c)));
                            if (read == -1) {
                                return -1;
                            }
                            e2.f8232c += read;
                            cVar.f8203b += (long) read;
                            return (long) read;
                        } catch (AssertionError e3) {
                            if (k.a(e3)) {
                                throw new IOException(e3);
                            }
                            throw e3;
                        }
                    }
                }

                public void close() {
                    inputStream.close();
                }

                public r a() {
                    return rVar;
                }

                public String toString() {
                    return "source(" + inputStream + ")";
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    public static q b(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        a c2 = c(socket);
        return c2.a(a(socket.getInputStream(), c2));
    }

    private static a c(final Socket socket) {
        return new a() {
            /* access modifiers changed from: protected */
            public IOException a(IOException iOException) {
                SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
                if (iOException != null) {
                    socketTimeoutException.initCause(iOException);
                }
                return socketTimeoutException;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
             arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
             candidates:
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
             arg types: [java.util.logging.Level, java.lang.String, java.lang.AssertionError]
             candidates:
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
            /* access modifiers changed from: protected */
            public void a() {
                try {
                    socket.close();
                } catch (Exception e2) {
                    k.f8218a.log(Level.WARNING, "Failed to close timed out socket " + socket, (Throwable) e2);
                } catch (AssertionError e3) {
                    if (k.a(e3)) {
                        k.f8218a.log(Level.WARNING, "Failed to close timed out socket " + socket, (Throwable) e3);
                        return;
                    }
                    throw e3;
                }
            }
        };
    }

    static boolean a(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }
}
