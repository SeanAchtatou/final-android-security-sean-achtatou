package okio;

import java.util.concurrent.TimeUnit;

/* compiled from: ForwardingTimeout */
public class h extends r {

    /* renamed from: a  reason: collision with root package name */
    private r f8208a;

    public h(r rVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f8208a = rVar;
    }

    public final r a() {
        return this.f8208a;
    }

    public final h a(r rVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f8208a = rVar;
        return this;
    }

    public r a(long j, TimeUnit timeUnit) {
        return this.f8208a.a(j, timeUnit);
    }

    public long c_() {
        return this.f8208a.c_();
    }

    public boolean d_() {
        return this.f8208a.d_();
    }

    public long d() {
        return this.f8208a.d();
    }

    public r a(long j) {
        return this.f8208a.a(j);
    }

    public r e_() {
        return this.f8208a.e_();
    }

    public r f_() {
        return this.f8208a.f_();
    }

    public void g() {
        this.f8208a.g();
    }
}
