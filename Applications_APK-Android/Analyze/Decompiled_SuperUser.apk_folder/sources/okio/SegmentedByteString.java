package okio;

import java.util.Arrays;

final class SegmentedByteString extends ByteString {

    /* renamed from: f  reason: collision with root package name */
    final transient byte[][] f8187f;

    /* renamed from: g  reason: collision with root package name */
    final transient int[] f8188g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SegmentedByteString(c cVar, int i) {
        super(null);
        int i2 = 0;
        s.a(cVar.f8203b, 0, (long) i);
        n nVar = cVar.f8202a;
        int i3 = 0;
        int i4 = 0;
        while (i4 < i) {
            if (nVar.f8232c == nVar.f8231b) {
                throw new AssertionError("s.limit == s.pos");
            }
            i4 += nVar.f8232c - nVar.f8231b;
            i3++;
            nVar = nVar.f8235f;
        }
        this.f8187f = new byte[i3][];
        this.f8188g = new int[(i3 * 2)];
        n nVar2 = cVar.f8202a;
        int i5 = 0;
        while (i2 < i) {
            this.f8187f[i5] = nVar2.f8230a;
            int i6 = (nVar2.f8232c - nVar2.f8231b) + i2;
            if (i6 > i) {
                i6 = i;
            }
            this.f8188g[i5] = i6;
            this.f8188g[this.f8187f.length + i5] = nVar2.f8231b;
            nVar2.f8233d = true;
            i5++;
            nVar2 = nVar2.f8235f;
            i2 = i6;
        }
    }

    public String a() {
        return i().a();
    }

    public String b() {
        return i().b();
    }

    public String e() {
        return i().e();
    }

    public ByteString f() {
        return i().f();
    }

    public ByteString c() {
        return i().c();
    }

    public ByteString d() {
        return i().d();
    }

    public ByteString a(int i, int i2) {
        return i().a(i, i2);
    }

    public byte a(int i) {
        s.a((long) this.f8188g[this.f8187f.length - 1], (long) i, 1);
        int b2 = b(i);
        return this.f8187f[b2][(i - (b2 == 0 ? 0 : this.f8188g[b2 - 1])) + this.f8188g[this.f8187f.length + b2]];
    }

    private int b(int i) {
        int binarySearch = Arrays.binarySearch(this.f8188g, 0, this.f8187f.length, i + 1);
        return binarySearch >= 0 ? binarySearch : binarySearch ^ -1;
    }

    public int g() {
        return this.f8188g[this.f8187f.length - 1];
    }

    public byte[] h() {
        int i = 0;
        byte[] bArr = new byte[this.f8188g[this.f8187f.length - 1]];
        int length = this.f8187f.length;
        int i2 = 0;
        while (i < length) {
            int i3 = this.f8188g[length + i];
            int i4 = this.f8188g[i];
            System.arraycopy(this.f8187f[i], i3, bArr, i2, i4 - i2);
            i++;
            i2 = i4;
        }
        return bArr;
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        int i = 0;
        int length = this.f8187f.length;
        int i2 = 0;
        while (i < length) {
            int i3 = this.f8188g[length + i];
            int i4 = this.f8188g[i];
            n nVar = new n(this.f8187f[i], i3, (i3 + i4) - i2);
            if (cVar.f8202a == null) {
                nVar.f8236g = nVar;
                nVar.f8235f = nVar;
                cVar.f8202a = nVar;
            } else {
                cVar.f8202a.f8236g.a(nVar);
            }
            i++;
            i2 = i4;
        }
        cVar.f8203b = ((long) i2) + cVar.f8203b;
    }

    public boolean a(int i, ByteString byteString, int i2, int i3) {
        if (i < 0 || i > g() - i3) {
            return false;
        }
        int b2 = b(i);
        while (i3 > 0) {
            int i4 = b2 == 0 ? 0 : this.f8188g[b2 - 1];
            int min = Math.min(i3, ((this.f8188g[b2] - i4) + i4) - i);
            if (!byteString.a(i2, this.f8187f[b2], (i - i4) + this.f8188g[this.f8187f.length + b2], min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            b2++;
        }
        return true;
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        if (i < 0 || i > g() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int b2 = b(i);
        while (i3 > 0) {
            int i4 = b2 == 0 ? 0 : this.f8188g[b2 - 1];
            int min = Math.min(i3, ((this.f8188g[b2] - i4) + i4) - i);
            if (!s.a(this.f8187f[b2], (i - i4) + this.f8188g[this.f8187f.length + b2], bArr, i2, min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            b2++;
        }
        return true;
    }

    private ByteString i() {
        return new ByteString(h());
    }

    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ByteString) || ((ByteString) obj).g() != g() || !a(0, (ByteString) obj, 0, g())) {
            z = false;
        } else {
            z = true;
        }
        return z;
    }

    public int hashCode() {
        int i = this.f8185d;
        if (i == 0) {
            i = 1;
            int length = this.f8187f.length;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                byte[] bArr = this.f8187f[i2];
                int i4 = this.f8188g[length + i2];
                int i5 = this.f8188g[i2];
                int i6 = (i5 - i3) + i4;
                int i7 = i4;
                int i8 = i;
                for (int i9 = i7; i9 < i6; i9++) {
                    i8 = (i8 * 31) + bArr[i9];
                }
                i2++;
                i3 = i5;
                i = i8;
            }
            this.f8185d = i;
        }
        return i;
    }

    public String toString() {
        return i().toString();
    }
}
