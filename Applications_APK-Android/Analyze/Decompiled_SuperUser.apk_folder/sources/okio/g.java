package okio;

/* compiled from: ForwardingSource */
public abstract class g implements q {

    /* renamed from: a  reason: collision with root package name */
    private final q f8207a;

    public g(q qVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f8207a = qVar;
    }

    public long a(c cVar, long j) {
        return this.f8207a.a(cVar, j);
    }

    public r a() {
        return this.f8207a.a();
    }

    public void close() {
        this.f8207a.close();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f8207a.toString() + ")";
    }
}
