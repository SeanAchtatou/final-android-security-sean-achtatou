package okio;

import java.io.Closeable;
import java.io.Flushable;

/* compiled from: Sink */
public interface p extends Closeable, Flushable {
    r a();

    void a_(c cVar, long j);

    void close();

    void flush();
}
