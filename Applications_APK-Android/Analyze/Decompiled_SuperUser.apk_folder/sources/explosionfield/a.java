package explosionfield;

import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import java.util.Random;

/* compiled from: ExplosionAnimator */
public class a extends ValueAnimator {

    /* renamed from: a  reason: collision with root package name */
    static long f6985a = 1024;

    /* renamed from: b  reason: collision with root package name */
    private static final Interpolator f6986b = new AccelerateInterpolator(0.6f);

    /* renamed from: c  reason: collision with root package name */
    private static final float f6987c = ((float) b.a(5));

    /* renamed from: d  reason: collision with root package name */
    private static final float f6988d = ((float) b.a(20));
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public static final float f6989e = ((float) b.a(2));

    /* renamed from: f  reason: collision with root package name */
    private static final float f6990f = ((float) b.a(1));

    /* renamed from: g  reason: collision with root package name */
    private Paint f6991g = new Paint();

    /* renamed from: h  reason: collision with root package name */
    private C0098a[] f6992h;
    private Rect i;
    private View j;

    public a(View view, Bitmap bitmap, Rect rect) {
        this.i = new Rect(rect);
        this.f6992h = new C0098a[225];
        Random random = new Random(System.currentTimeMillis());
        int width = bitmap.getWidth() / 17;
        int height = bitmap.getHeight() / 17;
        for (int i2 = 0; i2 < 15; i2++) {
            for (int i3 = 0; i3 < 15; i3++) {
                this.f6992h[(i2 * 15) + i3] = a(bitmap.getPixel((i3 + 1) * width, (i2 + 1) * height), random);
            }
        }
        this.j = view;
        setFloatValues(0.0f, 1.4f);
        setInterpolator(f6986b);
        setDuration(f6985a);
    }

    private C0098a a(int i2, Random random) {
        C0098a aVar = new C0098a();
        aVar.f6994b = i2;
        aVar.f6997e = f6989e;
        if (random.nextFloat() < 0.2f) {
            aVar.f7000h = f6989e + ((f6987c - f6989e) * random.nextFloat());
        } else {
            aVar.f7000h = f6990f + ((f6989e - f6990f) * random.nextFloat());
        }
        float nextFloat = random.nextFloat();
        aVar.i = ((float) this.i.height()) * ((0.18f * random.nextFloat()) + 0.2f);
        aVar.i = nextFloat < 0.2f ? aVar.i : aVar.i + (aVar.i * 0.2f * random.nextFloat());
        aVar.j = ((float) this.i.height()) * (random.nextFloat() - 0.5f) * 1.8f;
        aVar.j = nextFloat < 0.2f ? aVar.j : nextFloat < 0.8f ? aVar.j * 0.6f : aVar.j * 0.3f;
        aVar.k = (4.0f * aVar.i) / aVar.j;
        aVar.l = (-aVar.k) / aVar.j;
        float centerX = ((float) this.i.centerX()) + (f6988d * (random.nextFloat() - 0.5f));
        aVar.f6998f = centerX;
        aVar.f6995c = centerX;
        float centerY = ((float) this.i.centerY()) + (f6988d * (random.nextFloat() - 0.5f));
        aVar.f6999g = centerY;
        aVar.f6996d = centerY;
        aVar.m = 0.14f * random.nextFloat();
        aVar.n = 0.4f * random.nextFloat();
        aVar.f6993a = 1.0f;
        return aVar;
    }

    public boolean a(Canvas canvas) {
        if (!isStarted()) {
            return false;
        }
        for (C0098a aVar : this.f6992h) {
            aVar.a(((Float) getAnimatedValue()).floatValue());
            if (aVar.f6993a > 0.0f) {
                this.f6991g.setColor(aVar.f6994b);
                this.f6991g.setAlpha((int) (((float) Color.alpha(aVar.f6994b)) * aVar.f6993a));
                canvas.drawCircle(aVar.f6995c, aVar.f6996d, aVar.f6997e, this.f6991g);
            }
        }
        this.j.invalidate();
        return true;
    }

    public void start() {
        super.start();
        this.j.invalidate(this.i);
    }

    /* renamed from: explosionfield.a$a  reason: collision with other inner class name */
    /* compiled from: ExplosionAnimator */
    private class C0098a {

        /* renamed from: a  reason: collision with root package name */
        float f6993a;

        /* renamed from: b  reason: collision with root package name */
        int f6994b;

        /* renamed from: c  reason: collision with root package name */
        float f6995c;

        /* renamed from: d  reason: collision with root package name */
        float f6996d;

        /* renamed from: e  reason: collision with root package name */
        float f6997e;

        /* renamed from: f  reason: collision with root package name */
        float f6998f;

        /* renamed from: g  reason: collision with root package name */
        float f6999g;

        /* renamed from: h  reason: collision with root package name */
        float f7000h;
        float i;
        float j;
        float k;
        float l;
        float m;
        float n;

        private C0098a() {
        }

        public void a(float f2) {
            float f3 = 0.0f;
            float f4 = f2 / 1.4f;
            if (f4 < this.m || f4 > 1.0f - this.n) {
                this.f6993a = 0.0f;
                return;
            }
            float f5 = (f4 - this.m) / ((1.0f - this.m) - this.n);
            float f6 = f5 * 1.4f;
            if (f5 >= 0.7f) {
                f3 = (f5 - 0.7f) / 0.3f;
            }
            this.f6993a = 1.0f - f3;
            float f7 = this.j * f6;
            this.f6995c = this.f6998f + f7;
            this.f6996d = ((float) (((double) this.f6999g) - (((double) this.l) * Math.pow((double) f7, 2.0d)))) - (f7 * this.k);
            this.f6997e = a.f6989e + ((this.f7000h - a.f6989e) * f6);
        }
    }
}
