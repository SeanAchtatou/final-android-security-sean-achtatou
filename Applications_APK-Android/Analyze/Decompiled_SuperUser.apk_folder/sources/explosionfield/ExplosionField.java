package explosionfield;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ExplosionField extends View {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public List<a> f6979a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private int[] f6980b = new int[2];

    public ExplosionField(Context context) {
        super(context);
        a();
    }

    public ExplosionField(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public ExplosionField(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        Arrays.fill(this.f6980b, b.a(32));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (a a2 : this.f6979a) {
            a2.a(canvas);
        }
    }

    public void a(Bitmap bitmap, Rect rect, long j, long j2) {
        if (bitmap != null) {
            a aVar = new a(this, bitmap, rect);
            aVar.addListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animator) {
                    ExplosionField.this.f6979a.remove(animator);
                }
            });
            aVar.setStartDelay(j);
            aVar.setDuration(j2);
            this.f6979a.add(aVar);
            aVar.start();
        }
    }

    public void a(final View view) {
        Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
        int[] iArr = new int[2];
        getLocationOnScreen(iArr);
        rect.offset(-iArr[0], -iArr[1]);
        rect.inset(-this.f6980b[0], -this.f6980b[1]);
        ValueAnimator duration = ValueAnimator.ofFloat(0.0f, 1.0f).setDuration(150L);
        duration.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            /* renamed from: a  reason: collision with root package name */
            Random f6982a = new Random();

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                view.setTranslationX((this.f6982a.nextFloat() - 0.5f) * ((float) view.getWidth()) * 0.05f);
                view.setTranslationY((this.f6982a.nextFloat() - 0.5f) * ((float) view.getHeight()) * 0.05f);
            }
        });
        duration.start();
        view.animate().setDuration(150).setStartDelay((long) 100).scaleX(0.0f).scaleY(0.0f).alpha(0.0f).start();
        a(b.a(view), rect, (long) 100, a.f6985a);
    }

    public static ExplosionField a(Activity activity) {
        ExplosionField explosionField = new ExplosionField(activity);
        ((ViewGroup) activity.findViewById(16908290)).addView(explosionField, new ViewGroup.LayoutParams(-1, -1));
        return explosionField;
    }
}
