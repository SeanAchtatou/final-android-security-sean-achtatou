package explosionfield;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

/* compiled from: Utils */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final float f7001a = Resources.getSystem().getDisplayMetrics().density;

    /* renamed from: b  reason: collision with root package name */
    private static final Canvas f7002b = new Canvas();

    public static int a(int i) {
        return Math.round(((float) i) * f7001a);
    }

    public static Bitmap a(View view) {
        Drawable drawable;
        if ((view instanceof ImageView) && (drawable = ((ImageView) view).getDrawable()) != null && (drawable instanceof BitmapDrawable)) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        view.clearFocus();
        Bitmap a2 = a(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888, 1);
        if (a2 == null) {
            return a2;
        }
        synchronized (f7002b) {
            Canvas canvas = f7002b;
            canvas.setBitmap(a2);
            view.draw(canvas);
            canvas.setBitmap(null);
        }
        return a2;
    }

    public static Bitmap a(int i, int i2, Bitmap.Config config, int i3) {
        try {
            return Bitmap.createBitmap(i, i2, config);
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
            if (i3 <= 0) {
                return null;
            }
            System.gc();
            return a(i, i2, config, i3 - 1);
        }
    }
}
