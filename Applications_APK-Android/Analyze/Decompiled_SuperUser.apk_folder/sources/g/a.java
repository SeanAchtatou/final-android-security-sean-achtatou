package g;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ForegroundMonitorService */
public class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static boolean f7031a = Log.isLoggable("ALFMS", 2);

    /* renamed from: c  reason: collision with root package name */
    private static a f7032c;

    /* renamed from: b  reason: collision with root package name */
    private BroadcastReceiver f7033b = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                a.this.f7036f.d();
            } else if ("android.intent.action.SCREEN_ON".equals(intent.getAction()) && a.this.b()) {
                a.this.f7036f.c();
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final ActivityManager f7034d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public String[] f7035e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final b f7036f = new b(this, null);
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public List f7037g = new ArrayList();

    /* renamed from: h  reason: collision with root package name */
    private PowerManager f7038h;

    /* renamed from: g.a$a  reason: collision with other inner class name */
    /* compiled from: ForegroundMonitorService */
    public interface C0101a {
        void a(String[] strArr);

        void b(String[] strArr);
    }

    /* compiled from: ForegroundMonitorService */
    private class b implements Runnable {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final Handler f7041b;

        private b() {
            this.f7041b = new Handler(Looper.getMainLooper());
        }

        /* synthetic */ b(a aVar, b bVar) {
            this();
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f7041b.postDelayed(this, 500);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f7041b.post(new Runnable() {
                public void run() {
                    b.this.f7041b.removeCallbacks(this);
                }
            });
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.f7041b.post(new Runnable() {
                public void run() {
                    if (a.this.f7035e != null) {
                        synchronized (a.this.f7037g) {
                            for (C0101a a2 : a.this.f7037g) {
                                a2.a(a.this.f7035e);
                            }
                        }
                    }
                }
            });
            a();
        }

        /* access modifiers changed from: package-private */
        public void d() {
            b();
            this.f7041b.post(new Runnable() {
                public void run() {
                    if (a.this.f7035e != null) {
                        synchronized (a.this.f7037g) {
                            for (C0101a b2 : a.this.f7037g) {
                                b2.b(a.this.f7035e);
                            }
                        }
                    }
                }
            });
        }

        public void run() {
            this.f7041b.removeCallbacks(this);
            if (Build.VERSION.SDK_INT >= 21) {
                List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = a.this.f7034d.getRunningAppProcesses();
                if (runningAppProcesses != null && runningAppProcesses.size() > 0) {
                    ActivityManager.RunningAppProcessInfo runningAppProcessInfo = runningAppProcesses.get(0);
                    if (runningAppProcessInfo != null) {
                        if (runningAppProcessInfo.importance == 100) {
                            if (runningAppProcessInfo.pkgList != null && runningAppProcessInfo.pkgList.length > 0) {
                                a.this.a(runningAppProcessInfo.pkgList);
                            } else if (a.f7031a) {
                                Log.e("ALFMS", "L top running info null or empty pkgList for process:%s" + runningAppProcessInfo.processName);
                            }
                        } else if (a.f7031a) {
                            Log.e("ALFMS", "L top running process:%s importance not IMPORTANCE_FOREGROUND" + runningAppProcessInfo.processName);
                        }
                    } else if (a.f7031a) {
                        Log.e("ALFMS", "L top running null");
                    }
                } else if (a.f7031a) {
                    Log.w("ALFMS", "L top running list null");
                }
            } else {
                List<ActivityManager.RunningTaskInfo> runningTasks = a.this.f7034d.getRunningTasks(1);
                if (runningTasks != null && !runningTasks.isEmpty()) {
                    ActivityManager.RunningTaskInfo runningTaskInfo = runningTasks.get(0);
                    if (runningTaskInfo != null) {
                        ComponentName componentName = runningTaskInfo.topActivity;
                        if (componentName != null) {
                            String packageName = componentName.getPackageName();
                            if (a.f7031a) {
                                Log.d("ALFMS", "top running %s" + packageName);
                            }
                            a.this.a(new String[]{packageName});
                        } else if (a.f7031a) {
                            Log.e("ALFMS", "top running null topActivity");
                        }
                    } else if (a.f7031a) {
                        Log.e("ALFMS", "top running first taskInfo is null");
                    }
                } else if (a.f7031a) {
                    Log.e("ALFMS", "top running taskList empty");
                }
            }
            this.f7041b.postDelayed(this, 500);
        }
    }

    private a(Context context) {
        this.f7034d = (ActivityManager) context.getSystemService(ServiceManagerNative.ACTIVITY);
        this.f7038h = (PowerManager) context.getSystemService("power");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        context.registerReceiver(this.f7033b, intentFilter);
    }

    public static a a(Context context) {
        if (f7032c == null) {
            synchronized (a.class) {
                if (f7032c == null) {
                    f7032c = new a(context);
                }
            }
        }
        return f7032c;
    }

    public void a(C0101a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Null arg is not acceptable.");
        }
        synchronized (this.f7037g) {
            this.f7037g.add(aVar);
            if (this.f7038h.isScreenOn()) {
                this.f7036f.a();
            }
        }
    }

    public void b(C0101a aVar) {
        if (aVar != null) {
            synchronized (this.f7037g) {
                if (this.f7037g.remove(aVar) && !b()) {
                    this.f7036f.b();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean b() {
        return !this.f7037g.isEmpty();
    }

    private boolean a(String[] strArr, String[] strArr2) {
        if (strArr.length != strArr2.length) {
            return false;
        }
        for (int i = 0; i < strArr.length; i++) {
            if (!strArr[i].equals(strArr2[i])) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void a(String[] strArr) {
        if (strArr != null) {
            if (this.f7035e == null) {
                this.f7035e = strArr;
            } else if (!a(this.f7035e, strArr)) {
                synchronized (this.f7037g) {
                    for (C0101a aVar : this.f7037g) {
                        aVar.b(this.f7035e);
                        aVar.a(strArr);
                    }
                }
                this.f7035e = strArr;
            }
        }
    }
}
