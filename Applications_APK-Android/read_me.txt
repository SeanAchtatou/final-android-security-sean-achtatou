You should have 3 folders.

/Analyze : This is where you can import the application(s) to be analyzed by App-MD.
	   The application analyzed will be decomposed as well at this location for 		   later viewing.

/Non-Safe and /Safe : Both of the folders contains malwares and benign applications.
		      It has been used as a Validation of the App-MD.

Note : Do not alter, rename or change any files or folders.
