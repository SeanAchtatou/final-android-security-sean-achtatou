import math
import numpy as np
import time
import os
import random
import pickle
import matplotlib.pyplot as plt

from xml.dom import minidom
from sklearn.neural_network import MLPClassifier as Neural_Network
from sklearn.neighbors import KNeighborsClassifier as KNN

parent_dir = os.path.dirname(os.path.dirname(os.path.abspath('ressource_model_AI.py')))
apk_tool = f'{parent_dir}/jadx/build/jadx/bin/jadx'

path_safe = 'Safe'
path_unsafe = 'Non-Safe'

existing_safe_apk = os.listdir(path_safe)
existing_unsafe_apk = os.listdir(path_unsafe)

xml_permissions_safe = [[],[]]
xml_permissions_unsafe = [[],[]]

print(f'\033[93m[SYSTEM]\033[00m Reverse Engineering on the safe APK.')
for apk in existing_safe_apk:
    if f'decompiled_{apk}' not in os.listdir(f'Decompiled_safe'):
        print(f'\033[93m--[SYSTEM]\033[00m Start of the reverse Engineering on the APK \033[42m\033[5m{apk}\033[00m running ... Please wait.')
        os.system(apk_tool + " " + f"-d Decompiled_safe/decompiled_{apk}" + " " + "./" + os.path.join(path_safe, apk))
    else:
        print(f'\033[33m--[WARNING]\033[00m APK \033[04m\033[96m<{apk}>\033[00m has already been decompiled. ')

    try:
        xml_file = minidom.parse(f'Decompiled_safe/decompiled_{apk}/resources/AndroidManifest.xml')
        use_permissions = xml_file.getElementsByTagName('uses-permission')

        permissions_apk = []
        for permissions in use_permissions:
            permissions_apk.append(permissions.attributes['android:name'].value)

        xml_permissions_safe[0].append(permissions_apk)
        xml_permissions_safe[1].append('safe')
    except:
        print(f'\033[31m[ERROR]\033[31m Unable to reach XML.file')

print(f'\033[93m[SYSTEM]\033[00m Reverse Engineering on the unsafe APK.')
for apk in existing_unsafe_apk:
    if f'decompiled_{apk}' not in os.listdir(f'Decompiled_unsafe'):
        print(f'\033[93m[SYSTEM]\033[00m Start of the reverse Engineering on the APK \033[42m\033[5m{apk}\033[00m running ... Please wait.')
        os.system(apk_tool + " " + f"-d Decompiled_unsafe/decompiled_{apk}" + " " + "./" + os.path.join(path_unsafe, apk))
    else:
        print(f'\033[33m--[WARNING]\033[00m APK \033[04m\033[96m<{apk}>\033[00m has already been decompiled. ')

    try:
        xml_file = minidom.parse(f'Decompiled_unsafe/decompiled_{apk}/resources/AndroidManifest.xml')
        use_permissions = xml_file.getElementsByTagName('uses-permission')

        permissions_apk = []
        for permissions in use_permissions:
                permissions_apk.append(permissions.attributes['android:name'].value)

        xml_permissions_unsafe[0].append(permissions_apk)
        xml_permissions_unsafe[1].append('unsafe')
    except:
        print(f'\033[31m[ERROR]\033[00m Unable to reach XML.file')

perm_vector = ['android.permission.ACCESS_FINE_LOCATION',
'android.permission.ACCESS_COARSE_LOCATION',
'android.permission.ACCESS_MEDIA_LOCATION',
'android.permission.ACTIVITY_RECOGNITION',
'android.permission.ADD_VOICEMAIL',
'android.permission.ANSWER_PHONE_CALLS',
'android.permission.BODY_SENSORS',
'android.permission.CAMERA',
'android.permission.CALL_PHONE',
'android.permission.GET_ACCOUNTS',
'android.permission.PROCESS_OUTGOING_CALLS',
'android.permission.READ_CONTACTS',
'android.permission.RECEIVE_BOOT_COMPLETED',
'android.permission.RECORD_AUDIO',
'android.permission.READ_PHONE_STATE',
'android.permission.READ_PHONE_NUMBERS',
'android.permission.READ_CALL_LOG',
'android.permission.RECEIVE_SMS',
'android.permission.READ_SMS',
'android.permission.RECEIVE_WAP_PUSH',
'android.permission.RECEIVE_MMS',
'android.permission.READ_EXTERNAL_STORAGE',
'android.permission.READ_CALENDAR',
'android.permission.SEND_SMS',
'android.permission.USE_SIP',
'android.permission.WRITE_CALL_LOG',
'android.permission.WRITE_EXTERNAL_STORAGE',
'android.permission.WRITE_CONTACTS',
'android.permission.WRITE_CALENDAR']





'''perm_vector = ['android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS',
               'android.permission.READ_EXTERNAL_STORAGE',
               'android.permission.ACCESS_COARSE_LOCATION',
               'android.permission.PROCESS_OUTGOING_CALLS',
               'android.permission.BLUETOOTH',
               'android.permission.SET_ALARM',
               'android.permission.INSTALL_SHORTCUT',
               'android.permission.USE_FINGERPRINT',
               'android.permission.ACCESS_NETWORK_STATE',
               'android.permission.ADD_VOICEMAIL',
               'android.permission.SET_WALLPAPER',
               'android.permission.RECORD_AUDIO',
               'android.permission.MODIFY_AUDIO_SETTINGS',
               'android.permission.RECEIVE_SMS',
               'android.permission.CHANGE_WIFI_STATE',
               'android.permission.ACCESS_MEDIA_LOCATION',
               'android.permission.WRITE_CONTACTS',
               'android.permission.REORDER_TASKS',
               'android.permission.READ_PHONE_NUMBERS',
               'android.permission.ANSWER_PHONE_CALLS',
               'android.permission.DISABLE_KEYGUARD',
               'android.permission.WAKE_LOCK',
               'android.permission.CAMERA',
               'android.permission.READ_SMS',
               'android.permission.WRITE_CALL_LOG',
               'android.permission.UNINSTALL_SHORTCUT',
               'android.permission.ACCESS_FINE_LOCATION',
               'android.permission.CALL_PHONE',
               'android.permission.RECEIVE_WAP_PUSH',
               'android.permission.READ_SYNC_SETTINGS',
               'android.permission.READ_PHONE_STATE',
               'android.permission.SET_WALLPAPER_HINTS',
               'android.permission.ACCESS_LOCATION_EXTRA_COMMANDS',
               'android.permission.WRITE_CALENDAR',
               'android.permission.TRANSMIT_IR',
               'android.permission.GET_ACCOUNTS',
               'android.permission.SEND_SMS',
               'android.permission.READ_CALL_LOG',
               'android.permission.BLUETOOTH_ADMIN',
               'android.permission.BROADCAST_STICKY',
               'android.permission.KILL_BACKGROUND_PROCESSES',
               'android.permission.ACCESS_NOTIFICATION_POLICY',
               'android.permission.WRITE_SYNC_SETTINGS',
               'android.permission.EXPAND_STATUS_BAR',
               'android.permission.CHANGE_NETWORK_STATE',
               'android.permission.GET_PACKAGE_SIZE',
               'android.permission.REQUEST_INSTALL_PACKAGES',
               'android.permission.NFC',
               'android.permission.SET_TIME_ZONE',
               'android.permission.ACTIVITY_RECOGNITION',
               'android.permission.VIBRATE',
               'android.permission.READ_SYNC_STATS',
               'android.permission.BODY_SENSORS',
               'android.permission.CHANGE_WIFI_MULTICAST_STATE',
               'android.permission.RECEIVE_MMS',
               'android.permission.INTERNET',
               'android.permission.USE_SIP',
               'android.permission.READ_CONTACTS',
               'android.permission.RECEIVE_BOOT_COMPLETED',
               'android.permission.WRITE_EXTERNAL_STORAGE',
               'android.permission.READ_CALENDAR',
               'android.permission.ACCESS_WIFI_STATE']'''


nn_vectors = []
nn_labels = []


for perm_in_apk in xml_permissions_safe[0]:
    vector = []
    for permI in perm_vector:
        if permI in perm_in_apk:
            vector.append(1)
        else:
            vector.append(0)
    nn_vectors.append(vector)
    nn_labels.append("Good")


for perm_in_apk in xml_permissions_unsafe[0]:
    vector = []
    for permI in perm_vector:
        if permI in perm_in_apk:
            vector.append(1)
        else:
            vector.append(0)
    nn_vectors.append(vector)
    nn_labels.append("Bad")

shuffleSet = list(zip(nn_vectors,nn_labels))
random.shuffle(shuffleSet)
nn_vectors, nn_labels = zip(*shuffleSet)
nn_vectors = list(nn_vectors)
nn_labels = list(nn_labels)


model = Neural_Network(max_iter=200)
#model = KNN(n_neighbors=27)
model.fit(nn_vectors,nn_labels)
print(model.score(nn_vectors,nn_labels))

pickle.dump(model,open('model_resources_KNN_DANGEROUS.sav', 'wb'))

























'''xml_permissions_safe = [[],[]]
xml_permissions_unsafe = [[],[]]
existing_safe_apk = os.listdir('Test_Set_Safe')
existing_unsafe_apk = os.listdir('Test_Set_Non-Safe')
for apk in existing_safe_apk:
    if f'decompiled_{apk}' not in os.listdir(f'Decompiled_test_safe'):
        print(f'\033[93m--[SYSTEM]\033[00m Start of the reverse Engineering on the APK \033[42m\033[5m{apk}\033[00m running ... Please wait.')
        os.system(apk_tool + " " + f"-d Decompiled_test_safe/decompiled_{apk}" + " " + os.path.join(f'Test_Set_Safe', apk))
    else:
        print(f'\033[33m--[WARNING]\033[00m APK \033[04m\033[96m<{apk}>\033[00m has already been decompiled. ')

    try:
        xml_file = minidom.parse(f'Decompiled_test_safe/decompiled_{apk}/resources/AndroidManifest.xml')
        use_permissions = xml_file.getElementsByTagName('uses-permission')

        permissions_apk = []
        for permissions in use_permissions:

            permissions_apk.append(permissions.attributes['android:name'].value)

        xml_permissions_safe[0].append(permissions_apk)
        xml_permissions_safe[1].append('safe')
    except:
        print(f'\033[31m[ERROR]\033[31m Unable to reach XML.file')

print(f'\033[93m[SYSTEM]\033[00m Reverse Engineering on the unsafe APK.')
for apk in existing_unsafe_apk:
    if f'decompiled_{apk}' not in os.listdir(f'Decompiled_test_unsafe'):
        print(f'\033[93m--[SYSTEM]\033[00m Start of the reverse Engineering on the APK \033[42m\033[5m{apk}\033[00m running ... Please wait.')
        os.system(apk_tool + " " + f"-d Decompiled_test_unsafe/decompiled_{apk}" + " " + os.path.join(f'Test_Set_Non-Safe', apk))
    else:
        print(f'\033[33m--[WARNING]\033[00m APK \033[04m\033[96m<{apk}>\033[00m has already been decompiled. ')

    try:
        xml_file = minidom.parse(f'Decompiled_test_unsafe/decompiled_{apk}/resources/AndroidManifest.xml')
        use_permissions = xml_file.getElementsByTagName('uses-permission')

        permissions_apk = []
        for permissions in use_permissions:

                permissions_apk.append(permissions.attributes['android:name'].value)

        xml_permissions_unsafe[0].append(permissions_apk)
        xml_permissions_unsafe[1].append('unsafe')
    except:
        print(f'\033[31m[ERROR]\033[00m Unable to reach XML.file')



nn_vectors = []
nn_labels = []

#for perm in perm_vector:

for perm_in_apk in xml_permissions_safe[0]:
    vector = []
    for permI in perm_vector:
        if permI in perm_in_apk:
            vector.append(1)
        else:
            vector.append(0)
    nn_vectors.append(vector)
    nn_labels.append("Good")


for perm_in_apk in xml_permissions_unsafe[0]:
    vector = []
    for permI in perm_vector:
        if permI in perm_in_apk:
            vector.append(1)
        else:
            vector.append(0)
    nn_vectors.append(vector)
    nn_labels.append("Bad")



print(model.score(nn_vectors,nn_labels))'''




