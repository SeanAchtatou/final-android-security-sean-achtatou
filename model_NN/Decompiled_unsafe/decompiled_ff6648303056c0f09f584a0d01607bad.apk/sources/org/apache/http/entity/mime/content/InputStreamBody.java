package org.apache.http.entity.mime.content;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.annotation.NotThreadSafe;

@NotThreadSafe
public class InputStreamBody extends AbstractContentBody {
    private final String filename;
    private final InputStream in;

    public InputStreamBody(InputStream in2, String mimeType, String filename2) {
        super(mimeType);
        if (in2 == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        }
        this.in = in2;
        this.filename = filename2;
    }

    public InputStreamBody(InputStream in2, String filename2) {
        this(in2, "application/octet-stream", filename2);
    }

    public InputStream getInputStream() {
        return this.in;
    }

    @Deprecated
    public void writeTo(OutputStream out, int mode) throws IOException {
        Object[] objArr = {out};
        InputStreamBody.class.getMethod("writeTo", OutputStream.class).invoke(this, objArr);
    }

    public void writeTo(OutputStream out) throws IOException {
        String str;
        if (out == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        try {
            byte[] tmp = new byte[4096];
            while (true) {
                Object[] objArr = {tmp};
                int l = ((Integer) InputStream.class.getMethod("read", byte[].class).invoke(this.in, objArr)).intValue();
                if (l != -1) {
                    Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
                    OutputStream.class.getMethod("write", clsArr).invoke(out, tmp, new Integer(0), new Integer(l));
                } else {
                    OutputStream.class.getMethod("flush", new Class[0]).invoke(out, new Object[0]);
                    return;
                }
            }
        } finally {
            int i = 0;
            Class[] clsArr2 = new Class[i];
            Object[] objArr2 = new Object[i];
            str = "close";
            InputStream.class.getMethod(str, clsArr2).invoke(this.in, objArr2);
        }
    }

    public String getTransferEncoding() {
        return "binary";
    }

    public String getCharset() {
        return null;
    }

    public long getContentLength() {
        return -1;
    }

    public String getFilename() {
        return this.filename;
    }
}
