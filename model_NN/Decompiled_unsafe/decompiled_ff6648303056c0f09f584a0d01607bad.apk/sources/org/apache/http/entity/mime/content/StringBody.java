package org.apache.http.entity.mime.content;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.james.mime4j.field.ContentTypeField;

@NotThreadSafe
public class StringBody extends AbstractContentBody {
    private final Charset charset;
    private final byte[] content;

    public StringBody(String text, String mimeType, Charset charset2) throws UnsupportedEncodingException {
        super(mimeType);
        if (text == null) {
            throw new IllegalArgumentException("Text may not be null");
        }
        if (charset2 == null) {
            charset2 = (Charset) Charset.class.getMethod("defaultCharset", new Class[0]).invoke(null, new Object[0]);
        }
        Object[] objArr = {(String) Charset.class.getMethod("name", new Class[0]).invoke(charset2, new Object[0])};
        this.content = (byte[]) String.class.getMethod("getBytes", String.class).invoke(text, objArr);
        this.charset = charset2;
    }

    public StringBody(String text, Charset charset2) throws UnsupportedEncodingException {
        this(text, ContentTypeField.TYPE_TEXT_PLAIN, charset2);
    }

    public StringBody(String text) throws UnsupportedEncodingException {
        this(text, ContentTypeField.TYPE_TEXT_PLAIN, null);
    }

    public Reader getReader() {
        return new InputStreamReader(new ByteArrayInputStream(this.content), this.charset);
    }

    @Deprecated
    public void writeTo(OutputStream out, int mode) throws IOException {
        Object[] objArr = {out};
        StringBody.class.getMethod("writeTo", OutputStream.class).invoke(this, objArr);
    }

    public void writeTo(OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        InputStream in = new ByteArrayInputStream(this.content);
        byte[] tmp = new byte[4096];
        while (true) {
            Object[] objArr = {tmp};
            int l = ((Integer) InputStream.class.getMethod("read", byte[].class).invoke(in, objArr)).intValue();
            if (l != -1) {
                Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
                OutputStream.class.getMethod("write", clsArr).invoke(out, tmp, new Integer(0), new Integer(l));
            } else {
                OutputStream.class.getMethod("flush", new Class[0]).invoke(out, new Object[0]);
                return;
            }
        }
    }

    public String getTransferEncoding() {
        return "8bit";
    }

    public String getCharset() {
        return (String) Charset.class.getMethod("name", new Class[0]).invoke(this.charset, new Object[0]);
    }

    public Map<String, String> getContentTypeParameters() {
        Map<String, String> map = new HashMap<>();
        Object[] objArr = {ContentTypeField.PARAM_CHARSET, (String) Charset.class.getMethod("name", new Class[0]).invoke(this.charset, new Object[0])};
        Map.class.getMethod("put", Object.class, Object.class).invoke(map, objArr);
        return map;
    }

    public long getContentLength() {
        return (long) this.content.length;
    }

    public String getFilename() {
        return null;
    }
}
