package org.apache.http.entity.mime.content;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.annotation.NotThreadSafe;

@NotThreadSafe
public class FileBody extends AbstractContentBody {
    private final File file;

    public FileBody(File file2, String mimeType) {
        super(mimeType);
        if (file2 == null) {
            throw new IllegalArgumentException("File may not be null");
        }
        this.file = file2;
    }

    public FileBody(File file2) {
        this(file2, "application/octet-stream");
    }

    public InputStream getInputStream() throws IOException {
        return new FileInputStream(this.file);
    }

    @Deprecated
    public void writeTo(OutputStream out, int mode) throws IOException {
        Object[] objArr = {out};
        FileBody.class.getMethod("writeTo", OutputStream.class).invoke(this, objArr);
    }

    public void writeTo(OutputStream out) throws IOException {
        String str;
        if (out == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        InputStream in = new FileInputStream(this.file);
        try {
            byte[] tmp = new byte[4096];
            while (true) {
                Object[] objArr = {tmp};
                int l = ((Integer) InputStream.class.getMethod("read", byte[].class).invoke(in, objArr)).intValue();
                if (l != -1) {
                    Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
                    OutputStream.class.getMethod("write", clsArr).invoke(out, tmp, new Integer(0), new Integer(l));
                } else {
                    OutputStream.class.getMethod("flush", new Class[0]).invoke(out, new Object[0]);
                    return;
                }
            }
        } finally {
            int i = 0;
            Class[] clsArr2 = new Class[i];
            Object[] objArr2 = new Object[i];
            str = "close";
            InputStream.class.getMethod(str, clsArr2).invoke(in, objArr2);
        }
    }

    public String getTransferEncoding() {
        return "binary";
    }

    public String getCharset() {
        return null;
    }

    public long getContentLength() {
        return ((Long) File.class.getMethod("length", new Class[0]).invoke(this.file, new Object[0])).longValue();
    }

    public String getFilename() {
        return (String) File.class.getMethod("getName", new Class[0]).invoke(this.file, new Object[0]);
    }

    public File getFile() {
        return this.file;
    }
}
