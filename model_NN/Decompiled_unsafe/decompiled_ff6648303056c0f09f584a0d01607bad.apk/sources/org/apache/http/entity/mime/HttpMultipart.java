package org.apache.http.entity.mime;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.List;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.james.mime4j.field.ContentTypeField;
import org.apache.james.mime4j.message.Body;
import org.apache.james.mime4j.message.BodyPart;
import org.apache.james.mime4j.message.Entity;
import org.apache.james.mime4j.message.Header;
import org.apache.james.mime4j.message.MessageWriter;
import org.apache.james.mime4j.message.Multipart;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.ByteArrayBuffer;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.CharsetUtil;

@NotThreadSafe
public class HttpMultipart extends Multipart {
    private static final ByteArrayBuffer CR_LF;
    private static final ByteArrayBuffer TWO_DASHES;
    private HttpMultipartMode mode = HttpMultipartMode.STRICT;

    private static ByteArrayBuffer encode(Charset charset, String string) {
        Object[] objArr = {string};
        Object[] objArr2 = {(CharBuffer) CharBuffer.class.getMethod("wrap", CharSequence.class).invoke(null, objArr)};
        ByteBuffer encoded = (ByteBuffer) Charset.class.getMethod("encode", CharBuffer.class).invoke(charset, objArr2);
        ByteArrayBuffer bab = new ByteArrayBuffer(((Integer) ByteBuffer.class.getMethod("remaining", new Class[0]).invoke(encoded, new Object[0])).intValue());
        Method method = ByteBuffer.class.getMethod("array", new Class[0]);
        int intValue = ((Integer) ByteBuffer.class.getMethod("position", new Class[0]).invoke(encoded, new Object[0])).intValue();
        int intValue2 = ((Integer) ByteBuffer.class.getMethod("remaining", new Class[0]).invoke(encoded, new Object[0])).intValue();
        Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
        ByteArrayBuffer.class.getMethod("append", clsArr).invoke(bab, (byte[]) method.invoke(encoded, new Object[0]), new Integer(intValue), new Integer(intValue2));
        return bab;
    }

    private static void writeBytes(ByteArrayBuffer b, OutputStream out) throws IOException {
        Method method = ByteArrayBuffer.class.getMethod("buffer", new Class[0]);
        int intValue = ((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(b, new Object[0])).intValue();
        Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
        OutputStream.class.getMethod("write", clsArr).invoke(out, (byte[]) method.invoke(b, new Object[0]), new Integer(0), new Integer(intValue));
    }

    private static void writeBytes(ByteSequence b, OutputStream out) throws IOException {
        if (b instanceof ByteArrayBuffer) {
            Object[] objArr = {(ByteArrayBuffer) b, out};
            HttpMultipart.class.getMethod("writeBytes", ByteArrayBuffer.class, OutputStream.class).invoke(null, objArr);
            return;
        }
        Object[] objArr2 = {(byte[]) ByteSequence.class.getMethod("toByteArray", new Class[0]).invoke(b, new Object[0])};
        OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr2);
    }

    static {
        Object[] objArr = {MIME.DEFAULT_CHARSET, CharsetUtil.CRLF};
        CR_LF = (ByteArrayBuffer) HttpMultipart.class.getMethod("encode", Charset.class, String.class).invoke(null, objArr);
        Object[] objArr2 = {MIME.DEFAULT_CHARSET, "--"};
        TWO_DASHES = (ByteArrayBuffer) HttpMultipart.class.getMethod("encode", Charset.class, String.class).invoke(null, objArr2);
    }

    public HttpMultipart(String subType) {
        super(subType);
    }

    public HttpMultipartMode getMode() {
        return this.mode;
    }

    public void setMode(HttpMultipartMode mode2) {
        this.mode = mode2;
    }

    /* access modifiers changed from: protected */
    public Charset getCharset() {
        Method method = HttpMultipart.class.getMethod("getParent", new Class[0]);
        Method method2 = Entity.class.getMethod("getHeader", new Class[0]);
        Method method3 = Header.class.getMethod("getField", String.class);
        ContentTypeField cField = (ContentTypeField) ((Field) method3.invoke((Header) method2.invoke((Entity) method.invoke(this, new Object[0]), new Object[0]), "Content-Type"));
        switch (AnonymousClass1.$SwitchMap$org$apache$http$entity$mime$HttpMultipartMode[((Integer) HttpMultipartMode.class.getMethod("ordinal", new Class[0]).invoke(this.mode, new Object[0])).intValue()]) {
            case 1:
                return MIME.DEFAULT_CHARSET;
            case 2:
                if (((String) ContentTypeField.class.getMethod("getCharset", new Class[0]).invoke(cField, new Object[0])) != null) {
                    Class[] clsArr = {String.class};
                    return (Charset) CharsetUtil.class.getMethod("getCharset", clsArr).invoke(null, (String) ContentTypeField.class.getMethod("getCharset", new Class[0]).invoke(cField, new Object[0]));
                }
                return (Charset) CharsetUtil.class.getMethod("getCharset", String.class).invoke(null, "ISO-8859-1");
            default:
                return null;
        }
    }

    /* renamed from: org.apache.http.entity.mime.HttpMultipart$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$apache$http$entity$mime$HttpMultipartMode = new int[((HttpMultipartMode[]) HttpMultipartMode.class.getMethod("values", new Class[0]).invoke(null, new Object[0])).length];

        static {
            try {
                $SwitchMap$org$apache$http$entity$mime$HttpMultipartMode[((Integer) HttpMultipartMode.class.getMethod("ordinal", new Class[0]).invoke(HttpMultipartMode.STRICT, new Object[0])).intValue()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$apache$http$entity$mime$HttpMultipartMode[((Integer) HttpMultipartMode.class.getMethod("ordinal", new Class[0]).invoke(HttpMultipartMode.BROWSER_COMPATIBLE, new Object[0])).intValue()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public String getBoundary() {
        Method method = HttpMultipart.class.getMethod("getParent", new Class[0]);
        Method method2 = Entity.class.getMethod("getHeader", new Class[0]);
        Object[] objArr = {"Content-Type"};
        Method method3 = Header.class.getMethod("getField", String.class);
        return (String) ContentTypeField.class.getMethod("getBoundary", new Class[0]).invoke((ContentTypeField) ((Field) method3.invoke((Header) method2.invoke((Entity) method.invoke(this, new Object[0]), new Object[0]), objArr)), new Object[0]);
    }

    private void doWriteTo(HttpMultipartMode mode2, OutputStream out, boolean writeContent) throws IOException {
        List<BodyPart> bodyParts = getBodyParts();
        Charset charset = getCharset();
        ByteArrayBuffer boundary = encode(charset, getBoundary());
        switch (AnonymousClass1.$SwitchMap$org$apache$http$entity$mime$HttpMultipartMode[mode2.ordinal()]) {
            case 1:
                String preamble = getPreamble();
                if (!(preamble == null || preamble.length() == 0)) {
                    writeBytes(encode(charset, preamble), out);
                    writeBytes(CR_LF, out);
                }
                for (int i = 0; i < bodyParts.size(); i++) {
                    writeBytes(TWO_DASHES, out);
                    writeBytes(boundary, out);
                    writeBytes(CR_LF, out);
                    BodyPart part = bodyParts.get(i);
                    for (Field field : part.getHeader().getFields()) {
                        writeBytes(field.getRaw(), out);
                        writeBytes(CR_LF, out);
                    }
                    writeBytes(CR_LF, out);
                    if (writeContent) {
                        MessageWriter.DEFAULT.writeBody(part.getBody(), out);
                    }
                    writeBytes(CR_LF, out);
                }
                writeBytes(TWO_DASHES, out);
                writeBytes(boundary, out);
                writeBytes(TWO_DASHES, out);
                writeBytes(CR_LF, out);
                String epilogue = getEpilogue();
                if (epilogue != null && epilogue.length() != 0) {
                    writeBytes(encode(charset, epilogue), out);
                    writeBytes(CR_LF, out);
                    return;
                }
                return;
            case 2:
                for (int i2 = 0; i2 < bodyParts.size(); i2++) {
                    writeBytes(TWO_DASHES, out);
                    writeBytes(boundary, out);
                    writeBytes(CR_LF, out);
                    BodyPart part2 = bodyParts.get(i2);
                    Field cd = part2.getHeader().getField("Content-Disposition");
                    writeBytes(encode(charset, cd.getName() + ": " + cd.getBody()), out);
                    writeBytes(CR_LF, out);
                    writeBytes(CR_LF, out);
                    if (writeContent) {
                        MessageWriter.DEFAULT.writeBody(part2.getBody(), out);
                    }
                    writeBytes(CR_LF, out);
                }
                writeBytes(TWO_DASHES, out);
                writeBytes(boundary, out);
                writeBytes(TWO_DASHES, out);
                writeBytes(CR_LF, out);
                return;
            default:
                return;
        }
    }

    public void writeTo(OutputStream out) throws IOException {
        HttpMultipartMode httpMultipartMode = this.mode;
        Class[] clsArr = {HttpMultipartMode.class, OutputStream.class, Boolean.TYPE};
        HttpMultipart.class.getMethod("doWriteTo", clsArr).invoke(this, httpMultipartMode, out, new Boolean(true));
    }

    public long getTotalLength() {
        List<?> bodyParts = getBodyParts();
        long contentLen = 0;
        for (int i = 0; i < bodyParts.size(); i++) {
            Body body = bodyParts.get(i).getBody();
            if (!(body instanceof ContentBody)) {
                return -1;
            }
            long len = ((ContentBody) body).getContentLength();
            if (len < 0) {
                return -1;
            }
            contentLen += len;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            doWriteTo(this.mode, out, false);
            return ((long) out.toByteArray().length) + contentLen;
        } catch (IOException e) {
            return -1;
        }
    }
}
