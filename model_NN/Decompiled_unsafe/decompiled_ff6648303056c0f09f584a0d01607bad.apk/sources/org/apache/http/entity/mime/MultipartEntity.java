package org.apache.http.entity.mime;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.message.BasicHeader;
import org.apache.james.mime4j.field.ContentTypeField;
import org.apache.james.mime4j.field.Fields;
import org.apache.james.mime4j.message.Body;
import org.apache.james.mime4j.message.BodyPart;
import org.apache.james.mime4j.message.Entity;
import org.apache.james.mime4j.message.Message;
import org.apache.james.mime4j.parser.Field;

@ThreadSafe
public class MultipartEntity implements HttpEntity {
    private static final char[] MULTIPART_CHARS = ((char[]) String.class.getMethod("toCharArray", new Class[0]).invoke("-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", new Object[0]));
    private final Header contentType;
    private volatile boolean dirty;
    private long length;
    private final Message message;
    private final HttpMultipart multipart;

    public MultipartEntity(HttpMultipartMode mode, String boundary, Charset charset) {
        this.multipart = new HttpMultipart("form-data");
        this.contentType = new BasicHeader("Content-Type", (String) MultipartEntity.class.getMethod("generateContentType", String.class, Charset.class).invoke(this, boundary, charset));
        this.dirty = true;
        this.message = new Message();
        Message.class.getMethod("setHeader", org.apache.james.mime4j.message.Header.class).invoke(this.message, new org.apache.james.mime4j.message.Header());
        HttpMultipart.class.getMethod("setParent", Entity.class).invoke(this.multipart, this.message);
        HttpMultipart.class.getMethod("setMode", HttpMultipartMode.class).invoke(this.multipart, mode == null ? HttpMultipartMode.STRICT : mode);
        Method method = Message.class.getMethod("getHeader", new Class[0]);
        org.apache.james.mime4j.message.Header.class.getMethod("addField", Field.class).invoke((org.apache.james.mime4j.message.Header) method.invoke(this.message, new Object[0]), (ContentTypeField) Fields.class.getMethod("contentType", String.class).invoke(null, (String) Header.class.getMethod("getValue", new Class[0]).invoke(this.contentType, new Object[0])));
    }

    public MultipartEntity(HttpMultipartMode mode) {
        this(mode, null, null);
    }

    public MultipartEntity() {
        this(HttpMultipartMode.STRICT, null, null);
    }

    /* access modifiers changed from: protected */
    public String generateContentType(String boundary, Charset charset) {
        StringBuilder buffer = new StringBuilder();
        Object[] objArr = {"multipart/form-data; boundary="};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr);
        if (boundary != null) {
            Object[] objArr2 = {boundary};
            StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr2);
        } else {
            Random rand = new Random();
            Class[] clsArr = {Integer.TYPE};
            int count = ((Integer) Random.class.getMethod("nextInt", clsArr).invoke(rand, new Integer(11))).intValue() + 30;
            for (int i = 0; i < count; i++) {
                char[] cArr = MULTIPART_CHARS;
                int length2 = MULTIPART_CHARS.length;
                Class[] clsArr2 = {Integer.TYPE};
                char c = cArr[((Integer) Random.class.getMethod("nextInt", clsArr2).invoke(rand, new Integer(length2))).intValue()];
                Class[] clsArr3 = {Character.TYPE};
                StringBuilder.class.getMethod("append", clsArr3).invoke(buffer, new Character(c));
            }
        }
        if (charset != null) {
            Object[] objArr3 = {"; charset="};
            StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr3);
            Object[] objArr4 = {(String) Charset.class.getMethod("name", new Class[0]).invoke(charset, new Object[0])};
            StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr4);
        }
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(buffer, new Object[0]);
    }

    public void addPart(String name, ContentBody contentBody) {
        HttpMultipart httpMultipart = this.multipart;
        Object[] objArr = {new FormBodyPart(name, contentBody)};
        HttpMultipart.class.getMethod("addBodyPart", BodyPart.class).invoke(httpMultipart, objArr);
        this.dirty = true;
    }

    public boolean isRepeatable() {
        Method method;
        Method method2;
        Method method3 = HttpMultipart.class.getMethod("getBodyParts", new Class[0]);
        Iterator<?> it = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke((List) method3.invoke(this.multipart, new Object[0]), new Object[0]);
        do {
            if (!((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(it, new Object[0])).booleanValue()) {
                return true;
            }
            method = Iterator.class.getMethod("next", new Class[0]);
            method2 = FormBodyPart.class.getMethod("getBody", new Class[0]);
        } while (((Long) ContentBody.class.getMethod("getContentLength", new Class[0]).invoke((ContentBody) ((Body) method2.invoke((FormBodyPart) method.invoke(it, new Object[0]), new Object[0])), new Object[0])).longValue() >= 0);
        return false;
    }

    public boolean isChunked() {
        return !((Boolean) MultipartEntity.class.getMethod("isRepeatable", new Class[0]).invoke(this, new Object[0])).booleanValue();
    }

    public boolean isStreaming() {
        return !((Boolean) MultipartEntity.class.getMethod("isRepeatable", new Class[0]).invoke(this, new Object[0])).booleanValue();
    }

    public long getContentLength() {
        if (this.dirty) {
            this.length = ((Long) HttpMultipart.class.getMethod("getTotalLength", new Class[0]).invoke(this.multipart, new Object[0])).longValue();
            this.dirty = false;
        }
        return this.length;
    }

    public Header getContentType() {
        return this.contentType;
    }

    public Header getContentEncoding() {
        return null;
    }

    public void consumeContent() throws IOException, UnsupportedOperationException {
        if (((Boolean) MultipartEntity.class.getMethod("isStreaming", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
        }
    }

    public InputStream getContent() throws IOException, UnsupportedOperationException {
        throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
    }

    public void writeTo(OutputStream outstream) throws IOException {
        Object[] objArr = {outstream};
        HttpMultipart.class.getMethod("writeTo", OutputStream.class).invoke(this.multipart, objArr);
    }
}
