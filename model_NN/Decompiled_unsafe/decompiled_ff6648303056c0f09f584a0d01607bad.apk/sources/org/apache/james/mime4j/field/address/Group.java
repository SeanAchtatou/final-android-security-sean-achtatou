package org.apache.james.mime4j.field.address;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.apache.james.mime4j.codec.EncoderUtil;

public class Group extends Address {
    private static final long serialVersionUID = 1;
    private final MailboxList mailboxList;
    private final String name;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Group(java.lang.String r8, org.apache.james.mime4j.field.address.Mailbox... r9) {
        /*
            r7 = this;
            org.apache.james.mime4j.field.address.MailboxList r0 = new org.apache.james.mime4j.field.address.MailboxList
            r3 = 1
            java.lang.Class[] r4 = new java.lang.Class[r3]
            java.lang.Object[] r5 = new java.lang.Object[r3]
            r3 = 0
            java.lang.Class<java.lang.Object[]> r6 = java.lang.Object[].class
            r4[r3] = r6
            r5[r3] = r9
            java.lang.String r3 = "asList"
            java.lang.Class<java.util.Arrays> r6 = java.util.Arrays.class
            java.lang.reflect.Method r3 = r6.getMethod(r3, r4)
            r6 = 0
            java.lang.Object r1 = r3.invoke(r6, r5)
            java.util.List r1 = (java.util.List) r1
            r2 = 1
            r0.<init>(r1, r2)
            r7.<init>(r8, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.address.Group.<init>(java.lang.String, org.apache.james.mime4j.field.address.Mailbox[]):void");
    }

    public Group(String name2, Collection<Mailbox> mailboxes) {
        this(name2, new MailboxList(new ArrayList(mailboxes), true));
    }

    public Group(String name2, MailboxList mailboxes) {
        if (name2 == null) {
            throw new IllegalArgumentException();
        } else if (mailboxes == null) {
            throw new IllegalArgumentException();
        } else {
            this.name = name2;
            this.mailboxList = mailboxes;
        }
    }

    public static Group parse(String rawGroupString) {
        Object[] objArr = {rawGroupString};
        Address address = (Address) Address.class.getMethod("parse", String.class).invoke(null, objArr);
        if (address instanceof Group) {
            return (Group) address;
        }
        throw new IllegalArgumentException("Not a group address");
    }

    public String getName() {
        return this.name;
    }

    public MailboxList getMailboxes() {
        return this.mailboxList;
    }

    public String getDisplayString(boolean includeRoute) {
        StringBuilder sb = new StringBuilder();
        Object[] objArr = {this.name};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr);
        Class[] clsArr = {Character.TYPE};
        StringBuilder.class.getMethod("append", clsArr).invoke(sb, new Character(':'));
        boolean first = true;
        Iterator i$ = (Iterator) MailboxList.class.getMethod("iterator", new Class[0]).invoke(this.mailboxList, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Mailbox mailbox = (Mailbox) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0]);
                if (first) {
                    first = false;
                } else {
                    Class[] clsArr2 = {Character.TYPE};
                    StringBuilder.class.getMethod("append", clsArr2).invoke(sb, new Character(','));
                }
                Class[] clsArr3 = {Character.TYPE};
                StringBuilder.class.getMethod("append", clsArr3).invoke(sb, new Character(' '));
                Class[] clsArr4 = {Boolean.TYPE};
                Object[] objArr2 = {(String) Mailbox.class.getMethod("getDisplayString", clsArr4).invoke(mailbox, new Boolean(includeRoute))};
                StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
            } else {
                Object[] objArr3 = {";"};
                StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr3);
                return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
            }
        }
    }

    public String getEncodedString() {
        StringBuilder sb = new StringBuilder();
        Object[] objArr = {this.name};
        Object[] objArr2 = {(String) EncoderUtil.class.getMethod("encodeAddressDisplayName", String.class).invoke(null, objArr)};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
        Class[] clsArr = {Character.TYPE};
        StringBuilder.class.getMethod("append", clsArr).invoke(sb, new Character(':'));
        boolean first = true;
        Iterator i$ = (Iterator) MailboxList.class.getMethod("iterator", new Class[0]).invoke(this.mailboxList, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Mailbox mailbox = (Mailbox) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0]);
                if (first) {
                    first = false;
                } else {
                    Class[] clsArr2 = {Character.TYPE};
                    StringBuilder.class.getMethod("append", clsArr2).invoke(sb, new Character(','));
                }
                Class[] clsArr3 = {Character.TYPE};
                StringBuilder.class.getMethod("append", clsArr3).invoke(sb, new Character(' '));
                Object[] objArr3 = {(String) Mailbox.class.getMethod("getEncodedString", new Class[0]).invoke(mailbox, new Object[0])};
                StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr3);
            } else {
                Class[] clsArr4 = {Character.TYPE};
                StringBuilder.class.getMethod("append", clsArr4).invoke(sb, new Character(';'));
                return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void doAddMailboxesTo(List<Mailbox> results) {
        Iterator i$ = (Iterator) MailboxList.class.getMethod("iterator", new Class[0]).invoke(this.mailboxList, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Object[] objArr = {(Mailbox) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0])};
                ((Boolean) List.class.getMethod("add", Object.class).invoke(results, objArr)).booleanValue();
            } else {
                return;
            }
        }
    }
}
