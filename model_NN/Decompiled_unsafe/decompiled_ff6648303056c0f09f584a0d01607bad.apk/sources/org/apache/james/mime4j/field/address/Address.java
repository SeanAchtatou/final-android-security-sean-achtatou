package org.apache.james.mime4j.field.address;

import java.io.Serializable;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.List;
import org.apache.james.mime4j.field.address.parser.ASTaddress;
import org.apache.james.mime4j.field.address.parser.AddressListParser;
import org.apache.james.mime4j.field.address.parser.ParseException;

public abstract class Address implements Serializable {
    private static final long serialVersionUID = 634090661990433426L;

    /* access modifiers changed from: protected */
    public abstract void doAddMailboxesTo(List<Mailbox> list);

    public abstract String getDisplayString(boolean z);

    public abstract String getEncodedString();

    /* access modifiers changed from: package-private */
    public final void addMailboxesTo(List<Mailbox> results) {
        Object[] objArr = {results};
        Address.class.getMethod("doAddMailboxesTo", List.class).invoke(this, objArr);
    }

    public final String getDisplayString() {
        Class[] clsArr = {Boolean.TYPE};
        return (String) Address.class.getMethod("getDisplayString", clsArr).invoke(this, new Boolean(false));
    }

    public static Address parse(String rawAddressString) {
        AddressListParser parser = new AddressListParser(new StringReader(rawAddressString));
        try {
            Method method = Builder.class.getMethod("getInstance", new Class[0]);
            Object[] objArr = {(ASTaddress) AddressListParser.class.getMethod("parseAddress", new Class[0]).invoke(parser, new Object[0])};
            return (Address) Builder.class.getMethod("buildAddress", ASTaddress.class).invoke((Builder) method.invoke(null, new Object[0]), objArr);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String toString() {
        Class[] clsArr = {Boolean.TYPE};
        return (String) Address.class.getMethod("getDisplayString", clsArr).invoke(this, new Boolean(false));
    }
}
