package org.apache.james.mime4j.io;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import org.apache.james.mime4j.util.ByteArrayBuffer;

public class BufferedLineReaderInputStream extends LineReaderInputStream {
    private byte[] buffer;
    private int buflen;
    private int bufpos;
    private final int maxLineLen;
    private boolean truncated;

    public BufferedLineReaderInputStream(InputStream instream, int buffersize, int maxLineLen2) {
        super(instream);
        if (instream == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        } else if (buffersize <= 0) {
            throw new IllegalArgumentException("Buffer size may not be negative or zero");
        } else {
            this.buffer = new byte[buffersize];
            this.bufpos = 0;
            this.buflen = 0;
            this.maxLineLen = maxLineLen2;
            this.truncated = false;
        }
    }

    public BufferedLineReaderInputStream(InputStream instream, int buffersize) {
        this(instream, buffersize, -1);
    }

    private void expand(int newlen) {
        byte[] newbuffer = new byte[newlen];
        int len = this.buflen - this.bufpos;
        if (len > 0) {
            byte[] bArr = this.buffer;
            int i = this.bufpos;
            int i2 = this.bufpos;
            Class[] clsArr = {Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE};
            System.class.getMethod("arraycopy", clsArr).invoke(null, bArr, new Integer(i), newbuffer, new Integer(i2), new Integer(len));
        }
        this.buffer = newbuffer;
    }

    public void ensureCapacity(int len) {
        if (len > this.buffer.length) {
            Class[] clsArr = {Integer.TYPE};
            BufferedLineReaderInputStream.class.getMethod("expand", clsArr).invoke(this, new Integer(len));
        }
    }

    public int fillBuffer() throws IOException {
        if (this.bufpos > 0) {
            int len = this.buflen - this.bufpos;
            if (len > 0) {
                byte[] bArr = this.buffer;
                int i = this.bufpos;
                byte[] bArr2 = this.buffer;
                Class[] clsArr = {Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE};
                System.class.getMethod("arraycopy", clsArr).invoke(null, bArr, new Integer(i), bArr2, new Integer(0), new Integer(len));
            }
            this.bufpos = 0;
            this.buflen = len;
        }
        int off = this.buflen;
        int len2 = this.buffer.length - off;
        InputStream inputStream = this.in;
        byte[] bArr3 = this.buffer;
        Class[] clsArr2 = {byte[].class, Integer.TYPE, Integer.TYPE};
        int l = ((Integer) InputStream.class.getMethod("read", clsArr2).invoke(inputStream, bArr3, new Integer(off), new Integer(len2))).intValue();
        if (l == -1) {
            return -1;
        }
        this.buflen = off + l;
        return l;
    }

    public boolean hasBufferedData() {
        return this.bufpos < this.buflen;
    }

    public void truncate() {
        BufferedLineReaderInputStream.class.getMethod("clear", new Class[0]).invoke(this, new Object[0]);
        this.truncated = true;
    }

    public int read() throws IOException {
        if (this.truncated) {
            return -1;
        }
        do {
            if (!((Boolean) BufferedLineReaderInputStream.class.getMethod("hasBufferedData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            } else {
                byte[] bArr = this.buffer;
                int i = this.bufpos;
                this.bufpos = i + 1;
                return bArr[i] & 255;
            }
        } while (((Integer) BufferedLineReaderInputStream.class.getMethod("fillBuffer", new Class[0]).invoke(this, new Object[0])).intValue() != -1);
        return -1;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        if (this.truncated) {
            return -1;
        }
        if (b == null) {
            return 0;
        }
        do {
            if (!((Boolean) BufferedLineReaderInputStream.class.getMethod("hasBufferedData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            } else {
                int chunk = this.buflen - this.bufpos;
                if (chunk > len) {
                    chunk = len;
                }
                byte[] bArr = this.buffer;
                int i = this.bufpos;
                System.class.getMethod("arraycopy", Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE).invoke(null, bArr, new Integer(i), b, new Integer(off), new Integer(chunk));
                this.bufpos += chunk;
                return chunk;
            }
        } while (((Integer) BufferedLineReaderInputStream.class.getMethod("fillBuffer", new Class[0]).invoke(this, new Object[0])).intValue() != -1);
        return -1;
    }

    public int read(byte[] b) throws IOException {
        if (this.truncated) {
            return -1;
        }
        if (b == null) {
            return 0;
        }
        int length = b.length;
        Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
        return ((Integer) BufferedLineReaderInputStream.class.getMethod("read", clsArr).invoke(this, b, new Integer(0), new Integer(length))).intValue();
    }

    public boolean markSupported() {
        return false;
    }

    public int readLine(ByteArrayBuffer dst) throws IOException {
        int chunk;
        if (dst == null) {
            throw new IllegalArgumentException("Buffer may not be null");
        } else if (this.truncated) {
            return -1;
        } else {
            int total = 0;
            boolean found = false;
            int bytesRead = 0;
            while (!found) {
                if (!((Boolean) BufferedLineReaderInputStream.class.getMethod("hasBufferedData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                    bytesRead = ((Integer) BufferedLineReaderInputStream.class.getMethod("fillBuffer", new Class[0]).invoke(this, new Object[0])).intValue();
                    if (bytesRead == -1) {
                        break;
                    }
                }
                Class[] clsArr = {Byte.TYPE};
                int i = ((Integer) BufferedLineReaderInputStream.class.getMethod("indexOf", clsArr).invoke(this, new Byte((byte) 10))).intValue();
                if (i != -1) {
                    found = true;
                    chunk = (i + 1) - ((Integer) BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]).invoke(this, new Object[0])).intValue();
                } else {
                    chunk = ((Integer) BufferedLineReaderInputStream.class.getMethod("length", new Class[0]).invoke(this, new Object[0])).intValue();
                }
                if (chunk > 0) {
                    Method method = BufferedLineReaderInputStream.class.getMethod("buf", new Class[0]);
                    int intValue = ((Integer) BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]).invoke(this, new Object[0])).intValue();
                    Class[] clsArr2 = {byte[].class, Integer.TYPE, Integer.TYPE};
                    ByteArrayBuffer.class.getMethod("append", clsArr2).invoke(dst, (byte[]) method.invoke(this, new Object[0]), new Integer(intValue), new Integer(chunk));
                    Class[] clsArr3 = {Integer.TYPE};
                    ((Integer) BufferedLineReaderInputStream.class.getMethod("skip", clsArr3).invoke(this, new Integer(chunk))).intValue();
                    total += chunk;
                }
                if (this.maxLineLen > 0) {
                    if (((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(dst, new Object[0])).intValue() >= this.maxLineLen) {
                        throw new MaxLineLimitException("Maximum line length limit exceeded");
                    }
                }
            }
            if (total == 0 && bytesRead == -1) {
                return -1;
            }
            return total;
        }
    }

    public int indexOf(byte[] pattern, int off, int len) {
        if (pattern == null) {
            throw new IllegalArgumentException("Pattern may not be null");
        } else if (off < this.bufpos || len < 0 || off + len > this.buflen) {
            throw new IndexOutOfBoundsException();
        } else if (len < pattern.length) {
            return -1;
        } else {
            int[] shiftTable = new int[256];
            for (int i = 0; i < shiftTable.length; i++) {
                shiftTable[i] = pattern.length + 1;
            }
            for (int i2 = 0; i2 < pattern.length; i2++) {
                shiftTable[pattern[i2] & 255] = pattern.length - i2;
            }
            int j = 0;
            while (j <= len - pattern.length) {
                int cur = off + j;
                boolean match = true;
                int i3 = 0;
                while (true) {
                    if (i3 >= pattern.length) {
                        break;
                    } else if (this.buffer[cur + i3] != pattern[i3]) {
                        match = false;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (match) {
                    return cur;
                }
                int pos = cur + pattern.length;
                if (pos >= this.buffer.length) {
                    break;
                }
                j += shiftTable[this.buffer[pos] & 255];
            }
            return -1;
        }
    }

    public int indexOf(byte[] pattern) {
        int i = this.bufpos;
        Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
        return ((Integer) BufferedLineReaderInputStream.class.getMethod("indexOf", clsArr).invoke(this, pattern, new Integer(i), new Integer(this.buflen - this.bufpos))).intValue();
    }

    public int indexOf(byte b, int off, int len) {
        if (off < this.bufpos || len < 0 || off + len > this.buflen) {
            throw new IndexOutOfBoundsException();
        }
        for (int i = off; i < off + len; i++) {
            if (this.buffer[i] == b) {
                return i;
            }
        }
        return -1;
    }

    public int indexOf(byte b) {
        int i = this.bufpos;
        Class[] clsArr = {Byte.TYPE, Integer.TYPE, Integer.TYPE};
        return ((Integer) BufferedLineReaderInputStream.class.getMethod("indexOf", clsArr).invoke(this, new Byte(b), new Integer(i), new Integer(this.buflen - this.bufpos))).intValue();
    }

    public byte charAt(int pos) {
        if (pos >= this.bufpos && pos <= this.buflen) {
            return this.buffer[pos];
        }
        throw new IndexOutOfBoundsException();
    }

    public byte[] buf() {
        return this.buffer;
    }

    public int pos() {
        return this.bufpos;
    }

    public int limit() {
        return this.buflen;
    }

    public int length() {
        return this.buflen - this.bufpos;
    }

    public int capacity() {
        return this.buffer.length;
    }

    public int skip(int n) {
        int chunk = ((Integer) Math.class.getMethod("min", Integer.TYPE, Integer.TYPE).invoke(null, new Integer(n), new Integer(this.buflen - this.bufpos))).intValue();
        this.bufpos += chunk;
        return chunk;
    }

    public void clear() {
        this.bufpos = 0;
        this.buflen = 0;
    }

    public String toString() {
        StringBuilder buffer2 = new StringBuilder();
        Object[] objArr = {"[pos: "};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer2, objArr);
        int i = this.bufpos;
        Class[] clsArr = {Integer.TYPE};
        StringBuilder.class.getMethod("append", clsArr).invoke(buffer2, new Integer(i));
        Object[] objArr2 = {"]"};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer2, objArr2);
        Object[] objArr3 = {"[limit: "};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer2, objArr3);
        int i2 = this.buflen;
        Class[] clsArr2 = {Integer.TYPE};
        StringBuilder.class.getMethod("append", clsArr2).invoke(buffer2, new Integer(i2));
        Object[] objArr4 = {"]"};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer2, objArr4);
        Object[] objArr5 = {"["};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer2, objArr5);
        for (int i3 = this.bufpos; i3 < this.buflen; i3++) {
            Class[] clsArr3 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr3).invoke(buffer2, new Character((char) this.buffer[i3]));
        }
        Object[] objArr6 = {"]"};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer2, objArr6);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(buffer2, new Object[0]);
    }
}
