package org.apache.james.mime4j.util;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public class StringArrayMap implements Serializable {
    private static final long serialVersionUID = -5833051164281786907L;
    private final Map<String, Object> map = new HashMap();

    public static String asString(Object pValue) {
        if (pValue == null) {
            return null;
        }
        if (pValue instanceof String) {
            return (String) pValue;
        }
        if (pValue instanceof String[]) {
            return ((String[]) pValue)[0];
        }
        if (pValue instanceof List) {
            Class[] clsArr = {Integer.TYPE};
            Object[] objArr = {new Integer(0)};
            return (String) List.class.getMethod("get", clsArr).invoke((List) pValue, objArr);
        }
        StringBuilder sb = new StringBuilder();
        Class[] clsArr2 = {String.class};
        Object[] objArr2 = {"Invalid parameter class: "};
        Object[] objArr3 = {((Class) Object.class.getMethod("getClass", new Class[0]).invoke(pValue, new Object[0])).getName()};
        Method method = StringBuilder.class.getMethod("append", String.class);
        throw new IllegalStateException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr2).invoke(sb, objArr2), objArr3), new Object[0]));
    }

    public static String[] asStringArray(Object pValue) {
        if (pValue == null) {
            return null;
        }
        if (pValue instanceof String) {
            return new String[]{(String) pValue};
        } else if (pValue instanceof String[]) {
            return (String[]) pValue;
        } else {
            if (pValue instanceof List) {
                List<?> l = (List) pValue;
                Class[] clsArr = {Object[].class};
                return (String[]) ((Object[]) List.class.getMethod("toArray", clsArr).invoke(l, new String[((Integer) List.class.getMethod("size", new Class[0]).invoke(l, new Object[0])).intValue()]));
            }
            StringBuilder sb = new StringBuilder();
            Class[] clsArr2 = {String.class};
            Object[] objArr = {"Invalid parameter class: "};
            Object[] objArr2 = {((Class) Object.class.getMethod("getClass", new Class[0]).invoke(pValue, new Object[0])).getName()};
            Method method = StringBuilder.class.getMethod("append", String.class);
            throw new IllegalStateException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr2).invoke(sb, objArr), objArr2), new Object[0]));
        }
    }

    public static Enumeration<String> asStringEnum(final Object pValue) {
        if (pValue == null) {
            return null;
        }
        if (pValue instanceof String) {
            return new Enumeration<String>() {
                private Object value = pValue;

                public boolean hasMoreElements() {
                    return this.value != null;
                }

                public String nextElement() {
                    if (this.value == null) {
                        throw new NoSuchElementException();
                    }
                    String s = (String) this.value;
                    this.value = null;
                    return s;
                }
            };
        }
        if (pValue instanceof String[]) {
            final String[] values = (String[]) pValue;
            return new Enumeration<String>() {
                private int offset;

                public boolean hasMoreElements() {
                    return this.offset < values.length;
                }

                public String nextElement() {
                    if (this.offset >= values.length) {
                        throw new NoSuchElementException();
                    }
                    String[] strArr = values;
                    int i = this.offset;
                    this.offset = i + 1;
                    return strArr[i];
                }
            };
        } else if (pValue instanceof List) {
            return (Enumeration) Collections.class.getMethod("enumeration", Collection.class).invoke(null, (List) pValue);
        } else {
            StringBuilder sb = new StringBuilder();
            Class[] clsArr = {String.class};
            Object[] objArr = {"Invalid parameter class: "};
            Object[] objArr2 = {((Class) Object.class.getMethod("getClass", new Class[0]).invoke(pValue, new Object[0])).getName()};
            Method method = StringBuilder.class.getMethod("append", String.class);
            throw new IllegalStateException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), new Object[0]));
        }
    }

    public static Map<String, String[]> asMap(Map<String, Object> pMap) {
        Map<String, String[]> result = new HashMap<>(((Integer) Map.class.getMethod("size", new Class[0]).invoke(pMap, new Object[0])).intValue());
        Method method = Map.class.getMethod("entrySet", new Class[0]);
        Iterator i$ = (Iterator) Set.class.getMethod("iterator", new Class[0]).invoke((Set) method.invoke(pMap, new Object[0]), new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Map.Entry<String, Object> entry = (Map.Entry) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0]);
                Object[] objArr = {Map.Entry.class.getMethod("getValue", new Class[0]).invoke(entry, new Object[0])};
                Method method2 = Map.Entry.class.getMethod("getKey", new Class[0]);
                Object[] objArr2 = {method2.invoke(entry, new Object[0]), (String[]) StringArrayMap.class.getMethod("asStringArray", Object.class).invoke(null, objArr)};
                Map.class.getMethod("put", Object.class, Object.class).invoke(result, objArr2);
            } else {
                return (Map) Collections.class.getMethod("unmodifiableMap", Map.class).invoke(null, result);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void addMapValue(Map<String, Object> pMap, String pName, String pValue) {
        Object o = pMap.get(pName);
        if (o == null) {
            o = pValue;
        } else if (o instanceof String) {
            List<Object> list = new ArrayList<>();
            list.add(o);
            list.add(pValue);
            o = list;
        } else if (o instanceof List) {
            ((List) o).add(pValue);
        } else if (o instanceof String[]) {
            List<String> list2 = new ArrayList<>();
            for (String str : (String[]) o) {
                list2.add(str);
            }
            list2.add(pValue);
            o = list2;
        } else {
            throw new IllegalStateException("Invalid object type: " + o.getClass().getName());
        }
        pMap.put(pName, o);
    }

    /* access modifiers changed from: protected */
    public String convertName(String pName) {
        return (String) String.class.getMethod("toLowerCase", new Class[0]).invoke(pName, new Object[0]);
    }

    public String getValue(String pName) {
        Object[] objArr = {pName};
        Object[] objArr2 = {(String) StringArrayMap.class.getMethod("convertName", String.class).invoke(this, objArr)};
        Class[] clsArr = {Object.class};
        return (String) StringArrayMap.class.getMethod("asString", clsArr).invoke(null, Map.class.getMethod("get", Object.class).invoke(this.map, objArr2));
    }

    public String[] getValues(String pName) {
        Object[] objArr = {pName};
        Object[] objArr2 = {(String) StringArrayMap.class.getMethod("convertName", String.class).invoke(this, objArr)};
        Class[] clsArr = {Object.class};
        return (String[]) StringArrayMap.class.getMethod("asStringArray", clsArr).invoke(null, Map.class.getMethod("get", Object.class).invoke(this.map, objArr2));
    }

    public Enumeration<String> getValueEnum(String pName) {
        Object[] objArr = {pName};
        Object[] objArr2 = {(String) StringArrayMap.class.getMethod("convertName", String.class).invoke(this, objArr)};
        Class[] clsArr = {Object.class};
        return (Enumeration) StringArrayMap.class.getMethod("asStringEnum", clsArr).invoke(null, Map.class.getMethod("get", Object.class).invoke(this.map, objArr2));
    }

    public Enumeration<String> getNames() {
        Class[] clsArr = {Collection.class};
        return (Enumeration) Collections.class.getMethod("enumeration", clsArr).invoke(null, (Set) Map.class.getMethod("keySet", new Class[0]).invoke(this.map, new Object[0]));
    }

    public Map<String, String[]> getMap() {
        Class[] clsArr = {Map.class};
        return (Map) StringArrayMap.class.getMethod("asMap", clsArr).invoke(null, this.map);
    }

    public void addValue(String pName, String pValue) {
        Object[] objArr = {pName};
        Object[] objArr2 = {this.map, (String) StringArrayMap.class.getMethod("convertName", String.class).invoke(this, objArr), pValue};
        StringArrayMap.class.getMethod("addMapValue", Map.class, String.class, String.class).invoke(this, objArr2);
    }

    public String[] getNameArray() {
        Collection<String> c = (Set) Map.class.getMethod("keySet", new Class[0]).invoke(this.map, new Object[0]);
        Class[] clsArr = {Object[].class};
        return (String[]) ((Object[]) Collection.class.getMethod("toArray", clsArr).invoke(c, new String[((Integer) Collection.class.getMethod("size", new Class[0]).invoke(c, new Object[0])).intValue()]));
    }
}
