package org.apache.james.mime4j.codec;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class QuotedPrintableOutputStream extends FilterOutputStream {
    private boolean closed = false;
    private QuotedPrintableEncoder encoder;

    public QuotedPrintableOutputStream(OutputStream out, boolean binary) {
        super(out);
        this.encoder = new QuotedPrintableEncoder(1024, binary);
        Object[] objArr = {out};
        QuotedPrintableEncoder.class.getMethod("initEncoding", OutputStream.class).invoke(this.encoder, objArr);
    }

    public void close() throws IOException {
        if (!this.closed) {
            try {
                QuotedPrintableEncoder.class.getMethod("completeEncoding", new Class[0]).invoke(this.encoder, new Object[0]);
            } finally {
                this.closed = true;
            }
        }
    }

    public void flush() throws IOException {
        QuotedPrintableEncoder.class.getMethod("flushOutput", new Class[0]).invoke(this.encoder, new Object[0]);
    }

    public void write(int b) throws IOException {
        Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
        QuotedPrintableOutputStream.class.getMethod("write", clsArr).invoke(this, new byte[]{(byte) b}, new Integer(0), new Integer(1));
    }

    public void write(byte[] b, int off, int len) throws IOException {
        if (this.closed) {
            throw new IOException("QuotedPrintableOutputStream has been closed");
        }
        QuotedPrintableEncoder quotedPrintableEncoder = this.encoder;
        Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
        QuotedPrintableEncoder.class.getMethod("encodeChunk", clsArr).invoke(quotedPrintableEncoder, b, new Integer(off), new Integer(len));
    }
}
