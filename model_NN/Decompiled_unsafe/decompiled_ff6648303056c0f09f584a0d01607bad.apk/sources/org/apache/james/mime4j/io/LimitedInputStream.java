package org.apache.james.mime4j.io;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;

public class LimitedInputStream extends PositionInputStream {
    private final long limit;

    public LimitedInputStream(InputStream instream, long limit2) {
        super(instream);
        if (limit2 < 0) {
            throw new IllegalArgumentException("Limit may not be negative");
        }
        this.limit = limit2;
    }

    private void enforceLimit() throws IOException {
        if (this.position >= this.limit) {
            throw new IOException("Input stream limit exceeded");
        }
    }

    public int read() throws IOException {
        LimitedInputStream.class.getMethod("enforceLimit", new Class[0]).invoke(this, new Object[0]);
        return super.read();
    }

    public int read(byte[] b, int off, int len) throws IOException {
        LimitedInputStream.class.getMethod("enforceLimit", new Class[0]).invoke(this, new Object[0]);
        int intValue = ((Integer) LimitedInputStream.class.getMethod("getBytesLeft", new Class[0]).invoke(this, new Object[0])).intValue();
        Class[] clsArr = {Integer.TYPE, Integer.TYPE};
        return super.read(b, off, ((Integer) Math.class.getMethod("min", clsArr).invoke(null, new Integer(len), new Integer(intValue))).intValue());
    }

    public long skip(long n) throws IOException {
        LimitedInputStream.class.getMethod("enforceLimit", new Class[0]).invoke(this, new Object[0]);
        Method method = LimitedInputStream.class.getMethod("getBytesLeft", new Class[0]);
        Class[] clsArr = {Long.TYPE, Long.TYPE};
        return super.skip(((Long) Math.class.getMethod("min", clsArr).invoke(null, new Long(n), new Long((long) ((Integer) method.invoke(this, new Object[0])).intValue()))).longValue());
    }

    private int getBytesLeft() {
        Class[] clsArr = {Long.TYPE, Long.TYPE};
        return (int) ((Long) Math.class.getMethod("min", clsArr).invoke(null, new Long(2147483647L), new Long(this.limit - this.position))).longValue();
    }
}
