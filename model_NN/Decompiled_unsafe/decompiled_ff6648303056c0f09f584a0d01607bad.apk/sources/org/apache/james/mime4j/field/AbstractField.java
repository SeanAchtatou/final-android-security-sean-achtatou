package org.apache.james.mime4j.field;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.util.MimeUtil;

public abstract class AbstractField implements ParsedField {
    private static final Pattern FIELD_NAME_PATTERN;
    private static final DefaultFieldParser parser = new DefaultFieldParser();
    private final String body;
    private final String name;
    private final ByteSequence raw;

    static {
        Object[] objArr = {"^([\\x21-\\x39\\x3b-\\x7e]+):"};
        FIELD_NAME_PATTERN = (Pattern) Pattern.class.getMethod("compile", String.class).invoke(null, objArr);
    }

    protected AbstractField(String name2, String body2, ByteSequence raw2) {
        this.name = name2;
        this.body = body2;
        this.raw = raw2;
    }

    public static ParsedField parse(ByteSequence raw2) throws MimeException {
        Object[] objArr = {raw2};
        Class[] clsArr = {ByteSequence.class, String.class};
        return (ParsedField) AbstractField.class.getMethod("parse", clsArr).invoke(null, raw2, (String) ContentUtil.class.getMethod("decode", ByteSequence.class).invoke(null, objArr));
    }

    public static ParsedField parse(String rawStr) throws MimeException {
        Object[] objArr = {rawStr};
        Class[] clsArr = {ByteSequence.class, String.class};
        return (ParsedField) AbstractField.class.getMethod("parse", clsArr).invoke(null, (ByteSequence) ContentUtil.class.getMethod("encode", String.class).invoke(null, objArr), rawStr);
    }

    public static DefaultFieldParser getParser() {
        return parser;
    }

    public String getName() {
        return this.name;
    }

    public ByteSequence getRaw() {
        return this.raw;
    }

    public String getBody() {
        return this.body;
    }

    public boolean isValidField() {
        return ((ParseException) AbstractField.class.getMethod("getParseException", new Class[0]).invoke(this, new Object[0])) == null;
    }

    public ParseException getParseException() {
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr = {this.name};
        Object[] objArr2 = {": "};
        Method method = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr3 = {this.body};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), new Object[0]);
    }

    private static ParsedField parse(ByteSequence raw2, String rawStr) throws MimeException {
        Object[] objArr = {rawStr};
        String unfolded = (String) MimeUtil.class.getMethod("unfold", String.class).invoke(null, objArr);
        Object[] objArr2 = {unfolded};
        Matcher fieldMatcher = (Matcher) Pattern.class.getMethod("matcher", CharSequence.class).invoke(FIELD_NAME_PATTERN, objArr2);
        if (!((Boolean) Matcher.class.getMethod("find", new Class[0]).invoke(fieldMatcher, new Object[0])).booleanValue()) {
            throw new MimeException("Invalid field in string");
        }
        Class[] clsArr = {Integer.TYPE};
        String name2 = (String) Matcher.class.getMethod("group", clsArr).invoke(fieldMatcher, new Integer(1));
        int intValue = ((Integer) Matcher.class.getMethod("end", new Class[0]).invoke(fieldMatcher, new Object[0])).intValue();
        Class[] clsArr2 = {Integer.TYPE};
        String body2 = (String) String.class.getMethod("substring", clsArr2).invoke(unfolded, new Integer(intValue));
        if (((Integer) String.class.getMethod("length", new Class[0]).invoke(body2, new Object[0])).intValue() > 0) {
            Class[] clsArr3 = {Integer.TYPE};
            if (((Character) String.class.getMethod("charAt", clsArr3).invoke(body2, new Integer(0))).charValue() == ' ') {
                Class[] clsArr4 = {Integer.TYPE};
                body2 = (String) String.class.getMethod("substring", clsArr4).invoke(body2, new Integer(1));
            }
        }
        return (ParsedField) DefaultFieldParser.class.getMethod("parse", String.class, String.class, ByteSequence.class).invoke(parser, name2, body2, raw2);
    }
}
