package org.apache.james.mime4j.codec;

import java.util.Iterator;

public class ByteQueue implements Iterable<Byte> {
    private UnboundedFifoByteBuffer buf;
    private int initialCapacity;

    public ByteQueue() {
        this.initialCapacity = -1;
        this.buf = new UnboundedFifoByteBuffer();
    }

    public ByteQueue(int initialCapacity2) {
        this.initialCapacity = -1;
        this.buf = new UnboundedFifoByteBuffer(initialCapacity2);
        this.initialCapacity = initialCapacity2;
    }

    public void enqueue(byte b) {
        UnboundedFifoByteBuffer unboundedFifoByteBuffer = this.buf;
        Class[] clsArr = {Byte.TYPE};
        ((Boolean) UnboundedFifoByteBuffer.class.getMethod("add", clsArr).invoke(unboundedFifoByteBuffer, new Byte(b))).booleanValue();
    }

    public byte dequeue() {
        return ((Byte) UnboundedFifoByteBuffer.class.getMethod("remove", new Class[0]).invoke(this.buf, new Object[0])).byteValue();
    }

    public int count() {
        return ((Integer) UnboundedFifoByteBuffer.class.getMethod("size", new Class[0]).invoke(this.buf, new Object[0])).intValue();
    }

    public void clear() {
        if (this.initialCapacity != -1) {
            this.buf = new UnboundedFifoByteBuffer(this.initialCapacity);
        } else {
            this.buf = new UnboundedFifoByteBuffer();
        }
    }

    public Iterator<Byte> iterator() {
        return (Iterator) UnboundedFifoByteBuffer.class.getMethod("iterator", new Class[0]).invoke(this.buf, new Object[0]);
    }
}
