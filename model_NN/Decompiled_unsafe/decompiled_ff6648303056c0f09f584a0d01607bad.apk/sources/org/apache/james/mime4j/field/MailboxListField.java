package org.apache.james.mime4j.field;

import java.lang.reflect.Method;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.field.address.AddressList;
import org.apache.james.mime4j.field.address.MailboxList;
import org.apache.james.mime4j.field.address.parser.ParseException;
import org.apache.james.mime4j.util.ByteSequence;

public class MailboxListField extends AbstractField {
    static final FieldParser PARSER = new FieldParser() {
        public ParsedField parse(String name, String body, ByteSequence raw) {
            return new MailboxListField(name, body, raw);
        }
    };
    private static Log log;
    private MailboxList mailboxList;
    private ParseException parseException;
    private boolean parsed = false;

    static {
        Object[] objArr = {MailboxListField.class};
        log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
    }

    MailboxListField(String name, String body, ByteSequence raw) {
        super(name, body, raw);
    }

    public MailboxList getMailboxList() {
        if (!this.parsed) {
            MailboxListField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return this.mailboxList;
    }

    public ParseException getParseException() {
        if (!this.parsed) {
            MailboxListField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return this.parseException;
    }

    private void parse() {
        String body = (String) MailboxListField.class.getMethod("getBody", new Class[0]).invoke(this, new Object[0]);
        try {
            Object[] objArr = {body};
            Method method = AddressList.class.getMethod("flatten", new Class[0]);
            this.mailboxList = (MailboxList) method.invoke((AddressList) AddressList.class.getMethod("parse", String.class).invoke(null, objArr), new Object[0]);
        } catch (ParseException e) {
            if (((Boolean) Log.class.getMethod("isDebugEnabled", new Class[0]).invoke(log, new Object[0])).booleanValue()) {
                Log log2 = log;
                Object[] objArr2 = {"Parsing value '"};
                Object[] objArr3 = {body};
                Method method2 = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr4 = {"': "};
                Method method3 = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr5 = {(String) ParseException.class.getMethod("getMessage", new Class[0]).invoke(e, new Object[0])};
                Method method4 = StringBuilder.class.getMethod("append", String.class);
                Method method5 = StringBuilder.class.getMethod("toString", new Class[0]);
                Object[] objArr6 = {(String) method5.invoke((StringBuilder) method4.invoke((StringBuilder) method3.invoke((StringBuilder) method2.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr2), objArr3), objArr4), objArr5), new Object[0])};
                Log.class.getMethod("debug", Object.class).invoke(log2, objArr6);
            }
            this.parseException = e;
        }
        this.parsed = true;
    }
}
