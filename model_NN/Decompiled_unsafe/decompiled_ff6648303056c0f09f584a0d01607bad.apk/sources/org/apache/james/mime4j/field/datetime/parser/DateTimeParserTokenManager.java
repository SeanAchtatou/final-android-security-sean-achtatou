package org.apache.james.mime4j.field.datetime.parser;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Method;

public class DateTimeParserTokenManager implements DateTimeParserConstants {
    static int commentNest;
    static final long[] jjbitVec0 = {0, 0, -1, -1};
    public static final int[] jjnewLexState = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 0, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1};
    static final int[] jjnextStates = new int[0];
    public static final String[] jjstrLiteralImages = {"", "\r", "\n", ",", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ":", null, "UT", "GMT", "EST", "EDT", "CST", "CDT", "MST", "MDT", "PST", "PDT", null, null, null, null, null, null, null, null, null, null, null, null, null, null};
    static final long[] jjtoMore = {69956427317248L};
    static final long[] jjtoSkip = {343597383680L};
    static final long[] jjtoSpecial = {68719476736L};
    static final long[] jjtoToken = {70437463654399L};
    public static final String[] lexStateNames = {"DEFAULT", "INCOMMENT", "NESTED_COMMENT"};
    protected char curChar;
    int curLexState;
    public PrintStream debugStream;
    int defaultLexState;
    StringBuffer image;
    protected SimpleCharStream input_stream;
    int jjimageLen;
    int jjmatchedKind;
    int jjmatchedPos;
    int jjnewStateCnt;
    int jjround;
    private final int[] jjrounds;
    private final int[] jjstateSet;
    int lengthOfMatch;

    public void setDebugStream(PrintStream ds) {
        this.debugStream = ds;
    }

    private final int jjStopStringLiteralDfa_0(int pos, long active0) {
        switch (pos) {
            case 0:
                if ((34334373872L & active0) != 0) {
                    this.jjmatchedKind = 35;
                    break;
                }
                break;
            case 1:
                if ((34334373872L & active0) != 0 && this.jjmatchedPos == 0) {
                    this.jjmatchedKind = 35;
                    this.jjmatchedPos = 0;
                    break;
                }
        }
        return -1;
    }

    private final int jjStartNfa_0(int pos, long active0) {
        Class[] clsArr = {Integer.TYPE, Long.TYPE};
        int intValue = ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopStringLiteralDfa_0", clsArr).invoke(this, new Integer(pos), new Long(active0))).intValue();
        Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveNfa_0", clsArr2).invoke(this, new Integer(intValue), new Integer(pos + 1))).intValue();
    }

    private final int jjStopAtPos(int pos, int kind) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        return pos + 1;
    }

    private final int jjStartNfaWithStates_0(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = ((Character) SimpleCharStream.class.getMethod("readChar", new Class[0]).invoke(this.input_stream, new Object[0])).charValue();
            Class[] clsArr = {Integer.TYPE, Integer.TYPE};
            return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveNfa_0", clsArr).invoke(this, new Integer(state), new Integer(pos + 1))).intValue();
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int jjMoveStringLiteralDfa0_0() {
        switch (this.curChar) {
            case 10:
                Class[] clsArr = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr).invoke(this, new Integer(0), new Integer(2))).intValue();
            case 13:
                Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr2).invoke(this, new Integer(0), new Integer(1))).intValue();
            case '(':
                Class[] clsArr3 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr3).invoke(this, new Integer(0), new Integer(37))).intValue();
            case ',':
                Class[] clsArr4 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr4).invoke(this, new Integer(0), new Integer(3))).intValue();
            case ':':
                Class[] clsArr5 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr5).invoke(this, new Integer(0), new Integer(23))).intValue();
            case 'A':
                Class[] clsArr6 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr6).invoke(this, new Long(278528))).intValue();
            case 'C':
                Class[] clsArr7 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr7).invoke(this, new Long(1610612736))).intValue();
            case 'D':
                Class[] clsArr8 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr8).invoke(this, new Long(4194304))).intValue();
            case 'E':
                Class[] clsArr9 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr9).invoke(this, new Long(402653184))).intValue();
            case 'F':
                Class[] clsArr10 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr10).invoke(this, new Long(4352))).intValue();
            case 'G':
                Class[] clsArr11 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr11).invoke(this, new Long(67108864))).intValue();
            case 'J':
                Class[] clsArr12 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr12).invoke(this, new Long(198656))).intValue();
            case 'M':
                Class[] clsArr13 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr13).invoke(this, new Long(6442491920L))).intValue();
            case 'N':
                Class[] clsArr14 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr14).invoke(this, new Long(2097152))).intValue();
            case 'O':
                Class[] clsArr15 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr15).invoke(this, new Long(1048576))).intValue();
            case 'P':
                Class[] clsArr16 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr16).invoke(this, new Long(25769803776L))).intValue();
            case 'S':
                Class[] clsArr17 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr17).invoke(this, new Long(525824))).intValue();
            case 'T':
                Class[] clsArr18 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr18).invoke(this, new Long(160))).intValue();
            case 'U':
                Class[] clsArr19 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr19).invoke(this, new Long(33554432))).intValue();
            case 'W':
                Class[] clsArr20 = {Long.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa1_0", clsArr20).invoke(this, new Long(64))).intValue();
            default:
                Class[] clsArr21 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveNfa_0", clsArr21).invoke(this, new Integer(0), new Integer(0))).intValue();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final int jjMoveStringLiteralDfa1_0(long active0) {
        try {
            this.curChar = ((Character) SimpleCharStream.class.getMethod("readChar", new Class[0]).invoke(this.input_stream, new Object[0])).charValue();
            switch (this.curChar) {
                case 'D':
                    Class[] clsArr = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr).invoke(this, new Long(active0), new Long(22817013760L))).intValue();
                case 'M':
                    Class[] clsArr2 = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr2).invoke(this, new Long(active0), new Long(67108864))).intValue();
                case 'S':
                    Class[] clsArr3 = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr3).invoke(this, new Long(active0), new Long(11408506880L))).intValue();
                case 'T':
                    if ((33554432 & active0) != 0) {
                        Class[] clsArr4 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr4).invoke(this, new Integer(1), new Integer(25))).intValue();
                    }
                    break;
                case 'a':
                    Class[] clsArr5 = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr5).invoke(this, new Long(active0), new Long(43520))).intValue();
                case 'c':
                    Class[] clsArr6 = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr6).invoke(this, new Long(active0), new Long(1048576))).intValue();
                case 'e':
                    Class[] clsArr7 = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr7).invoke(this, new Long(active0), new Long(4722752))).intValue();
                case 'h':
                    Class[] clsArr8 = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr8).invoke(this, new Long(active0), new Long(128))).intValue();
                case 'o':
                    Class[] clsArr9 = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr9).invoke(this, new Long(active0), new Long(2097168))).intValue();
                case 'p':
                    Class[] clsArr10 = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr10).invoke(this, new Long(active0), new Long(16384))).intValue();
                case 'r':
                    Class[] clsArr11 = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr11).invoke(this, new Long(active0), new Long(256))).intValue();
                case 'u':
                    Class[] clsArr12 = {Long.TYPE, Long.TYPE};
                    return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveStringLiteralDfa2_0", clsArr12).invoke(this, new Long(active0), new Long(459808))).intValue();
            }
            Class[] clsArr13 = {Integer.TYPE, Long.TYPE};
            return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStartNfa_0", clsArr13).invoke(this, new Integer(0), new Long(active0))).intValue();
        } catch (IOException e) {
            Class[] clsArr14 = {Integer.TYPE, Long.TYPE};
            ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopStringLiteralDfa_0", clsArr14).invoke(this, new Integer(0), new Long(active0))).intValue();
            return 1;
        }
    }

    private final int jjMoveStringLiteralDfa2_0(long old0, long active0) {
        long active02 = active0 & old0;
        if (active02 == 0) {
            Class[] clsArr = {Integer.TYPE, Long.TYPE};
            return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStartNfa_0", clsArr).invoke(this, new Integer(0), new Long(old0))).intValue();
        }
        try {
            this.curChar = ((Character) SimpleCharStream.class.getMethod("readChar", new Class[0]).invoke(this.input_stream, new Object[0])).charValue();
            switch (this.curChar) {
                case 'T':
                    if ((67108864 & active02) != 0) {
                        Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr2).invoke(this, new Integer(2), new Integer(26))).intValue();
                    } else if ((134217728 & active02) != 0) {
                        Class[] clsArr3 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr3).invoke(this, new Integer(2), new Integer(27))).intValue();
                    } else if ((268435456 & active02) != 0) {
                        Class[] clsArr4 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr4).invoke(this, new Integer(2), new Integer(28))).intValue();
                    } else if ((536870912 & active02) != 0) {
                        Class[] clsArr5 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr5).invoke(this, new Integer(2), new Integer(29))).intValue();
                    } else if ((1073741824 & active02) != 0) {
                        Class[] clsArr6 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr6).invoke(this, new Integer(2), new Integer(30))).intValue();
                    } else if ((2147483648L & active02) != 0) {
                        Class[] clsArr7 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr7).invoke(this, new Integer(2), new Integer(31))).intValue();
                    } else if ((4294967296L & active02) != 0) {
                        Class[] clsArr8 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr8).invoke(this, new Integer(2), new Integer(32))).intValue();
                    } else if ((8589934592L & active02) != 0) {
                        Class[] clsArr9 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr9).invoke(this, new Integer(2), new Integer(33))).intValue();
                    } else if ((17179869184L & active02) != 0) {
                        Class[] clsArr10 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr10).invoke(this, new Integer(2), new Integer(34))).intValue();
                    }
                    break;
                case 'b':
                    if ((4096 & active02) != 0) {
                        Class[] clsArr11 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr11).invoke(this, new Integer(2), new Integer(12))).intValue();
                    }
                    break;
                case 'c':
                    if ((4194304 & active02) != 0) {
                        Class[] clsArr12 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr12).invoke(this, new Integer(2), new Integer(22))).intValue();
                    }
                    break;
                case 'd':
                    if ((64 & active02) != 0) {
                        Class[] clsArr13 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr13).invoke(this, new Integer(2), new Integer(6))).intValue();
                    }
                    break;
                case 'e':
                    if ((32 & active02) != 0) {
                        Class[] clsArr14 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr14).invoke(this, new Integer(2), new Integer(5))).intValue();
                    }
                    break;
                case 'g':
                    if ((262144 & active02) != 0) {
                        Class[] clsArr15 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr15).invoke(this, new Integer(2), new Integer(18))).intValue();
                    }
                    break;
                case 'i':
                    if ((256 & active02) != 0) {
                        Class[] clsArr16 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr16).invoke(this, new Integer(2), new Integer(8))).intValue();
                    }
                    break;
                case 'l':
                    if ((131072 & active02) != 0) {
                        Class[] clsArr17 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr17).invoke(this, new Integer(2), new Integer(17))).intValue();
                    }
                    break;
                case 'n':
                    if ((16 & active02) != 0) {
                        Class[] clsArr18 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr18).invoke(this, new Integer(2), new Integer(4))).intValue();
                    } else if ((1024 & active02) != 0) {
                        Class[] clsArr19 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr19).invoke(this, new Integer(2), new Integer(10))).intValue();
                    } else if ((2048 & active02) != 0) {
                        Class[] clsArr20 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr20).invoke(this, new Integer(2), new Integer(11))).intValue();
                    } else if ((65536 & active02) != 0) {
                        Class[] clsArr21 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr21).invoke(this, new Integer(2), new Integer(16))).intValue();
                    }
                    break;
                case 'p':
                    if ((524288 & active02) != 0) {
                        Class[] clsArr22 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr22).invoke(this, new Integer(2), new Integer(19))).intValue();
                    }
                    break;
                case 'r':
                    if ((8192 & active02) != 0) {
                        Class[] clsArr23 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr23).invoke(this, new Integer(2), new Integer(13))).intValue();
                    } else if ((16384 & active02) != 0) {
                        Class[] clsArr24 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr24).invoke(this, new Integer(2), new Integer(14))).intValue();
                    }
                    break;
                case 't':
                    if ((512 & active02) != 0) {
                        Class[] clsArr25 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr25).invoke(this, new Integer(2), new Integer(9))).intValue();
                    } else if ((1048576 & active02) != 0) {
                        Class[] clsArr26 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr26).invoke(this, new Integer(2), new Integer(20))).intValue();
                    }
                    break;
                case 'u':
                    if ((128 & active02) != 0) {
                        Class[] clsArr27 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr27).invoke(this, new Integer(2), new Integer(7))).intValue();
                    }
                    break;
                case 'v':
                    if ((2097152 & active02) != 0) {
                        Class[] clsArr28 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr28).invoke(this, new Integer(2), new Integer(21))).intValue();
                    }
                    break;
                case 'y':
                    if ((32768 & active02) != 0) {
                        Class[] clsArr29 = {Integer.TYPE, Integer.TYPE};
                        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr29).invoke(this, new Integer(2), new Integer(15))).intValue();
                    }
                    break;
            }
            Class[] clsArr30 = {Integer.TYPE, Long.TYPE};
            return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStartNfa_0", clsArr30).invoke(this, new Integer(1), new Long(active02))).intValue();
        } catch (IOException e) {
            Class[] clsArr31 = {Integer.TYPE, Long.TYPE};
            ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopStringLiteralDfa_0", clsArr31).invoke(this, new Integer(1), new Long(active02))).intValue();
            return 2;
        }
    }

    private final void jjCheckNAdd(int state) {
        if (this.jjrounds[state] != this.jjround) {
            int[] iArr = this.jjstateSet;
            int i = this.jjnewStateCnt;
            this.jjnewStateCnt = i + 1;
            iArr[i] = state;
            this.jjrounds[state] = this.jjround;
        }
    }

    private final void jjAddStates(int start, int end) {
        while (true) {
            int[] iArr = this.jjstateSet;
            int i = this.jjnewStateCnt;
            this.jjnewStateCnt = i + 1;
            iArr[i] = jjnextStates[start];
            int start2 = start + 1;
            if (start != end) {
                start = start2;
            } else {
                return;
            }
        }
    }

    private final void jjCheckNAddTwoStates(int state1, int state2) {
        Class[] clsArr = {Integer.TYPE};
        DateTimeParserTokenManager.class.getMethod("jjCheckNAdd", clsArr).invoke(this, new Integer(state1));
        Class[] clsArr2 = {Integer.TYPE};
        DateTimeParserTokenManager.class.getMethod("jjCheckNAdd", clsArr2).invoke(this, new Integer(state2));
    }

    private final void jjCheckNAddStates(int start, int end) {
        while (true) {
            int i = jjnextStates[start];
            Class[] clsArr = {Integer.TYPE};
            DateTimeParserTokenManager.class.getMethod("jjCheckNAdd", clsArr).invoke(this, new Integer(i));
            int start2 = start + 1;
            if (start != end) {
                start = start2;
            } else {
                return;
            }
        }
    }

    private final void jjCheckNAddStates(int start) {
        int i = jjnextStates[start];
        Class[] clsArr = {Integer.TYPE};
        DateTimeParserTokenManager.class.getMethod("jjCheckNAdd", clsArr).invoke(this, new Integer(i));
        int i2 = jjnextStates[start + 1];
        Class[] clsArr2 = {Integer.TYPE};
        DateTimeParserTokenManager.class.getMethod("jjCheckNAdd", clsArr2).invoke(this, new Integer(i2));
    }

    private final int jjMoveNfa_0(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 4;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long l = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((287948901175001088L & l) != 0) {
                                if (kind > 46) {
                                    kind = 46;
                                }
                                jjCheckNAdd(3);
                                continue;
                            } else if ((4294967808L & l) != 0) {
                                if (kind > 36) {
                                    kind = 36;
                                }
                                jjCheckNAdd(2);
                                continue;
                            } else if ((43980465111040L & l) != 0 && kind > 24) {
                                kind = 24;
                                continue;
                            }
                        case 2:
                            if ((4294967808L & l) != 0) {
                                kind = 36;
                                jjCheckNAdd(2);
                                continue;
                            } else {
                                continue;
                            }
                        case 3:
                            if ((287948901175001088L & l) != 0) {
                                kind = 46;
                                jjCheckNAdd(3);
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((576456345801194494L & l2) != 0) {
                                kind = 35;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i3 = (this.curChar & 255) >> 6;
                long j = 1 << (this.curChar & '?');
                do {
                    i--;
                    int i4 = this.jjstateSet[i];
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 4 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int jjStopStringLiteralDfa_1(int pos, long active0) {
        return -1;
    }

    private final int jjStartNfa_1(int pos, long active0) {
        Class[] clsArr = {Integer.TYPE, Long.TYPE};
        int intValue = ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopStringLiteralDfa_1", clsArr).invoke(this, new Integer(pos), new Long(active0))).intValue();
        Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveNfa_1", clsArr2).invoke(this, new Integer(intValue), new Integer(pos + 1))).intValue();
    }

    private final int jjStartNfaWithStates_1(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = ((Character) SimpleCharStream.class.getMethod("readChar", new Class[0]).invoke(this.input_stream, new Object[0])).charValue();
            Class[] clsArr = {Integer.TYPE, Integer.TYPE};
            return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveNfa_1", clsArr).invoke(this, new Integer(state), new Integer(pos + 1))).intValue();
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int jjMoveStringLiteralDfa0_1() {
        switch (this.curChar) {
            case '(':
                Class[] clsArr = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr).invoke(this, new Integer(0), new Integer(40))).intValue();
            case ')':
                Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr2).invoke(this, new Integer(0), new Integer(38))).intValue();
            default:
                Class[] clsArr3 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveNfa_1", clsArr3).invoke(this, new Integer(0), new Integer(0))).intValue();
        }
    }

    private final int jjMoveNfa_1(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 3;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long j = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 41) {
                                kind = 41;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 39) {
                                kind = 39;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long j2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 41) {
                                kind = 41;
                            }
                            if (this.curChar == '\\') {
                                int[] iArr = this.jjstateSet;
                                int i3 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i3 + 1;
                                iArr[i3] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 39) {
                                kind = 39;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if (kind > 41) {
                                kind = 41;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i22 = (this.curChar & 255) >> 6;
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 41) {
                                kind = 41;
                                continue;
                            }
                        case 1:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 39) {
                                kind = 39;
                                continue;
                            }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 3 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int jjStopStringLiteralDfa_2(int pos, long active0) {
        return -1;
    }

    private final int jjStartNfa_2(int pos, long active0) {
        Class[] clsArr = {Integer.TYPE, Long.TYPE};
        int intValue = ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopStringLiteralDfa_2", clsArr).invoke(this, new Integer(pos), new Long(active0))).intValue();
        Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
        return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveNfa_2", clsArr2).invoke(this, new Integer(intValue), new Integer(pos + 1))).intValue();
    }

    private final int jjStartNfaWithStates_2(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = ((Character) SimpleCharStream.class.getMethod("readChar", new Class[0]).invoke(this.input_stream, new Object[0])).charValue();
            Class[] clsArr = {Integer.TYPE, Integer.TYPE};
            return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveNfa_2", clsArr).invoke(this, new Integer(state), new Integer(pos + 1))).intValue();
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int jjMoveStringLiteralDfa0_2() {
        switch (this.curChar) {
            case '(':
                Class[] clsArr = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr).invoke(this, new Integer(0), new Integer(43))).intValue();
            case ')':
                Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjStopAtPos", clsArr2).invoke(this, new Integer(0), new Integer(44))).intValue();
            default:
                Class[] clsArr3 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) DateTimeParserTokenManager.class.getMethod("jjMoveNfa_2", clsArr3).invoke(this, new Integer(0), new Integer(0))).intValue();
        }
    }

    private final int jjMoveNfa_2(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 3;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long j = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 45) {
                                kind = 45;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 42) {
                                kind = 42;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long j2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 45) {
                                kind = 45;
                            }
                            if (this.curChar == '\\') {
                                int[] iArr = this.jjstateSet;
                                int i3 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i3 + 1;
                                iArr[i3] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 42) {
                                kind = 42;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if (kind > 45) {
                                kind = 45;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i22 = (this.curChar & 255) >> 6;
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 45) {
                                kind = 45;
                                continue;
                            }
                        case 1:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 42) {
                                kind = 42;
                                continue;
                            }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 3 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    public DateTimeParserTokenManager(SimpleCharStream stream) {
        this.debugStream = System.out;
        this.jjrounds = new int[4];
        this.jjstateSet = new int[8];
        this.curLexState = 0;
        this.defaultLexState = 0;
        this.input_stream = stream;
    }

    public DateTimeParserTokenManager(SimpleCharStream stream, int lexState) {
        this(stream);
        Class[] clsArr = {Integer.TYPE};
        DateTimeParserTokenManager.class.getMethod("SwitchTo", clsArr).invoke(this, new Integer(lexState));
    }

    public void ReInit(SimpleCharStream stream) {
        this.jjnewStateCnt = 0;
        this.jjmatchedPos = 0;
        this.curLexState = this.defaultLexState;
        this.input_stream = stream;
        DateTimeParserTokenManager.class.getMethod("ReInitRounds", new Class[0]).invoke(this, new Object[0]);
    }

    private final void ReInitRounds() {
        this.jjround = -2147483647;
        int i = 4;
        while (true) {
            int i2 = i;
            i = i2 - 1;
            if (i2 > 0) {
                this.jjrounds[i] = Integer.MIN_VALUE;
            } else {
                return;
            }
        }
    }

    public void ReInit(SimpleCharStream stream, int lexState) {
        Object[] objArr = {stream};
        DateTimeParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(this, objArr);
        Class[] clsArr = {Integer.TYPE};
        DateTimeParserTokenManager.class.getMethod("SwitchTo", clsArr).invoke(this, new Integer(lexState));
    }

    public void SwitchTo(int lexState) {
        if (lexState >= 3 || lexState < 0) {
            StringBuilder sb = new StringBuilder();
            Class[] clsArr = {String.class};
            Object[] objArr = {"Error: Ignoring invalid lexical state : "};
            Class[] clsArr2 = {Integer.TYPE};
            Object[] objArr2 = {new Integer(lexState)};
            Method method = StringBuilder.class.getMethod("append", clsArr2);
            Object[] objArr3 = {". State unchanged."};
            Method method2 = StringBuilder.class.getMethod("append", String.class);
            throw new TokenMgrError((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), new Object[0]), 2);
        }
        this.curLexState = lexState;
    }

    /* access modifiers changed from: protected */
    public Token jjFillToken() {
        int i = this.jjmatchedKind;
        Class[] clsArr = {Integer.TYPE};
        Token t = (Token) Token.class.getMethod("newToken", clsArr).invoke(null, new Integer(i));
        t.kind = this.jjmatchedKind;
        String im = jjstrLiteralImages[this.jjmatchedKind];
        if (im == null) {
            im = (String) SimpleCharStream.class.getMethod("GetImage", new Class[0]).invoke(this.input_stream, new Object[0]);
        }
        t.image = im;
        t.beginLine = ((Integer) SimpleCharStream.class.getMethod("getBeginLine", new Class[0]).invoke(this.input_stream, new Object[0])).intValue();
        t.beginColumn = ((Integer) SimpleCharStream.class.getMethod("getBeginColumn", new Class[0]).invoke(this.input_stream, new Object[0])).intValue();
        t.endLine = ((Integer) SimpleCharStream.class.getMethod("getEndLine", new Class[0]).invoke(this.input_stream, new Object[0])).intValue();
        t.endColumn = ((Integer) SimpleCharStream.class.getMethod("getEndColumn", new Class[0]).invoke(this.input_stream, new Object[0])).intValue();
        return t;
    }

    public Token getNextToken() {
        Token specialToken = null;
        int curPos = 0;
        while (true) {
            try {
                this.curChar = this.input_stream.BeginToken();
                this.image = null;
                this.jjimageLen = 0;
                while (true) {
                    switch (this.curLexState) {
                        case 0:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_0();
                            break;
                        case 1:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_1();
                            break;
                        case 2:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_2();
                            break;
                    }
                    if (this.jjmatchedKind != Integer.MAX_VALUE) {
                        if (this.jjmatchedPos + 1 < curPos) {
                            this.input_stream.backup((curPos - this.jjmatchedPos) - 1);
                        }
                        if ((jjtoToken[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                            Token matchedToken = jjFillToken();
                            matchedToken.specialToken = specialToken;
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                            return matchedToken;
                        } else if ((jjtoSkip[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                            if ((jjtoSpecial[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                                Token matchedToken2 = jjFillToken();
                                if (specialToken == null) {
                                    specialToken = matchedToken2;
                                } else {
                                    matchedToken2.specialToken = specialToken;
                                    specialToken.next = matchedToken2;
                                    specialToken = matchedToken2;
                                }
                            }
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                        } else {
                            MoreLexicalActions();
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                            curPos = 0;
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            try {
                                this.curChar = this.input_stream.readChar();
                            } catch (IOException e) {
                            }
                        }
                    }
                }
            } catch (IOException e2) {
                this.jjmatchedKind = 0;
                Token matchedToken3 = jjFillToken();
                matchedToken3.specialToken = specialToken;
                return matchedToken3;
            }
        }
        int error_line = this.input_stream.getEndLine();
        int error_column = this.input_stream.getEndColumn();
        String error_after = null;
        boolean EOFSeen = false;
        try {
            this.input_stream.readChar();
            this.input_stream.backup(1);
        } catch (IOException e3) {
            EOFSeen = true;
            error_after = curPos <= 1 ? "" : this.input_stream.GetImage();
            if (this.curChar == 10 || this.curChar == 13) {
                error_line++;
                error_column = 0;
            } else {
                error_column++;
            }
        }
        if (!EOFSeen) {
            this.input_stream.backup(1);
            if (curPos <= 1) {
                error_after = "";
            } else {
                error_after = this.input_stream.GetImage();
            }
        }
        throw new TokenMgrError(EOFSeen, this.curLexState, error_line, error_column, error_after, this.curChar, 0);
    }

    /* access modifiers changed from: package-private */
    public void MoreLexicalActions() {
        int i = this.jjimageLen;
        int i2 = this.jjmatchedPos + 1;
        this.lengthOfMatch = i2;
        this.jjimageLen = i + i2;
        switch (this.jjmatchedKind) {
            case 39:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer = this.image;
                SimpleCharStream simpleCharStream = this.input_stream;
                int i3 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream, new Integer(i3)));
                this.jjimageLen = 0;
                StringBuffer stringBuffer2 = this.image;
                Method method = StringBuffer.class.getMethod("length", new Class[0]);
                StringBuffer.class.getMethod("deleteCharAt", Integer.TYPE).invoke(stringBuffer2, new Integer(((Integer) method.invoke(this.image, new Object[0])).intValue() - 2));
                return;
            case 40:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer3 = this.image;
                SimpleCharStream simpleCharStream2 = this.input_stream;
                int i4 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer3, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream2, new Integer(i4)));
                this.jjimageLen = 0;
                commentNest = 1;
                return;
            case 41:
            default:
                return;
            case 42:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer4 = this.image;
                SimpleCharStream simpleCharStream3 = this.input_stream;
                int i5 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer4, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream3, new Integer(i5)));
                this.jjimageLen = 0;
                StringBuffer stringBuffer5 = this.image;
                Method method2 = StringBuffer.class.getMethod("length", new Class[0]);
                StringBuffer.class.getMethod("deleteCharAt", Integer.TYPE).invoke(stringBuffer5, new Integer(((Integer) method2.invoke(this.image, new Object[0])).intValue() - 2));
                return;
            case 43:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer6 = this.image;
                SimpleCharStream simpleCharStream4 = this.input_stream;
                int i6 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer6, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream4, new Integer(i6)));
                this.jjimageLen = 0;
                commentNest++;
                return;
            case 44:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer7 = this.image;
                SimpleCharStream simpleCharStream5 = this.input_stream;
                int i7 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer7, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream5, new Integer(i7)));
                this.jjimageLen = 0;
                commentNest--;
                if (commentNest == 0) {
                    DateTimeParserTokenManager.class.getMethod("SwitchTo", Integer.TYPE).invoke(this, new Integer(1));
                    return;
                }
                return;
        }
    }
}
