package org.apache.james.mime4j.parser;

import java.lang.reflect.Method;

public final class MimeEntityConfig implements Cloneable {
    private boolean countLineNumbers = false;
    private long maxContentLen = -1;
    private int maxHeaderCount = 1000;
    private int maxLineLen = 1000;
    private boolean maximalBodyDescriptor = false;
    private boolean strictParsing = false;

    public boolean isMaximalBodyDescriptor() {
        return this.maximalBodyDescriptor;
    }

    public void setMaximalBodyDescriptor(boolean maximalBodyDescriptor2) {
        this.maximalBodyDescriptor = maximalBodyDescriptor2;
    }

    public void setStrictParsing(boolean strictParsing2) {
        this.strictParsing = strictParsing2;
    }

    public boolean isStrictParsing() {
        return this.strictParsing;
    }

    public void setMaxLineLen(int maxLineLen2) {
        this.maxLineLen = maxLineLen2;
    }

    public int getMaxLineLen() {
        return this.maxLineLen;
    }

    public void setMaxHeaderCount(int maxHeaderCount2) {
        this.maxHeaderCount = maxHeaderCount2;
    }

    public int getMaxHeaderCount() {
        return this.maxHeaderCount;
    }

    public void setMaxContentLen(long maxContentLen2) {
        this.maxContentLen = maxContentLen2;
    }

    public long getMaxContentLen() {
        return this.maxContentLen;
    }

    public void setCountLineNumbers(boolean countLineNumbers2) {
        this.countLineNumbers = countLineNumbers2;
    }

    public boolean isCountLineNumbers() {
        return this.countLineNumbers;
    }

    public MimeEntityConfig clone() {
        try {
            return (MimeEntityConfig) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr = {"[max body descriptor: "};
        boolean z = this.maximalBodyDescriptor;
        Class[] clsArr2 = {Boolean.TYPE};
        Object[] objArr2 = {new Boolean(z)};
        Method method = StringBuilder.class.getMethod("append", clsArr2);
        Object[] objArr3 = {", strict parsing: "};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        boolean z2 = this.strictParsing;
        Class[] clsArr3 = {Boolean.TYPE};
        Object[] objArr4 = {new Boolean(z2)};
        Method method3 = StringBuilder.class.getMethod("append", clsArr3);
        Object[] objArr5 = {", max line length: "};
        Method method4 = StringBuilder.class.getMethod("append", String.class);
        int i = this.maxLineLen;
        Class[] clsArr4 = {Integer.TYPE};
        Object[] objArr6 = {new Integer(i)};
        Method method5 = StringBuilder.class.getMethod("append", clsArr4);
        Object[] objArr7 = {", max header count: "};
        Method method6 = StringBuilder.class.getMethod("append", String.class);
        int i2 = this.maxHeaderCount;
        Class[] clsArr5 = {Integer.TYPE};
        Object[] objArr8 = {new Integer(i2)};
        Method method7 = StringBuilder.class.getMethod("append", clsArr5);
        Object[] objArr9 = {", max content length: "};
        Method method8 = StringBuilder.class.getMethod("append", String.class);
        long j = this.maxContentLen;
        Class[] clsArr6 = {Long.TYPE};
        Object[] objArr10 = {new Long(j)};
        Method method9 = StringBuilder.class.getMethod("append", clsArr6);
        Object[] objArr11 = {", count line numbers: "};
        Method method10 = StringBuilder.class.getMethod("append", String.class);
        boolean z3 = this.countLineNumbers;
        Class[] clsArr7 = {Boolean.TYPE};
        Object[] objArr12 = {new Boolean(z3)};
        Method method11 = StringBuilder.class.getMethod("append", clsArr7);
        Object[] objArr13 = {"]"};
        Method method12 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method12.invoke((StringBuilder) method11.invoke((StringBuilder) method10.invoke((StringBuilder) method9.invoke((StringBuilder) method8.invoke((StringBuilder) method7.invoke((StringBuilder) method6.invoke((StringBuilder) method5.invoke((StringBuilder) method4.invoke((StringBuilder) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), objArr4), objArr5), objArr6), objArr7), objArr8), objArr9), objArr10), objArr11), objArr12), objArr13), new Object[0]);
    }
}
