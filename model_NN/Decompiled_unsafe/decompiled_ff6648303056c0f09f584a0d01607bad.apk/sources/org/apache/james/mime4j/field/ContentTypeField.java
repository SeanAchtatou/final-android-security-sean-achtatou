package org.apache.james.mime4j.field;

import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser;
import org.apache.james.mime4j.field.contenttype.parser.ParseException;
import org.apache.james.mime4j.field.contenttype.parser.TokenMgrError;
import org.apache.james.mime4j.util.ByteSequence;

public class ContentTypeField extends AbstractField {
    public static final String PARAM_BOUNDARY = "boundary";
    public static final String PARAM_CHARSET = "charset";
    static final FieldParser PARSER = new FieldParser() {
        public ParsedField parse(String name, String body, ByteSequence raw) {
            return new ContentTypeField(name, body, raw);
        }
    };
    public static final String TYPE_MESSAGE_RFC822 = "message/rfc822";
    public static final String TYPE_MULTIPART_DIGEST = "multipart/digest";
    public static final String TYPE_MULTIPART_PREFIX = "multipart/";
    public static final String TYPE_TEXT_PLAIN = "text/plain";
    private static Log log;
    private String mimeType = "";
    private Map<String, String> parameters = new HashMap();
    private ParseException parseException;
    private boolean parsed = false;

    static {
        Object[] objArr = {ContentTypeField.class};
        log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
    }

    ContentTypeField(String name, String body, ByteSequence raw) {
        super(name, body, raw);
    }

    public ParseException getParseException() {
        if (!this.parsed) {
            ContentTypeField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return this.parseException;
    }

    public String getMimeType() {
        if (!this.parsed) {
            ContentTypeField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return this.mimeType;
    }

    public String getParameter(String name) {
        if (!this.parsed) {
            ContentTypeField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        Class[] clsArr = {Object.class};
        return (String) Map.class.getMethod("get", clsArr).invoke(this.parameters, (String) String.class.getMethod("toLowerCase", new Class[0]).invoke(name, new Object[0]));
    }

    public Map<String, String> getParameters() {
        if (!this.parsed) {
            ContentTypeField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        Class[] clsArr = {Map.class};
        return (Map) Collections.class.getMethod("unmodifiableMap", clsArr).invoke(null, this.parameters);
    }

    public boolean isMimeType(String mimeType2) {
        if (!this.parsed) {
            ContentTypeField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return ((Boolean) String.class.getMethod("equalsIgnoreCase", String.class).invoke(this.mimeType, mimeType2)).booleanValue();
    }

    public boolean isMultipart() {
        if (!this.parsed) {
            ContentTypeField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        Class[] clsArr = {String.class};
        return ((Boolean) String.class.getMethod("startsWith", clsArr).invoke(this.mimeType, TYPE_MULTIPART_PREFIX)).booleanValue();
    }

    public String getBoundary() {
        Class[] clsArr = {String.class};
        return (String) ContentTypeField.class.getMethod("getParameter", clsArr).invoke(this, PARAM_BOUNDARY);
    }

    public String getCharset() {
        Class[] clsArr = {String.class};
        return (String) ContentTypeField.class.getMethod("getParameter", clsArr).invoke(this, PARAM_CHARSET);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x005a, code lost:
        if (((java.lang.String) org.apache.james.mime4j.field.ContentTypeField.class.getMethod("getBoundary", new java.lang.Class[0]).invoke(r5, new java.lang.Object[0])) == null) goto L_0x005c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getMimeType(org.apache.james.mime4j.field.ContentTypeField r5, org.apache.james.mime4j.field.ContentTypeField r6) {
        /*
            if (r5 == 0) goto L_0x005c
            r1 = 0
            java.lang.Class[] r2 = new java.lang.Class[r1]
            java.lang.Object[] r3 = new java.lang.Object[r1]
            java.lang.String r1 = "getMimeType"
            java.lang.Class<org.apache.james.mime4j.field.ContentTypeField> r4 = org.apache.james.mime4j.field.ContentTypeField.class
            java.lang.reflect.Method r1 = r4.getMethod(r1, r2)
            java.lang.Object r0 = r1.invoke(r5, r3)
            java.lang.String r0 = (java.lang.String) r0
            r1 = 0
            java.lang.Class[] r2 = new java.lang.Class[r1]
            java.lang.Object[] r3 = new java.lang.Object[r1]
            java.lang.String r1 = "length"
            java.lang.Class<java.lang.String> r4 = java.lang.String.class
            java.lang.reflect.Method r1 = r4.getMethod(r1, r2)
            java.lang.Object r1 = r1.invoke(r0, r3)
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r0 = r1.intValue()
            if (r0 == 0) goto L_0x005c
            r1 = 0
            java.lang.Class[] r2 = new java.lang.Class[r1]
            java.lang.Object[] r3 = new java.lang.Object[r1]
            java.lang.String r1 = "isMultipart"
            java.lang.Class<org.apache.james.mime4j.field.ContentTypeField> r4 = org.apache.james.mime4j.field.ContentTypeField.class
            java.lang.reflect.Method r1 = r4.getMethod(r1, r2)
            java.lang.Object r1 = r1.invoke(r5, r3)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r0 = r1.booleanValue()
            if (r0 == 0) goto L_0x0086
            r1 = 0
            java.lang.Class[] r2 = new java.lang.Class[r1]
            java.lang.Object[] r3 = new java.lang.Object[r1]
            java.lang.String r1 = "getBoundary"
            java.lang.Class<org.apache.james.mime4j.field.ContentTypeField> r4 = org.apache.james.mime4j.field.ContentTypeField.class
            java.lang.reflect.Method r1 = r4.getMethod(r1, r2)
            java.lang.Object r0 = r1.invoke(r5, r3)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto L_0x0086
        L_0x005c:
            if (r6 == 0) goto L_0x0083
            java.lang.String r0 = "multipart/digest"
            r1 = 1
            java.lang.Class[] r2 = new java.lang.Class[r1]
            java.lang.Object[] r3 = new java.lang.Object[r1]
            r1 = 0
            java.lang.Class<java.lang.String> r4 = java.lang.String.class
            r2[r1] = r4
            r3[r1] = r0
            java.lang.String r1 = "isMimeType"
            java.lang.Class<org.apache.james.mime4j.field.ContentTypeField> r4 = org.apache.james.mime4j.field.ContentTypeField.class
            java.lang.reflect.Method r1 = r4.getMethod(r1, r2)
            java.lang.Object r1 = r1.invoke(r6, r3)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r0 = r1.booleanValue()
            if (r0 == 0) goto L_0x0083
            java.lang.String r0 = "message/rfc822"
        L_0x0082:
            return r0
        L_0x0083:
            java.lang.String r0 = "text/plain"
            goto L_0x0082
        L_0x0086:
            r1 = 0
            java.lang.Class[] r2 = new java.lang.Class[r1]
            java.lang.Object[] r3 = new java.lang.Object[r1]
            java.lang.String r1 = "getMimeType"
            java.lang.Class<org.apache.james.mime4j.field.ContentTypeField> r4 = org.apache.james.mime4j.field.ContentTypeField.class
            java.lang.reflect.Method r1 = r4.getMethod(r1, r2)
            java.lang.Object r0 = r1.invoke(r5, r3)
            java.lang.String r0 = (java.lang.String) r0
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.ContentTypeField.getMimeType(org.apache.james.mime4j.field.ContentTypeField, org.apache.james.mime4j.field.ContentTypeField):java.lang.String");
    }

    public static String getCharset(ContentTypeField f) {
        if (f != null) {
            String charset = (String) ContentTypeField.class.getMethod("getCharset", new Class[0]).invoke(f, new Object[0]);
            if (charset != null) {
                if (((Integer) String.class.getMethod("length", new Class[0]).invoke(charset, new Object[0])).intValue() > 0) {
                    return charset;
                }
            }
        }
        return "us-ascii";
    }

    private void parse() {
        String body = getBody();
        ContentTypeParser parser = new ContentTypeParser(new StringReader(body));
        try {
            parser.parseAll();
        } catch (ParseException e) {
            if (log.isDebugEnabled()) {
                log.debug("Parsing value '" + body + "': " + e.getMessage());
            }
            this.parseException = e;
        } catch (TokenMgrError e2) {
            if (log.isDebugEnabled()) {
                log.debug("Parsing value '" + body + "': " + e2.getMessage());
            }
            this.parseException = new ParseException(e2.getMessage());
        }
        String type = parser.getType();
        String subType = parser.getSubType();
        if (!(type == null || subType == null)) {
            this.mimeType = (type + "/" + subType).toLowerCase();
            List<String> paramNames = parser.getParamNames();
            List<String> paramValues = parser.getParamValues();
            if (!(paramNames == null || paramValues == null)) {
                int len = Math.min(paramNames.size(), paramValues.size());
                for (int i = 0; i < len; i++) {
                    this.parameters.put(paramNames.get(i).toLowerCase(), paramValues.get(i));
                }
            }
        }
        this.parsed = true;
    }
}
