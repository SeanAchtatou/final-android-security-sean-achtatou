package org.apache.james.mime4j.field.contentdisposition.parser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;

public class SimpleCharStream {
    public static final boolean staticFlag = false;
    int available;
    protected int[] bufcolumn;
    protected char[] buffer;
    protected int[] bufline;
    public int bufpos;
    int bufsize;
    protected int column;
    protected int inBuf;
    protected Reader inputStream;
    protected int line;
    protected int maxNextCharInd;
    protected boolean prevCharIsCR;
    protected boolean prevCharIsLF;
    protected int tabSize;
    int tokenBegin;

    /* access modifiers changed from: protected */
    public void setTabSize(int i) {
        this.tabSize = i;
    }

    /* access modifiers changed from: protected */
    public int getTabSize(int i) {
        return this.tabSize;
    }

    /* access modifiers changed from: protected */
    public void ExpandBuff(boolean wrapAround) {
        char[] newbuffer = new char[(this.bufsize + 2048)];
        int[] newbufline = new int[(this.bufsize + 2048)];
        int[] newbufcolumn = new int[(this.bufsize + 2048)];
        if (wrapAround) {
            try {
                char[] cArr = this.buffer;
                int i = this.tokenBegin;
                System.class.getMethod("arraycopy", Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE).invoke(null, cArr, new Integer(i), newbuffer, new Integer(0), new Integer(this.bufsize - this.tokenBegin));
                char[] cArr2 = this.buffer;
                int i2 = this.bufsize - this.tokenBegin;
                int i3 = this.bufpos;
                System.class.getMethod("arraycopy", Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE).invoke(null, cArr2, new Integer(0), newbuffer, new Integer(i2), new Integer(i3));
                this.buffer = newbuffer;
                int[] iArr = this.bufline;
                int i4 = this.tokenBegin;
                System.class.getMethod("arraycopy", Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE).invoke(null, iArr, new Integer(i4), newbufline, new Integer(0), new Integer(this.bufsize - this.tokenBegin));
                int[] iArr2 = this.bufline;
                int i5 = this.bufsize - this.tokenBegin;
                int i6 = this.bufpos;
                System.class.getMethod("arraycopy", Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE).invoke(null, iArr2, new Integer(0), newbufline, new Integer(i5), new Integer(i6));
                this.bufline = newbufline;
                int[] iArr3 = this.bufcolumn;
                int i7 = this.tokenBegin;
                System.class.getMethod("arraycopy", Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE).invoke(null, iArr3, new Integer(i7), newbufcolumn, new Integer(0), new Integer(this.bufsize - this.tokenBegin));
                int[] iArr4 = this.bufcolumn;
                int i8 = this.bufsize - this.tokenBegin;
                int i9 = this.bufpos;
                System.class.getMethod("arraycopy", Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE).invoke(null, iArr4, new Integer(0), newbufcolumn, new Integer(i8), new Integer(i9));
                this.bufcolumn = newbufcolumn;
                int i10 = this.bufpos + (this.bufsize - this.tokenBegin);
                this.bufpos = i10;
                this.maxNextCharInd = i10;
            } catch (Throwable t) {
                throw new Error((String) Throwable.class.getMethod("getMessage", new Class[0]).invoke(t, new Object[0]));
            }
        } else {
            char[] cArr3 = this.buffer;
            int i11 = this.tokenBegin;
            System.class.getMethod("arraycopy", Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE).invoke(null, cArr3, new Integer(i11), newbuffer, new Integer(0), new Integer(this.bufsize - this.tokenBegin));
            this.buffer = newbuffer;
            int[] iArr5 = this.bufline;
            int i12 = this.tokenBegin;
            System.class.getMethod("arraycopy", Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE).invoke(null, iArr5, new Integer(i12), newbufline, new Integer(0), new Integer(this.bufsize - this.tokenBegin));
            this.bufline = newbufline;
            int[] iArr6 = this.bufcolumn;
            int i13 = this.tokenBegin;
            System.class.getMethod("arraycopy", Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE).invoke(null, iArr6, new Integer(i13), newbufcolumn, new Integer(0), new Integer(this.bufsize - this.tokenBegin));
            this.bufcolumn = newbufcolumn;
            int i14 = this.bufpos - this.tokenBegin;
            this.bufpos = i14;
            this.maxNextCharInd = i14;
        }
        this.bufsize += 2048;
        this.available = this.bufsize;
        this.tokenBegin = 0;
    }

    /* access modifiers changed from: protected */
    public void FillBuff() throws IOException {
        if (this.maxNextCharInd == this.available) {
            if (this.available == this.bufsize) {
                if (this.tokenBegin > 2048) {
                    this.maxNextCharInd = 0;
                    this.bufpos = 0;
                    this.available = this.tokenBegin;
                } else if (this.tokenBegin < 0) {
                    this.maxNextCharInd = 0;
                    this.bufpos = 0;
                } else {
                    SimpleCharStream.class.getMethod("ExpandBuff", Boolean.TYPE).invoke(this, new Boolean(false));
                }
            } else if (this.available > this.tokenBegin) {
                this.available = this.bufsize;
            } else if (this.tokenBegin - this.available < 2048) {
                SimpleCharStream.class.getMethod("ExpandBuff", Boolean.TYPE).invoke(this, new Boolean(true));
            } else {
                this.available = this.tokenBegin;
            }
        }
        try {
            Reader reader = this.inputStream;
            char[] cArr = this.buffer;
            int i = this.maxNextCharInd;
            int i2 = ((Integer) Reader.class.getMethod("read", char[].class, Integer.TYPE, Integer.TYPE).invoke(reader, cArr, new Integer(i), new Integer(this.available - this.maxNextCharInd))).intValue();
            if (i2 == -1) {
                Reader.class.getMethod("close", new Class[0]).invoke(this.inputStream, new Object[0]);
                throw new IOException();
            }
            this.maxNextCharInd += i2;
        } catch (IOException e) {
            this.bufpos--;
            SimpleCharStream.class.getMethod("backup", Integer.TYPE).invoke(this, new Integer(0));
            if (this.tokenBegin == -1) {
                this.tokenBegin = this.bufpos;
            }
            throw e;
        }
    }

    public char BeginToken() throws IOException {
        this.tokenBegin = -1;
        char c = ((Character) SimpleCharStream.class.getMethod("readChar", new Class[0]).invoke(this, new Object[0])).charValue();
        this.tokenBegin = this.bufpos;
        return c;
    }

    /* access modifiers changed from: protected */
    public void UpdateLineColumn(char c) {
        this.column++;
        if (this.prevCharIsLF) {
            this.prevCharIsLF = false;
            int i = this.line;
            this.column = 1;
            this.line = i + 1;
        } else if (this.prevCharIsCR) {
            this.prevCharIsCR = false;
            if (c == 10) {
                this.prevCharIsLF = true;
            } else {
                int i2 = this.line;
                this.column = 1;
                this.line = i2 + 1;
            }
        }
        switch (c) {
            case 9:
                this.column--;
                this.column += this.tabSize - (this.column % this.tabSize);
                break;
            case 10:
                this.prevCharIsLF = true;
                break;
            case 13:
                this.prevCharIsCR = true;
                break;
        }
        this.bufline[this.bufpos] = this.line;
        this.bufcolumn[this.bufpos] = this.column;
    }

    public char readChar() throws IOException {
        if (this.inBuf > 0) {
            this.inBuf--;
            int i = this.bufpos + 1;
            this.bufpos = i;
            if (i == this.bufsize) {
                this.bufpos = 0;
            }
            return this.buffer[this.bufpos];
        }
        int i2 = this.bufpos + 1;
        this.bufpos = i2;
        if (i2 >= this.maxNextCharInd) {
            SimpleCharStream.class.getMethod("FillBuff", new Class[0]).invoke(this, new Object[0]);
        }
        char c = this.buffer[this.bufpos];
        SimpleCharStream.class.getMethod("UpdateLineColumn", Character.TYPE).invoke(this, new Character(c));
        return c;
    }

    public int getColumn() {
        return this.bufcolumn[this.bufpos];
    }

    public int getLine() {
        return this.bufline[this.bufpos];
    }

    public int getEndColumn() {
        return this.bufcolumn[this.bufpos];
    }

    public int getEndLine() {
        return this.bufline[this.bufpos];
    }

    public int getBeginColumn() {
        return this.bufcolumn[this.tokenBegin];
    }

    public int getBeginLine() {
        return this.bufline[this.tokenBegin];
    }

    public void backup(int amount) {
        this.inBuf += amount;
        int i = this.bufpos - amount;
        this.bufpos = i;
        if (i < 0) {
            this.bufpos += this.bufsize;
        }
    }

    public SimpleCharStream(Reader dstream, int startline, int startcolumn, int buffersize) {
        this.bufpos = -1;
        this.column = 0;
        this.line = 1;
        this.prevCharIsCR = false;
        this.prevCharIsLF = false;
        this.maxNextCharInd = 0;
        this.inBuf = 0;
        this.tabSize = 8;
        this.inputStream = dstream;
        this.line = startline;
        this.column = startcolumn - 1;
        this.bufsize = buffersize;
        this.available = buffersize;
        this.buffer = new char[buffersize];
        this.bufline = new int[buffersize];
        this.bufcolumn = new int[buffersize];
    }

    public SimpleCharStream(Reader dstream, int startline, int startcolumn) {
        this(dstream, startline, startcolumn, 4096);
    }

    public SimpleCharStream(Reader dstream) {
        this(dstream, 1, 1, 4096);
    }

    public void ReInit(Reader dstream, int startline, int startcolumn, int buffersize) {
        this.inputStream = dstream;
        this.line = startline;
        this.column = startcolumn - 1;
        if (this.buffer == null || buffersize != this.buffer.length) {
            this.bufsize = buffersize;
            this.available = buffersize;
            this.buffer = new char[buffersize];
            this.bufline = new int[buffersize];
            this.bufcolumn = new int[buffersize];
        }
        this.prevCharIsCR = false;
        this.prevCharIsLF = false;
        this.maxNextCharInd = 0;
        this.inBuf = 0;
        this.tokenBegin = 0;
        this.bufpos = -1;
    }

    public void ReInit(Reader dstream, int startline, int startcolumn) {
        Class[] clsArr = {Reader.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(this, dstream, new Integer(startline), new Integer(startcolumn), new Integer(4096));
    }

    public void ReInit(Reader dstream) {
        Class[] clsArr = {Reader.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(this, dstream, new Integer(1), new Integer(1), new Integer(4096));
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SimpleCharStream(InputStream dstream, String encoding, int startline, int startcolumn, int buffersize) throws UnsupportedEncodingException {
        this(encoding == null ? new InputStreamReader(dstream) : new InputStreamReader(dstream, encoding), startline, startcolumn, buffersize);
    }

    public SimpleCharStream(InputStream dstream, int startline, int startcolumn, int buffersize) {
        this(new InputStreamReader(dstream), startline, startcolumn, buffersize);
    }

    public SimpleCharStream(InputStream dstream, String encoding, int startline, int startcolumn) throws UnsupportedEncodingException {
        this(dstream, encoding, startline, startcolumn, 4096);
    }

    public SimpleCharStream(InputStream dstream, int startline, int startcolumn) {
        this(dstream, startline, startcolumn, 4096);
    }

    public SimpleCharStream(InputStream dstream, String encoding) throws UnsupportedEncodingException {
        this(dstream, encoding, 1, 1, 4096);
    }

    public SimpleCharStream(InputStream dstream) {
        this(dstream, 1, 1, 4096);
    }

    public void ReInit(InputStream dstream, String encoding, int startline, int startcolumn, int buffersize) throws UnsupportedEncodingException {
        InputStreamReader inputStreamReader = encoding == null ? new InputStreamReader(dstream) : new InputStreamReader(dstream, encoding);
        SimpleCharStream.class.getMethod("ReInit", Reader.class, Integer.TYPE, Integer.TYPE, Integer.TYPE).invoke(this, inputStreamReader, new Integer(startline), new Integer(startcolumn), new Integer(buffersize));
    }

    public void ReInit(InputStream dstream, int startline, int startcolumn, int buffersize) {
        InputStreamReader inputStreamReader = new InputStreamReader(dstream);
        Class[] clsArr = {Reader.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(this, inputStreamReader, new Integer(startline), new Integer(startcolumn), new Integer(buffersize));
    }

    public void ReInit(InputStream dstream, String encoding) throws UnsupportedEncodingException {
        Class[] clsArr = {InputStream.class, String.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(this, dstream, encoding, new Integer(1), new Integer(1), new Integer(4096));
    }

    public void ReInit(InputStream dstream) {
        Class[] clsArr = {InputStream.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(this, dstream, new Integer(1), new Integer(1), new Integer(4096));
    }

    public void ReInit(InputStream dstream, String encoding, int startline, int startcolumn) throws UnsupportedEncodingException {
        Class[] clsArr = {InputStream.class, String.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(this, dstream, encoding, new Integer(startline), new Integer(startcolumn), new Integer(4096));
    }

    public void ReInit(InputStream dstream, int startline, int startcolumn) {
        Class[] clsArr = {InputStream.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(this, dstream, new Integer(startline), new Integer(startcolumn), new Integer(4096));
    }

    public String GetImage() {
        if (this.bufpos >= this.tokenBegin) {
            return new String(this.buffer, this.tokenBegin, (this.bufpos - this.tokenBegin) + 1);
        }
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr = {new String(this.buffer, this.tokenBegin, this.bufsize - this.tokenBegin)};
        Object[] objArr2 = {new String(this.buffer, 0, this.bufpos + 1)};
        Method method = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), new Object[0]);
    }

    public char[] GetSuffix(int len) {
        char[] ret = new char[len];
        if (this.bufpos + 1 >= len) {
            char[] cArr = this.buffer;
            Class[] clsArr = {Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE};
            System.class.getMethod("arraycopy", clsArr).invoke(null, cArr, new Integer((this.bufpos - len) + 1), ret, new Integer(0), new Integer(len));
        } else {
            char[] cArr2 = this.buffer;
            int i = this.bufsize - ((len - this.bufpos) - 1);
            Class[] clsArr2 = {Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE};
            System.class.getMethod("arraycopy", clsArr2).invoke(null, cArr2, new Integer(i), ret, new Integer(0), new Integer((len - this.bufpos) - 1));
            char[] cArr3 = this.buffer;
            Class[] clsArr3 = {Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE};
            System.class.getMethod("arraycopy", clsArr3).invoke(null, cArr3, new Integer(0), ret, new Integer((len - this.bufpos) - 1), new Integer(this.bufpos + 1));
        }
        return ret;
    }

    public void Done() {
        this.buffer = null;
        this.bufline = null;
        this.bufcolumn = null;
    }

    public void adjustBeginLineColumn(int newLine, int newCol) {
        int len;
        int start = this.tokenBegin;
        if (this.bufpos >= this.tokenBegin) {
            len = (this.bufpos - this.tokenBegin) + this.inBuf + 1;
        } else {
            len = (this.bufsize - this.tokenBegin) + this.bufpos + 1 + this.inBuf;
        }
        int i = 0;
        int j = 0;
        int columnDiff = 0;
        while (i < len) {
            int[] iArr = this.bufline;
            j = start % this.bufsize;
            int i2 = iArr[j];
            int[] iArr2 = this.bufline;
            start++;
            int k = start % this.bufsize;
            if (i2 != iArr2[k]) {
                break;
            }
            this.bufline[j] = newLine;
            int nextColDiff = (this.bufcolumn[k] + columnDiff) - this.bufcolumn[j];
            this.bufcolumn[j] = newCol + columnDiff;
            columnDiff = nextColDiff;
            i++;
        }
        if (i < len) {
            int newLine2 = newLine + 1;
            this.bufline[j] = newLine;
            this.bufcolumn[j] = newCol + columnDiff;
            int i3 = i;
            while (true) {
                int i4 = i3 + 1;
                if (i3 >= len) {
                    break;
                }
                int[] iArr3 = this.bufline;
                j = start % this.bufsize;
                start++;
                if (iArr3[j] != this.bufline[start % this.bufsize]) {
                    this.bufline[j] = newLine2;
                    i3 = i4;
                    newLine2++;
                } else {
                    this.bufline[j] = newLine2;
                    i3 = i4;
                }
            }
        }
        this.line = this.bufline[j];
        this.column = this.bufcolumn[j];
    }
}
