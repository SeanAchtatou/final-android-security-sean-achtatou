package org.apache.james.mime4j.codec;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class QuotedPrintableInputStream extends InputStream {
    private static Log log;
    ByteQueue byteq = new ByteQueue();
    private boolean closed = false;
    ByteQueue pushbackq = new ByteQueue();
    private byte state = 0;
    private InputStream stream;

    static {
        Object[] objArr = {QuotedPrintableInputStream.class};
        log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
    }

    public QuotedPrintableInputStream(InputStream stream2) {
        this.stream = stream2;
    }

    public void close() throws IOException {
        this.closed = true;
    }

    public int read() throws IOException {
        if (this.closed) {
            throw new IOException("QuotedPrintableInputStream has been closed");
        }
        QuotedPrintableInputStream.class.getMethod("fillBuffer", new Class[0]).invoke(this, new Object[0]);
        if (((Integer) ByteQueue.class.getMethod("count", new Class[0]).invoke(this.byteq, new Object[0])).intValue() == 0) {
            return -1;
        }
        byte val = ((Byte) ByteQueue.class.getMethod("dequeue", new Class[0]).invoke(this.byteq, new Object[0])).byteValue();
        return val < 0 ? val & 255 : val;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x008a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:2:0x001c A[LOOP:0: B:3:0x003b->B:2:0x001c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0057 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0077 A[SYNTHETIC] */
    private void populatePushbackQueue() throws java.io.IOException {
        /*
            r7 = this;
            org.apache.james.mime4j.codec.ByteQueue r1 = r7.pushbackq
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]
            java.lang.Object[] r5 = new java.lang.Object[r3]
            java.lang.String r3 = "count"
            java.lang.Class<org.apache.james.mime4j.codec.ByteQueue> r6 = org.apache.james.mime4j.codec.ByteQueue.class
            java.lang.reflect.Method r3 = r6.getMethod(r3, r4)
            java.lang.Object r3 = r3.invoke(r1, r5)
            java.lang.Integer r3 = (java.lang.Integer) r3
            int r1 = r3.intValue()
            if (r1 == 0) goto L_0x003b
        L_0x001b:
            return
        L_0x001c:
            org.apache.james.mime4j.codec.ByteQueue r1 = r7.pushbackq
            byte r2 = (byte) r0
            r3 = 1
            java.lang.Class[] r4 = new java.lang.Class[r3]
            java.lang.Object[] r5 = new java.lang.Object[r3]
            r3 = 0
            java.lang.Class r6 = java.lang.Byte.TYPE
            r4[r3] = r6
            java.lang.Byte r6 = new java.lang.Byte
            r6.<init>(r2)
            r5[r3] = r6
            java.lang.String r3 = "enqueue"
            java.lang.Class<org.apache.james.mime4j.codec.ByteQueue> r6 = org.apache.james.mime4j.codec.ByteQueue.class
            java.lang.reflect.Method r3 = r6.getMethod(r3, r4)
            r3.invoke(r1, r5)
        L_0x003b:
            java.io.InputStream r1 = r7.stream
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]
            java.lang.Object[] r5 = new java.lang.Object[r3]
            java.lang.String r3 = "read"
            java.lang.Class<java.io.InputStream> r6 = java.io.InputStream.class
            java.lang.reflect.Method r3 = r6.getMethod(r3, r4)
            java.lang.Object r3 = r3.invoke(r1, r5)
            java.lang.Integer r3 = (java.lang.Integer) r3
            int r0 = r3.intValue()
            switch(r0) {
                case -1: goto L_0x0077;
                case 9: goto L_0x001c;
                case 10: goto L_0x008a;
                case 13: goto L_0x008a;
                case 32: goto L_0x001c;
                default: goto L_0x0057;
            }
        L_0x0057:
            org.apache.james.mime4j.codec.ByteQueue r1 = r7.pushbackq
            byte r2 = (byte) r0
            r3 = 1
            java.lang.Class[] r4 = new java.lang.Class[r3]
            java.lang.Object[] r5 = new java.lang.Object[r3]
            r3 = 0
            java.lang.Class r6 = java.lang.Byte.TYPE
            r4[r3] = r6
            java.lang.Byte r6 = new java.lang.Byte
            r6.<init>(r2)
            r5[r3] = r6
            java.lang.String r3 = "enqueue"
            java.lang.Class<org.apache.james.mime4j.codec.ByteQueue> r6 = org.apache.james.mime4j.codec.ByteQueue.class
            java.lang.reflect.Method r3 = r6.getMethod(r3, r4)
            r3.invoke(r1, r5)
            goto L_0x001b
        L_0x0077:
            org.apache.james.mime4j.codec.ByteQueue r1 = r7.pushbackq
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]
            java.lang.Object[] r5 = new java.lang.Object[r3]
            java.lang.String r3 = "clear"
            java.lang.Class<org.apache.james.mime4j.codec.ByteQueue> r6 = org.apache.james.mime4j.codec.ByteQueue.class
            java.lang.reflect.Method r3 = r6.getMethod(r3, r4)
            r3.invoke(r1, r5)
            goto L_0x001b
        L_0x008a:
            org.apache.james.mime4j.codec.ByteQueue r1 = r7.pushbackq
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]
            java.lang.Object[] r5 = new java.lang.Object[r3]
            java.lang.String r3 = "clear"
            java.lang.Class<org.apache.james.mime4j.codec.ByteQueue> r6 = org.apache.james.mime4j.codec.ByteQueue.class
            java.lang.reflect.Method r3 = r6.getMethod(r3, r4)
            r3.invoke(r1, r5)
            org.apache.james.mime4j.codec.ByteQueue r1 = r7.pushbackq
            byte r2 = (byte) r0
            r3 = 1
            java.lang.Class[] r4 = new java.lang.Class[r3]
            java.lang.Object[] r5 = new java.lang.Object[r3]
            r3 = 0
            java.lang.Class r6 = java.lang.Byte.TYPE
            r4[r3] = r6
            java.lang.Byte r6 = new java.lang.Byte
            r6.<init>(r2)
            r5[r3] = r6
            java.lang.String r3 = "enqueue"
            java.lang.Class<org.apache.james.mime4j.codec.ByteQueue> r6 = org.apache.james.mime4j.codec.ByteQueue.class
            java.lang.reflect.Method r3 = r6.getMethod(r3, r4)
            r3.invoke(r1, r5)
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.codec.QuotedPrintableInputStream.populatePushbackQueue():void");
    }

    private void fillBuffer() throws IOException {
        byte msdChar = 0;
        while (this.byteq.count() == 0) {
            if (this.pushbackq.count() == 0) {
                populatePushbackQueue();
                if (this.pushbackq.count() == 0) {
                    return;
                }
            }
            byte b = this.pushbackq.dequeue();
            switch (this.state) {
                case 0:
                    if (b == 61) {
                        this.state = 1;
                        break;
                    } else {
                        this.byteq.enqueue(b);
                        break;
                    }
                case 1:
                    if (b != 13) {
                        if ((b < 48 || b > 57) && ((b < 65 || b > 70) && (b < 97 || b > 102))) {
                            if (b != 61) {
                                if (log.isWarnEnabled()) {
                                    log.warn("Malformed MIME; expected \\r or [0-9A-Z], got " + ((int) b));
                                }
                                this.state = 0;
                                this.byteq.enqueue((byte) 61);
                                this.byteq.enqueue(b);
                                break;
                            } else {
                                if (log.isWarnEnabled()) {
                                    log.warn("Malformed MIME; got ==");
                                }
                                this.byteq.enqueue((byte) 61);
                                break;
                            }
                        } else {
                            this.state = 3;
                            msdChar = b;
                            break;
                        }
                    } else {
                        this.state = 2;
                        break;
                    }
                    break;
                case 2:
                    if (b != 10) {
                        if (log.isWarnEnabled()) {
                            log.warn("Malformed MIME; expected 10, got " + ((int) b));
                        }
                        this.state = 0;
                        this.byteq.enqueue((byte) 61);
                        this.byteq.enqueue((byte) 13);
                        this.byteq.enqueue(b);
                        break;
                    } else {
                        this.state = 0;
                        break;
                    }
                case 3:
                    if ((b >= 48 && b <= 57) || ((b >= 65 && b <= 70) || (b >= 97 && b <= 102))) {
                        byte msd = asciiCharToNumericValue(msdChar);
                        byte low = asciiCharToNumericValue(b);
                        this.state = 0;
                        this.byteq.enqueue((byte) ((msd << 4) | low));
                        break;
                    } else {
                        if (log.isWarnEnabled()) {
                            log.warn("Malformed MIME; expected [0-9A-Z], got " + ((int) b));
                        }
                        this.state = 0;
                        this.byteq.enqueue((byte) 61);
                        this.byteq.enqueue(msdChar);
                        this.byteq.enqueue(b);
                        break;
                    }
                default:
                    log.error("Illegal state: " + ((int) this.state));
                    this.state = 0;
                    this.byteq.enqueue(b);
                    break;
            }
        }
    }

    private byte asciiCharToNumericValue(byte c) {
        if (c >= 48 && c <= 57) {
            return (byte) (c - 48);
        }
        if (c >= 65 && c <= 90) {
            return (byte) ((c - 65) + 10);
        }
        if (c >= 97 && c <= 122) {
            return (byte) ((c - 97) + 10);
        }
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {Character.TYPE};
        Object[] objArr = {new Character((char) c)};
        Object[] objArr2 = {" is not a hexadecimal digit"};
        Method method = StringBuilder.class.getMethod("append", String.class);
        throw new IllegalArgumentException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), new Object[0]));
    }
}
