package org.apache.james.mime4j.field;

public class DefaultFieldParser extends DelegatingFieldParser {
    public DefaultFieldParser() {
        Object[] objArr = {"Content-Transfer-Encoding", ContentTransferEncodingField.PARSER};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr);
        Object[] objArr2 = {"Content-Type", ContentTypeField.PARSER};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr2);
        Object[] objArr3 = {"Content-Disposition", ContentDispositionField.PARSER};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr3);
        FieldParser dateTimeParser = DateTimeField.PARSER;
        Object[] objArr4 = {FieldName.DATE, dateTimeParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr4);
        Object[] objArr5 = {FieldName.RESENT_DATE, dateTimeParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr5);
        FieldParser mailboxListParser = MailboxListField.PARSER;
        Object[] objArr6 = {FieldName.FROM, mailboxListParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr6);
        Object[] objArr7 = {FieldName.RESENT_FROM, mailboxListParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr7);
        FieldParser mailboxParser = MailboxField.PARSER;
        Object[] objArr8 = {FieldName.SENDER, mailboxParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr8);
        Object[] objArr9 = {FieldName.RESENT_SENDER, mailboxParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr9);
        FieldParser addressListParser = AddressListField.PARSER;
        Object[] objArr10 = {FieldName.TO, addressListParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr10);
        Object[] objArr11 = {FieldName.RESENT_TO, addressListParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr11);
        Object[] objArr12 = {FieldName.CC, addressListParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr12);
        Object[] objArr13 = {FieldName.RESENT_CC, addressListParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr13);
        Object[] objArr14 = {FieldName.BCC, addressListParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr14);
        Object[] objArr15 = {FieldName.RESENT_BCC, addressListParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr15);
        Object[] objArr16 = {FieldName.REPLY_TO, addressListParser};
        DefaultFieldParser.class.getMethod("setFieldParser", String.class, FieldParser.class).invoke(this, objArr16);
    }
}
