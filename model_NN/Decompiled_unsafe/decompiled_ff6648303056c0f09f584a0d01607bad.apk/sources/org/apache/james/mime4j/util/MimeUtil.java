package org.apache.james.mime4j.util;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.field.ContentTypeField;

public final class MimeUtil {
    public static final String ENC_7BIT = "7bit";
    public static final String ENC_8BIT = "8bit";
    public static final String ENC_BASE64 = "base64";
    public static final String ENC_BINARY = "binary";
    public static final String ENC_QUOTED_PRINTABLE = "quoted-printable";
    public static final String MIME_HEADER_CONTENT_DESCRIPTION = "content-description";
    public static final String MIME_HEADER_CONTENT_DISPOSITION = "content-disposition";
    public static final String MIME_HEADER_CONTENT_ID = "content-id";
    public static final String MIME_HEADER_LANGAUGE = "content-language";
    public static final String MIME_HEADER_LOCATION = "content-location";
    public static final String MIME_HEADER_MD5 = "content-md5";
    public static final String MIME_HEADER_MIME_VERSION = "mime-version";
    public static final String PARAM_CREATION_DATE = "creation-date";
    public static final String PARAM_FILENAME = "filename";
    public static final String PARAM_MODIFICATION_DATE = "modification-date";
    public static final String PARAM_READ_DATE = "read-date";
    public static final String PARAM_SIZE = "size";
    private static final ThreadLocal<DateFormat> RFC822_DATE_FORMAT = new ThreadLocal<DateFormat>() {
        /* access modifiers changed from: protected */
        public DateFormat initialValue() {
            return new Rfc822DateFormat();
        }
    };
    private static int counter = 0;
    private static final Log log;
    private static final Random random = new Random();

    static {
        Object[] objArr = {MimeUtil.class};
        log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
    }

    private MimeUtil() {
    }

    public static boolean isSameMimeType(String pType1, String pType2) {
        if (!(pType1 == null || pType2 == null)) {
            if (((Boolean) String.class.getMethod("equalsIgnoreCase", String.class).invoke(pType1, pType2)).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMessage(String pMimeType) {
        if (pMimeType != null) {
            Class[] clsArr = {String.class};
            if (((Boolean) String.class.getMethod("equalsIgnoreCase", clsArr).invoke(pMimeType, ContentTypeField.TYPE_MESSAGE_RFC822)).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMultipart(String pMimeType) {
        if (pMimeType != null) {
            Method method = String.class.getMethod("toLowerCase", new Class[0]);
            Object[] objArr = {ContentTypeField.TYPE_MULTIPART_PREFIX};
            if (((Boolean) String.class.getMethod("startsWith", String.class).invoke((String) method.invoke(pMimeType, new Object[0]), objArr)).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    public static boolean isBase64Encoding(String pTransferEncoding) {
        return ((Boolean) String.class.getMethod("equalsIgnoreCase", String.class).invoke(ENC_BASE64, pTransferEncoding)).booleanValue();
    }

    public static boolean isQuotedPrintableEncoded(String pTransferEncoding) {
        return ((Boolean) String.class.getMethod("equalsIgnoreCase", String.class).invoke(ENC_QUOTED_PRINTABLE, pTransferEncoding)).booleanValue();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0063 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0063 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map<java.lang.String, java.lang.String> getHeaderParams(java.lang.String r25) {
        /*
            java.lang.String r25 = r25.trim()
            java.lang.String r25 = unfold(r25)
            java.util.HashMap r21 = new java.util.HashMap
            r21.<init>()
            java.lang.String r23 = ";"
            r0 = r25
            r1 = r23
            int r23 = r0.indexOf(r1)
            r24 = -1
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x0066
            r17 = r25
            r20 = 0
        L_0x0023:
            java.lang.String r23 = ""
            r0 = r21
            r1 = r23
            r2 = r17
            r0.put(r1, r2)
            if (r20 == 0) goto L_0x0187
            char[] r12 = r20.toCharArray()
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r23 = 64
            r0 = r18
            r1 = r23
            r0.<init>(r1)
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            r23 = 64
            r0 = r19
            r1 = r23
            r0.<init>(r1)
            r7 = 0
            r4 = 1
            r8 = 2
            r6 = 3
            r5 = 4
            r9 = 5
            r3 = 99
            r22 = 0
            r13 = 0
            r10 = r12
            int r0 = r10.length
            r16 = r0
            r15 = 0
        L_0x005a:
            r0 = r16
            if (r15 >= r0) goto L_0x0162
            char r11 = r10[r15]
            switch(r22) {
                case 0: goto L_0x0094;
                case 1: goto L_0x00b8;
                case 2: goto L_0x00d0;
                case 3: goto L_0x00d9;
                case 4: goto L_0x0113;
                case 5: goto L_0x00e4;
                case 99: goto L_0x008b;
                default: goto L_0x0063;
            }
        L_0x0063:
            int r15 = r15 + 1
            goto L_0x005a
        L_0x0066:
            r23 = 0
            java.lang.String r24 = ";"
            r0 = r25
            r1 = r24
            int r24 = r0.indexOf(r1)
            r0 = r25
            r1 = r23
            r2 = r24
            java.lang.String r17 = r0.substring(r1, r2)
            int r23 = r17.length()
            int r23 = r23 + 1
            r0 = r25
            r1 = r23
            java.lang.String r20 = r0.substring(r1)
            goto L_0x0023
        L_0x008b:
            r23 = 59
            r0 = r23
            if (r11 != r0) goto L_0x0063
            r22 = 0
            goto L_0x0063
        L_0x0094:
            r23 = 61
            r0 = r23
            if (r11 != r0) goto L_0x00a4
            org.apache.commons.logging.Log r23 = org.apache.james.mime4j.util.MimeUtil.log
            java.lang.String r24 = "Expected header param name, got '='"
            r23.error(r24)
            r22 = 99
            goto L_0x0063
        L_0x00a4:
            r23 = 0
            r0 = r18
            r1 = r23
            r0.setLength(r1)
            r23 = 0
            r0 = r19
            r1 = r23
            r0.setLength(r1)
            r22 = 1
        L_0x00b8:
            r23 = 61
            r0 = r23
            if (r11 != r0) goto L_0x00ca
            int r23 = r18.length()
            if (r23 != 0) goto L_0x00c7
            r22 = 99
            goto L_0x0063
        L_0x00c7:
            r22 = 2
            goto L_0x0063
        L_0x00ca:
            r0 = r18
            r0.append(r11)
            goto L_0x0063
        L_0x00d0:
            r14 = 0
            switch(r11) {
                case 9: goto L_0x00d7;
                case 32: goto L_0x00d7;
                case 34: goto L_0x00eb;
                default: goto L_0x00d4;
            }
        L_0x00d4:
            r22 = 3
            r14 = 1
        L_0x00d7:
            if (r14 == 0) goto L_0x0063
        L_0x00d9:
            r14 = 0
            switch(r11) {
                case 9: goto L_0x00ee;
                case 32: goto L_0x00ee;
                case 59: goto L_0x00ee;
                default: goto L_0x00dd;
            }
        L_0x00dd:
            r0 = r19
            r0.append(r11)
        L_0x00e2:
            if (r14 == 0) goto L_0x0063
        L_0x00e4:
            switch(r11) {
                case 9: goto L_0x0063;
                case 32: goto L_0x0063;
                case 59: goto L_0x010f;
                default: goto L_0x00e7;
            }
        L_0x00e7:
            r22 = 99
            goto L_0x0063
        L_0x00eb:
            r22 = 4
            goto L_0x00d7
        L_0x00ee:
            java.lang.String r23 = r18.toString()
            java.lang.String r23 = r23.trim()
            java.lang.String r23 = r23.toLowerCase()
            java.lang.String r24 = r19.toString()
            java.lang.String r24 = r24.trim()
            r0 = r21
            r1 = r23
            r2 = r24
            r0.put(r1, r2)
            r22 = 5
            r14 = 1
            goto L_0x00e2
        L_0x010f:
            r22 = 0
            goto L_0x0063
        L_0x0113:
            switch(r11) {
                case 34: goto L_0x0129;
                case 92: goto L_0x0150;
                default: goto L_0x0116;
            }
        L_0x0116:
            if (r13 == 0) goto L_0x0121
            r23 = 92
            r0 = r19
            r1 = r23
            r0.append(r1)
        L_0x0121:
            r13 = 0
            r0 = r19
            r0.append(r11)
            goto L_0x0063
        L_0x0129:
            if (r13 != 0) goto L_0x0148
            java.lang.String r23 = r18.toString()
            java.lang.String r23 = r23.trim()
            java.lang.String r23 = r23.toLowerCase()
            java.lang.String r24 = r19.toString()
            r0 = r21
            r1 = r23
            r2 = r24
            r0.put(r1, r2)
            r22 = 5
            goto L_0x0063
        L_0x0148:
            r13 = 0
            r0 = r19
            r0.append(r11)
            goto L_0x0063
        L_0x0150:
            if (r13 == 0) goto L_0x015b
            r23 = 92
            r0 = r19
            r1 = r23
            r0.append(r1)
        L_0x015b:
            if (r13 != 0) goto L_0x0160
            r13 = 1
        L_0x015e:
            goto L_0x0063
        L_0x0160:
            r13 = 0
            goto L_0x015e
        L_0x0162:
            r23 = 3
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x0187
            java.lang.String r23 = r18.toString()
            java.lang.String r23 = r23.trim()
            java.lang.String r23 = r23.toLowerCase()
            java.lang.String r24 = r19.toString()
            java.lang.String r24 = r24.trim()
            r0 = r21
            r1 = r23
            r2 = r24
            r0.put(r1, r2)
        L_0x0187:
            return r21
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.util.MimeUtil.getHeaderParams(java.lang.String):java.util.Map");
    }

    public static String createUniqueBoundary() {
        StringBuilder sb = new StringBuilder();
        Object[] objArr = {"-=Part."};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr);
        int intValue = ((Integer) MimeUtil.class.getMethod("nextCounterValue", new Class[0]).invoke(null, new Object[0])).intValue();
        Class[] clsArr = {Integer.TYPE};
        Object[] objArr2 = {(String) Integer.class.getMethod("toHexString", clsArr).invoke(null, new Integer(intValue))};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
        Class[] clsArr2 = {Character.TYPE};
        StringBuilder.class.getMethod("append", clsArr2).invoke(sb, new Character('.'));
        long longValue = ((Long) Random.class.getMethod("nextLong", new Class[0]).invoke(random, new Object[0])).longValue();
        Class[] clsArr3 = {Long.TYPE};
        Object[] objArr3 = {(String) Long.class.getMethod("toHexString", clsArr3).invoke(null, new Long(longValue))};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr3);
        Class[] clsArr4 = {Character.TYPE};
        StringBuilder.class.getMethod("append", clsArr4).invoke(sb, new Character('.'));
        long longValue2 = ((Long) System.class.getMethod("currentTimeMillis", new Class[0]).invoke(null, new Object[0])).longValue();
        Class[] clsArr5 = {Long.TYPE};
        Object[] objArr4 = {(String) Long.class.getMethod("toHexString", clsArr5).invoke(null, new Long(longValue2))};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr4);
        Class[] clsArr6 = {Character.TYPE};
        StringBuilder.class.getMethod("append", clsArr6).invoke(sb, new Character('.'));
        long longValue3 = ((Long) Random.class.getMethod("nextLong", new Class[0]).invoke(random, new Object[0])).longValue();
        Class[] clsArr7 = {Long.TYPE};
        Object[] objArr5 = {(String) Long.class.getMethod("toHexString", clsArr7).invoke(null, new Long(longValue3))};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr5);
        Object[] objArr6 = {"=-"};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr6);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
    }

    public static String createUniqueMessageId(String hostName) {
        StringBuilder sb = new StringBuilder("<Mime4j.");
        int intValue = ((Integer) MimeUtil.class.getMethod("nextCounterValue", new Class[0]).invoke(null, new Object[0])).intValue();
        Class[] clsArr = {Integer.TYPE};
        Object[] objArr = {(String) Integer.class.getMethod("toHexString", clsArr).invoke(null, new Integer(intValue))};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr);
        Class[] clsArr2 = {Character.TYPE};
        StringBuilder.class.getMethod("append", clsArr2).invoke(sb, new Character('.'));
        long longValue = ((Long) Random.class.getMethod("nextLong", new Class[0]).invoke(random, new Object[0])).longValue();
        Class[] clsArr3 = {Long.TYPE};
        Object[] objArr2 = {(String) Long.class.getMethod("toHexString", clsArr3).invoke(null, new Long(longValue))};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
        Class[] clsArr4 = {Character.TYPE};
        StringBuilder.class.getMethod("append", clsArr4).invoke(sb, new Character('.'));
        long longValue2 = ((Long) System.class.getMethod("currentTimeMillis", new Class[0]).invoke(null, new Object[0])).longValue();
        Class[] clsArr5 = {Long.TYPE};
        Object[] objArr3 = {(String) Long.class.getMethod("toHexString", clsArr5).invoke(null, new Long(longValue2))};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr3);
        if (hostName != null) {
            Class[] clsArr6 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr6).invoke(sb, new Character('@'));
            Object[] objArr4 = {hostName};
            StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr4);
        }
        Class[] clsArr7 = {Character.TYPE};
        StringBuilder.class.getMethod("append", clsArr7).invoke(sb, new Character('>'));
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
    }

    public static String formatDate(Date date, TimeZone zone) {
        DateFormat df = (DateFormat) ThreadLocal.class.getMethod("get", new Class[0]).invoke(RFC822_DATE_FORMAT, new Object[0]);
        if (zone == null) {
            Object[] objArr = {(TimeZone) TimeZone.class.getMethod("getDefault", new Class[0]).invoke(null, new Object[0])};
            DateFormat.class.getMethod("setTimeZone", TimeZone.class).invoke(df, objArr);
        } else {
            Object[] objArr2 = {zone};
            DateFormat.class.getMethod("setTimeZone", TimeZone.class).invoke(df, objArr2);
        }
        return (String) DateFormat.class.getMethod("format", Date.class).invoke(df, date);
    }

    public static String fold(String s, int usedCharacters) {
        int length = ((Integer) String.class.getMethod("length", new Class[0]).invoke(s, new Object[0])).intValue();
        if (usedCharacters + length <= 76) {
            return s;
        }
        StringBuilder sb = new StringBuilder();
        int lastLineBreak = -usedCharacters;
        Class[] clsArr = {String.class, Integer.TYPE};
        int wspIdx = ((Integer) MimeUtil.class.getMethod("indexOfWsp", clsArr).invoke(null, s, new Integer(0))).intValue();
        while (wspIdx != length) {
            Class[] clsArr2 = {String.class, Integer.TYPE};
            int nextWspIdx = ((Integer) MimeUtil.class.getMethod("indexOfWsp", clsArr2).invoke(null, s, new Integer(wspIdx + 1))).intValue();
            if (nextWspIdx - lastLineBreak > 76) {
                Class[] clsArr3 = {Integer.TYPE, Integer.TYPE};
                int intValue = ((Integer) Math.class.getMethod("max", clsArr3).invoke(null, new Integer(0), new Integer(lastLineBreak))).intValue();
                Class[] clsArr4 = {Integer.TYPE, Integer.TYPE};
                Object[] objArr = {(String) String.class.getMethod("substring", clsArr4).invoke(s, new Integer(intValue), new Integer(wspIdx))};
                StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr);
                Object[] objArr2 = {CharsetUtil.CRLF};
                StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
                lastLineBreak = wspIdx;
            }
            wspIdx = nextWspIdx;
        }
        Class[] clsArr5 = {Integer.TYPE, Integer.TYPE};
        int intValue2 = ((Integer) Math.class.getMethod("max", clsArr5).invoke(null, new Integer(0), new Integer(lastLineBreak))).intValue();
        Class[] clsArr6 = {Integer.TYPE};
        Object[] objArr3 = {(String) String.class.getMethod("substring", clsArr6).invoke(s, new Integer(intValue2))};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr3);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
    }

    public static String unfold(String s) {
        int length = ((Integer) String.class.getMethod("length", new Class[0]).invoke(s, new Object[0])).intValue();
        for (int idx = 0; idx < length; idx++) {
            Class[] clsArr = {Integer.TYPE};
            char c = ((Character) String.class.getMethod("charAt", clsArr).invoke(s, new Integer(idx))).charValue();
            if (c == 13 || c == 10) {
                Class[] clsArr2 = {String.class, Integer.TYPE};
                return (String) MimeUtil.class.getMethod("unfold0", clsArr2).invoke(null, s, new Integer(idx));
            }
        }
        return s;
    }

    private static String unfold0(String s, int crlfIdx) {
        int length = ((Integer) String.class.getMethod("length", new Class[0]).invoke(s, new Object[0])).intValue();
        StringBuilder sb = new StringBuilder(length);
        if (crlfIdx > 0) {
            Class[] clsArr = {Integer.TYPE, Integer.TYPE};
            Object[] objArr = {(String) String.class.getMethod("substring", clsArr).invoke(s, new Integer(0), new Integer(crlfIdx))};
            StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr);
        }
        for (int idx = crlfIdx + 1; idx < length; idx++) {
            Class[] clsArr2 = {Integer.TYPE};
            char c = ((Character) String.class.getMethod("charAt", clsArr2).invoke(s, new Integer(idx))).charValue();
            if (!(c == 13 || c == 10)) {
                Class[] clsArr3 = {Character.TYPE};
                StringBuilder.class.getMethod("append", clsArr3).invoke(sb, new Character(c));
            }
        }
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
    }

    private static int indexOfWsp(String s, int fromIndex) {
        int len = ((Integer) String.class.getMethod("length", new Class[0]).invoke(s, new Object[0])).intValue();
        int index = fromIndex;
        while (index < len) {
            Class[] clsArr = {Integer.TYPE};
            char c = ((Character) String.class.getMethod("charAt", clsArr).invoke(s, new Integer(index))).charValue();
            if (c == ' ' || c == 9) {
                return index;
            }
            index++;
        }
        return len;
    }

    private static synchronized int nextCounterValue() {
        int i;
        synchronized (MimeUtil.class) {
            i = counter;
            counter = i + 1;
        }
        return i;
    }

    private static final class Rfc822DateFormat extends SimpleDateFormat {
        private static final long serialVersionUID = 1;

        public Rfc822DateFormat() {
            super("EEE, d MMM yyyy HH:mm:ss ", Locale.US);
        }

        public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition pos) {
            StringBuffer sb = super.format(date, toAppendTo, pos);
            Calendar calendar = this.calendar;
            Class[] clsArr = {Integer.TYPE};
            int zoneMillis = ((Integer) Calendar.class.getMethod("get", clsArr).invoke(calendar, new Integer(15))).intValue();
            Calendar calendar2 = this.calendar;
            Class[] clsArr2 = {Integer.TYPE};
            int minutes = ((zoneMillis + ((Integer) Calendar.class.getMethod("get", clsArr2).invoke(calendar2, new Integer(16))).intValue()) / 1000) / 60;
            if (minutes < 0) {
                Class[] clsArr3 = {Character.TYPE};
                StringBuffer.class.getMethod("append", clsArr3).invoke(sb, new Character('-'));
                minutes = -minutes;
            } else {
                Class[] clsArr4 = {Character.TYPE};
                StringBuffer.class.getMethod("append", clsArr4).invoke(sb, new Character('+'));
            }
            Class[] clsArr5 = {Integer.TYPE};
            Object[] objArr = {new Integer(minutes / 60)};
            Class[] clsArr6 = {Integer.TYPE};
            Object[] objArr2 = {(Integer) Integer.class.getMethod("valueOf", clsArr5).invoke(null, objArr), (Integer) Integer.class.getMethod("valueOf", clsArr6).invoke(null, new Integer(minutes % 60))};
            Object[] objArr3 = {"%02d%02d", objArr2};
            Object[] objArr4 = {(String) String.class.getMethod("format", String.class, Object[].class).invoke(null, objArr3)};
            StringBuffer.class.getMethod("append", String.class).invoke(sb, objArr4);
            return sb;
        }
    }
}
