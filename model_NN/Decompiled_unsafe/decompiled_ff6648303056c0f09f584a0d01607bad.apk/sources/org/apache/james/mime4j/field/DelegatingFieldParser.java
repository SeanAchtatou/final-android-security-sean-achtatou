package org.apache.james.mime4j.field;

import java.util.HashMap;
import java.util.Map;
import org.apache.james.mime4j.util.ByteSequence;

public class DelegatingFieldParser implements FieldParser {
    private FieldParser defaultParser = UnstructuredField.PARSER;
    private Map<String, FieldParser> parsers = new HashMap();

    public void setFieldParser(String name, FieldParser parser) {
        Map<String, FieldParser> map = this.parsers;
        Object[] objArr = {(String) String.class.getMethod("toLowerCase", new Class[0]).invoke(name, new Object[0]), parser};
        Map.class.getMethod("put", Object.class, Object.class).invoke(map, objArr);
    }

    public FieldParser getParser(String name) {
        Map<String, FieldParser> map = this.parsers;
        Object[] objArr = {(String) String.class.getMethod("toLowerCase", new Class[0]).invoke(name, new Object[0])};
        FieldParser field = (FieldParser) Map.class.getMethod("get", Object.class).invoke(map, objArr);
        if (field == null) {
            return this.defaultParser;
        }
        return field;
    }

    public ParsedField parse(String name, String body, ByteSequence raw) {
        Class[] clsArr = {String.class};
        Object[] objArr = {name};
        Object[] objArr2 = {name, body, raw};
        return (ParsedField) FieldParser.class.getMethod("parse", String.class, String.class, ByteSequence.class).invoke((FieldParser) DelegatingFieldParser.class.getMethod("getParser", clsArr).invoke(this, objArr), objArr2);
    }
}
