package org.apache.james.mime4j.field.structured.parser;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

public class StructuredFieldParser implements StructuredFieldParserConstants {
    private static int[] jj_la1_0;
    private Vector<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private final int[] jj_la1;
    public Token jj_nt;
    private int jj_ntk;
    private boolean preserveFolding;
    public Token token;
    public StructuredFieldParserTokenManager token_source;

    public boolean isFoldingPreserved() {
        return this.preserveFolding;
    }

    public void setFoldingPreserved(boolean preserveFolding2) {
        this.preserveFolding = preserveFolding2;
    }

    public String parse() throws ParseException {
        try {
            return (String) StructuredFieldParser.class.getMethod("doParse", new Class[0]).invoke(this, new Object[0]);
        } catch (TokenMgrError e) {
            throw new ParseException(e);
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0028 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x000e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0046 A[FALL_THROUGH] */
    private final java.lang.String doParse() throws org.apache.james.mime4j.field.structured.parser.ParseException {
        /*
            r12 = this;
            r7 = -1
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r4 = 50
            r0.<init>(r4)
            r3 = 0
            r1 = 1
        L_0x000a:
            int r4 = r12.jj_ntk
            if (r4 != r7) goto L_0x0043
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            java.lang.String r8 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.structured.parser.StructuredFieldParser> r11 = org.apache.james.mime4j.field.structured.parser.StructuredFieldParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            java.lang.Object r8 = r8.invoke(r12, r10)
            java.lang.Integer r8 = (java.lang.Integer) r8
            int r4 = r8.intValue()
        L_0x0025:
            switch(r4) {
                case 11: goto L_0x0046;
                case 12: goto L_0x0046;
                case 13: goto L_0x0046;
                case 14: goto L_0x0046;
                case 15: goto L_0x0046;
                default: goto L_0x0028;
            }
        L_0x0028:
            int[] r4 = r12.jj_la1
            r5 = 0
            int r6 = r12.jj_gen
            r4[r5] = r6
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            java.lang.String r8 = "toString"
            java.lang.Class<java.lang.StringBuffer> r11 = java.lang.StringBuffer.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            java.lang.Object r4 = r8.invoke(r0, r10)
            java.lang.String r4 = (java.lang.String) r4
            return r4
        L_0x0043:
            int r4 = r12.jj_ntk
            goto L_0x0025
        L_0x0046:
            int r4 = r12.jj_ntk
            if (r4 != r7) goto L_0x008d
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            java.lang.String r8 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.structured.parser.StructuredFieldParser> r11 = org.apache.james.mime4j.field.structured.parser.StructuredFieldParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            java.lang.Object r8 = r8.invoke(r12, r10)
            java.lang.Integer r8 = (java.lang.Integer) r8
            int r4 = r8.intValue()
        L_0x0061:
            switch(r4) {
                case 11: goto L_0x00ec;
                case 12: goto L_0x0184;
                case 13: goto L_0x0128;
                case 14: goto L_0x01c4;
                case 15: goto L_0x0090;
                default: goto L_0x0064;
            }
        L_0x0064:
            int[] r4 = r12.jj_la1
            r5 = 1
            int r6 = r12.jj_gen
            r4[r5] = r6
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class r11 = java.lang.Integer.TYPE
            r9[r8] = r11
            java.lang.Integer r11 = new java.lang.Integer
            r11.<init>(r7)
            r10[r8] = r11
            java.lang.String r8 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.structured.parser.StructuredFieldParser> r11 = org.apache.james.mime4j.field.structured.parser.StructuredFieldParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r12, r10)
            org.apache.james.mime4j.field.structured.parser.ParseException r4 = new org.apache.james.mime4j.field.structured.parser.ParseException
            r4.<init>()
            throw r4
        L_0x008d:
            int r4 = r12.jj_ntk
            goto L_0x0061
        L_0x0090:
            r4 = 15
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class r11 = java.lang.Integer.TYPE
            r9[r8] = r11
            java.lang.Integer r11 = new java.lang.Integer
            r11.<init>(r4)
            r10[r8] = r11
            java.lang.String r8 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.structured.parser.StructuredFieldParser> r11 = org.apache.james.mime4j.field.structured.parser.StructuredFieldParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            java.lang.Object r2 = r8.invoke(r12, r10)
            org.apache.james.mime4j.field.structured.parser.Token r2 = (org.apache.james.mime4j.field.structured.parser.Token) r2
            if (r1 == 0) goto L_0x00cf
            r1 = 0
        L_0x00b4:
            java.lang.String r4 = r2.image
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<java.lang.String> r11 = java.lang.String.class
            r9[r8] = r11
            r10[r8] = r4
            java.lang.String r8 = "append"
            java.lang.Class<java.lang.StringBuffer> r11 = java.lang.StringBuffer.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r0, r10)
            goto L_0x000a
        L_0x00cf:
            if (r3 == 0) goto L_0x00b4
            java.lang.String r4 = " "
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<java.lang.String> r11 = java.lang.String.class
            r9[r8] = r11
            r10[r8] = r4
            java.lang.String r8 = "append"
            java.lang.Class<java.lang.StringBuffer> r11 = java.lang.StringBuffer.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r0, r10)
            r3 = 0
            goto L_0x00b4
        L_0x00ec:
            r4 = 11
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class r11 = java.lang.Integer.TYPE
            r9[r8] = r11
            java.lang.Integer r11 = new java.lang.Integer
            r11.<init>(r4)
            r10[r8] = r11
            java.lang.String r8 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.structured.parser.StructuredFieldParser> r11 = org.apache.james.mime4j.field.structured.parser.StructuredFieldParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            java.lang.Object r2 = r8.invoke(r12, r10)
            org.apache.james.mime4j.field.structured.parser.Token r2 = (org.apache.james.mime4j.field.structured.parser.Token) r2
            java.lang.String r4 = r2.image
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<java.lang.String> r11 = java.lang.String.class
            r9[r8] = r11
            r10[r8] = r4
            java.lang.String r8 = "append"
            java.lang.Class<java.lang.StringBuffer> r11 = java.lang.StringBuffer.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r0, r10)
            goto L_0x000a
        L_0x0128:
            r4 = 13
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class r11 = java.lang.Integer.TYPE
            r9[r8] = r11
            java.lang.Integer r11 = new java.lang.Integer
            r11.<init>(r4)
            r10[r8] = r11
            java.lang.String r8 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.structured.parser.StructuredFieldParser> r11 = org.apache.james.mime4j.field.structured.parser.StructuredFieldParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            java.lang.Object r2 = r8.invoke(r12, r10)
            org.apache.james.mime4j.field.structured.parser.Token r2 = (org.apache.james.mime4j.field.structured.parser.Token) r2
            if (r1 == 0) goto L_0x0167
            r1 = 0
        L_0x014c:
            java.lang.String r4 = r2.image
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<java.lang.String> r11 = java.lang.String.class
            r9[r8] = r11
            r10[r8] = r4
            java.lang.String r8 = "append"
            java.lang.Class<java.lang.StringBuffer> r11 = java.lang.StringBuffer.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r0, r10)
            goto L_0x000a
        L_0x0167:
            if (r3 == 0) goto L_0x014c
            java.lang.String r4 = " "
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<java.lang.String> r11 = java.lang.String.class
            r9[r8] = r11
            r10[r8] = r4
            java.lang.String r8 = "append"
            java.lang.Class<java.lang.StringBuffer> r11 = java.lang.StringBuffer.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r0, r10)
            r3 = 0
            goto L_0x014c
        L_0x0184:
            r4 = 12
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class r11 = java.lang.Integer.TYPE
            r9[r8] = r11
            java.lang.Integer r11 = new java.lang.Integer
            r11.<init>(r4)
            r10[r8] = r11
            java.lang.String r8 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.structured.parser.StructuredFieldParser> r11 = org.apache.james.mime4j.field.structured.parser.StructuredFieldParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            java.lang.Object r2 = r8.invoke(r12, r10)
            org.apache.james.mime4j.field.structured.parser.Token r2 = (org.apache.james.mime4j.field.structured.parser.Token) r2
            boolean r4 = r12.preserveFolding
            if (r4 == 0) goto L_0x000a
            java.lang.String r4 = "\r\n"
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<java.lang.String> r11 = java.lang.String.class
            r9[r8] = r11
            r10[r8] = r4
            java.lang.String r8 = "append"
            java.lang.Class<java.lang.StringBuffer> r11 = java.lang.StringBuffer.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r0, r10)
            goto L_0x000a
        L_0x01c4:
            r4 = 14
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class r11 = java.lang.Integer.TYPE
            r9[r8] = r11
            java.lang.Integer r11 = new java.lang.Integer
            r11.<init>(r4)
            r10[r8] = r11
            java.lang.String r8 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.structured.parser.StructuredFieldParser> r11 = org.apache.james.mime4j.field.structured.parser.StructuredFieldParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            java.lang.Object r2 = r8.invoke(r12, r10)
            org.apache.james.mime4j.field.structured.parser.Token r2 = (org.apache.james.mime4j.field.structured.parser.Token) r2
            r3 = 1
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.structured.parser.StructuredFieldParser.doParse():java.lang.String");
    }

    static {
        StructuredFieldParser.class.getMethod("jj_la1_0", new Class[0]).invoke(null, new Object[0]);
    }

    private static void jj_la1_0() {
        jj_la1_0 = new int[]{63488, 63488};
    }

    public StructuredFieldParser(InputStream stream) {
        this(stream, null);
    }

    public StructuredFieldParser(InputStream stream, String encoding) {
        this.preserveFolding = false;
        this.jj_la1 = new int[2];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
            this.token_source = new StructuredFieldParserTokenManager(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 2; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void ReInit(InputStream stream) {
        Object[] objArr = {stream, null};
        StructuredFieldParser.class.getMethod("ReInit", InputStream.class, String.class).invoke(this, objArr);
    }

    public void ReInit(InputStream stream, String encoding) {
        try {
            SimpleCharStream simpleCharStream = this.jj_input_stream;
            Class[] clsArr = {InputStream.class, String.class, Integer.TYPE, Integer.TYPE};
            SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, encoding, new Integer(1), new Integer(1));
            StructuredFieldParserTokenManager structuredFieldParserTokenManager = this.token_source;
            Object[] objArr = {this.jj_input_stream};
            StructuredFieldParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(structuredFieldParserTokenManager, objArr);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 2; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public StructuredFieldParser(Reader stream) {
        this.preserveFolding = false;
        this.jj_la1 = new int[2];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new StructuredFieldParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 2; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(Reader stream) {
        SimpleCharStream simpleCharStream = this.jj_input_stream;
        Class[] clsArr = {Reader.class, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, new Integer(1), new Integer(1));
        StructuredFieldParserTokenManager structuredFieldParserTokenManager = this.token_source;
        Object[] objArr = {this.jj_input_stream};
        StructuredFieldParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(structuredFieldParserTokenManager, objArr);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 2; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public StructuredFieldParser(StructuredFieldParserTokenManager tm) {
        this.preserveFolding = false;
        this.jj_la1 = new int[2];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 2; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(StructuredFieldParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 2; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private final Token jj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        if (oldToken.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) StructuredFieldParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            this.jj_gen++;
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw ((ParseException) StructuredFieldParser.class.getMethod("generateParseException", new Class[0]).invoke(this, new Object[0]));
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) StructuredFieldParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    public final Token getToken(int index) {
        Token t;
        int i = 0;
        Token t2 = this.token;
        while (i < index) {
            if (t2.next != null) {
                t = t2.next;
            } else {
                t = (Token) StructuredFieldParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
                t2.next = t;
            }
            i++;
            t2 = t;
        }
        return t2;
    }

    private final int jj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token token4 = (Token) StructuredFieldParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token3.next = token4;
            int i = token4.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    public ParseException generateParseException() {
        Vector.class.getMethod("removeAllElements", new Class[0]).invoke(this.jj_expentries, new Object[0]);
        boolean[] la1tokens = new boolean[18];
        for (int i = 0; i < 18; i++) {
            la1tokens[i] = false;
        }
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i2 = 0; i2 < 2; i2++) {
            if (this.jj_la1[i2] == this.jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i2] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                }
            }
        }
        for (int i3 = 0; i3 < 18; i3++) {
            if (la1tokens[i3]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i3;
                Vector<int[]> vector = this.jj_expentries;
                Object[] objArr = {this.jj_expentry};
                Vector.class.getMethod("addElement", Object.class).invoke(vector, objArr);
            }
        }
        int[][] exptokseq = new int[((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()][];
        int i4 = 0;
        while (true) {
            if (i4 >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()) {
                return new ParseException(this.token, exptokseq, tokenImage);
            }
            Vector<int[]> vector2 = this.jj_expentries;
            Class[] clsArr = {Integer.TYPE};
            exptokseq[i4] = (int[]) Vector.class.getMethod("elementAt", clsArr).invoke(vector2, new Integer(i4));
            i4++;
        }
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }
}
