package org.apache.james.mime4j.descriptor;

import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.field.datetime.DateTime;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParser;
import org.apache.james.mime4j.field.datetime.parser.ParseException;
import org.apache.james.mime4j.field.language.parser.ContentLanguageParser;
import org.apache.james.mime4j.field.mimeversion.parser.MimeVersionParser;
import org.apache.james.mime4j.field.structured.parser.StructuredFieldParser;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.MimeUtil;

public class MaximalBodyDescriptor extends DefaultBodyDescriptor {
    private static final int DEFAULT_MAJOR_VERSION = 1;
    private static final int DEFAULT_MINOR_VERSION = 0;
    private String contentDescription;
    private DateTime contentDispositionCreationDate;
    private MimeException contentDispositionCreationDateParseException;
    private DateTime contentDispositionModificationDate;
    private MimeException contentDispositionModificationDateParseException;
    private Map<String, String> contentDispositionParameters;
    private DateTime contentDispositionReadDate;
    private MimeException contentDispositionReadDateParseException;
    private long contentDispositionSize;
    private MimeException contentDispositionSizeParseException;
    private String contentDispositionType;
    private String contentId;
    private List<String> contentLanguage;
    private MimeException contentLanguageParseException;
    private String contentLocation;
    private MimeException contentLocationParseException;
    private String contentMD5Raw;
    private boolean isContentDescriptionSet;
    private boolean isContentDispositionSet;
    private boolean isContentIdSet;
    private boolean isContentLanguageSet;
    private boolean isContentLocationSet;
    private boolean isContentMD5Set;
    private boolean isMimeVersionSet;
    private int mimeMajorVersion;
    private int mimeMinorVersion;
    private MimeException mimeVersionException;

    protected MaximalBodyDescriptor() {
        this(null);
    }

    public MaximalBodyDescriptor(BodyDescriptor parent) {
        super(parent);
        this.isMimeVersionSet = false;
        this.mimeMajorVersion = 1;
        this.mimeMinorVersion = 0;
        this.contentId = null;
        this.isContentIdSet = false;
        this.contentDescription = null;
        this.isContentDescriptionSet = false;
        this.contentDispositionType = null;
        this.contentDispositionParameters = (Map) Collections.class.getMethod("emptyMap", new Class[0]).invoke(null, new Object[0]);
        this.contentDispositionModificationDate = null;
        this.contentDispositionModificationDateParseException = null;
        this.contentDispositionCreationDate = null;
        this.contentDispositionCreationDateParseException = null;
        this.contentDispositionReadDate = null;
        this.contentDispositionReadDateParseException = null;
        this.contentDispositionSize = -1;
        this.contentDispositionSizeParseException = null;
        this.isContentDispositionSet = false;
        this.contentLanguage = null;
        this.contentLanguageParseException = null;
        this.isContentIdSet = false;
        this.contentLocation = null;
        this.contentLocationParseException = null;
        this.isContentLocationSet = false;
        this.contentMD5Raw = null;
        this.isContentMD5Set = false;
    }

    public void addField(Field field) {
        Method method = Field.class.getMethod("getName", new Class[0]);
        String value = (String) Field.class.getMethod("getBody", new Class[0]).invoke(field, new Object[0]);
        Method method2 = String.class.getMethod("trim", new Class[0]);
        String name = (String) String.class.getMethod("toLowerCase", new Class[0]).invoke((String) method2.invoke((String) method.invoke(field, new Object[0]), new Object[0]), new Object[0]);
        if (!((Boolean) String.class.getMethod("equals", Object.class).invoke(MimeUtil.MIME_HEADER_MIME_VERSION, name)).booleanValue() || this.isMimeVersionSet) {
            if (!((Boolean) String.class.getMethod("equals", Object.class).invoke(MimeUtil.MIME_HEADER_CONTENT_ID, name)).booleanValue() || this.isContentIdSet) {
                if (!((Boolean) String.class.getMethod("equals", Object.class).invoke(MimeUtil.MIME_HEADER_CONTENT_DESCRIPTION, name)).booleanValue() || this.isContentDescriptionSet) {
                    if (!((Boolean) String.class.getMethod("equals", Object.class).invoke(MimeUtil.MIME_HEADER_CONTENT_DISPOSITION, name)).booleanValue() || this.isContentDispositionSet) {
                        if (!((Boolean) String.class.getMethod("equals", Object.class).invoke(MimeUtil.MIME_HEADER_LANGAUGE, name)).booleanValue() || this.isContentLanguageSet) {
                            if (!((Boolean) String.class.getMethod("equals", Object.class).invoke(MimeUtil.MIME_HEADER_LOCATION, name)).booleanValue() || this.isContentLocationSet) {
                                if (!((Boolean) String.class.getMethod("equals", Object.class).invoke(MimeUtil.MIME_HEADER_MD5, name)).booleanValue() || this.isContentMD5Set) {
                                    super.addField(field);
                                    return;
                                }
                                Object[] objArr = {value};
                                MaximalBodyDescriptor.class.getMethod("parseMD5", String.class).invoke(this, objArr);
                                return;
                            }
                            Object[] objArr2 = {value};
                            MaximalBodyDescriptor.class.getMethod("parseLocation", String.class).invoke(this, objArr2);
                            return;
                        }
                        Object[] objArr3 = {value};
                        MaximalBodyDescriptor.class.getMethod("parseLanguage", String.class).invoke(this, objArr3);
                        return;
                    }
                    Object[] objArr4 = {value};
                    MaximalBodyDescriptor.class.getMethod("parseContentDisposition", String.class).invoke(this, objArr4);
                    return;
                }
                Object[] objArr5 = {value};
                MaximalBodyDescriptor.class.getMethod("parseContentDescription", String.class).invoke(this, objArr5);
                return;
            }
            Object[] objArr6 = {value};
            MaximalBodyDescriptor.class.getMethod("parseContentId", String.class).invoke(this, objArr6);
            return;
        }
        Object[] objArr7 = {value};
        MaximalBodyDescriptor.class.getMethod("parseMimeVersion", String.class).invoke(this, objArr7);
    }

    private void parseMD5(String value) {
        this.isContentMD5Set = true;
        if (value != null) {
            this.contentMD5Raw = (String) String.class.getMethod("trim", new Class[0]).invoke(value, new Object[0]);
        }
    }

    private void parseLocation(String value) {
        this.isContentLocationSet = true;
        if (value != null) {
            StructuredFieldParser parser = new StructuredFieldParser(new StringReader(value));
            Class[] clsArr = {Boolean.TYPE};
            StructuredFieldParser.class.getMethod("setFoldingPreserved", clsArr).invoke(parser, new Boolean(false));
            try {
                this.contentLocation = (String) StructuredFieldParser.class.getMethod("parse", new Class[0]).invoke(parser, new Object[0]);
            } catch (MimeException e) {
                this.contentLocationParseException = e;
            }
        }
    }

    private void parseLanguage(String value) {
        this.isContentLanguageSet = true;
        if (value != null) {
            try {
                this.contentLanguage = (List) ContentLanguageParser.class.getMethod("parse", new Class[0]).invoke(new ContentLanguageParser(new StringReader(value)), new Object[0]);
            } catch (MimeException e) {
                this.contentLanguageParseException = e;
            }
        }
    }

    private void parseContentDisposition(String value) {
        this.isContentDispositionSet = true;
        Object[] objArr = {value};
        this.contentDispositionParameters = (Map) MimeUtil.class.getMethod("getHeaderParams", String.class).invoke(null, objArr);
        Object[] objArr2 = {""};
        this.contentDispositionType = (String) Map.class.getMethod("get", Object.class).invoke(this.contentDispositionParameters, objArr2);
        Object[] objArr3 = {"modification-date"};
        String contentDispositionModificationDate2 = (String) Map.class.getMethod("get", Object.class).invoke(this.contentDispositionParameters, objArr3);
        if (contentDispositionModificationDate2 != null) {
            try {
                Object[] objArr4 = {contentDispositionModificationDate2};
                this.contentDispositionModificationDate = (DateTime) MaximalBodyDescriptor.class.getMethod("parseDate", String.class).invoke(this, objArr4);
            } catch (ParseException e) {
                this.contentDispositionModificationDateParseException = e;
            }
        }
        Object[] objArr5 = {"creation-date"};
        String contentDispositionCreationDate2 = (String) Map.class.getMethod("get", Object.class).invoke(this.contentDispositionParameters, objArr5);
        if (contentDispositionCreationDate2 != null) {
            try {
                Object[] objArr6 = {contentDispositionCreationDate2};
                this.contentDispositionCreationDate = (DateTime) MaximalBodyDescriptor.class.getMethod("parseDate", String.class).invoke(this, objArr6);
            } catch (ParseException e2) {
                this.contentDispositionCreationDateParseException = e2;
            }
        }
        Object[] objArr7 = {"read-date"};
        String contentDispositionReadDate2 = (String) Map.class.getMethod("get", Object.class).invoke(this.contentDispositionParameters, objArr7);
        if (contentDispositionReadDate2 != null) {
            try {
                Object[] objArr8 = {contentDispositionReadDate2};
                this.contentDispositionReadDate = (DateTime) MaximalBodyDescriptor.class.getMethod("parseDate", String.class).invoke(this, objArr8);
            } catch (ParseException e3) {
                this.contentDispositionReadDateParseException = e3;
            }
        }
        Object[] objArr9 = {"size"};
        String size = (String) Map.class.getMethod("get", Object.class).invoke(this.contentDispositionParameters, objArr9);
        if (size != null) {
            try {
                Object[] objArr10 = {size};
                this.contentDispositionSize = ((Long) Long.class.getMethod("parseLong", String.class).invoke(null, objArr10)).longValue();
            } catch (NumberFormatException e4) {
                Method method = NumberFormatException.class.getMethod("getMessage", new Class[0]);
                this.contentDispositionSizeParseException = (MimeException) ((Throwable) MimeException.class.getMethod("fillInStackTrace", new Class[0]).invoke(new MimeException((String) method.invoke(e4, new Object[0]), e4), new Object[0]));
            }
        }
        Object[] objArr11 = {""};
        Map.class.getMethod("remove", Object.class).invoke(this.contentDispositionParameters, objArr11);
    }

    private DateTime parseDate(String date) throws ParseException {
        return (DateTime) DateTimeParser.class.getMethod("date_time", new Class[0]).invoke(new DateTimeParser(new StringReader(date)), new Object[0]);
    }

    private void parseContentDescription(String value) {
        if (value == null) {
            this.contentDescription = "";
        } else {
            this.contentDescription = (String) String.class.getMethod("trim", new Class[0]).invoke(value, new Object[0]);
        }
        this.isContentDescriptionSet = true;
    }

    private void parseContentId(String value) {
        if (value == null) {
            this.contentId = "";
        } else {
            this.contentId = (String) String.class.getMethod("trim", new Class[0]).invoke(value, new Object[0]);
        }
        this.isContentIdSet = true;
    }

    private void parseMimeVersion(String value) {
        MimeVersionParser parser = new MimeVersionParser(new StringReader(value));
        try {
            MimeVersionParser.class.getMethod("parse", new Class[0]).invoke(parser, new Object[0]);
            int major = ((Integer) MimeVersionParser.class.getMethod("getMajorVersion", new Class[0]).invoke(parser, new Object[0])).intValue();
            if (major != -1) {
                this.mimeMajorVersion = major;
            }
            int minor = ((Integer) MimeVersionParser.class.getMethod("getMinorVersion", new Class[0]).invoke(parser, new Object[0])).intValue();
            if (minor != -1) {
                this.mimeMinorVersion = minor;
            }
        } catch (MimeException e) {
            this.mimeVersionException = e;
        }
        this.isMimeVersionSet = true;
    }

    public int getMimeMajorVersion() {
        return this.mimeMajorVersion;
    }

    public int getMimeMinorVersion() {
        return this.mimeMinorVersion;
    }

    public MimeException getMimeVersionParseException() {
        return this.mimeVersionException;
    }

    public String getContentDescription() {
        return this.contentDescription;
    }

    public String getContentId() {
        return this.contentId;
    }

    public String getContentDispositionType() {
        return this.contentDispositionType;
    }

    public Map<String, String> getContentDispositionParameters() {
        return this.contentDispositionParameters;
    }

    public String getContentDispositionFilename() {
        return (String) Map.class.getMethod("get", Object.class).invoke(this.contentDispositionParameters, "filename");
    }

    public DateTime getContentDispositionModificationDate() {
        return this.contentDispositionModificationDate;
    }

    public MimeException getContentDispositionModificationDateParseException() {
        return this.contentDispositionModificationDateParseException;
    }

    public DateTime getContentDispositionCreationDate() {
        return this.contentDispositionCreationDate;
    }

    public MimeException getContentDispositionCreationDateParseException() {
        return this.contentDispositionCreationDateParseException;
    }

    public DateTime getContentDispositionReadDate() {
        return this.contentDispositionReadDate;
    }

    public MimeException getContentDispositionReadDateParseException() {
        return this.contentDispositionReadDateParseException;
    }

    public long getContentDispositionSize() {
        return this.contentDispositionSize;
    }

    public MimeException getContentDispositionSizeParseException() {
        return this.contentDispositionSizeParseException;
    }

    public List<String> getContentLanguage() {
        return this.contentLanguage;
    }

    public MimeException getContentLanguageParseException() {
        return this.contentLanguageParseException;
    }

    public String getContentLocation() {
        return this.contentLocation;
    }

    public MimeException getContentLocationParseException() {
        return this.contentLocationParseException;
    }

    public String getContentMD5Raw() {
        return this.contentMD5Raw;
    }
}
