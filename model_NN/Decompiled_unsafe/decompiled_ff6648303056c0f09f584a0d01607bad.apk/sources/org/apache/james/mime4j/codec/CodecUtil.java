package org.apache.james.mime4j.codec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CodecUtil {
    static final int DEFAULT_ENCODING_BUFFER_SIZE = 1024;

    public static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[DEFAULT_ENCODING_BUFFER_SIZE];
        while (true) {
            Object[] objArr = {buffer};
            int inputLength = ((Integer) InputStream.class.getMethod("read", byte[].class).invoke(in, objArr)).intValue();
            if (-1 != inputLength) {
                Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
                OutputStream.class.getMethod("write", clsArr).invoke(out, buffer, new Integer(0), new Integer(inputLength));
            } else {
                return;
            }
        }
    }

    public static void encodeQuotedPrintableBinary(InputStream in, OutputStream out) throws IOException {
        Object[] objArr = {in, out};
        QuotedPrintableEncoder.class.getMethod("encode", InputStream.class, OutputStream.class).invoke(new QuotedPrintableEncoder(DEFAULT_ENCODING_BUFFER_SIZE, true), objArr);
    }

    public static void encodeQuotedPrintable(InputStream in, OutputStream out) throws IOException {
        Object[] objArr = {in, out};
        QuotedPrintableEncoder.class.getMethod("encode", InputStream.class, OutputStream.class).invoke(new QuotedPrintableEncoder(DEFAULT_ENCODING_BUFFER_SIZE, false), objArr);
    }

    public static void encodeBase64(InputStream in, OutputStream out) throws IOException {
        Base64OutputStream b64Out = new Base64OutputStream(out);
        Object[] objArr = {in, b64Out};
        CodecUtil.class.getMethod("copy", InputStream.class, OutputStream.class).invoke(null, objArr);
        Base64OutputStream.class.getMethod("close", new Class[0]).invoke(b64Out, new Object[0]);
    }

    public static OutputStream wrapQuotedPrintable(OutputStream out, boolean binary) throws IOException {
        return new QuotedPrintableOutputStream(out, binary);
    }

    public static OutputStream wrapBase64(OutputStream out) throws IOException {
        return new Base64OutputStream(out);
    }
}
