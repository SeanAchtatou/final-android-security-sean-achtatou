package org.apache.james.mime4j.parser;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import org.apache.commons.logging.Log;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.descriptor.BodyDescriptor;
import org.apache.james.mime4j.descriptor.MutableBodyDescriptor;
import org.apache.james.mime4j.io.BufferedLineReaderInputStream;
import org.apache.james.mime4j.io.LimitedInputStream;
import org.apache.james.mime4j.io.LineNumberSource;
import org.apache.james.mime4j.io.LineReaderInputStream;
import org.apache.james.mime4j.io.LineReaderInputStreamAdaptor;
import org.apache.james.mime4j.io.MimeBoundaryInputStream;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.util.MimeUtil;

public class MimeEntity extends AbstractEntity {
    private static final int T_IN_BODYPART = -2;
    private static final int T_IN_MESSAGE = -3;
    private LineReaderInputStreamAdaptor dataStream;
    private final BufferedLineReaderInputStream inbuffer;
    private final LineNumberSource lineSource;
    private MimeBoundaryInputStream mimeStream;
    private int recursionMode;
    private boolean skipHeader;
    private byte[] tmpbuf;

    public MimeEntity(LineNumberSource lineSource2, BufferedLineReaderInputStream inbuffer2, BodyDescriptor parent, int startState, int endState, MimeEntityConfig config) {
        super(parent, startState, endState, config);
        this.lineSource = lineSource2;
        this.inbuffer = inbuffer2;
        this.dataStream = new LineReaderInputStreamAdaptor(inbuffer2, ((Integer) MimeEntityConfig.class.getMethod("getMaxLineLen", new Class[0]).invoke(config, new Object[0])).intValue());
        this.skipHeader = false;
    }

    public MimeEntity(LineNumberSource lineSource2, BufferedLineReaderInputStream inbuffer2, BodyDescriptor parent, int startState, int endState) {
        this(lineSource2, inbuffer2, parent, startState, endState, new MimeEntityConfig());
    }

    public int getRecursionMode() {
        return this.recursionMode;
    }

    public void setRecursionMode(int recursionMode2) {
        this.recursionMode = recursionMode2;
    }

    public void skipHeader(String contentType) {
        if (this.state != 0) {
            StringBuilder sb = new StringBuilder();
            Class[] clsArr = {String.class};
            Object[] objArr = {"Invalid state: "};
            int i = this.state;
            Class[] clsArr2 = {Integer.TYPE};
            Object[] objArr2 = {(String) MimeEntity.class.getMethod("stateToString", clsArr2).invoke(null, new Integer(i))};
            Method method = StringBuilder.class.getMethod("append", String.class);
            throw new IllegalStateException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), new Object[0]));
        }
        this.skipHeader = true;
        Object[] objArr3 = {"Content-Type: "};
        Object[] objArr4 = {contentType};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        Method method3 = StringBuilder.class.getMethod("toString", new Class[0]);
        Object[] objArr5 = {(String) method3.invoke((StringBuilder) method2.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr3), objArr4), new Object[0])};
        MutableBodyDescriptor mutableBodyDescriptor = this.body;
        RawField rawField = new RawField((ByteSequence) ContentUtil.class.getMethod("encode", String.class).invoke(null, objArr5), 12);
        Object[] objArr6 = {rawField};
        MutableBodyDescriptor.class.getMethod("addField", Field.class).invoke(mutableBodyDescriptor, objArr6);
    }

    /* access modifiers changed from: protected */
    public int getLineNumber() {
        if (this.lineSource == null) {
            return -1;
        }
        return ((Integer) LineNumberSource.class.getMethod("getLineNumber", new Class[0]).invoke(this.lineSource, new Object[0])).intValue();
    }

    /* access modifiers changed from: protected */
    public LineReaderInputStream getDataStream() {
        return this.dataStream;
    }

    public EntityStateMachine advance() throws IOException, MimeException {
        int i = 5;
        switch (this.state) {
            case T_IN_MESSAGE /*-3*/:
            case 7:
            case 12:
                this.state = this.endState;
                break;
            case T_IN_BODYPART /*-2*/:
                MimeEntity.class.getMethod("advanceToBoundary", new Class[0]).invoke(this, new Object[0]);
                if (((Boolean) MimeBoundaryInputStream.class.getMethod("eof", new Class[0]).invoke(this.mimeStream, new Object[0])).booleanValue()) {
                    if (!((Boolean) MimeBoundaryInputStream.class.getMethod("isLastPart", new Class[0]).invoke(this.mimeStream, new Object[0])).booleanValue()) {
                        Object[] objArr = {Event.MIME_BODY_PREMATURE_END};
                        MimeEntity.class.getMethod("monitor", Event.class).invoke(this, objArr);
                        MimeEntity.class.getMethod("clearMimeStream", new Class[0]).invoke(this, new Object[0]);
                        this.state = 9;
                        break;
                    }
                }
                if (!((Boolean) MimeBoundaryInputStream.class.getMethod("isLastPart", new Class[0]).invoke(this.mimeStream, new Object[0])).booleanValue()) {
                    MimeEntity.class.getMethod("clearMimeStream", new Class[0]).invoke(this, new Object[0]);
                    MimeEntity.class.getMethod("createMimeStream", new Class[0]).invoke(this, new Object[0]);
                    this.state = T_IN_BODYPART;
                    return (EntityStateMachine) MimeEntity.class.getMethod("nextMimeEntity", new Class[0]).invoke(this, new Object[0]);
                }
                MimeEntity.class.getMethod("clearMimeStream", new Class[0]).invoke(this, new Object[0]);
                this.state = 9;
            case -1:
            case 1:
            case 2:
            case 11:
            default:
                if (this.state == this.endState) {
                    this.state = -1;
                    break;
                } else {
                    StringBuilder sb = new StringBuilder();
                    Class[] clsArr = {String.class};
                    Object[] objArr2 = {"Invalid state: "};
                    int i2 = this.state;
                    Class[] clsArr2 = {Integer.TYPE};
                    Object[] objArr3 = {(String) MimeEntity.class.getMethod("stateToString", clsArr2).invoke(null, new Integer(i2))};
                    Method method = StringBuilder.class.getMethod("append", String.class);
                    throw new IllegalStateException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr2), objArr3), new Object[0]));
                }
            case 0:
                if (!this.skipHeader) {
                    this.state = 3;
                    break;
                } else {
                    this.state = 5;
                    break;
                }
            case 3:
            case 4:
                if (((Boolean) MimeEntity.class.getMethod("parseField", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                    i = 4;
                }
                this.state = i;
                break;
            case 5:
                String mimeType = (String) MutableBodyDescriptor.class.getMethod("getMimeType", new Class[0]).invoke(this.body, new Object[0]);
                if (this.recursionMode != 3) {
                    if (!((Boolean) MimeUtil.class.getMethod("isMultipart", String.class).invoke(null, mimeType)).booleanValue()) {
                        if (this.recursionMode != 1) {
                            if (((Boolean) MimeUtil.class.getMethod("isMessage", String.class).invoke(null, mimeType)).booleanValue()) {
                                this.state = T_IN_MESSAGE;
                                return (EntityStateMachine) MimeEntity.class.getMethod("nextMessage", new Class[0]).invoke(this, new Object[0]);
                            }
                        }
                        this.state = 12;
                        break;
                    } else {
                        this.state = 6;
                        MimeEntity.class.getMethod("clearMimeStream", new Class[0]).invoke(this, new Object[0]);
                        break;
                    }
                } else {
                    this.state = 12;
                    break;
                }
            case 6:
                if (!((Boolean) LineReaderInputStreamAdaptor.class.getMethod("isUsed", new Class[0]).invoke(this.dataStream, new Object[0])).booleanValue()) {
                    MimeEntity.class.getMethod("createMimeStream", new Class[0]).invoke(this, new Object[0]);
                    this.state = 8;
                    break;
                } else {
                    MimeEntity.class.getMethod("advanceToBoundary", new Class[0]).invoke(this, new Object[0]);
                    this.state = 7;
                    break;
                }
            case 8:
                MimeEntity.class.getMethod("advanceToBoundary", new Class[0]).invoke(this, new Object[0]);
                if (((Boolean) MimeBoundaryInputStream.class.getMethod("isLastPart", new Class[0]).invoke(this.mimeStream, new Object[0])).booleanValue()) {
                    MimeEntity.class.getMethod("clearMimeStream", new Class[0]).invoke(this, new Object[0]);
                    this.state = 7;
                    break;
                } else {
                    MimeEntity.class.getMethod("clearMimeStream", new Class[0]).invoke(this, new Object[0]);
                    MimeEntity.class.getMethod("createMimeStream", new Class[0]).invoke(this, new Object[0]);
                    this.state = T_IN_BODYPART;
                    return (EntityStateMachine) MimeEntity.class.getMethod("nextMimeEntity", new Class[0]).invoke(this, new Object[0]);
                }
            case 9:
                this.state = 7;
                break;
            case 10:
                this.state = 3;
                break;
        }
        return null;
    }

    private void createMimeStream() throws MimeException, IOException {
        String boundary = (String) MutableBodyDescriptor.class.getMethod("getBoundary", new Class[0]).invoke(this.body, new Object[0]);
        int bufferSize = ((Integer) String.class.getMethod("length", new Class[0]).invoke(boundary, new Object[0])).intValue() * 2;
        if (bufferSize < 4096) {
            bufferSize = 4096;
        }
        try {
            if (this.mimeStream != null) {
                this.mimeStream = new MimeBoundaryInputStream(new BufferedLineReaderInputStream(this.mimeStream, bufferSize, ((Integer) MimeEntityConfig.class.getMethod("getMaxLineLen", new Class[0]).invoke(this.config, new Object[0])).intValue()), boundary);
            } else {
                BufferedLineReaderInputStream bufferedLineReaderInputStream = this.inbuffer;
                Class[] clsArr = {Integer.TYPE};
                BufferedLineReaderInputStream.class.getMethod("ensureCapacity", clsArr).invoke(bufferedLineReaderInputStream, new Integer(bufferSize));
                this.mimeStream = new MimeBoundaryInputStream(this.inbuffer, boundary);
            }
            this.dataStream = new LineReaderInputStreamAdaptor(this.mimeStream, ((Integer) MimeEntityConfig.class.getMethod("getMaxLineLen", new Class[0]).invoke(this.config, new Object[0])).intValue());
        } catch (IllegalArgumentException e) {
            throw new MimeException((String) IllegalArgumentException.class.getMethod("getMessage", new Class[0]).invoke(e, new Object[0]), e);
        }
    }

    private void clearMimeStream() {
        this.mimeStream = null;
        this.dataStream = new LineReaderInputStreamAdaptor(this.inbuffer, ((Integer) MimeEntityConfig.class.getMethod("getMaxLineLen", new Class[0]).invoke(this.config, new Object[0])).intValue());
    }

    private void advanceToBoundary() throws IOException {
        Class[] clsArr;
        if (!((Boolean) LineReaderInputStreamAdaptor.class.getMethod("eof", new Class[0]).invoke(this.dataStream, new Object[0])).booleanValue()) {
            if (this.tmpbuf == null) {
                this.tmpbuf = new byte[2048];
            }
            do {
                clsArr = new Class[]{byte[].class};
            } while (((Integer) InputStream.class.getMethod("read", clsArr).invoke((InputStream) MimeEntity.class.getMethod("getLimitedContentStream", new Class[0]).invoke(this, new Object[0]), this.tmpbuf)).intValue() != -1);
        }
    }

    private EntityStateMachine nextMessage() {
        InputStream instream;
        String transferEncoding = (String) MutableBodyDescriptor.class.getMethod("getTransferEncoding", new Class[0]).invoke(this.body, new Object[0]);
        if (((Boolean) MimeUtil.class.getMethod("isBase64Encoding", String.class).invoke(null, transferEncoding)).booleanValue()) {
            Object[] objArr = {"base64 encoded message/rfc822 detected"};
            Log.class.getMethod("debug", Object.class).invoke(this.log, objArr);
            instream = new Base64InputStream(this.dataStream);
        } else {
            if (((Boolean) MimeUtil.class.getMethod("isQuotedPrintableEncoded", String.class).invoke(null, transferEncoding)).booleanValue()) {
                Object[] objArr2 = {"quoted-printable encoded message/rfc822 detected"};
                Log.class.getMethod("debug", Object.class).invoke(this.log, objArr2);
                instream = new QuotedPrintableInputStream(this.dataStream);
            } else {
                instream = this.dataStream;
            }
        }
        if (this.recursionMode == 2) {
            return new RawEntity(instream);
        }
        MimeEntity message = new MimeEntity(this.lineSource, new BufferedLineReaderInputStream(instream, 4096, ((Integer) MimeEntityConfig.class.getMethod("getMaxLineLen", new Class[0]).invoke(this.config, new Object[0])).intValue()), this.body, 0, 1, this.config);
        int i = this.recursionMode;
        Class[] clsArr = {Integer.TYPE};
        MimeEntity.class.getMethod("setRecursionMode", clsArr).invoke(message, new Integer(i));
        return message;
    }

    private EntityStateMachine nextMimeEntity() {
        if (this.recursionMode == 2) {
            return new RawEntity(this.mimeStream);
        }
        MimeEntity mimeentity = new MimeEntity(this.lineSource, new BufferedLineReaderInputStream(this.mimeStream, 4096, ((Integer) MimeEntityConfig.class.getMethod("getMaxLineLen", new Class[0]).invoke(this.config, new Object[0])).intValue()), this.body, 10, 11, this.config);
        int i = this.recursionMode;
        Class[] clsArr = {Integer.TYPE};
        MimeEntity.class.getMethod("setRecursionMode", clsArr).invoke(mimeentity, new Integer(i));
        return mimeentity;
    }

    private InputStream getLimitedContentStream() {
        long maxContentLimit = ((Long) MimeEntityConfig.class.getMethod("getMaxContentLen", new Class[0]).invoke(this.config, new Object[0])).longValue();
        if (maxContentLimit >= 0) {
            return new LimitedInputStream(this.dataStream, maxContentLimit);
        }
        return this.dataStream;
    }

    public InputStream getContentStream() {
        switch (this.state) {
            case 6:
            case 8:
            case 9:
            case 12:
                break;
            case 7:
            case 10:
            case 11:
            default:
                StringBuilder sb = new StringBuilder();
                Class[] clsArr = {String.class};
                Object[] objArr = {"Invalid state: "};
                int i = this.state;
                Class[] clsArr2 = {Integer.TYPE};
                Object[] objArr2 = {(String) MimeEntity.class.getMethod("stateToString", clsArr2).invoke(null, new Integer(i))};
                Method method = StringBuilder.class.getMethod("append", String.class);
                throw new IllegalStateException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), new Object[0]));
        }
        return (InputStream) MimeEntity.class.getMethod("getLimitedContentStream", new Class[0]).invoke(this, new Object[0]);
    }
}
