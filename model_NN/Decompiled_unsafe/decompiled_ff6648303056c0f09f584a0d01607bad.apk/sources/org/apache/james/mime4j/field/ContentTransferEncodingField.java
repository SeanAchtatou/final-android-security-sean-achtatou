package org.apache.james.mime4j.field;

import java.lang.reflect.Method;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.MimeUtil;

public class ContentTransferEncodingField extends AbstractField {
    static final FieldParser PARSER = new FieldParser() {
        public ParsedField parse(String name, String body, ByteSequence raw) {
            return new ContentTransferEncodingField(name, body, raw);
        }
    };
    private String encoding;

    ContentTransferEncodingField(String name, String body, ByteSequence raw) {
        super(name, body, raw);
        Method method = String.class.getMethod("trim", new Class[0]);
        this.encoding = (String) String.class.getMethod("toLowerCase", new Class[0]).invoke((String) method.invoke(body, new Object[0]), new Object[0]);
    }

    public String getEncoding() {
        return this.encoding;
    }

    public static String getEncoding(ContentTransferEncodingField f) {
        if (f != null) {
            Method method = ContentTransferEncodingField.class.getMethod("getEncoding", new Class[0]);
            if (((Integer) String.class.getMethod("length", new Class[0]).invoke((String) method.invoke(f, new Object[0]), new Object[0])).intValue() != 0) {
                return (String) ContentTransferEncodingField.class.getMethod("getEncoding", new Class[0]).invoke(f, new Object[0]);
            }
        }
        return MimeUtil.ENC_7BIT;
    }
}
