package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.descriptor.BodyDescriptor;
import org.apache.james.mime4j.field.AbstractField;
import org.apache.james.mime4j.field.ParsedField;
import org.apache.james.mime4j.parser.AbstractContentHandler;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.MimeUtil;

public abstract class SimpleContentHandler extends AbstractContentHandler {
    private Header currHeader;

    public abstract void bodyDecoded(BodyDescriptor bodyDescriptor, InputStream inputStream) throws IOException;

    public abstract void headers(Header header);

    public final void startHeader() {
        this.currHeader = new Header();
    }

    public final void field(Field field) throws MimeException {
        Object[] objArr = {(ByteSequence) Field.class.getMethod("getRaw", new Class[0]).invoke(field, new Object[0])};
        Object[] objArr2 = {(ParsedField) AbstractField.class.getMethod("parse", ByteSequence.class).invoke(null, objArr)};
        Header.class.getMethod("addField", Field.class).invoke(this.currHeader, objArr2);
    }

    public final void endHeader() {
        Header tmp = this.currHeader;
        this.currHeader = null;
        Object[] objArr = {tmp};
        SimpleContentHandler.class.getMethod("headers", Header.class).invoke(this, objArr);
    }

    public final void body(BodyDescriptor bd, InputStream is) throws IOException {
        Class[] clsArr = {String.class};
        if (((Boolean) MimeUtil.class.getMethod("isBase64Encoding", clsArr).invoke(null, (String) BodyDescriptor.class.getMethod("getTransferEncoding", new Class[0]).invoke(bd, new Object[0]))).booleanValue()) {
            Object[] objArr = {bd, new Base64InputStream(is)};
            SimpleContentHandler.class.getMethod("bodyDecoded", BodyDescriptor.class, InputStream.class).invoke(this, objArr);
            return;
        }
        Class[] clsArr2 = {String.class};
        if (((Boolean) MimeUtil.class.getMethod("isQuotedPrintableEncoded", clsArr2).invoke(null, (String) BodyDescriptor.class.getMethod("getTransferEncoding", new Class[0]).invoke(bd, new Object[0]))).booleanValue()) {
            Object[] objArr2 = {bd, new QuotedPrintableInputStream(is)};
            SimpleContentHandler.class.getMethod("bodyDecoded", BodyDescriptor.class, InputStream.class).invoke(this, objArr2);
            return;
        }
        Object[] objArr3 = {bd, is};
        SimpleContentHandler.class.getMethod("bodyDecoded", BodyDescriptor.class, InputStream.class).invoke(this, objArr3);
    }
}
