package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import org.apache.james.mime4j.codec.CodecUtil;
import org.apache.james.mime4j.storage.MultiReferenceStorage;
import org.apache.james.mime4j.util.CharsetUtil;

class StorageTextBody extends TextBody {
    private Charset charset;
    private MultiReferenceStorage storage;

    public StorageTextBody(MultiReferenceStorage storage2, Charset charset2) {
        this.storage = storage2;
        this.charset = charset2;
    }

    public String getMimeCharset() {
        Class[] clsArr = {String.class};
        return (String) CharsetUtil.class.getMethod("toMimeCharset", clsArr).invoke(null, (String) Charset.class.getMethod("name", new Class[0]).invoke(this.charset, new Object[0]));
    }

    public Reader getReader() throws IOException {
        return new InputStreamReader((InputStream) MultiReferenceStorage.class.getMethod("getInputStream", new Class[0]).invoke(this.storage, new Object[0]), this.charset);
    }

    public void writeTo(OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException();
        }
        InputStream in = (InputStream) MultiReferenceStorage.class.getMethod("getInputStream", new Class[0]).invoke(this.storage, new Object[0]);
        Object[] objArr = {in, out};
        CodecUtil.class.getMethod("copy", InputStream.class, OutputStream.class).invoke(null, objArr);
        InputStream.class.getMethod("close", new Class[0]).invoke(in, new Object[0]);
    }

    public StorageTextBody copy() {
        MultiReferenceStorage.class.getMethod("addReference", new Class[0]).invoke(this.storage, new Object[0]);
        return new StorageTextBody(this.storage, this.charset);
    }

    public void dispose() {
        if (this.storage != null) {
            MultiReferenceStorage.class.getMethod("delete", new Class[0]).invoke(this.storage, new Object[0]);
            this.storage = null;
        }
    }
}
