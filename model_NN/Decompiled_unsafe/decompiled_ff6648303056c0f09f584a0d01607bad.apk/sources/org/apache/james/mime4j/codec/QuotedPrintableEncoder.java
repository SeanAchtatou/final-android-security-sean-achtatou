package org.apache.james.mime4j.codec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

final class QuotedPrintableEncoder {
    private static final byte CR = 13;
    private static final byte EQUALS = 61;
    private static final byte[] HEX_DIGITS = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70};
    private static final byte LF = 10;
    private static final byte QUOTED_PRINTABLE_LAST_PLAIN = 126;
    private static final int QUOTED_PRINTABLE_MAX_LINE_LENGTH = 76;
    private static final int QUOTED_PRINTABLE_OCTETS_PER_ESCAPE = 3;
    private static final byte SPACE = 32;
    private static final byte TAB = 9;
    private final boolean binary;
    private final byte[] inBuffer;
    private int nextSoftBreak = 77;
    private OutputStream out = null;
    private final byte[] outBuffer;
    private int outputIndex = 0;
    private boolean pendingCR;
    private boolean pendingSpace;
    private boolean pendingTab;

    public QuotedPrintableEncoder(int bufferSize, boolean binary2) {
        this.inBuffer = new byte[bufferSize];
        this.outBuffer = new byte[(bufferSize * 3)];
        this.binary = binary2;
        this.pendingSpace = false;
        this.pendingTab = false;
        this.pendingCR = false;
    }

    /* access modifiers changed from: package-private */
    public void initEncoding(OutputStream out2) {
        this.out = out2;
        this.pendingSpace = false;
        this.pendingTab = false;
        this.pendingCR = false;
        this.nextSoftBreak = 77;
    }

    /* access modifiers changed from: package-private */
    public void encodeChunk(byte[] buffer, int off, int len) throws IOException {
        for (int inputIndex = off; inputIndex < len + off; inputIndex++) {
            byte b = buffer[inputIndex];
            Class[] clsArr = {Byte.TYPE};
            QuotedPrintableEncoder.class.getMethod("encode", clsArr).invoke(this, new Byte(b));
        }
    }

    /* access modifiers changed from: package-private */
    public void completeEncoding() throws IOException {
        QuotedPrintableEncoder.class.getMethod("writePending", new Class[0]).invoke(this, new Object[0]);
        QuotedPrintableEncoder.class.getMethod("flushOutput", new Class[0]).invoke(this, new Object[0]);
    }

    public void encode(InputStream in, OutputStream out2) throws IOException {
        Object[] objArr = {out2};
        QuotedPrintableEncoder.class.getMethod("initEncoding", OutputStream.class).invoke(this, objArr);
        while (true) {
            Object[] objArr2 = {this.inBuffer};
            int inputLength = ((Integer) InputStream.class.getMethod("read", byte[].class).invoke(in, objArr2)).intValue();
            if (inputLength > -1) {
                byte[] bArr = this.inBuffer;
                Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
                QuotedPrintableEncoder.class.getMethod("encodeChunk", clsArr).invoke(this, bArr, new Integer(0), new Integer(inputLength));
            } else {
                QuotedPrintableEncoder.class.getMethod("completeEncoding", new Class[0]).invoke(this, new Object[0]);
                return;
            }
        }
    }

    private void writePending() throws IOException {
        if (this.pendingSpace) {
            Class[] clsArr = {Byte.TYPE};
            QuotedPrintableEncoder.class.getMethod("plain", clsArr).invoke(this, new Byte((byte) SPACE));
        } else if (this.pendingTab) {
            Class[] clsArr2 = {Byte.TYPE};
            QuotedPrintableEncoder.class.getMethod("plain", clsArr2).invoke(this, new Byte((byte) TAB));
        } else if (this.pendingCR) {
            Class[] clsArr3 = {Byte.TYPE};
            QuotedPrintableEncoder.class.getMethod("plain", clsArr3).invoke(this, new Byte((byte) CR));
        }
        QuotedPrintableEncoder.class.getMethod("clearPending", new Class[0]).invoke(this, new Object[0]);
    }

    private void clearPending() throws IOException {
        this.pendingSpace = false;
        this.pendingTab = false;
        this.pendingCR = false;
    }

    private void encode(byte next) throws IOException {
        if (next == 10) {
            if (this.binary) {
                QuotedPrintableEncoder.class.getMethod("writePending", new Class[0]).invoke(this, new Object[0]);
                Class[] clsArr = {Byte.TYPE};
                QuotedPrintableEncoder.class.getMethod("escape", clsArr).invoke(this, new Byte(next));
            } else if (this.pendingCR) {
                if (this.pendingSpace) {
                    Class[] clsArr2 = {Byte.TYPE};
                    QuotedPrintableEncoder.class.getMethod("escape", clsArr2).invoke(this, new Byte((byte) SPACE));
                } else if (this.pendingTab) {
                    Class[] clsArr3 = {Byte.TYPE};
                    QuotedPrintableEncoder.class.getMethod("escape", clsArr3).invoke(this, new Byte((byte) TAB));
                }
                QuotedPrintableEncoder.class.getMethod("lineBreak", new Class[0]).invoke(this, new Object[0]);
                QuotedPrintableEncoder.class.getMethod("clearPending", new Class[0]).invoke(this, new Object[0]);
            } else {
                QuotedPrintableEncoder.class.getMethod("writePending", new Class[0]).invoke(this, new Object[0]);
                Class[] clsArr4 = {Byte.TYPE};
                QuotedPrintableEncoder.class.getMethod("plain", clsArr4).invoke(this, new Byte(next));
            }
        } else if (next != 13) {
            QuotedPrintableEncoder.class.getMethod("writePending", new Class[0]).invoke(this, new Object[0]);
            if (next == 32) {
                if (this.binary) {
                    Class[] clsArr5 = {Byte.TYPE};
                    QuotedPrintableEncoder.class.getMethod("escape", clsArr5).invoke(this, new Byte(next));
                    return;
                }
                this.pendingSpace = true;
            } else if (next == 9) {
                if (this.binary) {
                    Class[] clsArr6 = {Byte.TYPE};
                    QuotedPrintableEncoder.class.getMethod("escape", clsArr6).invoke(this, new Byte(next));
                    return;
                }
                this.pendingTab = true;
            } else if (next < 32) {
                Class[] clsArr7 = {Byte.TYPE};
                QuotedPrintableEncoder.class.getMethod("escape", clsArr7).invoke(this, new Byte(next));
            } else if (next > 126) {
                Class[] clsArr8 = {Byte.TYPE};
                QuotedPrintableEncoder.class.getMethod("escape", clsArr8).invoke(this, new Byte(next));
            } else if (next == 61) {
                Class[] clsArr9 = {Byte.TYPE};
                QuotedPrintableEncoder.class.getMethod("escape", clsArr9).invoke(this, new Byte(next));
            } else {
                Class[] clsArr10 = {Byte.TYPE};
                QuotedPrintableEncoder.class.getMethod("plain", clsArr10).invoke(this, new Byte(next));
            }
        } else if (this.binary) {
            Class[] clsArr11 = {Byte.TYPE};
            QuotedPrintableEncoder.class.getMethod("escape", clsArr11).invoke(this, new Byte(next));
        } else {
            this.pendingCR = true;
        }
    }

    private void plain(byte next) throws IOException {
        int i = this.nextSoftBreak - 1;
        this.nextSoftBreak = i;
        if (i <= 1) {
            QuotedPrintableEncoder.class.getMethod("softBreak", new Class[0]).invoke(this, new Object[0]);
        }
        QuotedPrintableEncoder.class.getMethod("write", Byte.TYPE).invoke(this, new Byte(next));
    }

    private void escape(byte next) throws IOException {
        int i = this.nextSoftBreak - 1;
        this.nextSoftBreak = i;
        if (i <= 3) {
            QuotedPrintableEncoder.class.getMethod("softBreak", new Class[0]).invoke(this, new Object[0]);
        }
        int nextUnsigned = next & 255;
        QuotedPrintableEncoder.class.getMethod("write", Byte.TYPE).invoke(this, new Byte((byte) EQUALS));
        this.nextSoftBreak--;
        byte b = HEX_DIGITS[nextUnsigned >> 4];
        QuotedPrintableEncoder.class.getMethod("write", Byte.TYPE).invoke(this, new Byte(b));
        this.nextSoftBreak--;
        byte b2 = HEX_DIGITS[nextUnsigned % 16];
        QuotedPrintableEncoder.class.getMethod("write", Byte.TYPE).invoke(this, new Byte(b2));
    }

    private void write(byte next) throws IOException {
        byte[] bArr = this.outBuffer;
        int i = this.outputIndex;
        this.outputIndex = i + 1;
        bArr[i] = next;
        if (this.outputIndex >= this.outBuffer.length) {
            QuotedPrintableEncoder.class.getMethod("flushOutput", new Class[0]).invoke(this, new Object[0]);
        }
    }

    private void softBreak() throws IOException {
        Class[] clsArr = {Byte.TYPE};
        QuotedPrintableEncoder.class.getMethod("write", clsArr).invoke(this, new Byte((byte) EQUALS));
        QuotedPrintableEncoder.class.getMethod("lineBreak", new Class[0]).invoke(this, new Object[0]);
    }

    private void lineBreak() throws IOException {
        Class[] clsArr = {Byte.TYPE};
        QuotedPrintableEncoder.class.getMethod("write", clsArr).invoke(this, new Byte((byte) CR));
        Class[] clsArr2 = {Byte.TYPE};
        QuotedPrintableEncoder.class.getMethod("write", clsArr2).invoke(this, new Byte((byte) LF));
        this.nextSoftBreak = QUOTED_PRINTABLE_MAX_LINE_LENGTH;
    }

    /* access modifiers changed from: package-private */
    public void flushOutput() throws IOException {
        if (this.outputIndex < this.outBuffer.length) {
            OutputStream outputStream = this.out;
            byte[] bArr = this.outBuffer;
            int i = this.outputIndex;
            Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
            OutputStream.class.getMethod("write", clsArr).invoke(outputStream, bArr, new Integer(0), new Integer(i));
        } else {
            OutputStream outputStream2 = this.out;
            Object[] objArr = {this.outBuffer};
            OutputStream.class.getMethod("write", byte[].class).invoke(outputStream2, objArr);
        }
        this.outputIndex = 0;
    }
}
