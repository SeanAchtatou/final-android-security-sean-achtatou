package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import org.apache.james.mime4j.codec.CodecUtil;
import org.apache.james.mime4j.field.ContentTypeField;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.ByteArrayBuffer;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.util.MimeUtil;

public class MessageWriter {
    private static final byte[] CRLF = {13, 10};
    private static final byte[] DASHES = {45, 45};
    public static final MessageWriter DEFAULT = new MessageWriter();

    protected MessageWriter() {
    }

    public void writeBody(Body body, OutputStream out) throws IOException {
        if (body instanceof Message) {
            Object[] objArr = {(Message) body, out};
            MessageWriter.class.getMethod("writeEntity", Entity.class, OutputStream.class).invoke(this, objArr);
        } else if (body instanceof Multipart) {
            Object[] objArr2 = {(Multipart) body, out};
            MessageWriter.class.getMethod("writeMultipart", Multipart.class, OutputStream.class).invoke(this, objArr2);
        } else if (body instanceof SingleBody) {
            Method method = SingleBody.class.getMethod("writeTo", OutputStream.class);
            method.invoke((SingleBody) body, out);
        } else {
            throw new IllegalArgumentException("Unsupported body class");
        }
    }

    public void writeEntity(Entity entity, OutputStream out) throws IOException {
        Header header = (Header) Entity.class.getMethod("getHeader", new Class[0]).invoke(entity, new Object[0]);
        if (header == null) {
            throw new IllegalArgumentException("Missing header");
        }
        Object[] objArr = {header, out};
        MessageWriter.class.getMethod("writeHeader", Header.class, OutputStream.class).invoke(this, objArr);
        Body body = (Body) Entity.class.getMethod("getBody", new Class[0]).invoke(entity, new Object[0]);
        if (body == null) {
            throw new IllegalArgumentException("Missing body");
        }
        Method method = Entity.class.getMethod("getContentTransferEncoding", new Class[0]);
        Class[] clsArr = {OutputStream.class, String.class, Boolean.TYPE};
        OutputStream encOut = (OutputStream) MessageWriter.class.getMethod("encodeStream", clsArr).invoke(this, out, (String) method.invoke(entity, new Object[0]), new Boolean(body instanceof BinaryBody));
        Object[] objArr2 = {body, encOut};
        MessageWriter.class.getMethod("writeBody", Body.class, OutputStream.class).invoke(this, objArr2);
        if (encOut != out) {
            OutputStream.class.getMethod("close", new Class[0]).invoke(encOut, new Object[0]);
        }
    }

    public void writeMultipart(Multipart multipart, OutputStream out) throws IOException {
        Object[] objArr = {multipart};
        Object[] objArr2 = {(ContentTypeField) MessageWriter.class.getMethod("getContentType", Multipart.class).invoke(this, objArr)};
        ByteSequence boundary = (ByteSequence) MessageWriter.class.getMethod("getBoundary", ContentTypeField.class).invoke(this, objArr2);
        Object[] objArr3 = {(ByteSequence) Multipart.class.getMethod("getPreambleRaw", new Class[0]).invoke(multipart, new Object[0]), out};
        MessageWriter.class.getMethod("writeBytes", ByteSequence.class, OutputStream.class).invoke(this, objArr3);
        Object[] objArr4 = {CRLF};
        OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr4);
        Method method = Multipart.class.getMethod("getBodyParts", new Class[0]);
        Iterator i$ = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke((List) method.invoke(multipart, new Object[0]), new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Method method2 = Iterator.class.getMethod("next", new Class[0]);
                Object[] objArr5 = {DASHES};
                OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr5);
                Object[] objArr6 = {boundary, out};
                MessageWriter.class.getMethod("writeBytes", ByteSequence.class, OutputStream.class).invoke(this, objArr6);
                Object[] objArr7 = {CRLF};
                OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr7);
                Object[] objArr8 = {(BodyPart) method2.invoke(i$, new Object[0]), out};
                MessageWriter.class.getMethod("writeEntity", Entity.class, OutputStream.class).invoke(this, objArr8);
                Object[] objArr9 = {CRLF};
                OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr9);
            } else {
                Object[] objArr10 = {DASHES};
                OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr10);
                Object[] objArr11 = {boundary, out};
                MessageWriter.class.getMethod("writeBytes", ByteSequence.class, OutputStream.class).invoke(this, objArr11);
                Object[] objArr12 = {DASHES};
                OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr12);
                Object[] objArr13 = {CRLF};
                OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr13);
                Object[] objArr14 = {(ByteSequence) Multipart.class.getMethod("getEpilogueRaw", new Class[0]).invoke(multipart, new Object[0]), out};
                MessageWriter.class.getMethod("writeBytes", ByteSequence.class, OutputStream.class).invoke(this, objArr14);
                return;
            }
        }
    }

    public void writeHeader(Header header, OutputStream out) throws IOException {
        Iterator i$ = (Iterator) Header.class.getMethod("iterator", new Class[0]).invoke(header, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Method method = Iterator.class.getMethod("next", new Class[0]);
                Object[] objArr = {(ByteSequence) Field.class.getMethod("getRaw", new Class[0]).invoke((Field) method.invoke(i$, new Object[0]), new Object[0]), out};
                MessageWriter.class.getMethod("writeBytes", ByteSequence.class, OutputStream.class).invoke(this, objArr);
                Object[] objArr2 = {CRLF};
                OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr2);
            } else {
                Object[] objArr3 = {CRLF};
                OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr3);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public OutputStream encodeStream(OutputStream out, String encoding, boolean binaryBody) throws IOException {
        if (((Boolean) MimeUtil.class.getMethod("isBase64Encoding", String.class).invoke(null, encoding)).booleanValue()) {
            return (OutputStream) CodecUtil.class.getMethod("wrapBase64", OutputStream.class).invoke(null, out);
        }
        if (!((Boolean) MimeUtil.class.getMethod("isQuotedPrintableEncoded", String.class).invoke(null, encoding)).booleanValue()) {
            return out;
        }
        Class[] clsArr = {OutputStream.class, Boolean.TYPE};
        return (OutputStream) CodecUtil.class.getMethod("wrapQuotedPrintable", clsArr).invoke(null, out, new Boolean(binaryBody));
    }

    private ContentTypeField getContentType(Multipart multipart) {
        Entity parent = (Entity) Multipart.class.getMethod("getParent", new Class[0]).invoke(multipart, new Object[0]);
        if (parent == null) {
            throw new IllegalArgumentException("Missing parent entity in multipart");
        }
        Header header = (Header) Entity.class.getMethod("getHeader", new Class[0]).invoke(parent, new Object[0]);
        if (header == null) {
            throw new IllegalArgumentException("Missing header in parent entity");
        }
        Object[] objArr = {"Content-Type"};
        ContentTypeField contentType = (ContentTypeField) ((Field) Header.class.getMethod("getField", String.class).invoke(header, objArr));
        if (contentType != null) {
            return contentType;
        }
        throw new IllegalArgumentException("Content-Type field not specified");
    }

    private ByteSequence getBoundary(ContentTypeField contentType) {
        String boundary = (String) ContentTypeField.class.getMethod("getBoundary", new Class[0]).invoke(contentType, new Object[0]);
        if (boundary == null) {
            throw new IllegalArgumentException("Multipart boundary not specified");
        }
        return (ByteSequence) ContentUtil.class.getMethod("encode", String.class).invoke(null, boundary);
    }

    private void writeBytes(ByteSequence byteSequence, OutputStream out) throws IOException {
        if (byteSequence instanceof ByteArrayBuffer) {
            ByteArrayBuffer bab = (ByteArrayBuffer) byteSequence;
            Method method = ByteArrayBuffer.class.getMethod("buffer", new Class[0]);
            int intValue = ((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(bab, new Object[0])).intValue();
            Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
            OutputStream.class.getMethod("write", clsArr).invoke(out, (byte[]) method.invoke(bab, new Object[0]), new Integer(0), new Integer(intValue));
            return;
        }
        Object[] objArr = {(byte[]) ByteSequence.class.getMethod("toByteArray", new Class[0]).invoke(byteSequence, new Object[0])};
        OutputStream.class.getMethod("write", byte[].class).invoke(out, objArr);
    }
}
