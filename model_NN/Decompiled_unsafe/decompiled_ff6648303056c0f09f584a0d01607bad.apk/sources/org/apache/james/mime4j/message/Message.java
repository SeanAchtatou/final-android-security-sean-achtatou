package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.MimeIOException;
import org.apache.james.mime4j.field.AddressListField;
import org.apache.james.mime4j.field.DateTimeField;
import org.apache.james.mime4j.field.FieldName;
import org.apache.james.mime4j.field.Fields;
import org.apache.james.mime4j.field.MailboxField;
import org.apache.james.mime4j.field.MailboxListField;
import org.apache.james.mime4j.field.UnstructuredField;
import org.apache.james.mime4j.field.address.Address;
import org.apache.james.mime4j.field.address.AddressList;
import org.apache.james.mime4j.field.address.Mailbox;
import org.apache.james.mime4j.field.address.MailboxList;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.parser.MimeEntityConfig;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.storage.DefaultStorageProvider;
import org.apache.james.mime4j.storage.StorageProvider;

public class Message extends Entity implements Body {
    public Message() {
    }

    public Message(Message other) {
        super(other);
    }

    public Message(InputStream is) throws IOException, MimeIOException {
        this(is, null, (StorageProvider) DefaultStorageProvider.class.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]));
    }

    public Message(InputStream is, MimeEntityConfig config) throws IOException, MimeIOException {
        this(is, config, (StorageProvider) DefaultStorageProvider.class.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]));
    }

    public Message(InputStream is, MimeEntityConfig config, StorageProvider storageProvider) throws IOException, MimeIOException {
        try {
            MimeStreamParser parser = new MimeStreamParser(config);
            Object[] objArr = {new MessageBuilder(this, storageProvider)};
            MimeStreamParser.class.getMethod("setContentHandler", ContentHandler.class).invoke(parser, objArr);
            Object[] objArr2 = {is};
            MimeStreamParser.class.getMethod("parse", InputStream.class).invoke(parser, objArr2);
        } catch (MimeException e) {
            throw new MimeIOException(e);
        }
    }

    public void writeTo(OutputStream out) throws IOException {
        Object[] objArr = {this, out};
        MessageWriter.class.getMethod("writeEntity", Entity.class, OutputStream.class).invoke(MessageWriter.DEFAULT, objArr);
    }

    public String getMessageId() {
        Object[] objArr = {FieldName.MESSAGE_ID};
        Field field = (Field) Message.class.getMethod("obtainField", String.class).invoke(this, objArr);
        if (field == null) {
            return null;
        }
        return (String) Field.class.getMethod("getBody", new Class[0]).invoke(field, new Object[0]);
    }

    public void createMessageId(String hostname) {
        Method method = Message.class.getMethod("obtainHeader", new Class[0]);
        Object[] objArr = {hostname};
        Object[] objArr2 = {(Field) Fields.class.getMethod("messageId", String.class).invoke(null, objArr)};
        Header.class.getMethod("setField", Field.class).invoke((Header) method.invoke(this, new Object[0]), objArr2);
    }

    public String getSubject() {
        Object[] objArr = {FieldName.SUBJECT};
        UnstructuredField field = (UnstructuredField) ((Field) Message.class.getMethod("obtainField", String.class).invoke(this, objArr));
        if (field == null) {
            return null;
        }
        return (String) UnstructuredField.class.getMethod("getValue", new Class[0]).invoke(field, new Object[0]);
    }

    public void setSubject(String subject) {
        Header header = (Header) Message.class.getMethod("obtainHeader", new Class[0]).invoke(this, new Object[0]);
        if (subject == null) {
            Object[] objArr = {FieldName.SUBJECT};
            ((Integer) Header.class.getMethod("removeFields", String.class).invoke(header, objArr)).intValue();
            return;
        }
        Object[] objArr2 = {subject};
        Object[] objArr3 = {(UnstructuredField) Fields.class.getMethod("subject", String.class).invoke(null, objArr2)};
        Header.class.getMethod("setField", Field.class).invoke(header, objArr3);
    }

    public Date getDate() {
        Object[] objArr = {FieldName.DATE};
        DateTimeField dateField = (DateTimeField) ((Field) Message.class.getMethod("obtainField", String.class).invoke(this, objArr));
        if (dateField == null) {
            return null;
        }
        return (Date) DateTimeField.class.getMethod("getDate", new Class[0]).invoke(dateField, new Object[0]);
    }

    public void setDate(Date date) {
        Object[] objArr = {date, null};
        Message.class.getMethod("setDate", Date.class, TimeZone.class).invoke(this, objArr);
    }

    public void setDate(Date date, TimeZone zone) {
        Header header = (Header) Message.class.getMethod("obtainHeader", new Class[0]).invoke(this, new Object[0]);
        if (date == null) {
            Object[] objArr = {FieldName.DATE};
            ((Integer) Header.class.getMethod("removeFields", String.class).invoke(header, objArr)).intValue();
            return;
        }
        Object[] objArr2 = {FieldName.DATE, date, zone};
        Object[] objArr3 = {(DateTimeField) Fields.class.getMethod("date", String.class, Date.class, TimeZone.class).invoke(null, objArr2)};
        Header.class.getMethod("setField", Field.class).invoke(header, objArr3);
    }

    public Mailbox getSender() {
        Class[] clsArr = {String.class};
        return (Mailbox) Message.class.getMethod("getMailbox", clsArr).invoke(this, FieldName.SENDER);
    }

    public void setSender(Mailbox sender) {
        Object[] objArr = {FieldName.SENDER, sender};
        Message.class.getMethod("setMailbox", String.class, Mailbox.class).invoke(this, objArr);
    }

    public MailboxList getFrom() {
        Class[] clsArr = {String.class};
        return (MailboxList) Message.class.getMethod("getMailboxList", clsArr).invoke(this, FieldName.FROM);
    }

    public void setFrom(Mailbox from) {
        Object[] objArr = {FieldName.FROM, from};
        Message.class.getMethod("setMailboxList", String.class, Mailbox.class).invoke(this, objArr);
    }

    public void setFrom(Mailbox... from) {
        Object[] objArr = {FieldName.FROM, from};
        Message.class.getMethod("setMailboxList", String.class, Mailbox[].class).invoke(this, objArr);
    }

    public void setFrom(Collection<Mailbox> from) {
        Object[] objArr = {FieldName.FROM, from};
        Message.class.getMethod("setMailboxList", String.class, Collection.class).invoke(this, objArr);
    }

    public AddressList getTo() {
        Class[] clsArr = {String.class};
        return (AddressList) Message.class.getMethod("getAddressList", clsArr).invoke(this, FieldName.TO);
    }

    public void setTo(Address to) {
        Object[] objArr = {FieldName.TO, to};
        Message.class.getMethod("setAddressList", String.class, Address.class).invoke(this, objArr);
    }

    public void setTo(Address... to) {
        Object[] objArr = {FieldName.TO, to};
        Message.class.getMethod("setAddressList", String.class, Address[].class).invoke(this, objArr);
    }

    public void setTo(Collection<Address> to) {
        Object[] objArr = {FieldName.TO, to};
        Message.class.getMethod("setAddressList", String.class, Collection.class).invoke(this, objArr);
    }

    public AddressList getCc() {
        Class[] clsArr = {String.class};
        return (AddressList) Message.class.getMethod("getAddressList", clsArr).invoke(this, FieldName.CC);
    }

    public void setCc(Address cc) {
        Object[] objArr = {FieldName.CC, cc};
        Message.class.getMethod("setAddressList", String.class, Address.class).invoke(this, objArr);
    }

    public void setCc(Address... cc) {
        Object[] objArr = {FieldName.CC, cc};
        Message.class.getMethod("setAddressList", String.class, Address[].class).invoke(this, objArr);
    }

    public void setCc(Collection<Address> cc) {
        Object[] objArr = {FieldName.CC, cc};
        Message.class.getMethod("setAddressList", String.class, Collection.class).invoke(this, objArr);
    }

    public AddressList getBcc() {
        Class[] clsArr = {String.class};
        return (AddressList) Message.class.getMethod("getAddressList", clsArr).invoke(this, FieldName.BCC);
    }

    public void setBcc(Address bcc) {
        Object[] objArr = {FieldName.BCC, bcc};
        Message.class.getMethod("setAddressList", String.class, Address.class).invoke(this, objArr);
    }

    public void setBcc(Address... bcc) {
        Object[] objArr = {FieldName.BCC, bcc};
        Message.class.getMethod("setAddressList", String.class, Address[].class).invoke(this, objArr);
    }

    public void setBcc(Collection<Address> bcc) {
        Object[] objArr = {FieldName.BCC, bcc};
        Message.class.getMethod("setAddressList", String.class, Collection.class).invoke(this, objArr);
    }

    public AddressList getReplyTo() {
        Class[] clsArr = {String.class};
        return (AddressList) Message.class.getMethod("getAddressList", clsArr).invoke(this, FieldName.REPLY_TO);
    }

    public void setReplyTo(Address replyTo) {
        Object[] objArr = {FieldName.REPLY_TO, replyTo};
        Message.class.getMethod("setAddressList", String.class, Address.class).invoke(this, objArr);
    }

    public void setReplyTo(Address... replyTo) {
        Object[] objArr = {FieldName.REPLY_TO, replyTo};
        Message.class.getMethod("setAddressList", String.class, Address[].class).invoke(this, objArr);
    }

    public void setReplyTo(Collection<Address> replyTo) {
        Object[] objArr = {FieldName.REPLY_TO, replyTo};
        Message.class.getMethod("setAddressList", String.class, Collection.class).invoke(this, objArr);
    }

    private Mailbox getMailbox(String fieldName) {
        Object[] objArr = {fieldName};
        MailboxField field = (MailboxField) ((Field) Message.class.getMethod("obtainField", String.class).invoke(this, objArr));
        if (field == null) {
            return null;
        }
        return (Mailbox) MailboxField.class.getMethod("getMailbox", new Class[0]).invoke(field, new Object[0]);
    }

    private void setMailbox(String fieldName, Mailbox mailbox) {
        Header header = (Header) Message.class.getMethod("obtainHeader", new Class[0]).invoke(this, new Object[0]);
        if (mailbox == null) {
            Object[] objArr = {fieldName};
            ((Integer) Header.class.getMethod("removeFields", String.class).invoke(header, objArr)).intValue();
            return;
        }
        Object[] objArr2 = {fieldName, mailbox};
        Object[] objArr3 = {(MailboxField) Fields.class.getMethod("mailbox", String.class, Mailbox.class).invoke(null, objArr2)};
        Header.class.getMethod("setField", Field.class).invoke(header, objArr3);
    }

    private MailboxList getMailboxList(String fieldName) {
        Object[] objArr = {fieldName};
        MailboxListField field = (MailboxListField) ((Field) Message.class.getMethod("obtainField", String.class).invoke(this, objArr));
        if (field == null) {
            return null;
        }
        return (MailboxList) MailboxListField.class.getMethod("getMailboxList", new Class[0]).invoke(field, new Object[0]);
    }

    private void setMailboxList(String fieldName, Mailbox mailbox) {
        Set set;
        if (mailbox == null) {
            set = null;
        } else {
            Object[] objArr = {mailbox};
            set = (Set) Collections.class.getMethod("singleton", Object.class).invoke(null, objArr);
        }
        Object[] objArr2 = {fieldName, set};
        Message.class.getMethod("setMailboxList", String.class, Collection.class).invoke(this, objArr2);
    }

    private void setMailboxList(String fieldName, Mailbox... mailboxes) {
        List list;
        if (mailboxes == null) {
            list = null;
        } else {
            Object[] objArr = {mailboxes};
            list = (List) Arrays.class.getMethod("asList", Object[].class).invoke(null, objArr);
        }
        Object[] objArr2 = {fieldName, list};
        Message.class.getMethod("setMailboxList", String.class, Collection.class).invoke(this, objArr2);
    }

    private void setMailboxList(String fieldName, Collection<Mailbox> mailboxes) {
        Header header = (Header) Message.class.getMethod("obtainHeader", new Class[0]).invoke(this, new Object[0]);
        if (mailboxes != null) {
            if (!((Boolean) Collection.class.getMethod("isEmpty", new Class[0]).invoke(mailboxes, new Object[0])).booleanValue()) {
                Object[] objArr = {fieldName, mailboxes};
                Object[] objArr2 = {(MailboxListField) Fields.class.getMethod("mailboxList", String.class, Iterable.class).invoke(null, objArr)};
                Header.class.getMethod("setField", Field.class).invoke(header, objArr2);
                return;
            }
        }
        Object[] objArr3 = {fieldName};
        ((Integer) Header.class.getMethod("removeFields", String.class).invoke(header, objArr3)).intValue();
    }

    private AddressList getAddressList(String fieldName) {
        Object[] objArr = {fieldName};
        AddressListField field = (AddressListField) ((Field) Message.class.getMethod("obtainField", String.class).invoke(this, objArr));
        if (field == null) {
            return null;
        }
        return (AddressList) AddressListField.class.getMethod("getAddressList", new Class[0]).invoke(field, new Object[0]);
    }

    private void setAddressList(String fieldName, Address address) {
        Set set;
        if (address == null) {
            set = null;
        } else {
            Object[] objArr = {address};
            set = (Set) Collections.class.getMethod("singleton", Object.class).invoke(null, objArr);
        }
        Object[] objArr2 = {fieldName, set};
        Message.class.getMethod("setAddressList", String.class, Collection.class).invoke(this, objArr2);
    }

    private void setAddressList(String fieldName, Address... addresses) {
        List list;
        if (addresses == null) {
            list = null;
        } else {
            Object[] objArr = {addresses};
            list = (List) Arrays.class.getMethod("asList", Object[].class).invoke(null, objArr);
        }
        Object[] objArr2 = {fieldName, list};
        Message.class.getMethod("setAddressList", String.class, Collection.class).invoke(this, objArr2);
    }

    private void setAddressList(String fieldName, Collection<Address> addresses) {
        Header header = (Header) Message.class.getMethod("obtainHeader", new Class[0]).invoke(this, new Object[0]);
        if (addresses != null) {
            if (!((Boolean) Collection.class.getMethod("isEmpty", new Class[0]).invoke(addresses, new Object[0])).booleanValue()) {
                Object[] objArr = {fieldName, addresses};
                Object[] objArr2 = {(AddressListField) Fields.class.getMethod("addressList", String.class, Iterable.class).invoke(null, objArr)};
                Header.class.getMethod("setField", Field.class).invoke(header, objArr2);
                return;
            }
        }
        Object[] objArr3 = {fieldName};
        ((Integer) Header.class.getMethod("removeFields", String.class).invoke(header, objArr3)).intValue();
    }
}
