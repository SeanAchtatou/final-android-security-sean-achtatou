package org.apache.james.mime4j.field.address.parser;

import java.lang.reflect.Method;
import java.util.Stack;

class JJTAddressListParserState {
    private Stack<Integer> marks = new Stack<>();
    private int mk = 0;
    private boolean node_created;
    private Stack<Node> nodes = new Stack<>();
    private int sp = 0;

    JJTAddressListParserState() {
    }

    /* access modifiers changed from: package-private */
    public boolean nodeCreated() {
        return this.node_created;
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        Stack.class.getMethod("removeAllElements", new Class[0]).invoke(this.nodes, new Object[0]);
        Stack.class.getMethod("removeAllElements", new Class[0]).invoke(this.marks, new Object[0]);
        this.sp = 0;
        this.mk = 0;
    }

    /* access modifiers changed from: package-private */
    public Node rootNode() {
        Stack<Node> stack = this.nodes;
        Class[] clsArr = {Integer.TYPE};
        return (Node) Stack.class.getMethod("elementAt", clsArr).invoke(stack, new Integer(0));
    }

    /* access modifiers changed from: package-private */
    public void pushNode(Node n) {
        Stack.class.getMethod("push", Object.class).invoke(this.nodes, n);
        this.sp++;
    }

    /* access modifiers changed from: package-private */
    public Node popNode() {
        int i = this.sp - 1;
        this.sp = i;
        if (i < this.mk) {
            this.mk = ((Integer) Integer.class.getMethod("intValue", new Class[0]).invoke((Integer) Stack.class.getMethod("pop", new Class[0]).invoke(this.marks, new Object[0]), new Object[0])).intValue();
        }
        return (Node) Stack.class.getMethod("pop", new Class[0]).invoke(this.nodes, new Object[0]);
    }

    /* access modifiers changed from: package-private */
    public Node peekNode() {
        return (Node) Stack.class.getMethod("peek", new Class[0]).invoke(this.nodes, new Object[0]);
    }

    /* access modifiers changed from: package-private */
    public int nodeArity() {
        return this.sp - this.mk;
    }

    /* access modifiers changed from: package-private */
    public void clearNodeScope(Node n) {
        while (this.sp > this.mk) {
            JJTAddressListParserState.class.getMethod("popNode", new Class[0]).invoke(this, new Object[0]);
        }
        Method method = Stack.class.getMethod("pop", new Class[0]);
        this.mk = ((Integer) Integer.class.getMethod("intValue", new Class[0]).invoke((Integer) method.invoke(this.marks, new Object[0]), new Object[0])).intValue();
    }

    /* access modifiers changed from: package-private */
    public void openNodeScope(Node n) {
        Stack<Integer> stack = this.marks;
        Object[] objArr = {new Integer(this.mk)};
        Stack.class.getMethod("push", Object.class).invoke(stack, objArr);
        this.mk = this.sp;
        Node.class.getMethod("jjtOpen", new Class[0]).invoke(n, new Object[0]);
    }

    /* access modifiers changed from: package-private */
    public void closeNodeScope(Node n, int num) {
        this.mk = ((Integer) Integer.class.getMethod("intValue", new Class[0]).invoke((Integer) Stack.class.getMethod("pop", new Class[0]).invoke(this.marks, new Object[0]), new Object[0])).intValue();
        while (true) {
            int num2 = num;
            num = num2 - 1;
            if (num2 > 0) {
                Node c = (Node) JJTAddressListParserState.class.getMethod("popNode", new Class[0]).invoke(this, new Object[0]);
                Node.class.getMethod("jjtSetParent", Node.class).invoke(c, n);
                Node.class.getMethod("jjtAddChild", Node.class, Integer.TYPE).invoke(n, c, new Integer(num));
            } else {
                Node.class.getMethod("jjtClose", new Class[0]).invoke(n, new Object[0]);
                JJTAddressListParserState.class.getMethod("pushNode", Node.class).invoke(this, n);
                this.node_created = true;
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void closeNodeScope(Node n, boolean condition) {
        if (condition) {
            int a = ((Integer) JJTAddressListParserState.class.getMethod("nodeArity", new Class[0]).invoke(this, new Object[0])).intValue();
            this.mk = ((Integer) Integer.class.getMethod("intValue", new Class[0]).invoke((Integer) Stack.class.getMethod("pop", new Class[0]).invoke(this.marks, new Object[0]), new Object[0])).intValue();
            while (true) {
                int a2 = a;
                a = a2 - 1;
                if (a2 > 0) {
                    Node c = (Node) JJTAddressListParserState.class.getMethod("popNode", new Class[0]).invoke(this, new Object[0]);
                    Node.class.getMethod("jjtSetParent", Node.class).invoke(c, n);
                    Node.class.getMethod("jjtAddChild", Node.class, Integer.TYPE).invoke(n, c, new Integer(a));
                } else {
                    Node.class.getMethod("jjtClose", new Class[0]).invoke(n, new Object[0]);
                    JJTAddressListParserState.class.getMethod("pushNode", Node.class).invoke(this, n);
                    this.node_created = true;
                    return;
                }
            }
        } else {
            this.mk = ((Integer) Integer.class.getMethod("intValue", new Class[0]).invoke((Integer) Stack.class.getMethod("pop", new Class[0]).invoke(this.marks, new Object[0]), new Object[0])).intValue();
            this.node_created = false;
        }
    }
}
