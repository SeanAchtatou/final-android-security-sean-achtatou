package org.apache.james.mime4j.storage;

import java.io.IOException;
import java.io.OutputStream;

public abstract class StorageOutputStream extends OutputStream {
    private boolean closed;
    private byte[] singleByte;
    private boolean usedUp;

    /* access modifiers changed from: protected */
    public abstract Storage toStorage0() throws IOException;

    /* access modifiers changed from: protected */
    public abstract void write0(byte[] bArr, int i, int i2) throws IOException;

    protected StorageOutputStream() {
    }

    public final Storage toStorage() throws IOException {
        if (this.usedUp) {
            throw new IllegalStateException("toStorage may be invoked only once");
        }
        if (!this.closed) {
            StorageOutputStream.class.getMethod("close", new Class[0]).invoke(this, new Object[0]);
        }
        this.usedUp = true;
        return (Storage) StorageOutputStream.class.getMethod("toStorage0", new Class[0]).invoke(this, new Object[0]);
    }

    public final void write(int b) throws IOException {
        if (this.closed) {
            throw new IOException("StorageOutputStream has been closed");
        }
        if (this.singleByte == null) {
            this.singleByte = new byte[1];
        }
        this.singleByte[0] = (byte) b;
        byte[] bArr = this.singleByte;
        Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
        StorageOutputStream.class.getMethod("write0", clsArr).invoke(this, bArr, new Integer(0), new Integer(1));
    }

    public final void write(byte[] buffer) throws IOException {
        if (this.closed) {
            throw new IOException("StorageOutputStream has been closed");
        } else if (buffer == null) {
            throw new NullPointerException();
        } else if (buffer.length != 0) {
            int length = buffer.length;
            Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
            StorageOutputStream.class.getMethod("write0", clsArr).invoke(this, buffer, new Integer(0), new Integer(length));
        }
    }

    public final void write(byte[] buffer, int offset, int length) throws IOException {
        if (this.closed) {
            throw new IOException("StorageOutputStream has been closed");
        } else if (buffer == null) {
            throw new NullPointerException();
        } else if (offset < 0 || length < 0 || offset + length > buffer.length) {
            throw new IndexOutOfBoundsException();
        } else if (length != 0) {
            Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
            StorageOutputStream.class.getMethod("write0", clsArr).invoke(this, buffer, new Integer(offset), new Integer(length));
        }
    }

    public void close() throws IOException {
        this.closed = true;
    }
}
