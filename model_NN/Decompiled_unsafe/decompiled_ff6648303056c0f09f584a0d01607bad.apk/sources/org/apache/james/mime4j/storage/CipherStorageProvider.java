package org.apache.james.mime4j.storage;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class CipherStorageProvider extends AbstractStorageProvider {
    private final String algorithm;
    private final StorageProvider backend;
    private final KeyGenerator keygen;

    public CipherStorageProvider(StorageProvider backend2) {
        this(backend2, "Blowfish");
    }

    public CipherStorageProvider(StorageProvider backend2, String algorithm2) {
        if (backend2 == null) {
            throw new IllegalArgumentException();
        }
        try {
            this.backend = backend2;
            this.algorithm = algorithm2;
            Object[] objArr = {algorithm2};
            this.keygen = (KeyGenerator) KeyGenerator.class.getMethod("getInstance", String.class).invoke(null, objArr);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public StorageOutputStream createStorageOutputStream() throws IOException {
        Method method = CipherStorageProvider.class.getMethod("getSecretKeySpec", new Class[0]);
        return new CipherStorageOutputStream((StorageOutputStream) StorageProvider.class.getMethod("createStorageOutputStream", new Class[0]).invoke(this.backend, new Object[0]), this.algorithm, (SecretKeySpec) method.invoke(this, new Object[0]));
    }

    private SecretKeySpec getSecretKeySpec() {
        Method method = KeyGenerator.class.getMethod("generateKey", new Class[0]);
        return new SecretKeySpec((byte[]) SecretKey.class.getMethod("getEncoded", new Class[0]).invoke((SecretKey) method.invoke(this.keygen, new Object[0]), new Object[0]), this.algorithm);
    }

    private static final class CipherStorageOutputStream extends StorageOutputStream {
        private final String algorithm;
        private final CipherOutputStream cipherOut;
        private final SecretKeySpec skeySpec;
        private final StorageOutputStream storageOut;

        public CipherStorageOutputStream(StorageOutputStream out, String algorithm2, SecretKeySpec skeySpec2) throws IOException {
            try {
                this.storageOut = out;
                this.algorithm = algorithm2;
                this.skeySpec = skeySpec2;
                Object[] objArr = {algorithm2};
                Cipher cipher = (Cipher) Cipher.class.getMethod("getInstance", String.class).invoke(null, objArr);
                Class[] clsArr = {Integer.TYPE, Key.class};
                Cipher.class.getMethod("init", clsArr).invoke(cipher, new Integer(1), skeySpec2);
                this.cipherOut = new CipherOutputStream(out, cipher);
            } catch (GeneralSecurityException e) {
                throw ((IOException) ((Throwable) IOException.class.getMethod("initCause", Throwable.class).invoke(new IOException(), e)));
            }
        }

        public void close() throws IOException {
            super.close();
            CipherOutputStream.class.getMethod("close", new Class[0]).invoke(this.cipherOut, new Object[0]);
        }

        /* access modifiers changed from: protected */
        public void write0(byte[] buffer, int offset, int length) throws IOException {
            CipherOutputStream cipherOutputStream = this.cipherOut;
            Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
            CipherOutputStream.class.getMethod("write", clsArr).invoke(cipherOutputStream, buffer, new Integer(offset), new Integer(length));
        }

        /* access modifiers changed from: protected */
        public Storage toStorage0() throws IOException {
            return new CipherStorage((Storage) StorageOutputStream.class.getMethod("toStorage", new Class[0]).invoke(this.storageOut, new Object[0]), this.algorithm, this.skeySpec);
        }
    }

    private static final class CipherStorage implements Storage {
        private final String algorithm;
        private Storage encrypted;
        private final SecretKeySpec skeySpec;

        public CipherStorage(Storage encrypted2, String algorithm2, SecretKeySpec skeySpec2) {
            this.encrypted = encrypted2;
            this.algorithm = algorithm2;
            this.skeySpec = skeySpec2;
        }

        public void delete() {
            if (this.encrypted != null) {
                Storage.class.getMethod("delete", new Class[0]).invoke(this.encrypted, new Object[0]);
                this.encrypted = null;
            }
        }

        public InputStream getInputStream() throws IOException {
            if (this.encrypted == null) {
                throw new IllegalStateException("storage has been deleted");
            }
            try {
                Object[] objArr = {this.algorithm};
                Cipher cipher = (Cipher) Cipher.class.getMethod("getInstance", String.class).invoke(null, objArr);
                SecretKeySpec secretKeySpec = this.skeySpec;
                Class[] clsArr = {Integer.TYPE, Key.class};
                Cipher.class.getMethod("init", clsArr).invoke(cipher, new Integer(2), secretKeySpec);
                return new CipherInputStream((InputStream) Storage.class.getMethod("getInputStream", new Class[0]).invoke(this.encrypted, new Object[0]), cipher);
            } catch (GeneralSecurityException e) {
                throw ((IOException) ((Throwable) IOException.class.getMethod("initCause", Throwable.class).invoke(new IOException(), e)));
            }
        }
    }
}
