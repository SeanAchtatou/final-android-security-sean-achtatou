package org.apache.james.mime4j.storage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.james.mime4j.codec.CodecUtil;

public abstract class AbstractStorageProvider implements StorageProvider {
    protected AbstractStorageProvider() {
    }

    public final Storage store(InputStream in) throws IOException {
        StorageOutputStream out = (StorageOutputStream) AbstractStorageProvider.class.getMethod("createStorageOutputStream", new Class[0]).invoke(this, new Object[0]);
        Object[] objArr = {in, out};
        CodecUtil.class.getMethod("copy", InputStream.class, OutputStream.class).invoke(null, objArr);
        return (Storage) StorageOutputStream.class.getMethod("toStorage", new Class[0]).invoke(out, new Object[0]);
    }
}
