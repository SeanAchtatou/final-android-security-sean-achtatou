package org.apache.james.mime4j.util;

public final class ByteArrayBuffer implements ByteSequence {
    private byte[] buffer;
    private int len;

    public ByteArrayBuffer(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException("Buffer capacity may not be negative");
        }
        this.buffer = new byte[capacity];
    }

    public ByteArrayBuffer(byte[] bytes, boolean dontCopy) {
        this(bytes, bytes.length, dontCopy);
    }

    public ByteArrayBuffer(byte[] bytes, int len2, boolean dontCopy) {
        if (bytes == null) {
            throw new IllegalArgumentException();
        } else if (len2 < 0 || len2 > bytes.length) {
            throw new IllegalArgumentException();
        } else {
            if (dontCopy) {
                this.buffer = bytes;
            } else {
                this.buffer = new byte[len2];
                byte[] bArr = this.buffer;
                Class[] clsArr = {Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE};
                System.class.getMethod("arraycopy", clsArr).invoke(null, bytes, new Integer(0), bArr, new Integer(0), new Integer(len2));
            }
            this.len = len2;
        }
    }

    private void expand(int newlen) {
        Class[] clsArr = {Integer.TYPE, Integer.TYPE};
        byte[] newbuffer = new byte[((Integer) Math.class.getMethod("max", clsArr).invoke(null, new Integer(this.buffer.length << 1), new Integer(newlen))).intValue()];
        byte[] bArr = this.buffer;
        int i = this.len;
        Class[] clsArr2 = {Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE};
        System.class.getMethod("arraycopy", clsArr2).invoke(null, bArr, new Integer(0), newbuffer, new Integer(0), new Integer(i));
        this.buffer = newbuffer;
    }

    public void append(byte[] b, int off, int len2) {
        if (b != null) {
            if (off < 0 || off > b.length || len2 < 0 || off + len2 < 0 || off + len2 > b.length) {
                throw new IndexOutOfBoundsException();
            } else if (len2 != 0) {
                int newlen = this.len + len2;
                if (newlen > this.buffer.length) {
                    Class[] clsArr = {Integer.TYPE};
                    ByteArrayBuffer.class.getMethod("expand", clsArr).invoke(this, new Integer(newlen));
                }
                byte[] bArr = this.buffer;
                int i = this.len;
                Class[] clsArr2 = {Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE};
                System.class.getMethod("arraycopy", clsArr2).invoke(null, b, new Integer(off), bArr, new Integer(i), new Integer(len2));
                this.len = newlen;
            }
        }
    }

    public void append(int b) {
        int newlen = this.len + 1;
        if (newlen > this.buffer.length) {
            Class[] clsArr = {Integer.TYPE};
            ByteArrayBuffer.class.getMethod("expand", clsArr).invoke(this, new Integer(newlen));
        }
        this.buffer[this.len] = (byte) b;
        this.len = newlen;
    }

    public void clear() {
        this.len = 0;
    }

    public byte[] toByteArray() {
        byte[] b = new byte[this.len];
        if (this.len > 0) {
            byte[] bArr = this.buffer;
            int i = this.len;
            Class[] clsArr = {Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE};
            System.class.getMethod("arraycopy", clsArr).invoke(null, bArr, new Integer(0), b, new Integer(0), new Integer(i));
        }
        return b;
    }

    public byte byteAt(int i) {
        if (i >= 0 && i < this.len) {
            return this.buffer[i];
        }
        throw new IndexOutOfBoundsException();
    }

    public int capacity() {
        return this.buffer.length;
    }

    public int length() {
        return this.len;
    }

    public byte[] buffer() {
        return this.buffer;
    }

    public int indexOf(byte b) {
        int i = this.len;
        Class[] clsArr = {Byte.TYPE, Integer.TYPE, Integer.TYPE};
        return ((Integer) ByteArrayBuffer.class.getMethod("indexOf", clsArr).invoke(this, new Byte(b), new Integer(0), new Integer(i))).intValue();
    }

    public int indexOf(byte b, int beginIndex, int endIndex) {
        if (beginIndex < 0) {
            beginIndex = 0;
        }
        if (endIndex > this.len) {
            endIndex = this.len;
        }
        if (beginIndex > endIndex) {
            return -1;
        }
        for (int i = beginIndex; i < endIndex; i++) {
            if (this.buffer[i] == b) {
                return i;
            }
        }
        return -1;
    }

    public void setLength(int len2) {
        if (len2 < 0 || len2 > this.buffer.length) {
            throw new IndexOutOfBoundsException();
        }
        this.len = len2;
    }

    public boolean isEmpty() {
        return this.len == 0;
    }

    public boolean isFull() {
        return this.len == this.buffer.length;
    }

    public String toString() {
        return new String((byte[]) ByteArrayBuffer.class.getMethod("toByteArray", new Class[0]).invoke(this, new Object[0]));
    }
}
