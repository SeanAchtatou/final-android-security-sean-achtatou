package org.apache.james.mime4j.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class PositionInputStream extends FilterInputStream {
    private long markedPosition = 0;
    protected long position = 0;

    public PositionInputStream(InputStream inputStream) {
        super(inputStream);
    }

    public long getPosition() {
        return this.position;
    }

    public int available() throws IOException {
        return ((Integer) InputStream.class.getMethod("available", new Class[0]).invoke(this.in, new Object[0])).intValue();
    }

    public int read() throws IOException {
        int b = ((Integer) InputStream.class.getMethod("read", new Class[0]).invoke(this.in, new Object[0])).intValue();
        if (b != -1) {
            this.position++;
        }
        return b;
    }

    public void close() throws IOException {
        InputStream.class.getMethod("close", new Class[0]).invoke(this.in, new Object[0]);
    }

    public void reset() throws IOException {
        InputStream.class.getMethod("reset", new Class[0]).invoke(this.in, new Object[0]);
        this.position = this.markedPosition;
    }

    public boolean markSupported() {
        return ((Boolean) InputStream.class.getMethod("markSupported", new Class[0]).invoke(this.in, new Object[0])).booleanValue();
    }

    public void mark(int readlimit) {
        InputStream inputStream = this.in;
        Class[] clsArr = {Integer.TYPE};
        InputStream.class.getMethod("mark", clsArr).invoke(inputStream, new Integer(readlimit));
        this.markedPosition = this.position;
    }

    public long skip(long n) throws IOException {
        long c = ((Long) InputStream.class.getMethod("skip", Long.TYPE).invoke(this.in, new Long(n))).longValue();
        if (c > 0) {
            this.position += c;
        }
        return c;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        int c = ((Integer) InputStream.class.getMethod("read", byte[].class, Integer.TYPE, Integer.TYPE).invoke(this.in, b, new Integer(off), new Integer(len))).intValue();
        if (c > 0) {
            this.position += (long) c;
        }
        return c;
    }
}
