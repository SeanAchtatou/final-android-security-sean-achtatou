package org.apache.james.mime4j.storage;

import java.lang.reflect.Method;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DefaultStorageProvider {
    public static final String DEFAULT_STORAGE_PROVIDER_PROPERTY = "org.apache.james.mime4j.defaultStorageProvider";
    private static volatile StorageProvider instance = null;
    private static Log log;

    static {
        Object[] objArr = {DefaultStorageProvider.class};
        log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
        DefaultStorageProvider.class.getMethod("initialize", new Class[0]).invoke(null, new Object[0]);
    }

    private DefaultStorageProvider() {
    }

    public static StorageProvider getInstance() {
        return instance;
    }

    public static void setInstance(StorageProvider instance2) {
        if (instance2 == null) {
            throw new IllegalArgumentException();
        }
        instance = instance2;
    }

    private static void initialize() {
        Object[] objArr = {DEFAULT_STORAGE_PROVIDER_PROPERTY};
        String clazz = (String) System.class.getMethod("getProperty", String.class).invoke(null, objArr);
        if (clazz != null) {
            try {
                instance = (StorageProvider) Class.forName(clazz).newInstance();
            } catch (Exception e) {
                Log log2 = log;
                Object[] objArr2 = {"Unable to create or instantiate StorageProvider class '"};
                Object[] objArr3 = {clazz};
                Method method = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr4 = {"'. Using default instead."};
                Method method2 = StringBuilder.class.getMethod("append", String.class);
                Method method3 = StringBuilder.class.getMethod("toString", new Class[0]);
                Object[] objArr5 = {(String) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr2), objArr3), objArr4), new Object[0]), e};
                Log.class.getMethod("warn", Object.class, Throwable.class).invoke(log2, objArr5);
            }
        }
        if (instance == null) {
            instance = new ThresholdStorageProvider(new TempFileStorageProvider(), 1024);
        }
    }

    static void reset() {
        instance = null;
        DefaultStorageProvider.class.getMethod("initialize", new Class[0]).invoke(null, new Object[0]);
    }
}
