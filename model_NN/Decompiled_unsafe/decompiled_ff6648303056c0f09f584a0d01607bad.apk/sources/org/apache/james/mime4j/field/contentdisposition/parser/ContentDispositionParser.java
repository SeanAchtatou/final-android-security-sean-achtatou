package org.apache.james.mime4j.field.contentdisposition.parser;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ContentDispositionParser implements ContentDispositionParserConstants {
    private static int[] jj_la1_0;
    private String dispositionType;
    private Vector<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private final int[] jj_la1;
    public Token jj_nt;
    private int jj_ntk;
    private List<String> paramNames;
    private List<String> paramValues;
    public Token token;
    public ContentDispositionParserTokenManager token_source;

    public String getDispositionType() {
        return this.dispositionType;
    }

    public List<String> getParamNames() {
        return this.paramNames;
    }

    public List<String> getParamValues() {
        return this.paramValues;
    }

    public static void main(String[] args) throws ParseException {
        while (true) {
            try {
                ContentDispositionParser.class.getMethod("parseLine", new Class[0]).invoke(new ContentDispositionParser(System.in), new Object[0]);
            } catch (Exception x) {
                Exception.class.getMethod("printStackTrace", new Class[0]).invoke(x, new Object[0]);
                return;
            }
        }
    }

    public final void parseLine() throws ParseException {
        int i;
        ContentDispositionParser.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        if (this.jj_ntk == -1) {
            i = ((Integer) ContentDispositionParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 1:
                Class[] clsArr = {Integer.TYPE};
                ContentDispositionParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(1));
                break;
            default:
                this.jj_la1[0] = this.jj_gen;
                break;
        }
        Class[] clsArr2 = {Integer.TYPE};
        ContentDispositionParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(2));
    }

    public final void parseAll() throws ParseException {
        ContentDispositionParser.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        Class[] clsArr = {Integer.TYPE};
        ContentDispositionParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(0));
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x004f A[FALL_THROUGH, LOOP:0: B:1:0x0025->B:8:0x004f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0044 A[SYNTHETIC] */
    public final void parse() throws org.apache.james.mime4j.field.contentdisposition.parser.ParseException {
        /*
            r8 = this;
            r1 = 20
            r4 = 1
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            r4 = 0
            java.lang.Class r7 = java.lang.Integer.TYPE
            r5[r4] = r7
            java.lang.Integer r7 = new java.lang.Integer
            r7.<init>(r1)
            r6[r4] = r7
            java.lang.String r4 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser> r7 = org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            java.lang.Object r0 = r4.invoke(r8, r6)
            org.apache.james.mime4j.field.contentdisposition.parser.Token r0 = (org.apache.james.mime4j.field.contentdisposition.parser.Token) r0
            java.lang.String r1 = r0.image
            r8.dispositionType = r1
        L_0x0025:
            int r1 = r8.jj_ntk
            r2 = -1
            if (r1 != r2) goto L_0x004c
            r4 = 0
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            java.lang.String r4 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser> r7 = org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            java.lang.Object r4 = r4.invoke(r8, r6)
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r1 = r4.intValue()
        L_0x0041:
            switch(r1) {
                case 3: goto L_0x004f;
                default: goto L_0x0044;
            }
        L_0x0044:
            int[] r1 = r8.jj_la1
            r2 = 1
            int r3 = r8.jj_gen
            r1[r2] = r3
            return
        L_0x004c:
            int r1 = r8.jj_ntk
            goto L_0x0041
        L_0x004f:
            r1 = 3
            r4 = 1
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            r4 = 0
            java.lang.Class r7 = java.lang.Integer.TYPE
            r5[r4] = r7
            java.lang.Integer r7 = new java.lang.Integer
            r7.<init>(r1)
            r6[r4] = r7
            java.lang.String r4 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser> r7 = org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            r4.invoke(r8, r6)
            r4 = 0
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            java.lang.String r4 = "parameter"
            java.lang.Class<org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser> r7 = org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            r4.invoke(r8, r6)
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser.parse():void");
    }

    public final void parameter() throws ParseException {
        Class[] clsArr = {Integer.TYPE};
        Object[] objArr = {new Integer(20)};
        Class[] clsArr2 = {Integer.TYPE};
        ContentDispositionParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(4));
        Method method = ContentDispositionParser.class.getMethod("value", new Class[0]);
        List<String> list = this.paramNames;
        String str = ((Token) ContentDispositionParser.class.getMethod("jj_consume_token", clsArr).invoke(this, objArr)).image;
        Object[] objArr2 = {str};
        ((Boolean) List.class.getMethod("add", Object.class).invoke(list, objArr2)).booleanValue();
        Object[] objArr3 = {(String) method.invoke(this, new Object[0])};
        ((Boolean) List.class.getMethod("add", Object.class).invoke(this.paramValues, objArr3)).booleanValue();
    }

    public final String value() throws ParseException {
        int i;
        Token t;
        if (this.jj_ntk == -1) {
            i = ((Integer) ContentDispositionParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 18:
                Class[] clsArr = {Integer.TYPE};
                t = (Token) ContentDispositionParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(18));
                break;
            case 19:
                Class[] clsArr2 = {Integer.TYPE};
                t = (Token) ContentDispositionParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(19));
                break;
            case 20:
                Class[] clsArr3 = {Integer.TYPE};
                t = (Token) ContentDispositionParser.class.getMethod("jj_consume_token", clsArr3).invoke(this, new Integer(20));
                break;
            default:
                this.jj_la1[2] = this.jj_gen;
                Class[] clsArr4 = {Integer.TYPE};
                ContentDispositionParser.class.getMethod("jj_consume_token", clsArr4).invoke(this, new Integer(-1));
                throw new ParseException();
        }
        return t.image;
    }

    static {
        ContentDispositionParser.class.getMethod("jj_la1_0", new Class[0]).invoke(null, new Object[0]);
    }

    private static void jj_la1_0() {
        jj_la1_0 = new int[]{2, 8, 1835008};
    }

    public ContentDispositionParser(InputStream stream) {
        this(stream, null);
    }

    public ContentDispositionParser(InputStream stream, String encoding) {
        this.paramNames = new ArrayList();
        this.paramValues = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
            this.token_source = new ContentDispositionParserTokenManager(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 3; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void ReInit(InputStream stream) {
        Object[] objArr = {stream, null};
        ContentDispositionParser.class.getMethod("ReInit", InputStream.class, String.class).invoke(this, objArr);
    }

    public void ReInit(InputStream stream, String encoding) {
        try {
            SimpleCharStream simpleCharStream = this.jj_input_stream;
            Class[] clsArr = {InputStream.class, String.class, Integer.TYPE, Integer.TYPE};
            SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, encoding, new Integer(1), new Integer(1));
            ContentDispositionParserTokenManager contentDispositionParserTokenManager = this.token_source;
            Object[] objArr = {this.jj_input_stream};
            ContentDispositionParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(contentDispositionParserTokenManager, objArr);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 3; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public ContentDispositionParser(Reader stream) {
        this.paramNames = new ArrayList();
        this.paramValues = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new ContentDispositionParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(Reader stream) {
        SimpleCharStream simpleCharStream = this.jj_input_stream;
        Class[] clsArr = {Reader.class, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, new Integer(1), new Integer(1));
        ContentDispositionParserTokenManager contentDispositionParserTokenManager = this.token_source;
        Object[] objArr = {this.jj_input_stream};
        ContentDispositionParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(contentDispositionParserTokenManager, objArr);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public ContentDispositionParser(ContentDispositionParserTokenManager tm) {
        this.paramNames = new ArrayList();
        this.paramValues = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(ContentDispositionParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private final Token jj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        if (oldToken.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) ContentDispositionParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            this.jj_gen++;
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw ((ParseException) ContentDispositionParser.class.getMethod("generateParseException", new Class[0]).invoke(this, new Object[0]));
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) ContentDispositionParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    public final Token getToken(int index) {
        Token t;
        int i = 0;
        Token t2 = this.token;
        while (i < index) {
            if (t2.next != null) {
                t = t2.next;
            } else {
                t = (Token) ContentDispositionParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
                t2.next = t;
            }
            i++;
            t2 = t;
        }
        return t2;
    }

    private final int jj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token token4 = (Token) ContentDispositionParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token3.next = token4;
            int i = token4.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    public ParseException generateParseException() {
        Vector.class.getMethod("removeAllElements", new Class[0]).invoke(this.jj_expentries, new Object[0]);
        boolean[] la1tokens = new boolean[23];
        for (int i = 0; i < 23; i++) {
            la1tokens[i] = false;
        }
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i2 = 0; i2 < 3; i2++) {
            if (this.jj_la1[i2] == this.jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i2] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                }
            }
        }
        for (int i3 = 0; i3 < 23; i3++) {
            if (la1tokens[i3]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i3;
                Vector<int[]> vector = this.jj_expentries;
                Object[] objArr = {this.jj_expentry};
                Vector.class.getMethod("addElement", Object.class).invoke(vector, objArr);
            }
        }
        int[][] exptokseq = new int[((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()][];
        int i4 = 0;
        while (true) {
            if (i4 >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()) {
                return new ParseException(this.token, exptokseq, tokenImage);
            }
            Vector<int[]> vector2 = this.jj_expentries;
            Class[] clsArr = {Integer.TYPE};
            exptokseq[i4] = (int[]) Vector.class.getMethod("elementAt", clsArr).invoke(vector2, new Integer(i4));
            i4++;
        }
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }
}
