package org.apache.james.mime4j.util;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public class ContentUtil {
    private ContentUtil() {
    }

    public static ByteSequence encode(String string) {
        Class[] clsArr = {Charset.class, String.class};
        return (ByteSequence) ContentUtil.class.getMethod("encode", clsArr).invoke(null, CharsetUtil.US_ASCII, string);
    }

    public static ByteSequence encode(Charset charset, String string) {
        Object[] objArr = {string};
        Object[] objArr2 = {(CharBuffer) CharBuffer.class.getMethod("wrap", CharSequence.class).invoke(null, objArr)};
        ByteBuffer encoded = (ByteBuffer) Charset.class.getMethod("encode", CharBuffer.class).invoke(charset, objArr2);
        ByteArrayBuffer bab = new ByteArrayBuffer(((Integer) ByteBuffer.class.getMethod("remaining", new Class[0]).invoke(encoded, new Object[0])).intValue());
        Method method = ByteBuffer.class.getMethod("array", new Class[0]);
        int intValue = ((Integer) ByteBuffer.class.getMethod("position", new Class[0]).invoke(encoded, new Object[0])).intValue();
        int intValue2 = ((Integer) ByteBuffer.class.getMethod("remaining", new Class[0]).invoke(encoded, new Object[0])).intValue();
        Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
        ByteArrayBuffer.class.getMethod("append", clsArr).invoke(bab, (byte[]) method.invoke(encoded, new Object[0]), new Integer(intValue), new Integer(intValue2));
        return bab;
    }

    public static String decode(ByteSequence byteSequence) {
        Charset charset = CharsetUtil.US_ASCII;
        int intValue = ((Integer) ByteSequence.class.getMethod("length", new Class[0]).invoke(byteSequence, new Object[0])).intValue();
        Class[] clsArr = {Charset.class, ByteSequence.class, Integer.TYPE, Integer.TYPE};
        return (String) ContentUtil.class.getMethod("decode", clsArr).invoke(null, charset, byteSequence, new Integer(0), new Integer(intValue));
    }

    public static String decode(Charset charset, ByteSequence byteSequence) {
        int intValue = ((Integer) ByteSequence.class.getMethod("length", new Class[0]).invoke(byteSequence, new Object[0])).intValue();
        Class[] clsArr = {Charset.class, ByteSequence.class, Integer.TYPE, Integer.TYPE};
        return (String) ContentUtil.class.getMethod("decode", clsArr).invoke(null, charset, byteSequence, new Integer(0), new Integer(intValue));
    }

    public static String decode(ByteSequence byteSequence, int offset, int length) {
        Charset charset = CharsetUtil.US_ASCII;
        Class[] clsArr = {Charset.class, ByteSequence.class, Integer.TYPE, Integer.TYPE};
        return (String) ContentUtil.class.getMethod("decode", clsArr).invoke(null, charset, byteSequence, new Integer(offset), new Integer(length));
    }

    public static String decode(Charset charset, ByteSequence byteSequence, int offset, int length) {
        if (byteSequence instanceof ByteArrayBuffer) {
            Method method = ByteArrayBuffer.class.getMethod("buffer", new Class[0]);
            Class[] clsArr = {Charset.class, byte[].class, Integer.TYPE, Integer.TYPE};
            return (String) ContentUtil.class.getMethod("decode", clsArr).invoke(null, charset, (byte[]) method.invoke((ByteArrayBuffer) byteSequence, new Object[0]), new Integer(offset), new Integer(length));
        }
        Method method2 = ByteSequence.class.getMethod("toByteArray", new Class[0]);
        Class[] clsArr2 = {Charset.class, byte[].class, Integer.TYPE, Integer.TYPE};
        return (String) ContentUtil.class.getMethod("decode", clsArr2).invoke(null, charset, (byte[]) method2.invoke(byteSequence, new Object[0]), new Integer(offset), new Integer(length));
    }

    private static String decode(Charset charset, byte[] buffer, int offset, int length) {
        Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
        Class[] clsArr2 = {ByteBuffer.class};
        Object[] objArr = {(ByteBuffer) ByteBuffer.class.getMethod("wrap", clsArr).invoke(null, buffer, new Integer(offset), new Integer(length))};
        return (String) CharBuffer.class.getMethod("toString", new Class[0]).invoke((CharBuffer) Charset.class.getMethod("decode", clsArr2).invoke(charset, objArr), new Object[0]);
    }
}
