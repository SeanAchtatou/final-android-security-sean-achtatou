package org.apache.james.mime4j.storage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TempFileStorageProvider extends AbstractStorageProvider {
    private static final String DEFAULT_PREFIX = "m4j";
    private final File directory;
    private final String prefix;
    private final String suffix;

    public TempFileStorageProvider() {
        this(DEFAULT_PREFIX, null, null);
    }

    public TempFileStorageProvider(File directory2) {
        this(DEFAULT_PREFIX, null, directory2);
    }

    public TempFileStorageProvider(String prefix2, String suffix2, File directory2) {
        if (prefix2 != null) {
            if (((Integer) String.class.getMethod("length", new Class[0]).invoke(prefix2, new Object[0])).intValue() >= 3) {
                if (directory2 != null) {
                    if (!((Boolean) File.class.getMethod("isDirectory", new Class[0]).invoke(directory2, new Object[0])).booleanValue()) {
                        if (!((Boolean) File.class.getMethod("mkdirs", new Class[0]).invoke(directory2, new Object[0])).booleanValue()) {
                            throw new IllegalArgumentException("invalid directory");
                        }
                    }
                }
                this.prefix = prefix2;
                this.suffix = suffix2;
                this.directory = directory2;
                return;
            }
        }
        throw new IllegalArgumentException("invalid prefix");
    }

    public StorageOutputStream createStorageOutputStream() throws IOException {
        Object[] objArr = {this.prefix, this.suffix, this.directory};
        File file = (File) File.class.getMethod("createTempFile", String.class, String.class, File.class).invoke(null, objArr);
        File.class.getMethod("deleteOnExit", new Class[0]).invoke(file, new Object[0]);
        return new TempFileStorageOutputStream(file);
    }

    private static final class TempFileStorageOutputStream extends StorageOutputStream {
        private File file;
        private OutputStream out;

        public TempFileStorageOutputStream(File file2) throws IOException {
            this.file = file2;
            this.out = new FileOutputStream(file2);
        }

        public void close() throws IOException {
            super.close();
            OutputStream.class.getMethod("close", new Class[0]).invoke(this.out, new Object[0]);
        }

        /* access modifiers changed from: protected */
        public void write0(byte[] buffer, int offset, int length) throws IOException {
            OutputStream outputStream = this.out;
            Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
            OutputStream.class.getMethod("write", clsArr).invoke(outputStream, buffer, new Integer(offset), new Integer(length));
        }

        /* access modifiers changed from: protected */
        public Storage toStorage0() throws IOException {
            return new TempFileStorage(this.file);
        }
    }

    private static final class TempFileStorage implements Storage {
        private static final Set<File> filesToDelete = new HashSet();
        private File file;

        public TempFileStorage(File file2) {
            this.file = file2;
        }

        public void delete() {
            synchronized (filesToDelete) {
                if (this.file != null) {
                    Set<File> set = filesToDelete;
                    Object[] objArr = {this.file};
                    ((Boolean) Set.class.getMethod("add", Object.class).invoke(set, objArr)).booleanValue();
                    this.file = null;
                }
                Iterator<File> iterator = (Iterator) Set.class.getMethod("iterator", new Class[0]).invoke(filesToDelete, new Object[0]);
                while (true) {
                    if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(iterator, new Object[0])).booleanValue()) {
                        Method method = Iterator.class.getMethod("next", new Class[0]);
                        if (((Boolean) File.class.getMethod("delete", new Class[0]).invoke((File) method.invoke(iterator, new Object[0]), new Object[0])).booleanValue()) {
                            Iterator.class.getMethod("remove", new Class[0]).invoke(iterator, new Object[0]);
                        }
                    }
                }
            }
        }

        public InputStream getInputStream() throws IOException {
            if (this.file != null) {
                return new BufferedInputStream(new FileInputStream(this.file));
            }
            throw new IllegalStateException("storage has been deleted");
        }
    }
}
