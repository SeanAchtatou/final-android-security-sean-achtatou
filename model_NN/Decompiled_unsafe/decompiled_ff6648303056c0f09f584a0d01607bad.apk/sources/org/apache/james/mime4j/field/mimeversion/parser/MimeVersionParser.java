package org.apache.james.mime4j.field.mimeversion.parser;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

public class MimeVersionParser implements MimeVersionParserConstants {
    public static final int INITIAL_VERSION_VALUE = -1;
    private static int[] jj_la1_0;
    private Vector<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private final int[] jj_la1;
    public Token jj_nt;
    private int jj_ntk;
    private int major;
    private int minor;
    public Token token;
    public MimeVersionParserTokenManager token_source;

    public int getMinorVersion() {
        return this.minor;
    }

    public int getMajorVersion() {
        return this.major;
    }

    public final void parseLine() throws ParseException {
        int i;
        MimeVersionParser.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        if (this.jj_ntk == -1) {
            i = ((Integer) MimeVersionParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 1:
                Class[] clsArr = {Integer.TYPE};
                MimeVersionParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(1));
                break;
            default:
                this.jj_la1[0] = this.jj_gen;
                break;
        }
        Class[] clsArr2 = {Integer.TYPE};
        MimeVersionParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(2));
    }

    public final void parseAll() throws ParseException {
        MimeVersionParser.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        Class[] clsArr = {Integer.TYPE};
        MimeVersionParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(0));
    }

    public final void parse() throws ParseException {
        Class[] clsArr = {Integer.TYPE};
        Token major2 = (Token) MimeVersionParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(17));
        Class[] clsArr2 = {Integer.TYPE};
        MimeVersionParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(18));
        Class[] clsArr3 = {Integer.TYPE};
        Token minor2 = (Token) MimeVersionParser.class.getMethod("jj_consume_token", clsArr3).invoke(this, new Integer(17));
        try {
            Object[] objArr = {major2.image};
            this.major = ((Integer) Integer.class.getMethod("parseInt", String.class).invoke(null, objArr)).intValue();
            Object[] objArr2 = {minor2.image};
            this.minor = ((Integer) Integer.class.getMethod("parseInt", String.class).invoke(null, objArr2)).intValue();
        } catch (NumberFormatException e) {
            throw new ParseException((String) NumberFormatException.class.getMethod("getMessage", new Class[0]).invoke(e, new Object[0]));
        }
    }

    static {
        MimeVersionParser.class.getMethod("jj_la1_0", new Class[0]).invoke(null, new Object[0]);
    }

    private static void jj_la1_0() {
        jj_la1_0 = new int[]{2};
    }

    public MimeVersionParser(InputStream stream) {
        this(stream, null);
    }

    public MimeVersionParser(InputStream stream, String encoding) {
        this.major = -1;
        this.minor = -1;
        this.jj_la1 = new int[1];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
            this.token_source = new MimeVersionParserTokenManager(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 1; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void ReInit(InputStream stream) {
        Object[] objArr = {stream, null};
        MimeVersionParser.class.getMethod("ReInit", InputStream.class, String.class).invoke(this, objArr);
    }

    public void ReInit(InputStream stream, String encoding) {
        try {
            SimpleCharStream simpleCharStream = this.jj_input_stream;
            Class[] clsArr = {InputStream.class, String.class, Integer.TYPE, Integer.TYPE};
            SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, encoding, new Integer(1), new Integer(1));
            MimeVersionParserTokenManager mimeVersionParserTokenManager = this.token_source;
            Object[] objArr = {this.jj_input_stream};
            MimeVersionParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(mimeVersionParserTokenManager, objArr);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 1; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public MimeVersionParser(Reader stream) {
        this.major = -1;
        this.minor = -1;
        this.jj_la1 = new int[1];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new MimeVersionParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 1; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(Reader stream) {
        SimpleCharStream simpleCharStream = this.jj_input_stream;
        Class[] clsArr = {Reader.class, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, new Integer(1), new Integer(1));
        MimeVersionParserTokenManager mimeVersionParserTokenManager = this.token_source;
        Object[] objArr = {this.jj_input_stream};
        MimeVersionParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(mimeVersionParserTokenManager, objArr);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 1; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public MimeVersionParser(MimeVersionParserTokenManager tm) {
        this.major = -1;
        this.minor = -1;
        this.jj_la1 = new int[1];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 1; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(MimeVersionParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 1; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private final Token jj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        if (oldToken.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) MimeVersionParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            this.jj_gen++;
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw ((ParseException) MimeVersionParser.class.getMethod("generateParseException", new Class[0]).invoke(this, new Object[0]));
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) MimeVersionParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    public final Token getToken(int index) {
        Token t;
        int i = 0;
        Token t2 = this.token;
        while (i < index) {
            if (t2.next != null) {
                t = t2.next;
            } else {
                t = (Token) MimeVersionParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
                t2.next = t;
            }
            i++;
            t2 = t;
        }
        return t2;
    }

    private final int jj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token token4 = (Token) MimeVersionParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token3.next = token4;
            int i = token4.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    public ParseException generateParseException() {
        Vector.class.getMethod("removeAllElements", new Class[0]).invoke(this.jj_expentries, new Object[0]);
        boolean[] la1tokens = new boolean[21];
        for (int i = 0; i < 21; i++) {
            la1tokens[i] = false;
        }
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i2 = 0; i2 < 1; i2++) {
            if (this.jj_la1[i2] == this.jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i2] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                }
            }
        }
        for (int i3 = 0; i3 < 21; i3++) {
            if (la1tokens[i3]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i3;
                Vector<int[]> vector = this.jj_expentries;
                Object[] objArr = {this.jj_expentry};
                Vector.class.getMethod("addElement", Object.class).invoke(vector, objArr);
            }
        }
        int[][] exptokseq = new int[((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()][];
        int i4 = 0;
        while (true) {
            if (i4 >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()) {
                return new ParseException(this.token, exptokseq, tokenImage);
            }
            Vector<int[]> vector2 = this.jj_expentries;
            Class[] clsArr = {Integer.TYPE};
            exptokseq[i4] = (int[]) Vector.class.getMethod("elementAt", clsArr).invoke(vector2, new Integer(i4));
            i4++;
        }
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }
}
