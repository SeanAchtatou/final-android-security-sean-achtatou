package com.Security.Update;

public class MyList {
    public int count = 0;
    public item head = null;
    public item last = null;

    public void Add(Object o) {
        item t = new item();
        t.object = o;
        if (this.head == null) {
            this.head = t;
            this.last = t;
            this.count++;
            return;
        }
        this.last.next = t;
        this.last = t;
    }

    public item FindObject(Object o) {
        if (this.count == 0) {
            return null;
        }
        for (item h = this.head; h != null; h = h.next) {
            if (h.object == o) {
                return h;
            }
        }
        return null;
    }

    public void Delete(Object o) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        if (this.count != 0) {
            item pr = null;
            for (item h = this.head; h != null; h = h.next) {
                if (h.object == o) {
                    if (pr == null) {
                        this.head = h.next;
                    }
                    if (h.next == null) {
                        this.last = pr;
                    }
                    if (h.next == null) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if ((pr != null) && z) {
                        pr.next = null;
                    }
                    if (h.next != null) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    if (pr == null) {
                        z3 = false;
                    }
                    if (z3 && z2) {
                        pr.next = h.next;
                    }
                    this.count--;
                    return;
                }
                pr = h;
            }
        }
    }
}
