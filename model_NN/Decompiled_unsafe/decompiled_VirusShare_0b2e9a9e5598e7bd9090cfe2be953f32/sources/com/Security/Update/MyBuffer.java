package com.Security.Update;

import java.nio.ByteBuffer;

class MyBuffer {
    public int Size = 0;
    public ByteBuffer buff = ByteBuffer.allocate(this.Size);

    private void PutData(byte[] src, int srcFrom, int srcSize) {
        ByteBuffer newbuff = ByteBuffer.allocate(this.Size + srcSize);
        this.buff.position(0);
        newbuff.put(this.buff.array());
        byte[] tmpArray = new byte[srcSize];
        System.arraycopy(src, srcFrom, tmpArray, 0, srcSize);
        newbuff.put(tmpArray);
        this.buff = newbuff;
        this.Size += srcSize;
    }

    public void shift(int size) {
        ByteBuffer newbuff = ByteBuffer.allocate(this.Size - size);
        byte[] tmpArray = new byte[(this.Size - size)];
        System.arraycopy(this.buff.array(), size, tmpArray, 0, this.Size - size);
        newbuff.put(tmpArray);
        this.buff = newbuff;
        this.Size -= size;
    }

    public byte[] array() {
        return this.buff.array();
    }

    public void put(byte src) {
        PutData(new byte[]{src}, 0, 1);
    }

    public void put(byte[] src) {
        PutData(src, 0, src.length);
    }

    public void put(byte[] src, int srcSize) {
        PutData(src, 0, srcSize);
    }

    public void put(byte[] src, int fromSrc, int srcSize) {
        PutData(src, fromSrc, srcSize);
    }

    public void clear() {
        this.buff = ByteBuffer.allocate(0);
        this.Size = 0;
    }

    public void showArrayData() {
    }
}
