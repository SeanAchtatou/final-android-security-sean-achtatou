package com.Security.Update;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/* compiled from: MySocket */
class NIOServer {
    public Selector selector;

    public NIOServer() throws IOException {
        this.selector = null;
        this.selector = Selector.open();
    }

    public int DispQuery() throws IOException {
        int r = this.selector.select(100);
        if (r > -1) {
            Iterator<SelectionKey> selectedKeys = this.selector.selectedKeys().iterator();
            while (selectedKeys.hasNext()) {
                SelectionKey key = selectedKeys.next();
                selectedKeys.remove();
                try {
                    if (key.isValid()) {
                        if (key.isReadable()) {
                            ((CustomSocket) key.attachment()).onRead(key);
                        } else if (key.isConnectable()) {
                            ((SocketChannel) key.channel()).finishConnect();
                            if (((SocketChannel) key.channel()).isConnected()) {
                                key.interestOps(1);
                                ((CustomSocket) key.attachment()).onConnect(key);
                            }
                        } else if (key.isWritable()) {
                            ((CustomSocket) key.attachment()).onWrite(key);
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
        return r;
    }
}
