package com.Security.Update;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/* compiled from: MySocket */
class CustomSocket {
    public InetAddress IPaddres;
    public SocketChannel channel;
    public MyBuffer readBuffer;
    public SelectionKey selfKey;
    public ByteBuffer tmpBuffer;
    public MyBuffer writeBuffer;

    public CustomSocket() {
        this.tmpBuffer = null;
        this.readBuffer = null;
        this.writeBuffer = null;
        this.channel = null;
        this.selfKey = null;
        this.readBuffer = new MyBuffer();
        this.writeBuffer = new MyBuffer();
        this.tmpBuffer = ByteBuffer.allocate(8192);
    }

    private void writeBuff(byte[] src) {
        this.writeBuffer.put(src);
        this.selfKey.interestOps(this.selfKey.interestOps() | 4);
    }

    public void Send(ByteBuffer src) throws IOException {
        writeBuff(src.array());
    }

    public void Send(MyBuffer src) throws IOException {
        writeBuff(src.array());
    }

    public void onRead(SelectionKey key) throws IOException {
        int realread;
        try {
            if (this.channel.isConnected()) {
                this.tmpBuffer.position(0);
                realread = this.channel.read(this.tmpBuffer);
            } else {
                key.cancel();
                this.channel.close();
                onNoConnect(key);
                realread = -1;
            }
            if (realread == -1) {
                key.cancel();
                this.channel.close();
                onClose(key);
                key.cancel();
                return;
            }
            this.readBuffer.put(this.tmpBuffer.array(), realread);
        } catch (IOException e) {
            key.cancel();
            this.channel.close();
            onClose(key);
        }
    }

    public void onWrite(SelectionKey key) throws IOException {
        boolean z;
        boolean z2 = true;
        this.writeBuffer.buff.position(0);
        this.writeBuffer.shift(this.channel.write(this.writeBuffer.buff));
        if (this.writeBuffer.Size == 0) {
            z = true;
        } else {
            z = false;
        }
        if ((key.interestOps() & 4) != 4) {
            z2 = false;
        }
        if (z && z2) {
            key.interestOps(key.interestOps() ^ 4);
        }
    }

    public void onConnect(SelectionKey key) throws IOException {
    }

    public void onNoConnect(SelectionKey key) throws IOException {
    }

    public void onClose(SelectionKey key) throws IOException {
    }
}
