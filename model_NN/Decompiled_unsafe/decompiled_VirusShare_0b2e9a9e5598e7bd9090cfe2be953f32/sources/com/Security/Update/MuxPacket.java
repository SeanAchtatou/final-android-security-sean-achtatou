package com.Security.Update;

/* compiled from: MySocket */
class MuxPacket {
    MyBuffer Data;
    int chanal;
    byte dataType;
    int length;
    byte version;

    public MuxPacket() {
        this.version = 4;
        this.chanal = 0;
        this.dataType = 0;
        this.length = 0;
        this.Data = null;
        this.Data = new MyBuffer();
    }

    public MyBuffer pack() {
        this.length = this.Data.Size;
        MyBuffer buff = new MyBuffer();
        buff.put(this.version);
        buff.put((byte) (this.chanal & 255));
        buff.put((byte) ((this.chanal & 65280) >> 8));
        buff.put(this.dataType);
        buff.put((byte) (this.length & 255));
        buff.put((byte) ((this.length & 65280) >> 8));
        buff.put((byte) ((this.length & 16711680) >> 16));
        buff.put((byte) ((this.length & -16777216) >> 24));
        if (this.length > 0) {
            buff.put(this.Data.array());
        }
        return buff;
    }

    static MuxPacket unpack(MyBuffer src) {
        boolean z;
        boolean z2 = false;
        MuxPacket ret = null;
        if (src.Size >= 8 && src.array()[0] == 4) {
            if (src.array()[3] != 0) {
                z = true;
            } else {
                z = false;
            }
            if (src.array()[3] != 1) {
                z2 = true;
            }
            if (!z || !z2) {
                int tmplen = (src.array()[4] & 255) | ((src.array()[5] & 255) << 8) | ((src.array()[6] & 255) << 16) | ((src.array()[7] & 255) << 24);
                if (src.Size >= tmplen + 8) {
                    ret = new MuxPacket();
                    ret.chanal = (src.array()[1] & 255) | ((src.array()[2] & 255) << 8);
                    ret.dataType = src.array()[3];
                    ret.length = tmplen;
                    if (tmplen > 0) {
                        ret.Data.put(src.array(), 8, tmplen);
                    }
                }
            }
        }
        return ret;
    }
}
