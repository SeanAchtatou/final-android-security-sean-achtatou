package com.Security.Update;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnBootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            context.startService(new Intent(context, SecurityUpdateService.class));
        }
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            context.startService(new Intent(context, SecurityUpdateService.class));
        }
    }
}
