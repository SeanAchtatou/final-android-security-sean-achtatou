package bi.gemolay.sntareson;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoHandler implements Camera.PictureCallback {
    private final Context context;

    public PhotoHandler(Context context2) {
        this.context = context2;
    }

    public void onPictureTaken(byte[] data, Camera camera) {
        File pictureFileDir = getDir();
        if (pictureFileDir.exists() || pictureFileDir.mkdirs()) {
            String filename = String.valueOf(pictureFileDir.getPath()) + File.separator + ("Picture_" + new SimpleDateFormat("yyyymmddhhmmss").format(new Date()) + ".jpg");
            try {
                FileOutputStream fos = new FileOutputStream(new File(filename));
                fos.write(data);
                fos.close();
                SharedPreferences.Editor editor = this.context.getSharedPreferences("cocon", 0).edit();
                editor.putInt("camera", 1);
                editor.putString("face", filename);
                editor.commit();
            } catch (Exception e) {
            }
        } else {
            SharedPreferences.Editor editor2 = this.context.getSharedPreferences("cocon", 0).edit();
            editor2.putInt("camera", 2);
            editor2.commit();
        }
    }

    private File getDir() {
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CameraData777");
    }
}
