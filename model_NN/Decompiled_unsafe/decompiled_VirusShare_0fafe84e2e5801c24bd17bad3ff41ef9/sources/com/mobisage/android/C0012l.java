package com.mobisage.android;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ViewSwitcher;
import com.mobisage.android.SNSSSLSocketFactory;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: com.mobisage.android.l  reason: case insensitive filesystem */
abstract class C0012l extends FrameLayout {
    /* access modifiers changed from: private */
    public C0012l a;
    protected int adSize;
    protected int adViewState;
    private a b;
    protected C0014n backWebView;
    private c c;
    protected Context context;
    private LinkedBlockingQueue<C0002b> d;
    private String e;
    private String f;
    protected C0014n frontWebView;
    private String g;
    protected IMobiSageAdViewListener listener;
    protected Handler mainHandler;
    protected ViewSwitcher viewSwitcher;
    protected C0013m webClient;

    static /* synthetic */ void a(C0012l lVar, C0002b bVar) {
        lVar.d.remove(bVar);
        lVar.adViewState = 0;
    }

    public C0012l(Context context2, String str, String str2, String str3) {
        super(context2);
        this.context = context2;
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.adSize = -1;
        this.a = this;
        initMobiSageAdView(context2);
    }

    public C0012l(Context context2, int i, String str, String str2, String str3) {
        super(context2);
        this.context = context2;
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.adSize = i;
        this.a = this;
        initMobiSageAdView(context2);
    }

    public C0012l(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        this.adSize = -1;
        this.adSize = attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/" + context2.getPackageName(), "adsize", -1);
        this.e = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/" + context2.getPackageName(), "publisherID");
        this.g = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/" + context2.getPackageName(), "customdata");
        this.f = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/" + context2.getPackageName(), "keyword");
        this.a = this;
        initMobiSageAdView(context2);
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
    }

    public String getKeyword() {
        return this.f;
    }

    public void setMobiSageAdViewListener(IMobiSageAdViewListener listener2) {
        this.listener = listener2;
    }

    public void setKeyword(String keyword) {
        this.f = keyword;
    }

    public String getCustomData() {
        return this.g;
    }

    public void setCustomData(String customData) {
        this.g = customData;
    }

    /* access modifiers changed from: protected */
    public void initMobiSageAdView(Context context2) {
        setAnimationCacheEnabled(true);
        MobiSageManager.getInstance().a(context2);
        this.webClient = new C0013m();
        this.webClient.a = new SNSSSLSocketFactory.a(this, (byte) 0);
        if (this.e == null) {
            this.e = C0018r.a;
        }
        this.e = this.e.replace(" ", "").toLowerCase();
        this.adViewState = 0;
        this.d = new LinkedBlockingQueue<>();
        this.b = new a(this, (byte) 0);
        this.c = new c(this, (byte) 0);
        this.mainHandler = new Handler(context2.getMainLooper(), new b(this, (byte) 0));
        if (this.adSize == -1) {
            this.adSize = MobiSageAdSize.a();
        }
        this.viewSwitcher = new ViewSwitcher(context2);
        this.viewSwitcher.setLayoutParams(new FrameLayout.LayoutParams(MobiSageAdSize.b(this.adSize), MobiSageAdSize.c(this.adSize), 153));
        addView(this.viewSwitcher);
    }

    /* access modifiers changed from: protected */
    public void sendADRequest() {
        this.adViewState = 1;
        C0002b bVar = new C0002b();
        bVar.g = this.b;
        bVar.c.putInt("AdSize", this.adSize);
        bVar.c.putString("PublisherID", this.e);
        if (this.f != null) {
            bVar.c.putString("Keyword", this.f);
        } else {
            bVar.c.putString("Keyword", "");
        }
        bVar.c.putInt("NetworkState", C0025y.c(this.context));
        this.d.add(bVar);
        C0005e.a().a(1005, bVar);
    }

    /* access modifiers changed from: protected */
    public boolean requestADFromDE() {
        if (this.adViewState == 1) {
            return false;
        }
        while (this.d.size() != 0) {
            C0002b poll = this.d.poll();
            poll.g = null;
            C0005e.a().a(1007, poll);
        }
        switch (a()) {
            case -1:
                destoryAdView();
                return false;
            case 0:
                return true;
            case 1:
                sendADRequest();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void destoryAdView() {
        this.c = null;
        this.b = null;
        while (this.d.size() != 0) {
            C0005e.a().a(1007, this.d.poll());
        }
        this.d.clear();
        this.d = null;
        this.listener = null;
        this.webClient = null;
        this.viewSwitcher.clearAnimation();
        this.viewSwitcher.removeAllViews();
        this.frontWebView.destroy();
        this.frontWebView = null;
        if (this.backWebView != null) {
            this.backWebView.destroy();
            this.backWebView = null;
        }
        removeAllViews();
        this.viewSwitcher = null;
        this.a = null;
        this.context = null;
        System.gc();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001c A[Catch:{ Exception -> 0x0046 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a() {
        /*
            r7 = this;
            r6 = 0
            r5 = -1
            r4 = 1
            r3 = 0
            android.content.Context r0 = r7.context
            if (r0 != 0) goto L_0x000a
            r0 = r5
        L_0x0009:
            return r0
        L_0x000a:
            android.content.Context r0 = r7.context     // Catch:{ Exception -> 0x0046 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0046 }
        L_0x0010:
            java.lang.String r1 = r0.getName()     // Catch:{ Exception -> 0x0046 }
            java.lang.String r2 = "android.app.Activity"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x0046 }
            if (r1 != 0) goto L_0x0022
            java.lang.Class r0 = r0.getSuperclass()     // Catch:{ Exception -> 0x0046 }
            if (r0 != 0) goto L_0x0010
        L_0x0022:
            java.lang.String r1 = "mResumed"
            java.lang.reflect.Field r1 = r0.getDeclaredField(r1)     // Catch:{ Exception -> 0x0046 }
            r2 = 1
            r1.setAccessible(r2)     // Catch:{ Exception -> 0x0046 }
            android.content.Context r2 = r7.context     // Catch:{ Exception -> 0x0046 }
            boolean r1 = r1.getBoolean(r2)     // Catch:{ Exception -> 0x0046 }
            java.lang.String r2 = "mDestroyed"
            java.lang.reflect.Field r0 = r0.getDeclaredField(r2)     // Catch:{ Exception -> 0x00a6 }
            r2 = 1
            r0.setAccessible(r2)     // Catch:{ Exception -> 0x00a6 }
            android.content.Context r2 = r7.context     // Catch:{ Exception -> 0x00a6 }
            boolean r0 = r0.getBoolean(r2)     // Catch:{ Exception -> 0x00a6 }
        L_0x0042:
            if (r0 == 0) goto L_0x004b
            r0 = r5
            goto L_0x0009
        L_0x0046:
            r0 = move-exception
            r0 = r3
        L_0x0048:
            r1 = r0
            r0 = r3
            goto L_0x0042
        L_0x004b:
            if (r1 != 0) goto L_0x004f
            r0 = r3
            goto L_0x0009
        L_0x004f:
            android.content.Context r0 = r7.context
            java.lang.String r1 = "power"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.os.PowerManager r0 = (android.os.PowerManager) r0
            boolean r0 = r0.isScreenOn()
            if (r0 != 0) goto L_0x0061
            r0 = r3
            goto L_0x0009
        L_0x0061:
            com.mobisage.android.w r0 = com.mobisage.android.C0023w.a()
            java.lang.String r1 = "adswitch"
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x00a3
            com.mobisage.android.IMobiSageAdViewListener r0 = r7.listener
            if (r0 == 0) goto L_0x00a0
            com.mobisage.android.n r0 = r7.frontWebView
            if (r0 == 0) goto L_0x00a0
            android.widget.ViewSwitcher r0 = r7.viewSwitcher
            com.mobisage.android.n r1 = r7.frontWebView
            r0.removeView(r1)
            com.mobisage.android.n r0 = r7.frontWebView
            r0.destroy()
            r7.frontWebView = r6
            com.mobisage.android.n r0 = r7.backWebView
            if (r0 == 0) goto L_0x009b
            android.widget.ViewSwitcher r0 = r7.viewSwitcher
            com.mobisage.android.n r1 = r7.backWebView
            r0.removeView(r1)
            com.mobisage.android.n r0 = r7.backWebView
            r0.destroy()
            r7.backWebView = r6
        L_0x009b:
            com.mobisage.android.IMobiSageAdViewListener r0 = r7.listener
            r0.onMobiSageAdViewHide(r7)
        L_0x00a0:
            r0 = r3
            goto L_0x0009
        L_0x00a3:
            r0 = r4
            goto L_0x0009
        L_0x00a6:
            r0 = move-exception
            r0 = r1
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobisage.android.C0012l.a():int");
    }

    /* access modifiers changed from: protected */
    public void onRefreshTaskTrigger() {
        requestADFromDE();
    }

    /* access modifiers changed from: protected */
    public void requestADFinish(C0002b action) {
        this.d.remove(action);
        if (this.frontWebView == null) {
            this.frontWebView = new C0014n(this.context);
            this.frontWebView.getSettings().setSupportZoom(false);
            this.frontWebView.setBackgroundColor(0);
            this.frontWebView.e(this.e);
            this.frontWebView.d(this.g);
            this.frontWebView.setWebViewClient(this.webClient);
            this.frontWebView.loadDataWithBaseURL("", action.d.getString("BannerHTML"), "text/html", PROTOCOL_ENCODING.value, "");
            if (this.listener != null) {
                this.listener.onMobiSageAdViewShow(this);
            }
        } else {
            if (this.backWebView == null) {
                this.backWebView = new C0014n(this.context);
                this.backWebView.setBackgroundColor(0);
                this.backWebView.e(this.e);
                this.backWebView.getSettings().setSupportZoom(false);
            }
            this.backWebView.d(this.g);
            this.backWebView.setWebViewClient(this.webClient);
            this.backWebView.loadDataWithBaseURL("", action.d.getString("BannerHTML"), "text/html", PROTOCOL_ENCODING.value, "");
            if (this.listener != null) {
                this.listener.onMobiSageAdViewUpdate(this);
            }
        }
        if (action.d.containsKey("LpgCache")) {
            C0002b bVar = new C0002b();
            bVar.g = this.c;
            bVar.c.putStringArrayList("LpgCache", action.d.getStringArrayList("LpgCache"));
            this.d.add(bVar);
            C0005e.a().a(1006, bVar);
        }
    }

    /* renamed from: com.mobisage.android.l$b */
    class b implements Handler.Callback {
        /* synthetic */ b(C0012l lVar, byte b) {
            this();
        }

        private b() {
        }

        /* Debug info: failed to restart local var, previous not found, register: 1 */
        public final boolean handleMessage(Message message) {
            switch (message.what) {
                case 1000:
                    C0012l.this.a.requestADFinish((C0002b) message.obj);
                    break;
                case 1001:
                    C0012l.a(C0012l.this.a, (C0002b) message.obj);
                    break;
                case 1002:
                    C0012l.this.a.d.remove((C0002b) message.obj);
                    break;
                case 1003:
                    C0012l.this.a.d.remove((C0002b) message.obj);
                    break;
                case 1004:
                    C0012l.this.a.onRefreshTaskTrigger();
                    break;
                default:
                    return false;
            }
            return true;
        }
    }

    /* renamed from: com.mobisage.android.l$a */
    class a implements C0001a {
        private a() {
        }

        /* synthetic */ a(C0012l lVar, byte b) {
            this();
        }

        public final void a(C0002b bVar) {
            Message obtainMessage = C0012l.this.mainHandler.obtainMessage(1000);
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
        }

        public final void b(C0002b bVar) {
            Message obtainMessage = C0012l.this.mainHandler.obtainMessage(1001);
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
        }
    }

    /* renamed from: com.mobisage.android.l$c */
    class c implements C0001a {
        private c() {
        }

        /* synthetic */ c(C0012l lVar, byte b) {
            this();
        }

        public final void a(C0002b bVar) {
            Message obtainMessage = C0012l.this.mainHandler.obtainMessage(1002);
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
        }

        public final void b(C0002b bVar) {
            Message obtainMessage = C0012l.this.mainHandler.obtainMessage(1003);
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
        }
    }

    /* access modifiers changed from: protected */
    public void onLoadAdFinish() {
        switch (this.viewSwitcher.getChildCount()) {
            case 0:
                this.viewSwitcher.addView(this.frontWebView);
                break;
            case 1:
                this.frontWebView.setWebViewClient(null);
                this.viewSwitcher.addView(this.backWebView);
                this.viewSwitcher.setDisplayedChild(this.viewSwitcher.indexOfChild(this.backWebView));
                break;
            default:
                this.frontWebView.setWebViewClient(null);
                this.viewSwitcher.setDisplayedChild(this.viewSwitcher.indexOfChild(this.backWebView));
                break;
        }
        switchAdView();
    }

    /* access modifiers changed from: protected */
    public void switchAdView() {
        this.adViewState = 0;
        if (this.backWebView != null) {
            C0014n nVar = this.frontWebView;
            this.frontWebView = this.backWebView;
            this.backWebView = nVar;
            this.backWebView.clearCache(true);
            this.backWebView.destroyDrawingCache();
            this.backWebView.clearView();
        }
    }
}
