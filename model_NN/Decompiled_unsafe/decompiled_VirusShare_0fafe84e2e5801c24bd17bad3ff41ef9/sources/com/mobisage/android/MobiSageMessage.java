package com.mobisage.android;

import android.os.Bundle;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import org.apache.http.client.methods.HttpRequestBase;

public abstract class MobiSageMessage implements IMobiSageMessage {
    int b;
    UUID c = UUID.randomUUID();
    public IMobiSageMessageCallback callback;
    public Bundle result = new Bundle();

    protected MobiSageMessage() {
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        this.result.clear();
    }

    public HttpRequestBase createHttpRequest() throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        return null;
    }

    public Runnable createMessageRunnable() {
        return null;
    }
}
