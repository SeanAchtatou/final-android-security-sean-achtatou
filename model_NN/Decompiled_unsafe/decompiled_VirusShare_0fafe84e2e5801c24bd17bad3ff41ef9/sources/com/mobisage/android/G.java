package com.mobisage.android;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.concurrent.ConcurrentHashMap;

abstract class G implements Handler.Callback {
    protected Handler a = new Handler(Looper.myLooper(), this);
    protected ConcurrentHashMap<Integer, O> b = new ConcurrentHashMap<>();

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        this.b.clear();
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public boolean handleMessage(Message message) {
        if (message.what == 1007) {
            for (O next : this.b.values()) {
                if (next.d((C0002b) message.obj)) {
                    next.e((C0002b) message.obj);
                }
            }
        } else if (this.b.containsKey(Integer.valueOf(message.what))) {
            this.b.get(Integer.valueOf(message.what)).a(message);
            return true;
        }
        return false;
    }

    public final void a(int i, C0002b bVar) {
        Message obtainMessage = this.a.obtainMessage(i);
        obtainMessage.obj = bVar;
        obtainMessage.sendToTarget();
    }
}
