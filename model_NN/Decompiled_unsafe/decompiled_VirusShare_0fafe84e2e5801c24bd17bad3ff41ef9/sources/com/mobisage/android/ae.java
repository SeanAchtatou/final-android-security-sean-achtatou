package com.mobisage.android;

import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

final class ae extends P {
    ae() {
    }

    public final Animation a(int i) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, -1.0f * ((float) MobiSageAdSize.c(i)), 0.0f);
        translateAnimation.setDuration(1000);
        return translateAnimation;
    }

    public final Animation b(int i) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) MobiSageAdSize.c(i));
        translateAnimation.setDuration(1000);
        return translateAnimation;
    }
}
