package com.mobisage.android;

import java.io.UnsupportedEncodingException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

/* renamed from: com.mobisage.android.v  reason: case insensitive filesystem */
final class C0022v extends MobiSageReqMessage {
    public final HttpRequestBase createHttpRequest() {
        try {
            HttpPost httpPost = new HttpPost("http://config.adsage.com/mobisdk/02/cfg.htm");
            httpPost.setHeader("Connection", "close");
            httpPost.setHeader("Host", "config.adsage.com");
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            StringBuilder sb = new StringBuilder();
            sb.append("pid=" + C0018r.a);
            if (this.params.containsKey("key")) {
                sb.append("&key=" + this.params.getString("key"));
            }
            httpPost.setEntity(new StringEntity(sb.toString()));
            return httpPost;
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public final Runnable createMessageRunnable() {
        return new C0024x(this);
    }
}
