package com.mobisage.android;

import java.io.File;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

public class MobiSageResMessage extends MobiSageMessage {
    Boolean d;
    long e;
    public String sourceURL;
    public String targetURL;
    public String tempURL;

    public MobiSageResMessage() {
        this.b = 2;
    }

    public HttpRequestBase createHttpRequest() {
        HttpGet httpGet = new HttpGet(this.sourceURL);
        File file = new File(this.tempURL);
        if (file.exists()) {
            this.d = true;
            this.e = file.length();
            httpGet.setHeader("Range", String.format("bytes=%d-", Long.valueOf(file.length())));
        } else {
            this.d = false;
        }
        return httpGet;
    }

    public Runnable createMessageRunnable() {
        return new L(this);
    }
}
