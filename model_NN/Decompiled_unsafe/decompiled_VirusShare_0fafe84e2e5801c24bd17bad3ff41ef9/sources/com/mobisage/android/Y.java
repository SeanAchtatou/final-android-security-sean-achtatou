package com.mobisage.android;

import android.os.Message;

final class Y extends G {
    private static Y c = new Y();

    public static Y a() {
        return c;
    }

    private Y() {
        W w = new W(this.a);
        this.b.put(Integer.valueOf(w.c), w);
        ac acVar = new ac(this.a);
        this.b.put(Integer.valueOf(acVar.c), acVar);
        ab abVar = new ab(this.a);
        this.b.put(Integer.valueOf(abVar.c), abVar);
        T t = new T(this.a);
        this.b.put(Integer.valueOf(t.c), t);
        S s = new S(this.a);
        this.b.put(Integer.valueOf(s.c), s);
        U u = new U(this.a);
        this.b.put(Integer.valueOf(u.c), u);
        V v = new V(this.a);
        this.b.put(Integer.valueOf(v.c), v);
        Message obtainMessage = this.a.obtainMessage(u.j);
        obtainMessage.obj = new C0002b();
        obtainMessage.sendToTarget();
    }
}
