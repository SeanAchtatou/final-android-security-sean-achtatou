package com.mobisage.android;

import android.os.Handler;
import android.text.format.Time;
import com.mobisage.android.SNSSSLSocketFactory;
import java.io.File;
import java.net.URLEncoder;
import java.util.UUID;

final class U extends aa {
    U(Handler handler) {
        super(handler);
        this.c = u.p;
    }

    /* access modifiers changed from: protected */
    public final void f(C0002b bVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("http://trc.adsage.com/trc/sdk/x.gif?");
        sb.append("ver=bd_01");
        sb.append("&pid=" + C0018r.a);
        sb.append("&kw=" + URLEncoder.encode(bVar.c.getString("keyword")));
        Time time = new Time();
        time.setToNow();
        String str = C0018r.f + "/Track" + "/" + String.valueOf(time.toMillis(true) / 1000) + "_" + UUID.randomUUID().toString() + ".dat";
        File file = new File(str);
        file.mkdirs();
        SNSSSLSocketFactory.a.a(file, sb.toString());
        this.e.put(bVar.b, bVar);
        X x = new X();
        x.a = str;
        x.callback = this.g;
        bVar.f.add(x);
        this.f.put(x.c, bVar.b);
        H.a().a(x);
    }
}
