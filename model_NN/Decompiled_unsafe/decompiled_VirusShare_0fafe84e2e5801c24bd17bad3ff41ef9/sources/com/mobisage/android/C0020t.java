package com.mobisage.android;

import android.os.Environment;
import java.io.File;

/* renamed from: com.mobisage.android.t  reason: case insensitive filesystem */
final class C0020t extends Q {
    protected C0020t() {
    }

    public final void run() {
        long currentTimeMillis = System.currentTimeMillis();
        a(new File(C0018r.e + "/Temp" + "/"), currentTimeMillis);
        a(new File(C0018r.e + "/Cache" + "/"), currentTimeMillis);
        a(new File(Environment.getExternalStorageDirectory() + "/MobiSage" + "/Temp" + "/"), currentTimeMillis);
        a(new File(Environment.getExternalStorageDirectory() + "/MobiSage" + "/Cache" + "/"), currentTimeMillis);
    }

    private static void a(File file, long j) {
        if (file.exists()) {
            File[] listFiles = file.listFiles();
            for (File file2 : listFiles) {
                if (file2.lastModified() + 604800000 < j) {
                    file2.delete();
                }
            }
        }
    }
}
