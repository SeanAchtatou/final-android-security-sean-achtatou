package com.mobisage.android;

import android.os.Build;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EncodingUtils;

final class I extends MobiSageReqMessage {
    I() {
    }

    private static final byte[] a(int i) {
        return new byte[]{(byte) i, (byte) (i >> 8), (byte) (i >> 16), i >> 24};
    }

    public final HttpRequestBase createHttpRequest() {
        try {
            HttpPost httpPost = new HttpPost("http://mobi.adsage.com/sdk/default.js");
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byteArrayOutputStream.write(a(4), 0, 1);
            byteArrayOutputStream.write(a(MobiSageAdSize.b(this.params.getInt("AdSize"))));
            byteArrayOutputStream.write(a(MobiSageAdSize.c(this.params.getInt("AdSize"))));
            byte[] asciiBytes = EncodingUtils.getAsciiBytes(Build.MODEL);
            byteArrayOutputStream.write(a(asciiBytes.length));
            byteArrayOutputStream.write(asciiBytes);
            byte[] asciiBytes2 = EncodingUtils.getAsciiBytes(Build.VERSION.RELEASE);
            byteArrayOutputStream.write(a(asciiBytes2.length));
            byteArrayOutputStream.write(asciiBytes2);
            byteArrayOutputStream.write(a(this.params.getInt("NetworkState")), 0, 1);
            byte[] asciiBytes3 = EncodingUtils.getAsciiBytes(this.params.getString("PublisherID"));
            byteArrayOutputStream.write(a(asciiBytes3.length));
            byteArrayOutputStream.write(asciiBytes3);
            byte[] asciiBytes4 = EncodingUtils.getAsciiBytes(C0025y.a);
            byteArrayOutputStream.write(a(asciiBytes4.length));
            byteArrayOutputStream.write(asciiBytes4);
            byte[] asciiBytes5 = EncodingUtils.getAsciiBytes(this.params.getString("Keyword"));
            byteArrayOutputStream.write(a(asciiBytes5.length));
            byteArrayOutputStream.write(asciiBytes5);
            byteArrayOutputStream.write(a(1), 0, 1);
            byte[] asciiBytes6 = EncodingUtils.getAsciiBytes(MobiSageEnviroment.SDK_Version);
            byteArrayOutputStream.write(a(asciiBytes6.length));
            byteArrayOutputStream.write(asciiBytes6);
            byteArrayOutputStream.write(a(2));
            byte[] asciiBytes7 = EncodingUtils.getAsciiBytes(C0018r.c);
            byteArrayOutputStream.write(a(asciiBytes7.length));
            byteArrayOutputStream.write(asciiBytes7);
            byteArrayOutputStream.write(a(1), 0, 1);
            byte[] asciiBytes8 = EncodingUtils.getAsciiBytes(D.a().b());
            byteArrayOutputStream.write(a(asciiBytes8.length));
            byteArrayOutputStream.write(asciiBytes8);
            byteArrayOutputStream.write(a(0));
            byte[] asciiBytes9 = EncodingUtils.getAsciiBytes(C0018r.b);
            byteArrayOutputStream.write(a(asciiBytes9.length));
            byteArrayOutputStream.write(asciiBytes9);
            httpPost.setEntity(new ByteArrayEntity(byteArrayOutputStream.toByteArray()));
            httpPost.setHeader("Connection", "close");
            byteArrayOutputStream.close();
            return httpPost;
        } catch (IOException e) {
            return null;
        }
    }
}
