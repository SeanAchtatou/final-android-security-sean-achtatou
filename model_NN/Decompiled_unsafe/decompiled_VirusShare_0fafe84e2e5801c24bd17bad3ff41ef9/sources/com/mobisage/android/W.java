package com.mobisage.android;

import android.os.Handler;
import java.io.File;

final class W extends aa {
    W(Handler handler) {
        super(handler);
        this.c = u.j;
    }

    /* access modifiers changed from: protected */
    public final void f(C0002b bVar) {
        this.e.put(bVar.b, bVar);
        File file = new File(C0018r.f + "/Track" + "/");
        if (!file.exists()) {
            file.mkdirs();
        }
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File path : listFiles) {
                X x = new X();
                x.a = path.getPath();
                x.callback = this.g;
                bVar.f.add(x);
                this.f.put(x.c, bVar.b);
                H.a().a(x);
            }
        }
        if (bVar.a()) {
            this.e.remove(bVar.b);
            if (bVar.g != null) {
                bVar.g.a(bVar);
            }
        }
    }
}
