package com.mobisage.android;

import org.apache.http.client.HttpClient;

abstract class N implements Runnable {
    protected MobiSageMessage a;
    protected HttpClient b;

    protected N(MobiSageMessage mobiSageMessage) {
        this.a = mobiSageMessage;
    }

    public void a() {
        if (this.b != null) {
            this.b.getConnectionManager().shutdown();
        }
    }

    public void run() {
        H.a().c(this.a);
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        this.a = null;
    }
}
