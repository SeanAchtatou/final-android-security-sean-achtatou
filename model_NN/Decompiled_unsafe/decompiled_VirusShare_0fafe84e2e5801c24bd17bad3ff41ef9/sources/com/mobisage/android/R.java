package com.mobisage.android;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

final class R {
    private static R a = new R();
    private ScheduledThreadPoolExecutor b = new ScheduledThreadPoolExecutor(4);
    private ConcurrentHashMap<UUID, ScheduledFuture<?>> c = new ConcurrentHashMap<>();

    public static R a() {
        return a;
    }

    private R() {
    }

    public final void a(Q q) {
        if (true == q.d) {
            this.c.put(q.b, this.b.scheduleAtFixedRate(q, q.c, q.e, TimeUnit.SECONDS));
            return;
        }
        this.c.put(q.b, this.b.schedule(q, q.c, TimeUnit.SECONDS));
    }

    public final void b(Q q) {
        if (this.c.containsKey(q.b)) {
            this.c.get(q.b).cancel(true);
            this.c.remove(q.b);
        }
    }
}
