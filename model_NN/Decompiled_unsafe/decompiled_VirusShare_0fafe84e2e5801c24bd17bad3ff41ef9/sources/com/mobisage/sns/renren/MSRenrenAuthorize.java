package com.mobisage.sns.renren;

import java.net.URLEncoder;

public class MSRenrenAuthorize extends MSRenrenMessage {
    public MSRenrenAuthorize(String clientID) {
        super(clientID);
        this.urlPath = "https://graph.renren.com/oauth/authorize";
        this.httpMethod = "GET";
        this.paramMap.put("client_id", clientID);
        this.paramMap.put("response_type", "code");
        this.paramMap.put("redirect_uri", "http://graph.renren.com/oauth/login_success.html");
        this.paramMap.put("display", "touch");
    }

    public String generateAuthorizeURL() {
        StringBuilder sb = new StringBuilder();
        for (String str : this.paramMap.keySet()) {
            if (sb.length() == 0) {
                sb.append("?");
            } else {
                sb.append("&");
            }
            sb.append(str + "=" + URLEncoder.encode((String) this.paramMap.get(str)));
        }
        return this.urlPath + sb.toString();
    }
}
