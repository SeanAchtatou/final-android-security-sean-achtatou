package com.mobisage.sns.renren;

import MobWin.cnst.PROTOCOL_ENCODING;
import com.tencent.lbsapi.core.e;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;

public class MSRenrenPhotosUpload extends MSRenrenMessage {
    private String a;

    public MSRenrenPhotosUpload(String accessToken, String secretKey) {
        super(accessToken, secretKey);
        this.urlPath = "http://api.renren.com/restserver.do";
        this.httpMethod = "POST";
        this.paramMap.put("access_token", accessToken);
        this.paramMap.put("v", "2.0");
        this.paramMap.put("method", "photos.upload");
        this.paramMap.put("format", "json");
    }

    public void addParam(String name, String value) {
        if (name.equals("upload")) {
            this.a = value;
        } else {
            super.addParam(name, value);
        }
    }

    public HttpRequestBase createHttpRequest() throws IOException, NoSuchAlgorithmException {
        if (this.isSignature) {
            generateSignature();
        }
        UUID randomUUID = UUID.randomUUID();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String str = "\r\n--" + randomUUID.toString() + "\r\n";
        byte[] bytes = str.getBytes(e.e);
        byteArrayOutputStream.write(bytes, 0, bytes.length);
        for (String str2 : this.paramMap.keySet()) {
            byte[] bytes2 = ("Content-Disposition: form-data; name=\"" + str2 + "\"\r\n\r\n" + ((String) this.paramMap.get(str2))).getBytes(PROTOCOL_ENCODING.value);
            byteArrayOutputStream.write(bytes2, 0, bytes2.length);
            byte[] bytes3 = str.getBytes(PROTOCOL_ENCODING.value);
            byteArrayOutputStream.write(bytes3, 0, bytes3.length);
        }
        byte[] bytes4 = ("Content-Disposition: form-data; name=\"upload\"; filename=\"" + this.a + "\"\r\n").getBytes(PROTOCOL_ENCODING.value);
        byteArrayOutputStream.write(bytes4, 0, bytes4.length);
        byte[] bytes5 = "Content-Type:application/octet-stream\r\n\r\n".getBytes(PROTOCOL_ENCODING.value);
        byteArrayOutputStream.write(bytes5, 0, bytes5.length);
        FileInputStream fileInputStream = new FileInputStream(new File(this.a));
        byte[] bArr = new byte[1024];
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                fileInputStream.close();
                byte[] bytes6 = ("\r\n--" + randomUUID.toString() + "--\r\n").getBytes(PROTOCOL_ENCODING.value);
                byteArrayOutputStream.write(bytes6, 0, bytes6.length);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                HttpPost httpPost = new HttpPost(this.urlPath);
                httpPost.setHeader("Content-Type", "multipart/form-data; boundary=" + randomUUID.toString());
                httpPost.setEntity(new ByteArrayEntity(byteArray));
                byteArrayOutputStream.close();
                return httpPost;
            }
        }
    }
}
