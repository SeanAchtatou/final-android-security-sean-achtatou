package com.mobisage.sns.renren;

public class MSRenrenToken extends MSRenrenMessage {
    public MSRenrenToken(String clientID, String clientSecret, String code) {
        super(clientID);
        this.secretKey = clientSecret;
        this.urlPath = "https://graph.renren.com/oauth/token";
        this.httpMethod = "GET";
        this.paramMap.put("client_id", clientID);
        this.paramMap.put("client_secret", clientSecret);
        this.paramMap.put("code", code);
        this.paramMap.put("grant_type", "authorization_code");
        this.paramMap.put("redirect_uri", "http://graph.renren.com/oauth/login_success.html");
    }
}
