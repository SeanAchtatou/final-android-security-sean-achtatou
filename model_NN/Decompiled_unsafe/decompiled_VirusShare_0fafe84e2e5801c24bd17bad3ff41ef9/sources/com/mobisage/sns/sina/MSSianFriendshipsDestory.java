package com.mobisage.sns.sina;

public class MSSianFriendshipsDestory extends MSSinaWeiboMessage {
    public MSSianFriendshipsDestory(String appKey, String accessToken) {
        super(appKey, accessToken);
        this.urlPath = "https://api.weibo.com/2/friendships/destroy.json";
        this.httpMethod = "POST";
    }
}
