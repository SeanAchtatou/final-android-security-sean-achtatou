package com.taobao.munion.ads;

import android.graphics.Bitmap;
import android.util.Log;
import com.taobao.munion.ads.internal.ImageDownloader;
import java.io.Serializable;
import org.json.JSONArray;
import org.json.JSONObject;

public final class Ad implements Serializable {
    private static final String a = "Ad";
    private static final long b = 1;
    private static final ImageDownloader c = new ImageDownloader();
    private String A = null;
    private String B = null;
    private Bitmap C = null;
    private TYPE d = null;
    private int e = 0;
    private int[] f = new int[2];
    private boolean g = false;
    private boolean h = false;
    private boolean i = false;
    private String j = null;
    private String k = null;
    private String l = null;
    private String m = null;
    private Bitmap n = null;
    private String o = null;
    private String p = null;
    private String q = null;
    private String r = null;
    private int[] s = null;
    private Bitmap t = null;
    private String u = null;
    private String v = null;
    private float w = 0.0f;
    private int x = 0;
    private int[] y;
    private int[] z;

    public enum TYPE {
        IMAGE_AD,
        TEXT_AD
    }

    public Ad(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("t")) {
                int i2 = jSONObject.getInt("t");
                if (1 == i2) {
                    this.d = TYPE.IMAGE_AD;
                } else if (2 == i2) {
                    this.d = TYPE.TEXT_AD;
                }
                if (jSONObject.has("r")) {
                    this.e = jSONObject.getInt("r");
                }
                if (jSONObject.has("d")) {
                    JSONArray jSONArray = jSONObject.getJSONArray("d");
                    for (int i3 = 0; i3 < 2; i3++) {
                        this.f[i3] = jSONArray.getInt(i3);
                    }
                    if (jSONObject.has("g")) {
                        int i4 = jSONObject.getInt("g");
                        if (1 == i4) {
                            this.h = true;
                        } else if (i4 == 0) {
                            this.h = false;
                        }
                    }
                    if (jSONObject.has("p")) {
                        int i5 = jSONObject.getInt("p");
                        if (1 == i5) {
                            this.i = true;
                        } else if (i5 == 0) {
                            this.i = false;
                        }
                    }
                    if (jSONObject.has("u")) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject("u");
                        this.k = jSONObject2.has("m") ? jSONObject2.getString("m") : null;
                        this.j = jSONObject2.has("u") ? jSONObject2.getString("u") : null;
                    }
                    if (jSONObject.has("v")) {
                        JSONArray jSONArray2 = jSONObject.getJSONArray("v");
                        int length = jSONArray2 != null ? jSONArray2.length() : 0;
                        for (int i6 = 0; i6 < length; i6++) {
                            JSONObject jSONObject3 = jSONArray2.getJSONObject(i6);
                            if (jSONObject3 != null) {
                                String string = jSONObject3.has("t") ? jSONObject3.getString("t") : null;
                                if ("bg".equals(string)) {
                                    if (jSONObject3.has("u")) {
                                        this.l = jSONObject3.getString("u");
                                    } else {
                                        this.o = jSONObject3.has("c1") ? jSONObject3.getString("c1") : "#000000";
                                        this.p = jSONObject3.has("c2") ? jSONObject3.getString("c2") : "#4c4c4c";
                                    }
                                    this.m = jSONObject3.has("cu") ? jSONObject3.getString("cu") : null;
                                } else if ("i".equals(string)) {
                                    this.q = jSONObject3.has("u") ? jSONObject3.getString("u") : null;
                                    this.r = jSONObject3.has("cu") ? jSONObject3.getString("cu") : null;
                                    String string2 = jSONObject3.has("p") ? jSONObject3.getString("p") : null;
                                    if (string2 != null) {
                                        String[] split = string2.substring(1, string2.length() - 1).split(",");
                                        this.s = new int[split.length];
                                        if (4 == split.length) {
                                            this.s[0] = Integer.parseInt(split[0]);
                                            this.s[1] = Integer.parseInt(split[1]);
                                            this.s[2] = Integer.parseInt(split[2]);
                                            this.s[3] = Integer.parseInt(split[3]);
                                        }
                                    }
                                } else if ("t".equals(string)) {
                                    this.u = jSONObject3.has("x") ? jSONObject3.getString("x") : "Sample ads";
                                    this.v = jSONObject3.has("fc") ? jSONObject3.getString("fc") : "#ffffff";
                                    this.w = (float) (jSONObject3.has("fs") ? jSONObject3.getInt("fs") : 13);
                                    this.x = jSONObject3.has("b") ? jSONObject3.getInt("b") : 1;
                                    String string3 = jSONObject3.has("p") ? jSONObject3.getString("p") : null;
                                    if (string3 != null) {
                                        String[] split2 = string3.substring(1, string3.length() - 1).split(",");
                                        this.y = new int[split2.length];
                                        if (4 == split2.length) {
                                            this.y[0] = Integer.parseInt(split2[0]);
                                            this.y[1] = Integer.parseInt(split2[1]);
                                            this.y[2] = Integer.parseInt(split2[2]);
                                            this.y[3] = Integer.parseInt(split2[3]);
                                        }
                                    }
                                } else if ("l".equals(string)) {
                                    this.A = jSONObject3.has("u") ? jSONObject3.getString("u") : null;
                                    this.B = jSONObject3.has("cu") ? jSONObject3.getString("cu") : null;
                                    String string4 = jSONObject3.has("p") ? jSONObject3.getString("p") : null;
                                    if (string4 != null) {
                                        String[] split3 = string4.substring(1, string4.length() - 1).split(",");
                                        this.z = new int[split3.length];
                                        if (4 == split3.length) {
                                            this.z[0] = Integer.parseInt(split3[0]);
                                            this.z[1] = Integer.parseInt(split3[1]);
                                            this.z[2] = Integer.parseInt(split3[2]);
                                            this.z[3] = Integer.parseInt(split3[3]);
                                        }
                                    }
                                }
                            }
                        }
                        this.g = true;
                        if (TYPE.IMAGE_AD == this.d) {
                            this.n = c.a(this.l, ImageDownloader.Type.AD_IMAGE);
                            if (this.n == null) {
                                this.g = false;
                                Log.d(a, "get ad iamge failed");
                                return;
                            }
                        } else if (TYPE.TEXT_AD == this.d) {
                            this.n = c.a(this.l, ImageDownloader.Type.BG_IMAGE);
                            this.t = c.a(this.q, ImageDownloader.Type.ICON_IMAGE);
                            if (this.n == null) {
                                this.g = false;
                                Log.d(a, "get background image failed");
                                return;
                            }
                        }
                        this.C = c.a(this.A, ImageDownloader.Type.LOGO_IMAGE);
                        return;
                    }
                    this.g = false;
                    return;
                }
                this.g = false;
                return;
            }
            this.g = false;
        } catch (Exception e2) {
            this.g = false;
            Log.e("AdView", "Failed to parse ads:" + e2);
        }
    }

    private boolean x() {
        return this.h;
    }

    private boolean y() {
        return this.i;
    }

    private String z() {
        return this.A;
    }

    public final String a() {
        return this.j;
    }

    public final String b() {
        return this.k;
    }

    public final String c() {
        return this.u;
    }

    public final String d() {
        return this.l;
    }

    public final String e() {
        return this.m;
    }

    public final String f() {
        return this.q;
    }

    public final String g() {
        return this.r;
    }

    public final String h() {
        return this.o;
    }

    public final String i() {
        return this.p;
    }

    public final String j() {
        return this.v;
    }

    public final float k() {
        return this.w;
    }

    public final String l() {
        return this.B;
    }

    public final int m() {
        return this.x;
    }

    public final int[] n() {
        return this.s;
    }

    public final int[] o() {
        return this.y;
    }

    public final int[] p() {
        return this.z;
    }

    public final int[] q() {
        return this.f;
    }

    public final int r() {
        return this.e;
    }

    public final boolean s() {
        return this.g;
    }

    public final TYPE t() {
        return this.d;
    }

    public final Bitmap u() {
        return this.n;
    }

    public final Bitmap v() {
        return this.t;
    }

    public final Bitmap w() {
        return this.C;
    }
}
