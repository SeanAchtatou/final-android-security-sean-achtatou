package com.baidu;

import android.util.Log;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;

class bk {
    private static final SimpleDateFormat a = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");

    static {
        a();
    }

    bk() {
    }

    public static int a(String str) {
        return a("Mobads SDK", str);
    }

    public static int a(String str, String str2) {
        if (!a(3)) {
            return -1;
        }
        d(str, str2);
        return Log.d(str, str2);
    }

    public static int a(String str, String str2, Throwable th) {
        return a(String.format("%s:: %s", str, str2), th);
    }

    public static int a(String str, Throwable th) {
        if (!a(5)) {
            return -1;
        }
        c("Mobads SDK", str, th);
        return Log.w("Mobads SDK", str, th);
    }

    public static int a(Throwable th) {
        return a("", th);
    }

    public static void a() {
        w.b("_b_sdk.log");
    }

    public static boolean a(int i) {
        return a("Mobads SDK", i);
    }

    public static boolean a(String str, int i) {
        return i >= 6;
    }

    public static int b(String str) {
        if (!a(5)) {
            return -1;
        }
        d("Mobads SDK", str);
        return Log.w("Mobads SDK", str);
    }

    public static int b(String str, String str2) {
        return b(String.format("%s:: %s", str, str2));
    }

    public static int b(String str, String str2, Throwable th) {
        return b(String.format("%s:: %s", str, str2), th);
    }

    public static int b(String str, Throwable th) {
        if (!a(6)) {
            return -1;
        }
        c("Mobads SDK", str, th);
        return Log.e("Mobads SDK", str, th);
    }

    public static int c(String str) {
        if (!a(6)) {
            return -1;
        }
        d("Mobads SDK", str);
        return Log.e("Mobads SDK", str);
    }

    public static int c(String str, String str2) {
        return c(String.format("%s:: %s", str, str2));
    }

    private static void c(String str, String str2, Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        d(str, str2 + "\n" + stringWriter.toString());
        printWriter.close();
        try {
            stringWriter.close();
        } catch (IOException e) {
            Log.w("Log.debug", "", e);
        }
    }

    private static synchronized void d(String str, String str2) {
        synchronized (bk.class) {
        }
    }
}
