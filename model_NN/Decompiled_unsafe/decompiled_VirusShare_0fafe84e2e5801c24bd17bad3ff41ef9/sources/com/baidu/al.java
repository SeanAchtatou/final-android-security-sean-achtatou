package com.baidu;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.View;

class al implements View.OnClickListener {
    final /* synthetic */ aj a;

    al(aj ajVar) {
        this.a = ajVar;
    }

    public void onClick(View view) {
        try {
            if (!"mounted".equals(Environment.getExternalStorageState())) {
                this.a.b.d.post(new am(this));
                return;
            }
            bk.b("action.onClick [to download]");
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.a.b.c.c()));
            intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            this.a.b.startActivity(intent);
            as.a().a(this.a.b.c);
        } catch (Exception e) {
            bk.a(e);
        }
    }
}
