package com.baidu;

public enum FailReason {
    NO_AD("没有推广返回"),
    OTHER_EXCEPTION("网络或其它异常");
    
    private String a;

    private FailReason(String str) {
        this.a = str;
    }

    public String getValue() {
        return this.a;
    }
}
