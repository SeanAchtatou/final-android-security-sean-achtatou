package com.baidu;

enum t {
    TEXT(1),
    IMAGE(2),
    APP(3);
    
    private int d;

    private t(int i) {
        this.d = i;
    }

    public static t a(String str) {
        return TEXT.toString().equals(str.toLowerCase()) ? TEXT : IMAGE.toString().equals(str.toLowerCase()) ? IMAGE : APP.toString().equals(str.toLowerCase()) ? APP : TEXT;
    }

    public static t[] a() {
        return (t[]) e.clone();
    }

    public int b() {
        return this.d;
    }

    public AdType c() {
        AdType adType = AdType.TEXT;
        switch (u.a[ordinal()]) {
            case 1:
            case 2:
                return AdType.TEXT;
            case 3:
                return AdType.IMAGE;
            default:
                return adType;
        }
    }

    public String toString() {
        return super.toString().toLowerCase();
    }
}
