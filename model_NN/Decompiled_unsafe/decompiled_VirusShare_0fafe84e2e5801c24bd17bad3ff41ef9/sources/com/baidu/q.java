package com.baidu;

import android.content.Context;
import java.util.TimerTask;

class q extends TimerTask {
    final /* synthetic */ o a;
    private Context b;
    private s c;
    private String d;

    public q(o oVar) {
        this.a = oVar;
    }

    public void a(Context context, String str, s sVar) {
        this.b = context;
        this.d = str;
        this.c = sVar;
    }

    public void run() {
        if (this.b == null || this.c == null || this.d == null) {
            bk.b("context == " + this.b + " action == " + this.c + " indexer== " + this.d);
            return;
        }
        bk.b("AdRequestWrapper.run", this.a.g.toString());
        new p(this.a, this.b, this.d, this.c).start();
    }
}
