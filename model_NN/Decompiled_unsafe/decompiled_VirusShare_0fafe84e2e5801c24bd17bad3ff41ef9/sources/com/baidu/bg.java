package com.baidu;

import android.graphics.drawable.BitmapDrawable;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import java.util.HashMap;
import java.util.Map;

class bg extends l {
    private Map<String, GifAnimView> k = new HashMap();
    private int l = 255;

    public bg(AdView adView) {
        super(adView);
        bk.b("ImageAdContainer", "{");
        setFocusable(true);
        setClickable(true);
        bk.b("ImageAdContainer", "}");
    }

    private void l() {
        GifAnimView gifAnimView;
        if (this.b != null) {
            for (Map.Entry next : this.k.entrySet()) {
                ((GifAnimView) next.getValue()).setVisibility(8);
                ((GifAnimView) next.getValue()).a();
            }
            setBackgroundDrawable(null);
            switch (bi.a[this.b.n().ordinal()]) {
                case 1:
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(this.b.c());
                    bitmapDrawable.setAlpha(this.l);
                    setBackgroundDrawable(bitmapDrawable);
                    break;
                case 2:
                    if (!this.k.containsKey(this.b.m())) {
                        gifAnimView = new GifAnimView(getContext(), new bh(this));
                        gifAnimView.a(w.e(false, getContext(), this.b.a()));
                        addView(gifAnimView);
                        this.k.put(this.b.m(), gifAnimView);
                    } else {
                        gifAnimView = this.k.get(this.b.m());
                        gifAnimView.setVisibility(0);
                        gifAnimView.b();
                    }
                    gifAnimView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                    ViewGroup.LayoutParams layoutParams = getLayoutParams();
                    gifAnimView.a(layoutParams.width, layoutParams.height);
                    gifAnimView.a(this.l);
                    break;
            }
            if (this.e != null) {
                removeView(this.e);
                this.e = null;
            }
            this.e = new ImageView(getContext());
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(this.h, this.h);
            layoutParams2.addRule(15);
            layoutParams2.addRule(11);
            this.e.setLayoutParams(layoutParams2);
            this.e.setImageBitmap(this.b.e());
            this.e.setAlpha((int) (((double) this.l) * 0.8d));
            addView(this.e);
        }
    }

    public void a(int i) {
    }

    public void a(Ad ad, t tVar, int i, ag agVar) {
        bk.b("ImageAdContainer", "setAd");
        this.g = agVar.a;
        this.h = agVar.b;
        this.c = (int) (0.1041666641831398d * ((double) this.h));
        if (ad == null || ad.c() == null) {
            throw new IllegalArgumentException();
        }
        this.d = false;
        this.b = ad;
        l();
        super.a(ad, tVar, i, agVar);
    }

    public void b(int i) {
        boolean z = this.l != i;
        this.l = i;
        if (z) {
            l();
        }
    }

    public void setBackgroundColor(int i) {
    }

    public void setVisibility(int i) {
        if (getVisibility() != i) {
            super.setVisibility(i);
            bk.b("ImageAdContainer.setVisibility", "visibility:" + i);
            try {
                GifAnimView gifAnimView = this.k.get(this.b.m());
                if (gifAnimView == null) {
                    return;
                }
                if (i != 0) {
                    gifAnimView.a();
                } else {
                    gifAnimView.b();
                }
            } catch (Exception e) {
                bk.a(e);
            }
        }
    }
}
