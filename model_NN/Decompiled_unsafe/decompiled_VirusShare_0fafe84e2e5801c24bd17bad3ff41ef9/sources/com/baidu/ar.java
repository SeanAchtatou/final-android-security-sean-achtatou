package com.baidu;

import android.content.Context;
import android.content.Intent;

class ar extends bn {
    public ar(AdView adView) {
        super(adView);
    }

    private void m() {
        try {
            bk.b("AppAdContainer.toShop");
            Intent intent = new Intent();
            intent.setClass(getContext(), AppActivity.class);
            intent.putExtra("curl", h().getClickURL());
            intent.putExtra("clklogurl", h().h());
            intent.putExtra("surl", h().getSURL());
            intent.putExtra("tit", h().getTitle());
            intent.putExtra("desc", h().getDescription());
            intent.putExtra("ad_charge", h().o());
            intent.putExtra("limg", h().a());
            getContext().startActivity(intent);
            c().put(ay.Click, Integer.valueOf(c().get(ay.Click).intValue() + 1));
            as.a().a(getContext());
        } catch (Exception e) {
            bk.a(e);
        }
    }

    public void a(Context context) {
        bk.b("AppAdContainer.clicked");
        m();
    }

    public void b(Context context) {
        bk.b("AppAdContainer.phoned");
        m();
    }
}
