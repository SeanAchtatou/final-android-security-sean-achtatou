package com.baidu;

import java.io.File;
import java.util.Comparator;

class f implements Comparator<File> {
    final /* synthetic */ c a;

    f(c cVar) {
        this.a = cVar;
    }

    /* renamed from: a */
    public int compare(File file, File file2) {
        return (int) (file.lastModified() - file2.lastModified());
    }
}
