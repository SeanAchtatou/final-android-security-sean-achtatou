package com.baidu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import com.tencent.lbsapi.core.e;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

final class w {
    private static final Proxy a = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80));
    private static final Proxy b = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80));

    w() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045 A[SYNTHETIC, Splitter:B:25:0x0045] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(android.content.Context r6, java.net.URL r7) {
        /*
            r4 = 0
            java.lang.String r0 = "getBitmap"
            java.lang.String r1 = r7.toString()
            com.baidu.bk.b(r0, r1)
            java.net.HttpURLConnection r0 = b(r6, r7)     // Catch:{ IOException -> 0x0027, all -> 0x0041 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x0027, all -> 0x0041 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x0059, all -> 0x0052 }
            if (r0 == 0) goto L_0x001b
            r0.close()     // Catch:{ IOException -> 0x001d }
        L_0x001b:
            r0 = r1
        L_0x001c:
            return r0
        L_0x001d:
            r0 = move-exception
            java.lang.String r2 = "AdUtil.readFromJar"
            java.lang.String r3 = ""
            com.baidu.bk.a(r2, r3, r0)
            r0 = r1
            goto L_0x001c
        L_0x0027:
            r0 = move-exception
            r1 = r4
        L_0x0029:
            java.lang.String r2 = "AdUtil.readFromJar"
            java.lang.String r3 = ""
            com.baidu.bk.a(r2, r3, r0)     // Catch:{ all -> 0x0057 }
            if (r1 == 0) goto L_0x0035
            r1.close()     // Catch:{ IOException -> 0x0037 }
        L_0x0035:
            r0 = r4
            goto L_0x001c
        L_0x0037:
            r0 = move-exception
            java.lang.String r1 = "AdUtil.readFromJar"
            java.lang.String r2 = ""
            com.baidu.bk.a(r1, r2, r0)
            r0 = r4
            goto L_0x001c
        L_0x0041:
            r0 = move-exception
            r1 = r4
        L_0x0043:
            if (r1 == 0) goto L_0x0048
            r1.close()     // Catch:{ IOException -> 0x0049 }
        L_0x0048:
            throw r0
        L_0x0049:
            r1 = move-exception
            java.lang.String r2 = "AdUtil.readFromJar"
            java.lang.String r3 = ""
            com.baidu.bk.a(r2, r3, r1)
            goto L_0x0048
        L_0x0052:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0043
        L_0x0057:
            r0 = move-exception
            goto L_0x0043
        L_0x0059:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.w.a(android.content.Context, java.net.URL):android.graphics.Bitmap");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0055 A[SYNTHETIC, Splitter:B:25:0x0055] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(java.net.URL r6, boolean r7) {
        /*
            r4 = 0
            java.lang.String r0 = "AdUtil.readFromJar"
            java.lang.String r1 = r6.toString()
            com.baidu.bk.b(r0, r1)
            java.net.URLConnection r6 = r6.openConnection()     // Catch:{ IOException -> 0x0037, all -> 0x0051 }
            java.net.JarURLConnection r6 = (java.net.JarURLConnection) r6     // Catch:{ IOException -> 0x0037, all -> 0x0051 }
            r0 = 0
            r6.setConnectTimeout(r0)     // Catch:{ IOException -> 0x0037, all -> 0x0051 }
            r0 = 0
            r6.setReadTimeout(r0)     // Catch:{ IOException -> 0x0037, all -> 0x0051 }
            r6.setUseCaches(r7)     // Catch:{ IOException -> 0x0037, all -> 0x0051 }
            r6.connect()     // Catch:{ IOException -> 0x0037, all -> 0x0051 }
            java.io.InputStream r0 = r6.getInputStream()     // Catch:{ IOException -> 0x0037, all -> 0x0051 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x0069, all -> 0x0062 }
            if (r0 == 0) goto L_0x002b
            r0.close()     // Catch:{ IOException -> 0x002d }
        L_0x002b:
            r0 = r1
        L_0x002c:
            return r0
        L_0x002d:
            r0 = move-exception
            java.lang.String r2 = "AdUtil.readFromJar"
            java.lang.String r3 = ""
            com.baidu.bk.a(r2, r3, r0)
            r0 = r1
            goto L_0x002c
        L_0x0037:
            r0 = move-exception
            r1 = r4
        L_0x0039:
            java.lang.String r2 = "AdUtil.readFromJar"
            java.lang.String r3 = ""
            com.baidu.bk.a(r2, r3, r0)     // Catch:{ all -> 0x0067 }
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0045:
            r0 = r4
            goto L_0x002c
        L_0x0047:
            r0 = move-exception
            java.lang.String r1 = "AdUtil.readFromJar"
            java.lang.String r2 = ""
            com.baidu.bk.a(r1, r2, r0)
            r0 = r4
            goto L_0x002c
        L_0x0051:
            r0 = move-exception
            r1 = r4
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0058:
            throw r0
        L_0x0059:
            r1 = move-exception
            java.lang.String r2 = "AdUtil.readFromJar"
            java.lang.String r3 = ""
            com.baidu.bk.a(r2, r3, r1)
            goto L_0x0058
        L_0x0062:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0053
        L_0x0067:
            r0 = move-exception
            goto L_0x0053
        L_0x0069:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.w.a(java.net.URL, boolean):android.graphics.Bitmap");
    }

    public static String a(Context context, String str) {
        bk.b("AdUtil.read", str);
        try {
            byte[] i = i(context, str);
            return i != null ? new String(i, e.e) : "";
        } catch (Exception e) {
            Log.w("Mobads SDK", "AdUtil.read", e);
            return "";
        }
    }

    public static String a(Context context, String str, int i, int i2) {
        if (str.startsWith("file:///")) {
            return k(context, str);
        }
        StringBuilder sb = new StringBuilder();
        HttpURLConnection b2 = b(context, str, i, i2);
        b2.connect();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(b2.getInputStream()));
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                break;
            }
            sb.append(readLine);
        }
        bk.b("AdUtil.get", String.format("Header: %d/%d %d %s \t%s", Integer.valueOf(sb.length()), Integer.valueOf(b2.getContentLength()), Integer.valueOf(b2.getResponseCode()), b2.getResponseMessage(), sb));
        if (bufferedReader != null) {
            bufferedReader.close();
        }
        if (b2.getContentLength() < 0) {
            sb = new StringBuilder("{error}");
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046 A[SYNTHETIC, Splitter:B:12:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006c A[SYNTHETIC, Splitter:B:26:0x006c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.net.URL r6) {
        /*
            java.lang.String r0 = "AdUtil.readFromJar"
            java.lang.String r1 = r6.toString()
            com.baidu.bk.b(r0, r1)
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.net.URLConnection r6 = r6.openConnection()     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
            java.net.JarURLConnection r6 = (java.net.JarURLConnection) r6     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
            r2 = 0
            r6.setConnectTimeout(r2)     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
            r2 = 0
            r6.setReadTimeout(r2)     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
            r2 = 0
            r6.setUseCaches(r2)     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
            r6.connect()     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
            java.io.InputStream r4 = r6.getInputStream()     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x007c, all -> 0x0066 }
        L_0x0032:
            java.lang.String r0 = r2.readLine()     // Catch:{ IOException -> 0x003c }
            if (r0 == 0) goto L_0x004e
            r1.append(r0)     // Catch:{ IOException -> 0x003c }
            goto L_0x0032
        L_0x003c:
            r0 = move-exception
        L_0x003d:
            java.lang.String r3 = "AdUtil.readFromJar"
            java.lang.String r4 = ""
            com.baidu.bk.a(r3, r4, r0)     // Catch:{ all -> 0x0079 }
            if (r2 == 0) goto L_0x0049
            r2.close()     // Catch:{ IOException -> 0x005d }
        L_0x0049:
            java.lang.String r0 = r1.toString()
            return r0
        L_0x004e:
            if (r2 == 0) goto L_0x0049
            r2.close()     // Catch:{ IOException -> 0x0054 }
            goto L_0x0049
        L_0x0054:
            r0 = move-exception
            java.lang.String r2 = "AdUtil.readFromJar"
            java.lang.String r3 = ""
            com.baidu.bk.a(r2, r3, r0)
            goto L_0x0049
        L_0x005d:
            r0 = move-exception
            java.lang.String r2 = "AdUtil.readFromJar"
            java.lang.String r3 = ""
            com.baidu.bk.a(r2, r3, r0)
            goto L_0x0049
        L_0x0066:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x006a:
            if (r1 == 0) goto L_0x006f
            r1.close()     // Catch:{ IOException -> 0x0070 }
        L_0x006f:
            throw r0
        L_0x0070:
            r1 = move-exception
            java.lang.String r2 = "AdUtil.readFromJar"
            java.lang.String r3 = ""
            com.baidu.bk.a(r2, r3, r1)
            goto L_0x006f
        L_0x0079:
            r0 = move-exception
            r1 = r2
            goto L_0x006a
        L_0x007c:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.w.a(java.net.URL):java.lang.String");
    }

    public static String a(boolean z, Context context, String str) {
        return z ? c(str) : a(context, str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0065 A[SYNTHETIC, Splitter:B:35:0x0065] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r6, java.lang.String r7, java.lang.String r8, boolean r9) {
        /*
            r4 = 0
            r0 = 0
            if (r9 == 0) goto L_0x001c
            r1 = 32768(0x8000, float:4.5918E-41)
        L_0x0007:
            java.io.FileOutputStream r0 = r6.openFileOutput(r7, r1)     // Catch:{ Exception -> 0x0079, all -> 0x005f }
            if (r0 == 0) goto L_0x001e
            java.lang.String r1 = "utf-8"
            byte[] r1 = r8.getBytes(r1)     // Catch:{ Exception -> 0x003a, all -> 0x0072 }
            r0.write(r1)     // Catch:{ Exception -> 0x003a, all -> 0x0072 }
        L_0x0016:
            if (r0 == 0) goto L_0x001b
            r0.close()     // Catch:{ Exception -> 0x0056 }
        L_0x001b:
            return
        L_0x001c:
            r1 = r4
            goto L_0x0007
        L_0x001e:
            java.lang.String r1 = "Mobads SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003a, all -> 0x0072 }
            r2.<init>()     // Catch:{ Exception -> 0x003a, all -> 0x0072 }
            java.lang.String r3 = "AdUtil.write fout is null:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x003a, all -> 0x0072 }
            if (r0 != 0) goto L_0x0054
            r3 = 1
        L_0x002e:
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x003a, all -> 0x0072 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x003a, all -> 0x0072 }
            android.util.Log.w(r1, r2)     // Catch:{ Exception -> 0x003a, all -> 0x0072 }
            goto L_0x0016
        L_0x003a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x003e:
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.write"
            android.util.Log.w(r2, r3, r0)     // Catch:{ all -> 0x0077 }
            if (r1 == 0) goto L_0x001b
            r1.close()     // Catch:{ Exception -> 0x004b }
            goto L_0x001b
        L_0x004b:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r2 = "AdUtil.write"
            android.util.Log.w(r1, r2, r0)
            goto L_0x001b
        L_0x0054:
            r3 = r4
            goto L_0x002e
        L_0x0056:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r2 = "AdUtil.write"
            android.util.Log.w(r1, r2, r0)
            goto L_0x001b
        L_0x005f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ Exception -> 0x0069 }
        L_0x0068:
            throw r0
        L_0x0069:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.write"
            android.util.Log.w(r2, r3, r1)
            goto L_0x0068
        L_0x0072:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0063
        L_0x0077:
            r0 = move-exception
            goto L_0x0063
        L_0x0079:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.w.a(android.content.Context, java.lang.String, java.lang.String, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0068 A[SYNTHETIC, Splitter:B:22:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0082 A[SYNTHETIC, Splitter:B:32:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0096 A[SYNTHETIC, Splitter:B:39:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r5, java.lang.String r6, boolean r7) {
        /*
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            r0 = 0
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            java.io.File r2 = new java.io.File     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            java.lang.String r3 = java.io.File.separator     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            java.lang.String r1 = r1.toString()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            boolean r1 = r2.exists()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            if (r1 != 0) goto L_0x003e
            java.io.File r1 = r2.getParentFile()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            r1.mkdirs()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            r2.createNewFile()     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
        L_0x003e:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            r1.<init>(r2, r7)     // Catch:{ FileNotFoundException -> 0x005b, IOException -> 0x0075, all -> 0x0090 }
            java.lang.String r0 = "utf-8"
            byte[] r0 = r6.getBytes(r0)     // Catch:{ FileNotFoundException -> 0x00a7, IOException -> 0x00a5 }
            r1.write(r0)     // Catch:{ FileNotFoundException -> 0x00a7, IOException -> 0x00a5 }
            if (r1 == 0) goto L_0x000c
            r1.close()     // Catch:{ IOException -> 0x0052 }
            goto L_0x000c
        L_0x0052:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r2 = "AdUtil.writeExt"
            android.util.Log.w(r1, r2, r0)
            goto L_0x000c
        L_0x005b:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x005f:
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.writeExt"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x00a3 }
            if (r1 == 0) goto L_0x000c
            r1.close()     // Catch:{ IOException -> 0x006c }
            goto L_0x000c
        L_0x006c:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r2 = "AdUtil.writeExt"
            android.util.Log.w(r1, r2, r0)
            goto L_0x000c
        L_0x0075:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0079:
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.writeExt"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x00a3 }
            if (r1 == 0) goto L_0x000c
            r1.close()     // Catch:{ IOException -> 0x0086 }
            goto L_0x000c
        L_0x0086:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r2 = "AdUtil.writeExt"
            android.util.Log.w(r1, r2, r0)
            goto L_0x000c
        L_0x0090:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0094:
            if (r1 == 0) goto L_0x0099
            r1.close()     // Catch:{ IOException -> 0x009a }
        L_0x0099:
            throw r0
        L_0x009a:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.writeExt"
            android.util.Log.w(r2, r3, r1)
            goto L_0x0099
        L_0x00a3:
            r0 = move-exception
            goto L_0x0094
        L_0x00a5:
            r0 = move-exception
            goto L_0x0079
        L_0x00a7:
            r0 = move-exception
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.w.a(java.lang.String, java.lang.String, boolean):void");
    }

    public static void a(boolean z, Context context, String str, String str2, boolean z2) {
        if (z) {
            a(str, str2, z2);
        } else {
            a(context, str, str2, z2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.w.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.baidu.w.a(android.content.Context, java.net.URL):android.graphics.Bitmap
      com.baidu.w.a(java.net.URL, boolean):android.graphics.Bitmap
      com.baidu.w.a(android.content.Context, java.lang.String):java.lang.String
      com.baidu.w.a(java.lang.String, boolean):boolean */
    public static boolean a(String str) {
        return a(str, false);
    }

    public static boolean a(String str, boolean z) {
        bk.b("AdUtil.existsExt", str);
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        }
        return new File(z ? str : Environment.getExternalStorageDirectory() + File.separator + str).exists();
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0091 A[SYNTHETIC, Splitter:B:30:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0096 A[Catch:{ IOException -> 0x00bf }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00cf A[SYNTHETIC, Splitter:B:52:0x00cf] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00d4 A[Catch:{ IOException -> 0x00d8 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(boolean r9, android.content.Context r10, java.net.URL r11, java.lang.String r12) {
        /*
            r7 = 0
            r6 = 1
            r5 = 0
            java.lang.String r0 = "AdUtil.save"
            java.lang.String r1 = "[%s] %s"
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r5] = r12
            java.lang.String r3 = r11.toString()
            r2[r6] = r3
            java.lang.String r1 = java.lang.String.format(r1, r2)
            com.baidu.bk.b(r0, r1)
            r1 = 0
            java.net.URLConnection r0 = r11.openConnection()     // Catch:{ IOException -> 0x00eb, all -> 0x00ca }
            java.net.JarURLConnection r0 = (java.net.JarURLConnection) r0     // Catch:{ IOException -> 0x00eb, all -> 0x00ca }
            r0.connect()     // Catch:{ IOException -> 0x00eb, all -> 0x00ca }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00eb, all -> 0x00ca }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x00eb, all -> 0x00ca }
            r2.<init>(r0)     // Catch:{ IOException -> 0x00eb, all -> 0x00ca }
            if (r9 == 0) goto L_0x009b
            java.lang.String r0 = android.os.Environment.getExternalStorageState()     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            java.lang.String r3 = "mounted"
            boolean r0 = r3.equals(r0)     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            if (r0 != 0) goto L_0x004f
            if (r2 == 0) goto L_0x003f
            r2.close()     // Catch:{ IOException -> 0x0046 }
        L_0x003f:
            if (r7 == 0) goto L_0x0044
            r1.close()     // Catch:{ IOException -> 0x0046 }
        L_0x0044:
            r0 = r5
        L_0x0045:
            return r0
        L_0x0046:
            r0 = move-exception
            java.lang.String r1 = "AdUtil.saveJar"
            java.lang.String r2 = ""
            com.baidu.bk.b(r1, r2, r0)
            goto L_0x0044
        L_0x004f:
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            r4.<init>()     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            java.lang.String r4 = java.io.File.separator     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            java.lang.StringBuilder r0 = r0.append(r12)     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            r3.<init>(r0)     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            r0 = r1
        L_0x0075:
            r1 = 5120(0x1400, float:7.175E-42)
            byte[] r1 = new byte[r1]     // Catch:{ IOException -> 0x0084, all -> 0x00e4 }
        L_0x0079:
            int r3 = r2.read(r1)     // Catch:{ IOException -> 0x0084, all -> 0x00e4 }
            if (r3 <= 0) goto L_0x00a9
            r4 = 0
            r0.write(r1, r4, r3)     // Catch:{ IOException -> 0x0084, all -> 0x00e4 }
            goto L_0x0079
        L_0x0084:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0088:
            java.lang.String r3 = "AdUtil.saveJar"
            java.lang.String r4 = ""
            com.baidu.bk.b(r3, r4, r0)     // Catch:{ all -> 0x00e9 }
            if (r2 == 0) goto L_0x0094
            r2.close()     // Catch:{ IOException -> 0x00bf }
        L_0x0094:
            if (r1 == 0) goto L_0x0099
            r1.close()     // Catch:{ IOException -> 0x00bf }
        L_0x0099:
            r0 = r6
            goto L_0x0045
        L_0x009b:
            java.io.BufferedOutputStream r0 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            r1 = 0
            java.io.FileOutputStream r1 = r10.openFileOutput(r12, r1)     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            r0.<init>(r1)     // Catch:{ IOException -> 0x00a6, all -> 0x00e1 }
            goto L_0x0075
        L_0x00a6:
            r0 = move-exception
            r1 = r7
            goto L_0x0088
        L_0x00a9:
            if (r2 == 0) goto L_0x00ae
            r2.close()     // Catch:{ IOException -> 0x00b5 }
        L_0x00ae:
            if (r0 == 0) goto L_0x00b3
            r0.close()     // Catch:{ IOException -> 0x00b5 }
        L_0x00b3:
            r0 = r6
            goto L_0x0045
        L_0x00b5:
            r0 = move-exception
            java.lang.String r1 = "AdUtil.saveJar"
            java.lang.String r2 = ""
            com.baidu.bk.b(r1, r2, r0)
            r0 = r5
            goto L_0x0045
        L_0x00bf:
            r0 = move-exception
            java.lang.String r1 = "AdUtil.saveJar"
            java.lang.String r2 = ""
            com.baidu.bk.b(r1, r2, r0)
            r0 = r5
            goto L_0x0045
        L_0x00ca:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x00cd:
            if (r2 == 0) goto L_0x00d2
            r2.close()     // Catch:{ IOException -> 0x00d8 }
        L_0x00d2:
            if (r1 == 0) goto L_0x00d7
            r1.close()     // Catch:{ IOException -> 0x00d8 }
        L_0x00d7:
            throw r0
        L_0x00d8:
            r1 = move-exception
            java.lang.String r2 = "AdUtil.saveJar"
            java.lang.String r3 = ""
            com.baidu.bk.b(r2, r3, r1)
            goto L_0x00d7
        L_0x00e1:
            r0 = move-exception
            r1 = r7
            goto L_0x00cd
        L_0x00e4:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00cd
        L_0x00e9:
            r0 = move-exception
            goto L_0x00cd
        L_0x00eb:
            r0 = move-exception
            r1 = r7
            r2 = r7
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.w.a(boolean, android.content.Context, java.net.URL, java.lang.String):boolean");
    }

    private static HttpURLConnection b(Context context, String str, int i, int i2) {
        HttpURLConnection httpURLConnection;
        URL url = new URL(str);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
        NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(1);
        if (networkInfo2 != null && networkInfo2.isAvailable()) {
            bk.b("", "WIFI is available");
            httpURLConnection = (HttpURLConnection) url.openConnection();
        } else if (networkInfo == null || !networkInfo.isAvailable()) {
            httpURLConnection = null;
        } else {
            String extraInfo = networkInfo.getExtraInfo();
            String lowerCase = extraInfo != null ? extraInfo.toLowerCase() : "";
            bk.b("current APN", lowerCase);
            httpURLConnection = (lowerCase.startsWith("cmwap") || lowerCase.startsWith("uniwap") || lowerCase.startsWith("3gwap")) ? (HttpURLConnection) url.openConnection(a) : lowerCase.startsWith("ctwap") ? (HttpURLConnection) url.openConnection(b) : (HttpURLConnection) url.openConnection();
        }
        httpURLConnection.setConnectTimeout(i);
        httpURLConnection.setReadTimeout(i2);
        return httpURLConnection;
    }

    private static HttpURLConnection b(Context context, URL url) {
        return j(context, url.toString());
    }

    public static boolean b(Context context, String str) {
        bk.b("AdUtil.delete", str);
        return context.deleteFile(str);
    }

    public static boolean b(String str) {
        bk.b("AdUtil.deleteExt", str);
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        }
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + str);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    public static boolean b(boolean z, Context context, String str) {
        return z ? b(str) : b(context, str);
    }

    public static boolean b(boolean z, Context context, URL url, String str) {
        BufferedOutputStream bufferedOutputStream;
        bk.b("AdUtil.save", String.format("[%s] %s", str, url.toString()));
        HttpURLConnection b2 = b(context, url);
        b2.connect();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(b2.getInputStream());
        if (!z) {
            bufferedOutputStream = new BufferedOutputStream(context.openFileOutput(str, 0));
        } else if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        } else {
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(Environment.getExternalStorageDirectory() + File.separator + str));
        }
        byte[] bArr = new byte[5120];
        while (true) {
            int read = bufferedInputStream.read(bArr);
            if (read <= 0) {
                break;
            }
            bufferedOutputStream.write(bArr, 0, read);
        }
        if (bufferedInputStream != null) {
            bufferedInputStream.close();
        }
        if (bufferedOutputStream != null) {
            bufferedOutputStream.close();
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0075 A[SYNTHETIC, Splitter:B:23:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x008c A[SYNTHETIC, Splitter:B:32:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x009d A[SYNTHETIC, Splitter:B:39:0x009d] */
    /* JADX WARNING: Removed duplicated region for block: B:55:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:57:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String c(java.lang.String r5) {
        /*
            java.lang.String r0 = "AdUtil.readExt"
            com.baidu.bk.b(r0, r5)
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x001c
            java.lang.String r1 = "mounted_ro"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x001c
            java.lang.String r0 = ""
        L_0x001b:
            return r0
        L_0x001c:
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = java.io.File.separator
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            java.lang.String r0 = ""
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x001b
            r2 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x006b, IOException -> 0x0082, all -> 0x0099 }
            r3.<init>(r1)     // Catch:{ FileNotFoundException -> 0x006b, IOException -> 0x0082, all -> 0x0099 }
            int r1 = r3.available()     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b0, all -> 0x00aa }
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b0, all -> 0x00aa }
            r3.read(r1)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b0, all -> 0x00aa }
            java.lang.String r2 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b0, all -> 0x00aa }
            java.lang.String r4 = "utf-8"
            r2.<init>(r1, r4)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b0, all -> 0x00aa }
            if (r3 == 0) goto L_0x005f
            r3.close()     // Catch:{ IOException -> 0x0061 }
        L_0x005f:
            r0 = r2
            goto L_0x001b
        L_0x0061:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readExt"
            android.util.Log.w(r1, r3, r0)
            r0 = r2
            goto L_0x001b
        L_0x006b:
            r1 = move-exception
        L_0x006c:
            java.lang.String r3 = "Mobads SDK"
            java.lang.String r4 = "AdUtil.readExt"
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x00ad }
            if (r2 == 0) goto L_0x001b
            r2.close()     // Catch:{ IOException -> 0x0079 }
            goto L_0x001b
        L_0x0079:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readExt"
            android.util.Log.w(r2, r3, r1)
            goto L_0x001b
        L_0x0082:
            r1 = move-exception
        L_0x0083:
            java.lang.String r3 = "Mobads SDK"
            java.lang.String r4 = "AdUtil.readExt"
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x00ad }
            if (r2 == 0) goto L_0x001b
            r2.close()     // Catch:{ IOException -> 0x0090 }
            goto L_0x001b
        L_0x0090:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readExt"
            android.util.Log.w(r2, r3, r1)
            goto L_0x001b
        L_0x0099:
            r0 = move-exception
            r1 = r2
        L_0x009b:
            if (r1 == 0) goto L_0x00a0
            r1.close()     // Catch:{ IOException -> 0x00a1 }
        L_0x00a0:
            throw r0
        L_0x00a1:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readExt"
            android.util.Log.w(r2, r3, r1)
            goto L_0x00a0
        L_0x00aa:
            r0 = move-exception
            r1 = r3
            goto L_0x009b
        L_0x00ad:
            r0 = move-exception
            r1 = r2
            goto L_0x009b
        L_0x00b0:
            r1 = move-exception
            r2 = r3
            goto L_0x0083
        L_0x00b3:
            r1 = move-exception
            r2 = r3
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.w.c(java.lang.String):java.lang.String");
    }

    public static boolean c(Context context, String str) {
        boolean exists = context.getFileStreamPath(str).exists();
        bk.b("AdUtil.exists", exists + " " + str);
        return exists;
    }

    public static boolean c(boolean z, Context context, String str) {
        return z ? a(str) : c(context, str);
    }

    public static Bitmap d(boolean z, Context context, String str) {
        Bitmap bitmap;
        Exception exc;
        Bitmap bitmap2;
        IOException iOException;
        bk.b("AdUtil.readFromFile", str);
        try {
            FileInputStream f = f(z, context, str);
            Bitmap decodeStream = BitmapFactory.decodeStream(f);
            if (f == null) {
                return decodeStream;
            }
            try {
                f.close();
                return decodeStream;
            } catch (IOException e) {
                IOException iOException2 = e;
                bitmap2 = decodeStream;
                iOException = iOException2;
            } catch (Exception e2) {
                Exception exc2 = e2;
                bitmap = decodeStream;
                exc = exc2;
                bk.b("AdUtil.readFromFile", exc);
                return bitmap;
            }
        } catch (IOException e3) {
            IOException iOException3 = e3;
            bitmap2 = null;
            iOException = iOException3;
        } catch (Exception e4) {
            Exception exc3 = e4;
            bitmap = null;
            exc = exc3;
            bk.b("AdUtil.readFromFile", exc);
            return bitmap;
        }
        bk.b("AdUtil.readFromFile", iOException);
        return bitmap2;
    }

    public static String d(Context context, String str) {
        return a(context, str, 40000, 60000);
    }

    public static void d(String str) {
        bk.c("BaiduMobAds SDK", str);
        throw new SecurityException(str);
    }

    public static String e(String str) {
        return "__sdk_" + f(str);
    }

    public static void e(Context context, String str) {
        context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str)));
    }

    public static byte[] e(boolean z, Context context, String str) {
        bk.b("AdUtil.getISFromFile", str);
        try {
            FileInputStream f = f(z, context, str);
            byte[] bArr = new byte[f.available()];
            f.read(bArr);
            return bArr;
        } catch (IOException e) {
            bk.b("AdUtil.getISFromFile", "", e);
            return null;
        }
    }

    private static FileInputStream f(boolean z, Context context, String str) {
        bk.b("AdUtil.getISFromFile", str);
        if (!z) {
            return context.openFileInput(str);
        }
        try {
            String externalStorageState = Environment.getExternalStorageState();
            if (!"mounted".equals(externalStorageState) && !"mounted_ro".equals(externalStorageState)) {
                return null;
            }
            return new FileInputStream(Environment.getExternalStorageDirectory() + File.separator + str);
        } catch (IOException e) {
            bk.b("AdUtil.getISFromFile", "", e);
            return null;
        }
    }

    public static String f(String str) {
        byte[] bytes = str.getBytes();
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bytes);
            byte[] digest = instance.digest();
            char[] cArr2 = new char[32];
            int i = 0;
            for (int i2 = 0; i2 < 16; i2++) {
                byte b2 = digest[i2];
                int i3 = i + 1;
                cArr2[i] = cArr[(b2 >>> 4) & 15];
                i = i3 + 1;
                cArr2[i3] = cArr[b2 & 15];
            }
            return new String(cArr2);
        } catch (NoSuchAlgorithmException e) {
            bk.b("AdUtil.getMD5", "", e);
            return null;
        }
    }

    public static void f(Context context, String str) {
        bk.b("AdUtil.browser", str);
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        intent.addFlags(268435456);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            bk.b("BaiduMobAds SDK", "[failed] " + str, e);
        }
    }

    public static void g(Context context, String str) {
        if (!h(context, str)) {
            d("Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"" + str + "\" />");
        }
    }

    public static boolean h(Context context, String str) {
        boolean z = context.checkCallingOrSelfPermission(str) != -1;
        bk.b("hasPermission ", z + " | " + str);
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0031, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0032, code lost:
        android.util.Log.e("Mobads SDK", "AdUtil.readBinary", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x004b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x004c, code lost:
        android.util.Log.e("Mobads SDK", "AdUtil.readBinary", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x005d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x005e, code lost:
        android.util.Log.e("Mobads SDK", "AdUtil.readBinary", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0066, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0067, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x006d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x006e, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0078, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0079, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002c A[SYNTHETIC, Splitter:B:20:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0046 A[SYNTHETIC, Splitter:B:31:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0059 A[SYNTHETIC, Splitter:B:39:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0066 A[ExcHandler: all (r1v16 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x0007] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] i(android.content.Context r6, java.lang.String r7) {
        /*
            r2 = 0
            java.io.FileInputStream r0 = r6.openFileInput(r7)     // Catch:{ FileNotFoundException -> 0x0021, IOException -> 0x003b, all -> 0x0055 }
            if (r0 == 0) goto L_0x0083
            int r1 = r0.available()     // Catch:{ FileNotFoundException -> 0x0078, IOException -> 0x006d, all -> 0x0066 }
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x0078, IOException -> 0x006d, all -> 0x0066 }
            r0.read(r1)     // Catch:{ FileNotFoundException -> 0x007d, IOException -> 0x0072, all -> 0x0066 }
        L_0x0010:
            if (r0 == 0) goto L_0x0015
            r0.close()     // Catch:{ IOException -> 0x0017 }
        L_0x0015:
            r0 = r1
        L_0x0016:
            return r0
        L_0x0017:
            r0 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readBinary"
            android.util.Log.e(r2, r3, r0)
            r0 = r1
            goto L_0x0016
        L_0x0021:
            r0 = move-exception
            r1 = r2
        L_0x0023:
            java.lang.String r3 = "Mobads SDK"
            java.lang.String r4 = "AdUtil.readBinary"
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x006b }
            if (r1 == 0) goto L_0x002f
            r1.close()     // Catch:{ IOException -> 0x0031 }
        L_0x002f:
            r0 = r2
            goto L_0x0016
        L_0x0031:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readBinary"
            android.util.Log.e(r1, r3, r0)
            r0 = r2
            goto L_0x0016
        L_0x003b:
            r0 = move-exception
            r1 = r2
        L_0x003d:
            java.lang.String r3 = "Mobads SDK"
            java.lang.String r4 = "AdUtil.readBinary"
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x006b }
            if (r1 == 0) goto L_0x0049
            r1.close()     // Catch:{ IOException -> 0x004b }
        L_0x0049:
            r0 = r2
            goto L_0x0016
        L_0x004b:
            r0 = move-exception
            java.lang.String r1 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readBinary"
            android.util.Log.e(r1, r3, r0)
            r0 = r2
            goto L_0x0016
        L_0x0055:
            r0 = move-exception
            r1 = r2
        L_0x0057:
            if (r1 == 0) goto L_0x005c
            r1.close()     // Catch:{ IOException -> 0x005d }
        L_0x005c:
            throw r0
        L_0x005d:
            r1 = move-exception
            java.lang.String r2 = "Mobads SDK"
            java.lang.String r3 = "AdUtil.readBinary"
            android.util.Log.e(r2, r3, r1)
            goto L_0x005c
        L_0x0066:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0057
        L_0x006b:
            r0 = move-exception
            goto L_0x0057
        L_0x006d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x003d
        L_0x0072:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x003d
        L_0x0078:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0023
        L_0x007d:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x0023
        L_0x0083:
            r1 = r2
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.w.i(android.content.Context, java.lang.String):byte[]");
    }

    private static HttpURLConnection j(Context context, String str) {
        return b(context, str, 40000, 60000);
    }

    private static String k(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        int indexOf = str.indexOf(63);
        URLConnection openConnection = new URL(indexOf >= 0 ? str.substring(0, indexOf) : str).openConnection();
        openConnection.connect();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(openConnection.getInputStream()));
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                break;
            }
            sb.append(readLine);
        }
        if (bufferedReader != null) {
            bufferedReader.close();
        }
        return sb.toString();
    }
}
