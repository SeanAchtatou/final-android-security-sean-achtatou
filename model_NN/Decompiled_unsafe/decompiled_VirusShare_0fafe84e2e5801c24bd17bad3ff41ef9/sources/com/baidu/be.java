package com.baidu;

import android.graphics.Bitmap;
import android.os.SystemClock;

class be extends Thread {
    final /* synthetic */ GifAnimView a;

    private be(GifAnimView gifAnimView) {
        this.a = gifAnimView;
    }

    /* synthetic */ be(GifAnimView gifAnimView, bc bcVar) {
        this(gifAnimView);
    }

    public void run() {
        if (this.a.a != null) {
            while (this.a.c) {
                if (!this.a.d) {
                    bb d = this.a.a.d();
                    Bitmap unused = this.a.b = d.a;
                    long j = (long) d.b;
                    if (this.a.l != null) {
                        this.a.l.sendMessage(this.a.l.obtainMessage());
                        SystemClock.sleep(j);
                    } else {
                        return;
                    }
                } else {
                    SystemClock.sleep(500);
                }
            }
        }
    }
}
