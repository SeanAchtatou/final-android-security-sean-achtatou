package com.baidu;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

class e implements FilenameFilter {
    final /* synthetic */ c a;

    e(c cVar) {
        this.a = cVar;
    }

    public boolean accept(File file, String str) {
        if (!str.startsWith("__sdk_")) {
            return false;
        }
        try {
            ArrayList<JSONObject> arrayList = new ArrayList<>();
            for (t tVar : t.a()) {
                arrayList.add(this.a.k.getJSONObject(tVar.toString()));
            }
            for (t tVar2 : t.a()) {
                arrayList.add(this.a.j.getJSONObject(tVar2.toString()));
            }
            for (JSONObject jSONObject : arrayList) {
                Iterator<String> keys = jSONObject.keys();
                while (true) {
                    if (keys.hasNext()) {
                        if (str.equals(w.e(jSONObject.getJSONObject(keys.next().toString()).getJSONObject("content").getString("w_picurl")))) {
                            return false;
                        }
                    }
                }
            }
        } catch (JSONException e) {
        }
        return true;
    }
}
