package com.android.mmreader346;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class fileview extends ListActivity {
    private appcell appCell;
    private List<String> items = null;
    private TextView mPath;
    private List<String> paths = null;
    private String rootPath;

    public void onCreate(Bundle icicle) {
        requestWindowFeature(1);
        super.onCreate(icicle);
        setContentView((int) R.layout.fileselect);
        this.appCell = (appcell) getApplication();
        this.mPath = (TextView) findViewById(R.id.mPath);
        ((Button) findViewById(R.id.buttonConfirm)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                fileview.this.finish();
            }
        });
        ((Button) findViewById(R.id.buttonCancle)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                fileview.this.finish();
            }
        });
        if (Environment.getExternalStorageDirectory() == null) {
            this.rootPath = "/";
        } else {
            this.rootPath = "/sdcard";
        }
        getFileDir(this.rootPath);
    }

    private void getFileDir(String filePath) {
        File f = new File(filePath);
        File[] files = f.listFiles();
        if (files != null) {
            this.mPath.setText(filePath);
            this.items = new ArrayList();
            this.paths = new ArrayList();
            if (!filePath.equals(this.rootPath)) {
                this.items.add("b1");
                this.paths.add(this.rootPath);
                this.items.add("b2");
                this.paths.add(f.getParent());
            }
            for (File file : files) {
                if (file != null && file.isDirectory()) {
                    this.items.add(file.getName());
                    this.paths.add(file.getPath());
                } else if (file != null) {
                    String fName = file.getName();
                    String ext = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
                    if (ext.equals("m4a") || ext.equals("mp3") || ext.equals("mid") || ext.equals("xmf") || ext.equals("ogg") || ext.equals("wav")) {
                        this.items.add(file.getName());
                        this.paths.add(file.getPath());
                    }
                }
            }
            setListAdapter(new MyAdapter(this, this.items, this.paths));
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        File file = new File(this.paths.get(position));
        if (file.isDirectory()) {
            getFileDir(this.paths.get(position));
            return;
        }
        String fName = file.getName();
        String fPath = file.getPath();
        String ext = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        if (ext.equals("m4a") || ext.equals("mp3") || ext.equals("mid") || ext.equals("xmf") || ext.equals("ogg") || ext.equals("wav")) {
            String fullpath = fPath;
            this.appCell.setmusicfile(fullpath);
            this.appCell.writemusicfile(fullpath);
            Intent i = new Intent();
            Bundle b2 = new Bundle();
            b2.putString("filename", fName);
            i.putExtras(b2);
            setResult(-1, i);
            finish();
        }
    }
}
