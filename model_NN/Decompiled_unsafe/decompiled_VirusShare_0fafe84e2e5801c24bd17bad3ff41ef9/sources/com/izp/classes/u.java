package com.izp.classes;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;

public class u extends p {
    b c;
    m d = new m();

    public void a(int i) {
        this.c.a(this.c.N + 5, "can not get ad resource!");
    }

    public void a(b bVar, String str) {
        this.c = bVar;
        this.d.a(str, this);
    }

    public void a(InputStream inputStream) {
        Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
        if (decodeStream == null) {
            this.c.a(this.c.N + 4, "can not get ad resource!");
            return;
        }
        this.c.a.r = decodeStream;
        this.c.R.add("RECEIVE_AD_NOTIFICATION");
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
