package com.imocha;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import org.xml.sax.Attributes;

final class d extends b {
    d() {
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2) {
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Context context, am amVar) {
        if (context == null) {
            return false;
        }
        try {
            Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + this.a));
            intent.addFlags(268435456);
            context.startActivity(intent);
            return true;
        } catch (Exception e) {
            try {
                context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.a)));
                return true;
            } catch (Exception e2) {
                e.printStackTrace();
                e2.printStackTrace();
                return false;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(String str, Attributes attributes) {
    }
}
