package com.imocha;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FullScreenAdActivity extends Activity {
    private static ScheduledExecutorService p = Executors.newScheduledThreadPool(3);
    /* access modifiers changed from: private */
    public am a;
    private String b;
    /* access modifiers changed from: private */
    public FrameLayout c;
    private ImageView d;
    private VideoView e;
    private String f = "";
    /* access modifiers changed from: private */
    public boolean g = false;
    private int h;
    private RelativeLayout i;
    private RelativeLayout j;
    private a k;
    private boolean l = false;
    /* access modifiers changed from: private */
    public int m = -65536;
    /* access modifiers changed from: private */
    public ArrayList n = new ArrayList();
    /* access modifiers changed from: private */
    public Handler o = new Handler();

    private void a(Bundle bundle, String str) {
        double d2;
        try {
            d2 = Double.parseDouble(str == null ? "5.0" : str);
        } catch (Exception e2) {
            e2.printStackTrace();
            d2 = 5.0d;
        }
        if (bundle == null) {
            p.schedule(new aw(this), (long) (d2 * 1000.0d), TimeUnit.MILLISECONDS);
        }
    }

    public void finish() {
        setResult(AdView.RESULT_CODE);
        this.k = null;
        super.finish();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x01dc  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0207  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0258  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0262  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0307  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x032e  */
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r12) {
        /*
            r11 = this;
            super.onCreate(r12)
            r0 = 1
            r11.requestWindowFeature(r0)
            android.view.Window r0 = r11.getWindow()
            r1 = 1024(0x400, float:1.435E-42)
            r2 = 1024(0x400, float:1.435E-42)
            r0.setFlags(r1, r2)
            com.imocha.aq r0 = new com.imocha.aq
            r0.<init>(r11, r11)
            r11.c = r0
            android.widget.FrameLayout r0 = r11.c
            r1 = 4
            r0.setVisibility(r1)
            android.widget.RelativeLayout r0 = new android.widget.RelativeLayout
            r0.<init>(r11)
            r11.i = r0
            android.widget.FrameLayout r0 = r11.c
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0.setBackgroundColor(r1)
            android.widget.RelativeLayout r0 = r11.i
            android.view.ViewGroup$LayoutParams r1 = new android.view.ViewGroup$LayoutParams
            r2 = -1
            r3 = -1
            r1.<init>(r2, r3)
            r0.setLayoutParams(r1)
            android.widget.RelativeLayout r0 = new android.widget.RelativeLayout
            r0.<init>(r11)
            r11.j = r0
            android.widget.RelativeLayout r0 = r11.j
            r1 = -65536(0xffffffffffff0000, float:NaN)
            r0.setBackgroundColor(r1)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r1 = -2
            r2 = -2
            r0.<init>(r1, r2)
            r1 = 13
            r0.addRule(r1)
            android.widget.RelativeLayout r1 = r11.i
            android.widget.RelativeLayout r2 = r11.j
            r1.addView(r2, r0)
            android.content.Intent r0 = r11.getIntent()
            java.lang.String r1 = "SPACE_ID"
            java.lang.String r0 = r0.getStringExtra(r1)
            r11.b = r0
            java.util.HashMap r0 = com.imocha.IMochaAdView.f
            java.lang.String r1 = r11.b
            java.lang.Object r0 = r0.get(r1)
            com.imocha.am r0 = (com.imocha.am) r0
            r11.a = r0
            r0 = 0
            r11.g = r0
            com.imocha.am r0 = r11.a
            if (r0 != 0) goto L_0x0083
            r11.finish()
            r0 = 0
        L_0x007d:
            if (r0 != 0) goto L_0x00e3
            r11.finish()
        L_0x0082:
            return
        L_0x0083:
            com.imocha.am r0 = r11.a
            java.lang.String r0 = r0.c()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            com.imocha.af r2 = com.imocha.af.a()
            java.lang.String r2 = r2.g(r11)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = "/"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r11.f = r0
            com.imocha.al r0 = new com.imocha.al
            com.imocha.am r1 = r11.a
            r0.<init>(r11, r11, r1)
            r11.k = r0
            com.imocha.a r0 = r11.k
            com.imocha.an r1 = new com.imocha.an
            r1.<init>(r11)
            r0.b = r1
            com.imocha.a r0 = r11.k
            r1 = 1
            r0.clearCache(r1)
            com.imocha.a r0 = r11.k
            r0.clearHistory()
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r1 = -1
            r2 = -1
            r0.<init>(r1, r2)
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r2 = -1
            r3 = -1
            r1.<init>(r2, r3)
            r2 = 13
            r0.addRule(r2)
            android.widget.RelativeLayout r0 = r11.j
            com.imocha.a r2 = r11.k
            r0.addView(r2, r1)
            r0 = 1
            goto L_0x007d
        L_0x00e3:
            com.imocha.am r0 = r11.a
            java.util.Hashtable r0 = r0.g
            java.lang.String r1 = "type"
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "fullscreen"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x02ef
            com.imocha.am r0 = r11.a
            com.imocha.m r0 = r0.k
            com.imocha.m r1 = com.imocha.m.Html
            if (r0 != r1) goto L_0x026e
            java.lang.String r0 = r11.f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r1.<init>(r0)
            java.lang.String r0 = "/"
            java.lang.StringBuilder r0 = r1.append(r0)
            com.imocha.af r1 = com.imocha.af.a()
            java.lang.String r2 = r11.f
            com.imocha.am r3 = r11.a
            com.imocha.m r3 = r3.k
            java.lang.String r3 = r3.a()
            java.lang.String r1 = r1.d(r2, r3)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r11.f = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "file://"
            r0.<init>(r1)
            java.lang.String r1 = r11.f
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.imocha.a r1 = r11.k
            r1.loadUrl(r0)
            com.imocha.am r0 = r11.a
            com.imocha.k r0 = r0.a()
            com.imocha.a r1 = r11.k
            double r2 = r0.f
            r4 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r2 = r2 * r4
            int r2 = (int) r2
            r1.setInitialScale(r2)
            java.util.ArrayList r1 = r11.n
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "html scaling="
            r2.<init>(r3)
            double r3 = r0.f
            r5 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r3 = r3 * r5
            java.lang.StringBuilder r0 = r2.append(r3)
            java.lang.String r0 = r0.toString()
            r1.add(r0)
            java.util.ArrayList r0 = r11.n
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "webView.scaling="
            r1.<init>(r2)
            com.imocha.a r2 = r11.k
            float r2 = r2.getScale()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.add(r1)
            java.lang.String r0 = com.imocha.k.a
            r11.a(r12, r0)
        L_0x018b:
            com.imocha.af r0 = com.imocha.af.a()
            int r0 = r0.e(r11)
            com.imocha.af r1 = com.imocha.af.a()
            int r1 = r1.a(r11)
            android.widget.ImageButton r2 = new android.widget.ImageButton
            r2.<init>(r11)
            if (r1 != 0) goto L_0x0307
            r3 = 240(0xf0, float:3.36E-43)
            if (r0 != r3) goto L_0x02f4
            r0 = 20
            java.lang.String r3 = "close_20x20.png"
        L_0x01aa:
            android.graphics.drawable.Drawable r3 = com.imocha.af.c(r11, r3)
            r2.setBackgroundDrawable(r3)
            com.imocha.at r3 = new com.imocha.at
            r3.<init>(r11)
            r2.setOnClickListener(r3)
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams
            r3.<init>(r0, r0)
            r0 = 11
            r4 = -1
            r3.addRule(r0, r4)
            r0 = 10
            r4 = -1
            r3.addRule(r0, r4)
            r0 = 0
            r4 = 5
            r5 = 5
            r6 = 0
            r3.setMargins(r0, r4, r5, r6)
            android.widget.RelativeLayout r0 = r11.i
            r0.addView(r2, r3)
            com.imocha.am r0 = r11.a
            com.imocha.u r0 = r0.c
            if (r0 == 0) goto L_0x01e7
            com.imocha.am r0 = r11.a
            com.imocha.u r0 = r0.c
            java.lang.String r0 = r0.a
            java.lang.String r2 = "none"
            r0.equals(r2)
        L_0x01e7:
            com.imocha.am r0 = r11.a
            boolean r0 = r0.f
            if (r0 == 0) goto L_0x0243
            com.imocha.am r0 = r11.a
            com.imocha.g r0 = r0.e
            if (r0 != 0) goto L_0x0243
            com.imocha.ap r0 = new com.imocha.ap
            r0.<init>(r11, r11)
            r11.d = r0
            java.lang.String r0 = "full_480x800.png"
            com.imocha.af r2 = com.imocha.af.a()
            int r2 = r2.e(r11)
            double r2 = (double) r2
            if (r1 != 0) goto L_0x032e
            r4 = 4642648265865560064(0x406e000000000000, double:240.0)
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 != 0) goto L_0x030d
            java.lang.String r0 = "full_320x480.png"
            r1 = 4635048441494372352(0x4053000000000000, double:76.0)
            r3 = 4623507967449235456(0x402a000000000000, double:13.0)
            r7 = r3
            r4 = r0
            r9 = r1
            r2 = r9
            r0 = r7
        L_0x0218:
            android.graphics.drawable.Drawable r4 = com.imocha.af.c(r11, r4)
            android.widget.ImageView r5 = r11.d
            r5.setBackgroundDrawable(r4)
            android.widget.RelativeLayout$LayoutParams r4 = new android.widget.RelativeLayout$LayoutParams
            int r2 = (int) r2
            int r0 = (int) r0
            r4.<init>(r2, r0)
            r0 = 9
            r1 = -1
            r4.addRule(r0, r1)
            r0 = 12
            r1 = -1
            r4.addRule(r0, r1)
            android.widget.RelativeLayout r0 = new android.widget.RelativeLayout
            r0.<init>(r11)
            android.widget.ImageView r1 = r11.d
            r0.addView(r1, r4)
            android.widget.RelativeLayout r1 = r11.j
            r1.addView(r0)
        L_0x0243:
            android.widget.FrameLayout r0 = r11.c
            android.widget.RelativeLayout r1 = r11.i
            android.view.ViewGroup$LayoutParams r2 = new android.view.ViewGroup$LayoutParams
            r3 = -1
            r4 = -1
            r2.<init>(r3, r4)
            r0.addView(r1, r2)
            android.widget.FrameLayout r0 = r11.c
            r11.setContentView(r0)
            if (r12 != 0) goto L_0x025e
            r0 = 0
            com.imocha.am r1 = r11.a
            com.imocha.IMochaAdView.sendImpressAdLog(r0, r1)
        L_0x025e:
            boolean r0 = r11.l
            if (r0 == 0) goto L_0x0082
            com.imocha.as r0 = new com.imocha.as
            r0.<init>(r11, r11)
            android.widget.FrameLayout r1 = r11.c
            r1.addView(r0)
            goto L_0x0082
        L_0x026e:
            com.imocha.am r0 = r11.a
            com.imocha.m r0 = r0.k
            com.imocha.m r1 = com.imocha.m.Image
            if (r0 != r1) goto L_0x02ea
            com.imocha.am r0 = r11.a
            com.imocha.k r0 = r0.a()
            if (r0 == 0) goto L_0x018b
            java.lang.String r0 = r11.f
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFile(r0)
            if (r0 != 0) goto L_0x0290
            r11.finish()
        L_0x0289:
            java.lang.String r0 = com.imocha.k.a
            r11.a(r12, r0)
            goto L_0x018b
        L_0x0290:
            r0.recycle()
            r0 = 0
            java.lang.String r1 = "1.txt"
            java.io.InputStream r0 = com.imocha.af.b(r11, r1)     // Catch:{ Exception -> 0x02c5, all -> 0x02d8 }
            java.lang.String r1 = com.imocha.af.a(r0)     // Catch:{ Exception -> 0x0342, all -> 0x033b }
            r0.close()     // Catch:{ Exception -> 0x02e5 }
        L_0x02a1:
            java.lang.String r0 = "[IMAGE]"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "file:///"
            r2.<init>(r3)
            java.lang.String r3 = r11.f
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.String r2 = r1.replace(r0, r2)
            com.imocha.a r0 = r11.k
            java.lang.String r1 = "about:blank"
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)
            goto L_0x0289
        L_0x02c5:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x02c9:
            r11.finish()     // Catch:{ all -> 0x0340 }
            r0.printStackTrace()     // Catch:{ all -> 0x0340 }
            r1.close()     // Catch:{ Exception -> 0x02d3 }
            goto L_0x0289
        L_0x02d3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0289
        L_0x02d8:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x02dc:
            r1.close()     // Catch:{ Exception -> 0x02e0 }
        L_0x02df:
            throw r0
        L_0x02e0:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02df
        L_0x02e5:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x02a1
        L_0x02ea:
            r11.finish()
            goto L_0x018b
        L_0x02ef:
            r11.finish()
            goto L_0x018b
        L_0x02f4:
            r3 = 480(0x1e0, float:6.73E-43)
            if (r0 != r3) goto L_0x0301
            java.lang.String r0 = "close_25x25.png"
            r3 = 30
            r7 = r3
            r3 = r0
            r0 = r7
            goto L_0x01aa
        L_0x0301:
            r0 = 25
            java.lang.String r3 = "close_25x25.png"
            goto L_0x01aa
        L_0x0307:
            r0 = 50
            java.lang.String r3 = "close_50x50.png"
            goto L_0x01aa
        L_0x030d:
            r4 = 4644337115725824000(0x4074000000000000, double:320.0)
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 != 0) goto L_0x0323
            java.lang.String r0 = "full_320x480.png"
            r1 = 4636878028842991616(0x4059800000000000, double:102.0)
            r3 = 4625759767262920704(0x4032000000000000, double:18.0)
            r7 = r3
            r4 = r0
            r9 = r1
            r2 = r9
            r0 = r7
            goto L_0x0218
        L_0x0323:
            r1 = -4611686018427387904(0xc000000000000000, double:-2.0)
            r3 = -4611686018427387904(0xc000000000000000, double:-2.0)
            r7 = r3
            r4 = r0
            r9 = r1
            r2 = r9
            r0 = r7
            goto L_0x0218
        L_0x032e:
            java.lang.String r0 = "full_480x800.png"
            r1 = -4611686018427387904(0xc000000000000000, double:-2.0)
            r3 = -4611686018427387904(0xc000000000000000, double:-2.0)
            r7 = r3
            r4 = r0
            r9 = r1
            r2 = r9
            r0 = r7
            goto L_0x0218
        L_0x033b:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x02dc
        L_0x0340:
            r0 = move-exception
            goto L_0x02dc
        L_0x0342:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x02c9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imocha.FullScreenAdActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.e != null) {
            this.e.pause();
            this.h = this.e.getCurrentPosition();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.e != null && this.h >= 0) {
            this.e.seekTo(this.h);
            this.e.start();
            this.h = -1;
        }
        super.onResume();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        b bVar = this.a.b;
        if (bVar.getClass().getName().equals(ae.class.getName())) {
            Log.v("FullScreenAdActivity", bVar.getClass().getName().toString());
            return super.onTouchEvent(motionEvent);
        }
        if (motionEvent.getAction() == 0) {
            this.g = true;
            if (bVar != null) {
                bVar.a(this, this.a);
            }
            if (this.e != null) {
                this.e.stopPlayback();
            }
            IMochaAdView.sendClickAdLog(null, this.a);
        } else {
            super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public void onWindowFocusChanged(boolean z) {
        if (!this.g || !z) {
            super.onWindowFocusChanged(z);
        } else {
            finish();
        }
    }
}
