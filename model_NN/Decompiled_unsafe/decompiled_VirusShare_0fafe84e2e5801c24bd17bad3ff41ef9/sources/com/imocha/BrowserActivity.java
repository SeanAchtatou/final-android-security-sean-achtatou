package com.imocha;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class BrowserActivity extends Activity implements View.OnClickListener, az {
    private a a = null;

    private View a(int i, String[] strArr, boolean z, int i2) {
        t tVar = new t(this);
        tVar.setId(i);
        tVar.a = new Bitmap[strArr.length];
        for (int i3 = 0; i3 < strArr.length; i3++) {
            tVar.a[i3] = BitmapFactory.decodeStream(af.b(tVar.getContext(), strArr[i3]));
        }
        tVar.a(tVar.a[1]);
        tVar.setOnClickListener(this);
        tVar.setEnabled(z);
        tVar.setVisibility(i2);
        return tVar;
    }

    private View a(int i, String[] strArr, boolean z, RelativeLayout.LayoutParams layoutParams) {
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.addView(a(i, strArr, z, 0), layoutParams);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams2.weight = 1.0f;
        relativeLayout.setLayoutParams(layoutParams2);
        return relativeLayout;
    }

    private void e() {
        t tVar = (t) findViewById(4113);
        if (tVar != null) {
            tVar.setEnabled(this.a.canGoBack());
            t tVar2 = (t) findViewById(4114);
            if (tVar2 != null) {
                tVar2.setEnabled(this.a.canGoForward());
                t tVar3 = (t) findViewById(4115);
                if (tVar3 != null) {
                    tVar3.setVisibility(0);
                    t tVar4 = (t) findViewById(4118);
                    if (tVar4 != null) {
                        tVar4.setVisibility(8);
                    }
                }
            }
        }
    }

    public final void a() {
        e();
        ((ProgressBar) findViewById(4112)).setVisibility(8);
    }

    public final void a(int i) {
        ((ProgressBar) findViewById(4112)).setProgress(i);
    }

    public final void a(String str) {
        setTitle(str);
    }

    public final boolean b() {
        return false;
    }

    public final void c() {
        t tVar = (t) findViewById(4115);
        if (tVar != null) {
            tVar.setVisibility(8);
            t tVar2 = (t) findViewById(4118);
            if (tVar2 != null) {
                tVar2.setVisibility(0);
            }
        }
        ProgressBar progressBar = (ProgressBar) findViewById(4112);
        progressBar.setProgress(0);
        progressBar.setVisibility(0);
    }

    public final void d() {
        e();
    }

    public void finish() {
        this.a.clearCache(true);
        this.a.clearHistory();
        super.finish();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case 4113:
                this.a.goBack();
                return;
            case 4114:
                this.a.goForward();
                return;
            case 4115:
                this.a.reload();
                return;
            case 4116:
                String url = this.a.getUrl();
                if (url != null) {
                    try {
                        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(this, "此页面不支持GOOGLE浏览器", 0).show();
                        return;
                    }
                } else {
                    return;
                }
            case 4117:
                finish();
                return;
            case 4118:
                this.a.stopLoading();
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.imocha.BrowserActivity.a(int, java.lang.String[], boolean, android.widget.RelativeLayout$LayoutParams):android.view.View
     arg types: [int, java.lang.String[], int, android.widget.RelativeLayout$LayoutParams]
     candidates:
      com.imocha.BrowserActivity.a(int, java.lang.String[], boolean, int):android.view.View
      com.imocha.BrowserActivity.a(int, java.lang.String[], boolean, android.widget.RelativeLayout$LayoutParams):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.imocha.BrowserActivity.a(int, java.lang.String[], boolean, int):android.view.View
     arg types: [int, java.lang.String[], int, int]
     candidates:
      com.imocha.BrowserActivity.a(int, java.lang.String[], boolean, android.widget.RelativeLayout$LayoutParams):android.view.View
      com.imocha.BrowserActivity.a(int, java.lang.String[], boolean, int):android.view.View */
    public void onCreate(Bundle bundle) {
        String[] strArr;
        String[] strArr2;
        String[] strArr3;
        String[] strArr4;
        String[] strArr5;
        String[] strArr6;
        RelativeLayout.LayoutParams layoutParams;
        super.onCreate(bundle);
        requestWindowFeature(1);
        try {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(1);
            linearLayout.setBackgroundColor(-1);
            setContentView(linearLayout);
            ProgressBar progressBar = new ProgressBar(this, null, 16842872);
            progressBar.setMax(100);
            progressBar.setId(4112);
            linearLayout.addView(progressBar, new LinearLayout.LayoutParams(-1, -2));
            this.a = new a(this, null, null, 2);
            this.a.setId(4119);
            LinearLayout linearLayout2 = new LinearLayout(this);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
            layoutParams2.weight = 1.0f;
            linearLayout2.addView(this.a, layoutParams2);
            linearLayout.addView(linearLayout2, layoutParams2);
            LinearLayout linearLayout3 = new LinearLayout(this);
            linearLayout3.setOrientation(0);
            linearLayout3.setBackgroundDrawable(af.c(this, "browser_bg.png"));
            linearLayout.addView(linearLayout3);
            LinearLayout linearLayout4 = new LinearLayout(this);
            LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams3.weight = 1.0f;
            linearLayout3.addView(linearLayout4, layoutParams3);
            LinearLayout linearLayout5 = new LinearLayout(this);
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams4.gravity = 80;
            linearLayout3.addView(linearLayout5, layoutParams4);
            ImageView imageView = new ImageView(this);
            RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(72, 50);
            int e = af.a().e(this);
            if (e > 400) {
                layoutParams4.leftMargin = 5;
                imageView.setImageBitmap(BitmapFactory.decodeStream(af.b(this, "browser_logo_480.png")));
                linearLayout5.addView(imageView, 84, 17);
                strArr = new String[]{"finish_focus_480.png", "finish_default_480.png"};
                strArr2 = new String[]{"jump_1_480.png", "jump_2_480.png"};
                strArr3 = new String[]{"cancel_1_480.png", "cancel_2_480.png"};
                String[] strArr7 = {"refresh_1_480.png", "refresh_2_480.png"};
                layoutParams = layoutParams5;
                strArr6 = strArr7;
                String[] strArr8 = {"pre_focus_480.png", "pre_enable_480.png", "pre_disable_480.png"};
                strArr4 = new String[]{"next_focus_480.png", "next_enable_480.png", "next_disable_480.png"};
                strArr5 = strArr8;
            } else {
                String[] strArr9 = {"pre_focus_240.png", "pre_enable_240.png", "pre_disable_240.png"};
                String[] strArr10 = {"next_focus_240.png", "next_enable_240.png", "next_disable_240.png"};
                String[] strArr11 = {"refresh_1_240.png", "refresh_2_240.png"};
                String[] strArr12 = {"cancel_1_240.png", "cancel_2_240.png"};
                String[] strArr13 = {"jump_1_240.png", "jump_2_240.png"};
                String[] strArr14 = {"finish_focus_240.png", "finish_default_240.png"};
                RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(42, 30);
                Bitmap decodeStream = BitmapFactory.decodeStream(af.b(this, "browser_logo_240.png"));
                layoutParams4.bottomMargin = 3;
                layoutParams4.leftMargin = 7;
                imageView.setImageBitmap(decodeStream);
                linearLayout5.addView(imageView, (e / 240) * 30, (e / 240) * 11);
                strArr = strArr14;
                strArr2 = strArr13;
                strArr3 = strArr12;
                strArr4 = strArr10;
                strArr5 = strArr9;
                strArr6 = strArr11;
                layoutParams = layoutParams6;
            }
            layoutParams.addRule(13);
            linearLayout4.addView(a(4113, strArr5, false, layoutParams));
            linearLayout4.addView(a(4114, strArr4, false, layoutParams));
            RelativeLayout relativeLayout = new RelativeLayout(this);
            LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams7.weight = 1.0f;
            relativeLayout.setLayoutParams(layoutParams7);
            relativeLayout.addView(a(4115, strArr6, true, 8), layoutParams);
            relativeLayout.addView(a(4118, strArr3, true, 0), layoutParams);
            linearLayout4.addView(relativeLayout);
            linearLayout4.addView(a(4116, strArr2, true, layoutParams));
            linearLayout4.addView(a(4117, strArr, true, layoutParams));
        } catch (Exception e2) {
            e2.printStackTrace();
            Toast.makeText(this, "资源设置有问题", 0).show();
            finish();
        }
        this.a = (a) findViewById(4119);
        this.a.b = this;
        this.a.getSettings().setSupportZoom(true);
        this.a.getSettings().setJavaScriptEnabled(true);
        this.a.setVisibility(0);
        this.a.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        this.a.getSettings().setPluginsEnabled(true);
        this.a.getSettings().setDefaultFixedFontSize(2);
        this.a.getSettings().setBuiltInZoomControls(true);
        this.a.setHorizontalScrollbarOverlay(false);
        this.a.setHorizontalScrollBarEnabled(true);
        this.a.setVerticalScrollBarEnabled(false);
        try {
            this.a.loadUrl(getIntent().getStringExtra("url"));
        } catch (Exception e3) {
            Toast.makeText(this, "此页面无法显示", 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.a != null) {
            this.a.destroy();
        }
        super.onDestroy();
    }
}
