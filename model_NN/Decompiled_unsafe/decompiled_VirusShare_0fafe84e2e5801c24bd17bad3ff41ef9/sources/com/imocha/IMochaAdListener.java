package com.imocha;

public interface IMochaAdListener {
    void onEvent(AdView adView, IMochaEventCode iMochaEventCode);
}
