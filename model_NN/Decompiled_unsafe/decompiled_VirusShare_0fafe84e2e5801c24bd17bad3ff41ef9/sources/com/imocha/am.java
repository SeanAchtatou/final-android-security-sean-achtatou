package com.imocha;

import android.content.Context;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Hashtable;
import org.xml.sax.Attributes;

final class am {
    private static Object l = "subject";
    private static Object m = "content";
    ao a;
    b b;
    u c;
    j d;
    g e;
    boolean f = true;
    Hashtable g = new Hashtable();
    byte[] h;
    protected int i = 0;
    String j = "";
    m k = m.Image;
    private ArrayList n = new ArrayList();
    private k o = null;
    private String p = null;
    private ak q;

    am() {
    }

    /* access modifiers changed from: protected */
    public final k a() {
        double d2 = 2.68435454E8d;
        if (this.o == null) {
            if (this.n.size() > 0) {
                if (((String) this.g.get("type")).equals("banner")) {
                    for (int i2 = 0; i2 < this.n.size(); i2++) {
                        k kVar = (k) this.n.get(i2);
                        if (d2 > Math.abs(kVar.g - 1.0d)) {
                            d2 = Math.abs(kVar.g - 1.0d);
                            this.o = kVar;
                        }
                    }
                } else {
                    for (int i3 = 0; i3 < this.n.size(); i3++) {
                        k kVar2 = (k) this.n.get(i3);
                        if (d2 > Math.abs(kVar2.f - 1.0d)) {
                            d2 = Math.abs(kVar2.f - 1.0d);
                            this.o = kVar2;
                        }
                    }
                }
            }
            if (this.o != null) {
                if (this.o.c == null) {
                    this.o = null;
                    return this.o;
                }
                int lastIndexOf = this.o.c.lastIndexOf(46) + 1;
                if (lastIndexOf > 0 && lastIndexOf < this.o.c.length()) {
                    String substring = this.o.c.substring(lastIndexOf);
                    if (x.b(substring)) {
                        if (this.o.c.endsWith("html.zip")) {
                            this.k = m.Html;
                            this.k.a("html.zip");
                            this.k.b("hdtmobileindex.html");
                        } else {
                            this.k.a(substring);
                        }
                    } else if (x.a(substring)) {
                        this.k = m.Image;
                        this.k.a(substring);
                    }
                }
            }
        }
        return this.o;
    }

    /* access modifiers changed from: protected */
    public final void a(Context context, String str, String str2, Attributes attributes) {
        if (str2.equals("ad")) {
            int length = attributes.getLength();
            for (int i2 = 0; i2 < length; i2++) {
                String localName = attributes.getLocalName(i2);
                this.g.put(localName, attributes.getValue(localName));
            }
        } else if (str2.equals("img")) {
            k kVar = new k();
            if (str.equals("img")) {
                k.a = attributes.getValue("duration");
                k.b = attributes.getValue("dld_base");
            } else if (str.equals("url")) {
                kVar.d = attributes.getValue("width");
                kVar.e = attributes.getValue("height");
                kVar.i = attributes.getValue("value");
                kVar.c = String.valueOf(k.b) + kVar.i;
                double e2 = (double) af.a().e(context);
                double f2 = (double) af.a().f(context);
                kVar.g = e2 / ((double) Integer.parseInt(kVar.d));
                kVar.h = f2 / ((double) Integer.parseInt(kVar.e));
                if (kVar.g < kVar.h) {
                    kVar.f = kVar.g;
                } else {
                    kVar.f = kVar.h;
                }
                this.n.add(kVar);
            }
            this.k = m.Image;
        } else if (str2.equals("gateway")) {
            this.a = new ao();
            this.a.a = attributes.getValue("url");
        } else if (str2.equals("clk")) {
            if (this.b == null) {
                this.b = b.a(str, attributes);
            }
            this.b.b(str, attributes);
        } else if (str2.equals("tracks")) {
            if (this.d == null) {
                this.d = new j();
            }
            j.a(str, attributes);
        }
    }

    public final void a(String str, String str2, String str3) {
        if (!str2.equals("ad") && !str2.equals("img") && !str2.equals("gateway") && str2.equals("clk") && this.b != null) {
            this.b.a(str, str3);
        }
        if (str2.equals("tracks") && this.d != null) {
            this.d.a(str, str2, str3);
        }
    }

    public final void a(byte[] bArr) {
        this.h = bArr;
        this.i = bArr.length;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(IMochaAdView iMochaAdView) {
        this.q = new ak();
        return this.q.a(iMochaAdView, this);
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        if (this.e != null) {
            return this.e.a;
        }
        k a2 = a();
        if (a2 != null) {
            return a2.c;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final boolean b(IMochaAdView iMochaAdView) {
        return this.q.b(iMochaAdView, this);
    }

    /* access modifiers changed from: package-private */
    public final String c() {
        String str;
        String str2;
        String str3;
        if (this.p != null) {
            return this.p;
        }
        if (this.e != null) {
            str = this.e.a;
        } else {
            k a2 = a();
            str = a2 != null ? a2.c : null;
        }
        if (str != null) {
            int lastIndexOf = str.lastIndexOf(46);
            String substring = lastIndexOf >= 0 ? str.substring(lastIndexOf) : null;
            try {
                str2 = af.a(MessageDigest.getInstance("MD5").digest(str.getBytes()));
                if (substring != null) {
                    try {
                        if (this.k == m.Image) {
                            str2 = String.valueOf(str2) + substring;
                        }
                    } catch (Exception e2) {
                        Exception exc = e2;
                        str3 = str2;
                        e = exc;
                        e.printStackTrace();
                        str2 = str3;
                        this.p = str2;
                        return str2;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                str3 = null;
            }
        } else {
            str2 = null;
        }
        this.p = str2;
        return str2;
    }

    public final byte[] d() {
        return this.h;
    }
}
