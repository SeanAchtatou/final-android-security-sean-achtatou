package com.tencent.mobwin;

import android.view.View;

class k implements View.OnClickListener {
    final /* synthetic */ MobinWINBrowserActivity a;

    k(MobinWINBrowserActivity mobinWINBrowserActivity) {
        this.a = mobinWINBrowserActivity;
    }

    public void onClick(View view) {
        if (this.a.j != null && this.a.j.canGoForward()) {
            this.a.j.goForward();
        }
    }
}
