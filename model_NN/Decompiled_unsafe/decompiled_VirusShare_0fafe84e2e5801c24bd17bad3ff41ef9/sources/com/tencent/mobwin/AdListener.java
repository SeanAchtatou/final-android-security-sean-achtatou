package com.tencent.mobwin;

public interface AdListener {
    public static final int ERROR_CONNECTION_FAILED = 1;
    public static final int ERROR_GET_IMAGE_FAILED = 12;
    public static final int ERROR_GIF_DECODE_FAILED = 11;
    public static final int ERROR_NO_AVAILABLE_ADS = 10;
    public static final int ERROR_SERVER_DATA_EXCEPTION = 13;

    void onAdClick();

    void onReceiveAd();

    void onReceiveFailed(int i);
}
