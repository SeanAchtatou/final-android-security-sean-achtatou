package com.tencent.mobwin.core;

import android.view.animation.Animation;

class q implements Animation.AnimationListener {
    public Runnable a;
    final /* synthetic */ w b;

    private q(w wVar) {
        this.b = wVar;
    }

    /* synthetic */ q(w wVar, q qVar) {
        this(wVar);
    }

    public void onAnimationEnd(Animation animation) {
        if (this.a != null) {
            this.b.post(this.a);
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
