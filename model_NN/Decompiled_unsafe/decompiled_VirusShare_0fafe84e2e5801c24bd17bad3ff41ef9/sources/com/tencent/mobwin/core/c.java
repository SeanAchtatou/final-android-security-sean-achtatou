package com.tencent.mobwin.core;

import android.graphics.Bitmap;
import android.view.animation.Animation;

class c implements Runnable {
    final /* synthetic */ w a;
    private Animation b;

    public c(w wVar, Animation animation) {
        this.a = wVar;
        this.b = animation;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.mobwin.core.w.b(com.tencent.mobwin.core.w, boolean):void
     arg types: [com.tencent.mobwin.core.w, int]
     candidates:
      com.tencent.mobwin.core.w.b(com.tencent.mobwin.core.w, int):void
      com.tencent.mobwin.core.w.b(com.tencent.mobwin.core.w, android.view.View):void
      com.tencent.mobwin.core.w.b(com.tencent.mobwin.core.w, boolean):void */
    public void run() {
        o.a("IORY", "描绘前" + System.currentTimeMillis());
        try {
            this.a.l();
            this.a.ao = false;
            if (this.b != null) {
                this.a.startAnimation(this.b);
            }
            this.a.J();
            o.a("IORY", "描绘后" + System.currentTimeMillis());
            if (!(this.a.ai == null || this.a.ai.a == null || this.a.ai.d)) {
                this.a.D();
            }
            this.a.B();
            if (this.a.W != null) {
                this.a.W.recycle();
                this.a.W = (Bitmap) null;
            }
            this.a.al.requestLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
