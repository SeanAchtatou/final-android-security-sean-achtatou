package com.tencent.mobwin.core;

import android.os.Handler;
import android.os.Message;
import com.tencent.mobwin.core.b.a;
import com.tencent.mobwin.core.b.b;

class s implements a {
    final /* synthetic */ t h;

    s(t tVar) {
        this.h = tVar;
    }

    public void a(int i, b bVar) {
        Handler handler = bVar.h;
        if (handler != null) {
            switch (bVar.g) {
                case 1:
                    Message obtainMessage = handler.obtainMessage(4);
                    obtainMessage.arg1 = i;
                    handler.sendMessage(obtainMessage);
                    return;
                case 2:
                    Message obtainMessage2 = handler.obtainMessage(6);
                    obtainMessage2.arg1 = i;
                    handler.sendMessage(obtainMessage2);
                    return;
                case 3:
                    Message obtainMessage3 = handler.obtainMessage(12);
                    obtainMessage3.arg1 = i;
                    handler.sendMessage(obtainMessage3);
                    return;
                case 4:
                    handler.sendEmptyMessage(10);
                    return;
                case 5:
                default:
                    return;
                case 6:
                    Message obtainMessage4 = handler.obtainMessage(2);
                    obtainMessage4.arg1 = i;
                    handler.sendMessage(obtainMessage4);
                    return;
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(org.apache.http.HttpResponse r7, com.tencent.mobwin.core.b.b r8) {
        /*
            r6 = this;
            android.os.Handler r0 = r8.h     // Catch:{ Exception -> 0x0035 }
            if (r0 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            org.apache.http.HttpEntity r1 = r7.getEntity()     // Catch:{ Exception -> 0x0035 }
            java.io.InputStream r1 = r1.getContent()     // Catch:{ Exception -> 0x0035 }
            byte[] r1 = com.tencent.mobwin.core.t.b(r1)     // Catch:{ Exception -> 0x0035 }
            int r2 = r8.g     // Catch:{ Exception -> 0x0035 }
            switch(r2) {
                case 1: goto L_0x0017;
                case 2: goto L_0x006f;
                case 3: goto L_0x00e5;
                case 4: goto L_0x0100;
                case 5: goto L_0x0016;
                case 6: goto L_0x0049;
                default: goto L_0x0016;
            }     // Catch:{ Exception -> 0x0035 }
        L_0x0016:
            goto L_0x0004
        L_0x0017:
            r2 = 1
            com.qq.taf.jce.JceStruct r6 = com.tencent.mobwin.core.t.b(r1, r2)     // Catch:{ Exception -> 0x0035 }
            MobWin.ResAppLaunch r6 = (MobWin.ResAppLaunch) r6     // Catch:{ Exception -> 0x0035 }
            if (r0 == 0) goto L_0x0004
            if (r6 == 0) goto L_0x003c
            byte r1 = r6.a     // Catch:{ Exception -> 0x0035 }
            if (r1 != 0) goto L_0x003c
            r1 = 3
            android.os.Message r1 = r0.obtainMessage(r1)     // Catch:{ Exception -> 0x0035 }
            byte r2 = r6.a     // Catch:{ Exception -> 0x0035 }
            r1.arg1 = r2     // Catch:{ Exception -> 0x0035 }
            r1.obj = r6     // Catch:{ Exception -> 0x0035 }
            r0.sendMessage(r1)     // Catch:{ Exception -> 0x0035 }
            goto L_0x0004
        L_0x0035:
            r0 = move-exception
            com.tencent.mobwin.core.z r0 = new com.tencent.mobwin.core.z
            r0.<init>()
            throw r0
        L_0x003c:
            r1 = 4
            android.os.Message r1 = r0.obtainMessage(r1)     // Catch:{ Exception -> 0x0035 }
            byte r2 = r6.a     // Catch:{ Exception -> 0x0035 }
            r1.arg1 = r2     // Catch:{ Exception -> 0x0035 }
            r0.sendMessage(r1)     // Catch:{ Exception -> 0x0035 }
            goto L_0x0004
        L_0x0049:
            r2 = 6
            com.qq.taf.jce.JceStruct r6 = com.tencent.mobwin.core.t.b(r1, r2)     // Catch:{ Exception -> 0x005b }
            MobWin.ResActivateApp r6 = (MobWin.ResActivateApp) r6     // Catch:{ Exception -> 0x005b }
            if (r6 == 0) goto L_0x0062
            byte r1 = r6.code     // Catch:{ Exception -> 0x005b }
            if (r1 != 0) goto L_0x0062
            r1 = 1
            r0.sendEmptyMessage(r1)     // Catch:{ Exception -> 0x005b }
            goto L_0x0004
        L_0x005b:
            r0 = move-exception
            com.tencent.mobwin.core.z r0 = new com.tencent.mobwin.core.z     // Catch:{ Exception -> 0x0035 }
            r0.<init>()     // Catch:{ Exception -> 0x0035 }
            throw r0     // Catch:{ Exception -> 0x0035 }
        L_0x0062:
            r1 = 2
            android.os.Message r1 = r0.obtainMessage(r1)     // Catch:{ Exception -> 0x005b }
            byte r2 = r6.code     // Catch:{ Exception -> 0x005b }
            r1.arg1 = r2     // Catch:{ Exception -> 0x005b }
            r0.sendMessage(r1)     // Catch:{ Exception -> 0x005b }
            goto L_0x0004
        L_0x006f:
            java.lang.String r2 = "IORY"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r4 = "开始解码时间："
            r3.<init>(r4)     // Catch:{ Exception -> 0x00d0 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00d0 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00d0 }
            com.tencent.mobwin.core.o.a(r2, r3)     // Catch:{ Exception -> 0x00d0 }
            r2 = 2
            com.qq.taf.jce.JceStruct r6 = com.tencent.mobwin.core.t.b(r1, r2)     // Catch:{ Exception -> 0x00d0 }
            MobWin.ResGetAD r6 = (MobWin.ResGetAD) r6     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r1 = "IORY"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r3 = "解码结束时间："
            r2.<init>(r3)     // Catch:{ Exception -> 0x00d0 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00d0 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00d0 }
            com.tencent.mobwin.core.o.a(r1, r2)     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r1 = "LinkData"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r3 = "get ad:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x00d0 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00d0 }
            com.tencent.mobwin.core.o.a(r1, r2)     // Catch:{ Exception -> 0x00d0 }
            if (r6 == 0) goto L_0x00d7
            byte r1 = r6.a     // Catch:{ Exception -> 0x00d0 }
            if (r1 != 0) goto L_0x00d7
            r1 = 5
            android.os.Message r1 = r0.obtainMessage(r1)     // Catch:{ Exception -> 0x00d0 }
            byte r2 = r6.a     // Catch:{ Exception -> 0x00d0 }
            r1.arg1 = r2     // Catch:{ Exception -> 0x00d0 }
            r1.obj = r6     // Catch:{ Exception -> 0x00d0 }
            r0.sendMessage(r1)     // Catch:{ Exception -> 0x00d0 }
            goto L_0x0004
        L_0x00d0:
            r0 = move-exception
            com.tencent.mobwin.core.z r0 = new com.tencent.mobwin.core.z     // Catch:{ Exception -> 0x0035 }
            r0.<init>()     // Catch:{ Exception -> 0x0035 }
            throw r0     // Catch:{ Exception -> 0x0035 }
        L_0x00d7:
            r1 = 6
            android.os.Message r1 = r0.obtainMessage(r1)     // Catch:{ Exception -> 0x00d0 }
            byte r2 = r6.a     // Catch:{ Exception -> 0x00d0 }
            r1.arg1 = r2     // Catch:{ Exception -> 0x00d0 }
            r0.sendMessage(r1)     // Catch:{ Exception -> 0x00d0 }
            goto L_0x0004
        L_0x00e5:
            r2 = 3
            com.qq.taf.jce.JceStruct r6 = com.tencent.mobwin.core.t.b(r1, r2)     // Catch:{ Exception -> 0x00f9 }
            MobWin.ResReportAdPlayInfo r6 = (MobWin.ResReportAdPlayInfo) r6     // Catch:{ Exception -> 0x00f9 }
            if (r6 == 0) goto L_0x0004
            byte r1 = r6.a     // Catch:{ Exception -> 0x00f9 }
            if (r1 != 0) goto L_0x0004
            r1 = 11
            r0.sendEmptyMessage(r1)     // Catch:{ Exception -> 0x00f9 }
            goto L_0x0004
        L_0x00f9:
            r0 = move-exception
            com.tencent.mobwin.core.z r0 = new com.tencent.mobwin.core.z     // Catch:{ Exception -> 0x0035 }
            r0.<init>()     // Catch:{ Exception -> 0x0035 }
            throw r0     // Catch:{ Exception -> 0x0035 }
        L_0x0100:
            r2 = 4
            com.qq.taf.jce.JceStruct r6 = com.tencent.mobwin.core.t.b(r1, r2)     // Catch:{ Exception -> 0x0116 }
            MobWin.ResClickAD r6 = (MobWin.ResClickAD) r6     // Catch:{ Exception -> 0x0116 }
            if (r6 == 0) goto L_0x0004
            byte r1 = r6.b()     // Catch:{ Exception -> 0x0116 }
            if (r1 != 0) goto L_0x0004
            r1 = 9
            r0.sendEmptyMessage(r1)     // Catch:{ Exception -> 0x0116 }
            goto L_0x0004
        L_0x0116:
            r0 = move-exception
            com.tencent.mobwin.core.z r0 = new com.tencent.mobwin.core.z     // Catch:{ Exception -> 0x0035 }
            r0.<init>()     // Catch:{ Exception -> 0x0035 }
            throw r0     // Catch:{ Exception -> 0x0035 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mobwin.core.s.a(org.apache.http.HttpResponse, com.tencent.mobwin.core.b.b):void");
    }
}
