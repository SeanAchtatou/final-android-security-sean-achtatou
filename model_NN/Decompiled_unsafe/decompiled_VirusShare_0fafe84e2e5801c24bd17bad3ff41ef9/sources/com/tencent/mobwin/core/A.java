package com.tencent.mobwin.core;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.DisplayMetrics;
import com.tencent.mobwin.core.a.d;
import com.tencent.mobwin.utils.b;
import java.util.HashMap;

public class A {
    public static final int a = 0;
    public static final int b = 1;
    public static final String[] c = {"back.png", "back_disable.png", "back_pressed.png", "forward.png", "forward_disable.png", "forward_pressed.png", "jumpout.png", "jumpout_pressed.png", "quit.png", "quit_pressed.png", "refresh.png", "refresh_pressed.png", "close_album.png", "close_album_pressed.png", "toolbar_body.png", "toolbar_body_pressed.png", "toolbar_header.png", "divideline.png", "process_bar.png"};
    public static final String[] d = {"banner_frame.png", "button.png", "button_pressed.png", "mobwinLogo.png"};
    private static final String e = "PictureManager";
    private static A j = null;
    private static Object n = new Object();
    private Context f;
    private HashMap g = new HashMap();
    private String h;
    private String i;
    private int k = 0;
    private t l = null;
    private Handler m = null;

    private A() {
    }

    private Bitmap a(String str) {
        String b2 = b.b(str);
        byte[] b3 = x.b(b2, this.f);
        o.a(e, "getLocalImage" + b2);
        if (b3 == null) {
            return null;
        }
        try {
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(b3, 0, b3.length);
            if (decodeByteArray == null) {
                return decodeByteArray;
            }
            decodeByteArray.setDensity(0);
            return decodeByteArray;
        } catch (Exception e2) {
            return null;
        }
    }

    public static A a() {
        if (j == null) {
            synchronized (n) {
                j = new A();
            }
        }
        return j;
    }

    public static void a(Activity activity, Handler handler) {
        a().c(activity, handler);
        a().d();
    }

    private String b(String str, int i2) {
        if (i2 == 0) {
            this.h = d.j();
            String str2 = String.valueOf(this.h) + str;
            o.a(e, "embedBrowserResUrl" + this.h);
            return str2;
        } else if (i2 == 1) {
            this.i = d.k();
            String str3 = String.valueOf(this.i) + str;
            o.a(e, "bannerResUrl" + this.i);
            return str3;
        } else {
            o.b(e, "图片资源获取地址出错！");
            return null;
        }
    }

    public static void b(Activity activity, Handler handler) {
        a().c(activity, handler);
        a().b();
    }

    private void c(Activity activity, Handler handler) {
        this.f = activity;
        this.m = handler;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (activity != null) {
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        }
        this.k = displayMetrics.densityDpi;
    }

    private void c(String str, int i2) {
        o.a(e, "downloadBitmap" + str);
        if (this.l == null) {
            this.l = new t();
        }
        if (i2 == 0) {
            this.l.a(this.f, str, this.m, 2);
        } else if (i2 == 1) {
            this.l.a(this.f, str, this.m, 3);
        }
    }

    public static void f() {
        if (j != null) {
            j.g();
        }
    }

    private void g() {
        this.f = null;
        this.g.clear();
        if (this.l != null) {
            this.l.a();
        }
        this.l = null;
    }

    public Bitmap a(String str, int i2) {
        Bitmap bitmap = (Bitmap) j.g.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        String b2 = b(str, i2);
        if (b2 == null || b2.equals("")) {
            o.b(e, "没有获取到图片地址");
            return null;
        }
        Bitmap a2 = a(b2);
        if (a2 == null) {
            c(b2, i2);
            return null;
        }
        j.g.put(str, a2);
        return a2;
    }

    public void b() {
        for (int i2 = 0; i2 < c.length; i2++) {
            String b2 = b(c[i2], 0);
            o.a(e, "initBrowserBitmaps: " + b2);
            Bitmap a2 = a(b2);
            if (a2 != null) {
                this.g.put(c[i2], a2);
            } else {
                c(b2, 0);
            }
        }
    }

    public void c() {
        for (int i2 = 0; i2 < d.length; i2++) {
            String b2 = b(d[i2], 1);
            o.a(e, "initBannerBitmaps: " + b2);
            Bitmap a2 = a(b2);
            if (a2 != null) {
                this.g.put(d[i2], a2);
            } else {
                c(b2, 1);
            }
        }
    }

    public void d() {
        b();
        c();
    }

    public HashMap e() {
        if (this.g == null) {
            this.g = new HashMap();
        }
        return this.g;
    }
}
