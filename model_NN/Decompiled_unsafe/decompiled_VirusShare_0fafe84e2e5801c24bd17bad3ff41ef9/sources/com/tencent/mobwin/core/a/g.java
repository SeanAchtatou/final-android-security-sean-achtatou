package com.tencent.mobwin.core.a;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

class g implements LocationListener {
    g() {
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
            f.w = (int) (location.getLongitude() * 1000000.0d);
            f.x = (int) (location.getLatitude() * 1000000.0d);
            f.y = (int) (location.getAltitude() * 1000000.0d);
            f.G.removeUpdates(this);
            f.G = (LocationManager) null;
        }
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
