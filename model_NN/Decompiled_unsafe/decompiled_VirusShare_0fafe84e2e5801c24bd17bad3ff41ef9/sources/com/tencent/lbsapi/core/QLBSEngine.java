package com.tencent.lbsapi.core;

import LBSAPIProtocol.Cell;
import LBSAPIProtocol.GPS;
import LBSAPIProtocol.Measure;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import com.tencent.lbsapi.QLBSNotification;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class QLBSEngine {
    private static final String c = "com.tencent.lbsapi.GPS_SINGLE";
    private String A = null;
    private String B = null;
    protected Runnable a = new b(this);
    protected Runnable b = new d(this);
    private Context d;
    /* access modifiers changed from: private */
    public WeakReference e;
    /* access modifiers changed from: private */
    public Handler f = null;
    private TelephonyManager g = null;
    /* access modifiers changed from: private */
    public LocationManager h = null;
    /* access modifiers changed from: private */
    public PendingIntent i;
    private BroadcastReceiver j;
    private WifiManager k = null;
    private BroadcastReceiver l = null;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public boolean o = false;
    private boolean p = true;
    private boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = false;
    private int s;
    private int t;
    private int u;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public int w;
    private int x;
    private ArrayList y = null;
    /* access modifiers changed from: private */
    public ArrayList z = null;

    public QLBSEngine(Context context, QLBSNotification qLBSNotification) {
        this.d = context;
        this.e = new WeakReference(qLBSNotification);
        IntentFilter intentFilter = new IntentFilter(c);
        this.j = new c(this);
        this.d.registerReceiver(this.j, intentFilter);
        s();
    }

    private static long a(byte[] bArr) {
        return ((long) (bArr[0] & 255)) | (((long) (bArr[1] & 255)) << 8) | (((long) (bArr[2] & 255)) << 16) | (((long) (bArr[3] & 255)) << 24) | (((long) (bArr[4] & 255)) << 32) | (((long) (bArr[5] & 255)) << 40) | (((long) (bArr[6] & 255)) << 48) | (((long) (bArr[7] & 255)) << 56);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0038 A[SYNTHETIC, Splitter:B:25:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x003d A[SYNTHETIC, Splitter:B:28:0x003d] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0050 A[SYNTHETIC, Splitter:B:37:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0055 A[SYNTHETIC, Splitter:B:40:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.util.ArrayList r6) {
        /*
            r5 = this;
            r3 = 0
            if (r6 == 0) goto L_0x0009
            int r0 = r6.size()
            if (r0 != 0) goto L_0x000a
        L_0x0009:
            return
        L_0x000a:
            android.content.Context r0 = r5.d     // Catch:{ Exception -> 0x0030, all -> 0x004b }
            java.lang.String r1 = "lbs_temp"
            r2 = 0
            java.io.FileOutputStream r0 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x0030, all -> 0x004b }
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x006f, all -> 0x0063 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x006f, all -> 0x0063 }
            r1.writeObject(r6)     // Catch:{ Exception -> 0x0074, all -> 0x0068 }
            if (r0 == 0) goto L_0x0020
            r0.close()     // Catch:{ IOException -> 0x002b }
        L_0x0020:
            if (r1 == 0) goto L_0x0009
            r1.close()     // Catch:{ IOException -> 0x0026 }
            goto L_0x0009
        L_0x0026:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0009
        L_0x002b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0020
        L_0x0030:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0033:
            r0.printStackTrace()     // Catch:{ all -> 0x006d }
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x0046 }
        L_0x003b:
            if (r1 == 0) goto L_0x0009
            r1.close()     // Catch:{ IOException -> 0x0041 }
            goto L_0x0009
        L_0x0041:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0009
        L_0x0046:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003b
        L_0x004b:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x004e:
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x005e }
        L_0x0058:
            throw r0
        L_0x0059:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0053
        L_0x005e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0058
        L_0x0063:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x004e
        L_0x0068:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x004e
        L_0x006d:
            r0 = move-exception
            goto L_0x004e
        L_0x006f:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0033
        L_0x0074:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.lbsapi.core.QLBSEngine.a(java.util.ArrayList):void");
    }

    private void b(int i2) {
        if (!this.n) {
            if (this.h == null) {
                this.h = (LocationManager) this.d.getSystemService("location");
                if (this.h == null) {
                    return;
                }
            }
            if (this.i == null) {
                this.i = PendingIntent.getBroadcast(this.d, 0, new Intent(c), 134217728);
            }
            if (this.i != null) {
                LocationManager locationManager = (LocationManager) this.d.getSystemService("location");
                if (locationManager.isProviderEnabled("network")) {
                    this.h.requestLocationUpdates("network", 2000, 5.0f, this.i);
                }
                if (locationManager.isProviderEnabled("gps")) {
                    this.h.requestLocationUpdates("gps", 2000, 5.0f, this.i);
                }
                this.n = true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void processReceiveGps(Location location) {
        try {
            this.x = (int) (location.getLongitude() * 1000000.0d);
            this.w = (int) (location.getLatitude() * 1000000.0d);
            if (!this.r) {
                if (this.v != 0) {
                    p();
                }
            } else if (this.n) {
                this.h.removeUpdates(this.i);
                this.n = false;
                if (!this.q) {
                    if (this.v == 0 && this.w == 900000000) {
                        if (this.e.get() != null) {
                            ((QLBSNotification) this.e.get()).onLocationNotification(0);
                            this.r = false;
                        }
                        if (this.f != null && this.a != null) {
                            this.f.removeCallbacks(this.a);
                            this.r = false;
                            return;
                        }
                        return;
                    }
                    if (this.e.get() != null) {
                        ((QLBSNotification) this.e.get()).onLocationNotification(1);
                        this.r = false;
                    }
                    if (this.f != null && this.a != null) {
                        this.f.removeCallbacks(this.a);
                    }
                } else if (this.m) {
                } else {
                    if (this.v == 0 && this.w == 900000000 && (this.z.size() == 0 || this.z == null)) {
                        if (this.e.get() != null) {
                            ((QLBSNotification) this.e.get()).onLocationNotification(0);
                            this.r = false;
                        }
                        if (this.f != null && this.a != null) {
                            this.f.removeCallbacks(this.a);
                            return;
                        }
                        return;
                    }
                    if (this.e.get() != null) {
                        ((QLBSNotification) this.e.get()).onLocationNotification(1);
                        this.r = false;
                    }
                    if (this.f != null && this.a != null) {
                        this.f.removeCallbacks(this.a);
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (!this.q) {
                if (this.v == 0) {
                    if (this.e.get() != null) {
                        ((QLBSNotification) this.e.get()).onLocationNotification(0);
                        this.r = false;
                    }
                    if (this.f != null && this.a != null) {
                        this.f.removeCallbacks(this.a);
                    }
                }
            } else if (this.m) {
            } else {
                if (this.v == 0 && (this.z.size() == 0 || this.z == null)) {
                    if (this.e.get() != null) {
                        ((QLBSNotification) this.e.get()).onLocationNotification(0);
                        this.r = false;
                    }
                    if (this.f != null && this.a != null) {
                        this.f.removeCallbacks(this.a);
                        return;
                    }
                    return;
                }
                if (this.e.get() != null) {
                    ((QLBSNotification) this.e.get()).onLocationNotification(1);
                    this.r = false;
                }
                if (this.f != null && this.a != null) {
                    this.f.removeCallbacks(this.a);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void processReceiveWifi() {
        try {
            if (this.m) {
                List<ScanResult> scanResults = this.k.getScanResults();
                if (this.z == null) {
                    this.z = new ArrayList();
                } else {
                    this.z.clear();
                }
                for (int i2 = 0; i2 < scanResults.size(); i2++) {
                    String[] split = scanResults.get(i2).BSSID.split(":");
                    byte[] bArr = {0, 0, 0, 0, 0, 0, 0, 0};
                    for (int i3 = 0; i3 < split.length; i3++) {
                        bArr[5 - i3] = (byte) Integer.parseInt(split[i3], 16);
                    }
                    this.z.add(Long.valueOf(a(bArr)));
                }
                if (!this.p) {
                    if (this.v == 0 && this.z.size() == 0) {
                        if (this.e.get() != null) {
                            ((QLBSNotification) this.e.get()).onLocationNotification(0);
                            this.r = false;
                        }
                        if (this.f != null) {
                            this.f.removeCallbacks(this.a);
                        }
                    } else {
                        if (this.e.get() != null) {
                            ((QLBSNotification) this.e.get()).onLocationNotification(1);
                            this.r = false;
                        }
                        if (!(this.f == null || this.a == null)) {
                            this.f.removeCallbacks(this.a);
                        }
                    }
                } else if (!this.n) {
                    if (this.v == 0 && this.w == 900000000 && this.z.size() == 0) {
                        if (this.e.get() != null) {
                            ((QLBSNotification) this.e.get()).onLocationNotification(0);
                            this.r = false;
                        }
                        if (!(this.f == null || this.a == null)) {
                            this.f.removeCallbacks(this.a);
                        }
                    } else {
                        if (this.e.get() != null) {
                            ((QLBSNotification) this.e.get()).onLocationNotification(1);
                            this.r = false;
                        }
                        if (!(this.f == null || this.a == null)) {
                            this.f.removeCallbacks(this.a);
                        }
                    }
                }
                this.m = false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            this.m = false;
            if (!this.p) {
                if (this.v == 0) {
                    if (this.e.get() != null) {
                        ((QLBSNotification) this.e.get()).onLocationNotification(0);
                        this.r = false;
                    }
                    if (this.f != null && this.a != null) {
                        this.f.removeCallbacks(this.a);
                    }
                }
            } else if (this.n) {
            } else {
                if (this.v == 0 && this.w == 900000000) {
                    if (this.e.get() != null) {
                        ((QLBSNotification) this.e.get()).onLocationNotification(0);
                        this.r = false;
                    }
                    if (this.f != null && this.a != null) {
                        this.f.removeCallbacks(this.a);
                        return;
                    }
                    return;
                }
                if (this.e.get() != null) {
                    ((QLBSNotification) this.e.get()).onLocationNotification(1);
                    this.r = false;
                }
                if (this.f != null && this.a != null) {
                    this.f.removeCallbacks(this.a);
                }
            }
        }
    }

    public static long r() {
        Calendar instance = Calendar.getInstance();
        instance.set(1970, 0, 1, 8, 0, 0);
        return (Calendar.getInstance().getTimeInMillis() - instance.getTimeInMillis()) / 1000;
    }

    private void readCell() {
        if (this.g == null) {
            this.g = (TelephonyManager) this.d.getSystemService("phone");
        }
        if (this.g != null) {
            try {
                this.A = this.g.getDeviceId();
                if (this.A == null) {
                    this.A = "";
                }
            } catch (Exception e2) {
                this.A = "";
            }
            if (this.g.getPhoneType() == 1) {
                GsmCellLocation gsmCellLocation = (GsmCellLocation) this.g.getCellLocation();
                if (gsmCellLocation != null) {
                    this.u = gsmCellLocation.getLac();
                    this.v = gsmCellLocation.getCid();
                    if (this.u > 0 || this.v > 0) {
                        String networkOperator = this.g.getNetworkOperator();
                        if (networkOperator != null) {
                            try {
                                this.s = Integer.parseInt(networkOperator.substring(0, 3));
                                this.t = Integer.parseInt(networkOperator.substring(3));
                            } catch (Exception e3) {
                                this.s = 0;
                                this.t = 0;
                                this.u = 0;
                                this.v = 0;
                                this.B = networkOperator;
                                return;
                            }
                        }
                        List<NeighboringCellInfo> neighboringCellInfo = this.g.getNeighboringCellInfo();
                        if (neighboringCellInfo != null) {
                            for (NeighboringCellInfo cid : neighboringCellInfo) {
                                this.y.add(Integer.valueOf(cid.getCid()));
                            }
                        }
                    } else {
                        this.u = 0;
                        this.v = 0;
                    }
                }
            } else if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 4 && this.g.getPhoneType() == 2) {
                try {
                    Class<?> cls = Class.forName("android.telephony.cdma.CdmaCellLocation");
                    if (cls != null) {
                        cls.getConstructor(null);
                        CellLocation cellLocation = this.g.getCellLocation();
                        Method method = cls.getMethod("getSystemId", new Class[0]);
                        if (method != null) {
                            this.u = ((Integer) method.invoke(cellLocation, null)).intValue();
                        }
                        Method method2 = cls.getMethod("getBaseStationId", new Class[0]);
                        if (method2 != null) {
                            this.v = ((Integer) method2.invoke(cellLocation, null)).intValue();
                        }
                        String networkOperator2 = this.g.getNetworkOperator();
                        if (networkOperator2 != null) {
                            try {
                                this.s = Integer.parseInt(networkOperator2.substring(0, 3));
                                this.t = Integer.parseInt(networkOperator2.substring(3));
                            } catch (Exception e4) {
                                this.B = networkOperator2;
                            }
                        }
                    }
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
            }
        }
        Log.e("111", toString());
    }

    private void s() {
        this.s = 0;
        this.t = 0;
        this.u = 0;
        this.v = 0;
        this.w = 900000000;
        this.x = 900000000;
        if (this.y == null) {
            this.y = new ArrayList();
        } else {
            this.y.clear();
        }
        if (this.z == null) {
            this.z = new ArrayList();
        } else {
            this.z.clear();
        }
        this.B = null;
    }

    private void t() {
        try {
            if (this.k == null) {
                this.k = (WifiManager) this.d.getSystemService("wifi");
            }
            if (this.l == null) {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
                this.l = new a(this);
                this.d.registerReceiver(this.l, intentFilter);
            }
            if (this.k != null) {
                this.q = this.k.isWifiEnabled();
                if (this.q) {
                    this.m = this.k.startScan();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a() {
        if (this.f != null) {
            this.f.removeCallbacks(this.a);
        }
        this.r = false;
        s();
    }

    public void a(int i2) {
        a(0, i2);
    }

    public void a(boolean z2) {
        this.p = z2;
    }

    public synchronized boolean a(int i2, int i3) {
        a();
        this.r = true;
        if (Settings.System.getInt(this.d.getContentResolver(), "airplane_mode_on", 0) == 0) {
            readCell();
        }
        if (d()) {
            t();
        } else {
            this.q = false;
        }
        if (this.p) {
            if (!b()) {
                this.p = false;
            }
            if (this.p) {
                b(i3);
            }
        }
        if (this.p || this.q) {
            if (this.f == null) {
                this.f = new Handler();
            }
            this.f.postDelayed(this.a, (long) (i3 * 1000));
        } else if (this.v == 0) {
            if (this.e.get() != null) {
                ((QLBSNotification) this.e.get()).onLocationNotification(0);
                this.r = false;
            }
        } else if (this.e.get() != null) {
            ((QLBSNotification) this.e.get()).onLocationNotification(1);
            this.r = false;
        }
        return true;
    }

    public boolean b() {
        LocationManager locationManager = (LocationManager) this.d.getSystemService("location");
        if (locationManager != null) {
            return locationManager.isProviderEnabled("gps") || locationManager.isProviderEnabled("network");
        }
        return false;
    }

    public int c() {
        readCell();
        if (this.s == 0 && this.t == 0) {
            return -1;
        }
        if (this.t == 0 || this.t == 2 || this.t == 7) {
            return 0;
        }
        if (this.t == 1 || this.t == 6) {
            return 1;
        }
        return (this.t == 3 || this.t == 5) ? 2 : -1;
    }

    public boolean d() {
        try {
            if (this.k == null) {
                this.k = (WifiManager) this.d.getSystemService("wifi");
            }
            if (this.k != null) {
                return this.k.isWifiEnabled();
            }
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public void e() {
        if (this.l != null) {
            this.d.unregisterReceiver(this.l);
        }
        if (this.e != null) {
            this.e.clear();
        }
        if (!(this.h == null || this.i == null)) {
            this.h.removeUpdates(this.i);
            this.h = null;
            this.i = null;
        }
        if (this.f != null) {
            this.f.removeCallbacks(this.a);
            this.f.removeCallbacks(this.b);
            this.f = null;
        }
        this.d.unregisterReceiver(this.j);
    }

    public int f() {
        return this.s;
    }

    public int g() {
        return this.t;
    }

    public int h() {
        return this.u;
    }

    public int i() {
        return this.v;
    }

    public int j() {
        return this.w;
    }

    public int k() {
        return this.x;
    }

    public ArrayList l() {
        return this.z;
    }

    public ArrayList m() {
        return this.y;
    }

    public String n() {
        return this.A == null ? "" : this.A;
    }

    public String o() {
        return this.B == null ? "" : this.B;
    }

    public void p() {
        ArrayList q2 = q();
        ArrayList arrayList = q2 == null ? new ArrayList() : q2;
        Measure measure = new Measure();
        measure.setLTime(r());
        measure.setStGps(new GPS(this.w, this.x, -1, 0));
        measure.setVCells(new ArrayList());
        measure.getVCells().add(new Cell((short) this.s, (short) this.t, this.u, this.v));
        Iterator it = this.y.iterator();
        while (it.hasNext()) {
            measure.getVCells().add(new Cell((short) this.s, (short) this.t, this.u, ((Integer) it.next()).intValue()));
        }
        measure.setVMacs(l());
        measure.setStrExtraInfo(o());
        arrayList.add(measure);
        a(arrayList);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0035 A[SYNTHETIC, Splitter:B:21:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003a A[SYNTHETIC, Splitter:B:24:0x003a] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0055 A[SYNTHETIC, Splitter:B:34:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x005a A[SYNTHETIC, Splitter:B:37:0x005a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList q() {
        /*
            r5 = this;
            r3 = 0
            android.content.Context r0 = r5.d     // Catch:{ Exception -> 0x0030, all -> 0x0050 }
            java.lang.String r1 = "lbs_temp"
            java.io.FileInputStream r1 = r0.openFileInput(r1)     // Catch:{ Exception -> 0x0030, all -> 0x0050 }
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x0078, all -> 0x006f }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0078, all -> 0x006f }
            java.lang.Object r0 = r2.readObject()     // Catch:{ Exception -> 0x007b, all -> 0x0073 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ Exception -> 0x007b, all -> 0x0073 }
            if (r2 == 0) goto L_0x0019
            r2.close()     // Catch:{ IOException -> 0x0026 }
        L_0x0019:
            if (r1 == 0) goto L_0x001e
            r1.close()     // Catch:{ IOException -> 0x002b }
        L_0x001e:
            android.content.Context r1 = r5.d
            java.lang.String r2 = "lbs_temp"
            r1.deleteFile(r2)
        L_0x0025:
            return r0
        L_0x0026:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0019
        L_0x002b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001e
        L_0x0030:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0033:
            if (r0 == 0) goto L_0x0038
            r0.close()     // Catch:{ IOException -> 0x0046 }
        L_0x0038:
            if (r1 == 0) goto L_0x003d
            r1.close()     // Catch:{ IOException -> 0x004b }
        L_0x003d:
            android.content.Context r0 = r5.d
            java.lang.String r1 = "lbs_temp"
            r0.deleteFile(r1)
            r0 = r3
            goto L_0x0025
        L_0x0046:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0038
        L_0x004b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003d
        L_0x0050:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x0065 }
        L_0x0058:
            if (r2 == 0) goto L_0x005d
            r2.close()     // Catch:{ IOException -> 0x006a }
        L_0x005d:
            android.content.Context r1 = r5.d
            java.lang.String r2 = "lbs_temp"
            r1.deleteFile(r2)
            throw r0
        L_0x0065:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0058
        L_0x006a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005d
        L_0x006f:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0053
        L_0x0073:
            r0 = move-exception
            r4 = r2
            r2 = r1
            r1 = r4
            goto L_0x0053
        L_0x0078:
            r0 = move-exception
            r0 = r3
            goto L_0x0033
        L_0x007b:
            r0 = move-exception
            r0 = r2
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.lbsapi.core.QLBSEngine.q():java.util.ArrayList");
    }

    public String toString() {
        return "mcc=" + this.s + " mnc" + this.t + " cell=" + this.v + "  lat=" + this.w + " wifiNum=" + this.z.size();
    }
}
