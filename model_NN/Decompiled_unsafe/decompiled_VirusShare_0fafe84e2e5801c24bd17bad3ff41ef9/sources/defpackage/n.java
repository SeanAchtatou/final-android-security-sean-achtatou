package defpackage;

import android.view.View;

/* renamed from: n  reason: default package */
final class n implements View.OnClickListener {
    final /* synthetic */ m a;

    n(m mVar) {
        this.a = mVar;
    }

    public final void onClick(View view) {
        if (this.a.x.canGoBack()) {
            this.a.x.goBack();
        }
    }
}
