package ua.netlizard.egypt;

public final class room {
    short[] Points;
    short[] Poly;
    int cen_x;
    int cen_y;
    short end_obj;
    int max_x;
    int max_y;
    int max_z;
    int min_x;
    int min_y;
    int min_z;
    int[] place;
    int places_index;
    int points;
    int polys;
    byte[] portal;
    int portals;
    short start_obj;

    public room() {
        this.portals = 0;
        this.start_obj = 0;
        this.end_obj = 0;
        this.portals = 0;
        this.polys = 0;
        this.portal = new byte[9];
        for (int i = 0; i < 7; i++) {
            this.portal[i] = -1;
        }
    }
}
