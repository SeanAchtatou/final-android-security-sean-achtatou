package ua.netlizard.egypt;

public final class NET_Lizard extends J2MEMIDlet {
    static NET_Lizard instance;
    static int midlet_mode = 0;
    static Game notifyDestroyed;
    static boolean red_key = true;

    public NET_Lizard() {
        instance = this;
        if (Uni.uni == null) {
            Uni.uni_start();
        }
        if (notifyDestroyed == null) {
            notifyDestroyed = new Game();
        }
    }

    public void startApp() {
        if (midlet_mode == 0) {
            notifyDestroyed.game_start();
        }
        midlet_mode = 1;
        red_key = true;
    }

    public void pauseApp() {
        midlet_mode = 2;
        notifyPaused();
    }

    public void destroyApp(boolean unconditional) {
        if (red_key) {
            try {
                Game.cikl_rn = false;
            } catch (Exception e) {
            }
            try {
                Snd2.soundClose();
            } catch (Exception e2) {
            }
        }
        Uni.uni_end();
        try {
            notifyDestroyed.thread = null;
        } catch (Exception e3) {
        }
        notifyDestroyed = null;
        midlet_mode = 0;
        notifyDestroyed();
    }

    public static void quitApp() {
        red_key = false;
        instance.destroyApp(true);
        instance.notifyDestroyed();
        instance = null;
    }
}
