package ua.netlizard.egypt;

public class VolumeControl extends Control {
    /* access modifiers changed from: package-private */
    public int getLevel() {
        return SoundSystem.instance.streamVol;
    }

    /* access modifiers changed from: package-private */
    public boolean isMuted() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public int setLevel(int level) {
        SoundSystem.instance.setVolume(level);
        return level;
    }

    /* access modifiers changed from: package-private */
    public void setMuted(boolean mute) {
    }
}
