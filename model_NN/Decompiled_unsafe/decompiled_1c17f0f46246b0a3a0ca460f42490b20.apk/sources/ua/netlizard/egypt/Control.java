package ua.netlizard.egypt;

public class Control {
    public static Control getControl(String controlType) {
        if (controlType.equals("VolumeControl")) {
            return SoundSystem.instance.vcontrol;
        }
        return null;
    }
}
