package ua.netlizard.egypt;

public final class scr {
    static boolean Loop_script = false;
    static final byte a_ = 97;
    static byte anim_script = 0;
    static final byte b_ = 98;
    static final byte c_ = 99;
    static final byte d_ = 100;
    static final byte e_ = 101;
    static boolean enable_script = false;
    static final byte f_ = 102;
    static final byte g_ = 103;
    static final byte h_ = 104;
    static final byte i_ = 105;
    static short in_loop_command = 0;
    static short inter_loop = 0;
    static final byte j_ = 106;
    static final byte k_ = 107;
    static final byte l_ = 108;
    static final byte m_ = 109;
    static final byte n_ = 110;
    static final byte o_ = 111;
    static final byte p_ = 112;
    static short param = 0;
    static byte proslushka = -1;
    static final byte q_ = 113;
    static final byte r_ = 114;
    static final byte s_ = 115;
    static byte scr_fraza = 0;
    static byte scr_max_str = 0;
    static byte scr_scrol = 0;
    static short scr_simvol = 0;
    static short scr_start_y = 0;
    static byte scr_str = 0;
    static short[][] script = null;
    static short script_iter = 0;
    static final int stp = 16;
    static final byte t_ = 116;
    static final byte u_ = 117;
    static final byte v_ = 118;
    static final byte w_ = 119;
    static final byte x_ = 120;
    static final byte y_ = 121;
    static final byte z_ = 122;
    String[][] slova;

    public static final void run_script() {
        if (Menu.menu_type != 10 && Menu.menu_type != 17 && enable_script && script != null) {
            int len = script.length;
            if (inter_loop < 0) {
                inter_loop = 0;
                script_iter = (short) (script_iter + script[script_iter][2] + 1);
                Loop_script = false;
            }
            if (!Loop_script) {
                while (true) {
                    if (script_iter >= len) {
                        break;
                    } else if (!command((byte) script[script_iter][0], script[script_iter])) {
                        m3d.Units[0].room_now = m3d.Units[0].room_now;
                        m3d.room_now = m3d.room_now;
                        return;
                    } else if (Loop_script) {
                        break;
                    } else if (proslushka >= 0) {
                        script_iter = (short) (script_iter + 1);
                        break;
                    } else {
                        script_iter = (short) (script_iter + 1);
                    }
                }
            } else {
                for (int i = script_iter + 1; i <= in_loop_command; i++) {
                    command((byte) script[i][0], script[i]);
                }
                inter_loop = (short) (inter_loop - 1);
            }
            if (script_iter >= len - 1) {
                enable_script = false;
            }
            math.change_room_camera();
        }
    }

    public static final boolean command(byte com2, short[] pered) {
        switch (com2) {
            case 97:
                m3d.s[0] = pered[1] << 16;
                m3d.s[1] = pered[2] << 16;
                m3d.s[2] = pered[3] << 16;
                m3d.rot_x = pered[4];
                m3d.rot_z = pered[5];
                math.change_room_camera();
                break;
            case 98:
                unit u = m3d.Units[pered[1]];
                u.pos_x = pered[2] << 16;
                u.pos_y = pered[3] << 16;
                u.pos_z = pered[4] << 16;
                m3d.vec_speed[0] = 0;
                m3d.vec_speed[1] = 0;
                m3d.vec_speed[2] = 0;
                u.Speed[0] = 0;
                u.Speed[1] = 0;
                u.Speed[2] = 0;
                math.change_room(u);
                if (u.type_unit != 5) {
                    math.phi(u);
                    break;
                }
                break;
            case 99:
                object ob = m3d.Objects[pered[1]];
                ob.pos_x = pered[2] << 16;
                ob.pos_y = pered[3] << 16;
                ob.pos_z = pered[4] << 16;
                ob.r_x = pered[5];
                ob.r_z = pered[6];
                break;
            case Player.UNREALIZED:
                if (Game.my_level == 16) {
                    m3d.end();
                    break;
                } else {
                    m3d.next_level();
                    return false;
                }
            case 101:
                unit u2 = m3d.Units[pered[1]];
                u2.death = false;
                u2.visible = true;
                u2.take = false;
                u2.life = m3d.life_unit[u2.type_unit];
                u2.type_anim = 0;
                u2.animation = true;
                break;
            case 102:
                if (Game.my_level != 16) {
                    m3d.draw2D(m3d.ppp, m3d.room_now);
                    m3d.draw_3d(m3d.draw_loop);
                    break;
                } else {
                    m3d.rot_z += pered[1];
                    if (m3d.rot_z < 0) {
                        m3d.rot_z += 360;
                    }
                    if (m3d.rot_z > 359) {
                        m3d.rot_z -= 360;
                    }
                    m3d.s[0] = m3d.Objects[pered[3]].pos_x;
                    m3d.s[1] = m3d.Objects[pered[3]].pos_y;
                    m3d.s[2] = m3d.Objects[pered[3]].pos_z;
                    int[] iArr = m3d.s;
                    iArr[0] = iArr[0] + (math.sin[m3d.rot_z] * pered[2]);
                    int[] iArr2 = m3d.s;
                    iArr2[1] = iArr2[1] + (math.cos[m3d.rot_z] * pered[2]);
                    break;
                }
            case 103:
                for (int i = 0; i < m3d.units; i++) {
                    if (m3d.Units[i].room_now == pered[1]) {
                        m3d.Units[i].take = false;
                        m3d.Units[i].visible = true;
                    }
                }
                break;
            case 104:
                for (int i2 = 0; i2 < m3d.objects; i2++) {
                    m3d.Objects[i2].visible = true;
                }
                break;
            case 105:
                m3d.open_rooms[pered[1]] = (byte) pered[2];
                break;
            case 106:
                Loop_script = true;
                inter_loop = pered[1];
                in_loop_command = (short) (pered[2] + script_iter);
                break;
            case 107:
                unit u3 = m3d.Units[0];
                if (pered[1] != 0) {
                    m3d.rot_z += pered[2];
                    if (m3d.rot_z >= 360) {
                        m3d.rot_z -= 360;
                    }
                    if (m3d.rot_z < 0) {
                        m3d.rot_z += 360;
                    }
                    u3.rr[0] = -math.sin[m3d.rot_z];
                    u3.rr[1] = -math.cos[m3d.rot_z];
                    break;
                } else {
                    m3d.rot_x += pered[2];
                    if (m3d.rot_x >= 360) {
                        m3d.rot_x -= 360;
                    }
                    if (m3d.rot_x < 0) {
                        m3d.rot_x += 360;
                        break;
                    }
                }
                break;
            case 108:
                switch (pered[1]) {
                    case 0:
                        int[] iArr3 = m3d.s;
                        iArr3[0] = iArr3[0] + (((int) ((((long) (-math.sin[m3d.rot_z])) * ((long) (-math.sin[m3d.rot_x]))) >> 16)) * pered[2]);
                        int[] iArr4 = m3d.s;
                        iArr4[1] = iArr4[1] + (((int) ((((long) (-math.cos[m3d.rot_z])) * ((long) (-math.sin[m3d.rot_x]))) >> 16)) * pered[2]);
                        int[] iArr5 = m3d.s;
                        iArr5[2] = iArr5[2] + ((-math.cos[m3d.rot_x]) * pered[2]);
                        break;
                    case 1:
                        int[] iArr6 = m3d.s;
                        iArr6[0] = iArr6[0] - (math.cos[m3d.rot_z] * pered[2]);
                        int[] iArr7 = m3d.s;
                        iArr7[1] = iArr7[1] - (math.sin[m3d.rot_z] * pered[2]);
                        break;
                    case 2:
                        int[] iArr8 = m3d.s;
                        iArr8[2] = iArr8[2] + (pered[2] << 16);
                        break;
                }
            case 109:
                boolean cl = false;
                if (pered[1] == 0) {
                    cl = true;
                }
                int i3 = 0;
                while (true) {
                    if (i3 < m3d.objects) {
                        if (m3d.Objects[i3].type_object == 9 && !m3d.Objects[i3].color_obj) {
                            m3d.Objects[i3].color_obj = cl;
                            break;
                        } else {
                            i3++;
                        }
                    } else {
                        break;
                    }
                }
                break;
            case 110:
                poly.dark_c = (short) (poly.dark_c + pered[1]);
                if (poly.dark_c > 256) {
                    poly.dark_c = 256;
                }
                if (poly.dark_c < 0) {
                    poly.dark_c = 0;
                    break;
                }
                break;
            case 111:
                AI.AI_goto(m3d.Units[pered[1]], pered[2] << 16, pered[3] << 16, 45, 0);
                if (m3d.Units[pered[1]].type_unit == 4) {
                    m3d.Units[pered[1]].Fly = false;
                    break;
                }
                break;
            case 112:
                object ob2 = m3d.Objects[pered[1]];
                switch (pered[2]) {
                    case 1:
                        ob2.pos_x += pered[3] << 16;
                        break;
                    case 2:
                        ob2.pos_y += pered[3] << 16;
                        break;
                    case 3:
                        ob2.pos_z += pered[3] << 16;
                        break;
                }
            case 113:
                if (pered[1] != 0) {
                    m3d.white = (short) (m3d.white + pered[2]);
                    break;
                } else {
                    m3d.white = pered[2];
                    break;
                }
            case 114:
                proslushka = (byte) pered[1];
                param = pered[2];
                enable_script = false;
                break;
            case 116:
                if (pered[1] < 1) {
                    VISIBLE_ALL_UNIT(true, false);
                    break;
                } else {
                    VISIBLE_ALL_UNIT(false, true);
                    break;
                }
            case 117:
                if (pered[2] < 1) {
                    VISIBLE_ONE_UNIT(pered[1], true, false);
                    break;
                } else {
                    VISIBLE_ONE_UNIT(pered[1], false, true);
                    break;
                }
            case 118:
                for (int i4 = m3d.Rooms[pered[1]].start_obj; i4 < m3d.Rooms[pered[1]].end_obj; i4++) {
                    m3d.Objects[i4].visible = !m3d.Objects[i4].visible;
                }
                break;
            case 119:
                m3d.open_rooms[pered[1]] = 10;
                for (int i5 = m3d.Rooms[pered[1]].start_obj; i5 < m3d.Rooms[pered[1]].end_obj; i5++) {
                    if (m3d.Objects[i5].type_object == 40 || m3d.Objects[i5].type_object == 43) {
                        m3d.Objects[i5].pos_z += pered[2] << 16;
                    } else {
                        m3d.Objects[i5].pos_z -= pered[2] << 16;
                    }
                }
                break;
            case 120:
                object ob3 = m3d.Objects[pered[1]];
                ob3.pos_x += pered[2] << 16;
                ob3.pos_y += pered[3] << 16;
                ob3.pos_z += pered[4] << 16;
                ob3.r_x += pered[5];
                if (ob3.r_x >= 360) {
                    ob3.r_x -= 360;
                }
                if (ob3.r_x < 0) {
                    ob3.r_x += 360;
                }
                ob3.r_z += pered[6];
                if (ob3.r_z >= 360) {
                    ob3.r_z -= 360;
                }
                if (ob3.r_z < 0) {
                    ob3.r_z += 360;
                    break;
                }
                break;
            case 121:
                m3d.draw2D(m3d.ppp, m3d.room_now);
                m3d.draw_3d(m3d.draw_loop);
                NET_Lizard.notifyDestroyed.repaint();
                NET_Lizard.notifyDestroyed.serviceRepaints();
                break;
            case 122:
                if (pered[1] < 0) {
                    Menu.enable = true;
                    Menu.menu_type = 17;
                    Game.Pause = true;
                    Menu.SMS = 0;
                    Menu.scrol = 0;
                } else {
                    Menu.M((byte) pered[1]);
                }
                script_iter = (short) (script_iter + 1);
                enable_script = false;
                return false;
        }
        return true;
    }

    public static final void proslushka_scripta() {
        if (proslushka >= 0) {
            object[] ob = m3d.Objects;
            unit[] un = m3d.Units;
            switch (proslushka) {
                case 0:
                    if (m3d.room_now == param) {
                        enable_script = true;
                        proslushka = -1;
                        return;
                    }
                    return;
                case 1:
                    if (un[param].death) {
                        enable_script = true;
                        proslushka = -1;
                        for (int i = 0; i < ob.length; i++) {
                            if (ob[i].type_object >= 120) {
                                ob[i].pos_x = un[param].pos_x;
                                ob[i].pos_y = un[param].pos_y;
                                ob[i].pos_z = un[param].pos_z;
                                ob[i].room_now = un[param].room_now;
                                return;
                            }
                        }
                        return;
                    }
                    return;
                case 2:
                    param = (short) (param - 1);
                    if (param <= 0) {
                        enable_script = true;
                        proslushka = -1;
                        return;
                    }
                    return;
                case 3:
                    if (!ob[param].visible) {
                        enable_script = true;
                        proslushka = -1;
                        return;
                    }
                    return;
                case 4:
                    if (m3d.Units[0].pos_y < (param << 16)) {
                        enable_script = true;
                        proslushka = -1;
                        return;
                    }
                    return;
                case 5:
                    if (m3d.Units[0].pos_y > (param << 16)) {
                        enable_script = true;
                        proslushka = -1;
                        return;
                    }
                    return;
                case 6:
                    if (m3d.Units[0].pos_z > (param << 16)) {
                        enable_script = true;
                        proslushka = -1;
                        return;
                    }
                    return;
                case 7:
                    if (m3d.kills == param) {
                        enable_script = true;
                        proslushka = -1;
                        return;
                    }
                    return;
                case 8:
                    if (m3d.Units[0].pos_x > (param << 16)) {
                        enable_script = true;
                        proslushka = -1;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public static final void skip_scr() {
        while (enable_script) {
            run_script();
            for (int i = 0; i < m3d.units; i++) {
                if (m3d.Units[i].type_unit != 5) {
                    math.phi(m3d.Units[i]);
                }
            }
            math.stop_all();
            m3d.Shuts_hits();
            m3d.shot_delay();
            m3d.time_change();
            m3d.reload_FPS();
            m3d.smesh_FPS_weapon();
            m3d.rot_FPS();
            m3d.time_rot_obj();
            m3d.give_obj();
            m3d.open_door();
            m3d.Sprite_life();
        }
    }

    private static final void VISIBLE_ONE_UNIT(int num, boolean take, boolean vis) {
        m3d.Units[num].take = take;
        m3d.Units[num].visible = vis;
    }

    private static final void VISIBLE_ALL_UNIT(boolean take, boolean vis) {
        unit[] Un = m3d.Units;
        for (int i = 1; i < m3d.Units.length; i++) {
            if (!Un[i].death) {
                Un[i].take = take;
                Un[i].visible = vis;
            }
        }
    }

    private static final void VIS() {
        unit[] Un = m3d.Units;
        for (int i = 1; i < m3d.units; i++) {
            Un[i].visible = true;
        }
    }

    static final void delete_script() {
        script = null;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static final void load_script(int r20) {
        /*
            java.lang.String r2 = new java.lang.String
            r2.<init>()
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            java.lang.String r18 = java.lang.String.valueOf(r2)
            r17.<init>(r18)
            r18 = 115(0x73, float:1.61E-43)
            java.lang.StringBuilder r17 = r17.append(r18)
            java.lang.String r2 = r17.toString()
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            java.lang.String r18 = java.lang.String.valueOf(r2)
            r17.<init>(r18)
            r0 = r17
            r1 = r20
            java.lang.StringBuilder r17 = r0.append(r1)
            java.lang.String r2 = r17.toString()
            ua.netlizard.egypt.Uni r17 = ua.netlizard.egypt.Uni.uni
            r18 = -1
            r0 = r17
            r1 = r18
            byte[] r12 = r0.load_pack(r2, r1)
            r5 = 0
            r8 = 0
        L_0x003b:
            int r0 = r12.length
            r17 = r0
            r0 = r17
            if (r8 < r0) goto L_0x0050
            short[][] r0 = new short[r5][]
            r17 = r0
            ua.netlizard.egypt.scr.script = r17
            r9 = 0
            r10 = 0
            r16 = 1
            r8 = 0
        L_0x004d:
            if (r8 < r5) goto L_0x0062
            return
        L_0x0050:
            byte r17 = r12[r8]
            r18 = 13
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x005f
            int r17 = r5 + 1
            r0 = r17
            short r5 = (short) r0
        L_0x005f:
            int r8 = r8 + 1
            goto L_0x003b
        L_0x0062:
            r14 = 1
            r13 = r9
        L_0x0064:
            int r0 = r12.length
            r17 = r0
            r0 = r17
            if (r13 < r0) goto L_0x00cd
        L_0x006b:
            short[][] r17 = ua.netlizard.egypt.scr.script
            int r18 = r14 + 1
            r0 = r18
            short[] r0 = new short[r0]
            r18 = r0
            r17[r8] = r18
            short[][] r17 = ua.netlizard.egypt.scr.script
            r17 = r17[r8]
            r18 = 0
            byte r19 = r12[r9]
            r17[r18] = r19
            int r9 = r9 + 2
            r10 = 0
            r7 = 0
            r14 = 1
        L_0x0086:
            byte r17 = r12[r9]
            r18 = 44
            r0 = r17
            r1 = r18
            if (r0 == r1) goto L_0x009a
            byte r17 = r12[r9]
            r18 = 13
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x0126
        L_0x009a:
            r15 = 1
            int r17 = r10 - r7
            int r3 = java.lang.Math.abs(r17)
            r4 = 0
        L_0x00a2:
            int r3 = r3 + -1
            if (r3 > 0) goto L_0x00e7
            int r6 = r9 - r10
            r11 = 0
        L_0x00a9:
            if (r11 < r10) goto L_0x00ea
            short[][] r17 = ua.netlizard.egypt.scr.script
            r17 = r17[r8]
            int r18 = r4 * r16
            r0 = r18
            short r0 = (short) r0
            r18 = r0
            r17[r14] = r18
            r16 = 1
            int r14 = r14 + 1
            r10 = -1
            r7 = -1
            byte r17 = r12[r9]
            r18 = 13
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x0126
            int r9 = r9 + 2
            int r8 = r8 + 1
            goto L_0x004d
        L_0x00cd:
            byte r17 = r12[r13]
            r18 = 13
            r0 = r17
            r1 = r18
            if (r0 == r1) goto L_0x006b
            byte r17 = r12[r13]
            r18 = 44
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x00e3
            int r14 = r14 + 1
        L_0x00e3:
            int r13 = r13 + 1
            goto L_0x0064
        L_0x00e7:
            int r15 = r15 * 10
            goto L_0x00a2
        L_0x00ea:
            int r17 = r6 + r11
            byte r17 = r12[r17]
            r18 = 45
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x00fa
            int r11 = r11 + 1
            r16 = -1
        L_0x00fa:
            int r17 = r6 + r11
            byte r17 = r12[r17]
            r18 = 48
            r0 = r17
            r1 = r18
            if (r0 < r1) goto L_0x0112
            int r17 = r6 + r11
            byte r17 = r12[r17]
            r18 = 57
            r0 = r17
            r1 = r18
            if (r0 <= r1) goto L_0x0119
        L_0x0112:
            int r6 = r6 + 1
            int r11 = r11 + -1
        L_0x0116:
            int r11 = r11 + 1
            goto L_0x00a9
        L_0x0119:
            int r17 = r6 + r11
            byte r17 = r12[r17]
            int r17 = r17 + -48
            int r17 = r17 * r15
            int r4 = r4 + r17
            int r15 = r15 / 10
            goto L_0x0116
        L_0x0126:
            byte r17 = r12[r9]
            r18 = 48
            r0 = r17
            r1 = r18
            if (r0 < r1) goto L_0x013a
            byte r17 = r12[r9]
            r18 = 57
            r0 = r17
            r1 = r18
            if (r0 <= r1) goto L_0x013c
        L_0x013a:
            int r7 = r7 + 1
        L_0x013c:
            r17 = -1
            r0 = r17
            if (r7 != r0) goto L_0x0143
            r7 = 0
        L_0x0143:
            int r17 = r10 + 1
            r0 = r17
            byte r10 = (byte) r0
            int r9 = r9 + 1
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.scr.load_script(int):void");
    }

    static void restart_script() {
        proslushka = -1;
        anim_script = 0;
        enable_script = true;
        script_iter = 0;
        inter_loop = 0;
        in_loop_command = 0;
        Loop_script = false;
    }
}
