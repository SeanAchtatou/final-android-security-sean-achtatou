package ua.netlizard.egypt;

public final class portal {
    static final int dist = -2097152;
    static final int stp = 16;
    static final int stp_draw = 18;
    byte norm;
    point_[] p;
    short room1;
    short room2;
    boolean see;

    public portal() {
        this.see = false;
        this.p = new point_[4];
        this.see = false;
    }

    public static final int clipPlane2(point_[] dst, point_[] src, int[][] nn) {
        long t_1;
        long t_2;
        int[] distance = m3d.distance_H;
        int o = dist;
        byte num_point = 3;
        byte rr = 0;
        int[] kl = m3d.remainder;
        for (int j = 0; j < 5; j++) {
            int[] n = nn[j];
            int x = n[0];
            int y = n[1];
            int z = n[2];
            rr = 0;
            int i = 0;
            int cof = num_point * 9;
            while (i < num_point) {
                point_ p1 = src[i];
                int cof2 = cof + 1;
                point_ p2 = src[kl[cof]];
                point_ p3 = dst[rr];
                int t1 = (int) ((((((long) p1.rx) * ((long) x)) + (((long) p1.ry) * ((long) y))) + (((long) (p1.rz + o)) * ((long) z))) >> 22);
                int t2 = (int) ((((((long) p2.rx) * ((long) x)) + (((long) p2.ry) * ((long) y))) + (((long) (p2.rz + o)) * ((long) z))) >> 22);
                if (t1 >= 0) {
                    p3.rx = p1.rx;
                    p3.ry = p1.ry;
                    p3.rz = p1.rz;
                    p3.u = p1.u;
                    p3.v = p1.v;
                    rr = (byte) (rr + 1);
                    p3 = dst[rr];
                }
                if (t2 - t1 > 0) {
                    int p4 = distance[t2 - t1];
                    t_1 = (long) (t2 * p4);
                    t_2 = (long) ((-t1) * p4);
                } else {
                    int p5 = distance[t1 - t2];
                    t_1 = (long) ((-t2) * p5);
                    t_2 = (long) (t1 * p5);
                }
                if ((t1 > 0 && t2 < 0) || (t2 > 0 && t1 < 0)) {
                    p3.rx = (int) (((((long) p1.rx) * t_1) + (((long) p2.rx) * t_2)) >> 18);
                    p3.ry = (int) (((((long) p1.ry) * t_1) + (((long) p2.ry) * t_2)) >> 18);
                    p3.rz = (int) (((((long) p1.rz) * t_1) + (((long) p2.rz) * t_2)) >> 18);
                    p3.u = (short) ((int) (((((long) p1.u) * t_1) + (((long) p2.u) * t_2)) >> 18));
                    p3.v = (short) ((int) (((((long) p1.v) * t_1) + (((long) p2.v) * t_2)) >> 18));
                    rr = (byte) (rr + 1);
                }
                i++;
                cof = cof2;
            }
            if (rr == 0) {
                return 0;
            }
            if (j == 0) {
                o = 0;
            }
            point_[] tmp = dst;
            dst = src;
            src = tmp;
            num_point = rr;
        }
        return rr;
    }

    public static final int clipPlaney(point_[] dst, point_[] src, int n, int num_point) {
        long t_1;
        long t_2;
        int[] distance = m3d.distance_H;
        int rr = 0;
        int cof = num_point * 9;
        int[] kl = m3d.remainder;
        int obzs = m3d.obzor;
        for (int i = 0; i < num_point; i++) {
            point_ p1 = src[i];
            point_ p2 = src[kl[cof + i]];
            int t1 = (int) (((((long) p1.ry) * ((long) obzs)) + (((long) p1.rz) * ((long) n))) >> 24);
            int t2 = (int) (((((long) p2.ry) * ((long) obzs)) + (((long) p2.rz) * ((long) n))) >> 24);
            point_ p3 = dst[rr];
            if (t1 >= 0) {
                p3.rx = p1.rx;
                p3.ry = p1.ry;
                p3.rz = p1.rz;
                p3.u = p1.u;
                p3.v = p1.v;
                rr++;
                p3 = dst[rr];
            }
            if (t2 - t1 > 0) {
                t_1 = (long) (distance[t2 - t1] * t2);
                t_2 = (long) ((-t1) * distance[t2 - t1]);
            } else {
                t_1 = (long) ((-t2) * distance[t1 - t2]);
                t_2 = (long) (distance[t1 - t2] * t1);
            }
            if ((t1 > 0 && t2 < 0) || (t2 > 0 && t1 < 0)) {
                p3.rx = (int) (((((long) p1.rx) * t_1) + (((long) p2.rx) * t_2)) >> 18);
                p3.ry = (int) (((((long) p1.ry) * t_1) + (((long) p2.ry) * t_2)) >> 18);
                p3.rz = (int) (((((long) p1.rz) * t_1) + (((long) p2.rz) * t_2)) >> 18);
                p3.u = (short) ((int) (((((long) p1.u) * t_1) + (((long) p2.u) * t_2)) >> 18));
                p3.v = (short) ((int) (((((long) p1.v) * t_1) + (((long) p2.v) * t_2)) >> 18));
                rr++;
            }
        }
        return rr;
    }

    public static final int clipPlane_y(point_[] dst, point_[] src, int n, int num_point) {
        long t_1;
        long t_2;
        int[] distance = m3d.distance_H;
        int rr = 0;
        int cof = num_point * 9;
        int[] kl = m3d.remainder;
        int obzs = m3d.obzor;
        for (int i = 0; i < num_point; i++) {
            point_ p1 = src[i];
            point_ p2 = src[kl[cof + i]];
            int t1 = (int) (((((long) p1.ry) * ((long) (-obzs))) + (((long) p1.rz) * ((long) n))) >> 24);
            int t2 = (int) (((((long) p2.ry) * ((long) (-obzs))) + (((long) p2.rz) * ((long) n))) >> 24);
            point_ p3 = dst[rr];
            if (t1 >= 0) {
                p3.rx = p1.rx;
                p3.ry = p1.ry;
                p3.rz = p1.rz;
                p3.u = p1.u;
                p3.v = p1.v;
                rr++;
                p3 = dst[rr];
            }
            if (t2 - t1 > 0) {
                t_1 = (long) (distance[t2 - t1] * t2);
                t_2 = (long) ((-t1) * distance[t2 - t1]);
            } else {
                t_1 = (long) ((-t2) * distance[t1 - t2]);
                t_2 = (long) (distance[t1 - t2] * t1);
            }
            if ((t1 > 0 && t2 < 0) || (t2 > 0 && t1 < 0)) {
                p3.rx = (int) (((((long) p1.rx) * t_1) + (((long) p2.rx) * t_2)) >> 18);
                p3.ry = (int) (((((long) p1.ry) * t_1) + (((long) p2.ry) * t_2)) >> 18);
                p3.rz = (int) (((((long) p1.rz) * t_1) + (((long) p2.rz) * t_2)) >> 18);
                p3.u = (short) ((int) (((((long) p1.u) * t_1) + (((long) p2.u) * t_2)) >> 18));
                p3.v = (short) ((int) (((((long) p1.v) * t_1) + (((long) p2.v) * t_2)) >> 18));
                rr++;
            }
        }
        return rr;
    }

    public static final int clipPlane_x(point_[] dst, point_[] src, int n, int num_point) {
        long t_1;
        long t_2;
        int[] distance = m3d.distance_H;
        int rr = 0;
        int cof = num_point * 9;
        int[] kl = m3d.remainder;
        int obzs = m3d.obzor;
        for (int i = 0; i < num_point; i++) {
            point_ p1 = src[i];
            point_ p2 = src[kl[cof + i]];
            int t1 = (int) (((((long) p1.rx) * ((long) (-obzs))) + (((long) p1.rz) * ((long) n))) >> 24);
            int t2 = (int) (((((long) p2.rx) * ((long) (-obzs))) + (((long) p2.rz) * ((long) n))) >> 24);
            point_ p3 = dst[rr];
            if (t1 >= 0) {
                p3.rx = p1.rx;
                p3.ry = p1.ry;
                p3.rz = p1.rz;
                p3.u = p1.u;
                p3.v = p1.v;
                rr++;
                p3 = dst[rr];
            }
            if (t2 - t1 > 0) {
                t_1 = (long) (distance[t2 - t1] * t2);
                t_2 = (long) ((-t1) * distance[t2 - t1]);
            } else {
                t_1 = (long) ((-t2) * distance[t1 - t2]);
                t_2 = (long) (distance[t1 - t2] * t1);
            }
            if ((t1 > 0 && t2 < 0) || (t2 > 0 && t1 < 0)) {
                p3.rx = (int) (((((long) p1.rx) * t_1) + (((long) p2.rx) * t_2)) >> 18);
                p3.ry = (int) (((((long) p1.ry) * t_1) + (((long) p2.ry) * t_2)) >> 18);
                p3.rz = (int) (((((long) p1.rz) * t_1) + (((long) p2.rz) * t_2)) >> 18);
                p3.u = (short) ((int) (((((long) p1.u) * t_1) + (((long) p2.u) * t_2)) >> 18));
                p3.v = (short) ((int) (((((long) p1.v) * t_1) + (((long) p2.v) * t_2)) >> 18));
                rr++;
            }
        }
        return rr;
    }

    public static final int clipPlanex(point_[] dst, point_[] src, int n, int num_point) {
        long t_1;
        long t_2;
        int[] distance = m3d.distance_H;
        int rr = 0;
        int cof = num_point * 9;
        int[] kl = m3d.remainder;
        int obzs = m3d.obzor;
        for (int i = 0; i < num_point; i++) {
            point_ p1 = src[i];
            point_ p2 = src[kl[cof + i]];
            int t1 = (int) (((((long) p1.rx) * ((long) obzs)) + (((long) p1.rz) * ((long) n))) >> 24);
            int t2 = (int) (((((long) p2.rx) * ((long) obzs)) + (((long) p2.rz) * ((long) n))) >> 24);
            point_ p3 = dst[rr];
            if (t1 >= 0) {
                p3.rx = p1.rx;
                p3.ry = p1.ry;
                p3.rz = p1.rz;
                p3.u = p1.u;
                p3.v = p1.v;
                rr++;
                p3 = dst[rr];
            }
            if (t2 - t1 > 0) {
                t_1 = (long) (distance[t2 - t1] * t2);
                t_2 = (long) ((-t1) * distance[t2 - t1]);
            } else {
                t_1 = (long) ((-t2) * distance[t1 - t2]);
                t_2 = (long) (distance[t1 - t2] * t1);
            }
            if ((t1 > 0 && t2 < 0) || (t2 > 0 && t1 < 0)) {
                p3.rx = (int) (((((long) p1.rx) * t_1) + (((long) p2.rx) * t_2)) >> 18);
                p3.ry = (int) (((((long) p1.ry) * t_1) + (((long) p2.ry) * t_2)) >> 18);
                p3.rz = (int) (((((long) p1.rz) * t_1) + (((long) p2.rz) * t_2)) >> 18);
                p3.u = (short) ((int) (((((long) p1.u) * t_1) + (((long) p2.u) * t_2)) >> 18));
                p3.v = (short) ((int) (((((long) p1.v) * t_1) + (((long) p2.v) * t_2)) >> 18));
                rr++;
            }
        }
        return rr;
    }
}
