package ua.netlizard.egypt;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;

public class DurakActivity extends Activity {
    private GameView mGameView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window win = getWindow();
        int screenHeight = win.getWindowManager().getDefaultDisplay().getHeight();
        int screenWidth = win.getWindowManager().getDefaultDisplay().getWidth();
        win.setFlags(1024, 1024);
        requestWindowFeature(1);
        requestWindowFeature(2);
        this.mGameView = new GameView(this);
        setContentView(this.mGameView);
        setVolumeControlStream(3);
        this.mGameView.start(screenWidth, screenHeight);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.mGameView.onKeyDown(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (this.mGameView.onKeyUp(keyCode, event)) {
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    public void onPause() {
        try {
            this.mGameView.onPause();
        } catch (Exception e) {
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mGameView.onResume();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mGameView.onDestroy();
        super.onDestroy();
    }
}
