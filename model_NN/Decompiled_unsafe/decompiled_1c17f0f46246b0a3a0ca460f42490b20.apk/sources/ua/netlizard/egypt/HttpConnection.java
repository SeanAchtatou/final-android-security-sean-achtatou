package ua.netlizard.egypt;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;

public class HttpConnection extends Connection {
    public static String GET = "GET";
    public static String POST = "POST";
    HttpURLConnection hurl_con;

    HttpConnection(HttpURLConnection con) {
        this.hurl_con = con;
    }

    /* access modifiers changed from: package-private */
    public void setRequestMethod(String method) throws ProtocolException {
        this.hurl_con.setRequestMethod(method);
    }

    /* access modifiers changed from: package-private */
    public int getLength() {
        return this.hurl_con.getContentLength();
    }

    /* access modifiers changed from: package-private */
    public InputStream openInputStream() throws IOException {
        return this.hurl_con.getInputStream();
    }

    /* access modifiers changed from: package-private */
    public void close() {
        this.hurl_con.disconnect();
    }
}
