package ua.netlizard.egypt;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import java.io.InputStream;

public class Image {
    static final boolean Image_dexor = true;
    static final String TAG = "IMAGE";
    static int del_old;
    static int delit_p = 24;
    static long mnj_p;
    static int tstRgbA = 0;
    static int xor_start = 100;
    private Bitmap m_Bitmap;
    private int m_Height = this.m_Bitmap.getHeight();
    private int m_Width = this.m_Bitmap.getWidth();

    private Image(Bitmap bmp) {
        this.m_Bitmap = bmp;
    }

    public Bitmap getBitmap() {
        return this.m_Bitmap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final ua.netlizard.egypt.Image createImage(java.lang.String r7) throws java.io.IOException {
        /*
            if (r7 != 0) goto L_0x0008
            java.lang.NullPointerException r4 = new java.lang.NullPointerException
            r4.<init>()
            throw r4
        L_0x0008:
            r1 = 0
            java.io.InputStream r3 = ua.netlizard.egypt.AndroidUtils.getResourceAsStream(r7)     // Catch:{ Exception -> 0x0055 }
            ua.netlizard.egypt.Image r2 = new ua.netlizard.egypt.Image     // Catch:{ Exception -> 0x0055 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ Exception -> 0x0055 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0055 }
        L_0x0016:
            if (r2 != 0) goto L_0x0062
            byte[] r0 = ua.netlizard.egypt.AndroidUtils.getResourceAsArray(r7)
            if (r0 == 0) goto L_0x0062
            ua.netlizard.egypt.Image r1 = new ua.netlizard.egypt.Image     // Catch:{ Exception -> 0x0060 }
            int r4 = ua.netlizard.egypt.Image.xor_start     // Catch:{ Exception -> 0x0060 }
            byte[] r4 = deXorI(r0, r4)     // Catch:{ Exception -> 0x0060 }
            r5 = 0
            int r6 = r0.length     // Catch:{ Exception -> 0x0060 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeByteArray(r4, r5, r6)     // Catch:{ Exception -> 0x0060 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0060 }
            r2 = r1
        L_0x0030:
            if (r2 != 0) goto L_0x0062
            int r4 = ua.netlizard.egypt.Image.xor_start     // Catch:{ Exception -> 0x0058 }
            deXorI(r0, r4)     // Catch:{ Exception -> 0x0058 }
            ua.netlizard.egypt.Image r1 = new ua.netlizard.egypt.Image     // Catch:{ Exception -> 0x0058 }
            r4 = 2
            byte r4 = r0[r4]     // Catch:{ Exception -> 0x0058 }
            int r4 = r4 + -3
            byte[] r4 = deXor(r0, r4)     // Catch:{ Exception -> 0x0058 }
            r5 = 3
            int r6 = r0.length     // Catch:{ Exception -> 0x0058 }
            int r6 = r6 + -3
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeByteArray(r4, r5, r6)     // Catch:{ Exception -> 0x0058 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0058 }
        L_0x004d:
            if (r1 != 0) goto L_0x005b
            java.io.IOException r4 = new java.io.IOException
            r4.<init>()
            throw r4
        L_0x0055:
            r4 = move-exception
            r2 = r1
            goto L_0x0016
        L_0x0058:
            r4 = move-exception
            r1 = r2
            goto L_0x004d
        L_0x005b:
            ua.netlizard.egypt.Image r1 = resize(r1)
            return r1
        L_0x0060:
            r4 = move-exception
            goto L_0x0030
        L_0x0062:
            r1 = r2
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.Image.createImage(java.lang.String):ua.netlizard.egypt.Image");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final ua.netlizard.egypt.Image createImage(byte[] r6, int r7, int r8) {
        /*
            if (r6 != 0) goto L_0x0008
            java.lang.NullPointerException r3 = new java.lang.NullPointerException
            r3.<init>()
            throw r3
        L_0x0008:
            int r3 = r7 + r8
            int r4 = r6.length
            if (r3 <= r4) goto L_0x0013
            java.lang.ArrayIndexOutOfBoundsException r3 = new java.lang.ArrayIndexOutOfBoundsException
            r3.<init>()
            throw r3
        L_0x0013:
            r1 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeByteArray(r6, r7, r8)     // Catch:{ Exception -> 0x0058 }
            ua.netlizard.egypt.Image r2 = new ua.netlizard.egypt.Image     // Catch:{ Exception -> 0x0058 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0058 }
        L_0x001d:
            if (r2 != 0) goto L_0x0065
            if (r6 == 0) goto L_0x0065
            ua.netlizard.egypt.Image r1 = new ua.netlizard.egypt.Image     // Catch:{ Exception -> 0x0063 }
            int r3 = ua.netlizard.egypt.Image.xor_start     // Catch:{ Exception -> 0x0063 }
            byte[] r3 = deXorI(r6, r3)     // Catch:{ Exception -> 0x0063 }
            r4 = 0
            int r5 = r6.length     // Catch:{ Exception -> 0x0063 }
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeByteArray(r3, r4, r5)     // Catch:{ Exception -> 0x0063 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0063 }
            r2 = r1
        L_0x0033:
            if (r2 != 0) goto L_0x0065
            int r3 = ua.netlizard.egypt.Image.xor_start     // Catch:{ Exception -> 0x005b }
            deXorI(r6, r3)     // Catch:{ Exception -> 0x005b }
            ua.netlizard.egypt.Image r1 = new ua.netlizard.egypt.Image     // Catch:{ Exception -> 0x005b }
            r3 = 2
            byte r3 = r6[r3]     // Catch:{ Exception -> 0x005b }
            int r3 = r3 + -3
            byte[] r3 = deXor(r6, r3)     // Catch:{ Exception -> 0x005b }
            r4 = 3
            int r5 = r6.length     // Catch:{ Exception -> 0x005b }
            int r5 = r5 + -3
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeByteArray(r3, r4, r5)     // Catch:{ Exception -> 0x005b }
            r1.<init>(r3)     // Catch:{ Exception -> 0x005b }
        L_0x0050:
            if (r1 != 0) goto L_0x005e
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            r3.<init>()
            throw r3
        L_0x0058:
            r3 = move-exception
            r2 = r1
            goto L_0x001d
        L_0x005b:
            r3 = move-exception
            r1 = r2
            goto L_0x0050
        L_0x005e:
            ua.netlizard.egypt.Image r1 = resize(r1)
            return r1
        L_0x0063:
            r3 = move-exception
            goto L_0x0033
        L_0x0065:
            r1 = r2
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.Image.createImage(byte[], int, int):ua.netlizard.egypt.Image");
    }

    public static final byte[] deXor(byte[] m, int sor) {
        int len = m.length;
        byte xor = (byte) sor;
        for (int n = 0; n < len; n++) {
            m[n] = (byte) (m[n] ^ xor);
            xor = (byte) (xor + 1);
        }
        return m;
    }

    public static final byte[] deXorI(byte[] inp, int xor) {
        int len = inp.length;
        for (int n = 0; n < len; n++) {
            inp[n] = (byte) (inp[n] ^ xor);
            xor++;
            if (xor > 255) {
                xor = 0;
            }
        }
        return inp;
    }

    static Image createDexor3(byte[] data, int offset, int length) {
        Image img = null;
        try {
            img = createImage(deXor(data, xor_start), offset, length);
        } catch (Exception e) {
        }
        if (img != null) {
            return img;
        }
        try {
            return createImage(deXorI(data, data[2] - 3), offset + 3, length - 3);
        } catch (Exception e2) {
            return img;
        }
    }

    static Image resize(Image img) {
        if (AndroidUtils.res_type != 2 || img == null) {
            return img;
        }
        int index = AndroidUtils.res.idx;
        int res_w = AndroidUtils.res.w[index];
        int res_h = AndroidUtils.res.h[index];
        if (res_w == AndroidUtils.drawWidth && res_h == AndroidUtils.drawHeight) {
            return img;
        }
        try {
            return resizeImage(img, 0, 0, img.getWidth(), img.getHeight(), (AndroidUtils.drawWidth * img.getWidth()) / res_w, (AndroidUtils.drawHeight * img.getHeight()) / res_h, 0, Image_dexor, Image_dexor);
        } catch (Exception e) {
            return img;
        }
    }

    public static final Image createImage(Image image) {
        return new Image(Bitmap.createBitmap(image.m_Bitmap));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static final Image createImage(Image image, int x, int y, int width, int height, int transform) {
        Matrix mat = new Matrix();
        mat.reset();
        return new Image(Bitmap.createBitmap(image.m_Bitmap, x, y, width, height, mat, false));
    }

    public static final Image createImage(InputStream input) {
        return new Image(BitmapFactory.decodeStream(input));
    }

    public static final Image createImage(int w, int h) {
        return new Image(Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888));
    }

    public static final Image createRGBImage(int[] rgb, int width, int height, boolean processAlpha) {
        if (!processAlpha) {
            for (int i = rgb.length - 1; i >= 0; i--) {
                rgb[i] = (rgb[i] & 16777215) | -16777216;
            }
        }
        return new Image(Bitmap.createBitmap(rgb, width, height, Bitmap.Config.ARGB_8888));
    }

    public final Graphics getGraphics() {
        return new Graphics(this.m_Bitmap);
    }

    public final int getHeight() {
        return this.m_Height;
    }

    public final int getWidth() {
        return this.m_Width;
    }

    public final void getRGB(int[] rgb, int offset, int scaleLength, int x, int y, int w, int h) {
        this.m_Bitmap.getPixels(rgb, offset, scaleLength, x, y, w, h);
    }

    public final boolean isMutable() {
        return this.m_Bitmap.isMutable();
    }

    static final boolean testRgbAlfa() {
        if (tstRgbA == 0) {
            int[] tt1 = new int[4];
            for (int i = 0; i < tt1.length; i++) {
                tt1[i] = -2130706433;
            }
            Image ti1 = createRGBImage(tt1, 2, 2, Image_dexor);
            Image ti2 = createImage(2, 2);
            Graphics g2 = ti2.getGraphics();
            g2.setClip(0, 0, 2, 2);
            g2.setColor(0);
            g2.fillRect(0, 0, 2, 2);
            g2.drawImage(ti1, 0, 0, 20);
            int[] tt2 = new int[4];
            ti2.getRGB(tt2, 0, 2, 0, 0, 2, 2);
            try {
                if ((tt2[0] & 255) <= 16 || (tt2[0] & 255) >= 224 || Display.getDisplay(NET_Lizard.instance).numAlphaLevels() <= 2) {
                    tstRgbA = -1;
                } else {
                    tstRgbA = 1;
                }
            } catch (Exception e) {
                tstRgbA = -1;
            }
        }
        if (tstRgbA > 0) {
            return Image_dexor;
        }
        return false;
    }

    static final Image resizeImage(Image in, int x, int y, int wi, int hi, int wo, int ho, int transform, boolean processAlpha, boolean sup) {
        return resizeImage(in, x, y, wi, hi, wo, ho, transform, processAlpha, sup, -1);
    }

    static final Image resizeImage(Image in, int x, int y, int wi, int hi, int wo, int ho, int transform, boolean processAlpha, boolean sup, int defcol) {
        boolean alfa2 = false;
        if (in.isMutable()) {
            processAlpha = false;
        }
        boolean ismut = in.isMutable();
        if (x == 0 && y == 0 && wo == wi && ho == hi && transform == 0) {
            return in;
        }
        if (processAlpha) {
            try {
                alfa2 = testRgbAlfa();
            } catch (Exception e) {
            }
        }
        int[] sprite = createImageInt(in, x, y, wi, hi, transform, processAlpha);
        if (!(wo == wi && ho == hi)) {
            sprite = resizeImageInt(sprite, wi, hi, wo, ho, sup, processAlpha, Image_dexor);
        }
        if (processAlpha && !alfa2 && sup) {
            corect_noalfa(sprite, defcol);
        }
        Image otp = createRGBImage(sprite, wo, ho, processAlpha);
        if (ismut) {
            otp = mutImage(otp);
        }
        return otp;
    }

    static final Image mutImage(Image inp) {
        if (inp == null || inp.isMutable()) {
            return null;
        }
        try {
            Image otp = createImage(inp.getWidth(), inp.getHeight());
            Graphics gh = otp.getGraphics();
            gh.setClip(0, 0, inp.getWidth(), inp.getHeight());
            gh.setColor(0);
            gh.fillRect(0, 0, inp.getWidth(), inp.getHeight());
            gh.drawImage(inp, 0, 0, 20);
            return otp;
        } catch (OutOfMemoryError e) {
            return inp;
        }
    }

    static int asmm(int i) {
        if (i < 0) {
            return i + 256;
        }
        return i;
    }

    static final int corect_noalfa(int dat, int defcol) {
        int A = (dat >> 24) & 255;
        if (A <= 0 || A >= 255) {
            return dat;
        }
        if (defcol >= 0) {
            int a0 = 256 - A;
            int a1 = A;
            int sprcol = dat;
            int r = (int) ((((long) (((defcol >> 16) & 255) * a0)) + ((long) (((sprcol >> 16) & 255) * a1))) >> 8);
            int g = (int) ((((long) (((defcol >> 8) & 255) * a0)) + ((long) (((sprcol >> 8) & 255) * a1))) >> 8);
            int b = (int) ((((long) ((defcol & 255) * a0)) + ((long) ((sprcol & 255) * a1))) >> 8);
            if (A < 8) {
                return 0;
            }
            return -16777216 | (r << 16) | (g << 8) | b;
        } else if (A <= 64) {
            return 0;
        } else {
            return dat | -16777216;
        }
    }

    static final void corect_noalfa(int[] sprite, int defcol) {
        for (int i = 0; i < sprite.length; i++) {
            sprite[i] = corect_noalfa(sprite[i], defcol);
        }
    }

    static final int deleniye(int a, int del) {
        return a / del;
    }

    static final int[] resizeImageInt(int[] inp, int wi, int hi, int wo, int ho, boolean sup, boolean processAlpha, boolean alfa2) {
        return resizeImageInt(inp, wi, hi, wo, ho, sup, processAlpha, alfa2, Image_dexor);
    }

    static final int[] resizeImageInt(int[] inp, int wi, int hi, int wo, int ho, boolean sup, boolean processAlpha, boolean alfa2, boolean rezk) {
        int[] tmp;
        if ((wo == wi && ho == hi) || inp == null) {
            return inp;
        }
        int defrezk = 0;
        int defrezk2 = 0;
        int defrezk3 = 0;
        int w = wo;
        int h = hi;
        if (w == wi && h == hi) {
            tmp = inp;
        } else {
            if (rezk) {
                int k = (w * 256) / wi;
                if (wi < w) {
                    k = (wi * 256) / w;
                }
                defrezk = (k * 15) >> 5;
                defrezk2 = defrezk;
                defrezk3 = defrezk;
            }
            tmp = new int[(w * h)];
            for (int y = 0; y < h; y++) {
                int xio = -1;
                int wp = w - 1;
                int prco = 0;
                for (int x = 0; x < w; x++) {
                    int xi = deleniye((x + 1) * wi, w) - 1;
                    if (xi < 0) {
                        xi = 0;
                    }
                    if (x == wp) {
                        xi = wi - 1;
                    }
                    if (xi >= wi) {
                        xi = wi - 1;
                    }
                    int kv = xi - xio;
                    if (sup) {
                        long R = 0;
                        long G = 0;
                        long B = 0;
                        long A = 0;
                        if (kv <= 1 || (kv == 2 && prco > 0)) {
                            int prc = deleniye((x + 1) * 256 * wi, w) & 255;
                            prco = prc;
                            int i = xi;
                            int pix0 = inp[(wi * y) + i];
                            if (i < wi - 1) {
                                i++;
                            }
                            tmp[(y * w) + x] = usredn_pix(pix0, inp[(wi * y) + i], rezk(prc, defrezk), processAlpha, alfa2);
                        } else {
                            int i2 = xio + 1;
                            while (i2 <= xi) {
                                int adri = (wi * y) + i2;
                                int tmpc = inp[adri];
                                if (i2 == xio + 1 && prco > 0) {
                                    int pix02 = inp[adri];
                                    if (i2 < wi - 1) {
                                        i2++;
                                    }
                                    adri = (wi * y) + i2;
                                    tmpc = usredn_pix(pix02, inp[adri], rezk(256, defrezk2), processAlpha, alfa2);
                                    int np = 1;
                                    if (processAlpha) {
                                        np = (tmpc >> 24) & 255;
                                        A += (long) np;
                                    }
                                    R += (long) (((tmpc >> 16) & 255) * np);
                                    G += (long) (((tmpc >> 8) & 255) * np);
                                    B += (long) ((tmpc & 255) * np);
                                }
                                if (i2 == xi) {
                                    int prc2 = deleniye((x + 1) * 256 * wi, w) & 255;
                                    prco = prc2;
                                    int pix03 = inp[adri];
                                    if (i2 < wi - 1) {
                                        i2++;
                                    }
                                    tmpc = usredn_pix(pix03, inp[(wi * y) + i2], rezk(prc2, defrezk3), processAlpha, alfa2);
                                }
                                int np2 = 1;
                                if (processAlpha) {
                                    np2 = (tmpc >> 24) & 255;
                                    A += (long) np2;
                                }
                                R += (long) (((tmpc >> 16) & 255) * np2);
                                G += (long) (((tmpc >> 8) & 255) * np2);
                                B += (long) ((tmpc & 255) * np2);
                                i2++;
                            }
                            if (kv == 2 && !processAlpha) {
                                if (processAlpha) {
                                    A >>= 1;
                                }
                                R >>= 1;
                                G >>= 1;
                                B >>= 1;
                            } else if (kv <= 0) {
                                A = 0;
                            } else if (processAlpha) {
                                if (A > 0) {
                                    R /= A;
                                    G /= A;
                                    B /= A;
                                }
                                A /= (long) kv;
                            } else {
                                R /= (long) kv;
                                G /= (long) kv;
                                B /= (long) kv;
                            }
                            if (alfa2) {
                                tmp[(y * w) + x] = (int) ((A << 24) | (R << 16) | (G << 8) | B);
                            } else if (A >= 128 || !processAlpha) {
                                tmp[(y * w) + x] = (int) (-16777216 | (R << 16) | (G << 8) | B);
                            } else {
                                tmp[(y * w) + x] = 0;
                            }
                        }
                    } else {
                        tmp[(y * w) + x] = inp[(y * wi) + xi];
                    }
                    xio = xi;
                }
            }
        }
        if (wo == w && ho == h) {
            return tmp;
        }
        int[] otp = new int[(wo * ho)];
        if (rezk) {
            int k2 = (h * 256) / ho;
            if (ho < h) {
                k2 = (ho * 256) / h;
            }
            defrezk = (k2 * 15) >> 5;
            defrezk2 = defrezk;
            defrezk3 = defrezk;
        }
        for (int x2 = 0; x2 < wo; x2++) {
            int yio = -1;
            int hop = ho - 1;
            int prco2 = 0;
            for (int y2 = 0; y2 < ho; y2++) {
                int yi = deleniye((y2 + 1) * h, ho) - 1;
                if (yi < 0) {
                    yi = 0;
                }
                if (y2 == hop) {
                    yi = h - 1;
                }
                if (yi >= h) {
                    yi = h - 1;
                }
                int kv2 = yi - yio;
                if (sup) {
                    long R2 = 0;
                    long G2 = 0;
                    long B2 = 0;
                    long A2 = 0;
                    if (kv2 <= 1 || (kv2 == 2 && prco2 > 0)) {
                        int prc3 = deleniye((y2 + 1) * 256 * h, ho) & 255;
                        prco2 = prc3;
                        int i3 = yi;
                        int pix04 = tmp[(w * i3) + x2];
                        if (i3 < h - 1) {
                            i3++;
                        }
                        otp[(y2 * wo) + x2] = usredn_pix(pix04, tmp[(w * i3) + x2], rezk(prc3, defrezk), processAlpha, alfa2);
                    } else {
                        int i4 = yio + 1;
                        while (i4 <= yi) {
                            int adri2 = (w * i4) + x2;
                            int tmpc2 = tmp[adri2];
                            if (i4 == yio + 1 && prco2 > 0) {
                                int pix05 = tmp[adri2];
                                if (i4 < h - 1) {
                                    i4++;
                                }
                                adri2 = (w * i4) + x2;
                                tmpc2 = usredn_pix(pix05, tmp[adri2], rezk(256, defrezk2), processAlpha, alfa2);
                                int np3 = 1;
                                if (processAlpha) {
                                    np3 = (tmpc2 >> 24) & 255;
                                    A2 += (long) np3;
                                }
                                R2 += (long) (((tmpc2 >> 16) & 255) * np3);
                                G2 += (long) (((tmpc2 >> 8) & 255) * np3);
                                B2 += (long) ((tmpc2 & 255) * np3);
                            }
                            if (i4 == yi) {
                                int prc4 = deleniye((y2 + 1) * 256 * h, ho) & 255;
                                prco2 = prc4;
                                int pix06 = tmp[adri2];
                                if (i4 < h - 1) {
                                    i4++;
                                }
                                tmpc2 = usredn_pix(pix06, tmp[(w * i4) + x2], rezk(prc4, defrezk3), processAlpha, alfa2);
                            }
                            int np4 = 1;
                            if (processAlpha) {
                                np4 = (tmpc2 >> 24) & 255;
                                A2 += (long) np4;
                            }
                            R2 += (long) (((tmpc2 >> 16) & 255) * np4);
                            G2 += (long) (((tmpc2 >> 8) & 255) * np4);
                            B2 += (long) ((tmpc2 & 255) * np4);
                            i4++;
                        }
                        if (kv2 == 2 && !processAlpha) {
                            if (processAlpha) {
                                A2 >>= 1;
                            }
                            R2 >>= 1;
                            G2 >>= 1;
                            B2 >>= 1;
                        } else if (kv2 <= 0) {
                            A2 = 0;
                        } else if (processAlpha) {
                            if (A2 > 0) {
                                R2 /= A2;
                                G2 /= A2;
                                B2 /= A2;
                            }
                            A2 /= (long) kv2;
                        } else {
                            R2 /= (long) kv2;
                            G2 /= (long) kv2;
                            B2 /= (long) kv2;
                        }
                        if (alfa2) {
                            otp[(y2 * wo) + x2] = (int) ((A2 << 24) | (R2 << 16) | (G2 << 8) | B2);
                        } else if (A2 >= 128 || !processAlpha) {
                            otp[(y2 * wo) + x2] = (int) (-16777216 | (R2 << 16) | (G2 << 8) | B2);
                        } else {
                            otp[(y2 * wo) + x2] = 0;
                        }
                    }
                } else {
                    otp[(y2 * wo) + x2] = tmp[(yi * w) + x2];
                }
                yio = yi;
            }
        }
        return otp;
    }

    static final int rezk(int prc, int defrezk) {
        if (prc <= defrezk) {
            return 0;
        }
        if (prc >= 256 - defrezk) {
            return 256;
        }
        return prc;
    }

    static final int usredn_pix3(int pix0, int pix1, int prc, boolean processAlpha, boolean alfa2) {
        long R;
        long G;
        long B;
        if (pix0 == pix1) {
            return pix0;
        }
        int prc0 = 256 - prc;
        int prc1 = prc;
        if (prc0 == 0) {
            return pix1;
        }
        if (prc1 == 0) {
            return pix0;
        }
        long A = 0;
        long A0 = 0;
        long A1 = 0;
        if (processAlpha) {
            A0 = (long) ((pix0 >> 24) & 255);
        }
        long R0 = (long) (((pix0 >> 16) & 255) * prc0);
        long G0 = (long) (((pix0 >> 8) & 255) * prc0);
        long B0 = (long) ((pix0 & 255) * prc0);
        if (processAlpha) {
            A1 = (long) ((pix1 >> 24) & 255);
        }
        long R1 = (long) (((pix1 >> 16) & 255) * prc1);
        long G1 = (long) (((pix1 >> 8) & 255) * prc1);
        long B1 = (long) ((pix1 & 255) * prc1);
        long Am = 0;
        if (processAlpha && (A0 < 255 || A1 < 255)) {
            Am = (((long) prc0) * A0) + (((long) prc1) * A1);
        }
        if (processAlpha) {
            A = ((((long) prc0) * A0) + (((long) prc1) * A1)) >> 8;
        }
        if (A < 0) {
            A = 0;
        }
        if (A > 255) {
            A = 255;
        }
        if (Am > 0) {
            R = ((R0 * A0) + (R1 * A1)) / Am;
            if (R < 0) {
                R = 0;
            }
            if (R > 255) {
                R = 255;
            }
            G = ((G0 * A0) + (G1 * A1)) / Am;
            if (G < 0) {
                G = 0;
            }
            if (G > 255) {
                G = 255;
            }
            B = ((B0 * A0) + (B1 * A1)) / Am;
            if (B < 0) {
                B = 0;
            }
            if (B > 255) {
                B = 255;
            }
        } else {
            long R2 = (R0 + R1) >> 8;
            if (R2 < 0) {
                R2 = 0;
            }
            if (R2 > 255) {
                R2 = 255;
            }
            long G2 = (G0 + G1) >> 8;
            if (G2 < 0) {
                G2 = 0;
            }
            if (G2 > 255) {
                G2 = 255;
            }
            long B2 = (B0 + B1) >> 8;
            if (B2 < 0) {
                B2 = 0;
            }
            if (B > 255) {
                B = 255;
            }
        }
        if (alfa2) {
            return (int) ((A << 24) | (R << 16) | (G << 8) | B);
        }
        if (A >= 128 || !processAlpha) {
            return (int) (-16777216 | (R << 16) | (G << 8) | B);
        }
        return 0;
    }

    static final int usredn_pix(int pix0, int pix1, int prc, boolean processAlpha, boolean alfa2) {
        int A0;
        int R0;
        int G0;
        int B0;
        if (pix0 == pix1) {
            return pix0;
        }
        int prc1 = prc;
        if (prc1 == 256) {
            return pix1;
        }
        if (prc1 == 0) {
            return pix0;
        }
        int A02 = (pix0 >> 24) & 255;
        int R02 = (pix0 >> 16) & 255;
        int G02 = (pix0 >> 8) & 255;
        int B02 = pix0 & 255;
        int dA = (((pix1 >> 24) & 255) - A02) * prc1;
        int dR = (((pix1 >> 16) & 255) - R02) * prc1;
        int dG = (((pix1 >> 8) & 255) - G02) * prc1;
        int dB = ((pix1 & 255) - B02) * prc1;
        if (dA < 0) {
            A0 = A02 - ((-dA) >> 8);
        } else {
            A0 = A02 + (dA >> 8);
        }
        if (dR < 0) {
            R0 = R02 - ((-dR) >> 8);
        } else {
            R0 = R02 + (dR >> 8);
        }
        if (dG < 0) {
            G0 = G02 - ((-dG) >> 8);
        } else {
            G0 = G02 + (dG >> 8);
        }
        if (dB < 0) {
            B0 = B02 - ((-dB) >> 8);
        } else {
            B0 = B02 + (dB >> 8);
        }
        if (A0 < 0) {
            A0 = 0;
        }
        if (A0 > 255) {
            A0 = 255;
        }
        if (R0 < 0) {
            R0 = 0;
        }
        if (R0 > 255) {
            R0 = 255;
        }
        if (G0 < 0) {
            G0 = 0;
        }
        if (G0 > 255) {
            G0 = 255;
        }
        if (B0 < 0) {
            B0 = 0;
        }
        if (B0 > 255) {
            B0 = 255;
        }
        if (alfa2) {
            return (A0 << 24) | (R0 << 16) | (G0 << 8) | B0;
        }
        if (A0 >= 128 || !processAlpha) {
            return -16777216 | (R0 << 16) | (G0 << 8) | B0;
        }
        return 0;
    }

    static final int[] createImageIntAlfa(Image in, int x, int y, int wo, int ho, int transform, boolean processAlpha, boolean test_alfa) {
        try {
            int[] imin1 = new int[(wo * ho)];
            in.getRGB(imin1, 0, wo, x, y, wo, ho);
            boolean alfa = false;
            int i = 0;
            while (true) {
                if (i >= imin1.length) {
                    break;
                } else if (((imin1[i] >> 24) & 255) < 255) {
                    alfa = Image_dexor;
                    break;
                } else {
                    i++;
                }
            }
            if (!test_alfa || !processAlpha || in.isMutable() || alfa) {
                return imin1;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    static final void ImageAlfaSetInt(int[] srci, int[] otpi, int w, int h, int fulafl) {
        if (srci != null && otpi != null) {
            if (fulafl == 255) {
                fulafl = 256;
            }
            int ln = srci.length;
            if (ln < otpi.length) {
                ln = otpi.length;
            }
            for (int i = 0; i < ln; i++) {
                int dt = srci[i];
                otpi[i] = (16777215 & dt) | ((((fulafl * ((dt >> 24) & 255)) >> 8) & 255) << 24);
            }
        }
    }

    static final int[] createImageInt(Image in, int x, int y, int wo, int ho, int transform, boolean processAlpha) {
        Image in1;
        int wi = wo;
        int hi = ho;
        Image in12 = in;
        int[] imin1 = createImageIntAlfa(in, x, y, wo, ho, transform, processAlpha, Image_dexor);
        if (imin1 == null) {
            if (!in.isMutable()) {
                in1 = createImage(wi, hi);
                Graphics g1 = in1.getGraphics();
                g1.setColor(0);
                g1.fillRect(0, 0, wi, hi);
                g1.drawImage(in, -x, -y, 20);
            } else {
                in1 = in12;
            }
            imin1 = new int[(wi * hi)];
            in1.getRGB(imin1, 0, wi, 0, 0, wi, hi);
            if (processAlpha && !in.isMutable()) {
                Image in2 = createImage(wi, hi);
                Graphics g2 = in2.getGraphics();
                g2.setColor(16777215);
                g2.fillRect(0, 0, wi, hi);
                g2.drawImage(in, -x, -y, 20);
                int[] imin2 = new int[(wi * hi)];
                in2.getRGB(imin2, 0, wi, 0, 0, wi, hi);
                for (int a = 0; a < imin1.length; a++) {
                    if ((imin1[a] & 255) > 32 || (imin2[a] & 255) < 223) {
                        imin1[a] = imin1[a] | -16777216;
                    } else {
                        imin1[a] = 0;
                    }
                }
            }
        }
        int[] imout = imin1;
        if (transform == 1) {
            int yo = ho - 1;
            loop1:
            for (int yi = 0; yi < ho; yi++) {
                int xo = wo - 1;
                for (int xi = 0; xi < wo; xi++) {
                    if ((yo * wo) + xo <= (yi * wo) + xi) {
                        break loop1;
                    }
                    int tmp = imout[(yo * wo) + xo];
                    imout[(yo * wo) + xo] = imout[(yi * wo) + xi];
                    imout[(yi * wo) + xi] = tmp;
                    xo--;
                }
                yo--;
            }
        } else if (transform == 2) {
            int yo2 = 0;
            for (int yi2 = 0; yi2 < ho; yi2++) {
                int xo2 = wo - 1;
                int xi2 = 0;
                while (xi2 < wo && xo2 > xi2) {
                    int tmp2 = imout[(yo2 * wo) + xo2];
                    imout[(yo2 * wo) + xo2] = imout[(yi2 * wo) + xi2];
                    imout[(yi2 * wo) + xi2] = tmp2;
                    xo2--;
                    xi2++;
                }
                yo2++;
            }
        }
        return imout;
    }
}
