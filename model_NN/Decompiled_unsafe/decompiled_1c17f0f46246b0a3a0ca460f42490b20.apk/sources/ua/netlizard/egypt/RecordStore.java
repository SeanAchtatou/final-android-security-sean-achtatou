package ua.netlizard.egypt;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class RecordStore {
    static int atmp;
    byte[] store_data;
    String store_name;

    private RecordStore(String name) {
        this.store_name = name;
    }

    static RecordStore openRecordStore(String name, boolean createIfNecessary) {
        RecordStore store = new RecordStore(name);
        store.store_data = store.getRecord(1);
        if (!createIfNecessary && (store.store_data == null || store.store_data.length <= 0)) {
            atmp = 1 / store.store_data.length;
        }
        return store;
    }

    static final String[] listRecordStores() {
        return GameView.instance.mContext.fileList();
    }

    static final void deleteRecordStore(String name) {
        GameView.instance.mContext.deleteFile(name);
    }

    /* access modifiers changed from: package-private */
    public final void closeRecordStore() {
        this.store_name = null;
    }

    /* access modifiers changed from: package-private */
    public final int getNumRecords() {
        if (this.store_data == null || this.store_data.length <= 1) {
            return 0;
        }
        return 1;
    }

    /* access modifiers changed from: package-private */
    public final int getSize() {
        return this.store_data.length;
    }

    /* access modifiers changed from: package-private */
    public final int addRecord(byte[] data, int offset, int size) {
        if (this.store_name == null) {
            return -1;
        }
        FileOutputStream fOut = null;
        try {
            fOut = GameView.instance.mContext.openFileOutput(this.store_name, 0);
            fOut.write(data, offset, size);
            try {
                fOut.close();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            try {
                fOut.close();
            } catch (Exception e3) {
            }
        } catch (Throwable th) {
            try {
                fOut.close();
            } catch (Exception e4) {
            }
            throw th;
        }
        return 1;
    }

    /* access modifiers changed from: package-private */
    public final int getRecord(int recordID, byte[] data, int offset) {
        byte[] loaded_data = getRecord(recordID);
        if (loaded_data == null) {
            return -1;
        }
        try {
            System.arraycopy(loaded_data, 0, data, offset, loaded_data.length);
            return 1;
        } catch (Exception e) {
            return -1;
        }
    }

    /* access modifiers changed from: package-private */
    public final byte[] getRecord(int recordID) {
        byte[] data = null;
        if (this.store_name != null && recordID == 1) {
            FileInputStream fIn = null;
            data = null;
            try {
                FileInputStream fIn2 = GameView.instance.mContext.openFileInput(this.store_name);
                int size = fIn2.available();
                if (size > 0) {
                    data = new byte[size];
                    fIn2.read(data);
                }
                try {
                    fIn2.close();
                } catch (Exception e) {
                }
            } catch (Exception e2) {
                try {
                    fIn.close();
                } catch (Exception e3) {
                }
            } catch (Throwable th) {
                try {
                    fIn.close();
                } catch (Exception e4) {
                }
                throw th;
            }
        }
        return data;
    }

    /* access modifiers changed from: package-private */
    public final long getLastModified() {
        return 12345678;
    }

    /* access modifiers changed from: package-private */
    public final int getRecordSize(int recordId) {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final int getSizeAvailable() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void setRecord(int recordId, byte[] newData, int offset, int numBytes) {
        deleteRecordStore(this.store_name);
        addRecord(newData, offset, numBytes);
    }
}
