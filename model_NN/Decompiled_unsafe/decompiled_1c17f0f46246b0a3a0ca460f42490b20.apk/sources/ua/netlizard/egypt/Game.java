package ua.netlizard.egypt;

import java.util.Random;

public class Game extends FullCanvas implements Runnable {
    static byte DOWN = 0;
    static final int H_sh = (Height / 18);
    static int Height = 0;
    static int Height_2 = (Height >> 1);
    static boolean Pause = true;
    static String SiteURL = null;
    static int Width = 0;
    static int Width_2 = (Width >> 1);
    static boolean a_pnt = false;
    static int aa = 0;
    static boolean accel_on = false;
    static boolean accelerate = false;
    static Image[] anim_f = null;
    static byte backGround = -1;
    static short[][] bounds = null;
    static boolean cikl_rn = true;
    static byte ddd = 0;
    static boolean deathmach = false;
    static boolean demo = false;
    static boolean first_load = false;
    static boolean first_take = false;
    static MyFont[] font = null;
    static boolean gg = true;
    static byte help_now = 0;
    static byte help_s = 0;
    static boolean in_game = false;
    static boolean inert = true;
    static int key = 0;
    static boolean krl2 = false;
    static byte kvo_podstilok = 6;
    static int kvo_zv = 11;
    static Image l_d = null;
    static int len_rms = Player.STARTED;
    static boolean load_scene = false;
    static boolean lz = true;
    static final int max_per = 50;
    static byte max_sens = 4;
    static boolean minimizeGame = false;
    static short my_code = 0;
    static byte my_level = 0;
    static Image n_l = null;
    static String name_rms = "k1";
    static boolean obrob_pnt = false;
    static boolean on_j = false;
    static int open_level = 0;
    static int per = 0;
    static boolean pnt_presdrag = false;
    static boolean pnt_pressed = false;
    static boolean pnt_released = false;
    static int pntsmx = 0;
    static int pntsmy = 0;
    static boolean podstilka_snd = false;
    static int r_Y = 0;
    static boolean rls_pnt = false;
    static byte s1 = 0;
    static int s_X = 0;
    static int s_Y = 0;
    static int s_xA = 0;
    static int s_yA = 0;
    static byte sens = 0;
    static boolean shop = true;
    static int shtora = 0;
    static final int size_T = 6;
    static int sm_y;
    static int sm_y_pnt;
    static String[] smenu;
    static boolean sound_enable = true;
    static int sound_rst = 0;
    static boolean sss = false;
    static boolean stand = true;
    static int time;
    static int time_game;
    static long time_pnt_prs;
    static long time_pnt_rls;
    static boolean touch_began;
    static boolean touch_end;
    static Image[] touch_menu;
    static boolean touch_move;
    static boolean touch_screan;
    static boolean ttt111 = false;
    static final int[] ver_data = {2, 10, 2, 2, 2, 10, 8, 7, 10, 2, 2, 2, 10, 8, 9};
    static boolean vibro_enable = true;
    static int xA;
    static int x_pnt;
    static int x_pnt_prs;
    static int yA;
    static int y_pnt;
    static int y_pnt_prs;
    int KEY_EXIT = 35;
    boolean anim1 = false;
    boolean chit_pound = false;
    int details_max = 2;
    Graphics g_bf = null;
    boolean k_prs = false;
    boolean k_prs_ds = false;
    int keyPressedActivate_cnt;
    boolean krl = true;
    int[] mg = null;
    private Random myRandom = new Random();
    boolean no_paint = true;
    String[] rot_screen_txt;
    Image scr_bf = null;
    int screen_buffer = -1;
    final int stand__t = 77;
    boolean stpnt = true;
    long test_time1;
    long test_time2;
    Thread thread;
    int tp_fly = -1;
    boolean tprsv = false;
    boolean vibra_l;
    boolean vibra_m;
    boolean vibra_s;
    boolean vibra_xl;

    private final int Rnd(int a) {
        return Math.abs(this.myRandom.nextInt() % a);
    }

    public Game() {
        set_var();
        this.thread = new Thread(this);
    }

    /* access modifiers changed from: package-private */
    public final void game_start() {
        Display.getDisplay(NET_Lizard.instance).setCurrent(this);
        this.thread.start();
    }

    public static void rest_sound() {
        boolean z = false;
        if (sound_rst >= 2) {
            sound_rst = 0;
            if (my_level == 0) {
                z = true;
            }
            if ((z & gg) || Menu.loading) {
                try {
                    Game game = NET_Lizard.notifyDestroyed;
                    music();
                } catch (Exception e) {
                }
            } else if (podstilka_snd) {
                podstilka((my_level - 1) % 5);
            }
        }
    }

    static {
        short[] sArr = new short[4];
        sArr[2] = 20;
        sArr[3] = 20;
        short[] sArr2 = new short[4];
        sArr2[1] = (short) (Height - 40);
        sArr2[2] = (short) (Width / 3);
        sArr2[3] = 40;
        short[] sArr3 = {(short) (Width / 3), (short) (Height - 40), (short) (Width / 3), 40};
        short[] sArr4 = {(short) ((Width / 3) * 2), (short) (Height - 40), (short) (Width / 3), 40};
        short[] sArr5 = new short[4];
        sArr5[2] = 38;
        sArr5[3] = 30;
        short[] sArr6 = new short[4];
        sArr6[2] = 30;
        sArr6[3] = 30;
        short[] sArr7 = new short[4];
        sArr7[2] = 41;
        sArr7[3] = 30;
        short[] sArr8 = new short[4];
        sArr8[2] = 41;
        sArr8[3] = 30;
        short[] sArr9 = new short[4];
        sArr9[2] = 124;
        sArr9[3] = 124;
        short[] sArr10 = new short[4];
        sArr10[1] = (short) ((Height >> 1) - (Height >> 3));
        sArr10[2] = (short) (Width / 3);
        sArr10[3] = (short) ((Height >> 1) - 30);
        short[] sArr11 = {(short) (Width / 3), (short) ((Height >> 1) - (Height >> 3)), (short) (Width / 3), (short) ((Height >> 1) - 30)};
        short[] sArr12 = {(short) ((Width / 3) * 2), (short) ((Height >> 1) - (Height >> 3)), (short) (Width / 3), (short) ((Height >> 1) - 30)};
        short[] sArr13 = {(short) (Width - 30), (short) (Height - 30), 30, 30};
        short[] sArr14 = new short[4];
        sArr14[1] = 30;
        short[] sArr15 = new short[4];
        sArr15[1] = (short) (Height - 30);
        sArr15[2] = (short) (Width >> 1);
        sArr15[3] = 30;
        bounds = new short[][]{sArr, new short[]{20, 20, 20, 20}, new short[]{40, 40, 20, 20}, new short[]{60, 60, 20, 20}, new short[]{80, 80, 20, 20}, new short[]{100, 100, 20, 20}, new short[]{120, 120, 20, 20}, sArr2, sArr3, sArr4, sArr5, sArr6, sArr7, sArr8, sArr9, sArr10, sArr11, sArr12, sArr13, sArr14, sArr15, new short[]{(short) (Width >> 1), (short) (Height - 30), (short) (Width >> 1), 30}};
    }

    public void showNotify() {
        super.showNotify();
        minimizeGame = false;
        if (sound_rst == 1) {
            sound_rst++;
        }
        try {
            repaint();
            serviceRepaints();
        } catch (Exception e) {
        }
    }

    public void hideNotify() {
        super.hideNotify();
        minimizeGame = true;
        super.hideNotify();
        try {
            if (!Menu.enable && !Pause) {
                Menu.enable = true;
                Pause = true;
            }
        } catch (Exception e) {
        }
        sound_rst = 1;
        sound_stop();
    }

    /* access modifiers changed from: package-private */
    public void set_var() {
        Width = getWidth();
        Height = getHeight();
        Width_2 = Width >> 1;
        Height_2 = Height >> 1;
        Menu.w_14 = Width / 14;
        Menu.h_14 = Height / 14;
    }

    private final void key_game() {
        if (!Menu.enable && !scr.enable_script && m3d.Units != null && !m3d.Units[0].death) {
            if ((my_code & 1) != 0) {
                if (!m3d.look_on) {
                    m3d.move_me_up();
                } else {
                    m3d.turn_up(sens);
                }
            }
            if ((my_code & 2) != 0) {
                if (!m3d.look_on) {
                    m3d.move_me_down();
                } else {
                    m3d.turn_down(sens);
                }
            }
            if ((my_code & 4) != 0) {
                m3d.turn_left((byte) (sens + 2));
            }
            if ((my_code & 8) != 0) {
                m3d.turn_right((byte) (sens + 2));
            }
            if ((my_code & 16) != 0) {
                m3d.turn_left((byte) (sens + 1));
            }
            if ((my_code & 32) != 0) {
                m3d.turn_right((byte) (sens + 1));
            }
            if ((my_code & 64) != 0) {
                m3d.shot_me();
            }
            if ((my_code & 128) != 0) {
                m3d.move_me_sleft();
            }
            if ((my_code & 256) != 0) {
                m3d.move_me_sright();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean test_touch() {
        touch_screan = true;
        try {
            boolean hasPointerEvents = hasPointerEvents();
            touch_screan = hasPointerEvents;
            return hasPointerEvents;
        } catch (Exception e) {
            touch_screan = false;
            return false;
        } catch (Throwable th) {
            touch_screan = false;
            return false;
        }
    }

    public final void Logic_moving() {
        scr.proslushka_scripta();
        if (!deathmach) {
            m3d.my_scr();
        } else {
            m3d.dm_scr();
        }
        m3d.speek();
        m3d.nuw_hit_unit = (byte) (m3d.nuw_hit_unit + 1);
        if (m3d.nuw_hit_unit >= m3d.units) {
            if (m3d.units > 1) {
                m3d.nuw_hit_unit = 1;
            } else {
                m3d.nuw_hit_unit = 0;
            }
        }
        m3d.light_for_me();
        scr.run_script();
        unit[] U = m3d.Units;
        if (!U[0].death) {
            for (int i = 0; i < m3d.units; i++) {
                unit u = U[i];
                if (!scr.enable_script) {
                    AI.AI_(u);
                }
                if (u.type_unit == 7) {
                    math.HIT_BOSS(u, 0);
                } else if (i != 0 && !u.death && u.visible) {
                    math.HIT_ME(u, 0);
                }
                math.phi(u);
                if (u.visible) {
                    m3d.kick_body(u);
                }
            }
            for (int i2 = 1; i2 < m3d.units; i2++) {
                anim.animation(U[i2]);
            }
        } else {
            m3d.white = (short) (m3d.white - 20);
            unit unit = U[0];
            unit.life = (short) (unit.life - 1);
            if (U[0].life < -50) {
                if (!deathmach) {
                    m3d.restart(my_level);
                    return;
                } else if (!Menu.if_need_score(false)) {
                    m3d.restart(my_level);
                    return;
                } else {
                    m3d.start_abc();
                    return;
                }
            }
        }
        math.stop_all();
        m3d.Shuts_hits();
        m3d.shot_delay();
        m3d.time_change();
        m3d.reload_FPS();
        m3d.smesh_FPS_weapon();
        m3d.rot_FPS();
        m3d.time_rot_obj();
        m3d.give_obj();
        m3d.plus_HP();
        if (my_level != 4) {
            m3d.open_door();
        } else {
            m3d.open_door_4();
        }
        m3d.Sprite_life();
    }

    private static long mem_stat1() {
        System.gc();
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }

    /* access modifiers changed from: package-private */
    public void draw_state(int a) {
        Menu.state_load = (byte) a;
        repaint();
        serviceRepaints();
    }

    private static void draw_look_on(Graphics g) {
        g.setColor(16777215);
        if (m3d.look_on) {
            g.fillTriangle(m3d.Width_2_real_3D, 30, m3d.Width_2_real_3D + 5, 35, m3d.Width_2_real_3D - 5, 35);
            g.fillTriangle(m3d.Width_2_real_3D, (Height - 30) - 2, m3d.Width_2_real_3D + 5, ((Height - 30) - 2) - 5, m3d.Width_2_real_3D - 5, ((Height - 30) - 2) - 5);
            g.fillTriangle(0, m3d.Height_2_real_3D + 30, 5, m3d.Height_2_real_3D + 30 + 5, 5, (m3d.Height_2_real_3D + 30) - 5);
            g.fillTriangle(Width, m3d.Height_2_real_3D + 30, Width - 5, m3d.Height_2_real_3D + 30 + 5, Width - 5, (m3d.Height_2_real_3D + 30) - 5);
        }
    }

    private static void draw_fire_in_gun(Graphics g) {
        int x = m3d.x_fire + 0;
        int y = m3d.y_fire + 30;
        if (m3d.weapon_now != 0 && m3d.weapon_now != 6 && Menu.spr_fire != null && Menu.ccc != null) {
            Menu.draw_fire(g, 5 - m3d.weapon_now, x, y);
        }
    }

    static final void sound_weapon(int weapon) {
        if (weapon != 0 && weapon != 6) {
            sound(weapon);
        }
    }

    static final void draw_time(Graphics g) {
        int t = time;
        if (m3d.Units[0].death) {
            t = time_game;
        }
        int sec = (t / FullCanvas.canvasRealWidth) % 60;
        String s = String.valueOf(t / 60000) + ":" + (sec < 10 ? "0" : "") + sec;
        font[0].drawString(g, s, (Width - font[0].stringWidth(s)) >> 1, 30, 20);
    }

    static final void draw_inter(Graphics g) {
        if (shtora > 0) {
            shtora--;
        }
        g.setColor(6054737);
        g.fillRect(0, (Height - 30) - 2, Width, 1);
        g.fillRect(0, 30, Width, 1);
        Menu.draw_spr(g, 9, 0, (Height - Menu.cc[9][3]) - 2);
        if (m3d.Units[0].life > 0) {
            font[0].drawString(g, new StringBuilder().append((int) m3d.Units[0].life).toString(), (Menu.cc[9][2] >> 1) - 2, Height - 18, 20);
        }
        g.setColor(0);
        g.fillRect(Menu.cc[9][2], Height - 30, Width - Menu.cc[9][2], 30);
        if (!(m3d.weapon_now == 0 || m3d.weapon_now == 6)) {
            String s = String.valueOf((int) m3d.ammo_in_mag[m3d.weapon_now]) + "/" + ((int) m3d.ammo[m3d.weapon_now]);
            font[0].drawString(g, s, ((Width - Menu.cc[8][2]) - font[0].stringWidth(s)) - 2, ((30 - font[0].getHeight()) >> 1) + (Height - 30), 20);
            Menu.draw_spr(g, 8, Width - Menu.cc[8][2], (Height - Menu.cc[8][3]) - 2);
        }
        if (m3d.fire2) {
            draw_fire_in_gun(g);
        }
        g.setClip(0, 0, Width, Height);
        draw_look_on(g);
        g.setColor(0);
        g.fillRect(0, 0, Width, 30);
        if (m3d.boss) {
            if ((my_level == 7 && m3d.room_now == 0) || (my_level == 11 && m3d.Units[0].pos_y > 0)) {
                g.setColor(12648448);
                g.fillRect(Width >> 3, 12, (m3d.Units[1].life * (Width - (Width >> 2))) / m3d.life_unit[m3d.Units[1].type_unit], 6);
            }
            if (my_level == 15 && (m3d.Units[0].pos_y >> 16) > 12650) {
                g.setColor(12648448);
                g.fillRect(Width >> 3, 12, (m3d.Units[13].life * (Width - (Width >> 2))) / m3d.life_unit[m3d.Units[13].type_unit], 6);
            }
        }
        if (deathmach) {
            draw_time(g);
        }
        int a = 1;
        if (Width > 320 || Height > 320) {
            a = 3;
        }
        int a_2 = a >> 1;
        g.setColor(16777215);
        g.fillRect(Width_2 - a_2, (Height_2 + m3d.razbros) - a_2, a, a);
        g.fillRect(Width_2 - a_2, (Height_2 - m3d.razbros) - a_2, a, a);
        g.fillRect((Width_2 + m3d.razbros) - a_2, Height_2 - a_2, a, a);
        g.fillRect((Width_2 - m3d.razbros) - a_2, Height_2 - a_2, a, a);
        if (!touch_screan) {
            return;
        }
        if (help_s == 0) {
            draw_touch(g);
        } else if (help_s <= 3) {
            draw_touch(g);
        }
    }

    static final void draw_screen(Graphics g) {
        if (my_level == 16 && sss) {
            g.drawRGB(m3d.display, 0, m3d.Width_real_3D, 0, 30, m3d.Width_real_3D, m3d.Height_real_3D - 2, false);
            sss = false;
        } else if (Menu.menu_type == 10) {
            if (Menu.draw_disp) {
                Menu.draw_disp = false;
            }
        } else if (Menu.menu_type == 2 || Menu.menu_type == 6 || Menu.menu_type == 7 || Menu.menu_type == 17) {
            g.drawRGB(m3d.display, 0, m3d.Width_real_3D, 0, 30, m3d.Width_real_3D, m3d.Height_real_3D - 2, false);
        } else if (load_scene && my_level == 0) {
            if (!Menu.chitalka && Menu.menu_type < 18) {
                m3d.dark_inter((Menu.start_h + ((Menu.active - Menu.scrol) * Menu.h_str)) - 30, Menu.h_str);
            }
            g.drawRGB(m3d.display, 0, m3d.Width_real_3D, 0, 30, m3d.Width_real_3D, m3d.Height_real_3D - 2, false);
            if (Width < 130) {
                g.drawImage(l_d, (Width - l_d.getWidth()) >> 1, (Height - l_d.getHeight()) - 1, 20);
            } else {
                g.drawImage(l_d, (Width - l_d.getWidth()) >> 1, (Height - 15) - (l_d.getHeight() >> 1), 20);
            }
        } else if (!Menu.loading) {
            g.setClip(0, 0, Width, Height);
            g.setColor(0);
            g.fillRect(0, 0, Width, Height);
        }
    }

    static final void reload_fps(Graphics g) {
        if (m3d.reload_FPS != 0) {
            g.setColor(16777215);
            g.fillRect(Width_2 >> 1, (((Height - 30) - 2) - 4) - 2, (Width_2 * ((20 - m3d.reload_FPS) + 1)) / 20, 4);
            g.drawRect((Width_2 >> 1) - 2, (((Height - 30) - 2) - 4) - 4, Width_2 + 3, 7);
        }
    }

    static final void draw_shtori(Graphics g) {
        int a = (H_sh * shtora) >> 3;
        g.setColor(0);
        g.fillRect(0, 30, Width, a);
        g.fillRect(0, ((Height - 30) - 2) - a, Width, a);
    }

    static final void draw_fil_line(Graphics g, boolean shtori) {
        g.setColor(0);
        g.fillRect(0, 0, m3d.Width, 30);
        g.fillRect(0, (Height - 30) - 2, Width, 32);
        if (shtori) {
            shtora++;
            if (shtora > 8) {
                shtora = 8;
            }
        }
    }

    static final void draw_lz(Graphics g) {
        if (Menu.state_load >= 100) {
            int a = 0;
            if (Menu.state_load > 105) {
                a = Menu.state_load - 105;
            }
            if (n_l != null) {
                if (n_l != null) {
                    g.drawImage(n_l, 0, 0, 20);
                } else {
                    g.setColor(0);
                    g.fillRect(0, 0, Width, Height);
                }
                if (Menu.state_load == 100) {
                    int a11 = !touch_screan ? 49 : 61;
                    font[0].drawString(g, smenu[a11], (Width - font[0].stringWidth(smenu[a11])) >> 1, (Height - font[0].getHeight()) >> 1, 20);
                }
                g.setClip(0, 0, Width, Height);
                if (Menu.state_load == 100) {
                    Menu.state_load = 101;
                } else if (Menu.state_load == 101) {
                    Menu.state_load = 100;
                }
                if (Menu.state_load >= 102) {
                    g.drawImage(l_d, (Width - l_d.getWidth()) >> 1, (Height - (l_d.getHeight() + 5)) + (((l_d.getHeight() - 5) * a) / 20), 20);
                }
                shtora++;
                if (shtora >= 4) {
                    shtora = 0;
                }
                if (anim_f != null) {
                    g.drawImage(anim_f[shtora], (Width - anim_f[shtora].getWidth()) >> 1, Height - anim_f[shtora].getHeight(), 20);
                }
                if (Menu.state_load < 102) {
                    g.drawImage(l_d, (Width - l_d.getWidth()) >> 1, (Height - (l_d.getHeight() + 5)) + (((l_d.getHeight() - 5) * a) / 20), 20);
                }
            }
        } else if (n_l != null) {
            g.setColor(0);
            g.fillRect(0, 0, Width, Height);
            g.drawImage(n_l, (Width - n_l.getWidth()) >> 1, ((Height >> 1) - n_l.getHeight()) >> 1, 20);
            n_l = null;
        } else {
            int w = Width - (Width >> 2);
            int start_x = (Width - w) >> 1;
            int start_y = m3d.Height_real_3D + 30 + 14;
            int start_y_pricol = m3d.Height_real_3D + 30 + ((30 - Menu.cc[7][3]) >> 1);
            int max_w = w / Menu.cc[7][2];
            g.setColor(8216655);
            g.fillRect(start_x, start_y, w, 1);
            int a2 = (Menu.state_load * max_w) / 100;
            for (int i = 0; i < a2; i++) {
                if ((i & 1) == 0) {
                    Menu.draw_spr(g, 7, (Menu.cc[7][2] * i) + start_x, start_y_pricol);
                }
            }
        }
    }

    static final void str_load(Graphics g) {
        g.setColor(0);
        g.fillRect(0, 0, Width, Height);
        String[] s = Uni.uni.sorter(smenu[29], font[0], Width, true);
        int h = font[0].getHeight();
        int start = Width_2 + ((Height_2 - (s.length * h)) >> 1);
        for (int i = 0; i < s.length; i++) {
            font[0].drawString(g, s[i], (Width - font[0].stringWidth(s[i])) >> 1, start + (i * h), 20);
        }
        font[0].drawString(g, smenu[13], (Width - font[0].stringWidth(smenu[13])) >> 1, (Height - font[0].getHeight()) >> 1, 20);
    }

    static final void accel_m(Graphics g) {
        if (backGround == 3) {
            g.setColor(0);
            g.fillRect(0, 0, Width, Height);
            return;
        }
        String[] ss1 = Uni.uni.sorter(Uni.uni.load_string("se"), font[0], Width, true);
        int h = font[0].getHeight();
        int start_y = (Height - (ss1.length * h)) >> 1;
        g.setColor(0);
        g.fillRect(0, 0, Width, Height);
        for (int i = 0; i < ss1.length; i++) {
            font[0].drawString(g, ss1[i], (Width - font[0].stringWidth(ss1[i])) >> 1, start_y + (h * i), 20);
        }
    }

    static final boolean search_string(String txt, String fnd, boolean no_reg) {
        if (txt == null || fnd == null || txt.length() < fnd.length()) {
            return false;
        }
        int j = 0;
        for (int i = 0; i < txt.length() && j < fnd.length(); i++) {
            if (cpChar(txt.charAt(i), fnd.charAt(j), no_reg)) {
                j++;
                if (j >= fnd.length()) {
                    return true;
                }
            } else {
                j = 0;
            }
        }
        return false;
    }

    static final boolean cpChar(char ch1, char ch2, boolean no_reg) {
        if (ch1 == ch2) {
            return true;
        }
        if (no_reg) {
            if (ch1 >= 'A' && ch1 <= 'Z' && ch2 >= 'a' && ch2 <= 'z' && ch1 - 'A' == ch2 - 'a') {
                return true;
            }
            if (ch1 >= 'a' && ch1 <= 'z' && ch2 >= 'A' && ch2 <= 'Z' && ch1 - 'a' == ch2 - 'A') {
                return true;
            }
            if (ch1 >= 1040 && ch1 <= 1071 && ch2 >= 1072 && ch2 <= 1103 && ch1 - 1040 == ch2 - 1072) {
                return true;
            }
            if (ch1 < 1072 || ch1 > 1103 || ch2 < 1040 || ch2 > 1071 || ch1 - 1072 != ch2 - 1040) {
                return false;
            }
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean test_fly_port() {
        if (this.tp_fly < 0) {
            this.tp_fly = 0;
            if (search_string("Android", "Fly", false)) {
                this.tp_fly = 1;
            }
        }
        if (this.tp_fly > 0) {
            return true;
        }
        return false;
    }

    private final boolean rot_screen(Graphics g, int col_back, MyFont font2) {
        try {
            int tw = getWidth();
            int th = getHeight();
            if (!(Math.abs(getWidthScr() - th) < Math.abs(getWidthScr() - tw) && Math.abs(getHeightScr() - tw) < Math.abs(getHeightScr() - th))) {
                return false;
            }
            int W = getWidthScr();
            int H = getHeightScr();
            if (this.rot_screen_txt == null || this.rot_screen_txt.length <= 0) {
                try {
                    this.rot_screen_txt = Uni.uni.sorter(Uni.uni.load_string("rot_scr_err"), font2, (W * 3) >> 2, true);
                } catch (Exception e) {
                }
            }
            if (this.rot_screen_txt == null || this.rot_screen_txt.length <= 0) {
                try {
                    this.rot_screen_txt = Uni.uni.sorter("This screen orientation is not supported.  Please adjust your screen position to continue.", font2, (W * 3) >> 2, true);
                } catch (Exception e2) {
                }
            }
            g.setClip(0, 0, W, H);
            g.setColor(col_back);
            g.fillRect(0, 0, W, H);
            if (this.rot_screen_txt != null) {
                int dy = font2.getHeight() * 2;
                int y = (H - ((this.rot_screen_txt.length * dy) - (dy - (font2.getHeight() - font2.fsy)))) >> 1;
                for (int i = 0; i < this.rot_screen_txt.length; i++) {
                    font2.drawString(g, this.rot_screen_txt[i], (W - font2.stringWidth(this.rot_screen_txt[i])) >> 1, y, 20);
                    y += dy;
                }
            }
            return true;
        } catch (Exception e3) {
            return false;
        }
    }

    public final void paint(Graphics g) {
        if (!this.stpnt) {
            if (rot_screen(g, 0, font[0])) {
                obrob_pnt();
                return;
            }
            try {
                if (this.screen_buffer < 0) {
                    this.screen_buffer = 0;
                    if (test_fly_port() && !isDoubleBuffered()) {
                        this.screen_buffer = 1;
                    }
                }
                if (this.screen_buffer > 0) {
                    if (this.scr_bf == null) {
                        this.scr_bf = Image.createImage(Width, Height);
                    }
                    if (this.g_bf == null) {
                        this.g_bf = this.scr_bf.getGraphics();
                    }
                    paint0(this.g_bf);
                    g.drawImage(this.scr_bf, 0, 0, 20);
                    return;
                }
                paint0(g);
            } catch (Exception e) {
            }
        }
    }

    public final void paint0(Graphics g) {
        paint1(g);
        obrob_pnt();
    }

    public final void paint1(Graphics g) {
        try {
            if (!Secur_Dem.vivDemoTxt(g)) {
                if (backGround > 0) {
                    if (backGround == 1) {
                        str_load(g);
                    } else {
                        accel_m(g);
                    }
                } else if (lz) {
                    draw_lz(g);
                } else if (Menu.enable) {
                    draw_fil_line(g, false);
                    draw_screen(g);
                    Menu.draw(g);
                } else {
                    g.drawRGB(m3d.display, 0, m3d.Width_real_3D, 0, 30, m3d.Width_real_3D, m3d.Height_real_3D - 2, false);
                    if (Menu.chit_fps) {
                        font[0].drawString(g, new StringBuilder().append(this.test_time1).toString(), 0, 30, 20);
                        font[0].drawString(g, new StringBuilder().append((int) m3d.shots).toString(), 0, 60, 20);
                    }
                    reload_fps(g);
                    if (shtora != 0) {
                        draw_shtori(g);
                    }
                    if (scr.enable_script) {
                        draw_fil_line(g, true);
                        Menu.draw_cancel(g, false);
                    } else if (my_level != 0) {
                        draw_inter(g);
                    }
                    poly.all_poly = 0;
                }
            }
        } catch (Exception e) {
        }
    }

    static void kill_font() {
        if (font[1] != null) {
            font[1].font = null;
            font[1] = null;
        }
    }

    static void load_font() {
        if (font[1] == null) {
            try {
                font[1] = MyFont.getFont(0, 0, 8, "0", "0");
                font[1].fsx = -1;
            } catch (Exception e) {
                kill_font();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void load_text() {
        String ss = Uni.uni.load_string("tm");
        font = new MyFont[2];
        font[0] = MyFont.getFont(0, 0, 8);
        font[0].fsx = -1;
        try {
            font[1] = MyFont.getFont(0, 0, 8, "0", "0");
            font[1].fsx = -1;
        } catch (Exception e) {
            kill_font();
        }
        smenu = Uni.uni.sorter(ss, font[0], 0, false);
        Menu.game = this;
        Menu.F = font;
        Menu.str_text = Menu.F[0].getHeight();
        if (Menu.F[1] != null) {
            Menu.str_text_big = Menu.F[1].getHeight();
        }
    }

    /* access modifiers changed from: package-private */
    public void load() {
        if (Height > 360) {
            bounds[20][0] = 0;
            bounds[20][1] = (short) (Height - 60);
            bounds[20][2] = (short) (Width >> 1);
            bounds[20][3] = 60;
            bounds[21][0] = (short) (Width >> 1);
            bounds[21][1] = (short) (Height - 60);
            bounds[21][2] = (short) (Width >> 1);
            bounds[21][3] = 60;
        }
        accelerate = false;
        Menu.load_c();
        Menu.load_cc();
        load_lz();
        Menu.load_menu();
        try {
            byte[] load_pack = Uni.uni.load_pack("lvl0", -1);
            byte[] mas = null;
            load_scene = true;
        } catch (Exception e) {
            load_scene = false;
        }
        lz = true;
        m3d.load_rm(false, true);
        if (load_scene) {
            m3d.load_m3d(0);
        }
        lz = true;
        test_touch();
        load_touch1();
        load_or();
        try {
            Uni uni = Uni.uni;
            l_d = Uni.createImage("nl");
        } catch (Exception e2) {
            cikl_rn = false;
        }
        Pause = true;
        Menu.enable = true;
        lz = false;
    }

    /* access modifiers changed from: package-private */
    public final void load_or() {
        try {
            Uni uni = Uni.uni;
            n_l = Uni.createImage("or");
        } catch (Exception e) {
        }
        anim_f = new Image[4];
        int i = 0;
        while (i < 4) {
            try {
                Image[] imageArr = anim_f;
                Uni uni2 = Uni.uni;
                imageArr[i] = Uni.createImage("or_f" + i);
                i++;
            } catch (Exception e2) {
                anim_f = null;
            }
        }
        try {
            Uni uni3 = Uni.uni;
            l_d = Uni.createImage("or_f");
        } catch (Exception e3) {
            l_d = null;
        }
        Menu.state_load = 125;
        key = 0;
        ddd = 0;
        while (true) {
            repaint();
            serviceRepaints();
            if ((!touch_screan || ddd != 1) && key != 53) {
                try {
                    Thread.sleep(200);
                } catch (Exception e4) {
                }
                if (Menu.state_load > 102) {
                    Menu.state_load = (byte) (Menu.state_load - 1);
                } else if (Menu.state_load == 102) {
                    Menu.state_load = 101;
                }
            }
        }
        l_d = null;
        anim_f = null;
        n_l = null;
    }

    /* access modifiers changed from: package-private */
    public final void load_lz() {
        try {
            Uni uni = Uni.uni;
            n_l = Uni.createImage("lz");
        } catch (Exception e) {
            cikl_rn = false;
        }
        repaint();
        serviceRepaints();
        int a = 11;
        while (true) {
            a--;
            if (a <= 0) {
                n_l = null;
                try {
                    Thread.sleep(100);
                    return;
                } catch (Exception e2) {
                    return;
                }
            } else {
                repaint();
                serviceRepaints();
                Menu.state_load = (byte) (Menu.state_load + 10);
                try {
                    Thread.sleep(200);
                } catch (Exception e3) {
                }
            }
        }
    }

    public final void run() {
        this.stpnt = false;
        load_text();
        Secur_Dem.createSecur(this, null);
        demo = Secur_Dem.testMainDemo(demo);
        if (Secur_Dem.exitGame()) {
            NET_Lizard.quitApp();
            return;
        }
        SiteURL = Secur_Dem.siteURL();
        Secur_Dem.tchscr = test_touch();
        load();
        key = 0;
        while (cikl_rn) {
            if (demo) {
                if (Secur_Dem.dkey_while_iter(((my_level == 0) & gg) | Menu.loading)) {
                    if (my_level != 0) {
                        Menu.back_menu();
                    }
                    Menu.menu_t = -1;
                    gg = true;
                }
            }
            if (Secur_Dem.vivBuy()) {
                repaint();
                serviceRepaints();
                try {
                    Thread.sleep(7);
                } catch (Exception e) {
                }
            } else {
                try {
                    Thread.sleep(7);
                } catch (Exception e2) {
                }
                if (my_code != 0) {
                    key_game();
                }
                touches();
                if (key == 0) {
                    DOWN = 0;
                    if (demo && Secur_Dem.startingBuy()) {
                    }
                } else if (this.k_prs || this.k_prs_ds) {
                    key_kr();
                    this.k_prs = false;
                }
                Menu.change_menu();
                Rpnt();
                m3d.fire2 = false;
            }
        }
        sound_close();
        m3d.save_rm(true, true);
        NET_Lizard.quitApp();
    }

    static void load_sound(int N) {
        if (sound_enable) {
            create_podstilka(N);
            if (podstilka_snd) {
                sound(0);
            }
        }
    }

    private final void key_kr() {
        key_k();
        if (krl2) {
            key = 0;
            krl2 = false;
        }
    }

    private final void key_k() {
        this.krl = true;
        boolean left = false;
        boolean right = false;
        boolean up = false;
        boolean down = false;
        boolean fire = false;
        if (Menu.menu_type == 15) {
            Menu.chit_5 = false;
            if (key == 35 && !this.chit_pound) {
                Menu.chit = String.valueOf(Menu.chit) + "#";
            } else if (!this.chit_pound) {
                Menu.chit = String.valueOf(Menu.chit) + (key - 48);
                krl2 = true;
            }
            if (Menu.chit.length() >= 4) {
                Menu.chit = "";
            }
        }
        switch (key) {
            case 50:
                up = true;
                break;
            case J2MECanvas.KEY_NUM4:
                left = true;
                break;
            case J2MECanvas.KEY_NUM5:
                fire = true;
                Menu.chit_5 = true;
                break;
            case J2MECanvas.KEY_NUM6:
                right = true;
                break;
            case J2MECanvas.KEY_NUM8:
                down = true;
                break;
        }
        if (key == 35) {
            if (Menu.menu_type != 15) {
                Menu.key_pound();
            } else if (this.chit_pound) {
                Menu.key_pound();
            }
        }
        if (key == 42 && !Menu.enable) {
            m3d.change_look_on();
        }
        if (key == 48) {
            m3d.change_weapon((byte) -1);
        }
        if (Menu.enable) {
            if (up) {
                Menu.move_up();
            }
            if (down) {
                Menu.move_down();
            }
            if (left) {
                Menu.move_left();
                krl2 = true;
            }
            if (right) {
                Menu.move_right();
                krl2 = true;
            }
            if (fire) {
                Menu.fire();
                krl2 = true;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void Rpnt() {
        vibra_th();
        long time1 = System.currentTimeMillis();
        if (!Pause) {
            Logic_moving();
            m3d.camera_view();
            m3d.draw2D(m3d.ppp, m3d.room_now);
            m3d.draw_3d(m3d.draw_loop);
        } else if (load_scene && my_level == 0) {
            m3d.script_mein_menu();
            m3d.draw2D(m3d.ppp, m3d.room_now);
            m3d.draw_3d(m3d.draw_loop);
        }
        repaint();
        serviceRepaints();
        long real_time = System.currentTimeMillis() - time1;
        this.test_time1 = real_time;
        if (!Pause) {
            time = (int) (((long) time) + real_time);
        }
        if (real_time < 77) {
            if (!Pause) {
                time = (int) (((long) time) + (77 - real_time));
            }
            try {
                Thread.sleep(77 - real_time);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void pointerPressedS(int x, int y) {
        touchPointer(x, y, true, false, false);
    }

    /* access modifiers changed from: protected */
    public final void pointerReleasedS(int x, int y) {
        touchPointer(x, y, false, true, false);
    }

    /* access modifiers changed from: protected */
    public final void pointerDraggedS(int x, int y) {
        touchPointer(x, y, false, false, true);
    }

    private static final void touchPointer(int x, int y, boolean pressed, boolean released, boolean dragged) {
        if (pressed || dragged) {
            if (!pnt_presdrag) {
                pressed = true;
            }
            pnt_presdrag = true;
            x_pnt = x;
            y_pnt = sm_y_pnt + y;
            if (y_pnt < 0) {
                sm_y_pnt += -y_pnt;
                y_pnt = sm_y_pnt + y;
            }
            try {
                int ht1 = Height - 1;
                if (ht1 > 0 && y_pnt > ht1) {
                    sm_y_pnt -= y_pnt - ht1;
                    y_pnt = sm_y_pnt + y;
                }
            } catch (Exception e) {
            }
            if (pressed) {
                a_pnt = true;
                obrob_pnt = false;
                rls_pnt = false;
                pnt_pressed = true;
                pntsmx = 0;
                pntsmy = 0;
            }
            if (pressed) {
                x_pnt_prs = x_pnt;
                y_pnt_prs = y_pnt;
                time_pnt_prs = System.currentTimeMillis();
            }
            rest_sound();
        }
        if (released) {
            pnt_released = true;
            time_pnt_rls = System.currentTimeMillis();
            if (obrob_pnt) {
                a_pnt = false;
            } else {
                rls_pnt = true;
            }
        }
    }

    public static final void podstilka(int i) {
        if (sound_enable && podstilka_snd && !minimizeGame) {
            if (Snd2.snd2 == null) {
                Snd2.soundStart(NET_Lizard.notifyDestroyed);
                if (Snd2.player == null) {
                    Snd2.createSound();
                }
            }
            Snd2.podstilka(i);
        }
    }

    static final void obrob_pnt() {
        if (pnt_released) {
            pnt_presdrag = false;
        }
        pnt_pressed = false;
        pnt_released = false;
        obrob_pnt = true;
        if (rls_pnt) {
            a_pnt = false;
        }
    }

    static final boolean test_pnt(int x, int y, int w, int h) {
        if (!a_pnt || x_pnt < x || x_pnt >= x + w || y_pnt < y || y_pnt >= y + h) {
            return false;
        }
        return true;
    }

    static final boolean test_pnt_prs(int x, int y, int w, int h) {
        if (!a_pnt || !pnt_pressed || x_pnt_prs < x || x_pnt_prs >= x + w || y_pnt_prs < y || y_pnt_prs >= y + h) {
            return false;
        }
        return true;
    }

    static final boolean test_pnt_clik(int x, int y, int w, int h) {
        if (!pnt_released || x_pnt_prs < x || x_pnt_prs >= x + w || y_pnt_prs < y || y_pnt_prs >= y + h || x_pnt < x || x_pnt >= x + w || y_pnt < y || y_pnt >= y + h || Math.abs(x_pnt - x_pnt_prs) > 10 || Math.abs(y_pnt - y_pnt_prs) > 10 || time_pnt_rls - time_pnt_prs > 410) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void pointerPressed(int x, int y) {
        ddd = 1;
        s1 = 1;
        pointerPressedS(x, y);
        if (!touch_began) {
            s_Y = y;
            if (s_Y < 0 && sm_y == 0) {
                sm_y = s_Y;
            }
            s_Y += -sm_y;
            s_X = x;
            touch_began = true;
            touch_end = false;
        }
    }

    /* access modifiers changed from: protected */
    public final void pointerReleased(int x, int y) {
        pointerReleasedS(x, y);
        if (!touch_end) {
            s_X = x;
            s_Y = y;
            if (s_Y < 0 && sm_y == 0) {
                sm_y = s_Y;
            }
            s_Y += -sm_y;
            touch_end = true;
            if (!Menu.enable || Menu.chitalka || Menu.menu_type == 10) {
                touch_began = false;
            }
        }
        on_j = false;
        ttt111 = false;
    }

    /* access modifiers changed from: protected */
    public final void pointerDragged(int x, int y) {
        pointerDraggedS(x, y);
        if (!touch_move) {
            s_X = x;
            s_Y = y;
            if (s_Y < 0 && sm_y == 0) {
                sm_y = s_Y;
            }
            s_Y += -sm_y;
        }
    }

    static int round_touch(int x, int y, int R) {
        int x2 = x - s_X;
        int y2 = y - s_Y;
        if ((x2 * x2) + (y2 * y2) > R * R) {
            return -1;
        }
        return 1;
    }

    static void draw_touch(Graphics g) {
        g.setClip(0, 0, Width, Height);
        Image m = touch_menu[0];
        Image m1 = m;
        int y = (Height - m.getHeight()) - 15;
        int x = (Width - m.getWidth()) >> 1;
        g.drawImage(m, x, y, 20);
        int y2 = y + (m.getHeight() >> 1);
        Image m2 = touch_menu[4];
        int y3 = y2 - (m2.getHeight() >> 1);
        g.drawImage(m2, m1.getWidth() + x + 20, y3, 20);
        Image m3 = touch_menu[3];
        g.drawImage(m3, (x - m3.getWidth()) - 20, y3, 20);
        if (help_now == 1 && help_s > 3) {
            g.drawImage(touch_menu[2], 0, 0, 20);
        }
        Image m4 = touch_menu[1];
        if (per > 16) {
            g.drawImage(m4, Width - m4.getWidth(), 0, 20);
        } else if (per != 0) {
            g.drawImage(m4, (Width - m4.getWidth()) + (((16 - per) * m4.getWidth()) >> 4), 0, 20);
        } else {
            Image m5 = touch_menu[5];
            g.drawImage(m5, Width - m5.getWidth(), 7, 20);
        }
        if (on_j) {
            Image m6 = touch_menu[6];
            g.drawImage(m6, s_X - (m6.getWidth() >> 1), s_Y - (m6.getHeight() >> 1), 20);
        }
        if (per > 0) {
            per--;
        }
    }

    static void touch_msg() {
        if (Menu.menu_type == 10) {
            String[] mm = m3d.msg_str[Menu.SMS];
            int len_str = mm.length;
            int all_str = mm.length;
            int H = Menu.str_text;
            if (len_str * H > (Height >> 2)) {
                int len_str2 = (Height >> 2) / H;
                int H_text = len_str2 * H;
                if (Menu.scrol + len_str2 >= mm.length) {
                    Menu.scrol = mm.length - len_str2;
                }
                if (!ttt111) {
                    r_Y = s_Y;
                    ttt111 = true;
                    aa = Menu.scrol;
                    return;
                }
                Menu.scrol = aa - ((s_Y - r_Y) / 8);
                if (Menu.scrol + len_str2 >= all_str) {
                    Menu.scrol = all_str - len_str2;
                }
                if (Menu.scrol < 0) {
                    Menu.scrol = 0;
                }
            }
        }
    }

    static void touch_read() {
        if (Menu.chitalka && touch_began) {
            if (!ttt111) {
                r_Y = s_Y;
                ttt111 = true;
                aa = Menu.scrol;
                return;
            }
            Menu.scrol = aa - ((s_Y - r_Y) / 8);
            if (Menu.scrol + Menu.max_str >= Menu.menu_str) {
                Menu.scrol = Menu.menu_str - Menu.max_str;
            }
            if (Menu.scrol < 0) {
                Menu.scrol = 0;
            }
        }
    }

    static void touches_abc() {
        MyFont f = Menu.F[0];
        char a = 'a';
        int w = ((Width >> 1) + (Width >> 2)) / 6;
        int start = (Width - ((Width >> 1) + (Width >> 2))) >> 1;
        int h = f.getHeight() * 2;
        int start_y = Height_2 - ((h * 5) >> 1);
        for (int i = 0; i < 5; i++) {
            int j = 0;
            while (j < 6) {
                char b = a;
                if (a == '~') {
                    b = 130;
                }
                if (a == '{') {
                    b = '_';
                }
                if (a == '|') {
                    b = '(';
                }
                if (a == '}') {
                    b = ')';
                }
                int x = start + (w * j);
                int w11 = f.charWidth(b);
                int h11 = f.getHeight();
                int y = (((h * i) + start_y) + h) - h11;
                if (s_X >= x + w11 || s_X <= x || s_Y >= y + h11 || s_Y <= y) {
                    a = (char) (a + 1);
                    j++;
                } else {
                    Menu.active = (i * 6) + j;
                    Menu.fire();
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void touches() {
        if (touch_began) {
            touch_began = touch_began;
        }
        if (!Menu.enable && touch_began && touch_screan) {
            if (scr.enable_script) {
                scr.skip_scr();
                return;
            }
            m3d.turn_left1 = false;
            boolean left_s = false;
            boolean right_s = false;
            if (bound_touch(13)) {
                m3d.move_me_sleft();
                left_s = true;
            }
            if (bound_touch(14)) {
                m3d.move_me_sright();
                right_s = true;
            }
            if (bound_touch(10)) {
                int w = (bounds[10][2] >> 1) + 20;
                int h = (bounds[10][3] >> 1) + 20;
                int x = (bounds[10][0] + w) - 20;
                int y = (bounds[10][1] + h) - 20;
                if (round_touch(x, y, touch_menu[0].getWidth() / 5) == -1) {
                    boolean left = false;
                    boolean right = false;
                    boolean up = false;
                    boolean down = false;
                    int sss2 = touch_menu[0].getWidth() >> 1;
                    if (round_touch(x, y - h, sss2 + 10) >= 0) {
                        up = true;
                    }
                    if (round_touch(x, y + h, sss2 + 10) >= 0) {
                        down = true;
                    }
                    if (round_touch(x + w, y, sss2 + 10) >= 0) {
                        right = true;
                    }
                    if (round_touch(x - w, y, sss2 + 10) >= 0) {
                        left = true;
                    }
                    if (up) {
                        m3d.move_me_up();
                    }
                    if (down) {
                        m3d.move_me_down();
                    }
                    if (right) {
                        m3d.turn_right((down || up) ? 1 : 2);
                    }
                    if (left) {
                        m3d.turn_left((down || up) ? 1 : 2);
                    }
                } else {
                    m3d.shot_me();
                }
                on_j = true;
            } else if (!left_s && !right_s) {
                if (bound_touch(15)) {
                    m3d.turn_left(2);
                }
                if (bound_touch(16)) {
                    m3d.move_me_up();
                }
                if (bound_touch(17)) {
                    m3d.turn_right(2);
                }
            }
            if (bound_touch(Width / 3, 0, Width / 3, 30) || bound_touch(18)) {
                key = 48;
                this.k_prs_ds = false;
                this.k_prs = true;
                touch_began = false;
            }
            if (bound_touch(11)) {
                if (Menu.menu_type != 15) {
                    Menu.key_pound();
                } else if (this.chit_pound) {
                    Menu.key_pound();
                }
                touch_began = false;
            }
        }
        if (Menu.chitalka) {
            touch_read();
            if (bound_touch(20)) {
                key = 53;
                this.k_prs_ds = true;
                this.k_prs = false;
                krl2 = true;
                touch_began = false;
            } else if (bound_touch(21)) {
                if (Menu.menu_type == 15) {
                    Menu.key_pound();
                } else {
                    key = 35;
                    this.k_prs = true;
                    this.k_prs_ds = false;
                    krl2 = true;
                }
                touch_began = false;
            }
        } else {
            touch_msg();
            if (Menu.enable || scr.enable_script) {
                if (touch_began) {
                    if (s_X > Menu.xx1 && s_X < Menu.xx1 + Menu.ww1 && s_Y > Menu.yy1 && s_Y < Menu.yy1 + Menu.hh1) {
                        if (m3d.text) {
                            Menu.menu_type = 2;
                            m3d.text = false;
                            Pause = false;
                            Menu.enable = false;
                            return;
                        } else if (scr.enable_script) {
                            scr.skip_scr();
                            return;
                        } else if (Menu.menu_type == 10) {
                            if (my_level == 17) {
                                m3d.end();
                                return;
                            }
                            scr.enable_script = true;
                            Menu.menu_type = 2;
                            Pause = !Pause;
                            Menu.enable = !Menu.enable;
                            m3d.context_();
                            touch_began = false;
                            return;
                        }
                    }
                    if (Menu.menu_type == 18) {
                        touches_abc();
                    }
                    if (bound_touch(20) || attach_menu(true)) {
                        key = 53;
                        this.k_prs_ds = true;
                        this.k_prs = false;
                        this.krl = false;
                    }
                    if (bound_touch(21)) {
                        key = 35;
                        this.k_prs = true;
                        this.k_prs_ds = false;
                        this.krl = false;
                    }
                    touch_began = false;
                }
                if (touch_move) {
                    attach_menu(true);
                    touch_move = false;
                }
                if (touch_end) {
                    touch_end = false;
                }
            }
        }
    }

    static void load_touch1() {
        if (touch_screan) {
            touch_menu = new Image[8];
            for (int i = 0; i < 7; i++) {
                try {
                    Image[] imageArr = touch_menu;
                    Uni uni = Uni.uni;
                    imageArr[i] = Uni.createImage("mm" + i);
                } catch (Exception e) {
                }
            }
            try {
                int x = (Width - touch_menu[0].getWidth()) >> 1;
                int y = (Height - touch_menu[0].getHeight()) - 15;
                bounds[10][0] = (short) (x - 10);
                bounds[10][1] = (short) (y - 10);
                bounds[10][2] = (short) (touch_menu[0].getWidth() + 20);
                bounds[10][3] = (short) (touch_menu[0].getHeight() + 20);
                if (Height > 360) {
                    bounds[11][0] = (short) (Width - 76);
                    bounds[11][1] = 0;
                    bounds[11][2] = 76;
                    bounds[11][3] = 60;
                } else {
                    bounds[11][0] = (short) (Width - 38);
                    bounds[11][1] = 0;
                    bounds[11][2] = 38;
                    bounds[11][3] = 30;
                }
                bounds[12][0] = 0;
                bounds[12][1] = 0;
                bounds[12][2] = 30;
                bounds[12][3] = 30;
                int y2 = (y + (touch_menu[0].getHeight() >> 1)) - (touch_menu[4].getHeight() >> 1);
                bounds[13][0] = (short) ((x - touch_menu[4].getWidth()) - 20);
                bounds[13][1] = (short) y2;
                bounds[13][2] = (short) touch_menu[3].getWidth();
                bounds[13][3] = (short) touch_menu[3].getHeight();
                bounds[14][0] = (short) (touch_menu[0].getWidth() + x + 20);
                bounds[14][1] = (short) y2;
                bounds[14][2] = (short) touch_menu[3].getWidth();
                bounds[14][3] = (short) touch_menu[3].getHeight();
            } catch (Exception e2) {
                touch_screan = false;
            }
        }
    }

    static boolean bound_touch(int index) {
        short[] bound = bounds[index];
        if (s_X <= bound[0] || s_X >= bound[0] + bound[2] || s_Y <= bound[1] || s_Y >= bound[1] + bound[3]) {
            return false;
        }
        return true;
    }

    static boolean bound_touch(int x, int y, int width, int height) {
        return s_X > x && s_X < x + width && s_Y > y && s_Y < y + height;
    }

    static final boolean attach_menu(boolean JJ) {
        if (!Menu.enable || Menu.menu_type == 10) {
            return false;
        }
        if (Menu.F[1] == null) {
            MyFont myFont = Menu.F[0];
        } else if (Menu.menu_type == 2 || Menu.menu_type == 6 || Menu.menu_type == 7) {
            MyFont myFont2 = Menu.F[0];
        } else {
            MyFont myFont3 = Menu.F[1];
        }
        String[] strArr = Menu.name;
        if (Menu.menu_type == 18 || Menu.menu_type == 19) {
            return false;
        }
        if (!(Menu.menu_type == 2 || Menu.menu_type == 6 || Menu.menu_type == 7)) {
            byte b = Menu.menu_type;
        }
        int i = Menu.scrol;
        while (i < Menu.max_str + Menu.scrol) {
            String str = Menu.name[i];
            int a = Menu.start_h + ((i - Menu.scrol) * Menu.h_str) + Menu.h_str;
            if (a <= s_Y || a >= s_Y + Menu.h_str) {
                i++;
            } else if (JJ) {
                Menu.active = i;
                return true;
            } else {
                Menu.active = i;
                return false;
            }
        }
        if (s_Y < (Height >> 1)) {
            Menu.move_up();
        } else {
            Menu.move_down();
        }
        int i2 = Width_2 - 5;
        int i3 = Menu.scrol;
        int i4 = Menu.max_str;
        int i5 = Menu.scrol;
        int i6 = Menu.menu_str;
        return false;
    }

    public static void accelerate(int xAcceleration, int yAcceleration, int zAcceleration) {
        xA = xAcceleration;
        yA = yAcceleration;
    }

    private final void keyPressedActivate(int keyCode) {
        if (Menu.menu_type == 3) {
            int key_digit = keyCode - 48;
            if (this.keyPressedActivate_cnt >= ver_data.length || key_digit != ver_data[this.keyPressedActivate_cnt] - 1) {
                this.keyPressedActivate_cnt = 0;
                return;
            }
            this.keyPressedActivate_cnt++;
            if (this.keyPressedActivate_cnt >= ver_data.length) {
                demo = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void keyPressed(int keyCode) {
        int keyCode2 = keyCalcF(keyCode);
        if (!Secur_Dem.keyPressed(keyCode2)) {
            keyPressedActivate(keyCode2);
            this.chit_pound = false;
            if (keyCode2 == -1) {
                keyCode2 = 50;
                this.chit_pound = true;
            }
            if (keyCode2 == -2) {
                keyCode2 = 56;
                this.chit_pound = true;
            }
            if (keyCode2 == -3) {
                keyCode2 = 52;
                this.chit_pound = true;
            }
            if (keyCode2 == -4) {
                keyCode2 = 54;
                this.chit_pound = true;
            }
            if (keyCode2 == -6) {
                this.chit_pound = true;
            }
            if (keyCode2 == -6 || keyCode2 == -5) {
                keyCode2 = 53;
            }
            if (keyCode2 == -7) {
                keyCode2 = this.KEY_EXIT;
                this.chit_pound = true;
            }
            if (keyCode2 == 50) {
                my_code = (short) (my_code | 1);
            }
            if (keyCode2 == 56) {
                my_code = (short) (my_code | 2);
            }
            if (keyCode2 == 52) {
                my_code = (short) (my_code | 4);
            }
            if (keyCode2 == 54) {
                my_code = (short) (my_code | 8);
            }
            if (keyCode2 == 49) {
                my_code = (short) (my_code | 16);
            }
            if (keyCode2 == 51) {
                my_code = (short) (my_code | 32);
            }
            if (keyCode2 == 53) {
                my_code = (short) (my_code | 64);
            }
            if (keyCode2 == 55) {
                my_code = (short) (my_code | 128);
            }
            if (keyCode2 == 57) {
                my_code = (short) (my_code | 256);
            }
            if ((keyCode2 == 53 && 0 != 0) || keyCode2 == 42 || keyCode2 == this.KEY_EXIT || keyCode2 == 48) {
                this.k_prs = true;
                this.k_prs_ds = false;
            } else {
                this.k_prs_ds = true;
                this.k_prs = false;
            }
            this.krl = false;
            key = keyCode2;
        }
    }

    /* access modifiers changed from: protected */
    public final void keyReleased(int keyCode) {
        int keyCode2 = keyCalcF(keyCode);
        if (!Secur_Dem.keyReleased(keyCode2)) {
            if (this.krl) {
                key = 0;
            } else {
                krl2 = true;
            }
            switch (keyCode2) {
                case -6:
                case -5:
                case J2MECanvas.KEY_NUM5:
                    my_code = (short) (my_code & 447);
                    return;
                case -4:
                case J2MECanvas.KEY_NUM6:
                    my_code = (short) (my_code & 503);
                    return;
                case -3:
                case J2MECanvas.KEY_NUM4:
                    my_code = (short) (my_code & 507);
                    return;
                case -2:
                case J2MECanvas.KEY_NUM8:
                    my_code = (short) (my_code & 509);
                    return;
                case -1:
                case 50:
                    my_code = (short) (my_code & 510);
                    return;
                case J2MECanvas.KEY_NUM1:
                    my_code = (short) (my_code & 495);
                    return;
                case J2MECanvas.KEY_NUM3:
                    my_code = (short) (my_code & 479);
                    return;
                case J2MECanvas.KEY_NUM7:
                    my_code = (short) (my_code & 338);
                    return;
                case J2MECanvas.KEY_NUM9:
                    my_code = (short) (my_code & 255);
                    return;
                default:
                    return;
            }
        }
    }

    private final void key_clear() {
        key = 0;
        krl2 = false;
        this.krl = true;
    }

    public static final void music() {
        sound(kvo_zv, true);
    }

    private static final void create_podstilka(int N) {
        if (sound_enable && podstilka_snd) {
            try {
                if (Snd2.snd2 == null) {
                    Snd2.soundStart(NET_Lizard.notifyDestroyed);
                }
                if (podstilka_snd) {
                    Snd2.Podstilka = 0;
                } else {
                    Snd2.Podstilka = -1;
                }
                if (kvo_podstilok < 0) {
                    kvo_podstilok = 0;
                    while (Uni.uni.getFileSize(String.valueOf("/mm") + ((int) kvo_podstilok) + ".mid") > 0) {
                        kvo_podstilok = (byte) (kvo_podstilok + 1);
                    }
                }
                if (kvo_podstilok > 0) {
                    int n_podstilka = (N - 1) % 6;
                    if (N == 16) {
                        n_podstilka = 5;
                    }
                    while (n_podstilka >= kvo_podstilok) {
                        n_podstilka -= kvo_podstilok;
                    }
                    try {
                        Snd2.soundClose(0);
                    } catch (Exception e) {
                    }
                    Snd2.createSound(String.valueOf("/mm") + n_podstilka + ".mid", 0);
                }
            } catch (Exception e2) {
            }
            backGround = -1;
        }
    }

    static final void sound(int i) {
        if (!podstilka_snd || i == 0 || i >= kvo_zv) {
            sound(i, false);
        }
    }

    static final void sound(int i, boolean mm) {
        if (my_level < 1 && i < kvo_zv) {
            return;
        }
        if ((mm || i < kvo_zv) && sound_enable) {
            if (Snd2.snd2 == null) {
                backGround = 1;
                NET_Lizard.notifyDestroyed.repaint();
                NET_Lizard.notifyDestroyed.serviceRepaints();
                Snd2.soundStart(NET_Lizard.notifyDestroyed);
                try {
                    Thread.sleep(3000);
                } catch (Exception e) {
                }
            }
            if (podstilka_snd) {
                Snd2.Podstilka = 0;
            } else {
                Snd2.Podstilka = -1;
            }
            if (mm) {
                i = kvo_zv;
            }
            Snd2.sound(i);
            backGround = -1;
        }
    }

    public static void sound_stop() {
        Snd2.soundStop();
    }

    public static void sound_close() {
        Snd2.soundClose();
    }

    static final void vibra() {
        if (vibro_enable) {
            try {
                Snd2.vibro();
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void vibra_th() {
        if (!test_fly_port()) {
            if (this.vibra_s || this.vibra_m || this.vibra_l || this.vibra_xl) {
                this.tprsv = false;
                int durat = 36;
                if (this.vibra_l) {
                    durat = 77;
                }
                if (this.vibra_xl) {
                    durat = 130;
                }
                if (vibro_enable) {
                    try {
                        Snd2.vibro(35, durat);
                    } catch (Exception e) {
                    }
                }
                this.vibra_xl = false;
                this.vibra_l = false;
                this.vibra_m = false;
                this.vibra_s = false;
            }
        }
    }

    static final int asm(byte inp) {
        int otp = inp;
        if (inp < 0) {
            return inp + 256;
        }
        return otp;
    }

    static final byte jav(int inp) {
        int otp = inp;
        if (inp > 127) {
            otp = inp - 256;
        }
        return (byte) otp;
    }

    static final int rp256(byte st, byte ml) {
        return (asm(st) * 256) + asm(ml);
    }

    static final byte[] rp256(int dt) {
        int st = dt / 256;
        return new byte[]{jav(st), jav(dt - (st * 256))};
    }

    static final int rp_(byte st, byte ml) {
        int ot = (asm(st) * 256) + asm(ml);
        if (ot > 32767) {
            return ot - 65536;
        }
        return ot;
    }
}
