package ua.netlizard.egypt;

public final class anim {
    static final byte[][] anim;
    static final byte[][] anims;

    static {
        byte[] bArr = new byte[12];
        bArr[0] = 1;
        bArr[2] = 6;
        bArr[3] = 1;
        bArr[4] = 3;
        bArr[5] = 8;
        bArr[6] = 1;
        bArr[7] = 11;
        bArr[8] = 2;
        bArr[9] = 12;
        bArr[10] = 1;
        bArr[11] = 13;
        byte[] bArr2 = new byte[12];
        bArr2[0] = 1;
        bArr2[2] = 6;
        bArr2[3] = 1;
        bArr2[4] = 1;
        bArr2[5] = 8;
        bArr2[6] = 1;
        bArr2[7] = 9;
        bArr2[8] = 3;
        bArr2[9] = 10;
        bArr2[10] = 1;
        bArr2[11] = 12;
        byte[] bArr3 = new byte[8];
        bArr3[0] = 1;
        bArr3[2] = 2;
        bArr3[3] = 3;
        bArr3[4] = 2;
        bArr3[5] = 1;
        bArr3[6] = 1;
        bArr3[7] = 5;
        byte[] bArr4 = new byte[4];
        bArr4[0] = 1;
        bArr4[2] = 9;
        bArr4[3] = 1;
        byte[] bArr5 = new byte[16];
        bArr5[0] = 1;
        bArr5[2] = 8;
        bArr5[3] = 1;
        bArr5[4] = 5;
        bArr5[5] = 10;
        bArr5[6] = 1;
        bArr5[7] = 20;
        bArr5[8] = 3;
        bArr5[9] = 15;
        bArr5[10] = 1;
        bArr5[11] = 17;
        bArr5[12] = 3;
        bArr5[13] = 18;
        bArr5[14] = 3;
        bArr5[15] = 22;
        byte[] bArr6 = new byte[12];
        bArr6[0] = 1;
        bArr6[2] = 6;
        bArr6[3] = 1;
        bArr6[4] = 3;
        bArr6[5] = 8;
        bArr6[6] = 4;
        bArr6[7] = 11;
        bArr6[8] = 4;
        bArr6[9] = 15;
        bArr6[10] = 1;
        bArr6[11] = 18;
        byte[] bArr7 = new byte[14];
        bArr7[0] = 8;
        bArr7[2] = 4;
        bArr7[3] = 8;
        bArr7[4] = 14;
        bArr7[5] = 12;
        bArr7[6] = 12;
        bArr7[7] = 26;
        bArr7[8] = 4;
        bArr7[9] = 38;
        bArr7[10] = 4;
        bArr7[11] = 42;
        bArr7[12] = 4;
        bArr7[13] = 46;
        anims = new byte[][]{new byte[1], bArr, bArr2, bArr3, bArr4, bArr5, bArr6, bArr7};
        byte[] bArr8 = new byte[14];
        bArr8[1] = 1;
        bArr8[2] = 2;
        bArr8[3] = 3;
        bArr8[4] = 4;
        bArr8[5] = 5;
        bArr8[6] = 6;
        bArr8[7] = 7;
        bArr8[8] = 8;
        bArr8[9] = 9;
        bArr8[10] = 10;
        bArr8[11] = 11;
        bArr8[12] = 12;
        bArr8[13] = 13;
        byte[] bArr9 = new byte[13];
        bArr9[1] = 1;
        bArr9[2] = 2;
        bArr9[3] = 3;
        bArr9[4] = 4;
        bArr9[5] = 5;
        bArr9[6] = 6;
        bArr9[7] = 7;
        bArr9[8] = 8;
        bArr9[9] = 10;
        bArr9[10] = 11;
        bArr9[11] = 12;
        bArr9[12] = 13;
        byte[] bArr10 = new byte[6];
        bArr10[1] = 1;
        bArr10[2] = 2;
        bArr10[3] = 3;
        bArr10[4] = 4;
        bArr10[5] = 6;
        byte[] bArr11 = new byte[10];
        bArr11[0] = 2;
        bArr11[2] = 1;
        bArr11[3] = 2;
        bArr11[4] = 3;
        bArr11[5] = 4;
        bArr11[6] = 5;
        bArr11[7] = 6;
        bArr11[8] = 7;
        bArr11[9] = 8;
        byte[] bArr12 = new byte[25];
        bArr12[1] = 1;
        bArr12[2] = 2;
        bArr12[3] = 3;
        bArr12[4] = 4;
        bArr12[5] = 5;
        bArr12[6] = 6;
        bArr12[7] = 7;
        bArr12[8] = 8;
        bArr12[9] = 9;
        bArr12[10] = 10;
        bArr12[11] = 11;
        bArr12[12] = 12;
        bArr12[13] = 13;
        bArr12[14] = 14;
        bArr12[15] = 15;
        bArr12[16] = 16;
        bArr12[17] = 17;
        bArr12[18] = 18;
        bArr12[19] = 19;
        bArr12[20] = 20;
        bArr12[21] = 20;
        bArr12[22] = 17;
        bArr12[23] = 16;
        bArr12[24] = 15;
        byte[] bArr13 = new byte[19];
        bArr13[1] = 1;
        bArr13[2] = 2;
        bArr13[3] = 3;
        bArr13[4] = 4;
        bArr13[5] = 5;
        bArr13[6] = 6;
        bArr13[7] = 7;
        bArr13[8] = 8;
        bArr13[9] = 9;
        bArr13[10] = 10;
        bArr13[11] = 11;
        bArr13[12] = 12;
        bArr13[13] = 13;
        bArr13[14] = 14;
        bArr13[15] = 14;
        bArr13[16] = 15;
        bArr13[17] = 16;
        bArr13[18] = 17;
        byte[] bArr14 = new byte[50];
        bArr14[1] = 1;
        bArr14[2] = 2;
        bArr14[3] = 3;
        bArr14[4] = 4;
        bArr14[5] = 5;
        bArr14[6] = 6;
        bArr14[7] = 7;
        bArr14[8] = 21;
        bArr14[9] = 22;
        bArr14[10] = 23;
        bArr14[11] = 22;
        bArr14[12] = 21;
        bArr14[13] = 22;
        bArr14[14] = 23;
        bArr14[15] = 24;
        bArr14[16] = 25;
        bArr14[17] = 26;
        bArr14[18] = 26;
        bArr14[19] = 26;
        bArr14[20] = 26;
        bArr14[21] = 25;
        bArr14[22] = 24;
        bArr14[23] = 23;
        bArr14[24] = 22;
        bArr14[25] = 21;
        bArr14[26] = 9;
        bArr14[27] = 10;
        bArr14[28] = 11;
        bArr14[29] = 12;
        bArr14[30] = 13;
        bArr14[31] = 14;
        bArr14[32] = 15;
        bArr14[33] = 15;
        bArr14[34] = 15;
        bArr14[35] = 16;
        bArr14[36] = 17;
        bArr14[37] = 9;
        bArr14[38] = 9;
        bArr14[39] = 19;
        bArr14[40] = 20;
        bArr14[41] = 21;
        bArr14[42] = 21;
        bArr14[43] = 20;
        bArr14[44] = 19;
        bArr14[45] = 9;
        bArr14[46] = 21;
        bArr14[47] = 22;
        bArr14[48] = 23;
        bArr14[49] = 22;
        anim = new byte[][]{new byte[1], bArr8, bArr9, bArr10, bArr11, bArr12, bArr13, bArr14};
    }

    static final void anim_boss(unit u) {
        u.cadr = (byte) (u.cadr + 1);
        if (u.cadr >= anims[u.type_unit][u.type_anim << 1]) {
            u.cadr = 0;
        }
        u.anim = anim[u.type_unit][anims[u.type_unit][(u.type_anim << 1) + 1] + u.cadr];
    }

    static final void anim_pauk(unit u) {
        if (!(u.type_anim == 2 || u.type_anim == 4)) {
            u.type_anim = 0;
            if (!(u.F[0] == 0 && u.F[1] == 0 && u.F[2] == 0)) {
                u.type_anim = 1;
            }
            if (u.Fly && Math.abs(u.Speed[2]) != 0) {
                u.type_anim = 3;
            }
        }
        u.cadr = (byte) (u.cadr + 1);
        if (u.cadr >= anims[u.type_unit][u.type_anim << 1]) {
            u.cadr = 0;
            if (u.type_anim == 2) {
                u.type_anim = 3;
                return;
            } else if (u.death) {
                u.type_anim = 5;
                u.animation = false;
            }
        }
        u.anim = anim[u.type_unit][anims[u.type_unit][(u.type_anim << 1) + 1] + u.cadr];
    }

    static final void anim_mutant(unit u) {
        if (u.type_anim > 4 || u.type_anim < 2) {
            u.type_anim = 0;
            if (!(u.F[0] == 0 && u.F[1] == 0 && u.F[2] == 0)) {
                u.type_anim = 1;
            }
        }
        u.cadr = (byte) (u.cadr + 1);
        if (u.cadr >= anims[u.type_unit][u.type_anim << 1]) {
            u.cadr = 0;
            if (u.type_anim == 2 || u.type_anim == 3) {
                u.type_anim = 0;
            }
            if (u.death) {
                u.type_anim = 5;
                u.animation = false;
            }
        }
        u.anim = anim[u.type_unit][anims[u.type_unit][(u.type_anim << 1) + 1] + u.cadr];
    }

    static final void anim_sharik(unit u) {
        if (u.type_anim < 1) {
            u.type_anim = 0;
        }
        u.cadr = (byte) (u.cadr + 1);
        if (u.cadr >= anims[u.type_unit][u.type_anim << 1]) {
            u.cadr = 0;
            if (u.type_anim > 0) {
                u.type_anim = 0;
            }
            if (u.death) {
                u.type_anim = 3;
                u.animation = false;
            }
        }
        u.anim = anim[u.type_unit][anims[u.type_unit][(u.type_anim << 1) + 1] + u.cadr];
    }

    static final void anim_akula(unit u) {
        u.type_anim = 0;
        if (!(u.F[0] == 0 && u.F[1] == 0 && u.F[2] == 0)) {
            u.type_anim = 1;
        }
        u.cadr = (byte) (u.cadr + 1);
        if (u.cadr >= anims[u.type_unit][u.type_anim << 1]) {
            u.cadr = 0;
            if (u.death) {
                u.type_anim = 0;
                u.animation = false;
            }
        }
        u.anim = anim[u.type_unit][anims[u.type_unit][(u.type_anim << 1) + 1] + u.cadr];
    }

    static final void anim_ossiris(unit u) {
        if ((u.type_anim != 2 && u.type_anim < 4) || u.type_anim > 7) {
            u.type_anim = 0;
            if (!(u.F[0] == 0 && u.F[1] == 0 && u.F[2] == 0)) {
                u.type_anim = 1;
            }
        }
        u.cadr = (byte) (u.cadr + 1);
        if (u.cadr >= anims[u.type_unit][u.type_anim << 1]) {
            u.cadr = 0;
            if (u.type_anim == 4) {
                u.type_anim = 5;
            }
            if (u.type_anim == 7) {
                u.type_anim = 0;
            }
            if (u.death) {
                u.type_anim = 3;
                u.animation = false;
            }
        }
        u.anim = anim[u.type_unit][anims[u.type_unit][(u.type_anim << 1) + 1] + u.cadr];
    }

    static final void anim_scorpion(unit u) {
        if (u.type_anim > 4 || u.type_anim < 2) {
            u.type_anim = 0;
            if (!(u.F[0] == 0 && u.F[1] == 0 && u.F[2] == 0)) {
                u.type_anim = 1;
            }
        }
        u.cadr = (byte) (u.cadr + 1);
        if (u.cadr >= anims[u.type_unit][u.type_anim << 1]) {
            u.cadr = 0;
            if (u.type_anim == 2) {
                u.type_anim = 0;
            }
            if (u.type_anim == 3) {
                u.type_anim = 0;
            }
            if (u.death) {
                u.type_anim = 5;
                u.animation = false;
            }
        }
        u.anim = anim[u.type_unit][anims[u.type_unit][(u.type_anim << 1) + 1] + u.cadr];
    }

    static final void animation(unit u) {
        if (u.animation) {
            switch (u.type_unit) {
                case 1:
                    anim_pauk(u);
                    return;
                case 2:
                    anim_mutant(u);
                    return;
                case 3:
                    anim_sharik(u);
                    return;
                case 4:
                    anim_akula(u);
                    return;
                case 5:
                    anim_ossiris(u);
                    return;
                case 6:
                    anim_scorpion(u);
                    return;
                case 7:
                    anim_boss(u);
                    return;
                default:
                    return;
            }
        }
    }
}
