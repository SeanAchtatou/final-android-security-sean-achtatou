package ua.netlizard.egypt;

public final class BIP {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0119  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void draw_BIP_compres(int[] r23, int r24, int r25, int r26) {
        /*
            short[][] r21 = ua.netlizard.egypt.m3d.weapon_size
            r21 = r21[r24]
            r22 = 0
            short r20 = r21[r22]
            short[][] r21 = ua.netlizard.egypt.m3d.weapon_size
            r21 = r21[r24]
            r22 = 1
            short r4 = r21[r22]
            r1 = r20
            int r15 = ua.netlizard.egypt.m3d.Width_real_3D
            int r14 = ua.netlizard.egypt.m3d.Height_real_3D
            int r21 = r26 * r4
            int r21 = r21 >> 8
            int r21 = r14 - r21
            int r26 = r21 + -2
            int r21 = r15 - r20
            int r21 = r21 * r25
            int r25 = r21 >> 8
            r0 = r25
            short r0 = (short) r0
            r21 = r0
            ua.netlizard.egypt.m3d.x_fire = r21
            r0 = r26
            short r0 = (short) r0
            r21 = r0
            ua.netlizard.egypt.m3d.y_fire = r21
            r0 = r25
            if (r0 >= r15) goto L_0x0042
            int r21 = r25 + r20
            if (r21 < 0) goto L_0x0042
            r0 = r26
            if (r0 >= r14) goto L_0x0042
            int r21 = r26 + r4
            if (r21 >= 0) goto L_0x0043
        L_0x0042:
            return
        L_0x0043:
            int[][] r21 = ua.netlizard.egypt.m3d.FPS_palitra
            r5 = r21[r24]
            byte[][] r21 = ua.netlizard.egypt.m3d.FPS_weapon
            r19 = r21[r24]
            int[] r11 = ua.netlizard.egypt.m3d.palitra_help
            ua.netlizard.egypt.unit[] r21 = ua.netlizard.egypt.m3d.Units
            r22 = 0
            r21 = r21[r22]
            r0 = r21
            short r3 = r0.light
            int r9 = r5.length
            r6 = 0
        L_0x0059:
            if (r6 < r9) goto L_0x00a0
            r7 = 0
            r18 = 0
            r17 = 0
            int r16 = r15 - r20
            if (r25 >= 0) goto L_0x006d
            r0 = r25
            int r0 = -r0
            r18 = r0
            int r16 = r16 - r25
            r25 = 0
        L_0x006d:
            if (r26 >= 0) goto L_0x0076
            r0 = r26
            int r0 = -r0
            r17 = r0
            r26 = 0
        L_0x0076:
            int r21 = r25 + r20
            r0 = r21
            if (r0 <= r15) goto L_0x0080
            int r20 = r15 - r25
            int r16 = r15 - r20
        L_0x0080:
            int r21 = r26 + r4
            r0 = r21
            if (r0 <= r14) goto L_0x0088
            int r4 = r14 - r26
        L_0x0088:
            r8 = 0
            r13 = 0
            r2 = 0
            r12 = 0
            int r21 = r26 * r15
            int r7 = r21 + r25
            r6 = r17
        L_0x0092:
            if (r6 >= r4) goto L_0x0042
            r12 = 0
            r9 = r18
        L_0x0097:
            r0 = r20
            if (r9 < r0) goto L_0x00d2
        L_0x009b:
            int r7 = r7 + r16
            int r6 = r6 + 1
            goto L_0x0092
        L_0x00a0:
            r10 = r5[r6]
            r21 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r21 = r21 & r10
            r22 = 16711680(0xff0000, float:2.3418052E-38)
            r22 = r22 & r10
            int r22 = r22 >> 16
            int r22 = r22 * r3
            int r22 = r22 >> 8
            int r22 = r22 << 16
            int r21 = r21 + r22
            r22 = 65280(0xff00, float:9.1477E-41)
            r22 = r22 & r10
            int r22 = r22 >> 8
            int r22 = r22 * r3
            int r22 = r22 >> 8
            int r22 = r22 << 8
            int r21 = r21 + r22
            r0 = r10 & 255(0xff, float:3.57E-43)
            r22 = r0
            int r22 = r22 * r3
            int r22 = r22 >> 8
            int r21 = r21 + r22
            r11[r6] = r21
            int r6 = r6 + 1
            goto L_0x0059
        L_0x00d2:
            if (r13 != 0) goto L_0x0103
            byte r2 = r19[r8]
            if (r2 >= 0) goto L_0x00db
            int r13 = -r2
            int r8 = r8 + 1
        L_0x00db:
            byte r21 = r19[r8]
            r2 = r11[r21]
            int r8 = r8 + 1
            if (r2 != 0) goto L_0x0117
            if (r13 <= 0) goto L_0x0117
            int r21 = r9 + r13
            r0 = r21
            if (r0 >= r1) goto L_0x00f8
            int r21 = r13 + 1
            int r12 = r12 + r21
            int r21 = r13 + 1
            int r7 = r7 + r21
            int r9 = r9 + r13
            r13 = 0
        L_0x00f5:
            int r9 = r9 + 1
            goto L_0x0097
        L_0x00f8:
            int r21 = r1 - r9
            int r21 = r13 - r21
            int r13 = r21 + 1
            int r21 = r1 - r9
            int r7 = r7 + r21
            goto L_0x009b
        L_0x0103:
            if (r2 != 0) goto L_0x0115
            if (r13 <= 0) goto L_0x0115
            int r21 = r9 + r13
            r0 = r21
            if (r0 > r1) goto L_0x0115
            int r12 = r12 + r13
            int r7 = r7 + r13
            int r21 = r13 + -1
            int r9 = r9 + r21
            r13 = 0
            goto L_0x00f5
        L_0x0115:
            int r13 = r13 + -1
        L_0x0117:
            if (r2 == 0) goto L_0x011b
            r23[r7] = r2
        L_0x011b:
            int r7 = r7 + 1
            int r12 = r12 + 1
            goto L_0x00f5
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.BIP.draw_BIP_compres(int[], int, int, int):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public static final void draw_BIP(int[] display, int num, int x, int y) {
        short s = m3d.weapon_size[num][0];
        short s2 = m3d.weapon_size[num][1];
        int W = s;
        int s_width = m3d.Width_real_3D;
        int s_height = m3d.Height_real_3D;
        byte[] texture = m3d.FPS_weapon[num];
        int[] palitra = m3d.FPS_palitra[num];
        if (x < s_width && x + s >= 0 && y < s_height && y + s2 >= 0) {
            int start_j = 0;
            int start_i = 0;
            int smesh = s_width - s;
            if (x < 0) {
                start_j = -x;
                smesh -= x;
                x = 0;
            }
            if (y < 0) {
                start_i = -y;
                y = 0;
            }
            int i = x + s;
            int width = s;
            if (i > s_width) {
                width = s_width - x;
                smesh = s_width - width;
            }
            int i2 = y + s2;
            int height = s2;
            if (i2 > s_height) {
                height = s_height - y;
            }
            int index = (y * s_width) + x;
            for (int i3 = start_i; i3 < height; i3++) {
                int j = start_j;
                int index2 = index;
                while (j < width) {
                    display[index2] = palitra[texture[(i3 * W) + j]];
                    j++;
                    index2++;
                }
                index = index2 + smesh;
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [byte, int], vars: [r4v0 ?, r4v1 ?, r4v2 ?]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:102)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:78)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:69)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:51)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:32)
        */
    public static final int[] load_bip_palitra(byte[] r7) {
        /*
            r1 = 3
            r0 = 0
            byte r5 = r7[r1]
            r6 = 1
            if (r5 != r6) goto L_0x0008
            r0 = 1
        L_0x0008:
            int r1 = r1 + 1
            int r1 = r1 + 1
            byte r4 = r7[r1]
            if (r4 >= 0) goto L_0x0012
            int r4 = r4 + 256
        L_0x0012:
            int[] r3 = new int[r4]
            if (r0 != 0) goto L_0x0037
            r2 = 0
        L_0x0017:
            if (r2 < r4) goto L_0x001a
        L_0x0019:
            return r3
        L_0x001a:
            int r1 = r1 + 1
            byte r5 = r7[r1]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << 16
            int r1 = r1 + 1
            byte r6 = r7[r1]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 8
            r5 = r5 | r6
            int r1 = r1 + 1
            byte r6 = r7[r1]
            r6 = r6 & 255(0xff, float:3.57E-43)
            r5 = r5 | r6
            r3[r2] = r5
            int r2 = r2 + 1
            goto L_0x0017
        L_0x0037:
            r2 = 0
        L_0x0038:
            if (r2 >= r4) goto L_0x0019
            int r1 = r1 + 1
            byte r5 = r7[r1]
            if (r5 == 0) goto L_0x0060
            r5 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            int r1 = r1 + 1
            byte r6 = r7[r1]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 16
            r5 = r5 | r6
            int r1 = r1 + 1
            byte r6 = r7[r1]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 8
            r5 = r5 | r6
            int r1 = r1 + 1
            byte r6 = r7[r1]
            r6 = r6 & 255(0xff, float:3.57E-43)
            r5 = r5 | r6
            r3[r2] = r5
        L_0x005d:
            int r2 = r2 + 1
            goto L_0x0038
        L_0x0060:
            int r1 = r1 + 3
            r5 = 0
            r3[r2] = r5
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.BIP.load_bip_palitra(byte[]):int[]");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:11:0x001e */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int get_(byte[] r7, byte r8) {
        /*
            r6 = 1
            r3 = 3
            r1 = 0
            byte r5 = r7[r3]
            if (r5 != r6) goto L_0x0008
            r1 = 1
        L_0x0008:
            int r3 = r3 + 1
            r5 = 2
            if (r8 != r5) goto L_0x0010
            byte r4 = r7[r3]
        L_0x000f:
            return r4
        L_0x0010:
            int r3 = r3 + 1
            byte r4 = r7[r3]
            if (r4 >= 0) goto L_0x0018
            int r4 = r4 + 256
        L_0x0018:
            if (r1 == 0) goto L_0x0051
            int r5 = r4 * 4
            int r3 = r5 + 5
        L_0x001e:
            if (r8 != 0) goto L_0x0036
            int r3 = r3 + 1
            byte r0 = r7[r3]
            if (r0 >= 0) goto L_0x0029
            int r5 = r0 + 256
            short r0 = (short) r5
        L_0x0029:
            int r3 = r3 + 1
            byte r2 = r7[r3]
            if (r2 >= 0) goto L_0x0032
            int r5 = r2 + 256
            short r2 = (short) r5
        L_0x0032:
            int r5 = r0 << 8
            r4 = r5 | r2
        L_0x0036:
            if (r8 != r6) goto L_0x000f
            int r3 = r3 + 2
            int r3 = r3 + 1
            byte r0 = r7[r3]
            if (r0 >= 0) goto L_0x0043
            int r5 = r0 + 256
            short r0 = (short) r5
        L_0x0043:
            int r3 = r3 + 1
            byte r2 = r7[r3]
            if (r2 >= 0) goto L_0x004c
            int r5 = r2 + 256
            short r2 = (short) r5
        L_0x004c:
            int r5 = r0 << 8
            r4 = r5 | r2
            goto L_0x000f
        L_0x0051:
            int r5 = r4 * 3
            int r3 = r5 + 5
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.BIP.get_(byte[], byte):int");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:44:0x005d */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:42:0x005d */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final byte[] load_bip_points(byte[] r19, boolean r20, int r21, int r22, int r23, int r24) {
        /*
            r9 = 3
            r2 = 0
            byte r17 = r19[r9]
            r18 = 1
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x000d
            r2 = 1
        L_0x000d:
            r4 = 0
            int r9 = r9 + 1
            byte r17 = r19[r9]
            r18 = 1
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x001b
            r4 = 1
        L_0x001b:
            int r9 = r9 + 1
            byte r13 = r19[r9]
            if (r13 >= 0) goto L_0x0023
            int r13 = r13 + 256
        L_0x0023:
            if (r2 == 0) goto L_0x005e
            int r17 = r13 * 4
            int r9 = r17 + 5
        L_0x0029:
            r15 = 0
            byte[] r15 = (byte[]) r15
            if (r20 != 0) goto L_0x0087
            int r9 = r9 + 1
            byte r17 = r19[r9]
            int r17 = r17 << 8
            int r9 = r9 + 1
            byte r18 = r19[r9]
            r16 = r17 | r18
            int r9 = r9 + 1
            byte r17 = r19[r9]
            int r17 = r17 << 8
            int r9 = r9 + 1
            byte r18 = r19[r9]
            r5 = r17 | r18
            if (r16 >= 0) goto L_0x004e
            r0 = r16
            int r0 = r0 + 256
            r16 = r0
        L_0x004e:
            if (r5 >= 0) goto L_0x0052
            int r5 = r5 + 256
        L_0x0052:
            int r9 = r9 + 1
            if (r4 != 0) goto L_0x006c
            int r14 = r16 * r5
            byte[] r15 = new byte[r14]
            r12 = 0
        L_0x005b:
            if (r12 < r14) goto L_0x0063
        L_0x005d:
            return r15
        L_0x005e:
            int r17 = r13 * 3
            int r9 = r17 + 5
            goto L_0x0029
        L_0x0063:
            int r9 = r9 + 1
            byte r17 = r19[r9]
            r15[r12] = r17
            int r12 = r12 + 1
            goto L_0x005b
        L_0x006c:
            int r9 = r9 + 1
            r0 = r19
            int r0 = r0.length
            r17 = r0
            int r14 = r17 - r9
            byte[] r15 = new byte[r14]
            r12 = 0
            r10 = r9
        L_0x0079:
            if (r12 < r14) goto L_0x007d
            r9 = r10
            goto L_0x005d
        L_0x007d:
            int r9 = r10 + 1
            byte r17 = r19[r10]
            r15[r12] = r17
            int r12 = r12 + 1
            r10 = r9
            goto L_0x0079
        L_0x0087:
            r11 = 0
            int r9 = r9 + 1
            byte r17 = r19[r9]
            int r17 = r17 << 8
            int r9 = r9 + 1
            byte r18 = r19[r9]
            r8 = r17 | r18
            int r9 = r9 + 1
            byte r17 = r19[r9]
            int r17 = r17 << 8
            int r9 = r9 + 1
            byte r18 = r19[r9]
            r7 = r17 | r18
            if (r8 >= 0) goto L_0x00a4
            int r8 = r8 + 256
        L_0x00a4:
            if (r7 >= 0) goto L_0x00a8
            int r7 = r7 + 256
        L_0x00a8:
            int r17 = r23 * r24
            r0 = r17
            byte[] r15 = new byte[r0]
            int r17 = r22 * r8
            int r17 = r17 + r21
            int r9 = r9 + r17
            int r9 = r9 + 1
            int r9 = r9 + 1
            r6 = 0
        L_0x00b9:
            r0 = r24
            if (r6 >= r0) goto L_0x005d
            int r17 = r6 * r8
            int r3 = r17 + r9
            r12 = 0
        L_0x00c2:
            r0 = r23
            if (r12 < r0) goto L_0x00c9
            int r6 = r6 + 1
            goto L_0x00b9
        L_0x00c9:
            int r17 = r3 + r12
            byte r17 = r19[r17]
            r15[r11] = r17
            int r11 = r11 + 1
            int r12 = r12 + 1
            goto L_0x00c2
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.BIP.load_bip_points(byte[], boolean, int, int, int, int):byte[]");
    }

    public static final void invert_texture(byte[] texture1, int size_tex) {
        int size_texture = (1 << size_tex) - 1;
        int half_size = size_texture >> 1;
        for (int i = size_texture; i >= 0; i--) {
            for (int j = half_size; j >= 0; j--) {
                byte tmp = texture1[((size_texture + 1) * j) + i];
                texture1[((size_texture + 1) * j) + i] = texture1[((size_texture - j) * (size_texture + 1)) + i];
                texture1[((size_texture - j) * (size_texture + 1)) + i] = tmp;
            }
        }
    }
}
