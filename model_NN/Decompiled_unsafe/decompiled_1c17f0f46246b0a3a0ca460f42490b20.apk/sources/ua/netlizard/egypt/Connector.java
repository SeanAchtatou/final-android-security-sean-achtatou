package ua.netlizard.egypt;

import java.net.HttpURLConnection;
import java.net.URL;

public class Connector {
    public static final int READ = 1;
    public static final int READ_WRITE = 2;
    public static final int WRITE = 3;

    static Connection open(String name) {
        return null;
    }

    static Connection open(String str, int mode, boolean timeouts) {
        try {
            return new HttpConnection((HttpURLConnection) new URL(str).openConnection());
        } catch (Exception e) {
            return null;
        }
    }
}
