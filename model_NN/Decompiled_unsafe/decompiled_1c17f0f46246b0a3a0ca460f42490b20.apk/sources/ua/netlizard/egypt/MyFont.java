package ua.netlizard.egypt;

import java.lang.reflect.Array;

public class MyFont {
    public static final int FACE_MONOSPACE = 32;
    public static final int FACE_PROPORTIONAL = 64;
    public static final int FACE_SYSTEM = 0;
    public static final int SIZE_LARGE = 16;
    public static final int SIZE_MEDIUM = 0;
    public static final int SIZE_SMALL = 8;
    public static final int STYLE_BOLD = 1;
    public static final int STYLE_ITALIC = 2;
    public static final int STYLE_PLAIN = 0;
    public static final int STYLE_UNDERLINED = 0;
    int Height_without_fsy;
    final int a_en_st;
    final int a_ru_st;
    boolean bigsmall;
    short[][] cf_;
    Image font;
    int fsx;
    int fsy;
    boolean kx2b = false;
    boolean ky2b = false;
    final byte[] mas_index_uni;
    int msf_e = 127;
    final int multilan_bigsmall_stsim;
    final int multilan_bigsmall_stsimsml;
    final int multilan_stsim;
    String nCf = "";
    String nFnt = "";
    final int small_en_sm;
    boolean small_english;
    final int small_ru_sm;
    boolean small_russian;
    final int st_ind_uni;

    public MyFont() {
        byte[] bArr = new byte[194];
        bArr[0] = 70;
        bArr[1] = 4;
        bArr[2] = 71;
        bArr[3] = 86;
        bArr[6] = 83;
        bArr[7] = 72;
        bArr[8] = 73;
        bArr[9] = 8;
        bArr[10] = 74;
        bArr[11] = 75;
        bArr[12] = 85;
        bArr[13] = 10;
        bArr[14] = 76;
        bArr[15] = 77;
        bArr[17] = 82;
        bArr[19] = 12;
        bArr[20] = 78;
        bArr[21] = 87;
        bArr[22] = 2;
        bArr[25] = 79;
        bArr[26] = 16;
        bArr[27] = 80;
        bArr[28] = 3;
        bArr[29] = 17;
        bArr[31] = 1;
        bArr[32] = 91;
        bArr[33] = -4;
        bArr[34] = 92;
        bArr[35] = 107;
        bArr[38] = 104;
        bArr[39] = 93;
        bArr[40] = 94;
        bArr[41] = -8;
        bArr[42] = 95;
        bArr[43] = 96;
        bArr[44] = 106;
        bArr[45] = -10;
        bArr[46] = 97;
        bArr[47] = 98;
        bArr[49] = 103;
        bArr[51] = -12;
        bArr[52] = 99;
        bArr[53] = 108;
        bArr[54] = -2;
        bArr[57] = 100;
        bArr[58] = -16;
        bArr[59] = 101;
        bArr[60] = -3;
        bArr[61] = -17;
        bArr[62] = -1;
        bArr[63] = 102;
        bArr[66] = 5;
        bArr[67] = -5;
        bArr[68] = 19;
        bArr[69] = -19;
        bArr[70] = 20;
        bArr[71] = -20;
        bArr[76] = 6;
        bArr[77] = -6;
        bArr[78] = 7;
        bArr[79] = -7;
        bArr[84] = 9;
        bArr[85] = -9;
        bArr[88] = 21;
        bArr[89] = -21;
        bArr[90] = 9;
        bArr[91] = -9;
        bArr[129] = 22;
        bArr[130] = -22;
        bArr[131] = 23;
        bArr[132] = -23;
        bArr[135] = 11;
        bArr[136] = -11;
        bArr[146] = 84;
        bArr[147] = 105;
        bArr[152] = 13;
        bArr[153] = -13;
        bArr[154] = 24;
        bArr[155] = -24;
        bArr[160] = 14;
        bArr[161] = -14;
        bArr[164] = 15;
        bArr[165] = -15;
        bArr[174] = 27;
        bArr[175] = -27;
        bArr[184] = 81;
        bArr[185] = 25;
        bArr[186] = -25;
        bArr[187] = 26;
        bArr[188] = -26;
        bArr[189] = 18;
        bArr[190] = -18;
        this.mas_index_uni = bArr;
        this.a_en_st = 100;
        this.a_ru_st = 131;
        this.small_en_sm = 35;
        this.small_ru_sm = -61;
        this.st_ind_uni = 192;
        this.small_english = true;
        this.small_russian = true;
        this.bigsmall = true;
        this.multilan_stsim = 100;
        this.multilan_bigsmall_stsim = 165;
        this.multilan_bigsmall_stsimsml = 201;
        this.fsx = 1;
        this.fsy = 1;
    }

    public static MyFont getDefaultFont() {
        MyFont myFont = new MyFont();
        myFont.font = null;
        myFont.create_font();
        myFont.cf_ = null;
        myFont.create_cf();
        return myFont;
    }

    public static MyFont getFont(int face, int style, int size) {
        MyFont myFont = new MyFont();
        myFont.font = null;
        myFont.create_font();
        myFont.cf_ = null;
        myFont.create_cf();
        return myFont;
    }

    public static MyFont getFont(int face, int style, int size, String n_fnt) {
        MyFont myFont = new MyFont();
        myFont.nFnt = n_fnt;
        myFont.font = null;
        myFont.create_font();
        myFont.cf_ = null;
        myFont.create_cf();
        return myFont;
    }

    public static MyFont getFont(int face, int style, int size, String n_fnt, String n_cf) {
        MyFont myFont = new MyFont();
        myFont.nFnt = n_fnt;
        myFont.nCf = n_cf;
        myFont.font = null;
        myFont.create_font();
        myFont.cf_ = null;
        myFont.create_cf();
        return myFont;
    }

    static final int asms(short inp) {
        int otp = inp;
        if (inp < 0) {
            return inp + 256;
        }
        return otp;
    }

    private final void create_cf() {
        byte[] mas_load = Uni.uni.load_pack("cf" + this.nCf, -1);
        int tt = mas_load.length / 6;
        if (mas_load.length != tt * 6) {
            tt = mas_load.length / 7;
            if (mas_load.length != tt * 7) {
                tt = mas_load.length / 8;
                if (mas_load.length == tt * 8) {
                    this.kx2b = true;
                    this.ky2b = true;
                }
            } else if (this.font != null) {
                if (this.font.getWidth() > this.font.getHeight()) {
                    this.kx2b = true;
                } else {
                    this.ky2b = true;
                }
            }
        }
        if (this.msf_e < tt) {
            this.msf_e = tt;
        }
        this.cf_ = (short[][]) Array.newInstance(Short.TYPE, this.msf_e + 2, 6);
        int nl = 0;
        for (int ny = 0; ny < this.msf_e; ny++) {
            for (int nx = 0; nx < 6; nx++) {
                this.cf_[ny][nx] = mas_load[nl];
                nl++;
                if (nx == 0 && this.kx2b) {
                    this.cf_[ny][nx] = (short) ((asms(this.cf_[ny][nx]) << 8) + asms(mas_load[nl]));
                    nl++;
                }
                if (nx == 1 && this.ky2b) {
                    this.cf_[ny][nx] = (short) ((asms(this.cf_[ny][nx]) << 8) + asms(mas_load[nl]));
                    nl++;
                }
            }
            if (this.cf_[ny][0] < 0) {
                short[] sArr = this.cf_[ny];
                sArr[0] = (short) (sArr[0] + 256);
            }
            if (this.cf_[ny][1] < 0) {
                short[] sArr2 = this.cf_[ny];
                sArr2[1] = (short) (sArr2[1] + 256);
            }
            if (nl >= mas_load.length) {
                break;
            }
        }
        if (this.cf_[0][2] == 0) {
            this.cf_[0][2] = this.cf_[16][2];
        }
        if (this.cf_[0][2] == 0) {
            this.cf_[0][2] = this.cf_[33][2];
        }
        this.cf_[0][3] = 0;
        if (tt < 146) {
            this.small_english = false;
            this.small_russian = false;
            this.bigsmall = false;
        } else {
            this.small_english = true;
            this.small_russian = true;
            this.bigsmall = true;
            int j = 33;
            for (int i = 100; i <= 125; i++) {
                if (this.cf_[i][2] == 0) {
                    for (int k = 0; k < this.cf_[i].length; k++) {
                        this.cf_[i][k] = this.cf_[j][k];
                    }
                }
                j++;
            }
            int j2 = 64;
            for (int i2 = 131; i2 <= 162; i2++) {
                if (this.cf_[i2][2] == 0) {
                    for (int k2 = 0; k2 < this.cf_[i2].length; k2++) {
                        this.cf_[i2][k2] = this.cf_[j2][k2];
                    }
                }
                j2++;
            }
            int j3 = 165;
            for (int i3 = 201; i3 <= 231; i3++) {
                if (this.cf_[i3][2] == 0) {
                    for (int k3 = 0; k3 < this.cf_[i3].length; k3++) {
                        this.cf_[i3][k3] = this.cf_[j3][k3];
                    }
                }
                j3++;
            }
            int j4 = 235;
            int i4 = 256;
            while (i4 <= 276) {
                try {
                    if (this.cf_[i4][2] == 0) {
                        for (int k4 = 0; k4 < this.cf_[i4].length; k4++) {
                            this.cf_[i4][k4] = this.cf_[j4][k4];
                        }
                    }
                    j4++;
                    i4++;
                } catch (Exception e) {
                }
            }
        }
        this.Height_without_fsy = this.cf_[get_cf(1062)][3];
        short s = 0;
        long tmp = 0;
        int kv = 0;
        int[] big_sims = new int[3];
        for (int j5 = 0; j5 < big_sims.length; j5++) {
            int rzmm = 0;
            int rzmn = 0;
            for (int i5 = 65; i5 <= 90; i5++) {
                if (!(i5 == big_sims[0] || i5 == big_sims[1] || i5 == big_sims[2])) {
                    short s2 = this.cf_[get_cf(i5)][3];
                    if (rzmm < s2) {
                        rzmm = s2;
                        rzmn = i5;
                    }
                }
            }
            big_sims[j5] = rzmn;
        }
        int pix_min = 100;
        int pix_max = -100;
        for (int i6 = 65; i6 <= 90; i6++) {
            if (!(i6 == 81 || i6 == 74 || i6 == big_sims[0] || i6 == big_sims[1] || i6 == big_sims[2])) {
                tmp += (long) this.cf_[get_cf(i6)][3];
                if (s < this.cf_[get_cf(i6)][3]) {
                    s = this.cf_[get_cf(i6)][3];
                }
                kv++;
                int j6 = get_cf(i6);
                int p_max = -this.cf_[j6][5];
                int p_min = -(this.cf_[j6][3] + this.cf_[j6][5]);
                if (pix_max < p_max) {
                    pix_max = p_max;
                }
                if (pix_min > p_min) {
                    pix_min = p_min;
                }
            }
        }
        this.Height_without_fsy = (int) (tmp / ((long) kv));
        this.Height_without_fsy = s;
        this.Height_without_fsy = pix_max - pix_min;
        int pix_min2 = 100;
        int pix_max2 = -100;
        for (int i7 = 0; i7 < this.cf_.length; i7++) {
            int p_max2 = -this.cf_[i7][5];
            int p_min2 = -(this.cf_[i7][3] + this.cf_[i7][5]);
            if (pix_max2 < p_max2) {
                pix_max2 = p_max2;
            }
            if (pix_min2 > p_min2) {
                pix_min2 = p_min2;
            }
        }
        int max_delt = pix_max2 - pix_min2;
        if (this.Height_without_fsy > max_delt) {
            this.Height_without_fsy = max_delt;
        }
        int rv_verh = (-pix_min2) - this.Height_without_fsy;
        int korekt = rv_verh - ((pix_max2 + rv_verh) >> 1);
        if (korekt != 0) {
            for (int i8 = 0; i8 < this.cf_.length; i8++) {
                short[] sArr3 = this.cf_[i8];
                sArr3[5] = (short) (sArr3[5] - korekt);
            }
        }
        int pix_min3 = 100;
        int pix_max3 = -100;
        for (int i9 = 65; i9 <= 90; i9++) {
            if (!(i9 == big_sims[0] || i9 == big_sims[1] || i9 == big_sims[2])) {
                int j7 = get_cf(i9);
                int p_max3 = -this.cf_[j7][5];
                int p_min3 = -(this.cf_[j7][3] + this.cf_[j7][5]);
                if (pix_max3 < p_max3) {
                    pix_max3 = p_max3;
                }
                if (pix_min3 > p_min3) {
                    pix_min3 = p_min3;
                }
            }
        }
        int max_delt2 = pix_max3 - pix_min3;
        int rv_verh2 = (-pix_min3) - this.Height_without_fsy;
        int korekt2 = rv_verh2 - ((pix_max3 + rv_verh2) >> 1);
        if (korekt2 != 0) {
            for (int i10 = 0; i10 < this.cf_.length; i10++) {
                short[] sArr4 = this.cf_[i10];
                sArr4[5] = (short) (sArr4[5] - korekt2);
            }
        }
    }

    private final void create_font() {
        try {
            this.font = Uni.createImage("/fnt" + this.nFnt + ".png");
        } catch (Exception e) {
        }
    }

    private int get_cf(int inp) {
        int aa = inp;
        if (aa == 946) {
            aa = 223;
        }
        if (aa == 161) {
            return 277;
        }
        if (aa == 191) {
            return 278;
        }
        if (aa < 192 || aa - 192 >= this.mas_index_uni.length) {
            if (aa > 848) {
                aa -= 848;
            }
            if (aa >= 97 && aa <= 122) {
                aa = this.small_english ? aa + 35 : aa - 32;
            } else if (aa >= 192 && aa <= 223) {
                aa -= 96;
            } else if (aa >= 224 && aa <= 255) {
                aa = this.small_russian ? aa - 61 : aa - 128;
            } else if (aa >= 154 && aa <= 191) {
                aa -= 26;
            }
            int aa2 = aa - 32;
            if (aa2 < 0 || aa2 >= this.msf_e) {
                aa2 = 0;
            }
            return aa2;
        }
        byte b = this.mas_index_uni[aa - 192];
        if (b < 0 || aa == 228) {
            int dtdop = -b;
            int tmpsm = 100;
            if (this.bigsmall) {
                tmpsm = 201;
            }
            return tmpsm + dtdop;
        }
        int tmpsm2 = 100;
        if (this.bigsmall) {
            tmpsm2 = 165;
        }
        return tmpsm2 + b;
    }

    public void setFont(MyFont font2) {
    }

    public int getBaselinePosition() {
        return 0;
    }

    public int getHeight() {
        return this.fsy + this.Height_without_fsy;
    }

    public int charWidth(char ch) {
        if (ch == 10 || ch == 13) {
            return 0;
        }
        int nim = get_cf(ch);
        if (nim >= this.cf_.length) {
            nim = 0;
        }
        return this.fsx + this.cf_[nim][2];
    }

    public int charsWidth(char[] ch, int offset, int length) {
        int otp = 0;
        for (int a = 0; a < length; a++) {
            otp += charWidth(ch[a + offset]);
        }
        return otp;
    }

    public int stringWidth(String str) {
        int otp = 0;
        int length = str.length();
        for (int a = 0; a < length; a++) {
            otp += charWidth(str.charAt(a));
        }
        return otp;
    }

    public int substringWidth(String str, int offset, int len) {
        int otp = 0;
        int a = 0;
        while (a < len && a + offset < str.length()) {
            otp += charWidth(str.charAt(a + offset));
            a++;
        }
        return otp;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public final void drawSubstring(Graphics g, String txt, int offset, int len, int xx, int yy, int anc) {
        int x = xx;
        int y = yy + this.Height_without_fsy;
        if (!(anc == 0 || anc == 20)) {
            int[] korr = calc_anc(txt, anc, offset, len);
            x += korr[0];
            y += korr[1];
        }
        int sc1 = g.getClipX();
        int sc2 = g.getClipY();
        int sc3 = g.getClipWidth();
        int sc4 = g.getClipHeight();
        for (int a = offset; a < offset + len; a++) {
            char ch = txt.charAt(a);
            int nim = get_cf(ch);
            if (nim >= this.cf_.length) {
                nim = 0;
            }
            short[] cf_nim = this.cf_[nim];
            short s = cf_nim[3];
            int x1 = x + cf_nim[4];
            int y1 = y - (cf_nim[5] + s);
            short s2 = cf_nim[2];
            short s3 = cf_nim[3];
            int xc = x1;
            int yc = y1;
            int w1 = s2;
            if (xc < sc1) {
                w1 = s2 - (sc1 - xc);
                xc = sc1;
            }
            int h1 = s3;
            if (yc < sc2) {
                h1 = s3 - (sc2 - yc);
                yc = sc2;
            }
            int i = xc + w1;
            int w12 = w1;
            if (i > sc1 + sc3) {
                w12 = (sc1 + sc3) - xc;
            }
            int i2 = yc + h1;
            int h12 = h1;
            if (i2 > sc2 + sc4) {
                h12 = (sc2 + sc4) - yc;
            }
            if (w12 > 0 && h12 > 0) {
                g.setClip(xc, yc, w12, h12);
                g.drawImage(this.font, x1 - cf_nim[0], y1 - cf_nim[1], 20);
            }
            if (!(ch == 10 || ch == 13)) {
                x += cf_nim[2] + this.fsx;
            }
        }
        g.setClip(sc1, sc2, sc3, sc4);
    }

    public final void drawString(Graphics g, String txt, int xx, int yy, int anc) {
        drawSubstring(g, txt, 0, txt.length(), xx, yy, anc);
    }

    public final void drawChars(Graphics g, char[] txt, int offset, int len, int xx, int yy, int anc) {
        int x = xx;
        int y = yy + this.Height_without_fsy;
        String tx = new String(txt, offset, len);
        if (!(anc == 0 || anc == 20)) {
            int[] korr = calc_anc(tx, anc, 0, len);
            x += korr[0];
            y += korr[1];
        }
        for (int a = offset; a < offset + len; a++) {
            x += drawChar(g, txt[a], x, y);
        }
    }

    private final int[] calc_anc(String txt, int anc, int offset, int len) {
        int[] otp = new int[2];
        int fh = this.Height_without_fsy;
        int fw = substringWidth(txt, offset, len);
        if ((anc & 32) > 0) {
            otp[1] = otp[1] - fh;
        }
        if ((anc & 2) > 0) {
            otp[1] = otp[1] - (fh >> 1);
        }
        if ((anc & 8) > 0) {
            otp[0] = otp[0] - fw;
        }
        if ((anc & 1) > 0) {
            otp[0] = otp[0] - (fw >> 1);
        }
        return otp;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:6:0x0044 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: int} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short, code=int, for r3v0, types: [short, int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int drawChar(ua.netlizard.egypt.Graphics r18, char r19, int r20, int r21) {
        /*
            r17 = this;
            int r6 = r18.getClipX()
            int r7 = r18.getClipY()
            int r8 = r18.getClipWidth()
            int r9 = r18.getClipHeight()
            r0 = r17
            r1 = r19
            int r5 = r0.get_cf(r1)
            r0 = r17
            short[][] r13 = r0.cf_
            int r13 = r13.length
            if (r5 < r13) goto L_0x0020
            r5 = 0
        L_0x0020:
            r0 = r17
            short[][] r13 = r0.cf_
            r2 = r13[r5]
            r13 = 3
            short r4 = r2[r13]
            r13 = 4
            short r13 = r2[r13]
            int r20 = r20 + r13
            r13 = 5
            short r13 = r2[r13]
            int r13 = r13 + r4
            int r21 = r21 - r13
            r13 = 2
            short r10 = r2[r13]
            r13 = 3
            short r3 = r2[r13]
            r11 = r20
            r12 = r21
            if (r11 >= r6) goto L_0x0044
            int r13 = r6 - r11
            int r10 = r10 - r13
            r11 = r6
        L_0x0044:
            if (r12 >= r7) goto L_0x004a
            int r13 = r7 - r12
            int r3 = r3 - r13
            r12 = r7
        L_0x004a:
            int r13 = r11 + r10
            int r14 = r6 + r8
            if (r13 <= r14) goto L_0x0054
            int r13 = r6 + r8
            int r10 = r13 - r11
        L_0x0054:
            int r13 = r12 + r3
            int r14 = r7 + r9
            if (r13 <= r14) goto L_0x005e
            int r13 = r7 + r9
            int r3 = r13 - r12
        L_0x005e:
            if (r10 <= 0) goto L_0x007e
            if (r3 <= 0) goto L_0x007e
            r0 = r18
            r0.setClip(r11, r12, r10, r3)
            r0 = r17
            ua.netlizard.egypt.Image r13 = r0.font
            r14 = 0
            short r14 = r2[r14]
            int r14 = r20 - r14
            r15 = 1
            short r15 = r2[r15]
            int r15 = r21 - r15
            r16 = 20
            r0 = r18
            r1 = r16
            r0.drawImage(r13, r14, r15, r1)
        L_0x007e:
            r0 = r18
            r0.setClip(r6, r7, r8, r9)
            r0 = r17
            r1 = r19
            int r13 = r0.charWidth(r1)
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.MyFont.drawChar(ua.netlizard.egypt.Graphics, char, int, int):int");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public final int drawChar(Graphics g, char ch, int xx, int yy, int anc) {
        int x = xx;
        int y = yy + this.Height_without_fsy;
        String tx = new StringBuilder().append(ch).toString();
        if (!(anc == 0 || anc == 20)) {
            int[] korr = calc_anc(tx, anc, 0, 1);
            x += korr[0];
            y += korr[1];
        }
        int sc1 = g.getClipX();
        int sc2 = g.getClipY();
        int sc3 = g.getClipWidth();
        int sc4 = g.getClipHeight();
        int nim = get_cf(ch);
        if (nim >= this.cf_.length) {
            nim = 0;
        }
        short s = this.cf_[nim][3];
        int x2 = x + this.cf_[nim][4];
        int y2 = y - (this.cf_[nim][5] + s);
        short s2 = this.cf_[nim][2];
        short s3 = this.cf_[nim][3];
        int xc = x2;
        int yc = y2;
        int w = s2;
        if (xc < sc1) {
            w = s2 - (sc1 - xc);
            xc = sc1;
        }
        int h = s3;
        if (yc < sc2) {
            h = s3 - (sc2 - yc);
            yc = sc2;
        }
        int i = xc + w;
        int w2 = w;
        if (i > sc1 + sc3) {
            w2 = (sc1 + sc3) - xc;
        }
        int i2 = yc + h;
        int h2 = h;
        if (i2 > sc2 + sc4) {
            h2 = (sc2 + sc4) - yc;
        }
        if (w2 > 0 && h2 > 0 && x2 >= (-w2)) {
            g.setClip(xc, yc, w2, h2);
            g.drawImage(this.font, x2 - this.cf_[nim][0], y2 - this.cf_[nim][1], 20);
        }
        g.setClip(sc1, sc2, sc3, sc4);
        return charWidth(ch);
    }
}
