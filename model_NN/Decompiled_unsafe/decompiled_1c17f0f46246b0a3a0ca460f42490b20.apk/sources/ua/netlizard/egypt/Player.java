package ua.netlizard.egypt;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.util.Log;

public class Player extends Control implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnSeekCompleteListener, MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener {
    public static final int CLOSED = 0;
    public static final int PREFETCHED = 300;
    public static final int REALIZED = 200;
    public static final int STARTED = 400;
    public static final long TIME_UNKNOWN = -1;
    public static final int UNREALIZED = 100;
    int loopCount;
    MediaPlayer mp;
    String name;
    boolean needplay = false;
    boolean prepared = false;
    Integer sndid = null;
    int status = 100;

    public Player(String n) {
        this.name = n;
    }

    /* access modifiers changed from: package-private */
    public void setPlayer() {
        if (this.mp == null || !this.prepared) {
            this.mp = null;
            this.mp = new MediaPlayer();
        }
        if (!this.prepared) {
            try {
                if (AndroidUtils.getResourceAsStream(this.name) != null) {
                    AssetFileDescriptor afd = AndroidUtils.mContext.getAssets().openFd(String.valueOf(AndroidUtils.stream_path) + this.name);
                    this.mp.setOnSeekCompleteListener(this);
                    this.mp.setOnPreparedListener(this);
                    this.mp.setOnErrorListener(this);
                    this.mp.setOnCompletionListener(this);
                    this.mp.setOnBufferingUpdateListener(this);
                    this.mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    this.mp.setAudioStreamType(3);
                    this.mp.prepare();
                    this.mp.setVolume(1.0f, 1.0f);
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void stopPlayer() {
        if (this.prepared) {
            if (this.mp.isPlaying()) {
                this.mp.pause();
                this.mp.seekTo(0);
            }
            this.needplay = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void freePlayer() {
        try {
            this.mp.stop();
        } catch (Exception e) {
        }
        try {
            this.mp.release();
        } catch (Exception e2) {
        }
        this.mp = null;
        this.needplay = false;
    }

    /* access modifiers changed from: package-private */
    public void restart() {
        this.prepared = false;
        this.needplay = false;
        this.mp = null;
        setPlayer();
        this.status = 100;
    }

    public void close() {
        this.status = 0;
        this.prepared = false;
        this.needplay = false;
        try {
            this.mp.stop();
        } catch (Exception e) {
        }
        try {
            this.mp.release();
        } catch (Exception e2) {
        }
        this.mp = null;
    }

    public void deallocate() {
    }

    /* access modifiers changed from: package-private */
    public String getContentType() {
        return "music";
    }

    /* access modifiers changed from: package-private */
    public long getDuration() {
        return -1;
    }

    /* access modifiers changed from: package-private */
    public long getMediaTime() {
        return -1;
    }

    /* access modifiers changed from: package-private */
    public void setMediaTime(long time) {
    }

    /* access modifiers changed from: package-private */
    public int getState() {
        return this.status;
    }

    /* access modifiers changed from: package-private */
    public void prefetch() {
        this.status = PREFETCHED;
    }

    /* access modifiers changed from: package-private */
    public void realize() {
        this.status = 200;
    }

    /* access modifiers changed from: package-private */
    public void setLoopCount(int count) {
        this.loopCount = count;
    }

    /* access modifiers changed from: package-private */
    public void start() {
        boolean z = false;
        this.status = STARTED;
        if (this.mp != null && this.mp.isPlaying()) {
            stop();
            this.mp.seekTo(0);
        }
        setPlayer();
        if (!this.prepared) {
            z = true;
        }
        this.needplay = z;
        if (this.prepared) {
            float vol = ((float) SoundSystem.instance.streamVol) / 100.0f;
            this.mp.setVolume(vol, vol);
            try {
                this.mp.seekTo(0);
            } catch (Exception e) {
            }
            this.mp.start();
        }
    }

    /* access modifiers changed from: package-private */
    public void stop() {
        this.status = PREFETCHED;
        stopPlayer();
    }

    public void onCompletion(MediaPlayer mp2) {
        if (SoundSystem.instance.stopped) {
            return;
        }
        if (this.loopCount > 1) {
            int i = this.loopCount;
            this.loopCount = i - 1;
            if (i > 1) {
                try {
                    mp2.seekTo(0);
                    mp2.start();
                } catch (IllegalStateException e) {
                }
            }
        } else {
            this.status = PREFETCHED;
        }
    }

    public boolean onError(MediaPlayer mp2, int what, int extra) {
        Log.d("PL", "ERROR = " + what + "   extra = " + extra);
        return false;
    }

    public void onPrepared(MediaPlayer mp2) {
        this.prepared = true;
        if (this.needplay) {
            float vol = ((float) SoundSystem.instance.streamVol) / 100.0f;
            mp2.setVolume(vol, vol);
            start();
        }
    }

    public void onSeekComplete(MediaPlayer mp2) {
    }

    public boolean onInfo(MediaPlayer mp2, int what, int extra) {
        return false;
    }

    public void onBufferingUpdate(MediaPlayer mp2, int percent) {
    }
}
