package ua.netlizard.egypt;

public final class AI {
    static byte difficult = 0;
    static final int dist_pauk_attak = 90000;
    static boolean enable = true;
    static final int stp = 16;
    static final int stp_one = 65536;

    public static final boolean AI_goto(unit U, int x1, int y1, int max_dist, int min_dist) {
        int x = (U.pos_x - x1) >> 16;
        int y = (U.pos_y - y1) >> 16;
        int leght = (x * x) + (y * y);
        int Pa0 = 0;
        int wfm = stp_one;
        while (true) {
            wfm >>= 1;
            if (wfm <= 0) {
                break;
            } else if ((Pa0 + wfm) * (Pa0 + wfm) <= leght) {
                Pa0 += wfm;
            }
        }
        if (Pa0 == 0) {
            Pa0 = 1;
        }
        int v_x = ((-x) * stp_one) / Pa0;
        int v_y = ((-y) * stp_one) / Pa0;
        U.rr[0] = v_x;
        U.rr[1] = v_y;
        if (Pa0 > max_dist) {
            U.F[0] = v_x;
            U.F[1] = v_y;
            return true;
        } else if (Pa0 >= min_dist) {
            return false;
        } else {
            U.F[0] = -v_x;
            U.F[1] = -v_y;
            return true;
        }
    }

    private static final boolean point2(unit U1, unit U2) {
        int x = (U1.pos_x - U2.pos_x) >> 16;
        int y = (U1.pos_y - U2.pos_y) >> 16;
        int z = (U1.pos_z - U2.pos_z) >> 16;
        if ((((long) x) * ((long) U1.rr[0])) + (((long) y) * ((long) U1.rr[1])) <= 0 && (x * x) + (y * y) + (z * z) < 16000000) {
            return true;
        }
        return false;
    }

    static final boolean line_cross(unit U1, unit U2) {
        if (U1.room_now == U2.room_now) {
            return true;
        }
        int portal_hit = -1;
        point_ start = m3d.help_dts[0];
        point_ end = m3d.help_dts[1];
        start.x = U1.pos_x;
        start.y = U1.pos_y;
        start.z = U1.pos_z;
        end.x = U2.pos_x;
        end.y = U2.pos_y;
        end.z = U2.pos_z;
        int room = U1.room_now;
        while (1 != 0) {
            portal_hit = math.TEST_HIT_PORTAL(room, start, end, portal_hit);
            if (portal_hit == -1) {
                break;
            }
            if (m3d.Portals[portal_hit].room1 == room) {
                room = m3d.Portals[portal_hit].room2;
            } else {
                room = m3d.Portals[portal_hit].room1;
            }
            if (U2.room_now == room) {
                return true;
            }
        }
        return false;
    }

    static final void pauk(unit U) {
        unit enemy = m3d.Units[0];
        if (U.visible) {
            if (!U.take) {
                if (Game.my_level == 4) {
                    int x = (m3d.Objects[0].pos_x - U.pos_x) >> 16;
                    int y = (m3d.Objects[0].pos_y - U.pos_y) >> 16;
                    if ((x * x) + (y * y) < 1690000) {
                        AI_goto(U, m3d.Objects[0].pos_x, m3d.Objects[0].pos_y, 50, 20);
                        U.F[0] = -U.F[0];
                        U.F[1] = -U.F[1];
                        return;
                    }
                    int x2 = (m3d.Objects[13].pos_x - U.pos_x) >> 16;
                    int y2 = (m3d.Objects[13].pos_y - U.pos_y) >> 16;
                    if ((x2 * x2) + (y2 * y2) < 1690000) {
                        AI_goto(U, m3d.Objects[13].pos_x, m3d.Objects[13].pos_y, 50, 20);
                        U.F[0] = -U.F[0];
                        U.F[1] = -U.F[1];
                        return;
                    }
                }
                if (U.type_anim == 2 && U.cadr == 1) {
                    AI_goto(U, U.to_x, U.to_y, 10, 0);
                    U.Speed[0] = U.F[0] * 100;
                    U.Speed[1] = U.F[1] * 100;
                    U.F[0] = 0;
                    U.F[1] = 0;
                    U.Fly = true;
                    U.Speed[2] = 3932160;
                    U.MHZ = 25;
                } else if (U.type_anim == 3) {
                    int x3 = (enemy.pos_x - U.pos_x) >> 16;
                    int y3 = (enemy.pos_y - U.pos_y) >> 16;
                    if ((x3 * x3) + (y3 * y3) + (x3 * ((enemy.pos_z - U.pos_z) >> 16)) >= dist_pauk_attak) {
                        return;
                    }
                    if (U.Speed[0] != 0 || U.Speed[1] != 0) {
                        U.Speed[0] = 0;
                        U.Speed[1] = 0;
                        m3d.shot_ossiris(U, (byte) 0);
                    }
                } else {
                    U.MHZ--;
                    room R = m3d.Rooms[U.room_now];
                    if (U.MHZ <= 0) {
                        U.MHZ = math.Rnd(60) + 20;
                        if (Math.abs(enemy.pos_x - U.pos_x) + Math.abs(enemy.pos_y - U.pos_y) + Math.abs(enemy.pos_z - U.pos_z) < 65536000) {
                            jump_pauk(U, enemy);
                        } else if (!line_cross(U, enemy) || !point2(U, enemy)) {
                            U.to_x = math.Rnd(R.max_x - R.min_x) + R.min_x;
                            U.to_y = math.Rnd(R.max_y - R.min_y) + R.min_y;
                            U.MHZ = 50;
                        } else {
                            U.to_x = enemy.pos_x;
                            U.to_y = enemy.pos_y;
                            if (Math.abs(m3d.myRandom.nextInt() & 3) == 0) {
                                jump_pauk(U, enemy);
                            }
                            U.MHZ = 7;
                        }
                    }
                    if (U.to_x != 0 || U.to_y != 0) {
                        AI_goto(U, U.to_x, U.to_y, 240, 20);
                    }
                }
            } else if (line_cross(U, enemy)) {
                U.take = false;
            }
        }
    }

    static final void jump_pauk(unit U, unit enemy) {
        U.to_x = enemy.pos_x;
        U.to_y = enemy.pos_y;
        AI_goto(U, U.to_x, U.to_y, 10, 0);
        U.F[0] = 0;
        U.F[1] = 0;
        U.type_anim = 2;
        U.cadr = -1;
    }

    static final void mutant(unit U) {
        unit enemy = m3d.Units[0];
        if (U.visible) {
            if (!U.take) {
                if (U.reload > 0) {
                    U.reload = (byte) (U.reload - 1);
                }
                room R = m3d.Rooms[U.room_now];
                if (U.MHZ != 500 || U.shot <= 0) {
                    U.MHZ--;
                    if (!U.attake) {
                        AI_goto(U, U.to_x, U.to_y, 240, 20);
                    } else if (!line_cross(U, enemy) || !point2(U, enemy)) {
                        AI_goto(U, U.to_x, U.to_y, 240, 20);
                    } else {
                        U.to_x = enemy.pos_x;
                        U.to_y = enemy.pos_y;
                        AI_goto(U, enemy.pos_x, enemy.pos_y, 240, 20);
                        U.F[0] = 0;
                        U.F[1] = 0;
                        if (U.reload == 0) {
                            U.type_anim = (byte) (Math.abs(math.myRandom.nextInt() & 1) + 2);
                            U.cadr = -1;
                            m3d.shot_mutant(U, enemy, U.type_anim == 2);
                            U.reload = 10;
                            U.shot = (byte) (U.shot + 1);
                            if (U.shot >= 3 && (U.shot >= 7 || Math.abs(math.myRandom.nextInt() & 1) == 0)) {
                                U.shot = (byte) (U.shot + 15);
                                U.MHZ = 500;
                                return;
                            }
                        }
                        U.MHZ = 250;
                    }
                    if (U.MHZ > 0) {
                        return;
                    }
                    if (!line_cross(U, enemy) || !point2(U, enemy)) {
                        U.to_x = math.Rnd(R.max_x - R.min_x) + R.min_x;
                        U.to_y = math.Rnd(R.max_y - R.min_y) + R.min_y;
                        U.MHZ = Math.abs(math.myRandom.nextInt() & 63) + 20;
                        U.attake = false;
                        return;
                    }
                    U.attake = true;
                    U.MHZ = 450;
                    U.to_x = enemy.pos_x;
                    U.to_y = enemy.pos_y;
                    return;
                }
                U.shot = (byte) (U.shot - 1);
                if (U.shot <= 0) {
                    U.shot = 0;
                    U.MHZ = 250;
                }
                AI_goto(U, U.to_x, U.to_y, 240, 20);
            } else if (line_cross(U, enemy)) {
                U.take = false;
            }
        }
    }

    static final void sharik(unit U) {
        unit enemy = m3d.Units[0];
        if (!U.take) {
            if (U.reload > 0) {
                U.reload = (byte) (U.reload - 1);
            }
            point_[] v1 = m3d.help_dts;
            point_ bb = v1[0];
            point_ start = v1[1];
            point_ end = v1[2];
            U.Fly = false;
            if (U.shot > 0) {
                U.shot = (byte) (U.shot - 1);
                AI_goto(U, enemy.pos_x, enemy.pos_y, 240, 20);
                if (U.reload == 0) {
                    U.type_anim = (byte) (Math.abs(math.myRandom.nextInt() & 1) + 1);
                    m3d.shot_sharic(U, enemy, U.type_anim == 1);
                    U.reload = 20;
                }
                int x = U.F[0];
                int y = U.F[1];
                if (U.to_x < 0) {
                    U.F[0] = -y;
                    U.F[1] = x;
                } else {
                    U.F[0] = y;
                    U.F[1] = -x;
                }
                U.F[2] = stp_one * U.to_y;
                return;
            }
            U.MHZ--;
            if (U.MHZ <= 0) {
                if (!line_cross(U, enemy) || !point2(U, enemy)) {
                    room R = m3d.Rooms[U.room_now];
                    U.to_x = math.Rnd(R.max_x - R.min_x) + R.min_x;
                    U.to_y = math.Rnd(R.max_y - R.min_y) + R.min_y;
                    U.attake = false;
                    U.MHZ = Math.abs(math.myRandom.nextInt() & 15) + 10;
                } else {
                    U.attake = true;
                    U.MHZ = Math.abs(math.myRandom.nextInt() & 15) + 30;
                }
            }
            if (U.attake) {
                bb.x = U.pos_x >> 16;
                bb.y = U.pos_y >> 16;
                bb.z = U.pos_z >> 16;
                bb.z1 = 4000;
                start.x = enemy.pos_x >> 16;
                start.y = enemy.pos_y >> 16;
                start.z = enemy.pos_z >> 16;
                math.vec_for_me(m3d.vec_speed, m3d.rot_x, m3d.rot_z);
                start.rx = m3d.vec_speed[0];
                start.ry = m3d.vec_speed[1];
                start.rz = m3d.vec_speed[2];
                end.x = start.x + ((m3d.vec_speed[0] * 4000) >> 16);
                end.y = start.y + ((m3d.vec_speed[1] * 4000) >> 16);
                end.z = start.z + ((m3d.vec_speed[2] * 4000) >> 16);
                if (math.HIT_SFERA(bb, math.min_value_uni2[U.type_unit], start, end)) {
                    U.shot = 20;
                    if (Math.abs(math.myRandom.nextInt() & 1) == 0) {
                        U.to_x = 1;
                    } else {
                        U.to_x = -1;
                    }
                    if (Math.abs(math.myRandom.nextInt() & 1) == 0) {
                        U.to_y = Math.abs(math.myRandom.nextInt() & 1);
                    } else {
                        U.to_y = -Math.abs(math.myRandom.nextInt() & 1);
                    }
                } else if (U.reload == 0) {
                    AI_goto(U, enemy.pos_x, enemy.pos_y, 240, 20);
                    U.F[0] = 0;
                    U.F[1] = 0;
                    U.type_anim = (byte) (Math.abs(math.myRandom.nextInt() & 1) + 1);
                    m3d.shot_sharic(U, enemy, U.type_anim == 1);
                    U.reload = 10;
                }
            } else if (U.to_x != 0 || U.to_y != 0) {
                AI_goto(U, U.to_x, U.to_y, 240, 20);
            }
        } else if (line_cross(U, enemy)) {
            U.take = false;
        }
    }

    static final void akula(unit U) {
        U.Fly = false;
        unit enemy = m3d.Units[0];
        if (!U.take) {
            U.MHZ--;
            room R = m3d.Rooms[U.room_now];
            if (U.MHZ <= 0) {
                if (!line_cross(U, enemy) || !point2(U, enemy)) {
                    U.MHZ = Math.abs(math.myRandom.nextInt() & 31) + 30;
                    U.to_x = math.Rnd(R.max_x - R.min_x) + R.min_x;
                    U.to_y = math.Rnd(R.max_y - R.min_y) + R.min_y;
                } else {
                    U.to_x = enemy.pos_x;
                    U.to_y = enemy.pos_y;
                    AI_goto(U, enemy.pos_x, enemy.pos_y, 240, 20);
                    U.to_x += U.F[0] * 600;
                    U.to_y += U.F[1] * 600;
                    U.F[0] = 0;
                    U.F[1] = 0;
                    U.MHZ = 350;
                }
            }
            if (U.to_x != 0 || U.to_y != 0) {
                if (enemy.pos_z > U.pos_z) {
                    U.F[2] = 32768;
                } else {
                    U.F[2] = -32768;
                }
                if (!AI_goto(U, U.to_x, U.to_y, 240, 20)) {
                    U.MHZ = 0;
                }
            }
        }
    }

    static final void ossiris_5(unit U) {
        if (m3d.open_rooms[53] == -1 && !U.take && U.visible) {
            unit enemy = m3d.Units[0];
            if (enemy.room_now == 30) {
                AI_goto(U, enemy.pos_x, enemy.pos_y, 250, 20);
            } else if (Math.abs(226099200 - U.pos_x) + Math.abs(-88473600 - U.pos_y) > 3276800) {
                AI_goto(U, 226099200, -88473600, 40, 20);
            } else {
                AI_goto(U, 173670400, -81920000, 250, 20);
                U.F[0] = 0;
                U.F[1] = 0;
            }
            if (U.type_anim == 2 && U.cadr == 2) {
                m3d.shot_ossiris(U, (byte) 2);
            }
            if (U.type_anim == 2) {
                if (U.cadr == 4) {
                    U.type_anim = 0;
                }
            } else if (Math.abs(enemy.pos_x - U.pos_x) + Math.abs(enemy.pos_y - U.pos_y) + Math.abs(enemy.pos_z - U.pos_z) < 26214400) {
                if (U.type_anim != 2) {
                    U.cadr = -1;
                }
                U.type_anim = 2;
                AI_goto(U, enemy.pos_x, enemy.pos_y, 250, 20);
                U.F[0] = 0;
                U.F[1] = 0;
            }
        }
    }

    static final void ossiris(unit U) {
        if (U.visible) {
            unit enemy = m3d.Units[0];
            if (U.take) {
                return;
            }
            if (U.life < (m3d.life_unit[5] >> 2)) {
                if (U.type_anim == 5) {
                    U.life = (short) (U.life + 2);
                    if (U.life >= (m3d.life_unit[U.type_unit] >> 2)) {
                        U.life = (short) (U.life + (m3d.life_unit[U.type_unit] >> 1));
                    }
                }
            } else if (U.type_anim == 5) {
                U.type_anim = 7;
                U.cadr = -1;
            } else if (U.type_anim != 7) {
                if (U.type_anim == 2 && U.cadr == 2) {
                    m3d.shot_ossiris(U, (byte) 2);
                }
                if (Math.abs(enemy.pos_x - U.pos_x) + Math.abs(enemy.pos_y - U.pos_y) + Math.abs(enemy.pos_z - U.pos_z) < 26214400) {
                    if (U.type_anim != 2) {
                        U.cadr = -1;
                    }
                    U.type_anim = 2;
                    AI_goto(U, enemy.pos_x, enemy.pos_y, 250, 20);
                    U.F[0] = 0;
                    U.F[1] = 0;
                } else if (U.type_anim != 2) {
                    U.MHZ--;
                    room R = m3d.Rooms[U.room_now];
                    if (U.MHZ <= 0) {
                        if (U.attake) {
                            U.to_x = math.Rnd(R.max_x - R.min_x) + R.min_x;
                            U.to_y = math.Rnd(R.max_y - R.min_y) + R.min_y;
                            U.MHZ = math.Rnd(60) + 20;
                            U.attake = false;
                        } else {
                            U.attake = true;
                            U.MHZ = math.Rnd(30) + 150;
                        }
                    }
                    if (U.attake) {
                        U.to_x = enemy.pos_x;
                        U.to_y = enemy.pos_y;
                        U.shot = (byte) (U.shot - 1);
                        if (U.shot <= 0) {
                            U.shot = 50;
                            AI_goto(U, U.to_x, U.to_y, 240, 20);
                            U.pos_x = enemy.pos_x + (U.F[0] * 800);
                            U.pos_y = enemy.pos_y + (U.F[1] * 800);
                            U.F[0] = 0;
                            U.F[1] = 0;
                            U.shot = 40;
                        }
                    }
                    if (U.to_x != 0 || U.to_y != 0) {
                        AI_goto(U, U.to_x, U.to_y, 240, 20);
                    }
                } else if (U.cadr == 4) {
                    U.type_anim = 0;
                }
            }
        }
    }

    static final void scorpion(unit U) {
        unit enemy = m3d.Units[0];
        if (U.visible) {
            if (!U.take) {
                if (U.reload > 0) {
                    U.reload = (byte) (U.reload - 1);
                }
                U.MHZ--;
                room R = m3d.Rooms[U.room_now];
                if (U.type_anim != 2) {
                    if (U.MHZ <= 0) {
                        if (!line_cross(U, enemy) || !point2(U, enemy)) {
                            U.attake = false;
                            U.MHZ = Math.abs(math.myRandom.nextInt() & 31) + 2;
                            U.to_x = math.Rnd(R.max_x - R.min_x) + R.min_x;
                            U.to_y = math.Rnd(R.max_y - R.min_y) + R.min_y;
                        } else {
                            U.attake = true;
                            U.MHZ = Player.PREFETCHED;
                        }
                    }
                    if (U.attake && line_cross(U, enemy) && point2(U, enemy)) {
                        U.MHZ = Player.PREFETCHED;
                        U.to_x = enemy.pos_x;
                        U.to_y = enemy.pos_y;
                        if (Math.abs(enemy.pos_x - U.pos_x) + Math.abs(enemy.pos_y - U.pos_y) + Math.abs(enemy.pos_z - U.pos_z) < 26214400) {
                            if (U.reload == 0) {
                                AI_goto(U, enemy.pos_x, enemy.pos_y, 240, 20);
                                U.F[0] = 0;
                                U.F[1] = 0;
                                U.cadr = -1;
                                U.type_anim = 3;
                                m3d.shot_ossiris(U, (byte) 1);
                                U.reload = 15;
                                return;
                            }
                            return;
                        } else if (Math.abs(enemy.pos_x - U.pos_x) + Math.abs(enemy.pos_y - U.pos_y) + Math.abs(enemy.pos_z - U.pos_z) < 78643200) {
                            if (U.reload == 0) {
                                AI_goto(U, enemy.pos_x, enemy.pos_y, 240, 20);
                                U.F[0] = 0;
                                U.F[1] = 0;
                                U.cadr = -1;
                                U.type_anim = 2;
                                U.reload = 15;
                                return;
                            }
                            return;
                        }
                    }
                    if (U.to_x != 0 || U.to_y != 0) {
                        AI_goto(U, U.to_x, U.to_y, 240, 20);
                    }
                } else if (U.cadr == 2) {
                    m3d.shot_scorpion(U, enemy);
                }
            } else if (line_cross(U, enemy)) {
                U.take = false;
            }
        }
    }

    static final void plevok_tipom() {
        unit[] un = m3d.Units;
        unit cherv = null;
        for (int i = 1; i < un.length; i++) {
            if (un[i].type_unit == 7) {
                cherv = un[i];
            }
        }
        for (int i2 = 1; i2 < un.length; i2++) {
            if (un[i2].death) {
                unit uu = un[i2];
                uu.death = false;
                uu.visible = true;
                uu.life = 100;
                uu.type_anim = 0;
                uu.cadr = 0;
                uu.anim = 0;
                uu.animation = true;
                uu.take = false;
                uu.pos_x = cherv.pos_x;
                uu.pos_y = cherv.pos_y;
                uu.pos_z = cherv.pos_z + 6553600;
                int a = math.Rnd(360);
                uu.rr[0] = math.sin[a];
                uu.rr[1] = math.cos[a];
                math.vec_for_me(m3d.vec_speed, 270 - Math.abs(math.myRandom.nextInt() % 30), Math.abs(math.myRandom.nextInt() % 360));
                for (int j = 0; j < 3; j++) {
                    m3d.creat_meet(cherv.pos_x, cherv.pos_y, cherv.pos_z + 1966080, cherv.room_now, false);
                }
                m3d.Create_sprite(cherv.pos_x, cherv.pos_y, cherv.pos_z, (byte) 2, cherv.room_now);
                m3d.Create_sprite(cherv.pos_x, cherv.pos_y, cherv.pos_z + 1966080, (byte) 2, cherv.room_now);
                uu.Speed[0] = m3d.vec_speed[0] * 200;
                uu.Speed[1] = m3d.vec_speed[1] * 200;
                uu.Speed[2] = m3d.vec_speed[2] * 200;
                uu.Fly = true;
                return;
            }
        }
    }

    static final void boss(unit U) {
        unit enemy = m3d.Units[0];
        if (enemy.room_now != 0) {
            U.MHZ = 30;
            U.type_anim = 6;
            return;
        }
        if (U.type_anim == 2 && U.cadr == 5) {
            plevok_tipom();
        }
        if (U.type_anim == 0) {
            AI_goto(U, enemy.pos_x, enemy.pos_y, 240, 20);
            U.F[0] = 0;
            U.F[1] = 0;
        }
        U.MHZ--;
        if (U.MHZ > 0) {
            return;
        }
        if (U.type_anim == 6) {
            U.type_anim = 5;
            U.MHZ = 4;
            U.cadr = -1;
        } else if (U.type_anim == 5) {
            U.MHZ = Math.abs(math.myRandom.nextInt() & 15) + 40;
            U.type_anim = 0;
            U.cadr = -1;
            U.shot = (byte) (Math.abs(math.myRandom.nextInt() & 7) + 5);
        } else if (U.type_anim == 0) {
            U.type_anim = 3;
            U.MHZ = 12;
            U.cadr = -1;
        } else if (U.type_anim == 3) {
            U.shot = (byte) (U.shot - 1);
            if (U.shot <= 0) {
                U.type_anim = 4;
                U.MHZ = 4;
                U.cadr = -1;
                return;
            }
            AI_goto(U, enemy.pos_x, enemy.pos_y, 240, 20);
            U.F[0] = 0;
            U.F[1] = 0;
            U.MHZ = 12;
        } else if (U.type_anim == 4) {
            U.shot = 0;
            U.type_anim = 1;
            U.cadr = -1;
            for (int i = 1; i < m3d.units; i++) {
                if (m3d.Units[i].death) {
                    U.shot = (byte) (U.shot + 1);
                }
            }
            U.MHZ = 12;
        } else if (U.type_anim == 1) {
            if (U.shot <= 0) {
                U.shot = 0;
                U.type_anim = 5;
                U.MHZ = 4;
                U.cadr = -1;
                return;
            }
            U.MHZ = 14;
            U.type_anim = 2;
            U.cadr = -1;
        } else if (U.type_anim == 2) {
            U.MHZ = 5;
            U.type_anim = 1;
            U.cadr = -1;
            U.shot = (byte) (U.shot - 1);
            if (U.shot <= 0) {
                U.MHZ = 70;
            }
        }
    }

    public static final void AI_(unit U) {
        if (enable && !U.death) {
            switch (U.type_unit) {
                case 1:
                    pauk(U);
                    return;
                case 2:
                    mutant(U);
                    return;
                case 3:
                    sharik(U);
                    return;
                case 4:
                    akula(U);
                    return;
                case 5:
                    if (Game.my_level == 5) {
                        ossiris_5(U);
                        return;
                    } else {
                        ossiris(U);
                        return;
                    }
                case 6:
                    scorpion(U);
                    return;
                case 7:
                    boss(U);
                    return;
                default:
                    return;
            }
        }
    }
}
