package ua.netlizard.egypt;

public class FullCanvas extends J2MECanvas {
    static final int KEY_DOWN_ARROW = -2;
    static final int KEY_LEFT_ARROW = -3;
    static final int KEY_RIGHT_ARROW = -4;
    static final int KEY_SOFTKEY1 = -6;
    static final int KEY_SOFTKEY2 = -7;
    static final int KEY_SOFTKEY3 = -5;
    static final int KEY_UP_ARROW = -1;
    static final boolean big_heap_F = false;
    public static final int canvasRealHeight = 200;
    public static final int canvasRealWidth = 1000;
    static int[] fillAColF = null;
    private static boolean kcFstF = true;
    private static boolean kcNokF = big_heap_F;
    private static boolean kcSimF = big_heap_F;
    static final String portName = "Android";
    static final boolean small_heap_F = false;

    public int getWidthScr() {
        return super.getWidth();
    }

    public int getHeightScr() {
        return super.getHeight();
    }

    public FullCanvas() {
        setFullScreenMode(true);
    }

    public void paint(Graphics g) {
    }

    static final int keyCalcF(int keyCode) {
        return keyCode;
    }

    static final Image obr_unit_F(Image bi, int nu) {
        try {
            Image tmp = Uni.createImage("/z" + nu + ".png");
            if (tmp != null) {
                return tmp;
            }
        } catch (Exception e) {
        }
        return Image.createImage(bi, 0, 0, bi.getWidth(), bi.getHeight(), 2);
    }

    static final void fillRectAlfaF(Graphics g, int x, int y, int w, int h, int col, int alfa) {
        int tr_x = g.getTranslateX();
        int tr_y = g.getTranslateY();
        g.translate(-tr_x, -tr_y);
        int x2 = x + tr_x;
        int y2 = y + tr_y;
        int w1 = 8 * canvasRealWidth;
        if (fillAColF != null && w1 < fillAColF.length) {
            w1 = fillAColF.length;
        }
        boolean fillR = true;
        int aCol = col + (alfa << 24);
        try {
            if (Display.getDisplay(NET_Lizard.instance).numAlphaLevels() > 2) {
                if (!(fillAColF != null && fillAColF.length == w1 && fillAColF[0] == aCol)) {
                    if (fillAColF == null || fillAColF.length != w1) {
                        if (fillAColF != null) {
                            fillAColF = null;
                        }
                        fillAColF = new int[w1];
                    }
                    int[] fillAColF_pc = fillAColF;
                    for (int m = 0; m < fillAColF_pc.length; m++) {
                        fillAColF_pc[m] = aCol;
                    }
                }
                int sc1 = g.getClipX();
                int sc2 = g.getClipY();
                int sc3 = g.getClipWidth();
                int sc4 = g.getClipHeight();
                int xc = x2;
                int yc = y2;
                if (xc < sc1) {
                    w -= sc1 - xc;
                    xc = sc1;
                }
                if (yc < sc2) {
                    h -= sc2 - yc;
                    yc = sc2;
                }
                if (xc + w > sc1 + sc3) {
                    w = (sc1 + sc3) - xc;
                }
                if (yc + h > sc2 + sc4) {
                    h = (sc2 + sc4) - yc;
                }
                if (w > 0 && h > 0) {
                    try {
                        g.setClip(xc, yc, w, h);
                        for (int y1 = yc; y1 < yc + h; y1 += 8) {
                            g.drawRGB(fillAColF, 0, canvasRealWidth, 0, y1, canvasRealWidth, 8, true);
                        }
                    } catch (Exception e) {
                    }
                }
                fillR = big_heap_F;
                g.setClip(sc1, sc2, sc3, sc4);
            }
        } catch (Exception e2) {
        }
        if (fillR) {
            try {
                int t_col = g.getColor();
                g.setColor(col);
                g.fillRect(x2, y2, w, h);
                g.setColor(t_col);
            } catch (Exception e3) {
            }
        }
        g.translate(tr_x, tr_y);
    }
}
