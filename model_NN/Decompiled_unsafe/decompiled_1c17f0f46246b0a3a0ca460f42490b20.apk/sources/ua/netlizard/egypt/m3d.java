package ua.netlizard.egypt;

import com.media.fx.Constants;
import java.lang.reflect.Array;
import java.util.Random;

public class m3d {
    static final int DETAIL_SHOT = 7;
    static final int DETAIL_SHOT_2 = 5;
    static int[][] FPS_palitra = null;
    static byte[][] FPS_weapon = null;
    static final int HEAD = 2293760;
    static short HP = 0;
    static final int H_door = 300;
    static final int H_door_stp = 19660800;
    static final int Height = Game.Height;
    static final int Height_2_3D = (((Height - 60) >> 1) << 16);
    static final int Height_2_real_3D = ((Height - 60) >> 1);
    static final int Height_real_3D = (Height - 60);
    static int[] NORMALS = null;
    private static final int[][] N_sprite;
    static main_object[] OBJ = null;
    static object[] Objects = null;
    static portal[] Portals = null;
    static room[] Rooms = null;
    static main_unit[] UNI = null;
    static unit[] Units = null;
    static final byte WEAPONS = 9;
    static final int Width = Game.Width;
    static final int Width_2_3D = (((Width + 0) >> 1) << 16);
    static final int Width_2_real_3D = ((Width + 0) >> 1);
    static final int Width_real_3D = (Width + 0);
    static int alfa_ = 0;
    static byte all_texture = 0;
    static byte[] ammo = new byte[7];
    static byte[] ammo_in_mag = new byte[7];
    static final byte[] anim_cadri;
    static final short[] anim_knife;
    static final short[] anim_mech;
    static final byte[] anim_poin;
    static byte[] auto_aim = null;
    static boolean boss = false;
    static byte change = 0;
    static int change_weapon = 0;
    static final byte cof_H = 30;
    static final byte cof_W = 0;
    static int[] coof_clip = null;
    static short[][] cu = null;
    static final byte[] delay_shot = {6, 5, 5, 2, 3, 4, 5};
    static int[] display = null;
    static final int dist = -2097152;
    static final int dist_door = 202500;
    static int[] distance_H = null;
    static final int distance_portal = 9830400;
    static short dm_level = 0;
    static int draw_len = 0;
    static int[] draw_loop = null;
    static boolean fire2 = false;
    static boolean for_level_4 = false;
    static point_[] help_dts = null;
    static int[] help_sort = null;
    static int[] help_sort_poly = null;
    static point_[] help_v = null;
    static int ind = 0;
    static int[] index_sort = null;
    static int[] index_sort_poly = null;
    static byte kills = cof_W;
    static final short[] life_unit = {100, 52, 150, 100, 80, 2000, 100, 2500};
    static int light1 = 0;
    static short[] lights = null;
    static boolean look_on = false;
    static boolean[] mas_object = null;
    static boolean[] mas_texture = null;
    static boolean[] mas_unit = null;
    static byte max_1 = max_shots;
    static byte max_2 = 30;
    static final int max_alfa = 65536;
    static final byte[] max_ammo;
    static final byte[] max_ammo_in_mag;
    static final byte max_change = 8;
    static final byte max_change_2 = 4;
    static final byte max_draw_portal_in = 40;
    static final int max_help_point = 10;
    static int[] max_point = null;
    static final int max_polys = 138;
    static final byte max_razbros = 20;
    static final byte max_shots = 50;
    static final byte max_smesh_x = 30;
    static final byte max_smesh_y = 40;
    static final byte max_sprite = 15;
    static final int max_xyz = (Width_real_3D * 140);
    static byte mhz = cof_W;
    static final int move_STP = 45875200;
    static boolean move_in_menu = false;
    static String[][] msg_str = null;
    public static Random myRandom = new Random();
    static short next_time = 0;
    static byte now_weap = cof_W;
    static byte nuw_hit_unit = 0;
    static int obj_0 = 0;
    static object object_help = null;
    static final int object_max = 50;
    static int object_now = 0;
    static byte object_sky = 0;
    static short objects = 0;
    static int obzor = ok_obzor;
    static int obzor_2 = (obzor << 16);
    static final int ok_obzor = Width_real_3D;
    static short[] open_doors_cof = null;
    static byte[] open_rooms = null;
    static int[][] palitra = null;
    static int[] palitra_help = null;
    static int[] palitra_object = null;
    static final int[][] planes;
    static final int poly_max = 7;
    static final short[][] pos;
    static final int[] ppp = {Height_2_real_3D, Height_2_real_3D + 1, Width_2_real_3D, Width_2_real_3D + 1};
    static byte[] quit_rooms = null;
    static byte razbros = cof_W;
    static final byte[] razbros_FPS;
    static byte reload_FPS = 0;
    static final byte reload_time = 20;
    static final int[] remainder;
    static byte resp = cof_W;
    static final short[] resp_;
    static final short[] resp_pauk = {-2898, 1500, -900, 4037, 900, 4039, 5285, -1500, 2700, -4014};
    static int room_now = 0;
    static byte room_with_sky_box = -1;
    static short rooms = 0;
    static short rot_FPS = 0;
    static int rot_ax = 0;
    static int rot_az = 0;
    static int rot_s_x = 0;
    static int rot_s_z = 0;
    static int rot_x = 0;
    static int rot_z = 0;
    static int[] s = null;
    static boolean shop = true;
    static int[][] shot = null;
    static int shot_0 = 0;
    static final int[] shot_color;
    static byte shot_delay = cof_W;
    static final byte[] shot_demage_me = {20, 10, 30, 20, 30, 60, Byte.MAX_VALUE};
    static final byte[] shot_demage_unit;
    static final byte[] shot_speed = {max_change, 10, 10, 11, 11, 11, max_change};
    static final byte[] shot_time_life = {2, 5, 7, 6, max_change, max_change, 2};
    static short shots = 0;
    static boolean sky_load = false;
    static boolean sky_olveis = false;
    static byte[] slot = null;
    static byte slots = cof_W;
    static final short[] smesh_weapon = {128, 256, 160, 256, 128, 256, 128, 256, 128, 256, 128, 256, 128, 256};
    static byte smesh_x = cof_W;
    static byte smesh_y = cof_W;
    static final int speed_door = 1310720;
    static final byte speed_smesh_x = 5;
    static final byte speed_smesh_y = 5;
    static int spr_0 = 0;
    static int[][] sprite = null;
    static short sprites = 0;
    static final int stp = 16;
    static final byte stp_3d = 4;
    static final byte stp_3d_one = 16;
    static final int stp_draw = 18;
    static final int stp_draw_one = 262144;
    static final int stp_one = 65536;
    static boolean text = false;
    static short[] text_coords = null;
    static byte[][] text_read = null;
    static byte[][] texture = null;
    static final int texture_max = 51;
    static final int texture_max_uni = 91;
    static byte[][] texture_object = null;
    static boolean turn_left1 = false;
    static boolean turn_left2 = false;
    static final int unit_max = 8;
    static short units = 0;
    static int[] vec_speed = null;
    static boolean walk = false;
    static final byte walk_max = 16;
    static byte walk_now = cof_W;
    static byte walk_shag = 4;
    static int weapon_now = 0;
    static short[][] weapon_size;
    static short white = 255;
    static short x_fire;
    static int x_light;
    static short y_fire;
    static int y_light;
    static int z_light;

    static {
        int[] iArr = new int[90];
        iArr[20] = 1;
        iArr[22] = 1;
        iArr[24] = 1;
        iArr[27] = 1;
        iArr[28] = 2;
        iArr[30] = 1;
        iArr[31] = 2;
        iArr[33] = 1;
        iArr[34] = 2;
        iArr[36] = 1;
        iArr[37] = 2;
        iArr[38] = 3;
        iArr[40] = 1;
        iArr[41] = 2;
        iArr[42] = 3;
        iArr[44] = 1;
        iArr[45] = 1;
        iArr[46] = 2;
        iArr[47] = 3;
        iArr[48] = 4;
        iArr[50] = 1;
        iArr[51] = 2;
        iArr[52] = 3;
        iArr[53] = 4;
        iArr[54] = 1;
        iArr[55] = 2;
        iArr[56] = 3;
        iArr[57] = 4;
        iArr[58] = 5;
        iArr[60] = 1;
        iArr[61] = 2;
        iArr[62] = 3;
        iArr[63] = 1;
        iArr[64] = 2;
        iArr[65] = 3;
        iArr[66] = 4;
        iArr[67] = 5;
        iArr[68] = 6;
        iArr[70] = 1;
        iArr[71] = 2;
        iArr[72] = 1;
        iArr[73] = 2;
        iArr[74] = 3;
        iArr[75] = 4;
        iArr[76] = 5;
        iArr[77] = 6;
        iArr[78] = 7;
        iArr[80] = 1;
        iArr[81] = 1;
        iArr[82] = 2;
        iArr[83] = 3;
        iArr[84] = 4;
        iArr[85] = 5;
        iArr[86] = 6;
        iArr[87] = 7;
        iArr[88] = 8;
        remainder = iArr;
        int[] iArr2 = new int[3];
        iArr2[2] = obzor;
        int[] iArr3 = new int[3];
        iArr3[1] = -obzor;
        iArr3[2] = Height_2_real_3D;
        int[] iArr4 = new int[3];
        iArr4[1] = obzor;
        iArr4[2] = Height_2_real_3D + 1;
        int[] iArr5 = new int[3];
        iArr5[0] = obzor;
        iArr5[2] = Width_2_real_3D;
        int[] iArr6 = new int[3];
        iArr6[0] = -obzor;
        iArr6[2] = Width_2_real_3D + 1;
        planes = new int[][]{iArr2, iArr3, iArr4, iArr5, iArr6};
        int[] iArr7 = new int[3];
        iArr7[1] = 1;
        iArr7[2] = 2;
        int[] iArr8 = new int[2];
        iArr8[1] = 1;
        int[] iArr9 = new int[2];
        iArr9[1] = 2;
        N_sprite = new int[][]{new int[]{3, 4, 5, 6}, new int[]{7, 8}, iArr7, new int[]{1, 2}, iArr8, iArr9, new int[]{12, 11, 10, 9}};
        byte[] bArr = new byte[7];
        bArr[0] = 1;
        slot = bArr;
        byte[] bArr2 = new byte[7];
        bArr2[1] = WEAPONS;
        bArr2[2] = 5;
        bArr2[3] = 30;
        bArr2[4] = 30;
        bArr2[5] = 10;
        max_ammo_in_mag = bArr2;
        byte[] bArr3 = new byte[7];
        bArr3[1] = 72;
        bArr3[2] = max_shots;
        bArr3[3] = 120;
        bArr3[4] = 120;
        bArr3[5] = max_shots;
        max_ammo = bArr3;
        byte[] bArr4 = new byte[7];
        bArr4[1] = 2;
        bArr4[2] = 4;
        bArr4[3] = 5;
        bArr4[4] = 6;
        bArr4[5] = 7;
        razbros_FPS = bArr4;
        short[] sArr = new short[14];
        sArr[0] = 128;
        sArr[1] = 128;
        sArr[2] = 150;
        sArr[3] = 180;
        sArr[4] = 200;
        sArr[5] = 200;
        sArr[6] = 220;
        sArr[7] = 220;
        sArr[8] = 256;
        sArr[9] = 240;
        sArr[11] = 130;
        sArr[12] = 70;
        sArr[13] = 130;
        anim_knife = sArr;
        short[] sArr2 = new short[10];
        sArr2[0] = 128;
        sArr2[1] = 256;
        sArr2[2] = 64;
        sArr2[3] = 200;
        sArr2[5] = 256;
        sArr2[6] = 230;
        sArr2[7] = 256;
        sArr2[8] = 256;
        sArr2[9] = 256;
        anim_mech = sArr2;
        byte[] bArr5 = new byte[5];
        bArr5[1] = 2;
        bArr5[2] = 2;
        bArr5[3] = 2;
        bArr5[4] = 1;
        anim_cadri = bArr5;
        int[] iArr10 = new int[7];
        iArr10[1] = 16776960;
        iArr10[2] = 16776960;
        iArr10[3] = 16776960;
        iArr10[4] = 16776960;
        iArr10[5] = 255;
        shot_color = iArr10;
        byte[] bArr6 = new byte[7];
        bArr6[1] = 22;
        bArr6[2] = 20;
        bArr6[3] = 30;
        bArr6[4] = 10;
        bArr6[5] = 5;
        bArr6[6] = 30;
        shot_demage_unit = bArr6;
        byte[] bArr7 = new byte[8];
        bArr7[2] = 30;
        anim_poin = bArr7;
        byte[][] bArr8 = new byte[stp_draw][];
        bArr8[0] = new byte[]{1, 3, 1, 4, 1, 5, 1, 7};
        byte[] bArr9 = new byte[2];
        bArr9[0] = 1;
        bArr8[1] = bArr9;
        bArr8[2] = new byte[]{1, 1};
        bArr8[3] = new byte[]{1, 1, 1, 2, 1, 3, 1, 4};
        bArr8[4] = new byte[]{1, 1};
        byte[] bArr10 = new byte[4];
        bArr10[0] = 1;
        bArr10[2] = 1;
        bArr10[3] = 1;
        bArr8[5] = bArr10;
        bArr8[6] = new byte[1];
        bArr8[7] = new byte[1];
        bArr8[8] = new byte[1];
        bArr8[9] = new byte[]{1, 1};
        bArr8[10] = new byte[1];
        bArr8[11] = new byte[1];
        bArr8[12] = new byte[1];
        bArr8[13] = new byte[1];
        bArr8[14] = new byte[1];
        bArr8[15] = new byte[1];
        bArr8[16] = new byte[1];
        bArr8[17] = new byte[1];
        text_read = bArr8;
        short[][] sArr3 = new short[stp_draw][];
        sArr3[0] = new short[]{-8819, 1140, -9054, 204, -5442, -5340, -1943, 59};
        sArr3[1] = new short[]{-740, -1471};
        sArr3[2] = new short[]{3226, 6936};
        sArr3[3] = new short[]{1643, 3441, 3146, 3441, 7074, -469, 1800, 2400};
        sArr3[4] = new short[]{2528, -6436};
        sArr3[5] = new short[]{-1349, -6730, 3760, -492};
        sArr3[6] = new short[1];
        sArr3[7] = new short[1];
        sArr3[8] = new short[1];
        sArr3[9] = new short[]{-5899, -874};
        sArr3[10] = new short[1];
        sArr3[11] = new short[1];
        sArr3[12] = new short[1];
        sArr3[13] = new short[1];
        sArr3[14] = new short[1];
        sArr3[15] = new short[1];
        sArr3[16] = new short[1];
        sArr3[17] = new short[1];
        pos = sArr3;
        short[] sArr4 = new short[6];
        sArr4[0] = 2100;
        sArr4[1] = -1800;
        sArr4[2] = 3600;
        sArr4[5] = 1500;
        resp_ = sArr4;
    }

    static void weap_r() {
        for (int i = 1; i < 7; i++) {
            slot[i] = cof_W;
            ammo_in_mag[i] = cof_W;
            ammo[i] = cof_W;
        }
        weapon_now = 0;
    }

    static final void m_m() {
        String[] mm = msg_str[Menu.SMS];
        int len_str = mm.length;
        int H = Menu.str_text;
        int H_text = len_str * H;
        int start_y = (Game.Height - H_text) >> 1;
        int W_text = (Game.Width >> 1) + (Game.Width >> 2);
        int start_x = (Game.Width - W_text) >> 1;
        if (H_text > (Game.Height >> 2)) {
            int len_str2 = (Game.Height >> 2) / H;
            H_text = len_str2 * H;
            start_y = (Game.Height - H_text) >> 1;
            if (Menu.scrol + len_str2 >= mm.length) {
                Menu.scrol = mm.length - len_str2;
            }
        }
        my_fill(start_x - 2, (start_y - 2) - 30, W_text + 4 + 1, H_text + 4 + 2 + 1, 0, 180);
    }

    static final void my_fill(int x, int y, int w, int h, int color1, int alfa) {
        int R = ((16711680 & color1) >> 16) * alfa;
        int G = ((65280 & color1) >> 8) * alfa;
        int B = (color1 & 255) * alfa;
        short d_alfa = (short) (255 - alfa);
        int[] display1 = display;
        int y2 = (Width * y) + x;
        while (true) {
            h--;
            if (h >= 0) {
                int len = w;
                while (true) {
                    len--;
                    if (len < 0) {
                        break;
                    }
                    int color2 = display1[y2 + len];
                    if (d_alfa == 0) {
                        display1[y2 + len] = color1;
                    } else {
                        display1[y2 + len] = ((((((16711680 & color2) >> 16) * d_alfa) + R) >> 8) << 16) + ((((((65280 & color2) >> 8) * d_alfa) + G) >> 8) << 8) + ((((color2 & 255) * d_alfa) + B) >> 8);
                    }
                }
                y2 += Width;
            } else {
                return;
            }
        }
    }

    static void speek() {
        if (!scr.enable_script && Game.my_level - 1 >= 0) {
            int pos1 = Game.my_level - 1;
            int len = text_read[pos1].length >> 1;
            byte[] h = text_read[pos1];
            short[] xy = pos[pos1];
            for (int i = 0; i < len; i++) {
                if (h[i << 1] != 0 && Math.abs(xy[i << 1] - (Units[0].pos_x >> 16)) + Math.abs(xy[(i << 1) + 1] - (Units[0].pos_y >> 16)) < 350) {
                    h[i << 1] = cof_W;
                    Menu.M(h[(i << 1) + 1]);
                    text = true;
                    Game.touch_began = false;
                }
            }
        }
    }

    static final void end_all() {
        Menu.enable = false;
        Game.Pause = false;
        int[] disp = display;
        int white2 = -100;
        while (true) {
            long time1 = System.currentTimeMillis();
            for (int i = 0; i < 5; i++) {
                Create_sprite((math.Rnd(2400) - 1200) << 16, (math.Rnd(1500) + 13000) << 16, -55705600, cof_W, 19);
            }
            Sprite_life();
            camera_view();
            draw2D(ppp, room_now);
            draw_3d(draw_loop);
            int len = (Height_real_3D - 2) * Width_real_3D;
            int R = white2 * 255;
            int G = white2 * 255;
            int B = white2 * 255;
            if (white2 > 255) {
                white2 = 255;
            }
            if (white2 >= 0) {
                while (true) {
                    len--;
                    if (len < 0) {
                        break;
                    }
                    int a = disp[len];
                    disp[len] = ((((((16711680 & a) >> 16) * (255 - white2)) + R) >> 8) << 16) | ((((((65280 & a) >> 8) * (255 - white2)) + G) >> 8) << 8) | ((((a & 255) * (255 - white2)) + B) >> 8);
                }
            }
            if (white2 == 255) {
                Menu.enable = true;
                Game.Pause = true;
                return;
            }
            white2 += 7;
            NET_Lizard.notifyDestroyed.repaint();
            NET_Lizard.notifyDestroyed.serviceRepaints();
            long time2 = System.currentTimeMillis() - time1;
            if (time2 < 77) {
                try {
                    Thread.sleep(77 - time2);
                } catch (Exception e) {
                }
            }
        }
    }

    static final void my_scr() {
        unit u = Units[0];
        switch (Game.my_level) {
            case 2:
                if (room_now == 45) {
                    vec_speed[0] = 0;
                    vec_speed[1] = 0;
                    vec_speed[2] = 0;
                    u.pos_x = -100466688;
                    u.pos_y = -141295616;
                    u.pos_z = -16384000;
                    rot_z = 180;
                    math.change_room(u);
                }
                if (Math.abs((u.pos_x >> 16) + 1200) + Math.abs((u.pos_y >> 16) + 1046) < 150) {
                    u.pos_x = -78643200;
                    u.pos_y = 147456000;
                    u.pos_z = distance_portal;
                    math.change_room(u);
                    return;
                }
                return;
            case 3:
                if (open_rooms[5] == -1) {
                    if (u.pos_y > 371916800) {
                        u.pos_y = 371916800;
                    }
                    if (u.pos_y < 270336000) {
                        u.pos_y = 270336000;
                    }
                }
                if (open_rooms[stp_draw] == -1) {
                    if (u.pos_y > 706150400) {
                        u.pos_y = 706150400;
                    }
                    if (u.pos_y < 604569600) {
                        u.pos_y = 604569600;
                        return;
                    }
                    return;
                }
                return;
            case 4:
                if (!for_level_4 && u.pos_y > 267059200) {
                    u.pos_y = 267059200;
                    return;
                }
                return;
            case 5:
                if (open_rooms[53] == -1 && room_now == 34 && u.pos_y > -67174400) {
                    u.pos_y = -67174400;
                }
                if ((u.pos_z >> 16) < -1050) {
                    kill_me(Units[0]);
                    return;
                }
                return;
            case 6:
                unit[] U = Units;
                for (int i = 1; i < units; i++) {
                    unit u1 = U[i];
                    if (!u1.death && u1.visible) {
                        if (u1.room_now == 5 || u1.room_now == 11 || u1.room_now == 27 || u1.room_now == 33) {
                            u1.rr[1] = 0;
                            u1.rr[0] = 65536;
                            u1.F[0] = 65536;
                        }
                        if (u1.room_now == 44 || u1.room_now == 40 || u1.room_now == 25) {
                            u1.rr[1] = 0;
                            u1.rr[0] = -65536;
                            u1.F[0] = -65536;
                        }
                    }
                }
                return;
            case 7:
                int x = u.pos_x >> 16;
                int y = u.pos_y >> 16;
                if (room_now > 2) {
                    rot_x = 359;
                }
                if (room_now == 5 || room_now == 3) {
                    u.F[1] = -65536;
                }
                if (room_now == 1) {
                    u.F[1] = 65536;
                }
                if (Objects[0].visible && Math.abs(x - 11) + Math.abs(y - 1) < 300) {
                    next_level();
                    return;
                }
                return;
            case 8:
                int x2 = u.pos_x >> 16;
                int y2 = u.pos_y >> 16;
                vec_speed[0] = 0;
                vec_speed[1] = 0;
                vec_speed[2] = 0;
                if (u.pos_z < -39321600) {
                    kill_me(Units[0]);
                }
                if (Math.abs(x2 - 1757) + Math.abs(y2 - 494) < 300 || Math.abs(x2 + 10050) + Math.abs(y2 - 484) < 300) {
                    u.pos_x = -49152000;
                    u.pos_y = -76349440;
                    math.change_room(u);
                }
                if (Math.abs(x2 - 4457) + Math.abs(y2 + 1605) < 300) {
                    u.pos_x = 462028800;
                    u.pos_y = 474087424;
                    u.pos_z = 6422528;
                    math.change_room(u);
                }
                if (Math.abs(x2 - 7050) + Math.abs(y2 - 3784) < 300) {
                    u.pos_x = 56164352;
                    u.pos_y = 82575360;
                    u.pos_z = 13959168;
                    math.change_room(u);
                }
                if (Math.abs(x2 + 4650) + Math.abs(y2 + 115) < 300) {
                    u.pos_x = -550502400;
                    u.pos_y = 139853824;
                    u.pos_z = 13631488;
                    math.change_room(u);
                }
                if (Math.abs(x2 + 5700) + Math.abs(y2 + 4165) < 300) {
                    u.pos_x = -167116800;
                    u.pos_y = -587530240;
                    u.pos_z = 14417920;
                    math.change_room(u);
                    return;
                }
                return;
            case 9:
                int x3 = u.pos_x >> 16;
                int y3 = u.pos_y >> 16;
                vec_speed[0] = 0;
                vec_speed[1] = 0;
                vec_speed[2] = 0;
                if (u.pos_z < -39321600) {
                    kill_me(Units[0]);
                }
                if (Math.abs(x3 + 7574) + Math.abs(y3 + 968) < 300) {
                    u.pos_x = -152305664;
                    u.pos_y = 397213696;
                    u.pos_z = 13107200;
                    math.change_room(u);
                }
                if (Math.abs(x3 + 8182) + Math.abs(y3 - 4599) < 300) {
                    u.pos_x = 742195200;
                    u.pos_y = 595132416;
                    u.pos_z = 13107200;
                    math.change_room(u);
                }
                if (Math.abs(x3 - 6817) + Math.abs(y3 - 4863) < 300 || Math.abs(x3 - 4432) + Math.abs(y3 + 13214) < 300) {
                    u.pos_x = 466419712;
                    u.pos_y = -32768000;
                    u.pos_z = 13107200;
                    math.change_room(u);
                }
                if (Math.abs(x3 - 6517) + Math.abs(y3 + 5000) < 300) {
                    u.pos_x = 289996800;
                    u.pos_y = -622460928;
                    u.pos_z = 13107200;
                    math.change_room(u);
                }
                if (Math.abs(x3 - 12217) + Math.abs(y3 - 399) < 300) {
                    next_level();
                    return;
                }
                return;
            case 10:
                if (Objects[51].color_obj && Math.abs((u.pos_x >> 16) + 5432) + Math.abs((u.pos_y >> 16) - 4508) < 350) {
                    Objects[51].color_obj = false;
                    scr.enable_script = true;
                    scr.proslushka = -1;
                }
                if (Objects[52].color_obj && Math.abs((u.pos_x - Objects[52].pos_x) >> 16) + Math.abs((u.pos_y - Objects[52].pos_y) >> 16) < 360) {
                    Objects[52].color_obj = false;
                    scr.enable_script = true;
                    scr.proslushka = -1;
                }
                if (Objects[64].color_obj && Math.abs((u.pos_x >> 16) - 8497) + Math.abs((u.pos_y >> 16) + 327) < 360) {
                    Objects[64].color_obj = false;
                    scr.enable_script = true;
                    scr.proslushka = -1;
                }
                if (Math.abs((u.pos_x >> 16) + 1673) + Math.abs((u.pos_y >> 16) - 1081) < 360) {
                    next_level();
                    return;
                } else if (u.pos_z < -26214400) {
                    kill_me(Units[0]);
                    return;
                } else {
                    return;
                }
            case J2MECanvas.GAME_C:
                if (open_rooms[8] == -1 && open_rooms[9] == -1 && Units[0].pos_y < 8192000) {
                    Units[0].pos_y = 125;
                    return;
                }
                return;
            case 12:
                int x4 = u.pos_x >> 16;
                int y4 = u.pos_y >> 16;
                vec_speed[0] = 0;
                vec_speed[1] = 0;
                vec_speed[2] = 0;
                if (Math.abs(x4 + 3890) + Math.abs(y4 + 5787) < 300) {
                    u.pos_x = 325255168;
                    u.pos_y = 72679424;
                    u.pos_z = 13107200;
                    math.change_room(u);
                }
                if (Math.abs(x4 + 5236) + Math.abs(y4 - 1109) < 300) {
                    u.pos_x = 571015168;
                    u.pos_y = -310640640;
                    u.pos_z = 13107200;
                    math.change_room(u);
                }
                if (Math.abs(x4 - 8713) + Math.abs(y4 + 2870) < 300) {
                    next_level();
                    return;
                } else if (u.pos_z < -39321600) {
                    kill_me(Units[0]);
                    return;
                } else {
                    return;
                }
            case Constants.RMS_store_recordID_SubscribtionMode:
                int x5 = u.pos_x >> 16;
                int y5 = u.pos_y >> 16;
                vec_speed[0] = 0;
                vec_speed[1] = 0;
                vec_speed[2] = 0;
                if (room_now == 5) {
                    u.pos_x = -78643200;
                    u.pos_y = -149094400;
                    rot_z = 180;
                    math.change_room(u);
                }
                if (Math.abs(x5 + 1200) + Math.abs(y5 - 2556) < 300) {
                    u.pos_x = 39321600;
                    u.pos_y = 144637952;
                    math.change_room(u);
                }
                if (Math.abs(x5 - 2922) + Math.abs(y5 - 1478) < 300) {
                    next_level();
                    return;
                }
                return;
            case Constants.RMS_store_recordID_DemoModeElapsed:
                int x6 = u.pos_x >> 16;
                int y6 = u.pos_y >> 16;
                vec_speed[0] = 0;
                vec_speed[1] = 0;
                vec_speed[2] = 0;
                if (Math.abs(x6 - 6150) + Math.abs(y6 - 4876) < 300) {
                    next_level();
                    return;
                }
                return;
            case 15:
                if (room_now == 29 && Units[0].pos_y < 840499200) {
                    Units[0].pos_y = 840499200;
                }
                if (Objects[8].visible && Math.abs((u.pos_x >> 16) - 6000) + Math.abs((u.pos_y >> 16) - 14500) < 300) {
                    next_level();
                    return;
                }
                return;
            case 16:
                if (Objects[7].light != 0) {
                    object object = Objects[7];
                    object.r_x -= 10;
                    Objects[8].r_x += 10;
                    if (Objects[7].r_x < 314) {
                        Objects[7].r_x = 314;
                        Objects[8].r_x = 45;
                        Objects[7].light = 0;
                        return;
                    }
                    return;
                }
                Objects[7].r_x += 10;
                object object2 = Objects[8];
                object2.r_x -= 10;
                if (Objects[7].r_x > 359) {
                    Objects[7].r_x = 359;
                    Objects[8].r_x = 0;
                    Objects[7].light = 1;
                    return;
                }
                return;
            default:
                return;
        }
    }

    static final void end() {
        Menu.loading = true;
        Menu.state_load = cof_W;
        delete_m3d();
        Game game = Menu.game;
        Game.load_font();
        if (Game.load_scene) {
            load_m3d(0);
        }
        Menu.menu_type = max_sprite;
        Menu.loading = false;
        Game.sound_stop();
        Game.music();
    }

    static final void next_level() {
        if (!Game.demo || Game.my_level < 1) {
            Game.my_level = (byte) (Game.my_level + 1);
            if (Game.open_level < Game.my_level && Game.my_level < 16) {
                Game.open_level = Game.my_level;
            }
            Menu.loading = true;
            Menu.state_load = cof_W;
            delete_m3d();
            Menu.loading = false;
            if (Game.my_level == 17) {
                weap_r();
            }
            if (!Menu.chit_level && !Menu.chit_life) {
                save_rm(true, false);
            }
            Menu.enable = true;
            Menu.loading = true;
            Menu.state_load = cof_W;
            load_m3d(Game.my_level);
            Menu.loading = false;
            Menu.menu_type = 2;
            Game.Pause = false;
            Menu.enable = false;
            return;
        }
        Game.gg = false;
        Secur_Dem.startBuy();
    }

    static final void rot_FPS() {
        rot_FPS = (short) (rot_FPS + 6);
        if (rot_FPS >= 360) {
            rot_FPS = (short) (rot_FPS - 360);
        }
    }

    static final void plus_HP() {
        if (!Units[0].death) {
            HP = (short) (HP + 1);
            if (HP >= 17) {
                HP = 0;
                unit unit = Units[0];
                unit.life = (short) (unit.life + 1);
                if (Units[0].life > 100) {
                    Units[0].life = 100;
                }
            }
        }
    }

    static final void reload_FPS() {
        if (reload_FPS > 0) {
            reload_FPS = (byte) (reload_FPS - 1);
            change = (byte) (change + 1);
            if (change >= 4) {
                change = 4;
            }
        }
    }

    public static final void shot_delay() {
        if (shot_delay > 0) {
            shot_delay = (byte) (shot_delay - 1);
        }
    }

    static final void time_change() {
        if (change > 0 && reload_FPS == 0) {
            change = (byte) (change - 1);
            if (change == 4) {
                weapon_now = change_weapon;
            }
        }
    }

    public static final void re_FPS() {
        int need;
        int i = weapon_now;
        if (ammo[i] != 0 && (need = max_ammo_in_mag[i] - ammo_in_mag[i]) != 0) {
            if (need > ammo[i]) {
                byte[] bArr = ammo_in_mag;
                bArr[i] = (byte) (bArr[i] + ammo[i]);
                ammo[i] = cof_W;
                return;
            }
            byte[] bArr2 = ammo_in_mag;
            bArr2[i] = (byte) (bArr2[i] + need);
            byte[] bArr3 = ammo;
            bArr3[i] = (byte) (bArr3[i] - need);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: int[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void shot_me() {
        /*
            r8 = 6
            r7 = 5
            r6 = 1
            r5 = 0
            byte r2 = ua.netlizard.egypt.m3d.shot_delay
            if (r2 != 0) goto L_0x001e
            byte r2 = ua.netlizard.egypt.m3d.change
            if (r2 != 0) goto L_0x001e
            byte r2 = ua.netlizard.egypt.m3d.reload_FPS
            if (r2 != 0) goto L_0x001e
            int r2 = ua.netlizard.egypt.m3d.weapon_now
            if (r2 == 0) goto L_0x001f
            int r2 = ua.netlizard.egypt.m3d.weapon_now
            if (r2 == r8) goto L_0x001f
            boolean r2 = find_ammo()
            if (r2 != 0) goto L_0x001f
        L_0x001e:
            return
        L_0x001f:
            int r2 = ua.netlizard.egypt.m3d.weapon_now
            ua.netlizard.egypt.Game.sound_weapon(r2)
            byte[] r2 = ua.netlizard.egypt.m3d.delay_shot
            int r3 = ua.netlizard.egypt.m3d.weapon_now
            byte r2 = r2[r3]
            ua.netlizard.egypt.m3d.shot_delay = r2
            int[] r2 = ua.netlizard.egypt.m3d.vec_speed
            int r3 = ua.netlizard.egypt.m3d.rot_x
            int r4 = ua.netlizard.egypt.m3d.rot_z
            ua.netlizard.egypt.math.vec_for_me(r2, r3, r4)
            int[] r2 = ua.netlizard.egypt.m3d.vec_speed
            byte r3 = ua.netlizard.egypt.m3d.razbros
            ua.netlizard.egypt.math.correct_vec_z(r2, r3)
            plus_smesh_FPS()
            int[] r0 = ua.netlizard.egypt.m3d.help_sort
            r0[r5] = r5
            ua.netlizard.egypt.unit[] r2 = ua.netlizard.egypt.m3d.Units
            r2 = r2[r5]
            short r2 = r2.room_now
            r0[r6] = r2
            r2 = 2
            byte[] r3 = ua.netlizard.egypt.m3d.shot_speed
            int r4 = ua.netlizard.egypt.m3d.weapon_now
            byte r3 = r3[r4]
            r0[r2] = r3
            r2 = 3
            byte[] r3 = ua.netlizard.egypt.m3d.shot_time_life
            int r4 = ua.netlizard.egypt.m3d.weapon_now
            byte r3 = r3[r4]
            r0[r2] = r3
            byte[] r2 = ua.netlizard.egypt.m3d.shot_demage_me
            int r3 = ua.netlizard.egypt.m3d.weapon_now
            byte r2 = r2[r3]
            int r2 = r2 << 8
            r1 = r2 | 3
            int r2 = ua.netlizard.egypt.m3d.weapon_now
            if (r2 != r7) goto L_0x0075
            byte[] r2 = ua.netlizard.egypt.m3d.shot_demage_me
            int r3 = ua.netlizard.egypt.m3d.weapon_now
            byte r2 = r2[r3]
            int r2 = r2 << 8
            r1 = r2 | 4
        L_0x0075:
            r2 = 4
            r0[r2] = r1
            int[] r2 = ua.netlizard.egypt.m3d.shot_color
            int r3 = ua.netlizard.egypt.m3d.weapon_now
            r2 = r2[r3]
            r0[r7] = r2
            int[] r2 = ua.netlizard.egypt.m3d.s
            int[] r3 = ua.netlizard.egypt.m3d.vec_speed
            ua.netlizard.egypt.math.shot(r2, r3, r0)
            int r2 = ua.netlizard.egypt.m3d.weapon_now
            if (r2 == 0) goto L_0x001e
            int r2 = ua.netlizard.egypt.m3d.weapon_now
            if (r2 == r8) goto L_0x001e
            ua.netlizard.egypt.unit[] r2 = ua.netlizard.egypt.m3d.Units
            r2 = r2[r5]
            int r2 = r2.pos_x
            ua.netlizard.egypt.m3d.x_light = r2
            ua.netlizard.egypt.unit[] r2 = ua.netlizard.egypt.m3d.Units
            r2 = r2[r5]
            int r2 = r2.pos_y
            ua.netlizard.egypt.m3d.y_light = r2
            ua.netlizard.egypt.unit[] r2 = ua.netlizard.egypt.m3d.Units
            r2 = r2[r5]
            int r2 = r2.pos_z
            ua.netlizard.egypt.m3d.z_light = r2
            ua.netlizard.egypt.m3d.fire2 = r6
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.m3d.shot_me():void");
    }

    public static final boolean find_ammo() {
        int i = weapon_now;
        if (ammo_in_mag[i] != 0) {
            byte[] bArr = ammo_in_mag;
            bArr[i] = (byte) (bArr[i] - 1);
            return true;
        } else if (ammo[i] == 0) {
            return false;
        } else {
            if (max_ammo_in_mag[i] > ammo[i]) {
                ammo_in_mag[i] = ammo[i];
                ammo[i] = cof_W;
            } else {
                byte[] bArr2 = ammo;
                bArr2[i] = (byte) (bArr2[i] - max_ammo_in_mag[i]);
                ammo_in_mag[i] = max_ammo_in_mag[i];
            }
            reload_FPS = 20;
            return false;
        }
    }

    public static final void change_weapon(byte a) {
        if (reload_FPS == 0 && change == 0) {
            change = max_change;
            int shot_chik = 0;
            if (a >= 0) {
                do {
                    slots = (byte) (slots + 1);
                    if (slots > 6) {
                        slots = cof_W;
                    }
                    if (slots == a) {
                        break;
                    }
                    shot_chik++;
                } while (shot_chik != 12);
            } else {
                do {
                    slots = (byte) (slots + 1);
                    if (slots > 6) {
                        slots = cof_W;
                    }
                    if (slot[slots] == 1) {
                        break;
                    }
                    shot_chik++;
                } while (shot_chik != 12);
            }
            change_weapon = slots;
        }
    }

    public static final void plus_smesh_FPS() {
        smesh_y = (byte) (smesh_y + ((-Math.abs(myRandom.nextInt() & 15)) - 16));
        if (smesh_y < -40) {
            smesh_y = -40;
        }
        if (smesh_y > 40) {
            smesh_y = 40;
        }
        int a = 1;
        if ((myRandom.nextInt() & 1) == 0) {
            a = -1;
        }
        smesh_x = (byte) (smesh_x + (((myRandom.nextInt() & 15) + 16) * a));
        if (smesh_x < -30) {
            smesh_x = -30;
        }
        if (smesh_x > 30) {
            smesh_x = 30;
        }
        razbros = (byte) (razbros + razbros_FPS[weapon_now]);
        if (razbros > 20) {
            razbros = 20;
        }
    }

    public static final void smesh_FPS_weapon() {
        if (smesh_x > 0) {
            smesh_x = (byte) (smesh_x - 5);
            if (smesh_x < 0) {
                smesh_x = cof_W;
            }
        }
        if (smesh_x < 0) {
            smesh_x = (byte) (smesh_x + 5);
            if (smesh_x > 0) {
                smesh_x = cof_W;
            }
        }
        if (smesh_y > 0) {
            smesh_y = (byte) (smesh_y - 5);
            if (smesh_y < 0) {
                smesh_y = cof_W;
            }
        }
        if (smesh_y < 0) {
            smesh_y = (byte) (smesh_y + 5);
            if (smesh_y > 0) {
                smesh_y = cof_W;
            }
        }
        if (razbros > 0) {
            razbros = (byte) (razbros - 1);
        }
    }

    public static final void delete_load_bags() {
        for (int i = 0; i < units; i++) {
            switch (Units[i].type_unit) {
                case 1:
                    Units[i].Points[7] = null;
                    Units[i].norms[7] = null;
                    break;
                case 2:
                    Units[i].Points[7] = null;
                    Units[i].norms[7] = null;
                    Units[i].Points[9] = null;
                    Units[i].norms[9] = null;
                    break;
                case 3:
                    Units[i].Points[5] = null;
                    Units[i].norms[5] = null;
                    break;
                case 5:
                    Units[i].Points[9] = null;
                    Units[i].norms[9] = null;
                    break;
                case 6:
                    Units[i].Points[7] = null;
                    Units[i].norms[7] = null;
                    break;
            }
        }
        if (mas_unit[1]) {
            UNI[1].Points[7] = null;
            UNI[1].norms[7] = null;
        }
        if (mas_unit[2]) {
            UNI[2].Points[7] = null;
            UNI[2].norms[7] = null;
            UNI[2].Points[9] = null;
            UNI[2].norms[9] = null;
        }
        if (mas_unit[3]) {
            UNI[3].Points[5] = null;
            UNI[3].norms[5] = null;
        }
        if (mas_unit[5]) {
            UNI[5].Points[9] = null;
            UNI[5].norms[9] = null;
        }
        if (mas_unit[6]) {
            UNI[6].Points[7] = null;
            UNI[6].norms[7] = null;
        }
    }

    static final void load_msg_str(int a) {
        String[] b = Uni.uni.load_mmstring("tg", 61)[a - 1];
        msg_str = new String[b.length][];
        for (int i = 0; i < b.length; i++) {
            msg_str[i] = Uni.uni.sorter(b[i], Game.font[0], (Game.Width >> 1) + (Game.Width >> 2), true);
        }
    }

    static final void delete_msg_str() {
        msg_str = null;
    }

    public static final void restart(int a) {
        Menu.loading = true;
        Menu.state_load = cof_W;
        if (Game.deathmach) {
            weap_r();
        }
        Rooms = null;
        Menu.game.draw_state(10);
        Portals = null;
        Menu.game.draw_state(20);
        Units = null;
        Menu.game.draw_state(40);
        OBJ = null;
        Menu.game.draw_state(50);
        Objects = null;
        Menu.game.draw_state(60);
        vec_speed = null;
        sprite = null;
        vec_speed = new int[3];
        shot = (int[][]) Array.newInstance(Integer.TYPE, 50, 12);
        sprite = (int[][]) Array.newInstance(Integer.TYPE, 15, 6);
        load_mas();
        Menu.game.draw_state(65);
        load_level(a);
        Menu.game.draw_state(70);
        load_meet();
        load_object();
        Menu.game.draw_state(80);
        load_main_unit();
        Menu.game.draw_state(90);
        delete_load_bags();
        reload_ex(a);
        Menu.game.draw_state(100);
        load_rm(true, false);
        Menu.game.draw_state(101);
        Game.my_level = (byte) a;
        Menu.loading = false;
        Game.Pause = false;
        Menu.enable = false;
    }

    public static final void load_meet() {
        mas_object[1] = true;
        mas_object[2] = true;
        mas_object[3] = true;
        object_help = new object();
    }

    public static final void delete_meet() {
        object_help = null;
    }

    static final void start_abc() {
        Menu.enable = true;
        Game.Pause = true;
        Menu.menu_type = 18;
        Menu.name_ = "";
    }

    public static final void load_m3d(int a) {
        if (a == 8 || a == 9 || a == 12) {
            sky_olveis = true;
        } else {
            sky_olveis = false;
        }
        if (a == 7 || a == 11 || a == 15) {
            boss = true;
        } else {
            boss = false;
        }
        if (a != 0) {
            Menu.load_il0();
        }
        Game.my_level = (byte) a;
        load_screen();
        Menu.game.draw_state(10);
        load_mas();
        Menu.game.draw_state(15);
        load_math();
        Menu.game.draw_state(25);
        load_level(a);
        Menu.game.draw_state(40);
        Game.load_sound(a);
        index_sort = new int[(units + 65 + objects)];
        help_sort = new int[(units + 65 + objects)];
        if (a != 0) {
            load_meet();
        }
        load_object();
        Menu.game.draw_state(50);
        load_unit();
        Menu.game.draw_state(60);
        delete_load_bags();
        Menu.game.draw_state(62);
        if (Game.deathmach) {
            scr.load_script(0);
        } else if (a != 0) {
            scr.load_script(a);
        }
        Menu.game.draw_state(70);
        reload_ex(a);
        if (a != 0) {
            load_FPS_weapon();
        }
        load_texture(a);
        Menu.game.draw_state(90);
        Units[0].room_now = Units[0].room_now;
        room_now = room_now;
        delete_mas();
        if (a != 0) {
            load_msg_str(a);
        }
        Menu.game.draw_state(100);
        if (Menu.chit_life) {
            for (int i = 1; i < 7; i++) {
                ammo_in_mag[i] = max_ammo_in_mag[i];
                ammo[i] = max_ammo[i];
                slot[i] = 1;
            }
        }
        System.gc();
        Game.touch_began = false;
        Game.touch_move = false;
        Game.touch_end = false;
        Menu.game.draw_state(101);
    }

    public static final void delete_m3d() {
        delete_screen();
        Menu.game.draw_state(10);
        delete_mas();
        Menu.game.draw_state(20);
        delete_math();
        Menu.game.draw_state(30);
        delete_level();
        Menu.game.draw_state(40);
        delete_object();
        Menu.game.draw_state(50);
        delete_unit();
        Menu.game.draw_state(60);
        scr.delete_script();
        Menu.game.draw_state(70);
        delete_texture();
        Menu.game.draw_state(80);
        delete_meet();
        delete_msg_str();
        Menu.game.draw_state(90);
        scr.delete_script();
        Menu.game.draw_state(100);
        Menu.game.draw_state(101);
        System.gc();
    }

    private static final int load_cu() {
        byte[] mas_load = Uni.uni.load_pack("cu", -1);
        int msc_e = mas_load.length / 6;
        int cvo = (short) msc_e;
        cu = (short[][]) Array.newInstance(Short.TYPE, msc_e, 6);
        for (int i = 0; i < msc_e; i++) {
            int index = i * 6;
            for (int j = 0; j < 6; j++) {
                cu[i][j] = mas_load[index + j];
                if (cu[i][j] < 0) {
                    short[] sArr = cu[i];
                    sArr[j] = (short) (sArr[j] + 256);
                }
            }
        }
        return cvo;
    }

    private static final void load_unit() {
        UNI = new main_unit[8];
        for (int num = 0; num < 8; num++) {
            if (mas_unit[num]) {
                byte[] mas = Uni.uni.load_pack("un" + num, -1);
                UNI[num] = new main_unit();
                main_unit u = UNI[num];
                int index_ = -2 + 2;
                u.max_x = math.to_short(mas[index_], mas[1]) << 16;
                int index_2 = index_ + 2;
                u.max_y = math.to_short(mas[index_2], mas[3]) << 16;
                int index_3 = index_2 + 2;
                u.max_z = math.to_short(mas[index_3], mas[5]) << 16;
                int index_4 = index_3 + 2;
                u.min_x = math.to_short(mas[index_4], mas[7]) << 16;
                int index_5 = index_4 + 2;
                u.min_y = math.to_short(mas[index_5], mas[9]) << 16;
                int index_6 = index_5 + 2;
                u.min_z = math.to_short(mas[index_6], mas[11]) << 16;
                int index_7 = index_6 + 2;
                int all_point = math.to_short(mas[index_7], mas[13]);
                int index_8 = index_7 + 2;
                int all_poly = math.to_short(mas[index_8], mas[15]);
                int index_9 = index_8 + 2;
                int all_textpoint = math.to_short(mas[index_9], mas[17]);
                int index_10 = index_9 + 2;
                int all_keys = math.to_short(mas[index_10], mas[19]);
                u.Points = new short[all_keys][];
                u.norms = new byte[all_keys][];
                u.point = all_point;
                for (int j = 0; j < all_keys; j++) {
                    u.norms[j] = new byte[(all_poly * 3)];
                    u.Points[j] = new short[(all_point * 3)];
                }
                u.Poly = new short[(all_poly * 16)];
                int all_textpoint2 = all_textpoint * 2;
                short[] text_point = new short[all_textpoint2];
                for (int j2 = 0; j2 < all_keys; j2++) {
                    for (int i = 0; i < all_point; i++) {
                        int index_11 = index_10 + 2;
                        u.Points[j2][i * 3] = (short) math.to_short(mas[index_11], mas[index_11 + 1]);
                        int index_12 = index_11 + 2;
                        u.Points[j2][(i * 3) + 1] = (short) math.to_short(mas[index_12], mas[index_12 + 1]);
                        index_10 = index_12 + 2;
                        u.Points[j2][(i * 3) + 2] = (short) math.to_short(mas[index_10], mas[index_10 + 1]);
                    }
                }
                int m = all_poly * 16;
                for (int i2 = 0; i2 < m; i2 += 16) {
                    int index_13 = index_10 + 2;
                    u.Poly[i2] = (short) (math.to_short(mas[index_13], mas[index_13 + 1]) * 3);
                    int index_14 = index_13 + 2;
                    u.Poly[i2 + 1] = (short) (math.to_short(mas[index_14], mas[index_14 + 1]) * 3);
                    index_10 = index_14 + 2;
                    u.Poly[i2 + 2] = (short) (math.to_short(mas[index_10], mas[index_10 + 1]) * 3);
                }
                int m2 = all_poly * 3;
                int index_15 = index_10 + 1;
                for (int j3 = 0; j3 < all_keys; j3++) {
                    for (int i3 = 0; i3 < m2; i3 += 3) {
                        int index_16 = index_15 + 1;
                        u.norms[j3][i3] = mas[index_16];
                        int index_17 = index_16 + 1;
                        u.norms[j3][i3 + 1] = mas[index_17];
                        index_15 = index_17 + 1;
                        u.norms[j3][i3 + 2] = mas[index_15];
                    }
                }
                int index_18 = index_15 - 1;
                for (int i4 = 0; i4 < all_textpoint2; i4 += 2) {
                    int index_19 = index_18 + 2;
                    text_point[i4] = (short) ((int) ((128 * ((long) math.to_short(mas[index_19], mas[index_19 + 1]))) / 1000));
                    index_18 = index_19 + 2;
                    text_point[i4 + 1] = (short) ((int) ((128 * ((long) math.to_short(mas[index_18], mas[index_18 + 1]))) / 1000));
                }
                int all_poly2 = all_poly * 16;
                for (int i5 = 0; i5 < all_poly2; i5 += 16) {
                    int index_20 = index_18 + 2;
                    u.Poly[i5 + 3] = text_point[math.to_short(mas[index_20], mas[index_20 + 1]) * 2];
                    u.Poly[i5 + 4] = text_point[(math.to_short(mas[index_20], mas[index_20 + 1]) * 2) + 1];
                    int index_21 = index_20 + 2;
                    u.Poly[i5 + 5] = text_point[math.to_short(mas[index_21], mas[index_21 + 1]) * 2];
                    u.Poly[i5 + 6] = text_point[(math.to_short(mas[index_21], mas[index_21 + 1]) * 2) + 1];
                    index_18 = index_21 + 2;
                    u.Poly[i5 + 7] = text_point[math.to_short(mas[index_18], mas[index_18 + 1]) * 2];
                    u.Poly[i5 + 8] = text_point[(math.to_short(mas[index_18], mas[index_18 + 1]) * 2) + 1];
                }
                for (int i6 = 0; i6 < all_poly2; i6 += 16) {
                    index_18 += 2;
                    u.Poly[i6 + 9] = (byte) math.to_short(mas[index_18], mas[index_18 + 1]);
                    mas_texture[u.Poly[i6 + 9]] = true;
                }
            }
        }
        load_main_unit();
    }

    public static final void load_main_unit() {
        for (int i = 1; i < Units.length; i++) {
            Units[i].norms = UNI[Units[i].type_unit].norms;
            Units[i].Points = UNI[Units[i].type_unit].Points;
            Units[i].Poly = UNI[Units[i].type_unit].Poly;
        }
    }

    public static final void delete_unit() {
        UNI = null;
        Units = null;
    }

    private static final void load_object() {
        OBJ = new main_object[50];
        for (int num = 0; num < 50; num++) {
            if (mas_object[num]) {
                byte[] mas = Uni.uni.load_pack("o/o" + num, -1);
                OBJ[num] = new main_object();
                int index_ = -2 + 2;
                OBJ[num].max_x = math.to_short(mas[index_], mas[1]) << 16;
                int index_2 = index_ + 2;
                OBJ[num].max_y = math.to_short(mas[index_2], mas[3]) << 16;
                int index_3 = index_2 + 2;
                OBJ[num].max_z = math.to_short(mas[index_3], mas[5]) << 16;
                int index_4 = index_3 + 2;
                OBJ[num].min_x = math.to_short(mas[index_4], mas[7]) << 16;
                int index_5 = index_4 + 2;
                OBJ[num].min_y = math.to_short(mas[index_5], mas[9]) << 16;
                int index_6 = index_5 + 2;
                OBJ[num].min_z = math.to_short(mas[index_6], mas[11]) << 16;
                int x = OBJ[num].max_x >> 16;
                int y = OBJ[num].max_y >> 16;
                int z = OBJ[num].max_z >> 16;
                int len = (x * x) + (y * y) + (z * z);
                int wfm = 65536;
                int leght = 0;
                while (true) {
                    wfm >>= 1;
                    if (wfm <= 0) {
                        break;
                    } else if ((leght + wfm) * (leght + wfm) <= len) {
                        leght += wfm;
                    }
                }
                OBJ[num].dist_w = ((leght << 3) * Width_real_3D) << 1;
                OBJ[num].dist_h = (leght << 1) * Height_real_3D;
                int index_7 = index_6 + 2 + 2 + 2 + 2 + 2;
                int all_point = math.to_short(mas[index_7], mas[21]);
                int index_8 = index_7 + 2;
                int all_poly = math.to_short(mas[index_8], mas[23]);
                int index_9 = index_8 + 2;
                int all_textpoint = math.to_short(mas[index_9], mas[25]);
                OBJ[num].point = all_point;
                OBJ[num].Points = new short[(all_point * 3)];
                OBJ[num].Poly = new short[(all_poly * 16)];
                int all_textpoint2 = all_textpoint * 2;
                int[] text_point = new int[all_textpoint2];
                for (int i = 0; i < all_point; i++) {
                    int index_10 = index_9 + 2;
                    OBJ[num].Points[i * 3] = (short) math.to_short(mas[index_10], mas[index_10 + 1]);
                    int index_11 = index_10 + 2;
                    OBJ[num].Points[(i * 3) + 1] = (short) math.to_short(mas[index_11], mas[index_11 + 1]);
                    index_9 = index_11 + 2;
                    OBJ[num].Points[(i * 3) + 2] = (short) math.to_short(mas[index_9], mas[index_9 + 1]);
                }
                for (int i2 = 0; i2 < all_poly; i2++) {
                    int index_12 = index_9 + 2;
                    OBJ[num].Poly[i2 * 16] = (short) (math.to_short(mas[index_12], mas[index_12 + 1]) * 3);
                    int index_13 = index_12 + 2;
                    OBJ[num].Poly[(i2 * 16) + 1] = (short) (math.to_short(mas[index_13], mas[index_13 + 1]) * 3);
                    index_9 = index_13 + 2;
                    OBJ[num].Poly[(i2 * 16) + 2] = (short) (math.to_short(mas[index_9], mas[index_9 + 1]) * 3);
                }
                for (int i3 = 0; i3 < all_poly; i3++) {
                    int index_14 = index_9 + 2;
                    OBJ[num].Poly[(i3 * 16) + 10] = (byte) ((int) ((((long) ((int) (((long) (65536 * math.to_short(mas[index_14], mas[index_14 + 1]))) / 10000))) * 127) >> 16));
                    int index_15 = index_14 + 2;
                    OBJ[num].Poly[(i3 * 16) + 11] = (byte) ((int) ((((long) ((int) (((long) (65536 * math.to_short(mas[index_15], mas[index_15 + 1]))) / 10000))) * 127) >> 16));
                    index_9 = index_15 + 2;
                    OBJ[num].Poly[(i3 * 16) + 12] = (byte) ((int) ((((long) ((int) (((long) (65536 * math.to_short(mas[index_9], mas[index_9 + 1]))) / 10000))) * 127) >> 16));
                }
                for (int i4 = 0; i4 < all_textpoint2; i4 += 2) {
                    int index_16 = index_9 + 2;
                    text_point[i4] = (int) (((long) (math.to_short(mas[index_16], mas[index_16 + 1]) * 64)) / 100);
                    index_9 = index_16 + 2;
                    text_point[i4 + 1] = (int) (((long) (math.to_short(mas[index_9], mas[index_9 + 1]) * 64)) / 100);
                }
                for (int i5 = 0; i5 < all_poly; i5++) {
                    int index_17 = index_9 + 2;
                    OBJ[num].Poly[(i5 * 16) + 3] = (short) text_point[math.to_short(mas[index_17], mas[index_17 + 1]) * 2];
                    OBJ[num].Poly[(i5 * 16) + 4] = (short) text_point[(math.to_short(mas[index_17], mas[index_17 + 1]) * 2) + 1];
                    int index_18 = index_17 + 2;
                    OBJ[num].Poly[(i5 * 16) + 5] = (short) text_point[math.to_short(mas[index_18], mas[index_18 + 1]) * 2];
                    OBJ[num].Poly[(i5 * 16) + 6] = (short) text_point[(math.to_short(mas[index_18], mas[index_18 + 1]) * 2) + 1];
                    index_9 = index_18 + 2;
                    OBJ[num].Poly[(i5 * 16) + 7] = (short) text_point[math.to_short(mas[index_9], mas[index_9 + 1]) * 2];
                    OBJ[num].Poly[(i5 * 16) + 8] = (short) text_point[(math.to_short(mas[index_9], mas[index_9 + 1]) * 2) + 1];
                }
                int index_19 = index_9 + 2;
                for (int i6 = 0; i6 < all_poly; i6++) {
                    OBJ[num].Poly[(i6 * 16) + 9] = mas[index_19];
                    if (OBJ[num].Poly[(i6 * 16) + 9] < 50) {
                        mas_texture[OBJ[num].Poly[(i6 * 16) + 9]] = true;
                    }
                    index_19++;
                }
            }
        }
        byte[] mas_load = Uni.uni.load_pack("oi", -1);
        for (object ob : Objects) {
            if (ob.type_object < 120) {
                int a = ob.type_object << 2;
                if (mas_load[a] == 1) {
                    ob.cull_face = true;
                }
                if (mas_load[a + 1] == 1) {
                    ob.texture = true;
                }
                if (mas_load[a + 2] == 1) {
                    ob.color_obj = true;
                }
                if (mas_load[a + 3] == 1) {
                    ob.phiz = true;
                }
                main_object ob1 = OBJ[ob.type_object];
                ob.Points = ob1.Points;
                ob.Poly = ob1.Poly;
                ob.max_x = ob1.max_x;
                ob.min_x = ob1.min_x;
                ob.max_y = ob1.max_y;
                ob.min_y = ob1.min_y;
                ob.max_z = ob1.max_z;
                ob.min_z = ob1.min_z;
            }
        }
        if (Game.my_level == 10) {
            for (int ss = 0; ss < objects; ss++) {
                if (Objects[ss].type_object == 9) {
                    Objects[ss].color_obj = true;
                }
            }
        }
    }

    public static final void delete_object() {
        OBJ = null;
        Objects = null;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v13, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r28v384, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void load_level(int r34) {
        /*
            boolean r28 = ua.netlizard.egypt.Game.demo
            if (r28 == 0) goto L_0x0016
            byte r28 = ua.netlizard.egypt.Game.my_level
            r29 = 1
            r0 = r28
            r1 = r29
            if (r0 <= r1) goto L_0x0016
            r28 = 0
            ua.netlizard.egypt.Game.gg = r28
            ua.netlizard.egypt.Secur_Dem.startBuy()
        L_0x0015:
            return
        L_0x0016:
            r13 = -2
            boolean r28 = ua.netlizard.egypt.Game.deathmach
            if (r28 != 0) goto L_0x02ac
            ua.netlizard.egypt.Uni r28 = ua.netlizard.egypt.Uni.uni
            java.lang.StringBuilder r29 = new java.lang.StringBuilder
            java.lang.String r30 = "lvl"
            r29.<init>(r30)
            r0 = r29
            r1 = r34
            java.lang.StringBuilder r29 = r0.append(r1)
            java.lang.String r29 = r29.toString()
            r30 = -1
            byte[] r16 = r28.load_pack(r29, r30)
        L_0x0036:
            r3 = 0
            int r13 = r13 + 2
            byte r28 = r16[r13]
            r29 = 1
            byte r29 = r16[r29]
            int r25 = ua.netlizard.egypt.math.to_short(r28, r29)
            if (r25 == 0) goto L_0x004d
            r0 = r25
            byte[] r0 = new byte[r0]
            r28 = r0
            ua.netlizard.egypt.m3d.quit_rooms = r28
        L_0x004d:
            r12 = 0
        L_0x004e:
            r0 = r25
            if (r12 < r0) goto L_0x02c9
            int r13 = r13 + 2
            int[] r28 = ua.netlizard.egypt.m3d.s
            r29 = 0
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 << 16
            r28[r29] = r30
            int r13 = r13 + 2
            int[] r28 = ua.netlizard.egypt.m3d.s
            r29 = 1
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 << 16
            r28[r29] = r30
            int r13 = r13 + 2
            int[] r28 = ua.netlizard.egypt.m3d.s
            r29 = 2
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 << 16
            r28[r29] = r30
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r28 = ua.netlizard.egypt.math.to_short(r28, r29)
            ua.netlizard.egypt.m3d.rot_x = r28
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r28 = ua.netlizard.egypt.math.to_short(r28, r29)
            ua.netlizard.egypt.m3d.rot_z = r28
            int r28 = ua.netlizard.egypt.m3d.rot_z
            r29 = 361(0x169, float:5.06E-43)
            r0 = r28
            r1 = r29
            if (r0 <= r1) goto L_0x00bc
            int r28 = ua.netlizard.egypt.m3d.rot_z
            r29 = 65536(0x10000, float:9.18355E-41)
            int r28 = r28 - r29
            ua.netlizard.egypt.m3d.rot_z = r28
        L_0x00bc:
            int r28 = ua.netlizard.egypt.m3d.rot_x
            r29 = 361(0x169, float:5.06E-43)
            r0 = r28
            r1 = r29
            if (r0 <= r1) goto L_0x00ce
            int r28 = ua.netlizard.egypt.m3d.rot_x
            r29 = 65536(0x10000, float:9.18355E-41)
            int r28 = r28 - r29
            ua.netlizard.egypt.m3d.rot_x = r28
        L_0x00ce:
            int r28 = ua.netlizard.egypt.m3d.rot_z
            if (r28 >= 0) goto L_0x02e2
            int r28 = ua.netlizard.egypt.m3d.rot_z
            r0 = r28
            int r0 = -r0
            r28 = r0
            ua.netlizard.egypt.m3d.rot_z = r28
        L_0x00db:
            int r28 = ua.netlizard.egypt.m3d.rot_z
            r29 = 360(0x168, float:5.04E-43)
            r0 = r28
            r1 = r29
            if (r0 != r1) goto L_0x00e9
            r28 = 0
            ua.netlizard.egypt.m3d.rot_z = r28
        L_0x00e9:
            int r28 = ua.netlizard.egypt.m3d.rot_x
            if (r28 >= 0) goto L_0x02ee
            int r28 = ua.netlizard.egypt.m3d.rot_x
            r0 = r28
            int r0 = r0 + 270
            r28 = r0
            ua.netlizard.egypt.m3d.rot_x = r28
        L_0x00f7:
            int r28 = ua.netlizard.egypt.m3d.rot_x
            r29 = 360(0x168, float:5.04E-43)
            r0 = r28
            r1 = r29
            if (r0 != r1) goto L_0x0105
            r28 = 0
            ua.netlizard.egypt.m3d.rot_x = r28
        L_0x0105:
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r28 = ua.netlizard.egypt.math.to_short(r28, r29)
            r0 = r28
            short r0 = (short) r0
            r28 = r0
            ua.netlizard.egypt.m3d.rooms = r28
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r10 = ua.netlizard.egypt.math.to_short(r28, r29)
            int[] r0 = new int[r10]
            r28 = r0
            ua.netlizard.egypt.m3d.NORMALS = r28
            r12 = 0
        L_0x012b:
            if (r12 < r10) goto L_0x02fa
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r10 = ua.netlizard.egypt.math.to_short(r28, r29)
            short[] r0 = new short[r10]
            r28 = r0
            ua.netlizard.egypt.m3d.text_coords = r28
            r12 = 0
        L_0x0140:
            if (r12 < r10) goto L_0x0368
            short r28 = ua.netlizard.egypt.m3d.rooms
            r0 = r28
            ua.netlizard.egypt.room[] r0 = new ua.netlizard.egypt.room[r0]
            r28 = r0
            ua.netlizard.egypt.m3d.Rooms = r28
            short r28 = ua.netlizard.egypt.m3d.rooms
            r0 = r28
            byte[] r0 = new byte[r0]
            r28 = r0
            ua.netlizard.egypt.m3d.open_rooms = r28
            r12 = 0
        L_0x0157:
            short r28 = ua.netlizard.egypt.m3d.rooms
            r0 = r28
            if (r12 < r0) goto L_0x03b8
            r21 = 0
        L_0x015f:
            short r28 = ua.netlizard.egypt.m3d.rooms
            r0 = r21
            r1 = r28
            if (r0 < r1) goto L_0x03c5
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r9 = ua.netlizard.egypt.math.to_short(r28, r29)
            ua.netlizard.egypt.portal[] r0 = new ua.netlizard.egypt.portal[r9]
            r28 = r0
            ua.netlizard.egypt.m3d.Portals = r28
            r12 = 0
        L_0x017a:
            if (r12 < r9) goto L_0x0730
            r21 = 0
        L_0x017e:
            r0 = r21
            if (r0 < r9) goto L_0x0789
            r21 = 0
        L_0x0184:
            r0 = r21
            if (r0 < r9) goto L_0x08e1
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r28 = ua.netlizard.egypt.math.to_short(r28, r29)
            r0 = r28
            short r0 = (short) r0
            r28 = r0
            ua.netlizard.egypt.m3d.objects = r28
            short r28 = ua.netlizard.egypt.m3d.objects
            r0 = r28
            ua.netlizard.egypt.object[] r0 = new ua.netlizard.egypt.object[r0]
            r28 = r0
            ua.netlizard.egypt.m3d.Objects = r28
            short r28 = ua.netlizard.egypt.m3d.objects
            r0 = r28
            short[] r0 = new short[r0]
            r28 = r0
            ua.netlizard.egypt.m3d.open_doors_cof = r28
            r12 = 0
        L_0x01b0:
            short r28 = ua.netlizard.egypt.m3d.objects
            r0 = r28
            if (r12 < r0) goto L_0x099f
            r14 = 0
            r21 = 0
        L_0x01b9:
            short r28 = ua.netlizard.egypt.m3d.rooms
            r0 = r21
            r1 = r28
            if (r0 < r1) goto L_0x09ac
            r28 = -1
            ua.netlizard.egypt.m3d.room_with_sky_box = r28
            r28 = -1
            ua.netlizard.egypt.m3d.object_sky = r28
            r12 = 0
        L_0x01ca:
            short r28 = ua.netlizard.egypt.m3d.objects
            r0 = r28
            if (r12 < r0) goto L_0x0c87
        L_0x01d0:
            r23 = 0
        L_0x01d2:
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r0 = r28
            int r0 = r0.length
            r28 = r0
            r0 = r23
            r1 = r28
            if (r0 < r1) goto L_0x0cc7
            r15 = 0
            byte[] r15 = (byte[]) r15
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r28 = ua.netlizard.egypt.math.to_short(r28, r29)
            int r28 = r28 + 1
            r0 = r28
            short r0 = (short) r0
            r28 = r0
            ua.netlizard.egypt.m3d.units = r28
            short r28 = ua.netlizard.egypt.m3d.units
            r0 = r28
            byte[] r0 = new byte[r0]
            r28 = r0
            ua.netlizard.egypt.m3d.auto_aim = r28
            short r28 = ua.netlizard.egypt.m3d.units
            r0 = r28
            ua.netlizard.egypt.unit[] r0 = new ua.netlizard.egypt.unit[r0]
            r28 = r0
            ua.netlizard.egypt.m3d.Units = r28
            r12 = 0
        L_0x020c:
            short r28 = ua.netlizard.egypt.m3d.units
            r0 = r28
            if (r12 < r0) goto L_0x0d15
            r14 = 1
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r29 = 0
            r28 = r28[r29]
            int[] r29 = ua.netlizard.egypt.m3d.s
            r30 = 0
            r29 = r29[r30]
            r0 = r29
            r1 = r28
            r1.pos_x = r0
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r29 = 0
            r28 = r28[r29]
            int[] r29 = ua.netlizard.egypt.m3d.s
            r30 = 1
            r29 = r29[r30]
            r0 = r29
            r1 = r28
            r1.pos_y = r0
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r29 = 0
            r28 = r28[r29]
            int[] r29 = ua.netlizard.egypt.m3d.s
            r30 = 2
            r29 = r29[r30]
            r0 = r29
            r1 = r28
            r1.pos_z = r0
            r21 = 0
        L_0x024b:
            short r28 = ua.netlizard.egypt.m3d.rooms
            r0 = r21
            r1 = r28
            if (r0 < r1) goto L_0x0d2f
            r12 = 0
        L_0x0254:
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r0 = r28
            int r0 = r0.length
            r28 = r0
            r0 = r28
            if (r12 < r0) goto L_0x0ed4
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r28 = ua.netlizard.egypt.math.to_short(r28, r29)
            r0 = r28
            byte r11 = (byte) r0
            ua.netlizard.egypt.m3d.light1 = r11
            int r28 = r11 * 3
            r0 = r28
            short[] r0 = new short[r0]
            r28 = r0
            ua.netlizard.egypt.m3d.lights = r28
            r12 = 0
        L_0x027b:
            if (r12 < r11) goto L_0x0ef0
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r11 = ua.netlizard.egypt.math.to_short(r28, r29)
            r28 = 0
            ua.netlizard.egypt.m3d.room_now = r28
            short r28 = ua.netlizard.egypt.m3d.units
            int r12 = r28 + -1
        L_0x0291:
            if (r12 >= 0) goto L_0x0f3d
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r29 = 0
            r28 = r28[r29]
            int r29 = ua.netlizard.egypt.m3d.room_now
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.room_now = r0
            r16 = 0
            byte[] r16 = (byte[]) r16
            goto L_0x0015
        L_0x02ac:
            ua.netlizard.egypt.Uni r28 = ua.netlizard.egypt.Uni.uni
            java.lang.StringBuilder r29 = new java.lang.StringBuilder
            java.lang.String r30 = "dm"
            r29.<init>(r30)
            r0 = r29
            r1 = r34
            java.lang.StringBuilder r29 = r0.append(r1)
            java.lang.String r29 = r29.toString()
            r30 = -1
            byte[] r16 = r28.load_pack(r29, r30)
            goto L_0x0036
        L_0x02c9:
            int r13 = r13 + 2
            byte[] r28 = ua.netlizard.egypt.m3d.quit_rooms
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            r0 = r29
            byte r0 = (byte) r0
            r29 = r0
            r28[r12] = r29
            int r12 = r12 + 1
            goto L_0x004e
        L_0x02e2:
            int r28 = ua.netlizard.egypt.m3d.rot_z
            r0 = r28
            int r0 = 360 - r0
            r28 = r0
            ua.netlizard.egypt.m3d.rot_z = r28
            goto L_0x00db
        L_0x02ee:
            int r28 = ua.netlizard.egypt.m3d.rot_x
            r0 = r28
            int r0 = 270 - r0
            r28 = r0
            ua.netlizard.egypt.m3d.rot_x = r28
            goto L_0x00f7
        L_0x02fa:
            int r13 = r13 + 2
            int[] r28 = ua.netlizard.egypt.m3d.NORMALS
            r29 = 65536(0x10000, float:9.18355E-41)
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r29 = r29 * r30
            r0 = r29
            long r0 = (long) r0
            r29 = r0
            r31 = 10000(0x2710, double:4.9407E-320)
            long r29 = r29 / r31
            r0 = r29
            int r0 = (int) r0
            r29 = r0
            r28[r12] = r29
            int r13 = r13 + 2
            int[] r28 = ua.netlizard.egypt.m3d.NORMALS
            int r29 = r12 + 1
            r30 = 65536(0x10000, float:9.18355E-41)
            byte r31 = r16[r13]
            int r32 = r13 + 1
            byte r32 = r16[r32]
            int r31 = ua.netlizard.egypt.math.to_short(r31, r32)
            int r30 = r30 * r31
            r0 = r30
            long r0 = (long) r0
            r30 = r0
            r32 = 10000(0x2710, double:4.9407E-320)
            long r30 = r30 / r32
            r0 = r30
            int r0 = (int) r0
            r30 = r0
            r28[r29] = r30
            int r13 = r13 + 2
            int[] r28 = ua.netlizard.egypt.m3d.NORMALS
            int r29 = r12 + 2
            r30 = 65536(0x10000, float:9.18355E-41)
            byte r31 = r16[r13]
            int r32 = r13 + 1
            byte r32 = r16[r32]
            int r31 = ua.netlizard.egypt.math.to_short(r31, r32)
            int r30 = r30 * r31
            r0 = r30
            long r0 = (long) r0
            r30 = r0
            r32 = 10000(0x2710, double:4.9407E-320)
            long r30 = r30 / r32
            r0 = r30
            int r0 = (int) r0
            r30 = r0
            r28[r29] = r30
            int r12 = r12 + 3
            goto L_0x012b
        L_0x0368:
            int r13 = r13 + 2
            short[] r28 = ua.netlizard.egypt.m3d.text_coords
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 * 64
            r0 = r29
            long r0 = (long) r0
            r29 = r0
            r31 = 100
            long r29 = r29 / r31
            r0 = r29
            int r0 = (int) r0
            r29 = r0
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r28[r12] = r29
            int r13 = r13 + 2
            short[] r28 = ua.netlizard.egypt.m3d.text_coords
            int r29 = r12 + 1
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 * 64
            r0 = r30
            long r0 = (long) r0
            r30 = r0
            r32 = 100
            long r30 = r30 / r32
            r0 = r30
            int r0 = (int) r0
            r30 = r0
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r12 = r12 + 2
            goto L_0x0140
        L_0x03b8:
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            ua.netlizard.egypt.room r29 = new ua.netlizard.egypt.room
            r29.<init>()
            r28[r12] = r29
            int r12 = r12 + 1
            goto L_0x0157
        L_0x03c5:
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.max_x = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.max_y = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.max_z = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.min_x = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.min_y = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.min_z = r0
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r7 = ua.netlizard.egypt.math.to_short(r28, r29)
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r8 = ua.netlizard.egypt.math.to_short(r28, r29)
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            int r29 = r7 * 3
            r0 = r29
            short[] r0 = new short[r0]
            r29 = r0
            r0 = r29
            r1 = r28
            r1.Points = r0
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            r0.points = r7
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            int r29 = r8 * 7
            r0 = r29
            short[] r0 = new short[r0]
            r29 = r0
            r0 = r29
            r1 = r28
            r1.Poly = r0
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            r0.polys = r8
            r12 = 0
        L_0x04a2:
            if (r12 < r7) goto L_0x0527
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r4 = r0.Poly
            r18 = 0
            r12 = 0
        L_0x04af:
            if (r12 < r8) goto L_0x058c
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r6 = ua.netlizard.egypt.math.to_short(r28, r29)
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            int r29 = r6 * 2
            r0 = r29
            int[] r0 = new int[r0]
            r29 = r0
            r0 = r29
            r1 = r28
            r1.place = r0
            r12 = 0
        L_0x04d0:
            if (r12 < r6) goto L_0x05f3
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            ua.netlizard.egypt.room[] r29 = ua.netlizard.egypt.m3d.Rooms
            r29 = r29[r21]
            r0 = r29
            int r0 = r0.max_x
            r29 = r0
            ua.netlizard.egypt.room[] r30 = ua.netlizard.egypt.m3d.Rooms
            r30 = r30[r21]
            r0 = r30
            int r0 = r0.min_x
            r30 = r0
            int r29 = r29 + r30
            int r29 = r29 >> 1
            r0 = r29
            r1 = r28
            r1.cen_x = r0
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            ua.netlizard.egypt.room[] r29 = ua.netlizard.egypt.m3d.Rooms
            r29 = r29[r21]
            r0 = r29
            int r0 = r0.max_y
            r29 = r0
            ua.netlizard.egypt.room[] r30 = ua.netlizard.egypt.m3d.Rooms
            r30 = r30[r21]
            r0 = r30
            int r0 = r0.min_y
            r30 = r0
            int r29 = r29 + r30
            int r29 = r29 >> 1
            r0 = r29
            r1 = r28
            r1.cen_y = r0
            r12 = 0
        L_0x0517:
            if (r12 < r8) goto L_0x0633
            int r13 = r13 + 1
            r12 = 0
        L_0x051c:
            if (r12 < r8) goto L_0x069c
            r12 = 0
        L_0x051f:
            if (r12 < r8) goto L_0x06e9
            int r13 = r13 + -1
            int r21 = r21 + 1
            goto L_0x015f
        L_0x0527:
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Points
            r28 = r0
            int r29 = r12 * 3
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Points
            r28 = r0
            int r29 = r12 * 3
            int r29 = r29 + 1
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Points
            r28 = r0
            int r29 = r12 * 3
            int r29 = r29 + 2
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r12 = r12 + 1
            goto L_0x04a2
        L_0x058c:
            int r18 = r12 * 7
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Poly
            r28 = r0
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 * 3
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r28[r18] = r29
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Poly
            r28 = r0
            int r29 = r18 + 1
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 * 3
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Poly
            r28 = r0
            int r29 = r18 + 2
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 * 3
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r12 = r12 + 1
            goto L_0x04af
        L_0x05f3:
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            int[] r0 = r0.place
            r28 = r0
            int r29 = r12 * 2
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 * 3
            r28[r29] = r30
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            int[] r0 = r0.place
            r28 = r0
            int r29 = r12 * 2
            int r29 = r29 + 1
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 * 7
            short r30 = r4[r30]
            r28[r29] = r30
            int r12 = r12 + 1
            goto L_0x04d0
        L_0x0633:
            int r18 = r12 * 7
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Poly
            r28 = r0
            int r29 = r18 + 3
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 * 2
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Poly
            r28 = r0
            int r29 = r18 + 4
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 * 2
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r13 = r13 + 2
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Poly
            r28 = r0
            int r29 = r18 + 5
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            int r30 = r30 * 2
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r12 = r12 + 1
            goto L_0x0517
        L_0x069c:
            int r13 = r13 + 1
            byte r5 = r16[r13]
            if (r5 >= 0) goto L_0x06a4
            int r5 = r5 + 256
        L_0x06a4:
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Poly
            r28 = r0
            int r29 = r12 * 7
            int r29 = r29 + 6
            short r0 = (short) r5
            r30 = r0
            r28[r29] = r30
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Poly
            r28 = r0
            int r29 = r12 * 7
            int r29 = r29 + 6
            short r28 = r28[r29]
            r29 = 50
            r0 = r28
            r1 = r29
            if (r0 >= r1) goto L_0x06e5
            boolean[] r28 = ua.netlizard.egypt.m3d.mas_texture
            ua.netlizard.egypt.room[] r29 = ua.netlizard.egypt.m3d.Rooms
            r29 = r29[r21]
            r0 = r29
            short[] r0 = r0.Poly
            r29 = r0
            int r30 = r12 * 7
            int r30 = r30 + 6
            short r29 = r29[r30]
            r30 = 1
            r28[r29] = r30
        L_0x06e5:
            int r12 = r12 + 1
            goto L_0x051c
        L_0x06e9:
            int r13 = r13 + 1
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r2 = ua.netlizard.egypt.math.to_short(r28, r29)
            r2 = r2 & 255(0xff, float:3.57E-43)
            r28 = 70
            r0 = r28
            if (r2 >= r0) goto L_0x06ff
            r2 = 70
        L_0x06ff:
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Poly
            r28 = r0
            int r29 = r12 * 7
            int r29 = r29 + 6
            short r28 = r28[r29]
            r0 = r28
            r5 = r0 & 255(0xff, float:3.57E-43)
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            r0 = r28
            short[] r0 = r0.Poly
            r28 = r0
            int r29 = r12 * 7
            int r29 = r29 + 6
            int r30 = r2 << 8
            r30 = r30 | r5
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r12 = r12 + 1
            goto L_0x051f
        L_0x0730:
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            ua.netlizard.egypt.portal r29 = new ua.netlizard.egypt.portal
            r29.<init>()
            r28[r12] = r29
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r12]
            r0 = r28
            ua.netlizard.egypt.point_[] r0 = r0.p
            r28 = r0
            r29 = 0
            ua.netlizard.egypt.point_ r30 = new ua.netlizard.egypt.point_
            r30.<init>()
            r28[r29] = r30
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r12]
            r0 = r28
            ua.netlizard.egypt.point_[] r0 = r0.p
            r28 = r0
            r29 = 1
            ua.netlizard.egypt.point_ r30 = new ua.netlizard.egypt.point_
            r30.<init>()
            r28[r29] = r30
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r12]
            r0 = r28
            ua.netlizard.egypt.point_[] r0 = r0.p
            r28 = r0
            r29 = 2
            ua.netlizard.egypt.point_ r30 = new ua.netlizard.egypt.point_
            r30.<init>()
            r28[r29] = r30
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r12]
            r0 = r28
            ua.netlizard.egypt.point_[] r0 = r0.p
            r28 = r0
            r29 = 3
            ua.netlizard.egypt.point_ r30 = new ua.netlizard.egypt.point_
            r30.<init>()
            r28[r29] = r30
            int r12 = r12 + 1
            goto L_0x017a
        L_0x0789:
            r12 = 0
        L_0x078a:
            r28 = 4
            r0 = r28
            if (r12 < r0) goto L_0x085e
            int r13 = r13 + 2
            r28 = 65536(0x10000, float:9.18355E-41)
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r28 = r28 * r29
            r0 = r28
            long r0 = (long) r0
            r28 = r0
            r30 = 10000(0x2710, double:4.9407E-320)
            long r28 = r28 / r30
            r0 = r28
            int r0 = (int) r0
            r25 = r0
            int r13 = r13 + 2
            r28 = 65536(0x10000, float:9.18355E-41)
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r28 = r28 * r29
            r0 = r28
            long r0 = (long) r0
            r28 = r0
            r30 = 10000(0x2710, double:4.9407E-320)
            long r28 = r28 / r30
            r0 = r28
            int r0 = (int) r0
            r26 = r0
            int r13 = r13 + 2
            r28 = 65536(0x10000, float:9.18355E-41)
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r28 = r28 * r29
            r0 = r28
            long r0 = (long) r0
            r28 = r0
            r30 = 10000(0x2710, double:4.9407E-320)
            long r28 = r28 / r30
            r0 = r28
            int r0 = (int) r0
            r27 = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 + -1
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.room1 = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 + -1
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.room2 = r0
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            r0 = r28
            short r0 = r0.room1
            r28 = r0
            r29 = 999(0x3e7, float:1.4E-42)
            r0 = r28
            r1 = r29
            if (r0 == r1) goto L_0x0848
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            r0 = r28
            short r0 = r0.room2
            r28 = r0
            r29 = 999(0x3e7, float:1.4E-42)
            r0 = r28
            r1 = r29
            if (r0 != r1) goto L_0x084c
        L_0x0848:
            r28 = 1
            ua.netlizard.egypt.m3d.sky_load = r28
        L_0x084c:
            if (r25 == 0) goto L_0x08c2
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            r29 = 1
            r0 = r29
            r1 = r28
            r1.norm = r0
        L_0x085a:
            int r21 = r21 + 1
            goto L_0x017e
        L_0x085e:
            int r13 = r13 + 2
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            r0 = r28
            ua.netlizard.egypt.point_[] r0 = r0.p
            r28 = r0
            r28 = r28[r12]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.x = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            r0 = r28
            ua.netlizard.egypt.point_[] r0 = r0.p
            r28 = r0
            r28 = r28[r12]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.y = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            r0 = r28
            ua.netlizard.egypt.point_[] r0 = r0.p
            r28 = r0
            r28 = r28[r12]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.z = r0
            int r12 = r12 + 1
            goto L_0x078a
        L_0x08c2:
            if (r26 == 0) goto L_0x08d1
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            r29 = 2
            r0 = r29
            r1 = r28
            r1.norm = r0
            goto L_0x085a
        L_0x08d1:
            if (r27 == 0) goto L_0x085a
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            r29 = 3
            r0 = r29
            r1 = r28
            r1.norm = r0
            goto L_0x085a
        L_0x08e1:
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            r0 = r28
            short r0 = r0.room1
            r28 = r0
            r29 = 999(0x3e7, float:1.4E-42)
            r0 = r28
            r1 = r29
            if (r0 >= r1) goto L_0x093e
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            ua.netlizard.egypt.portal[] r29 = ua.netlizard.egypt.m3d.Portals
            r29 = r29[r21]
            r0 = r29
            short r0 = r0.room1
            r29 = r0
            r28 = r28[r29]
            r0 = r28
            byte[] r0 = r0.portal
            r28 = r0
            ua.netlizard.egypt.room[] r29 = ua.netlizard.egypt.m3d.Rooms
            ua.netlizard.egypt.portal[] r30 = ua.netlizard.egypt.m3d.Portals
            r30 = r30[r21]
            r0 = r30
            short r0 = r0.room1
            r30 = r0
            r29 = r29[r30]
            r0 = r29
            int r0 = r0.portals
            r29 = r0
            r0 = r21
            byte r0 = (byte) r0
            r30 = r0
            r28[r29] = r30
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            ua.netlizard.egypt.portal[] r29 = ua.netlizard.egypt.m3d.Portals
            r29 = r29[r21]
            r0 = r29
            short r0 = r0.room1
            r29 = r0
            r28 = r28[r29]
            r0 = r28
            int r0 = r0.portals
            r29 = r0
            int r29 = r29 + 1
            r0 = r29
            r1 = r28
            r1.portals = r0
        L_0x093e:
            ua.netlizard.egypt.portal[] r28 = ua.netlizard.egypt.m3d.Portals
            r28 = r28[r21]
            r0 = r28
            short r0 = r0.room2
            r28 = r0
            r29 = 999(0x3e7, float:1.4E-42)
            r0 = r28
            r1 = r29
            if (r0 >= r1) goto L_0x099b
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            ua.netlizard.egypt.portal[] r29 = ua.netlizard.egypt.m3d.Portals
            r29 = r29[r21]
            r0 = r29
            short r0 = r0.room2
            r29 = r0
            r28 = r28[r29]
            r0 = r28
            byte[] r0 = r0.portal
            r28 = r0
            ua.netlizard.egypt.room[] r29 = ua.netlizard.egypt.m3d.Rooms
            ua.netlizard.egypt.portal[] r30 = ua.netlizard.egypt.m3d.Portals
            r30 = r30[r21]
            r0 = r30
            short r0 = r0.room2
            r30 = r0
            r29 = r29[r30]
            r0 = r29
            int r0 = r0.portals
            r29 = r0
            r0 = r21
            byte r0 = (byte) r0
            r30 = r0
            r28[r29] = r30
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            ua.netlizard.egypt.portal[] r29 = ua.netlizard.egypt.m3d.Portals
            r29 = r29[r21]
            r0 = r29
            short r0 = r0.room2
            r29 = r0
            r28 = r28[r29]
            r0 = r28
            int r0 = r0.portals
            r29 = r0
            int r29 = r29 + 1
            r0 = r29
            r1 = r28
            r1.portals = r0
        L_0x099b:
            int r21 = r21 + 1
            goto L_0x0184
        L_0x099f:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            ua.netlizard.egypt.object r29 = new ua.netlizard.egypt.object
            r29.<init>()
            r28[r12] = r29
            int r12 = r12 + 1
            goto L_0x01b0
        L_0x09ac:
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r17 = ua.netlizard.egypt.math.to_short(r28, r29)
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            short r0 = (short) r14
            r29 = r0
            r0 = r29
            r1 = r28
            r1.start_obj = r0
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r21]
            int r29 = r14 + r17
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.end_obj = r0
            r12 = 0
        L_0x09d7:
            r0 = r17
            if (r12 < r0) goto L_0x09df
            int r21 = r21 + 1
            goto L_0x01b9
        L_0x09df:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r21
            short r0 = (short) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.room_now = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.pos_x = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.pos_y = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.pos_z = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            r0 = r29
            r1 = r28
            r1.r_x = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.r_z = r0
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            int r0 = r0.r_z
            r28 = r0
            r29 = 360(0x168, float:5.04E-43)
            r0 = r28
            r1 = r29
            if (r0 <= r1) goto L_0x0a91
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r29 = 65536(0x10000, float:9.18355E-41)
            ua.netlizard.egypt.object[] r30 = ua.netlizard.egypt.m3d.Objects
            r30 = r30[r14]
            r0 = r30
            int r0 = r0.r_z
            r30 = r0
            int r29 = r29 - r30
            r0 = r29
            r1 = r28
            r1.r_z = r0
        L_0x0a91:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            int r0 = r0.r_x
            r28 = r0
            r29 = 360(0x168, float:5.04E-43)
            r0 = r28
            r1 = r29
            if (r0 <= r1) goto L_0x0abb
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r29 = 65536(0x10000, float:9.18355E-41)
            ua.netlizard.egypt.object[] r30 = ua.netlizard.egypt.m3d.Objects
            r30 = r30[r14]
            r0 = r30
            int r0 = r0.r_x
            r30 = r0
            int r29 = r29 - r30
            r0 = r29
            r1 = r28
            r1.r_x = r0
        L_0x0abb:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            int r0 = r0.r_z
            r28 = r0
            if (r28 >= 0) goto L_0x0ae1
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            ua.netlizard.egypt.object[] r29 = ua.netlizard.egypt.m3d.Objects
            r29 = r29[r14]
            r0 = r29
            int r0 = r0.r_z
            r29 = r0
            r0 = r29
            int r0 = r0 + 360
            r29 = r0
            r0 = r29
            r1 = r28
            r1.r_z = r0
        L_0x0ae1:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            int r0 = r0.r_x
            r28 = r0
            if (r28 >= 0) goto L_0x0c6b
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            ua.netlizard.egypt.object[] r29 = ua.netlizard.egypt.m3d.Objects
            r29 = r29[r14]
            r0 = r29
            int r0 = r0.r_x
            r29 = r0
            r0 = r29
            int r0 = r0 + 360
            r29 = r0
            r0 = r29
            r1 = r28
            r1.r_x = r0
        L_0x0b07:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            int r0 = r0.r_x
            r28 = r0
            r29 = 360(0x168, float:5.04E-43)
            r0 = r28
            r1 = r29
            if (r0 != r1) goto L_0x0b25
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r29 = 0
            r0 = r29
            r1 = r28
            r1.r_x = r0
        L_0x0b25:
            int r13 = r13 + 2
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            r0 = r29
            byte r0 = (byte) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.type_object = r0
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            byte r0 = r0.type_object
            r28 = r0
            r29 = 50
            r0 = r28
            r1 = r29
            if (r0 <= r1) goto L_0x0b75
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            byte r0 = r0.type_object
            r29 = r0
            int r29 = r29 + -50
            r0 = r29
            byte r0 = (byte) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.type_object = r0
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r29 = 0
            r0 = r29
            r1 = r28
            r1.visible = r0
        L_0x0b75:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            byte r0 = r0.type_object
            r28 = r0
            r29 = 20
            r0 = r28
            r1 = r29
            if (r0 != r1) goto L_0x0ba8
            byte[] r28 = ua.netlizard.egypt.m3d.open_rooms
            r29 = 1
            r28[r21] = r29
            short[] r28 = ua.netlizard.egypt.m3d.open_doors_cof
            ua.netlizard.egypt.object[] r29 = ua.netlizard.egypt.m3d.Objects
            r29 = r29[r14]
            r0 = r29
            int r0 = r0.pos_z
            r29 = r0
            int r29 = r29 >> 16
            r0 = r29
            int r0 = r0 + 300
            r29 = r0
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r28[r14] = r29
        L_0x0ba8:
            boolean[] r28 = ua.netlizard.egypt.m3d.mas_object
            ua.netlizard.egypt.object[] r29 = ua.netlizard.egypt.m3d.Objects
            r29 = r29[r14]
            r0 = r29
            byte r0 = r0.type_object
            r29 = r0
            r30 = 1
            r28[r29] = r30
            int r13 = r13 + 2
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.light = r0
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            byte r0 = r0.type_object
            r28 = r0
            r29 = 14
            r0 = r28
            r1 = r29
            if (r0 == r1) goto L_0x0bf7
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            byte r0 = r0.type_object
            r28 = r0
            r29 = 15
            r0 = r28
            r1 = r29
            if (r0 != r1) goto L_0x0c03
        L_0x0bf7:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r29 = 255(0xff, float:3.57E-43)
            r0 = r29
            r1 = r28
            r1.light = r0
        L_0x0c03:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            short r0 = r0.light
            r28 = r0
            if (r28 >= 0) goto L_0x0c2a
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            short r0 = r0.light
            r29 = r0
            r0 = r29
            int r0 = r0 + 256
            r29 = r0
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.light = r0
        L_0x0c2a:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            short r0 = r0.light
            r29 = r0
            r0 = r29
            r0 = r0 & 255(0xff, float:3.57E-43)
            r29 = r0
            r0 = r29
            short r0 = (short) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.light = r0
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r0 = r28
            short r0 = r0.light
            r28 = r0
            r29 = 70
            r0 = r28
            r1 = r29
            if (r0 >= r1) goto L_0x0c63
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            r29 = 70
            r0 = r29
            r1 = r28
            r1.light = r0
        L_0x0c63:
            int r13 = r13 + 2
            int r14 = r14 + 1
            int r12 = r12 + 1
            goto L_0x09d7
        L_0x0c6b:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r14]
            ua.netlizard.egypt.object[] r29 = ua.netlizard.egypt.m3d.Objects
            r29 = r29[r14]
            r0 = r29
            int r0 = r0.r_x
            r29 = r0
            r0 = r29
            int r0 = 360 - r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.r_x = r0
            goto L_0x0b07
        L_0x0c87:
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r12]
            r0 = r28
            byte r0 = r0.type_object
            r28 = r0
            r29 = 32
            r0 = r28
            r1 = r29
            if (r0 <= r1) goto L_0x0cc3
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r12]
            r0 = r28
            byte r0 = r0.type_object
            r28 = r0
            r29 = 37
            r0 = r28
            r1 = r29
            if (r0 >= r1) goto L_0x0cc3
            ua.netlizard.egypt.object[] r28 = ua.netlizard.egypt.m3d.Objects
            r28 = r28[r12]
            r0 = r28
            short r0 = r0.room_now
            r28 = r0
            r0 = r28
            byte r0 = (byte) r0
            r28 = r0
            ua.netlizard.egypt.m3d.room_with_sky_box = r28
            byte r0 = (byte) r12
            r28 = r0
            ua.netlizard.egypt.m3d.object_sky = r28
            goto L_0x01d0
        L_0x0cc3:
            int r12 = r12 + 1
            goto L_0x01ca
        L_0x0cc7:
            r15 = 0
            byte[] r15 = (byte[]) r15
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r23]
            r0 = r28
            byte[] r15 = r0.portal
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r23]
            r29 = 0
            r0 = r29
            r1 = r28
            r1.portal = r0
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r23]
            r0 = r28
            int r0 = r0.portals
            r19 = r0
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r23]
            r0 = r19
            byte[] r0 = new byte[r0]
            r29 = r0
            r0 = r29
            r1 = r28
            r1.portal = r0
            r20 = 0
        L_0x0cfa:
            r0 = r20
            r1 = r19
            if (r0 < r1) goto L_0x0d04
            int r23 = r23 + 1
            goto L_0x01d2
        L_0x0d04:
            ua.netlizard.egypt.room[] r28 = ua.netlizard.egypt.m3d.Rooms
            r28 = r28[r23]
            r0 = r28
            byte[] r0 = r0.portal
            r28 = r0
            byte r29 = r15[r20]
            r28[r20] = r29
            int r20 = r20 + 1
            goto L_0x0cfa
        L_0x0d15:
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            ua.netlizard.egypt.unit r29 = new ua.netlizard.egypt.unit
            r29.<init>()
            r28[r12] = r29
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r12]
            byte r0 = (byte) r12
            r29 = r0
            r0 = r29
            r1 = r28
            r1.index = r0
            int r12 = r12 + 1
            goto L_0x020c
        L_0x0d2f:
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r24 = ua.netlizard.egypt.math.to_short(r28, r29)
            r12 = 0
        L_0x0d3c:
            r0 = r24
            if (r12 < r0) goto L_0x0d44
            int r21 = r21 + 1
            goto L_0x024b
        L_0x0d44:
            int r13 = r13 + 2
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.pos_x = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.pos_y = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            int r29 = r29 << 16
            r0 = r29
            r1 = r28
            r1.pos_z = r0
            int r13 = r13 + 2
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            byte r29 = r16[r13]
            int r30 = r13 + 1
            byte r30 = r16[r30]
            int r29 = ua.netlizard.egypt.math.to_short(r29, r30)
            r0 = r29
            byte r0 = (byte) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.type_unit = r0
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r0 = r28
            byte r11 = r0.type_unit
            r28 = 30
            r0 = r28
            if (r11 <= r0) goto L_0x0e94
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r29 = 1
            r0 = r29
            r1 = r28
            r1.take = r0
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r29 = 0
            r0 = r29
            r1 = r28
            r1.visible = r0
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r29 = 1
            r0 = r29
            r1 = r28
            r1.death = r0
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r29 = 0
            r0 = r29
            r1 = r28
            r1.life = r0
            int r11 = r11 + -30
        L_0x0de7:
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            byte r0 = (byte) r11
            r29 = r0
            r0 = r29
            r1 = r28
            r1.type_unit = r0
            boolean[] r28 = ua.netlizard.egypt.m3d.mas_unit
            r29 = 1
            r28[r11] = r29
            int r13 = r13 + 2
            byte r28 = r16[r13]
            int r29 = r13 + 1
            byte r29 = r16[r29]
            int r22 = ua.netlizard.egypt.math.to_short(r28, r29)
            r28 = 360(0x168, float:5.04E-43)
            r0 = r22
            r1 = r28
            if (r0 <= r1) goto L_0x0e12
            r28 = 65536(0x10000, float:9.18355E-41)
            int r22 = r22 - r28
        L_0x0e12:
            if (r22 >= 0) goto L_0x0ecc
            r0 = r22
            int r0 = -r0
            r22 = r0
        L_0x0e19:
            r28 = 360(0x168, float:5.04E-43)
            r0 = r22
            r1 = r28
            if (r0 != r1) goto L_0x0e23
            r22 = 0
        L_0x0e23:
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r0 = r28
            int[] r0 = r0.rr
            r28 = r0
            r29 = 0
            int[] r30 = ua.netlizard.egypt.math.sin
            r30 = r30[r22]
            r0 = r30
            int r0 = -r0
            r30 = r0
            r28[r29] = r30
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r0 = r28
            int[] r0 = r0.rr
            r28 = r0
            r29 = 1
            int[] r30 = ua.netlizard.egypt.math.cos
            r30 = r30[r22]
            r0 = r30
            int r0 = -r0
            r30 = r0
            r28[r29] = r30
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r0 = r28
            int[] r0 = r0.v_target
            r28 = r0
            r29 = 0
            int[] r30 = ua.netlizard.egypt.math.sin
            r30 = r30[r22]
            r0 = r30
            int r0 = -r0
            r30 = r0
            r28[r29] = r30
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r0 = r28
            int[] r0 = r0.v_target
            r28 = r0
            r29 = 1
            int[] r30 = ua.netlizard.egypt.math.cos
            r30 = r30[r22]
            r0 = r30
            int r0 = -r0
            r30 = r0
            r28[r29] = r30
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r0 = r21
            short r0 = (short) r0
            r29 = r0
            r0 = r29
            r1 = r28
            r1.room_now = r0
            int r14 = r14 + 1
            int r12 = r12 + 1
            goto L_0x0d3c
        L_0x0e94:
            r28 = 20
            r0 = r28
            if (r11 <= r0) goto L_0x0eb6
            int r11 = r11 + -20
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r29 = 1
            r0 = r29
            r1 = r28
            r1.take = r0
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r29 = 0
            r0 = r29
            r1 = r28
            r1.visible = r0
            goto L_0x0de7
        L_0x0eb6:
            r28 = 10
            r0 = r28
            if (r11 <= r0) goto L_0x0de7
            int r11 = r11 + -10
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r14]
            r29 = 1
            r0 = r29
            r1 = r28
            r1.take = r0
            goto L_0x0de7
        L_0x0ecc:
            r0 = r22
            int r0 = 360 - r0
            r22 = r0
            goto L_0x0e19
        L_0x0ed4:
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r12]
            short[] r29 = ua.netlizard.egypt.m3d.life_unit
            ua.netlizard.egypt.unit[] r30 = ua.netlizard.egypt.m3d.Units
            r30 = r30[r12]
            r0 = r30
            byte r0 = r0.type_unit
            r30 = r0
            short r29 = r29[r30]
            r0 = r29
            r1 = r28
            r1.life = r0
            int r12 = r12 + 1
            goto L_0x0254
        L_0x0ef0:
            int r13 = r13 + 2
            short[] r28 = ua.netlizard.egypt.m3d.lights
            int r29 = r12 * 3
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r13 = r13 + 2
            short[] r28 = ua.netlizard.egypt.m3d.lights
            int r29 = r12 * 3
            int r29 = r29 + 1
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r13 = r13 + 2
            short[] r28 = ua.netlizard.egypt.m3d.lights
            int r29 = r12 * 3
            int r29 = r29 + 2
            byte r30 = r16[r13]
            int r31 = r13 + 1
            byte r31 = r16[r31]
            int r30 = ua.netlizard.egypt.math.to_short(r30, r31)
            r0 = r30
            short r0 = (short) r0
            r30 = r0
            r28[r29] = r30
            int r12 = r12 + 1
            goto L_0x027b
        L_0x0f3d:
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r12]
            ua.netlizard.egypt.math.change_room(r28)
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r12]
            ua.netlizard.egypt.math.phi(r28)
            if (r12 != 0) goto L_0x0fa4
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r12]
            r0 = r28
            short r0 = r0.room_now
            r28 = r0
            ua.netlizard.egypt.m3d.room_now = r28
            ua.netlizard.egypt.unit[] r28 = ua.netlizard.egypt.m3d.Units
            r28 = r28[r12]
            r0 = r28
            int[] r0 = r0.v_target
            r28 = r0
            int r29 = ua.netlizard.egypt.m3d.rot_x
            int r30 = ua.netlizard.egypt.m3d.rot_z
            ua.netlizard.egypt.math.vec_for_me(r28, r29, r30)
            int[] r28 = ua.netlizard.egypt.m3d.s
            r29 = 0
            ua.netlizard.egypt.unit[] r30 = ua.netlizard.egypt.m3d.Units
            r31 = 0
            r30 = r30[r31]
            r0 = r30
            int r0 = r0.pos_x
            r30 = r0
            r28[r29] = r30
            int[] r28 = ua.netlizard.egypt.m3d.s
            r29 = 1
            ua.netlizard.egypt.unit[] r30 = ua.netlizard.egypt.m3d.Units
            r31 = 0
            r30 = r30[r31]
            r0 = r30
            int r0 = r0.pos_y
            r30 = r0
            r28[r29] = r30
            int[] r28 = ua.netlizard.egypt.m3d.s
            r29 = 2
            ua.netlizard.egypt.unit[] r30 = ua.netlizard.egypt.m3d.Units
            r31 = 0
            r30 = r30[r31]
            r0 = r30
            int r0 = r0.pos_z
            r30 = r0
            r31 = 2293760(0x230000, float:3.214242E-39)
            int r30 = r30 + r31
            r28[r29] = r30
        L_0x0fa4:
            int r12 = r12 + -1
            goto L_0x0291
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.m3d.load_level(int):void");
    }

    public static final void delete_level() {
        quit_rooms = null;
        text_coords = null;
        open_rooms = null;
        open_doors_cof = null;
        NORMALS = null;
        Rooms = null;
        lights = null;
    }

    public static final void load_mas() {
        mas_texture = new boolean[51];
        mas_object = new boolean[50];
        mas_unit = new boolean[8];
    }

    public static final void delete_mas() {
        mas_texture = null;
        mas_object = null;
        mas_unit = null;
    }

    public static final void load_FPS_weapon() {
        FPS_weapon = new byte[9][];
        FPS_palitra = new int[9][];
        weapon_size = (short[][]) Array.newInstance(Short.TYPE, 27, 3);
        for (int i = 0; i < 9; i++) {
            byte[] buffer = Uni.uni.load_pack("fp" + i, -1);
            FPS_palitra[i] = BIP.load_bip_palitra(buffer);
            FPS_weapon[i] = BIP.load_bip_points(buffer, false, 0, 0, 0, 0);
            weapon_size[i][0] = (short) BIP.get_(buffer, cof_W);
            weapon_size[i][1] = (short) BIP.get_(buffer, (byte) 1);
            weapon_size[i][2] = (short) BIP.get_(buffer, (byte) 2);
        }
    }

    public static final void delete_FPS_weapon() {
        FPS_weapon = null;
        FPS_palitra = null;
        weapon_size = null;
    }

    public static final void load_texture(int level) {
        all_texture = cof_W;
        texture = new byte[51][];
        palitra = new int[51][];
        for (int i = 0; i < 51; i++) {
            if (mas_texture[i]) {
                all_texture = (byte) (all_texture + 1);
                try {
                    byte[] buffer = Uni.uni.load_pack("w/w" + i, -1);
                    palitra[i] = BIP.load_bip_palitra(buffer);
                    texture[i] = BIP.load_bip_points(buffer, false, 0, 0, 0, 0);
                    if (i > 6) {
                        BIP.invert_texture(texture[i], 6);
                    } else {
                        BIP.invert_texture(texture[i], 7);
                    }
                    byte[] buffer2 = null;
                } catch (Exception e) {
                }
            }
        }
        if (level != 0) {
            texture_object = new byte[texture_max_uni][];
            byte[] buffer3 = Uni.uni.load_pack("/u0", -1);
            int cvo1 = load_cu();
            palitra_object = BIP.load_bip_palitra(buffer3);
            for (int i2 = 0; i2 < cvo1; i2++) {
                texture_object[i2] = BIP.load_bip_points(buffer3, true, cu[i2][0], cu[i2][1], cu[i2][2], cu[i2][3]);
            }
            Menu.load_fire();
        }
    }

    static final void start_new() {
        Game.vibro_enable = true;
        Game.open_level = 0;
        Game.first_load = false;
        Snd2.volume = 60;
        Game.my_level = 1;
        Game.sens = cof_W;
        int index = 330;
        int len = "Net Lizard".length();
        for (int j = 0; j < 5; j++) {
            for (int i = 0; i < len; i++) {
                int tmp = "Net Lizard".charAt(i);
                if (tmp >= 848) {
                    tmp -= 848;
                }
                Uni.buffer2[index + i] = (byte) tmp;
            }
            index += 12;
        }
        Game.accel_on = true;
        save_rm(false, true);
    }

    static final void load_rm(boolean restart, boolean start) {
        Uni.rmsRead = true;
        Uni.fl_read();
        if (Game.first_load) {
            start_new();
            return;
        }
        if (start) {
            Snd2.volume = Uni.buffer2[0];
            if (Uni.buffer2[1] == 1) {
                Game.accel_on = true;
            } else {
                Game.accel_on = false;
            }
            if (Uni.buffer2[2] == 1) {
                Game.inert = false;
            } else {
                Game.inert = true;
            }
            Game.sens = Uni.buffer2[11];
            if (Uni.buffer2[13] == 0) {
                Game.first_take = true;
            } else {
                Game.first_take = false;
            }
            if (Uni.buffer2[16] == 1) {
                Game.vibro_enable = true;
            } else {
                Game.vibro_enable = false;
            }
        }
        if (!restart) {
            Game.my_level = Uni.buffer2[17];
        }
        if (Game.my_level == 0) {
            Game.my_level = 1;
        }
        AI.difficult = Uni.buffer2[stp_draw];
        Game.open_level = Uni.buffer2[19];
        int index = 20 + ((Game.my_level - 1) * stp_draw);
        int k = 1;
        AI.difficult = Uni.buffer2[index];
        int index2 = index + 1;
        for (int i = 0; i < 7; i++) {
            if ((Uni.buffer2[index2] & k) != 0) {
                slot[i] = 1;
            } else {
                slot[i] = cof_W;
            }
            k <<= 1;
        }
        int index3 = index2 + 1;
        for (int i2 = 0; i2 < 7; i2++) {
            ammo_in_mag[i2] = Uni.buffer2[index3];
            index3++;
        }
        for (int i3 = 0; i3 < 7; i3++) {
            ammo[i3] = Uni.buffer2[index3];
            index3++;
        }
        slot[0] = 1;
        slots = Uni.buffer2[index3];
        weapon_now = Uni.buffer2[index3 + 1];
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v20, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static final void save_rm(boolean r11, boolean r12) {
        /*
            r10 = 19
            r9 = 7
            r8 = 15
            r4 = 1
            r5 = 0
            if (r12 == 0) goto L_0x0053
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            int r6 = ua.netlizard.egypt.Snd2.volume
            byte r6 = (byte) r6
            r3[r5] = r6
            byte[] r6 = ua.netlizard.egypt.Uni.buffer2
            boolean r3 = ua.netlizard.egypt.Game.accel_on
            if (r3 == 0) goto L_0x00ac
            r3 = r4
        L_0x0017:
            r6[r4] = r3
            byte[] r6 = ua.netlizard.egypt.Uni.buffer2
            r7 = 2
            boolean r3 = ua.netlizard.egypt.Game.inert
            if (r3 == 0) goto L_0x00af
            r3 = r5
        L_0x0021:
            r6[r7] = r3
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            r6 = 11
            byte r7 = ua.netlizard.egypt.Game.sens
            r3[r6] = r7
            byte[] r6 = ua.netlizard.egypt.Uni.buffer2
            r7 = 12
            boolean r3 = ua.netlizard.egypt.Game.first_take
            if (r3 == 0) goto L_0x00b2
            r3 = r5
        L_0x0034:
            r6[r7] = r3
            byte[] r6 = ua.netlizard.egypt.Uni.buffer2
            r7 = 13
            boolean r3 = ua.netlizard.egypt.Game.first_load
            if (r3 == 0) goto L_0x00b4
            r3 = r5
        L_0x003f:
            r6[r7] = r3
            boolean r3 = ua.netlizard.egypt.Game.sound_enable
            if (r3 == 0) goto L_0x00b6
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            r3[r8] = r4
        L_0x0049:
            boolean r3 = ua.netlizard.egypt.Game.vibro_enable
            if (r3 == 0) goto L_0x00bb
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            r6 = 16
            r3[r6] = r4
        L_0x0053:
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            int r6 = ua.netlizard.egypt.Game.open_level
            byte r6 = (byte) r6
            r3[r10] = r6
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            byte r3 = r3[r10]
            if (r3 <= r8) goto L_0x0064
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            r3[r10] = r8
        L_0x0064:
            if (r11 == 0) goto L_0x00a6
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            r6 = 17
            byte r7 = ua.netlizard.egypt.Game.my_level
            r3[r6] = r7
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            r6 = 18
            byte r7 = ua.netlizard.egypt.AI.difficult
            r3[r6] = r7
            r1 = 20
            r2 = 1
            byte r3 = ua.netlizard.egypt.Game.my_level
            int r3 = r3 + -1
            int r3 = r3 * 18
            int r1 = r1 + r3
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            byte r6 = ua.netlizard.egypt.AI.difficult
            r3[r1] = r6
            int r1 = r1 + 1
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            r3[r1] = r5
            r0 = 0
        L_0x008d:
            if (r0 < r9) goto L_0x00c2
            int r1 = r1 + 1
            r0 = 0
        L_0x0092:
            if (r0 < r9) goto L_0x00d8
            r0 = 0
        L_0x0095:
            if (r0 < r9) goto L_0x00e5
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            byte r5 = ua.netlizard.egypt.m3d.slots
            r3[r1] = r5
            int r1 = r1 + 1
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            int r5 = ua.netlizard.egypt.m3d.weapon_now
            byte r5 = (byte) r5
            r3[r1] = r5
        L_0x00a6:
            ua.netlizard.egypt.Uni.rmsWrite = r4
            ua.netlizard.egypt.Uni.fl_write()
            return
        L_0x00ac:
            r3 = r5
            goto L_0x0017
        L_0x00af:
            r3 = r4
            goto L_0x0021
        L_0x00b2:
            r3 = r4
            goto L_0x0034
        L_0x00b4:
            r3 = r4
            goto L_0x003f
        L_0x00b6:
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            r3[r8] = r5
            goto L_0x0049
        L_0x00bb:
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            r6 = 16
            r3[r6] = r5
            goto L_0x0053
        L_0x00c2:
            byte[] r6 = ua.netlizard.egypt.Uni.buffer2
            byte r7 = r6[r1]
            byte[] r3 = ua.netlizard.egypt.m3d.slot
            byte r3 = r3[r0]
            if (r3 != 0) goto L_0x00d6
            r3 = r5
        L_0x00cd:
            r3 = r3 | r7
            byte r3 = (byte) r3
            r6[r1] = r3
            int r2 = r2 << 1
            int r0 = r0 + 1
            goto L_0x008d
        L_0x00d6:
            r3 = r2
            goto L_0x00cd
        L_0x00d8:
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            byte[] r5 = ua.netlizard.egypt.m3d.ammo_in_mag
            byte r5 = r5[r0]
            r3[r1] = r5
            int r1 = r1 + 1
            int r0 = r0 + 1
            goto L_0x0092
        L_0x00e5:
            byte[] r3 = ua.netlizard.egypt.Uni.buffer2
            byte[] r5 = ua.netlizard.egypt.m3d.ammo
            byte r5 = r5[r0]
            r3[r1] = r5
            int r1 = r1 + 1
            int r0 = r0 + 1
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.m3d.save_rm(boolean, boolean):void");
    }

    public static final void reload_ex(int a) {
        Game.shtora = 0;
        max_1 = max_shots;
        max_2 = 30;
        mhz = cof_W;
        resp = cof_W;
        dm_level = 0;
        now_weap = cof_W;
        Game.time = 0;
        next_time = 180;
        if (a != 0) {
            int b = text_read[a - 1].length >> 1;
            for (int i = 0; i < b; i++) {
                text_read[a - 1][i << 1] = 1;
            }
        }
        nuw_hit_unit = cof_W;
        obj_0 = (short) Units.length;
        spr_0 = (short) (obj_0 + objects);
        shot_0 = (short) (spr_0 + 15);
        if (a != 0) {
            scr.restart_script();
        }
        look_on = false;
        sprites = 0;
        shots = 0;
        vec_speed = new int[3];
        reload_FPS = cof_W;
        change = cof_W;
        white = 255;
        kills = cof_W;
        HP = 0;
    }

    public static final void delete_texture() {
        texture = null;
        palitra = null;
        palitra_object = null;
        texture_object = null;
        cu = null;
    }

    public static final void load_math() {
        index_sort_poly = new int[max_polys];
        help_sort_poly = new int[max_polys];
        draw_loop = new int[200];
        help_v = new point_[10];
        help_dts = new point_[10];
        for (int i = 0; i < 10; i++) {
            help_v[i] = new point_();
            help_dts[i] = new point_();
        }
        coof_clip = new int[((Height * 2) + 1)];
        for (int i2 = 1; i2 < (Height * 2) + 1; i2++) {
            coof_clip[i2] = 65536 / i2;
        }
        distance_H = new int[max_xyz];
        for (int i3 = 1; i3 < max_xyz; i3++) {
            distance_H[i3] = stp_draw_one / i3;
        }
        max_point = new int[414];
        s = new int[3];
        vec_speed = new int[3];
        shot = (int[][]) Array.newInstance(Integer.TYPE, 50, 12);
        sprite = (int[][]) Array.newInstance(Integer.TYPE, 15, 6);
        palitra_help = new int[128];
    }

    public static final void delete_math() {
        index_sort_poly = null;
        help_sort_poly = null;
        index_sort = null;
        help_sort = null;
        draw_loop = null;
        help_v = null;
        help_dts = null;
        coof_clip = null;
        distance_H = null;
        max_point = null;
        s = null;
        vec_speed = null;
        shot = null;
        sprite = null;
        auto_aim = null;
        palitra_help = null;
    }

    public static final void creat_sled_shot(int x, int y, int z, int room) {
        int[] data = help_sort;
        int[] data2 = index_sort;
        vec_speed[0] = 0;
        vec_speed[1] = 0;
        vec_speed[2] = 0;
        data2[0] = x;
        data2[1] = y;
        data2[2] = z;
        data[0] = 5;
        data[1] = room;
        data[2] = Math.abs(myRandom.nextInt() & 1) + 4;
        data[3] = 4;
        data[4] = 3;
        int room2 = Math.abs(myRandom.nextInt() & 255);
        if (room2 < 128) {
            room2 = 128;
        }
        data[5] = 16711680 | (room2 << 8);
        math.shot(data2, vec_speed, data);
    }

    public static final void creat_sled_toxic(int x, int y, int z, int room, int life) {
        int[] data = help_sort;
        int[] data2 = index_sort;
        vec_speed[0] = 0;
        vec_speed[1] = 0;
        vec_speed[2] = 0;
        data2[0] = x;
        data2[1] = y;
        data2[2] = z;
        data[0] = 4;
        data[1] = room;
        data[2] = Math.abs(myRandom.nextInt() & 1) + 4;
        data[3] = life;
        data[4] = 3;
        int room2 = Math.abs(myRandom.nextInt() & 255);
        if (room2 > 160) {
            room2 = 160;
        }
        data[5] = (room2 << 16) | 65280;
        math.shot(data2, vec_speed, data);
    }

    public static final void creat_sled_blust(int x, int y, int z, int room) {
        int[] data = help_sort;
        int[] data2 = index_sort;
        vec_speed[0] = 0;
        vec_speed[1] = 0;
        vec_speed[2] = 0;
        data2[0] = x;
        data2[1] = y;
        data2[2] = z;
        data[0] = 3;
        data[1] = room;
        data[2] = Math.abs(myRandom.nextInt() & 1) + 4;
        data[3] = 3;
        data[4] = 3;
        int room2 = Math.abs(myRandom.nextInt() & 255) + 70;
        if (room2 > 240) {
            room2 = 240;
        }
        data[5] = (room2 << 8) | 255;
        math.shot(data2, vec_speed, data);
    }

    public static final void dm_scr() {
        int time = Game.time / FullCanvas.canvasRealWidth;
        dm_level = (short) ((time / 30) % 6);
        if (time > next_time) {
            next_time = (short) (next_time + 60);
            max_1 = (byte) (max_1 - 5);
            max_2 = (byte) (max_2 - 5);
            if (max_1 < 0) {
                max_1 = cof_W;
            }
            if (max_2 < 0) {
                max_2 = cof_W;
            }
        }
        byte b = (byte) (mhz - 1);
        mhz = b;
        if (b <= 0) {
            switch (dm_level) {
                case 0:
                    mhz = max_1;
                    resp = (byte) (resp + 2);
                    if (resp > 9) {
                        resp = cof_W;
                    }
                    alive(1, resp_pauk[resp] << 16, resp_pauk[resp + 1] << 16, 3801088);
                    break;
                case 1:
                    mhz = max_2;
                    resp = (byte) (resp + 2);
                    if (resp > 9) {
                        resp = cof_W;
                    }
                    alive(1, resp_pauk[resp] << 16, resp_pauk[resp + 1] << 16, 3801088);
                    break;
                case 2:
                    mhz = max_1;
                    resp = (byte) (resp + 2);
                    if (resp > 9) {
                        resp = cof_W;
                    }
                    alive(1, resp_pauk[resp] << 16, resp_pauk[resp + 1] << 16, 3801088);
                    alive(6, resp_[(resp % 3) << 1] << 16, resp_[((resp % 3) << 1) + 1] << 16, 64225280);
                    break;
                case 3:
                    mhz = max_2;
                    resp = (byte) (resp + 2);
                    if (resp > 9) {
                        resp = cof_W;
                    }
                    alive(1, resp_pauk[resp] << 16, resp_pauk[resp + 1] << 16, 3801088);
                    alive(6, resp_[(resp % 3) << 1] << 16, resp_[((resp % 3) << 1) + 1] << 16, 64225280);
                    break;
                case 4:
                    mhz = max_1;
                    resp = (byte) (resp + 2);
                    if (resp > 9) {
                        resp = cof_W;
                    }
                    alive(1, resp_pauk[resp] << 16, resp_pauk[resp + 1] << 16, 3801088);
                    alive(6, resp_[(resp % 3) << 1] << 16, resp_[((resp % 3) << 1) + 1] << 16, 64225280);
                    alive(2, resp_[(resp % 3) << 1] << 16, resp_[((resp % 3) << 1) + 1] << 16, 64225280);
                    break;
                case 5:
                    mhz = max_2;
                    resp = (byte) (resp + 2);
                    if (resp > 9) {
                        resp = cof_W;
                    }
                    alive(1, resp_pauk[resp] << 16, resp_pauk[resp + 1] << 16, 3801088);
                    alive(6, resp_[(resp % 3) << 1] << 16, resp_[((resp % 3) << 1) + 1] << 16, 64225280);
                    alive(2, resp_[(resp % 3) << 1] << 16, resp_[((resp % 3) << 1) + 1] << 16, 64225280);
                    break;
            }
        }
        for (unit y : Units) {
            switch (y.room_now) {
                case J2MECanvas.GAME_C:
                case 12:
                    y.F[0] = 0;
                    y.F[1] = -65536;
                    break;
                case Constants.RMS_store_recordID_SubscribtionMode:
                    y.F[0] = 65536;
                    y.F[1] = 0;
                    break;
                case 16:
                    y.F[0] = -65536;
                    y.F[1] = 0;
                    break;
                case stp_draw /*18*/:
                    y.F[0] = 0;
                    y.F[1] = 65536;
                    break;
            }
        }
    }

    public static final boolean alive(int type_unit, int x, int y, int z) {
        unit[] U = Units;
        for (int i = 1; i < units; i++) {
            unit uu = U[i];
            if ((!uu.visible || uu.life < -55) && uu.type_unit == type_unit) {
                uu.death = false;
                uu.visible = true;
                uu.life = life_unit[type_unit];
                uu.type_anim = cof_W;
                uu.cadr = cof_W;
                uu.anim = 0;
                uu.animation = true;
                uu.take = false;
                uu.pos_x = x;
                uu.pos_y = y;
                uu.pos_z = z;
                vec_speed[0] = 0;
                vec_speed[1] = 0;
                vec_speed[2] = 0;
                uu.Speed[0] = 0;
                uu.Speed[1] = 0;
                uu.Speed[2] = 0;
                math.change_room(uu);
                AI.AI_goto(uu, Units[0].pos_x, Units[0].pos_y, 40, 10);
                uu.F[0] = 0;
                uu.F[1] = 0;
                return true;
            }
        }
        int i2 = 1;
        while (i2 < units) {
            unit uu2 = U[i2];
            if (uu2.type_unit != type_unit || AI.line_cross(uu2, Units[0])) {
                i2++;
            } else {
                uu2.life = life_unit[type_unit];
                uu2.pos_x = x;
                uu2.pos_y = y;
                uu2.pos_z = z;
                vec_speed[0] = 0;
                vec_speed[1] = 0;
                vec_speed[2] = 0;
                uu2.Speed[0] = 0;
                uu2.Speed[1] = 0;
                uu2.Speed[2] = 0;
                math.change_room(uu2);
                return true;
            }
        }
        return false;
    }

    public static final boolean creat_weapon(int type_object, unit U) {
        if (type_object >= 30) {
            type_object = 24;
        }
        object[] ob = Objects;
        int i = 0;
        while (i < objects) {
            if (ob[i].type_object != type_object || ob[i].visible || U.room_now == stp_draw || U.room_now == 16 || U.room_now == 13 || U.room_now == 12 || U.room_now == 11) {
                i++;
            } else {
                ob[i].visible = true;
                ob[i].pos_x = U.pos_x;
                ob[i].pos_y = U.pos_y;
                ob[i].pos_z = U.pos_z;
                ob[i].room_now = U.room_now;
                return true;
            }
        }
        return false;
    }

    public static final void creat_blood(int x, int y, int z, int room) {
        math.vec_for_me(vec_speed, Math.abs(myRandom.nextInt() % texture_max_uni) + 180, Math.abs(myRandom.nextInt() % 360));
        int[] data = help_sort;
        int[] data2 = index_sort;
        data2[0] = x;
        data2[1] = y;
        data2[2] = z;
        data[0] = 1;
        data[1] = room;
        data[2] = Math.abs(myRandom.nextInt() & 1) + 4;
        data[3] = 5;
        data[4] = 3;
        int room2 = Math.abs(myRandom.nextInt() & 255) + 70;
        if (room2 > 240) {
            room2 = 240;
        }
        data[5] = room2 << 16;
        math.shot(data2, vec_speed, data);
    }

    public static final void creat_iscru(int x, int y, int z, int room) {
        math.vec_for_me(vec_speed, Math.abs(myRandom.nextInt() % texture_max_uni) + 180, Math.abs(myRandom.nextInt() % 360));
        int[] data = help_sort;
        int[] data2 = index_sort;
        data2[0] = x;
        data2[1] = y;
        data2[2] = z;
        data[0] = 2;
        data[1] = room;
        data[2] = Math.abs(myRandom.nextInt() % 2) + 4;
        data[3] = 10;
        data[4] = 2;
        int room2 = Math.abs(myRandom.nextInt() & 255) + 100;
        if (room2 > 255) {
            room2 = 255;
        }
        data[5] = 16711680 | (room2 << 8);
        math.shot(data2, vec_speed, data);
    }

    public static final void creat_meet(int x, int y, int z, int room, boolean big) {
        math.vec_for_me(vec_speed, Math.abs(myRandom.nextInt() % texture_max_uni) + 180, Math.abs(myRandom.nextInt() % 360));
        int[] data = help_sort;
        int[] data2 = index_sort;
        data2[0] = x;
        data2[1] = y;
        data2[2] = z;
        data[0] = -2000 - Math.abs(myRandom.nextInt() % 3);
        if (big) {
            data[0] = -2002;
        }
        data[1] = room;
        data[2] = 100;
        data[3] = 400;
        data[4] = Math.abs(myRandom.nextInt() % 359);
        data[5] = 16777215;
        math.shot_2(data2, vec_speed, data);
    }

    public static final void shot_ossiris(unit U, byte os) {
        int[] data = help_sort;
        int[] data2 = index_sort;
        vec_speed[0] = U.rr[0];
        vec_speed[1] = U.rr[1];
        vec_speed[2] = 0;
        data2[0] = U.pos_x + (vec_speed[0] * 128);
        data2[1] = U.pos_y + (vec_speed[1] * 128);
        data2[2] = U.pos_z + 655360;
        data[0] = -1024;
        data[1] = Units[0].room_now;
        data[2] = (U.index << max_change) | 7;
        data[3] = 2;
        int js = 1;
        if (os == 1) {
            js = 6;
        }
        if (os == 2) {
            js = 5;
        }
        data[4] = (shot_demage_unit[js] << max_change) | 3;
        data[5] = 0;
        math.shot(data2, vec_speed, data);
    }

    public static final void shot_mutant(unit U, unit U_target, boolean left) {
        int[] data = help_sort;
        int[] data2 = index_sort;
        int a = 68;
        if (!left) {
            a = -68;
        }
        data2[0] = ((U.rr[0] * 90) - (U.rr[1] * a)) + U.pos_x;
        data2[1] = (U.rr[1] * 90) + (U.rr[0] * a) + U.pos_y;
        data2[2] = U.pos_z + 1966080;
        int x = (data2[0] - U_target.pos_x) >> 16;
        int y = (data2[1] - U_target.pos_y) >> 16;
        int z = (data2[2] - U_target.pos_z) >> 16;
        int leght = (x * x) + (y * y) + (z * z);
        int Pa0 = 0;
        int wfm = 65536;
        while (true) {
            wfm >>= 1;
            if (wfm <= 0) {
                break;
            } else if ((Pa0 + wfm) * (Pa0 + wfm) <= leght) {
                Pa0 += wfm;
            }
        }
        if (Pa0 == 0) {
            Pa0 = 1;
        }
        vec_speed[0] = ((-x) * 65536) / Pa0;
        vec_speed[1] = ((-y) * 65536) / Pa0;
        vec_speed[2] = ((-z) * 65536) / Pa0;
        data[0] = -8;
        data[1] = U.room_now;
        data[2] = (U.index << max_change) | 8;
        data[3] = 32;
        data[4] = (shot_demage_unit[2] << max_change) | 2;
        data[5] = 16776960;
        math.shot(data2, vec_speed, data);
    }

    public static final void shot_scorpion(unit U, unit U_target) {
        int[] data = help_sort;
        int[] data2 = index_sort;
        data2[0] = (U.rr[0] * 61) + U.pos_x;
        data2[1] = (U.rr[1] * 61) + U.pos_y;
        data2[2] = U.pos_z + 5373952;
        int x = (data2[0] - U_target.pos_x) >> 16;
        int y = (data2[1] - U_target.pos_y) >> 16;
        int z = (data2[2] - (U_target.pos_z + distance_portal)) >> 16;
        int leght = (x * x) + (y * y) + (z * z);
        int Pa0 = 0;
        int wfm = 65536;
        while (true) {
            wfm >>= 1;
            if (wfm <= 0) {
                break;
            } else if ((Pa0 + wfm) * (Pa0 + wfm) <= leght) {
                Pa0 += wfm;
            }
        }
        if (Pa0 == 0) {
            Pa0 = 1;
        }
        vec_speed[0] = ((-x) * 65536) / Pa0;
        vec_speed[1] = ((-y) * 65536) / Pa0;
        vec_speed[2] = ((-z) * 65536) / Pa0;
        data[0] = -2;
        data[1] = U.room_now;
        data[2] = (U.index << max_change) | 120;
        data[3] = 12;
        data[4] = 3;
        data[5] = 65280;
        math.shot_2(data2, vec_speed, data);
    }

    public static final void shot_sharic(unit U, unit U_target, boolean left) {
        int[] data = help_sort;
        int[] data2 = index_sort;
        int a = 80;
        if (!left) {
            a = -80;
        }
        data2[0] = ((U.rr[0] * 90) - (U.rr[1] * a)) + U.pos_x;
        data2[1] = (U.rr[1] * 90) + (U.rr[0] * a) + U.pos_y;
        data2[2] = U.pos_z - 2621440;
        int x = (data2[0] - U_target.pos_x) >> 16;
        int y = (data2[1] - U_target.pos_y) >> 16;
        int z = (data2[2] - U_target.pos_z) >> 16;
        int leght = (x * x) + (y * y) + (z * z);
        int Pa0 = 0;
        int wfm = 65536;
        while (true) {
            wfm >>= 1;
            if (wfm <= 0) {
                break;
            } else if ((Pa0 + wfm) * (Pa0 + wfm) <= leght) {
                Pa0 += wfm;
            }
        }
        if (Pa0 == 0) {
            Pa0 = 1;
        }
        vec_speed[0] = ((-x) * 65536) / Pa0;
        vec_speed[1] = ((-y) * 65536) / Pa0;
        vec_speed[2] = ((-z) * 65536) / Pa0;
        data[0] = -1;
        data[1] = U.room_now;
        data[2] = (U.index << max_change) | 7;
        data[3] = 54;
        data[4] = 3;
        data[5] = 255;
        math.shot(data2, vec_speed, data);
    }

    public static final void Shuts_hits() {
        int[][] shott = shot;
        point_[] point_Arr = help_dts;
        unit[] unitArr = Units;
        int[] iArr = NORMALS;
        for (int i = 49; i >= 0; i--) {
            int[] shot_ = shott[i];
            if (shot_[6] != 0) {
                shot_[6] = shot_[6] - 1;
                if (shot_[6] == 0) {
                    shots = (short) (shots - 1);
                } else if (shot_[9] <= -2000) {
                    shot_meet(shot_);
                } else {
                    type_shot(shot_);
                }
            }
        }
    }

    static void vec_g(int[] shot_) {
        int leght = shot_[8] & 255;
        int x = (shot_[3] * leght) >> 16;
        int y = (shot_[4] * leght) >> 16;
        int z = ((shot_[5] * leght) >> 16) - 8;
        int leght2 = (x * x) + (y * y) + (z * z);
        int Pa0 = 0;
        int wfm = 65536;
        while (true) {
            wfm >>= 1;
            if (wfm <= 0) {
                break;
            } else if ((Pa0 + wfm) * (Pa0 + wfm) <= leght2) {
                Pa0 += wfm;
            }
        }
        if (Pa0 == 0) {
            Pa0 = 1;
        }
        shot_[3] = (x * 65536) / Pa0;
        shot_[4] = (y * 65536) / Pa0;
        shot_[5] = (z * 65536) / Pa0;
        if (Pa0 > 120) {
            Pa0 = 120;
        }
        shot_[8] = (((shot_[8] & 65280) >> 8) << 8) | Pa0;
    }

    static void kick_body(unit u) {
        if (u.death && u.life > -600) {
            u.life = (short) (u.life - 1);
            if (u.life <= -600) {
                u.visible = false;
            }
        }
    }

    static void shot_meet(int[] shot_) {
        if (shot_[8] != 0) {
            int[] norm = NORMALS;
            int room = shot_[7];
            int speed_x = shot_[3] * shot_[8];
            int speed_y = shot_[4] * shot_[8];
            int speed_z = shot_[5] * shot_[8];
            int sx = speed_x >> 16;
            int sy = speed_y >> 16;
            int sz = speed_z >> 16;
            int speed_z2 = speed_z - 1048576;
            int leght = (sx * sx) + (sy * sy) + (sz * sz);
            int leght_speed = 0;
            int wfm = 65536;
            while (true) {
                wfm >>= 1;
                if (wfm <= 0) {
                    break;
                } else if ((leght_speed + wfm) * (leght_speed + wfm) <= leght) {
                    leght_speed += wfm;
                }
            }
            if (leght_speed > 100) {
                leght_speed = 100;
            }
            int help = coof_clip[leght_speed];
            shot_[3] = (int) ((((long) speed_x) * ((long) help)) >> 16);
            shot_[4] = (int) ((((long) speed_y) * ((long) help)) >> 16);
            shot_[5] = (int) ((((long) speed_z2) * ((long) help)) >> 16);
            if (leght_speed > 100) {
                shot_[8] = 100;
            } else {
                shot_[8] = leght_speed;
            }
            int speed_x2 = shot_[0];
            int speed_y2 = shot_[1];
            int speed_z3 = shot_[2];
            int help2 = shot_[8];
            int sx2 = shot_[0] + (shot_[3] * help2);
            int sy2 = shot_[1] + (shot_[4] * help2);
            int sz2 = shot_[2] + (shot_[5] * help2);
            room R = Rooms[room];
            int[] Place = R.place;
            short[] poin = R.Points;
            for (int j = Place.length - 2; j >= 0; j -= 2) {
                int np = Place[j];
                int nm = Place[j + 1];
                long t1 = (((long) ((sx2 >> 16) - poin[nm])) * ((long) norm[np])) + (((long) ((sy2 >> 16) - poin[nm + 1])) * ((long) norm[np + 1])) + (((long) ((sz2 >> 16) - poin[nm + 2])) * ((long) norm[np + 2]));
                if (t1 < 0) {
                    sx2 = (int) (((long) sx2) + ((-(t1 >> 16)) * ((long) norm[np])));
                    sy2 = (int) (((long) sy2) + ((-(t1 >> 16)) * ((long) norm[np + 1])));
                    sz2 = (int) (((long) sz2) + ((-(t1 >> 16)) * ((long) norm[np + 2])));
                    long t12 = ((((((long) norm[np]) * ((long) shot_[3])) + (((long) norm[np + 1]) * ((long) shot_[4]))) + (((long) norm[np + 2]) * ((long) shot_[5]))) >> 16) << 1;
                    shot_[3] = (int) (((long) shot_[3]) - ((((long) norm[np]) * t12) >> 16));
                    shot_[4] = (int) (((long) shot_[4]) - ((((long) norm[np + 1]) * t12) >> 16));
                    shot_[5] = (int) (((long) shot_[5]) - ((((long) norm[np + 2]) * t12) >> 16));
                    shot_[8] = shot_[8] >> 1;
                }
            }
            if (shot_[8] < 3) {
                type_sss(shot_);
                math.change_room_shot(shot_);
                shot_[8] = 0;
                return;
            }
            shot_[0] = sx2;
            shot_[1] = sy2;
            shot_[2] = sz2;
            if (Game.my_level > 7 && Game.my_level < 13 && !math.change_room_shot(shot_)) {
                shot_[6] = 1;
            }
        }
    }

    static void type_sss(int[] shot_) {
        int[] norm = NORMALS;
        room R = Rooms[shot_[7]];
        int[] Place = R.place;
        short[] poin = R.Points;
        for (int j = Place.length - 2; j >= 0; j -= 2) {
            int np = Place[j];
            int nm = Place[j + 1];
            long t1 = (((long) ((shot_[0] >> 16) - poin[nm])) * ((long) norm[np])) + (((long) ((shot_[1] >> 16) - poin[nm + 1])) * ((long) norm[np + 1])) + (((long) ((shot_[2] >> 16) - poin[nm + 2])) * ((long) norm[np + 2]));
            if (t1 <= 0) {
                shot_[0] = (int) (((long) shot_[0]) + (((-t1) * ((long) norm[np])) >> 16));
                shot_[1] = (int) (((long) shot_[1]) + (((-t1) * ((long) norm[np + 1])) >> 16));
                shot_[2] = (int) (((long) shot_[2]) + (((-t1) * ((long) norm[np + 2])) >> 16));
            }
        }
    }

    static void type_shot(int[] shot_) {
        int[] norm = NORMALS;
        point_[] v1 = help_dts;
        int type = shot_[9];
        if (type > 0) {
            if (type > 2) {
                if (shot_[9] == 3) {
                    shot_[2] = shot_[2] + 524288;
                }
                if (shot_[9] == 4) {
                    shot_[2] = shot_[2] - 1048576;
                    return;
                }
                return;
            }
            if (type == 2) {
                int r = ((shot_[11] & 16711680) >> 16) - 20;
                if (r < 0) {
                    r = 0;
                }
                int g = ((shot_[11] & 65280) >> 8) - 20;
                if (g < 0) {
                    g = 0;
                }
                shot_[11] = (r << 16) | (g << 8);
                shot_[5] = shot_[5] - 524288;
            }
            shot_[5] = shot_[5] - 524288;
            room R = Rooms[shot_[7]];
            int[] Place = R.place;
            short[] poin = R.Points;
            for (int j = Place.length - 2; j >= 0; j -= 2) {
                int np = Place[j];
                int nm = Place[j + 1];
                long t1 = (((long) ((shot_[0] >> 16) - poin[nm])) * ((long) norm[np])) + (((long) ((shot_[1] >> 16) - poin[nm + 1])) * ((long) norm[np + 1])) + (((long) ((shot_[2] >> 16) - poin[nm + 2])) * ((long) norm[np + 2]));
                if (t1 <= 0) {
                    shot_[0] = (int) (((long) shot_[0]) + (((-t1) * ((long) norm[np])) >> 16));
                    shot_[1] = (int) (((long) shot_[1]) + (((-t1) * ((long) norm[np + 1])) >> 16));
                    shot_[2] = (int) (((long) shot_[2]) + (((-t1) * ((long) norm[np + 2])) >> 16));
                }
            }
        }
        if (type <= 0 || type == -1024) {
            if (next_type_s(shot_)) {
                TEST_HIT_OBJ(shot_, v1);
            } else {
                return;
            }
        }
        shot_[0] = shot_[0] + shot_[3];
        shot_[1] = shot_[1] + shot_[4];
        shot_[2] = shot_[2] + shot_[5];
    }

    static final boolean next_type_s(int[] shot_) {
        int type = shot_[9];
        point_[] v1 = help_dts;
        int g = shot_[8] & 255;
        if (type == -1) {
            creat_sled_blust(shot_[0], shot_[1], shot_[2], shot_[7]);
        }
        if (type == -8) {
            creat_sled_shot(shot_[0], shot_[1], shot_[2], shot_[7]);
        }
        if (type == -2) {
            creat_sled_toxic(shot_[0], shot_[1], shot_[2], shot_[7], 5);
            point_ bb = v1[0];
            bb.x = shot_[0] >> 16;
            bb.y = shot_[1] >> 16;
            bb.z = shot_[2] >> 16;
            bb.rx = shot_[3];
            bb.ry = shot_[4];
            bb.rz = shot_[5];
            point_ bb2 = v1[1];
            bb2.x = (shot_[0] + (shot_[3] * g)) >> 16;
            bb2.y = (shot_[1] + (shot_[4] * g)) >> 16;
            bb2.z = (shot_[2] + (shot_[5] * g)) >> 16;
            point_ bb3 = v1[2];
            bb3.x = 0;
            bb3.y = 0;
            bb3.z = 0;
            TEST_HIT_OBJ(shot_, v1);
            shot_[0] = shot_[0] + (shot_[3] * g);
            shot_[1] = shot_[1] + (shot_[4] * g);
            shot_[2] = shot_[2] + (shot_[5] * g);
            for (int k = 0; k < 5; k++) {
                creat_sled_toxic(shot_[0] + ((myRandom.nextInt() & 31) << 16), shot_[1] + ((myRandom.nextInt() & 31) << 16), shot_[2] + ((myRandom.nextInt() & 31) << 16), shot_[7], 2);
            }
            vec_g(shot_);
            return false;
        }
        point_ bb4 = v1[0];
        bb4.x = shot_[0] >> 16;
        bb4.y = shot_[1] >> 16;
        bb4.z = shot_[2] >> 16;
        bb4.rx = shot_[3] >> g;
        bb4.ry = shot_[4] >> g;
        bb4.rz = shot_[5] >> g;
        point_ bb5 = v1[1];
        bb5.x = (shot_[0] + shot_[3]) >> 16;
        bb5.y = (shot_[1] + shot_[4]) >> 16;
        bb5.z = (shot_[2] + shot_[5]) >> 16;
        point_ bb6 = v1[2];
        bb6.x = 0;
        bb6.y = 0;
        bb6.z = 0;
        return true;
    }

    static void TEST_HIT_OBJ(int[] shot_, point_[] v1) {
        int aaa;
        short s2;
        boolean popali = false;
        unit[] U = Units;
        point_ bb = v1[2];
        point_ start = v1[0];
        point_ end = v1[1];
        int d = (shot_[8] & 65280) >> 8;
        int room = shot_[7];
        int portal_hit = -1;
        int kill = 0;
        if (shot_[9] == -2) {
            aaa = shot_[8] & 255;
        } else {
            aaa = 1 << (shot_[8] & 255);
        }
        while (1 != 0 && room != 999) {
            for (int j = 0; j < units; j++) {
                if (U[j].room_now == room && !U[j].death && d != j && U[j].visible) {
                    unit u = U[j];
                    bb.x = u.pos_x >> 16;
                    bb.y = u.pos_y >> 16;
                    bb.z = u.pos_z >> 16;
                    bb.z1 = aaa;
                    if (u.type_unit == 7) {
                        for (int o = 0; o < 6; o++) {
                            if (u.anim <= 9) {
                                bb.y = math.fiz_boss[0][o << 1];
                                bb.x = 0;
                                bb.z = math.fiz_boss[0][(o << 1) + 1];
                            } else if (u.anim >= 21) {
                                bb.y = math.fiz_boss[12][o << 1];
                                bb.x = 0;
                                bb.z = math.fiz_boss[12][(o << 1) + 1];
                            } else {
                                bb.y = math.fiz_boss[u.anim - 9][o << 1];
                                bb.x = 0;
                                bb.z = math.fiz_boss[u.anim - 9][(o << 1) + 1];
                            }
                            if (math.HIT_SFERA(bb, math.min_value_uni2[u.anim >= 21 ? (char) 7 : 3], start, end)) {
                                popali = true;
                                kill = j;
                            }
                        }
                    } else if (math.HIT_SFERA(bb, math.min_value_uni2[u.type_unit], start, end)) {
                        popali = true;
                        kill = j;
                    }
                }
            }
            if (popali) {
                end.x <<= 16;
                end.y <<= 16;
                end.z <<= 16;
                for (int k = 0; k < 7; k++) {
                    creat_blood(end.x, end.y, end.z, room);
                }
                if ((shot_[10] & 255) == 4) {
                    Create_sprite(end.x, end.y, end.z, cof_W, (short) room);
                }
                if (kill == 0) {
                    Create_sprite(end.x, end.y, end.z, (byte) 2, (short) room);
                }
                shots = (short) (shots - 1);
                shot_[6] = 0;
                kill_unit(U[kill], shot_, end);
                return;
            }
            start.x <<= 16;
            start.y <<= 16;
            start.z <<= 16;
            end.x <<= 16;
            end.y <<= 16;
            end.z <<= 16;
            portal_hit = math.TEST_HIT_PORTAL(room, start, end, portal_hit);
            if (portal_hit != -1) {
                if (Portals[portal_hit].room1 == room) {
                    s2 = Portals[portal_hit].room2;
                } else {
                    s2 = Portals[portal_hit].room1;
                }
                room = s2;
                start.x >>= 16;
                start.y >>= 16;
                start.z >>= 16;
                end.x >>= 16;
                end.y >>= 16;
                end.z >>= 16;
            } else {
                shot_[7] = room;
                if (math.HIT_PLANE(room, start, end) != -1) {
                    for (int k2 = 0; k2 < 7; k2++) {
                        creat_iscru(end.x, end.y, end.z, room);
                    }
                    Create_sprite(end.x, end.y, end.z, (byte) ((shot_[10] & 255) == 4 ? 0 : 1), (short) room);
                    shots = (short) (shots - 1);
                    shot_[6] = 0;
                    return;
                }
                return;
            }
        }
    }

    public static final void load_screen() {
        display = new int[(Width_real_3D * (Height_real_3D + 2))];
    }

    public static final void delete_screen() {
        display = null;
    }

    public static final void light_for_me() {
        int l = light1 * 3;
        unit u = Units[0];
        short[] li = lights;
        int me_x = Units[0].pos_x >> 16;
        int me_y = Units[0].pos_y >> 16;
        int me_z = Units[0].pos_z >> 16;
        if (fire2) {
            u.light = 255;
            return;
        }
        u.light = 0;
        int i = 0;
        while (true) {
            if (i >= l) {
                break;
            }
            int xx = me_x - li[i];
            int yy = me_y - li[i + 1];
            int zz = me_z - li[i + 2];
            int xx2 = (int) ((65536 * ((long) (((xx * xx) + (yy * yy)) + (zz * zz)))) >> 30);
            if (xx2 > 255) {
                xx2 = 255;
            }
            u.light = (short) (u.light + (255 - xx2));
            if (u.light > 255) {
                u.light = 255;
                break;
            }
            if (u.light < 20) {
                u.light = 30;
            }
            i += 3;
        }
        if (u.light < 0) {
            u.light = 0;
        }
        if (Game.my_level == 4) {
            int xx3 = me_x - (Objects[0].pos_x >> 16);
            int yy2 = me_y - (Objects[0].pos_y >> 16);
            int xx4 = (int) ((65536 * ((long) ((xx3 * xx3) + (yy2 * yy2)))) >> 28);
            if (xx4 > 255) {
                xx4 = 255;
            }
            u.light = (short) (u.light + (255 - xx4));
            if (u.light > 255) {
                u.light = 255;
            }
            if (u.light < 20) {
                u.light = 20;
            }
            int xx5 = me_x - (Objects[13].pos_x >> 16);
            int yy3 = me_y - (Objects[13].pos_y >> 16);
            int xx6 = (int) ((65536 * ((long) ((xx5 * xx5) + (yy3 * yy3)))) >> 28);
            if (xx6 > 255) {
                xx6 = 255;
            }
            u.light = (short) (u.light + (255 - xx6));
            if (u.light > 255) {
                u.light = 255;
            }
            if (u.light < 20) {
                u.light = 30;
            }
        }
    }

    /* JADX WARN: Type inference failed for: r5v26, types: [short[]] */
    /* JADX WARN: Type inference failed for: r3v9, types: [short] */
    /* JADX WARN: Type inference failed for: r5v27, types: [short[]] */
    /* JADX WARN: Type inference failed for: r4v13, types: [short] */
    /* JADX WARN: Type inference failed for: r5v28, types: [short[]] */
    /* JADX WARN: Type inference failed for: r3v10, types: [short] */
    /* JADX WARN: Type inference failed for: r5v29, types: [short[]] */
    /* JADX WARN: Type inference failed for: r4v14, types: [short] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short, code=int, for r3v10, types: [short] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short, code=int, for r3v9, types: [short] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short, code=int, for r4v13, types: [short] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short, code=int, for r4v14, types: [short] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void draw_weapon() {
        /*
            r8 = 256(0x100, float:3.59E-43)
            r7 = 360(0x168, float:5.04E-43)
            int r0 = ua.netlizard.egypt.m3d.weapon_now
            short[] r5 = ua.netlizard.egypt.m3d.smesh_weapon
            int r6 = ua.netlizard.egypt.m3d.weapon_now
            int r6 = r6 << 1
            short r3 = r5[r6]
            short[] r5 = ua.netlizard.egypt.m3d.smesh_weapon
            int r6 = ua.netlizard.egypt.m3d.weapon_now
            int r6 = r6 << 1
            int r6 = r6 + 1
            short r4 = r5[r6]
            byte r5 = ua.netlizard.egypt.m3d.smesh_x
            int r3 = r3 + r5
            byte r5 = ua.netlizard.egypt.m3d.smesh_y
            int r4 = r4 + r5
            int r5 = ua.netlizard.egypt.m3d.weapon_now
            if (r5 != 0) goto L_0x0034
            short[] r5 = ua.netlizard.egypt.m3d.anim_knife
            byte r6 = ua.netlizard.egypt.m3d.shot_delay
            int r6 = r6 << 1
            short r3 = r5[r6]
            short[] r5 = ua.netlizard.egypt.m3d.anim_knife
            byte r6 = ua.netlizard.egypt.m3d.shot_delay
            int r6 = r6 << 1
            int r6 = r6 + 1
            short r4 = r5[r6]
        L_0x0034:
            int r5 = ua.netlizard.egypt.m3d.weapon_now
            r6 = 6
            if (r5 != r6) goto L_0x0052
            byte[] r5 = ua.netlizard.egypt.m3d.anim_cadri
            byte r6 = ua.netlizard.egypt.m3d.shot_delay
            byte r5 = r5[r6]
            int r0 = r0 + r5
            short[] r5 = ua.netlizard.egypt.m3d.anim_mech
            byte r6 = ua.netlizard.egypt.m3d.shot_delay
            int r6 = r6 << 1
            short r3 = r5[r6]
            short[] r5 = ua.netlizard.egypt.m3d.anim_mech
            byte r6 = ua.netlizard.egypt.m3d.shot_delay
            int r6 = r6 << 1
            int r6 = r6 + 1
            short r4 = r5[r6]
        L_0x0052:
            byte r5 = ua.netlizard.egypt.m3d.change
            if (r5 <= 0) goto L_0x009e
            byte r5 = ua.netlizard.egypt.m3d.change
            int r5 = 4 - r5
            int r4 = java.lang.Math.abs(r5)
            int r4 = r4 << 6
        L_0x0060:
            short r5 = ua.netlizard.egypt.m3d.rot_FPS
            int r1 = r5 << 1
            short r5 = ua.netlizard.egypt.m3d.rot_FPS
            int r2 = r5 * 3
            if (r1 < r7) goto L_0x006c
            int r1 = r1 + -360
        L_0x006c:
            if (r2 < r7) goto L_0x0074
            int r2 = r2 + -360
            if (r2 < r7) goto L_0x0074
            int r2 = r2 + -360
        L_0x0074:
            int[] r5 = ua.netlizard.egypt.math.sin
            r5 = r5[r1]
            int r5 = r5 * 12
            int r5 = r5 >> 16
            int r5 = r5 + 13
            int r3 = r3 + r5
            int[] r5 = ua.netlizard.egypt.math.cos
            r5 = r5[r2]
            int r5 = r5 * 12
            int r5 = r5 >> 16
            int r5 = r5 + 13
            int r4 = r4 - r5
            if (r3 >= 0) goto L_0x008d
            r3 = 0
        L_0x008d:
            if (r3 <= r8) goto L_0x0091
            r3 = 256(0x100, float:3.59E-43)
        L_0x0091:
            if (r4 >= 0) goto L_0x0094
            r4 = 0
        L_0x0094:
            if (r4 <= r8) goto L_0x0098
            r4 = 256(0x100, float:3.59E-43)
        L_0x0098:
            int[] r5 = ua.netlizard.egypt.m3d.display
            ua.netlizard.egypt.BIP.draw_BIP_compres(r5, r0, r3, r4)
            return
        L_0x009e:
            byte r5 = ua.netlizard.egypt.m3d.walk_now
            int r5 = r5 << 2
            int r4 = r4 - r5
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.m3d.draw_weapon():void");
    }

    static final void context_() {
        int[] dis = display;
        int u = (Height_real_3D - 2) * Width_real_3D;
        int i = 53 * 219;
        int i2 = 53 * 144;
        int i3 = 53 * 43;
        while (true) {
            u--;
            if (u >= 0) {
                int a = dis[u];
                dis[u] = ((((((16711680 & a) >> 16) * 53) + 11607) >> 8) << 16) | ((((((65280 & a) >> 8) * 53) + 7632) >> 8) << 8) | ((((a & 255) * 53) + 2279) >> 8);
            } else {
                return;
            }
        }
    }

    static final void dark_inter(int y, int h) {
        int len = (y + h) * Width_real_3D;
        int[] dis = display;
        for (int u = Width_real_3D * y; u < len; u++) {
            int a = dis[u];
            dis[u] = ((((16711680 & a) + 12976128) >> 17) << 16) + ((((65280 & a) + 26368) >> 9) << 8) + (((a & 255) + 1) >> 1);
        }
    }

    static final void dark(short alfa1) {
        if (alfa1 > 255) {
            alfa1 = 255;
            white = 255;
        }
        if (alfa1 < 0) {
            alfa1 = 0;
            white = 0;
        }
        int u = (Height_real_3D - 2) * Width_real_3D;
        int[] dis = display;
        int b = alfa1;
        int g = 8;
        if (Units[0].death) {
            b = 1;
            g = 0;
        }
        while (true) {
            u--;
            if (u < 0) {
                break;
            }
            int a = dis[u];
            dis[u] = (((((16711680 & a) >> 16) * b) >> g) << 16) | (((((65280 & a) >> 8) * alfa1) >> 8) << 8) | (((a & 255) * alfa1) >> 8);
        }
        if (g != 0 && white < 255) {
            white = (short) (white + 1);
        }
    }

    public static final void draw_3d(int[] draw_new) {
        int[] iArr = new int[3];
        iArr[2] = 1;
        int[] iArr2 = new int[3];
        iArr2[1] = -obzor;
        int[] iArr3 = new int[3];
        iArr3[1] = obzor;
        int[] iArr4 = new int[3];
        iArr4[0] = obzor;
        int[] iArr5 = new int[3];
        iArr5[0] = -obzor;
        int[][] planes_portal = {iArr, iArr2, iArr3, iArr4, iArr5};
        portal[] ss = Portals;
        for (int i = Portals.length - 1; i >= 0; i--) {
            ss[i].see = false;
        }
        if (Game.my_level == 10 && room_now == 0) {
            draw_object(Objects[object_sky], planes);
        }
        for (int i2 = draw_len - 5; i2 >= 0; i2 -= 5) {
            planes_portal[1][2] = draw_new[i2];
            planes_portal[2][2] = draw_new[i2 + 1];
            planes_portal[3][2] = draw_new[i2 + 2];
            planes_portal[4][2] = draw_new[i2 + 3];
            draw_room(draw_new[i2 + 4], planes_portal);
        }
        draw_room(room_now, planes);
        draw_len = 0;
        if (Game.my_level != 0 && !scr.enable_script && !Units[0].death) {
            draw_weapon();
        }
        if (white != 255) {
            dark(white);
        }
    }

    public static final void draw_object(object ob, int[][] planes_) {
        int[] p = max_point;
        int[] sort_polyi = index_sort_poly;
        short[] Polygone = ob.Poly;
        int all_poly = ob.Poly.length >> 4;
        point_[] v = help_v;
        point_ v0 = v[0];
        point_ v1 = v[1];
        point_ v2 = v[2];
        point_[] dts = help_dts;
        alfa_ = 256;
        ob.pos_x = s[0];
        ob.pos_y = s[1];
        ob.pos_z = s[2];
        math.rot_OBJ(ob);
        math.sort_poly(Polygone, p);
        for (int i = all_poly - 1; i >= 0; i--) {
            int index = sort_polyi[i] << 4;
            short s2 = Polygone[index];
            v0.rx = p[s2];
            int rx = s2 + 1;
            v0.ry = p[rx];
            v0.rz = p[rx + 1];
            short s3 = Polygone[index + 1];
            v1.rx = p[s3];
            int rx2 = s3 + 1;
            v1.ry = p[rx2];
            v1.rz = p[rx2 + 1];
            short s4 = Polygone[index + 2];
            v2.rx = p[s4];
            int rx3 = s4 + 1;
            v2.ry = p[rx3];
            v2.rz = p[rx3 + 1];
            int index2 = index + 3;
            v0.u = Polygone[index2];
            int index3 = index2 + 1;
            v0.v = Polygone[index3];
            int index4 = index3 + 1;
            v1.u = Polygone[index4];
            int index5 = index4 + 1;
            v1.v = Polygone[index5];
            int index6 = index5 + 1;
            v2.u = Polygone[index6];
            int index7 = index6 + 1;
            v2.v = Polygone[index7];
            ind = Polygone[index7 + 1];
            int clip = portal.clipPlane2(dts, v, planes_);
            int clip2 = clip - 1;
            if (clip > 0) {
                if (ind < 50) {
                    int clip3 = clip2 - 1;
                    while (true) {
                        clip3--;
                        if (clip3 < 0) {
                            break;
                        }
                        poly.draw_poly(dts[0], dts[clip3 + 1], dts[clip3 + 2]);
                    }
                } else {
                    int clip4 = clip2 - 1;
                    while (true) {
                        clip4--;
                        if (clip4 < 0) {
                            break;
                        }
                        poly.draw_poly_fill(dts[0], dts[clip4 + 1], dts[clip4 + 2], false);
                    }
                }
            }
        }
    }

    private static final void draw_room(int room_active, int[][] planes_) {
        if (planes_[1][2] > Height_2_real_3D) {
            planes_[1][2] = Height_2_real_3D;
        }
        if (planes_[2][2] > Height_2_real_3D) {
            planes_[2][2] = Height_2_real_3D;
        }
        if (planes_[3][2] > Width_2_real_3D) {
            planes_[3][2] = Width_2_real_3D;
        }
        if (!(Game.my_level == 10 && room_now == 0) && (room_active == room_with_sky_box || sky_olveis)) {
            draw_object(Objects[object_sky], planes_);
        }
        draw_rooom(room_active, planes_);
        draw_all(planes_, sort_visual(room_active));
    }

    private static final int sort_visual(int room_active) {
        int[] sort_i = index_sort;
        int[] sort_z = help_sort;
        int my_x = s[0];
        int my_y = s[1];
        int my_z = s[2];
        int obg = obj_0;
        int index_ = 0;
        int[][] SPR = sprite;
        int[][] SHO = shot;
        int Ind_spr = 0;
        int i = 0;
        for (int j = units - 1; j > 0; j--) {
            unit U = Units[j];
            if (U.visible && room_active == U.room_now) {
                sort_i[index_] = j;
                sort_z[index_] = Math.abs(U.pos_x - my_x) + Math.abs(U.pos_y - my_y) + Math.abs(U.pos_z - my_z);
                index_++;
            }
        }
        for (int j2 = 0; j2 < objects; j2++) {
            object ob = Objects[j2];
            if (ob.visible && ob.room_now == room_active) {
                sort_i[index_] = j2 + obg;
                sort_z[index_] = Math.abs(ob.pos_x - my_x) + Math.abs(ob.pos_y - my_y) + Math.abs(ob.pos_z - my_z);
                index_++;
            }
        }
        int obg2 = obg + objects;
        while (Ind_spr < sprites) {
            if (SPR[i][4] == 0) {
                i++;
            } else {
                if (SPR[i][5] == room_active) {
                    sort_i[index_] = i + obg2;
                    sort_z[index_] = Math.abs(SPR[i][0] - my_x) + Math.abs(SPR[i][1] - my_y) + Math.abs(SPR[i][2] - my_z);
                    index_++;
                }
                Ind_spr++;
                i++;
            }
        }
        int obg3 = obg2 + 15;
        int Ind_spr2 = 0;
        int i2 = 0;
        while (Ind_spr2 < shots) {
            if (SHO[i2][6] == 0) {
                i2++;
            } else {
                if (SHO[i2][7] == room_active) {
                    sort_i[index_] = i2 + obg3;
                    sort_z[index_] = Math.abs(SHO[i2][0] - my_x) + Math.abs(SHO[i2][1] - my_y) + Math.abs(SHO[i2][2] - my_z);
                    index_++;
                }
                Ind_spr2++;
                i2++;
            }
        }
        math.quicksort(sort_z, sort_i, 0, index_ - 1);
        return index_;
    }

    public static final void draw2D(int[] planes1, int room) {
        int[] planes_portal1 = new int[4];
        int portals_in_room = Rooms[room].portals;
        byte[] bArr = Rooms[room].portal;
        int[] draw_new = draw_loop;
        while (true) {
            portals_in_room--;
            if (portals_in_room >= 0) {
                int next_room = see_portal_old(planes1, Rooms[room].portal[portals_in_room], draw_new, room);
                if (next_room >= 0) {
                    if (next_room < 999 && (open_rooms[next_room] == 1 || open_rooms[next_room] < 0)) {
                        Portals[Rooms[next_room].portal[0]].see = true;
                        Portals[Rooms[next_room].portal[1]].see = true;
                    }
                    int gg = draw_len - 5;
                    planes_portal1[0] = draw_new[gg];
                    planes_portal1[1] = draw_new[gg + 1];
                    planes_portal1[2] = draw_new[gg + 2];
                    planes_portal1[3] = draw_new[gg + 3];
                    if (draw_new[gg + 4] == 999) {
                        draw_new[gg + 4] = -1;
                    } else {
                        draw2D(planes_portal1, next_room);
                    }
                }
            } else {
                return;
            }
        }
    }

    static final void move_me_up() {
        Units[0].F[0] = -math.sin[rot_z];
        Units[0].F[1] = -math.cos[rot_z];
        walk = true;
    }

    static final void move_me_down() {
        Units[0].F[0] = math.sin[rot_z];
        Units[0].F[1] = math.cos[rot_z];
        walk = true;
    }

    static final void move_me_sleft() {
        Units[0].F[0] = math.cos[rot_z];
        Units[0].F[1] = -math.sin[rot_z];
        walk = true;
    }

    static final void move_me_sright() {
        Units[0].F[0] = -math.cos[rot_z];
        Units[0].F[1] = math.sin[rot_z];
        walk = true;
    }

    static final void move_up(int speed) {
        int[] iArr = s;
        iArr[0] = iArr[0] - (math.sin[rot_z] * speed);
        int[] iArr2 = s;
        iArr2[1] = iArr2[1] - (math.cos[rot_z] * speed);
    }

    static final void move_down(int speed) {
        int[] iArr = s;
        iArr[0] = iArr[0] + (math.sin[rot_z] * speed);
        int[] iArr2 = s;
        iArr2[1] = iArr2[1] + (math.cos[rot_z] * speed);
    }

    static final void turn_left(int speed) {
        turn_left1 = true;
        if (Game.inert) {
            rot_az -= speed + 1;
        }
        if (rot_az < (-5 << speed)) {
            rot_az = -5 << speed;
            return;
        }
        rot_z -= speed;
        if (rot_z < 0) {
            rot_z += 360;
        }
    }

    static final void turn_right(int speed) {
        turn_left1 = true;
        if (Game.inert) {
            rot_az += speed + 1;
        }
        if (rot_az > (5 << speed)) {
            rot_az = 5 << speed;
            return;
        }
        rot_z += speed;
        if (rot_z > 359) {
            rot_z -= 360;
        }
    }

    static final void turn_up(int speed) {
        turn_left2 = true;
        rot_ax -= speed + 2;
        if (rot_ax < (-5 << speed)) {
            rot_ax = -5 << speed;
        }
    }

    static final void turn_down(int speed) {
        turn_left2 = true;
        rot_ax += speed + 2;
        if (rot_ax > (5 << speed)) {
            rot_ax = 5 << speed;
        }
    }

    public static final void open_door_4() {
        unit U = Units[0];
        int x = U.pos_x;
        int y = U.pos_y;
        int i = U.pos_z;
        object[] obs = Objects;
        byte b = cof_W;
        byte c = cof_W;
        if (Math.abs((U.pos_x >> 16) - 1650) + Math.abs((U.pos_y >> 16) - 3450) < 300) {
            b = 1;
        }
        if (Math.abs((U.pos_x >> 16) - 3150) + Math.abs((U.pos_y >> 16) - 3450) < 300) {
            c = 1;
        }
        for (int j = 0; j < objects; j++) {
            object ob = obs[j];
            if (ob.type_object == 23) {
                if (Math.abs((ob.pos_x >> 16) - 1650) + Math.abs((ob.pos_y >> 16) - 3450) < 300) {
                    b = 1;
                }
                if (Math.abs((ob.pos_x >> 16) - 3150) + Math.abs((ob.pos_y >> 16) - 3450) < 300) {
                    c = 1;
                }
            }
        }
        byte a = (byte) (b + c);
        if (a == 2) {
            for_level_4 = true;
        } else {
            for_level_4 = false;
        }
        for (int i2 = 0; i2 < objects; i2++) {
            object ob2 = obs[i2];
            if (ob2.type_object == 20 || ob2.type_object == 21) {
                int i3 = (ob2.pos_x - x) >> 16;
                int i4 = (ob2.pos_y - y) >> 16;
                if (open_rooms[ob2.room_now] >= 0) {
                    open_rooms[ob2.room_now] = cof_W;
                }
                if (a == 0 || open_rooms[ob2.room_now] < 0) {
                    ob2.pos_z -= speed_door;
                    if ((ob2.pos_z >> 16) < open_doors_cof[i2] - 300) {
                        ob2.pos_z = (open_doors_cof[i2] << 16) - H_door_stp;
                        ob2.min_z = ob2.pos_z;
                        ob2.max_z = ob2.pos_z + H_door_stp;
                    } else if ((ob2.pos_z >> 16) < open_doors_cof[i2] - 300) {
                        ob2.pos_z = (open_doors_cof[i2] << 16) - H_door_stp;
                        ob2.min_z = ob2.pos_z;
                        ob2.max_z = ob2.pos_z + H_door_stp;
                    } else {
                        ob2.min_z = ob2.pos_z;
                        ob2.max_z = ob2.min_z + H_door_stp;
                    }
                } else if (a != 1 || (ob2.pos_z >> 16) <= (open_doors_cof[i2] >> 2)) {
                    ob2.pos_z += speed_door;
                    if (a != 2 || (ob2.pos_z >> 16) <= open_doors_cof[i2]) {
                        ob2.min_z = ob2.pos_z;
                        ob2.max_z = ob2.min_z + H_door_stp;
                    } else {
                        ob2.pos_z = open_doors_cof[i2] << 16;
                        ob2.min_z = ob2.pos_z;
                        ob2.max_z = ob2.pos_z + H_door_stp;
                    }
                } else {
                    ob2.pos_z -= speed_door;
                }
            }
        }
    }

    public static final void open_door() {
        unit U = Units[0];
        int x = U.pos_x;
        int y = U.pos_y;
        int i = U.pos_z;
        object[] obs = Objects;
        for (int i2 = 0; i2 < objects; i2++) {
            object ob = obs[i2];
            if (ob.type_object == 9 && !ob.color_obj) {
                if (ob.light < 252) {
                    ob.light = 252;
                }
                if (ob.light == 252) {
                    Create_sprite(ob.pos_x, ob.pos_y, ob.pos_z + ob.max_z + 196608, (byte) 6, ob.room_now);
                }
                ob.light = (short) (ob.light + 1);
                if (ob.light == 256) {
                    ob.light = 252;
                }
            }
            if (ob.type_object == 20 || ob.type_object == 21) {
                int xx = (ob.pos_x - x) >> 16;
                int yy = (ob.pos_y - y) >> 16;
                if (open_rooms[ob.room_now] >= 0) {
                    open_rooms[ob.room_now] = cof_W;
                }
                if ((xx * xx) + (yy * yy) >= dist_door || open_rooms[ob.room_now] < 0) {
                    ob.pos_z -= speed_door;
                    if ((ob.pos_z >> 16) < open_doors_cof[i2] - 300) {
                        ob.pos_z = (open_doors_cof[i2] << 16) - H_door_stp;
                        ob.min_z = ob.pos_z;
                        ob.max_z = ob.pos_z + H_door_stp;
                        if (open_rooms[ob.room_now] >= 0) {
                            open_rooms[ob.room_now] = 1;
                        }
                    } else {
                        ob.min_z = ob.pos_z;
                        ob.max_z = ob.min_z + H_door_stp;
                    }
                } else {
                    ob.pos_z += speed_door;
                    if ((ob.pos_z >> 16) > open_doors_cof[i2]) {
                        ob.pos_z = open_doors_cof[i2] << 16;
                        ob.min_z = ob.pos_z;
                        ob.max_z = ob.pos_z + H_door_stp;
                    } else {
                        ob.min_z = ob.pos_z;
                        ob.max_z = ob.min_z + H_door_stp;
                    }
                }
            }
        }
    }

    public static final void give_obj() {
        unit U = Units[0];
        int x = U.pos_x;
        int y = U.pos_y;
        int z = U.pos_z;
        int end_obj = Rooms[U.room_now].end_obj;
        object[] OB = Objects;
        if (Game.deathmach) {
            for (int i = 0; i < objects; i++) {
                object ob = OB[i];
                if (ob.visible && ob.type_object > 23 && ob.type_object < 30 && Math.abs(ob.pos_x - x) + Math.abs(ob.pos_y - y) + Math.abs(ob.pos_z - z) < 19005440) {
                    take_gun(ob);
                }
            }
            return;
        }
        for (int i2 = Rooms[U.room_now].start_obj; i2 < end_obj; i2++) {
            object ob2 = OB[i2];
            if (ob2.visible && ob2.type_object > 23 && ob2.type_object < 30 && Math.abs(ob2.pos_x - x) + Math.abs(ob2.pos_y - y) + Math.abs(ob2.pos_z - z) < 19005440) {
                take_gun(ob2);
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public static final void take_gun(object ob) {
        ob.visible = false;
        int take_weapon = ob.type_object - 23;
        if (slot[take_weapon] == 0) {
            slot[take_weapon] = 1;
            ammo_in_mag[take_weapon] = max_ammo_in_mag[take_weapon];
            ammo[take_weapon] = max_ammo[take_weapon];
            change_weapon((byte) take_weapon);
            return;
        }
        int a = ammo[take_weapon] + (max_ammo_in_mag[take_weapon] << 2);
        if (a > max_ammo[take_weapon]) {
            a = max_ammo[take_weapon];
        }
        ammo[take_weapon] = (byte) a;
    }

    public static final void time_rot_obj() {
        object[] obs = Objects;
        for (int i = 0; i < objects; i++) {
            object ob = obs[i];
            if ((ob.type_object > 23 && ob.type_object < 30) || ob.type_object == 8 || ob.type_object == 14) {
                ob.r_z += 5;
                if (ob.r_z > 359) {
                    ob.r_z -= 360;
                }
            }
        }
    }

    private static final boolean aim() {
        if (look_on) {
            return true;
        }
        unit[] ss = Units;
        unit my = ss[0];
        int dis = 0;
        byte now = cof_W;
        boolean first = false;
        for (int i = 1; i < units; i++) {
            if (!ss[i].death && auto_aim[i] >= 1) {
                unit k = ss[i];
                int dist_p = Math.abs(k.pos_x - my.pos_x) + Math.abs(k.pos_y - my.pos_y) + Math.abs(k.pos_z - my.pos_z);
                if (!first) {
                    dis = dist_p;
                    now = auto_aim[i];
                    first = true;
                    auto_aim[i] = cof_W;
                } else if (dist_p < dis) {
                    dis = dist_p;
                    now = auto_aim[i];
                }
            }
            auto_aim[i] = cof_W;
        }
        if (now == 1) {
            rot_x++;
        }
        if (now != 2) {
            return first;
        }
        rot_x--;
        return first;
    }

    static final void walk() {
        if (walk) {
            walk_now = (byte) (walk_now + walk_shag);
            if (16 <= walk_now || walk_now <= 0) {
                walk_shag = (byte) (-walk_shag);
                walk_now = (byte) (walk_now + (walk_shag << 1));
            }
            walk = false;
        } else {
            if (walk_shag < 0) {
                walk_shag = (byte) (-walk_shag);
            }
            if (walk_now > 0) {
                walk_now = (byte) (walk_now - walk_shag);
            }
            if (walk_now < 0) {
                walk_now = cof_W;
            }
        }
        int[] iArr = s;
        iArr[2] = iArr[2] + (walk_now << 16);
    }

    static final void script_mein_menu() {
        boolean z = false;
        if (!Game.accelerate || !Game.accel_on) {
            if (move_in_menu) {
                int[] iArr = s;
                iArr[1] = iArr[1] + 655360;
                if (s[1] >= move_STP) {
                    s[1] = move_STP;
                    if (!move_in_menu) {
                        z = true;
                    }
                    move_in_menu = z;
                }
            } else {
                int[] iArr2 = s;
                iArr2[1] = iArr2[1] - 655360;
                if (s[1] <= -45875200) {
                    s[1] = -45875200;
                    if (!move_in_menu) {
                        z = true;
                    }
                    move_in_menu = z;
                }
            }
            rot_z = (-(((s[1] >> 16) * 70) / 700)) + 90;
        } else {
            int[] iArr3 = s;
            iArr3[1] = iArr3[1] + ((5242880 * Game.xA) / FullCanvas.canvasRealWidth);
            if (s[1] >= move_STP) {
                s[1] = move_STP;
            }
            if (s[1] <= -45875200) {
                s[1] = -45875200;
            }
            rot_z = (-(((s[1] >> 16) * 70) / 700)) + 90;
        }
        if (rot_z < 0) {
            rot_z += 360;
        }
        if (rot_z >= 360) {
            rot_z -= 360;
        }
    }

    static final void camera_view() {
        unit U = Units[0];
        if (U.death) {
            if (s[2] > U.pos_z) {
                int[] iArr = s;
                iArr[2] = iArr[2] - 131072;
            }
            rot_x -= 5;
        } else if (!scr.enable_script) {
            room_now = U.room_now;
            s[0] = U.pos_x;
            s[1] = U.pos_y;
            s[2] = U.pos_z + HEAD;
            walk();
            if (!aim()) {
                if (rot_x < 270) {
                    rot_x += 2;
                    if (rot_x > 270) {
                        rot_x = 270;
                    }
                }
                if (rot_x > 270) {
                    rot_x -= 2;
                    if (rot_x < 270) {
                        rot_x = 270;
                    }
                }
            }
        }
        if (rot_az > 0) {
            if (turn_left1) {
                rot_az--;
            } else {
                rot_az -= 8;
                if (rot_az < 0) {
                    rot_az = 0;
                }
            }
        }
        if (rot_az < 0) {
            if (turn_left1) {
                rot_az++;
            } else {
                rot_az += 8;
                if (rot_az > 0) {
                    rot_az = 0;
                }
            }
        }
        if (rot_ax > 0) {
            if (turn_left2) {
                rot_ax--;
            } else {
                rot_ax -= 8;
                if (rot_ax < 0) {
                    rot_ax = 0;
                }
            }
        }
        if (rot_ax < 0) {
            if (turn_left2) {
                rot_ax++;
            } else {
                rot_ax += 8;
                if (rot_ax > 0) {
                    rot_ax = 0;
                }
            }
        }
        if (!scr.enable_script) {
            rot_z += rot_az;
            rot_x += rot_ax;
        }
        if (Game.accelerate && Game.accel_on && !scr.enable_script) {
            if (rot_s_x > 0) {
                rot_s_x -= 131072;
                if (rot_s_x < 0) {
                    rot_s_x = 0;
                }
            }
            if (rot_s_x < 0) {
                rot_s_x += 131072;
                if (rot_s_x > 0) {
                    rot_s_x = 0;
                }
            }
            if (rot_s_z > 0) {
                rot_s_z -= 131072;
                if (rot_s_z < 0) {
                    rot_s_z = 0;
                }
            }
            if (rot_s_z < 0) {
                rot_s_z += 131072;
                if (rot_s_z > 0) {
                    rot_s_z = 0;
                }
            }
            int x = Game.xA << 16;
            int y = Game.yA << 16;
            if (look_on) {
                int y2 = y - (Game.s_yA << 16);
                x -= Game.s_xA << 16;
                rot_s_x += (y2 * 10) / FullCanvas.canvasRealWidth;
            }
            rot_s_z += (-(x * 10)) / FullCanvas.canvasRealWidth;
            if (rot_s_x > 1114112) {
                rot_s_x = 1114112;
            }
            if (rot_s_x < -1114112) {
                rot_s_x = -1114112;
            }
            if (rot_s_z > 1114112) {
                rot_s_z = 1114112;
            }
            if (rot_s_z < -1114112) {
                rot_s_z = -1114112;
            }
            rot_z += rot_s_z >> 16;
            rot_x += rot_s_x >> 16;
        }
        if (rot_z < 0) {
            rot_z += 360;
        }
        if (rot_z >= 360) {
            rot_z -= 360;
        }
        if (rot_x >= 360) {
            rot_x = 359;
        }
        if (rot_x < 180) {
            rot_x = 180;
        }
        turn_left1 = false;
        turn_left2 = false;
    }

    private static final int see_portal_old(int[] planes_portal, int now_portal, int[] draw_new, int room_stand) {
        int clip;
        int clip2;
        int clip3;
        portal p = Portals[now_portal];
        if (Portals[now_portal].see) {
            return -1;
        }
        point_[] v = help_v;
        point_[] dts = help_dts;
        int[] ss = s;
        v[0] = p.p[3];
        v[1] = p.p[1];
        v[2] = p.p[0];
        v[3] = p.p[2];
        int ggg = p.norm;
        switch (ggg) {
            case 1:
                if (Math.max(v[0].y, Math.max(v[1].y, v[2].y)) > ss[1] && Math.max(v[0].z, Math.max(v[1].z, v[2].z)) > ss[2]) {
                    ggg = Math.abs(ss[ggg - 1] - v[0].x);
                    break;
                } else {
                    ggg = 9830401;
                    break;
                }
            case 2:
                if (Math.max(v[0].x, Math.max(v[1].x, v[2].x)) > ss[0] && Math.max(v[0].z, Math.max(v[1].z, v[2].z)) > ss[2]) {
                    ggg = Math.abs(ss[ggg - 1] - v[0].y);
                    break;
                } else {
                    ggg = 9830401;
                    break;
                }
                break;
            case 3:
                if (Math.max(v[0].y, Math.max(v[1].y, v[2].y)) > ss[1] && Math.max(v[0].x, Math.max(v[1].x, v[2].x)) > ss[0]) {
                    ggg = Math.abs(ss[ggg - 1] - v[0].z);
                    break;
                } else {
                    ggg = 9830401;
                    break;
                }
        }
        if (ggg <= distance_portal) {
            p.see = true;
            draw_new[draw_len] = planes_portal[0];
            draw_new[draw_len + 1] = planes_portal[1];
            draw_new[draw_len + 2] = planes_portal[2];
            draw_new[draw_len + 3] = planes_portal[3];
            if (p.room1 == room_stand) {
                draw_new[draw_len + 4] = p.room2;
                draw_len += 5;
                return p.room2;
            }
            draw_new[draw_len + 4] = p.room1;
            draw_len += 5;
            return p.room1;
        }
        for (int i = 3; i >= 0; i--) {
            math.xRotateVertex(v[i]);
        }
        int clip4 = portal.clipPlanex(dts, v, planes_portal[2], 4);
        if (clip4 == 0 || (clip = portal.clipPlane_x(v, dts, planes_portal[3], clip4)) == 0 || (clip2 = portal.clipPlaney(dts, v, planes_portal[1], clip)) == 0 || (clip3 = portal.clipPlane_y(v, dts, planes_portal[0], clip2)) == 0) {
            return -1;
        }
        for (int i2 = clip3 - 1; i2 >= 0; i2--) {
            math.projectVertex(v[i2]);
        }
        int max_y = v[0].sy;
        int min_y = v[0].sy;
        int max_x = v[0].sx;
        int min_x = v[0].sx;
        for (int i3 = clip3 - 1; i3 > 0; i3--) {
            if (max_x < v[i3].sx) {
                max_x = v[i3].sx;
            }
            if (min_x > v[i3].sx) {
                min_x = v[i3].sx;
            }
            if (max_y < v[i3].sy) {
                max_y = v[i3].sy;
            }
            if (min_y > v[i3].sy) {
                min_y = v[i3].sy;
            }
        }
        draw_new[draw_len] = Height_2_real_3D - min_y;
        draw_new[draw_len + 1] = (max_y - Height_2_real_3D) + 1;
        draw_new[draw_len + 2] = (Width_2_real_3D - min_x) + 1;
        draw_new[draw_len + 3] = (max_x - Width_2_real_3D) + 1;
        if (p.room1 == room_stand) {
            draw_new[draw_len + 4] = p.room2;
            draw_len += 5;
            p.see = true;
            return p.room2;
        }
        draw_new[draw_len + 4] = p.room1;
        draw_len += 5;
        p.see = true;
        return p.room1;
    }

    static final int new_alfa(int x, int y, int alfa) {
        int xx = (int) ((65536 * ((long) ((x * x) + (y * y)))) >> 28);
        if (xx > 255) {
            xx = 255;
        }
        int alfa2 = alfa + (255 - xx);
        if (alfa2 > 255) {
            return 255;
        }
        return alfa2;
    }

    static final int new_alfa_2(short[] poin, short[] Polygone, int i, int alfa, object ob) {
        int x = (ob.pos_x >> 16) - (((poin[Polygone[i]] + poin[Polygone[i + 1]]) + poin[Polygone[i + 2]]) / 3);
        int y = (ob.pos_y >> 16) - (((poin[Polygone[i] + 1] + poin[Polygone[i + 1] + 1]) + poin[Polygone[i + 2] + 1]) / 3);
        int xx = (int) ((65536 * ((long) ((x * x) + (y * y)))) >> 28);
        if (xx > 255) {
            xx = 255;
        }
        int alfa2 = alfa + (255 - xx);
        if (alfa2 > 255) {
            return 255;
        }
        return alfa2;
    }

    private static final void draw_rooom(int room_active, int[][] planes_) {
        short[] poin = Rooms[room_active].Points;
        short[] Polygone = Rooms[room_active].Poly;
        int all_poly = Rooms[room_active].polys * 7;
        point_[] v = help_v;
        point_[] dts = help_dts;
        point_ v0 = v[0];
        point_ v1 = v[1];
        point_ v2 = v[2];
        point_ dts0 = dts[0];
        short[] text_ = text_coords;
        math.xRotateVertex(poin, Rooms[room_active].points);
        int[] p = max_point;
        int i = s[0];
        int i2 = s[1];
        int i3 = s[2];
        int me_x = x_light >> 16;
        int me_y = y_light >> 16;
        int i4 = z_light;
        for (int i5 = 0; i5 < all_poly; i5 += 7) {
            short s2 = Polygone[i5];
            v0.rx = p[s2];
            int xx = s2 + 1;
            v0.ry = p[xx];
            v0.rz = p[xx + 1];
            short s3 = Polygone[i5 + 1];
            v1.rx = p[s3];
            int xx2 = s3 + 1;
            v1.ry = p[xx2];
            v1.rz = p[xx2 + 1];
            short s4 = Polygone[i5 + 2];
            v2.rx = p[s4];
            int xx3 = s4 + 1;
            v2.ry = p[xx3];
            v2.rz = p[xx3 + 1];
            short s5 = Polygone[i5 + 3];
            v0.u = text_[s5];
            v0.v = text_[s5 + 1];
            short s6 = Polygone[i5 + 4];
            v1.u = text_[s6];
            v1.v = text_[s6 + 1];
            short s7 = Polygone[i5 + 5];
            v2.u = text_[s7];
            v2.v = text_[s7 + 1];
            int clip = portal.clipPlane2(dts, v, planes_);
            if (clip > 0) {
                alfa_ = (Polygone[i5 + 6] & 65280) >> 8;
                if (fire2) {
                    alfa_ = new_alfa(me_x - poin[Polygone[i5]], me_y - poin[Polygone[i5] + 1], alfa_);
                }
                if (Game.my_level == 4) {
                    alfa_ = new_alfa_2(poin, Polygone, i5, alfa_, Objects[0]);
                    alfa_ = new_alfa_2(poin, Polygone, i5, alfa_, Objects[13]);
                }
            }
            int clip2 = clip - 1;
            if (clip > 0) {
                ind = Polygone[i5 + 6] & 255;
                if (ind >= 50) {
                    int clip3 = clip2 - 1;
                    while (true) {
                        clip3--;
                        if (clip3 < 0) {
                            break;
                        }
                        poly.draw_poly_fill(dts0, dts[clip3 + 1], dts[clip3 + 2], false);
                    }
                } else {
                    int clip4 = clip2 - 1;
                    while (true) {
                        clip4--;
                        if (clip4 < 0) {
                            break;
                        }
                        poly.draw_poly(dts0, dts[clip4 + 1], dts[clip4 + 2]);
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: int[][]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v9, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v11, resolved type: java.lang.Object[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void Create_sprite(int r7, int r8, int r9, byte r10, short r11) {
        /*
            r6 = 4
            r5 = 1
            short r3 = ua.netlizard.egypt.m3d.sprites
            r4 = 15
            if (r3 < r4) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            int[][] r0 = ua.netlizard.egypt.m3d.sprite
            r1 = 0
            r2 = 0
            short r3 = ua.netlizard.egypt.m3d.sprites
            int r3 = r3 + 1
            short r3 = (short) r3
            ua.netlizard.egypt.m3d.sprites = r3
        L_0x0014:
            if (r1 != 0) goto L_0x0008
            r3 = r0[r2]
            r3 = r3[r6]
            if (r3 >= r5) goto L_0x0040
            r3 = r0[r2]
            r4 = 0
            r3[r4] = r7
            r3 = r0[r2]
            r3[r5] = r8
            r3 = r0[r2]
            r4 = 2
            r3[r4] = r9
            r3 = r0[r2]
            r4 = 3
            r3[r4] = r10
            r3 = r0[r2]
            int[][] r4 = ua.netlizard.egypt.m3d.N_sprite
            r4 = r4[r10]
            int r4 = r4.length
            int r4 = r4 + 1
            r3[r6] = r4
            r3 = r0[r2]
            r4 = 5
            r3[r4] = r11
            goto L_0x0008
        L_0x0040:
            int r2 = r2 + 1
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.m3d.Create_sprite(int, int, int, byte, short):void");
    }

    public static final void Sprite_life() {
        int[][] SPR = sprite;
        int Ind_spr = 0;
        int i = 0;
        while (Ind_spr < sprites) {
            if (SPR[i][4] < 1) {
                i++;
            } else {
                int[] iArr = SPR[i];
                iArr[4] = iArr[4] - 1;
                if (SPR[i][4] < 1) {
                    sprites = (short) (sprites - 1);
                } else {
                    Ind_spr++;
                    i++;
                }
            }
        }
    }

    static void kill_with_meet(unit U) {
        switch (U.type_unit) {
            case 0:
            case 3:
            case 5:
            default:
                return;
            case 1:
                if (U.life < -10) {
                    int num = Math.abs(myRandom.nextInt() & 1) + 3;
                    U.visible = false;
                    for (int i = 0; i < num; i++) {
                        creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, false);
                    }
                    creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, true);
                    if (Math.abs(myRandom.nextInt() & 1) == 1) {
                        Create_sprite(U.pos_x, U.pos_y, U.pos_z + 524288, (byte) 2, U.room_now);
                    }
                    Create_sprite(U.pos_x, U.pos_y, U.pos_z, (byte) 2, U.room_now);
                    return;
                }
                return;
            case 2:
                if (U.life < -50) {
                    U.visible = false;
                    for (int i2 = 0; i2 < 8; i2++) {
                        creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, true);
                    }
                    Create_sprite(U.pos_x, U.pos_y, U.pos_z + 1048576, (byte) 2, U.room_now);
                    Create_sprite(U.pos_x, U.pos_y + 1048576, U.pos_z, (byte) 2, U.room_now);
                    Create_sprite(U.pos_x + 1048576, U.pos_y, U.pos_z, (byte) 2, U.room_now);
                    Create_sprite(U.pos_x, U.pos_y, U.pos_z, (byte) 2, U.room_now);
                    return;
                }
                return;
            case 4:
                if (U.life < -40) {
                    U.visible = false;
                    for (int i3 = 0; i3 < 3; i3++) {
                        creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, true);
                    }
                    for (int i4 = 0; i4 < 3; i4++) {
                        creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, false);
                    }
                    Create_sprite(U.pos_x, U.pos_y, U.pos_z, (byte) 2, U.room_now);
                    Create_sprite(U.pos_x, U.pos_y, U.pos_z + 1048576, (byte) 2, U.room_now);
                    return;
                }
                return;
            case 6:
                if (U.life < -40) {
                    U.visible = false;
                    for (int i5 = 0; i5 < 6; i5++) {
                        creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, true);
                    }
                    Create_sprite(U.pos_x, U.pos_y, U.pos_z, (byte) 2, U.room_now);
                    Create_sprite(U.pos_x, U.pos_y, U.pos_z + 1048576, (byte) 2, U.room_now);
                    return;
                }
                return;
            case 7:
                U.visible = false;
                for (int i6 = 0; i6 < 5; i6++) {
                    Create_sprite(U.pos_x, U.pos_y, U.pos_z + ((i6 * 150) << 16), (byte) 2, U.room_now);
                    Create_sprite(U.pos_x, U.pos_y, U.pos_z + ((i6 * 100) << 16), (byte) 2, U.room_now);
                    creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, true);
                    creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, true);
                    creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, true);
                    creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, true);
                    creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, false);
                    creat_meet(U.pos_x, U.pos_y, U.pos_z, U.room_now, false);
                }
                return;
        }
    }

    static void change_look_on() {
        if (Units != null && !Units[0].death) {
            look_on = !look_on;
        }
        Game.s_xA = Game.xA;
        Game.s_yA = Game.yA;
        rot_s_x = 0;
        rot_s_z = 0;
    }

    static void kill_me(unit U) {
        Game.time_game = Game.time;
        U.death = true;
        look_on = false;
        if (U.life > 0) {
            U.life = 0;
        }
    }

    static void see_on_me(unit U) {
        U.to_x = Units[0].pos_x;
        U.to_y = Units[0].pos_y;
        AI.AI_goto(U, U.to_x, U.to_y, 50, 10);
        U.F[0] = 0;
        U.F[1] = 0;
        U.MHZ = 0;
    }

    static void kill_unit(unit U, int[] sss, point_ p) {
        int demage;
        int demage2 = (sss[10] & 65280) >> 8;
        if (AI.difficult != 2 || U.type_unit == 0) {
            demage = demage2 + Math.abs(myRandom.nextInt() & 15) + 1;
        } else {
            demage = demage2 + 5;
        }
        switch (U.type_unit) {
            case 0:
                Game.vibra();
                if (!Menu.chit_life) {
                    if (AI.difficult == 0) {
                        demage >>= 2;
                    }
                    if (AI.difficult == 1) {
                        demage >>= 1;
                    }
                    if (demage < 6) {
                        demage = 6;
                    }
                    U.life = (short) (U.life - demage);
                    if (U.life < 0) {
                        U.life = 0;
                        kill_me(U);
                    }
                    HP = (short) (HP - 80);
                    if (HP < 80) {
                        HP = 80;
                        return;
                    }
                    return;
                }
                return;
            case 1:
                if (AI.difficult > 0 && !U.Fly) {
                    see_on_me(U);
                }
                U.life = (short) (U.life - demage);
                if (U.life <= 0) {
                    kills = (byte) (kills + 1);
                    if (Game.deathmach && now_weap != dm_level && creat_weapon(now_weap + 25, U)) {
                        now_weap = (byte) dm_level;
                    }
                    U.death = true;
                    U.type_anim = 4;
                    U.cadr = -1;
                    kill_with_meet(U);
                    return;
                }
                return;
            case 2:
                if (((sss[8] & 65280) >> 8) == 0) {
                    see_on_me(U);
                }
                U.life = (short) (U.life - demage);
                if (U.life <= 0) {
                    if (Game.deathmach && now_weap != dm_level && creat_weapon(now_weap + 25, U)) {
                        now_weap = (byte) dm_level;
                    }
                    kills = (byte) (kills + 1);
                    U.death = true;
                    U.type_anim = 4;
                    U.cadr = -1;
                    kill_with_meet(U);
                    return;
                }
                return;
            case 3:
                U.life = (short) (U.life - demage);
                if (U.life <= 0) {
                    U.death = true;
                    return;
                }
                return;
            case 4:
                U.life = (short) (U.life - demage);
                if (U.life <= 0) {
                    U.death = true;
                    kill_with_meet(U);
                    return;
                }
                return;
            case 5:
                if (Game.my_level == 5) {
                    U.life = (short) (U.life - 1);
                    if ((U.life & 7) == 1) {
                        Menu.M(cof_W);
                        text = true;
                        Game.touch_began = false;
                        U.life = life_unit[U.type_unit];
                        return;
                    }
                    return;
                } else if (U.type_anim != 4 && U.type_anim != 7) {
                    if (U.life >= 200 || U.type_anim != 5 || weapon_now == 6) {
                        U.life = (short) (U.life - demage);
                        if (U.life < (life_unit[5] >> 2) && U.type_anim != 5) {
                            U.cadr = -1;
                            U.type_anim = 4;
                        }
                        if (U.life < 0 && U.type_anim == 5) {
                            U.death = true;
                            U.type_anim = 6;
                            U.cadr = -1;
                            return;
                        }
                        return;
                    }
                    return;
                } else {
                    return;
                }
            case 6:
                U.life = (short) (U.life - demage);
                if (U.life <= 0) {
                    kills = (byte) (kills + 1);
                    if (Game.deathmach && now_weap != dm_level && creat_weapon(now_weap + 25, U)) {
                        now_weap = (byte) dm_level;
                    }
                    U.death = true;
                    U.type_anim = 4;
                    U.cadr = -1;
                    kill_with_meet(U);
                }
                see_on_me(U);
                return;
            case 7:
                U.life = (short) (U.life - demage);
                if (U.life <= 0) {
                    U.death = true;
                    U.type_anim = 4;
                    U.cadr = -1;
                    kill_with_meet(U);
                }
                if ((U.life & 3) == 0) {
                    creat_meet(p.x, p.y, p.z, U.room_now, false);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private static final void draw_un(int j) {
        if (poly.all_poly != 0) {
            unit u = Units[j];
            if (u.type_unit != 4 && u.type_unit != 7) {
                point_ a = new point_();
                a.x = u.pos_x;
                a.y = u.pos_y;
                a.z = u.pos_z + (anim_poin[u.type_unit] << 16);
                math.xRotateVertex(a);
                if (a.rz > 0) {
                    a.sx = (short) ((int) (((long) Width_2_real_3D) + ((((long) a.rx) * ((long) obzor)) / ((long) a.rz))));
                    a.sy = (short) ((int) (((long) Height_2_real_3D) - ((((long) a.ry) * ((long) obzor)) / ((long) a.rz))));
                    auto_aim[j] = 3;
                    if (a.sy - 5 >= Height_2_real_3D) {
                        auto_aim[j] = 1;
                    }
                    if (a.sy + 5 <= Height_2_real_3D) {
                        auto_aim[j] = 2;
                    }
                }
            }
        }
    }

    private static final boolean clip_pos(int[][] planes_, object obb) {
        main_object ob = OBJ[obb.type_object];
        int x = (int) (((((long) (s[0] - obb.pos_x)) * ((long) math.cos[rot_z])) - (((long) (s[1] - obb.pos_y)) * ((long) math.sin[rot_z]))) >> 16);
        int y = (int) (((((long) (s[0] - obb.pos_x)) * ((long) math.sin[rot_z])) + (((long) (s[1] - obb.pos_y)) * ((long) math.cos[rot_z]))) >> 16);
        int z = (int) (((((long) (s[2] - obb.pos_z)) * ((long) math.cos[rot_x])) - (((long) y) * ((long) math.sin[rot_x]))) >> 16);
        int x2 = x >> 16;
        int y2 = ((int) (((((long) (s[2] - obb.pos_z)) * ((long) math.sin[rot_x])) + (((long) y) * ((long) math.cos[rot_x]))) >> 16)) >> 16;
        int z2 = z >> 16;
        if (((long) z2) * ((long) planes_[0][2]) <= 0 || (((long) y2) * ((long) planes_[1][1])) + (((long) z2) * ((long) planes_[1][2])) <= ((long) (-ob.dist_h)) || (((long) y2) * ((long) planes_[2][1])) + (((long) z2) * ((long) planes_[2][2])) <= ((long) (-ob.dist_h)) || (((long) x2) * ((long) planes_[3][0])) + (((long) z2) * ((long) planes_[3][2])) <= ((long) (-ob.dist_w)) || (((long) x2) * ((long) planes_[4][0])) + (((long) z2) * ((long) planes_[4][2])) <= ((long) (-ob.dist_w))) {
            return true;
        }
        return false;
    }

    private static final void draw_all(int[][] planes_, int num) {
        int j;
        short[] Polygone;
        int all_poly;
        boolean un;
        int[][] SPR = sprite;
        int[][] SHO = shot;
        int[][] spr_n = N_sprite;
        object[] ob = Objects;
        int[] p = max_point;
        point_[] v = help_v;
        point_[] dts = help_dts;
        point_ v0 = v[0];
        point_ v1 = v[1];
        point_ v2 = v[2];
        point_ dts0 = dts[0];
        int[] sort_i = index_sort;
        int[] sort_polyi = index_sort_poly;
        int obg = obj_0;
        int spr = spr_0;
        int sho = shot_0;
        boolean text2 = false;
        int cos_z = math.cos[rot_z];
        int sin_z = math.sin[rot_z];
        int cos_x = math.cos[rot_x];
        int sin_x = math.sin[rot_x];
        for (int k = num - 1; k >= 0; k--) {
            if (sort_i[k] >= sho) {
                j = sort_i[k] - sho;
                if (SHO[j][9] <= -2000) {
                    object ob1 = object_help;
                    ob1.type_object = (byte) (((-SHO[j][9]) - 2000) + 1);
                    ob1.Poly = OBJ[ob1.type_object].Poly;
                    ob1.Points = OBJ[ob1.type_object].Points;
                    Polygone = ob1.Poly;
                    all_poly = OBJ[ob1.type_object].Poly.length >> 4;
                    ob1.pos_x = SHO[j][0];
                    ob1.pos_y = SHO[j][1];
                    ob1.pos_z = SHO[j][2];
                    ob1.r_z = SHO[j][10];
                    math.rot_OBJ(ob1);
                    un = false;
                } else {
                    if (SHO[j][11] != 0) {
                        poly.draw_shot(SHO[j], planes_);
                    }
                }
            } else {
                if (sort_i[k] >= spr) {
                    int j2 = sort_i[k] - spr;
                    poly.draw_spr(spr_n[SPR[j2][3]][SPR[j2][4] - 1], SPR[j2][0], SPR[j2][1], SPR[j2][2], planes_);
                } else if (sort_i[k] >= obg) {
                    j = sort_i[k] - obg;
                    object ob12 = ob[j];
                    if (ob12.type_object <= 32 || ob12.type_object >= 37) {
                        text2 = ob12.texture;
                        Polygone = ob12.Poly;
                        all_poly = ob12.Poly.length >> 4;
                        if (!clip_pos(planes_, ob12)) {
                            math.rot_OBJ(ob12);
                            alfa_ = ob12.light;
                            alfa_ = ob12.light;
                            if (fire2) {
                                int xx = (x_light - ob12.pos_x) >> 16;
                                int yy = (y_light - ob12.pos_y) >> 16;
                                int xx2 = (int) ((65536 * ((long) ((xx * xx) + (yy * yy)))) >> 28);
                                if (xx2 > 255) {
                                    xx2 = 255;
                                }
                                alfa_ += 255 - xx2;
                                if (alfa_ > 255) {
                                    alfa_ = 255;
                                }
                            }
                            un = false;
                        }
                    }
                } else {
                    j = sort_i[k];
                    Polygone = Units[j].Poly;
                    all_poly = Units[j].Poly.length >> 4;
                    math.rot_UNI(Units[j]);
                    text2 = false;
                    un = true;
                }
            }
            math.sort_poly(Polygone, p);
            for (int i = all_poly - 1; i >= 0; i--) {
                int index = sort_polyi[i] << 4;
                short s2 = Polygone[index + 13];
                short s3 = Polygone[index + 14];
                short s4 = Polygone[index + 15];
                Polygone[index + 13] = (short) (((s2 * cos_z) - (s3 * sin_z)) >> 16);
                Polygone[index + 14] = (short) (((s2 * sin_z) + (s3 * cos_z)) >> 16);
                Polygone[index + 15] = (short) (((s4 * cos_x) - (Polygone[index + 14] * sin_x)) >> 16);
                Polygone[index + 14] = (short) (((s4 * sin_x) + (Polygone[index + 14] * cos_x)) >> 16);
                short s5 = Polygone[index];
                if ((((long) Polygone[index + 13]) * ((long) p[s5])) + (((long) Polygone[index + 14]) * ((long) p[s5 + 1])) + (((long) Polygone[index + 15]) * ((long) p[s5 + 2])) > 0) {
                    v0.rx = p[s5];
                    int rx = s5 + 1;
                    v0.ry = p[rx];
                    v0.rz = p[rx + 1];
                    short s6 = Polygone[index + 1];
                    v1.rx = p[s6];
                    int rx2 = s6 + 1;
                    v1.ry = p[rx2];
                    v1.rz = p[rx2 + 1];
                    short s7 = Polygone[index + 2];
                    v2.rx = p[s7];
                    int rx3 = s7 + 1;
                    v2.ry = p[rx3];
                    v2.rz = p[rx3 + 1];
                    int index2 = index + 3;
                    v0.u = Polygone[index2];
                    int index3 = index2 + 1;
                    v0.v = Polygone[index3];
                    int index4 = index3 + 1;
                    v1.u = Polygone[index4];
                    int index5 = index4 + 1;
                    v1.v = Polygone[index5];
                    int index6 = index5 + 1;
                    v2.u = Polygone[index6];
                    int index7 = index6 + 1;
                    v2.v = Polygone[index7];
                    int index8 = index7 + 1;
                    int clip = portal.clipPlane2(dts, v, planes_);
                    int clip2 = clip - 1;
                    if (clip != 0) {
                        ind = Polygone[index8];
                        if (ind < 50) {
                            if (!text2) {
                                if (!un) {
                                    int clip3 = clip2 - 1;
                                    while (true) {
                                        clip3--;
                                        if (clip3 < 0) {
                                            break;
                                        }
                                        poly.draw_poly_2(dts0, dts[clip3 + 1], dts[clip3 + 2]);
                                    }
                                } else {
                                    int clip4 = clip2 - 1;
                                    while (true) {
                                        clip4--;
                                        if (clip4 < 0) {
                                            break;
                                        }
                                        poly.draw_poly_2_unit(dts0, dts[clip4 + 1], dts[clip4 + 2]);
                                    }
                                }
                            } else {
                                int clip5 = clip2 - 1;
                                while (true) {
                                    clip5--;
                                    if (clip5 < 0) {
                                        break;
                                    }
                                    poly.draw_poly(dts0, dts[clip5 + 1], dts[clip5 + 2]);
                                }
                            }
                        } else {
                            int clip6 = clip2 - 1;
                            while (true) {
                                clip6--;
                                if (clip6 < 0) {
                                    break;
                                }
                                poly.draw_poly_fill(dts0, dts[clip6 + 1], dts[clip6 + 2], false);
                            }
                        }
                    }
                }
            }
            if (un) {
                draw_un(j);
            }
            poly.all_poly = 0;
        }
    }

    public void m3d() {
    }
}
