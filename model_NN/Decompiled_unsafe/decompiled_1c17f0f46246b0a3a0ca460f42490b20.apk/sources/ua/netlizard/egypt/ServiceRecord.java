package ua.netlizard.egypt;

public class ServiceRecord {
    static int AUTHENTICATE_ENCRYPT = 0;
    static int AUTHENTICATE_NOENCRYPT = 1;
    static int NOAUTHENTICATE_NOENCRYPT = 2;

    /* access modifiers changed from: package-private */
    public int[] getAttributeIDs() {
        int[] iArr = new int[3];
        iArr[1] = 1;
        iArr[2] = 2;
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public DataElement getAttributeValue(int attrID) {
        return null;
    }

    /* access modifiers changed from: package-private */
    public String getConnectionURL(int requiredSecurity, boolean mustBeMaster) {
        return "null";
    }

    /* access modifiers changed from: package-private */
    public RemoteDevice getHostDevice() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean populateRecord(int[] attrIDs) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean setAttributeValue(int attrID, DataElement attrValue) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void setDeviceServiceClasses(int classes) {
    }
}
