package ua.netlizard.egypt;

import java.util.HashMap;

public class Manager {
    static String[] content_types = {"audio/x-wav", "audio/mpeg", "audio/midi"};
    static HashMap<String, Player> players = new HashMap<>();

    static Player createPlayer(String name, String type) {
        Player p = players.get(name);
        if (p != null) {
            return p;
        }
        Player p2 = new Player(name);
        players.put(name, p2);
        return p2;
    }

    static String[] getSupportedContentTypes(String protocol) {
        return content_types;
    }
}
