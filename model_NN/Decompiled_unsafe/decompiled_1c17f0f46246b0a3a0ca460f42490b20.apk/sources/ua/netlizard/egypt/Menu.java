package ua.netlizard.egypt;

import com.media.fx.Constants;
import java.lang.reflect.Array;

class Menu {
    static MyFont[] F = null;
    static byte SMS = 0;
    static char a = 13;
    static int active = 0;
    static String add_str = null;
    static int anim = 0;
    static byte[] anim_incon = new byte[3];
    static boolean back = false;
    static short[] c = null;
    static byte[][] cc = null;
    static byte[][] ccc = null;
    static String chit = null;
    static boolean chit_5 = false;
    static boolean chit_fps = false;
    static boolean chit_level = false;
    static boolean chit_life = false;
    static boolean chitalka = false;
    static byte[] command = null;
    static short[] data = null;
    static boolean draw_disp = false;
    static boolean enable = false;
    static Game game = null;
    static int h_14 = (Game.Height / 14);
    static short h_str = 0;
    static int hh1 = 0;
    static Image il0 = null;
    static int last_active = 0;
    static int last_scrol = 0;
    static boolean loading = false;
    static int main_str = 0;
    static final int max_anim = 16;
    static final int max_data = 17;
    static int max_str = 0;
    static short menu_center = 0;
    static short menu_max_size = 0;
    static int menu_str = 0;
    static byte menu_t = -1;
    static byte menu_type = 0;
    static byte najato = 0;
    static String[] name = null;
    static String name_ = "";
    static int old_menu = -1;
    static int scrol = 0;
    static short shift_h = 0;
    static int speed_anim = 4;
    static Image spr_fire = null;
    static Image[] spr_menu = null;
    static short start_h = 0;
    static byte state_load = 0;
    static final int stp = 16;
    static int str_text;
    static int str_text_big;
    static short[] times;
    static byte[] type;
    static int w_14 = (Game.Width / 14);
    static int ww1 = 0;
    static int xx1 = 0;
    static int yy1 = 0;

    static final void load_menu() {
        MyFont F1;
        if (!chitalka) {
            if (F[1] == null) {
                F1 = F[0];
            } else if (menu_type == 2 || menu_type == 6 || menu_type == 7) {
                F1 = F[0];
            } else {
                F1 = F[1];
            }
            int i = m3d.Height >> 4;
            menu_center = (short) (m3d.Height >> 1);
            menu_max_size = (short) (m3d.Height_real_3D - (h_14 << 1));
            h_str = (short) (F1.getHeight() + (m3d.Height >> 4));
            shift_h = (short) ((h_str - F1.getHeight()) >> 1);
            max_str = menu_max_size / h_str;
            if (max_str > menu_str) {
                max_str = menu_str;
            }
            start_h = (short) (menu_center - ((max_str * h_str) >> 1));
            return;
        }
        menu_center = (short) (m3d.Height >> 1);
        menu_max_size = (short) (m3d.Height_real_3D - (h_14 << 1));
        h_str = (short) (F[0].getHeight() + 7);
        shift_h = (short) ((h_str - F[0].getHeight()) >> 1);
        max_str = menu_max_size / h_str;
        if (max_str > menu_str) {
            max_str = menu_str;
        }
        start_h = (short) (menu_center - ((max_str * h_str) >> 1));
    }

    static final void change_menu() {
        if (menu_t != menu_type) {
            menu_t = menu_type;
            change_menu(menu_type);
        }
    }

    static void creat_menu() {
        load_menu();
        if (!chitalka) {
            int len = menu_str * 3;
            int index = 0;
            String[] str_menu = Game.smenu;
            name = new String[menu_str];
            command = new byte[menu_str];
            type = new byte[menu_str];
            for (int i = 0; i < len; i += 3) {
                name[index] = str_menu[add_str.charAt(i)];
                command[index] = (byte) add_str.charAt(i + 1);
                type[index] = (byte) add_str.charAt(i + 2);
                index++;
            }
        }
    }

    static void massage_box() {
        draw_disp = true;
    }

    static void add_menu(int Name, int Exe, int Type) {
        add_str = String.valueOf(add_str) + ((char) Name);
        add_str = String.valueOf(add_str) + ((char) Exe);
        add_str = String.valueOf(add_str) + ((char) Type);
        menu_str++;
    }

    static void menu_boss() {
        add_menu(58, 101, 0);
        add_menu(59, 102, 0);
        add_menu(60, 103, 0);
    }

    static void menu_context() {
        main_str = -1;
        add_menu(3, 6, 0);
        add_menu(24, max_data, 0);
        add_menu(16, 0, 1);
        add_menu(41, 0, 4);
        if (Game.accelerate) {
            add_menu(30, 0, 5);
        }
        add_menu(50, 0, 6);
        add_menu(25, 12, 0);
        add_menu(10, 13, 0);
    }

    static void game_level() {
        main_str = 21;
        add_menu(19, 10, 98);
        add_menu(20, 10, 99);
    }

    static void menu_option() {
        main_str = 7;
        add_menu(16, 0, 1);
        add_menu(max_data, 0, 2);
        add_menu(50, 0, 6);
        add_menu(44, 0, 3);
        add_menu(41, 0, 4);
        add_menu(42, 15, 0);
        add_menu(23, 0, 0);
    }

    static void menu_main() {
        main_str = 14;
        add_menu(2, 1, 0);
        if (Game.demo) {
            add_menu(4, 20, 0);
        }
        if (Game.open_level > 0) {
            add_menu(3, 9, 120);
        }
        if (Game.open_level > 0 || chit_level) {
            add_menu(6, 7, 0);
        }
        add_menu(27, 18, 0);
        add_menu(28, 19, 0);
        add_menu(7, 2, 0);
        add_menu(8, 4, 0);
        add_menu(9, 5, 0);
        add_menu(10, 14, 0);
    }

    static void menu_difficult() {
        main_str = 22;
        add_menu(19, 9, 10);
        add_menu(20, 9, 11);
        add_menu(21, 9, 12);
        add_menu(23, 0, 0);
    }

    static void menu_buy() {
    }

    static void menu_more_game() {
    }

    static void in_menu_from_context() {
        main_str = 47;
        add_menu(0, 3, 0);
        add_menu(1, 10, 0);
    }

    static void exit_game_from_context() {
        main_str = 47;
        add_menu(0, 3, 0);
        add_menu(1, 11, 0);
    }

    static void exit_game_from_main() {
        main_str = 47;
        add_menu(0, 0, 0);
        add_menu(1, 11, 0);
    }

    static void menu_help() {
        main_str = 8;
        name = text_read("he");
        menu_str = name.length;
        chitalka = true;
    }

    static void menu_abc() {
    }

    static void menu_score() {
        main_str = 28;
    }

    static void menu_about() {
        main_str = 9;
        name = text_read("ab");
        menu_str = name.length;
        chitalka = true;
    }

    static void start_sound() {
        main_str = 15;
        add_menu(0, 0, -1);
        add_menu(1, 0, -2);
    }

    static void sbros() {
        main_str = 43;
        add_menu(0, 2, 0);
        add_menu(1, 16, 0);
    }

    static void menu_sbrosilis() {
        main_str = 48;
        add_menu(23, 2, 0);
    }

    private static final String[] text_read(String name2) {
        return Uni.uni.sorter(Uni.uni.load_string(name2), F[0], (Game.Width - (w_14 << 1)) - 1, true);
    }

    private static void creat_levels() {
        if (chit_level) {
            for (int i = 0; i < max_data; i++) {
                if (i < 10) {
                    add_menu(i + 31, 9, i + 100);
                } else {
                    add_menu(i + 31 + 10, 9, i + 100);
                }
            }
        } else {
            int o = Game.open_level;
            if (o > 15) {
                o = 15;
            }
            for (int i2 = 0; i2 < o; i2++) {
                if (i2 < 10) {
                    add_menu(i2 + 31, 9, i2 + 100);
                } else {
                    add_menu(i2 + 31 + 10, 9, i2 + 100);
                }
            }
        }
        add_menu(23, 0, 0);
    }

    static void change_menu(byte type1) {
        if (najato < 0) {
            active = last_active;
            scrol = last_scrol;
        } else {
            active = 0;
            scrol = 0;
        }
        name = null;
        command = null;
        type = null;
        menu_str = 0;
        add_str = "";
        chitalka = false;
        switch (type1) {
            case 0:
                start_sound();
                break;
            case 1:
                menu_main();
                break;
            case 2:
                menu_context();
                break;
            case 3:
                menu_option();
                break;
            case 4:
                creat_levels();
                break;
            case 5:
                menu_difficult();
                break;
            case 6:
                in_menu_from_context();
                break;
            case 7:
                exit_game_from_context();
                break;
            case 8:
                exit_game_from_main();
                break;
            case 9:
                sbros();
                break;
            case 10:
                massage_box();
                return;
            case 12:
                menu_buy();
                break;
            case Constants.RMS_store_recordID_SubscribtionMode:
                menu_more_game();
                break;
            case Constants.RMS_store_recordID_DemoModeElapsed:
                menu_help();
                break;
            case 15:
                menu_about();
                break;
            case 16:
                menu_sbrosilis();
                break;
            case max_data /*17*/:
                menu_boss();
                m3d.context_();
                break;
            case 18:
                menu_abc();
                break;
            case 19:
                menu_score();
                break;
        }
        creat_menu();
        if (last_active >= menu_str || scrol + max_str >= menu_str) {
            active = 0;
            scrol = 0;
            if (type1 == 5) {
                active = 1;
            }
        }
    }

    static final void start_icon(byte next) {
        if (menu_type != 2) {
            if ((menu_type >= 0 && menu_type <= 9) || menu_type == 20) {
                anim_incon[next] = 2;
            }
            switch (menu_type) {
                case Constants.RMS_store_recordID_DemoModeElapsed:
                    anim_incon[2] = 2;
                    return;
                case 15:
                    anim_incon[2] = 2;
                    return;
                default:
                    return;
            }
        }
    }

    static void fire() {
        boolean z;
        boolean z2 = false;
        najato = 1;
        if (menu_type == 19) {
            if (m3d.Units.length != 1) {
                menu_type = 2;
                Game.Pause = false;
                enable = false;
                m3d.restart(Game.my_level);
                return;
            }
            start_icon((byte) 0);
            menu_type = 1;
        } else if (menu_type != 18) {
            start_icon((byte) 0);
            if (chitalka) {
                if (chit_5 && menu_type == 15 && chit != null && chit != "") {
                    if (chit.charAt(0) == '#' && chit.charAt(1) == '2' && chit.charAt(2) == '5') {
                        chit_level = !chit_level;
                    }
                    if (chit.charAt(0) == '#' && chit.charAt(1) == '7' && chit.charAt(2) == '5') {
                        if (chit_fps) {
                            z = false;
                        } else {
                            z = true;
                        }
                        chit_fps = z;
                    }
                    if (chit.charAt(0) == '#' && chit.charAt(1) == '8' && chit.charAt(2) == '5') {
                        if (!chit_life) {
                            z2 = true;
                        }
                        chit_life = z2;
                    }
                }
                key_pound();
            } else if (menu_type == 10) {
                from_menu();
            } else {
                if (!(type[active] == 0 || command[active] == 10)) {
                    switch (type[active]) {
                        case -3:
                            Game.cikl_rn = false;
                            break;
                        case -2:
                            Game.sound_enable = true;
                            Game.music();
                            break;
                        case -1:
                            Game.sound_enable = false;
                            Snd2.volume = 0;
                            break;
                        case 1:
                            move_right();
                            return;
                        case 2:
                            if (!Game.vibro_enable) {
                                z2 = true;
                            }
                            Game.vibro_enable = z2;
                            if (Game.vibro_enable) {
                                Game.vibra();
                                return;
                            }
                            return;
                        case 3:
                            if (!Game.podstilka_snd) {
                                z2 = true;
                            }
                            Game.podstilka_snd = z2;
                            return;
                        case 4:
                            Game.sens = (byte) (Game.sens + 1);
                            if (Game.sens > Game.max_sens) {
                                Game.sens = 0;
                                return;
                            }
                            return;
                        case 5:
                            if (!Game.accel_on) {
                                z2 = true;
                            }
                            Game.accel_on = z2;
                            return;
                        case 6:
                            if (!Game.inert) {
                                z2 = true;
                            }
                            Game.inert = z2;
                            return;
                    }
                }
                last_active = active;
                last_scrol = scrol;
                byte b = command[active];
                old_menu = menu_type;
                anim = 16;
                switch (b) {
                    case 0:
                        menu_type = 1;
                        return;
                    case 1:
                        menu_type = 5;
                        return;
                    case 2:
                        menu_type = 3;
                        return;
                    case 3:
                        menu_type = 2;
                        return;
                    case 4:
                        menu_type = 14;
                        return;
                    case 5:
                        menu_type = 15;
                        chit = "";
                        return;
                    case 6:
                        from_menu();
                        return;
                    case 7:
                        menu_type = 4;
                        return;
                    case 9:
                        Game.deathmach = false;
                        if (Game.load_scene) {
                            loading = true;
                            state_load = 0;
                            m3d.delete_m3d();
                            loading = false;
                        }
                        int level = 1;
                        m3d.weapon_now = 0;
                        if (type[active] == 120) {
                            m3d.load_rm(false, false);
                            level = Game.my_level;
                        }
                        if (type[active] >= 100 && type[active] != 120) {
                            level = active + 1;
                            Game.my_level = (byte) level;
                            m3d.load_rm(true, false);
                        }
                        if (type[active] <= 12 && type[active] >= 10) {
                            AI.difficult = (byte) (type[active] - 10);
                            m3d.weap_r();
                        }
                        if (!Game.demo || Game.my_level <= 1) {
                            Game.my_level = (byte) level;
                            loading = true;
                            state_load = 0;
                            Game.kill_font();
                            m3d.load_m3d(level);
                            loading = false;
                            menu_type = 2;
                            Game.Pause = false;
                            enable = false;
                            return;
                        }
                        Game.gg = false;
                        Secur_Dem.startBuy();
                        return;
                    case 10:
                        back_menu();
                        return;
                    case J2MECanvas.GAME_C:
                        Game.cikl_rn = false;
                        return;
                    case 12:
                        menu_type = 6;
                        return;
                    case Constants.RMS_store_recordID_SubscribtionMode:
                        menu_type = 7;
                        return;
                    case Constants.RMS_store_recordID_DemoModeElapsed:
                        menu_type = 8;
                        return;
                    case 15:
                        menu_type = 9;
                        return;
                    case 16:
                        menu_type = 16;
                        for (int i = 0; i < Uni.buffer2.length; i++) {
                            Uni.buffer2[i] = 0;
                        }
                        Uni.rmsWrite = true;
                        Uni.fl_write();
                        return;
                    case max_data /*17*/:
                        loading = true;
                        state_load = 0;
                        m3d.restart(Game.my_level);
                        loading = false;
                        Game.Pause = false;
                        enable = false;
                        return;
                    case 18:
                        m3d.weap_r();
                        Game.deathmach = true;
                        AI.difficult = 1;
                        loading = true;
                        state_load = 0;
                        Game.kill_font();
                        m3d.load_m3d(1);
                        loading = false;
                        menu_type = 2;
                        Game.Pause = false;
                        enable = false;
                        return;
                    case 19:
                        menu_type = 19;
                        return;
                    case 20:
                        Game.gg = false;
                        Secur_Dem.startBuy();
                        return;
                    case 101:
                        M((byte) 1);
                        return;
                    case 102:
                        M((byte) 2);
                        return;
                    case 103:
                        M((byte) 4);
                        return;
                    default:
                        return;
                }
            }
        } else if (active == 29) {
            if (name_.length() != 0 && name_ != "") {
                name_ = name_.substring(0, name_.length() - 1);
            }
        } else if (name_.length() <= 9) {
            char b2 = (char) (active + 97);
            if (active == 26) {
                b2 = ' ';
            }
            if (active == 27) {
                b2 = '(';
            }
            if (active == 28) {
                b2 = ')';
            }
            name_ = String.valueOf(name_) + b2;
        }
    }

    static final void back_menu() {
        back = true;
        enable = true;
        Game.Pause = true;
        Game.deathmach = false;
        loading = true;
        state_load = 0;
        m3d.delete_m3d();
        Game.load_font();
        if (Game.load_scene) {
            m3d.load_m3d(0);
        }
        menu_type = 1;
        loading = false;
        Game.sound_stop();
        Game.music();
        back = false;
    }

    static final void from_menu() {
        boolean z;
        boolean z2 = false;
        if (m3d.text) {
            menu_type = 2;
            m3d.text = false;
            Game.Pause = false;
            enable = false;
            return;
        }
        if (Game.my_level == 16) {
            switch (SMS) {
                case 1:
                    m3d.next_level();
                    menu_type = menu_type;
                    m3d.text = false;
                    SMS = 0;
                    return;
                case 2:
                    SMS = 3;
                    Game.sss = true;
                    m3d.draw2D(m3d.ppp, m3d.room_now);
                    m3d.draw_3d(m3d.draw_loop);
                    m3d.context_();
                    return;
                case 3:
                    m3d.end_all();
                    m3d.end();
                    break;
                case 4:
                    SMS = 5;
                    Game.sss = true;
                    m3d.draw2D(m3d.ppp, m3d.room_now);
                    m3d.draw_3d(m3d.draw_loop);
                    m3d.context_();
                    return;
                case 6:
                    m3d.end();
                    break;
            }
        }
        if (menu_type == 10) {
            if (Game.my_level == max_data) {
                m3d.end();
                return;
            } else {
                scr.enable_script = true;
                menu_type = 2;
            }
        }
        if (menu_type == 2) {
            if (Game.Pause) {
                z = false;
            } else {
                z = true;
            }
            Game.Pause = z;
            if (!enable) {
                z2 = true;
            }
            enable = z2;
            m3d.context_();
        }
    }

    static void M(byte message) {
        menu_type = 10;
        Game.Pause = true;
        enable = true;
        SMS = message;
        scrol = 0;
    }

    static boolean if_need_score(boolean write) {
        int t = Game.time_game / FullCanvas.canvasRealWidth;
        int min = t / 60;
        int sec = t % 60;
        int i = 0;
        while (true) {
            if (i >= 5) {
                break;
            } else if (((Uni.buffer2[(i * 12) + 330 + 10] & 255) * 60) + (Uni.buffer2[(i * 12) + 330 + 11] & 255) >= t) {
                i++;
            } else if (!write) {
                return true;
            } else {
                for (int j = 4; j > i; j--) {
                    for (int b = 0; b < 12; b++) {
                        Uni.buffer2[(j * 12) + 330 + b] = Uni.buffer2[((j - 1) * 12) + 330 + b];
                    }
                }
                for (int j2 = 0; j2 < 12; j2++) {
                    Uni.buffer2[(i * 12) + 330 + j2] = 0;
                }
                int len = name_.length();
                if (len > 10) {
                    len = 10;
                }
                for (int j3 = 0; j3 < len; j3++) {
                    int tmp = name_.charAt(j3);
                    if (tmp >= 848) {
                        tmp -= 848;
                    }
                    Uni.buffer2[(i * 12) + 330 + j3] = (byte) tmp;
                }
                Uni.buffer2[(i * 12) + 330 + 10] = (byte) min;
                Uni.buffer2[(i * 12) + 330 + 11] = (byte) sec;
                Uni.rmsWrite = true;
                Uni.fl_write();
            }
        }
        return false;
    }

    static void key_pound() {
        if (menu_type == 18) {
            menu_type = 19;
            if_need_score(true);
        } else if (scr.enable_script) {
            scr.skip_scr();
        } else {
            start_icon((byte) 1);
            if (Game.my_level == max_data) {
                from_menu();
                return;
            }
            from_menu();
            switch (menu_type) {
                case 0:
                case 3:
                case 4:
                case 5:
                case 8:
                case Constants.RMS_store_recordID_DemoModeElapsed:
                case 15:
                    menu_type = 1;
                    return;
                case 1:
                    menu_type = 8;
                    return;
                case 2:
                case 9:
                case 10:
                case J2MECanvas.GAME_C:
                case 12:
                case Constants.RMS_store_recordID_SubscribtionMode:
                default:
                    return;
                case 6:
                case 7:
                    menu_type = 2;
                    return;
            }
        }
    }

    static final void move_down() {
        if (move_delay()) {
            if (menu_type == 18) {
                active += 6;
                if (active > 29) {
                    active -= 6;
                }
            } else if (menu_type == 10) {
                scrol++;
            } else if (!chitalka) {
                active++;
                if (active >= menu_str) {
                    active = 0;
                    scrol = 0;
                }
                if (active >= max_str + scrol) {
                    scrol++;
                }
            } else if (max_str < menu_str && max_str + scrol < menu_str) {
                scrol++;
            }
        }
    }

    static final boolean move_delay() {
        if (Game.DOWN <= 0) {
            Game.DOWN = 13;
        } else if (Game.DOWN != 10) {
            Game.DOWN = (byte) (Game.DOWN - 1);
            return false;
        }
        return true;
    }

    static final void move_up() {
        if (move_delay()) {
            if (menu_type == 18) {
                active -= 6;
                if (active < 0) {
                    active += 6;
                }
            } else if (menu_type == 10) {
                if (scrol > 0) {
                    scrol--;
                }
            } else if (!chitalka) {
                active--;
                if (active < 0) {
                    active = menu_str - 1;
                    scrol = menu_str - max_str;
                }
                if (active < scrol) {
                    scrol--;
                }
            } else if (scrol > 0) {
                scrol--;
            }
        }
    }

    static final void move_left() {
        boolean z;
        boolean z2 = false;
        if (menu_type == 18) {
            active--;
            if (active < 0) {
                active = 0;
            }
        } else if (type != null && type.length != 0) {
            if (type[active] == 1) {
                if (Snd2.volume != 0) {
                    Snd2.volume -= 10;
                    if (Snd2.volume == 0) {
                        Game.sound_enable = false;
                        Game.sound_stop();
                    }
                    Snd2.setVolume();
                } else {
                    Game.sound_enable = true;
                    Game.music();
                    Snd2.volume = 100;
                    Snd2.setVolume();
                }
            }
            if (type[active] == 2) {
                if (Game.vibro_enable) {
                    z = false;
                } else {
                    z = true;
                }
                Game.vibro_enable = z;
            }
            if (type[active] == 3) {
                if (!Game.podstilka_snd) {
                    z2 = true;
                }
                Game.podstilka_snd = z2;
            }
            if (type[active] == 4) {
                Game.sens = (byte) (Game.sens - 1);
                if (Game.sens < 0) {
                    Game.sens = Game.max_sens;
                }
            }
        }
    }

    static final void move_right() {
        boolean z;
        boolean z2 = true;
        if (menu_type == 18) {
            active++;
            if (active > 29) {
                active = 29;
            }
        } else if (type != null && type.length != 0) {
            if (type[active] == 1) {
                if (Snd2.volume != 100) {
                    Snd2.volume += 10;
                    if (Snd2.volume == 10) {
                        Game.sound_enable = true;
                        Game.music();
                    }
                    Snd2.setVolume();
                } else {
                    Game.sound_enable = false;
                    Snd2.volume = 0;
                    Snd2.setVolume();
                    Game.sound_stop();
                }
            }
            if (type[active] == 2) {
                if (Game.vibro_enable) {
                    z = false;
                } else {
                    z = true;
                }
                Game.vibro_enable = z;
            }
            if (type[active] == 3) {
                if (Game.podstilka_snd) {
                    z2 = false;
                }
                Game.podstilka_snd = z2;
            }
            if (type[active] == 4) {
                Game.sens = (byte) (Game.sens + 1);
                if (Game.sens > Game.max_sens) {
                    Game.sens = 0;
                }
            }
        }
    }

    static final void draw_ok(Graphics g, boolean active2) {
        int i;
        int y = m3d.Height - ((cc[0][3] + 30) >> 1);
        if (active2) {
            i = 1;
        } else {
            i = 0;
        }
        draw_spr(g, i, 0, y);
        draw_spr(g, 6, cc[0][2] + 0, ((cc[0][3] >> 1) + y) - (cc[6][3] >> 1));
    }

    static final void draw_cancel(Graphics g, boolean active2) {
        int i;
        int y = m3d.Height - ((cc[2][3] + 30) >> 1);
        int x = m3d.Width - cc[2][2];
        if (active2) {
            i = 3;
        } else {
            i = 2;
        }
        draw_spr(g, i, x, y);
        draw_spr(g, 6, x - cc[6][2], ((cc[0][3] >> 1) + y) - (cc[6][3] >> 1));
    }

    static final void draw_turn(Graphics g, boolean active2) {
        int i = 4;
        int y = m3d.Height - ((cc[4][3] + 30) >> 1);
        int x = m3d.Width - cc[4][2];
        if (active2) {
            i = 5;
        }
        draw_spr(g, i, x, y);
        draw_spr(g, 6, x - cc[6][2], ((cc[0][3] >> 1) + y) - (cc[6][3] >> 1));
    }

    static void draw_menu_info(Graphics g) {
        g.setColor(0);
        g.fillRect(0, 0, Game.Width, 30);
        if (main_str >= 0) {
            F[0].drawString(g, Game.smenu[main_str], (Game.Width - F[0].stringWidth(Game.smenu[main_str])) >> 1, (30 - F[0].getHeight()) >> 1, 20);
        }
    }

    static void draw_active(Graphics g, int h) {
        g.setColor(0);
        for (int i = 0; i < h_str; i++) {
            if ((i & 1) == 0) {
                g.drawLine(0, h, Game.Width, h);
            }
            h++;
        }
    }

    static void draw_menu(Graphics g) {
        MyFont F1;
        boolean z;
        int str_len;
        boolean z2 = true;
        boolean gg = false;
        if (F[1] == null) {
            gg = true;
            F1 = F[0];
        } else if (menu_type == 2 || menu_type == 6 || menu_type == 7) {
            gg = true;
            F1 = F[0];
        } else {
            F1 = F[1];
        }
        String[] strArr = name;
        draw_menu_info(g);
        if (anim_incon[0] == 0) {
            z = false;
        } else {
            z = true;
        }
        draw_ok(g, z);
        if (menu_type == 18) {
            draw_abc(g);
        } else if (menu_type == 19) {
            draw_score(g);
        } else {
            if (anim_incon[2] == 0) {
                if (anim_incon[1] == 0) {
                    z2 = false;
                }
                draw_cancel(g, z2);
            } else {
                draw_turn(g, true);
            }
            if (menu_type == 2 || menu_type == 6 || menu_type == 7 || menu_type == max_data) {
                g.setColor(13002497);
                g.fillRect(0, start_h + ((active - scrol) * h_str), m3d.Width, h_str);
            }
            for (int i = scrol; i < max_str + scrol; i++) {
                String str = name[i];
                if (type[i] != 0) {
                    switch (type[i]) {
                        case 1:
                            str = String.valueOf(str) + " " + ((Snd2.volume == 0 || !Game.sound_enable) ? Game.smenu[12] : String.valueOf(Snd2.volume) + "%");
                            break;
                        case 2:
                            str = String.valueOf(str) + " " + (Game.vibro_enable ? Game.smenu[11] : Game.smenu[12]);
                            break;
                        case 3:
                            str = String.valueOf(str) + " " + (!Game.podstilka_snd ? Game.smenu[45] : Game.smenu[46]);
                            break;
                        case 4:
                            str = String.valueOf(str) + " " + ((int) Game.sens);
                            break;
                        case 5:
                            str = String.valueOf(str) + " " + (Game.accel_on ? Game.smenu[11] : Game.smenu[12]);
                            break;
                        case 6:
                            str = String.valueOf(str) + " " + (Game.inert ? Game.smenu[11] : Game.smenu[12]);
                            break;
                    }
                }
                if (i == active) {
                    draw_active(g, start_h + ((active - scrol) * h_str));
                }
                if (!gg) {
                    str_len = (Game.Width - F1.stringWidth(str)) >> 1;
                } else {
                    str_len = w_14;
                }
                F1.drawString(g, str, str_len, shift_h + start_h + ((i - scrol) * h_str), 20);
            }
            int start = Game.Width_2 - 4;
            if (scrol > 0) {
                g.setColor(16777215);
                g.fillTriangle(start, start_h, start + 8, start_h, start + 4, start_h - 8);
            }
            if (max_str + scrol < menu_str) {
                g.setColor(16777215);
                g.fillTriangle(start, Game.Height - start_h, start + 8, Game.Height - start_h, start + 4, (Game.Height - start_h) + 8);
            }
        }
    }

    static void draw_chitalka(Graphics g) {
        g.setColor(0);
        for (int p = 0; p < m3d.Height_real_3D; p += 2) {
            g.fillRect(0, p + 30, m3d.Width, 1);
        }
        draw_menu_info(g);
        draw_turn(g, anim_incon[2] != 0);
        for (int i = scrol; i < max_str + scrol; i++) {
            if (name[i].length() <= 0 || name[i].charAt(0) != '|') {
                F[0].drawString(g, name[i], w_14, shift_h + start_h + ((i - scrol) * h_str), 20);
            } else {
                F[0].drawSubstring(g, name[i], 1, name[i].length() - 1, (Game.Width - F[0].stringWidth(name[i])) >> 1, shift_h + start_h + ((i - scrol) * h_str), 20);
            }
        }
        int H = max_str * h_str;
        int w_scrol = w_14 >> 1;
        int x_scrol = (w_14 - w_scrol) >> 1;
        int scrol_n = scrol;
        int max_scrol = menu_str - max_str;
        int h_scrol = (max_str * H) / menu_str;
        int y_scrol = start_h + (((H - h_scrol) * scrol_n) / max_scrol);
        int x_scrol2 = (Game.Width - x_scrol) - w_scrol;
        g.setColor(16711680);
        g.fillRect(x_scrol2, y_scrol, w_scrol, h_scrol);
        g.setColor(0);
        g.drawRect(x_scrol2, y_scrol, w_scrol, h_scrol);
        g.setColor(16711680);
        draw_my_triangl(g, x_scrol2, start_h - 9, 9, 9);
        draw_my_triang2(g, x_scrol2, (Game.Height - start_h) + 3, 9, 9);
    }

    static void draw_my_triangl(Graphics g, int x, int y, int width, int height) {
        int half_w = width >> 1;
        int half_h = height >> 1;
        int half_y = y + half_h;
        int x2 = x + half_w;
        int a2 = 0;
        g.setColor(16711680);
        for (int i = y; i < half_y; i++) {
            g.drawLine(x2 - a2, y, x2 + a2, y);
            half_w++;
            a2++;
            y++;
        }
        g.fillRect(x2 - a2, y, width, half_h);
    }

    static void draw_my_triang2(Graphics g, int x, int y, int width, int height) {
        int half_w = width >> 1;
        int half_h = height >> 1;
        int i = y + half_h;
        g.setColor(16711680);
        g.fillRect(x, y, width, half_h);
        int x2 = x + half_w;
        int a2 = half_h - 1;
        int y2 = y + half_h;
        for (int i2 = ((half_h * 2) + y2) - 1; i2 >= y2; i2--) {
            g.drawLine(x2 - a2, y2, x2 + a2, y2);
            half_w++;
            a2--;
            y2++;
        }
    }

    /* JADX WARN: Type inference failed for: r0v5, types: [int] */
    /* JADX WARN: Type inference failed for: r0v7, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void draw_score(ua.netlizard.egypt.Graphics r19) {
        /*
            r1 = 6
            byte[][] r2 = ua.netlizard.egypt.Menu.cc
            r4 = 0
            r2 = r2[r4]
            r4 = 2
            byte r2 = r2[r4]
            int r4 = ua.netlizard.egypt.m3d.Height
            byte[][] r5 = ua.netlizard.egypt.Menu.cc
            r6 = 0
            r5 = r5[r6]
            r6 = 3
            byte r5 = r5[r6]
            int r5 = r5 + 30
            int r5 = r5 >> 1
            int r4 = r4 - r5
            byte[][] r5 = ua.netlizard.egypt.Menu.cc
            r6 = 0
            r5 = r5[r6]
            r6 = 3
            byte r5 = r5[r6]
            int r5 = r5 >> 1
            int r4 = r4 + r5
            byte[][] r5 = ua.netlizard.egypt.Menu.cc
            r6 = 6
            r5 = r5[r6]
            r6 = 3
            byte r5 = r5[r6]
            int r5 = r5 >> 1
            int r4 = r4 - r5
            r0 = r19
            draw_spr(r0, r1, r2, r4)
            r1 = 0
            r2 = 0
            int r4 = ua.netlizard.egypt.m3d.Height
            byte[][] r5 = ua.netlizard.egypt.Menu.cc
            r6 = 0
            r5 = r5[r6]
            r6 = 3
            byte r5 = r5[r6]
            int r5 = r5 + 30
            int r5 = r5 >> 1
            int r4 = r4 - r5
            r0 = r19
            draw_spr(r0, r1, r2, r4)
            ua.netlizard.egypt.MyFont[] r1 = ua.netlizard.egypt.Menu.F
            r2 = 0
            r1 = r1[r2]
            int r1 = r1.getHeight()
            ua.netlizard.egypt.MyFont[] r2 = ua.netlizard.egypt.Menu.F
            r4 = 0
            r2 = r2[r4]
            int r2 = r2.getHeight()
            int r2 = r2 >> 1
            int r10 = r1 + r2
            int r1 = ua.netlizard.egypt.Game.Height
            int r2 = r10 * 5
            int r1 = r1 - r2
            int r17 = r1 >> 1
            ua.netlizard.egypt.MyFont[] r1 = ua.netlizard.egypt.Menu.F
            r2 = 0
            r1 = r1[r2]
            int r1 = r1.getHeight()
            int r1 = r10 - r1
            int r16 = r1 >> 1
            r11 = 0
        L_0x0074:
            r1 = 5
            if (r11 < r1) goto L_0x0078
            return
        L_0x0078:
            java.lang.String r3 = ""
            java.lang.String r14 = ""
            r12 = 0
        L_0x007d:
            r1 = 10
            if (r12 < r1) goto L_0x010d
        L_0x0081:
            byte[] r1 = ua.netlizard.egypt.Uni.buffer2
            int r2 = r11 * 12
            int r2 = r2 + 330
            int r2 = r2 + 10
            byte r1 = r1[r2]
            r13 = r1 & 255(0xff, float:3.57E-43)
            byte[] r1 = ua.netlizard.egypt.Uni.buffer2
            int r2 = r11 * 12
            int r2 = r2 + 330
            int r2 = r2 + 11
            byte r1 = r1[r2]
            r15 = r1 & 255(0xff, float:3.57E-43)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r1 = 9
            if (r13 <= r1) goto L_0x0144
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r13)
            java.lang.String r1 = r1.toString()
        L_0x00ac:
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r2.<init>(r1)
            r1 = 58
            java.lang.StringBuilder r2 = r2.append(r1)
            r1 = 9
            if (r15 <= r1) goto L_0x0155
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r15)
            java.lang.String r1 = r1.toString()
        L_0x00ca:
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r14 = r1.toString()
            ua.netlizard.egypt.MyFont[] r1 = ua.netlizard.egypt.Menu.F
            r2 = 0
            r1 = r1[r2]
            int r4 = ua.netlizard.egypt.Menu.w_14
            int r2 = r17 + r16
            int r5 = r11 * r10
            int r5 = r5 + r2
            r6 = 20
            r2 = r19
            r1.drawString(r2, r3, r4, r5, r6)
            ua.netlizard.egypt.MyFont[] r1 = ua.netlizard.egypt.Menu.F
            r2 = 0
            r4 = r1[r2]
            int r1 = ua.netlizard.egypt.Game.Width
            ua.netlizard.egypt.MyFont[] r2 = ua.netlizard.egypt.Menu.F
            r5 = 0
            r2 = r2[r5]
            int r2 = r2.stringWidth(r14)
            int r1 = r1 - r2
            int r2 = ua.netlizard.egypt.Menu.w_14
            int r1 = r1 - r2
            int r7 = r1 + -1
            int r1 = r17 + r16
            int r2 = r11 * r10
            int r8 = r1 + r2
            r9 = 20
            r5 = r19
            r6 = r14
            r4.drawString(r5, r6, r7, r8, r9)
            int r11 = r11 + 1
            goto L_0x0074
        L_0x010d:
            byte[] r1 = ua.netlizard.egypt.Uni.buffer2
            int r2 = r11 * 12
            int r2 = r2 + 330
            int r2 = r2 + r12
            byte r18 = r1[r2]
            if (r18 == 0) goto L_0x0081
            if (r18 >= 0) goto L_0x0120
            r0 = r18
            int r0 = r0 + 256
            r18 = r0
        L_0x0120:
            r1 = 192(0xc0, float:2.69E-43)
            r0 = r18
            if (r0 < r1) goto L_0x012c
            r0 = r18
            int r0 = r0 + 848
            r18 = r0
        L_0x012c:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = java.lang.String.valueOf(r3)
            r1.<init>(r2)
            r0 = r18
            char r2 = (char) r0
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r3 = r1.toString()
            int r12 = r12 + 1
            goto L_0x007d
        L_0x0144:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r4 = "0"
            r1.<init>(r4)
            java.lang.StringBuilder r1 = r1.append(r13)
            java.lang.String r1 = r1.toString()
            goto L_0x00ac
        L_0x0155:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r4 = "0"
            r1.<init>(r4)
            java.lang.StringBuilder r1 = r1.append(r15)
            java.lang.String r1 = r1.toString()
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.Menu.draw_score(ua.netlizard.egypt.Graphics):void");
    }

    static void draw_abc(Graphics g) {
        g.setColor(0);
        g.fillRect(0, 0, Game.Width, 30);
        g.fillRect(0, Game.Height - 30, Game.Width, 30);
        g.setColor(10027008);
        g.fillRect(0, 30, Game.Width, Game.Height - 60);
        draw_spr(g, 0, Game.Width - cc[0][2], m3d.Height - ((cc[0][3] + 30) >> 1));
        MyFont f = F[0];
        char a2 = 'a';
        int w = ((Game.Width >> 1) + (Game.Width >> 2)) / 6;
        int start = (Game.Width - ((Game.Width >> 1) + (Game.Width >> 2))) >> 1;
        int h = f.getHeight() * 2;
        int start_y = Game.Height_2 - ((h * 5) >> 1);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 6; j++) {
                char b = a2;
                if (a2 == '~') {
                    b = 130;
                }
                if (a2 == '{') {
                    b = '_';
                }
                if (a2 == '|') {
                    b = '(';
                }
                if (a2 == '}') {
                    b = ')';
                }
                int i2 = start + (w * j);
                int i3 = (h * i) + start_y + h;
                f.drawChar(g, b, (w * j) + start, (h * i) + start_y + h);
                a2 = (char) (a2 + 1);
            }
        }
        g.setColor(65280);
        char b2 = (char) (active + 1040);
        if (active == 29) {
            b2 = 130;
        }
        if (active == 26) {
            b2 = '_';
        }
        if (active == 27) {
            b2 = '(';
        }
        if (active == 28) {
            b2 = ')';
        }
        g.setColor(16777215);
        g.fillRect(((active % 6) * w) + start, ((active / 6) * h) + start_y + h, f.charWidth(b2), 2);
        f.drawString(g, name_, (Game.Width - f.stringWidth(name_)) >> 1, (start_y - h) - 1, 20);
    }

    static final void draw_game_m(Graphics g) {
        Game.draw_fil_line(g, false);
        draw_cancel(g, false);
        String[] mm = m3d.msg_str[SMS];
        int len_str = mm.length;
        int H = str_text;
        int H_text = len_str * H;
        int start_y = (Game.Height - H_text) >> 1;
        int W_text = (Game.Width >> 1) + (Game.Width >> 2);
        int start_x = (Game.Width - W_text) >> 1;
        int h_scrol = 0;
        int y_scrol = 0;
        boolean scrolilg = false;
        if (H_text > (Game.Height >> 2)) {
            scrolilg = true;
            len_str = (Game.Height >> 2) / H;
            H_text = len_str * H;
            start_y = (Game.Height - H_text) >> 1;
            if (scrol + len_str >= mm.length) {
                scrol = mm.length - len_str;
            }
            h_scrol = (len_str * H_text) / mm.length;
            y_scrol = start_y + ((scrol * (H_text - h_scrol)) / (mm.length - len_str));
        }
        int xx = 0;
        if (scrolilg) {
            xx = 0 + 10;
        }
        int aa = 8216655;
        if (Game.my_level == 16) {
            if (SMS == 0 || SMS == 3 || SMS == 5) {
                aa = 16711680;
            } else {
                aa = 65280;
            }
        }
        g.setColor(aa);
        g.drawRect((start_x - 2) - 1, (start_y - 2) - 1, W_text + 6, H_text + 6 + 2);
        xx1 = (start_x - 2) - 1;
        yy1 = (start_y - 2) - 1;
        ww1 = W_text + 6;
        hh1 = H_text + 6 + 2;
        g.setColor(0);
        g.fillRect(start_x - 2, start_y - 2, W_text + 4 + 1, H_text + 4 + 2 + 1);
        if (scrolilg) {
            g.setColor(aa);
            g.fillRect((((start_x + W_text) + (xx >> 1)) - 1) + 1, (start_y - 2) - 1, 8, H_text + 6 + 2 + 1);
        }
        g.setColor(0);
        g.fillRect(start_x + W_text + (xx >> 1) + 1, y_scrol, 6, h_scrol);
        if (!scrolilg) {
            for (int i = 0; i < len_str; i++) {
                F[0].drawString(g, mm[i], (Game.Width - F[0].stringWidth(mm[i])) >> 1, start_y, 20);
                start_y += H;
            }
        } else {
            for (int i2 = scrol; i2 < scrol + len_str; i2++) {
                F[0].drawString(g, mm[i2], (Game.Width - F[0].stringWidth(mm[i2])) >> 1, start_y, 20);
                start_y += H;
            }
        }
        Game.draw_shtori(g);
    }

    static void loading(Graphics g) {
        g.setColor(0);
        g.fillRect(0, 0, Game.Width, Game.Height);
        if (state_load == 101) {
            g.setColor(0);
            g.fillRect(0, m3d.Height - 30, Game.Width, 30);
            return;
        }
        if (il0 != null) {
            g.setColor(0);
            g.fillRect(0, 0, Game.Width, Game.Height);
            g.drawImage(il0, (Game.Width - il0.getWidth()) >> 1, (Game.Height - il0.getHeight()) >> 1, 20);
            il0 = null;
        }
        int w = Game.Width - (Game.Width >> 2);
        int start_x = (Game.Width - w) >> 1;
        int start_y = m3d.Height_real_3D + 30 + 14;
        int start_y_pricol = m3d.Height_real_3D + 30 + ((30 - cc[7][3]) >> 1);
        int max_w = w / cc[7][2];
        g.setColor(8216655);
        g.fillRect(start_x, start_y, w, 1);
        int a2 = (state_load * max_w) / 100;
        if (!back && Game.my_level != 0) {
            String str = Game.smenu[Game.my_level <= 10 ? Game.my_level + 30 : Game.my_level + 40];
            if (Game.deathmach) {
                str = Game.smenu[27];
            }
            int jj = (Game.Height - F[0].getHeight()) >> 1;
            g.setColor(0);
            g.fillRect(0, jj - 2, Game.Width, F[0].getHeight() + 4);
            g.setColor(16777215);
            g.fillRect(0, jj - 2, Game.Width, 1);
            g.fillRect(0, F[0].getHeight() + jj + 2, Game.Width, 1);
            F[0].drawString(g, str, (Game.Width - F[0].stringWidth(str)) >> 1, jj, 20);
        }
        for (int i = 0; i < a2; i++) {
            if ((i & 1) == 0) {
                draw_spr(g, 7, (cc[7][2] * i) + start_x, start_y_pricol);
            }
        }
    }

    static void draw(Graphics g) {
        for (int i = 0; i < 3; i++) {
            if (anim_incon[i] > 0) {
                byte[] bArr = anim_incon;
                bArr[i] = (byte) (bArr[i] - 1);
            }
        }
        if (loading) {
            loading(g);
        } else if (menu_type == 10) {
            draw_game_m(g);
        } else if (!chitalka) {
            draw_menu(g);
        } else {
            draw_chitalka(g);
        }
    }

    public static final void draw_fire(Graphics g, int n, int x, int y) {
        int x2 = x + ccc[n][4];
        int y2 = y + ccc[n][5];
        g.setClip(x2, y2, ccc[n][2], ccc[n][3]);
        g.drawImage(spr_fire, x2 - ccc[n][0], y2 - ccc[n][1], 20);
        g.setClip(0, 0, Game.Width, Game.Height);
    }

    public static final void load_fire() {
        if (ccc == null && spr_fire == null) {
            try {
                byte[] mas_load = Uni.uni.load_pack("ccc", -1);
                int msc_e = mas_load.length / 6;
                ccc = (byte[][]) Array.newInstance(Byte.TYPE, msc_e, 6);
                for (int i = 0; i < msc_e; i++) {
                    int index = i * 6;
                    for (int j = 0; j < 6; j++) {
                        ccc[i][j] = mas_load[index + j];
                    }
                }
                Uni uni = Uni.uni;
                spr_fire = Uni.createImage("ccu");
            } catch (Exception e) {
                spr_fire = null;
                ccc = null;
            }
        }
    }

    public static final void delete_fire() {
        spr_fire = null;
        ccc = null;
    }

    public static final void load_c() {
        spr_menu = new Image[2];
        for (int i = 0; i < 2; i++) {
            try {
                Image[] imageArr = spr_menu;
                Uni uni = Uni.uni;
                imageArr[i] = Uni.createImage("c" + i);
            } catch (Exception e) {
            }
        }
    }

    public static final void load_cc() {
        byte[] mas_load = Uni.uni.load_pack("cc", -1);
        int msc_e = mas_load.length / 7;
        cc = (byte[][]) Array.newInstance(Byte.TYPE, msc_e, 7);
        for (int i = 0; i < msc_e; i++) {
            int index = i * 7;
            for (int j = 0; j < 7; j++) {
                cc[i][j] = mas_load[index + j];
            }
        }
    }

    static final void load_il0() {
        try {
            Uni uni = Uni.uni;
            il0 = Uni.createImage("il0");
        } catch (Exception e) {
        }
    }

    public static final void draw_spr(Graphics g, int n, int x, int y) {
        int x2 = x + cc[n][4];
        int y2 = y + cc[n][5];
        g.setClip(x2, y2, cc[n][2], cc[n][3]);
        g.drawImage(spr_menu[cc[n][6]], x2 - cc[n][0], y2 - cc[n][1], 20);
        g.setClip(0, 0, Game.Width, Game.Height);
    }
}
