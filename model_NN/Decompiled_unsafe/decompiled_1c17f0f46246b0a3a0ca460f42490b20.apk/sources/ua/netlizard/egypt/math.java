package ua.netlizard.egypt;

import java.util.Random;

public class math {
    static final int F_minus = 16;
    static final int G = 1048576;
    static final int[] cos;
    static final short[][] fiz_boss;
    static final int max_speed = 100;
    static final int[][] max_speeds1 = {new int[]{20, speed, 200, 70}, new int[]{20, 20, 200, 70}, new int[]{20, 19, 200, 70}, new int[]{20, 80, 200, 70}, new int[]{20, 60, 200, 70}, new int[]{20, 30, 200, 70}, new int[]{20, 20, 200, 70}, new int[]{20, 20, 200, 70}};
    static final int[] min_value_uni2 = {15625, 3600, 13225, 4900, 6400, 15625, 10000, 22500};
    public static Random myRandom = new Random();
    static final int[] sin;
    static final int[] small_fiz_ = {125, 60, 125, 70, 80, 125, 100, 150};
    static final int[] small_fiz_value = {8192000, 3932160, 8192000, 4587520, 5242880, 8192000, 6553600, 9830400};
    static final int speed = 40;
    static final int stp = 16;
    static final int stp_draw = 18;
    static final int stp_one = 65536;

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: int[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    static {
        /*
            r9 = 3
            r8 = 1
            r7 = 4
            r6 = 59
            r5 = 12
            java.util.Random r0 = new java.util.Random
            r0.<init>()
            ua.netlizard.egypt.math.myRandom = r0
            r0 = 8
            int[][] r0 = new int[r0][]
            r1 = 0
            int[] r2 = new int[r7]
            r2 = {20, 40, 200, 70} // fill-array
            r0[r1] = r2
            int[] r1 = new int[r7]
            r1 = {20, 20, 200, 70} // fill-array
            r0[r8] = r1
            r1 = 2
            int[] r2 = new int[r7]
            r2 = {20, 19, 200, 70} // fill-array
            r0[r1] = r2
            int[] r1 = new int[r7]
            r1 = {20, 80, 200, 70} // fill-array
            r0[r9] = r1
            int[] r1 = new int[r7]
            r1 = {20, 60, 200, 70} // fill-array
            r0[r7] = r1
            r1 = 5
            int[] r2 = new int[r7]
            r2 = {20, 30, 200, 70} // fill-array
            r0[r1] = r2
            r1 = 6
            int[] r2 = new int[r7]
            r2 = {20, 20, 200, 70} // fill-array
            r0[r1] = r2
            r1 = 7
            int[] r2 = new int[r7]
            r2 = {20, 20, 200, 70} // fill-array
            r0[r1] = r2
            ua.netlizard.egypt.math.max_speeds1 = r0
            r0 = 360(0x168, float:5.04E-43)
            int[] r0 = new int[r0]
            r1 = 0
            r2 = 65536(0x10000, float:9.18355E-41)
            r0[r1] = r2
            r1 = 65526(0xfff6, float:9.1821E-41)
            r0[r8] = r1
            r1 = 2
            r2 = 65496(0xffd8, float:9.178E-41)
            r0[r1] = r2
            r1 = 65446(0xffa6, float:9.171E-41)
            r0[r9] = r1
            r1 = 65376(0xff60, float:9.1611E-41)
            r0[r7] = r1
            r1 = 5
            r2 = 65287(0xff07, float:9.1487E-41)
            r0[r1] = r2
            r1 = 6
            r2 = 65177(0xfe99, float:9.1332E-41)
            r0[r1] = r2
            r1 = 7
            r2 = 65048(0xfe18, float:9.1152E-41)
            r0[r1] = r2
            r1 = 8
            r2 = 64898(0xfd82, float:9.0941E-41)
            r0[r1] = r2
            r1 = 9
            r2 = 64729(0xfcd9, float:9.0705E-41)
            r0[r1] = r2
            r1 = 10
            r2 = 64540(0xfc1c, float:9.044E-41)
            r0[r1] = r2
            r1 = 11
            r2 = 64332(0xfb4c, float:9.0148E-41)
            r0[r1] = r2
            r1 = 64104(0xfa68, float:8.9829E-41)
            r0[r5] = r1
            r1 = 13
            r2 = 63856(0xf970, float:8.9481E-41)
            r0[r1] = r2
            r1 = 14
            r2 = 63589(0xf865, float:8.9107E-41)
            r0[r1] = r2
            r1 = 15
            r2 = 63303(0xf747, float:8.8706E-41)
            r0[r1] = r2
            r1 = 16
            r2 = 62997(0xf615, float:8.8278E-41)
            r0[r1] = r2
            r1 = 17
            r2 = 62672(0xf4d0, float:8.7822E-41)
            r0[r1] = r2
            r1 = 18
            r2 = 62328(0xf378, float:8.734E-41)
            r0[r1] = r2
            r1 = 19
            r2 = 61966(0xf20e, float:8.6833E-41)
            r0[r1] = r2
            r1 = 20
            r2 = 61584(0xf090, float:8.6298E-41)
            r0[r1] = r2
            r1 = 21
            r2 = 61183(0xeeff, float:8.5736E-41)
            r0[r1] = r2
            r1 = 22
            r2 = 60764(0xed5c, float:8.5148E-41)
            r0[r1] = r2
            r1 = 23
            r2 = 60326(0xeba6, float:8.4535E-41)
            r0[r1] = r2
            r1 = 24
            r2 = 59870(0xe9de, float:8.3896E-41)
            r0[r1] = r2
            r1 = 25
            r2 = 59396(0xe804, float:8.3232E-41)
            r0[r1] = r2
            r1 = 26
            r2 = 58903(0xe617, float:8.254E-41)
            r0[r1] = r2
            r1 = 27
            r2 = 58393(0xe419, float:8.1826E-41)
            r0[r1] = r2
            r1 = 28
            r2 = 57865(0xe209, float:8.1086E-41)
            r0[r1] = r2
            r1 = 29
            r2 = 57319(0xdfe7, float:8.0321E-41)
            r0[r1] = r2
            r1 = 30
            r2 = 56756(0xddb4, float:7.9532E-41)
            r0[r1] = r2
            r1 = 31
            r2 = 56175(0xdb6f, float:7.8718E-41)
            r0[r1] = r2
            r1 = 32
            r2 = 55578(0xd91a, float:7.7881E-41)
            r0[r1] = r2
            r1 = 33
            r2 = 54963(0xd6b3, float:7.702E-41)
            r0[r1] = r2
            r1 = 34
            r2 = 54332(0xd43c, float:7.6135E-41)
            r0[r1] = r2
            r1 = 35
            r2 = 53684(0xd1b4, float:7.5227E-41)
            r0[r1] = r2
            r1 = 36
            r2 = 53020(0xcf1c, float:7.4297E-41)
            r0[r1] = r2
            r1 = 37
            r2 = 52339(0xcc73, float:7.3343E-41)
            r0[r1] = r2
            r1 = 38
            r2 = 51643(0xc9bb, float:7.2367E-41)
            r0[r1] = r2
            r1 = 39
            r2 = 50931(0xc6f3, float:7.137E-41)
            r0[r1] = r2
            r1 = 40
            r2 = 50203(0xc41b, float:7.035E-41)
            r0[r1] = r2
            r1 = 41
            r2 = 49461(0xc135, float:6.931E-41)
            r0[r1] = r2
            r1 = 42
            r2 = 48703(0xbe3f, float:6.8247E-41)
            r0[r1] = r2
            r1 = 43
            r2 = 47930(0xbb3a, float:6.7164E-41)
            r0[r1] = r2
            r1 = 44
            r2 = 47143(0xb827, float:6.6061E-41)
            r0[r1] = r2
            r1 = 45
            r2 = 46341(0xb505, float:6.4938E-41)
            r0[r1] = r2
            r1 = 46
            r2 = 45525(0xb1d5, float:6.3794E-41)
            r0[r1] = r2
            r1 = 47
            r2 = 44695(0xae97, float:6.2631E-41)
            r0[r1] = r2
            r1 = 48
            r2 = 43852(0xab4c, float:6.145E-41)
            r0[r1] = r2
            r1 = 49
            r2 = 42995(0xa7f3, float:6.0249E-41)
            r0[r1] = r2
            r1 = 50
            r2 = 42126(0xa48e, float:5.9031E-41)
            r0[r1] = r2
            r1 = 51
            r2 = 41243(0xa11b, float:5.7794E-41)
            r0[r1] = r2
            r1 = 52
            r2 = 40348(0x9d9c, float:5.654E-41)
            r0[r1] = r2
            r1 = 53
            r2 = 39441(0x9a11, float:5.5269E-41)
            r0[r1] = r2
            r1 = 54
            r2 = 38521(0x9679, float:5.398E-41)
            r0[r1] = r2
            r1 = 55
            r2 = 37590(0x92d6, float:5.2675E-41)
            r0[r1] = r2
            r1 = 56
            r2 = 36647(0x8f27, float:5.1353E-41)
            r0[r1] = r2
            r1 = 57
            r2 = 35693(0x8b6d, float:5.0017E-41)
            r0[r1] = r2
            r1 = 58
            r2 = 34729(0x87a9, float:4.8666E-41)
            r0[r1] = r2
            r1 = 33754(0x83da, float:4.73E-41)
            r0[r6] = r1
            r1 = 60
            r2 = 32768(0x8000, float:4.5918E-41)
            r0[r1] = r2
            r1 = 61
            r2 = 31772(0x7c1c, float:4.4522E-41)
            r0[r1] = r2
            r1 = 62
            r2 = 30767(0x782f, float:4.3114E-41)
            r0[r1] = r2
            r1 = 63
            r2 = 29753(0x7439, float:4.1693E-41)
            r0[r1] = r2
            r1 = 64
            r2 = 28729(0x7039, float:4.0258E-41)
            r0[r1] = r2
            r1 = 65
            r2 = 27697(0x6c31, float:3.8812E-41)
            r0[r1] = r2
            r1 = 66
            r2 = 26656(0x6820, float:3.7353E-41)
            r0[r1] = r2
            r1 = 67
            r2 = 25607(0x6407, float:3.5883E-41)
            r0[r1] = r2
            r1 = 68
            r2 = 24550(0x5fe6, float:3.4402E-41)
            r0[r1] = r2
            r1 = 69
            r2 = 23486(0x5bbe, float:3.2911E-41)
            r0[r1] = r2
            r1 = 70
            r2 = 22415(0x578f, float:3.141E-41)
            r0[r1] = r2
            r1 = 71
            r2 = 21336(0x5358, float:2.9898E-41)
            r0[r1] = r2
            r1 = 72
            r2 = 20252(0x4f1c, float:2.8379E-41)
            r0[r1] = r2
            r1 = 73
            r2 = 19161(0x4ad9, float:2.685E-41)
            r0[r1] = r2
            r1 = 74
            r2 = 18064(0x4690, float:2.5313E-41)
            r0[r1] = r2
            r1 = 75
            r2 = 16962(0x4242, float:2.3769E-41)
            r0[r1] = r2
            r1 = 76
            r2 = 15855(0x3def, float:2.2218E-41)
            r0[r1] = r2
            r1 = 77
            r2 = 14742(0x3996, float:2.0658E-41)
            r0[r1] = r2
            r1 = 78
            r2 = 13626(0x353a, float:1.9094E-41)
            r0[r1] = r2
            r1 = 79
            r2 = 12505(0x30d9, float:1.7523E-41)
            r0[r1] = r2
            r1 = 80
            r2 = 11380(0x2c74, float:1.5947E-41)
            r0[r1] = r2
            r1 = 81
            r2 = 10252(0x280c, float:1.4366E-41)
            r0[r1] = r2
            r1 = 82
            r2 = 9121(0x23a1, float:1.2781E-41)
            r0[r1] = r2
            r1 = 83
            r2 = 7987(0x1f33, float:1.1192E-41)
            r0[r1] = r2
            r1 = 84
            r2 = 6850(0x1ac2, float:9.599E-42)
            r0[r1] = r2
            r1 = 85
            r2 = 5712(0x1650, float:8.004E-42)
            r0[r1] = r2
            r1 = 86
            r2 = 4572(0x11dc, float:6.407E-42)
            r0[r1] = r2
            r1 = 87
            r2 = 3430(0xd66, float:4.806E-42)
            r0[r1] = r2
            r1 = 88
            r2 = 2287(0x8ef, float:3.205E-42)
            r0[r1] = r2
            r1 = 89
            r2 = 1144(0x478, float:1.603E-42)
            r0[r1] = r2
            r1 = 91
            r2 = -1144(0xfffffffffffffb88, float:NaN)
            r0[r1] = r2
            r1 = 92
            r2 = -2287(0xfffffffffffff711, float:NaN)
            r0[r1] = r2
            r1 = 93
            r2 = -3430(0xfffffffffffff29a, float:NaN)
            r0[r1] = r2
            r1 = 94
            r2 = -4572(0xffffffffffffee24, float:NaN)
            r0[r1] = r2
            r1 = 95
            r2 = -5712(0xffffffffffffe9b0, float:NaN)
            r0[r1] = r2
            r1 = 96
            r2 = -6850(0xffffffffffffe53e, float:NaN)
            r0[r1] = r2
            r1 = 97
            r2 = -7987(0xffffffffffffe0cd, float:NaN)
            r0[r1] = r2
            r1 = 98
            r2 = -9121(0xffffffffffffdc5f, float:NaN)
            r0[r1] = r2
            r1 = 99
            r2 = -10252(0xffffffffffffd7f4, float:NaN)
            r0[r1] = r2
            r1 = 100
            r2 = -11380(0xffffffffffffd38c, float:NaN)
            r0[r1] = r2
            r1 = 101(0x65, float:1.42E-43)
            r2 = -12505(0xffffffffffffcf27, float:NaN)
            r0[r1] = r2
            r1 = 102(0x66, float:1.43E-43)
            r2 = -13626(0xffffffffffffcac6, float:NaN)
            r0[r1] = r2
            r1 = 103(0x67, float:1.44E-43)
            r2 = -14742(0xffffffffffffc66a, float:NaN)
            r0[r1] = r2
            r1 = 104(0x68, float:1.46E-43)
            r2 = -15855(0xffffffffffffc211, float:NaN)
            r0[r1] = r2
            r1 = 105(0x69, float:1.47E-43)
            r2 = -16962(0xffffffffffffbdbe, float:NaN)
            r0[r1] = r2
            r1 = 106(0x6a, float:1.49E-43)
            r2 = -18064(0xffffffffffffb970, float:NaN)
            r0[r1] = r2
            r1 = 107(0x6b, float:1.5E-43)
            r2 = -19161(0xffffffffffffb527, float:NaN)
            r0[r1] = r2
            r1 = 108(0x6c, float:1.51E-43)
            r2 = -20252(0xffffffffffffb0e4, float:NaN)
            r0[r1] = r2
            r1 = 109(0x6d, float:1.53E-43)
            r2 = -21336(0xffffffffffffaca8, float:NaN)
            r0[r1] = r2
            r1 = 110(0x6e, float:1.54E-43)
            r2 = -22415(0xffffffffffffa871, float:NaN)
            r0[r1] = r2
            r1 = 111(0x6f, float:1.56E-43)
            r2 = -23486(0xffffffffffffa442, float:NaN)
            r0[r1] = r2
            r1 = 112(0x70, float:1.57E-43)
            r2 = -24550(0xffffffffffffa01a, float:NaN)
            r0[r1] = r2
            r1 = 113(0x71, float:1.58E-43)
            r2 = -25607(0xffffffffffff9bf9, float:NaN)
            r0[r1] = r2
            r1 = 114(0x72, float:1.6E-43)
            r2 = -26656(0xffffffffffff97e0, float:NaN)
            r0[r1] = r2
            r1 = 115(0x73, float:1.61E-43)
            r2 = -27697(0xffffffffffff93cf, float:NaN)
            r0[r1] = r2
            r1 = 116(0x74, float:1.63E-43)
            r2 = -28729(0xffffffffffff8fc7, float:NaN)
            r0[r1] = r2
            r1 = 117(0x75, float:1.64E-43)
            r2 = -29753(0xffffffffffff8bc7, float:NaN)
            r0[r1] = r2
            r1 = 118(0x76, float:1.65E-43)
            r2 = -30767(0xffffffffffff87d1, float:NaN)
            r0[r1] = r2
            r1 = 119(0x77, float:1.67E-43)
            r2 = -31772(0xffffffffffff83e4, float:NaN)
            r0[r1] = r2
            r1 = 120(0x78, float:1.68E-43)
            r2 = -32768(0xffffffffffff8000, float:NaN)
            r0[r1] = r2
            r1 = 121(0x79, float:1.7E-43)
            r2 = -33754(0xffffffffffff7c26, float:NaN)
            r0[r1] = r2
            r1 = 122(0x7a, float:1.71E-43)
            r2 = -34729(0xffffffffffff7857, float:NaN)
            r0[r1] = r2
            r1 = 123(0x7b, float:1.72E-43)
            r2 = -35693(0xffffffffffff7493, float:NaN)
            r0[r1] = r2
            r1 = 124(0x7c, float:1.74E-43)
            r2 = -36647(0xffffffffffff70d9, float:NaN)
            r0[r1] = r2
            r1 = 125(0x7d, float:1.75E-43)
            r2 = -37590(0xffffffffffff6d2a, float:NaN)
            r0[r1] = r2
            r1 = 126(0x7e, float:1.77E-43)
            r2 = -38521(0xffffffffffff6987, float:NaN)
            r0[r1] = r2
            r1 = 127(0x7f, float:1.78E-43)
            r2 = -39441(0xffffffffffff65ef, float:NaN)
            r0[r1] = r2
            r1 = 128(0x80, float:1.794E-43)
            r2 = -40348(0xffffffffffff6264, float:NaN)
            r0[r1] = r2
            r1 = 129(0x81, float:1.81E-43)
            r2 = -41243(0xffffffffffff5ee5, float:NaN)
            r0[r1] = r2
            r1 = 130(0x82, float:1.82E-43)
            r2 = -42126(0xffffffffffff5b72, float:NaN)
            r0[r1] = r2
            r1 = 131(0x83, float:1.84E-43)
            r2 = -42995(0xffffffffffff580d, float:NaN)
            r0[r1] = r2
            r1 = 132(0x84, float:1.85E-43)
            r2 = -43852(0xffffffffffff54b4, float:NaN)
            r0[r1] = r2
            r1 = 133(0x85, float:1.86E-43)
            r2 = -44695(0xffffffffffff5169, float:NaN)
            r0[r1] = r2
            r1 = 134(0x86, float:1.88E-43)
            r2 = -45525(0xffffffffffff4e2b, float:NaN)
            r0[r1] = r2
            r1 = 135(0x87, float:1.89E-43)
            r2 = -46341(0xffffffffffff4afb, float:NaN)
            r0[r1] = r2
            r1 = 136(0x88, float:1.9E-43)
            r2 = -47143(0xffffffffffff47d9, float:NaN)
            r0[r1] = r2
            r1 = 137(0x89, float:1.92E-43)
            r2 = -47930(0xffffffffffff44c6, float:NaN)
            r0[r1] = r2
            r1 = 138(0x8a, float:1.93E-43)
            r2 = -48703(0xffffffffffff41c1, float:NaN)
            r0[r1] = r2
            r1 = 139(0x8b, float:1.95E-43)
            r2 = -49461(0xffffffffffff3ecb, float:NaN)
            r0[r1] = r2
            r1 = 140(0x8c, float:1.96E-43)
            r2 = -50203(0xffffffffffff3be5, float:NaN)
            r0[r1] = r2
            r1 = 141(0x8d, float:1.98E-43)
            r2 = -50931(0xffffffffffff390d, float:NaN)
            r0[r1] = r2
            r1 = 142(0x8e, float:1.99E-43)
            r2 = -51643(0xffffffffffff3645, float:NaN)
            r0[r1] = r2
            r1 = 143(0x8f, float:2.0E-43)
            r2 = -52339(0xffffffffffff338d, float:NaN)
            r0[r1] = r2
            r1 = 144(0x90, float:2.02E-43)
            r2 = -53020(0xffffffffffff30e4, float:NaN)
            r0[r1] = r2
            r1 = 145(0x91, float:2.03E-43)
            r2 = -53684(0xffffffffffff2e4c, float:NaN)
            r0[r1] = r2
            r1 = 146(0x92, float:2.05E-43)
            r2 = -54332(0xffffffffffff2bc4, float:NaN)
            r0[r1] = r2
            r1 = 147(0x93, float:2.06E-43)
            r2 = -54963(0xffffffffffff294d, float:NaN)
            r0[r1] = r2
            r1 = 148(0x94, float:2.07E-43)
            r2 = -55578(0xffffffffffff26e6, float:NaN)
            r0[r1] = r2
            r1 = 149(0x95, float:2.09E-43)
            r2 = -56175(0xffffffffffff2491, float:NaN)
            r0[r1] = r2
            r1 = 150(0x96, float:2.1E-43)
            r2 = -56756(0xffffffffffff224c, float:NaN)
            r0[r1] = r2
            r1 = 151(0x97, float:2.12E-43)
            r2 = -57319(0xffffffffffff2019, float:NaN)
            r0[r1] = r2
            r1 = 152(0x98, float:2.13E-43)
            r2 = -57865(0xffffffffffff1df7, float:NaN)
            r0[r1] = r2
            r1 = 153(0x99, float:2.14E-43)
            r2 = -58393(0xffffffffffff1be7, float:NaN)
            r0[r1] = r2
            r1 = 154(0x9a, float:2.16E-43)
            r2 = -58903(0xffffffffffff19e9, float:NaN)
            r0[r1] = r2
            r1 = 155(0x9b, float:2.17E-43)
            r2 = -59396(0xffffffffffff17fc, float:NaN)
            r0[r1] = r2
            r1 = 156(0x9c, float:2.19E-43)
            r2 = -59870(0xffffffffffff1622, float:NaN)
            r0[r1] = r2
            r1 = 157(0x9d, float:2.2E-43)
            r2 = -60326(0xffffffffffff145a, float:NaN)
            r0[r1] = r2
            r1 = 158(0x9e, float:2.21E-43)
            r2 = -60764(0xffffffffffff12a4, float:NaN)
            r0[r1] = r2
            r1 = 159(0x9f, float:2.23E-43)
            r2 = -61183(0xffffffffffff1101, float:NaN)
            r0[r1] = r2
            r1 = 160(0xa0, float:2.24E-43)
            r2 = -61584(0xffffffffffff0f70, float:NaN)
            r0[r1] = r2
            r1 = 161(0xa1, float:2.26E-43)
            r2 = -61966(0xffffffffffff0df2, float:NaN)
            r0[r1] = r2
            r1 = 162(0xa2, float:2.27E-43)
            r2 = -62328(0xffffffffffff0c88, float:NaN)
            r0[r1] = r2
            r1 = 163(0xa3, float:2.28E-43)
            r2 = -62672(0xffffffffffff0b30, float:NaN)
            r0[r1] = r2
            r1 = 164(0xa4, float:2.3E-43)
            r2 = -62997(0xffffffffffff09eb, float:NaN)
            r0[r1] = r2
            r1 = 165(0xa5, float:2.31E-43)
            r2 = -63303(0xffffffffffff08b9, float:NaN)
            r0[r1] = r2
            r1 = 166(0xa6, float:2.33E-43)
            r2 = -63589(0xffffffffffff079b, float:NaN)
            r0[r1] = r2
            r1 = 167(0xa7, float:2.34E-43)
            r2 = -63856(0xffffffffffff0690, float:NaN)
            r0[r1] = r2
            r1 = 168(0xa8, float:2.35E-43)
            r2 = -64104(0xffffffffffff0598, float:NaN)
            r0[r1] = r2
            r1 = 169(0xa9, float:2.37E-43)
            r2 = -64332(0xffffffffffff04b4, float:NaN)
            r0[r1] = r2
            r1 = 170(0xaa, float:2.38E-43)
            r2 = -64540(0xffffffffffff03e4, float:NaN)
            r0[r1] = r2
            r1 = 171(0xab, float:2.4E-43)
            r2 = -64729(0xffffffffffff0327, float:NaN)
            r0[r1] = r2
            r1 = 172(0xac, float:2.41E-43)
            r2 = -64898(0xffffffffffff027e, float:NaN)
            r0[r1] = r2
            r1 = 173(0xad, float:2.42E-43)
            r2 = -65048(0xffffffffffff01e8, float:NaN)
            r0[r1] = r2
            r1 = 174(0xae, float:2.44E-43)
            r2 = -65177(0xffffffffffff0167, float:NaN)
            r0[r1] = r2
            r1 = 175(0xaf, float:2.45E-43)
            r2 = -65287(0xffffffffffff00f9, float:NaN)
            r0[r1] = r2
            r1 = 176(0xb0, float:2.47E-43)
            r2 = -65376(0xffffffffffff00a0, float:NaN)
            r0[r1] = r2
            r1 = 177(0xb1, float:2.48E-43)
            r2 = -65446(0xffffffffffff005a, float:NaN)
            r0[r1] = r2
            r1 = 178(0xb2, float:2.5E-43)
            r2 = -65496(0xffffffffffff0028, float:NaN)
            r0[r1] = r2
            r1 = 179(0xb3, float:2.51E-43)
            r2 = -65526(0xffffffffffff000a, float:NaN)
            r0[r1] = r2
            r1 = 180(0xb4, float:2.52E-43)
            r2 = -65536(0xffffffffffff0000, float:NaN)
            r0[r1] = r2
            r1 = 181(0xb5, float:2.54E-43)
            r2 = -65526(0xffffffffffff000a, float:NaN)
            r0[r1] = r2
            r1 = 182(0xb6, float:2.55E-43)
            r2 = -65496(0xffffffffffff0028, float:NaN)
            r0[r1] = r2
            r1 = 183(0xb7, float:2.56E-43)
            r2 = -65446(0xffffffffffff005a, float:NaN)
            r0[r1] = r2
            r1 = 184(0xb8, float:2.58E-43)
            r2 = -65376(0xffffffffffff00a0, float:NaN)
            r0[r1] = r2
            r1 = 185(0xb9, float:2.59E-43)
            r2 = -65287(0xffffffffffff00f9, float:NaN)
            r0[r1] = r2
            r1 = 186(0xba, float:2.6E-43)
            r2 = -65177(0xffffffffffff0167, float:NaN)
            r0[r1] = r2
            r1 = 187(0xbb, float:2.62E-43)
            r2 = -65048(0xffffffffffff01e8, float:NaN)
            r0[r1] = r2
            r1 = 188(0xbc, float:2.63E-43)
            r2 = -64898(0xffffffffffff027e, float:NaN)
            r0[r1] = r2
            r1 = 189(0xbd, float:2.65E-43)
            r2 = -64729(0xffffffffffff0327, float:NaN)
            r0[r1] = r2
            r1 = 190(0xbe, float:2.66E-43)
            r2 = -64540(0xffffffffffff03e4, float:NaN)
            r0[r1] = r2
            r1 = 191(0xbf, float:2.68E-43)
            r2 = -64332(0xffffffffffff04b4, float:NaN)
            r0[r1] = r2
            r1 = 192(0xc0, float:2.69E-43)
            r2 = -64104(0xffffffffffff0598, float:NaN)
            r0[r1] = r2
            r1 = 193(0xc1, float:2.7E-43)
            r2 = -63856(0xffffffffffff0690, float:NaN)
            r0[r1] = r2
            r1 = 194(0xc2, float:2.72E-43)
            r2 = -63589(0xffffffffffff079b, float:NaN)
            r0[r1] = r2
            r1 = 195(0xc3, float:2.73E-43)
            r2 = -63303(0xffffffffffff08b9, float:NaN)
            r0[r1] = r2
            r1 = 196(0xc4, float:2.75E-43)
            r2 = -62997(0xffffffffffff09eb, float:NaN)
            r0[r1] = r2
            r1 = 197(0xc5, float:2.76E-43)
            r2 = -62672(0xffffffffffff0b30, float:NaN)
            r0[r1] = r2
            r1 = 198(0xc6, float:2.77E-43)
            r2 = -62328(0xffffffffffff0c88, float:NaN)
            r0[r1] = r2
            r1 = 199(0xc7, float:2.79E-43)
            r2 = -61966(0xffffffffffff0df2, float:NaN)
            r0[r1] = r2
            r1 = 200(0xc8, float:2.8E-43)
            r2 = -61584(0xffffffffffff0f70, float:NaN)
            r0[r1] = r2
            r1 = 201(0xc9, float:2.82E-43)
            r2 = -61183(0xffffffffffff1101, float:NaN)
            r0[r1] = r2
            r1 = 202(0xca, float:2.83E-43)
            r2 = -60764(0xffffffffffff12a4, float:NaN)
            r0[r1] = r2
            r1 = 203(0xcb, float:2.84E-43)
            r2 = -60326(0xffffffffffff145a, float:NaN)
            r0[r1] = r2
            r1 = 204(0xcc, float:2.86E-43)
            r2 = -59870(0xffffffffffff1622, float:NaN)
            r0[r1] = r2
            r1 = 205(0xcd, float:2.87E-43)
            r2 = -59396(0xffffffffffff17fc, float:NaN)
            r0[r1] = r2
            r1 = 206(0xce, float:2.89E-43)
            r2 = -58903(0xffffffffffff19e9, float:NaN)
            r0[r1] = r2
            r1 = 207(0xcf, float:2.9E-43)
            r2 = -58393(0xffffffffffff1be7, float:NaN)
            r0[r1] = r2
            r1 = 208(0xd0, float:2.91E-43)
            r2 = -57865(0xffffffffffff1df7, float:NaN)
            r0[r1] = r2
            r1 = 209(0xd1, float:2.93E-43)
            r2 = -57319(0xffffffffffff2019, float:NaN)
            r0[r1] = r2
            r1 = 210(0xd2, float:2.94E-43)
            r2 = -56756(0xffffffffffff224c, float:NaN)
            r0[r1] = r2
            r1 = 211(0xd3, float:2.96E-43)
            r2 = -56175(0xffffffffffff2491, float:NaN)
            r0[r1] = r2
            r1 = 212(0xd4, float:2.97E-43)
            r2 = -55578(0xffffffffffff26e6, float:NaN)
            r0[r1] = r2
            r1 = 213(0xd5, float:2.98E-43)
            r2 = -54963(0xffffffffffff294d, float:NaN)
            r0[r1] = r2
            r1 = 214(0xd6, float:3.0E-43)
            r2 = -54332(0xffffffffffff2bc4, float:NaN)
            r0[r1] = r2
            r1 = 215(0xd7, float:3.01E-43)
            r2 = -53684(0xffffffffffff2e4c, float:NaN)
            r0[r1] = r2
            r1 = 216(0xd8, float:3.03E-43)
            r2 = -53020(0xffffffffffff30e4, float:NaN)
            r0[r1] = r2
            r1 = 217(0xd9, float:3.04E-43)
            r2 = -52339(0xffffffffffff338d, float:NaN)
            r0[r1] = r2
            r1 = 218(0xda, float:3.05E-43)
            r2 = -51643(0xffffffffffff3645, float:NaN)
            r0[r1] = r2
            r1 = 219(0xdb, float:3.07E-43)
            r2 = -50931(0xffffffffffff390d, float:NaN)
            r0[r1] = r2
            r1 = 220(0xdc, float:3.08E-43)
            r2 = -50203(0xffffffffffff3be5, float:NaN)
            r0[r1] = r2
            r1 = 221(0xdd, float:3.1E-43)
            r2 = -49461(0xffffffffffff3ecb, float:NaN)
            r0[r1] = r2
            r1 = 222(0xde, float:3.11E-43)
            r2 = -48703(0xffffffffffff41c1, float:NaN)
            r0[r1] = r2
            r1 = 223(0xdf, float:3.12E-43)
            r2 = -47930(0xffffffffffff44c6, float:NaN)
            r0[r1] = r2
            r1 = 224(0xe0, float:3.14E-43)
            r2 = -47143(0xffffffffffff47d9, float:NaN)
            r0[r1] = r2
            r1 = 225(0xe1, float:3.15E-43)
            r2 = -46341(0xffffffffffff4afb, float:NaN)
            r0[r1] = r2
            r1 = 226(0xe2, float:3.17E-43)
            r2 = -45525(0xffffffffffff4e2b, float:NaN)
            r0[r1] = r2
            r1 = 227(0xe3, float:3.18E-43)
            r2 = -44695(0xffffffffffff5169, float:NaN)
            r0[r1] = r2
            r1 = 228(0xe4, float:3.2E-43)
            r2 = -43852(0xffffffffffff54b4, float:NaN)
            r0[r1] = r2
            r1 = 229(0xe5, float:3.21E-43)
            r2 = -42995(0xffffffffffff580d, float:NaN)
            r0[r1] = r2
            r1 = 230(0xe6, float:3.22E-43)
            r2 = -42126(0xffffffffffff5b72, float:NaN)
            r0[r1] = r2
            r1 = 231(0xe7, float:3.24E-43)
            r2 = -41243(0xffffffffffff5ee5, float:NaN)
            r0[r1] = r2
            r1 = 232(0xe8, float:3.25E-43)
            r2 = -40348(0xffffffffffff6264, float:NaN)
            r0[r1] = r2
            r1 = 233(0xe9, float:3.27E-43)
            r2 = -39441(0xffffffffffff65ef, float:NaN)
            r0[r1] = r2
            r1 = 234(0xea, float:3.28E-43)
            r2 = -38521(0xffffffffffff6987, float:NaN)
            r0[r1] = r2
            r1 = 235(0xeb, float:3.3E-43)
            r2 = -37590(0xffffffffffff6d2a, float:NaN)
            r0[r1] = r2
            r1 = 236(0xec, float:3.31E-43)
            r2 = -36647(0xffffffffffff70d9, float:NaN)
            r0[r1] = r2
            r1 = 237(0xed, float:3.32E-43)
            r2 = -35693(0xffffffffffff7493, float:NaN)
            r0[r1] = r2
            r1 = 238(0xee, float:3.34E-43)
            r2 = -34729(0xffffffffffff7857, float:NaN)
            r0[r1] = r2
            r1 = 239(0xef, float:3.35E-43)
            r2 = -33754(0xffffffffffff7c26, float:NaN)
            r0[r1] = r2
            r1 = 240(0xf0, float:3.36E-43)
            r2 = -32768(0xffffffffffff8000, float:NaN)
            r0[r1] = r2
            r1 = 241(0xf1, float:3.38E-43)
            r2 = -31772(0xffffffffffff83e4, float:NaN)
            r0[r1] = r2
            r1 = 242(0xf2, float:3.39E-43)
            r2 = -30767(0xffffffffffff87d1, float:NaN)
            r0[r1] = r2
            r1 = 243(0xf3, float:3.4E-43)
            r2 = -29753(0xffffffffffff8bc7, float:NaN)
            r0[r1] = r2
            r1 = 244(0xf4, float:3.42E-43)
            r2 = -28729(0xffffffffffff8fc7, float:NaN)
            r0[r1] = r2
            r1 = 245(0xf5, float:3.43E-43)
            r2 = -27697(0xffffffffffff93cf, float:NaN)
            r0[r1] = r2
            r1 = 246(0xf6, float:3.45E-43)
            r2 = -26656(0xffffffffffff97e0, float:NaN)
            r0[r1] = r2
            r1 = 247(0xf7, float:3.46E-43)
            r2 = -25607(0xffffffffffff9bf9, float:NaN)
            r0[r1] = r2
            r1 = 248(0xf8, float:3.48E-43)
            r2 = -24550(0xffffffffffffa01a, float:NaN)
            r0[r1] = r2
            r1 = 249(0xf9, float:3.49E-43)
            r2 = -23486(0xffffffffffffa442, float:NaN)
            r0[r1] = r2
            r1 = 250(0xfa, float:3.5E-43)
            r2 = -22415(0xffffffffffffa871, float:NaN)
            r0[r1] = r2
            r1 = 251(0xfb, float:3.52E-43)
            r2 = -21336(0xffffffffffffaca8, float:NaN)
            r0[r1] = r2
            r1 = 252(0xfc, float:3.53E-43)
            r2 = -20252(0xffffffffffffb0e4, float:NaN)
            r0[r1] = r2
            r1 = 253(0xfd, float:3.55E-43)
            r2 = -19161(0xffffffffffffb527, float:NaN)
            r0[r1] = r2
            r1 = 254(0xfe, float:3.56E-43)
            r2 = -18064(0xffffffffffffb970, float:NaN)
            r0[r1] = r2
            r1 = 255(0xff, float:3.57E-43)
            r2 = -16962(0xffffffffffffbdbe, float:NaN)
            r0[r1] = r2
            r1 = 256(0x100, float:3.59E-43)
            r2 = -15855(0xffffffffffffc211, float:NaN)
            r0[r1] = r2
            r1 = 257(0x101, float:3.6E-43)
            r2 = -14742(0xffffffffffffc66a, float:NaN)
            r0[r1] = r2
            r1 = 258(0x102, float:3.62E-43)
            r2 = -13626(0xffffffffffffcac6, float:NaN)
            r0[r1] = r2
            r1 = 259(0x103, float:3.63E-43)
            r2 = -12505(0xffffffffffffcf27, float:NaN)
            r0[r1] = r2
            r1 = 260(0x104, float:3.64E-43)
            r2 = -11380(0xffffffffffffd38c, float:NaN)
            r0[r1] = r2
            r1 = 261(0x105, float:3.66E-43)
            r2 = -10252(0xffffffffffffd7f4, float:NaN)
            r0[r1] = r2
            r1 = 262(0x106, float:3.67E-43)
            r2 = -9121(0xffffffffffffdc5f, float:NaN)
            r0[r1] = r2
            r1 = 263(0x107, float:3.69E-43)
            r2 = -7987(0xffffffffffffe0cd, float:NaN)
            r0[r1] = r2
            r1 = 264(0x108, float:3.7E-43)
            r2 = -6850(0xffffffffffffe53e, float:NaN)
            r0[r1] = r2
            r1 = 265(0x109, float:3.71E-43)
            r2 = -5712(0xffffffffffffe9b0, float:NaN)
            r0[r1] = r2
            r1 = 266(0x10a, float:3.73E-43)
            r2 = -4572(0xffffffffffffee24, float:NaN)
            r0[r1] = r2
            r1 = 267(0x10b, float:3.74E-43)
            r2 = -3430(0xfffffffffffff29a, float:NaN)
            r0[r1] = r2
            r1 = 268(0x10c, float:3.76E-43)
            r2 = -2287(0xfffffffffffff711, float:NaN)
            r0[r1] = r2
            r1 = 269(0x10d, float:3.77E-43)
            r2 = -1144(0xfffffffffffffb88, float:NaN)
            r0[r1] = r2
            r1 = 271(0x10f, float:3.8E-43)
            r2 = 1144(0x478, float:1.603E-42)
            r0[r1] = r2
            r1 = 272(0x110, float:3.81E-43)
            r2 = 2287(0x8ef, float:3.205E-42)
            r0[r1] = r2
            r1 = 273(0x111, float:3.83E-43)
            r2 = 3430(0xd66, float:4.806E-42)
            r0[r1] = r2
            r1 = 274(0x112, float:3.84E-43)
            r2 = 4572(0x11dc, float:6.407E-42)
            r0[r1] = r2
            r1 = 275(0x113, float:3.85E-43)
            r2 = 5712(0x1650, float:8.004E-42)
            r0[r1] = r2
            r1 = 276(0x114, float:3.87E-43)
            r2 = 6850(0x1ac2, float:9.599E-42)
            r0[r1] = r2
            r1 = 277(0x115, float:3.88E-43)
            r2 = 7987(0x1f33, float:1.1192E-41)
            r0[r1] = r2
            r1 = 278(0x116, float:3.9E-43)
            r2 = 9121(0x23a1, float:1.2781E-41)
            r0[r1] = r2
            r1 = 279(0x117, float:3.91E-43)
            r2 = 10252(0x280c, float:1.4366E-41)
            r0[r1] = r2
            r1 = 280(0x118, float:3.92E-43)
            r2 = 11380(0x2c74, float:1.5947E-41)
            r0[r1] = r2
            r1 = 281(0x119, float:3.94E-43)
            r2 = 12505(0x30d9, float:1.7523E-41)
            r0[r1] = r2
            r1 = 282(0x11a, float:3.95E-43)
            r2 = 13626(0x353a, float:1.9094E-41)
            r0[r1] = r2
            r1 = 283(0x11b, float:3.97E-43)
            r2 = 14742(0x3996, float:2.0658E-41)
            r0[r1] = r2
            r1 = 284(0x11c, float:3.98E-43)
            r2 = 15855(0x3def, float:2.2218E-41)
            r0[r1] = r2
            r1 = 285(0x11d, float:4.0E-43)
            r2 = 16962(0x4242, float:2.3769E-41)
            r0[r1] = r2
            r1 = 286(0x11e, float:4.01E-43)
            r2 = 18064(0x4690, float:2.5313E-41)
            r0[r1] = r2
            r1 = 287(0x11f, float:4.02E-43)
            r2 = 19161(0x4ad9, float:2.685E-41)
            r0[r1] = r2
            r1 = 288(0x120, float:4.04E-43)
            r2 = 20252(0x4f1c, float:2.8379E-41)
            r0[r1] = r2
            r1 = 289(0x121, float:4.05E-43)
            r2 = 21336(0x5358, float:2.9898E-41)
            r0[r1] = r2
            r1 = 290(0x122, float:4.06E-43)
            r2 = 22415(0x578f, float:3.141E-41)
            r0[r1] = r2
            r1 = 291(0x123, float:4.08E-43)
            r2 = 23486(0x5bbe, float:3.2911E-41)
            r0[r1] = r2
            r1 = 292(0x124, float:4.09E-43)
            r2 = 24550(0x5fe6, float:3.4402E-41)
            r0[r1] = r2
            r1 = 293(0x125, float:4.1E-43)
            r2 = 25607(0x6407, float:3.5883E-41)
            r0[r1] = r2
            r1 = 294(0x126, float:4.12E-43)
            r2 = 26656(0x6820, float:3.7353E-41)
            r0[r1] = r2
            r1 = 295(0x127, float:4.13E-43)
            r2 = 27697(0x6c31, float:3.8812E-41)
            r0[r1] = r2
            r1 = 296(0x128, float:4.15E-43)
            r2 = 28729(0x7039, float:4.0258E-41)
            r0[r1] = r2
            r1 = 297(0x129, float:4.16E-43)
            r2 = 29753(0x7439, float:4.1693E-41)
            r0[r1] = r2
            r1 = 298(0x12a, float:4.18E-43)
            r2 = 30767(0x782f, float:4.3114E-41)
            r0[r1] = r2
            r1 = 299(0x12b, float:4.19E-43)
            r2 = 31772(0x7c1c, float:4.4522E-41)
            r0[r1] = r2
            r1 = 300(0x12c, float:4.2E-43)
            r2 = 32768(0x8000, float:4.5918E-41)
            r0[r1] = r2
            r1 = 301(0x12d, float:4.22E-43)
            r2 = 33754(0x83da, float:4.73E-41)
            r0[r1] = r2
            r1 = 302(0x12e, float:4.23E-43)
            r2 = 34729(0x87a9, float:4.8666E-41)
            r0[r1] = r2
            r1 = 303(0x12f, float:4.25E-43)
            r2 = 35693(0x8b6d, float:5.0017E-41)
            r0[r1] = r2
            r1 = 304(0x130, float:4.26E-43)
            r2 = 36647(0x8f27, float:5.1353E-41)
            r0[r1] = r2
            r1 = 305(0x131, float:4.27E-43)
            r2 = 37590(0x92d6, float:5.2675E-41)
            r0[r1] = r2
            r1 = 306(0x132, float:4.29E-43)
            r2 = 38521(0x9679, float:5.398E-41)
            r0[r1] = r2
            r1 = 307(0x133, float:4.3E-43)
            r2 = 39441(0x9a11, float:5.5269E-41)
            r0[r1] = r2
            r1 = 308(0x134, float:4.32E-43)
            r2 = 40348(0x9d9c, float:5.654E-41)
            r0[r1] = r2
            r1 = 309(0x135, float:4.33E-43)
            r2 = 41243(0xa11b, float:5.7794E-41)
            r0[r1] = r2
            r1 = 310(0x136, float:4.34E-43)
            r2 = 42126(0xa48e, float:5.9031E-41)
            r0[r1] = r2
            r1 = 311(0x137, float:4.36E-43)
            r2 = 42995(0xa7f3, float:6.0249E-41)
            r0[r1] = r2
            r1 = 312(0x138, float:4.37E-43)
            r2 = 43852(0xab4c, float:6.145E-41)
            r0[r1] = r2
            r1 = 313(0x139, float:4.39E-43)
            r2 = 44695(0xae97, float:6.2631E-41)
            r0[r1] = r2
            r1 = 314(0x13a, float:4.4E-43)
            r2 = 45525(0xb1d5, float:6.3794E-41)
            r0[r1] = r2
            r1 = 315(0x13b, float:4.41E-43)
            r2 = 46341(0xb505, float:6.4938E-41)
            r0[r1] = r2
            r1 = 316(0x13c, float:4.43E-43)
            r2 = 47143(0xb827, float:6.6061E-41)
            r0[r1] = r2
            r1 = 317(0x13d, float:4.44E-43)
            r2 = 47930(0xbb3a, float:6.7164E-41)
            r0[r1] = r2
            r1 = 318(0x13e, float:4.46E-43)
            r2 = 48703(0xbe3f, float:6.8247E-41)
            r0[r1] = r2
            r1 = 319(0x13f, float:4.47E-43)
            r2 = 49461(0xc135, float:6.931E-41)
            r0[r1] = r2
            r1 = 320(0x140, float:4.48E-43)
            r2 = 50203(0xc41b, float:7.035E-41)
            r0[r1] = r2
            r1 = 321(0x141, float:4.5E-43)
            r2 = 50931(0xc6f3, float:7.137E-41)
            r0[r1] = r2
            r1 = 322(0x142, float:4.51E-43)
            r2 = 51643(0xc9bb, float:7.2367E-41)
            r0[r1] = r2
            r1 = 323(0x143, float:4.53E-43)
            r2 = 52339(0xcc73, float:7.3343E-41)
            r0[r1] = r2
            r1 = 324(0x144, float:4.54E-43)
            r2 = 53020(0xcf1c, float:7.4297E-41)
            r0[r1] = r2
            r1 = 325(0x145, float:4.55E-43)
            r2 = 53684(0xd1b4, float:7.5227E-41)
            r0[r1] = r2
            r1 = 326(0x146, float:4.57E-43)
            r2 = 54332(0xd43c, float:7.6135E-41)
            r0[r1] = r2
            r1 = 327(0x147, float:4.58E-43)
            r2 = 54963(0xd6b3, float:7.702E-41)
            r0[r1] = r2
            r1 = 328(0x148, float:4.6E-43)
            r2 = 55578(0xd91a, float:7.7881E-41)
            r0[r1] = r2
            r1 = 329(0x149, float:4.61E-43)
            r2 = 56175(0xdb6f, float:7.8718E-41)
            r0[r1] = r2
            r1 = 330(0x14a, float:4.62E-43)
            r2 = 56756(0xddb4, float:7.9532E-41)
            r0[r1] = r2
            r1 = 331(0x14b, float:4.64E-43)
            r2 = 57319(0xdfe7, float:8.0321E-41)
            r0[r1] = r2
            r1 = 332(0x14c, float:4.65E-43)
            r2 = 57865(0xe209, float:8.1086E-41)
            r0[r1] = r2
            r1 = 333(0x14d, float:4.67E-43)
            r2 = 58393(0xe419, float:8.1826E-41)
            r0[r1] = r2
            r1 = 334(0x14e, float:4.68E-43)
            r2 = 58903(0xe617, float:8.254E-41)
            r0[r1] = r2
            r1 = 335(0x14f, float:4.7E-43)
            r2 = 59396(0xe804, float:8.3232E-41)
            r0[r1] = r2
            r1 = 336(0x150, float:4.71E-43)
            r2 = 59870(0xe9de, float:8.3896E-41)
            r0[r1] = r2
            r1 = 337(0x151, float:4.72E-43)
            r2 = 60326(0xeba6, float:8.4535E-41)
            r0[r1] = r2
            r1 = 338(0x152, float:4.74E-43)
            r2 = 60764(0xed5c, float:8.5148E-41)
            r0[r1] = r2
            r1 = 339(0x153, float:4.75E-43)
            r2 = 61183(0xeeff, float:8.5736E-41)
            r0[r1] = r2
            r1 = 340(0x154, float:4.76E-43)
            r2 = 61584(0xf090, float:8.6298E-41)
            r0[r1] = r2
            r1 = 341(0x155, float:4.78E-43)
            r2 = 61966(0xf20e, float:8.6833E-41)
            r0[r1] = r2
            r1 = 342(0x156, float:4.79E-43)
            r2 = 62328(0xf378, float:8.734E-41)
            r0[r1] = r2
            r1 = 343(0x157, float:4.8E-43)
            r2 = 62672(0xf4d0, float:8.7822E-41)
            r0[r1] = r2
            r1 = 344(0x158, float:4.82E-43)
            r2 = 62997(0xf615, float:8.8278E-41)
            r0[r1] = r2
            r1 = 345(0x159, float:4.83E-43)
            r2 = 63303(0xf747, float:8.8706E-41)
            r0[r1] = r2
            r1 = 346(0x15a, float:4.85E-43)
            r2 = 63589(0xf865, float:8.9107E-41)
            r0[r1] = r2
            r1 = 347(0x15b, float:4.86E-43)
            r2 = 63856(0xf970, float:8.9481E-41)
            r0[r1] = r2
            r1 = 348(0x15c, float:4.88E-43)
            r2 = 64104(0xfa68, float:8.9829E-41)
            r0[r1] = r2
            r1 = 349(0x15d, float:4.89E-43)
            r2 = 64332(0xfb4c, float:9.0148E-41)
            r0[r1] = r2
            r1 = 350(0x15e, float:4.9E-43)
            r2 = 64540(0xfc1c, float:9.044E-41)
            r0[r1] = r2
            r1 = 351(0x15f, float:4.92E-43)
            r2 = 64729(0xfcd9, float:9.0705E-41)
            r0[r1] = r2
            r1 = 352(0x160, float:4.93E-43)
            r2 = 64898(0xfd82, float:9.0941E-41)
            r0[r1] = r2
            r1 = 353(0x161, float:4.95E-43)
            r2 = 65048(0xfe18, float:9.1152E-41)
            r0[r1] = r2
            r1 = 354(0x162, float:4.96E-43)
            r2 = 65177(0xfe99, float:9.1332E-41)
            r0[r1] = r2
            r1 = 355(0x163, float:4.97E-43)
            r2 = 65287(0xff07, float:9.1487E-41)
            r0[r1] = r2
            r1 = 356(0x164, float:4.99E-43)
            r2 = 65376(0xff60, float:9.1611E-41)
            r0[r1] = r2
            r1 = 357(0x165, float:5.0E-43)
            r2 = 65446(0xffa6, float:9.171E-41)
            r0[r1] = r2
            r1 = 358(0x166, float:5.02E-43)
            r2 = 65496(0xffd8, float:9.178E-41)
            r0[r1] = r2
            r1 = 359(0x167, float:5.03E-43)
            r2 = 65526(0xfff6, float:9.1821E-41)
            r0[r1] = r2
            ua.netlizard.egypt.math.cos = r0
            r0 = 360(0x168, float:5.04E-43)
            int[] r0 = new int[r0]
            r1 = 1144(0x478, float:1.603E-42)
            r0[r8] = r1
            r1 = 2
            r2 = 2287(0x8ef, float:3.205E-42)
            r0[r1] = r2
            r1 = 3430(0xd66, float:4.806E-42)
            r0[r9] = r1
            r1 = 4572(0x11dc, float:6.407E-42)
            r0[r7] = r1
            r1 = 5
            r2 = 5712(0x1650, float:8.004E-42)
            r0[r1] = r2
            r1 = 6
            r2 = 6850(0x1ac2, float:9.599E-42)
            r0[r1] = r2
            r1 = 7
            r2 = 7987(0x1f33, float:1.1192E-41)
            r0[r1] = r2
            r1 = 8
            r2 = 9121(0x23a1, float:1.2781E-41)
            r0[r1] = r2
            r1 = 9
            r2 = 10252(0x280c, float:1.4366E-41)
            r0[r1] = r2
            r1 = 10
            r2 = 11380(0x2c74, float:1.5947E-41)
            r0[r1] = r2
            r1 = 11
            r2 = 12505(0x30d9, float:1.7523E-41)
            r0[r1] = r2
            r1 = 13626(0x353a, float:1.9094E-41)
            r0[r5] = r1
            r1 = 13
            r2 = 14742(0x3996, float:2.0658E-41)
            r0[r1] = r2
            r1 = 14
            r2 = 15855(0x3def, float:2.2218E-41)
            r0[r1] = r2
            r1 = 15
            r2 = 16962(0x4242, float:2.3769E-41)
            r0[r1] = r2
            r1 = 16
            r2 = 18064(0x4690, float:2.5313E-41)
            r0[r1] = r2
            r1 = 17
            r2 = 19161(0x4ad9, float:2.685E-41)
            r0[r1] = r2
            r1 = 18
            r2 = 20252(0x4f1c, float:2.8379E-41)
            r0[r1] = r2
            r1 = 19
            r2 = 21336(0x5358, float:2.9898E-41)
            r0[r1] = r2
            r1 = 20
            r2 = 22415(0x578f, float:3.141E-41)
            r0[r1] = r2
            r1 = 21
            r2 = 23486(0x5bbe, float:3.2911E-41)
            r0[r1] = r2
            r1 = 22
            r2 = 24550(0x5fe6, float:3.4402E-41)
            r0[r1] = r2
            r1 = 23
            r2 = 25607(0x6407, float:3.5883E-41)
            r0[r1] = r2
            r1 = 24
            r2 = 26656(0x6820, float:3.7353E-41)
            r0[r1] = r2
            r1 = 25
            r2 = 27697(0x6c31, float:3.8812E-41)
            r0[r1] = r2
            r1 = 26
            r2 = 28729(0x7039, float:4.0258E-41)
            r0[r1] = r2
            r1 = 27
            r2 = 29753(0x7439, float:4.1693E-41)
            r0[r1] = r2
            r1 = 28
            r2 = 30767(0x782f, float:4.3114E-41)
            r0[r1] = r2
            r1 = 29
            r2 = 31772(0x7c1c, float:4.4522E-41)
            r0[r1] = r2
            r1 = 30
            r2 = 32768(0x8000, float:4.5918E-41)
            r0[r1] = r2
            r1 = 31
            r2 = 33754(0x83da, float:4.73E-41)
            r0[r1] = r2
            r1 = 32
            r2 = 34729(0x87a9, float:4.8666E-41)
            r0[r1] = r2
            r1 = 33
            r2 = 35693(0x8b6d, float:5.0017E-41)
            r0[r1] = r2
            r1 = 34
            r2 = 36647(0x8f27, float:5.1353E-41)
            r0[r1] = r2
            r1 = 35
            r2 = 37590(0x92d6, float:5.2675E-41)
            r0[r1] = r2
            r1 = 36
            r2 = 38521(0x9679, float:5.398E-41)
            r0[r1] = r2
            r1 = 37
            r2 = 39441(0x9a11, float:5.5269E-41)
            r0[r1] = r2
            r1 = 38
            r2 = 40348(0x9d9c, float:5.654E-41)
            r0[r1] = r2
            r1 = 39
            r2 = 41243(0xa11b, float:5.7794E-41)
            r0[r1] = r2
            r1 = 40
            r2 = 42126(0xa48e, float:5.9031E-41)
            r0[r1] = r2
            r1 = 41
            r2 = 42995(0xa7f3, float:6.0249E-41)
            r0[r1] = r2
            r1 = 42
            r2 = 43852(0xab4c, float:6.145E-41)
            r0[r1] = r2
            r1 = 43
            r2 = 44695(0xae97, float:6.2631E-41)
            r0[r1] = r2
            r1 = 44
            r2 = 45525(0xb1d5, float:6.3794E-41)
            r0[r1] = r2
            r1 = 45
            r2 = 46341(0xb505, float:6.4938E-41)
            r0[r1] = r2
            r1 = 46
            r2 = 47143(0xb827, float:6.6061E-41)
            r0[r1] = r2
            r1 = 47
            r2 = 47930(0xbb3a, float:6.7164E-41)
            r0[r1] = r2
            r1 = 48
            r2 = 48703(0xbe3f, float:6.8247E-41)
            r0[r1] = r2
            r1 = 49
            r2 = 49461(0xc135, float:6.931E-41)
            r0[r1] = r2
            r1 = 50
            r2 = 50203(0xc41b, float:7.035E-41)
            r0[r1] = r2
            r1 = 51
            r2 = 50931(0xc6f3, float:7.137E-41)
            r0[r1] = r2
            r1 = 52
            r2 = 51643(0xc9bb, float:7.2367E-41)
            r0[r1] = r2
            r1 = 53
            r2 = 52339(0xcc73, float:7.3343E-41)
            r0[r1] = r2
            r1 = 54
            r2 = 53020(0xcf1c, float:7.4297E-41)
            r0[r1] = r2
            r1 = 55
            r2 = 53684(0xd1b4, float:7.5227E-41)
            r0[r1] = r2
            r1 = 56
            r2 = 54332(0xd43c, float:7.6135E-41)
            r0[r1] = r2
            r1 = 57
            r2 = 54963(0xd6b3, float:7.702E-41)
            r0[r1] = r2
            r1 = 58
            r2 = 55578(0xd91a, float:7.7881E-41)
            r0[r1] = r2
            r1 = 56175(0xdb6f, float:7.8718E-41)
            r0[r6] = r1
            r1 = 60
            r2 = 56756(0xddb4, float:7.9532E-41)
            r0[r1] = r2
            r1 = 61
            r2 = 57319(0xdfe7, float:8.0321E-41)
            r0[r1] = r2
            r1 = 62
            r2 = 57865(0xe209, float:8.1086E-41)
            r0[r1] = r2
            r1 = 63
            r2 = 58393(0xe419, float:8.1826E-41)
            r0[r1] = r2
            r1 = 64
            r2 = 58903(0xe617, float:8.254E-41)
            r0[r1] = r2
            r1 = 65
            r2 = 59396(0xe804, float:8.3232E-41)
            r0[r1] = r2
            r1 = 66
            r2 = 59870(0xe9de, float:8.3896E-41)
            r0[r1] = r2
            r1 = 67
            r2 = 60326(0xeba6, float:8.4535E-41)
            r0[r1] = r2
            r1 = 68
            r2 = 60764(0xed5c, float:8.5148E-41)
            r0[r1] = r2
            r1 = 69
            r2 = 61183(0xeeff, float:8.5736E-41)
            r0[r1] = r2
            r1 = 70
            r2 = 61584(0xf090, float:8.6298E-41)
            r0[r1] = r2
            r1 = 71
            r2 = 61966(0xf20e, float:8.6833E-41)
            r0[r1] = r2
            r1 = 72
            r2 = 62328(0xf378, float:8.734E-41)
            r0[r1] = r2
            r1 = 73
            r2 = 62672(0xf4d0, float:8.7822E-41)
            r0[r1] = r2
            r1 = 74
            r2 = 62997(0xf615, float:8.8278E-41)
            r0[r1] = r2
            r1 = 75
            r2 = 63303(0xf747, float:8.8706E-41)
            r0[r1] = r2
            r1 = 76
            r2 = 63589(0xf865, float:8.9107E-41)
            r0[r1] = r2
            r1 = 77
            r2 = 63856(0xf970, float:8.9481E-41)
            r0[r1] = r2
            r1 = 78
            r2 = 64104(0xfa68, float:8.9829E-41)
            r0[r1] = r2
            r1 = 79
            r2 = 64332(0xfb4c, float:9.0148E-41)
            r0[r1] = r2
            r1 = 80
            r2 = 64540(0xfc1c, float:9.044E-41)
            r0[r1] = r2
            r1 = 81
            r2 = 64729(0xfcd9, float:9.0705E-41)
            r0[r1] = r2
            r1 = 82
            r2 = 64898(0xfd82, float:9.0941E-41)
            r0[r1] = r2
            r1 = 83
            r2 = 65048(0xfe18, float:9.1152E-41)
            r0[r1] = r2
            r1 = 84
            r2 = 65177(0xfe99, float:9.1332E-41)
            r0[r1] = r2
            r1 = 85
            r2 = 65287(0xff07, float:9.1487E-41)
            r0[r1] = r2
            r1 = 86
            r2 = 65376(0xff60, float:9.1611E-41)
            r0[r1] = r2
            r1 = 87
            r2 = 65446(0xffa6, float:9.171E-41)
            r0[r1] = r2
            r1 = 88
            r2 = 65496(0xffd8, float:9.178E-41)
            r0[r1] = r2
            r1 = 89
            r2 = 65526(0xfff6, float:9.1821E-41)
            r0[r1] = r2
            r1 = 90
            r2 = 65536(0x10000, float:9.18355E-41)
            r0[r1] = r2
            r1 = 91
            r2 = 65526(0xfff6, float:9.1821E-41)
            r0[r1] = r2
            r1 = 92
            r2 = 65496(0xffd8, float:9.178E-41)
            r0[r1] = r2
            r1 = 93
            r2 = 65446(0xffa6, float:9.171E-41)
            r0[r1] = r2
            r1 = 94
            r2 = 65376(0xff60, float:9.1611E-41)
            r0[r1] = r2
            r1 = 95
            r2 = 65287(0xff07, float:9.1487E-41)
            r0[r1] = r2
            r1 = 96
            r2 = 65177(0xfe99, float:9.1332E-41)
            r0[r1] = r2
            r1 = 97
            r2 = 65048(0xfe18, float:9.1152E-41)
            r0[r1] = r2
            r1 = 98
            r2 = 64898(0xfd82, float:9.0941E-41)
            r0[r1] = r2
            r1 = 99
            r2 = 64729(0xfcd9, float:9.0705E-41)
            r0[r1] = r2
            r1 = 100
            r2 = 64540(0xfc1c, float:9.044E-41)
            r0[r1] = r2
            r1 = 101(0x65, float:1.42E-43)
            r2 = 64332(0xfb4c, float:9.0148E-41)
            r0[r1] = r2
            r1 = 102(0x66, float:1.43E-43)
            r2 = 64104(0xfa68, float:8.9829E-41)
            r0[r1] = r2
            r1 = 103(0x67, float:1.44E-43)
            r2 = 63856(0xf970, float:8.9481E-41)
            r0[r1] = r2
            r1 = 104(0x68, float:1.46E-43)
            r2 = 63589(0xf865, float:8.9107E-41)
            r0[r1] = r2
            r1 = 105(0x69, float:1.47E-43)
            r2 = 63303(0xf747, float:8.8706E-41)
            r0[r1] = r2
            r1 = 106(0x6a, float:1.49E-43)
            r2 = 62997(0xf615, float:8.8278E-41)
            r0[r1] = r2
            r1 = 107(0x6b, float:1.5E-43)
            r2 = 62672(0xf4d0, float:8.7822E-41)
            r0[r1] = r2
            r1 = 108(0x6c, float:1.51E-43)
            r2 = 62328(0xf378, float:8.734E-41)
            r0[r1] = r2
            r1 = 109(0x6d, float:1.53E-43)
            r2 = 61966(0xf20e, float:8.6833E-41)
            r0[r1] = r2
            r1 = 110(0x6e, float:1.54E-43)
            r2 = 61584(0xf090, float:8.6298E-41)
            r0[r1] = r2
            r1 = 111(0x6f, float:1.56E-43)
            r2 = 61183(0xeeff, float:8.5736E-41)
            r0[r1] = r2
            r1 = 112(0x70, float:1.57E-43)
            r2 = 60764(0xed5c, float:8.5148E-41)
            r0[r1] = r2
            r1 = 113(0x71, float:1.58E-43)
            r2 = 60326(0xeba6, float:8.4535E-41)
            r0[r1] = r2
            r1 = 114(0x72, float:1.6E-43)
            r2 = 59870(0xe9de, float:8.3896E-41)
            r0[r1] = r2
            r1 = 115(0x73, float:1.61E-43)
            r2 = 59396(0xe804, float:8.3232E-41)
            r0[r1] = r2
            r1 = 116(0x74, float:1.63E-43)
            r2 = 58903(0xe617, float:8.254E-41)
            r0[r1] = r2
            r1 = 117(0x75, float:1.64E-43)
            r2 = 58393(0xe419, float:8.1826E-41)
            r0[r1] = r2
            r1 = 118(0x76, float:1.65E-43)
            r2 = 57865(0xe209, float:8.1086E-41)
            r0[r1] = r2
            r1 = 119(0x77, float:1.67E-43)
            r2 = 57319(0xdfe7, float:8.0321E-41)
            r0[r1] = r2
            r1 = 120(0x78, float:1.68E-43)
            r2 = 56756(0xddb4, float:7.9532E-41)
            r0[r1] = r2
            r1 = 121(0x79, float:1.7E-43)
            r2 = 56175(0xdb6f, float:7.8718E-41)
            r0[r1] = r2
            r1 = 122(0x7a, float:1.71E-43)
            r2 = 55578(0xd91a, float:7.7881E-41)
            r0[r1] = r2
            r1 = 123(0x7b, float:1.72E-43)
            r2 = 54963(0xd6b3, float:7.702E-41)
            r0[r1] = r2
            r1 = 124(0x7c, float:1.74E-43)
            r2 = 54332(0xd43c, float:7.6135E-41)
            r0[r1] = r2
            r1 = 125(0x7d, float:1.75E-43)
            r2 = 53684(0xd1b4, float:7.5227E-41)
            r0[r1] = r2
            r1 = 126(0x7e, float:1.77E-43)
            r2 = 53020(0xcf1c, float:7.4297E-41)
            r0[r1] = r2
            r1 = 127(0x7f, float:1.78E-43)
            r2 = 52339(0xcc73, float:7.3343E-41)
            r0[r1] = r2
            r1 = 128(0x80, float:1.794E-43)
            r2 = 51643(0xc9bb, float:7.2367E-41)
            r0[r1] = r2
            r1 = 129(0x81, float:1.81E-43)
            r2 = 50931(0xc6f3, float:7.137E-41)
            r0[r1] = r2
            r1 = 130(0x82, float:1.82E-43)
            r2 = 50203(0xc41b, float:7.035E-41)
            r0[r1] = r2
            r1 = 131(0x83, float:1.84E-43)
            r2 = 49461(0xc135, float:6.931E-41)
            r0[r1] = r2
            r1 = 132(0x84, float:1.85E-43)
            r2 = 48703(0xbe3f, float:6.8247E-41)
            r0[r1] = r2
            r1 = 133(0x85, float:1.86E-43)
            r2 = 47930(0xbb3a, float:6.7164E-41)
            r0[r1] = r2
            r1 = 134(0x86, float:1.88E-43)
            r2 = 47143(0xb827, float:6.6061E-41)
            r0[r1] = r2
            r1 = 135(0x87, float:1.89E-43)
            r2 = 46341(0xb505, float:6.4938E-41)
            r0[r1] = r2
            r1 = 136(0x88, float:1.9E-43)
            r2 = 45525(0xb1d5, float:6.3794E-41)
            r0[r1] = r2
            r1 = 137(0x89, float:1.92E-43)
            r2 = 44695(0xae97, float:6.2631E-41)
            r0[r1] = r2
            r1 = 138(0x8a, float:1.93E-43)
            r2 = 43852(0xab4c, float:6.145E-41)
            r0[r1] = r2
            r1 = 139(0x8b, float:1.95E-43)
            r2 = 42995(0xa7f3, float:6.0249E-41)
            r0[r1] = r2
            r1 = 140(0x8c, float:1.96E-43)
            r2 = 42126(0xa48e, float:5.9031E-41)
            r0[r1] = r2
            r1 = 141(0x8d, float:1.98E-43)
            r2 = 41243(0xa11b, float:5.7794E-41)
            r0[r1] = r2
            r1 = 142(0x8e, float:1.99E-43)
            r2 = 40348(0x9d9c, float:5.654E-41)
            r0[r1] = r2
            r1 = 143(0x8f, float:2.0E-43)
            r2 = 39441(0x9a11, float:5.5269E-41)
            r0[r1] = r2
            r1 = 144(0x90, float:2.02E-43)
            r2 = 38521(0x9679, float:5.398E-41)
            r0[r1] = r2
            r1 = 145(0x91, float:2.03E-43)
            r2 = 37590(0x92d6, float:5.2675E-41)
            r0[r1] = r2
            r1 = 146(0x92, float:2.05E-43)
            r2 = 36647(0x8f27, float:5.1353E-41)
            r0[r1] = r2
            r1 = 147(0x93, float:2.06E-43)
            r2 = 35693(0x8b6d, float:5.0017E-41)
            r0[r1] = r2
            r1 = 148(0x94, float:2.07E-43)
            r2 = 34729(0x87a9, float:4.8666E-41)
            r0[r1] = r2
            r1 = 149(0x95, float:2.09E-43)
            r2 = 33754(0x83da, float:4.73E-41)
            r0[r1] = r2
            r1 = 150(0x96, float:2.1E-43)
            r2 = 32768(0x8000, float:4.5918E-41)
            r0[r1] = r2
            r1 = 151(0x97, float:2.12E-43)
            r2 = 31772(0x7c1c, float:4.4522E-41)
            r0[r1] = r2
            r1 = 152(0x98, float:2.13E-43)
            r2 = 30767(0x782f, float:4.3114E-41)
            r0[r1] = r2
            r1 = 153(0x99, float:2.14E-43)
            r2 = 29753(0x7439, float:4.1693E-41)
            r0[r1] = r2
            r1 = 154(0x9a, float:2.16E-43)
            r2 = 28729(0x7039, float:4.0258E-41)
            r0[r1] = r2
            r1 = 155(0x9b, float:2.17E-43)
            r2 = 27697(0x6c31, float:3.8812E-41)
            r0[r1] = r2
            r1 = 156(0x9c, float:2.19E-43)
            r2 = 26656(0x6820, float:3.7353E-41)
            r0[r1] = r2
            r1 = 157(0x9d, float:2.2E-43)
            r2 = 25607(0x6407, float:3.5883E-41)
            r0[r1] = r2
            r1 = 158(0x9e, float:2.21E-43)
            r2 = 24550(0x5fe6, float:3.4402E-41)
            r0[r1] = r2
            r1 = 159(0x9f, float:2.23E-43)
            r2 = 23486(0x5bbe, float:3.2911E-41)
            r0[r1] = r2
            r1 = 160(0xa0, float:2.24E-43)
            r2 = 22415(0x578f, float:3.141E-41)
            r0[r1] = r2
            r1 = 161(0xa1, float:2.26E-43)
            r2 = 21336(0x5358, float:2.9898E-41)
            r0[r1] = r2
            r1 = 162(0xa2, float:2.27E-43)
            r2 = 20252(0x4f1c, float:2.8379E-41)
            r0[r1] = r2
            r1 = 163(0xa3, float:2.28E-43)
            r2 = 19161(0x4ad9, float:2.685E-41)
            r0[r1] = r2
            r1 = 164(0xa4, float:2.3E-43)
            r2 = 18064(0x4690, float:2.5313E-41)
            r0[r1] = r2
            r1 = 165(0xa5, float:2.31E-43)
            r2 = 16962(0x4242, float:2.3769E-41)
            r0[r1] = r2
            r1 = 166(0xa6, float:2.33E-43)
            r2 = 15855(0x3def, float:2.2218E-41)
            r0[r1] = r2
            r1 = 167(0xa7, float:2.34E-43)
            r2 = 14742(0x3996, float:2.0658E-41)
            r0[r1] = r2
            r1 = 168(0xa8, float:2.35E-43)
            r2 = 13626(0x353a, float:1.9094E-41)
            r0[r1] = r2
            r1 = 169(0xa9, float:2.37E-43)
            r2 = 12505(0x30d9, float:1.7523E-41)
            r0[r1] = r2
            r1 = 170(0xaa, float:2.38E-43)
            r2 = 11380(0x2c74, float:1.5947E-41)
            r0[r1] = r2
            r1 = 171(0xab, float:2.4E-43)
            r2 = 10252(0x280c, float:1.4366E-41)
            r0[r1] = r2
            r1 = 172(0xac, float:2.41E-43)
            r2 = 9121(0x23a1, float:1.2781E-41)
            r0[r1] = r2
            r1 = 173(0xad, float:2.42E-43)
            r2 = 7987(0x1f33, float:1.1192E-41)
            r0[r1] = r2
            r1 = 174(0xae, float:2.44E-43)
            r2 = 6850(0x1ac2, float:9.599E-42)
            r0[r1] = r2
            r1 = 175(0xaf, float:2.45E-43)
            r2 = 5712(0x1650, float:8.004E-42)
            r0[r1] = r2
            r1 = 176(0xb0, float:2.47E-43)
            r2 = 4572(0x11dc, float:6.407E-42)
            r0[r1] = r2
            r1 = 177(0xb1, float:2.48E-43)
            r2 = 3430(0xd66, float:4.806E-42)
            r0[r1] = r2
            r1 = 178(0xb2, float:2.5E-43)
            r2 = 2287(0x8ef, float:3.205E-42)
            r0[r1] = r2
            r1 = 179(0xb3, float:2.51E-43)
            r2 = 1144(0x478, float:1.603E-42)
            r0[r1] = r2
            r1 = 181(0xb5, float:2.54E-43)
            r2 = -1144(0xfffffffffffffb88, float:NaN)
            r0[r1] = r2
            r1 = 182(0xb6, float:2.55E-43)
            r2 = -2287(0xfffffffffffff711, float:NaN)
            r0[r1] = r2
            r1 = 183(0xb7, float:2.56E-43)
            r2 = -3430(0xfffffffffffff29a, float:NaN)
            r0[r1] = r2
            r1 = 184(0xb8, float:2.58E-43)
            r2 = -4572(0xffffffffffffee24, float:NaN)
            r0[r1] = r2
            r1 = 185(0xb9, float:2.59E-43)
            r2 = -5712(0xffffffffffffe9b0, float:NaN)
            r0[r1] = r2
            r1 = 186(0xba, float:2.6E-43)
            r2 = -6850(0xffffffffffffe53e, float:NaN)
            r0[r1] = r2
            r1 = 187(0xbb, float:2.62E-43)
            r2 = -7987(0xffffffffffffe0cd, float:NaN)
            r0[r1] = r2
            r1 = 188(0xbc, float:2.63E-43)
            r2 = -9121(0xffffffffffffdc5f, float:NaN)
            r0[r1] = r2
            r1 = 189(0xbd, float:2.65E-43)
            r2 = -10252(0xffffffffffffd7f4, float:NaN)
            r0[r1] = r2
            r1 = 190(0xbe, float:2.66E-43)
            r2 = -11380(0xffffffffffffd38c, float:NaN)
            r0[r1] = r2
            r1 = 191(0xbf, float:2.68E-43)
            r2 = -12505(0xffffffffffffcf27, float:NaN)
            r0[r1] = r2
            r1 = 192(0xc0, float:2.69E-43)
            r2 = -13626(0xffffffffffffcac6, float:NaN)
            r0[r1] = r2
            r1 = 193(0xc1, float:2.7E-43)
            r2 = -14742(0xffffffffffffc66a, float:NaN)
            r0[r1] = r2
            r1 = 194(0xc2, float:2.72E-43)
            r2 = -15855(0xffffffffffffc211, float:NaN)
            r0[r1] = r2
            r1 = 195(0xc3, float:2.73E-43)
            r2 = -16962(0xffffffffffffbdbe, float:NaN)
            r0[r1] = r2
            r1 = 196(0xc4, float:2.75E-43)
            r2 = -18064(0xffffffffffffb970, float:NaN)
            r0[r1] = r2
            r1 = 197(0xc5, float:2.76E-43)
            r2 = -19161(0xffffffffffffb527, float:NaN)
            r0[r1] = r2
            r1 = 198(0xc6, float:2.77E-43)
            r2 = -20252(0xffffffffffffb0e4, float:NaN)
            r0[r1] = r2
            r1 = 199(0xc7, float:2.79E-43)
            r2 = -21336(0xffffffffffffaca8, float:NaN)
            r0[r1] = r2
            r1 = 200(0xc8, float:2.8E-43)
            r2 = -22415(0xffffffffffffa871, float:NaN)
            r0[r1] = r2
            r1 = 201(0xc9, float:2.82E-43)
            r2 = -23486(0xffffffffffffa442, float:NaN)
            r0[r1] = r2
            r1 = 202(0xca, float:2.83E-43)
            r2 = -24550(0xffffffffffffa01a, float:NaN)
            r0[r1] = r2
            r1 = 203(0xcb, float:2.84E-43)
            r2 = -25607(0xffffffffffff9bf9, float:NaN)
            r0[r1] = r2
            r1 = 204(0xcc, float:2.86E-43)
            r2 = -26656(0xffffffffffff97e0, float:NaN)
            r0[r1] = r2
            r1 = 205(0xcd, float:2.87E-43)
            r2 = -27697(0xffffffffffff93cf, float:NaN)
            r0[r1] = r2
            r1 = 206(0xce, float:2.89E-43)
            r2 = -28729(0xffffffffffff8fc7, float:NaN)
            r0[r1] = r2
            r1 = 207(0xcf, float:2.9E-43)
            r2 = -29753(0xffffffffffff8bc7, float:NaN)
            r0[r1] = r2
            r1 = 208(0xd0, float:2.91E-43)
            r2 = -30767(0xffffffffffff87d1, float:NaN)
            r0[r1] = r2
            r1 = 209(0xd1, float:2.93E-43)
            r2 = -31772(0xffffffffffff83e4, float:NaN)
            r0[r1] = r2
            r1 = 210(0xd2, float:2.94E-43)
            r2 = -32768(0xffffffffffff8000, float:NaN)
            r0[r1] = r2
            r1 = 211(0xd3, float:2.96E-43)
            r2 = -33754(0xffffffffffff7c26, float:NaN)
            r0[r1] = r2
            r1 = 212(0xd4, float:2.97E-43)
            r2 = -34729(0xffffffffffff7857, float:NaN)
            r0[r1] = r2
            r1 = 213(0xd5, float:2.98E-43)
            r2 = -35693(0xffffffffffff7493, float:NaN)
            r0[r1] = r2
            r1 = 214(0xd6, float:3.0E-43)
            r2 = -36647(0xffffffffffff70d9, float:NaN)
            r0[r1] = r2
            r1 = 215(0xd7, float:3.01E-43)
            r2 = -37590(0xffffffffffff6d2a, float:NaN)
            r0[r1] = r2
            r1 = 216(0xd8, float:3.03E-43)
            r2 = -38521(0xffffffffffff6987, float:NaN)
            r0[r1] = r2
            r1 = 217(0xd9, float:3.04E-43)
            r2 = -39441(0xffffffffffff65ef, float:NaN)
            r0[r1] = r2
            r1 = 218(0xda, float:3.05E-43)
            r2 = -40348(0xffffffffffff6264, float:NaN)
            r0[r1] = r2
            r1 = 219(0xdb, float:3.07E-43)
            r2 = -41243(0xffffffffffff5ee5, float:NaN)
            r0[r1] = r2
            r1 = 220(0xdc, float:3.08E-43)
            r2 = -42126(0xffffffffffff5b72, float:NaN)
            r0[r1] = r2
            r1 = 221(0xdd, float:3.1E-43)
            r2 = -42995(0xffffffffffff580d, float:NaN)
            r0[r1] = r2
            r1 = 222(0xde, float:3.11E-43)
            r2 = -43852(0xffffffffffff54b4, float:NaN)
            r0[r1] = r2
            r1 = 223(0xdf, float:3.12E-43)
            r2 = -44695(0xffffffffffff5169, float:NaN)
            r0[r1] = r2
            r1 = 224(0xe0, float:3.14E-43)
            r2 = -45525(0xffffffffffff4e2b, float:NaN)
            r0[r1] = r2
            r1 = 225(0xe1, float:3.15E-43)
            r2 = -46341(0xffffffffffff4afb, float:NaN)
            r0[r1] = r2
            r1 = 226(0xe2, float:3.17E-43)
            r2 = -47143(0xffffffffffff47d9, float:NaN)
            r0[r1] = r2
            r1 = 227(0xe3, float:3.18E-43)
            r2 = -47930(0xffffffffffff44c6, float:NaN)
            r0[r1] = r2
            r1 = 228(0xe4, float:3.2E-43)
            r2 = -48703(0xffffffffffff41c1, float:NaN)
            r0[r1] = r2
            r1 = 229(0xe5, float:3.21E-43)
            r2 = -49461(0xffffffffffff3ecb, float:NaN)
            r0[r1] = r2
            r1 = 230(0xe6, float:3.22E-43)
            r2 = -50203(0xffffffffffff3be5, float:NaN)
            r0[r1] = r2
            r1 = 231(0xe7, float:3.24E-43)
            r2 = -50931(0xffffffffffff390d, float:NaN)
            r0[r1] = r2
            r1 = 232(0xe8, float:3.25E-43)
            r2 = -51643(0xffffffffffff3645, float:NaN)
            r0[r1] = r2
            r1 = 233(0xe9, float:3.27E-43)
            r2 = -52339(0xffffffffffff338d, float:NaN)
            r0[r1] = r2
            r1 = 234(0xea, float:3.28E-43)
            r2 = -53020(0xffffffffffff30e4, float:NaN)
            r0[r1] = r2
            r1 = 235(0xeb, float:3.3E-43)
            r2 = -53684(0xffffffffffff2e4c, float:NaN)
            r0[r1] = r2
            r1 = 236(0xec, float:3.31E-43)
            r2 = -54332(0xffffffffffff2bc4, float:NaN)
            r0[r1] = r2
            r1 = 237(0xed, float:3.32E-43)
            r2 = -54963(0xffffffffffff294d, float:NaN)
            r0[r1] = r2
            r1 = 238(0xee, float:3.34E-43)
            r2 = -55578(0xffffffffffff26e6, float:NaN)
            r0[r1] = r2
            r1 = 239(0xef, float:3.35E-43)
            r2 = -56175(0xffffffffffff2491, float:NaN)
            r0[r1] = r2
            r1 = 240(0xf0, float:3.36E-43)
            r2 = -56756(0xffffffffffff224c, float:NaN)
            r0[r1] = r2
            r1 = 241(0xf1, float:3.38E-43)
            r2 = -57319(0xffffffffffff2019, float:NaN)
            r0[r1] = r2
            r1 = 242(0xf2, float:3.39E-43)
            r2 = -57865(0xffffffffffff1df7, float:NaN)
            r0[r1] = r2
            r1 = 243(0xf3, float:3.4E-43)
            r2 = -58393(0xffffffffffff1be7, float:NaN)
            r0[r1] = r2
            r1 = 244(0xf4, float:3.42E-43)
            r2 = -58903(0xffffffffffff19e9, float:NaN)
            r0[r1] = r2
            r1 = 245(0xf5, float:3.43E-43)
            r2 = -59396(0xffffffffffff17fc, float:NaN)
            r0[r1] = r2
            r1 = 246(0xf6, float:3.45E-43)
            r2 = -59870(0xffffffffffff1622, float:NaN)
            r0[r1] = r2
            r1 = 247(0xf7, float:3.46E-43)
            r2 = -60326(0xffffffffffff145a, float:NaN)
            r0[r1] = r2
            r1 = 248(0xf8, float:3.48E-43)
            r2 = -60764(0xffffffffffff12a4, float:NaN)
            r0[r1] = r2
            r1 = 249(0xf9, float:3.49E-43)
            r2 = -61183(0xffffffffffff1101, float:NaN)
            r0[r1] = r2
            r1 = 250(0xfa, float:3.5E-43)
            r2 = -61584(0xffffffffffff0f70, float:NaN)
            r0[r1] = r2
            r1 = 251(0xfb, float:3.52E-43)
            r2 = -61966(0xffffffffffff0df2, float:NaN)
            r0[r1] = r2
            r1 = 252(0xfc, float:3.53E-43)
            r2 = -62328(0xffffffffffff0c88, float:NaN)
            r0[r1] = r2
            r1 = 253(0xfd, float:3.55E-43)
            r2 = -62672(0xffffffffffff0b30, float:NaN)
            r0[r1] = r2
            r1 = 254(0xfe, float:3.56E-43)
            r2 = -62997(0xffffffffffff09eb, float:NaN)
            r0[r1] = r2
            r1 = 255(0xff, float:3.57E-43)
            r2 = -63303(0xffffffffffff08b9, float:NaN)
            r0[r1] = r2
            r1 = 256(0x100, float:3.59E-43)
            r2 = -63589(0xffffffffffff079b, float:NaN)
            r0[r1] = r2
            r1 = 257(0x101, float:3.6E-43)
            r2 = -63856(0xffffffffffff0690, float:NaN)
            r0[r1] = r2
            r1 = 258(0x102, float:3.62E-43)
            r2 = -64104(0xffffffffffff0598, float:NaN)
            r0[r1] = r2
            r1 = 259(0x103, float:3.63E-43)
            r2 = -64332(0xffffffffffff04b4, float:NaN)
            r0[r1] = r2
            r1 = 260(0x104, float:3.64E-43)
            r2 = -64540(0xffffffffffff03e4, float:NaN)
            r0[r1] = r2
            r1 = 261(0x105, float:3.66E-43)
            r2 = -64729(0xffffffffffff0327, float:NaN)
            r0[r1] = r2
            r1 = 262(0x106, float:3.67E-43)
            r2 = -64898(0xffffffffffff027e, float:NaN)
            r0[r1] = r2
            r1 = 263(0x107, float:3.69E-43)
            r2 = -65048(0xffffffffffff01e8, float:NaN)
            r0[r1] = r2
            r1 = 264(0x108, float:3.7E-43)
            r2 = -65177(0xffffffffffff0167, float:NaN)
            r0[r1] = r2
            r1 = 265(0x109, float:3.71E-43)
            r2 = -65287(0xffffffffffff00f9, float:NaN)
            r0[r1] = r2
            r1 = 266(0x10a, float:3.73E-43)
            r2 = -65376(0xffffffffffff00a0, float:NaN)
            r0[r1] = r2
            r1 = 267(0x10b, float:3.74E-43)
            r2 = -65446(0xffffffffffff005a, float:NaN)
            r0[r1] = r2
            r1 = 268(0x10c, float:3.76E-43)
            r2 = -65496(0xffffffffffff0028, float:NaN)
            r0[r1] = r2
            r1 = 269(0x10d, float:3.77E-43)
            r2 = -65526(0xffffffffffff000a, float:NaN)
            r0[r1] = r2
            r1 = 270(0x10e, float:3.78E-43)
            r2 = -65536(0xffffffffffff0000, float:NaN)
            r0[r1] = r2
            r1 = 271(0x10f, float:3.8E-43)
            r2 = -65526(0xffffffffffff000a, float:NaN)
            r0[r1] = r2
            r1 = 272(0x110, float:3.81E-43)
            r2 = -65496(0xffffffffffff0028, float:NaN)
            r0[r1] = r2
            r1 = 273(0x111, float:3.83E-43)
            r2 = -65446(0xffffffffffff005a, float:NaN)
            r0[r1] = r2
            r1 = 274(0x112, float:3.84E-43)
            r2 = -65376(0xffffffffffff00a0, float:NaN)
            r0[r1] = r2
            r1 = 275(0x113, float:3.85E-43)
            r2 = -65287(0xffffffffffff00f9, float:NaN)
            r0[r1] = r2
            r1 = 276(0x114, float:3.87E-43)
            r2 = -65177(0xffffffffffff0167, float:NaN)
            r0[r1] = r2
            r1 = 277(0x115, float:3.88E-43)
            r2 = -65048(0xffffffffffff01e8, float:NaN)
            r0[r1] = r2
            r1 = 278(0x116, float:3.9E-43)
            r2 = -64898(0xffffffffffff027e, float:NaN)
            r0[r1] = r2
            r1 = 279(0x117, float:3.91E-43)
            r2 = -64729(0xffffffffffff0327, float:NaN)
            r0[r1] = r2
            r1 = 280(0x118, float:3.92E-43)
            r2 = -64540(0xffffffffffff03e4, float:NaN)
            r0[r1] = r2
            r1 = 281(0x119, float:3.94E-43)
            r2 = -64332(0xffffffffffff04b4, float:NaN)
            r0[r1] = r2
            r1 = 282(0x11a, float:3.95E-43)
            r2 = -64104(0xffffffffffff0598, float:NaN)
            r0[r1] = r2
            r1 = 283(0x11b, float:3.97E-43)
            r2 = -63856(0xffffffffffff0690, float:NaN)
            r0[r1] = r2
            r1 = 284(0x11c, float:3.98E-43)
            r2 = -63589(0xffffffffffff079b, float:NaN)
            r0[r1] = r2
            r1 = 285(0x11d, float:4.0E-43)
            r2 = -63303(0xffffffffffff08b9, float:NaN)
            r0[r1] = r2
            r1 = 286(0x11e, float:4.01E-43)
            r2 = -62997(0xffffffffffff09eb, float:NaN)
            r0[r1] = r2
            r1 = 287(0x11f, float:4.02E-43)
            r2 = -62672(0xffffffffffff0b30, float:NaN)
            r0[r1] = r2
            r1 = 288(0x120, float:4.04E-43)
            r2 = -62328(0xffffffffffff0c88, float:NaN)
            r0[r1] = r2
            r1 = 289(0x121, float:4.05E-43)
            r2 = -61966(0xffffffffffff0df2, float:NaN)
            r0[r1] = r2
            r1 = 290(0x122, float:4.06E-43)
            r2 = -61584(0xffffffffffff0f70, float:NaN)
            r0[r1] = r2
            r1 = 291(0x123, float:4.08E-43)
            r2 = -61183(0xffffffffffff1101, float:NaN)
            r0[r1] = r2
            r1 = 292(0x124, float:4.09E-43)
            r2 = -60764(0xffffffffffff12a4, float:NaN)
            r0[r1] = r2
            r1 = 293(0x125, float:4.1E-43)
            r2 = -60326(0xffffffffffff145a, float:NaN)
            r0[r1] = r2
            r1 = 294(0x126, float:4.12E-43)
            r2 = -59870(0xffffffffffff1622, float:NaN)
            r0[r1] = r2
            r1 = 295(0x127, float:4.13E-43)
            r2 = -59396(0xffffffffffff17fc, float:NaN)
            r0[r1] = r2
            r1 = 296(0x128, float:4.15E-43)
            r2 = -58903(0xffffffffffff19e9, float:NaN)
            r0[r1] = r2
            r1 = 297(0x129, float:4.16E-43)
            r2 = -58393(0xffffffffffff1be7, float:NaN)
            r0[r1] = r2
            r1 = 298(0x12a, float:4.18E-43)
            r2 = -57865(0xffffffffffff1df7, float:NaN)
            r0[r1] = r2
            r1 = 299(0x12b, float:4.19E-43)
            r2 = -57319(0xffffffffffff2019, float:NaN)
            r0[r1] = r2
            r1 = 300(0x12c, float:4.2E-43)
            r2 = -56756(0xffffffffffff224c, float:NaN)
            r0[r1] = r2
            r1 = 301(0x12d, float:4.22E-43)
            r2 = -56175(0xffffffffffff2491, float:NaN)
            r0[r1] = r2
            r1 = 302(0x12e, float:4.23E-43)
            r2 = -55578(0xffffffffffff26e6, float:NaN)
            r0[r1] = r2
            r1 = 303(0x12f, float:4.25E-43)
            r2 = -54963(0xffffffffffff294d, float:NaN)
            r0[r1] = r2
            r1 = 304(0x130, float:4.26E-43)
            r2 = -54332(0xffffffffffff2bc4, float:NaN)
            r0[r1] = r2
            r1 = 305(0x131, float:4.27E-43)
            r2 = -53684(0xffffffffffff2e4c, float:NaN)
            r0[r1] = r2
            r1 = 306(0x132, float:4.29E-43)
            r2 = -53020(0xffffffffffff30e4, float:NaN)
            r0[r1] = r2
            r1 = 307(0x133, float:4.3E-43)
            r2 = -52339(0xffffffffffff338d, float:NaN)
            r0[r1] = r2
            r1 = 308(0x134, float:4.32E-43)
            r2 = -51643(0xffffffffffff3645, float:NaN)
            r0[r1] = r2
            r1 = 309(0x135, float:4.33E-43)
            r2 = -50931(0xffffffffffff390d, float:NaN)
            r0[r1] = r2
            r1 = 310(0x136, float:4.34E-43)
            r2 = -50203(0xffffffffffff3be5, float:NaN)
            r0[r1] = r2
            r1 = 311(0x137, float:4.36E-43)
            r2 = -49461(0xffffffffffff3ecb, float:NaN)
            r0[r1] = r2
            r1 = 312(0x138, float:4.37E-43)
            r2 = -48703(0xffffffffffff41c1, float:NaN)
            r0[r1] = r2
            r1 = 313(0x139, float:4.39E-43)
            r2 = -47930(0xffffffffffff44c6, float:NaN)
            r0[r1] = r2
            r1 = 314(0x13a, float:4.4E-43)
            r2 = -47143(0xffffffffffff47d9, float:NaN)
            r0[r1] = r2
            r1 = 315(0x13b, float:4.41E-43)
            r2 = -46341(0xffffffffffff4afb, float:NaN)
            r0[r1] = r2
            r1 = 316(0x13c, float:4.43E-43)
            r2 = -45525(0xffffffffffff4e2b, float:NaN)
            r0[r1] = r2
            r1 = 317(0x13d, float:4.44E-43)
            r2 = -44695(0xffffffffffff5169, float:NaN)
            r0[r1] = r2
            r1 = 318(0x13e, float:4.46E-43)
            r2 = -43852(0xffffffffffff54b4, float:NaN)
            r0[r1] = r2
            r1 = 319(0x13f, float:4.47E-43)
            r2 = -42995(0xffffffffffff580d, float:NaN)
            r0[r1] = r2
            r1 = 320(0x140, float:4.48E-43)
            r2 = -42126(0xffffffffffff5b72, float:NaN)
            r0[r1] = r2
            r1 = 321(0x141, float:4.5E-43)
            r2 = -41243(0xffffffffffff5ee5, float:NaN)
            r0[r1] = r2
            r1 = 322(0x142, float:4.51E-43)
            r2 = -40348(0xffffffffffff6264, float:NaN)
            r0[r1] = r2
            r1 = 323(0x143, float:4.53E-43)
            r2 = -39441(0xffffffffffff65ef, float:NaN)
            r0[r1] = r2
            r1 = 324(0x144, float:4.54E-43)
            r2 = -38521(0xffffffffffff6987, float:NaN)
            r0[r1] = r2
            r1 = 325(0x145, float:4.55E-43)
            r2 = -37590(0xffffffffffff6d2a, float:NaN)
            r0[r1] = r2
            r1 = 326(0x146, float:4.57E-43)
            r2 = -36647(0xffffffffffff70d9, float:NaN)
            r0[r1] = r2
            r1 = 327(0x147, float:4.58E-43)
            r2 = -35693(0xffffffffffff7493, float:NaN)
            r0[r1] = r2
            r1 = 328(0x148, float:4.6E-43)
            r2 = -34729(0xffffffffffff7857, float:NaN)
            r0[r1] = r2
            r1 = 329(0x149, float:4.61E-43)
            r2 = -33754(0xffffffffffff7c26, float:NaN)
            r0[r1] = r2
            r1 = 330(0x14a, float:4.62E-43)
            r2 = -32768(0xffffffffffff8000, float:NaN)
            r0[r1] = r2
            r1 = 331(0x14b, float:4.64E-43)
            r2 = -31772(0xffffffffffff83e4, float:NaN)
            r0[r1] = r2
            r1 = 332(0x14c, float:4.65E-43)
            r2 = -30767(0xffffffffffff87d1, float:NaN)
            r0[r1] = r2
            r1 = 333(0x14d, float:4.67E-43)
            r2 = -29753(0xffffffffffff8bc7, float:NaN)
            r0[r1] = r2
            r1 = 334(0x14e, float:4.68E-43)
            r2 = -28729(0xffffffffffff8fc7, float:NaN)
            r0[r1] = r2
            r1 = 335(0x14f, float:4.7E-43)
            r2 = -27697(0xffffffffffff93cf, float:NaN)
            r0[r1] = r2
            r1 = 336(0x150, float:4.71E-43)
            r2 = -26656(0xffffffffffff97e0, float:NaN)
            r0[r1] = r2
            r1 = 337(0x151, float:4.72E-43)
            r2 = -25607(0xffffffffffff9bf9, float:NaN)
            r0[r1] = r2
            r1 = 338(0x152, float:4.74E-43)
            r2 = -24550(0xffffffffffffa01a, float:NaN)
            r0[r1] = r2
            r1 = 339(0x153, float:4.75E-43)
            r2 = -23486(0xffffffffffffa442, float:NaN)
            r0[r1] = r2
            r1 = 340(0x154, float:4.76E-43)
            r2 = -22415(0xffffffffffffa871, float:NaN)
            r0[r1] = r2
            r1 = 341(0x155, float:4.78E-43)
            r2 = -21336(0xffffffffffffaca8, float:NaN)
            r0[r1] = r2
            r1 = 342(0x156, float:4.79E-43)
            r2 = -20252(0xffffffffffffb0e4, float:NaN)
            r0[r1] = r2
            r1 = 343(0x157, float:4.8E-43)
            r2 = -19161(0xffffffffffffb527, float:NaN)
            r0[r1] = r2
            r1 = 344(0x158, float:4.82E-43)
            r2 = -18064(0xffffffffffffb970, float:NaN)
            r0[r1] = r2
            r1 = 345(0x159, float:4.83E-43)
            r2 = -16962(0xffffffffffffbdbe, float:NaN)
            r0[r1] = r2
            r1 = 346(0x15a, float:4.85E-43)
            r2 = -15855(0xffffffffffffc211, float:NaN)
            r0[r1] = r2
            r1 = 347(0x15b, float:4.86E-43)
            r2 = -14742(0xffffffffffffc66a, float:NaN)
            r0[r1] = r2
            r1 = 348(0x15c, float:4.88E-43)
            r2 = -13626(0xffffffffffffcac6, float:NaN)
            r0[r1] = r2
            r1 = 349(0x15d, float:4.89E-43)
            r2 = -12505(0xffffffffffffcf27, float:NaN)
            r0[r1] = r2
            r1 = 350(0x15e, float:4.9E-43)
            r2 = -11380(0xffffffffffffd38c, float:NaN)
            r0[r1] = r2
            r1 = 351(0x15f, float:4.92E-43)
            r2 = -10252(0xffffffffffffd7f4, float:NaN)
            r0[r1] = r2
            r1 = 352(0x160, float:4.93E-43)
            r2 = -9121(0xffffffffffffdc5f, float:NaN)
            r0[r1] = r2
            r1 = 353(0x161, float:4.95E-43)
            r2 = -7987(0xffffffffffffe0cd, float:NaN)
            r0[r1] = r2
            r1 = 354(0x162, float:4.96E-43)
            r2 = -6850(0xffffffffffffe53e, float:NaN)
            r0[r1] = r2
            r1 = 355(0x163, float:4.97E-43)
            r2 = -5712(0xffffffffffffe9b0, float:NaN)
            r0[r1] = r2
            r1 = 356(0x164, float:4.99E-43)
            r2 = -4572(0xffffffffffffee24, float:NaN)
            r0[r1] = r2
            r1 = 357(0x165, float:5.0E-43)
            r2 = -3430(0xfffffffffffff29a, float:NaN)
            r0[r1] = r2
            r1 = 358(0x166, float:5.02E-43)
            r2 = -2287(0xfffffffffffff711, float:NaN)
            r0[r1] = r2
            r1 = 359(0x167, float:5.03E-43)
            r2 = -1144(0xfffffffffffffb88, float:NaN)
            r0[r1] = r2
            ua.netlizard.egypt.math.sin = r0
            r0 = 13
            short[][] r0 = new short[r0][]
            r1 = 0
            short[] r2 = new short[r5]
            r3 = 100
            r2[r8] = r3
            r3 = 203(0xcb, float:2.84E-43)
            r2[r9] = r3
            r3 = 5
            r4 = 306(0x132, float:4.29E-43)
            r2[r3] = r4
            r3 = 7
            r4 = 409(0x199, float:5.73E-43)
            r2[r3] = r4
            r3 = 9
            r4 = 512(0x200, float:7.175E-43)
            r2[r3] = r4
            r3 = 11
            r4 = 615(0x267, float:8.62E-43)
            r2[r3] = r4
            r0[r1] = r2
            short[] r1 = new short[r5]
            r1 = {5, 100, 12, 203, 33, 306, 65, 409, 69, 512, 75, 615} // fill-array
            r0[r8] = r1
            r1 = 2
            short[] r2 = new short[r5]
            r2 = {5, 100, 17, 203, 94, 306, 180, 409, 267, 512, 336, 615} // fill-array
            r0[r1] = r2
            short[] r1 = new short[r5]
            r1 = {5, 100, 17, 214, 106, 306, 233, 336, 370, 325, 487, 308} // fill-array
            r0[r9] = r1
            short[] r1 = new short[r5]
            r1 = {1, 100, 1, 214, 28, 338, 138, 404, 264, 407, 379, 391} // fill-array
            r0[r7] = r1
            r1 = 5
            short[] r2 = new short[r5]
            r2 = {-64, 87, -157, 185, -234, 294, -226, 425, -176, 540, -103, 641} // fill-array
            r0[r1] = r2
            r1 = 6
            short[] r2 = new short[r5]
            r2 = {-52, 55, -189, 70, -327, 88, -461, 133, -586, 162, -704, 214} // fill-array
            r0[r1] = r2
            r1 = 7
            short[] r2 = new short[r5]
            r2 = {-52, 55, -121, 177, -229, 271, -371, 288, -504, 271, -620, 200} // fill-array
            r0[r1] = r2
            r1 = 8
            short[] r2 = new short[r5]
            r2 = {-6, 55, -1, 177, 6, 308, -17, 440, -116, 530, -250, 549} // fill-array
            r0[r1] = r2
            r1 = 9
            short[] r2 = new short[r5]
            r3 = 100
            r2[r8] = r3
            r3 = 203(0xcb, float:2.84E-43)
            r2[r9] = r3
            r3 = 5
            r4 = 306(0x132, float:4.29E-43)
            r2[r3] = r4
            r3 = 7
            r4 = 409(0x199, float:5.73E-43)
            r2[r3] = r4
            r3 = 9
            r4 = 512(0x200, float:7.175E-43)
            r2[r3] = r4
            r3 = 11
            r4 = 615(0x267, float:8.62E-43)
            r2[r3] = r4
            r0[r1] = r2
            r1 = 10
            short[] r2 = new short[r5]
            r2[r8] = r6
            r3 = 154(0x9a, float:2.16E-43)
            r2[r9] = r3
            r3 = 5
            r4 = 239(0xef, float:3.35E-43)
            r2[r3] = r4
            r3 = 7
            r4 = 347(0x15b, float:4.86E-43)
            r2[r3] = r4
            r3 = 9
            r4 = 444(0x1bc, float:6.22E-43)
            r2[r3] = r4
            r3 = 11
            r4 = 539(0x21b, float:7.55E-43)
            r2[r3] = r4
            r0[r1] = r2
            r1 = 11
            short[] r2 = new short[r5]
            r3 = 0
            r4 = 40
            r2[r3] = r4
            r2[r8] = r6
            r2[r9] = r6
            r3 = 5
            r2[r3] = r6
            r3 = 7
            r2[r3] = r6
            r3 = 9
            r2[r3] = r6
            r3 = 11
            r2[r3] = r6
            r0[r1] = r2
            short[] r1 = new short[r5]
            r2 = 0
            r3 = 40
            r1[r2] = r3
            r1[r8] = r6
            r1[r9] = r6
            r2 = 5
            r1[r2] = r6
            r2 = 7
            r1[r2] = r6
            r2 = 9
            r1[r2] = r6
            r2 = 11
            r1[r2] = r6
            r0[r5] = r1
            ua.netlizard.egypt.math.fiz_boss = r0
            r0 = 8
            int[] r0 = new int[r0]
            r0 = {125, 60, 125, 70, 80, 125, 100, 150} // fill-array
            ua.netlizard.egypt.math.small_fiz_ = r0
            r0 = 8
            int[] r0 = new int[r0]
            r0 = {8192000, 3932160, 8192000, 4587520, 5242880, 8192000, 6553600, 9830400} // fill-array
            ua.netlizard.egypt.math.small_fiz_value = r0
            r0 = 8
            int[] r0 = new int[r0]
            r0 = {15625, 3600, 13225, 4900, 6400, 15625, 10000, 22500} // fill-array
            ua.netlizard.egypt.math.min_value_uni2 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.math.<clinit>():void");
    }

    public static final int Rnd(int a) {
        return Math.abs(myRandom.nextInt() % a);
    }

    public static final int to_short(int b1, int b2) {
        if (b1 < 0) {
            b1 += 256;
        }
        if (b2 < 0) {
            b2 += 256;
        }
        return (b2 * 256) + b1;
    }

    public static final void correct_vec_z(int[] v, int razbros) {
        int acuracy = razbros >> 1;
        int x = v[0];
        int y = v[1];
        if (acuracy != 0) {
            int acuracy2 = acuracy - Rnd(acuracy << 1);
            if (acuracy2 < 0) {
                acuracy2 += 360;
            }
            v[0] = (int) (((((long) x) * ((long) cos[acuracy2])) - (((long) y) * ((long) sin[acuracy2]))) >> 16);
            v[1] = (int) (((((long) x) * ((long) sin[acuracy2])) + (((long) y) * ((long) cos[acuracy2]))) >> 16);
        }
    }

    private static final void target_vec(unit U, int x1, int y1, int z1, int acuracy) {
        int[] cos_ = cos;
        int[] sin_ = sin;
        int x = (x1 - U.pos_x) >> 16;
        int y = (y1 - U.pos_y) >> 16;
        int z = (z1 - U.pos_z) >> 16;
        int leght = (x * x) + (y * y) + (z * z);
        int Pa0 = 0;
        int wfm = stp_one;
        while (true) {
            wfm >>= 1;
            if (wfm <= 0) {
                break;
            } else if ((Pa0 + wfm) * (Pa0 + wfm) <= leght) {
                Pa0 += wfm;
            }
        }
        if (Pa0 == 0) {
            Pa0 = 1;
        }
        U.v_target[0] = (stp_one * x) / Pa0;
        U.v_target[1] = (stp_one * y) / Pa0;
        U.v_target[2] = (stp_one * z) / Pa0;
        int leght2 = (x * x) + (y * y);
        int wfm2 = stp_one;
        int Pa02 = 0;
        while (true) {
            wfm2 >>= 1;
            if (wfm2 <= 0) {
                break;
            } else if ((Pa02 + wfm2) * (Pa02 + wfm2) <= leght2) {
                Pa02 += wfm2;
            }
        }
        if (Pa02 == 0) {
            Pa02 = 1;
        }
        U.rr[0] = (stp_one * x) / Pa02;
        U.rr[1] = (stp_one * y) / Pa02;
        if (acuracy != 0) {
            int acuracy2 = acuracy - Rnd(acuracy << 1);
            if (acuracy2 < 0) {
                acuracy2 += 360;
            }
            U.v_target[0] = (int) (((((long) U.v_target[0]) * ((long) cos_[acuracy2])) - (((long) U.v_target[1]) * ((long) sin_[acuracy2]))) >> 16);
            U.v_target[1] = (int) (((((long) U.v_target[0]) * ((long) sin_[acuracy2])) + (((long) U.v_target[1]) * ((long) cos_[acuracy2]))) >> 16);
        }
    }

    public static final void vec_for_me(int[] v, int rot_x, int rot_z) {
        int[] sin_ = sin;
        int[] cos_ = cos;
        v[0] = (int) ((((long) (-sin_[rot_z])) * ((long) (-sin_[rot_x]))) >> 16);
        v[1] = (int) ((((long) (-cos_[rot_z])) * ((long) (-sin_[rot_x]))) >> 16);
        v[2] = -cos_[rot_x];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0031, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final int TEST_HIT_PORTAL(int r29, ua.netlizard.egypt.point_ r30, ua.netlizard.egypt.point_ r31, int r32) {
        /*
            ua.netlizard.egypt.portal[] r2 = ua.netlizard.egypt.m3d.Portals
            ua.netlizard.egypt.room[] r25 = ua.netlizard.egypt.m3d.Rooms
            r25 = r25[r29]
            r0 = r25
            byte[] r0 = r0.portal
            r16 = r0
            ua.netlizard.egypt.room[] r25 = ua.netlizard.egypt.m3d.Rooms
            r25 = r25[r29]
            r0 = r25
            int r0 = r0.portals
            r17 = r0
            int[] r3 = ua.netlizard.egypt.m3d.distance_H
            r19 = 0
            r20 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            int r4 = r17 + -1
        L_0x0024:
            if (r4 >= 0) goto L_0x0029
            r25 = -1
        L_0x0028:
            return r25
        L_0x0029:
            byte r25 = r16[r4]
            r0 = r25
            r1 = r32
            if (r0 != r1) goto L_0x0034
        L_0x0031:
            int r4 = r4 + -1
            goto L_0x0024
        L_0x0034:
            byte r25 = r16[r4]
            r18 = r2[r25]
            r0 = r18
            ua.netlizard.egypt.point_[] r15 = r0.p
            r25 = 0
            r14 = r15[r25]
            r0 = r18
            byte r0 = r0.norm
            r25 = r0
            switch(r25) {
                case 1: goto L_0x00fc;
                case 2: goto L_0x019a;
                case 3: goto L_0x0238;
                default: goto L_0x0049;
            }
        L_0x0049:
            if (r20 < 0) goto L_0x004d
            if (r19 <= 0) goto L_0x0051
        L_0x004d:
            if (r19 < 0) goto L_0x0031
            if (r20 > 0) goto L_0x0031
        L_0x0051:
            int r25 = r20 - r19
            if (r25 <= 0) goto L_0x02d6
            int r25 = r20 - r19
            r25 = r3[r25]
            int r25 = r25 * r20
            r0 = r25
            long r0 = (long) r0
            r21 = r0
            r0 = r19
            int r0 = -r0
            r25 = r0
            int r26 = r20 - r19
            r26 = r3[r26]
            int r25 = r25 * r26
            r0 = r25
            long r0 = (long) r0
            r23 = r0
        L_0x0070:
            r0 = r30
            int r0 = r0.x
            r25 = r0
            r0 = r25
            long r0 = (long) r0
            r25 = r0
            long r25 = r25 * r21
            r0 = r31
            int r0 = r0.x
            r27 = r0
            r0 = r27
            long r0 = (long) r0
            r27 = r0
            long r27 = r27 * r23
            long r25 = r25 + r27
            r27 = 18
            long r25 = r25 >> r27
            r0 = r25
            int r11 = (int) r0
            r0 = r30
            int r0 = r0.y
            r25 = r0
            r0 = r25
            long r0 = (long) r0
            r25 = r0
            long r25 = r25 * r21
            r0 = r31
            int r0 = r0.y
            r27 = r0
            r0 = r27
            long r0 = (long) r0
            r27 = r0
            long r27 = r27 * r23
            long r25 = r25 + r27
            r27 = 18
            long r25 = r25 >> r27
            r0 = r25
            int r12 = (int) r0
            r0 = r30
            int r0 = r0.z
            r25 = r0
            r0 = r25
            long r0 = (long) r0
            r25 = r0
            long r25 = r25 * r21
            r0 = r31
            int r0 = r0.z
            r27 = r0
            r0 = r27
            long r0 = (long) r0
            r27 = r0
            long r27 = r27 * r23
            long r25 = r25 + r27
            r27 = 18
            long r25 = r25 >> r27
            r0 = r25
            int r13 = (int) r0
            r0 = r18
            byte r0 = r0.norm
            r25 = r0
            switch(r25) {
                case 1: goto L_0x00e4;
                case 2: goto L_0x02f3;
                case 3: goto L_0x030b;
                default: goto L_0x00e2;
            }
        L_0x00e2:
            goto L_0x0031
        L_0x00e4:
            if (r10 > r13) goto L_0x0031
            if (r9 > r12) goto L_0x0031
            if (r7 < r13) goto L_0x0031
            if (r6 < r12) goto L_0x0031
            r0 = r30
            r0.x = r11
            r0 = r30
            r0.y = r12
            r0 = r30
            r0.z = r13
            byte r25 = r16[r4]
            goto L_0x0028
        L_0x00fc:
            r0 = r30
            int r0 = r0.x
            r25 = r0
            int r0 = r14.x
            r26 = r0
            int r25 = r25 - r26
            int r19 = r25 >> 16
            r0 = r31
            int r0 = r0.x
            r25 = r0
            int r0 = r14.x
            r26 = r0
            int r25 = r25 - r26
            int r20 = r25 >> 16
            int r0 = r14.y
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.y
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.y
            r27 = r0
            int r26 = java.lang.Math.max(r26, r27)
            int r6 = java.lang.Math.max(r25, r26)
            int r0 = r14.y
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.y
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.y
            r27 = r0
            int r26 = java.lang.Math.min(r26, r27)
            int r9 = java.lang.Math.min(r25, r26)
            int r0 = r14.z
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.z
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.z
            r27 = r0
            int r26 = java.lang.Math.max(r26, r27)
            int r7 = java.lang.Math.max(r25, r26)
            int r0 = r14.z
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.z
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.z
            r27 = r0
            int r26 = java.lang.Math.min(r26, r27)
            int r10 = java.lang.Math.min(r25, r26)
            goto L_0x0049
        L_0x019a:
            r0 = r30
            int r0 = r0.y
            r25 = r0
            int r0 = r14.y
            r26 = r0
            int r25 = r25 - r26
            int r19 = r25 >> 16
            r0 = r31
            int r0 = r0.y
            r25 = r0
            int r0 = r14.y
            r26 = r0
            int r25 = r25 - r26
            int r20 = r25 >> 16
            int r0 = r14.x
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.x
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.x
            r27 = r0
            int r26 = java.lang.Math.max(r26, r27)
            int r5 = java.lang.Math.max(r25, r26)
            int r0 = r14.x
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.x
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.x
            r27 = r0
            int r26 = java.lang.Math.min(r26, r27)
            int r8 = java.lang.Math.min(r25, r26)
            int r0 = r14.z
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.z
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.z
            r27 = r0
            int r26 = java.lang.Math.max(r26, r27)
            int r7 = java.lang.Math.max(r25, r26)
            int r0 = r14.z
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.z
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.z
            r27 = r0
            int r26 = java.lang.Math.min(r26, r27)
            int r10 = java.lang.Math.min(r25, r26)
            goto L_0x0049
        L_0x0238:
            r0 = r30
            int r0 = r0.z
            r25 = r0
            int r0 = r14.z
            r26 = r0
            int r25 = r25 - r26
            int r19 = r25 >> 16
            r0 = r31
            int r0 = r0.z
            r25 = r0
            int r0 = r14.z
            r26 = r0
            int r25 = r25 - r26
            int r20 = r25 >> 16
            int r0 = r14.x
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.x
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.x
            r27 = r0
            int r26 = java.lang.Math.max(r26, r27)
            int r5 = java.lang.Math.max(r25, r26)
            int r0 = r14.x
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.x
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.x
            r27 = r0
            int r26 = java.lang.Math.min(r26, r27)
            int r8 = java.lang.Math.min(r25, r26)
            int r0 = r14.y
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.y
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.y
            r27 = r0
            int r26 = java.lang.Math.max(r26, r27)
            int r6 = java.lang.Math.max(r25, r26)
            int r0 = r14.y
            r25 = r0
            r26 = 1
            r26 = r15[r26]
            r0 = r26
            int r0 = r0.y
            r26 = r0
            r27 = 2
            r27 = r15[r27]
            r0 = r27
            int r0 = r0.y
            r27 = r0
            int r26 = java.lang.Math.min(r26, r27)
            int r9 = java.lang.Math.min(r25, r26)
            goto L_0x0049
        L_0x02d6:
            r0 = r20
            int r0 = -r0
            r25 = r0
            int r26 = r19 - r20
            r26 = r3[r26]
            int r25 = r25 * r26
            r0 = r25
            long r0 = (long) r0
            r21 = r0
            int r25 = r19 - r20
            r25 = r3[r25]
            int r25 = r25 * r19
            r0 = r25
            long r0 = (long) r0
            r23 = r0
            goto L_0x0070
        L_0x02f3:
            if (r10 > r13) goto L_0x0031
            if (r8 > r11) goto L_0x0031
            if (r7 < r13) goto L_0x0031
            if (r5 < r11) goto L_0x0031
            r0 = r30
            r0.x = r11
            r0 = r30
            r0.y = r12
            r0 = r30
            r0.z = r13
            byte r25 = r16[r4]
            goto L_0x0028
        L_0x030b:
            if (r8 > r11) goto L_0x0031
            if (r9 > r12) goto L_0x0031
            if (r5 < r11) goto L_0x0031
            if (r6 < r12) goto L_0x0031
            r0 = r30
            r0.x = r11
            r0 = r30
            r0.y = r12
            r0 = r30
            r0.z = r13
            byte r25 = r16[r4]
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.math.TEST_HIT_PORTAL(int, ua.netlizard.egypt.point_, ua.netlizard.egypt.point_, int):int");
    }

    public static final int HIT_PLANE(int room, point_ s, point_ e) {
        long t_1;
        long t_2;
        int t = -1;
        int[] distance = m3d.distance_H;
        int[] Place = m3d.Rooms[room].place;
        short[] poin = m3d.Rooms[room].Points;
        int[] norm = m3d.NORMALS;
        for (int i = Place.length - 2; i >= 0; i -= 2) {
            int np = Place[i];
            int nm = Place[i + 1];
            int t1 = (int) ((((((long) ((s.x >> 16) - poin[nm])) * ((long) norm[np])) + (((long) ((s.y >> 16) - poin[nm + 1])) * ((long) norm[np + 1]))) + (((long) ((s.z >> 16) - poin[nm + 2])) * ((long) norm[np + 2]))) >> 16);
            int t2 = (int) ((((((long) ((e.x >> 16) - poin[nm])) * ((long) norm[np])) + (((long) ((e.y >> 16) - poin[nm + 1])) * ((long) norm[np + 1]))) + (((long) ((e.z >> 16) - poin[nm + 2])) * ((long) norm[np + 2]))) >> 16);
            int dd = t2 - t1;
            if (dd > 0) {
                t_1 = (long) (distance[dd] * t2);
                t_2 = (long) ((-t1) * distance[dd]);
            } else {
                t_1 = (long) ((-t2) * distance[-dd]);
                t_2 = (long) (distance[-dd] * t1);
            }
            if (t2 <= 0) {
                e.x = (int) (((((long) s.x) * t_1) + (((long) e.x) * t_2)) >> 18);
                e.y = (int) (((((long) s.y) * t_1) + (((long) e.y) * t_2)) >> 18);
                e.z = (int) (((((long) s.z) * t_1) + (((long) e.z) * t_2)) >> 18);
                t = i;
            }
        }
        return t;
    }

    public static final boolean HIT_SFERA(point_ sfera, int radius_2, point_ a, point_ b) {
        if (((a.x - sfera.x) * (a.x - sfera.x)) + ((a.y - sfera.y) * (a.y - sfera.y)) + ((a.z - sfera.z) * (a.z - sfera.z)) <= radius_2) {
            b.x = a.x;
            b.y = a.y;
            b.z = a.z;
            return true;
        } else if (((b.x - sfera.x) * (b.x - sfera.x)) + ((b.y - sfera.y) * (b.y - sfera.y)) + ((b.z - sfera.z) * (b.z - sfera.z)) <= radius_2) {
            return true;
        } else {
            b.uz = ((((sfera.x * a.rx) + (sfera.y * a.ry)) + (sfera.z * a.rz)) >> 16) - ((((a.x * a.rx) + (a.y * a.ry)) + (a.z * a.rz)) >> 16);
            b.rx = a.x + ((b.uz * a.rx) >> 16);
            b.ry = a.y + ((b.uz * a.ry) >> 16);
            b.rz = a.z + ((b.uz * a.rz) >> 16);
            if (((b.rx - sfera.x) * (b.rx - sfera.x)) + ((b.ry - sfera.y) * (b.ry - sfera.y)) + ((b.rz - sfera.z) * (b.rz - sfera.z)) > radius_2 || b.uz < 0 || b.uz > sfera.z1) {
                return false;
            }
            b.x = b.rx;
            b.y = b.ry;
            b.z = b.rz;
            return true;
        }
    }

    public static final void projectVertex(point_ a) {
        a.sx = (short) ((int) ((((long) m3d.Width_2_3D) + ((((long) a.rx) * ((long) m3d.obzor_2)) / ((long) a.rz))) >> 16));
        a.sy = (short) ((int) ((((long) m3d.Height_2_3D) - ((((long) a.ry) * ((long) m3d.obzor_2)) / ((long) a.rz))) >> 16));
    }

    public static final void xRotateVertex(point_ a) {
        int[] pos_ = m3d.s;
        a.rx = (int) (((((long) (pos_[0] - a.x)) * ((long) cos[m3d.rot_z])) - (((long) (pos_[1] - a.y)) * ((long) sin[m3d.rot_z]))) >> 16);
        a.ry = (int) (((((long) (pos_[0] - a.x)) * ((long) sin[m3d.rot_z])) + (((long) (pos_[1] - a.y)) * ((long) cos[m3d.rot_z]))) >> 16);
        a.rz = (int) (((((long) (pos_[2] - a.z)) * ((long) cos[m3d.rot_x])) - (((long) a.ry) * ((long) sin[m3d.rot_x]))) >> 16);
        a.ry = (int) (((((long) (pos_[2] - a.z)) * ((long) sin[m3d.rot_x])) + (((long) a.ry) * ((long) cos[m3d.rot_x]))) >> 16);
    }

    static final void rot_OBJ(object ob) {
        short[] Points = ob.Points;
        short[] Poly = ob.Poly;
        int my_x = m3d.s[0];
        int my_y = m3d.s[1];
        int my_z = m3d.s[2];
        int rot_zz = m3d.rot_z;
        int rot_xx = m3d.rot_x;
        int cos_z = cos[ob.r_z];
        int sin_z = sin[ob.r_z];
        int cos_x = cos[ob.r_x];
        int sin_x = sin[ob.r_x];
        int i = m3d.OBJ[ob.type_object].point;
        int index = (i - 1) * 3;
        int[] text_ = m3d.max_point;
        while (true) {
            i--;
            if (i < 0) {
                break;
            }
            text_[index] = (int) ((((long) Points[index]) * ((long) cos_z)) - (((long) Points[index + 1]) * ((long) sin_z)));
            int help3 = (int) (((((long) Points[index]) * ((long) sin_z)) + (((long) Points[index + 1]) * ((long) cos_z))) >> 16);
            text_[index + 2] = (int) ((((long) Points[index + 2]) * ((long) cos_x)) - (((long) help3) * ((long) sin_x)));
            int help = (my_y - (ob.pos_y + ((int) ((((long) Points[index + 2]) * ((long) sin_x)) + (((long) help3) * ((long) cos_x)))))) >> 16;
            int help2 = (my_x - (text_[index] + ob.pos_x)) >> 16;
            text_[index] = (int) ((((long) help2) * ((long) cos[rot_zz])) - (((long) help) * ((long) sin[rot_zz])));
            int help32 = (int) (((((long) help2) * ((long) sin[rot_zz])) + (((long) help) * ((long) cos[rot_zz]))) >> 16);
            int help4 = (my_z - (text_[index + 2] + ob.pos_z)) >> 16;
            text_[index + 2] = (int) ((((long) help4) * ((long) cos[rot_xx])) - (((long) help32) * ((long) sin[rot_xx])));
            text_[index + 1] = (int) ((((long) help4) * ((long) sin[rot_xx])) + (((long) help32) * ((long) cos[rot_xx])));
            index -= 3;
        }
        int i2 = Poly.length >> 4;
        while (true) {
            i2--;
            if (i2 >= 0) {
                int index2 = i2 << 4;
                short s = Poly[index2 + 10];
                short s2 = Poly[index2 + 11];
                Poly[index2 + 13] = (short) ((int) (((((long) s) * ((long) cos_z)) - (((long) s2) * ((long) sin_z))) >> 16));
                int help33 = (short) ((int) (((((long) s) * ((long) sin_z)) + (((long) s2) * ((long) cos_z))) >> 16));
                Poly[index2 + 15] = (short) ((int) (((((long) Poly[index2 + 12]) * ((long) cos_x)) - (((long) help33) * ((long) sin_x))) >> 16));
                Poly[index2 + 14] = (short) ((int) (((((long) Poly[index2 + 12]) * ((long) sin_x)) + (((long) help33) * ((long) cos_x))) >> 16));
            } else {
                return;
            }
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [short[]] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static final void rot_UNI(ua.netlizard.egypt.unit r26) {
        /*
            r0 = r26
            short[][] r0 = r0.Points
            r19 = r0
            r0 = r26
            int r0 = r0.anim
            r20 = r0
            r2 = r19[r20]
            r0 = r26
            byte[][] r0 = r0.norms
            r19 = r0
            r0 = r26
            int r0 = r0.anim
            r20 = r0
            r14 = r19[r20]
            r0 = r26
            short[] r3 = r0.Poly
            int[] r19 = ua.netlizard.egypt.m3d.s
            r20 = 0
            r11 = r19[r20]
            int[] r19 = ua.netlizard.egypt.m3d.s
            r20 = 1
            r12 = r19[r20]
            int[] r19 = ua.netlizard.egypt.m3d.s
            r20 = 2
            r13 = r19[r20]
            int r16 = ua.netlizard.egypt.m3d.rot_z
            int r15 = ua.netlizard.egypt.m3d.rot_x
            r6 = 0
            r7 = 0
            r0 = r26
            int r0 = r0.pos_z
            r19 = r0
            int[] r20 = ua.netlizard.egypt.math.small_fiz_value
            r0 = r26
            byte r0 = r0.type_unit
            r21 = r0
            r20 = r20[r21]
            int r4 = r19 - r20
            r0 = r26
            int[] r0 = r0.rr
            r19 = r0
            r20 = 0
            r19 = r19[r20]
            r0 = r19
            int r5 = -r0
            r0 = r26
            int[] r0 = r0.rr
            r19 = r0
            r20 = 1
            r19 = r19[r20]
            r0 = r19
            int r0 = -r0
            r17 = r0
            ua.netlizard.egypt.main_unit[] r19 = ua.netlizard.egypt.m3d.UNI
            r0 = r26
            byte r0 = r0.type_unit
            r20 = r0
            r19 = r19[r20]
            r0 = r19
            int r9 = r0.point
            int r19 = r9 + -1
            int r10 = r19 * 3
            int[] r18 = ua.netlizard.egypt.m3d.max_point
        L_0x007a:
            int r9 = r9 + -1
            if (r9 >= 0) goto L_0x008c
            int r0 = r3.length
            r19 = r0
            int r9 = r19 >> 4
            int r19 = r9 * 3
            int r6 = r19 + -1
        L_0x0087:
            int r9 = r9 + -1
            if (r9 >= 0) goto L_0x01a2
            return
        L_0x008c:
            short r19 = r2[r10]
            r0 = r19
            long r0 = (long) r0
            r19 = r0
            long r0 = (long) r5
            r21 = r0
            long r19 = r19 * r21
            int r21 = r10 + 1
            short r21 = r2[r21]
            r0 = r21
            long r0 = (long) r0
            r21 = r0
            r0 = r17
            long r0 = (long) r0
            r23 = r0
            long r21 = r21 * r23
            long r19 = r19 - r21
            r0 = r19
            int r0 = (int) r0
            r19 = r0
            r18[r10] = r19
            short r19 = r2[r10]
            r0 = r19
            long r0 = (long) r0
            r19 = r0
            r0 = r17
            long r0 = (long) r0
            r21 = r0
            long r19 = r19 * r21
            int r21 = r10 + 1
            short r21 = r2[r21]
            r0 = r21
            long r0 = (long) r0
            r21 = r0
            long r0 = (long) r5
            r23 = r0
            long r21 = r21 * r23
            long r19 = r19 + r21
            r21 = 16
            long r19 = r19 >> r21
            r0 = r19
            int r8 = (int) r0
            int r19 = r10 + 2
            int r20 = r10 + 2
            short r20 = r2[r20]
            int r20 = r20 << 16
            r18[r19] = r20
            int r8 = r8 << 16
            r0 = r26
            int r0 = r0.pos_y
            r19 = r0
            int r19 = r19 + r8
            int r19 = r12 - r19
            int r6 = r19 >> 16
            r19 = r18[r10]
            r0 = r26
            int r0 = r0.pos_x
            r20 = r0
            int r19 = r19 + r20
            int r19 = r11 - r19
            int r7 = r19 >> 16
            long r0 = (long) r7
            r19 = r0
            int[] r21 = ua.netlizard.egypt.math.cos
            r21 = r21[r16]
            r0 = r21
            long r0 = (long) r0
            r21 = r0
            long r19 = r19 * r21
            long r0 = (long) r6
            r21 = r0
            int[] r23 = ua.netlizard.egypt.math.sin
            r23 = r23[r16]
            r0 = r23
            long r0 = (long) r0
            r23 = r0
            long r21 = r21 * r23
            long r19 = r19 - r21
            r0 = r19
            int r0 = (int) r0
            r19 = r0
            r18[r10] = r19
            long r0 = (long) r7
            r19 = r0
            int[] r21 = ua.netlizard.egypt.math.sin
            r21 = r21[r16]
            r0 = r21
            long r0 = (long) r0
            r21 = r0
            long r19 = r19 * r21
            long r0 = (long) r6
            r21 = r0
            int[] r23 = ua.netlizard.egypt.math.cos
            r23 = r23[r16]
            r0 = r23
            long r0 = (long) r0
            r23 = r0
            long r21 = r21 * r23
            long r19 = r19 + r21
            r21 = 16
            long r19 = r19 >> r21
            r0 = r19
            int r8 = (int) r0
            int r19 = r10 + 2
            r19 = r18[r19]
            int r19 = r19 + r4
            int r19 = r13 - r19
            int r6 = r19 >> 16
            int r19 = r10 + 2
            long r0 = (long) r6
            r20 = r0
            int[] r22 = ua.netlizard.egypt.math.cos
            r22 = r22[r15]
            r0 = r22
            long r0 = (long) r0
            r22 = r0
            long r20 = r20 * r22
            long r0 = (long) r8
            r22 = r0
            int[] r24 = ua.netlizard.egypt.math.sin
            r24 = r24[r15]
            r0 = r24
            long r0 = (long) r0
            r24 = r0
            long r22 = r22 * r24
            long r20 = r20 - r22
            r0 = r20
            int r0 = (int) r0
            r20 = r0
            r18[r19] = r20
            int r19 = r10 + 1
            long r0 = (long) r6
            r20 = r0
            int[] r22 = ua.netlizard.egypt.math.sin
            r22 = r22[r15]
            r0 = r22
            long r0 = (long) r0
            r22 = r0
            long r20 = r20 * r22
            long r0 = (long) r8
            r22 = r0
            int[] r24 = ua.netlizard.egypt.math.cos
            r24 = r24[r15]
            r0 = r24
            long r0 = (long) r0
            r24 = r0
            long r22 = r22 * r24
            long r20 = r20 + r22
            r0 = r20
            int r0 = (int) r0
            r20 = r0
            r18[r19] = r20
            int r10 = r10 + -3
            goto L_0x007a
        L_0x01a2:
            int r10 = r9 << 4
            int r19 = r6 + -2
            byte r8 = r14[r19]
            int r19 = r6 + -1
            byte r7 = r14[r19]
            int r19 = r10 + 13
            long r0 = (long) r8
            r20 = r0
            long r0 = (long) r5
            r22 = r0
            long r20 = r20 * r22
            long r0 = (long) r7
            r22 = r0
            r0 = r17
            long r0 = (long) r0
            r24 = r0
            long r22 = r22 * r24
            long r20 = r20 - r22
            r22 = 16
            long r20 = r20 >> r22
            r0 = r20
            int r0 = (int) r0
            r20 = r0
            r0 = r20
            short r0 = (short) r0
            r20 = r0
            r3[r19] = r20
            int r19 = r10 + 14
            long r0 = (long) r8
            r20 = r0
            r0 = r17
            long r0 = (long) r0
            r22 = r0
            long r20 = r20 * r22
            long r0 = (long) r7
            r22 = r0
            long r0 = (long) r5
            r24 = r0
            long r22 = r22 * r24
            long r20 = r20 + r22
            r22 = 16
            long r20 = r20 >> r22
            r0 = r20
            int r0 = (int) r0
            r20 = r0
            r0 = r20
            short r0 = (short) r0
            r20 = r0
            r3[r19] = r20
            int r19 = r10 + 15
            byte r20 = r14[r6]
            r3[r19] = r20
            int r6 = r6 + -3
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.math.rot_UNI(ua.netlizard.egypt.unit):void");
    }

    public static final void change_room_camera() {
        room Room = m3d.Rooms[m3d.room_now];
        if (Room.max_x < m3d.s[0] || Room.min_x > m3d.s[0] || Room.max_y < m3d.s[1] || Room.min_y > m3d.s[1] || Room.max_z < m3d.s[2] || Room.min_z > m3d.s[2]) {
            int i = m3d.rooms - 1;
            while (i >= 0) {
                room R = m3d.Rooms[i];
                if (R.max_x <= m3d.s[0] || R.min_x >= m3d.s[0] || R.max_y <= m3d.s[1] || R.min_y >= m3d.s[1] || R.max_z <= m3d.s[2] || R.min_z >= m3d.s[2]) {
                    i--;
                } else {
                    m3d.room_now = (short) i;
                    return;
                }
            }
        }
    }

    public static final void HIT_BOSS(unit U, int i) {
        int a;
        int j = 0;
        while (j < m3d.units) {
            if (m3d.Units[i].type_unit != 7 || !m3d.Units[i].death) {
                j++;
            } else {
                return;
            }
        }
        boolean minus_life = false;
        int dist = small_fiz_value[7] >> 16;
        unit U1 = m3d.Units[i];
        if (U1.life != 0) {
            for (int o = 0; o < 6; o++) {
                if (U.anim <= 9) {
                    a = 0;
                } else if (U.anim >= 21) {
                    a = 12;
                } else {
                    a = U.anim - 9;
                }
                int x = ((int) ((((long) fiz_boss[a][o << 1]) * ((long) (-U.rr[0]))) >> 16)) - (U1.pos_x >> 16);
                int y = ((int) ((((long) fiz_boss[a][o << 1]) * ((long) (-U.rr[1]))) >> 16)) - (U1.pos_y >> 16);
                int z = fiz_boss[a][(o << 1) + 1] - (U1.pos_z >> 16);
                int leght = (x * x) + (y * y) + (z * z);
                if (min_value_uni2[7] > leght) {
                    int Pa0 = 0;
                    int wfm = stp_one;
                    while (true) {
                        wfm >>= 1;
                        if (wfm <= 0) {
                            break;
                        } else if ((Pa0 + wfm) * (Pa0 + wfm) <= leght) {
                            Pa0 += wfm;
                        }
                    }
                    U1.pos_x -= (m3d.coof_clip[Pa0] * x) * (dist - Pa0);
                    U1.pos_y -= (m3d.coof_clip[Pa0] * y) * (dist - Pa0);
                    U1.pos_z -= (m3d.coof_clip[Pa0] * z) * (dist - Pa0);
                    if (!minus_life) {
                        if (Rnd(2) == 0) {
                            U1.Speed[0] = (-U.rr[1]) * 100;
                            U1.Speed[1] = U.rr[0] * 100;
                        } else {
                            U1.Speed[0] = U.rr[1] * 100;
                            U1.Speed[1] = (-U.rr[0]) * 100;
                        }
                        U1.life = (short) (U1.life - (Pa0 >> 4));
                        if (U1.life < 0) {
                            U1.death = true;
                            U1.life = 0;
                        }
                        minus_life = true;
                    }
                }
            }
        }
    }

    public static final void HIT_ME(unit U, int i) {
        unit U1 = m3d.Units[i];
        int x = (U.pos_x - U1.pos_x) >> 16;
        int y = (U.pos_y - U1.pos_y) >> 16;
        int z = (U.pos_z - U1.pos_z) >> 16;
        int leght = (x * x) + (y * y) + (z * z);
        if (min_value_uni2[U1.type_unit] > leght) {
            int Pa0 = 0;
            int wfm = stp_one;
            while (true) {
                wfm >>= 1;
                if (wfm <= 0) {
                    U1.pos_x -= (m3d.coof_clip[Pa0] * x) * ((small_fiz_value[U1.type_unit] >> 16) - Pa0);
                    U1.pos_y -= (m3d.coof_clip[Pa0] * y) * ((small_fiz_value[U1.type_unit] >> 16) - Pa0);
                    U1.pos_z -= (m3d.coof_clip[Pa0] * z) * ((small_fiz_value[U1.type_unit] >> 16) - Pa0);
                    return;
                } else if ((Pa0 + wfm) * (Pa0 + wfm) <= leght) {
                    Pa0 += wfm;
                }
            }
        }
    }

    public static final void xRotateVertex(short[] b, int len) {
        int[] pos_ = m3d.s;
        int[] p = m3d.max_point;
        int x = pos_[0] >> 16;
        int y = pos_[1] >> 16;
        int z = pos_[2] >> 16;
        int cos_z = cos[m3d.rot_z];
        int sin_z = sin[m3d.rot_z];
        int cos_x = cos[m3d.rot_x];
        int sin_x = sin[m3d.rot_x];
        while (true) {
            len--;
            if (len >= 0) {
                int index = len * 3;
                int help = y - b[index + 1];
                int help2 = x - b[index];
                p[index] = (int) ((((long) help2) * ((long) cos_z)) - (((long) help) * ((long) sin_z)));
                p[index + 1] = (int) ((((long) help2) * ((long) sin_z)) + (((long) help) * ((long) cos_z)));
                int help3 = z - b[index + 2];
                int help22 = p[index + 1] >> 16;
                p[index + 2] = (int) ((((long) help3) * ((long) cos_x)) - (((long) help22) * ((long) sin_x)));
                p[index + 1] = (int) ((((long) help3) * ((long) sin_x)) + (((long) help22) * ((long) cos_x)));
            } else {
                return;
            }
        }
    }

    static final void sort_poly(short[] b, int[] p) {
        int[] sort_i = m3d.index_sort_poly;
        int[] sort_z = m3d.help_sort_poly;
        int leght = b.length >> 4;
        for (int i = 0; i < leght; i++) {
            int a = i << 4;
            sort_z[i] = p[b[a] + 5] + p[b[a + 1] + 5] + p[b[a + 2] + 5];
            sort_i[i] = i;
        }
        quicksort(sort_z, sort_i, 0, leght - 1);
    }

    static final void quicksort(int[] m, int[] m2, int a, int b) {
        if (a < b) {
            int c = partition(m, m2, a, b);
            quicksort(m, m2, a, c - 1);
            quicksort(m, m2, c + 1, b);
        }
    }

    private static final int partition(int[] m, int[] m2, int a, int b) {
        int i = a;
        for (int j = a; j <= b; j++) {
            if (m[j] <= m[b]) {
                int t = m[i];
                m[i] = m[j];
                m[j] = t;
                int t2 = m2[i];
                m2[i] = m2[j];
                m2[j] = t2;
                i++;
            }
        }
        return i - 1;
    }

    public static final void HIT_WORLD_(unit U, int room) {
        int[] norm = m3d.NORMALS;
        int[] Place = m3d.Rooms[room].place;
        short[] poin = m3d.Rooms[room].Points;
        int SSS = small_fiz_value[U.type_unit];
        for (int i = Place.length - 2; i >= 0; i -= 2) {
            int np = Place[i];
            int nm = Place[i + 1];
            long t1 = (((long) norm[np]) * ((long) ((U.pos_x >> 16) - poin[nm]))) + (((long) norm[np + 1]) * ((long) ((U.pos_y >> 16) - poin[nm + 1]))) + (((long) norm[np + 2]) * ((long) ((U.pos_z >> 16) - poin[nm + 2])));
            if (t1 < ((long) SSS)) {
                long t2 = ((long) SSS) - t1;
                U.pos_x = (int) (((long) U.pos_x) + ((((long) norm[np]) * t2) >> 16));
                U.pos_y = (int) (((long) U.pos_y) + ((((long) norm[np + 1]) * t2) >> 16));
                U.pos_z = (int) (((long) U.pos_z) + ((((long) norm[np + 2]) * t2) >> 16));
            }
        }
    }

    public static final boolean HIT_CUBE_CUBE(object ob, int j) {
        int len = m3d.Objects.length;
        object[] OBJ = m3d.Objects;
        for (int i = 0; i < len; i++) {
            if (OBJ[i].type_object == 23 && j != i) {
                object ob1 = OBJ[i];
                if (ob.pos_x - 3604480 < ob1.pos_x + 3604480 && ob.pos_x + 3604480 > ob1.pos_x - 3604480 && ob.pos_y - 3604480 < ob1.pos_y + 3604480 && ob.pos_y + 3604480 > ob1.pos_y - 3604480) {
                    return true;
                }
            }
        }
        return false;
    }

    public static final void HIT_CUBE_ROOM(object ob) {
        int[] norm = m3d.NORMALS;
        int[] Place = m3d.Rooms[ob.room_now].place;
        short[] poin = m3d.Rooms[ob.room_now].Points;
        for (int i = Place.length - 2; i >= 0; i -= 2) {
            int np = Place[i];
            int nm = Place[i + 1];
            long t1 = (((long) norm[np]) * ((long) ((ob.pos_x >> 16) - poin[nm]))) + (((long) norm[np + 1]) * ((long) ((ob.pos_y >> 16) - poin[nm + 1]))) + (((long) norm[np + 2]) * ((long) ((ob.pos_z >> 16) - poin[nm + 2])));
            if (t1 < ((long) 3604480)) {
                long t2 = ((long) 3604480) - t1;
                ob.pos_x = (int) (((long) ob.pos_x) + ((((long) norm[np]) * t2) >> 16));
                ob.pos_y = (int) (((long) ob.pos_y) + ((((long) norm[np + 1]) * t2) >> 16));
                ob.pos_z = (int) (((long) ob.pos_z) + ((((long) norm[np + 2]) * t2) >> 16));
            }
        }
    }

    public static final void change_room_obj(object ob) {
        room Room = m3d.Rooms[ob.room_now];
        if (Room.max_x < ob.pos_x || Room.min_x > ob.pos_x || Room.max_y < ob.pos_y || Room.min_y > ob.pos_y || Room.max_z < ob.pos_z || Room.min_z > ob.pos_z) {
            int i = m3d.rooms - 1;
            while (i >= 0) {
                room R = m3d.Rooms[i];
                if (R.max_x <= ob.pos_x || R.min_x >= ob.pos_x || R.max_y <= ob.pos_y || R.min_y >= ob.pos_y || R.max_z < ob.pos_z || R.min_z > ob.pos_z) {
                    i--;
                } else {
                    ob.room_now = (short) i;
                    return;
                }
            }
        }
    }

    public static final boolean change_room_shot(int[] shot) {
        room Room = m3d.Rooms[shot[7]];
        if (Room.max_x >= shot[0] && Room.min_x <= shot[0] && Room.max_y >= shot[1] && Room.min_y <= shot[1] && Room.max_z >= shot[2] && Room.min_z <= shot[2]) {
            return true;
        }
        int i = m3d.rooms - 1;
        while (i >= 0) {
            room R = m3d.Rooms[i];
            if (R.max_x <= shot[0] || R.min_x >= shot[0] || R.max_y <= shot[1] || R.min_y >= shot[1] || R.max_z < shot[2] || R.min_z > shot[2]) {
                i--;
            } else {
                shot[7] = (short) i;
                return true;
            }
        }
        return false;
    }

    public static final boolean change_room(unit U) {
        int[] vec = m3d.vec_speed;
        room Room = m3d.Rooms[U.room_now];
        if (Room.max_x < U.pos_x || Room.min_x > U.pos_x || Room.max_y < U.pos_y || Room.min_y > U.pos_y || Room.max_z < U.pos_z || Room.min_z > U.pos_z) {
            int i = m3d.rooms - 1;
            while (true) {
                if (i >= 0) {
                    room R = m3d.Rooms[i];
                    if (R.max_x > U.pos_x && R.min_x < U.pos_x && R.max_y > U.pos_y && R.min_y < U.pos_y && R.max_z > U.pos_z && R.min_z < U.pos_z) {
                        U.room_now = (short) i;
                        break;
                    }
                    i--;
                } else {
                    break;
                }
            }
        }
        if (vec[0] == 0 && vec[1] == 0 && vec[2] == 0) {
            return false;
        }
        vec[0] = U.pos_x + (vec[0] * small_fiz_[U.type_unit]);
        vec[1] = U.pos_y + (vec[1] * small_fiz_[U.type_unit]);
        vec[2] = U.pos_z + (vec[2] * small_fiz_[U.type_unit]);
        if (Room.max_x < vec[0] || Room.min_x > vec[0] || Room.max_y < vec[1] || Room.min_y > vec[1] || Room.max_z < vec[2] || Room.min_z > vec[2]) {
            int i2 = m3d.rooms - 1;
            while (i2 >= 0) {
                room R2 = m3d.Rooms[i2];
                if (R2.max_x <= vec[0] || R2.min_x >= vec[0] || R2.max_y <= vec[1] || R2.min_y >= vec[1] || R2.max_z <= vec[2] || R2.min_z >= vec[2]) {
                    i2--;
                } else {
                    HIT_WORLD_(U, i2);
                    return false;
                }
            }
            if (vec[0] > Room.max_x) {
                U.pos_x += Room.max_x - vec[0];
            }
            if (vec[0] < Room.min_x) {
                U.pos_x = (Room.min_x - vec[0]) + U.pos_x;
            }
            if (vec[1] > Room.max_y) {
                U.pos_y += Room.max_y - vec[1];
            }
            if (vec[1] < Room.min_y) {
                U.pos_y += Room.min_y - vec[1];
            }
            if (vec[2] > Room.max_z) {
                U.pos_z += Room.max_z - vec[2];
            }
            if (vec[2] < Room.min_z) {
                U.pos_z += Room.min_z - vec[2];
            }
        }
        return true;
    }

    private static final boolean HIT_WORLD(unit U, int leght_speed) {
        int[] Speed = U.Speed;
        int[] Speed_vec = m3d.vec_speed;
        boolean hit_test = false;
        int[] Place = m3d.Rooms[U.room_now].place;
        short[] poin = m3d.Rooms[U.room_now].Points;
        int[] norm = m3d.NORMALS;
        int SSS = small_fiz_value[U.type_unit];
        for (int i = Place.length - 2; i >= 0; i -= 2) {
            int p = Place[i];
            int nx = norm[p];
            int ny = norm[p + 1];
            int nz = norm[p + 2];
            int c = Place[i + 1];
            long t1 = (((long) nx) * ((long) ((U.pos_x >> 16) - poin[c]))) + (((long) ny) * ((long) ((U.pos_y >> 16) - poin[c + 1]))) + (((long) nz) * ((long) ((U.pos_z >> 16) - poin[c + 2])));
            if (t1 < ((long) SSS)) {
                if (nz > 0) {
                    hit_test = true;
                }
                long t2 = ((long) SSS) - t1;
                U.pos_x = (int) (((long) U.pos_x) + ((((long) nx) * t2) >> 16));
                U.pos_y = (int) (((long) U.pos_y) + ((((long) ny) * t2) >> 16));
                U.pos_z = (int) (((long) U.pos_z) + ((((long) nz) * t2) >> 16));
                long t12 = (((((long) nx) * ((long) Speed_vec[0])) + (((long) ny) * ((long) Speed_vec[1]))) + (((long) nz) * ((long) Speed_vec[2]))) >> 16;
                Speed_vec[0] = (int) (((long) Speed_vec[0]) - ((((long) nx) * t12) >> 16));
                Speed_vec[1] = (int) (((long) Speed_vec[1]) - ((((long) ny) * t12) >> 16));
                Speed_vec[2] = (int) (((long) Speed_vec[2]) - ((((long) nz) * t12) >> 16));
                Speed[0] = Speed_vec[0] * leght_speed;
                Speed[1] = Speed_vec[1] * leght_speed;
                Speed[2] = Speed_vec[2] * leght_speed;
            }
        }
        if (HIT_OBJECT(U)) {
            return true;
        }
        return hit_test;
    }

    private static final byte HIT_cube(int s_x, int s_y, int s_z, int e_x, int e_y, int e_z, int max_x, int max_y, int max_z, int min_x, int min_y, int min_z, unit U) {
        long t_1;
        long t_2;
        long t_12;
        long t_22;
        long t_13;
        long t_23;
        long t_14;
        long t_24;
        long t_15;
        long t_25;
        long t_16;
        long t_26;
        int[] distance = m3d.distance_H;
        byte HIT = 0;
        int t1 = (max_x - s_x) >> 16;
        int t2 = (max_x - e_x) >> 16;
        if (t2 >= 0 && t1 <= 0) {
            if (t2 - t1 > 0) {
                t_16 = (long) (distance[t2 - t1] * t2);
                t_26 = (long) ((-t1) * distance[t2 - t1]);
            } else {
                t_16 = (long) ((-t2) * distance[t1 - t2]);
                t_26 = (long) (distance[t1 - t2] * t1);
            }
            int my_x = (int) (((((long) s_x) * t_16) + (((long) e_x) * t_26)) >> 18);
            int my_y = (int) (((((long) s_y) * t_16) + (((long) e_y) * t_26)) >> 18);
            int my_z = (int) (((((long) s_z) * t_16) + (((long) e_z) * t_26)) >> 18);
            if (min_y <= my_y && min_z <= my_z && max_y >= my_y && max_z >= my_z) {
                e_x = my_x + stp_one;
                HIT = 1;
            }
        }
        int t12 = (max_y - s_y) >> 16;
        int t22 = (max_y - e_y) >> 16;
        if (t22 >= 0 && t12 <= 0) {
            if (t22 - t12 > 0) {
                t_15 = (long) (distance[t22 - t12] * t22);
                t_25 = (long) ((-t12) * distance[t22 - t12]);
            } else {
                t_15 = (long) ((-t22) * distance[t12 - t22]);
                t_25 = (long) (distance[t12 - t22] * t12);
            }
            int my_x2 = (int) (((((long) s_x) * t_15) + (((long) e_x) * t_25)) >> 18);
            int my_y2 = (int) (((((long) s_y) * t_15) + (((long) e_y) * t_25)) >> 18);
            int my_z2 = (int) (((((long) s_z) * t_15) + (((long) e_z) * t_25)) >> 18);
            if (min_x <= my_x2 && min_z <= my_z2 && max_x >= my_x2 && max_z >= my_z2) {
                e_y = my_y2 + stp_one;
                HIT = 2;
            }
        }
        int t13 = (s_x - min_x) >> 16;
        int t23 = (e_x - min_x) >> 16;
        if (t23 >= 0 && t13 <= 0) {
            if (t23 - t13 > 0) {
                t_14 = (long) (distance[t23 - t13] * t23);
                t_24 = (long) ((-t13) * distance[t23 - t13]);
            } else {
                t_14 = (long) ((-t23) * distance[t13 - t23]);
                t_24 = (long) (distance[t13 - t23] * t13);
            }
            int my_x3 = (int) (((((long) s_x) * t_14) + (((long) e_x) * t_24)) >> 18);
            int my_y3 = (int) (((((long) s_y) * t_14) + (((long) e_y) * t_24)) >> 18);
            int my_z3 = (int) (((((long) s_z) * t_14) + (((long) e_z) * t_24)) >> 18);
            if (min_y <= my_y3 && min_z <= my_z3 && max_y >= my_y3 && max_z >= my_z3) {
                e_x = my_x3 - stp_one;
                HIT = -1;
            }
        }
        int t14 = (s_y - min_y) >> 16;
        int t24 = (e_y - min_y) >> 16;
        if (t24 >= 0 && t14 <= 0) {
            if (t24 - t14 > 0) {
                t_13 = (long) (distance[t24 - t14] * t24);
                t_23 = (long) ((-t14) * distance[t24 - t14]);
            } else {
                t_13 = (long) ((-t24) * distance[t14 - t24]);
                t_23 = (long) (distance[t14 - t24] * t14);
            }
            int my_x4 = (int) (((((long) s_x) * t_13) + (((long) e_x) * t_23)) >> 18);
            int my_y4 = (int) (((((long) s_y) * t_13) + (((long) e_y) * t_23)) >> 18);
            int my_z4 = (int) (((((long) s_z) * t_13) + (((long) e_z) * t_23)) >> 18);
            if (min_x <= my_x4 && min_z <= my_z4 && max_x >= my_x4 && max_z >= my_z4) {
                e_y = my_y4 - stp_one;
                HIT = -2;
            }
        }
        int t15 = (s_z - min_z) >> 16;
        int t25 = (e_z - min_z) >> 16;
        if (t25 >= 0 && t15 <= 0) {
            if (t25 - t15 > 0) {
                t_12 = (long) (distance[t25 - t15] * t25);
                t_22 = (long) ((-t15) * distance[t25 - t15]);
            } else {
                t_12 = (long) ((-t25) * distance[t15 - t25]);
                t_22 = (long) (distance[t15 - t25] * t15);
            }
            int my_x5 = (int) (((((long) s_x) * t_12) + (((long) e_x) * t_22)) >> 18);
            int my_y5 = (int) (((((long) s_y) * t_12) + (((long) e_y) * t_22)) >> 18);
            int my_z5 = (int) (((((long) s_z) * t_12) + (((long) e_z) * t_22)) >> 18);
            if (min_x <= my_x5 && min_y <= my_y5 && max_x >= my_x5 && max_y >= my_y5) {
                e_z = my_z5 - stp_one;
                HIT = 3;
            }
        }
        int t16 = (max_z - s_z) >> 16;
        int t26 = (max_z - e_z) >> 16;
        if (t26 >= 0 && t16 <= 0) {
            if (t26 - t16 > 0) {
                t_1 = (long) (distance[t26 - t16] * t26);
                t_2 = (long) ((-t16) * distance[t26 - t16]);
            } else {
                t_1 = (long) ((-t26) * distance[t16 - t26]);
                t_2 = (long) (distance[t16 - t26] * t16);
            }
            int my_x6 = (int) (((((long) s_x) * t_1) + (((long) e_x) * t_2)) >> 18);
            int my_y6 = (int) (((((long) s_y) * t_1) + (((long) e_y) * t_2)) >> 18);
            int my_z6 = (int) (((((long) s_z) * t_1) + (((long) e_z) * t_2)) >> 18);
            if (min_x <= my_x6 && min_y <= my_y6 && max_x >= my_x6 && max_y >= my_y6) {
                e_z = my_z6 + stp_one;
                HIT = -3;
            }
        }
        U.pos_x = e_x;
        U.pos_y = e_y;
        U.pos_z = e_z;
        return HIT;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v0, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v1, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v3, resolved type: short} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static final boolean HIT_OBJECT(ua.netlizard.egypt.unit r22) {
        /*
            ua.netlizard.egypt.object[] r20 = ua.netlizard.egypt.m3d.Objects
            r0 = r22
            int[] r5 = r0.Speed
            r6 = 0
            r5 = r5[r6]
            if (r5 != 0) goto L_0x0020
            r0 = r22
            int[] r5 = r0.Speed
            r6 = 1
            r5 = r5[r6]
            if (r5 != 0) goto L_0x0020
            r0 = r22
            int[] r5 = r0.Speed
            r6 = 2
            r5 = r5[r6]
            if (r5 != 0) goto L_0x0020
            r16 = 0
        L_0x001f:
            return r16
        L_0x0020:
            r0 = r22
            int r5 = r0.pos_x
            r0 = r22
            int[] r6 = r0.Speed
            r7 = 0
            r6 = r6[r7]
            int r2 = r5 - r6
            r0 = r22
            int r5 = r0.pos_y
            r0 = r22
            int[] r6 = r0.Speed
            r7 = 1
            r6 = r6[r7]
            int r3 = r5 - r6
            r0 = r22
            int r5 = r0.pos_z
            r0 = r22
            int[] r6 = r0.Speed
            r7 = 2
            r6 = r6[r7]
            int r4 = r5 - r6
            r15 = -1
            r16 = 0
            short r19 = ua.netlizard.egypt.m3d.objects
            int[] r5 = ua.netlizard.egypt.math.small_fiz_value
            r0 = r22
            byte r6 = r0.type_unit
            r21 = r5[r6]
            r17 = 0
        L_0x0056:
            r0 = r17
            r1 = r19
            if (r0 >= r1) goto L_0x001f
            r18 = r20[r17]
            r0 = r18
            boolean r5 = r0.phiz
            if (r5 == 0) goto L_0x006a
            r0 = r18
            boolean r5 = r0.visible
            if (r5 != 0) goto L_0x006d
        L_0x006a:
            int r17 = r17 + 1
            goto L_0x0056
        L_0x006d:
            r0 = r22
            int r5 = r0.pos_x
            r0 = r22
            int r6 = r0.pos_y
            r0 = r22
            int r7 = r0.pos_z
            r0 = r18
            int r8 = r0.max_x
            r0 = r18
            int r9 = r0.pos_x
            int r8 = r8 + r9
            int r8 = r8 + r21
            r0 = r18
            int r9 = r0.max_y
            r0 = r18
            int r10 = r0.pos_y
            int r9 = r9 + r10
            int r9 = r9 + r21
            r0 = r18
            int r10 = r0.max_z
            r0 = r18
            int r11 = r0.pos_z
            int r10 = r10 + r11
            int r10 = r10 + r21
            r0 = r18
            int r11 = r0.min_x
            r0 = r18
            int r12 = r0.pos_x
            int r11 = r11 + r12
            int r11 = r11 - r21
            r0 = r18
            int r12 = r0.min_y
            r0 = r18
            int r13 = r0.pos_y
            int r12 = r12 + r13
            int r12 = r12 - r21
            r0 = r18
            int r13 = r0.min_z
            r0 = r18
            int r14 = r0.pos_z
            int r13 = r13 + r14
            int r13 = r13 - r21
            r14 = r22
            byte r15 = HIT_cube(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            if (r15 == 0) goto L_0x006a
            r16 = 1
            r0 = r18
            byte r5 = r0.type_object
            r6 = 23
            if (r5 != r6) goto L_0x0127
            r0 = r18
            int r5 = r0.pos_x
            int[] r6 = ua.netlizard.egypt.math.sin
            int r7 = ua.netlizard.egypt.m3d.rot_z
            r6 = r6[r7]
            int r6 = -r6
            int r6 = r6 * 20
            int r5 = r5 + r6
            r0 = r18
            r0.pos_x = r5
            r0 = r18
            int r5 = r0.pos_y
            int[] r6 = ua.netlizard.egypt.math.cos
            int r7 = ua.netlizard.egypt.m3d.rot_z
            r6 = r6[r7]
            int r6 = -r6
            int r6 = r6 * 20
            int r5 = r5 + r6
            r0 = r18
            r0.pos_y = r5
            change_room_obj(r18)
            HIT_CUBE_ROOM(r18)
            if (r15 >= 0) goto L_0x00fe
            int r5 = java.lang.Math.abs(r15)
            byte r15 = (byte) r5
        L_0x00fe:
            int r5 = r15 + -1
            if (r5 != 0) goto L_0x0112
            r0 = r22
            int r5 = r0.pos_y
            r0 = r22
            int[] r6 = r0.Speed
            r7 = 1
            r6 = r6[r7]
            int r5 = r5 - r6
            r0 = r22
            r0.pos_y = r5
        L_0x0112:
            int r5 = r15 + -1
            r6 = 1
            if (r5 != r6) goto L_0x0127
            r0 = r22
            int r5 = r0.pos_x
            r0 = r22
            int[] r6 = r0.Speed
            r7 = 0
            r6 = r6[r7]
            int r5 = r5 - r6
            r0 = r22
            r0.pos_x = r5
        L_0x0127:
            if (r15 >= 0) goto L_0x012e
            int r5 = java.lang.Math.abs(r15)
            byte r15 = (byte) r5
        L_0x012e:
            r0 = r22
            int[] r5 = r0.Speed
            int r6 = r15 + -1
            r7 = 0
            r5[r6] = r7
            r15 = 0
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.math.HIT_OBJECT(ua.netlizard.egypt.unit):boolean");
    }

    public static final void stop_all() {
        unit[] U = m3d.Units;
        for (int i = 0; i < m3d.units; i++) {
            unit u = U[i];
            u.F[0] = 0;
            u.F[1] = 0;
            u.F[2] = 0;
        }
    }

    static final void shot(int[] S, int[] F, int[] data) {
        if (m3d.shots < 49) {
            m3d.shots = (short) (m3d.shots + 1);
            int[][] shot = m3d.shot;
            for (int i = 49; i >= 0; i--) {
                if (shot[i][6] == 0) {
                    int[] m = shot[i];
                    int d = data[2] & 255;
                    m[8] = data[2];
                    m[3] = F[0] << d;
                    m[4] = F[1] << d;
                    m[5] = F[2] << d;
                    m[0] = S[0];
                    m[1] = S[1];
                    m[2] = S[2];
                    m[6] = data[3];
                    m[7] = data[1];
                    m[9] = data[0];
                    m[10] = data[4];
                    m[11] = data[5];
                    return;
                }
            }
            if (data[0] <= 0) {
                for (int i2 = 49; i2 >= 0; i2--) {
                    if (shot[i2][9] > 0) {
                        int[] m2 = shot[i2];
                        int d2 = data[2] & 255;
                        m2[8] = data[2];
                        m2[3] = F[0] << d2;
                        m2[4] = F[1] << d2;
                        m2[5] = F[2] << d2;
                        m2[0] = S[0];
                        m2[1] = S[1];
                        m2[2] = S[2];
                        m2[6] = data[3];
                        m2[7] = data[1];
                        m2[9] = data[0];
                        m2[10] = data[4];
                        m2[11] = data[5];
                        return;
                    }
                }
            }
        }
    }

    static final void shot_2(int[] S, int[] F, int[] data) {
        if (m3d.shots < 49) {
            m3d.shots = (short) (m3d.shots + 1);
            int[][] shot = m3d.shot;
            for (int i = 49; i >= 0; i--) {
                if (shot[i][6] == 0) {
                    int[] m = shot[i];
                    m[3] = F[0];
                    m[4] = F[1];
                    m[5] = F[2];
                    m[8] = data[2];
                    m[0] = S[0];
                    m[1] = S[1];
                    m[2] = S[2];
                    m[6] = data[3];
                    m[7] = data[1];
                    m[9] = data[0];
                    m[10] = data[4];
                    m[11] = data[5];
                    return;
                }
            }
            if (data[0] <= 0 && data[0] > -2000) {
                for (int i2 = 49; i2 >= 0; i2--) {
                    if (shot[i2][9] > 0) {
                        int[] m2 = shot[i2];
                        m2[3] = F[0];
                        m2[4] = F[1];
                        m2[5] = F[2];
                        m2[8] = data[2];
                        m2[0] = S[0];
                        m2[1] = S[1];
                        m2[2] = S[2];
                        m2[6] = data[3];
                        m2[7] = data[1];
                        m2[9] = data[0];
                        m2[10] = data[4];
                        m2[11] = data[5];
                        return;
                    }
                }
            }
        }
    }

    static final void phi(unit U) {
        if (U.life >= -90 && !U.take) {
            if (U.type_unit != 4 || !U.take) {
                int[] F1 = U.F;
                int[] Speed = U.Speed;
                int[] Speed_vec = m3d.vec_speed;
                int[] distance = m3d.coof_clip;
                int[] max_speeds = max_speeds1[U.type_unit];
                short run = 1;
                if (!U.Fly) {
                    Speed[0] = Speed[0] + (F1[0] * speed);
                    Speed[1] = Speed[1] + (F1[1] * speed);
                    Speed[2] = Speed[2] + (F1[2] * speed);
                } else {
                    Speed[2] = Speed[2] - G;
                    run = 2;
                }
                int sx = Speed[0] >> 16;
                int sy = Speed[1] >> 16;
                int sz = Speed[2] >> 16;
                int leght = (sx * sx) + (sy * sy) + (sz * sz);
                int leght_speed = 0;
                for (int wfm = 1073741824; wfm != 0; wfm >>= 2) {
                    int sx2 = leght_speed | wfm;
                    leght_speed >>= 1;
                    if (leght >= sx2) {
                        leght -= sx2;
                        leght_speed |= wfm;
                    }
                }
                Speed_vec[0] = (int) ((((long) Speed[0]) * ((long) distance[leght_speed])) >> 16);
                Speed_vec[1] = (int) ((((long) Speed[1]) * ((long) distance[leght_speed])) >> 16);
                Speed_vec[2] = (int) ((((long) Speed[2]) * ((long) distance[leght_speed])) >> 16);
                if (max_speeds[run] < leght_speed) {
                    leght_speed = max_speeds[run];
                    Speed[0] = Speed_vec[0] * leght_speed;
                    Speed[1] = Speed_vec[1] * leght_speed;
                    Speed[2] = Speed_vec[2] * leght_speed;
                } else if (!U.Fly && leght_speed > 0) {
                    int wfm2 = 16;
                    if (U.type_unit != 0) {
                        wfm2 = 20;
                    }
                    Speed[0] = Speed[0] + ((-Speed_vec[0]) * wfm2);
                    Speed[1] = Speed[1] + ((-Speed_vec[1]) * wfm2);
                    Speed[2] = Speed[2] + ((-Speed_vec[2]) * wfm2);
                }
                if (leght_speed < 15) {
                    Speed[0] = 0;
                    Speed[1] = 0;
                    Speed[2] = 0;
                }
                U.pos_x += Speed[0];
                U.pos_y += Speed[1];
                U.pos_z += Speed[2];
                if (HIT_WORLD(U, leght_speed)) {
                    U.Fly = false;
                } else {
                    U.Fly = true;
                }
                change_room(U);
            }
        }
    }
}
