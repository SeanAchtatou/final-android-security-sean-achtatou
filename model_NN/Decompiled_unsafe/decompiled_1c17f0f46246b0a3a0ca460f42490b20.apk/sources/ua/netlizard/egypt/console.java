package ua.netlizard.egypt;

public class console {
    static int Height = Game.Height;
    static int Width = Game.Width;
    static short[] index_mm;
    static int max_str;
    static String[] mm;
    static int now_str;
    static boolean visible = false;
    static int x = 0;
    static int y = 0;

    static void add(String s) {
        int m = max_str;
        if (now_str >= max_str) {
            for (int i = 0; i < m; i++) {
                index_mm[i] = (short) ((index_mm[i] + 1) % m);
            }
            mm[now_str % m] = new String(s);
        } else {
            mm[now_str] = new String(s);
        }
        now_str++;
    }

    static void draw_data(Graphics g, MyFont font) {
        if (visible) {
            g.setColor(0);
            g.fillRect(x, y, Width - x, Height - y);
            g.setClip(x, y, Width - x, Height - y);
            int h = font.getHeight();
            for (int i = 0; i < max_str; i++) {
                String j = mm[index_mm[i]];
                if (j != null) {
                    font.drawString(g, j, x, (h * i) + y, 20);
                }
            }
        }
    }

    static void start(int max) {
        end();
        max_str = max;
        now_str = 0;
        int m = max_str;
        mm = new String[max_str];
        index_mm = new short[max_str];
        for (int i = 0; i < m; i++) {
            mm[i] = new String();
            index_mm[i] = (short) i;
        }
    }

    static void end() {
        mm = null;
        index_mm = null;
    }

    static void clear() {
        now_str = 0;
        int m = max_str;
        for (int i = 0; i < m; i++) {
            mm[i] = new String();
            index_mm[i] = (short) i;
        }
    }
}
