package ua.netlizard.egypt;

import java.util.UUID;

public class DiscoveryAgent {
    static int CACHED = 0;
    static int GIAC = 1;
    static int LIAC = 2;
    static int NOT_DISCOVERABLE = 3;
    static int PREKNOWN = 4;
    DiscoveryListener m_Dlistener = null;
    int m_accessCode;

    DiscoveryAgent() {
    }

    /* access modifiers changed from: package-private */
    public boolean cancelInquiry(DiscoveryListener listener) {
        if (listener != this.m_Dlistener) {
            return false;
        }
        this.m_Dlistener = null;
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean cancelServiceSearch(int transID) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void deviceDiscovered(RemoteDevice d) {
    }

    /* access modifiers changed from: package-private */
    public boolean isInquiring() {
        return this.m_Dlistener != null;
    }

    /* access modifiers changed from: package-private */
    public RemoteDevice[] retrieveDevices(int option) {
        return null;
    }

    /* access modifiers changed from: package-private */
    public int searchServices(int[] attrSet, UUID[] uuidSet, RemoteDevice btDev, DiscoveryListener myListener) {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public String selectService(UUID uuid, int security, boolean master) {
        return "null";
    }

    /* access modifiers changed from: package-private */
    public boolean startInquiry(int accessCode, DiscoveryListener listener) {
        this.m_accessCode = accessCode;
        this.m_Dlistener = listener;
        return true;
    }
}
