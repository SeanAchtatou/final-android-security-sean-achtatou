package ua.netlizard.egypt;

import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.lang.reflect.Array;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    static final int T_Dragged = 3;
    static final int T_Pressed = 1;
    static final int T_Released = 2;
    static int[][] TouchBufferDat = ((int[][]) Array.newInstance(Integer.TYPE, 16, 3));
    static int TouchBufferLen = 0;
    public static GameView instance;
    static int screenHeight;
    static int screenWidth;
    NET_Lizard gameInstance;
    Context mContext;
    SurfaceHolder mSurfaceHolder;
    private SoundSystem m_SS;
    int touch_min_delta = 2;
    int x_pnt;
    int y_pnt;

    public GameView(Context context) {
        super(context);
        if (instance != null) {
        }
        instance = this;
        this.mContext = context;
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.mSurfaceHolder = holder;
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    /* access modifiers changed from: package-private */
    public final int getJ2MEKeyCode(int keyCode, KeyEvent msg) {
        int key = 0;
        if (keyCode == 19) {
            key = -1;
        }
        if (keyCode == 21) {
            key = -3;
        }
        if (keyCode == 22) {
            key = -4;
        }
        if (keyCode == 20) {
            key = -2;
        }
        if (keyCode == 23) {
            key = -6;
        }
        if (keyCode == 4) {
            key = -7;
        }
        if (keyCode >= 7 && keyCode <= 16) {
            key = (keyCode + 48) - 7;
        }
        if (keyCode == 67) {
            key = 35;
        }
        if (keyCode == 62) {
            key = 42;
        }
        if (keyCode == 45) {
            key = 49;
        }
        if (keyCode == 51) {
            key = 50;
        }
        if (keyCode == 33) {
            key = 51;
        }
        if (keyCode == 46) {
            key = 52;
        }
        if (keyCode == 48) {
            key = 53;
        }
        if (keyCode == 53) {
            key = 54;
        }
        if (keyCode == 49) {
            key = 55;
        }
        if (keyCode == 37) {
            key = 56;
        }
        if (keyCode == 43) {
            key = 57;
        }
        if (keyCode == 44) {
            return 48;
        }
        return key;
    }

    public static void TouchBuffer() {
        for (int i = 0; i < TouchBufferLen; i++) {
            switch (TouchBufferDat[i][0]) {
                case 1:
                    synchronized (NET_Lizard.notifyDestroyed) {
                        NET_Lizard.notifyDestroyed.pointerPressed(TouchBufferDat[i][1], TouchBufferDat[i][2]);
                    }
                    break;
                case 2:
                    synchronized (NET_Lizard.notifyDestroyed) {
                        NET_Lizard.notifyDestroyed.pointerReleased(TouchBufferDat[i][1], TouchBufferDat[i][2]);
                    }
                    break;
                case 3:
                    synchronized (NET_Lizard.notifyDestroyed) {
                        NET_Lizard.notifyDestroyed.pointerDragged(TouchBufferDat[i][1], TouchBufferDat[i][2]);
                    }
                    break;
            }
        }
        TouchBufferLen = 0;
    }

    public static void TouchBufferPush(int TouchMode, int x, int y) {
        if (TouchBufferLen < TouchBufferDat.length) {
            TouchBufferDat[TouchBufferLen][0] = TouchMode;
            TouchBufferDat[TouchBufferLen][1] = x;
            TouchBufferDat[TouchBufferLen][2] = y;
            TouchBufferLen++;
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        J2MECanvas ccc = NET_Lizard.notifyDestroyed;
        int x = (int) (event.getRawX() / ((float) J2MECanvas.scale));
        int y = (int) (event.getRawY() / ((float) J2MECanvas.scale));
        switch (event.getAction()) {
            case 0:
                try {
                    if (!J2MECanvas.reapint_process) {
                        ccc.pointerPressed(x, y);
                        break;
                    } else {
                        TouchBufferPush(1, x, y);
                        break;
                    }
                } catch (Exception e) {
                    break;
                }
            case 1:
            case 3:
            case 4:
                try {
                    if (!J2MECanvas.reapint_process) {
                        ccc.pointerReleased(x, y);
                        break;
                    } else {
                        TouchBufferPush(2, x, y);
                        break;
                    }
                } catch (Exception e2) {
                    break;
                }
            case 2:
                try {
                    if (Math.abs(x - this.x_pnt) >= this.touch_min_delta || Math.abs(y - this.y_pnt) >= this.touch_min_delta) {
                        if (!J2MECanvas.reapint_process) {
                            ccc.pointerDragged(x, y);
                            break;
                        } else {
                            TouchBufferPush(3, x, y);
                            break;
                        }
                    }
                } catch (Exception e3) {
                    break;
                }
        }
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        int key = getJ2MEKeyCode(keyCode, msg);
        try {
            NET_Lizard.notifyDestroyed.keyPressed(key);
        } catch (Exception e) {
        }
        if (key == 0) {
            return super.onKeyDown(keyCode, msg);
        }
        return false;
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        int key = getJ2MEKeyCode(keyCode, msg);
        try {
            NET_Lizard.notifyDestroyed.keyReleased(key);
        } catch (Exception e) {
        }
        if (key == 0) {
            return super.onKeyUp(keyCode, msg);
        }
        return true;
    }

    public void onPause() {
        try {
            NET_Lizard.notifyDestroyed.hideNotify();
        } catch (Exception e) {
        }
        try {
            SoundSystem.instance.pause();
        } catch (Exception e2) {
        }
    }

    public void onResume() {
        try {
            SoundSystem.instance.resume();
        } catch (Exception e) {
        }
        try {
            NET_Lizard.notifyDestroyed.showNotify();
        } catch (Exception e2) {
        }
    }

    public void onDestroy() {
        if (this.m_SS != null) {
            this.m_SS.freeAll();
        }
        this.gameInstance = null;
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void start(int w, int h) {
        screenWidth = w;
        screenHeight = h;
        if (this.gameInstance == null) {
            new AndroidUtils(this.mContext, w, h);
            new Display(this.mContext);
            this.m_SS = new SoundSystem();
            this.gameInstance = new NET_Lizard();
            this.gameInstance.startApp();
        }
    }
}
