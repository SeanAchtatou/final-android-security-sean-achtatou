package ua.netlizard.egypt;

import android.media.AudioManager;
import android.media.MediaPlayer;
import java.util.HashMap;

public class SoundSystem {
    static final String TAG = "APP_SS";
    public static SoundSystem instance;
    int maxVolume;
    MediaPlayer mp;
    boolean stopped;
    int streamVol;
    VolumeControl vcontrol;

    SoundSystem() {
        if (instance == null) {
            instance = this;
        }
        this.stopped = false;
        this.streamVol = ((AudioManager) GameView.instance.mContext.getSystemService("audio")).getStreamVolume(3);
        this.maxVolume = ((AudioManager) GameView.instance.mContext.getSystemService("audio")).getStreamMaxVolume(3);
        this.vcontrol = new VolumeControl();
    }

    public void free() {
        instance = null;
        freeAll();
    }

    public void setVolume(int volume) {
        this.streamVol = volume;
        float vol = ((float) this.streamVol) / 100.0f;
        HashMap<String, Player> pm = Manager.players;
        Player[] list = new Player[pm.values().size()];
        pm.values().toArray(list);
        for (int i = 0; i < list.length; i++) {
            if (list[i].mp != null) {
                list[i].mp.setVolume(vol, vol);
            }
        }
    }

    public void freeAll() {
        HashMap<String, Player> pm = Manager.players;
        Player[] list = new Player[pm.values().size()];
        pm.values().toArray(list);
        for (Player freePlayer : list) {
            freePlayer.freePlayer();
        }
    }

    public void pause() {
        this.stopped = true;
        HashMap<String, Player> pm = Manager.players;
        Player[] list = new Player[pm.values().size()];
        pm.values().toArray(list);
        for (int i = 0; i < list.length; i++) {
            try {
                MediaPlayer mp2 = list[i].mp;
                if (mp2 != null) {
                    mp2.release();
                }
            } catch (Exception e) {
            }
        }
    }

    public void resume() {
        this.stopped = false;
        HashMap<String, Player> pm = Manager.players;
        Player[] list = new Player[pm.values().size()];
        pm.values().toArray(list);
        for (int i = 0; i < list.length; i++) {
            try {
                list[i].restart();
            } catch (Exception e) {
            }
        }
    }
}
