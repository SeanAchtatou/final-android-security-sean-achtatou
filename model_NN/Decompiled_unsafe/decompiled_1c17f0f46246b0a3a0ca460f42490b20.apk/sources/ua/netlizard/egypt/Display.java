package ua.netlizard.egypt;

import android.content.Context;
import android.os.Vibrator;

public class Display {
    static Display instance;
    Vibrator v;

    Display(Context context) {
        instance = this;
        this.v = (Vibrator) context.getSystemService("vibrator");
    }

    /* access modifiers changed from: package-private */
    public final int numAlphaLevels() {
        return 256;
    }

    /* access modifiers changed from: package-private */
    public final boolean vibrate(int duration) {
        if (this.v == null) {
            return true;
        }
        this.v.vibrate((long) duration);
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void setCurrent(J2MECanvas c) {
    }

    static Display getDisplay(J2MEMIDlet m) {
        return instance;
    }
}
