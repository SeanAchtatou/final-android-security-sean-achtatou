package ua.netlizard.egypt;

import com.media.fx.Constants;
import java.io.DataInputStream;
import java.io.InputStream;

public final class Uni {
    static byte[] buffer2 = new byte[len_rms];
    static int[] full_can = {1668246830, 1852795753, 1630432617, 1680766313, 776369516, 1816355182, 1986097920};
    static int len_rms = Game.len_rms;
    static int[] p_png = {779120231};
    static boolean rmsRead = true;
    static boolean rmsWrite = false;
    private static String rn = Game.name_rms;
    public static Uni uni;
    static int xor_start = 100;

    static final Image createImage(String fileName) {
        byte[] dataPng = uni.load_pack(fileName, -1);
        try {
            return Image.createImage(dataPng, 0, dataPng.length);
        } catch (Exception e) {
            try {
                return Image.createImage(deXorI(dataPng, xor_start), 0, dataPng.length);
            } catch (Exception e2) {
                deXorI(dataPng, xor_start);
                return Image.createImage(deXor(dataPng, dataPng[2] - 3), 3, dataPng.length - 3);
            }
        }
    }

    private static final byte[] deXor(byte[] m, int sor) {
        int len = m.length;
        byte xor = (byte) sor;
        for (int n = 0; n < len; n++) {
            m[n] = (byte) (m[n] ^ xor);
            xor = (byte) (xor + 1);
        }
        return m;
    }

    public static final byte[] deXorI(byte[] inp, int xor) {
        int len = inp.length;
        for (int n = 0; n < len; n++) {
            inp[n] = (byte) (inp[n] ^ xor);
            xor++;
            if (xor > 255) {
                xor = 0;
            }
        }
        return inp;
    }

    public static final void uni_start() {
        uni = new Uni();
    }

    public static final void uni_end() {
        uni = null;
    }

    private static final byte[] rd_var(byte[] buffer) {
        return buffer;
    }

    private static final byte[] wr_var(byte[] buffer) {
        return buffer;
    }

    private static final void clr_var() {
        Game.first_load = true;
    }

    static final void fl_write() {
        buffer2 = wr_var(buffer2);
        if (rmsWrite) {
            db_write1(rn, buffer2);
        }
        rmsWrite = false;
    }

    static final void fl_read() {
        if (rmsRead) {
            buffer2 = db_read1(rn, buffer2);
        }
        rmsRead = false;
        buffer2 = rd_var(buffer2);
    }

    private static final void db_write1(String name, byte[] rms) {
        try {
            RecordStore rs1 = RecordStore.openRecordStore(name, true);
            if (rs1.getNumRecords() == 1) {
                rs1.closeRecordStore();
                if (RecordStore.listRecordStores() != null) {
                    RecordStore.deleteRecordStore(name);
                }
                rs1 = RecordStore.openRecordStore(name, true);
            }
            rs1.addRecord(rms, 0, rms.length);
            rs1.closeRecordStore();
        } catch (Exception e) {
        }
    }

    static final byte[] db_read1(String name, byte[] rms) {
        boolean db_rez = false;
        try {
            RecordStore rs1 = RecordStore.openRecordStore(name, true);
            if (rs1.getNumRecords() == 1) {
                rs1.getRecord(1, rms, 0);
                db_rez = true;
            }
            rs1.closeRecordStore();
        } catch (Exception e) {
        }
        if (db_rez) {
            return rms;
        }
        clr_var();
        byte[] rms2 = wr_var(rms);
        db_write1(name, rms2);
        return rms2;
    }

    /* access modifiers changed from: protected */
    public final int getFileSize0(String name_file) {
        int symbol = 0;
        int size = -1;
        try {
            InputStream in = AndroidUtils.getResourceAsStream(file_name(name_file));
            if (in == null) {
                return -1;
            }
            while (symbol != -1) {
                symbol = in.read();
                size++;
                if (size > 1) {
                    break;
                }
            }
            in.close();
            return size;
        } catch (Exception e) {
            size = -1;
        }
    }

    /* access modifiers changed from: protected */
    public final int getFileSize(String name_file) {
        int symbol = 0;
        int size = -1;
        try {
            InputStream in = AndroidUtils.getResourceAsStream(file_name(name_file));
            if (in == null) {
                return -1;
            }
            while (symbol != -1) {
                symbol = in.read();
                size++;
            }
            in.close();
            return size;
        } catch (Exception e) {
            size = -1;
        }
    }

    /* access modifiers changed from: protected */
    public final byte[] load_pack(String name_file, int len, int sor) {
        byte[] m = load_pack(name_file, len);
        byte xor = (byte) sor;
        for (int n = 0; n < len; n++) {
            m[n] = (byte) (m[n] ^ xor);
            xor = (byte) (xor + 1);
        }
        return m;
    }

    /* access modifiers changed from: protected */
    public final byte[] load_pack(String name_file, int len) {
        if (len < 0) {
            len = getFileSize(name_file);
        }
        String name_file2 = file_name(name_file);
        if (len <= 0) {
            return null;
        }
        try {
            Class.forName(int_to_str(full_can));
            return load_pack_nokia(name_file2, len);
        } catch (Exception e) {
            byte[] abyte0 = new byte[len];
            try {
                InputStream inputstream = AndroidUtils.getResourceAsStream(name_file2);
                inputstream.read(abyte0);
                inputstream.close();
                return abyte0;
            } catch (Exception e2) {
                return abyte0;
            }
        }
    }

    private final byte[] load_pack_nokia(String name_file, int len) {
        byte[] data = new byte[len];
        try {
            InputStream in = AndroidUtils.getResourceAsStream(name_file);
            if (in != null) {
                DataInputStream din = new DataInputStream(in);
                int offset = 0;
                do {
                    int bytes = din.read(data, offset, data.length - offset);
                    offset += bytes;
                    if (bytes == -1) {
                        break;
                    }
                } while (offset < data.length);
                din.close();
            }
        } catch (Exception e) {
        }
        return data;
    }

    static final String file_name(String name) {
        try {
            if (name.length() <= 0) {
                name = new String(String.valueOf((char) 47) + name);
            } else if (name.charAt(0) != '/') {
                name = new String(String.valueOf((char) 47) + name);
            }
            boolean no_razshir = true;
            for (int i = 0; i < name.length(); i++) {
                if (name.charAt(i) == '.') {
                    no_razshir = false;
                }
            }
            if (no_razshir) {
                return String.valueOf(name) + int_to_str(p_png);
            }
            return name;
        } catch (Exception e) {
            return name;
        }
    }

    public static final String int_to_str(int[] hh) {
        String str = new String();
        for (int i : hh) {
            long ss = (long) i;
            if (ss < 0) {
                ss += 4294967296L;
            }
            str = String.valueOf(str) + ((char) ((int) ((4278190080L & ss) >> 24)));
            byte s = (byte) ((int) ((16711680 & ss) >> 16));
            if (s == 0) {
                break;
            }
            str = String.valueOf(str) + ((char) s);
            byte s2 = (byte) ((int) ((65280 & ss) >> 8));
            if (s2 == 0) {
                break;
            }
            str = String.valueOf(str) + ((char) s2);
            byte s3 = (byte) ((int) (255 & ss));
            if (s3 == 0) {
                break;
            }
            str = String.valueOf(str) + ((char) s3);
        }
        return str;
    }

    public final String[][] load_mmstring(String n_fl, int ra) {
        String[] tmt = sorter(load_string(n_fl), null, 0, false, false);
        String[][] tmt2 = new String[tmt.length][];
        for (int i = 0; i < tmt2.length; i++) {
            if (tmt[i] != null) {
                String tmti = tmt[i];
                int ln_m = tmti.length();
                if (ln_m > 256) {
                    ln_m = 256;
                }
                String[] tmt2t = new String[ln_m];
                int k = 0;
                int js = 0;
                for (int j = 0; j <= tmti.length(); j++) {
                    int ch = ra;
                    if (j < tmti.length()) {
                        ch = tmti.charAt(j);
                    }
                    if (ch == ra && k < ln_m) {
                        tmt2t[k] = tmti.substring(js, j);
                        js = j + 1;
                        k++;
                    }
                }
                tmt2[i] = new String[k];
                System.arraycopy(tmt2t, 0, tmt2[i], 0, k);
            }
        }
        return tmt2;
    }

    /* access modifiers changed from: package-private */
    public final String[] sorter(String s, MyFont font, int txt_w, boolean width_en) {
        return sorter(s, font, txt_w, width_en, true);
    }

    /* access modifiers changed from: package-private */
    public final String[] sorter(String s, MyFont font, int txt_w, boolean width_en, boolean clr_spc) {
        char ch;
        int wm;
        int s_length = s.length();
        String[] buf = new String[s_length];
        int ww = 0;
        int w = 0;
        int ksim = 0;
        int n_str = 0;
        int str_s = 0;
        int str_e = 1;
        int str_s_e = 0;
        int i = 0;
        while (i < s_length + 1) {
            if (i >= s_length) {
                ch = 10;
            } else {
                ch = s.charAt(i);
            }
            int sim = ch;
            if (width_en) {
                wm = font.charWidth(ch);
            } else {
                wm = 5;
            }
            if (!(ch == 10 || ch == 13)) {
                w += wm;
                ww += wm;
            }
            switch (sim) {
                case 10:
                case Constants.RMS_store_recordID_SubscribtionMode:
                    str_e = i;
                    w = txt_w + 1;
                    ww = 0;
                    if (i < s_length) {
                        int i2 = i + 1;
                        if (i2 < s_length) {
                            char cho = ch;
                            char ch2 = s.charAt(i2);
                            if ((ch2 == 10 || ch2 == 13) && ch2 != cho) {
                                i2++;
                            }
                        }
                        str_s_e = i2;
                        i = i2 - 1;
                    } else {
                        str_s_e = s_length;
                    }
                    ch = 10;
                    break;
                case 32:
                    str_e = i + 1;
                    ww = 0;
                    break;
            }
            if ((w <= txt_w || !width_en) && ch != 10) {
                ksim++;
            } else {
                if (str_e == str_s && ksim > 0) {
                    str_e = str_s + ksim;
                    ww = 0;
                    if (i < s_length && i + 1 > str_e) {
                        ww = font.stringWidth(s.substring(str_e, i + 1));
                    }
                }
                if (str_e > str_s) {
                    buf[n_str] = s.substring(str_s, str_e);
                } else {
                    buf[n_str] = "";
                }
                str_s = str_e;
                if (str_s < str_s_e) {
                    str_s = str_s_e;
                }
                str_e = str_s;
                w = ww;
                ksim = (i - str_e) + 1;
                if (clr_spc && buf[n_str] != null && buf[n_str].length() > 0) {
                    while (buf[n_str].length() > 0 && buf[n_str].charAt(buf[n_str].length() - 1) == ' ') {
                        buf[n_str] = buf[n_str].substring(0, buf[n_str].length() - 1);
                    }
                }
                n_str++;
            }
            i++;
        }
        int kvo_str = n_str;
        String[] text = new String[kvo_str];
        System.arraycopy(buf, 0, text, 0, kvo_str);
        return text;
    }

    public final String load_string(String name_file) {
        return load_string(name_file, ' ');
    }

    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:69:0x0096 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:68:0x0096 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v2, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v6, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v7, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v8, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v9, resolved type: char} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r10v5, types: [int, byte] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String load_string(java.lang.String r14, char r15) {
        /*
            r13 = this;
            int r5 = r13.getFileSize(r14)
            java.lang.String r7 = new java.lang.String
            r7.<init>()
            byte[] r6 = r13.load_pack(r14, r5)
            if (r6 == 0) goto L_0x0012
            int r11 = r6.length
            if (r11 > 0) goto L_0x0014
        L_0x0012:
            r11 = 0
        L_0x0013:
            return r11
        L_0x0014:
            int r11 = r6.length
            r12 = 1
            if (r11 <= r12) goto L_0x004e
            r11 = 0
            byte r11 = r6[r11]
            r12 = 59
            if (r11 != r12) goto L_0x004e
            r11 = 1
            byte r11 = r6[r11]
            r12 = 67
            if (r11 != r12) goto L_0x004e
            int r11 = ua.netlizard.egypt.Uni.xor_start
            byte[] r6 = deXorI(r6, r11)
        L_0x002c:
            r2 = 0
            int r11 = r6.length
            r12 = 1
            if (r11 < r12) goto L_0x0039
            r11 = 0
            byte r11 = r6[r11]
            r12 = 95
            if (r11 != r12) goto L_0x0039
            r2 = 2
        L_0x0039:
            int r11 = r6.length
            r12 = 2
            if (r11 < r12) goto L_0x0067
            r11 = 0
            byte r11 = r6[r11]
            r12 = -1
            if (r11 != r12) goto L_0x0067
            r11 = 1
            byte r11 = r6[r11]
            r12 = -2
            if (r11 != r12) goto L_0x0067
            java.lang.String r11 = r13.to_unicode(r6)
            goto L_0x0013
        L_0x004e:
            int r11 = r6.length
            r12 = 1
            if (r11 <= r12) goto L_0x002c
            r11 = 0
            byte r11 = r6[r11]
            r12 = -101(0xffffffffffffff9b, float:NaN)
            if (r11 != r12) goto L_0x002c
            r11 = 1
            byte r11 = r6[r11]
            r12 = -101(0xffffffffffffff9b, float:NaN)
            if (r11 != r12) goto L_0x002c
            int r11 = ua.netlizard.egypt.Uni.xor_start
            byte[] r6 = deXorI(r6, r11)
            goto L_0x002c
        L_0x0067:
            r9 = r2
            r10 = 0
            int r11 = r6.length
            int r3 = r11 - r2
            int r4 = r3 + r2
            r0 = r2
        L_0x006f:
            if (r0 < r4) goto L_0x0083
        L_0x0071:
            char[] r1 = new char[r3]
            r0 = 0
        L_0x0074:
            if (r0 < r3) goto L_0x008e
        L_0x0076:
            if (r9 < r5) goto L_0x009c
        L_0x0078:
            r6 = 0
            byte[] r6 = (byte[]) r6
            java.lang.String r8 = new java.lang.String     // Catch:{ Exception -> 0x00c8 }
            r8.<init>(r1)     // Catch:{ Exception -> 0x00c8 }
            r7 = r8
        L_0x0081:
            r11 = r7
            goto L_0x0013
        L_0x0083:
            byte r11 = r6[r0]
            if (r11 != 0) goto L_0x008b
            int r3 = r0 - r2
            r5 = r0
            goto L_0x0071
        L_0x008b:
            int r0 = r0 + 1
            goto L_0x006f
        L_0x008e:
            int r11 = r0 + r2
            byte r10 = r6[r11]
            if (r10 >= 0) goto L_0x0096
            int r10 = r10 + 256
        L_0x0096:
            char r11 = (char) r10
            r1[r0] = r11
            int r0 = r0 + 1
            goto L_0x0074
        L_0x009c:
            byte r10 = r6[r9]
            if (r10 >= 0) goto L_0x00a2
            int r10 = r10 + 256
        L_0x00a2:
            if (r10 == 0) goto L_0x0078
            if (r10 != r15) goto L_0x00ac
            int r11 = r9 - r2
            r12 = 32
            r1[r11] = r12
        L_0x00ac:
            r11 = 10
            if (r10 >= r11) goto L_0x00b6
            int r11 = r9 - r2
            r12 = 32
            r1[r11] = r12
        L_0x00b6:
            r11 = 192(0xc0, float:2.69E-43)
            if (r10 < r11) goto L_0x00c5
            r11 = 255(0xff, float:3.57E-43)
            if (r10 > r11) goto L_0x00c5
            int r10 = r10 + 848
            int r11 = r9 - r2
            char r12 = (char) r10
            r1[r11] = r12
        L_0x00c5:
            int r9 = r9 + 1
            goto L_0x0076
        L_0x00c8:
            r11 = move-exception
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.Uni.load_string(java.lang.String, char):java.lang.String");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:42:0x0068 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:41:0x0068 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: int} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r3v0, types: [int, byte] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String to_unicode(byte[] r14) {
        /*
            r13 = this;
            r12 = 1
            r11 = 0
            r10 = 2
            r7 = 0
            int r8 = r14.length
            if (r8 < r10) goto L_0x0012
            byte r8 = r14[r11]
            r9 = -1
            if (r8 != r9) goto L_0x0012
            byte r8 = r14[r12]
            r9 = -2
            if (r8 != r9) goto L_0x0012
            r7 = 2
        L_0x0012:
            r5 = 0
            r4 = r7
        L_0x0014:
            int r8 = r14.length
            if (r4 < r8) goto L_0x0046
        L_0x0017:
            char[] r1 = new char[r5]
            r4 = r7
            r0 = 0
        L_0x001b:
            if (r0 < r5) goto L_0x005a
            java.lang.String r6 = new java.lang.String
            r6.<init>(r1)
            r1 = 0
            char[] r1 = (char[]) r1
            if (r6 == 0) goto L_0x0045
            int r8 = r6.length()
            if (r8 < r10) goto L_0x0045
            char r8 = r6.charAt(r11)
            r9 = 95
            if (r8 != r9) goto L_0x0045
            char r8 = r6.charAt(r12)
            r9 = 38
            if (r8 != r9) goto L_0x0045
            int r8 = r6.length()
            java.lang.String r6 = r6.substring(r10, r8)
        L_0x0045:
            return r6
        L_0x0046:
            byte r8 = r14[r4]
            if (r8 != 0) goto L_0x0055
            int r8 = r4 + 1
            int r9 = r14.length
            if (r8 >= r9) goto L_0x0017
            int r8 = r4 + 1
            byte r8 = r14[r8]
            if (r8 == 0) goto L_0x0017
        L_0x0055:
            int r5 = r5 + 1
            int r4 = r4 + 2
            goto L_0x0014
        L_0x005a:
            byte r2 = r14[r4]
            if (r2 >= 0) goto L_0x0060
            int r2 = r2 + 256
        L_0x0060:
            int r4 = r4 + 1
            byte r3 = r14[r4]
            if (r3 >= 0) goto L_0x0068
            int r3 = r3 + 256
        L_0x0068:
            int r4 = r4 + 1
            int r8 = r3 * 256
            int r8 = r8 + r2
            char r8 = (char) r8
            r1[r0] = r8
            int r0 = r0 + 1
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.Uni.to_unicode(byte[]):java.lang.String");
    }

    public final String[][] load_mmstring(String n_fl) {
        return load_mmstring(n_fl, 61);
    }

    static byte[] NetOpenConnection(String URL) {
        int i = 0;
        byte[] data = new byte[10];
        try {
            HttpConnection h = (HttpConnection) Connector.open(URL, 1, true);
            InputStream is = h.openInputStream();
            while (true) {
                int ch = is.read();
                if (ch == -1) {
                    is.close();
                    h.close();
                    byte[] mas = new byte[i];
                    System.arraycopy(data, 0, mas, 0, i);
                    return mas;
                }
                data[i] = (byte) ch;
                i++;
            }
        } catch (Exception e) {
            return null;
        }
    }
}
