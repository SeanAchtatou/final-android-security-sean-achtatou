package ua.netlizard.egypt;

public class Font {
    public static final int FACE_SYSTEM = 0;
    public static final int SIZE_SMALL = 8;
    public static final int STYLE_PLAIN = 0;
    int face;
    int size;
    int style;

    protected Font(int a, int b, int c) {
        this.face = a;
        this.style = b;
        this.size = c;
    }

    static Font getFont(int a, int b, int c) {
        return new Font(a, b, c);
    }
}
