package ua.netlizard.egypt;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;

public class Graphics {
    public static final int BASELINE = 64;
    public static final int BOTTOM = 32;
    public static final int DOTTED = 1;
    public static final int HCENTER = 1;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    public static final int SOLID = 0;
    public static final int TOP = 16;
    public static final int VCENTER = 2;
    private Bitmap m_Bitmap;
    private Canvas m_Canvas;
    private RectF m_ClipRect;
    private int m_Color;
    private Font m_CurrFont;
    private Matrix m_Matrix;
    private Paint m_Paint;
    private RectF m_Rect;
    private Region m_Region;
    private int m_TransX;
    private int m_TransY;

    Graphics(Bitmap bitmap) {
        this.m_Bitmap = bitmap;
        this.m_Canvas = new Canvas(bitmap);
        if (this.m_Canvas != null) {
            initGraphicsWithParams();
        } else {
            initGraphics();
        }
    }

    Graphics(Canvas canvas) {
        this.m_Canvas = canvas;
        if (this.m_Canvas != null) {
            initGraphicsWithParams();
        } else {
            initGraphics();
        }
    }

    Graphics() {
        initGraphics();
    }

    private void initGraphicsWithParams() {
        this.m_Rect = new RectF(0.0f, 0.0f, (float) this.m_Canvas.getWidth(), (float) this.m_Canvas.getHeight());
        this.m_ClipRect = new RectF(0.0f, 0.0f, (float) this.m_Canvas.getWidth(), (float) this.m_Canvas.getHeight());
        this.m_Paint = new Paint();
        this.m_Region = new Region();
        this.m_Region.set(0, 0, this.m_Canvas.getWidth(), this.m_Canvas.getHeight());
        this.m_Matrix = new Matrix();
        this.m_Matrix.reset();
        this.m_TransX = 0;
        this.m_TransY = 0;
    }

    private void initGraphics() {
        this.m_Rect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);
        this.m_ClipRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);
        this.m_Paint = new Paint();
        this.m_Region = new Region();
        this.m_Region.set(0, 0, 0, 0);
        this.m_Matrix = new Matrix();
        this.m_Matrix.reset();
        this.m_TransX = 0;
        this.m_TransY = 0;
    }

    public void setCanvas(Canvas canvas) {
        this.m_Canvas = canvas;
        this.m_Rect.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
        this.m_ClipRect.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
        this.m_Region.set(0, 0, getWidth(), getHeight());
        this.m_TransX = 0;
        this.m_TransY = 0;
    }

    public Canvas getCanvas() {
        return this.m_Canvas;
    }

    /* access modifiers changed from: package-private */
    public final int getWidth() {
        return this.m_Canvas.getWidth();
    }

    /* access modifiers changed from: package-private */
    public final int getHeight() {
        return this.m_Canvas.getHeight();
    }

    /* access modifiers changed from: package-private */
    public final void setClip(int x, int y, int w, int h) {
        this.m_ClipRect.set((float) x, (float) y, (float) (x + w), (float) (y + h));
        this.m_Region.set(x, y, x + w, y + h);
        this.m_Canvas.clipRegion(this.m_Region, Region.Op.REPLACE);
        this.m_Canvas.clipRect(this.m_ClipRect, Region.Op.REPLACE);
    }

    /* access modifiers changed from: package-private */
    public final void setColor(int c) {
        setColor((c >> 16) & 255, (c >> 8) & 255, c & 255);
    }

    /* access modifiers changed from: package-private */
    public final void setColor(int r, int g, int b) {
        this.m_Color = -16777216 + ((r & 255) << 16) + ((g & 255) << 8) + (b & 255);
    }

    /* access modifiers changed from: package-private */
    public final void fillRect(int x, int y, int w, int h) {
        int xx = x;
        int yy = y;
        this.m_Rect.set((float) xx, (float) yy, (float) (xx + w), (float) (yy + h));
        this.m_Canvas.clipRegion(this.m_Region, Region.Op.REPLACE);
        this.m_Canvas.clipRect(this.m_Rect, Region.Op.INTERSECT);
        this.m_Canvas.drawColor(this.m_Color);
        this.m_Canvas.clipRect(this.m_ClipRect, Region.Op.REPLACE);
        this.m_Canvas.clipRegion(this.m_Region, Region.Op.REPLACE);
    }

    /* access modifiers changed from: package-private */
    public final void fillTriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        if (Math.abs(x2 - x3) > Math.abs(y2 - y3)) {
            int dx = 1;
            if (x3 < x2) {
                dx = -1;
            }
            for (int x = x2; x != x3; x += dx) {
                drawLine(x1, y1, x, y2);
            }
            return;
        }
        int dy = 1;
        if (y3 < y2) {
            dy = -1;
        }
        for (int y = y2; y != y3; y += dy) {
            drawLine(x1, y1, x2, y);
        }
    }

    /* access modifiers changed from: package-private */
    public final void drawImage(Image img, int x, int y, int a) {
        if (img != null && img.getBitmap() != null) {
            int offx = 0;
            int offy = 0;
            if ((a & 1) == 1) {
                offx = 0 - (img.getWidth() >> 1);
            }
            if ((a & 2) == 2) {
                offy = 0 - (img.getHeight() >> 1);
            }
            if ((a & 8) == 8) {
                offx -= img.getWidth();
            }
            if ((a & 32) == 32) {
                offy -= img.getHeight();
            }
            int x2 = x + offx;
            int y2 = y + offy;
            this.m_Rect.set((float) x2, (float) y2, (float) (img.getWidth() + x2), (float) (img.getHeight() + y2));
            this.m_Canvas.drawBitmap(img.getBitmap(), (Rect) null, this.m_Rect, (Paint) null);
        }
    }

    /* access modifiers changed from: package-private */
    public final void drawLine(int x1, int y1, int x2, int y2) {
        this.m_Paint.setColor(this.m_Color);
        this.m_Canvas.drawLine((float) x1, (float) y1, (float) x2, (float) y2, this.m_Paint);
    }

    /* access modifiers changed from: package-private */
    public final void drawRect(int x, int y, int w, int h) {
        int xx = x;
        int yy = y;
        fillRect(xx, yy, w, 1);
        fillRect(xx, yy + h, w + 1, 1);
        fillRect(xx, yy, 1, h);
        fillRect(xx + w, yy, 1, h);
    }

    /* access modifiers changed from: package-private */
    public final void drawRGB(int[] rgbData, int offset, int scanLength, int x, int y, int width, int height, boolean processAlpha) {
        this.m_Canvas.drawBitmap(rgbData, offset, width, x, y, width, height, processAlpha, (Paint) null);
    }

    /* access modifiers changed from: package-private */
    public final int getClipWidth() {
        return (int) (this.m_ClipRect.right - this.m_ClipRect.left);
    }

    /* access modifiers changed from: package-private */
    public final int getClipHeight() {
        return (int) (this.m_ClipRect.bottom - this.m_ClipRect.top);
    }

    /* access modifiers changed from: package-private */
    public final int getClipX() {
        return (int) this.m_ClipRect.left;
    }

    /* access modifiers changed from: package-private */
    public final int getClipY() {
        return (int) this.m_ClipRect.top;
    }

    /* access modifiers changed from: package-private */
    public final int getColor() {
        return this.m_Color;
    }

    /* access modifiers changed from: package-private */
    public final int getTranslateX() {
        return this.m_TransX;
    }

    /* access modifiers changed from: package-private */
    public final int getTranslateY() {
        return this.m_TransY;
    }

    /* access modifiers changed from: package-private */
    public final void translate(int x, int y) {
        this.m_TransX += x;
        this.m_TransY += y;
        this.m_Matrix.reset();
        this.m_Matrix.postTranslate((float) this.m_TransX, (float) this.m_TransY);
        this.m_Canvas.setMatrix(this.m_Matrix);
    }

    /* access modifiers changed from: package-private */
    public final void setColorARGB(int color) {
        this.m_Color = color;
    }

    /* access modifiers changed from: package-private */
    public final void setFont(Font font) {
        this.m_CurrFont = font;
    }

    /* access modifiers changed from: package-private */
    public final void drawString(String str, int x, int y, int a) {
        drawSubstring(str, 0, str.length(), x, y, a);
    }

    /* access modifiers changed from: package-private */
    public final void drawSubstring(String str, int offset, int len, int x, int y, int a) {
        this.m_Paint.setColor(this.m_Color);
        this.m_Canvas.drawText(str.substring(offset, offset + len), (float) x, (float) y, this.m_Paint);
    }
}
