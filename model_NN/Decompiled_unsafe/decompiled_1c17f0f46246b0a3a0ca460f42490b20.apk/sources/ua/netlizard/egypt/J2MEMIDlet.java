package ua.netlizard.egypt;

public class J2MEMIDlet {
    /* access modifiers changed from: protected */
    public void destroyApp(boolean unconditional) {
    }

    /* access modifiers changed from: package-private */
    public String getAppProperty(String key) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void notifyDestroyed() {
        System.exit(0);
    }

    /* access modifiers changed from: package-private */
    public void notifyPaused() {
    }

    /* access modifiers changed from: package-private */
    public void pauseApp() {
    }

    /* access modifiers changed from: package-private */
    public boolean platformRequest(String URL) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void resumeRequest() {
    }

    /* access modifiers changed from: package-private */
    public void startApp() {
    }
}
