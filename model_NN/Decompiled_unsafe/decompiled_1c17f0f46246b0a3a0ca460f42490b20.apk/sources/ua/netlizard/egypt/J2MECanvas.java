package ua.netlizard.egypt;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.SurfaceHolder;

public class J2MECanvas {
    public static final int DOWN = 6;
    public static final int FIRE = 8;
    public static final int GAME_A = 9;
    public static final int GAME_B = 10;
    public static final int GAME_C = 11;
    public static final int GAME_D = 12;
    public static final int KEY_NUM0 = 48;
    public static final int KEY_NUM1 = 49;
    public static final int KEY_NUM2 = 50;
    public static final int KEY_NUM3 = 51;
    public static final int KEY_NUM4 = 52;
    public static final int KEY_NUM5 = 53;
    public static final int KEY_NUM6 = 54;
    public static final int KEY_NUM7 = 55;
    public static final int KEY_NUM8 = 56;
    public static final int KEY_NUM9 = 57;
    public static final int KEY_POUND = 35;
    public static final int KEY_STAR = 42;
    public static final int LEFT = 2;
    public static final int RIGHT = 5;
    public static final int UP = 1;
    static Canvas c = null;
    static SurfaceHolder mSurfaceHolder;
    static boolean reapint_process = false;
    static Canvas sc;
    static int scale;
    private Graphics m_Graphics = new Graphics();
    private GameView m_MainView = GameView.instance;
    private Matrix m_Matrix = new Matrix();
    private Matrix m_MatrixIdent;
    private Bitmap m_Screen = Bitmap.createBitmap(this.m_ScreenWidth, this.m_ScreenHeight, Bitmap.Config.ARGB_8888);
    protected int m_ScreenHeight = (GameView.screenHeight / scale);
    protected int m_ScreenWidth = (GameView.screenWidth / scale);

    J2MECanvas() {
        scale = AndroidUtils.scale;
        this.m_Matrix.reset();
        this.m_Matrix.setScale((float) scale, (float) scale);
        this.m_MatrixIdent = new Matrix();
        this.m_MatrixIdent.reset();
        mSurfaceHolder = this.m_MainView.getHolder();
    }

    public int getGameAction(int keyCode) {
        return 0;
    }

    public int getWidth() {
        return this.m_ScreenWidth;
    }

    public int getHeight() {
        return this.m_ScreenHeight;
    }

    public void paint(Graphics g) {
    }

    /* access modifiers changed from: protected */
    public void hideNotify() {
    }

    public boolean isDoubleBuffered() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void keyPressed(int keyCode) {
    }

    /* access modifiers changed from: protected */
    public void keyReleased(int keyCode) {
    }

    /* access modifiers changed from: protected */
    public void pointerPressed(int x, int y) {
    }

    /* access modifiers changed from: protected */
    public void pointerDragged(int x, int y) {
    }

    /* access modifiers changed from: protected */
    public void pointerReleased(int x, int y) {
    }

    /* access modifiers changed from: package-private */
    public final boolean hasPointerEvents() {
        return true;
    }

    public void repaint() {
        Canvas c2 = null;
        reapint_process = true;
        Canvas sc2 = new Canvas(this.m_Screen);
        try {
            c2 = mSurfaceHolder.lockCanvas();
            if (c2 != null) {
                this.m_Graphics.setCanvas(sc2);
                paint(this.m_Graphics);
                synchronized (mSurfaceHolder) {
                    c2.setMatrix(this.m_Matrix);
                    c2.drawBitmap(this.m_Screen, 0.0f, 0.0f, (Paint) null);
                    c2.setMatrix(this.m_MatrixIdent);
                }
            }
            if (c2 != null) {
                try {
                    mSurfaceHolder.unlockCanvasAndPost(c2);
                } catch (Exception e) {
                    System.out.println("J2MECanvas exception2");
                }
            }
        } catch (Exception e2) {
            try {
                System.out.println("J2MECanvas exception");
                if (c2 != null) {
                    try {
                        mSurfaceHolder.unlockCanvasAndPost(c2);
                    } catch (Exception e3) {
                        System.out.println("J2MECanvas exception2");
                    }
                }
            } catch (Throwable th) {
                if (c2 != null) {
                    try {
                        mSurfaceHolder.unlockCanvasAndPost(c2);
                    } catch (Exception e4) {
                        System.out.println("J2MECanvas exception2");
                    }
                }
                throw th;
            }
        }
        reapint_process = false;
        GameView.TouchBuffer();
    }

    public void serviceRepaints() {
    }

    /* access modifiers changed from: package-private */
    public void setFullScreenMode(boolean mode) {
    }

    /* access modifiers changed from: protected */
    public void showNotify() {
    }

    /* access modifiers changed from: protected */
    public void sizeChanged(int w, int h) {
    }
}
