package ua.netlizard.egypt;

import android.util.Log;
import java.io.InputStream;
import java.util.Random;

public class Snd2 implements Runnable {
    static int Podstilka = -1;
    static int anymusic = -1;
    static boolean autoDeallocate = false;
    static Game b;
    static boolean cikl = false;
    static boolean cikl_rn = true;
    static int kvo_podstilok = -1;
    static String l_versionSound = "v";
    static int[] masVersionsSound;
    static int maxNumberVersion = 8;
    static boolean minSound = false;
    static boolean motor = false;
    static int msoundm = 6;
    static Random myRandom = new Random();
    static String[] nameSound = null;
    static String[] name_file;
    static String name_file_last;
    static String name_podstilka = "/mm";
    static int numberPlaysAnymusic = 1;
    static int numberPlaysMusic = 3;
    static int numberPlaysPodstilka = 100;
    static boolean optimizeSounds = false;
    static int[] p_mid = {778922340};
    static Player[] player;
    static int podstilka2 = -1;
    static boolean prioritySounds = true;
    static boolean runnable = false;
    static Snd2 snd2;
    static boolean snd2_created = false;
    static int sound1 = -1;
    static int soundDefault = -1;
    static int[] soundNprior = null;
    static int soundOld = -1;
    static String soundPath = "";
    static String soundPathOld = "";
    static boolean stoppedClose = false;
    static boolean testVersionsSound = true;
    static Thread thread;
    static long vibroTimeNew = 0;
    static int volume = 100;
    static boolean xwav_en = false;

    public Snd2(Game game) {
        b = game;
        if (minSound) {
            Game.kvo_zv = msoundm;
        }
    }

    public Snd2() {
        if (minSound) {
            Game.kvo_zv = msoundm;
        }
    }

    public static final void soundStart(Game game) {
        snd2 = new Snd2(game);
        if (minSound) {
            Game.kvo_zv = msoundm;
        }
    }

    public static final void soundStart() {
        if (minSound) {
            Game.kvo_zv = msoundm;
        }
    }

    public void run() {
        cikl = true;
        if (player == null) {
            createSound1();
        }
        int prefetched = -1;
        while (cikl_rn) {
            if (soundDefault >= 0 && prefetched >= 0 && prefetched != soundDefault && !soundStarted()) {
                soundStart(soundDefault, false);
                prefetched = soundDefault;
                soundOld = prefetched;
            }
            if (motor && autoDeallocate && soundDefault < 0 && prefetched >= 0 && !soundStarted()) {
                soundDeallocate(prefetched);
                prefetched = -1;
            }
            try {
                Thread.sleep(3);
            } catch (Exception e) {
            }
            if (sound1 >= 0) {
                sound1(sound1);
                prefetched = sound1;
                sound1 = -1;
            }
            try {
                Thread.sleep(17);
            } catch (Exception e2) {
            }
        }
        cikl = false;
    }

    public static void createSound() {
        snd2_created = true;
        if (snd2 == null) {
            soundStart(NET_Lizard.notifyDestroyed);
        }
        if (snd2 == null) {
            soundStart();
        }
        testWav();
        if (player == null) {
            createSound1();
        }
        if (runnable) {
            cikl_rn = true;
            if (thread == null) {
                thread = new Thread(snd2);
                thread.start();
            }
            if (thread.getPriority() != 1) {
                thread.setPriority(1);
            }
        }
    }

    public static boolean cpStr(String t1, String t2) {
        if (t1 == null || t2 == null) {
            if (t1 == null && t2 == null) {
                return true;
            }
            return false;
        } else if (t1.length() != t2.length()) {
            return false;
        } else {
            for (int i = 0; i < t1.length(); i++) {
                if (t1.charAt(i) != t2.charAt(i)) {
                    return false;
                }
            }
            return true;
        }
    }

    public static void createSound1() {
        if (player == null) {
            player = new Player[(Game.kvo_zv + 1)];
        }
        if (name_file == null) {
            name_file = new String[(Game.kvo_zv + 1)];
        }
        for (int i = 0; i < player.length; i++) {
            if ((nameSound == null || i < nameSound.length || i >= Game.kvo_zv) && player[i] == null) {
                createSound(i);
            }
        }
        soundPathOld = soundPath;
    }

    public static void createLevelSounds(String[] names) {
        nameSound = names;
        masVersionsSound = null;
        reCreateSound();
    }

    public static void reCreateSound() {
        if (!snd2_created) {
            createSound();
        }
        soundStop();
        if (player == null) {
            createSound();
        } else if (nameSound == null && masVersionsSound == null) {
            createSound();
        } else {
            for (int i = 0; i < player.length; i++) {
                if ((nameSound != null && i < nameSound.length) || (masVersionsSound != null && i < masVersionsSound.length && masVersionsSound[i] > 1)) {
                    soundClose(i);
                    createSound(i);
                }
            }
        }
    }

    public static void createSound(int i) {
        createSound(i, Math.abs(myRandom.nextInt() % maxNumberVersion));
    }

    public static void createSound(int i, int nv) {
        int j;
        if (!snd2_created) {
            createSound();
        }
        if (minSound) {
            Game.kvo_zv = msoundm;
        }
        if (player == null) {
            player = new Player[(Game.kvo_zv + 1)];
        }
        if (player[i] == null || player[i].getState() == 0) {
            String name = "/" + i + ".mp3";
            if (i == Game.kvo_zv) {
                name = "/mm.mid";
            }
            if (nameSound != null && i < nameSound.length) {
                name = new String(nameSound[i]);
            }
            if (testVersionsSound && (j = create_masVersionsSound(String.valueOf(file_name_nr(name)) + l_versionSound, i)) > 0) {
                while (nv >= j) {
                    nv -= j;
                }
                name = file_name(String.valueOf(file_name_nr(name)) + l_versionSound + nv);
            }
            createSound(name, i);
        }
    }

    public static void testWav() {
        try {
            String[] as = Manager.getSupportedContentTypes(null);
            xwav_en = false;
            for (String equals : as) {
                if (equals.equals("audio/x-wav")) {
                    xwav_en = true;
                    return;
                }
            }
        } catch (Exception e) {
        }
    }

    private static final int kvoPodstilok() {
        kvo_podstilok = kvoVersionSound(name_podstilka, kvo_podstilok);
        return kvo_podstilok;
    }

    static final int kvoVersionSound(String namever, int stkvo) {
        String namever2 = file_name_nr(namever);
        if (stkvo < 0) {
            stkvo = 0;
            while (true) {
                if (Uni.uni.getFileSize(String.valueOf(namever2) + stkvo + ".mid") <= 0 && Uni.uni.getFileSize(String.valueOf(namever2) + stkvo + ".amr") <= 0 && Uni.uni.getFileSize(String.valueOf(namever2) + stkvo + ".mp3") <= 0 && Uni.uni.getFileSize(String.valueOf(namever2) + stkvo + ".wav") <= 0) {
                    break;
                }
                stkvo++;
            }
        }
        return stkvo;
    }

    static final int create_masVersionsSound(String name, int i) {
        String name2 = file_name_nr(name);
        if (masVersionsSound == null) {
            masVersionsSound = new int[(Game.kvo_zv + 1)];
            for (int j = 0; j < masVersionsSound.length; j++) {
                masVersionsSound[j] = -1;
            }
        }
        int[] iArr = masVersionsSound;
        int kvoVersionSound = kvoVersionSound(name2, masVersionsSound[i]);
        iArr[i] = kvoVersionSound;
        return kvoVersionSound;
    }

    static final String anyMusic(String namemusic) {
        return anyMusic(namemusic, Math.abs(myRandom.nextInt() % maxNumberVersion));
    }

    static final String anyMusic(String namemusic, int nv) {
        int j;
        if (player == null) {
            createSound();
        }
        String namemusic2 = file_name(namemusic);
        soundStop();
        if (testVersionsSound && (j = kvoVersionSound(String.valueOf(file_name_nr(namemusic2)) + l_versionSound, -1)) > 0) {
            while (nv >= j) {
                nv -= j;
            }
            namemusic2 = file_name(String.valueOf(file_name_nr(namemusic2)) + l_versionSound + nv);
        }
        if (getFileSize(namemusic2) <= 0) {
            return null;
        }
        int i = Game.kvo_zv;
        soundClose(i);
        createSound(namemusic2, i);
        podstilka2 = i;
        anymusic = i;
        sound1(i);
        return name_file[i];
    }

    static final String podstilka(int n_podstilka) {
        if (player == null) {
            createSound();
        }
        kvoPodstilok();
        if (n_podstilka < 0) {
            n_podstilka = 0;
        }
        soundStop();
        int i = Game.kvo_zv;
        if (kvo_podstilok > 0) {
            while (n_podstilka >= kvo_podstilok) {
                n_podstilka -= kvo_podstilok;
            }
            soundClose(i);
            createSound(String.valueOf(name_podstilka) + n_podstilka + ".mid", i);
            podstilka2 = i;
        }
        sound1(Game.kvo_zv);
        return name_file[i];
    }

    static final int getFileSize(String name_file2) {
        int symbol = 0;
        int size = -1;
        try {
            InputStream in = AndroidUtils.getResourceAsStream(file_name(name_file2));
            if (in == null) {
                return -1;
            }
            while (symbol != -1) {
                symbol = in.read();
                size++;
            }
            in.close();
            return size;
        } catch (Exception e) {
            size = -1;
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0178 A[ExcHandler: OutOfMemoryError (e java.lang.OutOfMemoryError), PHI: r1 r3 r10 
      PHI: (r1v4 'inputstream' java.io.InputStream) = (r1v6 'inputstream' java.io.InputStream), (r1v3 'inputstream' java.io.InputStream) binds: [B:47:0x0124, B:40:0x00f1] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r3v2 'popitka2_mid' boolean) = (r3v4 'popitka2_mid' boolean), (r3v1 'popitka2_mid' boolean) binds: [B:47:0x0124, B:40:0x00f1] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r10v8 's' java.lang.String) = (r10v10 's' java.lang.String), (r10v7 's' java.lang.String) binds: [B:47:0x0124, B:40:0x00f1] A[DONT_GENERATE, DONT_INLINE], Splitter:B:40:0x00f1] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x017c A[ExcHandler: OutOfMemoryError (e java.lang.OutOfMemoryError), PHI: r1 r10 
      PHI: (r1v1 'inputstream' java.io.InputStream) = (r1v0 'inputstream' java.io.InputStream), (r1v11 'inputstream' java.io.InputStream), (r1v10 'inputstream' java.io.InputStream), (r1v9 'inputstream' java.io.InputStream) binds: [B:8:0x0049, B:30:0x00bb, B:22:0x0089, B:14:0x0057] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r10v5 's' java.lang.String) = (r10v4 's' java.lang.String), (r10v13 's' java.lang.String), (r10v12 's' java.lang.String), (r10v4 's' java.lang.String) binds: [B:8:0x0049, B:30:0x00bb, B:22:0x0089, B:14:0x0057] A[DONT_GENERATE, DONT_INLINE], Splitter:B:8:0x0049] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void createSound(java.lang.String r10, int r11) {
        /*
            java.lang.String r7 = ua.netlizard.egypt.Snd2.soundPath
            if (r7 == 0) goto L_0x0021
            int r7 = ua.netlizard.egypt.Game.kvo_zv
            if (r11 == r7) goto L_0x0021
            java.lang.String r4 = new java.lang.String
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = ua.netlizard.egypt.Snd2.soundPath
            java.lang.String r8 = java.lang.String.valueOf(r8)
            r7.<init>(r8)
            java.lang.StringBuilder r7 = r7.append(r10)
            java.lang.String r7 = r7.toString()
            r4.<init>(r7)
            r10 = r4
        L_0x0021:
            java.lang.String r10 = file_name(r10)
            ua.netlizard.egypt.Player[] r7 = ua.netlizard.egypt.Snd2.player
            r8 = 0
            r7[r11] = r8
            r1 = 0
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x018b }
            r8 = 0
            int r9 = r10.length()     // Catch:{ Exception -> 0x018b }
            int r9 = r9 + -4
            java.lang.String r8 = r10.substring(r8, r9)     // Catch:{ Exception -> 0x018b }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x018b }
            r7.<init>(r8)     // Catch:{ Exception -> 0x018b }
            java.lang.String r8 = ".mp3"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x018b }
            java.lang.String r10 = r7.toString()     // Catch:{ Exception -> 0x018b }
        L_0x0049:
            int r7 = getFileSize(r10)     // Catch:{ Exception -> 0x0188, OutOfMemoryError -> 0x017c }
            if (r7 <= 0) goto L_0x0053
            java.io.InputStream r1 = ua.netlizard.egypt.AndroidUtils.getResourceAsStream(r10)     // Catch:{ Exception -> 0x0188, OutOfMemoryError -> 0x017c }
        L_0x0053:
            if (r1 != 0) goto L_0x0085
            java.lang.String r7 = ".mp3"
            boolean r7 = r10.endsWith(r7)     // Catch:{ Exception -> 0x0185, OutOfMemoryError -> 0x017c }
            if (r7 != 0) goto L_0x0085
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0185, OutOfMemoryError -> 0x017c }
            r8 = 0
            int r9 = r10.length()     // Catch:{ Exception -> 0x0185, OutOfMemoryError -> 0x017c }
            int r9 = r9 + -4
            java.lang.String r8 = r10.substring(r8, r9)     // Catch:{ Exception -> 0x0185, OutOfMemoryError -> 0x017c }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0185, OutOfMemoryError -> 0x017c }
            r7.<init>(r8)     // Catch:{ Exception -> 0x0185, OutOfMemoryError -> 0x017c }
            java.lang.String r8 = ".mp3"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x0185, OutOfMemoryError -> 0x017c }
            java.lang.String r10 = r7.toString()     // Catch:{ Exception -> 0x0185, OutOfMemoryError -> 0x017c }
            int r7 = getFileSize(r10)     // Catch:{ Exception -> 0x0185, OutOfMemoryError -> 0x017c }
            if (r7 <= 0) goto L_0x0085
            java.io.InputStream r1 = ua.netlizard.egypt.AndroidUtils.getResourceAsStream(r10)     // Catch:{ Exception -> 0x0185, OutOfMemoryError -> 0x017c }
        L_0x0085:
            if (r1 != 0) goto L_0x00b7
            java.lang.String r7 = ".wav"
            boolean r7 = r10.endsWith(r7)     // Catch:{ Exception -> 0x0182, OutOfMemoryError -> 0x017c }
            if (r7 != 0) goto L_0x00b7
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0182, OutOfMemoryError -> 0x017c }
            r8 = 0
            int r9 = r10.length()     // Catch:{ Exception -> 0x0182, OutOfMemoryError -> 0x017c }
            int r9 = r9 + -4
            java.lang.String r8 = r10.substring(r8, r9)     // Catch:{ Exception -> 0x0182, OutOfMemoryError -> 0x017c }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0182, OutOfMemoryError -> 0x017c }
            r7.<init>(r8)     // Catch:{ Exception -> 0x0182, OutOfMemoryError -> 0x017c }
            java.lang.String r8 = ".wav"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x0182, OutOfMemoryError -> 0x017c }
            java.lang.String r10 = r7.toString()     // Catch:{ Exception -> 0x0182, OutOfMemoryError -> 0x017c }
            int r7 = getFileSize(r10)     // Catch:{ Exception -> 0x0182, OutOfMemoryError -> 0x017c }
            if (r7 <= 0) goto L_0x00b7
            java.io.InputStream r1 = ua.netlizard.egypt.AndroidUtils.getResourceAsStream(r10)     // Catch:{ Exception -> 0x0182, OutOfMemoryError -> 0x017c }
        L_0x00b7:
            if (r1 != 0) goto L_0x00e9
            java.lang.String r7 = ".amr"
            boolean r7 = r10.endsWith(r7)     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x017c }
            if (r7 != 0) goto L_0x00e9
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x017c }
            r8 = 0
            int r9 = r10.length()     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x017c }
            int r9 = r9 + -4
            java.lang.String r8 = r10.substring(r8, r9)     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x017c }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x017c }
            r7.<init>(r8)     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x017c }
            java.lang.String r8 = ".amr"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x017c }
            java.lang.String r10 = r7.toString()     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x017c }
            int r7 = getFileSize(r10)     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x017c }
            if (r7 <= 0) goto L_0x00e9
            java.io.InputStream r1 = ua.netlizard.egypt.AndroidUtils.getResourceAsStream(r10)     // Catch:{ Exception -> 0x017f, OutOfMemoryError -> 0x017c }
        L_0x00e9:
            r3 = 1
            r2 = 0
        L_0x00eb:
            int r2 = r2 + 1
            if (r1 != 0) goto L_0x0120
            java.lang.String r7 = ".mid"
            boolean r7 = r10.endsWith(r7)     // Catch:{ Exception -> 0x017a, OutOfMemoryError -> 0x0178 }
            if (r7 != 0) goto L_0x0120
            r3 = 0
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017a, OutOfMemoryError -> 0x0178 }
            r8 = 0
            int r9 = r10.length()     // Catch:{ Exception -> 0x017a, OutOfMemoryError -> 0x0178 }
            int r9 = r9 + -4
            java.lang.String r8 = r10.substring(r8, r9)     // Catch:{ Exception -> 0x017a, OutOfMemoryError -> 0x0178 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x017a, OutOfMemoryError -> 0x0178 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x017a, OutOfMemoryError -> 0x0178 }
            java.lang.String r8 = ".mid"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x017a, OutOfMemoryError -> 0x0178 }
            java.lang.String r10 = r7.toString()     // Catch:{ Exception -> 0x017a, OutOfMemoryError -> 0x0178 }
            int r7 = getFileSize(r10)     // Catch:{ Exception -> 0x017a, OutOfMemoryError -> 0x0178 }
            if (r7 <= 0) goto L_0x0120
            java.io.InputStream r1 = ua.netlizard.egypt.AndroidUtils.getResourceAsStream(r10)     // Catch:{ Exception -> 0x017a, OutOfMemoryError -> 0x0178 }
        L_0x0120:
            java.lang.String r5 = "audio/midi"
            java.lang.String r7 = ".amr"
            boolean r7 = r10.endsWith(r7)     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            if (r7 == 0) goto L_0x012c
            java.lang.String r5 = "audio/amr"
        L_0x012c:
            java.lang.String r7 = ".wav"
            boolean r7 = r10.endsWith(r7)     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            if (r7 == 0) goto L_0x013a
            boolean r7 = ua.netlizard.egypt.Snd2.xwav_en     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            if (r7 == 0) goto L_0x0172
            java.lang.String r5 = "audio/x-wav"
        L_0x013a:
            java.lang.String r7 = ".mp3"
            boolean r7 = r10.endsWith(r7)     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            if (r7 == 0) goto L_0x0144
            java.lang.String r5 = "audio/mpeg"
        L_0x0144:
            ua.netlizard.egypt.Player r6 = ua.netlizard.egypt.Manager.createPlayer(r10, r5)     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            ua.netlizard.egypt.Player[] r7 = ua.netlizard.egypt.Snd2.player     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            r7 = r7[r11]     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            if (r7 == 0) goto L_0x0151
            soundClose(r11)     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
        L_0x0151:
            ua.netlizard.egypt.Player[] r7 = ua.netlizard.egypt.Snd2.player     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            r7[r11] = r6     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            java.lang.String[] r7 = ua.netlizard.egypt.Snd2.name_file     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            r7[r11] = r10     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            ua.netlizard.egypt.Player[] r7 = ua.netlizard.egypt.Snd2.player     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            r7 = r7[r11]     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            r7.realize()     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            boolean r7 = ua.netlizard.egypt.Snd2.motor     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            if (r7 != 0) goto L_0x016b
            ua.netlizard.egypt.Player[] r7 = ua.netlizard.egypt.Snd2.player     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            r7 = r7[r11]     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
            r7.prefetch()     // Catch:{ Exception -> 0x0175, OutOfMemoryError -> 0x0178 }
        L_0x016b:
            r3 = 0
        L_0x016c:
            if (r3 == 0) goto L_0x0171
            r7 = 3
            if (r2 <= r7) goto L_0x00eb
        L_0x0171:
            return
        L_0x0172:
            java.lang.String r5 = "audio/wav"
            goto L_0x013a
        L_0x0175:
            r0 = move-exception
            r1 = 0
            goto L_0x016c
        L_0x0178:
            r7 = move-exception
            goto L_0x016c
        L_0x017a:
            r7 = move-exception
            goto L_0x0120
        L_0x017c:
            r7 = move-exception
            goto L_0x00e9
        L_0x017f:
            r7 = move-exception
            goto L_0x00e9
        L_0x0182:
            r7 = move-exception
            goto L_0x00b7
        L_0x0185:
            r7 = move-exception
            goto L_0x0085
        L_0x0188:
            r7 = move-exception
            goto L_0x0053
        L_0x018b:
            r7 = move-exception
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: ua.netlizard.egypt.Snd2.createSound(java.lang.String, int):void");
    }

    public static void soundStop() {
        if (player != null) {
            int i = 0;
            while (i < player.length) {
                try {
                    soundStop(i);
                    i++;
                } catch (Exception e) {
                    return;
                }
            }
        }
    }

    public static void soundClose() {
        sound1 = -1;
        soundDefault = -1;
        if (runnable) {
            cikl_rn = false;
            try {
                Thread.sleep(33);
            } catch (Exception e) {
            }
            thread = null;
        }
        if (player != null) {
            int i = 0;
            while (i < player.length) {
                try {
                    soundClose(i);
                    i++;
                } catch (Exception e2) {
                }
            }
        }
        player = null;
    }

    public static final String sound(int i) {
        if (!snd2_created) {
            createSound();
        }
        if (player == null) {
            createSound();
        }
        if (i == podstilka2) {
            soundClose(i);
            createSound(i);
            podstilka2 = -1;
        }
        if (runnable) {
            sound1 = i;
        } else {
            sound1(i);
        }
        return name_file[i];
    }

    private static final void sound1(int i) {
        if (!snd2_created) {
            createSound();
        }
        if (player == null) {
            createSound();
        }
        soundStart(i, true);
    }

    private static final void soundStart(int i, boolean snd) {
        if (!snd2_created) {
            createSound();
        }
        if (!cpStr(soundPath, soundPathOld)) {
            try {
                soundStop();
                for (int j = 0; j < Game.kvo_zv; j++) {
                    soundClose(j);
                }
                createSound1();
            } catch (Exception e) {
            }
            soundPathOld = soundPath;
        }
        if ((!optimizeSounds && !stoppedClose) || !soundStarted() || i != soundOld) {
            if (prioritySounds && soundStarted() && i < soundOld && soundOld != Game.kvo_zv) {
                boolean ret = true;
                if (soundNprior != null) {
                    for (int i2 : soundNprior) {
                        if (i2 == soundOld) {
                            ret = false;
                        }
                    }
                }
                if (ret) {
                    return;
                }
            }
            soundStop();
            if (motor && i != soundOld) {
                soundDeallocate(soundOld);
            }
            soundPrefetch(i);
            try {
                player[i].setMediaTime(-1);
            } catch (Exception e2) {
            }
            if (snd) {
                int lpc = 1;
                try {
                    if (i == Game.kvo_zv) {
                        lpc = numberPlaysMusic;
                    }
                    if (i == Podstilka || i == podstilka2) {
                        lpc = numberPlaysPodstilka;
                    }
                    if (i == anymusic) {
                        lpc = numberPlaysAnymusic;
                        anymusic = -1;
                    }
                    player[i].setLoopCount(lpc);
                } catch (Exception e3) {
                }
            }
            try {
                Player player2 = player[i];
                ((VolumeControl) Player.getControl("VolumeControl")).setLevel(volume);
            } catch (Exception e4) {
            }
            if (snd) {
                try {
                    player[i].start();
                    name_file_last = name_file[i];
                    Log.d("PT", "sound start =" + i);
                } catch (Exception e5) {
                    return;
                }
            }
            soundOld = i;
        }
    }

    public static final void setVolume() {
        if (soundStarted()) {
            try {
                Player player2 = player[soundOld];
                ((VolumeControl) Player.getControl("VolumeControl")).setLevel(volume);
            } catch (Exception e) {
            }
        }
    }

    public static final boolean soundStarted() {
        try {
            return soundOld >= 0 && player[soundOld] != null && player[soundOld].getState() == 400;
        } catch (Exception e) {
            return false;
        }
    }

    public static final void soundOldStop() {
        if (soundOld >= 0) {
            soundStop(soundOld);
        }
    }

    public static final void soundClose(int i) {
        try {
            if (player[i].getState() == 400) {
                player[i].stop();
            }
            player[i].close();
        } catch (Exception e) {
        }
        player[i] = null;
    }

    public static void soundStop(int i) {
        if (i >= 0 && player[i] != null) {
            try {
                if (player[i].getState() == 400) {
                    Log.d("stopped =" + i, "");
                    player[i].stop();
                    if (stoppedClose) {
                        player[i].close();
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    public static final void soundPrefetch(int i) {
        try {
            if (player[i] == null) {
                createSound(i);
            }
            if (player[i] != null) {
                if (player[i].getState() == 0) {
                    createSound(i);
                }
                if (player[i].getState() == 100) {
                    player[i].realize();
                }
                if (player[i].getState() == 200) {
                    player[i].prefetch();
                }
            }
        } catch (Exception e) {
        }
    }

    public static final void soundDeallocate(int i) {
        try {
            if (player[i] != null && player[i].getState() == 300) {
                player[i].deallocate();
            }
        } catch (Exception e) {
        }
    }

    public static void vibro() {
        vibro(35, 110);
    }

    public static void vibro(int freq, int durat) {
        long tm = System.currentTimeMillis();
        if (tm >= vibroTimeNew) {
            try {
                Display.getDisplay(NET_Lizard.instance).vibrate(durat);
            } catch (Exception e) {
            }
            vibroTimeNew = ((long) durat) + tm + 7;
        }
    }

    static final String file_name_nr(String name) {
        boolean no_razshir = true;
        int nr = 0;
        int i = 0;
        while (i < name.length()) {
            try {
                if (name.charAt(i) == '.') {
                    no_razshir = false;
                    nr = i;
                }
                i++;
            } catch (Exception e) {
                return name;
            }
        }
        if (!no_razshir) {
            return name.substring(0, nr);
        }
        return name;
    }

    static final String file_name(String name) {
        try {
            if (name.length() <= 0) {
                name = new String(String.valueOf((char) 47) + name);
            } else if (name.charAt(0) != '/') {
                name = new String(String.valueOf((char) 47) + name);
            }
            boolean no_razshir = true;
            for (int i = 0; i < name.length(); i++) {
                if (name.charAt(i) == '.') {
                    no_razshir = false;
                }
            }
            if (no_razshir) {
                return String.valueOf(name) + int_to_str(p_mid);
            }
            return name;
        } catch (Exception e) {
            return name;
        }
    }

    public static final String int_to_str(int[] hh) {
        String str = new String();
        for (int i : hh) {
            long ss = (long) i;
            if (ss < 0) {
                ss += 4294967296L;
            }
            str = String.valueOf(str) + ((char) ((int) ((4278190080L & ss) >> 24)));
            byte s = (byte) ((int) ((16711680 & ss) >> 16));
            if (s == 0) {
                break;
            }
            str = String.valueOf(str) + ((char) s);
            byte s2 = (byte) ((int) ((65280 & ss) >> 8));
            if (s2 == 0) {
                break;
            }
            str = String.valueOf(str) + ((char) s2);
            byte s3 = (byte) ((int) (255 & ss));
            if (s3 == 0) {
                break;
            }
            str = String.valueOf(str) + ((char) s3);
        }
        return str;
    }
}
