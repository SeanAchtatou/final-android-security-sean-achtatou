package ua.netlizard.egypt;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import java.io.InputStream;

public class AndroidUtils {
    static final String TAG = "AU_DEBUG";
    static ResFolderList base = null;
    static final String base_name = "base";
    static final String basedata_name = "data";
    static ResFolderList basefolder = null;
    public static int drawHeight = 0;
    public static int drawWidth = 0;
    static AndroidUtils instance = null;
    static Context mContext = null;
    static ResFolderList nores = null;
    static final String nores_name = "nores";
    public static int realHeight = 0;
    public static int realWidth = 0;
    static ResFolderList res = null;
    static final String res_name = "res";
    static int res_type;
    public static int scale = 1;
    static String stream_path;

    class ResFolderList {
        int count = 0;
        int[] h;
        int idx = -1;
        String[] name;
        String name_mask;
        boolean valid = false;
        int[] w;

        ResFolderList() {
        }

        ResFolderList(String mask, String[] list) {
            scanFolders(mask, list);
            printInfo();
        }

        ResFolderList(String name2, String[] list, String[] list2) {
            scanFolders(name2, list, list2);
            printInfo();
        }

        /* access modifiers changed from: package-private */
        public final void printInfo() {
            Log.d(AndroidUtils.TAG, "============================================");
            Log.d(AndroidUtils.TAG, "name = " + this.name_mask);
            Log.d(AndroidUtils.TAG, "============================================");
            for (int i = 0; i < this.count; i++) {
                Log.d(AndroidUtils.TAG, "folder path = " + this.name[i]);
            }
            Log.d(AndroidUtils.TAG, "============================================");
        }

        private final void scanFolders(String bname, String[] list, String[] list2) {
            this.name_mask = bname;
            this.count = 0;
            this.w = null;
            this.h = null;
            int index = -1;
            int i = 0;
            while (true) {
                if (i >= list.length) {
                    break;
                } else if (list[i].equals(bname)) {
                    index = i;
                    break;
                } else {
                    i++;
                }
            }
            if (index >= 0) {
                this.valid = true;
                if (list2 != null && list2.length != 0) {
                    int n = 0;
                    for (int i2 = 0; i2 < list2.length; i2++) {
                        int xx = list2[i2].indexOf(120);
                        if (xx < 0) {
                            xx = list2[i2].indexOf(88);
                        }
                        int pw = -1;
                        int ph = -1;
                        if (xx < 0) {
                            try {
                                int pw2 = Integer.parseInt(list2[i2]);
                                n++;
                            } catch (Exception e) {
                            }
                        } else {
                            try {
                                pw = Integer.parseInt(list2[i2].substring(0, xx));
                            } catch (Exception e2) {
                            }
                            try {
                                ph = Integer.parseInt(list2[i2].substring(xx + 1, list2[i2].length()));
                            } catch (Exception e3) {
                            }
                            if (pw >= 0 || ph >= 0) {
                                n++;
                            }
                        }
                    }
                    if (n != 0) {
                        this.name_mask = String.valueOf(this.name_mask) + "/";
                        this.w = new int[n];
                        this.h = new int[n];
                        this.name = new String[n];
                        this.count = 0;
                        for (int i3 = 0; i3 < list2.length; i3++) {
                            int xx2 = list2[i3].indexOf(120);
                            if (xx2 < 0) {
                                xx2 = list2[i3].indexOf(88);
                            }
                            int pw3 = -1;
                            int ph2 = -1;
                            if (xx2 < 0) {
                                try {
                                    this.w[this.count] = Integer.parseInt(list2[i3]);
                                    this.h[this.count] = -1;
                                    this.name[this.count] = list2[i3];
                                    this.count++;
                                } catch (Exception e4) {
                                }
                            } else {
                                try {
                                    pw3 = Integer.parseInt(list2[i3].substring(0, xx2));
                                } catch (Exception e5) {
                                }
                                try {
                                    ph2 = Integer.parseInt(list2[i3].substring(xx2 + 1, list2[i3].length()));
                                } catch (Exception e6) {
                                }
                                this.w[this.count] = pw3;
                                this.h[this.count] = ph2;
                                this.name[this.count] = String.valueOf(this.name_mask) + list2[i3];
                                this.count++;
                            }
                        }
                        sort();
                    }
                }
            }
        }

        private final void scanFolders(String mask, String[] list) {
            boolean z = false;
            this.name_mask = mask;
            this.count = 0;
            this.w = null;
            this.h = null;
            int n = 0;
            for (String startsWith : list) {
                if (startsWith.startsWith(this.name_mask)) {
                    n++;
                }
            }
            this.w = new int[n];
            this.h = new int[n];
            this.name = new String[n];
            this.count = 0;
            for (int i = 0; i < list.length; i++) {
                if (list[i].startsWith(this.name_mask)) {
                    int pw = -1;
                    int ph = -1;
                    String folder = list[i];
                    int idx2 = folder.indexOf(120, this.name_mask.length());
                    if (idx2 < 0) {
                        idx2 = folder.indexOf(88, this.name_mask.length());
                    }
                    if (idx2 < 0) {
                        try {
                            pw = Integer.parseInt(folder.substring(this.name_mask.length()));
                        } catch (Exception e) {
                        }
                    } else {
                        try {
                            pw = Integer.parseInt(folder.substring(this.name_mask.length(), idx2));
                        } catch (Exception e2) {
                        }
                        try {
                            ph = Integer.parseInt(folder.substring(idx2 + 1, folder.length()));
                        } catch (Exception e3) {
                        }
                    }
                    this.w[this.count] = pw;
                    this.h[this.count] = ph;
                    this.name[this.count] = folder;
                    this.count++;
                }
            }
            sort();
            if (this.count > 0) {
                z = true;
            }
            this.valid = z;
        }

        private final void sort() {
            if (this.count >= 2) {
                for (int i = this.count - 2; i >= 0; i--) {
                    for (int n = 0; n < i; n++) {
                        boolean swap = false;
                        int tmp_w = this.w[n];
                        int tmp_h = this.h[n];
                        String tmp_n = this.name[n];
                        int tmp2_w = this.w[n + 1];
                        int tmp2_h = this.h[n + 1];
                        String tmp2_n = this.name[n + 1];
                        if (tmp_h >= 0 || tmp2_h >= 0) {
                            if (tmp_h < 0 && tmp2_h > 0) {
                                swap = true;
                            } else if (tmp_h > 0 && tmp2_h < 0) {
                                swap = false;
                            } else if (tmp_w > tmp2_w || (tmp_w == tmp2_w && tmp_h > tmp2_h)) {
                                swap = true;
                            }
                        } else if (tmp_w > tmp2_w) {
                            swap = true;
                        }
                        if (swap) {
                            this.w[n + 1] = tmp_w;
                            this.h[n + 1] = tmp_h;
                            this.name[n + 1] = tmp_n;
                            this.w[n] = tmp2_w;
                            this.h[n] = tmp2_h;
                            this.name[n] = tmp2_n;
                        }
                    }
                }
            }
        }

        public final void selectIndex1(int sw, int sh) {
            int index1 = -1;
            int index2 = -1;
            int index3 = -1;
            for (int i = 0; i < this.count; i++) {
                index1 = i;
                if (sw < this.w[i]) {
                    break;
                }
            }
            for (int i2 = 0; i2 < this.count; i2++) {
                index2 = i2;
                if (sh < this.h[i2]) {
                    break;
                }
            }
            int i3 = 0;
            while (true) {
                if (i3 < this.count) {
                    if (sw == this.w[i3] && sh == this.h[i3]) {
                        index3 = i3;
                        break;
                    }
                    i3++;
                } else {
                    break;
                }
            }
            if (index3 != -1) {
                this.idx = index3;
            } else if (index1 >= 0 && index2 >= 0 && index1 != index2 && this.w[index1] == this.w[index2]) {
                this.idx = index2;
            } else if (index1 == index2 || index1 != -1) {
                this.idx = index1;
            } else {
                this.idx = 0;
            }
        }

        public final void selectIndexBySpace(int sw, int sh) {
            int[] weights = new int[this.count];
            for (int i = 0; i < this.count; i++) {
                weights[i] = Math.abs(sw - this.w[i]) + Math.abs(sh - this.h[i]);
            }
            int min = weights[0];
            int index = 0;
            for (int i2 = 1; i2 < this.count; i2++) {
                if (min > weights[i2]) {
                    min = weights[i2];
                    index = i2;
                }
            }
            this.idx = index;
        }

        public final void selectIndexBySpaceSteve(int sw, int sh, boolean base) {
            int[] weights = new int[this.count];
            int S = sw * sh;
            int kwh = (sw * 256) / sh;
            for (int i = 0; i < this.count; i++) {
                int dw = (Math.abs(sw - this.w[i]) * 256) / sw;
                int dh = (Math.abs(sh - this.h[i]) * 256) / sh;
                int Si = this.w[i] * this.h[i];
                int dkwh = Math.abs(kwh - ((this.w[i] * 256) / this.h[i]));
                int dS = (Math.abs(S - Si) * 256) / S;
                if (base) {
                    weights[i] = dS;
                } else {
                    weights[i] = dw + dh + dkwh;
                }
            }
            int min = weights[0];
            int index = 0;
            for (int i2 = 1; i2 < this.count; i2++) {
                if (min > weights[i2]) {
                    min = weights[i2];
                    index = i2;
                }
            }
            this.idx = index;
        }

        public final void selectIndexBySpaceTest(int sw, int sh) {
            selectIndexBySpaceSteve(sw, sh, true);
            Log.d(AndroidUtils.TAG, "index = " + this.idx + " - w = " + this.w[this.idx] + " h = " + this.h[this.idx] + "    res w = " + sw + "  h = " + sh);
        }

        public final String getPath() {
            if (this.count == 0 && this.name_mask != null) {
                return this.name_mask;
            }
            if (this.count == 1 || this.idx < 0) {
                return this.name[0];
            }
            return this.name[this.idx];
        }
    }

    AndroidUtils(Context c, int w, int h) {
        instance = this;
        mContext = c;
        realWidth = w;
        realHeight = h;
        drawWidth = w;
        drawHeight = h;
        try {
            String[] base_folders = mContext.getResources().getAssets().list("");
            basefolder = new ResFolderList(basedata_name, base_folders, mContext.getResources().getAssets().list(basedata_name));
            if (!basefolder.valid) {
                basefolder = null;
            }
            if (basefolder != null) {
                basefolder.idx = 0;
            }
            try {
                base = new ResFolderList(base_name, base_folders, mContext.getResources().getAssets().list(base_name));
                if (!base.valid) {
                    base = null;
                }
                if (base != null) {
                    base.selectIndexBySpaceSteve(GameView.screenWidth, GameView.screenHeight, true);
                    scale = calcScale(base.w[base.idx], base.h[base.idx]);
                }
            } catch (Exception e) {
            }
            drawWidth = realWidth / scale;
            drawHeight = realHeight / scale;
            try {
                res = new ResFolderList(res_name, base_folders, mContext.getResources().getAssets().list(res_name));
                if (!res.valid) {
                    res = null;
                }
                if (res != null) {
                    res.selectIndexBySpaceSteve(drawWidth, drawHeight, false);
                }
            } catch (Exception e2) {
            }
            try {
                nores = new ResFolderList(nores_name, base_folders, mContext.getResources().getAssets().list(nores_name));
                if (!nores.valid) {
                    nores = null;
                }
                if (nores != null) {
                    nores.selectIndexBySpaceSteve(drawWidth, drawHeight, false);
                }
            } catch (Exception e3) {
            }
        } catch (Exception e4) {
        }
        res_type = -1;
        stream_path = null;
    }

    /* access modifiers changed from: package-private */
    public final int calcScale(int w, int h) {
        float fw = ((float) GameView.screenWidth) / ((float) w);
        float fh = ((float) GameView.screenHeight) / ((float) h);
        if (GameView.screenWidth <= GameView.screenHeight) {
            if (fw > 1.6f) {
                return Math.round(fw);
            }
        } else if (fh > 1.6f) {
            return Math.round(fh);
        }
        return 1;
    }

    public static InputStream getResourceAsStream(String name) {
        try {
            AssetManager am = mContext.getResources().getAssets();
            InputStream in = null;
            if (0 == 0) {
                try {
                    if (res != null) {
                        res_type = 2;
                        stream_path = res.getPath();
                        in = am.open(String.valueOf(stream_path) + name);
                    }
                } catch (Exception e) {
                    in = null;
                }
            }
            if (in == null) {
                try {
                    if (nores != null) {
                        res_type = 3;
                        stream_path = nores.getPath();
                        in = am.open(String.valueOf(stream_path) + name);
                    }
                } catch (Exception e2) {
                    in = null;
                }
            }
            if (in == null) {
                try {
                    if (base != null) {
                        res_type = 1;
                        stream_path = base.getPath();
                        in = am.open(String.valueOf(stream_path) + name);
                    }
                } catch (Exception e3) {
                    in = null;
                }
            }
            if (in != null) {
                return in;
            }
            try {
                if (basefolder == null) {
                    return in;
                }
                res_type = 0;
                stream_path = basefolder.getPath();
                return am.open(String.valueOf(stream_path) + name);
            } catch (Exception e4) {
                return null;
            }
        } catch (Exception e5) {
            return null;
        }
    }

    public static byte[] getResourceAsArray(String name) {
        try {
            InputStream in = getResourceAsStream(name);
            int size = 0;
            while (in.read() != -1) {
                size++;
            }
            in.close();
            InputStream in2 = getResourceAsStream(name);
            byte[] data = new byte[size];
            in2.read(data);
            in2.close();
            return data;
        } catch (Exception e) {
            return null;
        }
    }
}
