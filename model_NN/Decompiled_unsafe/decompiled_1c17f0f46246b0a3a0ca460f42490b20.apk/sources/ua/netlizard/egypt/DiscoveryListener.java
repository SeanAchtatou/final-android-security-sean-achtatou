package ua.netlizard.egypt;

public interface DiscoveryListener {
    public static final int INQUIRY_COMPLETED = 0;
    public static final int INQUIRY_ERROR = 1;
    public static final int INQUIRY_TERMINATED = 2;
    public static final int SERVICE_SEARCH_COMPLETED = 3;
    public static final int SERVICE_SEARCH_DEVICE_NOT_REACHABLE = 4;
    public static final int SERVICE_SEARCH_ERROR = 5;
    public static final int SERVICE_SEARCH_NO_RECORDS = 6;
    public static final int SERVICE_SEARCH_TERMINATED = 7;

    void deviceDiscovered(RemoteDevice remoteDevice, DeviceClass deviceClass);

    void inquiryCompleted(int i);

    void serviceSearchCompleted(int i, int i2);

    void servicesDiscovered(int i, ServiceRecord[] serviceRecordArr);
}
