package ua.netlizard.egypt;

public class LocalDevice {
    LocalDevice() {
    }

    /* access modifiers changed from: package-private */
    public String getBluetoothAddress() {
        return "null";
    }

    /* access modifiers changed from: package-private */
    public DeviceClass getDeviceClass() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public int getDiscoverable() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public DiscoveryAgent getDiscoveryAgent() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public String getFriendlyName() {
        return "getFriendlyName";
    }

    static LocalDevice getLocalDevice() {
        return null;
    }

    static String getProperty(String property) {
        if (property.equals("bluetooth.connected.devices.max")) {
            return "1";
        }
        if (property.equals("bluetooth.l2cap.receiveMTU.max")) {
            return "128";
        }
        return "null";
    }

    /* access modifiers changed from: package-private */
    public ServiceRecord getRecord(Connection notifier) {
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean setDiscoverable(int mode) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void updateRecord(ServiceRecord srvRecord) {
    }
}
