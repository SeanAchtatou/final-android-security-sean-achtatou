package ua.netlizard.egypt;

public final class poly {
    static final int Height = Game.Height;
    static final int Height_2_3D = (((Height - 60) >> 1) << 16);
    static final int Height_2_real_3D = ((Height - 60) >> 1);
    static final int Height_real_3D = (Height - 60);
    static final int Width = Game.Width;
    static final int Width_2_3D = (((Width + 0) >> 1) << 16);
    static final int Width_2_real_3D = ((Width + 0) >> 1);
    static final int Width_real_3D = (Width + 0);
    static int all_poly = 0;
    static final byte cof_H = 30;
    static final byte cof_W = 0;
    static final int[] coloR;
    static short dark_c = 0;
    static final int max_xyz = (Width_real_3D * 140);
    static int obzor = ok_obzor;
    static int obzor_2 = (obzor << 16);
    static final int ok_obzor = Width_real_3D;
    static final int size_tex = 6;
    static final int size_tex_unit = 7;
    static final int size_text_full = 4095;
    static final int size_text_unit_full = 16383;
    static final int size_texture = 63;
    static final int size_texture_unit = 127;
    static final int stp = 16;
    static final byte stp_3d = 4;
    static final byte stp_3d_one = 16;
    static final int stp_draw = 18;
    static final int stp_draw_one = 262144;
    static final int stp_one = 65536;

    static {
        int[] iArr = new int[8];
        iArr[1] = 16777215;
        iArr[2] = 4522142;
        iArr[3] = 13122096;
        iArr[4] = 394793;
        iArr[5] = 3836671;
        iArr[6] = 9356543;
        iArr[7] = 16766976;
        coloR = iArr;
    }

    static final void draw_spr(int n, int x, int y, int z, int[][] planes_) {
        int[] display1 = m3d.display;
        byte[] texture1 = m3d.texture_object[n];
        int[] p = m3d.palitra_object;
        int[] distance1 = m3d.distance_H;
        point_ a = new point_();
        point_ b = new point_();
        a.x = x;
        a.y = y;
        a.z = z;
        math.xRotateVertex(a);
        if (a.rz > 0) {
            short s = m3d.cu[n][2];
            short s2 = m3d.cu[n][3];
            b.rx = a.rx + (m3d.cu[n][4] << 16);
            b.ry = a.ry + (m3d.cu[n][5] << 16);
            b.rz = a.rz;
            a.sx = (short) ((int) (((long) Width_2_real_3D) + ((((long) a.rx) * ((long) obzor)) / ((long) a.rz))));
            a.sy = (short) ((int) (((long) Height_2_real_3D) - ((((long) a.ry) * ((long) obzor)) / ((long) a.rz))));
            b.sx = (short) ((int) (((long) Width_2_real_3D) + ((((long) b.rx) * ((long) obzor)) / ((long) a.rz))));
            b.sy = (short) ((int) (((long) Height_2_real_3D) - ((((long) b.ry) * ((long) obzor)) / ((long) a.rz))));
            int start_u = 0;
            int start_v = 0;
            int size_y = Math.abs((a.sy - b.sy) << 1);
            int size_x = Math.abs((a.sx - b.sx) << 1);
            if (max_xyz > size_x && max_xyz > size_y) {
                int cof_w = s * distance1[size_x];
                int cof_h = s2 * distance1[size_y];
                int start_x = b.sx - size_x;
                int start_y = a.sy - size_y;
                if (n >= 0 && n <= 6) {
                    start_y += size_y >> 1;
                }
                int end_x = start_x + size_x;
                int end_y = start_y + size_y;
                if (start_x <= planes_[4][2] + Width_2_real_3D && start_y <= planes_[2][2] + Height_2_real_3D && end_x >= Width_2_real_3D - planes_[3][2] && end_y >= Height_2_real_3D - planes_[1][2]) {
                    if (start_x <= Width_2_real_3D - planes_[3][2]) {
                        start_u = cof_w * ((Width_2_real_3D - planes_[3][2]) - start_x);
                        start_x = Width_2_real_3D - planes_[3][2];
                    }
                    if (start_y <= Height_2_real_3D - planes_[1][2]) {
                        start_v = cof_h * ((Height_2_real_3D - planes_[1][2]) - start_y);
                        start_y = Height_2_real_3D - planes_[1][2];
                    }
                    if (end_x >= planes_[4][2] + Width_2_real_3D) {
                        end_x = (planes_[4][2] + Width_2_real_3D) - 2;
                    }
                    if (end_y >= planes_[2][2] + Height_2_real_3D) {
                        end_y = (planes_[2][2] + Height_2_real_3D) - 3;
                    }
                    int cof_y = start_v;
                    int wd = (Width_real_3D * start_y) + start_x;
                    int len1 = end_y - start_y;
                    int x_len = end_x - start_x;
                    while (true) {
                        int len12 = len1;
                        len1 = len12 - 1;
                        if (len12 > 0) {
                            wd += Width_real_3D;
                            int len = x_len;
                            int dd = wd;
                            int cof_x = start_u + (((cof_y >> stp_draw) * s) << stp_draw);
                            while (true) {
                                int len2 = len;
                                len = len2 - 1;
                                if (len2 <= 0) {
                                    break;
                                }
                                int jj = p[texture1[cof_x >> stp_draw]];
                                dd++;
                                if (jj != 0) {
                                    int n2 = display1[dd];
                                    display1[dd] = ((((16711680 & n2) + (16711680 & jj)) >> 17) << 16) + ((((65280 & n2) + (65280 & jj)) >> 9) << 8) + (((n2 & 255) + (jj & 255)) >> 1);
                                }
                                cof_x += cof_w;
                            }
                            cof_y += cof_h;
                        } else {
                            return;
                        }
                    }
                }
            }
        }
    }

    static final void draw_unit(int j, int[][] planes_, boolean flip) {
    }

    static final void draw_poly_2_unit(point_ a, point_ b, point_ c) {
        all_poly++;
        int[] distance1 = m3d.coof_clip;
        int obz = obzor_2;
        a.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) a.rx) * ((long) obz)) / ((long) a.rz))) >> 16));
        a.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) a.ry) * ((long) obz)) / ((long) a.rz))) >> 16));
        b.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) b.rx) * ((long) obz)) / ((long) b.rz))) >> 16));
        b.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) b.ry) * ((long) obz)) / ((long) b.rz))) >> 16));
        c.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) c.rx) * ((long) obz)) / ((long) c.rz))) >> 16));
        c.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) c.ry) * ((long) obz)) / ((long) c.rz))) >> 16));
        if (a.sy > b.sy) {
            point_ tmp_vertex = a;
            a = b;
            b = tmp_vertex;
        }
        if (a.sy > c.sy) {
            point_ tmp_vertex2 = a;
            a = c;
            c = tmp_vertex2;
        }
        if (b.sy > c.sy) {
            point_ tmp_vertex3 = b;
            b = c;
            c = tmp_vertex3;
        }
        int k = (b.sy - a.sy) * distance1[c.sy - a.sy];
        int dd = (a.sx + (((c.sx - a.sx) * k) >> 16)) - b.sx;
        if (dd < 0) {
            draw_affine_unit(a, b, c, (-((a.u + (((c.u - a.u) * k) >> 16)) - b.u)) * distance1[-dd], (-((a.v + (((c.v - a.v) * k) >> 16)) - b.v)) * distance1[-dd]);
        } else {
            draw_affine_unit(a, b, c, ((a.u + (((c.u - a.u) * k) >> 16)) - b.u) * distance1[dd], ((a.v + (((c.v - a.v) * k) >> 16)) - b.v) * distance1[dd]);
        }
    }

    static final void draw_affine_unit(point_ a, point_ b, point_ c, int du_dsx, int dv_dsx) {
        int sy;
        byte[] texture1 = m3d.texture[m3d.ind];
        int[] pp = m3d.palitra[m3d.ind];
        int[] display1 = m3d.display;
        int[] distance = m3d.coof_clip;
        int cof_width = a.sy * Width_real_3D;
        int ca = a.sx << 16;
        int cb = b.sx << 16;
        int ba = a.sx << 16;
        int u_ca = a.u << 16;
        int u_cb = b.u << 16;
        int u_ba = a.u << 16;
        int v_ca = a.v << 16;
        int v_cb = b.v << 16;
        int v_ba = a.v << 16;
        int cof_ca = (c.sx - a.sx) * distance[c.sy - a.sy];
        int cof_ba = (b.sx - a.sx) * distance[b.sy - a.sy];
        int cof_cb = (c.sx - b.sx) * distance[c.sy - b.sy];
        int u_cof_ca = (c.u - a.u) * distance[c.sy - a.sy];
        int u_cof_ba = (b.u - a.u) * distance[b.sy - a.sy];
        int u_cof_cb = (c.u - b.u) * distance[c.sy - b.sy];
        int v_cof_ca = (c.v - a.v) * distance[c.sy - a.sy];
        int v_cof_ba = (b.v - a.v) * distance[b.sy - a.sy];
        int v_cof_cb = (c.v - b.v) * distance[c.sy - b.sy];
        if (cof_ca < cof_ba) {
            sy = a.sy;
            while (sy <= b.sy) {
                int u_start = u_ca;
                int v_start = v_ca;
                int dd = cof_width + (ca >> 16);
                int len = cecil(ba - ca);
                while (true) {
                    int dd2 = dd;
                    len--;
                    if (len < 0) {
                        break;
                    }
                    dd = dd2 + 1;
                    v_start += dv_dsx;
                    u_start += du_dsx;
                    display1[dd2] = pp[texture1[(((v_start >> 16) << 7) + (u_start >> 16)) & size_text_unit_full]];
                }
                ca += cof_ca;
                ba += cof_ba;
                u_ca += u_cof_ca;
                v_ca += v_cof_ca;
                cof_width += Width_real_3D;
                sy++;
            }
        } else {
            int sy2 = a.sy;
            while (sy <= b.sy) {
                int u_end = u_ba;
                int v_end = v_ba;
                int dd3 = cof_width + (ba >> 16);
                int len2 = cecil(ca - ba);
                while (true) {
                    int dd4 = dd3;
                    len2--;
                    if (len2 < 0) {
                        break;
                    }
                    dd3 = dd4 + 1;
                    v_end += dv_dsx;
                    u_end += du_dsx;
                    display1[dd4] = pp[texture1[(((v_end >> 16) << 7) + (u_end >> 16)) & size_text_unit_full]];
                }
                ca += cof_ca;
                ba += cof_ba;
                u_ba += u_cof_ba;
                v_ba += v_cof_ba;
                cof_width += Width_real_3D;
                sy2 = sy + 1;
            }
        }
        if (cof_ca + ca < cof_cb + cb) {
            while (sy < c.sy) {
                int u_start2 = u_ca;
                int v_start2 = v_ca;
                int dd5 = cof_width + (ca >> 16);
                int len3 = cecil(cb - ca);
                while (true) {
                    int dd6 = dd5;
                    len3--;
                    if (len3 < 0) {
                        break;
                    }
                    dd5 = dd6 + 1;
                    v_start2 += dv_dsx;
                    u_start2 += du_dsx;
                    display1[dd6] = pp[texture1[(((v_start2 >> 16) << 7) + (u_start2 >> 16)) & size_text_unit_full]];
                }
                ca += cof_ca;
                cb += cof_cb;
                u_ca += u_cof_ca;
                v_ca += v_cof_ca;
                cof_width += Width_real_3D;
                sy++;
            }
            return;
        }
        while (sy < c.sy) {
            int u_end2 = u_cb;
            int v_end2 = v_cb;
            int dd7 = cof_width + (cb >> 16);
            int len4 = cecil(ca - cb);
            while (true) {
                int dd8 = dd7;
                len4--;
                if (len4 < 0) {
                    break;
                }
                dd7 = dd8 + 1;
                v_end2 += dv_dsx;
                u_end2 += du_dsx;
                display1[dd8] = pp[texture1[(((v_end2 >> 16) << 7) + (u_end2 >> 16)) & size_text_unit_full]];
            }
            ca += cof_ca;
            cb += cof_cb;
            u_cb += u_cof_cb;
            v_cb += v_cof_cb;
            cof_width += Width_real_3D;
            sy++;
        }
    }

    static final void draw_poly_2(point_ a, point_ b, point_ c) {
        int[] distance1 = m3d.coof_clip;
        int obz = obzor_2;
        a.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) a.rx) * ((long) obz)) / ((long) a.rz))) >> 16));
        a.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) a.ry) * ((long) obz)) / ((long) a.rz))) >> 16));
        b.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) b.rx) * ((long) obz)) / ((long) b.rz))) >> 16));
        b.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) b.ry) * ((long) obz)) / ((long) b.rz))) >> 16));
        c.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) c.rx) * ((long) obz)) / ((long) c.rz))) >> 16));
        c.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) c.ry) * ((long) obz)) / ((long) c.rz))) >> 16));
        if (a.sy > b.sy) {
            point_ tmp_vertex = a;
            a = b;
            b = tmp_vertex;
        }
        if (a.sy > c.sy) {
            point_ tmp_vertex2 = a;
            a = c;
            c = tmp_vertex2;
        }
        if (b.sy > c.sy) {
            point_ tmp_vertex3 = b;
            b = c;
            c = tmp_vertex3;
        }
        int k = (b.sy - a.sy) * distance1[c.sy - a.sy];
        int dd = (a.sx + (((c.sx - a.sx) * k) >> 16)) - b.sx;
        if (dd < 0) {
            draw_affine(a, b, c, (-((a.u + (((c.u - a.u) * k) >> 16)) - b.u)) * distance1[-dd], (-((a.v + (((c.v - a.v) * k) >> 16)) - b.v)) * distance1[-dd]);
        } else {
            draw_affine(a, b, c, ((a.u + (((c.u - a.u) * k) >> 16)) - b.u) * distance1[dd], ((a.v + (((c.v - a.v) * k) >> 16)) - b.v) * distance1[dd]);
        }
    }

    private static final int cecil(int x) {
        int k = (x >> 16) << 16;
        if (k < x) {
            return (k >> 16) + 1;
        }
        return k >> 16;
    }

    static final void draw_affine(point_ a, point_ b, point_ c, int du_dsx, int dv_dsx) {
        int sy;
        byte[] texture1 = m3d.texture[m3d.ind];
        int[] pp = m3d.palitra[m3d.ind];
        int[] display1 = m3d.display;
        int[] distance = m3d.coof_clip;
        int cof_width = a.sy * Width_real_3D;
        int ca = a.sx << 16;
        int cb = b.sx << 16;
        int ba = a.sx << 16;
        int u_ca = a.u << 16;
        int u_cb = b.u << 16;
        int u_ba = a.u << 16;
        int v_ca = a.v << 16;
        int v_cb = b.v << 16;
        int v_ba = a.v << 16;
        int cof_ca = (c.sx - a.sx) * distance[c.sy - a.sy];
        int cof_ba = (b.sx - a.sx) * distance[b.sy - a.sy];
        int cof_cb = (c.sx - b.sx) * distance[c.sy - b.sy];
        int u_cof_ca = (c.u - a.u) * distance[c.sy - a.sy];
        int u_cof_ba = (b.u - a.u) * distance[b.sy - a.sy];
        int u_cof_cb = (c.u - b.u) * distance[c.sy - b.sy];
        int v_cof_ca = (c.v - a.v) * distance[c.sy - a.sy];
        int v_cof_ba = (b.v - a.v) * distance[b.sy - a.sy];
        int v_cof_cb = (c.v - b.v) * distance[c.sy - b.sy];
        if (cof_ca < cof_ba) {
            sy = a.sy;
            while (sy <= b.sy) {
                int u_start = u_ca;
                int v_start = v_ca;
                int dd = cof_width + (ca >> 16);
                int len = cecil(ba - ca);
                while (true) {
                    int dd2 = dd;
                    len--;
                    if (len < 0) {
                        break;
                    }
                    dd = dd2 + 1;
                    v_start += dv_dsx;
                    u_start += du_dsx;
                    display1[dd2] = pp[texture1[(((v_start >> 16) << 6) + (u_start >> 16)) & size_text_full]];
                }
                ca += cof_ca;
                ba += cof_ba;
                u_ca += u_cof_ca;
                v_ca += v_cof_ca;
                cof_width += Width_real_3D;
                sy++;
            }
        } else {
            int sy2 = a.sy;
            while (sy <= b.sy) {
                int u_end = u_ba;
                int v_end = v_ba;
                int dd3 = cof_width + (ba >> 16);
                int len2 = cecil(ca - ba);
                while (true) {
                    int dd4 = dd3;
                    len2--;
                    if (len2 < 0) {
                        break;
                    }
                    dd3 = dd4 + 1;
                    v_end += dv_dsx;
                    u_end += du_dsx;
                    display1[dd4] = pp[texture1[(((v_end >> 16) << 6) + (u_end >> 16)) & size_text_full]];
                }
                ca += cof_ca;
                ba += cof_ba;
                u_ba += u_cof_ba;
                v_ba += v_cof_ba;
                cof_width += Width_real_3D;
                sy2 = sy + 1;
            }
        }
        if (cof_ca + ca < cof_cb + cb) {
            while (sy < c.sy) {
                int u_start2 = u_ca;
                int v_start2 = v_ca;
                int dd5 = cof_width + (ca >> 16);
                int len3 = cecil(cb - ca);
                while (true) {
                    int dd6 = dd5;
                    len3--;
                    if (len3 < 0) {
                        break;
                    }
                    dd5 = dd6 + 1;
                    v_start2 += dv_dsx;
                    u_start2 += du_dsx;
                    display1[dd6] = pp[texture1[(((v_start2 >> 16) << 6) + (u_start2 >> 16)) & size_text_full]];
                }
                ca += cof_ca;
                cb += cof_cb;
                u_ca += u_cof_ca;
                v_ca += v_cof_ca;
                cof_width += Width_real_3D;
                sy++;
            }
            return;
        }
        while (sy < c.sy) {
            int u_end2 = u_cb;
            int v_end2 = v_cb;
            int dd7 = cof_width + (cb >> 16);
            int len4 = cecil(ca - cb);
            while (true) {
                int dd8 = dd7;
                len4--;
                if (len4 < 0) {
                    break;
                }
                dd7 = dd8 + 1;
                v_end2 += dv_dsx;
                u_end2 += du_dsx;
                display1[dd8] = pp[texture1[(((v_end2 >> 16) << 6) + (u_end2 >> 16)) & size_text_full]];
            }
            ca += cof_ca;
            cb += cof_cb;
            u_cb += u_cof_cb;
            v_cb += v_cof_cb;
            cof_width += Width_real_3D;
            sy++;
        }
    }

    static final void draw_shot(int[] sho, int[][] planes_) {
        int color = sho[11];
        int size = ((sho[10] & 255) << 16) >> 1;
        int rx = (int) (((((long) (m3d.s[0] - sho[0])) * ((long) math.cos[m3d.rot_z])) - (((long) (m3d.s[1] - sho[1])) * ((long) math.sin[m3d.rot_z]))) >> 16);
        int ry = (int) (((((long) (m3d.s[0] - sho[0])) * ((long) math.sin[m3d.rot_z])) + (((long) (m3d.s[1] - sho[1])) * ((long) math.cos[m3d.rot_z]))) >> 16);
        int rz = (int) (((((long) (m3d.s[2] - sho[2])) * ((long) math.cos[m3d.rot_x])) - (((long) ry) * ((long) math.sin[m3d.rot_x]))) >> 16);
        if (rz > 0) {
            int ry2 = (int) (((((long) (m3d.s[2] - sho[2])) * ((long) math.sin[m3d.rot_x])) + (((long) ry) * ((long) math.cos[m3d.rot_x]))) >> 16);
            int[] display1 = m3d.display;
            int X1 = (short) ((int) (((long) Width_2_real_3D) + ((((long) rx) * ((long) obzor)) / ((long) rz))));
            int Y1 = (short) ((int) (((long) Height_2_real_3D) - ((((long) ry2) * ((long) obzor)) / ((long) rz))));
            int size_y = sho[10] & 255;
            int size_x = sho[10] & 255;
            int start_x = X1 - (size_x >> 1);
            int start_y = Y1 - (size_y >> 1);
            int end_x = start_x + size_x;
            int end_y = start_y + size_y;
            if (start_x <= planes_[4][2] + Width_2_real_3D && start_y <= planes_[2][2] + Height_2_real_3D && end_x >= Width_2_real_3D - planes_[3][2] && end_y >= Height_2_real_3D - planes_[1][2]) {
                if (start_x <= Width_2_real_3D - planes_[3][2]) {
                    start_x = Width_2_real_3D - planes_[3][2];
                }
                if (start_y <= Height_2_real_3D - planes_[1][2]) {
                    start_y = Height_2_real_3D - planes_[1][2];
                }
                if (end_x >= planes_[4][2] + Width_2_real_3D) {
                    end_x = (planes_[4][2] + Width_2_real_3D) - 2;
                }
                if (end_y >= planes_[2][2] + Height_2_real_3D) {
                    end_y = (planes_[2][2] + Height_2_real_3D) - 3;
                }
                int X12 = end_y - start_y;
                int rx2 = end_x - start_x;
                int Y2 = (Width_real_3D * start_y) + start_x;
                while (true) {
                    int X13 = X12;
                    X12 = X13 - 1;
                    if (X13 > 0) {
                        Y2 += Width_real_3D;
                        int Y12 = rx2;
                        int X2 = Y2;
                        while (true) {
                            int X22 = X2;
                            int Y13 = Y12;
                            Y12 = Y13 - 1;
                            if (Y13 <= 0) {
                                break;
                            }
                            X2 = X22 + 1;
                            display1[X22] = color;
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public static final void draw_my_point(int x, int y, int z) {
        int rx = (int) (((((long) (m3d.s[0] - x)) * ((long) math.cos[m3d.rot_z])) - (((long) (m3d.s[1] - y)) * ((long) math.sin[m3d.rot_z]))) >> 16);
        int ry = (int) (((((long) (m3d.s[0] - x)) * ((long) math.sin[m3d.rot_z])) + (((long) (m3d.s[1] - y)) * ((long) math.cos[m3d.rot_z]))) >> 16);
        int rz = (int) (((((long) (m3d.s[2] - z)) * ((long) math.cos[m3d.rot_x])) - (((long) ry) * ((long) math.sin[m3d.rot_x]))) >> 16);
        if (rz > 0) {
            int ry2 = (int) (((((long) (m3d.s[2] - z)) * ((long) math.sin[m3d.rot_x])) + (((long) ry) * ((long) math.cos[m3d.rot_x]))) >> 16);
            int X = (short) ((int) ((((long) Width_2_3D) + ((((long) rx) * ((long) obzor_2)) / ((long) rz))) >> 16));
            int Y = (short) ((int) ((((long) Height_2_3D) - ((((long) ry2) * ((long) obzor_2)) / ((long) rz))) >> 16));
            if (X >= 0 && Y >= 0 && X < Width_real_3D && Y < Height_real_3D) {
                m3d.display[(Width_real_3D * Y) + X] = 65280;
            }
        }
    }

    static final void draw_poly_fill(point_ a, point_ b, point_ c, boolean half) {
        int x2;
        int x22;
        int[] distance = m3d.coof_clip;
        int obz = obzor_2;
        int color = coloR[m3d.ind - 50];
        a.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) a.rx) * ((long) obz)) / ((long) a.rz))) >> 16));
        a.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) a.ry) * ((long) obz)) / ((long) a.rz))) >> 16));
        b.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) b.rx) * ((long) obz)) / ((long) b.rz))) >> 16));
        b.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) b.ry) * ((long) obz)) / ((long) b.rz))) >> 16));
        c.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) c.rx) * ((long) obz)) / ((long) c.rz))) >> 16));
        c.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) c.ry) * ((long) obz)) / ((long) c.rz))) >> 16));
        if (a.sy > b.sy) {
            point_ tmp_vertex = a;
            a = b;
            b = tmp_vertex;
        }
        if (a.sy > c.sy) {
            point_ tmp_vertex2 = a;
            a = c;
            c = tmp_vertex2;
        }
        if (b.sy > c.sy) {
            point_ tmp_vertex3 = b;
            b = c;
            c = tmp_vertex3;
        }
        int[] display1 = m3d.display;
        int A = 0;
        int B = (c.sx - a.sx) * distance[c.sy - a.sy];
        int C = (c.sx - b.sx) * distance[c.sy - b.sy];
        int D = C * (a.sy - b.sy);
        int G = (b.sx - a.sx) * distance[b.sy - a.sy];
        int E = 0;
        int alfa_all = m3d.alfa_;
        if (!half) {
            for (int sy = a.sy; sy < c.sy; sy++) {
                int x1 = a.sx + (A >> 16);
                if (sy >= b.sy) {
                    x22 = b.sx + (D >> 16);
                } else {
                    x22 = a.sx + (E >> 16);
                }
                A += B;
                D += C;
                E += G;
                if (x1 > x22) {
                    int dd = x1;
                    x1 = x22;
                    x22 = dd;
                }
                int dd2 = (Width_real_3D * sy) + x1;
                int i = x1;
                while (true) {
                    int dd3 = dd2;
                    if (i >= x22) {
                        break;
                    }
                    dd2 = dd3 + 1;
                    display1[dd3] = color;
                    i++;
                }
            }
            return;
        }
        int sy2 = (alfa_all * 255) >> 8;
        int obz2 = (sy2 << 16) | (sy2 << 8) | sy2;
        for (int sy3 = a.sy; sy3 < c.sy; sy3++) {
            int x12 = a.sx + (A >> 16);
            if (sy3 >= b.sy) {
                x2 = b.sx + (D >> 16);
            } else {
                x2 = a.sx + (E >> 16);
            }
            A += B;
            D += C;
            E += G;
            if (x12 > x2) {
                int dd4 = x12;
                x12 = x2;
                x2 = dd4;
            }
            int dd5 = (Width_real_3D * sy3) + x12;
            int i2 = x12;
            while (true) {
                int dd6 = dd5;
                if (i2 >= x2) {
                    break;
                }
                dd5 = dd6 + 1;
                display1[dd6] = obz2;
                i2++;
            }
        }
    }

    public static final void draw_poly(point_ a, point_ b, point_ c) {
        int s1;
        int s2;
        int k;
        int[] distance1 = m3d.coof_clip;
        int[] distance = m3d.distance_H;
        int obz = obzor_2;
        a.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) a.rx) * ((long) obz)) / ((long) a.rz))) >> 16));
        a.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) a.ry) * ((long) obz)) / ((long) a.rz))) >> 16));
        b.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) b.rx) * ((long) obz)) / ((long) b.rz))) >> 16));
        b.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) b.ry) * ((long) obz)) / ((long) b.rz))) >> 16));
        c.sx = (short) ((int) ((((long) Width_2_3D) + ((((long) c.rx) * ((long) obz)) / ((long) c.rz))) >> 16));
        c.sy = (short) ((int) ((((long) Height_2_3D) - ((((long) c.ry) * ((long) obz)) / ((long) c.rz))) >> 16));
        if (a.sy > b.sy) {
            point_ tmp_vertex = a;
            a = b;
            b = tmp_vertex;
        }
        if (a.sy > c.sy) {
            point_ tmp_vertex2 = a;
            a = c;
            c = tmp_vertex2;
        }
        if (b.sy > c.sy) {
            point_ tmp_vertex3 = b;
            b = c;
            c = tmp_vertex3;
        }
        int dd = a.rz >> 16;
        if (dd < 0) {
            a.z1 = -distance[-dd];
        } else {
            a.z1 = distance[dd];
        }
        a.uz = a.u * a.z1;
        a.vz = a.v * a.z1;
        int dd2 = b.rz >> 16;
        if (dd2 < 0) {
            b.z1 = -distance[-dd2];
        } else {
            b.z1 = distance[dd2];
        }
        b.uz = b.u * b.z1;
        b.vz = b.v * b.z1;
        int dd3 = c.rz >> 16;
        if (dd3 < 0) {
            c.z1 = -distance[-dd3];
        } else {
            c.z1 = distance[dd3];
        }
        c.uz = c.u * c.z1;
        c.vz = c.v * c.z1;
        int k2 = (b.sy - a.sy) * distance1[c.sy - a.sy];
        int dd4 = (a.sx + ((int) ((((long) (c.sx - a.sx)) * ((long) k2)) >> 16))) - b.sx;
        if (dd4 < 0) {
            int dd5 = distance1[-dd4];
            s1 = -((int) ((((long) ((a.uz + ((int) ((((long) (c.uz - a.uz)) * ((long) k2)) >> 16))) - b.uz)) * ((long) dd5)) >> 12));
            s2 = -((int) ((((long) ((a.vz + ((int) ((((long) (c.vz - a.vz)) * ((long) k2)) >> 16))) - b.vz)) * ((long) dd5)) >> 12));
            k = -((int) ((((long) ((a.z1 + ((int) ((((long) (c.z1 - a.z1)) * ((long) k2)) >> 16))) - b.z1)) * ((long) dd5)) >> 12));
        } else {
            int dd6 = distance1[dd4];
            s1 = (int) ((((long) ((a.uz + ((int) ((((long) (c.uz - a.uz)) * ((long) k2)) >> 16))) - b.uz)) * ((long) dd6)) >> 12);
            s2 = (int) ((((long) ((a.vz + ((int) ((((long) (c.vz - a.vz)) * ((long) k2)) >> 16))) - b.vz)) * ((long) dd6)) >> 12);
            k = (int) ((((long) ((a.z1 + ((int) ((((long) (c.z1 - a.z1)) * ((long) k2)) >> 16))) - b.z1)) * ((long) dd6)) >> 12);
        }
        if ((c.sx - a.sx) * distance[c.sy - a.sy] < (b.sx - a.sx) * distance[b.sy - a.sy]) {
            very_high_text_ab_x1(a, b, c, s1, s2, k);
        } else {
            very_high_text_ab_x2(a, b, c, s1, s2, k);
        }
        if ((c.sx - a.sx) * distance[c.sy - a.sy] > (c.sx - b.sx) * distance[c.sy - b.sy]) {
            very_high_text_bc_x1(a, b, c, s1, s2, k);
        } else {
            very_high_text_bc_x2(a, b, c, s1, s2, k);
        }
    }

    private static final void very_high_text_ab_x1(point_ a, point_ b, point_ c, int duz, int dvz, int dz1) {
        int dd;
        byte[] texture1 = m3d.texture[m3d.ind];
        int[] pp = m3d.palitra[m3d.ind];
        int[] distance = m3d.distance_H;
        int[] distance1 = m3d.coof_clip;
        int[] display1 = m3d.display;
        int alfa_all = m3d.alfa_;
        int A = distance1[c.sy - a.sy];
        int B = distance1[b.sy - a.sy];
        for (int current_sy = a.sy; current_sy < b.sy; current_sy++) {
            int k = (current_sy - a.sy) * A;
            int x_start = a.sx + ((int) ((((long) (c.sx - a.sx)) * ((long) k)) >> 16));
            int uz_start = a.uz + ((int) ((((long) (c.uz - a.uz)) * ((long) k)) >> 16));
            int vz_start = a.vz + ((int) ((((long) (c.vz - a.vz)) * ((long) k)) >> 16));
            int z1_start = a.z1 + ((int) ((((long) (c.z1 - a.z1)) * ((long) k)) >> 16));
            int k2 = (current_sy - a.sy) * B;
            int z1_end = a.z1 + ((int) ((((long) (b.z1 - a.z1)) * ((long) k2)) >> 16));
            int dd2 = (Width_real_3D * current_sy) + x_start;
            int length = (a.sx + ((int) ((((long) (b.sx - a.sx)) * ((long) k2)) >> 16))) - x_start;
            int x_start2 = distance[z1_start];
            int u_a = uz_start * x_start2;
            int v_a = vz_start * x_start2;
            while (length >= 16) {
                uz_start += duz;
                vz_start += dvz;
                z1_start += dz1;
                int x_start3 = distance[z1_start];
                int u_b = uz_start * x_start3;
                int v_b = vz_start * x_start3;
                int du = (u_b - u_a) >> 4;
                int dv = (v_b - v_a) >> 4;
                int len = 16;
                while (true) {
                    int len2 = len;
                    dd = dd2;
                    len = len2 - 1;
                    if (len2 <= 0) {
                        break;
                    }
                    int x_start4 = pp[texture1[(((v_a >> stp_draw) << 6) + (u_a >> stp_draw)) & size_text_full]];
                    dd2 = dd + 1;
                    display1[dd] = (((((16711680 & x_start4) >> 16) * alfa_all) >> 8) << 16) + (((((65280 & x_start4) >> 8) * alfa_all) >> 8) << 8) + (((x_start4 & 255) * alfa_all) >> 8);
                    u_a += du;
                    v_a += dv;
                }
                length -= 16;
                u_a = u_b;
                v_a = v_b;
                dd2 = dd;
            }
            if (length > 0) {
                int x_start5 = distance[z1_end];
                int len3 = distance1[length];
                int du2 = (int) ((((long) (((a.uz + ((int) ((((long) (b.uz - a.uz)) * ((long) k2)) >> 16))) * x_start5) - u_a)) * ((long) len3)) >> 16);
                int dv2 = (int) ((((long) (((a.vz + ((int) ((((long) (b.vz - a.vz)) * ((long) k2)) >> 16))) * x_start5) - v_a)) * ((long) len3)) >> 16);
                while (true) {
                    int length2 = length;
                    int dd3 = dd2;
                    length = length2 - 1;
                    if (length2 <= 0) {
                        break;
                    }
                    int x_start6 = pp[texture1[(((v_a >> stp_draw) << 6) + (u_a >> stp_draw)) & size_text_full]];
                    dd2 = dd3 + 1;
                    display1[dd3] = (((((16711680 & x_start6) >> 16) * alfa_all) >> 8) << 16) + (((((65280 & x_start6) >> 8) * alfa_all) >> 8) << 8) + (((x_start6 & 255) * alfa_all) >> 8);
                    u_a += du2;
                    v_a += dv2;
                }
            }
        }
    }

    private static final void very_high_text_ab_x2(point_ a, point_ b, point_ c, int duz, int dvz, int dz1) {
        int dd;
        byte[] texture1 = m3d.texture[m3d.ind];
        int[] pp = m3d.palitra[m3d.ind];
        int[] distance = m3d.distance_H;
        int[] distance1 = m3d.coof_clip;
        int[] display1 = m3d.display;
        int alfa_all = m3d.alfa_;
        int A = distance1[b.sy - a.sy];
        int B = distance1[c.sy - a.sy];
        for (int current_sy = a.sy; current_sy < b.sy; current_sy++) {
            int k = (current_sy - a.sy) * A;
            int x_end = a.sx + ((int) ((((long) (b.sx - a.sx)) * ((long) k)) >> 16));
            int uz_end = a.uz + ((int) ((((long) (b.uz - a.uz)) * ((long) k)) >> 16));
            int vz_end = a.vz + ((int) ((((long) (b.vz - a.vz)) * ((long) k)) >> 16));
            int z1_end = a.z1 + ((int) ((((long) (b.z1 - a.z1)) * ((long) k)) >> 16));
            int k2 = (current_sy - a.sy) * B;
            int z1_start = a.z1 + ((int) ((((long) (c.z1 - a.z1)) * ((long) k2)) >> 16));
            int dd2 = (Width_real_3D * current_sy) + x_end;
            int length = (a.sx + ((int) ((((long) (c.sx - a.sx)) * ((long) k2)) >> 16))) - x_end;
            int x_end2 = distance[z1_end];
            int u_a = uz_end * x_end2;
            int v_a = vz_end * x_end2;
            while (length >= 16) {
                uz_end += duz;
                vz_end += dvz;
                z1_end += dz1;
                int x_end3 = distance[z1_end];
                int u_b = uz_end * x_end3;
                int v_b = vz_end * x_end3;
                int du = (u_b - u_a) >> 4;
                int dv = (v_b - v_a) >> 4;
                int len = 16;
                while (true) {
                    int len2 = len;
                    dd = dd2;
                    len = len2 - 1;
                    if (len2 <= 0) {
                        break;
                    }
                    int x_end4 = pp[texture1[(((v_a >> stp_draw) << 6) + (u_a >> stp_draw)) & size_text_full]];
                    dd2 = dd + 1;
                    display1[dd] = (((((16711680 & x_end4) >> 16) * alfa_all) >> 8) << 16) + (((((65280 & x_end4) >> 8) * alfa_all) >> 8) << 8) + (((x_end4 & 255) * alfa_all) >> 8);
                    u_a += du;
                    v_a += dv;
                }
                length -= 16;
                u_a = u_b;
                v_a = v_b;
                dd2 = dd;
            }
            if (length > 0) {
                int x_end5 = distance[z1_start];
                int len3 = distance1[length];
                int du2 = (int) ((((long) (((a.uz + ((int) ((((long) (c.uz - a.uz)) * ((long) k2)) >> 16))) * x_end5) - u_a)) * ((long) len3)) >> 16);
                int dv2 = (int) ((((long) (((a.vz + ((int) ((((long) (c.vz - a.vz)) * ((long) k2)) >> 16))) * x_end5) - v_a)) * ((long) len3)) >> 16);
                while (true) {
                    int length2 = length;
                    int dd3 = dd2;
                    length = length2 - 1;
                    if (length2 <= 0) {
                        break;
                    }
                    int x_end6 = pp[texture1[(((v_a >> stp_draw) << 6) + (u_a >> stp_draw)) & size_text_full]];
                    dd2 = dd3 + 1;
                    display1[dd3] = (((((16711680 & x_end6) >> 16) * alfa_all) >> 8) << 16) + (((((65280 & x_end6) >> 8) * alfa_all) >> 8) << 8) + (((x_end6 & 255) * alfa_all) >> 8);
                    u_a += du2;
                    v_a += dv2;
                }
            }
        }
    }

    private static final void very_high_text_bc_x1(point_ a, point_ b, point_ c, int duz, int dvz, int dz1) {
        int dd;
        byte[] texture1 = m3d.texture[m3d.ind];
        int[] pp = m3d.palitra[m3d.ind];
        int[] distance = m3d.distance_H;
        int[] distance1 = m3d.coof_clip;
        int[] display1 = m3d.display;
        int alfa_all = m3d.alfa_;
        int A = distance1[c.sy - a.sy];
        int B = distance1[c.sy - b.sy];
        for (int current_sy = b.sy; current_sy < c.sy; current_sy++) {
            int k = (current_sy - a.sy) * A;
            int x_start = a.sx + ((int) ((((long) (c.sx - a.sx)) * ((long) k)) >> 16));
            int uz_start = a.uz + ((int) ((((long) (c.uz - a.uz)) * ((long) k)) >> 16));
            int vz_start = a.vz + ((int) ((((long) (c.vz - a.vz)) * ((long) k)) >> 16));
            int z1_start = a.z1 + ((int) ((((long) (c.z1 - a.z1)) * ((long) k)) >> 16));
            int k2 = (current_sy - b.sy) * B;
            int z1_end = b.z1 + ((int) ((((long) (c.z1 - b.z1)) * ((long) k2)) >> 16));
            int dd2 = (Width_real_3D * current_sy) + x_start;
            int length = (b.sx + ((int) ((((long) (c.sx - b.sx)) * ((long) k2)) >> 16))) - x_start;
            int x_start2 = distance[z1_start];
            int u_a = uz_start * x_start2;
            int v_a = vz_start * x_start2;
            while (length >= 16) {
                uz_start += duz;
                vz_start += dvz;
                z1_start += dz1;
                int x_start3 = distance[z1_start];
                int u_b = uz_start * x_start3;
                int v_b = vz_start * x_start3;
                int du = (u_b - u_a) >> 4;
                int dv = (v_b - v_a) >> 4;
                int len = 16;
                while (true) {
                    int len2 = len;
                    dd = dd2;
                    len = len2 - 1;
                    if (len2 <= 0) {
                        break;
                    }
                    int x_start4 = pp[texture1[(((v_a >> stp_draw) << 6) + (u_a >> stp_draw)) & size_text_full]];
                    dd2 = dd + 1;
                    display1[dd] = (((((16711680 & x_start4) >> 16) * alfa_all) >> 8) << 16) + (((((65280 & x_start4) >> 8) * alfa_all) >> 8) << 8) + (((x_start4 & 255) * alfa_all) >> 8);
                    u_a += du;
                    v_a += dv;
                }
                length -= 16;
                u_a = u_b;
                v_a = v_b;
                dd2 = dd;
            }
            if (length > 0) {
                int x_start5 = distance[z1_end];
                int len3 = distance1[length];
                int du2 = (int) ((((long) (((b.uz + ((int) ((((long) (c.uz - b.uz)) * ((long) k2)) >> 16))) * x_start5) - u_a)) * ((long) len3)) >> 16);
                int dv2 = (int) ((((long) (((b.vz + ((int) ((((long) (c.vz - b.vz)) * ((long) k2)) >> 16))) * x_start5) - v_a)) * ((long) len3)) >> 16);
                while (true) {
                    int length2 = length;
                    int dd3 = dd2;
                    length = length2 - 1;
                    if (length2 <= 0) {
                        break;
                    }
                    int x_start6 = pp[texture1[(((v_a >> stp_draw) << 6) + (u_a >> stp_draw)) & size_text_full]];
                    dd2 = dd3 + 1;
                    display1[dd3] = (((((16711680 & x_start6) >> 16) * alfa_all) >> 8) << 16) + (((((65280 & x_start6) >> 8) * alfa_all) >> 8) << 8) + (((x_start6 & 255) * alfa_all) >> 8);
                    u_a += du2;
                    v_a += dv2;
                }
            }
        }
    }

    private static final void very_high_text_bc_x2(point_ a, point_ b, point_ c, int duz, int dvz, int dz1) {
        int dd;
        byte[] texture1 = m3d.texture[m3d.ind];
        int[] pp = m3d.palitra[m3d.ind];
        int[] distance = m3d.distance_H;
        int[] distance1 = m3d.coof_clip;
        int[] display1 = m3d.display;
        int alfa_all = m3d.alfa_;
        int A = distance1[c.sy - b.sy];
        int B = distance1[c.sy - a.sy];
        for (int current_sy = b.sy; current_sy < c.sy; current_sy++) {
            int k = (current_sy - b.sy) * A;
            int x_end = b.sx + ((int) ((((long) (c.sx - b.sx)) * ((long) k)) >> 16));
            int uz_end = b.uz + ((int) ((((long) (c.uz - b.uz)) * ((long) k)) >> 16));
            int vz_end = b.vz + ((int) ((((long) (c.vz - b.vz)) * ((long) k)) >> 16));
            int z1_end = b.z1 + ((int) ((((long) (c.z1 - b.z1)) * ((long) k)) >> 16));
            int k2 = (current_sy - a.sy) * B;
            int z1_start = a.z1 + ((int) ((((long) (c.z1 - a.z1)) * ((long) k2)) >> 16));
            int dd2 = (Width_real_3D * current_sy) + x_end;
            int length = (a.sx + ((int) ((((long) (c.sx - a.sx)) * ((long) k2)) >> 16))) - x_end;
            int x_end2 = distance[z1_end];
            int u_a = uz_end * x_end2;
            int v_a = vz_end * x_end2;
            while (length >= 16) {
                uz_end += duz;
                vz_end += dvz;
                z1_end += dz1;
                int x_end3 = distance[z1_end];
                int u_b = uz_end * x_end3;
                int v_b = vz_end * x_end3;
                int du = (u_b - u_a) >> 4;
                int dv = (v_b - v_a) >> 4;
                int len = 16;
                while (true) {
                    int len2 = len;
                    dd = dd2;
                    len = len2 - 1;
                    if (len2 <= 0) {
                        break;
                    }
                    int x_end4 = pp[texture1[(((v_a >> stp_draw) << 6) + (u_a >> stp_draw)) & size_text_full]];
                    dd2 = dd + 1;
                    display1[dd] = (((((16711680 & x_end4) >> 16) * alfa_all) >> 8) << 16) + (((((65280 & x_end4) >> 8) * alfa_all) >> 8) << 8) + (((x_end4 & 255) * alfa_all) >> 8);
                    u_a += du;
                    v_a += dv;
                }
                length -= 16;
                u_a = u_b;
                v_a = v_b;
                dd2 = dd;
            }
            if (length > 0) {
                int x_end5 = distance[z1_start];
                int len3 = distance1[length];
                int du2 = (int) ((((long) (((a.uz + ((int) ((((long) (c.uz - a.uz)) * ((long) k2)) >> 16))) * x_end5) - u_a)) * ((long) len3)) >> 16);
                int dv2 = (int) ((((long) (((a.vz + ((int) ((((long) (c.vz - a.vz)) * ((long) k2)) >> 16))) * x_end5) - v_a)) * ((long) len3)) >> 16);
                while (true) {
                    int length2 = length;
                    int dd3 = dd2;
                    length = length2 - 1;
                    if (length2 <= 0) {
                        break;
                    }
                    int x_end6 = pp[texture1[(((v_a >> stp_draw) << 6) + (u_a >> stp_draw)) & size_text_full]];
                    dd2 = dd3 + 1;
                    display1[dd3] = (((((16711680 & x_end6) >> 16) * alfa_all) >> 8) << 16) + (((((65280 & x_end6) >> 8) * alfa_all) >> 8) << 8) + (((x_end6 & 255) * alfa_all) >> 8);
                    u_a += du2;
                    v_a += dv2;
                }
            }
        }
    }

    static final byte clipPlane2(point_[] dst, point_[] src, int[][] nn) {
        long t_1;
        long t_2;
        int[] distance = m3d.distance_H;
        int o = -2097152;
        int num_point = 3;
        int rr = cof_W;
        int[] kl = m3d.remainder;
        for (int j = 0; j < 5; j++) {
            int[] n = nn[j];
            int x = n[0];
            int y = n[1];
            int z = n[2];
            rr = cof_W;
            int i = 0;
            int cof = num_point * 9;
            while (i < num_point) {
                point_ p1 = src[i];
                int cof2 = cof + 1;
                point_ p2 = src[kl[cof]];
                point_ p3 = dst[rr];
                int t1 = (int) ((((((long) p1.rx) * ((long) x)) + (((long) p1.ry) * ((long) y))) + (((long) (p1.rz + o)) * ((long) z))) >> 22);
                int t2 = (int) ((((((long) p2.rx) * ((long) x)) + (((long) p2.ry) * ((long) y))) + (((long) (p2.rz + o)) * ((long) z))) >> 22);
                if (t1 >= 0) {
                    p3.rx = p1.rx;
                    p3.ry = p1.ry;
                    p3.rz = p1.rz;
                    p3.u = p1.u;
                    p3.v = p1.v;
                    rr = (byte) (rr + 1);
                    p3 = dst[rr];
                }
                if (t2 - t1 > 0) {
                    int p = distance[t2 - t1];
                    t_1 = (long) (t2 * p);
                    t_2 = (long) ((-t1) * p);
                } else {
                    int p4 = distance[t1 - t2];
                    t_1 = (long) ((-t2) * p4);
                    t_2 = (long) (t1 * p4);
                }
                if ((t1 > 0 && t2 < 0) || (t2 > 0 && t1 < 0)) {
                    p3.rx = (int) (((((long) p1.rx) * t_1) + (((long) p2.rx) * t_2)) >> 18);
                    p3.ry = (int) (((((long) p1.ry) * t_1) + (((long) p2.ry) * t_2)) >> 18);
                    p3.rz = (int) (((((long) p1.rz) * t_1) + (((long) p2.rz) * t_2)) >> 18);
                    p3.u = (short) ((int) (((((long) p1.u) * t_1) + (((long) p2.u) * t_2)) >> 18));
                    p3.v = (short) ((int) (((((long) p1.v) * t_1) + (((long) p2.v) * t_2)) >> 18));
                    rr = (byte) (rr + 1);
                }
                i++;
                cof = cof2;
            }
            if (rr == 0) {
                return cof_W;
            }
            if (j == 0) {
                o = 0;
            }
            point_[] tmp = dst;
            dst = src;
            src = tmp;
            num_point = rr;
        }
        return rr;
    }
}
