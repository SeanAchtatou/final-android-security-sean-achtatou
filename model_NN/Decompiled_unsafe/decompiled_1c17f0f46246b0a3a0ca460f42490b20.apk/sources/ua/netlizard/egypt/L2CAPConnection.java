package ua.netlizard.egypt;

public class L2CAPConnection extends Connection {
    public static final int DEFAULT_MTU = 672;
    public static final int MINIMAL_MTU = 48;

    /* access modifiers changed from: package-private */
    public int getReceiveMTU() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getTransmitMTU() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean ready() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public int receive(byte[] inBuf) {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void send(byte[] data) {
    }

    /* access modifiers changed from: package-private */
    public void close() {
    }
}
