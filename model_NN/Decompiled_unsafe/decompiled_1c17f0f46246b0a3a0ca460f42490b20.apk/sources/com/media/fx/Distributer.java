package com.media.fx;

import java.util.Hashtable;

public class Distributer extends InfoObject {
    public Hashtable<String, String[]> DistrFieldsOrder = new Hashtable<>(4);

    public Distributer() {
        String[] strArr = {"", Constants.distID, Constants.distChannelID, Constants.distName, Constants.distWWW, Constants.distWWWTitle, Constants.distMoreGamesLink, Constants.distMoreGamesTitle, Constants.distPartnerFlags, Constants.distGameFlags, Constants.distKeySizeRegister, Constants.distKeySizeBonus, Constants.distKeySizeSubscribtionType1, Constants.distKeySizeSubscribtionType2, Constants.distKeySizeSubscribtionType3, Constants.distKeySizeSubscribtionType4, Constants.distTailSeparator, Constants.distSmsKeyVersion};
        String[] strArr2 = {""};
        this.DistrFieldsOrder.put("CFG_1", strArr);
        this.DistrFieldsOrder.put("JAD_1", strArr2);
        this.DistrFieldsOrder.put("CFG_2", strArr);
        this.DistrFieldsOrder.put("JAD_2", strArr2);
        id("0001");
        channelId("00");
        name(Constants.ctryName);
        gameLink("www.game.com");
        gameLinkTitle("Game link title");
        moreGamesLink("www.games.com");
        moreGamesTitle("More games");
        partnerFlags("A");
        gameFlags("FLAG");
        keySize_Register("6");
        keySize_Bonus("4");
        keySize_SubscribeType1("6");
        keySize_SubscribeType2("6");
        keySize_SubscribeType3("6");
        keySize_SubscribeType4("6");
        smsCustomTailSeparator("#");
        smsKeyVersion("0");
    }

    public final String id() {
        return info(Constants.distID);
    }

    public final String channelId() {
        String info = info(Constants.distChannelID);
        while (true) {
            if (info.length() >= (Config.versionInt() > 1 ? 5 : 2)) {
                return info;
            }
            info = "0" + info;
        }
    }

    public final String name() {
        return info(Constants.distName);
    }

    public final String gameLink() {
        return info(Constants.distWWW);
    }

    public final String gameLinkTitle() {
        return info(Constants.distWWWTitle);
    }

    public final String moreGamesLink() {
        return info(Constants.distMoreGamesLink);
    }

    public final String moreGamesTitle() {
        return info(Constants.distMoreGamesTitle);
    }

    public final String partnerFlags() {
        return info(Constants.distPartnerFlags);
    }

    public final String gameFlags() {
        return info(Constants.distGameFlags);
    }

    public final String keySize_Register() {
        return info(Constants.distKeySizeRegister);
    }

    public final String keySize_Bonus() {
        return info(Constants.distKeySizeBonus);
    }

    public final String keySize_SubscribtionType1() {
        return info(Constants.distKeySizeSubscribtionType1);
    }

    public final String keySize_SubscribtionType2() {
        return info(Constants.distKeySizeSubscribtionType2);
    }

    public final String keySize_SubscribtionType3() {
        return info(Constants.distKeySizeSubscribtionType3);
    }

    public final String keySize_SubscribtionType4() {
        return info(Constants.distKeySizeSubscribtionType4);
    }

    public final String smsCustomTailSeparator() {
        return info(Constants.distTailSeparator);
    }

    public final String smsKeyVersion() {
        return info(Constants.distSmsKeyVersion);
    }

    public final String id(String val) {
        return info(Constants.distID, val);
    }

    public final String channelId(String val) {
        return info(Constants.distChannelID, val);
    }

    public final String name(String val) {
        return info(Constants.distName, val);
    }

    public final String gameLinkTitle(String val) {
        return info(Constants.distWWWTitle, val);
    }

    public final String gameLink(String val) {
        return info(Constants.distWWW, val);
    }

    public final String moreGamesLink(String val) {
        return info(Constants.distMoreGamesLink, val);
    }

    public final String moreGamesTitle(String val) {
        return info(Constants.distMoreGamesTitle, val);
    }

    public final String partnerFlags(String val) {
        return info(Constants.distPartnerFlags, val);
    }

    public final String gameFlags(String val) {
        return info(Constants.distGameFlags, val);
    }

    public final String keySize_Register(String val) {
        return info(Constants.distKeySizeRegister, val);
    }

    public final String keySize_Bonus(String val) {
        return info(Constants.distKeySizeBonus, val);
    }

    public final String keySize_SubscribeType1(String val) {
        return info(Constants.distKeySizeSubscribtionType1, val);
    }

    public final String keySize_SubscribeType2(String val) {
        return info(Constants.distKeySizeSubscribtionType2, val);
    }

    public final String keySize_SubscribeType3(String val) {
        return info(Constants.distKeySizeSubscribtionType3, val);
    }

    public final String keySize_SubscribeType4(String val) {
        return info(Constants.distKeySizeSubscribtionType4, val);
    }

    public final String smsCustomTailSeparator(String val) {
        return info(Constants.distTailSeparator, val);
    }

    public final String smsKeyVersion(String val) {
        return info(Constants.distSmsKeyVersion, val);
    }

    public final boolean checkGameFlag(String fl) {
        return gameFlags().indexOf(fl) >= 0 || flag(fl);
    }

    public final void readFromArray(String[] arr) {
        readFieldsFromArray(this.DistrFieldsOrder.get("CFG_" + Config.version()), arr);
    }

    public final void readConfig_from_CFG(String[] cfgContents) {
        readConfig_from_CFG(cfgContents, 0, 1);
    }

    public final void readConfig_from_CFG(String[] cfgContents, int i, int cfgLength) {
        while (i < cfgLength && i < cfgContents.length) {
            String[] split = Util.split(Constants.cfgSeparator, cfgContents[i]);
            if (split.length > 1) {
                readFromArray(split);
            }
            i++;
        }
    }
}
