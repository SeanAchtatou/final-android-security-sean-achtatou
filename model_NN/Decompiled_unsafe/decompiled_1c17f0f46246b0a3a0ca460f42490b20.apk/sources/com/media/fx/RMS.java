package com.media.fx;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

public class RMS implements Constants {
    public static final int CurrentCountry() {
        return ((Integer) RMS_load(Constants.RMS_store_name_generalInfo, 1, new Integer(-1))).intValue();
    }

    public static final int CurrentCountry(int v) {
        return ((Integer) RMS_save(Constants.RMS_store_name_generalInfo, 1, new Integer(v))).intValue();
    }

    public static final long ActivationKey() {
        return ((Long) RMS_load(Constants.RMS_store_name_generalInfo, 7, new Long(0))).longValue();
    }

    public static final long ActivationKey(long v) {
        return ((Long) RMS_save(Constants.RMS_store_name_generalInfo, 7, new Long(((Long) RMS_save(Constants.RMS_store_name_generalInfo, 22, new Long(v))).longValue()))).longValue();
    }

    public static final long LastModified() {
        return ((Long) RMS_load(Constants.RMS_store_name_generalInfo, 2, new Long(0))).longValue();
    }

    public static final long LastModified(long v) {
        return ((Long) RMS_save(Constants.RMS_store_name_generalInfo, 2, new Long(v), false)).longValue();
    }

    public static final long LastModified_update() {
        return LastModified(System.currentTimeMillis());
    }

    public static final long RunsCount_Update() {
        return ((Long) RMS_save(Constants.RMS_store_name_generalInfo, 3, new Long(RunsCount() + 1))).longValue();
    }

    public static final long RunsCount() {
        return ((Long) RMS_load(Constants.RMS_store_name_generalInfo, 3, new Long(0))).longValue();
    }

    public static final String Platform_Update() {
        return (String) RMS_save(Constants.RMS_store_name_generalInfo, 4, System.getProperty("java.vm.name"));
    }

    public static final String Platform() {
        return (String) RMS_load(Constants.RMS_store_name_generalInfo, 4, new String(""));
    }

    public static final long SubscriptionKey() {
        return ((Long) RMS_load(Constants.RMS_store_name_generalInfo, 12, new Long(0))).longValue();
    }

    public static final long SubscriptionKey(long v) {
        return ((Long) RMS_save(Constants.RMS_store_name_generalInfo, 12, new Long(v))).longValue();
    }

    public static final String EnteredKey(String v) {
        return (String) RMS_save(Constants.RMS_store_name_generalInfo, 6, new String(v));
    }

    public static final String EnteredKey() {
        return (String) RMS_load(Constants.RMS_store_name_generalInfo, 6, new String(""));
    }

    public static final String EnteredKeyActivation(String v) {
        return (String) RMS_save(Constants.RMS_store_name_generalInfo, 23, new String((String) RMS_save(Constants.RMS_store_name_generalInfo, 25, new String(v))));
    }

    public static final String EnteredKeyActivation() {
        return (String) RMS_load(Constants.RMS_store_name_generalInfo, 25, new String(""));
    }

    public static final int SubscribtionMode() {
        return ((Integer) RMS_load(Constants.RMS_store_name_generalInfo, 13, new Integer(0))).intValue();
    }

    public static final int SubscribtionMode(int v) {
        return ((Integer) RMS_save(Constants.RMS_store_name_generalInfo, 13, new Integer(v))).intValue();
    }

    public static final long RememberBonus(long v, int bonus_type) {
        return ((Long) RMS_save(Constants.RMS_store_name_bonuses, -1, new Long((10 * v) + ((long) bonus_type)))).longValue();
    }

    public static final int IncreaseSendFriendAttemptCounter() {
        return ((Integer) RMS_save(Constants.RMS_store_name_generalInfo, 8, new Integer(SendFriendAttemptsCounter() + 1))).intValue();
    }

    public static final int SendFriendAttemptsCounter() {
        return ((Integer) RMS_load(Constants.RMS_store_name_generalInfo, 8, new Integer(0))).intValue();
    }

    public static final long ExpirationDate() {
        return ((Long) RMS_load(Constants.RMS_store_name_generalInfo, 9, new Long(0))).longValue();
    }

    public static final long ExpirationDate(long v) {
        return ((Long) RMS_save(Constants.RMS_store_name_generalInfo, 9, new Long(v))).longValue();
    }

    public static final int RegistrationType(int v) {
        return ((Integer) RMS_save(Constants.RMS_store_name_generalInfo, 10, new Integer(v))).intValue();
    }

    public static final int RegistrationType() {
        return ((Integer) RMS_load(Constants.RMS_store_name_generalInfo, 10, new Integer(-1))).intValue();
    }

    public static final int DemoModeElapsed(int v) {
        return ((Integer) RMS_save(Constants.RMS_store_name_generalInfo, 14, new Integer(v))).intValue();
    }

    public static final int DemoModeElapsed() {
        return ((Integer) RMS_load(Constants.RMS_store_name_generalInfo, 14, new Integer(0))).intValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005d, code lost:
        close_store(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0060, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0065, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005c A[ExcHandler: all (r0v3 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0008] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final long CheckBonus(long r14) {
        /*
            r12 = 10
            r4 = 0
            r0 = 1
            r1 = 0
            r2 = 0
            r7 = r0
        L_0x0008:
            com.media.fx.Distributer r3 = com.media.fx.Config.distInfo     // Catch:{ Exception -> 0x0061, all -> 0x005c }
            java.lang.String r3 = r3.keySize_Bonus()     // Catch:{ Exception -> 0x0061, all -> 0x005c }
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x0061, all -> 0x005c }
            if (r2 >= r3) goto L_0x001a
            int r3 = r7 * 10
            int r2 = r2 + 1
            r7 = r3
            goto L_0x0008
        L_0x001a:
            long r2 = (long) r7     // Catch:{ Exception -> 0x0061, all -> 0x005c }
            long r14 = r14 % r2
            java.lang.String r2 = "bonuses"
            com.media.fx.RecordStore r1 = open_store(r2)     // Catch:{ Exception -> 0x0061, all -> 0x005c }
            r6 = r0
        L_0x0023:
            int r0 = r1.getNumRecords()     // Catch:{ Exception -> 0x0064, all -> 0x005c }
            if (r6 > r0) goto L_0x0056
            java.lang.Long r0 = new java.lang.Long     // Catch:{ Exception -> 0x0064, all -> 0x005c }
            r2 = 0
            r0.<init>(r2)     // Catch:{ Exception -> 0x0064, all -> 0x005c }
            java.lang.Object r0 = RMS_load(r1, r6, r0)     // Catch:{ Exception -> 0x0064, all -> 0x005c }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Exception -> 0x0064, all -> 0x005c }
            long r2 = r0.longValue()     // Catch:{ Exception -> 0x0064, all -> 0x005c }
            int r0 = r7 * 10
            long r8 = (long) r0     // Catch:{ Exception -> 0x0064, all -> 0x005c }
            long r2 = r2 % r8
            r8 = 10
            long r8 = r2 % r8
            long r8 = r2 - r8
            long r10 = r14 * r12
            int r0 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r0 != 0) goto L_0x0052
            r1.deleteRecord(r6)     // Catch:{ Exception -> 0x0064, all -> 0x005c }
            close_store(r1)
            r0 = r2
        L_0x0051:
            return r0
        L_0x0052:
            int r0 = r6 + 1
            r6 = r0
            goto L_0x0023
        L_0x0056:
            r0 = r1
        L_0x0057:
            close_store(r0)
            r0 = r4
            goto L_0x0051
        L_0x005c:
            r0 = move-exception
            close_store(r1)
            throw r0
        L_0x0061:
            r0 = move-exception
            r0 = r1
            goto L_0x0057
        L_0x0064:
            r0 = move-exception
            r0 = r1
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.media.fx.RMS.CheckBonus(long):long");
    }

    public static RecordStore open_store(String name) {
        try {
            return RecordStore.openRecordStore(name, true);
        } catch (Exception e) {
            return null;
        }
    }

    public static void create_records_before(int record, RecordStore store) {
        try {
            int numRecords = store.getNumRecords();
            if (numRecords < record) {
                for (int i = numRecords + 1; i <= record; i++) {
                    store.addRecord(null, 0, 0);
                }
            }
        } catch (Exception e) {
        }
    }

    public static void close_store(RecordStore store) {
        if (store != null) {
            try {
                store.closeRecordStore();
            } catch (Exception e) {
            }
        }
    }

    public static Object RMS_save(String store_name, int recId, Object val) {
        return RMS_save(store_name, recId, val, true);
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r21v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r21v2, resolved type: java.lang.Long} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r21v3, resolved type: java.lang.Long} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: java.lang.Long} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d A[SYNTHETIC, Splitter:B:18:0x006d] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0093  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object RMS_save(java.lang.String r19, int r20, java.lang.Object r21, boolean r22) {
        /*
            r5 = 0
            r6 = -1
            r3 = 0
            r1 = 0
            com.media.fx.RecordStore r8 = open_store(r19)     // Catch:{ Exception -> 0x0129, all -> 0x0126 }
            if (r20 > 0) goto L_0x0011
            int r20 = r8.getNextRecordID()     // Catch:{ Exception -> 0x012e }
        L_0x0011:
            int r3 = r8.getNumRecords()     // Catch:{ Exception -> 0x012e }
            long r4 = (long) r3
            r0 = r20
            create_records_before(r0, r8)     // Catch:{ Exception -> 0x0133 }
            int r1 = r8.getNumRecords()     // Catch:{ Exception -> 0x0133 }
            long r2 = (long) r1
            r6 = -2
            java.io.ByteArrayOutputStream r9 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00b4 }
            r9.<init>()     // Catch:{ Exception -> 0x00b4 }
            java.io.DataOutputStream r10 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x00b4 }
            r10.<init>(r9)     // Catch:{ Exception -> 0x00b4 }
            java.lang.Class r1 = r21.getClass()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r11 = "java.lang.Integer"
            int r1 = r1.compareTo(r11)     // Catch:{ Exception -> 0x00b4 }
            if (r1 != 0) goto L_0x0097
            r0 = r21
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x00b4 }
            r1 = r0
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x00b4 }
            r10.writeInt(r1)     // Catch:{ Exception -> 0x00b4 }
        L_0x0048:
            r6 = -3
            r10.flush()     // Catch:{ Exception -> 0x00b4 }
            r9.flush()     // Catch:{ Exception -> 0x00b4 }
            r6 = -4
            byte[] r1 = r9.toByteArray()     // Catch:{ Exception -> 0x00b4 }
            r6 = -5
            r11 = 0
            int r12 = r1.length     // Catch:{ Exception -> 0x00b4 }
            r0 = r20
            r8.setRecord(r0, r1, r11, r12)     // Catch:{ Exception -> 0x00b4 }
            r6 = -6
            r10.close()     // Catch:{ Exception -> 0x00b4 }
            r9.close()     // Catch:{ Exception -> 0x00b4 }
        L_0x0067:
            r9 = -10
            int r1 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r1 > 0) goto L_0x008e
            java.lang.Class r1 = r21.getClass()     // Catch:{ all -> 0x00e9 }
            java.lang.String r1 = r1.getName()     // Catch:{ all -> 0x00e9 }
            java.lang.String r9 = "java.lang.Long"
            int r1 = r1.compareTo(r9)     // Catch:{ all -> 0x00e9 }
            if (r1 != 0) goto L_0x008e
            java.lang.Long r21 = new java.lang.Long     // Catch:{ all -> 0x00e9 }
            r9 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 * r9
            long r4 = r6 - r4
            r6 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 * r6
            long r1 = r4 - r2
            r0 = r21
            r0.<init>(r1)     // Catch:{ all -> 0x00e9 }
        L_0x008e:
            close_store(r8)
            if (r22 == 0) goto L_0x0096
            LastModified_update()
        L_0x0096:
            return r21
        L_0x0097:
            java.lang.Class r1 = r21.getClass()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r11 = "java.lang.Long"
            int r1 = r1.compareTo(r11)     // Catch:{ Exception -> 0x00b4 }
            if (r1 != 0) goto L_0x00cb
            r0 = r21
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Exception -> 0x00b4 }
            r1 = r0
            long r11 = r1.longValue()     // Catch:{ Exception -> 0x00b4 }
            r10.writeLong(r11)     // Catch:{ Exception -> 0x00b4 }
            goto L_0x0048
        L_0x00b4:
            r1 = move-exception
            r13 = r2
            r1 = r13
            r15 = r4
            r3 = r15
            r17 = r6
            r5 = r17
            r7 = r8
        L_0x00be:
            r8 = -10
            long r5 = r5 + r8
            r8 = r7
            r13 = r3
            r15 = r5
            r6 = r15
            r4 = r13
            r17 = r1
            r2 = r17
            goto L_0x0067
        L_0x00cb:
            java.lang.Class r1 = r21.getClass()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r11 = "java.lang.Byte"
            int r1 = r1.compareTo(r11)     // Catch:{ Exception -> 0x00b4 }
            if (r1 != 0) goto L_0x00ee
            r0 = r21
            java.lang.Byte r0 = (java.lang.Byte) r0     // Catch:{ Exception -> 0x00b4 }
            r1 = r0
            byte r1 = r1.byteValue()     // Catch:{ Exception -> 0x00b4 }
            r10.writeByte(r1)     // Catch:{ Exception -> 0x00b4 }
            goto L_0x0048
        L_0x00e9:
            r1 = move-exception
        L_0x00ea:
            close_store(r8)
            throw r1
        L_0x00ee:
            java.lang.Class r1 = r21.getClass()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r11 = "java.lang.Boolean"
            int r1 = r1.compareTo(r11)     // Catch:{ Exception -> 0x00b4 }
            if (r1 != 0) goto L_0x010c
            r0 = r21
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Exception -> 0x00b4 }
            r1 = r0
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x00b4 }
            r10.writeBoolean(r1)     // Catch:{ Exception -> 0x00b4 }
            goto L_0x0048
        L_0x010c:
            java.lang.Class r1 = r21.getClass()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r11 = "java.lang.String"
            int r1 = r1.compareTo(r11)     // Catch:{ Exception -> 0x00b4 }
            if (r1 != 0) goto L_0x0048
            r0 = r21
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00b4 }
            r1 = r0
            r10.writeUTF(r1)     // Catch:{ Exception -> 0x00b4 }
            goto L_0x0048
        L_0x0126:
            r1 = move-exception
            r8 = r5
            goto L_0x00ea
        L_0x0129:
            r8 = move-exception
            r13 = r6
            r7 = r5
            r5 = r13
            goto L_0x00be
        L_0x012e:
            r5 = move-exception
            r13 = r6
            r5 = r13
            r7 = r8
            goto L_0x00be
        L_0x0133:
            r3 = move-exception
            r13 = r4
            r3 = r13
            r15 = r6
            r5 = r15
            r7 = r8
            goto L_0x00be
        */
        throw new UnsupportedOperationException("Method not decompiled: com.media.fx.RMS.RMS_save(java.lang.String, int, java.lang.Object, boolean):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0015, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0016, code lost:
        r2 = r1;
        r1 = r0;
        r0 = r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001a A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object RMS_load(java.lang.String r3, int r4, java.lang.Object r5) {
        /*
            r0 = 0
            com.media.fx.RecordStore r0 = open_store(r3)     // Catch:{ Exception -> 0x001a, all -> 0x000d }
            java.lang.Object r5 = RMS_load(r0, r4, r5)     // Catch:{ Exception -> 0x001a, all -> 0x0015 }
        L_0x0009:
            close_store(r0)
            return r5
        L_0x000d:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
        L_0x0011:
            close_store(r1)
            throw r0
        L_0x0015:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
            goto L_0x0011
        L_0x001a:
            r1 = move-exception
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.media.fx.RMS.RMS_load(java.lang.String, int, java.lang.Object):java.lang.Object");
    }

    public static Object RMS_load(RecordStore store, int record, Object def) throws Exception {
        try {
            byte[] record2 = store.getRecord(record);
            if (record2 == null || def == null) {
                return def;
            }
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(record2);
            DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);
            if (def.getClass().getName().compareTo("java.lang.Integer") == 0) {
                return new Integer(dataInputStream.readInt());
            }
            if (def.getClass().getName().compareTo("java.lang.Long") == 0) {
                return new Long(dataInputStream.readLong());
            }
            if (def.getClass().getName().compareTo("java.lang.Byte") == 0) {
                return new Byte(dataInputStream.readByte());
            }
            if (def.getClass().getName().compareTo("java.lang.Boolean") == 0) {
                return new Boolean(dataInputStream.readBoolean());
            }
            if (def.getClass().getName().compareTo("java.lang.String") == 0) {
                return new String(dataInputStream.readUTF());
            }
            dataInputStream.close();
            byteArrayInputStream.close();
            return def;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public static final long LastModified_current() {
        RecordStore recordStore = null;
        long j = 0;
        try {
            recordStore = open_store(Constants.RMS_store_name_generalInfo);
            j = recordStore.getLastModified();
        } catch (Exception e) {
        } catch (Throwable th) {
            close_store(recordStore);
            throw th;
        }
        close_store(recordStore);
        return j;
    }
}
