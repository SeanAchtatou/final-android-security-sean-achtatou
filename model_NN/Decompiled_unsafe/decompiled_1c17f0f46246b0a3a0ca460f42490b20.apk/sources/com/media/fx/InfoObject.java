package com.media.fx;

import android.content.Context;
import java.util.Hashtable;

public class InfoObject implements Constants {
    protected Context context = null;
    public final Hashtable<String, String> infoHash = new Hashtable<>(12);

    public final String info(String name) {
        return this.infoHash.get(name);
    }

    public final String info(String name, String val) {
        this.infoHash.put(name, val);
        return val;
    }

    public final boolean flag(String name) {
        String info = info(name);
        return (info.compareTo("0") == 0 || info.compareTo("") == 0) ? false : true;
    }

    public final boolean flag(String name, boolean val) {
        this.infoHash.put(name, val ? "1" : "0");
        return val;
    }

    public final int infoInt(String name) {
        String info = info(name);
        if (info == "") {
            return 0;
        }
        try {
            return Integer.parseInt(info);
        } catch (Exception e) {
            return 0;
        }
    }

    public final int infoInt(String name, int val) {
        this.infoHash.put(name, String.valueOf(val));
        return val;
    }

    public final void init(Context ctx) {
        this.context = ctx;
    }

    public final void readFieldsFromArray(String[] fields, String[] arr) {
        int i = 0;
        while (i < arr.length && i < fields.length) {
            if (fields[i].compareTo("") != 0) {
                info(fields[i], arr[i]);
            }
            i++;
        }
    }
}
