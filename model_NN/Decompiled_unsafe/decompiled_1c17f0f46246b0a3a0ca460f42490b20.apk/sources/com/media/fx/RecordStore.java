package com.media.fx;

import android.content.Context;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

public class RecordStore {
    private static RecordStore a = new RecordStore();
    private Context b = null;
    private String c = "";
    private boolean d = false;
    private Vector<AnonymousClass1> e = new Vector<>();

    /* renamed from: com.media.fx.RecordStore$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        private int a;
        private byte[] b;

        /* synthetic */ AnonymousClass1(RecordStore recordStore) {
            this(recordStore, (byte) 0);
        }

        private AnonymousClass1(RecordStore recordStore, byte b2) {
            this.a = 0;
            this.b = new byte[0];
        }

        public void a(int i) {
            this.a = i;
        }

        public int a() {
            return this.a;
        }

        public void a(byte[] bArr) {
            this.b = bArr;
        }

        public byte[] b() {
            return this.b;
        }
    }

    public RecordStore getInstance() {
        return a;
    }

    public static void init(Context context) {
        a.b = context;
    }

    public static RecordStore openRecordStore(String recordStoreName, boolean createIfNecessary) {
        a.c = recordStoreName;
        if (createIfNecessary) {
            try {
                a.b.openFileInput(recordStoreName).close();
            } catch (FileNotFoundException e2) {
                try {
                    a.b.openFileOutput(recordStoreName, 0).close();
                } catch (Exception e3) {
                }
            } catch (IOException e4) {
            }
        }
        RecordStore recordStore = a;
        recordStore.a(recordStore.c);
        return a;
    }

    private void a(String str) {
        try {
            FileInputStream openFileInput = this.b.openFileInput(str);
            DataInputStream dataInputStream = new DataInputStream(openFileInput);
            while (true) {
                try {
                    int readInt = dataInputStream.readInt();
                    byte[] bArr = new byte[readInt];
                    dataInputStream.read(bArr);
                    AnonymousClass1 r4 = new AnonymousClass1(this);
                    r4.a(readInt);
                    if (readInt <= 0) {
                        bArr = null;
                    }
                    r4.a(bArr);
                    this.e.add(r4);
                } catch (EOFException e2) {
                    dataInputStream.close();
                    openFileInput.close();
                    return;
                }
            }
        } catch (Exception e3) {
        }
    }

    public int getNumRecords() {
        return this.e.size();
    }

    public int addRecord(byte[] data, int offset, int numBytes) {
        AnonymousClass1 r1 = new AnonymousClass1(this);
        r1.a(numBytes);
        byte[] bArr = null;
        if (data != null && numBytes > 0) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
            bArr = new byte[numBytes];
            byteArrayInputStream.read(bArr, offset, numBytes);
            try {
                byteArrayInputStream.close();
            } catch (Exception e2) {
            }
        }
        r1.a(bArr);
        this.e.add(r1);
        this.d = true;
        return this.e.size();
    }

    public void deleteRecord(int recordId) {
        AnonymousClass1 r0 = this.e.get(recordId - 1);
        r0.a(0);
        r0.a((byte[]) null);
        this.e.setElementAt(r0, recordId - 1);
        this.d = true;
    }

    public void closeRecordStore() {
        String str = this.c;
        if (this.d) {
            try {
                FileOutputStream openFileOutput = this.b.openFileOutput(str, 0);
                DataOutputStream dataOutputStream = new DataOutputStream(openFileOutput);
                for (int i = 0; i < this.e.size(); i++) {
                    AnonymousClass1 r0 = this.e.get(i);
                    dataOutputStream.writeInt(r0.a());
                    if (r0.b() != null) {
                        dataOutputStream.write(r0.b());
                    }
                }
                dataOutputStream.flush();
                openFileOutput.flush();
                dataOutputStream.close();
                openFileOutput.close();
            } catch (Exception e2) {
            }
        }
        this.e.clear();
        this.c = "";
        this.d = false;
    }

    public int getNextRecordID() {
        return this.e.size() + 1;
    }

    public void setRecord(int recordId, byte[] newData, int offset, int numBytes) {
        boolean z;
        AnonymousClass1 r0 = this.e.get(recordId - 1);
        byte[] b2 = r0.b();
        this.d = newData.length != (b2 == null ? 0 : b2.length);
        for (int i = 0; i < newData.length && !this.d; i++) {
            if (newData[i] != b2[i]) {
                z = true;
            } else {
                z = false;
            }
            this.d = z;
        }
        if (this.d) {
            r0.a(numBytes);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(newData);
            byte[] bArr = new byte[numBytes];
            byteArrayInputStream.read(bArr, offset, numBytes);
            try {
                byteArrayInputStream.close();
            } catch (Exception e2) {
            }
            r0.a(bArr);
            this.e.setElementAt(r0, recordId - 1);
        }
    }

    public byte[] getRecord(int recordId) {
        if (this.e.size() < recordId) {
            return null;
        }
        return this.e.get(recordId - 1).b();
    }

    public long getLastModified() {
        return this.b.getFileStreamPath(this.c).lastModified();
    }
}
