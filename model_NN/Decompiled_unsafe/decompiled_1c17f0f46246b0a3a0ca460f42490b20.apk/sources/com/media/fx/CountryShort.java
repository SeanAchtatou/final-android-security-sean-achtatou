package com.media.fx;

public final class CountryShort {
    private int a = -1;
    private int b = -1;
    private String c = "";
    private String d = "";

    public CountryShort(int id, String name, String code) {
        this.a = id;
        this.b = id;
        this.c = name;
        this.d = code;
    }

    public final String name() {
        return this.c == "" ? this.d : this.c;
    }

    public final String code() {
        return this.d;
    }

    public final String name(String val) {
        this.c = val;
        return val;
    }

    public final String code(String val) {
        this.d = val;
        return val;
    }

    public final int id() {
        return this.a;
    }

    public final int sec_id() {
        return this.b;
    }

    public final int sec_id(int val) {
        this.b = val;
        return val;
    }
}
