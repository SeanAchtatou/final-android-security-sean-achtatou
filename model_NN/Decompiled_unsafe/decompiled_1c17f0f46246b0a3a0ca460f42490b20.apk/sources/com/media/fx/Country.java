package com.media.fx;

import java.util.Hashtable;
import java.util.Vector;

public class Country extends InfoObject {
    public static final Hashtable<String, String[]> CntryFieldsOrder = new Hashtable<>(4);
    public static final Hashtable<String, Integer> countriesCodesList = new Hashtable<>(0);
    public static final Hashtable<Integer, Integer> countriesIdList = new Hashtable<>(0);
    public static final Vector<CountryShort> countriesList = new Vector<>(0);
    public static final Hashtable<String, Integer> countriesNamesList = new Hashtable<>(0);

    public Country() {
        String[] strArr = {Constants.ctryName, Constants.ctryPrefix, Constants.ctryNumReg, Constants.ctryPriceReg, Constants.ctryPrefixBonus1, Constants.ctryNumBonus1, Constants.ctryPriceBonus1, Constants.ctryPrefixSpec, Constants.ctryNumSpec, Constants.ctryPriceSpec, Constants.ctryPrefixSubscribeType1, Constants.ctryNumSubscribeType1, Constants.ctryPriceSubscribeType1, Constants.ctryDurationSubscribeType1, Constants.ctryPrefixSubscribeType2, Constants.ctryNumSubscribeType2, Constants.ctryPriceSubscribeType2, Constants.ctryDurationSubscribeType2, Constants.ctryPrefixSubscribeType3, Constants.ctryNumSubscribeType3, Constants.ctryPriceSubscribeType3, Constants.ctryDurationSubscribeType3, Constants.ctryPrefixSubscribeType4, Constants.ctryNumSubscribeType4, Constants.ctryPriceSubscribeType4, Constants.ctryDurationSubscribeType4, Constants.ctrySpecParam1, Constants.ctrySpecParam2, Constants.ctrySpecParam3, Constants.ctrySpecParam4};
        String[] strArr2 = {Constants.ctryName, Constants.ctryCode, Constants.ctryPrefix, Constants.ctryNumReg, Constants.ctryPriceReg, Constants.ctryParamReg, Constants.ctryPrefixBonus1, Constants.ctryNumBonus1, Constants.ctryPriceBonus1, Constants.ctryParamBonus1, Constants.ctryPrefixBonus2, Constants.ctryNumBonus2, Constants.ctryPriceBonus2, Constants.ctryParamBonus2, Constants.ctryPrefixBonus3, Constants.ctryNumBonus3, Constants.ctryPriceBonus3, Constants.ctryParamBonus3, Constants.ctryPrefixBonus4, Constants.ctryNumBonus4, Constants.ctryPriceBonus4, Constants.ctryParamBonus4, Constants.ctryPrefixSubscribeType1, Constants.ctryNumSubscribeType1, Constants.ctryPriceSubscribeType1, Constants.ctryDurationSubscribeType1, Constants.ctryPrefixSubscribeType2, Constants.ctryNumSubscribeType2, Constants.ctryPriceSubscribeType2, Constants.ctryDurationSubscribeType2, Constants.ctryPrefixSubscribeType3, Constants.ctryNumSubscribeType3, Constants.ctryPriceSubscribeType3, Constants.ctryDurationSubscribeType3, Constants.ctryPrefixSubscribeType4, Constants.ctryNumSubscribeType4, Constants.ctryPriceSubscribeType4, Constants.ctryDurationSubscribeType4, Constants.ctryPrefixSpec, Constants.ctryNumSpec, Constants.ctryPriceSpec, Constants.ctryParamSpec, Constants.ctrySpecParam1, Constants.ctrySpecParam2, Constants.ctrySpecParam3, Constants.ctrySpecParam4};
        if (!CntryFieldsOrder.contains("CFG_1")) {
            CntryFieldsOrder.put("CFG_1", strArr);
        }
        if (!CntryFieldsOrder.contains("CFG_2")) {
            CntryFieldsOrder.put("CFG_2", strArr2);
        }
        id("-1");
        name("Russia");
        code("");
        prefix("BOMB");
        srvNumReg("1132");
        priceReg("94.00 RUR ex. VAT");
        paramReg("");
        prefix_bonus(1, "BOMB");
        srvNumBonus(1, "1132");
        priceBonus(1, "7.50 RUR ex. VAT");
        paramBonus(1, "");
        prefix_spec("SPEC");
        srvNumSpec("1136");
        priceSpec("50 RUR ex.VAT");
        paramSpec("");
        prefix_subscribe(5, "SUB1");
        srvNumSubscribe(5, "1133");
        priceSubscribe(5, "5.00 RUR ex. VAT");
        durationSubscribe(5, "1");
        prefix_subscribe(6, "SUB2");
        srvNumSubscribe(6, "1134");
        priceSubscribe(6, "25.00 RUR ex. VAT");
        durationSubscribe(6, "7");
        prefix_subscribe(7, "SUB3");
        srvNumSubscribe(7, "1135");
        priceSubscribe(7, "50.00 RUR ex. VAT");
        durationSubscribe(7, "14");
        prefix_subscribe(8, "SUB4");
        srvNumSubscribe(8, "1137");
        priceSubscribe(8, "100.00 RUR ex. VAT");
        durationSubscribe(8, "30");
        specParam1("");
        specParam2("");
        specParam3("");
        specParam4("");
    }

    public final String id() {
        return info(Constants.ctryId);
    }

    public final int id_int() {
        return infoInt(Constants.ctryId);
    }

    public final String name() {
        return info(Constants.ctryName);
    }

    public final String code() {
        return info(Constants.ctryCode);
    }

    public final String prefix() {
        return info(Constants.ctryPrefix);
    }

    public final String prefix_bonus() {
        return prefix_bonus(1);
    }

    public final String prefix_bonus(int bonusType) {
        return info((Config.version_is("1") || bonusType == 1) ? Constants.ctryPrefixBonus1 : bonusType == 2 ? Constants.ctryPrefixBonus2 : bonusType == 3 ? Constants.ctryPrefixBonus3 : bonusType == 4 ? Constants.ctryPrefixBonus4 : Constants.ctryPrefixBonus1);
    }

    public final String prefix_spec() {
        return info(Constants.ctryPrefixSpec);
    }

    public final String prefix_subscribe(int subsType) {
        String str;
        if (subsType != 5) {
            if (subsType == 6) {
                str = Constants.ctryPrefixSubscribeType2;
            } else if (subsType == 7) {
                str = Constants.ctryPrefixSubscribeType3;
            } else if (subsType == 8) {
                str = Constants.ctryPrefixSubscribeType4;
            }
            return info(str);
        }
        str = Constants.ctryPrefixSubscribeType1;
        return info(str);
    }

    public final String srvNumReg() {
        return info(Constants.ctryNumReg);
    }

    public final String srvNumBonus() {
        return srvNumBonus(1);
    }

    public final String srvNumBonus(int bonusType) {
        return info((Config.version_is("1") || bonusType == 1) ? Constants.ctryNumBonus1 : bonusType == 2 ? Constants.ctryNumBonus2 : bonusType == 3 ? Constants.ctryNumBonus3 : bonusType == 4 ? Constants.ctryNumBonus4 : Constants.ctryNumBonus1);
    }

    public final String srvNumSubscribe(int subsType) {
        String str;
        if (subsType != 5) {
            if (subsType == 6) {
                str = Constants.ctryNumSubscribeType2;
            } else if (subsType == 7) {
                str = Constants.ctryNumSubscribeType3;
            } else if (subsType == 8) {
                str = Constants.ctryNumSubscribeType4;
            }
            return info(str);
        }
        str = Constants.ctryNumSubscribeType1;
        return info(str);
    }

    public final String srvNumSpec() {
        return info(Constants.ctryNumSpec);
    }

    public final String priceReg() {
        return info(Constants.ctryPriceReg);
    }

    public final String priceBonus() {
        return priceBonus(1);
    }

    public final String priceBonus(int bonusType) {
        return info((Config.version_is("1") || bonusType == 1) ? Constants.ctryPriceBonus1 : bonusType == 2 ? Constants.ctryPriceBonus2 : bonusType == 3 ? Constants.ctryPriceBonus3 : bonusType == 4 ? Constants.ctryPriceBonus4 : Constants.ctryPriceBonus1);
    }

    public final String priceSubscribe(int subsType) {
        String str;
        if (subsType != 5) {
            if (subsType == 6) {
                str = Constants.ctryPriceSubscribeType2;
            } else if (subsType == 7) {
                str = Constants.ctryPriceSubscribeType3;
            } else if (subsType == 8) {
                str = Constants.ctryPriceSubscribeType4;
            }
            return info(str);
        }
        str = Constants.ctryPriceSubscribeType1;
        return info(str);
    }

    public final String priceSpec() {
        return info(Constants.ctryPriceSpec);
    }

    public final String durationSubscribe(int subsType) {
        String str;
        if (subsType != 5) {
            if (subsType == 6) {
                str = Constants.ctryDurationSubscribeType2;
            } else if (subsType == 7) {
                str = Constants.ctryDurationSubscribeType3;
            } else if (subsType == 8) {
                str = Constants.ctryDurationSubscribeType4;
            }
            return info(str);
        }
        str = Constants.ctryDurationSubscribeType1;
        return info(str);
    }

    public final String paramReg() {
        return info(Constants.ctryParamReg);
    }

    public final String paramBonus(int bonusType) {
        return info((Config.version_is("1") || bonusType == 1) ? Constants.ctryParamBonus1 : bonusType == 2 ? Constants.ctryParamBonus2 : bonusType == 3 ? Constants.ctryParamBonus3 : bonusType == 4 ? Constants.ctryParamBonus4 : Constants.ctryParamBonus1);
    }

    public final String paramSpec() {
        return info(Constants.ctryParamSpec);
    }

    public final String specParam1() {
        return info(Constants.ctrySpecParam1);
    }

    public final String specParam2() {
        return info(Constants.ctrySpecParam2);
    }

    public final String specParam3() {
        return info(Constants.ctrySpecParam3);
    }

    public final String specParam4() {
        return info(Constants.ctrySpecParam4);
    }

    public final String id(String val) {
        return info(Constants.ctryId, val);
    }

    public final String name(String val) {
        return info(Constants.ctryName, val);
    }

    public final String code(String val) {
        return info(Constants.ctryCode, val);
    }

    public final String prefix(String val) {
        return info(Constants.ctryPrefix, val);
    }

    public final String prefix_spec(String val) {
        return info(Constants.ctryPrefixSpec, val);
    }

    public final String prefix_bonus(int bonusType, String val) {
        return info((Config.version_is("1") || bonusType == 1) ? Constants.ctryPrefixBonus1 : bonusType == 2 ? Constants.ctryPrefixBonus2 : bonusType == 3 ? Constants.ctryPrefixBonus3 : bonusType == 4 ? Constants.ctryPrefixBonus4 : Constants.ctryPrefixBonus1, val);
    }

    public final String prefix_subscribe(int subsType, String val) {
        String str;
        if (subsType != 5) {
            if (subsType == 6) {
                str = Constants.ctryPrefixSubscribeType2;
            } else if (subsType == 7) {
                str = Constants.ctryPrefixSubscribeType3;
            } else if (subsType == 8) {
                str = Constants.ctryPrefixSubscribeType4;
            }
            return info(str, val);
        }
        str = Constants.ctryPrefixSubscribeType1;
        return info(str, val);
    }

    public final String srvNumReg(String val) {
        return info(Constants.ctryNumReg, val);
    }

    public final String srvNumBonus(int bonusType, String val) {
        return info((Config.version_is("1") || bonusType == 1) ? Constants.ctryNumBonus1 : bonusType == 2 ? Constants.ctryNumBonus2 : bonusType == 3 ? Constants.ctryNumBonus3 : bonusType == 4 ? Constants.ctryNumBonus4 : Constants.ctryNumBonus1, val);
    }

    public final String srvNumSpec(String val) {
        return info(Constants.ctryNumSpec, val);
    }

    public final String srvNumSubscribe(int subsType, String val) {
        String str;
        if (subsType != 5) {
            if (subsType == 6) {
                str = Constants.ctryNumSubscribeType2;
            } else if (subsType == 7) {
                str = Constants.ctryNumSubscribeType3;
            } else if (subsType == 8) {
                str = Constants.ctryNumSubscribeType4;
            }
            return info(str, val);
        }
        str = Constants.ctryNumSubscribeType1;
        return info(str, val);
    }

    public final String priceReg(String val) {
        return info(Constants.ctryPriceReg, val);
    }

    public final String priceBonus(int bonusType, String val) {
        return info((Config.version_is("1") || bonusType == 1) ? Constants.ctryPriceBonus1 : bonusType == 2 ? Constants.ctryPriceBonus2 : bonusType == 3 ? Constants.ctryPriceBonus3 : bonusType == 4 ? Constants.ctryPriceBonus4 : Constants.ctryPriceBonus1, val);
    }

    public final String priceSpec(String val) {
        return info(Constants.ctryPriceSpec, val);
    }

    public final String priceSubscribe(int subsType, String val) {
        String str;
        if (subsType != 5) {
            if (subsType == 6) {
                str = Constants.ctryPriceSubscribeType2;
            } else if (subsType == 7) {
                str = Constants.ctryPriceSubscribeType3;
            } else if (subsType == 8) {
                str = Constants.ctryPriceSubscribeType4;
            }
            return info(str, val);
        }
        str = Constants.ctryPriceSubscribeType1;
        return info(str, val);
    }

    public final String durationSubscribe(int subsType, String val) {
        String str;
        if (subsType != 5) {
            if (subsType == 6) {
                str = Constants.ctryDurationSubscribeType2;
            } else if (subsType == 7) {
                str = Constants.ctryDurationSubscribeType3;
            } else if (subsType == 8) {
                str = Constants.ctryDurationSubscribeType4;
            }
            return info(str, val);
        }
        str = Constants.ctryDurationSubscribeType1;
        return info(str, val);
    }

    public final String paramReg(String val) {
        return info(Constants.ctryParamReg, val);
    }

    public final String paramBonus(int bonusType, String val) {
        return info((Config.version_is("1") || bonusType == 1) ? Constants.ctryParamBonus1 : bonusType == 2 ? Constants.ctryParamBonus2 : bonusType == 3 ? Constants.ctryParamBonus3 : bonusType == 4 ? Constants.ctryParamBonus4 : Constants.ctryParamBonus1, val);
    }

    public final String paramSpec(String val) {
        return info(Constants.ctryParamSpec, val);
    }

    public final String specParam1(String val) {
        return info(Constants.ctrySpecParam1, val);
    }

    public final String specParam2(String val) {
        return info(Constants.ctrySpecParam2, val);
    }

    public final String specParam3(String val) {
        return info(Constants.ctrySpecParam3, val);
    }

    public final String specParam4(String val) {
        return info(Constants.ctrySpecParam4, val);
    }

    public final int addCountry(Country country) {
        return addCountry(country.id_int(), country.name(), country.code());
    }

    public final int addCountry(int id, String name, String code) {
        int size = countriesList.size();
        countriesList.setSize(size + 1);
        countriesList.setElementAt(new CountryShort(id, name, code), size);
        countriesIdList.put(new Integer(id), new Integer(size));
        return size;
    }

    public final int mapCountry(int num) {
        if (num < 0 || num >= countriesList.size()) {
            return -1;
        }
        CountryShort elementAt = countriesList.elementAt(num);
        countriesNamesList.put(elementAt.name(), new Integer(elementAt.id()));
        countriesCodesList.put(elementAt.code(), new Integer(elementAt.id()));
        return num;
    }

    public final Country readFromArray(int id1, String[] arr) {
        id(String.valueOf(id1));
        readFieldsFromArray(CntryFieldsOrder.get("CFG_" + Config.version()), arr);
        return this;
    }

    public final int readCountriesList_from_CFG(String[] cfg) {
        Country country = new Country();
        clearCountries();
        clearMaps();
        for (int i = 1; i < cfg.length; i++) {
            String[] split = Util.split(Constants.cfgSeparator, cfg[i]);
            country.readFromArray(i, split);
            if (split.length > 1) {
                mapCountry(addCountry(country));
            }
        }
        return countriesList.size();
    }

    public final int readCountriesList(String[] cfg) {
        return readCountriesList_from_CFG(cfg);
    }

    public final Country findMappedCountry_in_CFG(String[] cfg, String cname, String ccode) {
        int i = -1;
        if (ccode.compareTo("") != 0 && countriesCodesList.containsKey(ccode)) {
            i = countriesCodesList.get(ccode).intValue();
        } else if (cname.compareTo("") != 0 && countriesNamesList.containsKey(cname)) {
            i = countriesNamesList.get(cname).intValue();
        }
        if (i >= 0) {
            return readCountry_from_CFG(cfg, i);
        }
        return null;
    }

    public final CountryShort getCountry_from_countriesList_by_id(int id) {
        return countriesList.elementAt(countriesIdList.get(new Integer(id)).intValue());
    }

    public final Country readCountry_from_CFG(String[] cfg, int id) {
        if (id >= cfg.length || id < 0) {
            return null;
        }
        Country country = new Country();
        country.init(this.context);
        country.readFromArray(id, Util.split(Constants.cfgSeparator, cfg[id]));
        return country;
    }

    public final Country readCountry_by_id(String[] cfg, int id) {
        return readCountry_from_CFG(cfg, id);
    }

    public static final int readCountry_ID_by_NUM(int num) {
        if (num < 0 || num >= countriesList.size()) {
            return -1;
        }
        return countriesList.elementAt(num).id();
    }

    public static final int readCountry_NUM_by_ID(int id) {
        Integer num = countriesIdList.get(new Integer(id));
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    public final void clearCountries() {
        countriesList.setSize(0);
        countriesIdList.clear();
    }

    public final void clearMaps() {
        countriesCodesList.clear();
        countriesNamesList.clear();
    }
}
