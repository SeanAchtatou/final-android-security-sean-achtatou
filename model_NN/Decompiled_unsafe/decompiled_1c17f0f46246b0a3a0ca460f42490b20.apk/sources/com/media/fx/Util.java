package com.media.fx;

import android.content.Context;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.util.Random;

public class Util {
    private static Random a = new Random();
    private static String b = "0123456789ABCDEF";
    private static String c = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String[] split(String separator, String str) {
        int i = 0;
        int i2 = 1;
        int i3 = 0;
        while (true) {
            i3 = str.indexOf(separator, i3) + 1;
            if (i3 <= 0) {
                break;
            }
            i2++;
        }
        String[] strArr = new String[i2];
        int i4 = 0;
        int i5 = 0;
        while (true) {
            i4 = str.indexOf(separator, i4) + 1;
            if (i4 > 0) {
                strArr[i5] = str.substring(i, i4 - 1);
                i5++;
                i = (i4 - 1) + separator.length();
            } else {
                strArr[i5] = str.substring(i);
                return strArr;
            }
        }
    }

    public static final String format(String str, String[] args) {
        return format(str, args, '%');
    }

    public static final String format(String str, String[] args, char ch) {
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer("");
        int i2 = 0;
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (charAt != ch) {
                stringBuffer.append(charAt);
            } else if (i + 1 >= str.length() || str.charAt(i + 1) != ch) {
                stringBuffer.append(args[i2]);
                i2++;
            } else {
                stringBuffer.append(charAt);
                i++;
            }
            i++;
        }
        return stringBuffer.toString();
    }

    public static final String[] format(String[] strs, String[][] args) {
        String[] strArr = new String[strs.length];
        for (int i = 0; i < strs.length; i++) {
            strArr[i] = format(strs[i], args[i]);
        }
        return strArr;
    }

    public static final int getCheckSum(String str) {
        return getCheckSum(str, 0, 0);
    }

    public static final int getCheckSum(String str, int beginFrom) {
        return getCheckSum(str, beginFrom, 0);
    }

    public static final int getCheckSum(String str, int beginFrom, int version) {
        char c2 = 0;
        if (version == 0) {
            while (beginFrom < str.length()) {
                c2 = (((((c2 & 128) >> 7) | (c2 << 1)) & 255) ^ (str.charAt(beginFrom) & 255)) & 255;
                beginFrom++;
            }
        }
        return c2;
    }

    public static final String extractCheckSum(String separator, String str) {
        int indexOf = str.indexOf(separator);
        return indexOf >= 0 ? str.substring(0, indexOf - 1) : str;
    }

    public static String[] readResourceAsArr(Context mid, String resName) {
        return readResourceAsArr(mid, resName, true);
    }

    public static String[] readResourceAsArr(Context mid, String resName, boolean decr) {
        return split("\n", readResource(mid, resName, decr));
    }

    public static String readResource(Context mid, String resName) {
        return readResource(mid, resName, true);
    }

    public static String readResource(Context mid, String resName, boolean decr) {
        try {
            InputStream open = mid.getAssets().open(resName);
            char[] cArr = new char[((open.available() / 2) - 1)];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = open.read();
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(read);
            }
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);
            dataInputStream.readChar();
            for (int i = 0; i < cArr.length; i++) {
                cArr[i] = dataInputStream.readChar();
            }
            dataInputStream.close();
            byteArrayInputStream.close();
            byteArrayOutputStream.close();
            String str = new String(cArr);
            if (decr) {
                return decrypt(str, 0);
            }
            return str;
        } catch (Exception e) {
            return null;
        }
    }

    public static final int rnd(int min, int max) {
        return (Math.abs(a.nextInt()) % (max - min)) + min;
    }

    public static final long generateRandomKey(int enc_ver) {
        if (enc_ver != 0) {
            return 0;
        }
        Integer.parseInt(Config.distInfo.smsKeyVersion());
        int i = 1;
        int i2 = 9;
        for (int i3 = 0; i3 < 5; i3++) {
            i *= 10;
            i2 = (i2 * 10) + 9;
        }
        return (long) rnd(i, i2);
    }

    public static final String generateKeyOfType(int t, String prefix, long rndKey) {
        boolean z = false;
        if (Config.versionInt() > 1) {
            Integer.parseInt(Config.distInfo.smsKeyVersion());
            String channelId = Config.distInfo.channelId();
            String substring = channelId.substring(0, 2);
            String substring2 = channelId.substring(2, 5);
            if (rndKey == 0) {
                rndKey = generateRandomKey(0);
            }
            String encryptLight = encryptLight(toBase36((10000 * rndKey) + (Long.parseLong(substring2) * 10) + ((long) (t % 10)), 7), 2);
            try {
                z = Long.parseLong(encryptLight) == 0;
            } catch (Exception e) {
            }
            return z ? "" : prefix + substring + encryptLight + Config.sms_key_version().substring(0, 1);
        }
        if (rndKey == 0) {
            rndKey = generateRandomKey(0);
        }
        String encryptLight2 = encryptLight(toBase36((10000 * rndKey) + (Long.parseLong(Config.distInfo.id()) * 10) + ((long) (t % 10)), 7), 2);
        try {
            z = Long.parseLong(encryptLight2) == 0;
        } catch (Exception e2) {
        }
        if (z) {
            return "";
        }
        return prefix + encryptLight2 + Config.sms_key_version().substring(0, 1);
    }

    public static byte[] encryptLightByte(byte[] b2, int enc_ver) {
        byte[] bArr = new byte[b2.length];
        if (enc_ver == 0) {
            for (int i = 0; i < b2.length; i++) {
                bArr[(i + 5) % b2.length] = (byte) (b2[i] ^ 37);
            }
            for (int i2 = 0; i2 < b2.length; i2++) {
                bArr[i2] = (byte) (bArr[i2] ^ bArr[(i2 + 2) % b2.length]);
            }
        }
        return bArr;
    }

    public static byte[] decryptLightByte(byte[] b2, int enc_ver) {
        byte[] bArr = new byte[b2.length];
        if (enc_ver == 0) {
            for (int length = b2.length - 1; length >= 0; length--) {
                b2[length] = (byte) (b2[length] ^ b2[(length + 2) % b2.length]);
            }
            for (int i = 0; i < b2.length; i++) {
                bArr[i] = (byte) (b2[(i + 5) % b2.length] ^ 37);
            }
        }
        return bArr;
    }

    public static byte[] packDigits(String s) {
        if (s.length() % 2 == 1) {
            s = s + "0";
        }
        byte[] bArr = new byte[(s.length() / 2)];
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = (byte) (Byte.parseByte(s.substring(i << 1, (i << 1) + 1)) + ((byte) (Byte.parseByte(s.substring((i << 1) + 1, (i << 1) + 2)) << 4)));
        }
        return bArr;
    }

    public static String unpackDigits(byte[] b2) {
        StringBuffer stringBuffer = new StringBuffer("");
        for (int i = 0; i < b2.length; i++) {
            stringBuffer.append(String.valueOf(b.charAt(b2[i] & 15)));
            stringBuffer.append(String.valueOf(b.charAt((b2[i] >> 4) & 15)));
        }
        return stringBuffer.toString();
    }

    public static String encryptLight(String s, int enc_ver) {
        if (s.length() == 0) {
            return s;
        }
        if (enc_ver == 0) {
            return new String(encryptLightByte(s.getBytes(), enc_ver));
        }
        if (enc_ver == 1) {
            return unpackDigits(encryptLightByte(packDigits(s), 0));
        }
        if (enc_ver != 2 || s.length() <= 2) {
            return s;
        }
        StringBuffer stringBuffer = new StringBuffer(s.substring(0, 2));
        for (int i = 0; i < s.length() - 2; i++) {
            stringBuffer.append(s.charAt(((c.indexOf(s.charAt(1)) + i) % (s.length() - 2)) + 2));
        }
        return stringBuffer.toString();
    }

    public static String decryptLight(String s, int enc_ver) {
        if (enc_ver == 0) {
            return new String(decryptLightByte(s.getBytes(), enc_ver));
        }
        if (enc_ver == 1) {
            return unpackDigits(decryptLightByte(packDigits(s), 0));
        }
        if (enc_ver != 2 || s.length() <= 2) {
            return s;
        }
        StringBuffer stringBuffer = new StringBuffer(s.substring(0, 2));
        int length = s.length() - 2;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(s.charAt((((i + length) - (c.indexOf(s.charAt(1)) % length)) % length) + 2));
        }
        return stringBuffer.toString();
    }

    public static String toBase36(long l, int targetLength) {
        StringBuffer stringBuffer = new StringBuffer("");
        while (l > 0) {
            stringBuffer.append(c.charAt((int) (l % 36)));
            l /= 36;
        }
        while (stringBuffer.length() < targetLength) {
            stringBuffer.append("0");
        }
        return stringBuffer.toString();
    }

    public static long fromBase36(String s) {
        long j = 0;
        long j2 = 1;
        int i = 0;
        while (i < s.length()) {
            j += ((long) c.indexOf(s.charAt(i))) * j2;
            i++;
            j2 *= 36;
        }
        return j;
    }

    public static String decrypt(String str, int i) {
        int i2;
        char c2;
        char[] charArray = str.toCharArray();
        char[] cArr = new char[charArray.length];
        int length = charArray.length + 1;
        for (int i3 = 1; i3 < length; i3++) {
            cArr[i3 - 1] = charArray[i3 - 1];
        }
        int i4 = 1;
        while (i4 < length) {
            i4 = (i4 << 1) | 1;
        }
        int i5 = i4 >> 1;
        for (int i6 = length - i5; i6 > 0; i6 -= (charArray.length - i5 > 0 ? 0 : 1) + (charArray.length - i5)) {
            int i7 = 0;
            int i8 = 4;
            while (true) {
                int i9 = i7;
                int i10 = i8;
                if (i10 > ((i5 + 1) << 1)) {
                    break;
                }
                int i11 = (i5 + 1) >> 1;
                int i12 = 1;
                int i13 = 0;
                int i14 = 0;
                int i15 = 0;
                while (i13 <= i9) {
                    if (i13 < i9 || ((((i10 >> 1) - 2) + (i9 << 1)) - i15) - (i5 >> 1) > 0) {
                        i2 = (((((i10 >> 1) + -2) & i12) == 0 ? 1 : -1) * (i11 >> 1)) + 2 + i14;
                        i14 = i2;
                    } else {
                        int i16 = ((i10 >> 1) + i6) - 3;
                        char c3 = cArr[i16];
                        char c4 = cArr[((i6 - 1) - (i9 << 1)) + i15 + (i5 >> 1)];
                        char c5 = cArr[((i10 >> 1) + i6) - 3];
                        cArr[((i6 - 1) - (i9 << 1)) + i15 + (i5 >> 1)] = c5;
                        char c6 = (char) (c3 + (c4 - c5));
                        cArr[i16] = c6;
                        if (((((i10 >> 1) + (i9 << 1)) - (i5 >> 1)) - i15) - 2 == 0) {
                            c2 = 0;
                        } else {
                            int i17 = cArr[((i10 >> 1) + i6) - 3] + 142;
                            c2 = (char) ((((i17 / 10) * 10) + ((((i17 % 10) + 10) + ((i6 - 3) % 10)) % 10)) - 142);
                            cArr[((i10 >> 1) + i6) - 3] = c2;
                        }
                        int i18 = c2 + c6;
                        int i19 = ((i6 - 1) - (i9 << 1)) + i15 + (i5 >> 1);
                        int i20 = cArr[i15 + ((i6 - 1) - (i9 << 1)) + (i5 >> 1)] + 142;
                        char c7 = (char) ((((i20 / 10) * 10) + ((((i20 % 10) + 10) + ((i6 - 3) % 10)) % 10)) - 142);
                        cArr[i19] = c7;
                        i2 = i18 + c7;
                    }
                    i13++;
                    i12 <<= 1;
                    i11 >>= 1;
                    i15 = i2;
                }
                i7 = ((i10 >> 1) == (2 << i9) ? 1 : 0) + i9;
                i8 = i10 + 2;
            }
        }
        for (int i21 = 1; i21 < length; i21++) {
            charArray[i21 - 1] = cArr[i21 - 1];
        }
        return new String(charArray);
    }
}
