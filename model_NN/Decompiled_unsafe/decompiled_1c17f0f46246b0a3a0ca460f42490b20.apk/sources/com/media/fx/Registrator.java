package com.media.fx;

import android.content.Context;
import android.telephony.TelephonyManager;
import java.util.Date;
import java.util.Random;
import java.util.Vector;

public final class Registrator {
    private static TelephonyManager c = null;
    private static long d = 0;
    private static long e = 0;
    private int a = 0;
    private String b = "";
    public Config cfg;

    public Registrator(Context ctx) {
        RecordStore.init(ctx);
        this.cfg = new Config(ctx);
        this.cfg.readConfig();
        isSubscribed();
        c = (TelephonyManager) ctx.getSystemService("phone");
    }

    public static String CheckSecurity(Context ctx) {
        RecordStore.init(ctx);
        e = RMS.LastModified();
        d = RMS.LastModified_current();
        if (RMS.RunsCount_Update() == 1) {
            RMS.Platform_Update();
            return "";
        } else if (RMS.Platform().compareTo(System.getProperty("java.vm.name")) != 0) {
            return "" + "+platform";
        } else {
            return "";
        }
    }

    public static String getLm() {
        return "" + e;
    }

    public static String getLmc() {
        return "" + d;
    }

    public static String getLmcMinusLm() {
        return "" + (d - e);
    }

    public static boolean CheckLastModified() {
        long LastModified = RMS.LastModified();
        long LastModified_current = RMS.LastModified_current();
        return LastModified == 0 || (LastModified_current >= LastModified && LastModified_current - LastModified < 500);
    }

    public static boolean SimCardExists() {
        return c.getSimState() == 5;
    }

    public final boolean setCountry(int country) {
        return this.cfg.setCountry(country);
    }

    public final Vector<CountryShort> getCountriesList() {
        return Country.countriesList;
    }

    public final Country getCountryInfo() {
        return Config.countryInfo;
    }

    public final String getActivationKey() {
        return getActivationKey("");
    }

    public final String getActivationKey(String smsTail) {
        String str;
        if (smsTail == null) {
            smsTail = "";
        }
        RMS.RMS_save(Constants.RMS_store_name_generalInfo, 24, Config.countryInfo.code());
        if (getRegistrationType() == -1) {
            setRegistrationType(0);
        }
        long ActivationKey = RMS.ActivationKey();
        String prefix = Config.countryInfo.prefix();
        if (ActivationKey == 0) {
            ActivationKey = RMS.ActivationKey(Util.generateRandomKey(0));
        }
        String generateKeyOfType = Util.generateKeyOfType(0, prefix, ActivationKey);
        if (generateKeyOfType.compareTo("") == 0) {
            this.a = 1;
            this.b = Constants.errors[this.a];
        }
        if (generateKeyOfType.compareTo("") == 0) {
            str = "";
        } else {
            str = generateKeyOfType + (smsTail.compareTo("") == 0 ? "" : Config.distInfo.smsCustomTailSeparator() + smsTail);
        }
        return str.toLowerCase();
    }

    public final String getSubscriptionKey(int subsType) {
        return getSubscriptionKey(subsType, "");
    }

    public final String getSubscriptionKey(int subsType, String smsTail) {
        String str;
        if (smsTail == null) {
            smsTail = "";
        }
        long SubscriptionKey = RMS.SubscriptionKey();
        String prefix_subscribe = Config.countryInfo.prefix_subscribe(subsType);
        if (SubscriptionKey == 0) {
            SubscriptionKey = RMS.SubscriptionKey(Util.generateRandomKey(0));
        }
        String generateKeyOfType = Util.generateKeyOfType(subsType, prefix_subscribe, SubscriptionKey);
        if (generateKeyOfType.compareTo("") == 0) {
            str = "";
        } else {
            str = generateKeyOfType + (smsTail.compareTo("") == 0 ? "" : Config.distInfo.smsCustomTailSeparator() + smsTail);
        }
        return str.toLowerCase();
    }

    public final boolean requestSerial() {
        return requestSerial("");
    }

    public final boolean requestSerial(String smsTail) {
        setRegistrationType(0);
        return sendSMS(Config.countryInfo.srvNumReg(), getActivationKey(smsTail));
    }

    public final boolean requestSubscription(int subsType) {
        return requestSubscription(subsType, "");
    }

    public final boolean requestSubscription(int subsType, String smsTail) {
        boolean z = setRegistrationType(subsType) == subsType;
        if (z) {
            return sendSMS(Config.countryInfo.srvNumSubscribe(subsType), getSubscriptionKey(subsType, smsTail));
        }
        return z;
    }

    public final boolean checkSerial(String sn) {
        return checkSerial(sn, getRegistrationType());
    }

    public final boolean checkSerial(String sn, int regType) {
        boolean z = true;
        boolean z2 = (regType < 0 || sn == null || sn.compareTo("") == 0) ? false : true;
        if (!z2) {
            return z2;
        }
        RMS.RMS_save(Constants.RMS_store_name_generalInfo, 21, sn);
        RMS.EnteredKey(sn);
        if (regType == 0) {
            RMS.EnteredKeyActivation(sn);
            if (!isRegistered() || getConfigStatus(sn).compareTo(parseVal("good")) != 0) {
                z = false;
            }
        } else if (regType == 5 || regType == 6 || regType == 7 || regType == 8) {
            boolean isSubscribed = isSubscribed(false);
            if (isSubscribed) {
                if (SubscribtionMode() == 0) {
                    Date date = new Date();
                    date.setTime(date.getTime() + (Long.parseLong(getCountryInfo().durationSubscribe(regType)) * 24 * 60 * 60 * 1000));
                    RMS.ExpirationDate(date.getTime());
                    z = isSubscribed;
                } else if (SubscribtionMode() == 1) {
                    RMS.SubscriptionKey(0);
                    z = isSubscribed;
                }
            }
            z = isSubscribed;
        } else {
            z = false;
        }
        if (z) {
            return z;
        }
        RMS.EnteredKey("");
        if (regType != 0) {
            return z;
        }
        RMS.EnteredKeyActivation("");
        RMS.RMS_save(Constants.RMS_store_name_generalInfo, 21, "");
        return z;
    }

    public final String testChSn() {
        return (String) RMS.RMS_load(Constants.RMS_store_name_generalInfo, 21, new String(""));
    }

    public final String testChAk() {
        return (String) RMS.RMS_load(Constants.RMS_store_name_generalInfo, 24, new String(""));
    }

    public final boolean isRegistred() {
        return isRegistered();
    }

    public final boolean isRegistered() {
        try {
            if (Long.parseLong(Util.decrypt(RMS.EnteredKeyActivation(), 0)) == RMS.ActivationKey()) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public final String getConfigMode() {
        boolean z;
        int i;
        int i2;
        int i3;
        try {
            new char[1][0] = 0;
            if (Long.parseLong(Util.decrypt(RMS.EnteredKeyActivation(), 0)) == RMS.ActivationKey()) {
                z = true;
            } else {
                z = false;
            }
            char[] charArray = String.valueOf(new Boolean(z).toString()).toCharArray();
            int length = charArray.length - 1;
            int i4 = 0;
            while (true) {
                if (isRegistered()) {
                    i = -1;
                } else {
                    i = 0;
                }
                if (length <= i) {
                    break;
                }
                i4 += ((charArray[length] << 1) - (1 / charArray.length)) + (charArray[length] - 12);
                length--;
            }
            if (i4 == 0) {
                i2 = 1;
            } else {
                i2 = (charArray[0] + (((i4 / 3) * charArray[1]) / charArray[3])) - (Long.parseLong(Util.decrypt(RMS.EnteredKeyActivation(), 0)) == RMS.ActivationKey() ? 2 : 5);
            }
            do {
                if (i2 != Integer.parseInt(getTestRes()) && i2 != Integer.parseInt(getTestRess())) {
                    return "" + i2;
                }
                i3 = 0;
                for (int i5 = 0; i5 < charArray.length; i5++) {
                    i3++;
                }
            } while (new Integer(i2).toString().compareTo(parseVal("good")) != 0);
            return "" + (((i3 * 2) + i2) - ((i3 << 2) / 2));
        } catch (Exception e2) {
            return "0";
        }
    }

    /* JADX WARN: Type inference failed for: r5v1, types: [char[]] */
    /* JADX WARN: Type inference failed for: r0v24, types: [char] */
    /* JADX WARN: Type inference failed for: r7v2, types: [char, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char, code=int, for r0v24, types: [char] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String getConfigStatus(java.lang.String r10) {
        /*
            r9 = this;
            r3 = 1
            r1 = 0
            if (r10 != 0) goto L_0x0006
            java.lang.String r10 = "good"
        L_0x0006:
            java.lang.Boolean r2 = new java.lang.Boolean     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r0 = "generalInfo"
            r4 = 23
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r6 = ""
            r5.<init>(r6)     // Catch:{ Exception -> 0x00a1 }
            java.lang.Object r0 = com.media.fx.RMS.RMS_load(r0, r4, r5)     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00a1 }
            r4 = 0
            java.lang.String r0 = com.media.fx.Util.decrypt(r0, r4)     // Catch:{ Exception -> 0x00a1 }
            long r4 = java.lang.Long.parseLong(r0)     // Catch:{ Exception -> 0x00a1 }
            long r6 = com.media.fx.RMS.ActivationKey()     // Catch:{ Exception -> 0x00a1 }
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x0072
            r0 = r3
        L_0x002b:
            r2.<init>(r0)     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x00a1 }
            char[] r5 = r0.toCharArray()     // Catch:{ Exception -> 0x00a1 }
            byte[] r6 = r10.getBytes()     // Catch:{ Exception -> 0x00a1 }
            r0 = r1
            r2 = r1
        L_0x0040:
            int r4 = r6.length     // Catch:{ Exception -> 0x00a1 }
            if (r0 >= r4) goto L_0x0076
            byte r4 = r6[r0]     // Catch:{ Exception -> 0x00a1 }
            int r4 = r4 / 3
            int r7 = java.lang.Math.min(r0, r2)     // Catch:{ Exception -> 0x00a1 }
            int r8 = r5.length     // Catch:{ Exception -> 0x00a1 }
            int r8 = r8 + -1
            int r7 = java.lang.Math.min(r7, r8)     // Catch:{ Exception -> 0x00a1 }
            char r7 = r5[r7]     // Catch:{ Exception -> 0x00a1 }
            int r4 = r4 + r7
            int r4 = r4 + r2
            boolean r7 = r9.isRegistered()     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r2 = r9.getConfigMode()     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r8 = "good"
            java.lang.String r8 = r9.parseVal(r8)     // Catch:{ Exception -> 0x00a1 }
            int r2 = r2.compareTo(r8)     // Catch:{ Exception -> 0x00a1 }
            if (r2 != 0) goto L_0x0074
            r2 = r3
        L_0x006b:
            if (r7 == r2) goto L_0x006e
            r0 = r1
        L_0x006e:
            int r0 = r0 + 1
            r2 = r4
            goto L_0x0040
        L_0x0072:
            r0 = r1
            goto L_0x002b
        L_0x0074:
            r2 = r1
            goto L_0x006b
        L_0x0076:
            r0 = r2
        L_0x0077:
            int r2 = r5.length     // Catch:{ Exception -> 0x00a1 }
            if (r1 >= r2) goto L_0x008b
            if (r1 != 0) goto L_0x007e
            char r0 = r5[r1]     // Catch:{ Exception -> 0x00a1 }
        L_0x007e:
            int r0 = r0 + 1
            int r0 = r0 * 7
            int r0 = r0 / 3
            int r1 = r1 + 1
            if (r1 > 0) goto L_0x0077
            java.lang.String r0 = "0"
        L_0x008a:
            return r0
        L_0x008b:
            r1 = 81
            if (r0 <= r1) goto L_0x0095
            r1 = 4052(0xfd4, float:5.678E-42)
            if (r0 >= r1) goto L_0x0095
            int r0 = r0 / 4
        L_0x0095:
            java.lang.Integer r1 = new java.lang.Integer     // Catch:{ Exception -> 0x00a1 }
            int r0 = r0 + -270
            r1.<init>(r0)     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x00a1 }
            goto L_0x008a
        L_0x00a1:
            r0 = move-exception
            java.lang.String r0 = "0"
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.media.fx.Registrator.getConfigStatus(java.lang.String):java.lang.String");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v20, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v37, resolved type: char} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v38, resolved type: char} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String getMediaAccess() {
        /*
            r13 = this;
            r3 = 1
            r1 = 0
            int r0 = r13.hashCode()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            byte[] r4 = r0.getBytes()
            r0 = r1
            r2 = r1
        L_0x0010:
            int r5 = r4.length
            if (r0 >= r5) goto L_0x0035
            byte r5 = r4[r0]
            int r5 = r5 / 3
            int r6 = java.lang.Math.min(r0, r2)
            byte r6 = r4[r6]
            int r5 = r5 + r6
            int r2 = r2 + r5
            java.lang.String r5 = "test"
            java.lang.String r5 = r13.getConfigStatus(r5)
            java.lang.String r6 = r13.getConfigMode()
            int r5 = r5.compareTo(r6)
            if (r5 == 0) goto L_0x0032
            int r5 = r0 % 25
            int r2 = r2 / r5
        L_0x0032:
            int r0 = r0 + 1
            goto L_0x0010
        L_0x0035:
            java.lang.Boolean r5 = new java.lang.Boolean     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r0 = "generalInfo"
            r6 = 23
            java.lang.String r7 = new java.lang.String     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r8 = ""
            r7.<init>(r8)     // Catch:{ Exception -> 0x00c3 }
            java.lang.Object r0 = com.media.fx.RMS.RMS_load(r0, r6, r7)     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00c3 }
            r6 = 0
            java.lang.String r0 = com.media.fx.Util.decrypt(r0, r6)     // Catch:{ Exception -> 0x00c3 }
            long r6 = java.lang.Long.parseLong(r0)     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r0 = "generalInfo"
            r8 = 22
            java.lang.Long r9 = new java.lang.Long     // Catch:{ Exception -> 0x00c3 }
            r10 = 0
            r9.<init>(r10)     // Catch:{ Exception -> 0x00c3 }
            java.lang.Object r0 = com.media.fx.RMS.RMS_load(r0, r8, r9)     // Catch:{ Exception -> 0x00c3 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Exception -> 0x00c3 }
            long r8 = r0.longValue()     // Catch:{ Exception -> 0x00c3 }
            int r0 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r0 != 0) goto L_0x0092
            r0 = r3
        L_0x006b:
            r5.<init>(r0)     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x00c3 }
            char[] r5 = r0.toCharArray()     // Catch:{ Exception -> 0x00c3 }
            r0 = 1
            char r0 = r5[r0]     // Catch:{ Exception -> 0x00c3 }
        L_0x007d:
            r3 = 25
            if (r0 >= r3) goto L_0x0094
            r3 = 3
            int r6 = r5.length     // Catch:{ Exception -> 0x00c3 }
            int r7 = r4.length     // Catch:{ Exception -> 0x00c3 }
            int r6 = java.lang.Math.min(r6, r7)     // Catch:{ Exception -> 0x00c3 }
            int r6 = r6 << 1
            int r3 = r3 / r6
            int r6 = r0 % 2
            int r3 = r3 * r6
            int r2 = r2 + r3
            int r0 = r0 + 1
            goto L_0x007d
        L_0x0092:
            r0 = r1
            goto L_0x006b
        L_0x0094:
            int r0 = r5.length     // Catch:{ Exception -> 0x00c3 }
            int r0 = r0 + -1
            r12 = r0
            r0 = r2
            r2 = r12
        L_0x009a:
            int r3 = r2 + 1
            int r4 = r5.length     // Catch:{ Exception -> 0x00c3 }
            if (r3 != r4) goto L_0x00a0
            r0 = r1
        L_0x00a0:
            char r3 = r5[r2]     // Catch:{ Exception -> 0x00c3 }
            int r3 = r3 / 2
            int r0 = r0 + r3
            int r3 = r0 * 3
            int r0 = r2 + -1
            if (r0 > 0) goto L_0x00c7
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c3 }
            r0.<init>()     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00c3 }
            int r1 = r3 + 361
            int r2 = r5.length     // Catch:{ Exception -> 0x00c3 }
            int r1 = r1 / r2
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00c3 }
        L_0x00c2:
            return r0
        L_0x00c3:
            r0 = move-exception
            java.lang.String r0 = "0"
            goto L_0x00c2
        L_0x00c7:
            r2 = r0
            r0 = r3
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.media.fx.Registrator.getMediaAccess():java.lang.String");
    }

    public final String parseVal(String val) {
        int i = 1;
        char[] charArray = String.valueOf(true).toCharArray();
        if (val != null) {
            val.charAt(0);
        }
        int i2 = 0;
        for (int length = charArray.length - 1; length >= 0; length--) {
            i2 += ((charArray[length] << 1) - (1 / charArray.length)) + (charArray[length] - 12);
        }
        if (i2 != 0) {
            i = (((charArray[1] * (i2 / 3)) / charArray[3]) + charArray[0]) - 2;
        }
        return "" + i;
    }

    public final String getTestRes() {
        int i = 0;
        Integer num = new Integer(1);
        Random random = new Random();
        int i2 = 0;
        Integer num2 = num;
        int i3 = 0;
        while (i3 < Math.abs(Math.min(Math.max(random.nextInt(), 302), random.nextInt())) + 7 && i2 < 5) {
            i3 += 2;
            i2++;
            num2 = new Integer(num2.intValue() + 97);
        }
        for (int i4 = 0; i4 < 5; i4++) {
            i++;
        }
        return new Integer((i * 7) + num2.intValue()).toString();
    }

    public final String getTestRess() {
        if ((3 - (getTestRes().length() * 7)) + 1 == 15) {
            return "wrong";
        }
        int intValue = Integer.valueOf(getTestRes()).intValue() + 7;
        int i = 0;
        do {
            i--;
            intValue -= i;
        } while (i > -4);
        return "" + (intValue - ((String.valueOf(false).length() + 2) * 9));
    }

    public final boolean isSubscribed() {
        return isSubscribed(true);
    }

    public final boolean isSubscribed(boolean checkExpirationDate) {
        boolean z;
        try {
            z = Long.parseLong(Util.decrypt(RMS.EnteredKey(), 0)) == RMS.SubscriptionKey();
        } catch (Exception e2) {
            z = false;
        }
        boolean z2 = (SubscribtionMode() == 1 || !checkExpirationDate) ? true : new Date().getTime() <= RMS.ExpirationDate();
        if (!z || !z2) {
            return false;
        }
        return true;
    }

    public final int SubscribtionMode() {
        return RMS.SubscribtionMode();
    }

    public final int SubscribtionMode(int mode) {
        return RMS.SubscribtionMode(mode);
    }

    public final boolean isActivated() {
        return (isRegistered() && getConfigMode().compareTo(parseVal("good")) == 0) || isSubscribed();
    }

    public final String getBonusKey(int bonusType) {
        return getBonusKey(bonusType, "");
    }

    public final String getBonusKey(int bonusType, String smsTail) {
        return getBonusKey(bonusType, smsTail, Util.generateRandomKey(0));
    }

    public final String getBonusKey(int bonusType, String smsTail, long bonusKey) {
        String str;
        if (smsTail == null) {
            smsTail = "";
        }
        String generateKeyOfType = Util.generateKeyOfType(bonusType, Config.countryInfo.prefix_bonus(bonusType), bonusKey);
        if (generateKeyOfType.compareTo("") == 0) {
            str = "";
        } else {
            str = generateKeyOfType + (smsTail.compareTo("") == 0 ? "" : Config.distInfo.smsCustomTailSeparator() + smsTail);
        }
        return str.toLowerCase();
    }

    public final boolean requestBonus(int bonusType) {
        return requestBonus(bonusType, false, "");
    }

    public final boolean requestBonus(int bonusType, String smsTail) {
        return requestBonus(bonusType, false, smsTail);
    }

    public final boolean requestBonus(int bonusType, boolean bonusActivationRequired) {
        return requestBonus(bonusType, bonusActivationRequired, "");
    }

    public final boolean requestBonus(int bonusType, boolean bonusActivationRequired, String smsTail) {
        long generateRandomKey = Util.generateRandomKey(0);
        if (!sendSMS(Config.countryInfo.srvNumBonus(bonusType), getBonusKey(bonusType, smsTail, generateRandomKey))) {
            return false;
        }
        if (bonusActivationRequired) {
            RMS.RememberBonus(generateRandomKey, bonusType);
        }
        return true;
    }

    public final boolean checkBonus(String code) {
        return RMS.CheckBonus(Long.parseLong(code)) != 0;
    }

    public final boolean sendSMS(String number, String text) {
        try {
            if (text.compareTo("") != 0) {
                return SMS.send(number, text);
            }
            this.a = 1;
            this.b = Constants.errors[this.a];
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public final String getGameLink() {
        return Config.distInfo.gameLink();
    }

    public final String getGameLinkTitle() {
        return Config.distInfo.gameLinkTitle();
    }

    public final String getMoreGamesTitle() {
        return Config.distInfo.moreGamesTitle();
    }

    public final String getMoreGamesLink() {
        return Config.distInfo.moreGamesLink();
    }

    public final boolean sendFriendSms(String number) {
        return sendFriendSms(number, false);
    }

    public final boolean sendFriendSms(String str, boolean z) {
        return false;
    }

    public final int getSendFriendSmsAttemptsCounter() {
        return RMS.SendFriendAttemptsCounter();
    }

    public final long getExpirationDate() {
        return RMS.ExpirationDate();
    }

    public final int getRegistrationType() {
        return RMS.RegistrationType();
    }

    public final int setRegistrationType(int regType) {
        return RMS.RegistrationType(regType);
    }

    public final int getActivationKeySize() {
        return Integer.parseInt(Config.distInfo.keySize_Register());
    }

    public final int getBonusKeySize() {
        return Integer.parseInt(Config.distInfo.keySize_Bonus());
    }

    public final int getCurrentSubscribtionKeySize() {
        return getSubscribtionKeySize(getRegistrationType());
    }

    public final int getSubscribtionKeySize(int subsType) {
        String valueOf = String.valueOf(getActivationKeySize());
        if (subsType == 5) {
            valueOf = Config.distInfo.keySize_SubscribtionType1();
        } else if (subsType == 6) {
            valueOf = Config.distInfo.keySize_SubscribtionType2();
        } else if (subsType == 7) {
            valueOf = Config.distInfo.keySize_SubscribtionType3();
        } else if (subsType == 8) {
            valueOf = Config.distInfo.keySize_SubscribtionType4();
        }
        return Integer.parseInt(valueOf);
    }

    public static final String getRegistratorVersion() {
        return "1";
    }

    public final String getConfigVersion() {
        return Config.version();
    }

    public final String getSmsCodeVersion() {
        return Config.sms_key_version();
    }

    public final int getDemoModeElapsed() {
        return RMS.DemoModeElapsed();
    }

    public final int setDemoModeElapsed(int minutes) {
        return RMS.DemoModeElapsed(minutes);
    }

    public final int getErrorCode() {
        return this.a;
    }

    public final String getErrorText() {
        return this.b;
    }
}
