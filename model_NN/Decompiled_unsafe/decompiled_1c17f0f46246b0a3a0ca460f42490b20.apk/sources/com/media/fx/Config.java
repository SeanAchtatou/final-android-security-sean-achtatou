package com.media.fx;

import android.content.Context;

public class Config implements Constants {
    private static String a = "0";
    private static Context b = null;
    private static String[] c = null;
    public static final int cfgScheme = 1;
    public static Country countryInfo = new Country();
    private static int d = 0;
    public static Distributer distInfo = new Distributer();

    public Config(Context ctx) {
        b = ctx;
        distInfo.init(ctx);
        countryInfo.init(ctx);
    }

    public static String version() {
        return a;
    }

    public static int versionInt() {
        return Integer.parseInt(a);
    }

    public static boolean version_is(String v) {
        return v.compareTo(a) == 0;
    }

    public static String sms_key_version() {
        return distInfo.smsKeyVersion();
    }

    public static String[] cfgContents() {
        if (c == null) {
            c = Util.readResourceAsArr(b, Constants.cfgFileName);
            d = 0;
        }
        d++;
        return c;
    }

    public static void release_cfgContents() {
        if (c != null) {
            d--;
        }
        if (d == 0) {
            c = null;
            d = 0;
        }
    }

    public final boolean setCountry(int country_num) {
        if (country_num < 0) {
            return false;
        }
        Country readCountry_by_id = countryInfo.readCountry_by_id(cfgContents(), Country.readCountry_ID_by_NUM(country_num));
        release_cfgContents();
        RMS.CurrentCountry(readCountry_by_id == null ? -1 : Country.readCountry_NUM_by_ID(readCountry_by_id.id_int()));
        if (readCountry_by_id != null) {
            countryInfo = readCountry_by_id;
        } else {
            countryInfo.id("-1");
        }
        return RMS.CurrentCountry() != -1;
    }

    public final boolean readConfig() {
        return readConfig(RMS.CurrentCountry());
    }

    private static String a() {
        a = Util.split(Constants.cfgSeparator, cfgContents()[0])[0];
        release_cfgContents();
        return a;
    }

    public final boolean readConfig(int country) {
        String[] cfgContents = cfgContents();
        a();
        distInfo.readConfig_from_CFG(cfgContents);
        if (countryInfo.readCountriesList(cfgContents) <= country) {
            country = -1;
        }
        release_cfgContents();
        return setCountry(country);
    }
}
