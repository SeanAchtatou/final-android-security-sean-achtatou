package com.android.marketplay.app;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import com.android.a.a.YrZKkcheugaIkBUkbvzpSvvzKHepOcDW;
import java.util.concurrent.atomic.AtomicBoolean;

public class USvc extends Service {
    private static final String z;
    BroadcastReceiver a = new a(this);
    private final YrZKkcheugaIkBUkbvzpSvvzKHepOcDW b = new b(this);
    /* access modifiers changed from: private */
    public AtomicBoolean c = new AtomicBoolean(false);

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0027, code lost:
        if (r1 > r2) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0029, code lost:
        com.android.marketplay.app.USvc.z = new java.lang.String(r0).intern();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0035, code lost:
        r5 = 'A';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0038, code lost:
        r5 = 'D';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        r5 = 'i';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003e, code lost:
        r5 = 'c';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0009, code lost:
        if (r1 <= 1) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000b, code lost:
        r3 = r0;
        r4 = r2;
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0010, code lost:
        r6 = r1[r2];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0014, code lost:
        switch((r4 % 5)) {
            case 0: goto L_0x0035;
            case 1: goto L_0x0038;
            case 2: goto L_0x003b;
            case 3: goto L_0x003e;
            default: goto L_0x0017;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        r5 = '{';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0019, code lost:
        r1[r2] = (char) (r5 ^ r6);
        r2 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001f, code lost:
        if (r0 != 0) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0021, code lost:
        r1 = r3;
        r4 = r2;
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0025, code lost:
        r1 = r0;
        r0 = r3;
     */
    static {
        /*
            java.lang.String r0 = " *\r\u0011\u0014( G\n\u00155!\u0007\u0017U '\u001d\n\u0014/j\u001c\u0010\b%j\u0006\r"
            char[] r0 = r0.toCharArray()
            int r1 = r0.length
            r2 = 0
            r3 = 1
            if (r1 > r3) goto L_0x0027
        L_0x000b:
            r3 = r0
            r4 = r2
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0010:
            char r6 = r1[r2]
            int r5 = r4 % 5
            switch(r5) {
                case 0: goto L_0x0035;
                case 1: goto L_0x0038;
                case 2: goto L_0x003b;
                case 3: goto L_0x003e;
                default: goto L_0x0017;
            }
        L_0x0017:
            r5 = 123(0x7b, float:1.72E-43)
        L_0x0019:
            r5 = r5 ^ r6
            char r5 = (char) r5
            r1[r2] = r5
            int r2 = r4 + 1
            if (r0 != 0) goto L_0x0025
            r1 = r3
            r4 = r2
            r2 = r0
            goto L_0x0010
        L_0x0025:
            r1 = r0
            r0 = r3
        L_0x0027:
            if (r1 > r2) goto L_0x000b
            java.lang.String r1 = new java.lang.String
            r1.<init>(r0)
            java.lang.String r0 = r1.intern()
            com.android.marketplay.app.USvc.z = r0
            return
        L_0x0035:
            r5 = 65
            goto L_0x0019
        L_0x0038:
            r5 = 68
            goto L_0x0019
        L_0x003b:
            r5 = 105(0x69, float:1.47E-43)
            goto L_0x0019
        L_0x003e:
            r5 = 99
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.marketplay.app.USvc.<clinit>():void");
    }

    public IBinder onBind(Intent intent) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(z);
        registerReceiver(this.a, intentFilter);
        return this.b;
    }
}
