package b;

import b.a.i;
import c.f;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    public static final g f486a = new a().a();

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, Set<f>> f487b;

    public static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final Map<String, Set<f>> f488a = new LinkedHashMap();

        public g a() {
            return new g(this);
        }
    }

    private g(a aVar) {
        this.f487b = i.a(aVar.f488a);
    }

    private static f a(X509Certificate x509Certificate) {
        return i.a(f.a(x509Certificate.getPublicKey().getEncoded()));
    }

    public static String a(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha1/" + a((X509Certificate) certificate).b();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    /* access modifiers changed from: package-private */
    public Set<f> a(String str) {
        Set<f> set = this.f487b.get(str);
        int indexOf = str.indexOf(46);
        Set<f> set2 = indexOf != str.lastIndexOf(46) ? this.f487b.get("*." + str.substring(indexOf + 1)) : null;
        if (set == null && set2 == null) {
            return null;
        }
        if (set == null || set2 == null) {
            return set == null ? set2 : set;
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.addAll(set);
        linkedHashSet.addAll(set2);
        return linkedHashSet;
    }

    public void a(String str, List<Certificate> list) {
        Set<f> a2 = a(str);
        if (a2 != null) {
            int size = list.size();
            int i = 0;
            while (i < size) {
                if (!a2.contains(a((X509Certificate) list.get(i)))) {
                    i++;
                } else {
                    return;
                }
            }
            StringBuilder append = new StringBuilder().append("Certificate pinning failure!").append("\n  Peer certificate chain:");
            int size2 = list.size();
            for (int i2 = 0; i2 < size2; i2++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i2);
                append.append("\n    ").append(a((Certificate) x509Certificate)).append(": ").append(x509Certificate.getSubjectDN().getName());
            }
            append.append("\n  Pinned certificates for ").append(str).append(":");
            for (f b2 : a2) {
                append.append("\n    sha1/").append(b2.b());
            }
            throw new SSLPeerUnverifiedException(append.toString());
        }
    }
}
