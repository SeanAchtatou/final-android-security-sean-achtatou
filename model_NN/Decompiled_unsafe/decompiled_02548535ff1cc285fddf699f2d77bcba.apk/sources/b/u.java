package b;

import b.a.b.r;
import b.a.c;
import b.a.d;
import b.a.d.b;
import b.a.h;
import b.a.i;
import b.q;
import java.net.Proxy;
import java.net.ProxySelector;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public final class u implements Cloneable {
    /* access modifiers changed from: private */
    public static final List<v> y = i.a(v.HTTP_2, v.SPDY_3, v.HTTP_1_1);
    /* access modifiers changed from: private */
    public static final List<k> z = i.a(k.f496a, k.f497b, k.f498c);

    /* renamed from: a  reason: collision with root package name */
    final n f528a;

    /* renamed from: b  reason: collision with root package name */
    final Proxy f529b;

    /* renamed from: c  reason: collision with root package name */
    final List<v> f530c;
    final List<k> d;
    final List<s> e;
    final List<s> f;
    final ProxySelector g;
    final m h;
    final c i;
    final d j;
    final SocketFactory k;
    final SSLSocketFactory l;
    final HostnameVerifier m;
    final g n;
    final b o;
    final b p;
    final j q;
    final o r;
    final boolean s;
    final boolean t;
    final boolean u;
    final int v;
    final int w;
    final int x;

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        n f531a;

        /* renamed from: b  reason: collision with root package name */
        Proxy f532b;

        /* renamed from: c  reason: collision with root package name */
        List<v> f533c;
        List<k> d;
        final List<s> e;
        final List<s> f;
        ProxySelector g;
        m h;
        c i;
        d j;
        SocketFactory k;
        SSLSocketFactory l;
        HostnameVerifier m;
        g n;
        b o;
        b p;
        j q;
        o r;
        boolean s;
        boolean t;
        boolean u;
        int v;
        int w;
        int x;

        public a() {
            this.e = new ArrayList();
            this.f = new ArrayList();
            this.f531a = new n();
            this.f533c = u.y;
            this.d = u.z;
            this.g = ProxySelector.getDefault();
            this.h = m.f505a;
            this.k = SocketFactory.getDefault();
            this.m = b.f421a;
            this.n = g.f486a;
            this.o = b.f469a;
            this.p = b.f469a;
            this.q = new j();
            this.r = o.f509a;
            this.s = true;
            this.t = true;
            this.u = true;
            this.v = 10000;
            this.w = 10000;
            this.x = 10000;
        }

        a(u uVar) {
            this.e = new ArrayList();
            this.f = new ArrayList();
            this.f531a = uVar.f528a;
            this.f532b = uVar.f529b;
            this.f533c = uVar.f530c;
            this.d = uVar.d;
            this.e.addAll(uVar.e);
            this.f.addAll(uVar.f);
            this.g = uVar.g;
            this.h = uVar.h;
            this.j = uVar.j;
            this.i = uVar.i;
            this.k = uVar.k;
            this.l = uVar.l;
            this.m = uVar.m;
            this.n = uVar.n;
            this.o = uVar.o;
            this.p = uVar.p;
            this.q = uVar.q;
            this.r = uVar.r;
            this.s = uVar.s;
            this.t = uVar.t;
            this.u = uVar.u;
            this.v = uVar.v;
            this.w = uVar.w;
            this.x = uVar.x;
        }

        public a a(long j2, TimeUnit timeUnit) {
            if (j2 < 0) {
                throw new IllegalArgumentException("timeout < 0");
            } else if (timeUnit == null) {
                throw new IllegalArgumentException("unit == null");
            } else {
                long millis = timeUnit.toMillis(j2);
                if (millis > 2147483647L) {
                    throw new IllegalArgumentException("Timeout too large.");
                } else if (millis != 0 || j2 <= 0) {
                    this.v = (int) millis;
                    return this;
                } else {
                    throw new IllegalArgumentException("Timeout too small.");
                }
            }
        }

        public a a(List<v> list) {
            List a2 = i.a(list);
            if (!a2.contains(v.HTTP_1_1)) {
                throw new IllegalArgumentException("protocols doesn't contain http/1.1: " + a2);
            } else if (a2.contains(v.HTTP_1_0)) {
                throw new IllegalArgumentException("protocols must not contain http/1.0: " + a2);
            } else if (a2.contains(null)) {
                throw new IllegalArgumentException("protocols must not contain null");
            } else {
                this.f533c = i.a(a2);
                return this;
            }
        }

        public a a(HostnameVerifier hostnameVerifier) {
            if (hostnameVerifier == null) {
                throw new NullPointerException("hostnameVerifier == null");
            }
            this.m = hostnameVerifier;
            return this;
        }

        public a a(SSLSocketFactory sSLSocketFactory) {
            if (sSLSocketFactory == null) {
                throw new NullPointerException("sslSocketFactory == null");
            }
            this.l = sSLSocketFactory;
            return this;
        }

        public u a() {
            return new u(this);
        }

        public a b(long j2, TimeUnit timeUnit) {
            if (j2 < 0) {
                throw new IllegalArgumentException("timeout < 0");
            } else if (timeUnit == null) {
                throw new IllegalArgumentException("unit == null");
            } else {
                long millis = timeUnit.toMillis(j2);
                if (millis > 2147483647L) {
                    throw new IllegalArgumentException("Timeout too large.");
                } else if (millis != 0 || j2 <= 0) {
                    this.w = (int) millis;
                    return this;
                } else {
                    throw new IllegalArgumentException("Timeout too small.");
                }
            }
        }

        public a c(long j2, TimeUnit timeUnit) {
            if (j2 < 0) {
                throw new IllegalArgumentException("timeout < 0");
            } else if (timeUnit == null) {
                throw new IllegalArgumentException("unit == null");
            } else {
                long millis = timeUnit.toMillis(j2);
                if (millis > 2147483647L) {
                    throw new IllegalArgumentException("Timeout too large.");
                } else if (millis != 0 || j2 <= 0) {
                    this.x = (int) millis;
                    return this;
                } else {
                    throw new IllegalArgumentException("Timeout too small.");
                }
            }
        }
    }

    static {
        c.f413b = new c() {
            public r a(e eVar) {
                return ((w) eVar).f539c.f385b;
            }

            public b.a.c.b a(j jVar, a aVar, r rVar) {
                return jVar.a(aVar, rVar);
            }

            public d a(u uVar) {
                return uVar.g();
            }

            public h a(j jVar) {
                return jVar.f493a;
            }

            public void a(e eVar, f fVar, boolean z) {
                ((w) eVar).a(fVar, z);
            }

            public void a(k kVar, SSLSocket sSLSocket, boolean z) {
                kVar.a(sSLSocket, z);
            }

            public void a(q.a aVar, String str) {
                aVar.a(str);
            }

            public boolean a(j jVar, b.a.c.b bVar) {
                return jVar.b(bVar);
            }

            public void b(j jVar, b.a.c.b bVar) {
                jVar.a(bVar);
            }
        };
    }

    public u() {
        this(new a());
    }

    private u(a aVar) {
        this.f528a = aVar.f531a;
        this.f529b = aVar.f532b;
        this.f530c = aVar.f533c;
        this.d = aVar.d;
        this.e = i.a(aVar.e);
        this.f = i.a(aVar.f);
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
        this.j = aVar.j;
        this.k = aVar.k;
        if (aVar.l != null) {
            this.l = aVar.l;
        } else {
            try {
                SSLContext instance = SSLContext.getInstance("TLS");
                instance.init(null, null, null);
                this.l = instance.getSocketFactory();
            } catch (GeneralSecurityException e2) {
                throw new AssertionError();
            }
        }
        this.m = aVar.m;
        this.n = aVar.n;
        this.o = aVar.o;
        this.p = aVar.p;
        this.q = aVar.q;
        this.r = aVar.r;
        this.s = aVar.s;
        this.t = aVar.t;
        this.u = aVar.u;
        this.v = aVar.v;
        this.w = aVar.w;
        this.x = aVar.x;
    }

    public int a() {
        return this.v;
    }

    public e a(x xVar) {
        return new w(this, xVar);
    }

    public int b() {
        return this.w;
    }

    public int c() {
        return this.x;
    }

    public Proxy d() {
        return this.f529b;
    }

    public ProxySelector e() {
        return this.g;
    }

    public m f() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public d g() {
        return this.i != null ? this.i.f478a : this.j;
    }

    public o h() {
        return this.r;
    }

    public SocketFactory i() {
        return this.k;
    }

    public SSLSocketFactory j() {
        return this.l;
    }

    public HostnameVerifier k() {
        return this.m;
    }

    public g l() {
        return this.n;
    }

    public b m() {
        return this.p;
    }

    public b n() {
        return this.o;
    }

    public j o() {
        return this.q;
    }

    public boolean p() {
        return this.s;
    }

    public boolean q() {
        return this.t;
    }

    public boolean r() {
        return this.u;
    }

    public n s() {
        return this.f528a;
    }

    public List<v> t() {
        return this.f530c;
    }

    public List<k> u() {
        return this.d;
    }

    public List<s> v() {
        return this.e;
    }

    public List<s> w() {
        return this.f;
    }

    public a x() {
        return new a(this);
    }
}
