package b;

import b.a.i;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

public final class p {

    /* renamed from: a  reason: collision with root package name */
    private final ac f510a;

    /* renamed from: b  reason: collision with root package name */
    private final h f511b;

    /* renamed from: c  reason: collision with root package name */
    private final List<Certificate> f512c;
    private final List<Certificate> d;

    private p(ac acVar, h hVar, List<Certificate> list, List<Certificate> list2) {
        this.f510a = acVar;
        this.f511b = hVar;
        this.f512c = list;
        this.d = list2;
    }

    public static p a(SSLSession sSLSession) {
        Certificate[] certificateArr;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        }
        h a2 = h.a(cipherSuite);
        String protocol = sSLSession.getProtocol();
        if (protocol == null) {
            throw new IllegalStateException("tlsVersion == null");
        }
        ac a3 = ac.a(protocol);
        try {
            certificateArr = sSLSession.getPeerCertificates();
        } catch (SSLPeerUnverifiedException e) {
            certificateArr = null;
        }
        List a4 = certificateArr != null ? i.a(certificateArr) : Collections.emptyList();
        Certificate[] localCertificates = sSLSession.getLocalCertificates();
        return new p(a3, a2, a4, localCertificates != null ? i.a(localCertificates) : Collections.emptyList());
    }

    public h a() {
        return this.f511b;
    }

    public List<Certificate> b() {
        return this.f512c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof p)) {
            return false;
        }
        p pVar = (p) obj;
        return i.a(this.f511b, pVar.f511b) && this.f511b.equals(pVar.f511b) && this.f512c.equals(pVar.f512c) && this.d.equals(pVar.d);
    }

    public int hashCode() {
        return (((((((this.f510a != null ? this.f510a.hashCode() : 0) + 527) * 31) + this.f511b.hashCode()) * 31) + this.f512c.hashCode()) * 31) + this.d.hashCode();
    }
}
