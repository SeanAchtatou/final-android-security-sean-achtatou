package b;

import b.a.b.f;
import b.a.i;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f502a = Pattern.compile("(\\d{2,4})[^\\d]*");

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f503b = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");

    /* renamed from: c  reason: collision with root package name */
    private static final Pattern f504c = Pattern.compile("(\\d{1,2})[^\\d]*");
    private static final Pattern d = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");
    private final String e;
    private final String f;
    private final long g;
    private final String h;
    private final String i;
    private final boolean j;
    private final boolean k;
    private final boolean l;
    private final boolean m;

    private l(String str, String str2, long j2, String str3, String str4, boolean z, boolean z2, boolean z3, boolean z4) {
        this.e = str;
        this.f = str2;
        this.g = j2;
        this.h = str3;
        this.i = str4;
        this.j = z;
        this.k = z2;
        this.m = z3;
        this.l = z4;
    }

    private static int a(String str, int i2, int i3, boolean z) {
        for (int i4 = i2; i4 < i3; i4++) {
            char charAt = str.charAt(i4);
            if (((charAt < ' ' && charAt != 9) || charAt >= 127 || (charAt >= '0' && charAt <= '9') || ((charAt >= 'a' && charAt <= 'z') || ((charAt >= 'A' && charAt <= 'Z') || charAt == ':'))) == (!z)) {
                return i4;
            }
        }
        return i3;
    }

    private static long a(String str) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong <= 0) {
                return Long.MIN_VALUE;
            }
            return parseLong;
        } catch (NumberFormatException e2) {
            if (str.matches("-?\\d+")) {
                return !str.startsWith("-") ? Long.MAX_VALUE : Long.MIN_VALUE;
            }
            throw e2;
        }
    }

    private static long a(String str, int i2, int i3) {
        int a2 = a(str, i2, i3, false);
        int i4 = -1;
        int i5 = -1;
        int i6 = -1;
        int i7 = -1;
        int i8 = -1;
        int i9 = -1;
        Matcher matcher = d.matcher(str);
        while (a2 < i3) {
            int a3 = a(str, a2 + 1, i3, true);
            matcher.region(a2, a3);
            if (i4 == -1 && matcher.usePattern(d).matches()) {
                i4 = Integer.parseInt(matcher.group(1));
                i5 = Integer.parseInt(matcher.group(2));
                i6 = Integer.parseInt(matcher.group(3));
            } else if (i7 == -1 && matcher.usePattern(f504c).matches()) {
                i7 = Integer.parseInt(matcher.group(1));
            } else if (i8 == -1 && matcher.usePattern(f503b).matches()) {
                i8 = f503b.pattern().indexOf(matcher.group(1).toLowerCase(Locale.US)) / 4;
            } else if (i9 == -1 && matcher.usePattern(f502a).matches()) {
                i9 = Integer.parseInt(matcher.group(1));
            }
            a2 = a(str, a3 + 1, i3, false);
        }
        if (i9 >= 70 && i9 <= 99) {
            i9 += 1900;
        }
        if (i9 >= 0 && i9 <= 69) {
            i9 += 2000;
        }
        if (i9 < 1601) {
            throw new IllegalArgumentException();
        } else if (i8 == -1) {
            throw new IllegalArgumentException();
        } else if (i7 < 1 || i7 > 31) {
            throw new IllegalArgumentException();
        } else if (i4 < 0 || i4 > 23) {
            throw new IllegalArgumentException();
        } else if (i5 < 0 || i5 > 59) {
            throw new IllegalArgumentException();
        } else if (i6 < 0 || i6 > 59) {
            throw new IllegalArgumentException();
        } else {
            GregorianCalendar gregorianCalendar = new GregorianCalendar(i.d);
            gregorianCalendar.setLenient(false);
            gregorianCalendar.set(1, i9);
            gregorianCalendar.set(2, i8 - 1);
            gregorianCalendar.set(5, i7);
            gregorianCalendar.set(11, i4);
            gregorianCalendar.set(12, i5);
            gregorianCalendar.set(13, i6);
            gregorianCalendar.set(14, 0);
            return gregorianCalendar.getTimeInMillis();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.i.a(java.lang.String, int, int, char):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      b.a.i.a(java.lang.String, int, int, java.lang.String):int
      b.a.i.a(java.lang.String, int, int, char):int */
    static l a(long j2, r rVar, String str) {
        long j3;
        String str2;
        String str3;
        int length = str.length();
        int a2 = i.a(str, 0, length, ';');
        int a3 = i.a(str, 0, a2, '=');
        if (a3 == a2) {
            return null;
        }
        String c2 = i.c(str, 0, a3);
        if (c2.isEmpty()) {
            return null;
        }
        String c3 = i.c(str, a3 + 1, a2);
        long j4 = 253402300799999L;
        long j5 = -1;
        String str4 = null;
        String str5 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = true;
        boolean z4 = false;
        int i2 = a2 + 1;
        while (i2 < length) {
            int a4 = i.a(str, i2, length, ';');
            int a5 = i.a(str, i2, a4, '=');
            String c4 = i.c(str, i2, a5);
            String c5 = a5 < a4 ? i.c(str, a5 + 1, a4) : "";
            if (c4.equalsIgnoreCase("expires")) {
                try {
                    j4 = a(c5, 0, c5.length());
                    z4 = true;
                    str3 = str4;
                } catch (IllegalArgumentException e2) {
                    str3 = str4;
                }
            } else if (c4.equalsIgnoreCase("max-age")) {
                try {
                    j5 = a(c5);
                    z4 = true;
                    str3 = str4;
                } catch (NumberFormatException e3) {
                    str3 = str4;
                }
            } else if (c4.equalsIgnoreCase("domain")) {
                try {
                    str3 = b(c5);
                    z3 = false;
                } catch (IllegalArgumentException e4) {
                    str3 = str4;
                }
            } else if (c4.equalsIgnoreCase("path")) {
                str5 = c5;
                str3 = str4;
            } else if (c4.equalsIgnoreCase("secure")) {
                z = true;
                str3 = str4;
            } else if (c4.equalsIgnoreCase("httponly")) {
                z2 = true;
                str3 = str4;
            } else {
                str3 = str4;
            }
            String str6 = str3;
            i2 = a4 + 1;
            j4 = j4;
            str4 = str6;
        }
        if (j5 == Long.MIN_VALUE) {
            j3 = Long.MIN_VALUE;
        } else if (j5 != -1) {
            j3 = (j5 <= 9223372036854775L ? j5 * 1000 : Long.MAX_VALUE) + j2;
            if (j3 < j2 || j3 > 253402300799999L) {
                j3 = 253402300799999L;
            }
        } else {
            j3 = j4;
        }
        if (str4 == null) {
            str4 = rVar.f();
        } else if (!b(rVar, str4)) {
            return null;
        }
        if (str5 == null || !str5.startsWith("/")) {
            String h2 = rVar.h();
            int lastIndexOf = h2.lastIndexOf(47);
            str2 = lastIndexOf != 0 ? h2.substring(0, lastIndexOf) : "/";
        } else {
            str2 = str5;
        }
        return new l(c2, c3, j3, str4, str2, z, z2, z3, z4);
    }

    public static l a(r rVar, String str) {
        return a(System.currentTimeMillis(), rVar, str);
    }

    public static List<l> a(r rVar, q qVar) {
        List<String> c2 = qVar.c("Set-Cookie");
        ArrayList arrayList = null;
        int size = c2.size();
        for (int i2 = 0; i2 < size; i2++) {
            l a2 = a(rVar, c2.get(i2));
            if (a2 != null) {
                ArrayList arrayList2 = arrayList == null ? new ArrayList() : arrayList;
                arrayList2.add(a2);
                arrayList = arrayList2;
            }
        }
        return arrayList != null ? Collections.unmodifiableList(arrayList) : Collections.emptyList();
    }

    private static String b(String str) {
        if (str.endsWith(".")) {
            throw new IllegalArgumentException();
        }
        if (str.startsWith(".")) {
            str = str.substring(1);
        }
        String b2 = i.b(str);
        if (b2 != null) {
            return b2;
        }
        throw new IllegalArgumentException();
    }

    private static boolean b(r rVar, String str) {
        String f2 = rVar.f();
        if (f2.equals(str)) {
            return true;
        }
        return f2.endsWith(str) && f2.charAt((f2.length() - str.length()) + -1) == '.' && !i.c(f2);
    }

    public String a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.e);
        sb.append('=');
        sb.append(this.f);
        if (this.l) {
            if (this.g == Long.MIN_VALUE) {
                sb.append("; max-age=0");
            } else {
                sb.append("; expires=").append(f.a(new Date(this.g)));
            }
        }
        if (!this.m) {
            sb.append("; domain=").append(this.h);
        }
        sb.append("; path=").append(this.i);
        if (this.j) {
            sb.append("; secure");
        }
        if (this.k) {
            sb.append("; httponly");
        }
        return sb.toString();
    }
}
