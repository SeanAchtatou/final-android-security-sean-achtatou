package b;

import b.q;

public final class z {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final x f554a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final v f555b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final int f556c;
    /* access modifiers changed from: private */
    public final String d;
    /* access modifiers changed from: private */
    public final p e;
    /* access modifiers changed from: private */
    public final q f;
    /* access modifiers changed from: private */
    public final aa g;
    /* access modifiers changed from: private */
    public z h;
    /* access modifiers changed from: private */
    public z i;
    /* access modifiers changed from: private */
    public final z j;
    private volatile d k;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public x f557a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public v f558b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public int f559c;
        /* access modifiers changed from: private */
        public String d;
        /* access modifiers changed from: private */
        public p e;
        /* access modifiers changed from: private */
        public q.a f;
        /* access modifiers changed from: private */
        public aa g;
        /* access modifiers changed from: private */
        public z h;
        /* access modifiers changed from: private */
        public z i;
        /* access modifiers changed from: private */
        public z j;

        public a() {
            this.f559c = -1;
            this.f = new q.a();
        }

        private a(z zVar) {
            this.f559c = -1;
            this.f557a = zVar.f554a;
            this.f558b = zVar.f555b;
            this.f559c = zVar.f556c;
            this.d = zVar.d;
            this.e = zVar.e;
            this.f = zVar.f.b();
            this.g = zVar.g;
            this.h = zVar.h;
            this.i = zVar.i;
            this.j = zVar.j;
        }

        private void a(String str, z zVar) {
            if (zVar.g != null) {
                throw new IllegalArgumentException(str + ".body != null");
            } else if (zVar.h != null) {
                throw new IllegalArgumentException(str + ".networkResponse != null");
            } else if (zVar.i != null) {
                throw new IllegalArgumentException(str + ".cacheResponse != null");
            } else if (zVar.j != null) {
                throw new IllegalArgumentException(str + ".priorResponse != null");
            }
        }

        private void d(z zVar) {
            if (zVar.g != null) {
                throw new IllegalArgumentException("priorResponse.body != null");
            }
        }

        public a a(int i2) {
            this.f559c = i2;
            return this;
        }

        public a a(aa aaVar) {
            this.g = aaVar;
            return this;
        }

        public a a(p pVar) {
            this.e = pVar;
            return this;
        }

        public a a(q qVar) {
            this.f = qVar.b();
            return this;
        }

        public a a(v vVar) {
            this.f558b = vVar;
            return this;
        }

        public a a(x xVar) {
            this.f557a = xVar;
            return this;
        }

        public a a(z zVar) {
            if (zVar != null) {
                a("networkResponse", zVar);
            }
            this.h = zVar;
            return this;
        }

        public a a(String str) {
            this.d = str;
            return this;
        }

        public a a(String str, String str2) {
            this.f.c(str, str2);
            return this;
        }

        public z a() {
            if (this.f557a == null) {
                throw new IllegalStateException("request == null");
            } else if (this.f558b == null) {
                throw new IllegalStateException("protocol == null");
            } else if (this.f559c >= 0) {
                return new z(this);
            } else {
                throw new IllegalStateException("code < 0: " + this.f559c);
            }
        }

        public a b(z zVar) {
            if (zVar != null) {
                a("cacheResponse", zVar);
            }
            this.i = zVar;
            return this;
        }

        public a b(String str, String str2) {
            this.f.a(str, str2);
            return this;
        }

        public a c(z zVar) {
            if (zVar != null) {
                d(zVar);
            }
            this.j = zVar;
            return this;
        }
    }

    private z(a aVar) {
        this.f554a = aVar.f557a;
        this.f555b = aVar.f558b;
        this.f556c = aVar.f559c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f.a();
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
        this.j = aVar.j;
    }

    public x a() {
        return this.f554a;
    }

    public String a(String str) {
        return a(str, null);
    }

    public String a(String str, String str2) {
        String a2 = this.f.a(str);
        return a2 != null ? a2 : str2;
    }

    public int b() {
        return this.f556c;
    }

    public String c() {
        return this.d;
    }

    public p d() {
        return this.e;
    }

    public q e() {
        return this.f;
    }

    public aa f() {
        return this.g;
    }

    public a g() {
        return new a();
    }

    public d h() {
        d dVar = this.k;
        if (dVar != null) {
            return dVar;
        }
        d a2 = d.a(this.f);
        this.k = a2;
        return a2;
    }

    public String toString() {
        return "Response{protocol=" + this.f555b + ", code=" + this.f556c + ", message=" + this.d + ", url=" + this.f554a.a() + '}';
    }
}
