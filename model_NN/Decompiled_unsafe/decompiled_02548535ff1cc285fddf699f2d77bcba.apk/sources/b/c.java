package b;

import b.a.b;
import b.a.d;
import java.io.Closeable;
import java.io.Flushable;

public final class c implements Closeable, Flushable {

    /* renamed from: a  reason: collision with root package name */
    final d f478a;

    /* renamed from: b  reason: collision with root package name */
    private final b f479b;

    public void close() {
        this.f479b.close();
    }

    public void flush() {
        this.f479b.flush();
    }
}
