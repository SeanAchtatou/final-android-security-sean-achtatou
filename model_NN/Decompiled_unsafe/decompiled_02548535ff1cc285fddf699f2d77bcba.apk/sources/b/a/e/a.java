package b.a.e;

import b.a.e.c;
import b.aa;
import b.b.c;
import b.t;
import b.y;
import b.z;
import c.d;
import c.e;
import c.l;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class a implements b.b.a {
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final d f423c;
    private final c d;
    private final c e;
    private volatile boolean f;
    private boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    private final AtomicBoolean i = new AtomicBoolean();

    public a(boolean z, e eVar, d dVar, Random random, final Executor executor, final c cVar, final String str) {
        this.e = cVar;
        this.f423c = new d(z, dVar, random);
        this.d = new c(z, eVar, new c.a() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: b.a.e.a.a(b.a.e.a, boolean):boolean
             arg types: [b.a.e.a, int]
             candidates:
              b.a.e.a.a(int, java.lang.String):void
              b.b.a.a(int, java.lang.String):void
              b.a.e.a.a(b.a.e.a, boolean):boolean */
            public void a(int i, String str) {
                boolean unused = a.this.h = true;
                final int i2 = i;
                final String str2 = str;
                executor.execute(new b.a.e("OkHttp %s WebSocket Close Reply", new Object[]{str}) {
                    /* access modifiers changed from: protected */
                    public void b() {
                        a.this.b(i2, str2);
                    }
                });
            }

            public void a(aa aaVar) {
                cVar.a(aaVar);
            }

            public void a(final c.c cVar) {
                executor.execute(new b.a.e("OkHttp %s WebSocket Pong Reply", new Object[]{str}) {
                    /* access modifiers changed from: protected */
                    public void b() {
                        try {
                            a.this.f423c.a(cVar);
                        } catch (IOException e) {
                        }
                    }
                });
            }

            public void b(c.c cVar) {
                cVar.a(cVar);
            }
        });
    }

    private void a(IOException iOException) {
        if (!this.f && (iOException instanceof ProtocolException)) {
            try {
                this.f423c.a(1002, (String) null);
            } catch (IOException e2) {
            }
        }
        if (this.i.compareAndSet(false, true)) {
            try {
                b();
            } catch (IOException e3) {
            }
        }
        this.e.a(iOException, (z) null);
    }

    /* access modifiers changed from: private */
    public void b(int i2, String str) {
        if (!this.f) {
            try {
                this.f423c.a(i2, str);
            } catch (IOException e2) {
            }
        }
        if (this.i.compareAndSet(false, true)) {
            try {
                b();
            } catch (IOException e3) {
            }
        }
        this.e.a(i2, str);
    }

    public void a(int i2, String str) {
        if (this.f) {
            throw new IllegalStateException("closed");
        }
        this.f = true;
        try {
            this.f423c.a(i2, str);
        } catch (IOException e2) {
            if (this.i.compareAndSet(false, true)) {
                try {
                    b();
                } catch (IOException e3) {
                }
            }
            throw e2;
        }
    }

    public void a(y yVar) {
        int i2;
        if (yVar == null) {
            throw new NullPointerException("message == null");
        } else if (this.f) {
            throw new IllegalStateException("closed");
        } else if (this.g) {
            throw new IllegalStateException("must call close()");
        } else {
            t a2 = yVar.a();
            if (a2 == null) {
                throw new IllegalArgumentException("Message content type was null. Must use WebSocket.TEXT or WebSocket.BINARY.");
            }
            String b2 = a2.b();
            if (b.b.a.f470a.b().equals(b2)) {
                i2 = 1;
            } else if (b.b.a.f471b.b().equals(b2)) {
                i2 = 2;
            } else {
                throw new IllegalArgumentException("Unknown message content type: " + a2.a() + "/" + a2.b() + ". Must use WebSocket.TEXT or WebSocket.BINARY.");
            }
            d a3 = l.a(this.f423c.a(i2));
            try {
                yVar.a(a3);
                a3.close();
            } catch (IOException e2) {
                this.g = true;
                throw e2;
            }
        }
    }

    public boolean a() {
        try {
            this.d.a();
            return !this.h;
        } catch (IOException e2) {
            a(e2);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void b();
}
