package b.a.e;

import c.c;
import c.e;
import c.q;
import c.s;
import java.io.IOException;
import java.util.Random;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f438a = (!d.class.desiredAssertionStatus());

    /* renamed from: b  reason: collision with root package name */
    private final boolean f439b;

    /* renamed from: c  reason: collision with root package name */
    private final Random f440c;
    /* access modifiers changed from: private */
    public final c.d d;
    private boolean e;
    /* access modifiers changed from: private */
    public final c f = new c();
    private final a g = new a();
    /* access modifiers changed from: private */
    public boolean h;
    private final byte[] i;
    private final byte[] j;

    private final class a implements q {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public int f442b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public boolean f443c;
        /* access modifiers changed from: private */
        public boolean d;

        private a() {
        }

        public s a() {
            return d.this.d.a();
        }

        public void a_(c cVar, long j) {
            if (this.d) {
                throw new IOException("closed");
            }
            d.this.f.a_(cVar, j);
            long g = d.this.f.g();
            if (g > 0) {
                synchronized (d.this) {
                    d.this.a(this.f442b, g, this.f443c, false);
                }
                this.f443c = false;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.a.e.d.a(b.a.e.d, boolean):boolean
         arg types: [b.a.e.d, int]
         candidates:
          b.a.e.d.a(int, c.c):void
          b.a.e.d.a(c.e, long):void
          b.a.e.d.a(int, java.lang.String):void
          b.a.e.d.a(b.a.e.d, boolean):boolean */
        public void close() {
            if (this.d) {
                throw new IOException("closed");
            }
            synchronized (d.this) {
                d.this.a(this.f442b, d.this.f.b(), this.f443c, true);
            }
            this.d = true;
            boolean unused = d.this.h = false;
        }

        public void flush() {
            if (this.d) {
                throw new IOException("closed");
            }
            synchronized (d.this) {
                d.this.a(this.f442b, d.this.f.b(), this.f443c, false);
            }
            this.f443c = false;
        }
    }

    public d(boolean z, c.d dVar, Random random) {
        byte[] bArr = null;
        if (dVar == null) {
            throw new NullPointerException("sink == null");
        } else if (random == null) {
            throw new NullPointerException("random == null");
        } else {
            this.f439b = z;
            this.d = dVar;
            this.f440c = random;
            this.i = z ? new byte[4] : null;
            this.j = z ? new byte[2048] : bArr;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, long j2, boolean z, boolean z2) {
        int i3;
        if (!f438a && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (this.e) {
            throw new IOException("closed");
        } else {
            int i4 = z ? i2 : 0;
            if (z2) {
                i4 |= 128;
            }
            this.d.h(i4);
            if (this.f439b) {
                i3 = 128;
                this.f440c.nextBytes(this.i);
            } else {
                i3 = 0;
            }
            if (j2 <= 125) {
                this.d.h(i3 | ((int) j2));
            } else if (j2 <= 65535) {
                this.d.h(i3 | 126);
                this.d.g((int) j2);
            } else {
                this.d.h(i3 | 127);
                this.d.m(j2);
            }
            if (this.f439b) {
                this.d.c(this.i);
                a(this.f, j2);
            } else {
                this.d.a_(this.f, j2);
            }
            this.d.e();
        }
    }

    private void a(int i2, c cVar) {
        if (!f438a && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (this.e) {
            throw new IOException("closed");
        } else {
            int i3 = 0;
            if (cVar != null) {
                i3 = (int) cVar.b();
                if (((long) i3) > 125) {
                    throw new IllegalArgumentException("Payload size must be less than or equal to 125");
                }
            }
            this.d.h(i2 | 128);
            if (this.f439b) {
                this.d.h(i3 | 128);
                this.f440c.nextBytes(this.i);
                this.d.c(this.i);
                if (cVar != null) {
                    a(cVar, (long) i3);
                }
            } else {
                this.d.h(i3);
                if (cVar != null) {
                    this.d.a(cVar);
                }
            }
            this.d.e();
        }
    }

    private void a(e eVar, long j2) {
        if (f438a || Thread.holdsLock(this)) {
            long j3 = 0;
            while (j3 < j2) {
                int a2 = eVar.a(this.j, 0, (int) Math.min(j2, (long) this.j.length));
                if (a2 == -1) {
                    throw new AssertionError();
                }
                b.a(this.j, (long) a2, this.i, j3);
                this.d.c(this.j, 0, a2);
                j3 += (long) a2;
            }
            return;
        }
        throw new AssertionError();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.e.d.a.a(b.a.e.d$a, boolean):boolean
     arg types: [b.a.e.d$a, int]
     candidates:
      b.a.e.d.a.a(b.a.e.d$a, int):int
      b.a.e.d.a.a(b.a.e.d$a, boolean):boolean */
    public q a(int i2) {
        if (this.h) {
            throw new IllegalStateException("Another message writer is active. Did you call close()?");
        }
        this.h = true;
        int unused = this.g.f442b = i2;
        boolean unused2 = this.g.f443c = true;
        boolean unused3 = this.g.d = false;
        return this.g;
    }

    public void a(int i2, String str) {
        c cVar = null;
        if (!(i2 == 0 && str == null)) {
            if (i2 != 0) {
                b.a(i2, true);
            }
            cVar = new c();
            cVar.g(i2);
            if (str != null) {
                cVar.b(str);
            }
        }
        synchronized (this) {
            a(8, cVar);
            this.e = true;
        }
    }

    public void a(c cVar) {
        synchronized (this) {
            a(10, cVar);
        }
    }
}
