package b.a.e;

import b.aa;
import b.t;
import c.e;
import c.l;
import c.r;
import c.s;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f431a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final e f432b;

    /* renamed from: c  reason: collision with root package name */
    private final a f433c;
    private final r d = new b();
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public long h;
    /* access modifiers changed from: private */
    public long i;
    /* access modifiers changed from: private */
    public boolean j;
    private boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public final byte[] m = new byte[4];
    /* access modifiers changed from: private */
    public final byte[] n = new byte[2048];

    public interface a {
        void a(int i, String str);

        void a(aa aaVar);

        void a(c.c cVar);

        void b(c.c cVar);
    }

    private final class b implements r {
        private b() {
        }

        public long a(c.c cVar, long j) {
            long a2;
            if (c.this.e) {
                throw new IOException("closed");
            } else if (c.this.f) {
                throw new IllegalStateException("closed");
            } else {
                if (c.this.i == c.this.h) {
                    if (c.this.j) {
                        return -1;
                    }
                    c.this.e();
                    if (c.this.g != 0) {
                        throw new ProtocolException("Expected continuation opcode. Got: " + Integer.toHexString(c.this.g));
                    } else if (c.this.j && c.this.h == 0) {
                        return -1;
                    }
                }
                long min = Math.min(j, c.this.h - c.this.i);
                if (c.this.l) {
                    a2 = (long) c.this.f432b.a(c.this.n, 0, (int) Math.min(min, (long) c.this.n.length));
                    if (a2 == -1) {
                        throw new EOFException();
                    }
                    b.a(c.this.n, a2, c.this.m, c.this.i);
                    cVar.c(c.this.n, 0, (int) a2);
                } else {
                    a2 = c.this.f432b.a(cVar, min);
                    if (a2 == -1) {
                        throw new EOFException();
                    }
                }
                long unused = c.this.i = c.this.i + a2;
                return a2;
            }
        }

        public s a() {
            return c.this.f432b.a();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.a.e.c.a(b.a.e.c, boolean):boolean
         arg types: [b.a.e.c, int]
         candidates:
          b.a.e.c.a(b.a.e.c, long):long
          b.a.e.c.a(b.a.e.c, boolean):boolean */
        public void close() {
            if (!c.this.f) {
                boolean unused = c.this.f = true;
                if (!c.this.e) {
                    c.this.f432b.g(c.this.h - c.this.i);
                    while (!c.this.j) {
                        c.this.e();
                        c.this.f432b.g(c.this.h);
                    }
                }
            }
        }
    }

    public c(boolean z, e eVar, a aVar) {
        if (eVar == null) {
            throw new NullPointerException("source == null");
        } else if (aVar == null) {
            throw new NullPointerException("frameCallback == null");
        } else {
            this.f431a = z;
            this.f432b = eVar;
            this.f433c = aVar;
        }
    }

    private void b() {
        boolean z = true;
        if (this.e) {
            throw new IOException("closed");
        }
        byte h2 = this.f432b.h() & 255;
        this.g = h2 & 15;
        this.j = (h2 & 128) != 0;
        this.k = (h2 & 8) != 0;
        if (!this.k || this.j) {
            boolean z2 = (h2 & 64) != 0;
            boolean z3 = (h2 & 32) != 0;
            boolean z4 = (h2 & 16) != 0;
            if (z2 || z3 || z4) {
                throw new ProtocolException("Reserved flags are unsupported.");
            }
            byte h3 = this.f432b.h() & 255;
            if ((h3 & 128) == 0) {
                z = false;
            }
            this.l = z;
            if (this.l == this.f431a) {
                throw new ProtocolException("Client-sent frames must be masked. Server sent must not.");
            }
            this.h = (long) (h3 & Byte.MAX_VALUE);
            if (this.h == 126) {
                this.h = ((long) this.f432b.i()) & 65535;
            } else if (this.h == 127) {
                this.h = this.f432b.k();
                if (this.h < 0) {
                    throw new ProtocolException("Frame length 0x" + Long.toHexString(this.h) + " > 0x7FFFFFFFFFFFFFFF");
                }
            }
            this.i = 0;
            if (this.k && this.h > 125) {
                throw new ProtocolException("Control frame must be less than 125B.");
            } else if (this.l) {
                this.f432b.a(this.m);
            }
        } else {
            throw new ProtocolException("Control frames must be final.");
        }
    }

    private void c() {
        String str;
        short s;
        c.c cVar = null;
        if (this.i < this.h) {
            c.c cVar2 = new c.c();
            if (this.f431a) {
                this.f432b.b(cVar2, this.h);
                cVar = cVar2;
            } else {
                while (this.i < this.h) {
                    int a2 = this.f432b.a(this.n, 0, (int) Math.min(this.h - this.i, (long) this.n.length));
                    if (a2 == -1) {
                        throw new EOFException();
                    }
                    b.a(this.n, (long) a2, this.m, this.i);
                    cVar2.c(this.n, 0, a2);
                    this.i += (long) a2;
                }
                cVar = cVar2;
            }
        }
        switch (this.g) {
            case 8:
                if (cVar != null) {
                    long b2 = cVar.b();
                    if (b2 == 1) {
                        throw new ProtocolException("Malformed close payload length of 1.");
                    } else if (b2 != 0) {
                        s = cVar.i();
                        b.a(s, false);
                        str = cVar.p();
                        this.f433c.a(s, str);
                        this.e = true;
                        return;
                    }
                }
                str = "";
                s = 1000;
                this.f433c.a(s, str);
                this.e = true;
                return;
            case 9:
                this.f433c.a(cVar);
                return;
            case 10:
                this.f433c.b(cVar);
                return;
            default:
                throw new ProtocolException("Unknown control opcode: " + Integer.toHexString(this.g));
        }
    }

    private void d() {
        final t tVar;
        switch (this.g) {
            case 1:
                tVar = b.b.a.f470a;
                break;
            case 2:
                tVar = b.b.a.f471b;
                break;
            default:
                throw new ProtocolException("Unknown opcode: " + Integer.toHexString(this.g));
        }
        final e a2 = l.a(this.d);
        AnonymousClass1 r2 = new aa() {
            public t a() {
                return tVar;
            }

            public long b() {
                return -1;
            }

            public e c() {
                return a2;
            }
        };
        this.f = false;
        this.f433c.a(r2);
        if (!this.f) {
            throw new IllegalStateException("Listener failed to call close on message payload.");
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        while (!this.e) {
            b();
            if (this.k) {
                c();
            } else {
                return;
            }
        }
    }

    public void a() {
        b();
        if (this.k) {
            c();
        } else {
            d();
        }
    }
}
