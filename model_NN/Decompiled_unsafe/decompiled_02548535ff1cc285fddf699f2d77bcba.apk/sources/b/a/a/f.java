package b.a.a;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    public static final c.f f300a = c.f.a(":status");

    /* renamed from: b  reason: collision with root package name */
    public static final c.f f301b = c.f.a(":method");

    /* renamed from: c  reason: collision with root package name */
    public static final c.f f302c = c.f.a(":path");
    public static final c.f d = c.f.a(":scheme");
    public static final c.f e = c.f.a(":authority");
    public static final c.f f = c.f.a(":host");
    public static final c.f g = c.f.a(":version");
    public final c.f h;
    public final c.f i;
    final int j;

    public f(c.f fVar, c.f fVar2) {
        this.h = fVar;
        this.i = fVar2;
        this.j = fVar.f() + 32 + fVar2.f();
    }

    public f(c.f fVar, String str) {
        this(fVar, c.f.a(str));
    }

    public f(String str, String str2) {
        this(c.f.a(str), c.f.a(str2));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return this.h.equals(fVar.h) && this.i.equals(fVar.i);
    }

    public int hashCode() {
        return ((this.h.hashCode() + 527) * 31) + this.i.hashCode();
    }

    public String toString() {
        return String.format("%s: %s", this.h.a(), this.i.a());
    }
}
