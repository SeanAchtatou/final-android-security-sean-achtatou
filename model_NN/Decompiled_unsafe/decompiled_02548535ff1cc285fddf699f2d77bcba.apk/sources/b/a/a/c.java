package b.a.a;

import java.io.Closeable;
import java.util.List;

public interface c extends Closeable {
    void a();

    void a(int i, int i2, List<f> list);

    void a(int i, long j);

    void a(int i, a aVar);

    void a(int i, a aVar, byte[] bArr);

    void a(n nVar);

    void a(boolean z, int i, int i2);

    void a(boolean z, int i, c.c cVar, int i2);

    void a(boolean z, boolean z2, int i, int i2, List<f> list);

    void b();

    void b(n nVar);

    int c();
}
