package b.a.a;

import java.util.concurrent.CountDownLatch;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private final CountDownLatch f337a = new CountDownLatch(1);

    /* renamed from: b  reason: collision with root package name */
    private long f338b = -1;

    /* renamed from: c  reason: collision with root package name */
    private long f339c = -1;

    l() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.f338b != -1) {
            throw new IllegalStateException();
        }
        this.f338b = System.nanoTime();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.f339c != -1 || this.f338b == -1) {
            throw new IllegalStateException();
        }
        this.f339c = System.nanoTime();
        this.f337a.countDown();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f339c != -1 || this.f338b == -1) {
            throw new IllegalStateException();
        }
        this.f339c = this.f338b - 1;
        this.f337a.countDown();
    }
}
