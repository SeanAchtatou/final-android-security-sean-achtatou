package b.a.a;

import java.util.Arrays;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    private int f341a;

    /* renamed from: b  reason: collision with root package name */
    private int f342b;

    /* renamed from: c  reason: collision with root package name */
    private int f343c;
    private final int[] d = new int[10];

    /* access modifiers changed from: package-private */
    public n a(int i, int i2, int i3) {
        if (i < this.d.length) {
            int i4 = 1 << i;
            this.f341a |= i4;
            if ((i2 & 1) != 0) {
                this.f342b |= i4;
            } else {
                this.f342b &= i4 ^ -1;
            }
            if ((i2 & 2) != 0) {
                this.f343c = i4 | this.f343c;
            } else {
                this.f343c = (i4 ^ -1) & this.f343c;
            }
            this.d[i] = i3;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f343c = 0;
        this.f342b = 0;
        this.f341a = 0;
        Arrays.fill(this.d, 0);
    }

    /* access modifiers changed from: package-private */
    public void a(n nVar) {
        for (int i = 0; i < 10; i++) {
            if (nVar.a(i)) {
                a(i, nVar.c(i), nVar.b(i));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i) {
        return ((1 << i) & this.f341a) != 0;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return Integer.bitCount(this.f341a);
    }

    /* access modifiers changed from: package-private */
    public int b(int i) {
        return this.d[i];
    }

    /* access modifiers changed from: package-private */
    public int c() {
        if ((2 & this.f341a) != 0) {
            return this.d[1];
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public int c(int i) {
        int i2 = 0;
        if (h(i)) {
            i2 = 2;
        }
        return g(i) ? i2 | 1 : i2;
    }

    /* access modifiers changed from: package-private */
    public int d(int i) {
        return (16 & this.f341a) != 0 ? this.d[4] : i;
    }

    /* access modifiers changed from: package-private */
    public int e(int i) {
        return (32 & this.f341a) != 0 ? this.d[5] : i;
    }

    /* access modifiers changed from: package-private */
    public int f(int i) {
        return (128 & this.f341a) != 0 ? this.d[7] : i;
    }

    /* access modifiers changed from: package-private */
    public boolean g(int i) {
        return ((1 << i) & this.f342b) != 0;
    }

    /* access modifiers changed from: package-private */
    public boolean h(int i) {
        return ((1 << i) & this.f343c) != 0;
    }
}
