package b.a.a;

import c.q;
import c.r;
import c.s;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public final class e {
    static final /* synthetic */ boolean d = (!e.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    long f290a = 0;

    /* renamed from: b  reason: collision with root package name */
    long f291b;

    /* renamed from: c  reason: collision with root package name */
    final a f292c;
    /* access modifiers changed from: private */
    public final int e;
    /* access modifiers changed from: private */
    public final d f;
    private final List<f> g;
    private List<f> h;
    private final b i;
    /* access modifiers changed from: private */
    public final c j = new c();
    /* access modifiers changed from: private */
    public final c k = new c();
    /* access modifiers changed from: private */
    public a l = null;

    final class a implements q {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ boolean f293a = (!e.class.desiredAssertionStatus());

        /* renamed from: c  reason: collision with root package name */
        private final c.c f295c = new c.c();
        /* access modifiers changed from: private */
        public boolean d;
        /* access modifiers changed from: private */
        public boolean e;

        a() {
        }

        private void a(boolean z) {
            long min;
            synchronized (e.this) {
                e.this.k.c();
                while (e.this.f291b <= 0 && !this.e && !this.d && e.this.l == null) {
                    try {
                        e.this.l();
                    } catch (Throwable th) {
                        e.this.k.b();
                        throw th;
                    }
                }
                e.this.k.b();
                e.this.k();
                min = Math.min(e.this.f291b, this.f295c.b());
                e.this.f291b -= min;
            }
            e.this.k.c();
            try {
                e.this.f.a(e.this.e, z && min == this.f295c.b(), this.f295c, min);
            } finally {
                e.this.k.b();
            }
        }

        public s a() {
            return e.this.k;
        }

        public void a_(c.c cVar, long j) {
            if (f293a || !Thread.holdsLock(e.this)) {
                this.f295c.a_(cVar, j);
                while (this.f295c.b() >= 16384) {
                    a(false);
                }
                return;
            }
            throw new AssertionError();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.a.a.d.a(int, boolean, c.c, long):void
         arg types: [int, int, ?[OBJECT, ARRAY], int]
         candidates:
          b.a.a.d.a(int, java.util.List<b.a.a.f>, boolean, boolean):b.a.a.e
          b.a.a.d.a(int, c.e, int, boolean):void
          b.a.a.d.a(b.a.a.d, int, java.util.List, boolean):void
          b.a.a.d.a(boolean, int, int, b.a.a.l):void
          b.a.a.d.a(int, boolean, c.c, long):void */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
            if (r6.f294b.f292c.e != false) goto L_0x0052;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
            if (r6.f295c.b() <= 0) goto L_0x0042;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
            if (r6.f295c.b() <= 0) goto L_0x0052;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x003b, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0042, code lost:
            b.a.a.e.a(r6.f294b).a(b.a.a.e.b(r6.f294b), true, (c.c) null, 0L);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
            r1 = r6.f294b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            r6.d = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0058, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0059, code lost:
            b.a.a.e.a(r6.f294b).c();
            b.a.a.e.f(r6.f294b);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() {
            /*
                r6 = this;
                r4 = 0
                r2 = 1
                boolean r0 = b.a.a.e.a.f293a
                if (r0 != 0) goto L_0x0015
                b.a.a.e r0 = b.a.a.e.this
                boolean r0 = java.lang.Thread.holdsLock(r0)
                if (r0 == 0) goto L_0x0015
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x0015:
                b.a.a.e r1 = b.a.a.e.this
                monitor-enter(r1)
                boolean r0 = r6.d     // Catch:{ all -> 0x003f }
                if (r0 == 0) goto L_0x001e
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
            L_0x001d:
                return
            L_0x001e:
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
                b.a.a.e r0 = b.a.a.e.this
                b.a.a.e$a r0 = r0.f292c
                boolean r0 = r0.e
                if (r0 != 0) goto L_0x0052
                c.c r0 = r6.f295c
                long r0 = r0.b()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0042
            L_0x0031:
                c.c r0 = r6.f295c
                long r0 = r0.b()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0052
                r6.a(r2)
                goto L_0x0031
            L_0x003f:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
                throw r0
            L_0x0042:
                b.a.a.e r0 = b.a.a.e.this
                b.a.a.d r0 = r0.f
                b.a.a.e r1 = b.a.a.e.this
                int r1 = r1.e
                r3 = 0
                r0.a(r1, r2, r3, r4)
            L_0x0052:
                b.a.a.e r1 = b.a.a.e.this
                monitor-enter(r1)
                r0 = 1
                r6.d = r0     // Catch:{ all -> 0x0068 }
                monitor-exit(r1)     // Catch:{ all -> 0x0068 }
                b.a.a.e r0 = b.a.a.e.this
                b.a.a.d r0 = r0.f
                r0.c()
                b.a.a.e r0 = b.a.a.e.this
                r0.j()
                goto L_0x001d
            L_0x0068:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0068 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.a.e.a.close():void");
        }

        public void flush() {
            if (f293a || !Thread.holdsLock(e.this)) {
                synchronized (e.this) {
                    e.this.k();
                }
                while (this.f295c.b() > 0) {
                    a(false);
                    e.this.f.c();
                }
                return;
            }
            throw new AssertionError();
        }
    }

    private final class b implements r {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ boolean f296a = (!e.class.desiredAssertionStatus());

        /* renamed from: c  reason: collision with root package name */
        private final c.c f298c;
        private final c.c d;
        private final long e;
        /* access modifiers changed from: private */
        public boolean f;
        /* access modifiers changed from: private */
        public boolean g;

        private b(long j) {
            this.f298c = new c.c();
            this.d = new c.c();
            this.e = j;
        }

        private void b() {
            e.this.j.c();
            while (this.d.b() == 0 && !this.g && !this.f && e.this.l == null) {
                try {
                    e.this.l();
                } finally {
                    e.this.j.b();
                }
            }
        }

        private void c() {
            if (this.f) {
                throw new IOException("stream closed");
            } else if (e.this.l != null) {
                throw new IOException("stream was reset: " + e.this.l);
            }
        }

        public long a(c.c cVar, long j) {
            long a2;
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            }
            synchronized (e.this) {
                b();
                c();
                if (this.d.b() == 0) {
                    a2 = -1;
                } else {
                    a2 = this.d.a(cVar, Math.min(j, this.d.b()));
                    e.this.f290a += a2;
                    if (e.this.f290a >= ((long) (e.this.f.e.f(65536) / 2))) {
                        e.this.f.a(e.this.e, e.this.f290a);
                        e.this.f290a = 0;
                    }
                    synchronized (e.this.f) {
                        e.this.f.f264c += a2;
                        if (e.this.f.f264c >= ((long) (e.this.f.e.f(65536) / 2))) {
                            e.this.f.a(0, e.this.f.f264c);
                            e.this.f.f264c = 0;
                        }
                    }
                }
            }
            return a2;
        }

        public s a() {
            return e.this.j;
        }

        /* access modifiers changed from: package-private */
        public void a(c.e eVar, long j) {
            boolean z;
            boolean z2;
            if (f296a || !Thread.holdsLock(e.this)) {
                while (j > 0) {
                    synchronized (e.this) {
                        z = this.g;
                        z2 = this.d.b() + j > this.e;
                    }
                    if (z2) {
                        eVar.g(j);
                        e.this.b(a.FLOW_CONTROL_ERROR);
                        return;
                    } else if (z) {
                        eVar.g(j);
                        return;
                    } else {
                        long a2 = eVar.a(this.f298c, j);
                        if (a2 == -1) {
                            throw new EOFException();
                        }
                        j -= a2;
                        synchronized (e.this) {
                            boolean z3 = this.d.b() == 0;
                            this.d.a(this.f298c);
                            if (z3) {
                                e.this.notifyAll();
                            }
                        }
                    }
                }
                return;
            }
            throw new AssertionError();
        }

        public void close() {
            synchronized (e.this) {
                this.f = true;
                this.d.s();
                e.this.notifyAll();
            }
            e.this.j();
        }
    }

    class c extends c.a {
        c() {
        }

        /* access modifiers changed from: protected */
        public IOException a(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        /* access modifiers changed from: protected */
        public void a() {
            e.this.b(a.CANCEL);
        }

        public void b() {
            if (a_()) {
                throw a((IOException) null);
            }
        }
    }

    e(int i2, d dVar, boolean z, boolean z2, List<f> list) {
        if (dVar == null) {
            throw new NullPointerException("connection == null");
        } else if (list == null) {
            throw new NullPointerException("requestHeaders == null");
        } else {
            this.e = i2;
            this.f = dVar;
            this.f291b = (long) dVar.f.f(65536);
            this.i = new b((long) dVar.e.f(65536));
            this.f292c = new a();
            boolean unused = this.i.g = z2;
            boolean unused2 = this.f292c.e = z;
            this.g = list;
        }
    }

    private boolean d(a aVar) {
        if (d || !Thread.holdsLock(this)) {
            synchronized (this) {
                if (this.l != null) {
                    return false;
                }
                if (this.i.g && this.f292c.e) {
                    return false;
                }
                this.l = aVar;
                notifyAll();
                this.f.b(this.e);
                return true;
            }
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: private */
    public void j() {
        boolean z;
        boolean b2;
        if (d || !Thread.holdsLock(this)) {
            synchronized (this) {
                z = !this.i.g && this.i.f && (this.f292c.e || this.f292c.d);
                b2 = b();
            }
            if (z) {
                a(a.CANCEL);
            } else if (!b2) {
                this.f.b(this.e);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.f292c.d) {
            throw new IOException("stream closed");
        } else if (this.f292c.e) {
            throw new IOException("stream finished");
        } else if (this.l != null) {
            throw new IOException("stream was reset: " + this.l);
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        try {
            wait();
        } catch (InterruptedException e2) {
            throw new InterruptedIOException();
        }
    }

    public int a() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.f291b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    public void a(a aVar) {
        if (d(aVar)) {
            this.f.b(this.e, aVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(c.e eVar, int i2) {
        if (d || !Thread.holdsLock(this)) {
            this.i.a(eVar, (long) i2);
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void a(List<f> list, g gVar) {
        if (d || !Thread.holdsLock(this)) {
            a aVar = null;
            boolean z = true;
            synchronized (this) {
                if (this.h == null) {
                    if (gVar.c()) {
                        aVar = a.PROTOCOL_ERROR;
                    } else {
                        this.h = list;
                        z = b();
                        notifyAll();
                    }
                } else if (gVar.d()) {
                    aVar = a.STREAM_IN_USE;
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(this.h);
                    arrayList.addAll(list);
                    this.h = arrayList;
                }
            }
            if (aVar != null) {
                b(aVar);
            } else if (!z) {
                this.f.b(this.e);
            }
        } else {
            throw new AssertionError();
        }
    }

    public void b(a aVar) {
        if (d(aVar)) {
            this.f.a(this.e, aVar);
        }
    }

    public synchronized boolean b() {
        boolean z = false;
        synchronized (this) {
            if (this.l == null) {
                if ((!this.i.g && !this.i.f) || ((!this.f292c.e && !this.f292c.d) || this.h == null)) {
                    z = true;
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public synchronized void c(a aVar) {
        if (this.l == null) {
            this.l = aVar;
            notifyAll();
        }
    }

    public boolean c() {
        return this.f.f263b == ((this.e & 1) == 1);
    }

    public synchronized List<f> d() {
        this.j.c();
        while (this.h == null && this.l == null) {
            try {
                l();
            } catch (Throwable th) {
                this.j.b();
                throw th;
            }
        }
        this.j.b();
        if (this.h != null) {
        } else {
            throw new IOException("stream was reset: " + this.l);
        }
        return this.h;
    }

    public s e() {
        return this.j;
    }

    public s f() {
        return this.k;
    }

    public r g() {
        return this.i;
    }

    public q h() {
        synchronized (this) {
            if (this.h == null && !c()) {
                throw new IllegalStateException("reply before requesting the sink");
            }
        }
        return this.f292c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.e.b.a(b.a.a.e$b, boolean):boolean
     arg types: [b.a.a.e$b, int]
     candidates:
      b.a.a.e.b.a(c.c, long):long
      b.a.a.e.b.a(c.e, long):void
      c.r.a(c.c, long):long
      b.a.a.e.b.a(b.a.a.e$b, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void i() {
        boolean b2;
        if (d || !Thread.holdsLock(this)) {
            synchronized (this) {
                boolean unused = this.i.g = true;
                b2 = b();
                notifyAll();
            }
            if (!b2) {
                this.f.b(this.e);
                return;
            }
            return;
        }
        throw new AssertionError();
    }
}
