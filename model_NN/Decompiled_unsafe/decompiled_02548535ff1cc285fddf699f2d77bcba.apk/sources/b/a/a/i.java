package b.a.a;

import b.a.a.b;
import b.a.a.h;
import c.e;
import c.f;
import c.r;
import c.s;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class i implements p {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Logger f312a = Logger.getLogger(b.class.getName());
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final f f313b = f.a("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");

    static final class a implements r {

        /* renamed from: a  reason: collision with root package name */
        int f314a;

        /* renamed from: b  reason: collision with root package name */
        byte f315b;

        /* renamed from: c  reason: collision with root package name */
        int f316c;
        int d;
        short e;
        private final e f;

        public a(e eVar) {
            this.f = eVar;
        }

        private void b() {
            int i = this.f316c;
            int a2 = i.b(this.f);
            this.d = a2;
            this.f314a = a2;
            byte h = (byte) (this.f.h() & 255);
            this.f315b = (byte) (this.f.h() & 255);
            if (i.f312a.isLoggable(Level.FINE)) {
                i.f312a.fine(b.a(true, this.f316c, this.f314a, h, this.f315b));
            }
            this.f316c = this.f.j() & Integer.MAX_VALUE;
            if (h != 9) {
                throw i.d("%s != TYPE_CONTINUATION", Byte.valueOf(h));
            } else if (this.f316c != i) {
                throw i.d("TYPE_CONTINUATION streamId changed", new Object[0]);
            }
        }

        public long a(c.c cVar, long j) {
            while (this.d == 0) {
                this.f.g((long) this.e);
                this.e = 0;
                if ((this.f315b & 4) != 0) {
                    return -1;
                }
                b();
            }
            long a2 = this.f.a(cVar, Math.min(j, (long) this.d));
            if (a2 == -1) {
                return -1;
            }
            this.d = (int) (((long) this.d) - a2);
            return a2;
        }

        public s a() {
            return this.f.a();
        }

        public void close() {
        }
    }

    static final class b {

        /* renamed from: a  reason: collision with root package name */
        private static final String[] f317a = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};

        /* renamed from: b  reason: collision with root package name */
        private static final String[] f318b = new String[64];

        /* renamed from: c  reason: collision with root package name */
        private static final String[] f319c = new String[256];

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
         arg types: [int, int]
         candidates:
          ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
          ClspMth{java.lang.String.replace(char, char):java.lang.String} */
        static {
            for (int i = 0; i < f319c.length; i++) {
                f319c[i] = String.format("%8s", Integer.toBinaryString(i)).replace(' ', '0');
            }
            f318b[0] = "";
            f318b[1] = "END_STREAM";
            int[] iArr = {1};
            f318b[8] = "PADDED";
            for (int i2 : iArr) {
                f318b[i2 | 8] = f318b[i2] + "|PADDED";
            }
            f318b[4] = "END_HEADERS";
            f318b[32] = "PRIORITY";
            f318b[36] = "END_HEADERS|PRIORITY";
            for (int i3 : new int[]{4, 32, 36}) {
                for (int i4 : iArr) {
                    f318b[i4 | i3] = f318b[i4] + '|' + f318b[i3];
                    f318b[i4 | i3 | 8] = f318b[i4] + '|' + f318b[i3] + "|PADDED";
                }
            }
            for (int i5 = 0; i5 < f318b.length; i5++) {
                if (f318b[i5] == null) {
                    f318b[i5] = f319c[i5];
                }
            }
        }

        b() {
        }

        static String a(byte b2, byte b3) {
            if (b3 == 0) {
                return "";
            }
            switch (b2) {
                case 2:
                case 3:
                case 7:
                case 8:
                    return f319c[b3];
                case 4:
                case 6:
                    return b3 == 1 ? "ACK" : f319c[b3];
                case 5:
                default:
                    String str = b3 < f318b.length ? f318b[b3] : f319c[b3];
                    return (b2 != 5 || (b3 & 4) == 0) ? (b2 != 0 || (b3 & 32) == 0) ? str : str.replace("PRIORITY", "COMPRESSED") : str.replace("HEADERS", "PUSH_PROMISE");
            }
        }

        static String a(boolean z, int i, int i2, byte b2, byte b3) {
            String format = b2 < f317a.length ? f317a[b2] : String.format("0x%02x", Byte.valueOf(b2));
            String a2 = a(b2, b3);
            Object[] objArr = new Object[5];
            objArr[0] = z ? "<<" : ">>";
            objArr[1] = Integer.valueOf(i);
            objArr[2] = Integer.valueOf(i2);
            objArr[3] = format;
            objArr[4] = a2;
            return String.format("%s 0x%08x %5d %-13s %s", objArr);
        }
    }

    static final class c implements b {

        /* renamed from: a  reason: collision with root package name */
        final h.a f320a;

        /* renamed from: b  reason: collision with root package name */
        private final e f321b;

        /* renamed from: c  reason: collision with root package name */
        private final a f322c = new a(this.f321b);
        private final boolean d;

        c(e eVar, int i, boolean z) {
            this.f321b = eVar;
            this.d = z;
            this.f320a = new h.a(i, this.f322c);
        }

        private List<f> a(int i, short s, byte b2, int i2) {
            a aVar = this.f322c;
            this.f322c.d = i;
            aVar.f314a = i;
            this.f322c.e = s;
            this.f322c.f315b = b2;
            this.f322c.f316c = i2;
            this.f320a.a();
            return this.f320a.b();
        }

        private void a(b.a aVar, int i) {
            int j = this.f321b.j();
            aVar.a(i, j & Integer.MAX_VALUE, (this.f321b.h() & 255) + 1, (Integer.MIN_VALUE & j) != 0);
        }

        private void a(b.a aVar, int i, byte b2, int i2) {
            if (i2 == 0) {
                throw i.d("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
            }
            boolean z = (b2 & 1) != 0;
            short h = (b2 & 8) != 0 ? (short) (this.f321b.h() & 255) : 0;
            if ((b2 & 32) != 0) {
                a(aVar, i2);
                i -= 5;
            }
            aVar.a(false, z, i2, -1, a(i.b(i, b2, h), h, b2, i2), g.HTTP_20_HEADERS);
        }

        private void b(b.a aVar, int i, byte b2, int i2) {
            boolean z = true;
            short s = 0;
            boolean z2 = (b2 & 1) != 0;
            if ((b2 & 32) == 0) {
                z = false;
            }
            if (z) {
                throw i.d("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
            }
            if ((b2 & 8) != 0) {
                s = (short) (this.f321b.h() & 255);
            }
            aVar.a(z2, i2, this.f321b, i.b(i, b2, s));
            this.f321b.g((long) s);
        }

        private void c(b.a aVar, int i, byte b2, int i2) {
            if (i != 5) {
                throw i.d("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
            } else if (i2 == 0) {
                throw i.d("TYPE_PRIORITY streamId == 0", new Object[0]);
            } else {
                a(aVar, i2);
            }
        }

        private void d(b.a aVar, int i, byte b2, int i2) {
            if (i != 4) {
                throw i.d("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
            } else if (i2 == 0) {
                throw i.d("TYPE_RST_STREAM streamId == 0", new Object[0]);
            } else {
                int j = this.f321b.j();
                a b3 = a.b(j);
                if (b3 == null) {
                    throw i.d("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(j));
                } else {
                    aVar.a(i2, b3);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.a.a.b.a.a(boolean, b.a.a.n):void
         arg types: [int, b.a.a.n]
         candidates:
          b.a.a.b.a.a(int, long):void
          b.a.a.b.a.a(int, b.a.a.a):void
          b.a.a.b.a.a(boolean, b.a.a.n):void */
        private void e(b.a aVar, int i, byte b2, int i2) {
            if (i2 != 0) {
                throw i.d("TYPE_SETTINGS streamId != 0", new Object[0]);
            } else if ((b2 & 1) != 0) {
                if (i != 0) {
                    throw i.d("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
                }
                aVar.a();
            } else if (i % 6 != 0) {
                throw i.d("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
            } else {
                n nVar = new n();
                for (int i3 = 0; i3 < i; i3 += 6) {
                    short i4 = this.f321b.i();
                    int j = this.f321b.j();
                    switch (i4) {
                        case 1:
                        case 6:
                            break;
                        case 2:
                            if (!(j == 0 || j == 1)) {
                                throw i.d("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                            }
                        case 3:
                            i4 = 4;
                            break;
                        case 4:
                            i4 = 7;
                            if (j >= 0) {
                                break;
                            } else {
                                throw i.d("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                            }
                        case 5:
                            if (j >= 16384 && j <= 16777215) {
                                break;
                            } else {
                                throw i.d("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(j));
                            }
                        default:
                            throw i.d("PROTOCOL_ERROR invalid settings id: %s", Short.valueOf(i4));
                    }
                    nVar.a(i4, 0, j);
                }
                aVar.a(false, nVar);
                if (nVar.c() >= 0) {
                    this.f320a.a(nVar.c());
                }
            }
        }

        private void f(b.a aVar, int i, byte b2, int i2) {
            short s = 0;
            if (i2 == 0) {
                throw i.d("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
            }
            if ((b2 & 8) != 0) {
                s = (short) (this.f321b.h() & 255);
            }
            aVar.a(i2, this.f321b.j() & Integer.MAX_VALUE, a(i.b(i - 4, b2, s), s, b2, i2));
        }

        private void g(b.a aVar, int i, byte b2, int i2) {
            boolean z = true;
            if (i != 8) {
                throw i.d("TYPE_PING length != 8: %s", Integer.valueOf(i));
            } else if (i2 != 0) {
                throw i.d("TYPE_PING streamId != 0", new Object[0]);
            } else {
                int j = this.f321b.j();
                int j2 = this.f321b.j();
                if ((b2 & 1) == 0) {
                    z = false;
                }
                aVar.a(z, j, j2);
            }
        }

        private void h(b.a aVar, int i, byte b2, int i2) {
            if (i < 8) {
                throw i.d("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
            } else if (i2 != 0) {
                throw i.d("TYPE_GOAWAY streamId != 0", new Object[0]);
            } else {
                int j = this.f321b.j();
                int j2 = this.f321b.j();
                int i3 = i - 8;
                a b3 = a.b(j2);
                if (b3 == null) {
                    throw i.d("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(j2));
                }
                f fVar = f.f572b;
                if (i3 > 0) {
                    fVar = this.f321b.c((long) i3);
                }
                aVar.a(j, b3, fVar);
            }
        }

        private void i(b.a aVar, int i, byte b2, int i2) {
            if (i != 4) {
                throw i.d("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
            }
            long j = ((long) this.f321b.j()) & 2147483647L;
            if (j == 0) {
                throw i.d("windowSizeIncrement was 0", Long.valueOf(j));
            } else {
                aVar.a(i2, j);
            }
        }

        public void a() {
            if (!this.d) {
                f c2 = this.f321b.c((long) i.f313b.f());
                if (i.f312a.isLoggable(Level.FINE)) {
                    i.f312a.fine(String.format("<< CONNECTION %s", c2.d()));
                }
                if (!i.f313b.equals(c2)) {
                    throw i.d("Expected a connection header but was %s", c2.a());
                }
            }
        }

        public boolean a(b.a aVar) {
            try {
                this.f321b.a(9);
                int a2 = i.b(this.f321b);
                if (a2 < 0 || a2 > 16384) {
                    throw i.d("FRAME_SIZE_ERROR: %s", Integer.valueOf(a2));
                }
                byte h = (byte) (this.f321b.h() & 255);
                byte h2 = (byte) (this.f321b.h() & 255);
                int j = this.f321b.j() & Integer.MAX_VALUE;
                if (i.f312a.isLoggable(Level.FINE)) {
                    i.f312a.fine(b.a(true, j, a2, h, h2));
                }
                switch (h) {
                    case 0:
                        b(aVar, a2, h2, j);
                        return true;
                    case 1:
                        a(aVar, a2, h2, j);
                        return true;
                    case 2:
                        c(aVar, a2, h2, j);
                        return true;
                    case 3:
                        d(aVar, a2, h2, j);
                        return true;
                    case 4:
                        e(aVar, a2, h2, j);
                        return true;
                    case 5:
                        f(aVar, a2, h2, j);
                        return true;
                    case 6:
                        g(aVar, a2, h2, j);
                        return true;
                    case 7:
                        h(aVar, a2, h2, j);
                        return true;
                    case 8:
                        i(aVar, a2, h2, j);
                        return true;
                    default:
                        this.f321b.g((long) a2);
                        return true;
                }
            } catch (IOException e) {
                return false;
            }
        }

        public void close() {
            this.f321b.close();
        }
    }

    static final class d implements c {

        /* renamed from: a  reason: collision with root package name */
        private final c.d f323a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f324b;

        /* renamed from: c  reason: collision with root package name */
        private final c.c f325c = new c.c();
        private final h.b d = new h.b(this.f325c);
        private int e = 16384;
        private boolean f;

        d(c.d dVar, boolean z) {
            this.f323a = dVar;
            this.f324b = z;
        }

        private void b(int i, long j) {
            while (j > 0) {
                int min = (int) Math.min((long) this.e, j);
                j -= (long) min;
                a(i, min, (byte) 9, j == 0 ? (byte) 4 : 0);
                this.f323a.a_(this.f325c, (long) min);
            }
        }

        public synchronized void a() {
            if (this.f) {
                throw new IOException("closed");
            } else if (this.f324b) {
                if (i.f312a.isLoggable(Level.FINE)) {
                    i.f312a.fine(String.format(">> CONNECTION %s", i.f313b.d()));
                }
                this.f323a.c(i.f313b.g());
                this.f323a.flush();
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i, byte b2, c.c cVar, int i2) {
            a(i, i2, (byte) 0, b2);
            if (i2 > 0) {
                this.f323a.a_(cVar, (long) i2);
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2, byte b2, byte b3) {
            if (i.f312a.isLoggable(Level.FINE)) {
                i.f312a.fine(b.a(false, i, i2, b2, b3));
            }
            if (i2 > this.e) {
                throw i.c("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(this.e), Integer.valueOf(i2));
            } else if ((Integer.MIN_VALUE & i) != 0) {
                throw i.c("reserved bit set: %s", Integer.valueOf(i));
            } else {
                i.b(this.f323a, i2);
                this.f323a.h(b2 & 255);
                this.f323a.h(b3 & 255);
                this.f323a.f(Integer.MAX_VALUE & i);
            }
        }

        public synchronized void a(int i, int i2, List<f> list) {
            if (this.f) {
                throw new IOException("closed");
            }
            this.d.a(list);
            long b2 = this.f325c.b();
            int min = (int) Math.min((long) (this.e - 4), b2);
            a(i, min + 4, (byte) 5, b2 == ((long) min) ? (byte) 4 : 0);
            this.f323a.f(Integer.MAX_VALUE & i2);
            this.f323a.a_(this.f325c, (long) min);
            if (b2 > ((long) min)) {
                b(i, b2 - ((long) min));
            }
        }

        public synchronized void a(int i, long j) {
            if (this.f) {
                throw new IOException("closed");
            } else if (j == 0 || j > 2147483647L) {
                throw i.c("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
            } else {
                a(i, 4, (byte) 8, (byte) 0);
                this.f323a.f((int) j);
                this.f323a.flush();
            }
        }

        public synchronized void a(int i, a aVar) {
            if (this.f) {
                throw new IOException("closed");
            } else if (aVar.s == -1) {
                throw new IllegalArgumentException();
            } else {
                a(i, 4, (byte) 3, (byte) 0);
                this.f323a.f(aVar.s);
                this.f323a.flush();
            }
        }

        public synchronized void a(int i, a aVar, byte[] bArr) {
            if (this.f) {
                throw new IOException("closed");
            } else if (aVar.s == -1) {
                throw i.c("errorCode.httpCode == -1", new Object[0]);
            } else {
                a(0, bArr.length + 8, (byte) 7, (byte) 0);
                this.f323a.f(i);
                this.f323a.f(aVar.s);
                if (bArr.length > 0) {
                    this.f323a.c(bArr);
                }
                this.f323a.flush();
            }
        }

        public synchronized void a(n nVar) {
            if (this.f) {
                throw new IOException("closed");
            }
            this.e = nVar.e(this.e);
            a(0, 0, (byte) 4, (byte) 1);
            this.f323a.flush();
        }

        public synchronized void a(boolean z, int i, int i2) {
            byte b2 = 0;
            synchronized (this) {
                if (this.f) {
                    throw new IOException("closed");
                }
                if (z) {
                    b2 = 1;
                }
                a(0, 8, (byte) 6, b2);
                this.f323a.f(i);
                this.f323a.f(i2);
                this.f323a.flush();
            }
        }

        public synchronized void a(boolean z, int i, c.c cVar, int i2) {
            if (this.f) {
                throw new IOException("closed");
            }
            byte b2 = 0;
            if (z) {
                b2 = (byte) 1;
            }
            a(i, b2, cVar, i2);
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z, int i, List<f> list) {
            if (this.f) {
                throw new IOException("closed");
            }
            this.d.a(list);
            long b2 = this.f325c.b();
            int min = (int) Math.min((long) this.e, b2);
            byte b3 = b2 == ((long) min) ? (byte) 4 : 0;
            if (z) {
                b3 = (byte) (b3 | 1);
            }
            a(i, min, (byte) 1, b3);
            this.f323a.a_(this.f325c, (long) min);
            if (b2 > ((long) min)) {
                b(i, b2 - ((long) min));
            }
        }

        public synchronized void a(boolean z, boolean z2, int i, int i2, List<f> list) {
            if (z2) {
                throw new UnsupportedOperationException();
            } else if (this.f) {
                throw new IOException("closed");
            } else {
                a(z, i, list);
            }
        }

        public synchronized void b() {
            if (this.f) {
                throw new IOException("closed");
            }
            this.f323a.flush();
        }

        public synchronized void b(n nVar) {
            int i = 0;
            synchronized (this) {
                if (this.f) {
                    throw new IOException("closed");
                }
                a(0, nVar.b() * 6, (byte) 4, (byte) 0);
                while (i < 10) {
                    if (nVar.a(i)) {
                        this.f323a.g(i == 4 ? 3 : i == 7 ? 4 : i);
                        this.f323a.f(nVar.b(i));
                    }
                    i++;
                }
                this.f323a.flush();
            }
        }

        public int c() {
            return this.e;
        }

        public synchronized void close() {
            this.f = true;
            this.f323a.close();
        }
    }

    /* access modifiers changed from: private */
    public static int b(int i, byte b2, short s) {
        if ((b2 & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        throw d("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
    }

    /* access modifiers changed from: private */
    public static int b(e eVar) {
        return ((eVar.h() & 255) << 16) | ((eVar.h() & 255) << 8) | (eVar.h() & 255);
    }

    /* access modifiers changed from: private */
    public static void b(c.d dVar, int i) {
        dVar.h((i >>> 16) & 255);
        dVar.h((i >>> 8) & 255);
        dVar.h(i & 255);
    }

    /* access modifiers changed from: private */
    public static IllegalArgumentException c(String str, Object... objArr) {
        throw new IllegalArgumentException(String.format(str, objArr));
    }

    /* access modifiers changed from: private */
    public static IOException d(String str, Object... objArr) {
        throw new IOException(String.format(str, objArr));
    }

    public b a(e eVar, boolean z) {
        return new c(eVar, 4096, z);
    }

    public c a(c.d dVar, boolean z) {
        return new d(dVar, z);
    }
}
